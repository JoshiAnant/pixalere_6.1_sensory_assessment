-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.11-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table test_live.sensory_assessment
DROP TABLE IF EXISTS `sensory_assessment`;
CREATE TABLE IF NOT EXISTS `sensory_assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `deleted_on` datetime(6) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `visited_on` datetime DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `skin_leftfoot` int(11) DEFAULT NULL,
  `skin_rightfoot` int(11) DEFAULT NULL,
  `skin_comments` text,
  `nails_leftfoot` int(11) DEFAULT NULL,
  `nails_rightfoot` int(11) DEFAULT NULL,
  `nails_comments` text,
  `deformity_leftfoot` int(11) DEFAULT NULL,
  `deformity_rightfoot` int(11) DEFAULT NULL,
  `deformity_comments` text,
  `footwear_leftfoot` int(11) DEFAULT NULL,
  `footwear_rightfoot` int(11) DEFAULT NULL,
  `footwear_comments` text,
  `tempcold_leftfoot` int(11) DEFAULT NULL,
  `tempcold_rightfoot` int(11) DEFAULT NULL,
  `temp_cold_comments` text,
  `temphot_leftfoot` int(11) DEFAULT NULL,
  `temphot_rightfoot` int(11) DEFAULT NULL,
  `temphot_comments` text,
  `rangeofmotion_leftfoot` int(11) DEFAULT NULL,
  `rangeofmotion_rightfoot` int(11) DEFAULT NULL,
  `rangeofmotion_comments` text,
  `sensation_monofilament_leftfoot` int(11) DEFAULT NULL,
  `sensation_monofilament_rightfoot` int(11) DEFAULT NULL,
  `sensation_monofilament_comments` text,
  `sensation_question_leftfoot` int(11) DEFAULT NULL,
  `sensation_question_rightfoot` int(11) DEFAULT NULL,
  `sensation_question_comments` text,
  `pedal_pulse_leftfoot` int(11) DEFAULT NULL,
  `pedal_pulse_rightfoot` int(11) DEFAULT NULL,
  `pedal_pulse_comments` text,
  `dependent_rubor_leftfoot` int(11) DEFAULT NULL,
  `dependent_rubor_rightfoot` int(11) DEFAULT NULL,
  `dependent_rubor_comments` text,
  `erythema_leftfoot` int(11) DEFAULT NULL,
  `erythema_rightfoot` int(11) DEFAULT NULL,
  `erythema_comments` text,
  `score_totals` int(255) DEFAULT NULL,
  `delete_signature` varchar(255) DEFAULT NULL,
  `user_signature` text,
  `professional_id` int(11) NOT NULL,
  `leftfoot_score` int(11) NOT NULL,
  `rightfoot_score` int(11) NOT NULL,
  `leftfoot_image_score` int(11) DEFAULT NULL,
  `rightfoot_image_score` int(11) DEFAULT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table test_live.sensory_assessment: ~0 rows (approximately)
/*!40000 ALTER TABLE `sensory_assessment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensory_assessment` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
