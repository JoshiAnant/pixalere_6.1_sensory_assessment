-- Scheduler Tables
CREATE TABLE report_schedule (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `active` int(11) NOT NULL DEFAULT '0',
    `created_on` datetime(6) DEFAULT NULL,
    `report_type` varchar(80) default '',
    `user_language` int(11) DEFAULT NULL,
    `display_locations` int(11) DEFAULT NULL,
    `use_title` int(11) DEFAULT NULL,
    `custom_title` varchar(255) default '',
    `recipients_emails` varchar(512) default '',
    `enable_reminder` int(11) DEFAULT NULL,
    `reminder_limit` int(11) DEFAULT NULL,
    `reminder_emails` varchar(255) default '',
    PRIMARY KEY (`id`),
    KEY `id` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `schedule_location` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `report_schedule_id` int(11) NOT NULL,
    `treatment_location_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `id` (`id`),
    KEY `report_schedule_id` (`report_schedule_id`),
    CONSTRAINT `report_schedule_loc_ibfk_1` 
    FOREIGN KEY (`report_schedule_id`) 
    REFERENCES `report_schedule` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `schedule_config` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `report_schedule_id` int(11) NOT NULL,
    `execution_on` datetime(6) DEFAULT NULL,
    `created_on` datetime(6) DEFAULT NULL,
    `start_date` date DEFAULT NULL,
    `end_date` date DEFAULT NULL,
    `extra_options` varchar(255) default '',
    `processed` int(11) DEFAULT NULL,
    `exit_status` varchar(512) default '',
    PRIMARY KEY (`id`),
    KEY `id` (`id`),
    KEY `report_schedule_id` (`report_schedule_id`),
    CONSTRAINT `report_schedule_conf_ibfk_1` 
    FOREIGN KEY (`report_schedule_id`) 
    REFERENCES `report_schedule` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `schedule_etiology` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `report_schedule_id` int(11) NOT NULL,
    `lookup_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `id` (`id`),
    KEY `report_schedule_id` (`report_schedule_id`),
    CONSTRAINT `report_schedule_etiology_ibfk_1` 
    FOREIGN KEY (`report_schedule_id`) 
    REFERENCES `report_schedule` (`id`)
) ENGINE=InnoDB;