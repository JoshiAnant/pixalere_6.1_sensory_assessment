alter table assessment_npwt  add vendor_name int;
alter table assessment_npwt add machine_acquirement int;
delete from components where field_name='site';
delete from components where field_name='left_foot_toes' and flowchart=12;
delete from components where field_name='right_foot_toes' and flowchart=12;
update wounds set status='Closed' where active=1 and status IS NULL and NOT EXISTS(select id from wound_profile_type where closed!=1 and wound_id=wounds.id) and EXISTs (select id from wound_profile_type where wound_id=wounds.id);
update assessment_wound set closed_date=NULL where closed_date='1970-01-01';
update components set required=0 where field_name='pain_comments';
insert into resources (id,name) values (201,'NPWT Vendor');
insert into lookup (active,resource_id) values(1,201);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'KCI',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,201);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select  1,id,'S&N',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0,1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
insert into resources (id,name) values (202,'NPWT Machine');
insert into lookup (active,resource_id) values(1,202);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select  1,id,'Owned',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,202);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select  1,id,'Rented',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',3, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',4, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',5, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',6, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',17, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0,1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',50, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 180, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Blisters', 180 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 180, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Skin Intact', 180 from lookup order by id desc  limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 180, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Dry', 180 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 180, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Rash', 180 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 180, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Skin Tears', 180 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 180, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Redness', 180 from lookup order by id desc  limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 183, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Heal Skin', 183 from lookup order by id desc  limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 183, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Observation', 183 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 181, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Active', 181 from lookup order by id desc  limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 181, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Closed', 181 from lookup order by id desc  limit 1;
delete from lookup_l10n where lookup_id=200;
delete from lookup_l10n where lookup_id=201;
delete from lookup where id=200;
delete from lookup where id=201;
insert into lookup (id,active,resource_id,locked) values(200,1,50,0);
insert into lookup_l10n (language_id,lookup_id,name,resource_id) values (1,200,'Training Area',50);
insert into lookup (id,active,resource_id,category_id,locked) values(201,1,51,5,0);
insert into lookup_l10n (language_id,lookup_id,name,resource_id) values (1,201,'Training Location',51);
update lookup set resource_id=51,category_id=200 where id=201;
update lookup set resource_id=50 where id=200;
update lookup set active=0 where id=990 or id=612;
update components set field_name='phn2' where field_name='secondary_phn';
update components set field_name='phn3' where field_name='third_phn';
