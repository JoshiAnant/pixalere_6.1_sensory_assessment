# Default language configuration entry
INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    (0, 1, 1, 'defaultLanguage', '', '', 'DRP', 'en,es,fr', '', 'en', 0);

# Add field language For enqueued reports
ALTER TABLE report_queue add user_language int(11) DEFAULT NULL;