update components set component_type='DATETIME',validation='time:{time:true}',validation_message='time : "Time must be in format 23:59"' where field_name='visited_on';
update components set validation='wound_date_year : {canadianDate : {data: ["#wound_date_day","#wound_date_month","#wound_date_year"]}},wound_date_day:"required",wound_date_month:"required"' where field_name='wound_date';
update lookup_l10n set name='Open' where name='Active' AND (resource_id=100 OR resource_id=181 OR resource_id=31 OR resource_id=153 OR resource_id=154 OR resource_id=155 OR resource_id=156);
