# Update of Trigger Mechanism
DROP TABLE trigger_library;
ALTER TABLE products ADD display_image INT(11) DEFAULT NULL;
ALTER TABLE products ADD display_description INT(11) DEFAULT NULL;
ALTER TABLE products ADD pdf VARCHAR(255) DEFAULT NULL;
ALTER TABLE products ADD recommendation_type CHAR(1) DEFAULT NULL;
ALTER TABLE triggers ADD active INT(11) DEFAULT NULL;
ALTER TABLE triggers ADD logic_operator VARCHAR(3) DEFAULT NULL;
# Skin Triggers Components
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (0,'wound_location','STRING','TXT',42,'pixalere.woundprofile.form.wound_location',7,1,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (180,'etiology','STRING','SALM',42,'pixalere.woundprofile.form.skin_etiology',33,1,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (0,'reason_for_care','STRING','TXTA',42,'pixalere.woundprofile.form.reason_for_care',NULL,1,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (183,'goals','STRING','SALP',42,'pixalere.woundprofile.form.skin_goals',34,1,0,0,0,0,NULL, 0,0);
ALTER TABLE triggers MODIFY COLUMN name varchar(90) NULL;
update components set resource_id=189 where resource_id=20 and flowchart=53;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (156,'status','INTEGER','DRP',17,'pixalere.woundassessment.form.wound_status',NULL,1,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (162,'discharge_reason','INTEGER','DRP',17,'pixalere.woundassessment.form.discharge_reason',NULL,2,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (0,'closed_date','STRING','DATE',17,'pixalere.woundassessment.form.closure_date',NULL,3,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (0,'wound_date','STRING','DATE',17,'pixalere.woundassessment.form.date_of_onset',NULL,4,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (0,'pain','INTEGER','DRP',17,'pixalere.woundassessment.form.pain',NULL,5,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (0,'pain_comments','STRING','TXT',17,'pixalere.patientprofile.form.pain_comments',NULL,6,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (157,'burn_wound','STRING','CHKB',17,'pixalere.burnassessment.form.burn_wound',NULL,7,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (158,'exudate_colour','STRING','CHKB',17,'pixalere.burnassessment.form.exudate_colour',NULL,8,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (159,'exudate_amount','STRING','CHKB',17,'pixalere.burnassessment.form.exudate_amount',NULL,9,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (160,'grafts','STRING','CHKB',17,'pixalere.burnassessment.form.grafts',NULL,10,0,0,0,0,NULL, 0,0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,hide_gui) VALUES (161,'donors','STRING','CHKB',17,'pixalere.burnassessment.form.donors',NULL,11,0,0,0,0,NULL, 0,0);
update components set resource_id=22 where flowchart=52 and field_name='left_dorsalis_pedis_doppler';
update components set resource_id=22 where flowchart=52 and field_name='right_dorsalis_pedis_doppler';
update lookup set orderby=1 where resource_id=104 and exists (Select id from lookup_l10n where name like '<b>1.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=2 where resource_id=104 and exists (Select id from lookup_l10n where name like '<b>2.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=3 where resource_id=104 and exists (Select id from lookup_l10n where name like '<b>3.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=4 where resource_id=104 and exists (Select id from lookup_l10n where name like '<b>4.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=1 where resource_id=105 and exists (Select id from lookup_l10n where name like '<b>1.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=2 where resource_id=105 and exists (Select id from lookup_l10n where name like '<b>2.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=3 where resource_id=105 and exists (Select id from lookup_l10n where name like '<b>3.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=4 where resource_id=105 and exists (Select id from lookup_l10n where name like '<b>4.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=1 where resource_id=106 and exists (Select id from lookup_l10n where name like '<b>1.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=2 where resource_id=106 and exists (Select id from lookup_l10n where name like '<b>2.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=3 where resource_id=106 and exists (Select id from lookup_l10n where name like '<b>3.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=4 where resource_id=106 and exists (Select id from lookup_l10n where name like '<b>4.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=1 where resource_id=107 and exists (Select id from lookup_l10n where name like '<b>1.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=2 where resource_id=107 and exists (Select id from lookup_l10n where name like '<b>2.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=3 where resource_id=107 and exists (Select id from lookup_l10n where name like '<b>3.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=4 where resource_id=107 and exists (Select id from lookup_l10n where name like '<b>4.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=1 where resource_id=108 and exists (Select id from lookup_l10n where name like '<b>1.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=2 where resource_id=108 and exists (Select id from lookup_l10n where name like '<b>2.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=3 where resource_id=108 and exists (Select id from lookup_l10n where name like '<b>3.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=4 where resource_id=108 and exists (Select id from lookup_l10n where name like '<b>4.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=1 where resource_id=109 and exists (Select id from lookup_l10n where name like '<b>1.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=2 where resource_id=109 and exists (Select id from lookup_l10n where name like '<b>2.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=3 where resource_id=109 and exists (Select id from lookup_l10n where name like '<b>3.%' and lookup_l10n.lookup_id=lookup.id);
update lookup set orderby=4 where resource_id=109 and exists (Select id from lookup_l10n where name like '<b>4.%' and lookup_l10n.lookup_id=lookup.id);
update components set allow_edit=1 where field_name='patient_limitations';
update components set ignore_info_popup=0 where field_name='push_scale';
update components set required=1 where field_name='status' and flowchart=17;
update components set required=1 where field_name='pain' and flowchart=17;
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (40, 1, 1, 'visitCost', '', '', 'INPN', '', '', '150', 0);
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (0, 66, 1, 'showBarcode', '', '', 'CHKB', '', '', '0', 0);
insert into lookup (active,resource_id) values(1,162);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Burn Closed',resource_id from lookup order by id desc limit 1;
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES ( 7, 16, 0, 'burnClosed', '', '', 'INPN', '', '', '3284', 1);
update configuration set current_value=(select id from lookup order by id desc limit 1) where config_setting='burnClosed';
insert into lookup (active,resource_id) values(1,162);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Other',resource_id from lookup order by id desc limit 1;
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (0, 66, 1, 'allowReferrals', '', '', 'CHKB', '', '', '1', 0);
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (0, 66, 1, 'showSkinPrevention', '', '', 'CHKB', '', '', '0', 0);
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (0, 66, 1, 'hidePixBranding', '', '', 'CHKB', '', '', '0', 0);
update components set resource_id=0 where field_name='exudate_odour' and flowchart=3;
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (0, 66, 1, 'showFoot', '', '', 'CHKB', '', '', '1', 0);
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (0, 66, 1, 'showLimb', '', '', 'CHKB', '', '', '1', 0);
INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (0, 66, 1, 'showInvestigations', '', '', 'CHKB', '', '', '1', 0);

