function loadBraden(patient_id) {
    refwin = window.open('BradenScaleSetup.do?patient_id=' + patient_id, 'braden_scale', 'width=980,height=650,scrollbars=yes,resizable=yes,status=no,location=no,menubar=no');
    refwin.focus()
}

function tbiScore() {
    var toe_pressure = document.form.right_toe_pressure.value;
    var brachial_pressure = document.form.right_brachial_pressure.value;

    if (toe_pressure != "" && brachial_pressure != "") {
        var other_bp = document.form.left_brachial_pressure.value;
        if (parseInt(brachial_pressure) < parseInt(other_bp)) {
            brachial_pressure = other_bp;
        }
        var f = parseInt(toe_pressure) / parseInt(brachial_pressure);
        f = Math.round(f * 100) / 100;
        document.getElementById("right_tbi_score").innerHTML = f;
    }
    else
    {
        document.getElementById("right_tbi_score").innerHTML = "";
    }

    var toe_pressure = document.form.left_toe_pressure.value;
    var brachial_pressure = document.form.left_brachial_pressure.value;
    if (toe_pressure != "" && brachial_pressure != "") {
        var other_bp = document.form.right_brachial_pressure.value;
        if (parseInt(brachial_pressure) < parseInt(other_bp)) {
            brachial_pressure = other_bp;
        }
        var f = parseInt(toe_pressure) / parseInt(brachial_pressure);
        f = Math.round(f * 100) / 100;
        document.getElementById("left_tbi_score").innerHTML = f;
    }
    else
    {
        document.getElementById("left_tbi_score").innerHTML = "";
    }
}

function brachialScore() {
    var dorsalis_pedis = document.form.right_dorsalis_pedis_ankle_brachial.value;
    var tibial_pedis = document.form.right_tibial_pedis_ankle_brachial.value;
    var brachial = document.form.right_ankle_brachial.value;
    if (tibial_pedis == "") {
        tibial_pedis = 0;
    } else {
        tibial_pedis = parseInt(tibial_pedis);
    }
    if (dorsalis_pedis == "") {
        dorsalis_pedis = 0;
    } else {
        dorsalis_pedis = parseInt(dorsalis_pedis);
    }

    var other_bp = document.form.left_ankle_brachial.value;
    if (parseInt(brachial) < parseInt(other_bp)) {
        brachial = other_bp;
    }
    if (brachial != "" && (tibial_pedis != "" || dorsalis_pedis != "")) {
        var d = "";
        if (tibial_pedis > dorsalis_pedis) {
            d = parseInt(tibial_pedis) / parseInt(brachial);
        } else {
            d = parseInt(dorsalis_pedis) / parseInt(brachial);
        }
        d = Math.round(d * 100) / 100;
        
        document.getElementById("right_abi_score_ankle_brachial").disabled=false;
        document.getElementById("right_abi_score_ankle_brachial").innerText = d;
    }
    else
    {
        document.getElementById("right_abi_score_ankle_brachial").innerText = "";
         
        document.getElementById("right_abi_score_ankle_brachial").disabled=true; 
    }

    var dorsalis_pedis = document.form.left_dorsalis_pedis_ankle_brachial.value;
    var tibial_pedis = document.form.left_tibial_pedis_ankle_brachial.value;
    var brachial = document.form.left_ankle_brachial.value;
    if (tibial_pedis == "") {
        tibial_pedis = 0;
    } else {
        tibial_pedis = parseInt(tibial_pedis);
    }
    if (dorsalis_pedis == "") {
        dorsalis_pedis = 0;
    } else {
        dorsalis_pedis = parseInt(dorsalis_pedis);
    }
    var other_bp = document.form.right_ankle_brachial.value;
    if (parseInt(brachial) < parseInt(other_bp)) {
        brachial = other_bp;
    }
    if (brachial != "" && (tibial_pedis != "" || dorsalis_pedis != "")) {
        var d = "";
        if (tibial_pedis > dorsalis_pedis) {
            d = parseInt(tibial_pedis) / parseInt(brachial);
        } else {
            d = parseInt(dorsalis_pedis) / parseInt(brachial);
        }
        d = Math.round(d * 100) / 100;
        
        document.getElementById("left_abi_score_ankle_brachial").disabled=false;
        document.getElementById("left_abi_score_ankle_brachial").innerText = d;
    }
    else
    {   
        document.getElementById("left_abi_score_ankle_brachial").innerText = "";
        document.getElementById("left_abi_score_ankle_brachial").disabled=true;
    }
}




