Merge Javascript/CSS for fewer HTTP requests..
/////

Merged jquery.idletimeout.js with jquery.idletimer.js

Merged old CalendarPopup in common.js.   Remove once we move to jquery datebox.

Merged datetime picker, multiselect with datatables.

Themeroller:
header: bg: 2c2c87 - 25%  ; border 23235c
clickable: bg: b3b3fd - 45% ; border 0b0b66 ; icon 0d1454
hover: bg: 6868d9 - %75 ; border 7e7eec
active: 33CC33
content: border 414ec3 icon 414ec3