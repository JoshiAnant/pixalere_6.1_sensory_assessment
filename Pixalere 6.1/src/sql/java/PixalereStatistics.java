/*
 * PixalereStatistics.java
 *
 * Created on August 18, 2006, 12:49 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */


import javax.mail.MessagingException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import com.pixalere.utils.PDate;
import com.pixalere.assessment.WoundAssessmentVO;
import com.pixalere.common.*;
import com.pixalere.wound.WoundProfileVO;
import com.pixalere.patient.PatientAccountVO;
import com.pixalere.patient.PatientProfileVO;
import com.pixalere.professional.UserVO;
import com.pixalere.utils.SendMailUsingAuthentication;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.ojb.broker.query.ReportQueryByCriteria;

/**
 * This script is run every monday.  It gets the current date,
 * and back dates it 1 month.  It then gets a tally of the
 * statistics from last month, and  a current tally of this month.
 * @author Strider
 */
public class PixalereStatistics {
    /* Have javascript close browser.
     *
     *
     */
    
    public PixalereStatistics(String db,String currentStartDay,String lastStartDay) {
        Calendar cal = new GregorianCalendar();
        database=db;
        
        
        
        
        //Do calls.
        int pcount=getPatientCount();
        int wcount=getWoundCount();
        int ucount=getUserCount();
        int acountNow=getAssessmentCount(currentStartDay,lastStartDay);
        //int acountLastMonth=getAssessmentCount(lastStartDay,currentStartDay);
        
        final String subject="Pixalere "+db+" Weekly Statistics";
        final String message="Patient Total: "+pcount+"\n" +
                "Wound Total: "+wcount+"\n" +
                "This Month Assessment Total:"+acountNow+"\n"+
                "Professional Count: "+ucount;
        
        System.out.println(message);
        
        try {
            
            SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
            mail.sendCustomMail("travdes@travdes.com", subject,message);
            
        } catch (MessagingException e) {
           // log.error("sending email: " + e.getMessage());
        } catch (Exception ex) {
           // log.error("sending email: " + ex.getMessage());
        }
        
        
    }
    public int getAssessmentCount(String startDate,String endDate) {
        PersistenceBroker broker = null;
        int count=0;
        try {
            broker = ConnectionLocator.getInstance().findBroker(database);
            
                WoundAssessmentVO wp=new WoundAssessmentVO();
                wp.setStatus(new Integer(1));
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                //crit.addNotEqualTo("status", "Closed");
                crit.addEqualTo("status", new Integer(1));
                crit.addGreaterOrEqualThan("timestamp",startDate);
                crit.addLessThan("timestamp",endDate);
                query = QueryFactory.newQuery(WoundAssessmentVO.class, crit);

                count= broker.getCount(query);
                
                
            return count;
        } catch (ConnectionLocatorException e) {
        } finally {
            if (broker != null)
                broker.close();
        }
        return 0;
    }
    public int getWoundCount() {
        PersistenceBroker broker = null;
        int count=0;
        try {
            broker = ConnectionLocator.getInstance().findBroker(database);
            
                WoundProfileVO wp=new WoundProfileVO();
                wp.setCurrent(new Integer(1));
                QueryByCriteria query = new QueryByCriteria(wp);
                count= broker.getCount(query);
                
                
            return count;
        } catch (ConnectionLocatorException e) {
           // logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
           // throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null)
                broker.close();
        }
        return 0;
    }
    public int getPatientCount() {
        PersistenceBroker broker = null;
        int count=0;
        try {
            broker = ConnectionLocator.getInstance().findBroker(database);
            
                PatientAccountVO wp=new PatientAccountVO();
                wp.setAccount_status(new Integer(1));
                QueryByCriteria query = new QueryByCriteria(wp);
                count= broker.getCount(query);
                
                
            return count;
        } catch (ConnectionLocatorException e) {
            //logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
           // throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null)
                broker.close();
        }
        return 0;
    }
    public int getUserCount() {
        PersistenceBroker broker = null;
        int count=0;
        try {
            broker = ConnectionLocator.getInstance().findBroker(database);
            
                UserVO wp=new UserVO();
                
                wp.setAccount_status(new Integer(1));
                QueryByCriteria query = new QueryByCriteria(wp);
                count= broker.getCount(query);
                
                
            return count;
        } catch (ConnectionLocatorException e) {
            //logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
           // throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null)
                broker.close();
        }
        return 0;
    }
    public static void main(String[] args) {
       
        String database = args[0];
        
        String startDate = args[1];
        String endDate = args[2];
        
        new PixalereStatistics(database,startDate,endDate);

    }
}
