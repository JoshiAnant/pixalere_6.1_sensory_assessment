import java.util.*;
import com.pixalere.utils.*;

import com.pixalere.admin.ListVO;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.professional.dao.UserDAO;
import com.pixalere.utils.Common;
import com.pixalere.admin.ListBD;

public class Conversion {
    private String db = "";
    
    public Conversion(String argument, String database) {
        try {
            db = database;
            // Fill the new field for the searchnames with a stripped down, uppercase representation of the name
            //setProfessionalSearchNames();
            //setPatientSearchNames();
            
            // For each WP that contained data in fields that is now on the PP, new records were created in the PP table.
            // These records are marked with current_flag=-1, and needs data from other PP reocrds
            //updatePatientProfiles();
            
            //setAlphaStartAndEnd();
            
            // Move the details fro C&S, VAC and other therapies from WP to Assessment
            //moveTherapies();
            
            // Convert the products to its own table
            convertProducts();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
/*
    public void moveTherapies() throws Exception {
        System.out.println();
        System.out.println("Move Therapies");
        int intCount=0;
            com.pixalere.patient.dao.PatientDAO patientDB = new com.pixalere.patient.dao.PatientDAO(db);
        com.pixalere.assessment.dao.WoundAssessmentDAO woundAssessmentDB = new com.pixalere.assessment.dao.WoundAssessmentDAO(db);
            com.pixalere.assessment.dao.WoundAssessmentLocationDAO woundAssessmentLocationDB = new com.pixalere.assessment.dao.WoundAssessmentLocationDAO(db);
        com.pixalere.assessment.dao.AssessmentDAO assessmentEachwoundDB = new com.pixalere.assessment.dao.AssessmentDAO(db);
        com.pixalere.assessment.WoundAssessmentVO woundAssessment = new com.pixalere.assessment.WoundAssessmentVO();
        woundAssessment.setHas_comments(-1);
        Collection lcol1 = null;
        lcol1 = woundAssessmentDB.findAllByCriteria(woundAssessment);
        Iterator iter1 = lcol1.iterator();
        while (iter1.hasNext()) {
            com.pixalere.assessment.WoundAssessmentVO was = (com.pixalere.assessment.WoundAssessmentVO) iter1.next();
 
            // Find the treatment location at the time of this update, by going through the assessments.
            // Now as a wp might not have assessments as all, first get the treatment location from the pp.
 
                    String strWasTimestamp=was.getTimestamp();
                    if(strWasTimestamp==null || strWasTimestamp=="")
                        {
                        strWasTimestamp="0";
                        }
                    Integer intWasTimestamp=Integer.parseInt(strWasTimestamp);
 
            Integer intTreatmentLocationID=0;
            Integer intTreatmentLocationDate=0;
            com.pixalere.patient.PatientAccountVO patientAccountVO = new com.pixalere.patient.PatientAccountVO();
            patientAccountVO.setCurrent_flag(1);
            patientAccountVO.setPatient_id(was.getPatient_id());
            Collection coll2 = patientDB.findAllByCriteria(patientAccountVO,false); // lowest id first
            Iterator iter2 = coll2.iterator();
            if (iter2.hasNext()) {
                com.pixalere.patient.PatientAccountVO pac = (com.pixalere.patient.PatientAccountVO) iter2.next();
                intTreatmentLocationID=pac.getTreatment_location_id();
                }
 
            // Now find a more exact location...
            com.pixalere.assessment.WoundAssessmentVO woundAssessment2 = new com.pixalere.assessment.WoundAssessmentVO();
            woundAssessment2.setPatient_id(was.getPatient_id());
                    Collection lcol3 = null;
            lcol3 = woundAssessmentDB.findAllByCriteria(woundAssessment2);
                    Iterator iter3 = lcol3.iterator();
                    while (iter3.hasNext())
                                {
                com.pixalere.assessment.WoundAssessmentVO was2 = (com.pixalere.assessment.WoundAssessmentVO) iter3.next();
                        String strTimestamp=was2.getTimestamp();
                        if(strTimestamp==null || strTimestamp=="")
                          {
                          strTimestamp="0";
                          }
                        Integer intTimestamp=Integer.parseInt(strTimestamp);
                        if(was2.getTreatment_location_id()!= null && intTimestamp>0 && intTimestamp<intTreatmentLocationDate && intTimestamp>intWasTimestamp)
                          {
                    intTreatmentLocationID=was2.getTreatment_location_id();
                    intTreatmentLocationDate=intTimestamp;
                          }
                        }
 
            String strCns_date="";
            String strCns_name="";
            String strCns_result="";
            String strCns_result_other="";
            String strAntibiotics="";
            Integer intCns_route=0;
            String strCns_duration_start="";
            String strCns_duration_end="";
            String strAdjunctive="";
            String strAdjunctive_other_text="";
            String strVac_start_date="";
            String strVac_end_date="";
            Integer intReading=0;
            Integer intReading_var=0;
 
            String strTherapies=was.getLaser()+"|";
 
            strCns_date=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strCns_name=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strCns_result=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strCns_result_other=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strAntibiotics=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            intCns_route=Integer.parseInt(strTherapies.substring(0,strTherapies.indexOf("|")));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strCns_duration_start=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strCns_duration_end=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strAdjunctive=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strAdjunctive_other_text=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strVac_start_date=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            strVac_end_date=strTherapies.substring(0,strTherapies.indexOf("|"));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            intReading_var=Integer.parseInt(strTherapies.substring(0,strTherapies.indexOf("|")));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            intReading=Integer.parseInt(strTherapies.substring(0,strTherapies.indexOf("|")));
            strTherapies=strTherapies.substring(strTherapies.indexOf("|")+1);
 
            // Find which alphas were existing at the time of this (former) wound profile update
            com.pixalere.assessment.WoundAssessmentLocationVO woundAssessmentLocation = new com.pixalere.assessment.WoundAssessmentLocationVO();
            woundAssessmentLocation.setWound_id(was.getWound_id());
            Collection lcol4 = null;
            lcol4 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation,0,false);
            Iterator iter4 = lcol4.iterator();
            while (iter4.hasNext()) {
                com.pixalere.assessment.WoundAssessmentLocationVO wal = (com.pixalere.assessment.WoundAssessmentLocationVO) iter4.next();
                if((wal.getOpen_timestamp()!="" && Integer.parseInt(wal.getOpen_timestamp())<=intWasTimestamp) && (wal.getDischarge()==0 || (wal.getClose_timestamp()!=null && wal.getClose_timestamp()!="" && Integer.parseInt(wal.getClose_timestamp())>=intWasTimestamp)))
                    {
                    com.pixalere.assessment.AssessmentEachwoundVO assessmentEachwoundNew = new com.pixalere.assessment.AssessmentEachwoundVO();
                    //assessmentEachwoundNew.setWound_not_assessed(1);
                    assessmentEachwoundNew.setFull_assessment(0);
                    assessmentEachwoundNew.setPatient_id(was.getPatient_id());
                    assessmentEachwoundNew.setWound_id(was.getWound_id());
                    assessmentEachwoundNew.setAssessment_id(was.getId());
                    // HA SPECIFIC
                    assessmentEachwoundNew.setStatus(256);
                    assessmentEachwoundNew.setAlpha_id(wal.getId());
 
                    assessmentEachwoundNew.setCs_date(strCns_date);
                    assessmentEachwoundNew.setAntibiotics(strAntibiotics);
                    assessmentEachwoundNew.setCsresult(strCns_result);
                    //assessmentEachwoundNew.setCs_date_show(1);
 
                    if(strVac_start_date!="" || strVac_end_date!="" || intReading>0 || intReading_var>0)
                        {
                        assessmentEachwoundNew.setVac_show(1);
                        }
                    else
                        {
                        assessmentEachwoundNew.setVac_show(0);
                        }
                    assessmentEachwoundNew.setVac_start_date(strVac_start_date);
                    assessmentEachwoundNew.setVac_end_date(strVac_end_date);
                    assessmentEachwoundNew.setReading(intReading);
                    assessmentEachwoundNew.setReading_var(intReading_var);
                    assessmentEachwoundNew.setAdjunctive(strAdjunctive);
                    //assessmentEachwoundNew.setAdjunctive_other(");
                    assessmentEachwoundNew.setAdjunctive_other_text(strAdjunctive_other_text);
 
                    if(strCns_date!="" || strCns_name!="" || strCns_result!="" || strCns_result_other!="" || strAntibiotics!="" || intCns_route>0 || strCns_duration_start!="" || strCns_duration_end!="")
                        {
                        assessmentEachwoundNew.setCs_show(1);
                        }
                    else
                        {
                        assessmentEachwoundNew.setCs_show(0);
                        }
 
                    assessmentEachwoundDB.insert(assessmentEachwoundNew);
                    }
                }
            was.setLaser("");
            was.setTreatment_location_id(intTreatmentLocationID);
            was.setHas_comments(0);
            woundAssessmentDB.update(was);
            intCount++;
          }
    System.out.println(intCount+" Therapies moved");
    }
 
    public void setAlphaStartAndEnd() throws Exception {
        System.out.println();
        System.out.println("Set Alpha Start and End date");
        int intCount=0;
            com.pixalere.assessment.dao.AssessmentDAO assessmentDB = new com.pixalere.assessment.dao.AssessmentDAO(db);
            com.pixalere.assessment.dao.WoundAssessmentLocationDAO woundAssessmentLocationDB = new com.pixalere.assessment.dao.WoundAssessmentLocationDAO(db);
            com.pixalere.wound.WoundManagerBD woundDB = new com.pixalere.wound.WoundManagerBD(db);
 
        com.pixalere.assessment.WoundAssessmentLocationVO woundAssessmentLocation = new com.pixalere.assessment.WoundAssessmentLocationVO();
        Collection lcol1 = null;
        lcol1 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation,0,false);
        Iterator iter1 = lcol1.iterator();
        while (iter1.hasNext()) {
            com.pixalere.assessment.WoundAssessmentLocationVO wal = (com.pixalere.assessment.WoundAssessmentLocationVO) iter1.next();
            // Search for startdate by getting the first assessment for this alpha
            com.pixalere.assessment.AssessmentEachwoundVO assessmentEachwound = new com.pixalere.assessment.AssessmentEachwoundVO();
            assessmentEachwound.setWound_id(wal.getWound_id());
            Collection lcol2 = null;
            lcol2 = assessmentDB.findAllByCriteria(assessmentEachwound,"assessment_id",false,1); // Ascending order;
            Iterator iter2 = lcol2.iterator();
            if (iter2.hasNext())
                {
                    com.pixalere.assessment.AssessmentEachwoundVO aew = (com.pixalere.assessment.AssessmentEachwoundVO) iter2.next();
                    wal.setOpen_timestamp(aew.getWoundAssessment().getTimestamp());
                    }
                else
                {
                // If no assessments found, just get startdate of wound profile
                com.pixalere.wound.WoundVO wound = new com.pixalere.wound.WoundVO();
                wound.setId(wal.getWound_id());
                com.pixalere.wound.WoundVO wnd = woundDB.retrieveWound(wound);
                        wal.setOpen_timestamp(wnd.getStart_date());
                }
 
            // If alpha closed, get end date by getting closure assessment for this alpha
            if(wal.getDischarge()==1)
                {
                Collection lcol3 = null;
 
                // HA SPECIFIC
                assessmentEachwound.setStatus(257); // Closed
                lcol3 = assessmentDB.findAllByCriteria(assessmentEachwound,"assessment_id",true,1); // Descending order;
                Iterator iter3 = lcol3.iterator();
                if (iter3.hasNext())
                    {
                            com.pixalere.assessment.AssessmentEachwoundVO aew = (com.pixalere.assessment.AssessmentEachwoundVO) iter3.next();
                            wal.setClose_timestamp(aew.getWoundAssessment().getTimestamp());
                            }
                    }
            woundAssessmentLocationDB.update(wal);
            System.out.println("Update wound for patient "+wal.getPatient_id());
            intCount++;
            }
    System.out.println(intCount+" Alphas set");
    }*/
    
    
    public void convertProducts() throws Exception {
        System.out.println();
        System.out.println("Convert Products "+PDate.getDateStringWithHour(PDate.getEpochTime()+""));
        int intCount=0;
        com.pixalere.assessment.dao.AssessmentDAO assessmentDB = new com.pixalere.assessment.dao.AssessmentDAO(db);
        
        com.pixalere.patient.dao.PatientDAO patientDB = new com.pixalere.patient.dao.PatientDAO(db);
        
        // Loop through all patients; when we retrieve all assessments first that data object is huge
        com.pixalere.patient.PatientAccountVO patientAccountVO = new com.pixalere.patient.PatientAccountVO();
        patientAccountVO.setCurrent_flag(1);
        Collection coll0 = patientDB.findAllByCriteria(patientAccountVO,false); // Lowest id first
        Iterator iter0 = coll0.iterator();
        System.out.println("# of Patients"+coll0.size());
        int size=coll0.size();
        int c=0;
        while (iter0.hasNext()) {
            com.pixalere.patient.PatientAccountVO pac = (com.pixalere.patient.PatientAccountVO) iter0.next();
            
            c++;
            
            if(c % 200 == 0){
                System.out.println("Current done "+ c +" Patients.");
            }
            //if(pac.getPatient_id().intValue() > 2351){
                // The contents of field "products" in table "assessment_eachwound" was temporary stored in field "pain_comments"
                com.pixalere.assessment.AssessmentEachwoundVO assessmentEachwound = new com.pixalere.assessment.AssessmentEachwoundVO();
//System.out.println("Convert products for patient "+pac.getPatient_id());
                assessmentEachwound.setPatient_id(pac.getPatient_id());
                Collection lcol = null;
                lcol = assessmentDB.findAllByCriteria(assessmentEachwound,"assessment_id",false,0); // Ascending order;
                Iterator iter = lcol.iterator();
                while (iter.hasNext()) {
                    com.pixalere.assessment.AssessmentEachwoundVO aew = (com.pixalere.assessment.AssessmentEachwoundVO) iter.next();
                    if(aew.getPain_comments()!=null && aew.getPain_comments().trim().length()>0 && !aew.getPain_comments().trim().equals("a:0:{}")) {
                        intCount++;
                        String strProductData = aew.getPain_comments();
                        String strQuantity = "";
                        String strProduct = "";
                        int intQuotePos = strProductData.indexOf('"');
                        while(intQuotePos > 0) {
                            strProductData = strProductData.substring(intQuotePos + 1, strProductData.length());
                            intQuotePos = strProductData.indexOf('"');
                            String strTag = strProductData.substring(0, intQuotePos);
                            strProductData = strProductData.substring(intQuotePos + 1, strProductData.length());
                            intQuotePos = strProductData.indexOf('"');
                            strProductData = strProductData.substring(intQuotePos + 1, strProductData.length());
                            intQuotePos = strProductData.indexOf('"');
                            
                            // Incase of "other product", there might be a quote char in the text
                            // Check for the next character, which should be semicolon, to make sure
                            if  (strProductData.indexOf((char)59)!=intQuotePos+1) {
                                // There is no semicolon following, now search for the
                                // semicolon and bracket and pretend there's a precending quote
                                intQuotePos = strProductData.indexOf((char)59+"}") -1;
                            }
                            String strInfo = strProductData.substring(0, intQuotePos);
                            strProductData = strProductData.substring(intQuotePos + 1, strProductData.length());
                            intQuotePos = strProductData.indexOf('"');
                            if(strTag.startsWith("quantity")) {
                                strQuantity = strInfo;
                                if(strQuantity.equals("Infinity") || strQuantity.indexOf("E")!=-1) // There's a case where it says "2.0E-5"
                                {
                                    strQuantity="1";
                                }
                                if(strQuantity.length()>5) {
                                    strQuantity=strQuantity.substring(0,5);
                                }
                                if(strQuantity.equals("0.0") || strQuantity.equals("0.000")) {
                                    strQuantity="0";
                                }
                            }
                            if(strTag.startsWith("product")) {
                                strProduct = strInfo;
                            }
                            if(strQuantity.length() > 0 && strProduct.length() > 0) {
                                com.pixalere.assessment.AssessmentProductVO assessmentProductVO = new com.pixalere.assessment.AssessmentProductVO();
                                assessmentProductVO.setPatient_id(aew.getWoundAssessment().getPatient_id());
                                assessmentProductVO.setWound_id(aew.getWoundAssessment().getWound_id());
                                assessmentProductVO.setAssessment_id(aew.getAssessment_id());
                                assessmentProductVO.setWound_assessment_id(aew.getId());
                                Integer intOther=strProduct.indexOf("Other;"); // This might be "Other;" or "ID:Other;"
                                if(intOther!=-1) {
                                    assessmentProductVO.setProduct_id(-1);
                                    assessmentProductVO.setOther(strProduct);
                                } else {
                                    if(strProduct.indexOf("ID:")!=-1) {
                                        assessmentProductVO.setProduct_id(Integer.parseInt(strProduct.substring(3)));
                                    } else // Sometimes there's no "ID:" tag for product id
                                    {
                                        assessmentProductVO.setProduct_id(Integer.parseInt(strProduct));
                                    }
                                }
                                assessmentProductVO.setAlpha_id(aew.getAlpha_id());
                                assessmentProductVO.setQuantity(strQuantity);
                                assessmentProductVO.setActive(aew.getWoundAssessment().getStatus());
                                assessmentDB.updateProduct(assessmentProductVO);
                                strQuantity = "";
                                strProduct = "";
                            }
                        }
                    }
                    if(aew.getPain_comments()==null || aew.getPain_comments().trim().length()>0) {
                        aew.setPain_comments("");
                        assessmentDB.update(aew);
                    }
                }
            //}
        }
        System.out.println(intCount+" Assessment Products converted "+PDate.getDateStringWithHour(PDate.getEpochTime()+""));
    }
    
    public void updatePatientProfiles() throws Exception {
        System.out.println();
        System.out.println("Update Patient Profiles");
        ListBD listBD = new ListBD(db);
        int intCount=0;
        com.pixalere.patient.dao.PatientProfileDAO patientProfileDB = new com.pixalere.patient.dao.PatientProfileDAO(db);
        
        // Loop through all patients; search for current record first.
        com.pixalere.patient.PatientProfileVO patientProfile0 = new com.pixalere.patient.PatientProfileVO();
        patientProfile0.setCurrent_flag(1);
        Collection coll0 = patientProfileDB.findAllByCriteria(patientProfile0,0);
        Iterator iter0 = coll0.iterator();
        while (iter0.hasNext()) {
            com.pixalere.patient.PatientProfileVO p0 = (com.pixalere.patient.PatientProfileVO) iter0.next();
            // Check if this patients that has data moved from WP to PP
            com.pixalere.patient.PatientProfileVO patientProfile1 = new com.pixalere.patient.PatientProfileVO();
            patientProfile1.setPatient_id(p0.getPatient_id());
            patientProfile1.setCurrent_flag(-1);
            Collection coll1 = patientProfileDB.findAllByCriteria(patientProfile1,1);
            Iterator iter1 = coll1.iterator();
            if (iter1.hasNext()) {
                com.pixalere.patient.PatientProfileVO p1 = (com.pixalere.patient.PatientProfileVO) iter1.next();
                com.pixalere.patient.PatientProfileVO patientProfile2 = new com.pixalere.patient.PatientProfileVO();
                // Find patients that has data moved from WP to PP
                patientProfile2.setPatient_id(p1.getPatient_id());
                Collection coll2 = patientProfileDB.findAllByCriteriaOrderBy(patientProfile2, "last_update", false); // Oldest first
                Iterator iter2 = coll2.iterator();
                com.pixalere.patient.PatientProfileVO previousPatientProfile = null;
                while (iter2.hasNext()) {
                    com.pixalere.patient.PatientProfileVO p2 = (com.pixalere.patient.PatientProfileVO) iter2.next();
                    String strInvestigations="";
                    if(p2.getInvestigation()!=null && p2.getInvestigation()!="" && p2.getInvestigation()!="0" && p2.getInvestigation()!="a:0:{}") {
                        Vector vecInvestigations=Serialize.arrayIze(p2.getInvestigation());
                        for(int intItem = 0; intItem < vecInvestigations.size(); intItem++) {
                            try {
                                ListVO vo = listBD.retrieveItem(Integer.parseInt((String)vecInvestigations.get(intItem)));
                                if (vo!=null) {
                                    strInvestigations=strInvestigations+vo.getDecrypted(vo.getEn());
                                    String strSignature=p2.getUser_signature();
                                    if(strSignature.length()>10) {
                                        strInvestigations=strInvestigations+": Documented "+strSignature.substring(6,17).trim();
                                    }
                                    strInvestigations=strInvestigations+"--,--;";
                                }
                            } catch (Exception e) {}
                            
                        }
                    }
                    p2.setInvestigation(strInvestigations);
                    if(p2.getCurrent_flag()!=-1) // If old PP
                    {
                        previousPatientProfile = p2;
                    } else {
                        if(previousPatientProfile!=null) {
                            p2.setProfessionals(previousPatientProfile.getProfessionals());
                            p2.setReferral_id(previousPatientProfile.getReferral_id());
                            p2.setPatient_res_id(previousPatientProfile.getPatient_res_id());
                            p2.setFunding_source_id(previousPatientProfile.getFunding_source_id());
                            p2.setDob_month(previousPatientProfile.getDob_month());
                            p2.setDob_day(previousPatientProfile.getDob_day());
                            p2.setDob_year(previousPatientProfile.getDob_year());
                            p2.setGender(previousPatientProfile.getGender());
                            p2.setAllergies(previousPatientProfile.getAllergies());
                            p2.setInterfering_meds(previousPatientProfile.getInterfering_meds());
                            p2.setCo_morbidities(previousPatientProfile.getCo_morbidities());
                            p2.setMedications_comments(previousPatientProfile.getMedications_comments());
                            p2.setStart_date(previousPatientProfile.getStart_date());
                            p2.setInterfering_meds_other(previousPatientProfile.getInterfering_meds_other());
                            p2.setSurgical_history(previousPatientProfile.getSurgical_history());
                        }
                    }
                    
                    if(iter2.hasNext()) // If not most recent record yet
                    {
                        p2.setCurrent_flag(0);
                    } else {
                        p2.setCurrent_flag(1);
                    }
                    patientProfileDB.insert(p2);
                }
//System.out.println("Updated patient "+p1.getPatient_id());
                intCount++;
            }
        }
        System.out.println(intCount+" Patient Profiles updated");
    }
    
    public void setProfessionalSearchNames() throws Exception {
        System.out.println();
        System.out.println("Set Professional Search Names");
        int intCount=0;
        com.pixalere.professional.dao.UserDAO userDB = new com.pixalere.professional.dao.UserDAO(db);
        Collection coll = userDB.findAllByCriteria(new com.pixalere.professional.UserVO());
        Iterator iter = coll.iterator();
        while (iter.hasNext()) {
            com.pixalere.professional.UserVO p = (com.pixalere.professional.UserVO) iter.next();
            p.setFirstname_search(Common.stripName(p.getFirstname()));
            p.setLastname_search(Common.stripName(p.getLastname()));
            userDB.update(p);
            intCount++;
        }
        System.out.println(intCount+" Professional Search Names set");
    }
    
    public void setPatientSearchNames() throws Exception {
        System.out.println();
        System.out.println("Set Patient Search Names");
        int intCount=0;
        com.pixalere.patient.dao.PatientDAO patientDB = new com.pixalere.patient.dao.PatientDAO(db);
        Collection coll = patientDB.findAllByCriteria(new com.pixalere.patient.PatientAccountVO(),false);
        Iterator iter = coll.iterator();
        while (iter.hasNext()) {
            com.pixalere.patient.PatientAccountVO p = (com.pixalere.patient.PatientAccountVO) iter.next();
            p.setLastName_search(getEncrypted(Common.stripName(getDecrypted(p.getLastName(),"pass4lastname")),"pass4lastname"));
            patientDB.update(p);
            intCount++;
        }
        System.out.println(intCount+" Patient Search Names set");
        
    }
    
    public String getEncrypted(String value, String password) {
        try {
            com.pixalere.utils.TripleDes des = new com.pixalere.utils.TripleDes(password);
            return des.encrypt(value);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    public String getDecrypted(String value, String password) {
        try {
            com.pixalere.utils.TripleDes des = new com.pixalere.utils.TripleDes(password);
            return des.decrypt(value);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    public static void main(String[] args) {
        String argument = args[0];
        String database = args[1];
        
        System.out.println("Converting database: "+database);
        System.out.println();
        new Conversion(argument, database);
        
    }
}
/* RUN
/usr/java/jdk1.5.0_11/bin/java  -cp ../lib/jxl.jar:.:../lib/commons-lang-2.1.jar:../lib/commons-beanutils.jar:../../main/resources/lib/servlet2_3.jar:../lib/mysql-connector-java-5.1.14-bin.jar:../lib/log4j-1.2.8.jar:../lib/db-ojb-1.0.4.jar:../lib/commons-resources.jar:../lib/commons-collections-3.1.jar:../lib/commons-digester.jar:../lib/commons-logging.jar:../lib/commons-pool-1.2.jar:../lib/commons-dbcp.jar:../lib/struts.jar com.pixalere.utils.FixPatients
  javac -classpath ../../main/webapp/WEB-INF/classes:.:../../main/webapp/../lib/commons-lang-2.1.jar:../../main/webapp/../lib/mail.jar:../../main/webapp/../lib/commons-beanutils.jar:../../main/webapp/main/resources/lib/servlet2_3.jar:../../main/webapp/../lib/mysql-connector-java-5.1.14-bin.jar:../../main/webapp/../lib/log4j-1.2.8.jar:../../main/webapp/../lib/db-ojb-1.0.4.jar:../../main/webapp/../lib/commons-resources.jar:../../main/webapp/../lib/commons-collections-3.1.jar:../../main/webapp/../lib/commons-digester.jar:../../main/webapp/../lib/commons-logging.jar:../../main/webapp/../lib/commons-pool-1.2.jar:../../main/webapp/../lib/commons-dbcp.jar PixalereStatistics.java
 
 */