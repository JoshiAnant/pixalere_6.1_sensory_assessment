
ALTER DATABASE test_live SET OFFLINE WITH ROLLBACK IMMEDIATE;
ALTER DATABASE test_live SET ONLINE;
drop database test_live;
create database test_live;
 ALTER DATABASE test_live SET OFFLINE WITH ROLLBACK IMMEDIATE;
ALTER DATABASE test_live SET ONLINE;
restore database test_live from disk = 'c:\FullVCHDB.bak' with REPlACE,MOVE 'PixalereProd' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\test_live.mdf',move 'PixalereProd_log' to 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\test_live_log.ldf';