
DROP TABLE IF EXISTS external_referrals;
DROP TABLE IF EXISTS ccac_reports;
DROP TABLE IF EXISTS nursing_care_plan_questions;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account_assignments
# ------------------------------------------------------------
DROP TABLE IF EXISTS library_category;
DROP TABLE IF EXISTS limb_adv_assessment;
DROP TABLE IF EXISTS limb_upper_assessment;
DROP TABLE IF EXISTS limb_basic_assessment;
DROP TABLE IF EXISTS library_category;
DROP TABLE IF EXISTS account_assignments;
DROP TABLE IF EXISTS lookup_l10n;
DROP TABLE IF EXISTS languages;
drop table if exists wound_acquired;
drop table if exists report_queue;
drop table if exists report_queue_treatments;
drop table if exists report_queue_etiologies;
drop table if exists report_queue_types;
drop table if exists assessment_npwt;
drop table if exists assessment_npwt_alpha;
CREATE TABLE account_assignments (
  id int(11) NOT NULL AUTO_INCREMENT,
  account_id int(11) DEFAULT NULL,
  assign_to_id int(11) DEFAULT NULL,
  account_type text,
  time_stamp int(11) DEFAULT NULL,
  assigned_by int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY account_id (account_id),
  KEY assign_to_id (assign_to_id),
  CONSTRAINT account_assignments_ibfk_1 FOREIGN KEY (account_id) REFERENCES patients (id),
  CONSTRAINT account_assignments_ibfk_2 FOREIGN KEY (assign_to_id) REFERENCES professional_accounts (id)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump of table assessment_antibiotics
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_antibiotics;

CREATE TABLE assessment_antibiotics (
  id int(11) NOT NULL AUTO_INCREMENT,
  assessment_treatment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  antibiotic varchar(255) DEFAULT NULL,
  start_date datetime DEFAULT NULL,
  end_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY assessment_treatment_id (assessment_treatment_id),
  CONSTRAINT assessment_antibiotics_ibfk_1 FOREIGN KEY (assessment_treatment_id) REFERENCES assessment_treatment (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_burn
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_burn;

CREATE TABLE assessment_burn (
  id int(11) NOT NULL AUTO_INCREMENT,
  status int(11) DEFAULT NULL,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  full_assessment int(11) DEFAULT NULL,
  pain int(11) DEFAULT NULL,
  pain_comments text,
  burn_wound text,
  exudate_colour text,
  exudate_amount text,
  grafts text,
  donors text,
  priority int(11) DEFAULT NULL,
  discharge_reason varchar(50) DEFAULT NULL,
  active int(11) DEFAULT NULL,
  discharge_other varchar(255) DEFAULT NULL,
  wound_date varchar(255) DEFAULT NULL,
  closed_date varchar(255) DEFAULT NULL,
  last_update varchar(255) DEFAULT NULL,
  cs_show int(11) DEFAULT NULL,
  prev_alpha_id int(11) DEFAULT NULL,
  moved_reason varchar(255) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY alpha_id (alpha_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT assessment_burn_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_burn_ibfk_2 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT assessment_burn_ibfk_3 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE,
  CONSTRAINT assessment_burn_ibfk_4 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_comments;

CREATE TABLE assessment_comments (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  professional_id int(11) DEFAULT NULL,
  time_stamp int(11) DEFAULT NULL,
  body text,
  user_signature text,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_reason text,
  delete_signature varchar(255) DEFAULT NULL,
  wcc_visit_required int(11) DEFAULT NULL,
  position_id int(11) DEFAULT NULL,
  referral_id int(11) DEFAULT NULL,
  comment_type varchar(25) DEFAULT NULL,
  ncp_changed int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY professional_id (professional_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT assessment_comments_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT assessment_comments_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_comments_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT assessment_comments_ibfk_4 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_drain
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_drain;

CREATE TABLE assessment_drain (
  id int(11) NOT NULL AUTO_INCREMENT,
  full_assessment int(11) NOT NULL DEFAULT '0',
  active int(11) DEFAULT '0',
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  priority int(11) DEFAULT '0',
  last_update varchar(255) DEFAULT NULL,
  type_of_drain int(11) DEFAULT '0',
  type_of_drain_other text,
  sutured int(11) DEFAULT NULL,
  tubes_changed_date varchar(255) DEFAULT NULL,
  peri_drain_area text,
  additional_days_date1 varchar(255) DEFAULT NULL,
  drainage_amount_mls1 int(10) DEFAULT NULL,
  drainage_amount_hrs1 int(2) DEFAULT NULL,
  drainage_amount1_start int(3) DEFAULT NULL,
  drainage_amount1_end int(3) DEFAULT NULL,
  additional_days_date2 varchar(255) DEFAULT NULL,
  drainage_amount_mls2 int(10) DEFAULT NULL,
  drainage_amount_hrs2 int(2) DEFAULT NULL,
  drainage_amount2_start int(3) DEFAULT NULL,
  drainage_amount2_end int(3) DEFAULT NULL,
  additional_days_date3 varchar(255) DEFAULT NULL,
  drainage_amount_mls3 int(10) DEFAULT NULL,
  drainage_amount_hrs3 int(2) DEFAULT NULL,
  drainage_amount3_start int(3) DEFAULT NULL,
  drainage_amount3_end int(3) DEFAULT NULL,
  additional_days_date4 varchar(255) DEFAULT NULL,
  drainage_amount_mls4 int(10) DEFAULT NULL,
  drainage_amount_hrs4 int(2) DEFAULT NULL,
  drainage_amount4_start int(3) DEFAULT NULL,
  drainage_amount4_end int(3) DEFAULT NULL,
  additional_days_date5 varchar(255) DEFAULT NULL,
  drainage_amount_mls5 int(10) DEFAULT NULL,
  drainage_amount_hrs5 int(2) DEFAULT NULL,
  drainage_amount5_start int(3) DEFAULT NULL,
  drainage_amount5_end int(3) DEFAULT NULL,
  drainage_num int(11) DEFAULT NULL,
  drain_characteristics1 text,
  drain_odour1 text,
  drain_characteristics2 text,
  drain_odour2 text,
  drain_characteristics3 text,
  drain_odour3 text,
  drain_characteristics4 text,
  drain_odour4 text,
  drain_characteristics5 text,
  drain_odour5 text,
  drain_removed int(11) DEFAULT NULL,
  closed_date varchar(255) DEFAULT NULL,
  drain_removed_intact int(11) DEFAULT NULL,
  discharge_reason int(11) DEFAULT NULL,
  drain_site text,
  adjunctive text,
  adjunctive_other_text text,
  cs_show int(11) DEFAULT '0',
  status int(11) DEFAULT NULL,
  pain int(11) DEFAULT NULL,
  pain_comments text,
  prev_alpha_id int(11) DEFAULT NULL,
  moved_reason varchar(255) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  KEY id (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY alpha_id (alpha_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT assessment_drain_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_drain_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT assessment_drain_ibfk_7 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE,
  CONSTRAINT assessment_drain_ibfk_8 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE,
  CONSTRAINT assessment_drain_ibfk_9 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_wound
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_wound;

CREATE TABLE assessment_wound (
  id int(11) NOT NULL AUTO_INCREMENT,
  status int(11) DEFAULT '0',
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  full_assessment int(11) DEFAULT '-1',
  pain int(11) DEFAULT NULL,
  pain_comments text,
  length_cm int(11) DEFAULT NULL,
  length_mm int(11) DEFAULT NULL,
  width_cm int(11) DEFAULT NULL,
  width_mm int(11) DEFAULT NULL,
  depth_cm int(11) DEFAULT NULL,
  depth_mm int(11) DEFAULT NULL,
  wound_edge text,
  periwound_skin text,
  exudate_amount int(11) DEFAULT NULL,
  wound_depth text,
  exudate_colour text,
  exudate_odour int(11) DEFAULT NULL,
  priority int(11) DEFAULT '0',
  exudate_type text,
  discharge_reason varchar(50) DEFAULT NULL,
  active int(11) DEFAULT '0',
  discharge_other text,
  recurrent int(11) DEFAULT '0',
  wound_date text,
  adjunctive text,
  adjunctive_other_text text,
  closed_date text,
  last_update text,
  num_fistulas int(11) DEFAULT '0',
  fistulas text,
  cs_show int(11) DEFAULT '0',
  prev_alpha_id int(11) DEFAULT NULL,
  moved_reason varchar(255) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  KEY id (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY alpha_id (alpha_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT assessment_wound_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_wound_ibfk_4 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE,
  CONSTRAINT assessment_wound_ibfk_5 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT assessment_wound_ibfk_6 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE,
  CONSTRAINT assessment_wound_ibfk_7 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_images;

CREATE TABLE assessment_images (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(11) DEFAULT NULL,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT '0',
  images text,
  which_image int(11) DEFAULT '0',
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  CONSTRAINT assessment_images_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_images_ibfk_2 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT assessment_images_ibfk_3 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE

) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;




# Dump of table assessment_incision
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_incision;

CREATE TABLE assessment_incision (
  id int(11) NOT NULL AUTO_INCREMENT,
  status int(11) DEFAULT NULL,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  full_assessment int(11) NOT NULL DEFAULT '0',
  wound_profile_type_id int(11) DEFAULT NULL,
  priority int(11) DEFAULT '0',
  date_surgery varchar(25) DEFAULT NULL,
  clinical_path int(11) DEFAULT NULL,
  clinical_path_date varchar(255) DEFAULT '',
  discharge_reason int(11) DEFAULT NULL,
  active int(11) DEFAULT '0',
  discharge_other varchar(255) DEFAULT NULL,
  last_update varchar(255) DEFAULT NULL,
  closed_date varchar(255) DEFAULT NULL,
  pain int(11) DEFAULT NULL,
  pain_comments text,
  incision_status int(11) DEFAULT NULL,
  incision_exudate text,
  peri_incisional text,
  surgery_date varchar(255) DEFAULT NULL,
  postop_day int(11) DEFAULT NULL,
  incision_closure_methods text,
  exudate_amount int(11) DEFAULT NULL,
  adjunctive text,
  adjunctive_other_text text,
  cs_show int(11) DEFAULT '0',
  postop_management text,
  postop_days int(5) DEFAULT '0',
  prev_alpha_id int(11) DEFAULT NULL,
  moved_reason varchar(255) DEFAULT NULL,
  deleted int(11) NOT NULL  DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  KEY id (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY alpha_id (alpha_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT assessment_incision_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_incision_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT assessment_incision_ibfk_7 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE,
  CONSTRAINT assessment_incision_ibfk_8 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE,
  CONSTRAINT assessment_incision_ibfk_9 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_mucocutaneous
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_mucocutaneous;

CREATE TABLE assessment_mucocutaneous (
  id int(11) NOT NULL AUTO_INCREMENT,
  alpha_id int(11) DEFAULT NULL,
  assessment_ostomy_id int(11) DEFAULT NULL,
  depth_cm int(11) DEFAULT NULL,
  depth_mm int(11) DEFAULT NULL,
  location_start int(11) DEFAULT NULL,
  location_end int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY assessment_ostomy_id (assessment_ostomy_id),
  CONSTRAINT assessment_mucocutaneous_ibfk_1 FOREIGN KEY (assessment_ostomy_id) REFERENCES assessment_ostomy (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_ostomy
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_ostomy;

CREATE TABLE assessment_ostomy (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(11) DEFAULT '0',
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  full_assessment int(11) DEFAULT '-1',
  date_surgery varchar(25) DEFAULT NULL,
  clinical_path int(11) DEFAULT NULL,
  clinical_path_date varchar(255) DEFAULT '',
  flange_pouch int(11) DEFAULT '0',
  flange_pouch_comments text,
  shape int(11) DEFAULT NULL,
  pouching text,
  appearance text,
  contour text,
  urine_type text,
  urine_colour text,
  urine_quantity int(11) DEFAULT NULL,
  stool_colour text,
  stool_consistency text,
  stool_quantity int(11) DEFAULT NULL,
  nutritional_status_other text,
  self_care_progress text,
  self_care_progress_comments text,
  profile text,
  num_mucocutaneous_margin int(11) DEFAULT NULL,
  mucocutaneous_margin text,
  mucocutaneous_margin_comments text,
  perifistula_skin text,
  mucous_fistula_present int(11) DEFAULT '0',
  devices int(11) DEFAULT NULL,
  periostomy_skin text,
  priority int(11) DEFAULT '0',
  status int(11) DEFAULT '0',
  discharge_reason int(11) DEFAULT '0',
  discharge_other text,
  last_update varchar(255) DEFAULT NULL,
  closed_date text,
  adjunctive text,
  adjunctive_other_text text,
  cs_show int(11) DEFAULT '0',
  construction int(11) DEFAULT '0',
  drainage text,
  drainage_comments text,
  nutritional_status int(11) DEFAULT '0',
  circumference_mm int(11) DEFAULT '0',
  length_mm int(11) DEFAULT '0',
  width_mm int(11) DEFAULT '0',
  postop_days int(5) DEFAULT '0',
  prev_alpha_id int(11) DEFAULT NULL,
  moved_reason varchar(255) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  KEY id (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY alpha_id (alpha_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT assessment_ostomy_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_ostomy_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id),
  CONSTRAINT assessment_ostomy_ibfk_7 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id),
  CONSTRAINT assessment_ostomy_ibfk_8 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id),
  CONSTRAINT assessment_ostomy_ibfk_9 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_product;

CREATE TABLE assessment_product (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(11) DEFAULT NULL,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  wound_assessment_id int(11) DEFAULT NULL,
  ostomy_assessment_id int(11) DEFAULT NULL,
  incision_assessment_id int(11) DEFAULT NULL,
  drain_assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  product_id int(11) DEFAULT NULL,
  quantity varchar(255) DEFAULT NULL,
  other varchar(255) DEFAULT NULL,
  burn_assessment_id int(11) DEFAULT '0',
  PRIMARY KEY (id),
  KEY wound_assessment_id (wound_assessment_id),
  KEY ostomy_assessment_id (ostomy_assessment_id),
  KEY incision_assessment_id (incision_assessment_id),
  KEY drain_assessment_id (drain_assessment_id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY alpha_id (alpha_id),
  CONSTRAINT assessment_product_ibfk_4 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_product_ibfk_5 FOREIGN KEY (wound_id) REFERENCES wounds (id),
  CONSTRAINT assessment_product_ibfk_6 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE,
  CONSTRAINT assessment_product_ibfk_7 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE

) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;




# Dump of table assessment_sinustracts
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_sinustracts;

CREATE TABLE assessment_sinustracts (
  id int(11) NOT NULL AUTO_INCREMENT,
  alpha_id int(11) DEFAULT NULL,
  assessment_wound_id int(11) DEFAULT NULL,
  depth_cm int(11) DEFAULT NULL,
  depth_mm int(11) DEFAULT NULL,
  location_start int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY assessment_wound_id (assessment_wound_id),
  CONSTRAINT assessment_sinustracts_ibfk_1 FOREIGN KEY (assessment_wound_id) REFERENCES assessment_wound (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_treatment
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_treatment;

CREATE TABLE assessment_treatment (
  id int(11) NOT NULL AUTO_INCREMENT,
  wound_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  review_done int(11) DEFAULT NULL,
  vac_show int(11) DEFAULT NULL,
  vac_start_date datetime DEFAULT NULL,
  vac_end_date datetime DEFAULT NULL,
  pressure_reading int(11) DEFAULT NULL,
  therapy_setting int(11) DEFAULT NULL,
  adjunctive_other varchar(255) DEFAULT NULL,
  treatmentmodalities_show int(11) DEFAULT NULL,
  cs_date_show int(11) DEFAULT NULL,
  cs_date datetime DEFAULT NULL,
  cs_show int(11) DEFAULT NULL,
  bandages_in varchar(25) DEFAULT NULL,
  bandages_out varchar(25) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY assessment_id (assessment_id),
  CONSTRAINT assessment_treatment_ibfk_1 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;



# Dump of table assessment_treatment_arrays
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_treatment_arrays;

CREATE TABLE assessment_treatment_arrays (
  id int(11) NOT NULL AUTO_INCREMENT,
  assessment_treatment_id int(11) DEFAULT NULL,
  resource_id int(11) DEFAULT NULL,
  lookup_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  other varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY assessment_treatment_id (assessment_treatment_id),
  CONSTRAINT assessment_treatment_arrays_ibfk_1 FOREIGN KEY (assessment_treatment_id) REFERENCES assessment_treatment (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_undermining
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_undermining;

CREATE TABLE assessment_undermining (
  id int(11) NOT NULL AUTO_INCREMENT,
  alpha_id int(11) DEFAULT NULL,
  assessment_wound_id int(11) DEFAULT NULL,
  depth_cm int(11) DEFAULT NULL,
  depth_mm int(11) DEFAULT NULL,
  location_start int(11) DEFAULT NULL,
  location_end int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY assessment_wound_id (assessment_wound_id),
  CONSTRAINT assessment_undermining_ibfk_1 FOREIGN KEY (assessment_wound_id) REFERENCES assessment_wound (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table assessment_woundbed
# ------------------------------------------------------------

DROP TABLE IF EXISTS assessment_woundbed;

CREATE TABLE assessment_woundbed (
  id int(11) NOT NULL AUTO_INCREMENT,
  alpha_id int(11) DEFAULT NULL,
  assessment_wound_id int(11) DEFAULT NULL,
  wound_bed int(11) DEFAULT NULL,
  percentage int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY assessment_wound_id (assessment_wound_id),
  CONSTRAINT assessment_woundbed_ibfk_1 FOREIGN KEY (assessment_wound_id) REFERENCES assessment_wound (id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump of table auto_referrals
# ------------------------------------------------------------

DROP TABLE IF EXISTS auto_referrals;

CREATE TABLE auto_referrals (
  id int(11) NOT NULL AUTO_INCREMENT,
  alpha_id int(11) DEFAULT NULL,
  auto_referral_type varchar(255) DEFAULT NULL,
  initial_within_3weeks int(11) DEFAULT NULL,
  referral_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY referral_id (referral_id),
  CONSTRAINT auto_referrals_ibfk_1 FOREIGN KEY (referral_id) REFERENCES referrals_tracking (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table braden
# ------------------------------------------------------------

DROP TABLE IF EXISTS braden;

CREATE TABLE braden (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT '0',
  active int(11) DEFAULT NULL,
  current_flag int(11) DEFAULT NULL,
  braden_sensory int(1) DEFAULT '0',
  braden_moisture int(1) DEFAULT '0',
  braden_activity int(1) DEFAULT '0',
  braden_mobility int(1) DEFAULT '0',
  braden_nutrition int(1) DEFAULT '0',
  braden_friction int(1) DEFAULT '0',
  professional_id int(11) DEFAULT NULL,
  user_signature varchar(255) DEFAULT '',
  last_update varchar(100) DEFAULT '',
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY professional_id (professional_id),
  CONSTRAINT braden_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;



# Dump of table components
# ------------------------------------------------------------

DROP TABLE IF EXISTS components;

CREATE TABLE components (
  id int(11) DEFAULT NULL auto_increment,primary key(id),
  resource_id int(11) DEFAULT NULL,
  field_name varchar(50) DEFAULT NULL,
  field_type varchar(11) DEFAULT NULL,
  component_type varchar(8) DEFAULT '',
  flowchart int(11) DEFAULT NULL,
  label_key varchar(255) DEFAULT NULL,
  table_id int(11) DEFAULT NULL,
  orderby int(11) DEFAULT NULL,
  required int(11) DEFAULT NULL,
  enabled_close int(11) DEFAULT NULL,
  info_popup_id int(11) DEFAULT NULL,
  allow_edit int(11) DEFAULT NULL,
  validation text,

  allow_required int(11) DEFAULT NULL,
  validation_message text DEFAULT NULL,
  hide int(11) DEFAULT NULL,
  ignore_info_popup int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES components WRITE;
/*!40000 ALTER TABLE components DISABLE KEYS */;


INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (1,0,'assessment_type','INTEGER','AT',3,'pixalere.woundassessment.form.assessment_type',NULL,1,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (2,31,'status','INTEGER','DRP',3,'pixalere.woundassessment.form.wound_status',NULL,2,1,0,35,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (3,32,'discharge_reason','INTEGER','DRP',3,'pixalere.woundassessment.form.discharge_reason',NULL,3,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (4,0,'closed_date','STRING','DATE',3,'pixalere.woundassessment.form.closure_date',NULL,4,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (5,0,'wound_date','STRING','DATE',3,'pixalere.woundassessment.form.date_of_onset',NULL,3,1,0,34,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (6,0,'recurrent','INTEGER','YN',3,'pixalere.woundassessment.form.recurrent',NULL,6,1,0,36,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (7,97,'pain','INTEGER','DRP',3,'pixalere.woundassessment.form.pain',NULL,7,1,0,27,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (380,0,'pain_comments','STRING','TXTA',3,'pixalere.pain_comments',NULL,7,1,0,37,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (8,0,'length','INTEGER','MEAS',3,'pixalere.woundassessment.form.length',NULL,8,1,0,28,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (9,0,'width','INTEGER','MEAS',3,'pixalere.woundassessment.form.width',NULL,9,1,0,29,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (10,0,'depth','INTEGER','MEAS',3,'pixalere.woundassessment.form.depth',NULL,10,1,0,30,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (11,0,'undermining_location','STRING','MMAS',3,'pixalere.woundassessment.form.undermining',20,11,1,0,31,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (12,0,'undermining_depth','STRING','MMAS',3,'pixalere.woundassessment.form.undermining',20,12,1,0,90,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (13,0,'sinus_depth','STRING','MMAS',3,'pixalere.woundassessment.form.sinus_tract',19,14,1,0,32,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (381,0,'fistulas','STRING','MTXT',3,'pixalere.woundassessment.form.fistulas',NULL,14,1,0,33,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (14,35,'wound_base','STRING','MPER',3,'pixalere.woundassessment.form.wound_base',21,14,1,0,24,1,'var c =0;var perc=0;forINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (i=0;i<document.form.woundbase.length;i++){ifINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (document.form.woundbase[i].checked==true){ifINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (document.getElementByIdINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (\"woundbase_percentage_\"+INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (i+1)).selectedIndex == -1 || document.getElementByIdINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (\"woundbase_percentage_\"+INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (i+1)).selectedIndex == 0){error=1;}var percentage = document.getElementByIdINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (\"woundbase_percentage_\"+INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (i+1)).options[document.getElementByIdINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (\"woundbase_percentage_\"+INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (i+1)).selectedIndex].value; ifINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (c == 0){perc=\"\"+percentage;}else{perc=perc+\",\"+percentage; }c++;}}document.form.woundbase_percentage.value=perc;ifINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (calcTotalWoundBedINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES () != 100 && calcTotalWoundBedINSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES () != 0){themessage=themessage + \"pixalere.woundassessment.error.percentage,\";error=1;}',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (15,24,'exudate_type','STRING','CHKB',3,'pixalere.woundassessment.form.exudate_type',NULL,15,1,0,38,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (16,25,'exudate_amount','INTEGER','RADO',3,'pixalere.woundassessment.form.exudate_amount',NULL,16,1,0,39,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (17,26,'exudate_odour','INTEGER','YN',3,'pixalere.woundassessment.form.odour',NULL,17,1,0,40,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (18,34,'wound_edge','STRING','CHKB',3,'pixalere.woundassessment.form.wound_edge',NULL,18,1,0,41,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (19,37,'periwound_skin','STRING','CHKB',3,'pixalere.woundassessment.form.periwound_skin',NULL,19,1,0,42,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (23,0,'products','STRING','PROD',3,'pixalere.woundassessment.form.products',NULL,23,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (24,0,'body','STRING','TXTA',3,'pixalere.woundassessment.form.treatment_comments',16,24,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (27,0,'vac_start_date','DATE','DATE',3,'pixalere.woundassessment.form.vacstart',36,27,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (28,0,'vac_end_date','DATE','DATE',3,'pixalere.woundassessment.form.vacend',36,28,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (29,40,'pressure_reading','INTEGER','DRP',3,'pixalere.woundassessment.form.pressure_reading',36,29,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (30,17,'adjunctive','STRING','CHKB',3,'pixalere.woundassessment.form.adjunctive',27,37,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (423,38,'csresult','STRING','CHKB',3,'pixalere.woundassessment.form.c_s_result',27,34,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (473,0,'antibiotics','STRING','MDDA',3,'pixalere.treatment.form.antibiotics',38,35,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (31,0,'images','STRING','IMG',3,'pixalere.woundassessment.form.images',NULL,38,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (450,0,'assessment_type','INTEGER','AT',4,'pixalere.woundassessment.form.assessment_type',NULL,1,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (32,155,'status','INTEGER','DRP',4,'pixalere.ostomyassessment.form.wound_status',NULL,1,0,0,51,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (33,149,'discharge_reason','INTEGER','DRP',4,'pixalere.ostomyassessment.form.discharge_reason',NULL,2,1,1,52,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (34,0,'closed_date','STRING','DATE',4,'pixalere.ostomyassessment.form.closure_date',NULL,3,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (35,0,'postop_days','INTEGER','NUM',4,'pixalere.ostomyassessment.form.postop_day',NULL,4,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (386,0,'clinical_path','INTEGER','OON',4,'pixalere.ostomyassessment.form.clinical_path',NULL,5,0,0,92,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (387,0,'clinical_path_date','STRING','DATE',4,'pixalere.ostomyassessment.form.clinical_path_date',NULL,6,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (410,135,'construction','INTEGER','RADO',4,'pixalere.ostomyassessment.form.construction',NULL,7,1,0,53,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (448,0,'circumference_mm','INTEGER','OMEA',4,'pixalere.ostomyassessment.form.circumference_mm',NULL,9,1,0,54,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (454,0,'length_mm','INTEGER','OMEA',4,'pixalere.ostomyassessment.form.length',NULL,9,1,0,55,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (455,0,'width_mm','INTEGER','OMEA',4,'pixalere.ostomyassessment.form.width',NULL,9,1,0,56,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (56,78,'shape','INTEGER','RADO',4,'pixalere.ostomyassessment.form.stomashape',NULL,8,1,0,57,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (57,62,'profile','STRING','CHKB',4,'pixalere.ostomyassessment.form.profile',NULL,10,1,0,58,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (59,74,'appearance','STRING','CHKB',4,'pixalere.ostomyassessment.form.appearance',NULL,11,1,0,59,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (61,89,'devices','INTEGER','RADO',4,'pixalere.ostomyassessment.form.devices',NULL,12,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (60,75,'contour','STRING','CHKB',4,'pixalere.ostomyassessment.form.skincontour',NULL,13,1,0,61,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (58,67,'pouching','STRING','CHKB',4,'pixalere.ostomyassessment.form.pouching',NULL,14,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (414,138,'mucocutaneous_margin','STRING','CHKB',4,'pixalere.ostomyassessment.form.mucocutaneousmargin',NULL,15,1,0,62,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (456,0,'mucocutaneous_margin_location','STRING','MMAS',4,'pixalere.ostomyassessment.form.mucocutaneous_margin_area',NULL,16,1,0,50,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (457,0,'mucocutaneous_margin_depth','STRING','MMAS',4,'pixalere.ostomyassessment.form.mucocutaneous_margin_area',NULL,16,1,0,60,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (415,0,'mucocutaneous_margin_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.mucocutaneousmargin_comments',NULL,17,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (46,69,'periostomy_skin','STRING','CHKB',4,'pixalere.ostomyassessment.form.periostomyskin',NULL,18,1,0,63,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (44,0,'mucous_fistula_present','INTEGER','YN',4,'pixalere.ostomyassessment.form.mucous_fistula_present',NULL,19,1,0,64,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (411,137,'drainage','STRING','CHKB',4,'pixalere.ostomyassessment.form.drainage',NULL,20,1,0,65,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (416,139,'perifistula_skin','STRING','CHKB',4,'pixalere.ostomyassessment.form.perifistulaskin',NULL,21,1,0,66,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (453,0,'drainage_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.drainage_comments',NULL,21,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (47,66,'urine_colour','STRING','CHKB',4,'pixalere.ostomyassessment.form.urinecolour',NULL,22,1,0,67,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (48,70,'urine_type','STRING','CHKB',4,'pixalere.ostomyassessment.form.urinetype',NULL,23,1,0,68,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (49,71,'urine_quantity','INTEGER','RADO',4,'pixalere.ostomyassessment.form.urinequantity',NULL,24,1,0,69,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (460,61,'stool_colour','STRING','CHKB',4,'pixalere.ostomyassessment.form.stoolcolour',NULL,26,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (461,72,'stool_consistency','STRING','CHKB',4,'pixalere.ostomyassessment.form.stoolconsistency',NULL,26,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (51,73,'stool_quantity','INTEGER','RADO',4,'pixalere.ostomyassessment.form.stoolquantity',NULL,25,1,0,70,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (52,0,'nutritional_status_other','STRING','TXTA',4,'pixalere.ostomyassessment.form.nutritionalstatusother',NULL,33,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (45,65,'flange_pouch','INTEGER','RADO',4,'pixalere.ostomyassessment.form.flangepouch',NULL,34,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (412,0,'flange_pouch_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.flangepouch_comments',NULL,35,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (390,68,'self_care_progress','STRING','CHKB',4,'pixalere.ostomyassessment.form.selfcareprogress',NULL,37,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (413,0,'self_care_progress_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.selfcareprogress_comments',NULL,37,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (446,147,'nutritional_status','INTEGER','RADO',4,'pixalere.ostomyassessment.form.nutritional_status',NULL,32,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (417,38,'csresult','STRING','CHKB',4,'pixalere.woundassessment.form.c_s_result',27,46,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (68,0,'images','STRING','IMG',4,'pixalere.woundassessment.form.images',NULL,48,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (146,0,'products','STRING','PROD',4,'pixalere.woundassessment.form.products',NULL,46,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (147,0,'body','STRING','TXTA',4,'pixalere.woundassessment.form.treatment_comments',16,46,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (156,0,'wound_location_detailed','STRING','TXT',8,'pixalere.woundprofile.form.wound_location',7,1,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (176,0,'wound_location_detailed','STRING','TXT',9,'pixalere.woundprofile.form.wound_location',7,1,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (226,0,'fix','STRING','FIX',2,'pixalere.viewer.fixes',NULL,0,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (451,0,'assessment_type','INTEGER','AT',5,'pixalere.woundassessment.form.assessment_type',NULL,1,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (69,154,'status','INTEGER','DRP',5,'pixalere.incisionassessment.form.wound_status',NULL,1,1,0,72,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (425,0,'postop_days','INTEGER','NUM',5,'pixalere.incisionassessment.form.postop_day',NULL,4,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (426,0,'clinical_path','INTEGER','OON',5,'pixalere.ostomyassessment.form.clinical_path',NULL,5,0,0,93,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (427,0,'clinical_path_date','STRING','DATE',5,'pixalere.ostomyassessment.form.clinical_path_date',NULL,6,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (70,150,'discharge_reason','INTEGER','DRP',5,'pixalere.incisionassessment.form.discharge_reason',NULL,2,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (71,0,'closed_date','STRING','DATE',5,'pixalere.incisionassessment.form.closure_date',NULL,3,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (459,152,'postop_management','STRING','CHKB',5,'pixalere.incisionassessment.form.postop_management',NULL,6,1,0,83,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (72,97,'pain','INTEGER','DRP',5,'pixalere.incisionassessment.form.pain',NULL,6,1,0,84,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (378,0,'pain_comments','STRING','TXTA',5,'pixalere.pain_comments',NULL,6,1,0,85,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (75,92,'incision_status','INTEGER','RADO',5,'pixalere.incisionassessment.form.incisionstatus',NULL,7,1,0,87,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (76,90,'incision_exudate','STRING','CHKB',5,'pixalere.incisionassessment.form.incisionexudate',NULL,8,1,0,88,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (77,91,'peri_incisional','STRING','CHKB',5,'pixalere.incisionassessment.form.ssinfection',NULL,10,1,0,89,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (78,0,'products','STRING','PROD',5,'pixalere.incisionassessment.form.products',NULL,30,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (81,0,'images','STRING','IMG',5,'pixalere.woundassessment.form.images',NULL,36,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (377,0,'body','STRING','TXTA',5,'pixalere.woundassessment.form.treatment_comments',16,32,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (209,99,'incision_closure_methods','STRING','CHKB',5,'pixalere.incisionassessment.form.incision_closure_methods',NULL,7,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (211,101,'exudate_amount','INTEGER','DRP',5,'pixalere.incisionassessment.form.incision_exudate_amount',NULL,9,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (421,38,'csresult','STRING','CHKB',5,'pixalere.woundassessment.form.c_s_result',27,33,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (475,0,'antibiotics','STRING','MDDA',5,'pixalere.treatment.form.antibiotics',38,35,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (452,0,'assessment_type','INTEGER','AT',6,'pixalere.woundassessment.form.assessment_type',NULL,1,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (428,153,'status','INTEGER','DRP',6,'pixalere.woundassessment.form.status',NULL,2,1,0,77,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (196,93,'type_of_drain','INTEGER','DRP',6,'pixalere.drainassessment.form.type_of_drain',NULL,5,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (200,0,'type_of_drain_other','STRING','TXT',6,'pixalere.drainassessment.form.type_of_drain_other',NULL,6,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (429,0,'drain_removed_intact','INTEGER','YN',6,'pixalere.drainassessment.form.drain_removed_intact',NULL,4,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (430,0,'drain_removed_date','STRING','DATE',6,'pixalere.drainassessment.form.drain_removed_date',NULL,2,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (431,144,'discharge_reason','INTEGER','DRP',6,'pixalere.drainassessment.form.drain_removed_reason',NULL,3,1,1,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (197,0,'sutured','INTEGER','YNN',6,'pixalere.drainassessment.form.sutured',NULL,8,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (215,102,'drain_site','STRING','CHKB',6,'pixalere.drainassessment.form.drain_site',NULL,9,1,0,78,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (433,0,'additional_days_date','STRING','DATE',6,'pixalere.drainassessment.form.additional_days_date',15,11,1,0,79,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (434,0,'drainage_amount_mls','INTEGER','TXT',6,'pixalere.drainassessment.form.drainage_amount_mls',15,12,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (435,0,'drainage_amount_hrs','INTEGER','TXT',6,'pixalere.drainassessment.form.drainage_amount_hrs',15,13,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (436,0,'drainage_amount','INTEGER','CLK',6,'pixalere.drainassessment.form.drainage_amount_start',15,14,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (199,95,'drain_characteristics','STRING','CHKB',6,'pixalere.drainassessment.form.characteristics',15,15,1,0,80,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (447,0,'tubes_changed_date','STRING','DATE',6,'pixalere.drainassessment.form.tubes_changed_date',NULL,16,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (198,94,'peri_drain_area','STRING','CHKB',6,'pixalere.drainassessment.form.peri_drain_area',NULL,17,1,0,81,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (419,38,'csresult','STRING','CHKB',6,'pixalere.woundassessment.form.c_s_result',27,38,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (478,0,'antibiotics','STRING','MDDA',6,'pixalere.treatment.form.antibiotics',38,40,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (202,0,'products','STRING','PROD',6,'pixalere.woundassessment.form.products',NULL,35,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (208,0,'body','STRING','TXTA',6,'pixalere.woundassessment.form.treatment_comments',16,36,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (204,0,'images','STRING','IMG',6,'pixalere.woundassessment.form.images',NULL,41,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (83,98,'therapy_setting','INTEGER','RADO',3,'pixalere.woundassessment.form.reading_group',36,28,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (87,1,'funding_source','INTEGER','DRP',1,'pixalere.patientprofile.form.funding_source',NULL,9,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (88,0,'treatment_location_id','INTEGER','DRP',1,'pixalere.patientprofile.form.treatment_location',NULL,8,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (89,7,'gender','INTEGER','DRP',1,'pixalere.patientprofile.form.gender',NULL,3,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (90,0,'dob','STRING','DOB',1,'pixalere.patientprofile.form.date_of_birth',NULL,4,1,0,0,1,'dob_year : {canadianDate: {data: [\'#dob_day\',\'#dob_month\',\'#dob_year\']}}},messages: {dob_year: \'Valid Date only\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (91,0,'age','INTEGER','AGE',1,'pixalere.patientprofile.form.age',NULL,5,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (92,0,'allergies','STRING','TXTA',1,'pixalere.patientprofile.form.allergies',NULL,6,1,0,99,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (93,0,'professionals','STRING','TXTA',1,'pixalere.patientprofile.form.health_care_professional_list',NULL,10,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (94,6,'interfering_factors','STRING','CHKB',1,'pixalere.patientprofile.form.factors_that_interfere',24,11,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (95,0,'interfering_factors_other','STRING','TXT',1,'pixalere.patientprofile.form.factors_that_interfere_other',NULL,12,1,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (96,165,'medications_comments','STRING','CHKB',1,'pixalere.patientprofile.form.current_medications',24,13,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (97,0,'co_morbidities','STRING','MLST',1,'pixalere.patientprofile.form.co_morbidities',24,10,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (217,0,'surgical_history','STRING','TXTA',1,'pixalere.patientprofile.form.surgical_history',NULL,10,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (218,104,'braden_sensory','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_sensory',13,14,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (219,105,'braden_moisture','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_moisture',13,14,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (220,106,'braden_activity','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_activity',13,14,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (221,107,'braden_mobility','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_mobility',13,14,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (222,108,'braden_nutrition','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_nutrition',13,14,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (223,109,'braden_friction','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_friction',13,14,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (224,110,'braden_score','INTEGER','BRT',13,'pixalere.patientprofile.form.braden_score',NULL,14,1,0,48,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (342,16,'investigation','STRING','DDA',1,'pixalere.patientprofile.form.other_investigations',35,15,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (343,13,'albumin','INTEGER','TXT',1,'pixalere.patientprofile.form.albumin',NULL,20,1,0,0,1,'albumin: {number: true,maxlength: 5}},messages: {albumin: \'Numbers and Decimal only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (344,14,'pre_albumin','INTEGER','TXT',1,'pixalere.patientprofile.form.pre_albumin',NULL,22,1,0,0,1,'pre_albumin: {number: true,maxlength: 5}},messages: {pre_albumin: \'Numbers and Decimal only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (345,0,'blood_sugar','DOUBLE','TXT',1,'pixalere.patientprofile.form.blood_sugar',NULL,25,1,0,0,1,'blood_sugar: {number: true}},messages: {blood_sugar: \'Numbers and Decimal only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (346,0,'blood_sugar_date','DATE','DATE',1,'pixalere.patientprofile.form.date_taken',NULL,30,1,0,0,1,'blood_sugar_date_year : {canadianDate: {data: [\'#blood_sugar_date_day\',\'#blood_sugar_date_month\',\'#blood_sugar_date_year\']}}},messages: {blood_sugar_date_year: \'Valid Date only\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (347,54,'blood_sugar_meal','INTEGER','TXT',1,'pixalere.patientprofile.form.blood_sugar_meal',NULL,35,1,0,0,1,'blood_sugar_meal: {digits: true}},messages: {blood_sugar_meal: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (348,0,'albumin_date','STRING','DATE',1,'pixalere.patientprofile.form.albumin_date',NULL,21,1,0,0,1,'albumin_date_year : {canadianDate: {data: [\'#albumin_date_day\',\'#albumin_date_month\',\'#albumin_date_year\']}}},messages: {albumin_date_year: \'Valid Date only\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (349,0,'prealbumin_date','STRING','DATE',1,'pixalere.patientprofile.form.prealbumin_date',NULL,23,1,0,0,1,'prealbumin_date_year : {canadianDate: {data: [\'#prealbumin_date_day\',\'#prealbumin_date_month\',\'#prealbumin_date_year\']}}},messages: {prealbumin_date_year: \'Valid Date only\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (98,0,'wound_location_detailed','STRING','TXT',2,'pixalere.woundprofile.form.wound_location',7,1,1,0,95,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (100,8,'etiology','STRING','SALM',2,'pixalere.woundprofile.form.etiology',33,3,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (102,0,'reason_for_care','STRING','TXTA',2,'pixalere.woundprofile.form.reason_for_care',NULL,2,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (103,86,'goals','STRING','SALP',2,'pixalere.woundprofile.form.goals',34,6,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (440,0,'operative_procedure_comments','STRING','TXTA',2,'pixalere.woundprofile.form.operative_procedure_comments',NULL,7,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (441,0,'date_surgery','STRING','DATE',2,'pixalere.woundprofile.form.date_surgery',NULL,8,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (442,0,'surgeon','STRING','TXT',2,'pixalere.woundprofile.form.surgeon',NULL,9,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (443,0,'marking_prior_surgery','INTEGER','YN',2,'pixalere.woundprofile.form.marking_prior_surgery',NULL,10,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (462,148,'type_of_ostomy','STRING','CHKB',2,'pixalere.woundprofile.form.type_of_ostomy',NULL,9,1,0,94,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (444,89,'patient_limitations','STRING','CHKB',1,'pixalere.woundprofile.form.patient_limitations',24,11,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (445,146,'teaching_goals','INTEGER','DRP',2,'pixalere.woundprofile.form.teaching_goals',NULL,12,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (141,0,'sinus_location','STRING','MMAS',3,'pixalere.woundassessment.form.sinus_tract',19,13,1,0,91,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (227,110,'left_missing_limbs','STRING','CHKB',11,'pixalere.patientprofile.form.left_missing_limb',NULL,20,0,0,45,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (228,110,'right_missing_limbs','STRING','CHKB',11,'pixalere.patientprofile.form.right_missing_limb',NULL,21,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (229,111,'left_pain_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.left_pain_assessment',NULL,22,0,0,46,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (230,111,'right_pain_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.right_pain_assessment',NULL,23,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (231,0,'pain_comments','STRING','TXTA',11,'pixalere.patientprofile.form.pain_comments',NULL,24,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (233,112,'left_skin_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.left_skin_assessment',NULL,26,0,0,47,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (234,112,'right_skin_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.right_skin_assessment',NULL,27,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (235,19,'left_temperature_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.left_temperature_leg',NULL,28,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (236,19,'right_temperature_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.right_temperature_leg',NULL,29,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (237,19,'left_temperature_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.left_temperature_foot',NULL,30,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (238,19,'right_temperature_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.right_temperature_foot',NULL,31,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (239,19,'left_temperature_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.left_temperature_toes',NULL,32,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (240,19,'right_temperature_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.right_temperature_toes',NULL,33,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (241,18,'left_skin_colour_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.left_skin_colour_leg',NULL,34,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (242,18,'right_skin_colour_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.right_skin_colour_leg',NULL,35,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (243,18,'left_skin_colour_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.left_skin_colour_foot',NULL,36,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (244,18,'right_skin_colour_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.right_skin_colour_foot',NULL,37,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (245,18,'left_skin_colour_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.left_skin_colour_toes',NULL,38,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (246,18,'right_skin_colour_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.right_skin_colour_toes',NULL,39,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (247,23,'left_dorsalis_pedis_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.left_dorsalis_pedis_palpation',NULL,40,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (248,23,'right_dorsalis_pedis_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.right_dorsalis_pedis_palpation',NULL,41,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (249,23,'left_posterior_tibial_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.left_posterior_tibial_palpation',NULL,42,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (250,23,'right_posterior_tibial_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.right_posterior_tibial_palpation',NULL,43,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (251,127,'doppler_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.doppler',NULL,92,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (253,23,'left_dorsalis_pedis_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.left_dorsalis_pedis_doppler',NULL,93,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (254,23,'right_dorsalis_pedis_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.right_dorsalis_pedis_doppler',NULL,94,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (407,23,'left_posterior_tibial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.left_posterior_tibial_doppler',NULL,95,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (408,23,'right_posterior_tibial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.right_posterior_tibial_doppler',NULL,96,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (255,23,'left_interdigitial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.left_interdigitial_doppler',NULL,97,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (256,23,'right_interdigitial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.right_interdigitial_doppler',NULL,98,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (257,127,'ankle_brachial_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.ankle_brachial_by',NULL,99,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (259,0,'ankle_brachial_date','STRING','DATE',11,'pixalere.patientprofile.form.ankle_brachial_date',NULL,100,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (261,0,'left_tibial_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.left_tibial_pedis',NULL,101,0,0,0,1,'left_tibial_pedis_ankle_brachial: {digits: true}},messages: {left_tibial_pedis_ankle_brachial: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (262,0,'right_tibial_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.right_tibial_pedis',NULL,102,0,0,0,1,'right_tibial_pedis_ankle_brachial: {digits: true}},messages: {right_tibial_pedis_ankle_brachial: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (263,0,'left_dorsalis_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.left_dorsalis_pedis_ankle_brachial',NULL,103,0,0,0,1,'left_dorsalis_pedis_ankle_brachial: {digits: true}},messages: {left_dorsalis_pedis_ankle_brachial: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (264,0,'right_dorsalis_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.right_dorsalis_pedis_ankle_brachial',NULL,104,0,0,0,1,'right_dorsalis_pedis_ankle_brachial: {digits: true}},messages: {right_dorsalis_pedis_ankle_brachial: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (265,0,'left_ankle_brachial','STRING','TXT',11,'pixalere.patientprofile.form.left_ankle_brachial',NULL,105,0,0,0,1,'left_ankle_brachial: {digits: true}},messages: {left_ankle_brachial: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (266,0,'right_ankle_brachial','STRING','TXT',11,'pixalere.patientprofile.form.right_ankle_brachial',NULL,106,0,0,0,1,'right_ankle_brachial: {digits: true}},messages: {right_ankle_brachial: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (267,0,'left_abi_score_ankle_brachial','STRING','BLSC',11,'pixalere.patientprofile.form.left_abi_score_ankle_brachial',NULL,107,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (268,0,'right_abi_score_ankle_brachial','STRING','BRSC',11,'pixalere.patientprofile.form.right_abi_score_ankle_brachial',NULL,108,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (269,127,'toe_brachial_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.toe_brachial_by',NULL,109,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (271,0,'toe_brachial_date','STRING','DATE',11,'pixalere.patientprofile.form.toe_brachial_date',NULL,110,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (273,0,'left_toe_pressure','STRING','TXT',11,'pixalere.patientprofile.form.left_toe_pressure',NULL,111,0,0,0,1,'left_toe_pressure: {digits: true}},messages: {left_toe_pressure: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (274,0,'right_toe_pressure','STRING','TXT',11,'pixalere.patientprofile.form.right_toe_pressure',NULL,112,0,0,0,1,'right_toe_pressure: {digits: true}},messages: {right_toe_pressure: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (275,0,'left_brachial_pressure','STRING','TXT',11,'pixalere.patientprofile.form.left_brachial_pressure',NULL,113,0,0,0,1,'left_brachial_pressure: {digits: true}},messages: {left_brachial_pressure: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (276,0,'right_brachial_pressure','STRING','TXT',11,'pixalere.patientprofile.form.right_brachial_pressure',NULL,114,0,0,0,1,'right_brachial_pressure: {digits: true}},messages: {right_brachial_pressure: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (277,0,'left_tbi_score','STRING','TLSC',11,'pixalere.patientprofile.form.left_tbi_score',NULL,115,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (278,0,'right_tbi_score','STRING','TRSC',11,'pixalere.patientprofile.form.right_tbi_score',NULL,116,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (352,0,'transcutaneous_date','STRING','DATE',11,'pixalere.patientprofile.form.transcutaneous_date',NULL,118,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (354,127,'transcutaneous_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.transcutaneous_by',NULL,117,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (279,128,'left_transcutaneous_pressure','INTEGER','DRP',11,'pixalere.patientprofile.form.left_transcutaneous_pressures',NULL,119,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (280,128,'right_transcutaneous_pressure','INTEGER','DRP',11,'pixalere.patientprofile.form.right_transcutaneous_pressures',NULL,120,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (281,21,'left_edema_severity','INTEGER','DRP',11,'pixalere.patientprofile.form.left_edema_severity',NULL,74,0,0,74,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (282,21,'right_edema_severity','INTEGER','DRP',11,'pixalere.patientprofile.form.right_edema_severity',NULL,75,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (283,20,'left_edema_location','INTEGER','DRP',11,'pixalere.patientprofile.form.left_edema_location',NULL,76,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (284,20,'right_edema_location','INTEGER','DRP',11,'pixalere.patientprofile.form.right_edema_location',NULL,77,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (285,0,'sleep_positions','STRING','TXTA',11,'pixalere.patientprofile.form.sleep_positions',NULL,78,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (287,0,'left_ankle','INTEGER','MEAS',11,'pixalere.patientprofile.form.left_ankle',NULL,80,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (288,0,'right_ankle','INTEGER','MEAS',11,'pixalere.patientprofile.form.right_ankle',NULL,81,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (291,0,'left_midcalf','INTEGER','MEAS',11,'pixalere.patientprofile.form.left_midcalf',NULL,82,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (292,0,'right_midcalf','INTEGER','MEAS',11,'pixalere.patientprofile.form.right_midcalf',NULL,83,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (295,126,'left_sensation','STRING','CHKB',11,'pixalere.patientprofile.form.left_sensation',NULL,84,0,0,75,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (296,126,'right_sensation','STRING','CHKB',11,'pixalere.patientprofile.form.right_sensation',NULL,85,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (297,0,'left_score_sensation','STRING','TXT',11,'pixalere.patientprofile.form.left_score_sensation',NULL,86,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (298,0,'right_score_sensation','STRING','TXT',11,'pixalere.patientprofile.form.right_score_sensation',NULL,87,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (299,113,'left_sensory','STRING','CHKB',11,'pixalere.patientprofile.form.left_sensory',NULL,88,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (300,113,'right_sensory','STRING','CHKB',11,'pixalere.patientprofile.form.right_sensory',NULL,89,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (301,114,'left_proprioception','INTEGER','DRP',12,'pixalere.patientprofile.form.left_proprioception',NULL,90,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (302,114,'right_proprioception','INTEGER','DRP',12,'pixalere.patientprofile.form.right_proprioception',NULL,91,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (328,115,'left_foot_deformities','STRING','CHKB',12,'pixalere.patientprofile.form.left_deformities',NULL,2,0,0,76,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (331,115,'right_foot_deformities','STRING','CHKB',12,'pixalere.patientprofile.form.right_deformities',NULL,3,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (329,116,'left_foot_skin','STRING','CHKB',12,'pixalere.patientprofile.form.left_skin',NULL,4,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (330,117,'left_foot_toes','STRING','CHKB',12,'pixalere.patientprofile.form.left_toes',NULL,6,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (332,116,'right_foot_skin','STRING','CHKB',12,'pixalere.patientprofile.form.right_skin',NULL,5,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (333,117,'right_foot_toes','STRING','CHKB',12,'pixalere.patientprofile.form.right_toes',NULL,7,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (403,118,'weight_bearing','INTEGER','DRP',12,'pixalere.patientprofile.form.weight_bearing_status',NULL,9,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (304,119,'balance','INTEGER','DRP',12,'pixalere.patientprofile.form.balance',NULL,10,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (305,120,'calf_muscle_pump','INTEGER','DRP',12,'pixalere.patientprofile.form.calf_muscle_pump',NULL,11,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (306,121,'mobility_aids','INTEGER','DRP',12,'pixalere.patientprofile.form.mobility_aids',NULL,13,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (307,0,'calf_muscle_comments','STRING','TXTA',12,'pixalere.patientprofile.form.calf_muscle_comments',NULL,12,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (308,0,'mobility_aids_comments','STRING','TXTA',12,'pixalere.patientprofile.form.mobility_aids_comments',NULL,14,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (309,0,'gait_pattern_comments','STRING','TXTA',12,'pixalere.patientprofile.form.gait_pattern_comments',NULL,15,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (310,0,'walking_distance_comments','STRING','TXTA',12,'pixalere.patientprofile.form.walking_distance_comments',NULL,16,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (311,0,'walking_endurance_comments','STRING','TXTA',12,'pixalere.patientprofile.form.walking_endurance_comments',NULL,17,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (312,0,'indoor_footwear','INTEGER','YN',12,'pixalere.patientprofile.form.indoor_footwear',NULL,19,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (313,0,'outdoor_footwear','INTEGER','YN',12,'pixalere.patientprofile.form.outdoor_footwear',NULL,21,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (314,0,'orthotics','INTEGER','YN',12,'pixalere.patientprofile.form.orthotics',NULL,23,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (315,0,'indoor_footwear_comments','STRING','TXTA',12,'pixalere.patientprofile.form.indoor_footwear_comments',NULL,20,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (316,0,'outdoor_footwear_comments','STRING','TXTA',12,'pixalere.patientprofile.form.outdoor_footwear_comments',NULL,22,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (317,0,'orthotics_comments','STRING','TXTA',12,'pixalere.patientprofile.form.orthotics_comments',NULL,24,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (318,122,'muscle_tone_functional','INTEGER','DRP',12,'pixalere.patientprofile.form.muscle_tone',NULL,26,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (319,123,'arches_foot_structure','INTEGER','DRP',12,'pixalere.patientprofile.form.arches_foot_structure',NULL,27,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (320,0,'supination_foot_structure','INTEGER','YN',12,'pixalere.patientprofile.form.supination',NULL,28,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (321,0,'pronation_foot_structure','INTEGER','YN',12,'pixalere.patientprofile.form.pronation',NULL,29,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (322,124,'dorsiflexion_active','STRING','CHKB',12,'pixalere.patientprofile.form.dorsiflexion_active',NULL,30,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (323,125,'dorsiflexion_passive','STRING','CHKB',12,'pixalere.patientprofile.form.dorsiflexion_passive',NULL,31,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (324,124,'plantarflexion_active','STRING','CHKB',12,'pixalere.patientprofile.form.plantarflexion_active',NULL,32,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (325,125,'plantarflexion_passive','STRING','CHKB',12,'pixalere.patientprofile.form.plantarflexion_passive',NULL,33,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (326,124,'greattoe_active','STRING','CHKB',12,'pixalere.patientprofile.form.greattoes_active',NULL,34,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (327,125,'greattoe_passive','STRING','CHKB',12,'pixalere.patientprofile.form.greattoes_passive',NULL,35,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (382,0,'comments','STRING','COMM',3,'pixalere.woundassessment.form.contains_comments',NULL,999,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (383,0,'comments','STRING','COMM',4,'pixalere.woundassessment.form.contains_comments',NULL,999,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (384,0,'comments','STRING','COMM',5,'pixalere.woundassessment.form.contains_comments',NULL,999,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (385,0,'comments','STRING','COMM',6,'pixalere.woundassessment.form.contains_comments',NULL,999,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (467,0,'left_less_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.left_capillary_less',NULL,39,0,0,73,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (468,0,'right_less_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.right_capillary_less',NULL,39,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (469,0,'left_more_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.left_capillary_more',NULL,39,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (470,0,'right_more_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.right_capillary_more',NULL,39,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (483,0,'referral','STRING','REF',3,'pixalere.referral.form.title_fc',NULL,998,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (484,0,'referral','STRING','REF',4,'pixalere.referral.form.title_fc',NULL,998,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (485,0,'referral','STRING','REF',5,'pixalere.referral.form.title_fc',NULL,998,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (486,0,'referral','STRING','REF',6,'pixalere.referral.form.title_fc',NULL,998,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (487,0,'foot_comments','STRING','TXTA',12,'pixalere.patientprofile.form.foot_comments',NULL,998,0,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (507,0,'bandages_in','STRING','TXT',3,'pixalere.treatment.form.bandages_in',36,25,0,0,0,1,'bandages_out: {digits: true}},messages: {bandages_out: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (505,97,'pain','INTEGER','DRP',6,'pixalere.incisionassessment.form.pain',NULL,9,1,0,84,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (506,0,'pain_comments','STRING','TXTA',6,'pixalere.pain_comments',NULL,10,1,0,85,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (508,38,'cs_show','INTEGER','YN',4,'pixalere.treatment.form.cs_done',NULL,45,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (509,38,'cs_show','INTEGER','YN',6,'pixalere.treatment.form.cs_done',NULL,37,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (510,38,'cs_show','INTEGER','YN',5,'pixalere.treatment.form.cs_done',NULL,32,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (511,38,'cs_show','INTEGER','YN',3,'pixalere.treatment.form.cs_done',NULL,32,1,0,0,1,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (519,0,'maintenance','STRING','ADM',3,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (520,0,'maintenance','STRING','ADM',4,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (521,0,'maintenance','STRING','ADM',5,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (522,0,'maintenance','STRING','ADM',6,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (523,0,'maintenance','STRING','ADM',17,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (524,0,'maintenance','STRING','ADM',1,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (525,0,'maintenance','STRING','ADM',2,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (526,0,'maintenance','STRING','ADM',7,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (527,0,'maintenance','STRING','ADM',8,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (528,0,'maintenance','STRING','ADM',9,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (529,0,'maintenance','STRING','ADM',10,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (530,0,'maintenance','STRING','ADM',11,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (531,0,'maintenance','STRING','ADM',12,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (532,0,'maintenance','STRING','ADM',13,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (533,0,'height','DOUBLE','TXT',40,'pixalere.patientprofile.form.height',NULL,1,0,0,0,1,'height: {number: true}},messages: {height: \'Numbers and Decimal only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (534,0,'weight','DOUBLE','TXT',40,'pixalere.patientprofile.form.weight',NULL,7,0,0,0,1,'weight: {number: true}},messages: {weight: \'Numbers and Decimal only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (535,0,'temperature','DOUBLE','TXT',40,'pixalere.patientprofile.form.temperature',NULL,10,0,0,0,1,'temperature: {number: true}},messages: {temperature: \'Numbers and Decimal only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (536,0,'pulse','INTEGER','TXT',40,'pixalere.patientprofile.form.pulse',NULL,15,0,0,0,1,'pulse: {digits: true}},messages: {pulse: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (537,0,'resp_rate','INTEGER','TXT',40,'pixalere.patientprofile.form.resp_rate',NULL,20,0,0,0,1,'resp_rate: {digits: true}},messages: {resp_rate: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (545,0,'blood_pressure','STRING','TXT',40,'pixalere.patientprofile.form.blood_pressure',NULL,60,0,0,0,1,'blood_pressure: {numberslash: true}},messages: {blood_pressure: \'Numbers and slashes only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (544,0,'maintenance','STRING','ADM',40,'pixalere.viewer.assessment_updates',NULL,1000,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (546,0,'pdf','STRING','PDF',3,'pixalere.viewer.pdf',NULL,1010,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (547,0,'pdf','STRING','PDF',4,'pixalere.viewer.pdf',NULL,1010,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (548,0,'pdf','STRING','PDF',5,'pixalere.viewer.pdf',NULL,1010,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (549,0,'pdf','STRING','PDF',6,'pixalere.viewer.pdf',NULL,1010,0,0,0,0,NULL,1,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (551,0,'bandages_out','STRING','TXT',3,'pixalere.treatment.form.bandages_out',36,26,0,0,0,1,'bandages_in: {digits: true}},messages: {bandages_in: \'Numbers only please\'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (552,0,'phn','STRING','TXT',1,'pixalere.patientaccount.form.phn',NULL,9,0,0,0,0,'',0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (553,0,'review_done','INTEGER','YN',3,'pixalere.treatment.review_done',36,960,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (554,0,'review_done','INTEGER','YN',4,'pixalere.treatment.review_done',36,960,0,0,0,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (555,0,'review_done','INTEGER','YN',5,'pixalere.treatment.review_done',36,960,0,0,97,0,NULL,0,NULL);
	INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (556,0,'review_done','INTEGER','YN',6,'pixalere.treatment.review_done',36,960,0,0,0,0,NULL,0,NULL);
        
/*!40000 ALTER TABLE components ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table config_yearly_quarters
# ------------------------------------------------------------

DROP TABLE IF EXISTS config_yearly_quarters;

CREATE TABLE config_yearly_quarters (
  id int(11) NOT NULL AUTO_INCREMENT,
  quarter int(11) DEFAULT '0',
  start_date date DEFAULT NULL,
  end_date date DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table configuration
# ------------------------------------------------------------

DROP TABLE IF EXISTS configuration;

CREATE TABLE configuration (
  id int(11) NOT NULL AUTO_INCREMENT,
  category int(3) DEFAULT NULL,
  orderby int(3) DEFAULT NULL,
  selfadmin int(1) DEFAULT NULL,
  config_setting varchar(255) DEFAULT NULL,
  enable_component_rule varchar(255) DEFAULT NULL,
  enable_options_rule varchar(255) DEFAULT NULL,
  component_type varchar(255) DEFAULT NULL,
  component_options varchar(255) DEFAULT NULL,
  component_values varchar(255) DEFAULT NULL,
  current_value text,
  readonly int(11) DEFAULT NULL,
  PRIMARY KEY (id)

) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=latin1;

LOCK TABLES configuration WRITE;
/*!40000 ALTER TABLE configuration DISABLE KEYS */;

INSERT INTO configuration (id, category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
	(4,0,3,1,'useDateCalendar','','','CHKB','','','1',0),
	(6,1,1,0,'allowWoundCare','','','CHKB','','','1',0),
	(7,1,2,0,'allowStoma','','','CHKB','','','1',0),
	(8,1,3,0,'allowDrain','','','CHKB','','','1',0),
	(9,1,4,0,'allowIncision','','','CHKB','','','1',0),
	(12,0,1,0,'useUserNames','','','CHKB','','','1',0),
	(13,0,2,1,'minPasswordLength','','','DRP','4,5,6,7,8,9,10','','8',0),
	(14,0,3,1,'requireNumbersAndCharsInPassword','','','DRP','alpha-numeric,alpha,alpha-numeric-uppercase,alpha-numeric-uppercase-specialcharacter','','alpha-numeric',0),
	(21,0,2,1,'showSpellCheckButton','','','CHKB','','','0',0),
	(23,0,1,1,'useDobCalendar','useDateCalendar==1','','CHKB','','','0',0),
	(25,0,3,1,'showBradenScale','','','CHKB','','','1',0),
	(30,6,3,1,'showCurvedIncisions','','','CHKB','','','0',0),
	(31,6,4,1,'allowSaveWPwithoutAlphas','','','CHKB','','','1',0),
	(38,6,11,1,'allowMultipleEtiologies','allowWoundCare==1','','CHKB','','','0',0),
	(39,6,12,1,'allowMultipleWoundProfiles','','','CHKB','','','1',0),
	(42,7,1,1,'showAssessDoneIndicator','','','CHKB','','','1',0),
	(43,7,2,1,'setupAssessmentScreen','','','CHKB','','','0',0),
	(45,7,4,1,'defaultWoundBed100Percent','','','CHKB','','','0',0),
	(46,7,5,1,'disableMeasurentsForPartial','','','CHKB','','','1',0),
	(47,7,6,1,'enforceLengthLargerThanWidth','','','CHKB','','','1',0),
	(48,7,7,1,'woundDepthRequiredForFullAssess','','','CHKB','','','1',0),
	(49,7,8,1,'woundLength','','','INPT','10,20,30,40,50,60,70,80,90,100','','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65',0),
	(50,7,9,1,'woundWidth','','','INPT','10,20,30,40,50,60,70,80,90,100','','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65',0),
	(51,7,10,1,'woundDepth','','','INPT','10,20,30,40,50,60,70,80,90,100','','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20',0),
	(52,7,11,1,'sinusTracts','allowWoundCare==1','','INPT','1,2,3,4,5,6,7,8,9,10','','1,2,3,4,5,6,7,8,9,10',0),
	(53,7,12,1,'undermining','allowWoundCare==1','','INPT','1,2,3,4,5,6,7,8,9,10','','1,2,3,4,5,6,7,8,9,10',0),
	(54,7,13,1,'exudateAutoNils','','','CHKB','','','1',0),
	(55,7,14,1,'disableImagesForPartial','','','CHKB','','','1',0),
	(57,8,1,1,'hideClosedAlphasOnTreatment','','','CHKB','','','0',0),
	(58,8,2,1,'showProductsFilter','','','CHKB','','','1',0),
	(59,8,3,1,'showProductsSearch','','','CHKB','','','0',0),
	(60,8,4,1,'showNursingVisitFrequency','','','CHKB','','','1',0),
	(62,0,1,1,'showCorrectionsWithStrikeThrough','','','CHKB','','','1',0),
	(64,10,1,1,'autoReferralsPrevRefDays','','','DRP','7,14,21,28','','21',0),
	(65,10,2,1,'makeAutoReferralsHealrate','','','CHKB','','','1',0),
	(66,10,3,1,'autoReferralsHealDays','makeAutoReferralsHealrate==1','','DRP','7,14,21,28','','7',0),
	(67,10,4,1,'autoReferralsHealrate','makeAutoReferralsHealrate==1','','DRP','10,20,30,40,50','10,20,30,40,50','30',0),
	(68,10,5,1,'makeAutoReferralsDressing','','','CHKB','','','1',0),
	(69,10,6,1,'autoReferralsDressingsDays','makeAutoReferralsDressing==1','','DRP','7,14,21,28','','21',0),
	(70,10,7,1,'autoReferralsDressingsPerWeek','makeAutoReferralsDressing==1','','DRP','1,2,3,4,5,6,7,8,9,10,11,12,13,14','','3',0),
	(71,10,8,1,'repopulateTreatmentComments','','','CHKB','','','0',0),
	(73,0,1,1,'checkPHN','','','DRP','pixalere.none,BC',',BC','BC',0),
	(75,0,1,0,'woundEtiologyPressure','allowWoundCare==1','','INPN','','','142',1),
	(76,7,2,0,'incisionClosureStatusRemoved','allowIncision==1','','INPN','','','1216',1),
	(77,7,3,0,'stoolQuantityNil','allowStoma==1','','INPN','','','1352',1),
	(78,7,4,0,'urineQuantityNil','allowStoma==1','','INPN','','','1346',1),
	(79,7,5,0,'mucocutaneous_margin_seperated','allowStoma==1','','INPN','','','1395',1),
	(80,8,6,0,'basicProductCategory','','','INPN','','','82',1),
	(81,7,11,0,'woundActive','','','INPN','','','256',1),
	(82,7,11,0,'woundClosed','','','INPN','','','257',1),
	(83,7,13,0,'incisionActive','','','INPN','','','1706',1),
	(84,7,14,0,'incisionClosed','','','INPN','','','1707',1),
	(85,7,15,0,'drainActive','','','INPN','','','1708',1),
	(86,7,16,0,'drainClosed','','','INPN','','','1709',1),
	(87,7,17,0,'ostomyActive','','','INPN','','','1704',1),
	(88,7,18,0,'ostomyClosed','','','INPN','','','1705',1),
	(89,7,19,0,'woundExudateTypeNil','','','INPN','','','222',1),
	(90,7,20,0,'woundExudateAmountNil','','','INPN','','','225',1),
	(91,7,21,0,'incisionExudateTypeNil','','','INPN','','','1085',1),
	(92,7,22,0,'incisionExudateAmountNil','','','INPN','','','1128',1),
	(93,7,23,0,'drainExudateTypeNil','','','INPN','','','1559',1),
	(94,7,24,0,'drainExudateAmountNil','','','INPN','','','1563',1),
	(95,0,25,0,'woundGoalHealable','','','INPN','','','1081',1),
	(97,0,26,0,'allowRecommendationAcknowledgement','','','CHKB','','','1',0),
	(99,0,27,0,'showLimbComments','','','CHKB','','','0',0),
	(101,0,28,1,'purgeTMPRecordsAfterXDays','','','INPN','','','7',0),
	(103,20,1,0,'reportWoundCount','','','CHKB','','','1',0),
	(104,20,1,0,'reportOverviewActivePatients','','','CHKB','','','1',0),
	(105,20,1,0,'reportReferralsAndRecommendations','','','CHKB','','','1',0),
	(107,20,1,0,'reportHealtimeAndCostPerEtiology','','','CHKB','','','1',0),
	(109,20,1,0,'reportLastAssessments','','','CHKB','','','1',0),
	(111,20,1,0,'reportPatientFile','','','CHKB','','','1',0),
	(115,0,1,0,'appAbb',' ','','INPN','','','Pix',0),
	(116,0,2,0,'appShort','','','INPN','','','Pixalere',0),
	(117,0,3,0,'appFull','',' ','INPN','','','Pixalere Wound Care Solution',0),
	(118,7,18,0,'ostomyHold','','','INPN','','','1710',1),
	(121,20,2,1,'includeOstomyInOnLastAssessmentReport','reportLastAssessments==1','','CHKB','','','0',0),
	(123,6,9,1,'requireIncisionTag','allowIncision==1','','CHKB','','','1',0),
	(124,6,9,1,'allowIncisionBeforeTag','allowIncision==1','','CHKB','','','0',0),
	(125,6,9,1,'removeIncisionWhenTagRemoved','allowIncision==1','','CHKB','','','1',0),
	(130,0,30,0,'male','','','INPN','','','130',1),
	(131,0,30,0,'female','','','INPN','','','131',0),
	(132,0,30,0,'acute','','','INPN','','','355',1),
	(133,0,30,0,'community','','','INPN','','','356',1),
	(134,0,30,0,'residential','','','INPN','','','-1',0),
	(135,0,30,1,'wrongpassword_info','','','INPT','','','411-411-4141',0),
	(136,0,30,1,'homeInfo_left','','','TEXT','','','<table align=\"left\"><tr valign=\"top\"><td width=\"57%\" class=\"title\"><b>Pixalere  Sites in VCH/PHC</b></td><td width=\"42%\" class=\"title\"><b>Phone Number</b></td></tr><tr valign=\"top\"><td  align=\"left\"><label><b>Vancouver </b></label></td><td  align=\"left\"><label>&nbsp;</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-1 Three Bridges</label></td><td  align=\"left\"><label>604-736-9844</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-2 North</label></td><td  align=\"left\"><label>604-253-3575</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-2 Pender</label></td><td  align=\"left\"><label>604-669-9181</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-2 DCHC</label></td><td  align=\"left\"><label>604-255-3151</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-3 Evergreen</label></td><td  align=\"left\"><label>604-872-2511</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-4 Pacific Spirit</label></td><td  align=\"left\"><label>604-261-6366</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-5 Raven Song</label></td><td  align=\"left\"><label>604-709-6400</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>CHA-6 South</label></td><td  align=\"left\"><label>604-321-6151</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>George Pearson Center</label></td><td  align=\"left\"><label>604-321-3231</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>G F Strong</label></td><td  align=\"left\"><label>604-734-1313</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Three Links </label></td><td  align=\"left\"><label>604-434-7211</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Villa Carital</label></td><td  align=\"left\"><label>604-434-0995</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Royal Ascot</label></td><td  align=\"left\"><label>604-254-5559</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Royal Arch</label></td><td  align=\"left\"><label>604-437-7343</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Yaletown</label></td><td  align=\"left\"><label>604-689-0022</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Haro Park Center</label></td><td  align=\"left\"><label>604-687-5584</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Louis Brier</label></td><td  align=\"left\"><label>604-261-9376</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>&nbsp;</label></td><td  align=\"left\"><label>&nbsp;</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label><b>North Shore</b></label></td><td  align=\"left\"><label>&nbsp;</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Central CHC</label></td><td  align=\"left\"><label>604-983-6700</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>West CHC</label></td><td  align=\"left\"><label>604-904-6200</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Parkgate CHC</label></td><td  align=\"left\"><label>604-904-6450</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>&nbsp;</label></td> <td  align=\"left\"><label>&nbsp;</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label><b>Richmond</b></label></td> <td  align=\"left\"><label>&nbsp;</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Richmond Home Care</label></td> <td  align=\"left\"><label>604-278-3361</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Richmond Lions  Manor</label></td><td  align=\"left\"><label>604-274-6311 </label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Minoru ECU</label></td><td  align=\"left\"><label>604-244-5307</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>&nbsp;</label></td> <td  align=\"left\"><label>&nbsp;</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label><b>Coast Garibaldi</b></label></td> <td  align=\"left\"><label>&nbsp;</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Powell River Home   Care</label></td> <td  align=\"left\"><label>604-485-3310</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Powell River Hospital</label></td>  <td  align=\"left\"><label>604-485-3211</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Evergreen ECU</label></td>  <td  align=\"left\"><label>604-485-2208</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Olive Devaud</label></td> <td  align=\"left\"><label>604-485-9868</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Texada Island</label></td>  <td  align=\"left\"><label>604-486-7525</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>Bella Coola Hospital</label></td>  <td  align=\"left\"><label>250-799-5311</label></td></tr><tr valign=\"top\"><td  align=\"left\"><label>RW Large Memorial  Hospital   <p>(Bella Bella)</p></label></td>  <td  align=\"left\"><label>250-957-2314</label></td></tr></table>',0),
	(137,0,30,1,'homeInfo_right','','','TEXT','','','<p><label class=\"title\">Trouble with:</label><br><label>Pixalere Website?</label><br><label>Forget your Password?</label><br><label>Camera Questions?</label><br><label>Step by Step Navigation?</label><br><label class=\"mainpageheader\">Help Desk 604-875-4334</label><br><label>*Contracted Long Term Care Sites will have a different site specific Help Desk Number</label></p><p>&nbsp;</p><p><label class=\"title\">Who We Are: The Pixalere Team</label><br><b>Shannon Handfield</b>, RN, BSN, CWOCN - Pixalere Clinical and Implementation Lead <br><b>Lori Block</b>, RN, BSN, CWOCN - Pixalere Clinical Educator <br><b>David Johns</b> - Systems Analyst  </p><p>&nbsp;</p><p><label><b>Ideas for Content Revision,</b> <br>email:</label> <a href=\"mailto:shannon.handfield@vch.ca\">shannon.handfield@vch.ca</a><br><label><b>Needing Pixalere Educational Support,</b> <br>email:</label> <a href=\"mailto:lori.block@vch.ca\">lori.block@vch.ca</a></p>',0),
	(138,0,30,1,'portal_link','','','INPT','','','',0),
	(139,0,30,1,'portal_redirect','','','CHKB','','','0',0),
	(140,0,30,1,'server_ip','','','INPT','','','http://mysql.pixalerehealthcare.com',0),
	(141,0,30,1,'allowFullAssessmentDue','','','CHKB','','','1',0),
	(143,0,29,1,'showReviewDone','','','CHKB','','','0',0),
	(144,8,29,1,'showTreatmentPlan','','','CHKB','','','0',0),
	(145,7,29,1,'recurrentRequired','','','CHKB','','','1',0),
	(146,10,9,1,'hideAllButAcknowledgeRecommendation','','','CHKB','','','1',0),
	(148,3,36,1,'deletePatientsOnSync','','','CHKB','','','1',0),
	(151,10,8,1,'makeAutoReferralAntimicrobial','','','CHKB','','','1',0),
	(153,10,9,1,'referralsAntimicrobialProducts','','','INPT','','','941,942,1230,1231,1173,1174,1000,1001,1170,1171,1172',0),
	(154,10,6,1,'autoReferralsAntimicrobialDays','makeAutoReferralAntimicrobial==1','','DRP','7,14,21,28','','21',0),
	(155,3,33,1,'offlineRedirect','','','CHKB','','','0',0),
	(157,10,7,0,'autoReferralPriority','','','INPN','','','1770',1),
	(158,10,7,0,'offlineAutoReferralHack','','','CHKB','','','1',0),
	(159,0,7,1,'showLast4MonthsOfComments','','','CHKB','','','0',0),
	(160,10,7,0,'alternativeLimbComment','','','CHKB','','','0',0),
	(171,2,7,1,'hidePatientAccountModification','','','CHKB','','','0',0),
	(172,2,8,1,'enableDataExtraction','','','CHKB','','','0',0),
	(173,2,9,1,'dataPath','','','INPT','','','c:/share/data',0),
	(174,2,10,1,'UNCPath','','','INPT','','','\\dc1serv534PixalereFileDrop',0),
	(175,2,11,1,'dataHowMuch','','','DRP','1,3,5','','3',0),
	(176,0,11,0,'noTreatmentListID','','','INPT','','','3200',1),
	(177,3,30,0,'unknownGender','','','INPN','','','2132',1),
	(180,2,30,1,'integration_validation','','','DRP','phn,secondary_phn,secondary_phn or phn,phn or secondary_phn','','secondary_phn or phn',0),
	(181,7,30,1,'deleteAssessmentTimeframe','','','DRP','3,7,14,28,-1','','14',0),
	(183,20,1,0,'reportWoundManagementPlan','',' ','CHKB',' ',' ','0',0),
	(185,20,1,0,'showTransferOnSearch','',' ','CHKB',' ',' ','1',0),
	(186,0,1,0,'changePasswordFlag','',' ','CHKB',' ',' ','1',0),
	(187,0,1,0,'passwordExpiring','',' ','CHKB',' ',' ','0',0),
	(188,0,1,0,'passwordExpiringDays','',' ','DRP','90,120,180,360,720',' ','180',0),
	(189,0,7,0,'hidePatientTimezone','','','CHKB','','','0',0),
	(190,0,0,1,'hidephn2','','','CHKB','','','0',0),
	(191,0,0,1,'hidephn3','','','CHKB','','','0',0),
	(192,0,0,1,'phn1_name','','','INPT','','','PHN#',0),
	(193,0,0,1,'phn2_name','hidephn2==0','','INPT','','','MRN#',0),
	(194,0,0,1,'phn3_name','hidephn3==0','','INPT','','','PARIS#',0),
	(195,0,0,1,'timezone','','','INPT','','','America/Vancouver',0),
	(196,0,0,1,'phn2_validate','','','DRP','intOnly(event),intAlphaOnly(event)','','intOnly(event)',0),
	(197,0,0,1,'phn3_validate','','','DRP','intOnly(event),intAlphaOnly(event)','','intOnly(event)',0),
	(198,0,0,1,'alphaDraggable','','','CHKB','','','1',0),
	(199,0,0,1,'phnFormat','','','INPT','','','4-3-3',0),
	(200,0,1,1,'treatmentCatName','',' ','INPT','',' ','Treatment Area',0),
	(201,0,1,1,'treatmentLocationName','',' ','INPT','',' ','Treatment Location',0),
	(202,0,1,1,'session_timeout','',' ','INPN','',' ','30',0),
	(203,0,1,1,'showDeleted','',' ','CHKB','',' ','1',0),
	(204,0,1,1,'passwordlockout','',' ','DRP','0,2,3,5,6,8,10',' ','0',0),
	(205,0,7,1,'image_width','',' ','DRP','640,1280,1600,2000',' ','640',0),
	(206,0,0,1,'customer_name','',' ','INPT','',' ','Acme Health Authority',0),
	(207,0,20,1,'prevalence_population','',' ','INPT','',' ','300000',0),
	(208,0,20,1,'dashboard_email','',' ','INPT','',' ','travis@pixalere.com',0),
	(209,0,0,1,'email_host','',' ','INPT','',' ','smtp.gmail.com',0),
	(210,0,0,1,'email_user','',' ','INPT','',' ','sendmail@pixalere.com',0),
	(211,0,0,1,'email_password','',' ','INPT','',' ','SendM@il7',0),
	(212,0,0,0,'user_stats','',' ','INPT','',' ','travis@pixalere.com,ken@pixalere.com',1),
	(213,0,0,1,'support_email','',' ','INPT','',' ','travis@pixalere.com',0),
	(214,0,0,1,'photos_path','',' ','INPT','',' ','/opt/pixalere_storage',0),
	(215,0,0,0,'customer_id','',' ','INPT','',' ','PIX1234',1),
	(216,0,0,1,'imperial','',' ','DRP','imperial,metric',' ','metric',1),
	(217,0,1,0,'termsandconditions','',' ','TEXT','',' ','This Agreement between Pixalere Healthcare Inc. (PHI) and I, User of Pixalere. BY INSTALLING OR USING THE PIXALERE SOFTWARE KNOWN AS THE PIXALERE DIGITAL WOUND ASSESSMENT PACKAGE (THE "PRODUCT"), I, USER OF PIXALERE, AM CONSENTING TO BE BOUND BY, AND IS BECOMING, A PARTY TO THIS AGREEMENT AND ANY ADDENDUM HERETO. IF I, USER OF PIXALERE, DO NOT AGREE TO ALL OF THE TERMS OF THIS AGREEMENT, I, USER OF PIXALERE, MUST NOT INSTALL OR USE THE PRODUCT. IF THESE TERMS ARE CONSIDERED AN OFFER, ACCEPTANCE IS EXPRESSLY LIMITED TO THESE TERMS. 1. LICENSE AGREEMENT. The use of any third party software product or interface included in the Product shall be governed by the third partys license agreement and not by this Agreement, whether that license agreement is presented for acceptance the first time that the third party software is invoked, is included in a file in electronic form, or is included in the package in printed form. 2. LICENSE GRANT. Pixalere Healthcare Inc grants me, User of Pixalere, a limited, royalty-free, non- exclusive, non-sub-licensable, and non-transferable license to: (a) use the demonstration version of the Product; (b) use the fully-enabled version of the Product; and (c) when using the fully- enabled version of the Product, to receive from Pixalere Healthcare hard-copy documentation, technical support, telephone assistance, or enhancements, new releases or updates to the Product (collectively the "Support Programs. If I, User of Pixalere, download or otherwise obtain in any manner any Support Programs they shall become part of the Product and the terms of this Agreement shall apply notwithstanding the version of the Product in use by me, User of Pixalere. Pixalere Healthcare Inc may elect, at its sole discretion, to provide said products and/or services without any obligation to me, User of Pixalere. Except to the extent specified to the contrary in this Agreement or in an addendum hereto, Pixalere Healthcare shall not be obligated to support the Product or the Support Programs, whether by providing advice, training, error-correction, modifications, updates, new releases or enhancement or otherwise. 3. RESTRICTIONS. Except as otherwise expressly permitted in this Agreement, I, User of Pixalere, may not: (i) customize, modify or create any derivative works of the Product, Support Programs or documentation, including translation or localization; (ii) decompile, disassemble, reverse engineer, or otherwise attempt to derive the source code for the Product and/or any enhancements, new releases or updates to the Product (except to the extent applicable laws specifically prohibit such restriction); (iii) encumber, sell, rent, lease, sublicense, or otherwise transfer rights to the Product and/or the Support Programs; (iv) remove or alter any trademark, logo, copyright or other proprietary notices, legends, symbols or labels in the Product and/or the Support Programs; or (v) publish any results of benchmark tests run on the Product and/ or any enhancements, new releases or updates to the Product to a third party without Pixalere Healthcare prior written consent. I, User of Pixalere, may not copy, alter, modify or resell the Product and/or the Support Programs in whole or in part. I, User of Pixalere, am permitted to use the Product and the Support Programs under the license granted herein in the course of providing wound care services by its staff to contracted customers and their clients. 4. FEES. There is no license fee for the demonstration version of the Product; there is a license fee for the fully enabled version of the Product. If I, User of Pixalere, wish to receive the Product on media, there may be a charge for the media and for shipping and handling. I, User of Pixalere, am responsible for any and all applicable taxes. 21 PIXALERE SOFTWARE AND SERVICES AGREEMENT 5. PROPRIETARY RIGHTS. Title, ownership rights, and any and all intellectual property rights in the Product and any and all Support Programs shall remain in Pixalere Healthcare and/or its suppliers. I, User of Pixalere, acknowledge such ownership and intellectual property rights and will not take any action to jeopardize, limit or interfere in any manner with Pixalere Healthcare or its suppliers ownership of or rights with respect to the Product or the Support Programs. The Product and the Support Programs are protected by copyright and other intellectual property laws and by international treaties. Title and related rights in the content accessed through the Product and/or Support Programs are the property of the applicable content owner and are protected by applicable law. The license granted under this Agreement gives me, User of Pixalere, no rights to such content. I, User of Pixalere, shall ensure that the Product and the Support Programs are protected at all times from misuse, damage, destruction or any form of unauthorized use. Pixalere Healthcare may use data collected via Pixalere for non-commercial, educational, or research purposes. 6. DISCLAIMERS. PIXALERE HEALTHCARE DOES NOT VERIFY THE CONTENT OF THE MATERIAL PROVIDED BY ME, USER OF PIXALERE, OR ANY THIRD PARTY THAT IS ACCESSED BY YOU THROUGH THE USE OF THE PRODUCT AND/OR ANY MATERIAL LINKED THROUGH SUCH CONTENT (COLLECTIVELY, THE "CONTENT"). THE PRODUCT AND CONTENT ARE PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTY OF ANY KIND, INCLUDING WITHOUT LIMITATION ANY EXPRESS OR IMPLIED WARRANTIES OF ANY KIND INCLUDING BUT NOT LIMITED TO WARRANTIES THAT THE PRODUCT AND/OR CONTENT ARE FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PRODUCT AND/OR CONTENT IS BORNE BY ME, USER OF PIXALERE. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS AGREEMENT. NO USE OF THE PRODUCT IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER. ALTHOUGH ALL CONTENT IS BELIEVED TO BE RELIABLE, PIXALERE HEALTHCARE DOES NOT GUARANTEE THE RELIABILITY, ACCURACY, COMPLETENESS, SAFETY, TIMELINESS, LEGALITY, USEFULNESS, ADEQUACY, OR SUITABILITY OF ANY CONTENT NOR IS IT RESPONSIBLE FOR ANY LIABILITY ARISING OUT OF THE CONTENT. THE CONTENT MAY BE HARMFUL, UNTIMELY, INCOMPLETE, OR INACCURATE AND, ACCORDINGLY, YOU AGREE TO EXERCISE CAUTION, DISCRETION AND COMMON SENSE WHEN USING THE CONTENT. THE CONTENT IS NOT INTENDED TO SUBSTITUTE FOR INFORMED PROFESSIONAL ADVICE. I, USER OF PIXALERE, SHOULD NOT USE THE PRODUCT AND/OR CONTENT FOR EMERGENCY PURPOSES OR TO SEEK OR PROVIDE DIAGNOSIS OR TREATMENT OF MEDICAL, PSYCHIATRIC OR PSYCHOLOGICAL PROBLEMS, OR AS A SUBSTITUTE FOR FACE-TO-FACE PROFESSIONAL MEDICAL, PSYCHIATRIC, PSYCHOLOGICAL, OR OTHER PROFESSIONAL CONSULTATION. IF YOU HAVE A MEDICAL OR PSYCHIATRIC EMERGENCY, CALL YOUR PHYSICIAN OR 911 IMMEDIATELY. YOU MUST USE YOUR JUDGMENT TO DETERMINE WHEN IT IS NECESSARY TO CONSULT WITH A PROVIDER WHO IS LOCAL, LICENSED IN YOUR PROVINCE, STATE OR COUNTRY, AVAILABLE IN PERSON, OR OTHERWISE POSSESSES THE QUALITIES REQUIRED TO PROPERLY DIAGNOSE OR ADVISE YOU, ESPECIALLY IN AREAS OF EXPERTISE, SUCH AS MEDICINE, REQUIRING LICENSING OR GOVERNMENT CERTIFICATION, OR FOR HIGH RISK ACTIVITIES. PIXALERE HEALTHCARE DOES NOT DIRECTLY OR INDIRECTLY PRACTICE MEDICINE OR DISPENSE MEDICAL SERVICES. 7. LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE 22 PIXALERE SOFTWARE AND SERVICES AGREEMENT LAW, IN NO EVENT WILL PIXALERE HEALTHCARE, ITS PHIS, SUPPLIERS OR RESELLERS BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR PUNITIVE DAMAGES ARISING OUT OF THE USE OF OR INABILITY TO USE THE PRODUCT OR CONTENT, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, ECONOMIC LOSS, LOSS OF PROFITS, THIRD PARTY CLAIMS, OR ANY AND ALL OTHER DAMAGES OR LOSSES, EVEN IF ADVISED OF THE POSSIBILITY THEREOF, AND REGARDLESS OF THE LEGAL OR EQUITABLE THEORY (CONTRACT, TORT OR OTHERWISE) UPON WHICH THE CLAIM IS BASED. IN ANY CASE, PIXALERE HEALTHCARE???S ENTIRE LIABILITY UNDER ANY PROVISION OF THIS AGREEMENT SHALL NOT EXCEED $100, WITH THE EXCEPTION OF DEATH OR PERSONAL INJURY CAUSED BY THE NEGLIGENCE OF PIXALERE HEALTHCARE TO THE EXTENT APPLICABLE LAW PROHIBITS THE LIMITATION OF DAMAGES IN SUCH CASES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THIS EXCLUSION AND LIMITATION MAY NOT BE APPLICABLE. ME, USER OF PIXALERE, SHALL BE SOLELY RESPONSIBLE, AT ITS OWN EXPENSE, FOR ACQUIRING, INSTALLING, MAINTAINING AND UPDATING ALL CONNECTIVITY EQUIPMENT, HARDWARE, SOFTWARE AND OTHER EQUIPMENT AS MAY BE NECESSARY FOR IT TO USE THE PRODUCT UNLESS PROVISIONS HAVE BEEN MADE IN AN ADDENDUM HERETO OR IN A SEPARATE AGREEMENT. 8. RELEASE AND WAIVER. I USE PIXALERE HEALTHCARE???S PRODUCTS AND/ OR SERVICES AT MY OWN RISK. To the maximum extent permitted by applicable law, You hereby release, and waive all claims against, Pixalere Healthcare and its employees and agents from any and all liability for claims, damages (actual and consequential), costs and expenses (including litigation costs and attorneys fees) of every nature and kind, arising out of or in any way connected with use of the Product and/or Support Programs. If You are a California corporation or resident, You waive Your rights under California Civil Code Section 1542, which states, "A general release does not extend to claims which the creditor does not know or suspect to exist in his favour at the time of executing the release, which if known by him must have materially affected his settlement with the debtor." Residents of other states and nations similarly waive their rights under applicable and/or analogous laws, statues, or regulations. 9. MISCELLANEOUS. (a) This Agreement and any addendum or amendments hereto constitutes the entire agreement between the parties concerning the subject matter hereof. (b) This Agreement and any addendum or amendments hereto may be amended only by a writing signed by both parties. (c) Except to the extent applicable law, if any, provides otherwise, this Agreement and any addendum or amendments hereto shall be governed by the laws of the Province of British Columbia and the federal laws of Canada applicable therein, excluding conflict of law provisions. (d.) This Agreement and any addendum or amendments hereto shall not be governed by the United Nations Convention on Contracts for the International Sale of Goods. (e) If any provision in this Agreement or any addendum or amendments hereto should be held illegal or unenforceable by a court having jurisdiction, such provision shall be modified to the extent necessary to render it enforceable without losing its intent, or severed from this Agreement if no such modification is possible, and other provisions of this Agreement and any addendum or amendments hereto shall remain in full force and effect. (f) The controlling language of this Agreement and any addendum or amendments hereto is English. If I, User of Pixalere, have received a translation into another language, it has been provided for my convenience only. (g) A waiver by either party of any term or condition of this Agreement and any addendum or amendments hereto or any breach thereof, in any one instance, shall not waive such term or condition or any subsequent breach thereof. (h) The provisions of this Agreement and any addendum or amendments hereto which require or contemplate performance after the expiration or termination of this Agreement shall be enforceable notwithstanding said expiration or termination. (i) I, User of Pixalere, may not assign or otherwise transfer by operation of law or otherwise 23 PIXALERE SOFTWARE AND SERVICES AGREEMENT this Agreement or any addendum or amendments hereto or any rights or obligations herein except in the case of a merger or the sale of all or substantially all of my  assets to another entity. PHI may assign this Agreement and any addendum or amendments hereto to a third party at any time. (j) This Agreement shall be binding upon and shall inure to the benefit of the parties, their successors and permitted assigns. (k) Neither party shall be in default or be liable for any delay, failure in performance (excepting the obligation to pay) or interruption of service resulting directly or indirectly from any cause beyond its reasonable control. (l) The relationship between Pixalere Healthcare and I, User of Pixalere, is that of independent contractors and neither I, User of Pixalere, nor my agents shall have any authority to bind Pixalere Healthcare in any way. (m) If any dispute arises under this Agreement, the prevailing party shall be reimbursed by the other party for any and all legal fees and costs associated therewith. (n) If any Pixalere Healthcare professional services are being provided, then such professional services are provided pursuant to the terms of a separate Professional Services Agreement between Pixalere Healthcare and I, User of Pixalere. The parties acknowledge that such services are acquired independently of the Product licensed hereunder, and that provision of such services is not essential to the functionality of the Product or Support Programs. (o) The headings to the sections of this Agreement and any addendum or amendments hereto are used for convenience only and shall have no substantive meaning. (p) Where any conflict occurs between the provisions contained in two or more of the documents forming this Agreement, the documents comprising this Agreement shall be read in the following order: (i) the provisions of this document; and (ii) the provisions of any addendum or amendment. 10. BEING OUTSIDE OF CANADA AND THE U.S. If I, User of Pixalere am located outside of Canada or the U.S., then I, User of Pixalere, am responsible for complying with any local laws in its jurisdiction which might impact its right to import, export or use the Product, and I, User of Pixalere, represent that it has complied with any regulations or registration procedures required by applicable law to make this license enforceable. 11. Notwithstanding provision 13 in the Service Agreement, the parties expressly agree that PHI reserves the right to collect, report, share and publish anonymized data contained in the Pixalere Software and any related database.',0),
	(218,0,1,0,'allowBurn','',' ','CHKB','',' ','0',0),
	(219,0,1,0,'img_server_url','',' ','INPT','',' ','http://mysql.pixalerehealthcare.com/IpadImageUpload.do',0),
	(220,0,1,0,'img_server_url_redirect','',' ','INPT','',' ','http://mysql.pixalerehealthcare.com/vm/viewer/ipad_success.vm',0),
	(221,0,0,0,'img_server_license_key','',' ','INPT','',' ','79FF1-0005B-55A90-00008-968FB-AB3E6D',1),
	(222,99,1,0,'patient_position_id','',' ','INPT','',' ','64',NULL);

/*!40000 ALTER TABLE configuration ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dashboard_reports
# ------------------------------------------------------------

DROP TABLE IF EXISTS dashboard_reports;

CREATE TABLE dashboard_reports (
  id int(11) NOT NULL AUTO_INCREMENT,
  email varchar(255) DEFAULT '0',
  wound_type varchar(25) DEFAULT NULL,
  etiology_all int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table dashboard_reports_etiology
# ------------------------------------------------------------

DROP TABLE IF EXISTS dashboard_reports_etiology;

CREATE TABLE dashboard_reports_etiology (
  id int(11) NOT NULL AUTO_INCREMENT,
  dashboard_report_id int(11) DEFAULT '0',
  etiology_id int(11) NOT NULL,
  PRIMARY KEY (id),
  KEY dashboard_report_id (dashboard_report_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table dashboard_reports_location
# ------------------------------------------------------------

DROP TABLE IF EXISTS dashboard_reports_location;

CREATE TABLE dashboard_reports_location (
  id int(11) NOT NULL AUTO_INCREMENT,
  dashboard_report_id int(11) DEFAULT '0',
  treatment_location_id int(11) NOT NULL,
  PRIMARY KEY (id),
  KEY dashboard_report_id (dashboard_report_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table dbversion
# ------------------------------------------------------------

DROP TABLE IF EXISTS dbversion;

CREATE TABLE dbversion (
  id int(11) DEFAULT NULL,
  version varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES dbversion WRITE;
/*!40000 ALTER TABLE dbversion DISABLE KEYS */;

INSERT INTO dbversion (id, version)
VALUES
	(1,'6.0');

/*!40000 ALTER TABLE dbversion ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dressing_change_frequency
# ------------------------------------------------------------

DROP TABLE IF EXISTS dressing_change_frequency;

CREATE TABLE dressing_change_frequency (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  active int(11) DEFAULT NULL,
  professional_id int(11) DEFAULT NULL,
  user_signature varchar(255) DEFAULT NULL,
  dcf int(11) DEFAULT NULL,
  time_stamp varchar(255) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  deleted_reason varchar(255) DEFAULT NULL,
  delete_signature varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY dcf (dcf),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  KEY alpha_id (alpha_id),
  KEY assessment_id (assessment_id)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS event;

CREATE TABLE event (
  id int(11) NOT NULL AUTO_INCREMENT,
  messageId varchar(255) DEFAULT '',
  sendingFacility varchar(255) DEFAULT '',
  messageDateTime varchar(255) DEFAULT '',
  merge int(11) DEFAULT '0',
  patient_from int(11) DEFAULT '0',
  patient_to int(11) DEFAULT '0',
  table_name varchar(255) DEFAULT '',
  row_id int(11) DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table foot_assessment
# ------------------------------------------------------------

DROP TABLE IF EXISTS foot_assessment;

CREATE TABLE foot_assessment (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(1) DEFAULT '0',
  patient_id int(11) DEFAULT '0',
  foot_show int(1) DEFAULT '0',
  left_foot_deformities text,
  left_foot_skin text,
  left_foot_toes text,
  right_foot_deformities text,
  right_foot_skin text,
  right_foot_toes text,
  weight_bearing int(11) DEFAULT '0',
  balance int(11) DEFAULT '0',
  calf_muscle_pump int(11) DEFAULT '0',
  mobility_aids int(11) DEFAULT '0',
  calf_muscle_comments text,
  mobility_aids_comments text,
  gait_pattern_comments text,
  walking_distance_comments text,
  walking_endurance_comments text,
  indoor_footwear int(11) DEFAULT '0',
  outdoor_footwear int(11) DEFAULT '0',
  indoor_footwear_comments text,
  outdoor_footwear_comments text,
  orthotics int(11) DEFAULT '0',
  orthotics_comments text,
  muscle_tone_functional int(11) DEFAULT '0',
  arches_foot_structure int(11) DEFAULT '0',
  supination_foot_structure int(11) DEFAULT '0',
  pronation_foot_structure int(11) DEFAULT '0',
  dorsiflexion_active text,
  dorsiflexion_passive text,
  plantarflexion_active text,
  plantarflexion_passive text,
  greattoe_active text,
  greattoe_passive text,
  professional_id int(11) DEFAULT '0',
  last_update varchar(255) DEFAULT '',
  left_proprioception int(11) DEFAULT '0',
  right_proprioception int(11) DEFAULT '0',
  user_signature text,
  foot_comments text,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY patient_id (patient_id),
  KEY professional_id (professional_id),
  CONSTRAINT footassess_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT footassess_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT foot_assessment_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id) ON DELETE CASCADE,
  CONSTRAINT foot_assessment_ibfk_2 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table information_popup
# ------------------------------------------------------------

DROP TABLE IF EXISTS information_popup;

CREATE TABLE information_popup (
  id int(11) NOT NULL AUTO_INCREMENT,
  title text,
  description text,
  component_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

LOCK TABLES information_popup WRITE;
/*!40000 ALTER TABLE information_popup DISABLE KEYS */;

INSERT INTO information_popup (id, title, description, component_id)
VALUES
	(1,'','<b>Wound Edge</b>: wound margin immediately adjacent to the wound opening <br><br><b>Callused</b>: hyperkeratosis, thickened layer of epidermis <br><b>Diffuse</b>: not well defined, indistinct, difficult to clearly define wound outline <br><b>Demarcated</b>: well defined, distinct, easy to clearly define wound outline <br><b>Epithelialization</b>: new, pink to purple, shiny tissue <br><b>Rolled</b>: rolling under of epithelial wound edge in a cavity wound <br><b>Scarred</b>: fibrotic regenerated tissue following wound healing',18),
	(5,'','<b>Periwound Skin</b>: surrounding area immediately adjacent to the wound margin <br><br><b>Blister</b>: an elevation or separation of the epidermis containing fluid <br><b>Boggy</b>: soft, spongy <br><b>Bruised</b>: dark red purplish blue tissue that fades to yellow green grey depending on the skin colour <br><b>Denuded</b>: superficial skin loss <br><b>Dry</b>: flaky skin <br><b>Edema</b>: interstitial collect of fluid <br><b>Erythema</b>: redness of the skin-may be intense bright red to dark red or purple <br><b>Excoriated</b>: superficial loss of tissue <br><b>Indurated</b>: abnormal firmness of the tissues with palpable margins <br><b>Intact</b>: unbroken skin <br><b>Macerated</b>: wet, white <br><b>Rash</b>: temporary eruption on the skin-often raised, red, sometimes itchy <br><b>Tape tear</b>: tiny superficial skin loss due to tape <br><b>Weepy</b>: moist, draining areas <br><b>Warmth</b>: increased warmth in tissue than skin in adjacent area',19),
	(6,'','<b>Odour</b>: Unpleasant smell noted after cleansing',17),
	(7,'','<b>Exudate</b>: Wound drainage  <br><br><b>Amount</b>: consider amount in relationship to the wound size\r\n',16),
	(18,'','Intermittent Claudication: Characterized by pain, cramping, burning and aching, caused by insufficient blood flow to the limbs with ambulation.',229),
	(24,'','<b>Wound Bed</b>: the base of the wound, comprised of any combination of the following options<br><br><b>Adipose</b>: layer of yellow globular tissue where fat is stored<br><b>Blister</b>: an elevation or separation of the epidermis tissue containing fluid<br><b>Bone</b>: Hard, rigid white connective tissue<br><b>Eschar</b>: dry, black/brown, dead tissue<br><b>Fascia</b>: tough silvery white tissue found covering muscle or within a muscle group<br><b>Friable</b>: fragile tissue that may bleed easily<br><b>Fully Epithelialized</b>: covered completed with new epithelial tissue<br><b>Foreign body</b>: objects such as mesh, hardware, suture<br><b>Granulation tissue</b>: firm, red, moist, pebbled healthy tissue<br><b>Hematoma</b>: localized collection of blood<br><b>Hypergranulation</b>: (proud flesh) red, moist tissue raised above the level of the skin<br><b>Malignant/Fungating</b>: cancerous tissue<br><b>Muscle</b>: red, firm, striated tissue<br><b>New Tissue Damage</b>: new damage due to pressure or trauma on an open wound bed; presents as dark purple, deep red or grey coloured tissue<br><b>Non-granulation tissue</b>: moist, red (pale to bright) non-pebbled tissue<br><b>No open wound:</b> tissue damage noted but the skin is still intact<br><b>Not visible:</b> a portion or all of the open wound bed that cannot be visualized<br><b>Scab</b>: superficial, dry crust<br><b>Slough</b>: dry or wet, loose or firmly attached, yellow to brown dead tissue<br><b>Superficial, red</b>: clean, open area with non-measurable depth<br><b>Tendon</b>: shiny white cord of fibrous connective tissue that connects muscle to bone<br><b>Underlying Tissue Structures:</b> joints, organs, etc.<br><b>Weepy skin</b>: drainage but no obvious open areas noted',14),
	(27,'','<b>Pain</b>: Quantified on the Visual Analogue Scale where 0= no pain to 10= excruciating pain as described by the patient/client/resident',7),
	(28,'','<b>Size</b>: measurements of wound<br><b>Length</b>: edge to edge, the longest measurement of the wound\r\n',8),
	(29,'','<b>Size</b>: measurements of wound<br><b> Width</b>: from edge to edge, the widest measurement of the wound at right angles to the length',9),
	(30,'','<b>Depth</b>: the deepest vertical measurement from the base of the wound to the level of the skin.',10),
	(31,'','<b>Undermining</b>: a separation of tissue that occurs underneath the intact skin of the wound perimeter ',11),
	(32,'','<b>Sinus or Tunnel</b>: a channel that extends from any part of the wound and tracks into deeper tissue.',13),
	(33,'','<b>Fistula</b>: an abnormal track connecting an organ to the skin surface, wound bed or to another organ.',381),
	(34,'','<b>Date of Onset:</b> Best known date of when this wound started.',5),
	(35,'','<b>Active:</b> Wound is open<br><b>Closed</b> Wound is closed',2),
	(36,'','<b>Recurrent:</b>wound has occured before in this location',6),
	(37,'','<b>Pain Comments</b>: Type in any additional pain comments, if needed',380),
	(38,'','<b>Type</b>: composition of exudate<br><b>Nil</b>: No drainage<br><b>Serous</b>: thin, clear<br><b>Sanguineous</b>: bloody<br><b> Green</b>: drainge with a green colour<br><b>Purulent</b>: thick, cloudy<br><b>Other</b>\r\n',15),
	(39,'','<b>Amount</b>: consider amount in relationship to the wound size<br><b>Nil</b><br><b>Small/scant</b><br><b>Moderate</b><br><b>Large/copious</b>\r\n',16),
	(40,'','<b>Odour</b>: Unpleasant smell noted after cleansing',17),
	(41,'','<b>Edge</b>: wound margin immediately adjacent to the wound opening<br><b>Callused</b>: hyperkeratosis, thickened layer of epidermis<br><b>Demarcated</b>: well defined, distinct, easy to clearly define wound outline<br><b>Diffuse</b>: not well defined, indistinct, difficult to clearly define wound outline<br><b>Epithelialized</b>: new, pink to purple, shiny tissue<br><b>Hypergranulation</b> :(proud flesh) red, moist tissue raised above the level of the skin<br><b>Rolled</b>: epithelial wound edge of a cavity wound which rolls under<br><b>Scarred</b>: fibrotic regenerated tissue following wound healing\r\n',18),
	(42,'','<b>Periwound skin</b>: surrounding area immediately adjacent to the wound margin<br><br><b>Blister</b>: an elevation or separation of the epidermis containing fluid<br><b>Boggy</b>: soft, spongy<br><b>Bruised</b>: dark red purplish blue tissue that fades to yellow green grey depending on the skin colour<br><b>Callused</b>: hyperkeratosis, thickened layer of epidermis<br><b>Dry</b>: flaky skin<br><b>Edema</b>: interstitial collect of fluid<br><b>Elevated Warmth</b>: increased warmth when compaired to skin in adjacent area<br><b>Erythema</b>: redness of the skin-may be intense bright red to dark red or purple<br><b>Excoriated/denuded</b>: superficial loss of tissue<br><b>Fragile</b>: skin that is at risk for breakdown<br><b>Indurated</b>: abnormal firmness of the tissues with palpable margins<br><b>Intact</b>: unbroken skin<br><b>Macerated</b>: wet, white<br><b>Rash</b>: temporary eruption on the skin-often raised, red, sometimes itchy<br><b>Tape tear</b>: tiny superficial skin loss due to tape<br><b>Weepy</b>: moist, draining areas\r\n',19),
	(43,'','<b>Dressing Change Frequency</b>: How often the dressing is changed',20),
	(44,'','<b>Nursing Visit Frequency</b>: How often the nurse visits to change the dressing',21),
	(45,'','<b>Missing Limb or Toes</b>: Check all that apply',227),
	(46,'','<b>Pain at Rest</b>: Pain experienced by patient while legs are at rest<br><b>Pain at Night</b>:Pain experienced in legs by patient at night<br><b>Decrease with Elevation</b>: Pain that decreases when legs are elevated<br><b>Increase with Elevation</b>: Pain that increases when legs are elevated<br><b>Ache</b>: Patient decrides pain in legs as an \"ache\"<br><b>Knife-Like</b>: Patient describes pain in legs as \"knife-like\"<br><b>Continuous</b>: Patient descrides pain in legs as continuous<br><b>Intermittent</b>: Patient describes pain in legs as intermittent<br><b>Intermittent Claudication</b>: Characterized by pain, cramping, burning and aching, caused by insufficient blood flow to the limbs with ambulation<br><b>With Deep Palpation</b>:Patient describes pain felt with deep palpation to legs<br><b>Non-verbal Response</b>:Patient unable to give verbal response to evaluator, please follow with a pain comment below<br><b>Pain Comment</b>: Free text box to document further pain comments',229),
	(47,'','<b>Normal/healthy</b>:Skin to legs has a normal and healthy appearance<br><b>Dry/Flakey</b>: Skin\'s appearance is dry and flakey to touch on the patients legs<br><b>Varicosities</b>: Dilated and distended veins that become progressively larger and painful<br><b>Hemosiderin Staining</b>: Venous hypertension of the lower leg causing leaking of red blood cells in surrounding tissue which over time presents as grayish, brown hyperpigmentation of the skin<br><b>Woody/Fibrous (Lipodermatosclerosis)</b>: Woody, fibrous hardening of the soft tissue that will often present as a \"champagne\" shaped lower leg<br><b>Stasis Dematitis</b>: Increased permeability of dermal capillaries from venous in\r\nsufficiency that cause an inflammatory reaction (eczema and edema) of the lower legs<br><b>Atrophie Blanche</b>: White, atrophic lesions/plaques associated with venous disease<br><b>Cellulitis</b>: Infection of the skin often characterized with local erythema, pain, swelling and elevated warmth<br><b>Hairless/Fragile/Shiny</b>: Leg has a fragile and shiny appearance with the abscence of hair<br><b>Dependent Rubor</b>: The lower limb turns red/blue as blood rushes into ischemic tissue. This occurs because the peripheral vessels are so severely damaged they are no longer able to constrict, but remain dilated<br><b>Blanching with Elevation</b>: Skin\'s pallor becomes pale or lighter in pigmentation when leg is elevated<br><b>Mottled/Irregular/Discolouration</b>:Skin on patients legs have an irregular surface (mottled) with areas of discolouration<br><b>Moist/Waxy</b>: Skin to legs are moist to the touch and have a waxy texture   \r\n',233),
	(48,'Braden Scale Risk Score Interventions','<br><b>Little to no risk</b>: 18 or greater<br><b>Low Risk:</b>15-18<br><b>Moderate Risk:</b>13-14\r\n<br><b>High Risk:</b>10-12<br><b>Very High Risk:</b>9 or less<br><br><b>MILD RISK (15-18) *</b><br>Frequent turning (e.g. q 2 hours)<br>Maximal remobilization<br>Pressure-reduction support surface if bed or chair-bound<br>Protect Heels ? Offload with Pillows<br>Manage Moisture<br>Manage Nutrition<br> Manage Friction and Shear<br>\r\n* If other major risk factors are present (advanced age, poor dietary intake of protein, diastolic pressure below 60, hemodynamic instability) advance to next level of risk<br><br><b>MODERATE RISK (13-14) *</b><br>Frequent turning with a planned schedule<br>Use foam wedges for 30 degree lateral positioning<br>Pressure-reduction support surface<br>Maximal remobilization<br>Protect Heels ? Offload with Pillows<br>Manage Moisture<br>Manage Nutrition<br> Manage Friction and Shear<br>\r\n* If other major risk factors are present, advanced to next level of risk<br><br><b>HIGH RISK (10-12)</b><br>Frequent turning with a planned schedule<br>Supplement with small shifts in position<br>Pressure-reduction support surface<br>Use foam wedges for 30 degree lateral positioning<br>Maximal remobilization<br>Protect Heels ? Offload with Pillows<br>Manage Moisture<br>Manage Nutrition<br> Manage Friction and Shear<br><br><b>VERY HIGH RISK (9 or below)</b><br>All of the above +<br>Use pressure-relieving surface if patient has intractable pain, or severe pain exacerbated by turning, or<br>Additional risk factors<br><b>*NO PRESSURE REDUCTION/RELIEF SURFACE IS A SUBSTITUTE FOR TURNING SCHEDULE</b><br>TURNING SCHEDULE REQUIRED REGARDLESS OF PRESSURE RELIEF SURFACE<br><br><b>MANAGE MOISTURE</b><br>Use commercial moisture barrier (Secura/Proshile)<br>Use absorbent pads or diapers that wick and hold moisture<br>Address cause if possible<br><br><b>MANAGE NUTRITION</b><br>Increase protein intake<br>Increase calorie intake to spare proteins<br>Supplement with Multi-vitamin<br>Offer liquid diet supplement<br>Offer glass of H20 with turning schedule<br>Consult Dietitian<br><br><b>MANAGE FRICTION AND SHEAR</b><br>Elevate head of bed no more than 30 degrees<br> Use Trapeze when indicated<br>Use lift sheet to move patient<br>Protect elbows and heels if being exposed to friction<br><br><b>OTHER GENERAL CARE ISSUES</b><br>No massage of reddened bony prominences<br> Maintain good hydration<br>Avoid drying the skin<br>Frequent turning and a planned schedule<br><br><b>CORRECT OR MANAGE ALL CO-MORBIDITIES SIMULTANEOUSLY WITH IMPLEMENTING INTERVENTIONS</b>',224),
	(49,'','<b>Lower Limb Assessment- basic (pain; skin condition; temperature; colour; edema location and severity; pulses by palpation)</b><br> To be done within two weeks of a wound being noted on a lower limb and every six months or with any significant change in the limb<br><b>Lower Limb Assessments- Ankle Brachial Pressure Index (ABPI\'s)</b><br> When compression is being considered, if pulses by palpations are diminished or absent, and every six months if using compression therapy',392),
	(50,'','<b>Approximated</b>: margin where the skin and stoma meet is well connected<br><b>Dissolvable Sutures</b>: a stitch that is made of a material that will dissolve with the body?s fluids and disappear<br><b>Fistula</b>: an abnormal track connecting an organ to the skin surface, wound bed, ostomy, or to another organ<br><b>Removable Sutures</b>: a stitch that is made of a material that will need to be removed at some point in time<br><b>Separated</b>: area of detachment(s) from the stoma to the skin<br><b>Suture Granuloma</b>: red, friable tissue and skin in the stoma margin where there are areas of retained or reactive suture material<br><b>Tenuous</b>: Thin or fragile connection between the skin and stoma<br><b>Undermining</b>: a separation of tissue that occurs underneath the intact skin of the stoma perimeter\r\n',456),
	(51,'','<b>Active:</b> Ostomy is active and functioning<br><b>Closed:</b> The ONLY time you close an ostomy profile is if the ostomy was surgically reversed (e.g. re-anastomosis). Do NOT close ostomy profiles as the patient will continue to be followed by the ostomy nurse, even if they are ?self care? \r\n',32),
	(52,'','<b>Anastomosis:</b> joining of two tubular organs, such as, ureter to ileal conduit<br><b>Resiting:</b> changing location of the stoma<br><b>Revision</b>: altering stoma within same location\r\n',33),
	(53,'','<b>(Double) Barrel</b>: an opening of the bowel resulting from a resection. The proximal and distal end are brought through the abdominal wall, everted (opened and turned inside out) and attached, creating two end stomas. The proximal will be functioning and the distal is the non-functioning stoma- also known as a mucous fistula.<br><b>End:</b> an opening of the bowel, in which the proximal end is brought through the abdominal wall, is everted and attached. The distal portion of bowel is either removed or is closed and remains inside the abdominal cavity.<br><b>Loop:</b> a loop of the bowel is brought through the abdominal wall, often supported with a rod or bridge on the skin. The anterior wall is opened; the proximal end will act as a fecal diversion, while the distal end will secrete mucous. They will remain connected via the posterior wall of the bowel.<br><b>Unmatured:</b> loop of bowel is brought through the abdominal wall, but is not surgically sutured to myocutaneous skin and relies on serousal inflammation to evert stoma\r\n',410),
	(54,'','<b>Diameter:</b> distance across a round stoma in millimeters',448),
	(55,'','<b>Length:</b> the longest measurement of the stoma',454),
	(56,'','<b>Width:</b> the longest measurement of the stoma at a right angle to the length',455),
	(57,'','<b>Irregular:</b> border of the stoma has irregular shaped edges<br><b>Oblong:</b> width is different from one end of the stoma to the other<br><b>Oval:</b> length is longer than width<br><b>Round:</b> length and width are the same\r\n',56),
	(58,'','<b>Flat:</b> stoma sits at the same level of the skin<br><b>Os flush:</b> opening of the bowel at skin level<br><b> Os off-center</b>: opening is not located in the center of the protruding bowel<br><b>Os tilted:</b> opening of the bowel is tilted<br><b>Prolapsed:</b> the telescoping of the bowel through the stoma<br><b>Raised:</b> stoma is sitting above level of the skin<br><b>Retracted:</b>disappearance of the normal stoma opening below skin level<br><b>Stenosis:</b> a narrowing of the stoma at level of the skin or fascia that impairs drainage of effluent<br> \r\n',57),
	(59,'','<b>Black (Necrosis):</b> ischemia of the stoma from inadequate blood supply<br><b> Dark Red</b>: Stoma has a deep/dark red hue<br><b>Dusky:</b> purple to a deep wine coloured hue from altered blood supply<br><b>Edematous:</b> interstitial collection of fluid<br><b>Friable:</b> fragile tissue that bleeds easily<br><b>Moist:</b> mucosal tissue is damp<br><b>Pale</b><br><b>Pink</b><br><b>Red</b><br><b>Slough</b>: dry or wet, loose or firmly attached, yellow to brown dead tissue<br><b>Turgor:</b> ability to change shape and return to normal appearance after (elasticity)<br><b>Trauma:</b> mucosal injury\r\n',59),
	(60,'','<b>Approximated:</b> margin where the skin and stoma meet is well connected<br><b>Dissolvable Sutures:</b> a stitch that is made of a material that will dissolve with the body?s fluids and disappear.<br><b>Fistula:</b> an abnormal track connecting an organ to the skin surface, wound bed, ostomy, or to another organ<br><b>Removable Sutures:</b> a stitch that is made of a material that will need to be removed at some point in time<br><b>Separated:</b> area of detachment(s) from the stoma to the skin<br><b>Suture Granuloma:</b> red, friable tissue and skin in the stoma margin where there are areas of retained or reactive suture material<br><b>Tenuous:</b> Thin or fragile connection between the skin and stoma<br><b>Undermining:</b> a separation of tissue that occurs underneath the intact skin of the stoma perimeter\r\n',457),
	(61,'','<b>Distended:</b> abnormal to patient, protruding of abdomen<br><b>Flat:</b> abdominal plane is flat<br><b>Hernia:</b> deficit in the fascia that allows loops of the intestine to protrude in areas of weakness. Can present as abnormal bumps on the abdomen<br><b>Loose/Wrinkly:</b> abdomen has folds of loose skin<br><b>Pendulous</b>: abdominal tissue that hangs loose<br><b>Rounded:</b> normal to patient, abdominal plane is rounded<br><b>Ribs</b><br><b>Soft:</b> abdomen is soft with palpation<br><b>Hard:</b> abdomen is firm or hard with palpation\r\n',60),
	(62,'','<b>Mucocutaneous Margin</b>: point where the epidermis and mucosa merge<br><br><b>Approximated:</b> margin where the skin and stoma meet is well connected<br><b>Dissolvable Sutures:</b> a stitch that is made of a material that will dissolve with the body\'s fluids and disappear.<br><b>Fistula:</b> an abnormal track connecting an organ to the skin surface, wound bed, ostomy, or to another organ<br><b>Fully Epithelialized:</b> covered completely with new epithelial tissue<br><b>Removable Sutures:</b> a stitch that is made of a material that will need to be removed at some point in time<br><b>Separated:</b> area of detachment(s) from the stoma to the skin<br><b>Suture Granuloma:</b> red, friable tissue and skin in the stoma margin where there are areas of retained or reactive suture material<br><b>Tenuous:</b> Thin or fragile connection between the skin and stoma',414),
	(63,'','<b>Allergic Reaction</b>: hypersensitivity to area where product was applied resulting in an inflammatory reaction<br><b>Boggy:</b> soft, spongy<br><b>Bruised:</b> dark red, purplish, blue tissue that fades to yellow, green, grey depending on the skin colour<br><b>Caput Medusae: (Peristomal Varices):</b> a purple hue caused by dilation of blood vessels noted around the stoma. Intermittent, spontaneous, or profuse bleeding may be noted by the patient and is often caused by portal hypertension.<br><b>Contact Dermatitis:</b> Hypersensitivity to area where product, stool or urine contacted skin resulting in an inflammatory reaction<br><b>Intact:</b> Skin around stoma is intact<br><b>Denuded:</b> superficial smooth loss of epithelium<br><b>Erythema:</b> redness of the skin-may be intense, bright red to dark red, or purple<br><b>Excoriated:</b> superficial loss of tissue that presents irregular with areas of erythema, rash, and denudation<br><b>Eczema:</b> superficial inflammation of the skin often causing red papules that itch and weep that can leave crusting and scaling<br><b>Folliculitis:</b> seen as red pustules and papules that are from bacterial inflammation of hair follicles<br><b>Fungal Rash:</b> overgrowth of fungal organisms that present as pustules on the skin. Satellite lesions (small red pustules) are often seen advancing from the edge of affected area.<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br><b>Intact:</b> unbroken skin<br><b>Macerated:</b> wet, white<br><b>Mucosal Transplant:</b> seeding of viable intestinal mucosa along suture line and onto peristomal skin.<br><b>Pseudo-verrucous lesions:</b> wart-like lesions from chronic moisture irritation around stoma<br><b>Psoriasis:</b> chronic disease characterized by proliferation of epidermis that often appears as a erythematic, thick, silvery-white, scaling plaque.<br><b>Pyoderma Gangrenosum:</b> ulcerative inflammatory skin condition of unknown etiology that starts as pustules and break open to form full thickness ulcers often with undermining, raged edges, and overhanging margins<br><b>Trauma:</b> loss of epidermis around stoma<br><b>Weepy:</b> moist, draining areas\r\n',46),
	(64,'','<b>Mucous Fistula:</b> distal end on the intestine that is brought through the intestinal wall, everted, and attached. Is considered ?non-functioning? but may still produce mucous',44),
	(65,'','<b>Nil</b><br><b>Small:</b> <20mls in 24 hours<br><b>Moderate:</b> 25-100mls in 24 hours<br><b>Large:</b> > 100mls in 24 hours<br><b>Clear</b><br><b>Mucous</b><br><b>Odorous</b><br><b>Fecal\r\n',411),
	(66,'','<b>Boggy:</b> soft, spongy<br><b>Erythema:</b> redness of the skin-may be intense bright red to dark red or purple<br><b>Excoriated:</b> superficial loss of tissue that presents irregular with areas of erythema, rash, and denudation<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br><b>Intact:</b> unbroken skin<br><b>Macerated:</b> wet, white<br><b>Rash:</b> temporary eruption on the skin-often raised, red, sometimes itchy<br><b>Weepy:</b> moist, draining areas',416),
	(67,'','<b>Nil</b>: no drainge<br><b>Red</b>: urine is red in colour<br><b>Amber</b>: urine is amber in colour<br><b>Yellow</b>: urine is yellow in colour<br><b>Pale Yellow</b>: urine is pale yellow',47),
	(68,'','<b>Bloody</b><br><b>Clear</b>: Normal to patient<br><b>Cloudy</b><br><b>Concentrated</b><br><b>Mucous</b><br><b>Odorous\r\n',48),
	(69,'','<b>Nil</b><br><b>Small</b><br><b>Moderate</b><br><b>Large\r\n',49),
	(70,'','<b>Nil</b><br><b>Small:</b> <200mls in 24 hours<br><b>Moderate:</b> < 500mls in 24 hours<br><b>Large:</b> <1200mls in 24 hours<br><b>High Output:</b> >1200mls in 24 hours\r\n',51),
	(71,'','<b>Bloody</b><br><b>Green</b> (dark, tenacious)<br><b>Brown<br><b>Yellow\r\n',53),
	(72,'','<b>Active:</b> Incision is active<br><b>Closed:</b> Incision has closed by primary intention or for other reasons of closure (see below)',69),
	(73,'','<b>Capillary Refill:</b> Length of time taken for normal skin colour to resume after pressure is applied. Normal time for a limb is less than 4 seconds',467),
	(74,'','<b>Edema</b>: Insterstital Collection of fliud<br><br><b>0 or None</b><br><b>+1 or Trace</b><br><b>+2 or Moderate</b><br><b>+3 or Deep</b><br><b>+4 or Very Deep</b><br><b>Homan\'s Sign (+ive):</b> Calf pain on dorsiflexion of the foot. This may be a sign of deep vein thrombosis, but it\'s absence does not rule it out<br><b>Stemmer\'s Sign (+ive):</b> Inability to tent (when pinched) edematous skin on the dorsal surface of a toe. This suggests lymphedema, but its absence does not rule it out',281),
	(75,'','<b>Sensation</b>: The use of a 5.07 monofilament to assess the foot for loss of protective sensation<br><br><b>Proprioception:</b> perception from nerve endings that provides the body with information concerning movement and position',295),
	(76,'','<b>Foot Deformities</b><br><br><b>Bunion:</b> Misaligned great toe joint that becomes tender and swollen with the tip angling towards the other toes.<br><b>Callus:</b> Usually seen on the plantar aspect of the foot, it is the thickening of epidermis at a site of external pressure and friction<br><b>Corn:</b> Usually seen on the toes, it is the thickening of epidermis at a site of external pressure and friction. May be painful.<br><b>Planter Wart:</b> Small circular lesion caused by a virus that presents as a small black dot surrounded by a callus, located on the planter surface of the foot<br><b>Dropped Metatarsal Head:</b> Planter foot deformity often seen with peripheral neuropathy and associated with atrophied fat pads, calluses, and high-risk areas for ulcer and infection.<br><b>Hammer (claw) toe:</b> Contraction of a toe joint from tightened ligaments and tendons that causes the joint and toe to curl downward.<br><b>Acute Charcot:</b> Progressive, degenerative disease of the foot joints, characterized by edema, pain, hemorrhage, heat, bone deformity, bone fragmentation, and joint instability. Treatment is critical in this phase.<br><b>Chronic Charcot:</b> Reconstruction and healing of foot joints and bones after acute charcot treated. Remodeling and fusion of damaged structures lead to decreased joint mobility and rounding of large bone fragments.<br><b>Crossed Toe:</b> Toe or Toes cross each other',328),
	(77,'','<b>Active</b>: Drain is active, still in use<br><b>Closed</b>: Drain is no longer in use, it is discontinued/removed',428),
	(78,'','<b>Intact</b>: Skin at drain site is intact<br><b>Eroded</b>: Skin at drain site is eroded, tissue loss<br><b>Hypergranulated</b>: (proud flesh) skin at drain site is red, moist tissue raised above the level of the skin',215),
	(79,'','<b>Additional Days</b>: If you are wishing to document previous drainage dates with drainage amounts and characteristics, choose the number of days and press \"OK\". Then fill in as needed ',433),
	(80,'','<b>Clear</b><br><b>Cloudy</b><br><b>Mucousy</b><br><b>Bile</b>: dark, tenacious<br><b>Odourous</b><br><b>Serous</b>: thin, clear<br><b>Sanguineous</b>: bloody<br><b>Yellow</b><br><b>Green</b><br><b>Purulent</b>: thick, cloudy',199),
	(81,'','<b>Elevated Warmth</b>: increased warmth when compared to skin in adjacent area<br><b>Erythema:</b> redness of the skin-may be intense bright red to dark red or purple<br><b>Excoriated</b>: superficial loss of tissue<br> <b>Fragile</b>: skin that is at risk for breakdown<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br>\r\n<b>Intact:</b> unbroken skin<br><b>Macerated</b>: wet, white<br><b>Rash:</b> temporary eruption on the skin-often raised, red, sometimes itchy',198),
	(82,'','<b>Nil</b><br><b>Serous:</b> thin, clear<br><b>Sanguineous:</b> bloody<br><b>Green</b><br><b>Brown</b><br><b>Purulent:</b> thick, cloudy<br><b>Other</b>\r\n',438),
	(83,'','<b>Post-op Management:</b> Check all that you assessed and are within normal parameter. A parameter checkbox left blank or if you check \"Not Assessed\" it means not assessed today. If you check \"Concerns Noted- See Progress Note\", it indicates that there is a concern with one or more of the parameters and a Progress Note will be done. If the parameter that is requiring a Progress Note is wound related, chart this in Pixalere (The PN is located on the Summary page).',459),
	(84,'','<b>Pain:</b> quantified on the Visual Analogue Scale where 0 = no pain and 10 = excruciating as described by the patient/client/resident',72),
	(85,'','<b>Pain Comments</b>: Type in any additional comments for pain assessment, if needed',378),
	(86,'','<b>In situ</b>: Remains unchanged<br><b>Removed</b>: Closure device is removed',210),
	(87,'','<b>Fully Epithelialized</b>: covered completely with new epithelial tissue<br><b>Approximated</b>: Edges of cut tissue are in contact to each other and healing by primary intention.<br><b>Tenuous:</b> Thin or fragile connection between the edges of cut tissue<br><b>Gaping</b>: Edges of cut tissue are not in contact with each other. If this occurs, reassess wound charting for wound assessment: healing by secondary intention.  \r\n',75),
	(88,'','<b>Type</b>: composition of exudate<br><br><b>Nil</b>: No drainage<br><b>Serous</b>: thin, clear<br><b>Sanguineous</b>: bloody<br><b> Green</b>: drainge with a green colour<br><b>Purulent</b>: thick, cloudy',76),
	(89,'','<b>Intact:</b> unbroken skin<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br><b>Erythema:</b> redness of the skin-may be intense bright red to dark red or purple<br><b>Edema:</b> interstitial collect of fluid<br><b>Blister:</b> an elevation or separation of the epidermis containing fluid<br><b>Rash:</b> temporary eruption on the skin-often raised, red, sometimes itchy',77),
	(90,'','<b>Undermining</b>: a separation of tissue that occurs underneath the intact skin of the wound perimeter ',12),
	(91,'','<b>Sinus or Tunnel</b>: a channel that extends from any part of the wound and tracks into deeper tissue.',141),
	(92,'','<b>Clinical Path</b>: Some sites have guidelines for post-operative outcomes and \"usual care\" for patients having certain procedures. If your site utilizes these guidelines, choose either \"on\" or \"off\" (client was on clinical path but has deviated from the listed outcomes or care). Choose \"N/A\" if you site does not utilize these guidelines.',386),
	(93,'','<b>Clinical Path</b>: Some sites have guidelines for post-operative outcomes and \"usual care\" for patients having certain procedures. If your site utilizes these guidelines, choose either \"on\" or \"off\" (client was on clinical path but has deviated from the listed outcomes or care). Choose \"N/A\" if you site does not utilize these guidelines.',426),
	(94,'','<b>Urostomy</b>: general term for urinary ostomy<br><b>Ileal Conduit</b>: portion of the ileum is segmented and brought forward through the abdomen creating a stoma. The ureters are then anastomosted to the ileal segment, which acts as a conduit to allow the passage of urine into a pouch or appliance the patient wears.<br><b>Continent Pouch- urinary</b>: general term for neobladder or continent cutaneous diversion<br><b>Neobladder (eg: Studer Pouch):</b> internal urinary reservoir or pouch is constructed from a portion of ileum or colon in which the ureters are anastomosted. The reservoir is then anastomosted to the urethra. After several weeks of healing, continent voiding will occur through the urethra either by intermittent catheterization and/or retained urethral sphincter control.<br><b>Continent Cutaneous Diversion (eg: Indiana Pouch):</b> internal urinary reservoir created from portion of the ileum and colon in which the ureters are anastomosted. A portion of the ileum is then brought forward through the abdominal, creating a stoma. After several weeks of healing, continent voiding will occur through the stoma when client performs catheterization at regular intervals.<br><b>Jejunostomy</b>: rarely performed, a portion of patient?s jejunum is brought through the abdomen and everted to create a stoma. Can be permanent or temporary and allows the passage of stool into a pouch or appliance the patient wears.<br><b>Ileostomy</b>: portion of patient?s ileum is brought through the abdomen and everted to create a stoma. Usually located on the abdominal right lower quadrant. Can be permanent or temporary and allows the passage of stool into a pouch or appliance the patient wears.<br><b>Cecostomy</b>: rarely performed, the patient?s cecum is brought through the abdomen. Often a temporary measure to allow decompression of the colon.<br><b>Colostomy:</b>  portion of patient?s colon is brought through the abdomen and everted to create a stoma. Can be permanent or temporary and allows the passage of stool into a pouch or appliance the patient wears.<br>-Ascending: Usually located on the abdominal right lower quadrant<br>-Transverse: May be located on either the abdominal right, mid, or left lower quadrant<br>-Descending: Usually located on the abdominal left lower quadrant<br>-Sigmoid: Usually located on the abdominal left lower quadrant\r\n',462),
	(95,'','<b>Alphas, Numbers, Tubes/Drains and\r\nOstomies</b>: These will generate an Assessment page unique to its designation<br><br><b> \"A, B, C, etc\"</b> : Used for charting Wounds<br><b>\"1, 2, 3, etc\"</b> : Used for charting Incisions<br><b> Number with Grey Line (tube)</b> : Used for charting Tubes/Drains<br><b>Yellow Circle</b> : Used for charting a Urostomy<br><b>Brown Circle</b> : Used for charting a Fecal Ostomy<br><br>Click on the ?wound? type you wish to mark on the blue person. Click on the blue person to mark where that ?wound? type is. If you want multiples of that ?wound? type, continue clicking on the blue person to generate further icons. If you made a mistake, click on the icon (in reverse order) again to erase\r\nFor example: This shows one wound, one tube/drain, one incision, and one fecal ostomy<br><br><b>Four Pointed Arrow (move icon)</b>: Used to move an active (yellow) Alpha, Number, Tube/Drain or Ostomy. Click on the ?Move\" icon then click on the alpha, number, tube/ drain, ostomy icon that is yellow in colour- you will see it becomes bolded. Click (not drag) your mouse to the location you wish to move the icon to then you will see it move. Continue charting as usual. You change will be documented in the Summary automatically for you.<br><br>\r\nThe second line that contains short incision images are extra features you can use to enhance Blue Person. Used to draw Incisions for reference only (non-functional).<b>Must be associated with an already existing incison number (\"1\", \"2\", etc), otherwise, no assessment documentation page will be generated.</b>',98),
	(96,'Tester','',88),
	(97,'Review','Review',555),
	(98,'Review','Review',555),
	(99,'Test Allergies','tadsfasdfadsf',92),
	(100,'Test Allergies','tadsfasdfadsf',92);

/*!40000 ALTER TABLE information_popup ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table library
# ------------------------------------------------------------

DROP TABLE IF EXISTS library;

CREATE TABLE library (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  pdf varchar(255) DEFAULT NULL,
  orderby int(11) DEFAULT NULL,
  category int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table license
# ------------------------------------------------------------

DROP TABLE IF EXISTS license;

CREATE TABLE license (
  id int(11) NOT NULL,
  license text,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES license WRITE;
/*!40000 ALTER TABLE license DISABLE KEYS */;

INSERT INTO license (id, license)
VALUES
	(1,'-----BEGIN PGP MESSAGE-----\nVersion: BCPG v1.43\n\nowJ4nJvAy8zAxHhA5Eru160fbzOeXpgknpOZnJpXnOqWmZPql5ibquuZnpdflJri\npyYqrKyrqwCVVkgDynMph5SmKniV5igYWgKRlbGZlamhgqtLiIKRgaEhV0pRYmae\nrSFXXmlufFpicmZOZklmajFQIL+4JD+3EsgoS8zJTIkvzSvJzLE1MjA10DfUhygv\nLU4tsjXlysxLzizOzAcZUp5fmpcCpDuZvFgYGJkYTFiZQE7SlQnIrEjMSS1KVdDw\ngTrNO7VSU8GmpCixLLPYoQAqrZecn2vHwMUpAPPs32z+f0qa6dYz7tld3rkhP+p9\n1+Fzbm8f/DI/mPkhXvhv8b6r7z1tp9zu7BNhVq3TPD//d/y7meuFPprPSzt72e3N\n4tefbn639LE7PjfzzdwJvn/N61gfZgqr2h1aUNLj7t35ZLaV8ePaa342UceNEv8W\ndOm5F51zVmFOjxYJ8nm9NemwogmTlL/SJtMLU0LPH7K9XLfrkZn23W3qHewtv+NZ\nm3vt32a1L12S/fBT/LI7c2I3vPAvEpoz8ZB/lPPNrgzX3rXtxlZheidnCVnpNbr6\n37/bfWNlcEP8WZ9f87iU9530etf0x+kDlxr3tWPyu7+tPlMl39lpvHOa97rjRrfV\nxT6yvVZ2lS6Nsyl7Jhf04GR4RsGk6kufTkkdzDSe5uzVuGWv0JTYVgdWizMa0+7b\nNc+fJ9Tyom+W9L9zhtc8TtwSc5x+QH/l/ZNdU0y+7zLfzHVUbtXqdece8Jr4Lg1n\nfr8jyCMj8dQf3dWbK5ll26+8EeaSuB2y7+tfS65PEqfDBcs2W6Qv2Hm+c9+H6dl9\nGmabZjcE/nK4yWVSpXst+c6/ZzYBVrVi2nnNnLeSfkx7FNyw4Wlrh/60aWrL/3xb\nuKDkR25zw8ntZ0W7vNsTHsX9/bTvybbsx5WC0Z0Miy8tu/+iT6ffZnOj1s0CK65S\ng5Z2VRum3WFBtaVGVy72Zv+vOZ41vTVCUbRgwr429h5Xo6+h8b5if3/sObv5DgCr\nN1mv\n=Z2oy\n-----END PGP MESSAGE-----');

/*!40000 ALTER TABLE license ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table limb_assessment
# ------------------------------------------------------------

DROP TABLE IF EXISTS limb_assessment;

CREATE TABLE limb_assessment (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(1) DEFAULT '0',
  patient_id int(11) DEFAULT '0',
  limb_show int(1) DEFAULT '0',
  left_missing_limbs text,
  right_missing_limbs text,
  left_pain_assessment text,
  right_pain_assessment text,
  pain_comments text,
  left_skin_assessment text,
  right_skin_assessment text,
  left_temperature_leg int(11) DEFAULT '0',
  right_temperature_leg int(11) DEFAULT '0',
  left_temperature_foot int(11) DEFAULT '0',
  right_temperature_foot int(11) DEFAULT '0',
  left_temperature_toes int(11) DEFAULT '0',
  right_temperature_toes int(11) DEFAULT '0',
  left_skin_colour_leg int(11) DEFAULT '0',
  right_skin_colour_leg int(11) DEFAULT '0',
  left_skin_colour_foot int(11) DEFAULT '0',
  right_skin_colour_foot int(11) DEFAULT '0',
  left_skin_colour_toes int(11) DEFAULT '0',
  right_skin_colour_toes int(11) DEFAULT '0',
  left_less_capillary int(11) DEFAULT '0',
  right_less_capillary int(11) DEFAULT '0',
  left_more_capillary int(11) DEFAULT '0',
  right_more_capillary int(11) DEFAULT '0',
  left_dorsalis_pedis_palpation int(11) DEFAULT '0',
  right_dorsalis_pedis_palpation int(11) DEFAULT '0',
  left_posterior_tibial_palpation int(11) DEFAULT '0',
  right_posterior_tibial_palpation int(11) DEFAULT '0',
  doppler_lab int(11) DEFAULT '0',
  left_dorsalis_pedis_doppler text,
  right_dorsalis_pedis_doppler text,
  left_posterior_tibial_doppler text,
  right_posterior_tibial_doppler text,
  left_interdigitial_doppler text,
  right_interdigitial_doppler text,
  ankle_brachial_lab int(11) DEFAULT '0',
  ankle_brachial_date text,
  left_tibial_pedis_ankle_brachial int(11) DEFAULT NULL,
  right_tibial_pedis_ankle_brachial int(11) DEFAULT NULL,
  left_dorsalis_pedis_ankle_brachial int(11) DEFAULT NULL,
  right_dorsalis_pedis_ankle_brachial int(11) DEFAULT NULL,
  left_ankle_brachial int(11) DEFAULT NULL,
  right_ankle_brachial int(11) DEFAULT NULL,
  toe_brachial_lab int(11) DEFAULT '0',
  toe_brachial_date text,
  left_toe_pressure text,
  right_toe_pressure text,
  left_brachial_pressure text,
  right_brachial_pressure text,
  transcutaneous_lab int(11) DEFAULT '0',
  transcutaneous_date text,
  left_transcutaneous_pressure int(11) DEFAULT '0',
  right_transcutaneous_pressure int(11) DEFAULT '0',
  left_edema_severity int(11) DEFAULT '0',
  right_edema_severity int(11) DEFAULT '0',
  left_edema_location int(11) DEFAULT '0',
  right_edema_location int(11) DEFAULT '0',
  sleep_positions text,
  left_ankle_cm int(11) DEFAULT NULL,
  left_ankle_mm int(11) DEFAULT NULL,
  right_ankle_cm int(11) DEFAULT NULL,
  right_ankle_mm int(11) DEFAULT NULL,
  left_midcalf_cm int(11) DEFAULT NULL,
  left_midcalf_mm int(11) DEFAULT NULL,
  right_midcalf_cm int(11) DEFAULT NULL,
  right_midcalf_mm int(11) DEFAULT NULL,
  left_sensation text,
  right_sensation text,
  left_score_sensation text,
  right_score_sensation text,
  left_sensory text,
  right_sensory text,
  user_signature text,
  last_update varchar(255) DEFAULT '',
  professional_id int(11) DEFAULT '0',
  limb_comments text,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY patient_id (patient_id),
  KEY professional_id (professional_id),
  CONSTRAINT limbassess_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT limbassess_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT limb_assessment_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id) ON DELETE CASCADE,
  CONSTRAINT limb_assessment_ibfk_2 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table location_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS location_images;

CREATE TABLE location_images (
  id int(11) NOT NULL AUTO_INCREMENT,
  orderby int(4) DEFAULT NULL,
  location text,
  wound_name text,
  wound_view text,
  gender text,
  limb int(1) DEFAULT NULL,
  lower_limb int(1) DEFAULT NULL,
  foot int(1) DEFAULT NULL,
  filename_full text,
  filename_left text,
  filename_midline text,
  filename_right text,
  blocked_boxes text,
  woundcare int(1) DEFAULT NULL,
  stoma int(1) DEFAULT NULL,
  incision int(1) DEFAULT NULL,
  drain int(1) DEFAULT NULL,
  slices text,
  burn int(11) DEFAULT NULL,
  rows int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

LOCK TABLES location_images WRITE;
/*!40000 ALTER TABLE location_images DISABLE KEYS */;

INSERT INTO location_images (id, orderby, location, wound_name, wound_view, gender, limb, lower_limb, foot, filename_full, filename_left, filename_midline, filename_right, blocked_boxes, woundcare, stoma, incision, drain, slices, burn, rows)
VALUES
	(1,1,'Head and neck','Head/neck (Anterior)','A','F',0,0,0,'f_ant_headneck.jpg','','','','F:C1,C2,C3,C4,C5,C13,C14,C15,C16,6,11,12,28,124,134,140,150,156,166,182,183,172,188',1,0,1,1,NULL,0,12),
	(2,1,'Head and neck','Head/neck (Posterior)','P','F',0,0,0,'f_pos_headneck.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,11,12,21,28,37,44,117,124,133,140,149,156,165,181,182,172,187,188',1,0,1,1,NULL,0,12),
	(3,1,'Head and neck','Head/neck (Anterior)','A','M',0,0,0,'m_ant_headneck.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,12,21,28,117,124,133,140,149,156,165,181,182,172,188',1,0,1,1,NULL,0,12),
	(4,1,'Head and neck','Head/neck (Posterior)','P','M',0,0,0,'m_pos_headneck.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,12,21,28,37,44,117,124,133,140,149,156,165,181,172,188',1,0,1,1,NULL,0,12),
	(5,2,'Full front','Front','A','F',0,0,0,'f_ant_full_front.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,12,101,117,133,149,165,181',0,0,0,0,NULL,0,12),
	(6,2,'Full front','Front','A','M',0,0,0,'m_ant_full_front.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,11,12,101,108,117,124,133,140,149,156,165,172,181,188',0,0,0,0,NULL,0,12),
	(7,3,'Shoulder','Shoulder (Anterior)','A','F',1,0,0,'','f_ant_left_shoulder.jpg','','f_ant_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R2,R9,R10,R11,R12,38,41,42,118;R:C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,R1,R2,R9,R10,R11,R12,39,40,55,123',1,0,1,1,NULL,0,12),
	(8,3,'Shoulder','Shoulder (Posterior)','P','F',1,0,0,'','f_pos_left_shoulder.jpg','','f_pos_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C6,C7,C12,C13,C14,C15,C16,R1,R2,R8,R9,R10,R11,R12,40,107;R:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R2,R9,R10,R11,R12,42,102,118,119',1,0,1,1,NULL,0,12),
	(9,3,'Shoulder','Shoulder (Anterior)','A','M',1,0,0,'','m_ant_left_shoulder.jpg','','m_ant_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C6,C7,C13,C14,C15,C16,R1,R9,R10,R11,R12,27,28,44,104,120,121;R:C1,C2,C3,C4,C10,C11,C12,C13,C14,C15,C16,R1,R9,R10,R11,R12,21,22,23,37,105,120,121',1,0,1,1,NULL,0,12),
	(10,3,'Shoulder','Shoulder (Posterior)','P','M',1,0,0,'','m_pos_left_shoulder.jpg','','m_pos_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,R1,R8,R9,R10,R11,R12,23,107;R:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R9,R10,R11,R12,26,102,118,119',1,0,1,1,NULL,0,12),
	(11,4,'Arm','Arm (Anterior)','A','F',1,0,0,'','f_ant_left_arm.jpg','','f_ant_right_arm.jpg','L:R1,C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,26,27,42,43,58,59,75,91,103,107,119,123,135,151,152,167,168,183,184,185;R:R1,C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,22,23,27,38,39,54,55,70,71,75,86,91,102,106,107,122,123,138,139,153,154,155,168,169,170,171,183,184,185,186,187',1,0,1,1,NULL,0,12),
	(12,4,'Arm','Arm (Posterior)','P','F',1,0,0,'','f_pos_left_arm.jpg','','f_pos_right_arm.jpg','L:C1,C2,C3,C4,C10,C11,C12,C13,C14,C15,C16,R1,R2,37,38,39,53,54,69,70,85,101,105,117,121,137,152,153,167,168,169,183,184,185;R:C1,C2,C3,C4,C5,C6,C7,C8,C14,C15,C16,R1,R2,43,44,45,59,60,61,76,77,92,93,109,121,125,137,141,153,154,169,170,185,186,187',1,0,1,1,NULL,0,12),
	(13,4,'Arm','Arm (Anterior)','A','M',1,0,0,'','m_ant_left_arm.jpg','','m_ant_right_arm.jpg','L:R1,C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,25,26,27,42,43,58,59,75,89,91,103,107,119,123,135,139,151,152,167,168,183,184,185;R:R1,C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,22,23,27,38,39,43,54,55,59,70,71,75,86,91,102,106,107,118,122,123,134,138,139,153,154,155,168,169,170,171,183,184,185,186,187',1,0,1,1,NULL,0,12),
	(14,4,'Arm','Arm (Posterior)','P','M',1,0,0,'','m_pos_left_arm.jpg','','m_pos_right_arm.jpg','L:C1,C2,C3,C10,C11,C12,C13,C14,C15,C16,R1,R2,36,37,38,39,40,52,53,54,55,68,69,70,84,85,100,101,116,121,132,137,152,153,167,168,169,183,184,185;R:C1,C2,C3,C4,C5,C6,C7,C14,C15,C16,R1,R2,41,42,43,44,45,58,59,60,61,75,76,77,92,93,104,108,109,120,125,136,137,152,153,168,169,170,184,185,186,187',1,0,1,1,NULL,0,12),
	(15,5,'Hand','Hand','O','F',1,0,0,'','f_left_hand.jpg','','f_right_hand.jpg','L:1,2,3,4,5,6,7,12,13,14,15,16,17,18,19,20,21,22,23,29,30,31,32,33,34,37,38,39,46,47,48,113,122,123,129,130,139,145,146,155,156,157,158,159,160,161,162,163,171,172,173,174,175,176,177,178,179,180,181,185,186,187,188,189,190,191,192;R:1,2,3,4,5,6,10,11,12,13,14,15,16,17,18,19,20,27,28,29,30,31,32,33,34,35,43,44,119,128,144,145,146,147,148,149,150,151,159,160,161,162,163,164,165,166,167,174,175,176,177,178,179,180,181,182,183,189,190,191,192',1,0,1,1,NULL,0,12),
	(16,5,'Hand','Hand','O','M',1,0,0,'','m_left_hand.jpg','','m_right_hand.jpg','L:C1,2,3,4,5,6,7,8,12,13,14,15,16,18,19,20,21,22,23,29,30,31,32,34,35,36,37,38,39,47,48,50,51,52,53,54,55,66,67,113,130,142,146,147,156,157,158,159,160,162,163,171,172,173,174,175,176,178,179,180,187,188,189,190,191,192;R:1,2,3,4,5,12,13,14,15,16,17,18,19,20,27,28,29,30,31,32,33,34,42,43,44,45,46,47,48,59,60,61,62,63,144,145,146,147,148,149,159,160,161,162,163,164,165,166,174,175,176,177,178,179,180,181,182,189,190,191,192',1,0,1,1,NULL,0,12),
	(17,6,'Chest','Chest','A','F',0,0,0,'f_ant_chest.jpg','','','','F:C1,C16,2,3,4,14,15,18,19,31,34,162,178',1,0,1,1,NULL,0,12),
	(18,6,'Chest','Chest','A','M',0,0,0,'m_ant_chest.jpg','','','','F:1,2,16,17,33,97,113,129,145,160,161,176,177,192',1,0,1,1,NULL,0,12),
	(19,7,'Abdomen','Abdomen','A','F',0,0,0,'f_ant_abdomen.jpg','','','','F:C1,C16,2,15,18,31,34,47,50,63,66,79,82,95,98,111,114,127,143',1,1,1,1,NULL,0,12),
	(20,7,'Abdomen','Abdomen','A','M',0,0,0,'m_ant_abdomen.jpg','','','','F:C1,C16,18,34,47,50,63,66,79,82,95,98,111,114,127,130,143,146,159,162,178',1,1,1,1,NULL,0,12),
	(21,8,'Flank','Flank','L','F',0,0,0,'','f_lat_left_flank.jpg','','f_lat_right_flank.jpg','L:C1,C2,C3,C4,C5,C6,C7,C13,C14,C15,C16,R1,R11,R12,24,25,26,40,41,56,60,76,92,107,108,123,124,136,140,152,153,156;R:C1,C2,C3,C4,C10,C11,C12,C13,C14,C15,C16,R1,R11,R12,24,25,41,53,69,85,101,117,118,133,134,149',1,0,1,1,NULL,0,12),
	(22,8,'Flank','Flank','L','M',0,0,0,'','m_lat_left_flank.jpg','','m_lat_right_flank.jpg','L:C1,C2,C3,C4,C5,C6,C7,C12,C13,C14,C15,C16,R1,R2,R11,R12,40,107,123,136,139,152,153,155;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R1,R2,R10,R11,R12,41,102,118,134,137',1,0,1,1,NULL,0,12),
	(23,9,'Full back','Back','P','F',0,0,0,'f_pos_full_back.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,11,12,85,101,117,124,133,140,149,165,181',1,1,1,1,NULL,0,12),
	(24,9,'Full back','Back','P','M',0,0,0,'m_pos_full_back.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,12,37,101,108,117,124,133,149',1,1,1,1,NULL,0,12),
	(25,10,'Upper back','Upper back','P','F',0,0,0,'f_pos_upper_back.jpg','','','','F:1,2,3,4,17,18,19,33,34,49,65,13,14,15,16,30,31,32,47,48,64,80,113,128,129,144,145,160,161,175,176,177,178,191,192',0,0,0,0,NULL,0,12),
	(26,10,'Upper back','Upper back','P','M',0,0,0,'m_pos_upper_back.jpg','','','','F:C1,C16,2,15,162,175,178,191',0,0,0,0,NULL,0,12),
	(27,11,'Lower back','Lower back','P','F',0,0,0,'f_pos_lower_back.jpg','','','','F:C1,C16,34,47,50,63,66,79,82,95,98,111,114,127,130,143',0,0,0,0,NULL,0,12),
	(28,11,'Lower back','Lower back','P','M',0,0,0,'m_pos_lower_back.jpg','','','','F:C1,C16,50,63,66,79,82,95,98,111,114,127,130',0,0,0,0,NULL,0,12),
	(29,12,'Buttock','Buttock','P','F',0,0,0,'f_pos_buttock.jpg','','','','F:R1,R12,C1,C16,18,19,30,31,34,47,50,55,56,57,58,63,71,72,73,74,87,88,89,104,105,120',1,0,1,1,NULL,0,12),
	(30,12,'Buttock','Buttock','P','M',0,0,0,'m_pos_buttock.jpg','','','','F:R1,R12,C1,C16,18,31,55,56,57,71,72,73,87,88,89,104,105,120,162,175',1,0,1,1,NULL,0,12),
	(31,13,'Sacralcoccyx','Sacralcoccyx','P','F',0,0,0,'f_pos_sacralcoccyx.jpg','','','','F:C1,C2,C3,C4,C5,C6,C11,C12,C13,C14,C15,C16,R1,R2,R3,R10,R11,R12,119,135,106,122,138',1,0,1,1,NULL,0,12),
	(32,13,'Sacralcoccyx','Sacralcoccyx','P','M',0,0,0,'m_pos_sacralcoccyx.jpg','','','','F:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R2,R3,R10,R11,R12,102,118,134,106,122,138',1,0,1,1,NULL,0,12),
	(33,23,'Full buttock (sx only)','Full buttock','P','F',0,0,0,'f_pos_full_buttock.jpg','','','','F:R1,R12,C1,C16,18,19,31,34,47,50,63,162,163,174,175',1,0,1,1,NULL,0,12),
	(34,23,'Full buttock (sx only)','Full buttock','P','M',0,0,0,'m_pos_full_buttock.jpg','','','','F:R1,R12,C1,C16,18,19,31,34,47,162,163,174,175',1,0,1,1,NULL,0,12),
	(35,15,'Hip','Hip','L','F',0,0,0,'','f_lat_left_hip.jpg','','f_lat_right_hip.jpg','L:C1,C2,C3,C4,C14,C15,C16,5,6,7,12,13,21,29,45,157,173,189;R:C1,C2,C3,C14,C15,C16,4,5,11,12,13,20,28,29,36,45,148,164,180',1,0,1,1,NULL,0,12),
	(36,15,'Hip','Hip','L','M',0,0,0,'','m_lat_left_hip.jpg','','m_lat_right_hip.jpg','L:C1,C2,C3,C4,C14,C15,C16,R1,21,22,23,28,29,37,38,45,53,157,172,173,188,189;R:C1,C2,C3,C14,C15,C16,R1,20,21,26,27,28,29,36,43,44,45,52,60,61,77,93,109,132,148,164,165,180,181',1,0,1,1,NULL,0,12),
	(37,16,'Pelvis','Pelvis (Anterior)','A','F',0,0,0,'f_ant_pelvis.jpg','','','','F:R8,R9,R10,R11,R12,1,16,17,80,81,82,94,95,96,97,98,99,109,110,111,112',1,0,1,1,NULL,0,12),
	(38,16,'Pelvis','Pelvis (Anterior)','A','M',0,0,0,'m_ant_pelvis.jpg','','','','F:R10,R11,R12,1,16,80,81,97,98,99,113,114,115,116,129,130,131,132,133,134,135,95,96,110,111,112,124,125,126,127,128,139,140,141,142,143,144,145,146',1,0,1,1,NULL,0,12),
	(39,17,'Perineum','Perineum','O','F',0,0,0,'f_perineum.jpg','','','','F:C1,C2,C3,C4,C5,C6,C11,C12,C13,C14,C15,C16,R1,R2,R3,R11,R12',1,0,1,1,NULL,0,12),
	(40,17,'Perineum','Perineum','O','M',0,0,0,'m_perineum.jpg','','','','F:C1,C2,C3,C4,C5,C6,C11,C12,C13,C14,C15,C16,R1,R2,R10,R11,R12,119,122,135,138',1,0,1,1,NULL,0,12),
	(41,24,'Full Leg Sx Only','Leg (Anterior)','A','F',1,1,0,'f_ant_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,8,9,10,25,41,54,57,70,86,73,89,102,105,121,137,153,166,169,182,185',1,0,1,1,NULL,0,12),
	(42,24,'Full Leg Sx Only','Leg (Posterior)','P','F',1,1,0,'f_pos_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,8,9,10,25,41,57,73,89,105,121,137,153,169,185',1,0,1,1,NULL,0,12),
	(43,24,'Full Leg Sx Only','Leg (Anterior)','A','M',1,1,0,'m_ant_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,70,86,150,166,171,182,187',1,0,1,1,NULL,0,12),
	(44,24,'Full Leg Sx Only','Leg (Posterior)','P','M',1,1,0,'m_pos_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,86,150,166,171,182,187',1,0,1,1,NULL,0,12),
	(45,19,'Upper leg','Upper leg (Anterior)','A','F',1,1,0,'','f_ant_left_upper_leg.jpg','','f_ant_right_upper_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,94,110,126,142,153,158,169,174,185,190;R:C1,C2,C9,C10,C11,C12,C13,C14,C15,C16,67,83,99,115,131,147,163,179',1,0,1,1,NULL,0,12),
	(46,19,'Upper leg','Upper leg (Posterior)','P','F',1,1,0,'','f_pos_left_upper_leg.jpg','','f_pos_right_upper_leg.jpg','L:C1,C2,C9,C10,C11,C12,C13,C14,C15,C16,51,67,83,99,115,131,147,163,179;R:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,94,110,126,142,158,174,190',1,0,1,1,NULL,0,12),
	(47,19,'Upper leg','Upper leg (Anterior)','A','M',1,1,0,'','m_ant_left_upper_leg.jpg','','m_ant_right_upper_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,105,121,137,142,153,158,169,174,185,190;R:C1,C2,C9,C10,C11,C12,C13,C14,C15,C16,99,115,131,147,163,179',1,0,1,1,NULL,0,12),
	(48,19,'Upper leg','Upper leg (Posterior)','P','M',1,1,0,'','m_pos_left_upper_leg.jpg','','m_pos_right_upper_leg.jpg','L:C1,C2,C3,C9,C10,C11,C12,C13,C14,C15,C16,116,132,148,164,180;R:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,62,78,94,110,126,142,158,174,190',1,0,1,1,NULL,0,12),
	(49,20,'Leg Above Knee Amp','Leg Above Knee Amp (Anterior)','A','F',1,1,0,'','f_ant_left_leg_amp_ak.jpg','','f_ant_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,73,89',1,0,1,1,NULL,0,12),
	(50,20,'Leg Above Knee Amp','Leg Above Knee Amp (Posterior)','P','F',1,1,0,'','f_pos_left_leg_amp_ak.jpg','','f_pos_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,70,86,;R:C1,C2,C3,C4,C5,C6,C7,C8,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,60,76,92',1,0,1,1,NULL,0,12),
	(51,20,'Leg Above Knee Amp','Leg Above Knee Amp (Anterior)','A','M',1,1,0,'','m_ant_left_leg_amp_ak.jpg','','m_ant_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,91;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,70,73,86,89',1,0,1,1,NULL,0,12),
	(52,20,'Leg Above Knee Amp','Leg Above Knee Amp (Posterior)','P','M',1,1,0,'','m_pos_left_leg_amp_ak.jpg','','m_pos_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,70,73,86,89;R:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,91',1,0,1,1,NULL,0,12),
	(53,21,'Leg Below Knee Amp','Leg Below Knee Amp (Anterior)','A','F',1,1,0,'','f_ant_left_leg_amp_bk.jpg','','f_ant_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R9,R10,R11,R12;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R9,R10,R11,R12,73,89,105,118,121',1,0,1,1,NULL,0,12),
	(54,21,'Leg Below Knee Amp','Leg Below Knee Amp (Posterior)','P','F',1,1,0,'','f_pos_left_leg_amp_bk.jpg','','f_pos_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C9,C10,C11,C12,C13,C14,C15,C16,R9,R10,R11,R12,70,86,102,118,121;R:C1,C2,C3,C4,C5,C6,C7,C8,C13,C14,C15,C16,R9,R10,R11,R12,76,92,108,121,124',1,0,1,1,NULL,0,12),
	(55,21,'Leg Below Knee Amp','Leg Below Knee Amp (Anterior)','A','M',1,1,0,'','m_ant_left_leg_amp_bk.jpg','','m_ant_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R8,R9,R10,R11,R12;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R8,R9,R10,R11,R12,73,86,89,102,105,',1,0,1,1,NULL,0,12),
	(56,21,'Leg Below Knee Amp','Leg Below Knee Amp (Posterior)','P','M',1,1,0,'','m_pos_left_leg_amp_bk.jpg','','m_pos_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C9,C10,C11,C12,C13,C14,C15,C16,R9,R10,R11,R12,70,86,102,118;R:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R9,R10,R11,R12',1,0,1,1,NULL,0,12),
	(57,22,'Lower leg','Lower leg (Anterior)','A','F',1,1,0,'','f_ant_left_lower_leg.jpg','','f_ant_right_lower_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C9,C15,C16,90,106,122,138,154,170,186;R:C1,C2,C8,C9,C10,C11,C12,C13,C14,C15,C16,103,119,135,151,167,183',0,0,0,0,NULL,0,12),
	(58,22,'Lower leg','Lower leg (Posterior)','P','F',1,1,0,'','f_pos_left_lower_leg.jpg','','f_pos_right_lower_leg.jpg','L:C1,C2,C8,C9,C10,C11,C12,C13,C14,C15,C16,3,131,147,151,163,167,179,183;R:C1,C2,C3,C4,C5,C6,C7,C8,C9,C15,C16,106,122,138,154,170,186',0,0,0,0,NULL,0,12),
	(59,22,'Lower leg','Lower leg (Anterior)','A','M',1,1,0,'','m_ant_left_lower_leg.jpg','','m_ant_right_lower_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C14,C15,C16,105,121,125,141,157,173,189;R:C1,C2,C3,C9,C10,C11,C12,C13,C14,C15,C16,104,120,132,136,148,164,180',0,0,0,0,NULL,0,12),
	(60,22,'Lower leg','Lower leg (Posterior)','P','M',1,1,0,'','m_pos_left_lower_leg.jpg','','m_pos_right_lower_leg.jpg','L:C1,C2,C3,C9,C10,C11,C12,C13,C14,C15,C16,148,164,180;R:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,94,105,110,121,126,137,141,142,153,157,158,169,173,174,185,189,190',0,0,0,0,NULL,0,12),
	(61,18,'Lower Legs','Lower Legs','O','F',1,1,0,'f_bil_lower_leg.jpg','','','','F:101,116,117,125,132,133,141,148,149,156,157,164,165,172,173,180,181,188,189',1,0,1,1,NULL,0,12),
	(62,18,'Lower Legs','Lower Legs','O','M',1,1,0,'m_bil_lower_leg.jpg','','','','F:129,136,137,144,145,152,153,160,161,168,169,176,177,184,185,192',1,0,1,1,NULL,0,12),
	(65,24,'Ankle and foot','Ankle/Foot','O','F',1,1,1,'','f_left_anklefoot.jpg','','f_right_anklefoot.jpg','L:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,37,38,39,40,41,42,43,44,48,53,54,55,56,57,58,59,60,71,72,73,74,75,76,88,104,113,114,115,116,117,118,119,129,130,131,132,145,146,147,148,161,162,163,164,165,166,177,178,179,180,181,182,183,184,185,186,187,188;R:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,38,39,40,41,42,43,44,45,54,55,56,57,58,59,60,70,71,72,73,74,75,89,90,105,106,123,124,125,126,127,128,141,142,143,144,156,157,158,159,160,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),
	(66,24,'Ankle and foot','Ankle/Foot','O','M',1,1,1,'','m_left_anklefoot.jpg','','m_right_anklefoot.jpg','L:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,37,38,39,40,41,42,43,44,48,53,54,55,56,57,58,59,60,71,72,73,74,75,76,88,104,113,114,115,116,117,118,119,129,130,131,132,145,146,147,148,161,162,163,164,165,166,177,178,179,180,181,182,183,184,185,186,187,188;R:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,38,39,40,41,42,43,44,45,54,55,56,57,58,59,60,70,71,72,73,74,75,89,90,105,106,123,124,125,126,127,128,141,142,143,144,156,157,158,159,160,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),
	(67,25,'Foot with Toes Amp','Foot with Toes Amp','O','F',1,1,1,'','f_left_anklefoot_amp.jpg','','f_right_anklefoot_amp.jpg','L:R1,R2,37,38,39,40,41,42,43,44,45,46,47,48,53,54,55,56,57,58,59,60,61,62,63,64,71,72,73,74,75,76,87,88,113,114,115,116,117,118,129,130,131,132,133,145,146,147,148,149,161,162,163,164,165,166,177,178,179,180,181,182,183,184,185,186,187,188,189;R:R1,R2,33,34,35,36,37,38,39,40,41,42,43,44,53,54,55,56,57,58,59,70,71,72,73,74,75,90,91,106,107,124,125,126,127,128,141,142,143,144,156,157,158,159,160,171,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),
	(68,25,'Foot with Toes Amp.','Foot with Toes Amp','O','M',1,1,1,'','m_left_anklefoot_amp.jpg','','m_right_anklefoot_amp.jpg','L:R1,R2,37,38,39,40,41,42,43,44,45,46,47,48,53,54,55,56,57,58,59,60,61,62,63,64,71,72,73,74,75,76,87,88,113,114,115,116,117,118,129,130,131,132,133,145,146,147,148,149,161,162,163,164,165,166,167,177,178,179,180,181,182,183,184,185,186,187,188,189;R:R1,R2,33,34,35,36,37,38,39,40,41,42,43,44,49,53,54,55,56,57,58,59,70,71,72,73,74,75,90,91,106,107,123,124,125,126,127,128,140,141,142,143,144,156,157,158,159,160,171,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),
	(69,26,'Toes','Toes','O','F',1,1,1,'','f_left_toes.jpg','','f_right_toes.jpg','L:R1,R10,R11,R12,17,18,19,20,21,22,23,24,26,27,28,29,30,39,40,56,96,110,111,112,115,116,117,118,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144;R:R1,R10,R11,R12,19,20,21,22,23,26,27,28,29,30,31,32,42,43,57,58,65,81,82,97,98,99,113,114,115,116,124,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144',1,0,1,1,NULL,0,12),
	(70,26,'Toes','Toes','O','M',1,1,1,'','m_left_toes.jpg','','m_right_toes.jpg','L:R1,R10,R11,R12,17,18,19,20,21,22,23,24,26,27,28,29,30,39,40,56,96,110,111,112,115,116,117,118,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144;R:R1,R10,R11,R12,18,19,20,21,22,23,26,27,28,29,30,31,32,42,43,57,58,65,81,82,97,98,99,113,114,115,116,124,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144',1,0,1,1,NULL,0,12),
	(71,21,'Knee','Knee (Anterior)','A','M',1,1,0,'','m_ant_left_knee.jpg','','m_ant_right_knee.jpg','L:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16;R:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16',1,0,1,1,NULL,0,12),
	(72,21,'Knee','Knee (Anterior)','A','F',1,1,0,'','f_ant_left_knee.jpg','','f_ant_right_knee.jpg','L:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16;R:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16',1,0,1,1,NULL,0,12),
	(73,27,'Full body','Full body (Ant. & Post.)','O','M',0,0,0,'m_fullbody.jpg','','','','F:1,2,3,6,7,8,9,10,11,14,15,16,17,18,23,24,25,26,31,32,33,34,35,38,39,40,41,42,43,46,47,48,49,56,57,64,65,72,73,80,81,88,89,96,97,104,105,112,113,120,121,128,129,136,137,144,145,152,153,160,225,226,231,232,233,234,239,240,241,242,247,248,249,250,255,256,257,258,263,264,265,266,271,272,273,274,279,280,281,282,287,288,289,290,295,296,297,298,303,304,305,306,311,312,313,314,319,320,321,322,327,328,329,330,335,336,337,338,343,344,345,346,351,352,353,354,359,360,361,362,367,368,369,370,375,376,377,378,383,384',1,1,1,1,'fc(hd):4[],5[],20[],21[],36[],37[];el(hd):22[25],27[25];er(hd):19[25],30[25];sc(hd):12[],13[],28[],29[];nk(nk):44[],45[],52[],53[];ch(at):51[50],54[50],67[],68[],69[],70[],83[],84[],85[],86[],99[],100[],101[],102[];ab(at):115[],116[],117[],118[],131[],132[],133[],134[],147[],148[],149[],150[];bk(pt):59[],60[],61[],62[],75[],76[],77[],78[],91[],92[],93[],94[],107[],108[],109[],110[],123[],124[],125[],126[],139[],140[],141[],142[];bu(bu):157[],158[],173[],174[],155[],156[],171[],172[];pe(gn):164[],165[];ru(ru):50[],66[],82[],98[],114[],63[],79[],95[],111[],127[];lu(lu):55[],71[],87[],103[],119[],58[],74[],90[],106[],122[];ro(ro):130[],146[],162[],143[],159[],175[];lo(lo):135[],151[],167[],138[],154[],170[];rh(rh):161[00],177[],178[],193[],194[],209[00],210[00],176[],191[],192[],207[],208[],223[00],224[00];lh(lh):168[00],183[],184[],199[],200[],215[00],216[00],217[00],218[00],169[],185[],186[],201[],202[];rt(rt):163[],179[],180[],195[],196[],211[],212[],227[],228[],243[],244[],189[],190[],205[],206[],221[],222[],237[],238[],253[],254[];lt(lt):166[],181[],182[],197[],198[],213[],214[],229[],230[],245[],246[],187[],188[],203[],204[],219[],220[],235[],236[],251[],252[];rl(rl):259[],260[],275[],276[],291[],292[],307[],308[],323[],324[],269[],270[],285[],286[],301[],302[],317[],318[],333[],334[];ll(ll):261[],262[],277[],278[],293[],294[],309[],310[],325[],326[],267[],268[],283[],284[],299[],300[],315[],316[],331[],332[];rf(rf):339[],340[],355[],356[],371[],372[],349[],350[],365[],366[],381[],382[];lf(lf):341[],342[],357[],358[],373[],374[],347[],348[],363[],364[],379[],380[];',1,24),
	(74,27,'Full body','Full body (Ant. & Post.)','O','F',0,0,0,'f_fullbody.jpg','','','','F:1,2,3,6,7,8,9,10,11,14,15,16,17,18,23,24,25,26,31,32,33,34,39,40,41,42,43,47,48,49,56,57,64,65,72,73,80,81,88,89,96,97,104,105,112,113,120,121,128,129,136,137,144,153,160,225,226,231,232,233,234,239,240,241,242,247,248,249,250,255,256,257,258,263,264,265,266,271,272,273,274,279,280,281,282,287,288,289,290,295,296,297,298,303,304,305,306,311,312,313,314,319,320,321,322,327,328,329,330,335,336,337,338,343,344,345,346,351,352,353,354,359,360,361,362,367,368,369,370,375,376,377,378,383,384',1,1,1,1,'fc(hd):4[],5[],20[],21[],36[],37[];er(hd):19[25],30[25],35[00],46[00];el(hd):22[25],27[25],38[00];sc(hd):12[],13[],28[],29[];nk(nk):44[],45[],52[],53[];ch(at):51[50],54[50],67[],68[],69[],70[],83[],84[],85[],86[],99[],100[],101[],102[],115[],116[],117[],118[];ab(at):131[],132[],133[],134[],147[],148[],149[],150[];bk(pt):59[],60[],61[],62[],75[],76[],77[],78[],91[],92[],93[],94[],107[],108[],109[],110[],123[],124[],125[],126[],139[],140[],141[],142[];bu(bu):157[],158[],173[],174[],155[],156[],171[],172[];pe(gn):164[],165[];ru(ru):50[],66[],82[],98[],63[],79[],95[],111[];lu(lu):55[],71[],87[],103[],58[],74[],90[],106[];ro(ro):114[],130[],146[],127[],143[],159[];lo(lo):119[],135[],151[],152[00],122[],138[],154[];rh(rh):161[],162[],177[],178[],193[],194[],209[00],210[00],223[00],224[00],175[],176[],191[],192[],207[],208[];lh(lh):167[],168[],183[],184[],199[],200[],215[00],216[00],169[],170[],185[],186[],201[],202[],217[00],218[00];rt(rt):163[],179[],180[],195[],196[],211[],212[],227[],228[],243[],244[],189[],190[],205[],206[],221[],222[],237[],238[],253[],254[];lt(lt):166[],181[],182[],197[],198[],213[],214[],229[],230[],245[],246[],187[],188[],203[],204[],219[],220[],235[],236[],251[],252[];rl(rl):259[],260[],275[],276[],291[],292[],307[],308[],324[],269[],270[],285[],286[],301[],302[],317[],318[],333[],334[];ll(ll):261[],262[],277[],278[],293[],294[],309[],310[],325[],326[],267[],268[],283[],284[],299[],300[],315[],316[],331[],332[];rf(rf):340[],356[],371[00],372[],349[],365[],366[],381[],382[];lf(lf):341[],342[],357[],358[],373[],374[],347[],348[],363[],364[],379[],380[];',1,24);

/*!40000 ALTER TABLE location_images ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logs_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS logs_access;

CREATE TABLE logs_access (
  time_stamp int(11) DEFAULT NULL,
  professional_id int(11) DEFAULT NULL,
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT NULL,
  action varchar(255) DEFAULT NULL,
  table_name varchar(255) DEFAULT NULL,
  record_id int(11) DEFAULT NULL,
  ip_address varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY professional_id (professional_id),
  KEY patient_id (patient_id)

) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=latin1;




# Dump of table lookup
# ------------------------------------------------------------

DROP TABLE IF EXISTS lookup;

CREATE TABLE lookup (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(11) DEFAULT NULL,
  resource_id int(11) DEFAULT NULL,
  name text,
  other int(11) DEFAULT NULL,
  orderby int(11) DEFAULT '1',
  title int(1) DEFAULT '0',
  solobox int(1) DEFAULT '0',
  update_access varchar(255) DEFAULT NULL,
  report_value text,
  category_id int(11) DEFAULT '0',
  icd9 varchar(10) DEFAULT NULL,
  icd10 varchar(10) DEFAULT NULL,
  cpt varchar(10) DEFAULT NULL,
  locked int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=3290 DEFAULT CHARSET=latin1;


LOCK TABLES lookup WRITE;
/*!40000 ALTER TABLE lookup DISABLE KEYS */;

INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1,1,1,'Work Save BC (WCB)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (2,1,1,'ICBC',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3,1,1,'Veterans Affairs Canada (DVA)',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (4,0,1,'Native Affairs',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (5,0,1,'Ministry of Human Resources (SS)',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (6,1,1,'Patient/Client/Resident',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (7,1,1,'Hospital/CHA/Facility',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (9,0,2,'Woundcare Clinician',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (10,1,2,'Physician Specialist',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (11,1,2,'GP',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (12,1,2,'Home Care Nurse',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (13,1,2,'Acute Hospital',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (14,1,2,'Rehab Hospital',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (15,1,2,'LTC Facility',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (16,1,2,'Home Support Agency',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (17,0,2,'Other',0,18,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (18,1,3,'Three Links',0,14,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (19,1,3,'GF Strong 2',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (20,1,3,'GF Strong 3',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (21,1,3,'GF Strong 4',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (22,1,3,'CHA1 3B',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (23,1,3,'CHA2 N',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (24,1,3,'CHA4 PS',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (25,1,3,'CHA5 RS',0,10,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (26,1,3,'CHA2 P',0,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (27,0,3,'CHA 3',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (28,1,3,'Vancouver Acute VGH',0,23,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (29,1,3,'Villa Carital',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (30,1,3,'Royal Ascot',0,16,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (31,1,3,'Royal Arch',0,17,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (32,1,3,'Yaletown',0,18,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (33,1,3,'SCI Wound Program',0,21,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (34,1,3,'Haro Park Center',0,19,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (35,1,3,'Louis Brier',0,20,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (36,1,4,'Interior Health',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (37,1,4,'Vancouver Coastal Health',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (38,1,4,'Vancouver Island Health',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (39,1,4,'Fraser Health',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (40,1,4,'Out of Province',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (41,1,4,'Northern Health',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (42,1,5,'Endocrine/Metabolic/Nutritional',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (43,0,41,'Diabetes mellitus',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (44,1,41,'Hyperthyroidism',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (45,1,41,'Hypothyroidism',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (46,1,5,'Heart/Circulation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (47,0,42,'Arteriosclerotic heart disease (ASHD)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (48,1,42,'Cardiac Dysrhythmia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (49,0,42,'Congestive heart failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (50,0,42,'Deep vein thrombosis',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (51,1,42,'Hypertension',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (52,0,42,'Peripheral vascular disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (53,0,42,'Other cardiovascular disease',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (54,1,5,'Muscoloskeletal',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (55,1,43,'Arthritis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (56,0,43,'Hip fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (57,0,43,'Missing limb (e.g. amputation)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (58,1,43,'Osteoporosis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (59,0,43,'Pathological bone fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (60,1,5,'Nuerological',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (61,0,44,'Amyotrophic lateral sclerosis (ALS)',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (62,0,44,'Alzheimers disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (63,1,44,'Aphasia',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (64,0,44,'Cerebral palsy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (65,0,44,'Cerebrovascular accident (stroke)',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (66,0,44,'Dementia other then Alzheimers disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (67,0,44,'Hemiplegia/hemiparesis',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (68,0,44,'Huntingtons chorea',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (69,0,44,'Multiple sclerosis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (70,1,44,'Paraplegia',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (71,0,44,'Parkinsons disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (72,1,44,'Quadriplegia',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (73,0,44,'Seizure disorder',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (74,0,44,'Transient ischemic attack (TIA)',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (75,0,44,'Traumatic brain injury',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (76,1,5,'Psychiatric/Mood',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (77,0,45,'Anxiety disorder',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (78,1,45,'Depression',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (79,0,45,'Manic depressive (bipolar disease)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (80,1,45,'Schizophrenia',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (81,1,5,'Pulmonary',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (82,1,46,'Asthma',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (83,1,46,'Emphysema/COPD',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (84,1,5,'Sensory',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (85,1,47,'Cataracts',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (86,0,47,'Diabetic retinopathy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (87,1,47,'Glaucoma',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (88,0,47,'Macular degeneration',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (89,1,5,'Infections',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (90,0,48,'MRSA',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (91,0,48,'VRE',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (92,1,5,'Other',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (93,1,48,'Osteomyelitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (94,1,48,'HIV',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (95,1,48,'Pneumonia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (96,1,48,'SARS',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (97,1,48,'Septicemia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (98,0,48,'STD',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (99,1,48,'Active TB',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (100,0,48,'UTI (last 30 days)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (101,1,48,'Viral Hepatitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (102,0,48,'Other',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (103,0,49,'Allergies',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (104,1,49,'Anemia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (105,1,49,'Cancer',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (106,0,49,'Gastrointestinal disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (107,0,49,'Liver disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (108,0,49,'Renal failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (109,0,49,'Immunodeficiency',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (110,0,49,'Edema/lymphedema',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (111,0,49,'Immobility',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (112,0,49,'Loss of sensation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (113,0,49,'None of Above',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (114,0,6,'N/A',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (115,0,6,'Poor nutrition',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (116,0,6,'Anticoagulants',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (117,0,6,'Poor perfusion',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (118,0,6,'Chemotherapy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (119,0,6,'Wound infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (120,0,6,'Diabetes',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (121,0,6,'Smoking',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (122,0,6,'Gold/Methotrexate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (123,0,6,'Immobility',0,13,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (124,0,6,'NSAIDS',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (125,0,6,'Elevated B.P.',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (126,0,6,'Steroids',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (127,0,6,'Frail skin',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (128,0,6,'Uncontrolled Blood           Sugar',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (129,0,6,'Chemical Dependency',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (130,1,7,'Male',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (131,1,7,'Female',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (132,1,7,'Other',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (133,0,8,'Neuropathic',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (134,1,8,'Arterial',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (135,1,8,'Malignant',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (136,0,8,'Surgical',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (137,0,8,'Venous',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (138,0,8,'Radiation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (139,1,8,'Trauma',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (140,0,8,'Inflammatory',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (141,0,8,'Burn',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (142,1,8,'Pressure Ulcer - Stage 1',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (143,0,8,'Other',1,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (144,1,9,'2 weeks or less',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (145,0,9,'3-4 weeks',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (146,1,9,'4-6 weeks',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (147,1,9,'6-8 weeks',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (148,1,9,'3 months',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (149,1,9,'6 months',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (150,0,12,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (151,1,12,'2',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (152,1,12,'3',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (153,1,12,'4',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (154,1,12,'X',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (155,1,13,'Normal 34-50 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (156,1,13,'Mild 28- 33 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (157,1,13,'Moderate 21 - 27 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (158,1,13,'Severe < 21 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (159,1,14,'Normal 200 - 420 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (160,1,14,'Mild 160 - 199 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (161,1,14,'Moderate 105 - 159 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (162,1,14,'Severe < 105 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (163,1,38,'MRSA',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (164,1,38,'Staph',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (165,1,38,'VRE',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (166,1,38,'E.coli',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (167,1,38,'Beta-hemolytic Strep',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (168,1,38,'Pseudomonas',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (169,1,38,'Other',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (170,0,39,'Oral',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (171,1,39,'Systemic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (172,1,39,'Topical',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (173,1,15,'Cellulitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (174,1,15,'Osteomyelitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (175,1,15,'Necrotizing fasciitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (176,1,15,'Deep wound infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (177,1,15,'Heavy colonization',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (178,1,16,'Biopsy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (179,1,16,'Bone Scan',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (180,1,16,'CT Scan',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (181,1,16,'X-Ray',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (182,1,16,'Sinogram',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (183,1,17,'Laser',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (184,1,17,'Massage',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (185,1,17,'Ultrasound',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (186,1,17,'Hyperbaric Oxygen',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (187,1,17,'Electro-stim',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (188,1,17,'Other',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (189,1,40,'25 mm/hg',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (190,1,40,'50 mm/hg',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (191,1,40,'75 mm/hg',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (192,1,40,'100 mm/hg',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (193,1,40,'125 mm/hg',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (194,1,40,'150 mm/hg',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (195,0,18,'P',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (196,0,18,'PK',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (197,0,18,'R',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (198,0,18,'H',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (199,0,18,'C',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (203,1,19,'Cool',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (204,1,19,'Warm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (205,1,19,'Hot',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (206,0,20,'F',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (207,0,20,'A',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (208,0,20,'C',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (209,0,20,'K',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (210,0,20,'G',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (211,0,20,'NE',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (212,1,21,'+1 Trace',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (213,1,21,'+2 Moderate',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (214,1,21,'+3 Deep',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (215,1,21,'+4 Very Deep',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (216,0,22,'Y',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (217,0,22,'N',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (218,1,22,'Absent',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (219,0,23,'Y',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (220,0,23,'N',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (221,1,23,'Absent',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (222,1,24,'Nil',0,1,NULL,1,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (223,0,24,'Watery',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (224,0,24,'Thick',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (225,1,25,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (226,0,25,'Wet/Moderate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (227,0,25,'Moist/Small',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (228,0,25,'Copious/Large',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (229,0,26,'Nil',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (230,0,26,'Foul',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (231,0,26,'Sweet',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (232,0,26,'Pungent',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (233,1,28,'TID',0,1,0,0,'0','21',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (234,1,28,'BID',0,2,0,0,'0','14',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (235,1,28,'QD/OD',0,3,0,0,'0','7',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (236,1,28,'Q2 days',0,4,0,0,'0','4',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (237,1,28,'Q3 days',0,5,0,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (238,1,28,'Q4 days',0,6,0,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (239,1,28,' 3x weekly',0,8,0,0,'0','3',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (240,1,28,'2x weekly',0,9,0,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (241,1,28,'1x weekly',0,10,0,0,'0','1',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (242,1,28,'other',0,14,0,0,'0','0',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (243,1,29,'TID',0,1,NULL,0,'0','21',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (244,1,29,'BID',0,1,NULL,0,'0','14',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (245,1,29,'QD/OD',0,1,NULL,0,'0','7',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (246,1,29,'Q2 days',0,1,NULL,0,'0','3.5',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (247,1,29,'Q3 days',0,1,NULL,0,'0','2.33',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (248,1,29,'Q4 days',0,1,NULL,0,'0','1.75',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (249,1,29,'3x weekly',0,1,NULL,0,'0','3',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (250,1,29,'2x weekly',0,1,NULL,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (251,1,29,'1x weekly',0,1,NULL,0,'0','1',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (252,1,29,'other',0,1,NULL,0,'0','0',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (253,0,30,'Paged Referral (within 2 hours)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (254,1,30,'Urgent (24 - 48hrs)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (255,1,30,'Clinical Review (1 - 7 days) ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (256,1,31,'Open',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (257,1,31,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (258,0,32,'Not Applicable',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (259,0,32,'Wound Healed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (260,0,32,'Patient Independant Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (261,0,32,'Patient Deceased',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (262,0,32,'Patient Moved',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (263,0,32,'Patient Transferred',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (265,0,34,'Attached',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (266,0,34,'Not Attached/Undermined',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (267,0,34,'Diffuse/Poorly Defined',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (268,0,34,'Demarcated/Punched Out',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (269,1,36,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (270,0,36,'Brown',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (271,0,36,'Red',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (272,0,36,'Yellow',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (273,1,36,'Green',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (274,0,37,'Healthy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (275,0,37,'Rash',0,9,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (276,0,37,'Erythema',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (277,0,37,'Painful',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (278,0,37,'Macerated',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (279,0,37,'Weepy',0,16,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (280,0,37,'Intact',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (281,0,37,'Itchy',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (282,0,37,'Indurated',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (283,1,10,'Patient inability to adhere to TX Plan',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (284,1,10,'Medication changes',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (285,1,10,'Unavailability of product(s)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (286,1,10,'Infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (287,1,10,'Limited social support',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (288,1,10,'Financial issues',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (289,1,10,'Delay in consultation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (290,1,10,'Deterioration in physical condition',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (291,1,10,'Staffing limitations',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (292,1,10,'Physical and / or mental limitations',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (293,1,10,'Co-Morbidities',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (294,1,10,'Palliative',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (295,1,10,'Other',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (296,1,42,'Hypotension',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (297,1,48,'Clostridium Diff',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (298,0,1,'',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (299,1,1,'Palliative Care Benefits Program',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (300,1,3,'CHA2 P Clinic',0,13,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (301,0,25,'Wet/Moderate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (302,0,25,'Copious/Large',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (303,1,25,'Scant',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (304,0,25,'Moist/Small',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (305,1,25,'Moist/Small',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (306,1,25,'Wet/Moderate',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (307,1,25,'Copious/Large',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (308,1,34,'Rolled',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (309,0,34,'Calloused/Thickened',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (310,0,37,'Edematous',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (311,0,37,'Bruised',0,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (312,0,37,'Fibrotic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (313,0,37,'Blisters',0,13,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (314,0,37,'Calloused',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (315,0,37,'Fragile',0,14,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (316,0,37,'Psoriasis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (317,1,16,'WBC Scan',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (318,1,8,'Venous/Arterial',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (319,1,3,'CHA2 N Clinic',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (320,1,3,'CHA5 RS Clinic',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (321,1,3,'CHA1 3B Clinic',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (322,1,3,'CHA4 PS Clinic',0,9,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (323,0,3,'Parkgate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (324,1,3,'NS HH Parkgate',0,23,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (325,1,3,'NS HH  West',0,24,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (326,1,3,'NS HH South',0,27,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (327,0,3,'Central Clinic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (328,1,3,'NS HH North',0,29,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (329,1,3,'NS HH Central Clinic',0,28,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (330,1,3,'NS HH West Clinic',0,25,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (331,1,3,'Lions Gate Hospital',0,30,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (332,1,3,'GPC Ward 2',0,30,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (333,1,3,'GPC Ward 3',0,31,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (334,1,3,'GPC Ward 4',0,32,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (335,1,3,'GPC Ward 5',0,33,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (336,1,3,'GPC Ward 6',0,34,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (337,1,3,'Minoru ',0,35,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (340,1,3,'Lions Manor',0,36,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (341,1,3,'Richmond Hospital',0,37,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (342,1,2,'Not Applicable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (343,1,3,'PR HCN Community',0,41,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (344,1,3,'Powell River Hospital',0,39,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (345,1,3,'PR Evergreen',0,43,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (346,1,3,'PR Olive Devaud',0,44,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (347,1,3,'PR HCN Clinic',0,42,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (348,1,3,'Powell River Hospital ACU',0,40,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (349,1,3,'Texada Island',0,45,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (350,0,1,'test_item',0,NULL,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (354,0,1,'test_item',0,NULL,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (355,0,11,'Acute',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (356,0,11,'Community',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (357,0,165,'Dougs Test Item',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (358,0,165,'Dougs Test Item2',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (359,0,165,'Dougs Test Item3',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (362,0,13,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (365,0,16,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (366,0,1,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (372,0,16,'q',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (377,0,39,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (378,0,10,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (379,1,12,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (380,0,8,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (381,0,7,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (382,0,6,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (383,0,49,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (384,0,48,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (385,0,41,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (386,0,4,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (390,0,43,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (392,0,55,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (393,1,55,'Walk-In',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (394,1,55,'On-Site',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (395,0,55,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (396,0,6,'Other',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (400,0,17,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (401,0,55,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (402,0,46,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (403,0,4,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (404,1,8,'Venous',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (405,1,18,'Pale',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (406,1,18,'Flesh tone',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (407,0,18,'Rubor',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (408,0,18,'Hemosiderin Staining',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (409,0,18,'Cyanotic',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (410,0,18,'Ischemia',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (411,0,18,'Woody/Fibrous',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (414,1,8,'Abscess',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (415,1,8,'Irradiation',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (416,0,8,'Inflammatory Disorder',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (417,1,8,'Skin Disease',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (418,1,9,'2-4 weeks',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (419,1,10,'Chronic Maintenance',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (420,0,11,'Acute',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (421,1,20,'Foot',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (422,1,20,'Up to Ankle',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (423,1,20,'Up to Midcalf',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (424,1,20,'Up to Knee',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (425,1,20,'Up to Groin',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (426,1,20,'No Edema',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (427,1,22,'Present',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (428,1,22,'Diminished',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (429,1,23,'Present',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (430,1,23,'Diminished',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (431,1,32,'Wound Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (432,0,32,'Patient Independent with Care',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (433,0,32,'Pt Selt Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (434,0,32,'Pt Self Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (435,1,32,'Patient Self Care',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (436,1,34,'Diffuse',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (437,1,34,'Demarcated',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (438,0,34,'Epithelialization',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (439,1,34,'Callused',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (440,1,34,'Scarred',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (441,1,34,'Epithelializing',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (442,0,36,'Grey',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (443,0,37,'Dry',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (444,0,37,'Indurated <2 cm',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (445,0,37,'Indurated >2 cm',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (446,0,37,'Warm',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (447,0,37,'Elevated Warmth',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (448,0,37,'Tape Tear',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (449,0,37,'Excoriated/Denuded',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (450,0,24,'Serous',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (451,0,24,'Sanguineous',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (452,0,24,'Purulent',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (453,0,24,'Other',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (463,0,36,'Serous',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (464,0,36,'Sanguineous',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (465,0,36,'Purulent',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (466,1,36,'Yellow',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (467,1,36,'Brown',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (468,0,24,'Thick',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (469,0,24,'Thin',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (470,0,24,'Watery/Thin',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (471,0,35,'test1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (472,1,35,'Bone',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (473,1,35,'Tendon',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (474,0,35,'Ligament',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (475,0,35,'Joint',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (476,1,35,'Underlying tissue structure(s)',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (477,1,35,'Muscle',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (478,1,35,'Fascia',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (479,1,35,'Adipose',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (480,1,35,'Eschar',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (481,1,35,'Slough',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (482,1,35,'Granulation tissue',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (483,1,35,'Non-granulation tissue',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (484,1,35,'Hypergranulation tissue',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (485,1,35,'Friable',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (486,1,35,'Blister',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (487,1,35,'Hematoma',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (488,0,35,'Non-blanchable(Stage 1)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (489,1,35,'Not visible',0,22,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (490,0,35,'Other',1,22,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (491,1,26,'No',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (492,1,26,'Yes',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (493,0,11,'residential care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (495,0,6,'Smoking',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (496,0,6,'Weight',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (497,0,6,'Immunocompromised',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (498,0,6,'Radiation Therapy',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (499,0,6,'Chemical Dependency',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (500,0,6,'Lifestyle Choices',0,9,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (501,0,6,'Altered Sensory Perception',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (502,0,6,'Moisture',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (503,0,6,'Inactivity',0,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (504,0,6,'Malnutrition',0,14,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (505,0,6,'Friction/Shear',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (506,0,6,'MRSA',0,16,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (507,0,6,'VRE',1,17,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (508,0,41,'Type I Diabetic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (509,0,41,'Type II Diabetic',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (510,1,41,'Type I Diabetes Mellitus',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (511,1,41,'Type II Diabetes Mellitus',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (512,1,44,'Cerebral Palsy',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (513,1,41,'Other Endocrine/Metabolic/Nutritional Disorders',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (514,1,44,'Other Neurological Disorders',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (515,0,45,'Other Psychiatric Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (516,1,45,'Other Psychiatric/Mood Disorders',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (517,1,46,'Other Respitory Disorders',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (518,1,47,'Other Sensory Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (519,1,48,'Other Infectious Diseases',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (520,1,49,'Other Medical Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (521,0,43,'Other Muscoloskeletal Disorders',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (522,1,43,'Other Musculoskeletal Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (523,1,42,'Coronary Artery Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (524,0,16,'N/A',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (525,1,49,'Gastrointestinal Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (526,0,49,'Edema',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (527,1,49,'Lymphedema',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (528,1,49,'Renal Failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (529,1,49,'Liver Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (530,0,49,'Urinary Disease',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (531,1,48,'Necrotizing Fasciitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (532,1,48,'Gangrene',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (533,1,48,'Sexually Transmitted Infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (534,1,42,'Congestive Heart Failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (535,1,42,'Peripheral Vascular Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (536,1,42,'Deep Vein Thrombosis',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (537,1,42,'Other Cardiovascular Disease',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (538,1,43,'Hip Fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (539,1,43,'Missing Limb (e.g. Amputation)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (540,1,43,'Pathological Bone Fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (541,1,44,'Alzheimers Disease',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (542,1,44,'Dementia other that Alzheimers Disease',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (543,1,44,'Multiple Sclerosis',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (544,1,44,'Parkinsons Disease',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (545,1,44,'Seizure Disorder',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (546,1,44,'Traumatic Brain Injury',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (547,1,44,'Cerebrovascular Accident (Stroke)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (548,1,44,'Transient Ischemic Attack (TIA)',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (549,1,44,'Hemiplegia/Hemiparesis',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (550,1,44,'Huntingtons Chorea',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (551,1,44,'Amyotrophic Lateral Sclerosis',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (552,1,45,'Anxiety Disorder',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (553,1,45,'Manic Depressive Disorder  (Bipolar)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (554,1,47,'Diabetic Retinopathy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (555,1,47,'Macular Degeneration',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (556,1,48,'UTI (within the last 30 Days)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (557,1,2,'Wound Care Clinician',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (558,0,6,'VRE',0,18,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (559,0,1,'q',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (561,0,165,'Eduards Item',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (562,0,165,'Item 1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (563,0,165,'Item 2',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (564,0,165,'Item 3',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (565,0,6,'VRE',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (566,0,8,'Other',1,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (567,0,1,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (568,0,2,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (573,0,4,'Peace Arch unit',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (574,0,41,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (575,0,42,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (576,0,43,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (577,0,45,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (578,0,48,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (579,0,49,'111',1,19,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (580,0,6,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (581,0,165,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (582,0,7,'Unknown',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (583,0,8,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (584,0,9,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (585,0,10,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (586,0,11,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (587,0,12,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (588,0,13,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (589,0,14,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (590,0,38,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (591,0,39,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (592,0,16,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (593,0,17,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (594,0,18,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (597,1,54,'Title',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (598,0,37,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (599,0,34,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (600,0,32,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (601,0,30,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (602,0,29,'11',0,1,NULL,0,'0','0',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (603,0,26,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (604,0,24,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (201,1,51,'Training Location',0,5,0,0,'684','',200,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (653,0,6,'Braden Risk Factors',0,2,1,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (654,0,6,'Chemical Dependency',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (655,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (656,0,6,'Smoking',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (657,0,6,'Weight',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (658,0,6,'Poor Perfusion',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (659,0,6,'Uncontrolled Blood Sugar',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (660,0,6,'Immumocompromised',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (661,0,6,'Radiation Therapy',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (662,0,6,'Chemical Dependecy',1,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (663,0,6,'Lifestyle Choices',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (664,0,6,'MRSA ',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (665,0,6,'VRE',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (666,0,6,'Braden Risk Factors',0,13,1,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (667,0,6,'Altered Sensory Perception',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (668,0,6,'Moisture',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (669,0,6,'Inactivity',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (670,0,6,'Immobility',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (671,0,6,'Malnutrition',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (672,0,6,'Friction/Shear',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (673,0,165,'Anticoagulants',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (674,0,165,'Vasoconstrictions',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (675,0,165,'Insulin',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (676,0,165,'Oral Hypoglycemics',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (677,0,165,'NSAIDS',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (678,0,165,'Steriods',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (679,0,165,'Gold',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (680,0,165,'Methotrexate',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (681,1,11,'Acute (e-Pro)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (682,0,11,'Community (PDC)',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (683,0,11,'Residential ',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (684,1,11,'Community',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (685,0,8,'Surgical (Secondary Intent',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (686,0,165,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (687,0,8,'Surgical(Secondary Intent)',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (688,1,24,'Serous',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (689,1,24,'Sanguineous',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (690,1,24,'Purulent',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (691,0,24,'Other ',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (692,1,36,'Red',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (693,1,36,'Pink',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (694,1,36,'Clear',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (695,1,36,'Grey',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (697,0,11,'111',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (698,0,49,'New',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (699,0,8,'Inflammatory Disorder',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (700,1,35,'Weepy skin',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (701,1,35,'Superficial, red',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (702,0,6,'Chemical Dependency',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (703,0,6,'Chemical Dependency',1,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (704,0,35,'Not Assessed',0,21,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (705,1,24,'Green',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (706,0,24,'Not Assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (707,0,6,'N/a',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (708,0,6,'Perception',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (709,0,6,'Altered Sensory Perception1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (710,0,14,'1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (714,0,6,'1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (715,0,6,'2',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (716,0,12,'XX',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (717,0,6,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (718,0,6,'test2',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (719,0,6,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (720,0,16,'2323',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (721,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (722,0,6,'Smoking',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (723,0,6,'Weigh',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (724,0,6,'Uncontrolled Blood Sugar',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (725,0,6,'Immunocompromised',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (726,0,6,'Radiation',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (727,0,6,'Chemical Dependency',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (728,0,6,'Lifestyle Choices',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (729,0,6,'Weight',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (730,0,6,'Chemical Depend',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (731,0,6,'Chemical Dependecy',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (732,0,6,'Braden Risk Factore',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (733,0,6,'Altered sensory perception',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (734,0,6,'Braden Risk Factors',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (735,1,6,'Radiation therapy',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (736,0,6,'Chemical Dependency',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (737,0,6,'Moisture',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (738,0,6,'Immobility',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (739,0,6,'Malnutrition',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (740,0,6,'Friction/Shear',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (741,0,165,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (742,0,34,'Not Assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (743,0,25,'Not Assessed',0,6,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (744,0,26,'Not Assessed',0,3,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (745,0,37,'Rash',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (746,1,37,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (747,1,37,'Dry',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (748,1,37,'Rash',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (749,1,37,'Macerated',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (750,0,37,'Erythema < 2cm ',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (751,0,37,'Indurated < 2cm',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (752,1,37,'Indurated 2cm and >',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (753,1,37,'Excoriated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (754,1,37,'Weepy',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (755,1,37,'Elevated Warmth',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (756,1,37,'Denuded',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (757,1,37,'Edema',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (758,1,37,'Boggy',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (759,1,37,'Blister',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (760,1,37,'Tape Tear',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (761,1,37,'Bruised',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (762,0,37,'Not Assessed',0,18,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (763,0,10,' 1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (764,0,6,'New',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (765,0,6,'One more',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (767,0,11,'wqewq',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (768,0,7,'weq',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (769,0,42,'wqewqeqweqwe',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (770,0,165,'Vasoconstrictors',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (771,0,165,'Vasoconstrictors',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (772,1,165,'N/A',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (773,1,165,'Anticoagulants',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (774,1,165,'Insulin',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (775,1,165,'Oral Hypoglycemics',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (776,1,165,'Steroids',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (777,1,165,'NSAIDs',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (778,1,165,'Gold',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (779,1,165,'Immunosuppressants',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (780,1,165,'Vasoconstrictors',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (781,0,8,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (782,0,6,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (783,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (784,0,8,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (785,0,6,'Other',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (786,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (787,0,6,'Chemical Dependecny',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (788,1,32,'Wound Merged',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (789,0,37,'Erythema',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (790,1,6,'Chemical dependency',1,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (791,0,6,'Poor Perfusion',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (792,0,6,'MRSA',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (793,0,6,'VRE',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (794,0,6,'Inactivity',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (795,1,165,'Chemotherapy',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (796,1,1,'Ministry of Children & Family ',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (797,1,1,'First Nations and Inuit Health ',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (798,1,8,'Neuropathic/Diabetic',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (799,1,32,'Wound Split',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (800,1,2,'Self',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (801,1,8,'Burn',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (802,0,8,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (805,1,8,'Surgery (Secondary Intent)',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (810,0,6,'Non-adherence to treatment plan',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (811,1,18,'Red',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (812,0,8,'Abscess',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (813,1,6,'N/A',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (814,1,6,'Smoking',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (815,1,6,'Under weight',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (816,1,6,'Uncontrolled blood sugar',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (817,1,6,'Poor perfusion',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (818,1,6,'Immunocompromised',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (819,1,6,'Uncontrolled pain',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (820,1,6,'Non-adherence to treatment plan',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (821,1,6,'Life style choices',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (822,1,6,'MRSA positive',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (823,1,6,'VRE positive',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (824,0,6,'Braden Risk Factors',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (825,0,6,'Moisture',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (826,0,6,'Immobility',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (827,0,6,'Inactivity',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (828,0,6,'Friction/Shear',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (831,1,49,'Colostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (832,1,49,'Ileostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (833,1,49,'Urostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (852,1,8,'Inflammatory Disease',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (854,1,32,'Surgical Intervention',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (855,1,2,'Other HCPs',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (856,1,37,'Erythema < 2cm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (857,0,37,'Erythema 2cm or >',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (859,1,28,'M-W-F',0,11,0,0,'0','3',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (860,1,29,'M-W-F',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (863,1,37,'Indurated < 2cm',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (864,1,35,'Fully epithelialized',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (874,1,35,'Malignant/fungating tissue',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (875,0,37,'Fragile',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (876,1,32,'Other',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (877,1,35,'Foreign body ',0,21,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (886,1,28,'Q5 days',0,7,0,0,'0','1',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (887,1,29,'Q5 days',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (889,1,37,'Callused',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (890,1,35,'Scab',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (891,0,35,'Burn - Intact Skin ',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (898,1,41,'Morbidly Obese',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (899,0,34,'Hypergranulation',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (900,0,30,'FYI only',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (905,1,12,'SDTI',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (906,0,35,'Suspected Deep Tissue Injury (SDTI)',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (907,0,35,'Approximated (sx only)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (908,0,8,'Surgery (Primary Intent)',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (200,1,50,'Training Area',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1000,1,60,'Incision',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1001,1,60,'Belt line',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1002,1,60,'Skin fold/crease',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1003,1,60,'Bony prominence',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1004,1,60,'Umbilicus',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1005,1,60,'Open Wound',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1006,1,166,'Rod',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1007,1,166,'Stent(s)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1008,1,166,'Catheter',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1009,1,166,'Bridge',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1010,1,148,'Urostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1011,1,148,'Ileostomy',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1012,1,148,'Descending Colostomy',0,14,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1013,1,148,'Sigmoid Colostomy',0,15,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1014,1,148,'Transverse Colostomy',0,13,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1015,1,63,'Nurse',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1016,1,63,'Family',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1017,1,68,'Viewed emptying pouch',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1018,1,77,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1019,1,77,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1020,1,76,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1021,1,75,'Rounded',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1022,1,67,'Belt line',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1023,1,69,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1024,1,73,'Small',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1025,1,72,'Thick',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1026,1,66,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1027,1,70,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1028,1,71,'Small',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1029,1,61,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1030,1,80,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1031,1,80,'No',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1032,1,81,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1033,1,81,'No',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1034,1,82,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1035,1,82,'No',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1036,1,83,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1037,1,83,'No',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1038,1,84,'Loop',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1039,1,84,'End',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1040,1,84,'Barrel',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1041,1,65,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1042,1,65,'Changed - teaching',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1043,1,65,'Changed - routine',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1044,1,89,'Knowledge deficit',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1045,1,89,'Cognitive impairment',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1046,1,89,'Visual impairment',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1047,1,89,'Hearing impairment',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1048,1,89,'Poor dexterity',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1049,1,89,'Limited mobility',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1050,1,89,'Lack of reliable caregiver assistance',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1051,1,88,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1052,1,88,'No',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1053,1,88,'Unknown',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1054,0,87,'Healing and stablization of ostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1055,0,87,'Independent management by client',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1056,0,87,'Family/support system management',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1057,0,87,'Maintenance of Ostomy by Nurse',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1058,1,85,'Congenital Disorder',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1059,1,85,'Diverticulitis',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1060,1,85,'Extrahepatic Biliary Atresia',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1061,1,85,'Familial Polyposis',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1062,1,85,'Infectious Enteritis',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1063,1,85,'Ischemic Disorders',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1064,1,85,'Crohns Disease',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1065,1,85,'Ulcerative Colitis',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1066,1,85,'Necrotizing Enterocolitis',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1067,1,85,'Obstructive Disorder',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1068,1,85,'Pseudomembraneous Colitis',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1069,1,85,'Cancer',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1070,1,85,'Radiation Enteritis',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1071,1,85,'Trauma',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1072,1,85,'Abscesses/fistulae',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1073,1,85,'Interstitial Cystitis',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1074,1,85,'Neurogenic Bladder',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1075,1,85,'Spinal Cord Injury',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1076,1,85,'Other',1,20,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1077,1,64,'No voiced concerns',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1078,1,64,'Able to discuss concerns',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1079,1,64,'Hesitant to discuss concerns',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1080,1,64,'Other',1,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1081,1,86,'Heal wound',0,1,0,0,'0','A',0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1082,1,86,'Maintain wound',0,3,0,0,'0','A',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1083,1,2,'test_item',0,NULL,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1085,1,90,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1086,1,90,'Serous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1087,1,90,'Sanguinous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1088,1,90,'Green',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1089,1,90,'Purulent',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1090,1,91,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1091,1,91,'Indurated 2cm or >',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1092,1,91,'Erythema 2cm or >',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1093,1,91,'Edematous',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1094,1,91,'Blisters',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1095,1,92,'Approximated',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1096,1,62,'Flat',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1097,1,93,'Jackson Pratt drain',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1098,0,93,'Synder',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1099,1,94,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1100,1,94,'Indurated < 2cm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1101,1,94,'Indurated 2cm or >',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1102,1,94,'Erythema < 2cm',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1103,1,95,'Serous',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1104,1,95,'Sanguinous',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1105,1,95,'Green',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1106,1,95,'Purulent',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1107,1,95,'Bile',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1108,1,95,'Clear',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1109,1,95,'Cloudy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1110,1,95,'Mucousy',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1111,1,96,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1112,1,96,'Malodourous',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1113,1,97,'0',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1114,1,97,'1',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1115,1,97,'2',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1116,1,97,'3',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1117,1,97,'4',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1118,1,97,'5',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1119,1,97,'6',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1120,1,97,'7',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1121,1,97,'8',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1122,1,97,'9',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1123,1,97,'10',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1124,1,98,'intermittent',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1125,1,98,'Constant',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1126,1,99,'Sutures',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1127,1,100,'Insitu',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1128,1,101,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1129,1,102,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1130,1,104,'<b>1. Completely Limited:</b><br>Unresponsive (does not moan, flinch or grasp) to painful stimuli, due to diminished level of consciousness or sedation, OR Limited ability to feel pain over most of body surface',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1131,1,104,'<b>2. Very Limited:</b><br>Responds only to painful stimuli.  Cannot communicate discomfort except by moaning or restlessness, OR Has a sensory impairment which limits the ability to feel pain or discomfort over 1/2 of body.',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1132,1,105,'<b>1. Constantly Moist:</b><br>Skin is kept moist almost constantly by perspiration, urine, etc.  Dampness is detected every time patient is moved or turned.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1133,1,106,'<b>1.Bedfast:</b><br>Confined to bed.  ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1134,1,107,'<b>1. Completely Immobile:</b><br>Does not make even slight changes in body or extremity position without assistance.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1135,1,108,'<b>1.Very Poor:</b><br>Never eats a complete meal.  Rarely eats more than 1/3 of any food offered.  Eats 2 servings or less of protein (meat or dairy products) per day.  Takes fluids poorly.  Does not take a liquid dietary supplement, OR Is NPO and/or maintained on clear liquids or IVs for more than 5 days.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1136,1,109,'<b>1.Problem:</b><br>Requires moderate to maximum assistance in moving.  Complete lifting without sliding against sheets is impossible.  Frequently slides down in bed or chair, requiring frequent repositioning with maximum assistance.  Spasticity contractures or agitation leads to almost constant friction.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1157,1,84,'Muscous fistula',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1158,1,78,'Round',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1159,1,78,'Oval',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1160,1,78,'Oblong',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1161,1,78,'Irregular',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1162,1,110,'Leg a/k',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1163,1,110,'Leg b/k',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1164,1,110,'Foot partial',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1165,1,110,'Foot all',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1166,1,110,'Great toe',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1167,1,110,'Second toe',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1168,1,110,'Third toe',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1169,1,110,'Fourth toe',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1170,1,110,'Fifth toe',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1171,1,111,'Pain at rest',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1172,1,111,'Pain at night',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1173,1,111,'Decrease with elevation',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1174,1,111,'Increase with elevation',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1175,1,111,'Ache',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1176,1,111,'Knife-like',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1177,1,111,'Continuous',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1178,1,111,'Intermittent',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1179,1,111,'Intermittent claudication',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1180,1,111,'With deep palpation',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1181,1,111,'Non verbal response',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1182,1,112,'Dry/flakey',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1183,1,112,'Varicosities',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1184,1,112,'Hemosiderin staining',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1185,1,112,'Woody/fibrous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1186,1,112,'Stasis dermatitis',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1187,1,112,'Atrophie blanche',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1188,1,112,'Cellulitis',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1189,1,112,'Hairless/fragile/shiny',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1190,1,112,'Blanching on elevation',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1191,1,113,'Numbness',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1192,1,113,'Burning',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1193,1,113,'Tingling',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1194,1,113,'Crawling',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1195,1,113,'Continuous',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1196,1,113,'Intermittent',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1197,1,114,'Intact',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1198,1,114,'Impaired',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1199,1,115,'None noted',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1200,1,115,'Bunion(s)',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1201,1,115,'Callus(es)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1202,1,115,'Corn(s)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1203,1,115,'Plantar wart(s)',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1204,1,115,'Hammertoe(s)',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1205,1,115,'Dropped MTH(s)',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1206,1,115,'Crossed toes',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1207,0,116,'None noted',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1208,0,116,'Incorrect nail length',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1209,0,116,'Ingrown nail(s)',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1210,0,116,'Involuted nail(s)',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1211,1,117,'Normal',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1212,1,117,'Nails too short',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1213,1,117,'Ingrown nail(s)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1214,1,117,'Involuted nail(s)',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1215,1,117,'Thickened nail(s)',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1216,1,116,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1217,1,116,'Dry',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1218,1,116,'Cracks/fissures',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1219,1,116,'Rash',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1220,1,116,'Shiny',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1221,1,116,'Waxy',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1222,1,116,'Weepy',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1223,1,116,'Blisters',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1224,1,116,'Inflamed',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1225,1,118,'None',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1226,1,118,'Partial',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1227,1,119,'Steady',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1228,1,119,'Unsteady',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1229,1,120,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1230,1,120,'Impaired',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1231,1,121,'None',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1232,1,121,'Crutches',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1233,1,121,'Brace',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1234,1,121,'Cane',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1235,1,121,'Prosthesis',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1236,1,121,'Splint',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1237,1,121,'Walker - no wheels',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1238,1,121,'Walker - 2 wheels',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1239,1,121,'Walker - 4 wheels',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1240,1,122,'High',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1241,1,122,'Normal',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1242,1,122,'Low',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1243,1,122,'Flaccid',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1244,1,123,'High',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1245,1,123,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1246,1,123,'Flat',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1247,1,124,'Normal ROM',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1248,1,124,'Normal strength',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1249,1,124,'Decrd ROM',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1250,1,124,'Decrd strength',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1251,1,126,'1st digit',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1252,1,126,'3rd digit',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1253,1,126,'5th digit',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1254,1,126,'1st MTH',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1255,1,126,'3rd MTH',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1256,1,126,'5th MTH',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1257,1,126,'Medial',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1258,1,126,'Heel',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1259,1,126,'Dorsum',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1260,1,128,'>60mm/hg',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1261,1,128,'>40mm/hg',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1262,1,128,'>30mm/hg',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1263,1,128,'>20mm/hg',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1264,1,128,'>50mm/hg',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1265,1,6,'Advanced age 85 or >',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1266,1,6,'Over weight',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1267,1,6,'Muscle atrophy',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1268,1,6,'Bone deformities/contractures',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1269,1,6,'Posture/positioning preferences',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1270,1,6,'Poor transferring skills',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1271,1,148,'Ascending Colostomy',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1272,1,37,'Erythema 2cm or >',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1273,1,8,'Pressure Ulcer - Stage 2',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1274,1,8,'Pressure Ulcer - Stage 3',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1275,1,8,'Pressure Ulcer - Stage 4',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1276,1,8,'Pressure Ulcer - Stage X',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1277,1,104,'<b>3. Slightly Limited:</b><br>Responds to verbal commands, but cannot always communicate discomfort or need to be turned, OR Has some sensory impairment which limits ability to feel pain or discomfort in 1 or 2 extremities.',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1278,1,105,'<b>2. Moist:</b><br>Skin is often, but not always moist.  Linen must be changed at least once a shift.',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1279,1,104,'<b>4. No Impairment:</b><br>Responds to verbal commands.  Has no sensory deficit which would limit ability to feel or voice pain or discomfort',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1280,1,105,'<b>3. Occasionally Moist:</b><br>Skin is occasionally moist, requiring an extra linen change approximately once a day.',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1281,1,105,'<b>4. Rarely Moist:</b><br>Skin is usually dry, linen requires changing only at routine intervals.',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1282,1,106,'<b>2.Chairfast:</b><br>Ability to walk severely limited or non-existent.  Cannot bear own weight and/or must be assisted into chair or wheelchair.',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1283,1,106,'<b>3. Walks Occasionally:</b><br>Walks occasionally during day, but for very short distances, with or without assistance.  Spends majority of each shift in bed or chair.',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1284,1,106,'<b>4.Walks Frequently:</b><br>Walks outside the room at least twice a day and inside room at least once every 2 hours during waking hours.',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1285,1,107,'<b>2.Very Limited:</b><br>Makes occasional slight changes in body or extremity position but unable to make frequent or significant changes independently.',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1286,1,107,'<b>3.Slightly Limited:</b><br>Makes frequent though slight changes in body or extremity position independently.',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1287,1,107,'<b>4. No Limitations:</b><br>Makes major and frequent changes in position without assistance.',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1288,1,108,'<b>2. Probably Inadequate:</b><br>Rarely eats a complete meal and generally eats only about 1/2 of any food offered.  Protein intake includes only 3 servings of meat or dairy products per day.  Occasionally will take a dietary supplement. OR Receives less than optimum amount of liquid diet or tube feeding.',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1289,1,108,'<b>3.Adequate:</b><br>Eats over half of most meals.  Eats a total of four servings of protein (meat, dairy products) each day.  Occasionally will refuse a meal, but will usually take a supplement if offered.  OR is on tube feeding or TPN regimen, which probably meets most of nutritional needs.',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1290,1,108,'<b>4.Excellent:</b><br>Eats most of every meal.  Never refuses a meal.  Usually eats a total of four or more servings of meat and dairy products.  Occasionally eats between meals.  Does not require supplementation.',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1291,1,109,'<b>2.Potential Problem:</b><br>Moves feebly or requires minimum assistance.  During a move, skin probably slides to some extent against sheets, chair, restraints or other devices.  Maintains relatively good position in chair or bed most of the time, but occasionally slides down.',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1292,1,109,'<b>3.No Apparent Problem:</b><br>Moves in bed and in chair independently and has sufficient muscle strength to lift up completely during move.  Maintains good position in bed or chair at all times.',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1293,1,91,'Rash',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1294,1,112,'Mottled',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1295,1,100,'Removed',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1296,1,37,'Fragile',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1298,0,35,'Suspected Deep Tissue Injury SDTI',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1299,0,35,'Fully epithialized',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1300,0,35,'Intact burn area',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1302,1,8,'Suspected Deep Tissue Injury SDTI',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1303,1,34,'Hypergranulation',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1304,1,24,'Other',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1305,0,32,'Surgical Intervention',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1306,0,32,'Other',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1309,1,111,'No pain',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1310,1,112,'Normal/healthy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1311,1,112,'Dependent rubor',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1312,1,112,'Moist/waxy',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1313,1,18,'Bluish-purple',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1314,1,18,'Black',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1315,1,22,'Triphasic',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1316,1,22,'Biphasic',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1317,1,22,'Monophasic',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1318,1,121,'Scooter',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1319,1,110,'No amputations',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1320,1,21,'0 None',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1321,1,21,'+ Homans Sign',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1322,1,21,'+ Stemmers Sign',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1323,1,92,'Tenuous',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1324,1,99,'Staples',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1325,1,99,'Retention Sutures',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1326,1,99,'Steri-strips',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1327,1,99,'Suriglue',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1328,1,101,'Scant ',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1329,1,101,'Small ',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1330,1,101,'Moderate',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1331,1,101,'Large',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1332,1,68,'Viewed pouch change',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1333,0,68,'Viewed appliance change',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1334,1,68,'Participated w emptying pouch',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1335,1,68,'Participated w pouch change',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1336,0,68,'Participated w appliance change',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1337,1,68,'Independent w emptying pouch',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1338,1,68,'Independent w pouch change',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1339,1,66,'Amber',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1340,1,66,'Yellow',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1341,1,66,'Pale yellow',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1342,1,70,'Cloudy',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1343,1,70,'Mucousy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1344,1,71,'Moderate',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1345,1,71,'Large',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1346,1,71,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1347,1,61,'Bile',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1348,1,61,'Brown',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1349,1,61,'Yellow',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1350,1,72,'Mushy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1351,1,72,'Watery',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1352,1,73,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1353,1,73,'Moderate',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1354,1,73,'Large',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1355,1,74,'Red',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1356,1,89,'Language barrier',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1357,1,93,'Hemovac drain',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1358,1,93,'Penrose drain',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1359,1,93,'Chest tube',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1360,1,93,'Nephrostomy tube',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1361,0,93,'PEG ',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1362,1,94,'Erythema 2 cm or >',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1363,0,94,'Edematous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1364,1,95,'Yellow',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1365,1,102,'Eroded',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1366,1,102,'Hypergranulated',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1367,1,134,'Genito-urinary',0,4,0,0,'681',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1368,1,135,'Post-op drainage',0,1,0,0,'681',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1369,1,136,'Loop',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1370,1,136,'End',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1371,1,136,'Barrel',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1372,1,137,'Clear',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1373,1,137,'Small',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1374,1,137,'Mucousy',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1375,1,138,'Dissolvable sutures',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1376,1,138,'Removable sutures',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1377,1,139,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1378,1,139,'Rash',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1379,1,139,'Weepy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1387,1,143,'Not applicable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1388,1,143,'Capped/Clamped',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1389,1,144,'Drain removed as ordered',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1390,1,146,'Patient independent with ostomy care',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1391,1,146,'Patient & caregiver independent with ostomy care',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1392,1,136,'Unmatured ',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1393,1,138,'Approximated',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1394,1,138,'Tenuous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1395,1,138,'Separated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1396,1,137,'Moderate',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1397,1,137,'Large',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1399,1,65,'Changed - leaked outside the flange',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1400,1,65,'Changed - accidentially removed',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1401,1,72,'Gas',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1402,1,75,'Flabby',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1403,1,75,'Distended',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1404,1,75,'Loose/wrinkly',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1405,1,75,'Soft',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1406,1,75,'Hard',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1407,1,75,'Hernia',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1408,0,75,'Flat',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1409,1,74,'Pink',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1410,1,74,'Dusky',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1411,1,74,'Pale',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1412,1,74,'Black',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1413,1,74,'Moist',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1414,1,74,'Edematous',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1415,1,74,'Friable',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1416,1,74,'Slough',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1417,1,74,'Trauma',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1418,1,138,'Suture granuloma ',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1419,1,70,'Odourous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1420,1,89,'Limited visiability of stoma by patient',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1421,1,134,'Laceration closure',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1422,1,135,'Obstruction',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1423,1,146,'Caregiver independent with ostomy care',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1424,1,87,'Permanent ostomy ',0,7,0,0,'0','O',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1425,1,87,'Temporary ostomy ',0,6,0,0,'0','O',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1426,1,115,'Acute Charcot',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1427,1,115,'Chronic Charcot',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1428,1,117,'Yellow coloured',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1429,1,117,'Fungal infection',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1430,1,121,'Wheelchair',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1431,0,124,'ROM normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1432,1,125,'Decrd ROM ',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1433,1,126,'Lateral',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1434,1,128,'20mm/hg or<',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1435,0,23,'Triphasic',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1436,0,23,'Biphasic',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1437,0,23,'Monophasic',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1438,1,127,'Done by WCC/designate',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1439,1,127,'Done in Vascular Lab',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1464,1,138,'Fistula',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1465,1,139,'Boggy',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1466,1,139,'Erythemic',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1467,1,139,'Indurated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1468,1,139,'Excoriated',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1469,1,139,'Macerated',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1470,1,67,'Bony prominence',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1471,1,67,'Fold/crease',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1472,1,67,'Incision',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1473,1,67,'Open wound',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1474,1,67,'Scar',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1475,1,67,'Umbilicus',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1476,1,62,'Raised',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1477,1,62,'Prolapsed',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1478,1,62,'Os off-centre',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1479,1,62,'Retracted',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1480,1,62,'Stenosed',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1481,1,62,'Os titled',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1482,1,62,'Os flush',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1483,1,65,'Changed - test/procedure',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1484,1,72,'Dry/hard',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1485,1,72,'Soft/formed',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1486,1,147,'NPO',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1487,1,147,'TPN',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1488,1,147,'Clear fluids',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1489,1,147,'Full fluids',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1490,1,147,'Soft diet',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1491,1,147,'Low residue',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1492,1,147,'Regular diet',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1493,0,68,'Independent w appliance change',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1494,1,69,'Bruised',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1495,1,69,'Excoriated',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1496,1,69,'Boggy',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1497,1,69,'Trauma',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1498,1,69,'Erythemic',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1499,1,69,'Indurated',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1500,1,69,'Macerated',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1501,1,69,'Weepy',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1502,1,69,'Denuded',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1503,1,69,'Mucosal transplanting',0,11,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1504,1,69,'Pseudo-verrucous lesions',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1505,1,69,'Contact dermatits',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1506,1,69,'Folliculitis',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1507,1,69,'Fungal rash',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1508,1,69,'Psoriasis',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1509,1,69,'Allergic reaction',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1510,1,69,'Eczema',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1511,1,69,'Pyoderma gangrenosum',0,19,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1512,1,69,'Caput Medusae',0,20,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1513,1,89,'No limitations',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1514,1,90,'Not assessed',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1515,1,92,'Gaping',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1516,1,101,'Not assessed',0,6,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1517,1,92,'Not assessed',0,4,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1518,1,99,'Not assessed',0,6,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1519,1,100,'Not assessed',0,3,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1520,1,68,'Viewed flange change',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1521,1,68,'Participated w flange change',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1522,1,68,'Independent w flange change',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1523,0,68,'Teaching booklet given',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1524,1,68,'Viewed teaching video',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1525,0,68,'Skin problems literature provided',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1526,0,68,'General info literature provided',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1527,0,68,'Change instructions given',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1528,0,68,'ET literature provided',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1529,0,68,'List of ostomy suppliers provided',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1530,0,68,'UOA visitor requested ',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1531,1,68,'UOA visitor ',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1532,1,149,'Anastomosis',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1533,1,149,'Re-siting',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1534,1,149,'Revision',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1535,1,67,'Drain ',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1536,1,70,'Bloody',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1537,1,74,'Turgor good',0,11,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1538,1,74,'Turgor poor',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1539,1,148,'Jejunostomy',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1540,1,148,'Cecostomy',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1541,1,166,'Not applicable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1542,1,67,'N/A',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1543,1,61,'Not assessed',0,5,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1544,1,72,'Not assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1545,1,70,'Concentrated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1546,1,150,'Closed (healed) ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1547,1,150,'Patient self care',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1548,1,150,'Revision',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1549,0,152,'Pain controlled',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1550,1,152,'Afebrile',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1551,1,152,'Lungs clear',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1552,1,152,'No calf pain',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1553,1,152,'Drinking well',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1554,1,152,'Eating well',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1555,1,152,'Bowels moving',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1556,1,152,'Voiding well',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1557,1,95,'Odourous',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1558,1,95,'Not applicable',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1561,1,95,'Not Assessed',0,12,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1565,1,144,'Drain accidentially pulled out',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1566,1,144,'Drain fell out',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1567,1,24,'Not assessed',0,7,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1568,1,25,'Not assessed',0,6,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1569,0,26,'Not assessed',0,3,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1570,1,34,'Not assessed',0,9,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1571,1,35,'Not assessed',0,23,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1572,1,37,'Not assessed',0,15,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1573,1,152,'Not assessed',0,10,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1574,1,91,'Indurated < 2cm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1575,1,91,'Erythema < 2cm',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1576,1,137,'Not assessed',0,3,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1577,1,138,'Not assessed',0,10,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1578,1,139,'Not assessed',0,9,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1579,1,66,'Not assessed',0,5,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1580,1,70,'Not assessed',0,7,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1581,1,71,'Not assessed',0,5,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1582,1,73,'Not assessed',0,5,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1583,1,74,'Not assessed',0,13,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1584,1,75,'Not assessed',0,8,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1585,1,68,'Not assessed',0,17,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1586,1,69,'Not assessed',0,22,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1587,1,147,'Not assessed',0,11,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1588,1,85,'Ileus',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1591,0,150,'Fully epithialized',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1592,0,150,'Fully ',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1593,0,150,'To be closed with 2nd intention',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1594,0,150,'Closed with primary intention',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1595,1,66,'Red',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1596,1,70,'Clear',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1597,1,61,'Bloody',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1598,1,72,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1599,0,150,'Self care',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1600,0,136,'Not Applicable',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1601,1,75,'Pendulous',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1602,1,168,'Temporary tube/drain',0,4,0,0,'681','D',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1603,1,169,'Permanent tube/drain',0,5,0,0,'681','D',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1604,1,89,'Lack of bathroom facilities',0,11,0,0,'681','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1605,1,165,'Diuretics',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1606,1,165,'Multivitamins',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1607,1,165,'Herbal supplements',0,13,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1610,1,118,'Full',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1611,1,125,'Normal ROM',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1612,1,125,'Normal strength ',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1613,1,125,'Decrd strength ',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1614,1,85,'Perforation',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1615,1,134,'Orthopedic - Knee',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1616,1,134,'Orthopedic - Other',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1617,1,134,'Cardiovascular',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1618,1,134,'Gastrointestinal',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1619,1,93,'Urethral catheter',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1620,1,93,'Suprapubic catheter',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1621,0,93,'Percutaneous tube',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1622,1,93,'Peritoneal Dialysis(PD) catheter',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1623,1,93,'Gastrostomy tube (G-tube; PEG)',0,13,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1624,1,93,'Jejeunostomy tube (J-tube)',0,14,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1625,1,93,'Biliary tube',0,14,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1626,1,68,'Viewed stoma',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1627,1,8,'Skin Tear',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1628,1,134,'Oncology - Other',0,7,0,0,'681',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1629,1,134,'Neuro',0,11,0,0,'681',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1630,1,134,'Orthopedic - Hip',0,9,0,0,'681',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1631,1,134,'Plastics/Skin/Wound',0,12,0,0,'681','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1632,1,134,'Oncology - Breast',0,6,0,0,'681','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1633,1,134,'Oncology - Prostate',0,6,0,0,'681',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1634,1,134,'Trauma',0,13,0,0,'681',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1635,1,135,'Retention',0,3,0,0,'681','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1636,1,148,'Ileal Conduit',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1637,1,148,'Neobladder',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1638,1,148,'Continent pouch - urinary',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1639,1,148,'Continent pouch - fecal',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1640,1,35,'No open wound',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1641,1,35,'New tissue damage ',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1642,1,152,'Not applicable',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1643,1,152,'Concerns noted - see Progress Notes',0,11,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1644,1,92,'Fully epithelialized',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1645,1,99,'Not applicable',0,1,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1646,1,94,'Rash',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1647,1,94,'Not assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1648,1,94,'Elevated warmth',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1649,1,136,'Unknown',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1650,1,138,'Fully epithelialized',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1651,1,28,'M-Th',0,12,0,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1652,1,28,'Tu-F',0,13,0,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1653,1,29,'M-Th',0,12,0,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1654,1,29,'Tu-F',0,13,0,0,'0','2',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1655,1,67,'Difficult placement',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1656,1,135,'Inability to swallow',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1661,0,150,'test',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1662,1,34,'Not applicable',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1663,1,94,'Fragile',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1664,1,94,'Macerated',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1665,1,94,'Excoriated',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1668,1,113,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1669,1,117,'Nails too long',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1670,1,74,'Dark red',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1671,1,1,'Pharmacare Coverage - 100% (ostomy)',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1672,1,89,'Limited financial means',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1673,1,8,'Fistula - stomatized',0,19,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1674,1,8,'Fistula - non stomatized',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1678,1,8,'Infectious',0,14,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1679,1,34,'Eroding',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1681,0,65,'Changed - leaked within the flange',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1684,1,167,'Heal incision',0,2,0,0,'0','T',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1685,0,93,'Synder drain',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1686,1,69,'Inflammatory process',0,21,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1687,1,144,'Patient/Client self care',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1688,1,93,'PleurX catheter',0,17,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1689,1,138,'Not visible/obscured',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1690,1,49,'Fecal Incontinence',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1691,1,49,'Urinary Incontinence',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1692,1,67,'Peristomal skin protruding',0,15,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1693,1,67,'Peristomal skin retracting',0,16,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1698,1,147,'NG feeding',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1699,1,147,'NJ feeding',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1700,1,147,'Gastrostomy feeding',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1701,1,91,'Not assessed',0,9,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1702,1,75,'Flat',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1703,1,93,'Seton drain',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1704,1,155,'Open/Nursing',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1705,1,155,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1706,1,154,'Open',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1707,1,154,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1708,1,153,'Open',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1709,1,153,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1710,1,155,'Open/Client Self Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1711,1,134,'Gynecology',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1712,1,38,'No Growth',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1714,1,150,'Gaping(heal by 2nd intent)',0,3,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1718,1,93,'Trans Esophgeal Prosthesis',0,18,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1719,1,144,'Drain/tube removed by Dr.',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1720,1,93,'Percutaneous tube',0,16,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1721,1,149,'Ostomy not required',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1722,1,93,'Mace tube',0,19,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1723,1,85,'Incontinence',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1724,1,85,'Infection',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1725,1,87,'To be detemined - ostomy',0,8,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1726,1,147,'Ileostomy Diet',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1727,1,68,'Sexual concerns discussed',0,13,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1728,1,68,'Colostomy irrigations',0,14,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1729,1,62,'Os centered',0,7,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1730,1,67,'Stoma flush ',0,11,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1731,1,67,'Stoma retracted',0,11,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1732,1,67,'Os tilted',0,13,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1733,1,67,'Os flush',0,14,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1734,1,62,'Not assessed',0,11,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1735,1,67,'Not assessed',0,17,0,1,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1736,1,74,'Cancerous lesion(s)',0,14,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1737,1,69,'Cancerous lesion(s)',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1738,1,69,'Peristiomal hernia',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1747,1,93,'Nasogastric tube',0,12,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1748,1,93,'Gastrojejunal (GJ ) tube',0,13,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1749,1,135,'Nutrition suppport',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1750,1,148,'J-Pouch Stage 1',0,9,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1751,1,148,'J-Pouch Stage 2',0,10,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1752,1,148,'J-Pouch Stage 3',0,11,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1753,1,149,'J-Pouch takedown',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1754,1,68,'J-Pouch bowel training',0,15,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1755,1,68,'J-Pouch perianal skin maintenence',0,16,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1756,1,32,'Lost contact with patient',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1757,1,150,'Lost contact with patient',0,5,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1758,1,144,'Lost contact with patient',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1759,1,149,'Lost contact with patient',0,6,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1770,0,30,'PAN Referral 1-7 days',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1771,1,140,'Other',1,99,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (2907,169,30,'Deceased',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (2908,169,30,'Transferred - Non Pix Site',1,99,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (2909,169,30,'Move out of Region',1,99,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3200,1,0,'No Treatment Designation',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,1);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3201,1,169,'Deceased',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3202,1,169,'Moved out of Region',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3203,1,169,'Transferred - Non Pix Site',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3204,1,170,'America/Vancouver',0,1,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3205,1,170,'America/Dawson_Creek',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3206,1,172,'Burn: Etiology 1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3207,1,173,'Burn: Goal 1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3208,1,157,'Pink - Light Red',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3209,1,157,'Dark Red',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3210,1,157,'White',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3211,1,157,'Yellow/Tan',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3212,1,157,'Brown/Black',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3213,1,157,'Blisters',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3214,1,157,'Eschar/Leathery',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3215,1,157,'Loose Tissue',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3216,1,157,'Granulation Buds',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3217,1,157,'Friable',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3218,1,157,'Healed',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3219,1,157,'Not Assessed',0,12,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3220,1,158,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3221,1,158,'Serous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3222,1,158,'Purulent',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3223,1,158,'Sanguineous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3224,1,158,'Dried',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3225,1,158,'Not Assessed',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3226,1,159,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3227,1,159,'Scant',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3228,1,159,'Moist/Small',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3229,1,159,'Wet/Moderate',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3230,1,159,'Copious/Large',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3231,1,159,'Not Assessed',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3232,1,160,'Tie-Over/Occlusive',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3233,1,160,'Open/Sheet/Mesh',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3234,1,160,'Fluid Pockets',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3235,1,160,'White/Pink',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3236,1,160,'Dusky/Purple',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3237,1,160,'Open Areas',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3238,1,160,'Crusted',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3239,1,160,'Friable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3240,1,160,'Healed/Dry/Flaking',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3241,1,160,'Not Assessed',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3242,1,161,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3243,1,161,'Clean/Moist',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3244,1,161,'Crusted',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3245,1,161,'Excessive Drainage',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3246,1,161,'Open Areas',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3247,1,161,'Healed/Dry/Flaking',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3248,1,161,'Friable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
     INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3255,1,164,'LRN',0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL);
	INSERT INTO lookup (id, active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (3249,1,161,'Not Assessed',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0);



/*!40000 ALTER TABLE lookup ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table nursing_care_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS nursing_care_plan;

CREATE TABLE nursing_care_plan (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT '0',
  wound_id int(11) NOT NULL DEFAULT '0',
  active int(11) NOT NULL DEFAULT '0',
  professional_id int(11) DEFAULT '0',
  user_signature varchar(255) DEFAULT NULL,
  body text,
  time_stamp varchar(255) DEFAULT NULL,
  assessment_id int(11) NOT NULL DEFAULT '0',
  wound_profile_type_id int(11) NOT NULL DEFAULT '0',
  visit_frequency int(11) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  deleted_reason varchar(255) DEFAULT '',
  delete_signature varchar(255) DEFAULT '',
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY id (id),
  KEY wound_id (wound_id),
  KEY professional_id (professional_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT nursing_care_plan_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT nursing_care_plan_ibfk_2 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT nursing_care_plan_ibfk_3 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table nursing_fixes
# ------------------------------------------------------------

DROP TABLE IF EXISTS nursing_fixes;

CREATE TABLE nursing_fixes (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) NOT NULL DEFAULT '0',
  row_id int(11) NOT NULL DEFAULT '0',
  professional_id int(11) NOT NULL DEFAULT '0',
  time_stamp text,
  initials text,
  field text,
  error text,
  delete_reason varchar(255) DEFAULT '',
  PRIMARY KEY (id),
  KEY professional_id (professional_id),
  KEY patient_id (patient_id),
  CONSTRAINT nursing_fixes_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT nursing_fixes_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;



# Dump of table password_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS password_history;

CREATE TABLE password_history (
  id int(11) NOT NULL AUTO_INCREMENT,
  professional_id int(11) DEFAULT NULL,
  password text,
  time_stamp text,
  PRIMARY KEY (id),
  KEY professional_id (professional_id),
  CONSTRAINT password_history_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;




# Dump of table patient_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS patient_accounts;

CREATE TABLE patient_accounts (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT NULL,
  name text,
  lastname text,
  lastname_search text,
  treatment_location_id int(11) DEFAULT NULL,
  phn text,
  outofprovince int(11) NOT NULL DEFAULT '0',
  current_flag int(11) DEFAULT '0',
  account_status int(11) DEFAULT NULL,
  created_by int(11) DEFAULT NULL,
  user_signature text,
  gender int(11) DEFAULT NULL,
  phn2 varchar(255) DEFAULT NULL,
  phn3 varchar(255) DEFAULT NULL,
  patient_residence int(11) DEFAULT NULL,
  addressLine1 varchar(255) DEFAULT '',
  addressLine2 varchar(255) DEFAULT '',
  addressCity varchar(255) DEFAULT '',
  addressProvince varchar(255) DEFAULT '',
  addressPostalCode varchar(255) DEFAULT '',
  dob varchar(255) DEFAULT '',
  time_stamp varchar(255) DEFAULT '',
  allergies text,
  delete_reason int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY created_by (created_by)

) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS diagnostic_investigation;

# Dump of table patient_investigations
# ------------------------------------------------------------
CREATE TABLE diagnostic_investigation (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) NOT NULL,
    active int(1) NOT NULL,
   blood_sugar float DEFAULT NULL,
  blood_sugar_meal varchar(6) default '',
  blood_sugar_date date DEFAULT NULL,
albumin int(11) DEFAULT NULL,
  pre_albumin int(11) DEFAULT NULL,
  albumin_date text,
  prealbumin_date text,
    hga1c int(3),
  professional_id int(11) DEFAULT '0',
  creation_date text,
  last_modified_date text,
  user_signature varchar(50) DEFAULT NULL,
  review_done int(11) DEFAULT '0',
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),KEY patient_id (patient_id),
  KEY professional_id (professional_id),
  CONSTRAINT di_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT di_ibfk_3 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS patient_investigations;

CREATE TABLE patient_investigations (
  id int(11) NOT NULL AUTO_INCREMENT,
  diagnostic_id int(11) DEFAULT NULL,
  investigation_date datetime DEFAULT NULL,
  lookup_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY diagnostic_id (diagnostic_id),
  CONSTRAINT patient_investigations_ibfk_2 FOREIGN KEY (diagnostic_id) REFERENCES diagnostic_investigation (id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;



# Dump of table patient_profile_arrays
# ------------------------------------------------------------

DROP TABLE IF EXISTS patient_profile_arrays;

CREATE TABLE patient_profile_arrays (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_profile_id int(11) DEFAULT '0',
  lookup_id int(11) DEFAULT '0',
  other varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY patient_profile_id (patient_profile_id),
  CONSTRAINT patient_profile_arrays_ibfk_1 FOREIGN KEY (patient_profile_id) REFERENCES patient_profiles (id) ON DELETE CASCADE

) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;




# Dump of table patient_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS patient_profiles;

CREATE TABLE patient_profiles (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT NULL,
  current_flag tinyint(4) NOT NULL DEFAULT '0',
  active int(11) NOT NULL DEFAULT '0',
  offline_flag int(11) DEFAULT NULL,
  treatment_location_id int(11) DEFAULT NULL,
  referral_id int(11) DEFAULT NULL,
  professionals text,

  professional_id int(11) DEFAULT '0',
  surgical_history text,
  
  start_date text,
  last_update text,
  user_signature varchar(50) DEFAULT NULL,
  review_done int(11) DEFAULT '0',
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY patient_id (patient_id),
  KEY professional_id (professional_id),
  CONSTRAINT patient_profiles_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT patient_profiles_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT patient_profiles_ibfk_3 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table patient_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS patient_report;

CREATE TABLE patient_report (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT '0',
  time_stamp varchar(255) DEFAULT '',
  active int(11) DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump of table patients
# ------------------------------------------------------------

DROP TABLE IF EXISTS patients;

CREATE TABLE patients (
  id int(11) NOT NULL AUTO_INCREMENT,
  time_stamp text,
  professional_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table physical_exam
# ------------------------------------------------------------

DROP TABLE IF EXISTS physical_exam;

CREATE TABLE physical_exam (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT NULL,
  active int(11) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  temperature float DEFAULT NULL,
  pulse int(11) DEFAULT NULL,
  resp_rate int(11) DEFAULT NULL,
  blood_pressure varchar(255) DEFAULT NULL,
  height float DEFAULT NULL,
  weight float DEFAULT NULL,
  user_signature varchar(255) DEFAULT NULL,
  last_update datetime DEFAULT NULL,
  professional_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS products;

CREATE TABLE products (
  category int(11) DEFAULT NULL,
  id int(11) NOT NULL AUTO_INCREMENT,
  title text,
  cost float DEFAULT NULL,
  description text,
  company text,
  url text,
  image text,
  active int(11) DEFAULT NULL,
  type text,
  quantity text,
  base_id int(11) DEFAULT NULL,
  all_treatment_locations int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY category (category),
  KEY id (id)
) ENGINE=MyISAM AUTO_INCREMENT=1127 DEFAULT CHARSET=latin1;

LOCK TABLES products WRITE;
/*!40000 ALTER TABLE products DISABLE KEYS */;

INSERT INTO products (category, id, title, cost, description, company, url, image, active, type, quantity, base_id, all_treatment_locations)
VALUES

	(27,419,'Combiderm non-adhesive 13 x 13','4.576',NULL,NULL,NULL,NULL,0,'n/a','1',419,1),
	(52,334,'Mepitel 5cm  x 7.5cm','3.48',NULL,NULL,NULL,NULL,0,'n/a','1',334,1),
	(52,335,'Mepitel 7.5cm  x 10cm','4.52',NULL,NULL,NULL,NULL,0,'n/a','1',335,1),
	(24,336,'Ete 5 x 10','',NULL,NULL,NULL,NULL,0,'n/a','1',336,1),
	(1,337,'Coloplast Prep','',NULL,NULL,NULL,NULL,0,'n/a','1',337,1),
	(63,338,'No Sting Wipe','0.90',NULL,NULL,NULL,NULL,0,'n/a','1',338,1),
	(1,532,'Allkare','',NULL,NULL,NULL,NULL,0,'n/a','1',532,1),
	(24,488,'Ete 10 x 10','',NULL,NULL,NULL,NULL,0,'n/a','1',488,1),
	(27,342,'Tegaderm 6 x 7cm','0.172',NULL,NULL,NULL,NULL,0,'n/a','1',342,1),
	(51,343,'Tegaderm 10cm x 12cm','0.805',NULL,NULL,NULL,NULL,0,'n/a','1',343,1),
	(2,533,'Tegaderm 4.4 x 4.4','',NULL,NULL,NULL,NULL,0,'n/a','1',533,1),
	(3,349,'Duoderm Thin 9.5 x 9.7 cm','',NULL,NULL,NULL,NULL,0,'n/a','1',349,1),
	(50,350,'Tegasorb Regular 7cm x 9cm','3.17',NULL,NULL,NULL,NULL,0,'n/a','1',350,1),
	(3,351,'Tegasorb Thin 7 x 9 oval','',NULL,NULL,NULL,NULL,0,'n/a','1',351,1),
	(4,355,'Duoderm Gel 15 gm','',NULL,NULL,NULL,NULL,0,'n/a','1',355,1),
	(61,357,'Intrasite 8 gm','3.05',NULL,NULL,NULL,NULL,0,'n/a','1',357,1),
	(6,358,'Allevyn 12.5 x 12.5','',NULL,NULL,NULL,NULL,0,'n/a','1',358,1),
	(6,359,'Allevyn 7.5 x 7.5','',NULL,NULL,NULL,NULL,0,'n/a','1',359,1),
	(6,360,'Lyofoam 10 x 10','4.502',NULL,NULL,NULL,NULL,0,'n/a','1',360,1),
	(7,362,'Allevyn 5 x 5','',NULL,NULL,NULL,NULL,0,'n/a','1',362,1),
	(46,363,'Kaltostat 7.5cm x 12cm','4.98',NULL,NULL,NULL,NULL,0,'n/a','1',363,1),
	(46,364,'Kaltostat 2gm rope','6.77',NULL,NULL,NULL,NULL,0,'centimeters','1',364,1),
	(59,365,'Kaltostat 5 x 5 cm','1.845',NULL,NULL,NULL,NULL,0,'n/a','1',365,1),
	(9,542,'Aquacel 2 x 45','3.541',NULL,NULL,NULL,NULL,0,'n/a','1',542,1),
	(9,367,'Aquacel 10 x 10 ','5.230',NULL,NULL,NULL,NULL,0,'n/a','1',367,1),
	(9,368,'Aquacel 5 x 5','2.125',NULL,NULL,NULL,NULL,0,'n/a','1',368,1),
	(33,552,'Blue pads','',NULL,NULL,NULL,NULL,0,'n/a','1',552,1),
	(79,370,'Coban 10cm','2.54',NULL,NULL,NULL,NULL,0,'rolls','1',370,1),
	(33,371,'Tensors 6 in','',NULL,NULL,NULL,NULL,0,'n/a','1',371,1),
	(59,372,'Gelfoam 10cm x 12cm','25.84',NULL,NULL,NULL,NULL,0,'n/a','1',372,1),
	(71,373,'Steri-strips','1.01',NULL,NULL,NULL,NULL,0,'n/a','1',373,1),
	(33,374,'Scissors','',NULL,NULL,NULL,NULL,0,'n/a','1',374,1),
	(33,375,'Gloves','',NULL,NULL,NULL,NULL,0,'n/a','1',375,1),
	(33,376,'Sterile gloves','',NULL,NULL,NULL,NULL,0,'n/a','1',376,1),
	(81,377,'Mesh pants','0.60',NULL,NULL,NULL,NULL,1,'n/a','1',377,1),
	(69,378,'VAC Dressing  Black - small','38.00',NULL,NULL,NULL,NULL,0,'n/a','1',378,1),
	(69,379,'VAC Dressing Black - medium','47.50',NULL,NULL,NULL,NULL,0,'n/a','1',379,1),
	(69,380,'VAC Dressing  Black - large','57.00',NULL,NULL,NULL,NULL,0,'n/a','1',380,1),
	(34,381,'Canister','34.00',NULL,NULL,NULL,NULL,0,'n/a','1',381,1),
	(78,553,'Tensor 15cm','0.85',NULL,NULL,NULL,NULL,1,'rolls','1',553,1),
	(14,383,'Alldress 10 x 10','2.380',NULL,NULL,NULL,NULL,0,'n/a','1',383,1),
	(14,384,'Alldress 15 x 15','2.880',NULL,NULL,NULL,NULL,0,'n/a','1',384,1),
	(70,385,'Medipore 5cm x 7cm','0.10',NULL,NULL,NULL,NULL,0,'n/a','1',385,1),
	(70,386,'Medipore 9cm x 10cm','',NULL,NULL,NULL,NULL,0,'n/a','1',386,1),
	(70,387,'Medipore 10cm x 15cm','0.36',NULL,NULL,NULL,NULL,0,'n/a','1',387,1),
	(70,389,'Medipore 9cm x 25cm','0.64',NULL,NULL,NULL,NULL,0,'n/a','1',389,1),
	(62,394,'Mesalt 5cm x 5cm','0.74',NULL,NULL,NULL,NULL,0,'n/a','1',394,1),
	(62,395,'Mesalt 10cm x 10cm','0.95',NULL,NULL,NULL,NULL,0,'n/a','1',395,1),
	(62,396,'Mesalt 2cm x 100cm','0.03980',NULL,NULL,NULL,NULL,0,'centimeters','1',396,1),
	(16,398,'Carbonet 10 x 10','4.493',NULL,NULL,NULL,NULL,0,'n/a','1',398,1),
	(22,401,'Cavilon','',NULL,NULL,NULL,NULL,0,'n/a','1',401,1),
	(74,402,'Secura/Unisalve Ointment','5.08',NULL,NULL,NULL,NULL,0,'n/a','1',402,1),
	(74,403,'Proshield Ointment','10.00',NULL,NULL,NULL,NULL,0,'n/a','1',403,1),
	(23,404,'Santyl (Rx item)','37.70',NULL,NULL,NULL,NULL,0,'n/a','1',404,1),
	(25,405,'Cotton wrap (Protouch)','',NULL,NULL,NULL,NULL,0,'n/a','1',405,1),
	(25,406,'Coban','',NULL,NULL,NULL,NULL,0,'n/a','1',406,1),
	(55,407,'Surpress','',NULL,NULL,NULL,NULL,0,'n/a','1',407,1),
	(59,408,'Profore','19.98',NULL,NULL,NULL,NULL,0,'n/a','1',408,1),
	(25,409,'Profore lite','20.81',NULL,NULL,NULL,NULL,0,'n/a','1',409,1),
	(25,410,'Profore 4 layer','',NULL,NULL,NULL,NULL,0,'n/a','1',410,1),
	(25,411,'Comprilan','',NULL,NULL,NULL,NULL,0,'n/a','1',411,1),
	(25,412,'Viscopaste 7.5 x 6','7.552',NULL,NULL,NULL,NULL,0,'n/a','1',412,1),
	(27,413,'Combiderm non-adhesive 15 x 25','15.744',NULL,NULL,NULL,NULL,0,'n/a','1',413,1),
	(27,414,'Combiderm non-adhesive 7.5 x 7.5','',NULL,NULL,NULL,NULL,0,'n/a','1',414,1),
	(77,415,'Exudry 23cm x 38cm','7.088',NULL,NULL,NULL,NULL,0,'n/a','1',415,1),
	(27,416,'Exu-dry 15 x 4','12.833',NULL,NULL,NULL,NULL,0,'n/a','1',416,1),
	(27,417,'Combiderm adhesive 10 x 10','',NULL,NULL,NULL,NULL,0,'n/a','1',417,1),
	(27,418,'Combiderm adhesive 13 x 13','',NULL,NULL,NULL,NULL,0,'n/a','1',418,1),
	(27,420,'Combiderm adhesive 15 x 18','',NULL,NULL,NULL,NULL,0,'n/a','1',420,1),
	(27,421,'Classic Supra 9 x 26','',NULL,NULL,NULL,NULL,0,'n/a','1',421,1),
	(27,422,'Classic Ultra 17 x 40','',NULL,NULL,NULL,NULL,0,'n/a','1',422,1),
	(65,424,'Promogran 28 cubic cms','11.904',NULL,NULL,NULL,NULL,0,'n/a','1',424,1),
	(20,566,'actisorb','',NULL,NULL,NULL,NULL,0,'n/a','1',566,1),
	(65,425,'Promogran 123 cubic cms','35.844',NULL,NULL,NULL,NULL,0,'n/a','1',425,1),
	(29,427,'Transpore 1in','0.96',NULL,NULL,NULL,NULL,0,'n/a','1',427,1),
	(29,428,'Transpore 2in','1.92',NULL,NULL,NULL,NULL,0,'n/a','1',428,1),
	(29,429,'Medipore H 4 in','6.98',NULL,NULL,NULL,NULL,0,'n/a','1',429,1),
	(29,430,'Mefix 6 in','',NULL,NULL,NULL,NULL,0,'n/a','1',430,1),
	(71,431,'Mefix 5cm x 100cm','2.81',NULL,NULL,NULL,NULL,0,'centimeters','1',431,1),
	(71,432,'Mefix 10cm x 100cm','4.89',NULL,NULL,NULL,NULL,0,'centimeters','1',432,1),
	(71,434,'Micropore 1.25cm','0.32',NULL,NULL,NULL,NULL,0,'centimetres','1',434,1),
	(71,435,'Micropore 2.5cm','0.67',NULL,NULL,NULL,NULL,0,'centimetres','1',435,1),
	(71,436,'Micropore 5cm','1.35',NULL,NULL,NULL,NULL,0,'centimetres','1',436,1),
	(20,444,'Actisorb Silver 220 6.5 x 9.5','3.023',NULL,NULL,NULL,NULL,0,'n/a','1',444,1),
	(20,445,'Actisorb Silver 220 10.5 x 10.5','2.831',NULL,NULL,NULL,NULL,0,'n/a','1',445,1),
	(40,547,'Mepilex transfer','6.675',NULL,NULL,NULL,NULL,0,'n/a','1',547,1),
	(39,544,'Iodosorb ung 40gm','48.96',NULL,NULL,NULL,NULL,0,'n/a','1',544,1),
	(82,451,'Sterile Normal Saline 15ml','',NULL,NULL,NULL,NULL,1,'n/a','1',451,1),
	(31,452,'Sterile Normal saline 30 ml','',NULL,NULL,NULL,NULL,0,'n/a','1',452,1),
	(31,453,'Sterile Normal saline 250 ml','0.88',NULL,NULL,NULL,NULL,0,'n/a','1',453,1),
	(82,454,'Sterile Normal Saline 100ml','0.92',NULL,NULL,NULL,NULL,1,'n/a','1',454,1),
	(31,455,'Sterile Normal saline 50 ml','',NULL,NULL,NULL,NULL,0,'n/a','1',455,1),
	(31,456,'Sterile water 250 ml','',NULL,NULL,NULL,NULL,0,'n/a','1',456,1),
	(82,457,'Sterile Water 100 ml','1.15',NULL,NULL,NULL,NULL,1,'n/a','1',457,1),
	(31,458,'Sterile water 20 ml','',NULL,NULL,NULL,NULL,0,'n/a','1',458,1),
	(75,459,'Gauze sterile 5cm x 5cm','0.027',NULL,NULL,NULL,NULL,1,'n/a','1',459,1),
	(75,460,'Gauze sterile 10cm x 10cm','0.064',NULL,NULL,NULL,NULL,1,'n/a','1',460,1),
	(75,461,'Gauze non-sterile 5cm x 5cm','0.02',NULL,NULL,NULL,NULL,0,'n/a','1',461,1),
	(75,462,'Gauze non-sterile 10cm x 10cm','0.0104',NULL,NULL,NULL,NULL,0,'n/a','1',462,1),
	(72,463,'Packing - 1.2cm x 76cm','0.89',NULL,NULL,NULL,NULL,1,'centimeters','1',463,1),
	(72,464,'Packing - 0.6cm x 4.6m','',NULL,NULL,NULL,NULL,1,'centimeters','1',464,1),
	(72,465,'Packing -  0.6cm x 76cm','0.89',NULL,NULL,NULL,NULL,1,'centimeters','1',465,1),
	(36,466,'ABD Sterile 5 x 7','0.17',NULL,NULL,NULL,NULL,0,'n/a','1',466,1),
	(36,471,'ABD Sterile 8 x 10','0.22',NULL,NULL,NULL,NULL,0,'n/a','1',471,1),
	(16,551,'Carbonet 10 x 20','7.743',NULL,NULL,NULL,NULL,0,'n/a','1',551,1),
	(16,549,'Carboflex 8 x 15 oval','',NULL,NULL,NULL,NULL,0,'n/a','1',549,1),
	(16,548,'Carboflex 10 x 10','',NULL,NULL,NULL,NULL,0,'n/a','1',548,1),
	(36,481,'Bottles - 4.6 m 1/4 in.','',NULL,NULL,NULL,NULL,0,'n/a','1',481,1),
	(36,482,'Bottles - 4.6 m 1/2 in.','',NULL,NULL,NULL,NULL,0,'n/a','1',482,1),
	(37,483,'Dressing tray ','',NULL,NULL,NULL,NULL,0,'n/a','1',483,1),
	(37,484,'Core dressing tray ','       ',NULL,NULL,NULL,NULL,0,'n/a','1',484,1),
	(37,485,'Sterile gloves','',NULL,NULL,NULL,NULL,0,'n/a','1',485,1),
	(78,486,'Tensor 10cm','0.53',NULL,NULL,NULL,NULL,1,'rolls','1',486,1),
	(37,487,'Blue pads','',NULL,NULL,NULL,NULL,0,'n/a','1',487,1),
	(36,489,'Bottles - 4.6 m 1 in.','',NULL,NULL,NULL,NULL,0,'n/a','1',489,1),
	(1,512,'Skin Prep','0.188',NULL,NULL,NULL,NULL,0,'n/a','1',512,1),
	(51,534,'Opsite - non sterile Flexifix 5cm  x 1000cm','0.1133',NULL,NULL,NULL,NULL,0,'centimeters','1',534,1),
	(50,514,'Tegasorb Regular 10cm  x 12cm','4.65',NULL,NULL,NULL,NULL,0,'n/a','1',514,1),
	(50,515,'Tegasorb Thin 10cm x 12cm ','',NULL,NULL,NULL,NULL,0,'n/a','1',515,1),
	(50,541,'Nuderm 10cm x 10cm','3.2786',NULL,NULL,NULL,NULL,0,'n/a','1',541,1),
	(50,540,'Nuderm 5cm x 5cm','1.52',NULL,NULL,NULL,NULL,0,'n/a','1',540,1),
	(58,543,'Jelonet 10cm  x 10cm','0.75',NULL,NULL,NULL,NULL,0,'n/a','1',543,1),
	(7,520,'Allevyn 10 x 10 cm','',NULL,NULL,NULL,NULL,0,'n/a','1',520,1),
	(7,521,'Allevyn 20 x 20','',NULL,NULL,NULL,NULL,0,'n/a','1',521,1),
	(9,522,'Aquacel 15 x 15 ','11.128',NULL,NULL,NULL,NULL,0,'n/a','1',522,1),
	(33,523,'Dressing trays','1.76',NULL,NULL,NULL,NULL,0,'n/a','1',523,1),
	(20,526,'Acticoat Burn ????? 10 x 10','11.987',NULL,NULL,NULL,NULL,0,'n/a','1',526,1),
	(20,527,'Acticoat Algin  1.9 x 30','14.894',NULL,NULL,NULL,NULL,0,'n/a','1',527,1),
	(39,528,'Iodosorb paste 5 gm','7.65',NULL,NULL,NULL,NULL,0,'n/a','1',528,1),
	(39,529,'Iodosorb ointment 10 gm','',NULL,NULL,NULL,NULL,0,'n/a','1',529,1),
	(31,546,'Sterile water 30 ml','',NULL,NULL,NULL,NULL,0,'n/a','1',546,1),
	(37,531,'Disposable scissors','',NULL,NULL,NULL,NULL,0,'n/a','1',531,1),
	(51,535,'Opsite - non sterile Flexifix 10cm x 1000cm','0.2060',NULL,NULL,NULL,NULL,0,'centimeters','1',535,1),
	(51,536,'Opsite - sterile Flexigrid 6cm x 7cm','0.17',NULL,NULL,NULL,NULL,0,'n/a','1',536,1),
	(51,537,'Opsite - sterile Flexigrid 10cm  x 12cm','0.85',NULL,NULL,NULL,NULL,0,'n/a','1',537,1),
	(2,538,'Opsite 3000 sterile 6 x 8.5 cm','',NULL,NULL,NULL,NULL,0,'n/a','1',538,1),
	(2,539,'Opsite 3000 sterile 10 x 14 cm','',NULL,NULL,NULL,NULL,0,'n/a','1',539,1),
	(31,555,'test','',NULL,NULL,NULL,NULL,0,'n/a','1',555,1),
	(51,559,'Tegaderm 6cm x 7cm','0.17',NULL,NULL,NULL,NULL,0,'n/a','1',559,1),
	(68,556,'Kling 5cm','0.10',NULL,NULL,NULL,NULL,0,'rolls','1',556,1),
	(68,557,'Kling 10cm','0.14',NULL,NULL,NULL,NULL,0,'rolls','1',557,1),
	(36,558,'Kling 6 inches','',NULL,NULL,NULL,NULL,0,'n/a','1',558,1),
	(73,560,'Povidone Iodine Swabstiks','0.17',NULL,NULL,NULL,NULL,0,'n/a','1',560,1),
	(81,561,'Tube Stocking','6.95',NULL,NULL,NULL,NULL,0,'centimetres','1',561,1),
	(77,562,'Super Soakers 9cm x 33cm','',NULL,NULL,NULL,NULL,0,'n/a','1',562,1),
	(33,563,'Sterile Vaseline','',NULL,NULL,NULL,NULL,0,'n/a','1',563,1),
	(43,564,'Sterile Vaseline','',NULL,NULL,NULL,NULL,0,'n/a','1',564,1),
	(74,565,'Vaseline  Sterile ','0.03',NULL,NULL,NULL,NULL,0,'n/a','1',565,1),
	(22,567,'','',NULL,NULL,NULL,NULL,0,'n/a','1',567,1),
	(36,568,'','',NULL,NULL,NULL,NULL,0,'n/a','1',568,1),
	(31,569,'','',NULL,NULL,NULL,NULL,0,'n/a','1',569,1),
	(22,570,'','',NULL,NULL,NULL,NULL,0,'n/a','1',570,1),
	(31,571,'','',NULL,NULL,NULL,NULL,0,'n/a','1',571,1),
	(31,572,'water','',NULL,NULL,NULL,NULL,0,'n/a','1',572,1),
	(2,573,'kjktyryt','',NULL,NULL,NULL,NULL,0,'n/a','1',573,1),
	(27,574,'test product','',NULL,NULL,NULL,NULL,0,'n/a','1',574,1),
	(60,575,'Allkare Wipe','0.18',NULL,NULL,NULL,NULL,0,'n/a','1',575,1),
	(54,576,'Iodosorb Ointment 10 gram tube','13.76',NULL,NULL,NULL,NULL,0,'n/a','1',576,1),
	(54,577,'Iodosorb Paste 5gm pkg','8.60',NULL,NULL,NULL,NULL,0,'n/a','1',577,1),
	(53,578,'Acticoat Burn  10cm x 10cm','12.96',NULL,NULL,NULL,NULL,0,'n/a','1',578,1),
	(53,579,'Acticoat Burn 10cmx20cm','',NULL,NULL,NULL,NULL,0,'n/a','1',579,1),
	(53,580,'Acticoat Absorbent 10cm x 12.5cm','18.15',NULL,NULL,NULL,NULL,0,'n/a','1',580,1),
	(53,581,'Acticoat Absorbent 1.9cm x 30cm','16.68',NULL,NULL,NULL,NULL,0,'n/a','1',581,1),
	(46,582,'Kaltostat 5cm x 5cm','1.99',NULL,NULL,NULL,NULL,0,'n/a','1',582,1),
	(59,583,'','',NULL,NULL,NULL,NULL,0,'n/a','1',583,1),
	(64,584,'Actisorb Silver 220 6.5cm x 9.5cm','3.14',NULL,NULL,NULL,NULL,0,'n/a','1',584,1),
	(64,585,'Actisorb Silver 200 10.5cm x 10.5cm','3.99',NULL,NULL,NULL,NULL,0,'n/a','1',585,1),
	(48,586,'Combiderm Non-adhesive 7.5cm x 7.5cm','3.20',NULL,NULL,NULL,NULL,0,'n/a','1',586,1),
	(48,587,'Combiderm Non-adhesive 13cm x 13cm','5.51',NULL,NULL,NULL,NULL,0,'n/a','1',587,1),
	(48,588,'Combiderm Adhesive 10cm x 10cm','4.73',NULL,NULL,NULL,NULL,0,'n/a','1',588,1),
	(48,589,'Combiderm Adhesive 13cm x 13cm','4.95',NULL,NULL,NULL,NULL,0,'n/a','1',589,1),
	(48,590,'Combiderm Adhesive 15cm x 18cm','11.66',NULL,NULL,NULL,NULL,0,'n/a','1',590,1),
	(55,591,'Profore 4 Layer','23.05',NULL,NULL,NULL,NULL,1,'n/a','1',591,1),
	(55,592,'Profore Lite','22.25',NULL,NULL,NULL,NULL,1,'n/a','1',592,1),
	(56,593,'Comprilan 8cm','',NULL,NULL,NULL,NULL,0,'rolls','1',593,1),
	(56,594,'Comprilan 10cm','',NULL,NULL,NULL,NULL,0,'rolls','1',594,1),
	(57,595,'Viscopaste 8cm x 6m','8.49',NULL,NULL,NULL,NULL,0,'rolls','1',595,1),
	(44,596,'Allevyn Adhesive 10cm x 10cm','',NULL,NULL,NULL,NULL,0,'n/a','1',596,1),
	(44,597,'Allevyn Adhesive 12.5cm x 12.5cm','6.79',NULL,NULL,NULL,NULL,0,'n/a','1',597,1),
	(44,598,'Allevyn Non-adhesive 5cm x 5cm','2.41',NULL,NULL,NULL,NULL,0,'n/a','1',598,1),
	(44,599,'Allevyn Non-adhesive 10cm x 10cm','5.87',NULL,NULL,NULL,NULL,0,'n/a','1',599,1),
	(44,600,'Allevyn Non-adhesive 20cm x 20cm','23.12',NULL,NULL,NULL,NULL,0,'n/a','1',600,1),
	(50,601,'Nuderm 15cm x 15cm','8.10',NULL,NULL,NULL,NULL,0,'n/a','1',601,1),
	(47,602,'Aquacel 5cm x 5cm','2.298',NULL,NULL,NULL,NULL,0,'n/a','1',602,1),
	(47,603,'Aquacel 10cm x 10cm','5.65',NULL,NULL,NULL,NULL,0,'n/a','1',603,1),
	(47,604,'Aquacel 2cm x 45cm','0.287',NULL,NULL,NULL,NULL,0,'centimeters','1',604,1),
	(58,605,'Adaptic 7.5cm x 7.5cm','0.73',NULL,NULL,NULL,NULL,0,'n/a','1',605,1),
	(66,606,'ETE 5cm x 10cm','',NULL,NULL,NULL,NULL,0,'n/a','1',606,1),
	(66,607,'ETE 10cm x 10cm','0.42',NULL,NULL,NULL,NULL,0,'n/a','1',607,1),
	(63,608,'Cavilon Cream','8.30',NULL,NULL,NULL,NULL,0,'n/a','1',608,1),
	(51,609,'Opsite - sterile Flexigrid 15cm x 20cm','2.29',NULL,NULL,NULL,NULL,0,'n/a','1',609,1),
	(44,610,'Tielle Plus 15cm x 20cm ','12.50',NULL,NULL,NULL,NULL,0,'n/a','1',610,1),
	(44,611,'Tielle Plus 11cm x 11xm','5.08',NULL,NULL,NULL,NULL,0,'n/a','1',611,1),
	(49,612,'Alldress 10cm x 10cm','2.29',NULL,NULL,NULL,NULL,0,'n/a','1',612,1),
	(67,613,'Non sterile gloves','',NULL,NULL,NULL,NULL,0,'n/a','1',613,1),
	(67,614,'Sterile gloves','',NULL,NULL,NULL,NULL,0,'n/a','1',614,1),
	(69,615,'VAC Dressing - small','',NULL,NULL,NULL,NULL,0,'n/a','1',615,1),
	(69,616,'Medium','',NULL,NULL,NULL,NULL,0,'n/a','1',616,1),
	(69,617,'Large','',NULL,NULL,NULL,NULL,0,'n/a','1',617,1),
	(72,618,'Packing 1.2cm x 4.5m','',NULL,NULL,NULL,NULL,1,'centimeters','1',618,1),
	(72,619,'Packing 2.5cm x 76cm','0.94',NULL,NULL,NULL,NULL,1,'centimeters','1',619,1),
	(72,620,'Packing - 2.5cm x 4.5m','',NULL,NULL,NULL,NULL,1,'centimeters','1',620,1),
	(75,621,'Gauze Abdominal Pad 12cm x 22cm','0.17',NULL,NULL,NULL,NULL,0,'n/a','1',621,1),
	(75,622,'Gauze Abdominal Pad 20cm x 25cm','0.22',NULL,NULL,NULL,NULL,0,'n/a','1',622,1),
	(82,623,'Dressing Tray - core','',NULL,NULL,NULL,NULL,0,'n/a','1',623,1),
	(82,624,'Dressing Tray','1.58',NULL,NULL,NULL,NULL,0,'n/a','1',624,1),
	(82,625,'Scissors','0.509',NULL,NULL,NULL,NULL,1,'n/a','1',625,1),
	(82,626,'Gloves non sterile pair','0.18',NULL,NULL,NULL,NULL,0,'n/a','1',626,1),
	(82,627,'Gloves sterile pair','0.40',NULL,NULL,NULL,NULL,0,'n/a','1',627,1),
	(82,628,'Probe','',NULL,NULL,NULL,NULL,1,'n/a','1',628,1),
	(82,629,'30cc syringe','0.24',NULL,NULL,NULL,NULL,0,'n/a','1',629,1),
	(82,630,'Irrigation catheter #8','',NULL,NULL,NULL,NULL,0,'n/a','1',630,1),
	(82,631,'Irrigation catheter #10','',NULL,NULL,NULL,NULL,0,'n/a','1',631,1),
	(82,632,'Blue Pads','0.13',NULL,NULL,NULL,NULL,0,'n/a','1',632,1),
	(69,633,'VAC Canister','36.00',NULL,NULL,NULL,NULL,0,'n/a','1',633,1),
	(69,634,'VAC Extension Tubing','',NULL,NULL,NULL,NULL,1,'n/a','1',634,1),
	(44,635,'Mepilex with border 10cm x 10cm','4.79',NULL,NULL,NULL,NULL,0,'n/a','1',635,1),
	(82,636,'Staple Remover','3.43',NULL,NULL,NULL,NULL,1,'n/a','1',636,1),
	(82,637,'Suture Removal Kit','',NULL,NULL,NULL,NULL,1,'n/a','1',637,1),
	(50,648,'Tegasorb Thin 7cm x 9cm','',NULL,NULL,NULL,NULL,0,'n/a','',648,1),
	(53,647,'Acticoat Burn 10cm x 20cm','20.15',NULL,NULL,NULL,NULL,0,'n/a','1',647,1),
	(77,646,'Exudry 38cm x 61cm','13.99',NULL,NULL,NULL,NULL,0,'n/a','1',646,1),
	(82,639,'Tongue Depressor non sterile','0.04',NULL,NULL,NULL,NULL,1,'n/a','1',639,1),
	(82,640,'Tongue Depressor sterile ','0.03',NULL,NULL,NULL,NULL,1,'n/a','1',640,1),
	(58,645,'Jelonet 10cm x 40cm','1.45',NULL,NULL,NULL,NULL,0,'n/a','1',645,1),
	(44,644,'Mepilex without border 15cm x 15cm','11.87',NULL,NULL,NULL,NULL,0,'n/a','1',644,1),
	(44,643,'Mepilex with border 15cm x 15cm','12.96',NULL,NULL,NULL,NULL,0,'n/a','1',643,1),
	(44,641,'Allevyn Adhesive 7.5cm x 7.5cm','2.786',NULL,NULL,NULL,NULL,0,'n/a','1',641,1),
	(82,638,'Q-Tip sterile','',NULL,NULL,NULL,NULL,1,'n/a','1',638,1),
	(58,649,'Jelonet 10cm x 700cm','20.90',NULL,NULL,NULL,NULL,0,'centimeters','1',649,1),
	(61,650,'Intrasite 15gm','4.85',NULL,NULL,NULL,NULL,0,'n/a','1',650,1),
	(84,652,'Silver Nitrate','',NULL,NULL,NULL,NULL,0,'n/a','1',652,1),
	(49,653,'Alldress 15cm x 15cm','2.78',NULL,NULL,NULL,NULL,0,'n/a','1',653,1),
	(77,654,'Classic Ultra','',NULL,NULL,NULL,NULL,0,'n/a','1',654,1),
	(77,655,'Classic Super','',NULL,NULL,NULL,NULL,0,'n/a','1',655,1),
	(82,656,'Sterile Normal Saline 500cc','',NULL,NULL,NULL,NULL,1,'n/a','1',656,1),
	(82,657,'Measuring guide','',NULL,NULL,NULL,NULL,0,'n/a','1',657,1),
	(44,658,'Mepilex without border 10cm x 10cm','',NULL,NULL,NULL,NULL,0,'n/a','1',658,1),
	(77,659,'Exudry 10cm x 15cm','',NULL,NULL,NULL,NULL,0,'n/a','1',659,1),
	(82,660,'Sterile Water 500ml','',NULL,NULL,NULL,NULL,1,'n/a','1',660,1),
	(69,661,'VAC Dressing White','60.00',NULL,NULL,NULL,NULL,0,'n/a','1',661,1),
	(82,665,'Sterile Normal Saline 250ml','',NULL,NULL,NULL,NULL,1,'n/a','1',665,1),
	(77,662,'Super Soakers 17cm x 40cm','',NULL,NULL,NULL,NULL,0,'n/a','1',662,1),
	(68,663,'Kling 6cm','',NULL,NULL,NULL,NULL,0,'rolls','1',663,1),
	(68,664,'Kling 15cm','',NULL,NULL,NULL,NULL,0,'rolls','1',664,1),
	(82,666,'Sterile Water 250ml','',NULL,NULL,NULL,NULL,1,'n/a','1',666,1),
	(74,667,'Secura/Triple Care Cream 40gm','3.47',NULL,NULL,NULL,NULL,0,'n/a','1',667,1),
	(74,668,'Secura/Triple Care EPC','8.46',NULL,NULL,NULL,NULL,0,'n/a','1',668,1),
	(74,669,'Secura/Triple Care Cream 78gm','5.38',NULL,NULL,NULL,NULL,0,'n/a','1',669,1),
	(82,670,'30cc syringe','0.24',NULL,NULL,NULL,NULL,0,'n/a',NULL,670,1),
	(82,671,'Wound Irrigation Tip Catheter','0.81',NULL,NULL,NULL,NULL,0,'n/a',NULL,671,1),
	(51,672,'Opsite - non sterile Flexifix 10cm roll','0.206',NULL,NULL,NULL,NULL,0,'centimeters',NULL,672,1),
	(51,673,'Opsite - non sterile Flexifix 5cm roll','0.1133',NULL,NULL,NULL,NULL,0,'centimeters',NULL,673,1),
	(71,674,'Mefix 10cm roll','4.89',NULL,NULL,NULL,NULL,0,'centimeters',NULL,674,1),
	(71,675,'Mefix 5cm roll','2.81',NULL,NULL,NULL,NULL,0,'centimeters',NULL,675,1),
	(71,676,'Mefix 10cm x 100cm','4.89',NULL,NULL,NULL,NULL,0,'centimeters',NULL,676,1),
	(71,677,'Mefix 5cm x 100cm','2.81',NULL,NULL,NULL,NULL,0,'centimeters',NULL,677,1),
	(51,678,'Opsite - non sterile Flexifix 10cm roll x 1000cm','0.2060',NULL,NULL,NULL,NULL,0,'centimeters',NULL,678,1),
	(51,679,'Opsite - non sterile Flexifix 5cm roll x 1000cm','0.1133',NULL,NULL,NULL,NULL,0,'centimeters',NULL,679,1),
	(51,680,'Opsite - non sterile Flexifix 10cm roll','0.206',NULL,NULL,NULL,NULL,0,'centimeters',NULL,680,1),
	(51,681,'Opsite - non sterile Flexifix 5cm roll','0.1133',NULL,NULL,NULL,NULL,0,'centimeters',NULL,681,1),
	(71,682,'Mefix 10cm roll','0.0489',NULL,NULL,NULL,NULL,0,'centimeters',NULL,682,1),
	(71,683,'Mefix 5cm roll','2.81',NULL,NULL,NULL,NULL,0,'centimeters',NULL,683,1),
	(49,723,'Alldress 10cm x 10cm','2.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,723,1),
	(58,684,'Jelonet 10cm x 700cm','0.3',NULL,NULL,NULL,NULL,0,'centimeters',NULL,684,1),
	(62,685,'Mesalt 2cm x 100cm','0.039',NULL,NULL,NULL,NULL,0,'centimeters',NULL,685,1),
	(51,686,'Opsite - non sterile Flexifix 10cm roll','0.206',NULL,NULL,NULL,NULL,0,'centimeters',NULL,686,1),
	(62,687,'Mesalt 2cm x 100cm','0.039',NULL,NULL,NULL,NULL,0,'centimeters',NULL,687,1),
	(51,688,'Opsite - non sterile Flexifix 5cm roll','0.1133',NULL,NULL,NULL,NULL,0,'centimeters',NULL,688,1),
	(71,689,'Mefix 10cm roll','0.0489',NULL,NULL,NULL,NULL,0,'centimeters',NULL,689,1),
	(47,690,'Aquacel 2cm x 45cm','0.287',NULL,NULL,NULL,NULL,0,'centimeters',NULL,690,1),
	(69,691,'Y connector','',NULL,NULL,NULL,NULL,0,'n/a',NULL,691,1),
	(69,692,'Trac Pad','',NULL,NULL,NULL,NULL,0,'n/a',NULL,692,1),
	(82,693,'30cc syringe','0.24',NULL,NULL,NULL,NULL,0,'n/a',NULL,693,1),
	(69,694,'VAC Trac Pad','',NULL,NULL,NULL,NULL,0,'n/a',NULL,694,1),
	(69,695,'VAC  Y connector','',NULL,NULL,NULL,NULL,0,'n/a',NULL,695,1),
	(69,696,'VAC Y connector','',NULL,NULL,NULL,NULL,0,'n/a',NULL,696,1),
	(70,697,'Mepore 10cm x 15cm','0.36',NULL,NULL,NULL,NULL,0,'n/a',NULL,697,1),
	(70,698,'Mepore 5cm x 7cm','0.10',NULL,NULL,NULL,NULL,1,'n/a',NULL,698,1),
	(70,699,'Mepore 9cm x 10cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,699,1),
	(70,700,'Mepore 9cm x 25cm','0.64',NULL,NULL,NULL,NULL,1,'n/a',NULL,700,1),
	(82,701,'Apron','',NULL,NULL,NULL,NULL,0,'n/a',NULL,701,1),
	(82,702,'Alcohol swab','',NULL,NULL,NULL,NULL,0,'n/a',NULL,702,1),
	(82,703,'Montgomery Straps','',NULL,NULL,NULL,NULL,1,'n/a',NULL,703,1),
	(69,704,'Drape','',NULL,NULL,NULL,NULL,0,'rolls',NULL,704,1),
	(69,705,'VAC Drape','',NULL,NULL,NULL,NULL,0,'n/a',NULL,705,1),
	(70,706,'Medipore 9cm  x  25cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,706,1),
	(70,707,'Medipore 9cm x 15cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,707,1),
	(70,708,'Medipore 9cm x 10cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,708,1),
	(70,709,'Medipore 5cm x 7cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,709,1),
	(70,710,'Medipore 9cm x 10cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,710,1),
	(70,711,'Medipore 9cm x 15cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,711,1),
	(70,712,'Medipore 9cm x 25cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,712,1),
	(82,713,'Apron','',NULL,NULL,NULL,NULL,0,'n/a',NULL,713,1),
	(81,714,'Tube Stocking (Flexinet)','6.95',NULL,NULL,NULL,NULL,1,'rolls',NULL,714,1),
	(71,715,'Mefix 10cm roll','0.0489',NULL,NULL,NULL,NULL,0,'centimeters',NULL,715,1),
	(71,716,'Mefix 5cm roll','2.81',NULL,NULL,NULL,NULL,1,'centimeters',NULL,716,1),
	(77,717,'Super Soakers 17cm x 40cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,717,1),
	(77,718,'Super Soakers 9cm x 33cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,718,1),
	(82,719,'Debridement scalpel','',NULL,NULL,NULL,NULL,0,'n/a',NULL,719,1),
	(51,720,'Opsite - non sterile Flexifix 10cm roll','0.206',NULL,NULL,NULL,NULL,0,'centimeters',NULL,720,1),
	(51,721,'Opsite - non sterile Flexifix 5cm roll','0.1133',NULL,NULL,NULL,NULL,0,'centimeters',NULL,721,1),
	(82,722,'Irrigition Tip Catheter','',NULL,NULL,NULL,NULL,0,'n/a',NULL,722,1),
	(49,724,'Mepilex Lite w border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,724,1),
	(49,725,'Melipex w border 15cm x 15cm','4.93',NULL,NULL,NULL,NULL,0,'n/a',NULL,725,1),
	(49,726,'Mepilex Lite w/o border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,726,1),
	(49,727,'Mepilex Lite w/o border 15cm x 15cm','5.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,727,1),
	(49,728,'Melipex Lite w border 15cm x 15cm','4.93',NULL,NULL,NULL,NULL,0,'n/a',NULL,728,1),
	(49,729,'Melipex Lite with border 15cm x 15cm','4.93',NULL,NULL,NULL,NULL,0,'n/a',NULL,729,1),
	(49,730,'Mepilex Lite with border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,730,1),
	(49,731,'Mepilex Lite without border 15cm x 15cm','5.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,731,1),
	(49,732,'Mepilex Lite without border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,732,1),
	(85,733,'Medical Maggots','14.50',NULL,NULL,NULL,NULL,0,'n/a',NULL,733,1),
	(59,734,'Dacron Chiffon Covering 20x20','14.50',NULL,NULL,NULL,NULL,0,'n/a',NULL,734,1),
	(59,735,'Dacron Chiffon Covering 30cm x 30cm','25.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,735,1),
	(59,736,'Nylon stocking','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,736,1),
	(85,737,'Dacron Chiffon Covering 20x20','14.50',NULL,NULL,NULL,NULL,0,'n/a',NULL,737,1),
	(85,738,'Dacron Chiffon Covering 20cm x 20cm','14.50',NULL,NULL,NULL,NULL,0,'n/a',NULL,738,1),
	(85,739,'Dacron Chiffon Covering 30cm x 30cm','25.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,739,1),
	(85,740,'Medical Maggots','88.88',NULL,NULL,NULL,NULL,0,'n/a',NULL,740,1),
	(85,741,'Medical Maggots','88.88',NULL,NULL,NULL,NULL,0,'n/a',NULL,741,1),
	(85,742,'Medical Maggots','88.88',NULL,NULL,NULL,NULL,0,'n/a',NULL,742,1),
	(70,743,'Medipore+Pad 5cm x 7cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,743,1),
	(70,744,'Medipore+Pad 9cm x 10cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,744,1),
	(70,745,'Medipore+Pad 9cm x 25cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,745,1),
	(70,746,'Medipore+Pad 9cm x 15cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,746,1),
	(59,747,'Acticoat Burn  10cm x 10cm','12.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,578,1),
	(59,748,'Acticoat Burn 10cm x 20cm','19.17',NULL,NULL,NULL,NULL,0,'n/a',NULL,647,1),
	(59,749,'Actisorb Silver 200 10.5cm x 10.5cm','3.99',NULL,NULL,NULL,NULL,0,'n/a',NULL,585,1),
	(59,750,'Actisorb Silver 220 6.5cm x 9.5cm','3.14',NULL,NULL,NULL,NULL,0,'n/a',NULL,584,1),
	(59,751,'Adaptic 7.5cm x 7.5cm','0.73',NULL,NULL,NULL,NULL,0,'n/a',NULL,605,1),
	(82,752,'30cc syringe','0.24',NULL,NULL,NULL,NULL,0,'n/a',NULL,693,1),
	(53,753,'Acticoat Burn  10cm x 10cm','12.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,578,1),
	(53,754,'Acticoat 10cm x 10cm','12.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,578,1),
	(59,755,'Acticoat 10cm x 20cm','19.17',NULL,NULL,NULL,NULL,0,'n/a',NULL,647,1),
	(64,756,'Actisorb Silver 200 10.5cm x 10.5cm','3.99',NULL,NULL,NULL,NULL,0,'n/a',NULL,585,1),
	(64,757,'Actisorb Silver 220 6.5cm x 9.5cm','3.14',NULL,NULL,NULL,NULL,0,'n/a',NULL,584,1),
	(66,758,'Adaptic 7.5cm x 7.5cm','0.73',NULL,NULL,NULL,NULL,0,'n/a',NULL,605,1),
	(66,759,'Adaptic 7.5cm x 20cm','2.07',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(59,760,'Alcohol swab','',NULL,NULL,NULL,NULL,0,'n/a',NULL,702,1),
	(44,761,'Allevyn Plus 12.5cm x 12.5cm','6.24',NULL,NULL,NULL,NULL,0,'n/a',NULL,597,1),
	(44,762,'Allevyn Adhesive 7.5cm x 7.5cm','2.52',NULL,NULL,NULL,NULL,0,'n/a',NULL,641,1),
	(44,763,'Allevyn Non-adhesive 10cm x 10cm','4.68',NULL,NULL,NULL,NULL,0,'n/a',NULL,599,1),
	(44,764,'Allevyn Non-adhesive 20cm x 20cm','17.43',NULL,NULL,NULL,NULL,0,'n/a',NULL,600,1),
	(44,765,'Allevyn Non-adhesive 5cm x 5cm','2.25',NULL,NULL,NULL,NULL,0,'n/a',NULL,598,1),
	(44,766,'Allevyn Plus 12.5cm x 12.5cm','6.24',NULL,NULL,NULL,NULL,0,'n/a',NULL,597,1),
	(44,767,'Allevyn Plus 12.5cm x 22.5cm','11.12',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(44,768,'Allevyn Plus 17.5cm x 17.5cm','12.42',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(59,769,'Apron','',NULL,NULL,NULL,NULL,0,'n/a',NULL,713,1),
	(47,770,'Aquacel 10cm x 10cm','5.76',NULL,NULL,NULL,NULL,0,'n/a',NULL,603,1),
	(47,771,'Aquacel 2cm x 45cm','0.17',NULL,NULL,NULL,NULL,0,'centimeters',NULL,690,1),
	(47,772,'Aquacel 5cm x 5cm','2.24',NULL,NULL,NULL,NULL,0,'n/a',NULL,602,1),
	(82,773,'Blue Pads','0.13',NULL,NULL,NULL,NULL,0,'n/a',NULL,632,1),
	(63,774,'Cavilon Barrier Cream 28gm','2.92',NULL,NULL,NULL,NULL,0,'n/a',NULL,608,1),
	(86,775,'Coban 2','18.48',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(48,776,'Combiderm Adhesive 10cm x 10cm','4.73',NULL,NULL,NULL,NULL,0,'n/a',NULL,588,1),
	(48,777,'Combiderm Adhesive 13cm x 13cm','4.95',NULL,NULL,NULL,NULL,0,'n/a',NULL,589,1),
	(48,778,'Combiderm Adhesive 15cm x 18cm','11.66',NULL,NULL,NULL,NULL,0,'n/a',NULL,590,1),
	(48,779,'Combiderm Non-adhesive 13cm x 13cm','5.51',NULL,NULL,NULL,NULL,1,'n/a',NULL,587,1),
	(48,780,'Combiderm Non-adhesive 7.5cm x 7.5cm','3.20',NULL,NULL,NULL,NULL,0,'n/a',NULL,586,1),
	(56,781,'Comprilan 10cm','8.32',NULL,NULL,NULL,NULL,0,'rolls',NULL,594,1),
	(59,782,'Comprilan 8cm','7.20',NULL,NULL,NULL,NULL,0,'rolls',NULL,593,1),
	(56,783,'Comprilan 8cm','7.20',NULL,NULL,NULL,NULL,0,'rolls',NULL,593,1),
	(85,784,'Maggots - Chiffon Covering 20.5cm x 20.5cm','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,738,1),
	(85,785,'Maggots - Chiffon Covering 30cm x 30cm','25.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,739,1),
	(85,786,'Maggots - Chiffon Dressing 10cm x 10cm','5.50',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(85,787,'Maggots - nylon stocking','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(59,788,'Maggots - Nylon Stocking','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,787,1),
	(59,789,'Maggots - Medical Grade','88.88',NULL,NULL,NULL,NULL,0,'n/a',NULL,742,1),
	(85,790,'Maggots - Medical Grade','88.88',NULL,NULL,NULL,NULL,0,'n/a',NULL,742,1),
	(85,791,'Maggots - Nylon Stocking','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,787,1),
	(59,792,'Debridement scalpel','',NULL,NULL,NULL,NULL,0,'n/a',NULL,719,1),
	(82,793,'Dressing Tray','1.58',NULL,NULL,NULL,NULL,0,'n/a',NULL,624,1),
	(82,794,'Dressing Tray - core','',NULL,NULL,NULL,NULL,0,'n/a',NULL,623,1),
	(77,795,'ETE 10cm x 10cm','0.42',NULL,NULL,NULL,NULL,0,'n/a',NULL,607,1),
	(59,796,'ETE 5cm x 10cm','0.31',NULL,NULL,NULL,NULL,0,'n/a',NULL,606,1),
	(77,797,'Exudry 10cm x 15cm','2.16',NULL,NULL,NULL,NULL,0,'n/a',NULL,659,1),
	(59,798,'Exudry 23cm x 38cm','7.86',NULL,NULL,NULL,NULL,0,'n/a',NULL,415,1),
	(59,799,'Exudry 38cm x 61cm','14.41',NULL,NULL,NULL,NULL,0,'n/a',NULL,646,1),
	(77,800,'Exudry 23cm x 38cm','7.86',NULL,NULL,NULL,NULL,0,'n/a',NULL,415,1),
	(77,801,'Exudry 38cm x 61cm','14.41',NULL,NULL,NULL,NULL,0,'n/a',NULL,646,1),
	(77,802,'Exudry 15cm x 23cm','3.75',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(77,803,'Exudry 38cm x 46cm','12.17',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(59,804,'Gelfoam 8cm x 12.5cm','26.61',NULL,NULL,NULL,NULL,0,'n/a',NULL,372,1),
	(61,805,'Intrasite 15gm','4.37',NULL,NULL,NULL,NULL,0,'n/a',NULL,650,1),
	(61,806,'Intrasite 8 gm','2.68',NULL,NULL,NULL,NULL,0,'n/a',NULL,357,1),
	(61,807,'Intrasite Gel 25gm','6.04',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(61,808,'Intrasite 8gm','2.68',NULL,NULL,NULL,NULL,0,'n/a',NULL,357,1),
	(61,809,'Intrasite Gel 15gm','4.37',NULL,NULL,NULL,NULL,0,'n/a',NULL,650,1),
	(61,810,'Intrasite Gel 8gm','2.68',NULL,NULL,NULL,NULL,0,'n/a',NULL,357,1),
	(54,811,'Iodosorb Ointment 10 gram tube','13.23',NULL,NULL,NULL,NULL,0,'n/a',NULL,576,1),
	(54,812,'Iodosorb Paste 5gm pkg','8.27',NULL,NULL,NULL,NULL,0,'n/a',NULL,577,1),
	(82,813,'Irrigition Tip Catheter','0.78',NULL,NULL,NULL,NULL,0,'n/a',NULL,722,1),
	(66,814,'Jelonet 10cm  x 10cm','0.72',NULL,NULL,NULL,NULL,0,'n/a',NULL,543,1),
	(66,815,'Jelonet 10cm x 40cm','1.45',NULL,NULL,NULL,NULL,0,'n/a',NULL,645,1),
	(59,816,'Jelonet 10cm x 700cm','0.3',NULL,NULL,NULL,NULL,0,'centimeters',NULL,684,1),
	(46,817,'Kaltostat 2gm rope','6.68',NULL,NULL,NULL,NULL,0,'n/a',NULL,364,1),
	(46,818,'Kaltostat 5cm x 5cm','1.97',NULL,NULL,NULL,NULL,1,'n/a',NULL,582,1),
	(46,819,'Kaltostat 7.5cm x 12cm','4.52',NULL,NULL,NULL,NULL,1,'n/a',NULL,363,1),
	(68,820,'Kling 10cm','0.14',NULL,NULL,NULL,NULL,0,'rolls',NULL,557,1),
	(59,821,'Kling 5cm','0.10',NULL,NULL,NULL,NULL,0,'rolls',NULL,556,1),
	(59,822,'Measuring guide','0.06',NULL,NULL,NULL,NULL,0,'n/a',NULL,657,1),
	(70,823,'Medipore+Pad 5cm x 7cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,743,1),
	(70,824,'Medipore+Pad 9cm x 10cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,744,1),
	(71,825,'Mefix 10cm roll','0.0489',NULL,NULL,NULL,NULL,1,'centimeters',NULL,715,1),
	(49,826,'Melipex Lite with border 15cm x 15cm','4.93',NULL,NULL,NULL,NULL,0,'n/a',NULL,729,1),
	(49,827,'Mepilex Lite with border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,730,1),
	(49,828,'Mepilex Lite with border 5cm x 12.5cm','2.09',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(49,829,'Mepilex Lite without border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,732,1),
	(59,830,'Mepilex Lite without border 15cm x 15cm','5.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,731,1),
	(49,831,'Mepilex Lite without border 15cm x 15cm','5.96',NULL,NULL,NULL,NULL,0,'n/a',NULL,731,1),
	(59,832,'Mepilex Transfer 15cm x 20cm','13.35',NULL,NULL,NULL,NULL,0,'n/a',NULL,547,1),
	(87,833,'Mepilex Transfer 15cm x 20cm','13.35',NULL,NULL,NULL,NULL,1,'n/a',NULL,547,1),
	(88,834,'Melipex Lite with border 15cm x 15cm','4.93',NULL,NULL,NULL,NULL,0,'n/a',NULL,729,1),
	(88,835,'Mepilex Lite with border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,1,'n/a',NULL,730,1),
	(88,836,'Mepilex Lite with border 5cm x 12.5cm','2.09',NULL,NULL,NULL,NULL,1,'n/a',NULL,828,1),
	(88,837,'Mepilex Lite without border 15cm x 15cm','5.96',NULL,NULL,NULL,NULL,1,'n/a',NULL,731,1),
	(49,838,'Mepilex with border 10cm x 10cm','3.87',NULL,NULL,NULL,NULL,1,'n/a',NULL,635,1),
	(49,839,'Mepilex with border 15cm x 15cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,643,1),
	(49,840,'Mepilex with border 7.5cm x 7.5cm','2.42',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(49,841,'Mepiex with boder 12.5cm x 12.5 cm','',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(49,842,'Mepilex with border 15cm x 15cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,643,1),
	(49,843,'Mepilex 15cm x 20 cm','12.69',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(59,844,'Mepilex with boder 12.5cm x 12.5 cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,841,1),
	(88,845,'Mepilex Lite with border 15cm x 15cm','4.93',NULL,NULL,NULL,NULL,1,'n/a',NULL,729,1),
	(49,846,'Mepilex  with border 15cm x 20 cm','12.69',NULL,NULL,NULL,NULL,0,'n/a',NULL,843,1),
	(59,847,'Mepilex with border 12.5cm x 12.5 cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,841,1),
	(49,848,'Mepilex with border 15cm x 20 cm','12.69',NULL,NULL,NULL,NULL,1,'n/a',NULL,843,1),
	(49,849,'Mepilex without border 10cm x 10cm','3.79',NULL,NULL,NULL,NULL,1,'n/a',NULL,658,1),
	(49,850,'Mepilex without border 15cm x 15cm','11.87',NULL,NULL,NULL,NULL,1,'n/a',NULL,644,1),
	(52,851,'Mepitel 5cm  x 7.5cm','3.47',NULL,NULL,NULL,NULL,1,'n/a',NULL,334,1),
	(52,852,'Mepitel 7.5cm  x 10cm','4.49',NULL,NULL,NULL,NULL,1,'n/a',NULL,335,1),
	(52,853,'Mepitel 10cm x 18cm','7.40',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(52,854,'Mepitel 20cm x 30cm','21.34',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(70,855,'Mepore 10cm x 15cm','0.36',NULL,NULL,NULL,NULL,0,'n/a',NULL,697,1),
	(62,856,'Mesalt 10cm x 10cm','0.95',NULL,NULL,NULL,NULL,1,'n/a',NULL,395,1),
	(62,857,'Mesalt 2cm x 100cm','0.04',NULL,NULL,NULL,NULL,1,'centimeters',NULL,687,1),
	(62,858,'Mesalt 5cm x 5cm','0.74',NULL,NULL,NULL,NULL,1,'n/a',NULL,394,1),
	(61,859,'Intrasite Conformable Dressing 10cm x20cm','4.74',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(61,860,'Intrasite Conformable 10cm x 20cm','4.74',NULL,NULL,NULL,NULL,0,'n/a',NULL,859,1),
	(71,861,'Micropore 1.25cm','0.32',NULL,NULL,NULL,NULL,1,'rolls',NULL,434,1),
	(71,862,'Micropore 2.5cm','0.67',NULL,NULL,NULL,NULL,1,'rolls',NULL,435,1),
	(71,863,'Micropore 5cm','1.35',NULL,NULL,NULL,NULL,1,'rolls',NULL,436,1),
	(63,864,'Cavilon No Sting Wipe','0.91',NULL,NULL,NULL,NULL,0,'n/a',NULL,338,1),
	(50,865,'Nuderm 10cm x 10cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,541,1),
	(50,866,'Nuderm 5cm x 5cm','1.52',NULL,NULL,NULL,NULL,1,'n/a',NULL,540,1),
	(51,867,'Opsite Flexigrid Transparent 10cm  x 12cm','1.17',NULL,NULL,NULL,NULL,0,'n/a',NULL,537,1),
	(51,868,'Opsite Flexigrid Transparent 15cm x 20cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,609,1),
	(59,869,'OpsiteFlexigrid  Transparent 6cm x 7cm','0.22',NULL,NULL,NULL,NULL,0,'n/a',NULL,536,1),
	(51,870,'Opsite Flexigrid Transparent 12cm x 25cm','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(51,871,'Tegaderm Transparent 5cm x 100cm','1.3',NULL,NULL,NULL,NULL,0,'centimeters',NULL,NULL,1),
	(51,872,'Tegaderm Transparent 10cm x 100cm','0.21',NULL,NULL,NULL,NULL,1,'centimeters',NULL,NULL,1),
	(51,873,'Tegaderm Transparent 5cm x 100cm','0.13',NULL,NULL,NULL,NULL,1,'centimeters',NULL,871,1),
	(73,874,'Povidone Iodine Swabstiks','0.17',NULL,NULL,NULL,NULL,1,'n/a',NULL,560,1),
	(73,875,'Povidone Iodine Wipes','0.06',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(73,876,'Providone Iodine Solution 500ml','4.18',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(73,877,'Povidone Iodine Solution 500ml','0.83',NULL,NULL,NULL,NULL,1,'milliliters',NULL,NULL,1),
	(59,878,'Silver Nitrate Stick','0.9',NULL,NULL,NULL,NULL,0,'n/a',NULL,652,1),
	(84,879,'Silver Nitrate Stick','0.9',NULL,NULL,NULL,NULL,1,'n/a',NULL,652,1),
	(55,880,'Profore Latex Free','21.59',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(65,881,'Promogran 123 cubic cms','32.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,425,1),
	(65,882,'Promogran 28 cubic cms','11.90',NULL,NULL,NULL,NULL,1,'n/a',NULL,424,1),
	(73,883,'Povidone Iodine Solution 500ml','0.84',NULL,NULL,NULL,NULL,1,'milliliters',NULL,876,1),
	(70,884,'Proshield Ointment','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,403,1),
	(74,885,'Secura Protective Cream 40gm','3.23',NULL,NULL,NULL,NULL,1,'n/a',NULL,667,1),
	(74,886,'Secura Protective Cream 78gm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,669,1),
	(74,887,'Secura Extra Protective Cream 90gm','7.67',NULL,NULL,NULL,NULL,1,'n/a',NULL,668,1),
	(74,888,'Secura Ointment','4.70',NULL,NULL,NULL,NULL,1,'n/a',NULL,402,1),
	(89,889,'Hospeco \"Supersoaker\" 10cm x 36cm','0.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(89,890,'Hospeco \"Supersoaker\" 18cm x 43cm','0.38',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(59,891,'Viscopaste 8cm x 6m','8.17',NULL,NULL,NULL,NULL,0,'rolls',NULL,595,1),
	(59,892,'Tegasorb Regular 10cm  x 12cm','3.08',NULL,NULL,NULL,NULL,0,'n/a',NULL,514,1),
	(50,893,'Tegasorb Regular 10cm x 10cm','4.33',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(50,894,'Tegasorb Regular 13cm x 15cm','4.33',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(50,895,'Tegasorb Thin 10cm x 12cm','2.01',NULL,NULL,NULL,NULL,1,'n/a',NULL,515,1),
	(50,896,'Tegasorb Thin 13cm x 15cm','3.31',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(69,897,'VAC Freedom Canister','38.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,633,1),
	(69,898,'VAC  ATS Canister','40.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(69,899,'VAC Canister - Freedom','38.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,633,1),
	(69,900,'VAC Canister - ATS','40.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,898,1),
	(69,901,'VAC Drape','9.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,705,1),
	(69,902,'VAC Dressing  Kit Black - large','63.18',NULL,NULL,NULL,NULL,0,'n/a',NULL,380,1),
	(69,903,'VAC Dressing  Kit Black - small','42.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,378,1),
	(59,904,'VAC Dressing  Kit Black - medium','53.46',NULL,NULL,NULL,NULL,0,'n/a',NULL,379,1),
	(69,905,'VAC Dressing  Kit Black - large','63.18',NULL,NULL,NULL,NULL,0,'n/a',NULL,380,1),
	(69,906,'VAC Dressing  Kit Black - medium','53.46',NULL,NULL,NULL,NULL,0,'n/a',NULL,379,1),
	(69,907,'VAC Dressing  Kit Black - small','42.50',NULL,NULL,NULL,NULL,0,'n/a',NULL,378,1),
	(69,908,'VAC Dressing Kit Black - X large','134.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(69,909,'VAC Dressing Kit - Abdominal','456.30',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(69,910,'VAC GranuFoam Dressing  Kit Black - large','63.18',NULL,NULL,NULL,NULL,1,'n/a',NULL,380,1),
	(69,911,'VAC GranuFoam Dressing Kit Black - medium','53.46',NULL,NULL,NULL,NULL,1,'n/a',NULL,379,1),
	(69,912,'VAC GranuFoam Dressing  Kit Black - small','42.50',NULL,NULL,NULL,NULL,1,'n/a',NULL,378,1),
	(69,913,'VAC GranuFoam Dressing Kit Black - X large','134.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,908,1),
	(69,914,'VAC VersaFoam Dressing Kit White - Large','60.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,661,1),
	(69,915,'VAC VersaFoam Dressing Kit White - Small','46.50',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(69,916,'VAC VersaFoam Dressing Kit White - large','60.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,661,1),
	(69,917,'VAC VersaFoam Dressing Kit White - small','46.50',NULL,NULL,NULL,NULL,1,'n/a',NULL,915,1),
	(69,918,'VAC Transparent Drape','9.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,705,1),
	(69,919,'VAC Trac Pad','27.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,694,1),
	(69,920,'VAC Deadend Cap','4.95',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(69,921,'VAC  Y  Connector','8.55',NULL,NULL,NULL,NULL,1,'n/a',NULL,696,1),
	(69,922,'VAC Rental Machine - ATS','82.80',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(69,923,'VAC Rental Machine - Freedom','82.80',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(69,924,'VAC Machine Rental - ATS','82.80',NULL,NULL,NULL,NULL,1,'n/a',NULL,922,1),
	(69,925,'VAC Machine Rental - Freedom','82.80',NULL,NULL,NULL,NULL,1,'n/a',NULL,923,1),
	(69,926,'VAC Machine Owned - ATS','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(69,927,'VAC Machine Owned - Freedom','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(53,928,'Aquacel Ag 10cm x10cm','9.02',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(53,929,'Aquacel Ag 15cm x15cm','17.71',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(53,930,'Silvercel Rope 2.5cm x 30.5cm','0.40',NULL,NULL,NULL,NULL,1,'centimeters',NULL,NULL,1),
	(53,931,'Tegaderm Mesh 5cm x 5cm','2.21',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(53,932,'Tegaderm Mesh 10cm x 12.7cm','4.53',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(53,933,'Tegaderm Mesh 10cm x 20cm','6.88',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(90,934,'Tegaderm Acrylic 7.9cm x 9.5cm','3.63',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(90,935,'Tegaderm Acrylic 11cm x 12.7cm','5.67',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(90,936,'Tegaderm Acrylic 14.9cm x 15.2cm (square)','9.24',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(90,937,'Tegaderm Acrylic 14.2cm x 15.2 (oval)','8.37',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(60,938,'PDI Remove Wipe','0.03',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(82,939,'Adhesive Remover Wipe','0.03',NULL,NULL,NULL,NULL,0,'n/a',NULL,938,1),
	(82,940,'30cc syringe','0.24',NULL,NULL,NULL,NULL,1,'n/a',NULL,693,1),
	(53,941,'Acticoat 10cm x 10cm','12.29',NULL,NULL,NULL,NULL,1,'n/a',NULL,578,1),
	(53,942,'Acticoat 10cm x 20cm','19.17',NULL,NULL,NULL,NULL,1,'n/a',NULL,647,1),
	(64,943,'Actisorb Silver 200 10.5cm x 10.5cm','3.99',NULL,NULL,NULL,NULL,1,'n/a',NULL,585,1),
	(64,944,'Actisorb Silver 220 6.5cm x 9.5cm','3.14',NULL,NULL,NULL,NULL,1,'n/a',NULL,584,1),
	(58,945,'Adaptic 7.5cm x 20cm','2.07',NULL,NULL,NULL,NULL,0,'n/a',NULL,759,1),
	(58,946,'Adaptic 7.5cm x 7.5cm','0.73',NULL,NULL,NULL,NULL,1,'n/a',NULL,605,1),
	(58,947,'Adaptic 7.5cm x 20cm','2.07',NULL,NULL,NULL,NULL,1,'n/a',NULL,759,1),
	(60,948,'Adhesive Remover Wipe','0.03',NULL,NULL,NULL,NULL,1,'n/a',NULL,938,1),
	(82,949,'Alcohol swab','',NULL,NULL,NULL,NULL,1,'n/a',NULL,702,1),
	(44,950,'Allevyn Adhesive 7.5cm x 7.5cm','2.52',NULL,NULL,NULL,NULL,1,'n/a',NULL,641,1),
	(44,951,'Allevyn Non-adhesive 10cm x 10cm','4.68',NULL,NULL,NULL,NULL,1,'n/a',NULL,599,1),
	(44,952,'Allevyn Non-adhesive 20cm x 20cm','17.43',NULL,NULL,NULL,NULL,1,'n/a',NULL,600,1),
	(44,953,'Allevyn Non-adhesive 5cm x 5cm','2.25',NULL,NULL,NULL,NULL,1,'n/a',NULL,598,1),
	(44,954,'Allevyn Plus 12.5cm x 12.5cm','6.24',NULL,NULL,NULL,NULL,1,'n/a',NULL,597,1),
	(44,955,'Allevyn Plus 17.5cm x 17.5cm','12.42',NULL,NULL,NULL,NULL,1,'n/a',NULL,768,1),
	(82,956,'Apron','',NULL,NULL,NULL,NULL,1,'n/a',NULL,713,1),
	(59,957,'Aquacel 10cm x 10cm','5.76',NULL,NULL,NULL,NULL,0,'n/a',NULL,603,1),
	(47,958,'Aquacel 2cm x 45cm','0.17',NULL,NULL,NULL,NULL,1,'centimeters',NULL,690,1),
	(47,959,'Aquacel 10cm x 10cm','5.76',NULL,NULL,NULL,NULL,1,'n/a',NULL,603,1),
	(47,960,'Aquacel 5cm x 5cm','2.24',NULL,NULL,NULL,NULL,1,'n/a',NULL,602,1),
	(53,961,'Aquacel Ag 10cm x10cm','9.02',NULL,NULL,NULL,NULL,1,'n/a',NULL,928,1),
	(53,962,'Aquacel Ag 15cm x15cm','17.71',NULL,NULL,NULL,NULL,1,'n/a',NULL,929,1),
	(82,963,'Blue Pads','0.13',NULL,NULL,NULL,NULL,1,'n/a',NULL,632,1),
	(63,964,'Cavilon Barrier Cream 28gm','2.92',NULL,NULL,NULL,NULL,1,'n/a',NULL,608,1),
	(63,965,'Cavilon No Sting Wipe','0.91',NULL,NULL,NULL,NULL,1,'n/a',NULL,338,1),
	(79,966,'Coban 10cm','2.54',NULL,NULL,NULL,NULL,1,'rolls',NULL,370,1),
	(86,967,'Coban2','18.48',NULL,NULL,NULL,NULL,1,'rolls',NULL,775,1),
	(48,968,'Combiderm Adhesive 10cm x 10cm','4.73',NULL,NULL,NULL,NULL,1,'n/a',NULL,588,1),
	(48,969,'Combiderm Adhesive 13cm x 13cm','4.95',NULL,NULL,NULL,NULL,1,'n/a',NULL,589,1),
	(48,970,'Combiderm Adhesive 15cm x 18cm','11.66',NULL,NULL,NULL,NULL,1,'n/a',NULL,590,1),
	(48,971,'Combiderm Non-adhesive 7.5cm x 7.5cm','3.20',NULL,NULL,NULL,NULL,1,'n/a',NULL,586,1),
	(56,972,'Comprilan 10cm','8.32',NULL,NULL,NULL,NULL,1,'rolls',NULL,594,1),
	(56,973,'Comprilan 8cm','7.20',NULL,NULL,NULL,NULL,1,'rolls',NULL,593,1),
	(82,974,'Debridement scalpel','',NULL,NULL,NULL,NULL,1,'n/a',NULL,719,1),
	(82,975,'Dressing Tray','1.58',NULL,NULL,NULL,NULL,1,'n/a',NULL,624,1),
	(82,976,'Dressing Tray - core','1.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,623,1),
	(77,977,'ETE 10cm x 10cm','0.42',NULL,NULL,NULL,NULL,1,'n/a',NULL,607,1),
	(77,978,'ETE 5cm x 10cm','0.31',NULL,NULL,NULL,NULL,1,'n/a',NULL,606,1),
	(77,979,'Exudry 10cm x 15cm','2.16',NULL,NULL,NULL,NULL,1,'n/a',NULL,659,1),
	(77,980,'Exudry 15cm x 23cm','3.75',NULL,NULL,NULL,NULL,1,'n/a',NULL,802,1),
	(77,981,'Exudry 23cm x 38cm','7.86',NULL,NULL,NULL,NULL,1,'n/a',NULL,415,1),
	(77,982,'Exudry 38cm x 46cm','12.17',NULL,NULL,NULL,NULL,1,'rolls',NULL,803,1),
	(77,983,'Exudry 38cm x 61cm','14.41',NULL,NULL,NULL,NULL,1,'n/a',NULL,646,1),
	(75,984,'Gauze Abdominal Pad 12cm x 22cm','0.17',NULL,NULL,NULL,NULL,1,'n/a',NULL,621,1),
	(75,985,'Gauze Abdominal Pad 20cm x 25cm','0.22',NULL,NULL,NULL,NULL,1,'n/a',NULL,622,1),
	(75,986,'Gauze non-sterile 10cm x 10cm','0.0104',NULL,NULL,NULL,NULL,1,'n/a',NULL,462,1),
	(75,987,'Gauze non-sterile 5cm x 5cm','0.02',NULL,NULL,NULL,NULL,1,'n/a',NULL,461,1),
	(59,988,'Gelfoam 8cm x 12.5cm','26.61',NULL,NULL,NULL,NULL,1,'n/a',NULL,372,1),
	(82,989,'Gloves non sterile pair','0.18',NULL,NULL,NULL,NULL,1,'n/a',NULL,626,1),
	(82,990,'Gloves sterile pair','0.40',NULL,NULL,NULL,NULL,1,'n/a',NULL,627,1),
	(89,991,'Hospeco','0.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,889,1),
	(89,992,'Hospeco \"Supersoaker\" 10cm x 36cm','0.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(89,993,'Hospeco','0.38',NULL,NULL,NULL,NULL,0,'n/a',NULL,890,1),
	(89,994,'Hospeco Supersoaker 18cm x 43cm','0.38',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(89,995,'Hospeco Supersoaker 10cm x 36cm','0.29',NULL,NULL,NULL,NULL,0,'n/a',NULL,992,1),
	(61,996,'Intrasite Conformable 10cm x 20cm','4.74',NULL,NULL,NULL,NULL,1,'n/a',NULL,859,1),
	(61,997,'Intrasite Gel 15gm','4.37',NULL,NULL,NULL,NULL,1,'n/a',NULL,650,1),
	(61,998,'Intrasite Gel 25gm','6.04',NULL,NULL,NULL,NULL,1,'n/a',NULL,807,1),
	(61,999,'Intrasite Gel 8gm','2.68',NULL,NULL,NULL,NULL,1,'n/a',NULL,357,1),
	(54,1000,'Iodosorb Ointment 10 gram tube','13.23',NULL,NULL,NULL,NULL,1,'n/a',NULL,576,1),
	(54,1001,'Iodosorb Paste 5gm pkg','8.27',NULL,NULL,NULL,NULL,1,'n/a',NULL,577,1),
	(82,1002,'Irrigition Tip Catheter','0.78',NULL,NULL,NULL,NULL,1,'n/a',NULL,722,1),
	(58,1003,'Jelonet 10cm  x 10cm','0.72',NULL,NULL,NULL,NULL,1,'n/a',NULL,543,1),
	(58,1004,'Jelonet 10cm x 40cm','1.45',NULL,NULL,NULL,NULL,1,'n/a',NULL,645,1),
	(58,1005,'Jelonet 10cm x 700cm','0.3',NULL,NULL,NULL,NULL,1,'centimeters',NULL,684,1),
	(46,1006,'Kaltostat 2gm rope','6.68',NULL,NULL,NULL,NULL,1,'n/a',NULL,364,1),
	(68,1007,'Kling 10cm','0.14',NULL,NULL,NULL,NULL,0,'rolls',NULL,557,1),
	(59,1008,'Kling 15cm','',NULL,NULL,NULL,NULL,0,'rolls',NULL,664,1),
	(59,1009,'Kling 5cm','0.10',NULL,NULL,NULL,NULL,0,'rolls',NULL,556,1),
	(85,1010,'Maggots - Chiffon Covering 20.5cm x 20.5cm','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,738,1),
	(85,1011,'Maggots - Chiffon Covering 30cm x 30cm','25.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,739,1),
	(85,1012,'Maggots - Chiffon Dressing 10cm x 10cm','5.50',NULL,NULL,NULL,NULL,1,'n/a',NULL,786,1),
	(59,1013,'Maggots - Medical Grade','88.88',NULL,NULL,NULL,NULL,0,'n/a',NULL,742,1),
	(85,1014,'Maggots - Nylon Stocking','10.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,787,1),
	(85,1015,'Maggots - Medical Grade','88.88',NULL,NULL,NULL,NULL,1,'n/a',NULL,742,1),
	(85,1016,'Maggots - Nylon Stocking','10.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,787,1),
	(68,1017,'Kling 10cm','0.14',NULL,NULL,NULL,NULL,0,'rolls',NULL,557,1),
	(85,1018,'Kling 15cm','',NULL,NULL,NULL,NULL,0,'rolls',NULL,664,1),
	(68,1019,'Kling 10cm','0.14',NULL,NULL,NULL,NULL,1,'rolls',NULL,557,1),
	(68,1020,'Kling 15cm','',NULL,NULL,NULL,NULL,1,'rolls',NULL,664,1),
	(68,1021,'Kling 5cm','0.10',NULL,NULL,NULL,NULL,1,'rolls',NULL,556,1),
	(82,1022,'Measuring guide','0.06',NULL,NULL,NULL,NULL,1,'n/a',NULL,657,1),
	(70,1023,'Medipore+Pad 5cm x 7cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,743,1),
	(57,1024,'Viscopaste 8cm x 6m','8.17',NULL,NULL,NULL,NULL,1,'rolls',NULL,595,1),
	(51,1025,'Opsite Flexigrid Transparent 10cm  x 12cm','1.17',NULL,NULL,NULL,NULL,1,'n/a',NULL,537,1),
	(51,1026,'Opsite Flexigrid Transparent 12cm x 25cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,870,1),
	(51,1027,'Opsite Flexigrid Transparent 15cm x 20cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,609,1),
	(51,1028,'OpsiteFlexigrid  Transparent 6cm x 7cm','0.22',NULL,NULL,NULL,NULL,1,'n/a',NULL,536,1),
	(50,1029,'Tegasorb Regular 10cm x 10cm','4.33',NULL,NULL,NULL,NULL,1,'n/a',NULL,893,1),
	(49,1030,'Mepilex with border 12.5cm x 12.5 cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,841,1),
	(50,1031,'Tegasorb Regular 10cm  x 12cm','3.08',NULL,NULL,NULL,NULL,1,'n/a',NULL,514,1),
	(88,1032,'Mepilex Lite without border 10cm x 10cm','2.96',NULL,NULL,NULL,NULL,1,'n/a',NULL,732,1),
	(74,1033,'Proshield Ointment','10.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,403,1),
	(82,1034,'Steri-strips','1.01',NULL,NULL,NULL,NULL,1,'n/a',NULL,373,1),
	(82,1035,'Vaseline  Sterile','0.03',NULL,NULL,NULL,NULL,1,'n/a',NULL,565,1),
	(72,1036,'Drain Sponge 7.5cm x 7.5cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1037,'Ostomy 2-Piece Flat w open end pouch - clip','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1038,'Ostomy 1-Piece Flat with open end pouch - clip','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1039,'Ostomy 1-Piece Flat with open end pouch - lock&roll','',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(91,1040,'Ostomy 1-Piece Convex with open end pouch - clip','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1041,'Ostomy 1-Piece Flat with closed pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1042,'Ostomy 1-Piece Convex with open end pouch - Lock&Roll','',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(91,1043,'Ostomy 1-Piece Convex with closed pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(59,1044,'Ostomy 2-Piece Flat with open end pouch - Lock&Roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1045,'Ostomy 2-Piece Flat with open end pouch - Lock&Roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1044,1),
	(91,1046,'Ostomy 2-Piece Flat with closed pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1047,'Ostomy 2-Piece Convex with open end pouch - clip','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1048,'Ostomy 2-Piece Convex with open end pouch Lock&Roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1049,'Ostomy 2-Piece Convex with closed pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1050,'Ostomy 1-Piece Flat with open end pouch - Lock&Roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1039,1),
	(91,1051,'Ostomy 2-Piece Convex with open end pouch - Lock&Roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1048,1),
	(91,1052,'Ostomy 1-Piece Convex with open end pouch - lock&roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1042,1),
	(91,1053,'Ostomy 1-Piece Flat with open end pouch - lock&roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1039,1),
	(91,1054,'Ostomy 2-Piece Convex with open end pouch - lock&roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1048,1),
	(91,1055,'Ostomy 2-Piece Flat with open end pouch - lock&roll','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1044,1),
	(91,1056,'Ostomy Mycostatin Powder','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1057,'Ostomy Stomahesive Powder','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(59,1058,'Ostomy Stomahesive Paste','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1059,'Ostomy Cavilon No Sting Wipe','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1060,'Ostomy Cavilon No Sting Spray','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1061,'Ostomy - Eakin Seal','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1062,'Ostomy Coloplast Strips','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1063,'Ostomy - Eakin Seal','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1061,1),
	(91,1064,'Ostomy Eakin Seal','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1061,1),
	(91,1065,'Ostomy Stomahesive Paste','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1058,1),
	(85,1066,'Maggots - Chiffon Dressing 60cm x 60cm','58.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(85,1067,'Maggots - Chiffon Covering 20.5cm x 20.5cm','19.00',NULL,NULL,NULL,NULL,0,'n/a',NULL,738,1),
	(85,1068,'Maggots - Chiffon Covering 30cm x 30cm','19.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,739,1),
	(85,1069,'Maggots - Chiffon Covering 20cm x 20cm','10.00',NULL,NULL,NULL,NULL,1,'n/a',NULL,738,1),
	(92,1070,'Urinary Drainage Bag','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(92,1071,'Urinary Catheter Kit','2.47',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(92,1072,'Urinary Catheter Kit','2.47',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(92,1073,'Urinary Foley Catheter (regular)','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(92,1074,'Urinary Foley Catheter (silastic)','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(92,1075,'Urinary Foley Catheter (silicone)','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(92,1076,'Urinary Leg Bag','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(92,1077,'Urinary Straight Catheter','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(92,1078,'Urinary Catheter Kit','2.47',NULL,NULL,NULL,NULL,1,'n/a',NULL,1072,1),
	(91,1079,'Ostomy - I Piece (Flat) with Urostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1080,'Ostomy - I Piece Flat with urostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1079,1),
	(91,1081,'Ostomy 2-Piece Flat with urostomy pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1082,'Ostomy 1 Piece convec with urosostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1083,'Ostomy 2 Piece convex with urostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,NULL,1),
	(91,1084,'Ostomy 1-Piece Convex with urosostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1082,1),
	(91,1085,'Ostomy  I - Piece Flat with urostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1079,1),
	(91,1086,'Ostomy  I-Piece Flat with urostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1079,1),
	(91,1087,'Ostomy  1-Piece Flat with urostomy pouch','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1079,1),
	(91,1088,'Ostomy 1-Piece Flat with urostomy pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1079,1),
	(91,1089,'Ostomy 2-Piece Flat with open end pouch - clip','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1037,1),
	(91,1090,'Ostomy 2-Piece Convex with urostomy pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1083,1),
	(82,1091,'Chlorhexidine 0.05% solution','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(91,1092,'Ostomy paste - strip','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1062,1),
	(59,1093,'Ostomy paste - tube','',NULL,NULL,NULL,NULL,0,'n/a',NULL,1058,1),
	(91,1094,'Ostomy Barrier Paste - ring','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1061,1),
	(91,1095,'Ostomy Barrier Paste - strip','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1062,1),
	(91,1096,'Ostomy Barrier Paste - tube','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1058,1),
	(91,1097,'Ostomy 1-Piece Convex with open end pouch - self-closure','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1042,1),
	(91,1098,'Ostomy 1-Piece Convex with urostomy pouch','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1082,1),
	(91,1099,'Ostomy 1-Piece Flat with open end pouch - self closure','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1039,1),
	(91,1100,'Ostomy 2-Piece Convex with open end pouch - self closure','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1048,1),
	(91,1101,'Ostomy 2-Piece Flat with open end pouch - self closure','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1044,1),
	(91,1102,'Ostomy  Powder - Mycostaton','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1056,1),
	(91,1103,'Ostomy Powder - Stomahesive','',NULL,NULL,NULL,NULL,1,'n/a',NULL,1057,1),
	(91,1104,'Ostomy Wound Manager','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(89,1105,'Supersoaker 18cm x 43cm','0.38',NULL,NULL,NULL,NULL,1,'n/a',NULL,994,1),
	(89,1106,'Supersoaker 10cm x 36cm','0.29',NULL,NULL,NULL,NULL,1,'n/a',NULL,992,1),
	(82,1107,'Sterile Normal Saline 60ml','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(82,1108,'Sterile Water 60mls','',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(59,1109,'Sterile Water 60ml','',NULL,NULL,NULL,NULL,1,'rolls',NULL,1108,1),
	(82,1110,'10cc syringe','',NULL,NULL,NULL,NULL,1,'rolls',NULL,NULL,1),
	(82,1111,'Chlorhexidine 0.05% swab stick','',NULL,NULL,NULL,NULL,1,'rolls',NULL,NULL,1),
	(70,1112,'Mepore Island Dressing 9cm x 15cm','0.36',NULL,NULL,NULL,NULL,0,'rolls',NULL,NULL,1),
	(59,1113,'Mepore Island Dressing 9cm x 15cm','0.36',NULL,NULL,NULL,NULL,0,'centimeters',NULL,1112,1),
	(70,1114,'Mepore Island Dressing 9cm x 20cm','0.55',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(70,1115,'Mepore Island Dressing 9cm x 15cm','0.36',NULL,NULL,NULL,NULL,1,'centimeters',NULL,1112,1),
	(70,1116,'Mepore Island Dressing 9cm x 30cm','',NULL,NULL,NULL,NULL,1,'rolls',NULL,NULL,1),
	(91,1117,'Ostomy Irrigation Sleeve','',NULL,NULL,NULL,NULL,1,'rolls',NULL,NULL,1),
	(91,1118,'Ostomy Powder - Mycostatin','',NULL,NULL,NULL,NULL,1,'rolls',NULL,NULL,1),
	(92,1119,'Urinary Foley Catheter (latex-free)','',NULL,NULL,NULL,NULL,1,'rolls',NULL,NULL,1),
	(75,1120,'Gauze Drain Sponge 3cmx3cm','',NULL,NULL,NULL,NULL,1,'n/a',NULL,NULL,1),
	(93,1121,'testCategory','1','test',NULL,NULL,NULL,NULL,'1','1',NULL,1),
	(71,1122,'billnye','232123$',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,1),
	(72,1123,'Risperidone','ewrwer',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,1),
	(93,1124,'testCategory','1','test',NULL,NULL,NULL,NULL,'1','1',NULL,1),
	(93,1125,'testCategory','1','test',NULL,NULL,NULL,NULL,'1','1',NULL,1),
	(59,1126,'abc','1',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,1);

/*!40000 ALTER TABLE products ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products_by_facility
# ------------------------------------------------------------

DROP TABLE IF EXISTS products_by_facility;

CREATE TABLE products_by_facility (
  id int(11) NOT NULL AUTO_INCREMENT,
  product_id int(11) DEFAULT NULL,
  treatment_location_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table products_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS products_categories;

CREATE TABLE products_categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  title text,
  active int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;


LOCK TABLES products_categories WRITE;
/*!40000 ALTER TABLE products_categories DISABLE KEYS */;

INSERT INTO products_categories (id, title, active)
VALUES
	(31,'Irrigation solutions',0),
	(29,'Tapes',0),
	(28,'Active Dressings',0),
	(25,'Compression Bandaging',0),
	(24,'Non Adherent Porous',0),
	(23,'Debriding Agents',0),
	(22,' Skin Barriers',0),
	(1,'Skin Sealants',0),
	(2,'Tranparent Films',0),
	(3,'Hydrocolloids',0),
	(4,'Hydrogels ',0),
	(27,'Absorbent Dressings',0),
	(7,'Foams - adhesive',0),
	(8,'Calcium Alginates',0),
	(9,'Hydrofibres',0),
	(14,'Cover Dressings',0),
	(15,'Hypertonic Dressing',0),
	(16,'Charcoal dressing',0),
	(37,'Accessories',0),
	(36,'Gauze',0),
	(20,'Antimicrobials - Ag',0),
	(6,'Foams -non-adhesive',0),
	(33,'Other',0),
	(34,'VAC Dressings',0),
	(38,'Tulle Gras',0),
	(39,'Antimicrobials - I',0),
	(40,'Exudate Transfer Dressing',0),
	(41,'Povidone Iodine',0),
	(42,'Tube Stocking',0),
	(43,'Skin Protectants',0),
	(44,'Foams - Polyurethane',1),
	(45,'',0),
	(46,'Calcium Alginates',1),
	(47,'Hydrofibres',1),
	(48,'Composite Dressings - High Absorbency ',1),
	(49,'Composite Dressings - Moderate Absorbency ',1),
	(50,'Hydrocolloids',1),
	(51,'Transparent Films',1),
	(52,'Silicone Protective Contact Dressings',1),
	(53,'Antimicrobials - Silver',1),
	(54,'Antimicrobials - Cadexomer Iodine',1),
	(55,'Compression Wrap Therapy - Active/Long Stretch',1),
	(56,'Compression Wrap Therapy  - Passive/Short Stretch',1),
	(57,'Compression Wrap Therapy - Zinc Impregnated ',1),
	(58,'Ointment Impregnated Gauze Dressings',1),
	(59,'Absorbable Gelatin Sponges',1),
	(60,'Adhesive Removers',1),
	(61,'Amorphous Gels',1),
	(62,'Hypertonic Saline Dressings',1),
	(63,'Skin Sealants',1),
	(64,'Charcoal Impregnated Dressings',1),
	(65,'Matrix Modifiers',1),
	(66,'Protective Contact Layer',0),
	(67,'Gloves',0),
	(68,'Gauze Wraps',1),
	(69,'NPWT (VAC) Dressings',1),
	(70,'Surgical Cover Dressings',1),
	(71,'Tapes',1),
	(72,'Test1 test',1),
	(73,'Povidone Iodine',1),
	(74,'Skin Protectants',1),
	(75,'Gauze Dressings',1),
	(76,'Solutions',0),
	(77,'Absorbent Dressings - Sterile',1),
	(78,'Tensor Wraps',1),
	(79,'Compression Wrap Therapy  - Self Adhesive',1),
	(80,'Tube Stocking',0),
	(81,'Mesh Dressings',1),
	(82,'Basics',1),
	(83,'Tensor Wraps',0),
	(84,'Silver Nitrate',1),
	(85,'Biological Debridement Therapy',1),
	(86,'Compression Wrap Therapy - Modified Short Stretch',1),
	(87,'Transfer Dressings',1),
	(88,'Composite Dressings - Light Absorbency',1),
	(89,'Absorbent Dressings - Non Sterile',1),
	(90,'Acrylic Dressings',1),
	(91,'Ostomy Supplies',1),
	(92,'Urinary Catheter Supplies',1),
	(93,'testCategory',1),
	(94,' 2',0),
	(95,'@@@@@@@@@@@@@@@@@@',0),
	(96,'@',0),
	(97,'@',0),
	(98,'@',0);


/*!40000 ALTER TABLE products_categories ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table professional_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS professional_accounts;

CREATE TABLE professional_accounts (
  id int(11) NOT NULL AUTO_INCREMENT,
  password text,
  account_status int(11) DEFAULT NULL,
  firstname text,
  firstname_search text,
  lastname text,
  lastname_search text,
  pager text,
  email text,
  training_flag int(1) DEFAULT '0',
  access_uploader int(1) DEFAULT NULL,
  access_reporting int(1) DEFAULT NULL,
  access_viewer int(1) DEFAULT NULL,
  access_admin int(1) DEFAULT NULL,
  access_allpatients int(1) DEFAULT NULL,
  access_assigning int(1) DEFAULT NULL,
  access_professionals int(11) DEFAULT NULL,
  access_patients int(11) DEFAULT NULL,
  access_regions int(11) DEFAULT NULL,
  access_referrals int(11) DEFAULT NULL,
  access_listdata int(11) DEFAULT NULL,
  access_audit int(11) DEFAULT NULL,
  access_superadmin int(11) DEFAULT NULL,
  individuals text,
  user_name text,
  creation_date text,
  updated_date text,
  deletion_date text,
  new_user int(11) DEFAULT NULL,
  timezone varchar(100) DEFAULT NULL,
  access_create_patient int(11) DEFAULT NULL,
  position_id int(11) DEFAULT NULL,
  locked int(11) DEFAULT NULL,
  invalid_password_count int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

LOCK TABLES professional_accounts WRITE;
/*!40000 ALTER TABLE professional_accounts DISABLE KEYS */;

INSERT INTO professional_accounts (id, password, account_status, firstname, firstname_search, lastname, lastname_search, pager, email, training_flag, access_uploader, access_reporting, access_viewer, access_admin, access_allpatients, access_assigning, access_professionals, access_patients, access_regions, access_referrals, access_listdata, access_audit, access_superadmin, individuals, user_name, creation_date, updated_date, deletion_date, new_user, timezone, access_create_patient, position_id, locked, invalid_password_count)
VALUES
	(7,'dcbe61b3a1554a0d10206ad36179b711',1,'Webmed','WEBMED','Test','TEST','','',1,1,1,1,1,1,1,1,1,1,0,0,1,1,'1',NULL,NULL,NULL,NULL,0,'America/Vancouver',1,5,0,0);

/*!40000 ALTER TABLE professional_accounts ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table referrals_tracking
# ------------------------------------------------------------

DROP TABLE IF EXISTS referrals_tracking;

CREATE TABLE referrals_tracking (
  id int(11) NOT NULL AUTO_INCREMENT,
  current_flag int(11) DEFAULT NULL,
  patient_account_id int(11) NOT NULL DEFAULT '0',
  wound_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  entry_type int(11) DEFAULT NULL,
  time_stamp text,
  top_priority int(11) DEFAULT '0',
  assessment_id int(11) NOT NULL DEFAULT '0',
  professional_id int(11) DEFAULT NULL,
  active int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY professional_id (professional_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY patient_account_id (patient_account_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT referrals_tracking_ibfk_5 FOREIGN KEY (patient_account_id) REFERENCES patient_accounts (id),
  CONSTRAINT referrals_tracking_ibfk_7 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT referrals_tracking_ibfk_8 FOREIGN KEY (wound_id) REFERENCES wounds (id),
  CONSTRAINT referrals_tracking_ibfk_9 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table report_storage
# ------------------------------------------------------------

DROP TABLE IF EXISTS report_storage;

CREATE TABLE report_storage (
  id int(11) NOT NULL AUTO_INCREMENT,
  date datetime DEFAULT NULL,
  description text NOT NULL,
  filename varchar(255) DEFAULT NULL,
  report_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS resources;

CREATE TABLE resources (
  id int(11) NOT NULL AUTO_INCREMENT,
  name text,
  report_value_label_key text,
  report_value_component_type text,
  report_value_field_type text,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=latin1;

LOCK TABLES resources WRITE;
/*!40000 ALTER TABLE resources DISABLE KEYS */;

INSERT INTO resources (id, name, report_value_label_key, report_value_component_type, report_value_field_type)
VALUES
	(1,'Funding Source',NULL,NULL,NULL),
	(2,'Referral Source',NULL,NULL,NULL),
	(3,'Treatment Location',NULL,NULL,NULL),
	(4,'Patient Residence',NULL,NULL,NULL),
	(5,'Co-morbidities',NULL,NULL,NULL),
	(6,'Interfering Factors',NULL,NULL,NULL),
	(7,'Gender',NULL,NULL,NULL),
	(8,'Etiology',NULL,NULL,NULL),
	(9,'Program Stay',NULL,NULL,NULL),
	(10,'Program Stay Variance',NULL,NULL,NULL),
	(11,'Patient Care',NULL,NULL,NULL),
	(12,'Pressure Ulcer Stage',NULL,NULL,NULL),
	(13,'Albumin',NULL,NULL,NULL),
	(14,'Pre Albumin',NULL,NULL,NULL),
	(15,'Infections',NULL,NULL,NULL),
	(16,'Investigations',NULL,NULL,NULL),
	(17,'VAC',NULL,NULL,NULL),
	(18,'Limb Color',NULL,NULL,NULL),
	(19,'Limb Temperature',NULL,NULL,NULL),
	(20,'Limb Edema',NULL,NULL,NULL),
	(21,'Limb Edema Severity',NULL,NULL,NULL),
	(22,'Doppler List',NULL,NULL,NULL),
	(23,'Palpation List',NULL,NULL,NULL),
	(24,'Exudate Type',NULL,NULL,NULL),
	(25,'Exudate Amount',NULL,NULL,NULL),
	(26,'Odour',NULL,NULL,NULL),
	(28,'Dressing Change Frequency','pixalere.admin.resources.form.weekly_dressing_changes','TXTN','INTEGER'),
	(29,'Nursing Visit Frequency','pixalere.admin.resources.form.weekly_nursing_visits','TXTN','INTEGER'),
	(30,'Priority','pixalere.admin.resources.form.response_time','TXTN','INTEGER'),
	(31,'Wound Status',NULL,NULL,NULL),
	(32,'Discharge Reason',NULL,NULL,NULL),
	(33,'Wound Bed',NULL,NULL,NULL),
	(34,'Wound Edge',NULL,NULL,NULL),
	(35,'Wound Bed Percent',NULL,NULL,NULL),
	(36,'Colour',NULL,NULL,NULL),
	(37,'Periwound Skin',NULL,NULL,NULL),
	(38,'C&S Result',NULL,NULL,NULL),
	(39,'C&S Route',NULL,NULL,NULL),
	(40,'Pressure Reading',NULL,NULL,NULL),
	(41,'Endocrine/Metabolic/Nutritional',NULL,NULL,NULL),
	(42,'Heart/Circulation',NULL,NULL,NULL),
	(43,'Musculoskeletal',NULL,NULL,NULL),
	(44,'Neurological',NULL,NULL,NULL),
	(45,'Psychiatric/Mood',NULL,NULL,NULL),
	(46,'Pulmonary',NULL,NULL,NULL),
	(47,'Sensory',NULL,NULL,NULL),
	(48,'Infections',NULL,NULL,NULL),
	(49,'Other',NULL,NULL,NULL),
	(50,'Richmond',NULL,NULL,NULL),
	(51,'Vancouver',NULL,NULL,NULL),
	(52,'Coast Garibaldi',NULL,NULL,NULL),
	(53,'North Shore',NULL,NULL,NULL),
	(54,'Blood Sugar',NULL,NULL,NULL),
	(55,'Visits',NULL,NULL,NULL),
	(56,'Professionals','pixalere.admin.resources.form.specialist_type','TXTN','INTEGER'),
	(57,'Positions',NULL,NULL,NULL),
	(58,'Interfering Medications',NULL,NULL,NULL),
	(59,'Ostomy: Pouch Selection',NULL,NULL,NULL),
	(60,'Ostomy: Proximity Of Ostomy',NULL,NULL,NULL),
	(61,'Ostomy: Stool Colour',NULL,NULL,NULL),
	(62,'Ostomy: Profile',NULL,NULL,NULL),
	(63,'Ostomy: Application Change',NULL,NULL,NULL),
	(64,'Ostomy: Psycho Social Concern',NULL,NULL,NULL),
	(65,'Ostomy: Nutritional Status',NULL,NULL,NULL),
	(66,'Ostomy: Urine Colour',NULL,NULL,NULL),
	(67,'Ostomy: Skin Texture',NULL,NULL,NULL),
	(68,'Ostomy: Stoma Colour',NULL,NULL,NULL),
	(69,'Ostomy: Stool Discharge',NULL,NULL,NULL),
	(70,'Ostomy: Urine Type',NULL,NULL,NULL),
	(71,'Ostomy: Urine Quantity',NULL,NULL,NULL),
	(72,'Ostomy: Stool Consistency',NULL,NULL,NULL),
	(73,'Ostomy: Stool Quantity',NULL,NULL,NULL),
	(74,'Ostomy: Skin Appearance',NULL,NULL,NULL),
	(75,'Ostomy: Skin Contour',NULL,NULL,NULL),
	(76,'Ostomy: Stoma Moisture',NULL,NULL,NULL),
	(77,'Ostomy: Stoma Height',NULL,NULL,NULL),
	(78,'Ostomy: Stoma Shape',NULL,NULL,NULL),
	(79,'Ostomy: Teaching Learning',NULL,NULL,NULL),
	(80,'Ostomy: Ostomy Permanent',NULL,NULL,NULL),
	(81,'Ostomy: Device Present',NULL,NULL,NULL),
	(82,'Ostomy: Teaching Status',NULL,NULL,NULL),
	(83,'Ostomy: Irrigation',NULL,NULL,NULL),
	(84,'Ostomy: Stoma Construction',NULL,NULL,NULL),
	(85,'Ostomy Etiology',NULL,NULL,NULL),
	(86,'Wound Goals',NULL,NULL,NULL),
	(87,'Ostomy: Goals',NULL,NULL,NULL),
	(88,'Ostomy: Measurement',NULL,NULL,NULL),
	(89,'Ostomy: Patient Limitations',NULL,NULL,NULL),
	(90,'Incision: Exudate',NULL,NULL,NULL),
	(91,'Incision: S & S Infection',NULL,NULL,NULL),
	(92,'Incision: Status',NULL,NULL,NULL),
	(93,'Drain: Type Of Drain',NULL,NULL,NULL),
	(94,'Drain: Peri Drain Skin',NULL,NULL,NULL),
	(95,'Drain: Drainage',NULL,NULL,NULL),
	(96,'Drain: Odour',NULL,NULL,NULL),
	(97,'Pain',NULL,NULL,NULL),
	(98,'Reading Variable',NULL,NULL,NULL),
	(99,'Incision Closure Type',NULL,NULL,NULL),
	(100,'Incision Closure Status',NULL,NULL,NULL),
	(101,'Incision Exudate Amount',NULL,NULL,NULL),
	(102,'Drain Site',NULL,NULL,NULL),
	(103,'Laser',NULL,NULL,NULL),
	(104,'Braden Sensory',NULL,NULL,NULL),
	(105,'Braden Moisture',NULL,NULL,NULL),
	(106,'Braden Activity',NULL,NULL,NULL),
	(107,'Braden Mobility',NULL,NULL,NULL),
	(108,'Braden Nutrition',NULL,NULL,NULL),
	(109,'Braden Friction',NULL,NULL,NULL),
	(110,'Missing Limbs',NULL,NULL,NULL),
	(111,'Pain Assessment',NULL,NULL,NULL),
	(112,'Skin Assessment',NULL,NULL,NULL),
	(113,'Sensory',NULL,NULL,NULL),
	(114,'Proprioception',NULL,NULL,NULL),
	(115,'Deformities',NULL,NULL,NULL),
	(116,'Skin',NULL,NULL,NULL),
	(117,'Toes',NULL,NULL,NULL),
	(118,'Weight Bearing Status',NULL,NULL,NULL),
	(119,'Balance',NULL,NULL,NULL),
	(120,'Calf Muscle Function',NULL,NULL,NULL),
	(121,'Mobility Aids',NULL,NULL,NULL),
	(122,'Muscle Tone',NULL,NULL,NULL),
	(123,'Arches',NULL,NULL,NULL),
	(124,'Foot Active',NULL,NULL,NULL),
	(125,'Foot Passive',NULL,NULL,NULL),
	(126,'Sensation',NULL,NULL,NULL),
	(127,'Lab Done',NULL,NULL,NULL),
	(128,'Trancutaneous Pressures',NULL,NULL,NULL),
	(129,'E-Stim',NULL,NULL,NULL),
	(130,'Massage',NULL,NULL,NULL),
	(131,'Ultrasound',NULL,NULL,NULL),
	(132,'Hbot',NULL,NULL,NULL),
	(134,'PostOp Etiology',NULL,NULL,NULL),
	(135,'Tubes&Drains Etiology',NULL,NULL,NULL),
	(136,'Ostomy: Construction',NULL,NULL,NULL),
	(137,'Ostomy: Drainage',NULL,NULL,NULL),
	(138,'Ostomy: Mucocutaneous Margin',NULL,NULL,NULL),
	(139,'Ostomy: Peri-fistula Skin',NULL,NULL,NULL),
	(143,'Tubes&Drains: Drainage Amount Desc',NULL,NULL,NULL),
	(144,'Tubes&Drains: Drain Removed Reason',NULL,NULL,NULL),
	(146,'Wound Profile: Teaching Goals',NULL,NULL,NULL),
	(147,'Ostomy: Nutritional Status',NULL,NULL,NULL),
	(148,'Ostomy: Type of Ostomy',NULL,NULL,NULL),
	(149,'Ostomy Assess: Discharge Reason',NULL,NULL,NULL),
	(150,'Incision Assess: Discharge Reason',NULL,NULL,NULL),
	(151,'Incision Assess: Discharge Reason',NULL,NULL,NULL),
	(152,'Incision Assess: Postop Management',NULL,NULL,NULL),
	(153,'Drain Status',NULL,NULL,NULL),
	(154,'Incision Status',NULL,NULL,NULL),
	(155,'Ostomy Status',NULL,NULL,NULL),
	(157,'Burn: Wound',NULL,NULL,NULL),
	(158,'Burn: Exudate',NULL,NULL,NULL),
	(159,'Burn: Exudate Amount',NULL,NULL,NULL),
	(160,'Burn: Grafts',NULL,NULL,NULL),
	(161,'Burn: Donors',NULL,NULL,NULL),
	(167,'Incision Goals',NULL,NULL,NULL),
	(168,'Tube/Drain Goals',NULL,NULL,NULL),
	(169,'Inactivate Reasons',NULL,NULL,NULL),
	(170,'Timezone','','',''),
	(172,'Burn Etiology',NULL,NULL,NULL),
	(173,'Burn Goals',NULL,NULL,NULL);

/*!40000 ALTER TABLE resources ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table table_updates
# ------------------------------------------------------------

DROP TABLE IF EXISTS table_updates;

CREATE TABLE table_updates (
  id int(11) NOT NULL AUTO_INCREMENT,
  components int(11) NOT NULL DEFAULT '0',
  configuration int(11) NOT NULL DEFAULT '0',
  information_popup int(11) NOT NULL DEFAULT '0',
  listdata int(11) NOT NULL DEFAULT '0',
  location_images int(11) NOT NULL DEFAULT '0',
  products int(11) NOT NULL DEFAULT '0',
  products_categories int(11) NOT NULL DEFAULT '0',
  resources int(11) NOT NULL DEFAULT '0',
  blue_model_images int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

LOCK TABLES table_updates WRITE;
/*!40000 ALTER TABLE table_updates DISABLE KEYS */;

INSERT INTO table_updates (id, components, configuration, information_popup, listdata, location_images, products, products_categories, resources, blue_model_images)
VALUES
	(1,0,1327823271,1332453286,1340748684,0,1329334112,1328039549,1332449444,0);

/*!40000 ALTER TABLE table_updates ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table treatment_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS treatment_comments;

CREATE TABLE treatment_comments (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT '0',
  wound_id int(11) NOT NULL DEFAULT '0',
  active int(11) NOT NULL DEFAULT '0',
  professional_id int(11) DEFAULT '0',
  user_signature varchar(255) DEFAULT NULL,
  body text,
  time_stamp varchar(255) DEFAULT NULL,
  assessment_id int(11) NOT NULL DEFAULT '0',
  wound_profile_type_id int(11) NOT NULL DEFAULT '0',
  deleted int(11)  NOT NULL DEFAULT '0',
  deleted_reason varchar(255) DEFAULT '',
  delete_signature varchar(255) DEFAULT '',
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY id (id),
  KEY wound_id (wound_id),
  KEY professional_id (professional_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  KEY assessment_id (assessment_id),
  CONSTRAINT treatment_comments_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT treatment_comments_ibfk_2 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT treatment_comments_ibfk_3 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE,
  CONSTRAINT treatment_comments_ibfk_4 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT treatment_comments_ibfk_5 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;



# Dump of table update_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS update_log;

CREATE TABLE update_log (
  id int(11) NOT NULL AUTO_INCREMENT,
  item_id int(11) DEFAULT NULL,
  professional text,
  which int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table user_account_regions
# ------------------------------------------------------------

DROP TABLE IF EXISTS user_account_regions;

CREATE TABLE user_account_regions (
  id int(11) NOT NULL AUTO_INCREMENT,
  treatment_location_id int(11) DEFAULT '0',
  user_account_id int(11) DEFAULT '0',
  PRIMARY KEY (id),
  KEY treatment_location_id (treatment_location_id),
  KEY user_account_id (user_account_id),
  CONSTRAINT user_account_regions_ibfk_1 FOREIGN KEY (treatment_location_id) REFERENCES lookup (id),
  CONSTRAINT user_account_regions_ibfk_2 FOREIGN KEY (user_account_id) REFERENCES professional_accounts (id)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

LOCK TABLES user_account_regions WRITE;
/*!40000 ALTER TABLE user_account_regions DISABLE KEYS */;

INSERT INTO user_account_regions (id, treatment_location_id, user_account_id)
VALUES
	(7,201,7);

/*!40000 ALTER TABLE user_account_regions ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS user_position;

CREATE TABLE user_position (
  id int(11) NOT NULL AUTO_INCREMENT,
  referral_popup int(11) DEFAULT '0',
  recommendation_popup int(11) DEFAULT '0',
  title varchar(255) DEFAULT '',
  active int(11) DEFAULT '0',
  colour varchar(25) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

LOCK TABLES user_position WRITE;
/*!40000 ALTER TABLE user_position DISABLE KEYS */;

INSERT INTO user_position (id, referral_popup, recommendation_popup, title, active, colour)
VALUES

	(1,0,1,'RN',1,'#e3e5fd'),
	(2,0,1,'LPN',0,'#e3e5fd'),
	(3,0,1,'UGN',1,'#e3e5fd'),
	(4,1,0,'CWC',0,'#e3e5fd'),
	(5,1,0,'WOCN',1,'#f1cbce'),
	(7,1,1,'PTA',1,'#e3e5fd'),
	(8,0,0,'UC',1,'#e3e5fd'),
	(9,0,0,'MOA',1,'#e3e5fd'),
	(10,0,0,'NP',0,'#e3e5fd'),
	(11,0,0,'RD',1,'#e3e5fd'),
	(12,0,0,'RN(ET)',0,'#e3e5fd'),
	(13,0,0,'Dr',0,'#e3e5fd'),
	(14,0,0,'dr',0,'#e3e5fd'),
	(15,0,0,'X',0,'#e3e5fd'),
	(16,0,0,'Abc',0,'#e3e5fd'),
	(17,0,0,'table',0,'#e3e5fd'),
	(18,0,0,'abc',0,'#e3e5fd'),
	(19,0,0,'abc ',0,'#e3e5fd'),
	(20,0,0,'test',0,'#e3e5fd'),
	(21,0,0,'testname',0,'#e3e5fd'),
	(22,0,0,'testname',0,'#e3e5fd'),
	(23,0,0,'test test',0,'#e3e5fd'),
	(24,0,0,'nikita',0,'#e3e5fd'),
	(25,0,0,'lol',0,'#e3e5fd'),
	(26,0,0,'lol',0,'#e3e5fd'),
	(27,0,0,'test',0,'#e3e5fd'),
	(28,0,0,'nick',0,'#e3e5fd'),
	(29,0,0,'nick',0,'#e3e5fd'),
	(30,0,0,'',0,'#e3e5fd'),
	(31,0,0,'123',0,'#e3e5fd'),
	(32,1,0,'doc',0,'#e3e5fd'),
	(33,0,0,'abc',0,'#e3e5fd'),
	(34,0,0,'abc',0,'#e3e5fd'),
	(35,0,0,'abc',0,'#e3e5fd'),
	(36,0,0,'abs',0,'#e3e5fd'),
	(37,0,0,'absx',0,'#e3e5fd'),
	(38,0,0,'xxx',0,'#e3e5fd'),
	(39,0,0,'xxx',0,'#e3e5fd'),
	(40,0,0,'xzxz',0,'#e3e5fd'),
	(41,0,0,'cwscwc',0,'#e3e5fd'),
	(42,0,0,'xzxzx',0,'#e3e5fd'),
	(43,0,0,'aaa',0,'#e3e5fd'),
	(44,0,0,'zxcv',0,'#e3e5fd'),
	(45,0,0,'xczxc',0,'#e3e5fd'),
	(46,0,0,'aaa',0,'#e3e5fd'),
	(47,0,0,'wew',0,'#e3e5fd'),
	(48,0,0,'xzx',0,'#e3e5fd'),
	(49,0,0,'awq',0,'#e3e5fd'),
	(50,0,0,'132',0,'#e3e5fd'),
	(51,0,0,'xxx',0,'#e3e5fd'),
	(52,0,0,'23',0,'#e3e5fd'),
	(53,1,1,'Test',1,'#e3e5fd'),
	(54,0,0,'',0,'#e3e5fd'),
	(55,0,0,'wew',0,'#e3e5fd'),
	(56,1,1,'Nick',0,'#e3e5fd'),
	(57,0,0,'test',0,'#e3e5fd'),
	(58,1,1,'Test',1,'#e3e5fd'),
	(59,0,0,'',1,'#e3e5fd'),
	(60,0,0,'',1,'#e3e5fd'),
	(61,0,0,'',1,'#e3e5fd'),
	(62,0,0,'',0,'#e3e5fd'),
	(63,0,0,'RN++',1,'#e3e5fd'),
	(64,0,0,'Patient',1,'#EEEEEE');


/*!40000 ALTER TABLE user_position ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wound_assessment
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_assessment;

CREATE TABLE wound_assessment (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(11) NOT NULL DEFAULT '0',
  patient_account_id int(11) DEFAULT '0',
  patient_id int(11) NOT NULL DEFAULT '0',
  wound_id int(11) DEFAULT NULL,
  treatment_location_id int(11) DEFAULT NULL,
  has_comments int(11) DEFAULT '0',
  offline_flag int(11) DEFAULT NULL,
  professional_id int(11) NOT NULL DEFAULT '0',
  time_stamp text,
  user_signature text,
  visit int(11) DEFAULT '0',
  reset_visit int(11) DEFAULT '0',
  reset_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY professional_id (professional_id),
  KEY id (id),
  CONSTRAINT wound_assessment_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT wound_assessment_ibfk_2 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT wound_assessment_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wound_assessment_location
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_assessment_location;

CREATE TABLE wound_assessment_location (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(1) DEFAULT NULL,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) NOT NULL DEFAULT '0',
  alpha varchar(25) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  discharge int(1) DEFAULT NULL,
  reactivate int(1) DEFAULT NULL,
  open_time_stamp text,
  close_time_stamp text,
  professional_id int(11) DEFAULT '0',
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  x int(11) DEFAULT NULL,
  y int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY id (id),
  KEY alpha (alpha),
  KEY professional_id (professional_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT wound_assessment_location_ibfk_1 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT wound_assessment_location_ibfk_7 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT wound_assessment_location_ibfk_8 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT wound_assessment_location_ibfk_9 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wound_etiology
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_etiology;

CREATE TABLE wound_etiology (
  id int(11) NOT NULL AUTO_INCREMENT,
  wound_profile_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  lookup_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY alpha_id (alpha_id),
  KEY lookup_id (lookup_id),
  KEY wound_profile_id (wound_profile_id),
  CONSTRAINT wound_etiology_ibfk_1 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE,
  CONSTRAINT wound_etiology_ibfk_2 FOREIGN KEY (lookup_id) REFERENCES lookup (id),
  CONSTRAINT wound_etiology_ibfk_3 FOREIGN KEY (wound_profile_id) REFERENCES wound_profiles (id) ON DELETE CASCADE,
  CONSTRAINT wound_etiology_ibfk_4 FOREIGN KEY (wound_profile_id) REFERENCES wound_profiles (id) ON DELETE CASCADE

) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;



# Dump of table wound_goals
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_goals;

CREATE TABLE wound_goals (
  id int(11) NOT NULL AUTO_INCREMENT,
  wound_profile_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  lookup_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY alpha_id (alpha_id),
  KEY lookup_id (lookup_id),
  KEY wound_profile_id (wound_profile_id),
  CONSTRAINT wound_goals_ibfk_1 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE,
  CONSTRAINT wound_goals_ibfk_2 FOREIGN KEY (lookup_id) REFERENCES lookup (id),
  CONSTRAINT wound_goals_ibfk_3 FOREIGN KEY (wound_profile_id) REFERENCES wound_profiles (id) ON DELETE CASCADE,
  CONSTRAINT wound_goals_ibfk_4 FOREIGN KEY (wound_profile_id) REFERENCES wound_profiles (id) ON DELETE CASCADE

) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;




# Dump of table wound_location_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_location_details;

CREATE TABLE wound_location_details (
  id int(11) NOT NULL AUTO_INCREMENT,
  alpha_id int(11) DEFAULT NULL,
  image char(25) DEFAULT NULL,
  box int(11) DEFAULT '0',
  deleted int(1)  NOT NULL DEFAULT '0',
  x int(11) DEFAULT NULL,
  y int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY alpha_id (alpha_id),
  CONSTRAINT wound_location_details_ibfk_8 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE

) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;




# Dump of table wound_profile_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_profile_type;

CREATE TABLE wound_profile_type (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  alpha_type varchar(5) DEFAULT '',
  closed int(1) DEFAULT '0',
  closed_date text,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  active int(11) DEFAULT NULL,
  professional_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY id (id),
  CONSTRAINT wound_profile_type_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT wound_profile_type_ibfk_2 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wound_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_profiles;

CREATE TABLE wound_profiles (
  id int(11) NOT NULL AUTO_INCREMENT,
  current_flag int(11) DEFAULT NULL,
  offline_flag int(11) DEFAULT NULL,
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) NOT NULL DEFAULT '0',
  last_update text,
  pressure_ulcer int(11) DEFAULT NULL,
  prev_prods text,
  referral int(11) DEFAULT NULL,
  reason_for_care text,
  active int(11) NOT NULL DEFAULT '0',
  professional_id int(11) DEFAULT '0',
  user_signature varchar(255) DEFAULT NULL,
  surgeon text,
  date_surgery text,
  operative_procedure_comments text,
  teaching_goals int(11) DEFAULT NULL,
  marking_prior_surgery int(11) DEFAULT NULL,
  type_of_ostomy text,
  review_done int(11) DEFAULT '0',
  burn_areas text,
  preliminary_tbsa text,
  final_tbsa text,
  tbsa_percentage text,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY patient_id (patient_id),
  KEY id (id),
  KEY wound_id (wound_id),
  KEY professional_id (professional_id),
  CONSTRAINT wound_profiles_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT wound_profiles_ibfk_2 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT wound_profiles_ibfk_5 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT wound_profiles_ibfk_6 FOREIGN KEY (wound_id) REFERENCES wounds (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wound_visits
# ------------------------------------------------------------

DROP TABLE IF EXISTS wound_visits;

CREATE TABLE wound_visits (
  total int(11) DEFAULT '0',
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  id int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table lookup add clinic int;

# Dump of table wounds
# ------------------------------------------------------------

DROP TABLE IF EXISTS wounds;

CREATE TABLE wounds (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(11) NOT NULL DEFAULT '0',
  patient_id int(11) NOT NULL DEFAULT '0',
  professional_id int(11) NOT NULL DEFAULT '0',
  wound_location varchar(255) NOT NULL DEFAULT '',
  wound_location_detailed varchar(255) NOT NULL DEFAULT '',
  wound_location_position char(1) NOT NULL DEFAULT '',
  image_name varchar(255) NOT NULL DEFAULT '',
  location_image_id int(11) NOT NULL DEFAULT '0',
  start_date varchar(55) NOT NULL DEFAULT '',
  discharge varchar(255) DEFAULT NULL,
  status varchar(255) DEFAULT NULL,
  startdate_signature varchar(255) NOT NULL DEFAULT '',
  closed_date varchar(255) DEFAULT '',
  deleted int(11) NOT NULL  DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY patient_id (patient_id),
  KEY professional_id (professional_id),
  KEY location_image_id (location_image_id),
  CONSTRAINT wounds_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT wounds_ibfk_2 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT wounds_ibfk_3 FOREIGN KEY (location_image_id) REFERENCES location_images (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
alter table products add uid varchar(255);
update components set ignore_info_popup=0;
update components set ignore_info_popup=1 where id=1;
update components set ignore_info_popup=1 where id=12;
update components set ignore_info_popup=1 where id=13;
update components set ignore_info_popup=1 where id=52;
update components set ignore_info_popup=1 where id=91;
update components set ignore_info_popup=1 where id=95;
update components set ignore_info_popup=1 where id=98;
update components set ignore_info_popup=1 where id=156;
update components set ignore_info_popup=1 where id=176;
update components set ignore_info_popup=1 where id=200;
update components set ignore_info_popup=1 where id=226;
update components set ignore_info_popup=1 where id=228;
update components set ignore_info_popup=1 where id=230;
update components set ignore_info_popup=1 where id=231;
update components set ignore_info_popup=1 where id=234;
update components set ignore_info_popup=1 where id=236;
update components set ignore_info_popup=1 where id=238;
update components set ignore_info_popup=1 where id=240;
update components set ignore_info_popup=1 where id=242;
update components set ignore_info_popup=1 where id=244;
update components set ignore_info_popup=1 where id=246;
update components set ignore_info_popup=1 where id=248;
update components set ignore_info_popup=1 where id=250;
update components set ignore_info_popup=1 where id=254;
update components set ignore_info_popup=1 where id=256;
update components set ignore_info_popup=1 where id=262;
update components set ignore_info_popup=1 where id=264;
update components set ignore_info_popup=1 where id=266;
update components set ignore_info_popup=1 where id=268;
update components set ignore_info_popup=1 where id=274;
update components set ignore_info_popup=1 where id=276;
update components set ignore_info_popup=1 where id=278;
update components set ignore_info_popup=1 where id=280;
update components set ignore_info_popup=1 where id=282;
update components set ignore_info_popup=1 where id=284;
update components set ignore_info_popup=1 where id=288;
update components set ignore_info_popup=1 where id=292;
update components set ignore_info_popup=1 where id=296;
update components set ignore_info_popup=1 where id=298;
update components set ignore_info_popup=1 where id=300;
update components set ignore_info_popup=1 where id=302;
update components set ignore_info_popup=1 where id=307;
update components set ignore_info_popup=1 where id=308;
update components set ignore_info_popup=1 where id=309;
update components set ignore_info_popup=1 where id=310;
update components set ignore_info_popup=1 where id=311;
update components set ignore_info_popup=1 where id=315;
update components set ignore_info_popup=1 where id=316;
update components set ignore_info_popup=1 where id=317;
update components set ignore_info_popup=1 where id=323;
update components set ignore_info_popup=1 where id=325;
update components set ignore_info_popup=1 where id=327;
update components set ignore_info_popup=1 where id=331;
update components set ignore_info_popup=1 where id=332;
update components set ignore_info_popup=1 where id=333;
update components set ignore_info_popup=1 where id=408;
update components set ignore_info_popup=1 where id=450;
update components set ignore_info_popup=1 where id=451;
update components set ignore_info_popup=1 where id=452;
update components set ignore_info_popup=1 where id=468;
update components set ignore_info_popup=1 where id=470;
update components set ignore_info_popup=1 where id=483;
update components set ignore_info_popup=1 where id=484;
update components set ignore_info_popup=1 where id=485;
update components set ignore_info_popup=1 where id=486;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
delete from configuration   where  config_setting='acute';
delete from configuration   where  config_setting='community';
update components set allow_required=1;
update components set allow_required=0,required=0 where  field_name='pdf' or id=35 or id=454 or id=455 or id=414 or id=448 or id=413 or id=32  or id=44 or id =411 or id=52 or id=412 or id=453;
update components set allow_required=0,required=0 where flowchart=11 OR flowchart=12 or field_name='maintenance' or field_name='treatment_location_id' or field_name='gender' or field_name='dob' or field_name='age' or field_name='allergies' or field_name='interfering_meds_other' or field_name='phn' or field_name='fix';
update components set allow_required=0,required=0 where  field_name='wound_location_detailed' OR field_name='operative_procedure_comments'  OR field_name='date_surgery'  OR field_name='surgeon'  OR field_name='making_prior_surgery'  OR field_name='type_of_ostomy'  OR field_name='patient_limitations'  OR field_name='teaching_goals'       ;
update components set allow_required=0,required=0 where   field_name='wound_date'  OR field_name='closed_date'  OR field_name='discharge_reason'  OR field_name='undermining_depth'  OR field_name='undermining_location'  OR field_name='sinus_depth'  OR field_name='sinus_location'  OR field_name='products'  OR field_name='vac_start_date'  OR field_name='vac_end_date'  OR field_name='pressure_reading'  OR field_name='adjunctive'  OR field_name='images'  OR field_name='therapy_setting'   OR field_name='comments'  OR field_name='bandages_in'  OR field_name='cs_show'  OR field_name='csresult'  OR field_name='referral'  OR field_name='bandages_out'  OR field_name='review_done';
update components set allow_required=0,required=0 where   field_name='clinical_path_date'  OR field_name='mucocotaneous_margin'  OR field_name='mucocutaneous_margin_comments'   OR field_name='mucocutaneous_margin_depth'   OR field_name='mucocutaneous_margin_location' ;
update components set allow_required=0,required=0 where field_name='additional_days_date'  OR field_name='drainage_amount_mls'  OR field_name='drainage_amount_hrs'  OR field_name='drainage_amount'  OR field_name='drain_characteristics'      OR field_name='antibiotics' ;
update components set allow_edit=0 where field_name='gender' OR field_name='dob' or field_name='allergies' or field_name='professionals' or field_name='co_morbidities' or field_name='surgical_history' or field_name='investigation';
delete from configuration where config_setting='requiredBradenScale';
update components set allow_required=0,required=0 where flowchart=40 or field_name='body' or field_name='body' or field_name='fistulas' or field_name='assessment_type';
update components set allow_required=0 where   field_name='wound_base' or (field_name='status' and (flowchart=3 or flowchart=4 or flowchart=5 or flowchart=6));
update components set allow_required=1,required=1 where field_name='height' or field_name='weight' or field_name='wound_date';
update components set validation='wound_year : {canadianDate : {data: ["#wound_day","#wound_month","#wound_year"]}},wound_year: "required",wound_day:"required",wound_month:"required"' where field_name='wound_date';
update components set required=0,allow_required=0 where flowchart=2;
update components set required=1,allow_required=1 where id=100 or id=101 or id=103;
update components set validation='etiology: {alphaElementRequired: { alphas: "etiology_alphas",field: "etiology"}}',validation_message='etiology: "$text.get(\'pixalere.woundprofile.error.wound_etiology\')"' where id=100;
update components set validation='goals: {alphaElementRequired: { alphas: "goals_alphas",field: "goals"}}',validation_message='goals: "$text.get(\'pixalere.woundprofile.error.wound_goals\')"' where id=103;
update components set validation="dob_year : {canadianDate: {data: ['#dob_day','#dob_month','#dob_year']}}",validation_message="dob_year: 'Valid Date only'" where id=90;
update components set validation="bandages_in: {digits: true}",validation_message="bandages_in: 'Numbers only please'" where id=551;
update components set validation="blood_pressure: {numberslash: true}",validation_message="blood_pressure: 'Numbers and slashes only please'" where id=545;
update components set validation="resp_rate: {digits: true}",validation_message="resp_rate: 'Numbers only please'" where id=537;
update components set validation="pulse: {digits: true}",validation_message="pulse: 'Numbers only please'" where id=536;
update components set validation="temperature: {number: true}",validation_message="temperature: 'Numbers and Decimal only please'" where id=535;
update components set validation="weight: {number: true}",validation_message="weight: 'Numbers and Decimal only please'" where id=534;
update components set validation="height: {number: true}",validation_message="height: 'Numbers and Decimal only please'" where id=533;
update components set validation="bandages_out: {digits: true}",validation_message="bandages_out: 'Numbers only please'" where id=507;
update components set validation="right_brachial_pressure: {digits: true}",validation_message="right_brachial_pressure: 'Numbers only please'" where id=276;
update components set validation="left_brachial_pressure: {digits: true}",validation_message="left_brachial_pressure: 'Numbers only please'" where id=275;
update components set validation="right_toe_pressure: {digits: true}",validation_message="right_toe_pressure: 'Numbers only please'" where id=274;
update components set validation="left_toe_pressure: {digits: true}",validation_message="left_toe_pressure: 'Numbers only please'" where id=273;
update components set validation="right_ankle_brachial: {digits: true}",validation_message="right_ankle_brachial: 'Numbers only please'" where id=266;
update components set validation="left_ankle_brachial: {digits: true}",validation_message="left_ankle_brachial: 'Numbers only please'" where id=265;
update components set validation="right_dorsalis_pedis_ankle_brachial: {digits: true}",validation_message="right_dorsalis_pedis_ankle_brachial: 'Numbers only please'" where id=264;
update components set validation="left_dorsalis_pedis_ankle_brachial: {digits: true}",validation_message="left_dorsalis_pedis_ankle_brachial: 'Numbers only please'" where id=263;
update components set validation="right_tibial_pedis_ankle_brachial: {digits: true}",validation_message="right_tibial_pedis_ankle_brachial: 'Numbers only please'" where id=262;
update components set validation="left_tibial_pedis_ankle_brachial: {digits: true}",validation_message="left_tibial_pedis_ankle_brachial: 'Numbers only please'" where id=261;
update components set validation="prealbumin_date_year : {canadianDate: {data: ['#prealbumin_date_day','#prealbumin_date_month','#prealbumin_date_year']}}",validation_message="prealbumin_date_year: 'Valid Date only'" where id=349;
update components set validation="albumin_date_year : {canadianDate: {data: ['#albumin_date_day','#albumin_date_month','#albumin_date_year']}}",validation_message="albumin_date_year: 'Valid Date only'" where id=348;
update components set validation="blood_sugar_meal: {time: true}",validation_message="blood_sugar_meal: 'Numbers only please'" where id=347;
update components set validation="blood_sugar_date_year : {canadianDate: {data: ['#blood_sugar_date_day','#blood_sugar_date_month','#blood_sugar_date_year']}}",validation_message="blood_sugar_date_year: 'Valid Date only'" where id=346;
update components set validation="blood_sugar: {number: true}",validation_message="blood_sugar: 'Numbers and Decimal only please'" where id=345;
update components set validation="pre_albumin: {number: true,maxlength: 5}",validation_message="pre_albumin: 'Numbers and Decimal only please'" where id=344;
update components set validation="albumin: {number: true,maxlength: 5}",validation_message="albumin: 'Numbers and Decimal only please'" where id=343;
update components set allow_required=1,required=1 where field_name='temperature' or field_name='blood_pressure' or  field_name='resp_rate' or field_name='pulse' or field_name='height' or field_name='weight' or field_name='wound_date';
update components set field_type='STRING' where field_name='blood_sugar_meal';
	INSERT INTO lookup ( active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1,156,'Closed (healed) ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
	INSERT INTO lookup ( active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1,156,'Patient self care',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
	INSERT INTO lookup ( active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1,156,'Revision',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (581,175,'wound_acquired','STRING','SALP',2,'pixalere.woundprofile.form.obtained',43,2,1,0,0,0,NULL,0,NULL);
INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (582,0,'purs_score','INTEGER','TXT',1,'pixalere.patientprofile.form.purs_score',NULL,16,1,0,0,0,NULL,0,NULL);
INSERT INTO resources (id,name,report_value_label_key,report_value_component_type,report_value_field_type) VALUES (175,'Acquired','','','');
INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide, ignore_info_popup) VALUES (583,0,'as_needed','INTEGER','Y',18,'pixalere.treatment.form.prn',NULL,5,1,0,0,0,NULL,0,NULL);
alter table patient_profiles add purs_score int default 0;
alter table components change hide hide_flowchart int default 0;
alter table components add hide_gui int default 0;
update components set hide_flowchart =0;
alter table nursing_care_plan add as_needed int;
update components set hide_gui=0;
INSERT INTO lookup ( active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1,175,'Internal',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
INSERT INTO lookup ( active, resource_id, name, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked) VALUES (1,175,'External',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'purgeReferrals','','','CHKB','','','1');
alter table patient_accounts add version_code varchar(255);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'hideVersionCode','','','CHKB','','','0');
update components set validation='wound_acquired: {alphaElementRequired: { alphas: "wound_acquired_alphas",field: "wound_acquired"}}',validation_message='wound_acquired: "$text.get(\'pixalere.woundprofile.error.wound_acquired\')"' where id=581;
CREATE TABLE wound_acquired (id int(11) NOT NULL auto_increment,primary key(id), wound_profile_id int, alpha_id int, lookup_id int) ENGINE=InnoDB;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'fiscalQuarter','','','DRP','3 months,12 weeks','3months,12weeks','3months');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'fiscalStart','','','DRP','Jan 1st,Apr 1st','jan1st,apr1st','jan1st');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'legacySkin','','','CHK','','','1');
drop table config_yearly_quarters;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'reportDashboard','','','CHK','','','1');
drop function if exists IsNumeric;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'3rdWeekEtiologyAlert','','','CHK','','','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'reportWoundManagementPlan','','','CHK','','','1');
CREATE FUNCTION IsNumeric (sIn varchar(1024)) RETURNS tinyint
RETURN sIn REGEXP '^(-|\\+){0,1}([0-9]+\\.[0-9]*|[0-9]*\\.[0-9]+|[0-9]+)$';
alter table patient_accounts change column delete_reason discharge_reason int;
alter table patient_accounts add action varchar(255);
CREATE TABLE report_queue (id int(11) NOT NULL auto_increment,primary key(id),timestamp varchar(255),email varchar(255),report_type varchar(255),start_date varchar(255),end_date varchar(255)) ENGINE=InnoDB;
CREATE TABLE report_queue_treatments (id int(11) NOT NULL auto_increment,primary key(id), report_id int, treatment_location_id int) ENGINE=InnoDB;
CREATE TABLE report_queue_etiologies (id int(11) NOT NULL auto_increment,primary key(id), report_id int, lookup_id int) ENGINE=InnoDB;
CREATE TABLE report_queue_types (id int(11) NOT NULL auto_increment,primary key(id), report_id int, wound_type varchar(255)) ENGINE=InnoDB;
CREATE TABLE languages (id int(11) NOT NULL auto_increment,primary key(id), name varchar(255)) ENGINE=InnoDB;
CREATE TABLE lookup_l10n (id int auto_increment,primary key(id),language_id int,lookup_id int,name text,resource_id int) ENGINE=InnoDB;
ALTER TABLE lookup_l10n add Foreign key (lookup_id) REFERENCES lookup(id) ;
ALTER TABLE lookup_l10n add Foreign key (language_id) REFERENCES languages(id) ;
insert into languages (name) values ('en');
insert into languages (name) values ('fr');
insert into  lookup_l10n (language_id,lookup_id,name,resource_id) select 1,id,name,resource_id from lookup;
alter table lookup drop name;
alter table lookup add modified_date  datetime;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'pushScale','','','CHK','','','1');
alter table assessment_wound add push_scale int;
INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_gui,hide_flowchart, ignore_info_popup) VALUES (584,0,'push_scale','INTEGER','TXT',3,'pixalere.assessment.form.pushscale',NULL,21,1,0,0,0,NULL,0,0,NULL);
update lookup set report_value=0 where id=225;
update lookup set report_value=1 where id=303;
update lookup set report_value=1 where id=305;
update lookup set report_value=2 where id=306;
update lookup set report_value=3 where id=307;
update lookup set report_value=3 where id=481;
update lookup set report_value=2 where id=482;
update lookup set report_value=1 where id=862;
update lookup set report_value=4 where id=480;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'lastAssessmentDays','','','DRP','7,14,21,28,31,60','7,14,21,28,31,60','7');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'multiLanguage','','','CHKB','','','1');

DROP TABLE IF EXISTS assessment_skin;
delete from configuration where config_setting='homeInfo_right';
INSERT INTO components (id, resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES (585,0,'hga1c','INTEGER','TXT',1,'pixalere.patientprofile.form.hga1c',NULL,45,1,0,0,0,'hga1c: {maxlength:3,digits:true}',NULL);
update components set required=0 where flowchart=40;
alter table patient_accounts add discharge_date date;
alter table patient_accounts add middle_name varchar(255);
alter table resources add lock_add_n_del int;
update resources set lock_add_n_del=0;
update resources set lock_add_n_del=1 where id=175;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'hideProductsFromWMP','','','CHKB','','','0');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,0,'integration','',' ','DRP','PCC Interface,HL7 Web Service,HL7 Flat File,XML Flate File (Paris),XML Web Service','pcc_webservice,hl7_webservice,hl7_flatfile,xml_flatfile,xml_webservice','1',0);
CREATE TABLE library_category (id int auto_increment,primary key(id),name varchar(255),order_by int, hide int) ENGINE=InnoDB;
insert into library_category (name,order_by,hide) values('Skin and Wound Product Guides',1,0);
insert into library_category (name,order_by,hide) values('Clinical Practice Documents',2,0);
insert into library_category (name,order_by,hide) values('Definitions',3,0);
insert into library_category (name,order_by,hide) values('Clinical Resources and Client Resources',4,0);
insert into library_category (name,order_by,hide) values('Products (dont delete)',5,1);
alter table library add category_id int;
update library set category_id=category;
alter table library drop category;
alter table products add library_id int;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'useRequiredHighlight','','','CHKB','','','0');
CREATE TABLE assessment_skin (
  id int(11) NOT NULL AUTO_INCREMENT,
  status int(11) DEFAULT '0',
  patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
  assessment_id int(11) DEFAULT NULL,
  alpha_id int(11) DEFAULT NULL,
  wound_profile_type_id int(11) DEFAULT NULL,
  full_assessment int(11) DEFAULT '-1',
  pain int(11) DEFAULT NULL,
  pain_comments text,
  length_cm int(11) DEFAULT NULL,
  length_mm int(11) DEFAULT NULL,
  width_cm int(11) DEFAULT NULL,
  width_mm int(11) DEFAULT NULL,
  depth_cm int(11) DEFAULT NULL,
  depth_mm int(11) DEFAULT NULL,
  discharge_reason varchar(50) DEFAULT NULL,
  active int(11) DEFAULT '0',
  discharge_other text,
  recurrent int(11) DEFAULT '0',
  wound_date text,
  adjunctive text,
  adjunctive_other_text text,
  closed_date text,
  last_update text,
  site text,
  prev_alpha_id int(11) DEFAULT NULL,
  moved_reason varchar(255) DEFAULT NULL,
  deleted int(11)  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  KEY id (id),
  KEY patient_id (patient_id),
  KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
  KEY alpha_id (alpha_id),
  KEY wound_profile_type_id (wound_profile_type_id),
  CONSTRAINT assessment_skin_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_skin_ibfk_4 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE,
  CONSTRAINT assessment_skin_ibfk_5 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,
  CONSTRAINT assessment_skin_ibfk_6 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE,
  CONSTRAINT assessment_skin_ibfk_7 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup)
VALUES
	( 0, 'assessment_type', 'INTEGER', 'AT',50, 'pixalere.woundassessment.form.assessment_type', NULL,  1, 1,  0, 0, 0, NULL, 1),
	( 181, 'status', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.wound_status', NULL,  2, 1, 0,  35, 1, NULL,  0),
	( 32, 'discharge_reason', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.discharge_reason', NULL, 3, 1, 1,  0, 1, NULL,  0),
	( 0, 'wound_date', 'STRING', 'DATE',50, 'pixalere.woundassessment.form.date_of_onset', NULL, 3, 1, 0,  34, 1, NULL,  0),
	( 0, 'closed_date', 'STRING', 'DATE',50, 'pixalere.woundassessment.form.closure_date', NULL,  4, 1, 1,  0, 1, NULL,  0),
	( 0, 'recurrent', 'INTEGER', 'YN',50, 'pixalere.woundassessment.form.recurrent', NULL,  6, 1, 0,  36, 1, NULL,  0),
	( 97, 'pain', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.pain', NULL,  7, 1, 0,  27, 1, NULL,  0),
	( 0, 'pain_comments', 'STRING', 'TXTA',50, 'pixalere.pain_comments', NULL,  7, 1, 0,  37, 1, NULL,  0),
	( 0, 'length', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.length', NULL,  8, 1,  0, 28, 1, NULL,  0),
	( 0, 'width', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.width', NULL,  9, 1, 0, 29, 1, NULL,  0),
	( 0, 'depth', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.depth', NULL,  10, 1, 0,  30, 1, NULL,  0),
	( 0, 'site', 'STRING', 'TXTA',50, 'pixalere.skinassessment.form.site', NULL,  16, 1, 0, 0,  1, NULL,  0),
	( 0, 'products', 'STRING', 'PROD',50, 'pixalere.woundassessment.form.products', NULL,  23, 1,  0, 0, 1, NULL,  0),
	( 0, 'body', 'STRING', 'TXTA',50, 'pixalere.woundassessment.form.treatment_comments', 16,  24, 1,  0, 0, 1, NULL,  0),
	( 0, 'vac_start_date', 'DATE', 'DATE',50, 'pixalere.woundassessment.form.vacstart', 36,  27, 1, 0,  0, 1, NULL,  0),
	( 0, 'vac_end_date', 'DATE', 'DATE',50, 'pixalere.woundassessment.form.vacend', 36,  28, 1, 0, 0,  1, NULL,  0),
	( 98, 'therapy_setting', 'INTEGER', 'RADO',50, 'pixalere.woundassessment.form.reading_group', 36,  28, 1,  0, 0, 1, NULL,  0),
	( 40, 'pressure_reading', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.pressure_reading', 36,  29, 1,  0, 0, 1, NULL, 0),
	( 38, 'cs_show', 'INTEGER', 'YN',50, 'pixalere.treatment.form.cs_done', NULL,  32, 1, 0, 0, 1, NULL,  0),
	( 38, 'csresult', 'STRING', 'CHKB',50, 'pixalere.woundassessment.form.c_s_result', 27,  34, 1, 0, 0, 1, NULL, 0),
	( 0, 'antibiotics', 'STRING', 'MDDA',50, 'pixalere.treatment.form.antibiotics', 38,  35, 1, 0, 0, 1, NULL, 0),
	( 17, 'adjunctive', 'STRING', 'CHKB',50, 'pixalere.woundassessment.form.adjunctive', 27,  37, 1, 0,  0, 1, NULL,  0),
	( 0, 'images', 'STRING', 'IMG',50, 'pixalere.woundassessment.form.images', NULL,  38, 1, 0,  0, 0, NULL,  0),
	( 0, 'review_done', 'INTEGER', 'YN',50, 'pixalere.treatment.review_done', 36,  960, 0, 0, 0,  0, NULL,  0),
	( 0, 'referral', 'STRING', 'REF',50, 'pixalere.referral.form.title_fc', NULL,  998, 0, 0, 0,  0, NULL,  1),
	( 0, 'comments', 'STRING', 'COMM',50, 'pixalere.woundassessment.form.contains_comments', NULL,  999, 0,  0, 0, 0, NULL,  0),
	( 0, 'maintenance', 'STRING', 'ADM',50, 'pixalere.viewer.assessment_updates', NULL,  1000, 0, 0, 0,  0, NULL,  0),
	( 0, 'pdf', 'STRING', 'PDF',50, 'pixalere.viewer.pdf', NULL,  1010,0,0, 0, 0,  NULL,  0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'allowSkin','',' ','CHKB','',' ','1','0');
insert into resources (id,name) values (180,'Skin Etiology');
insert into resources (id,name) values (181,'Skin Status');
insert into resources (id,name) values (182,'Skin Discharge Reason');
insert into resources (id,name) values (183,'Skin Goals');
insert into resources (id,name) values (191,'CCAC Questions');
insert into resources (id,name) values (192,'Clinical Barriers');
insert into resources (id,name) values (193,'IVTherapy Barriers');
insert into lookup (active,resource_id) values(1,180);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Etiology1',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,181);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Status1',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,182);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Reason1',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,183);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Goals1',resource_id from lookup order by id desc limit 1;
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
	( 7, 15, 0, 'skinActive', '', '', 'INPN', '', '', '3283', 1),
	( 7, 16, 0, 'skinClosed', '', '', 'INPN', '', '', '3284', 1);
alter table assessment_product add skin_assessment_id int;
update components set orderby=45 where id=247;
update components set orderby=46 where id=248;
update components set orderby=47 where id=249;
update components set orderby=48 where id=250;
update components set orderby=40 where id=467;
update components set orderby=41 where id=468;
update components set orderby=42 where id=469;
update components set orderby=43 where id=470;
update components set orderby=0 where id=450;
update components set orderby=0 where id=451;
update components set orderby=51 where id=508;
update components set orderby=50 where id=146;
update components set orderby=49 where id=147;
update components set orderby=52 where id=417;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 184, 'np_connector', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.np_connector', 51,  35, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 184, 'np_connector', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.np_connector', 51,  35, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 184, 'np_connector', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.np_connector', 51,  35, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 184, 'np_connector', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.np_connector', 51,  35, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 184, 'np_connector', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.np_connector', 51,  35, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 184, 'np_connector', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.np_connector', 51,  35, 1,  0, 0, 0, NULL, 1);
insert into resources (id,name) values (184,'Negative Pressure Connector');
insert into lookup (active,resource_id) values(1,184);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Bridge',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,184);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Y Connector',resource_id from lookup order by id desc limit 1;
alter table assessment_treatment add patient_id int;
alter table assessment_treatment add professional_id int;
update assessment_treatment set patient_id=(select patient_id from wound_assessment where wound_assessment.id=assessment_treatment.assessment_id);
update assessment_treatment set professional_id=(select professional_id from wound_assessment where wound_assessment.id=assessment_treatment.assessment_id);
CREATE TABLE assessment_npwt (
  id int(11) NOT NULL AUTO_INCREMENT,
    patient_id int(11) DEFAULT NULL,
  wound_id int(11) DEFAULT NULL,
secondary int(1) default 0,
  assessment_id int(11) DEFAULT NULL,
    professional_id int(11) default null,
  vac_start_date datetime DEFAULT NULL,
  vac_end_date datetime DEFAULT NULL,
  pressure_reading int(11) DEFAULT NULL,
  np_connector int(11) DEFAULT NULL,
  therapy_setting int(11) DEFAULT NULL,
    initiated_by int(11) DEFAULT NULL,
    serial_num varchar(255)   default NULL,
    kci_num varchar(255)  default NULL,
    reason_for_ending int(11) default null,
    goal_of_therapy int(11) default null,
primary key(id),
    KEY wound_id (wound_id),
  KEY assessment_id (assessment_id),
    CONSTRAINT assessment_npwt_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_npwt_ibfk_4 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE,
  CONSTRAINT assessment_npwt_ibfk_5 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE assessment_npwt_alpha (
  id int(11) NOT NULL AUTO_INCREMENT,
  assessment_npwt_id int(11) DEFAULT NULL,
  alpha_id int(11) default NULL,
    KEY alpha_id (alpha_id),
primary key(id),
  KEY assessment_npwt_id (assessment_npwt_id),
    CONSTRAINT assessment_npwt_alpha_ibfk_1 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id),
  CONSTRAINT assessment_npwt_alpha_ibfk_4 FOREIGN KEY (assessment_npwt_id) REFERENCES assessment_npwt (id) ON DELETE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=latin1;
insert into assessment_npwt (id,professional_id,vac_start_date,vac_end_date,pressure_reading,therapy_setting,assessment_id,patient_id,wound_id) select id,professional_id,vac_start_date,vac_end_date,pressure_reading,therapy_setting,assessment_id,patient_id,wound_id from assessment_treatment where pressure_reading IS NOT NULL and therapy_setting IS NOT NULL and vac_start_date iS NOT NULL;
alter table assessment_treatment drop column pressure_reading;
alter table assessment_treatment drop column therapy_setting;
alter table assessment_treatment drop column vac_start_date;
alter table assessment_treatment drop column vac_end_date;
alter table assessment_treatment drop column vac_show;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 0, NULL, 1);
insert into resources (id,name) values (185,'Initiated By');
insert into lookup (active,resource_id) values(1,185);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Acute',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,185);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Community',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 0, NULL, 1);
insert into resources (id,name) values (186,'Reason for Ending');
insert into lookup (active,resource_id) values(1,186);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Wound Closure',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,186);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Patient Died',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.goal_of_therapy', 51,  4, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 0, NULL, 1);
insert into resources (id,name) values (187,'Goal of Therapy');
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Wound Closure',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Prep for STSG',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Post STSG/Flap',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Fistula Closure',resource_id from lookup order by id desc limit 1;
update components set orderby=70,table_id=51 where field_name='vac_start_date';
update components set orderby=71,table_id=51 where field_name='vac_end_date';
update components set orderby=60 where field_name='antibiotics';
update components set orderby=57,table_id=36 where field_name='cs_show';
update components set orderby=58 where field_name='csresult';
update components set orderby=90 where field_name='images';
update components set orderby=72,table_id=51 where field_name='therapy_setting';
update components set orderby=73,table_id=51 where field_name='pressure_reading';
update components set orderby=61 where field_name='adjunctive';
update components set orderby=53 where field_name='body' and table_id=16;
update components set orderby=65 where field_name='np_connector';
update components set orderby=66 where field_name='initiated_by';
update components set orderby=76 where field_name='reason_for_ending';
update components set orderby=75 where field_name='goal_of_therapy';
alter table assessment_treatment drop column cs_show;
alter table assessment_treatment drop column cs_date_show;
alter table assessment_treatment drop column treatmentmodalities_show;
alter table assessment_treatment add cs_done int;
update assessment_treatment set cs_done=(select cs_show from assessment_wound where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
update assessment_treatment set cs_done=(select cs_show from assessment_drain where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
update assessment_treatment set cs_done=(select cs_show from assessment_incision where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
update assessment_treatment set cs_done=(select cs_show from assessment_ostomy where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
alter table assessment_wound drop column cs_show;
alter table assessment_drain drop column cs_show;
alter table assessment_incision drop column cs_show;
alter table assessment_ostomy drop column cs_show;
update components set field_name='cs_done' where field_name='cs_show';
alter table library add url varchar(255);
alter table library add media_type varchar(3);
update library set media_type='pdf';
alter table referrals_tracking add patient_id int;
DROP TABLE IF EXISTS schema_version;
CREATE TABLE schema_version (
  version varchar(20) NOT NULL,
  description varchar(100) DEFAULT NULL,
  type varchar(10) NOT NULL,
  script varchar(200) NOT NULL,
  checksum int(11) DEFAULT NULL,
  installed_by varchar(30) NOT NULL,
  installed_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  execution_time int(11) DEFAULT NULL,
  state varchar(15) NOT NULL,
  current_version tinyint(1) NOT NULL,
  PRIMARY KEY (version),
  UNIQUE KEY version (version),
  UNIQUE KEY script (script),
  KEY schema_version_current_version_index (current_version)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
LOCK TABLES schema_version WRITE;
/*!40000 ALTER TABLE schema_version DISABLE KEYS */;
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, state, current_version)
VALUES
	('5.0','initial script','SQL','V5.0__initial_script.sql',-1597298252,'root','2012-11-24 09:41:14',1519,'SUCCESS',0),
	('5.0.1','pushscale dashboard','SQL','V5.0.1__pushscale_dashboard.sql',1739429810,'root','2012-11-24 09:41:15',1318,'SUCCESS',0),
	('5.1','new features','SQL','V5.1__new_features.sql',-1039885761,'root','2012-11-24 09:41:17',2043,'SUCCESS',1),
        ('6.0','new features','SQL','V6.0__new_features.sql',-1039885761,'root','2012-11-24 09:41:17',2043,'SUCCESS',1),
        ('6.0.4','new features','SQL','V6_0_4__NPWT.sql',-1039885761,'root','2012-11-24 09:41:17',2043,'SUCCESS',1),
        ('6.0.5.2','new features','SQL','V6_0_5_2__PasswordReset.sql',-1039885761,'root','2012-11-24 09:41:17',2043,'SUCCESS',1);
/*!40000 ALTER TABLE schema_version ENABLE KEYS */;
UNLOCK TABLES;
alter table patient_accounts add funding_source int;
update components set field_name='funding_source' where field_name='funding_source_id';
create table limb_adv_assessment (id int(11) not null auto_increment, patient_id int(11), last_update varchar(255), active int(1),left_proprioception int(11),right_proprioception int(11),doppler_lab int(11) DEFAULT '0',left_dorsalis_pedis_doppler text,right_dorsalis_pedis_doppler text,left_posterior_tibial_doppler text,right_posterior_tibial_doppler text,left_interdigitial_doppler text,right_interdigitial_doppler text,ankle_brachial_lab int(11) DEFAULT '0',ankle_brachial_date text,left_tibial_pedis_ankle_brachial int(11) DEFAULT NULL,right_tibial_pedis_ankle_brachial int(11) DEFAULT NULL,left_dorsalis_pedis_ankle_brachial int(11) DEFAULT NULL,right_dorsalis_pedis_ankle_brachial int(11) DEFAULT NULL,left_ankle_brachial int(11) DEFAULT NULL,right_ankle_brachial int(11) DEFAULT NULL,toe_brachial_lab int(11) DEFAULT '0',toe_brachial_date text,left_toe_pressure text,right_toe_pressure text,left_digit_total int(11) DEFAULT '0',right_digit_total int(11) DEFAULT '0',right_api_score double,left_api_score double,left_brachial_pressure text,right_brachial_pressure text,transcutaneous_lab int(11) DEFAULT '0',transcutaneous_date text,left_transcutaneous_pressure int(11) DEFAULT '0',right_transcutaneous_pressure int(11) DEFAULT '0',left_sensation text,right_sensation text,left_score_sensation text,right_score_sensation text,left_foot_deformities text,left_foot_skin text,left_foot_toes text,right_foot_deformities text,left_pain_assessment text,right_pain_assessment text,left_temperature_leg int(11) DEFAULT '0',right_temperature_leg int(11) DEFAULT '0',left_temperature_foot int(11) DEFAULT '0',right_temperature_foot int(11) DEFAULT '0',left_temperature_toes int(11) DEFAULT '0',right_temperature_toes int(11) DEFAULT '0',right_foot_skin text,right_foot_toes text,deleted int(11)  NOT NULL DEFAULT '0',delete_signature varchar(255) DEFAULT NULL,deleted_reason varchar(255) DEFAULT NULL,data_entry_timestamp varchar(255) DEFAULT NULL,professional_id int(11),user_signature varchar(255),primary key(id),CONSTRAINT limb_adv_patient_1 FOREIGN KEY (patient_id) REFERENCES patients (id) ON DELETE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=latin1;
insert into limb_adv_assessment (id,patient_id,last_update,active,left_dorsalis_pedis_doppler,right_dorsalis_pedis_doppler,left_posterior_tibial_doppler,right_posterior_tibial_doppler,left_interdigitial_doppler,right_interdigitial_doppler,ankle_brachial_lab,ankle_brachial_date,left_tibial_pedis_ankle_brachial,right_tibial_pedis_ankle_brachial,left_dorsalis_pedis_ankle_brachial,right_dorsalis_pedis_ankle_brachial,left_ankle_brachial,right_ankle_brachial,toe_brachial_lab,toe_brachial_date,left_toe_pressure,right_toe_pressure,left_brachial_pressure,right_brachial_pressure,transcutaneous_lab,transcutaneous_date,left_transcutaneous_pressure,right_transcutaneous_pressure,left_sensation,right_sensation,left_score_sensation,right_score_sensation,data_entry_timestamp,user_signature,professional_id) select id,patient_id,last_update,active,left_dorsalis_pedis_doppler,right_dorsalis_pedis_doppler,left_posterior_tibial_doppler,right_posterior_tibial_doppler,left_interdigitial_doppler,right_interdigitial_doppler,ankle_brachial_lab,ankle_brachial_date,left_tibial_pedis_ankle_brachial,right_tibial_pedis_ankle_brachial,left_dorsalis_pedis_ankle_brachial,right_dorsalis_pedis_ankle_brachial,left_ankle_brachial,right_ankle_brachial,toe_brachial_lab,toe_brachial_date,left_toe_pressure,right_toe_pressure,left_brachial_pressure,right_brachial_pressure,transcutaneous_lab,transcutaneous_date,left_transcutaneous_pressure,right_transcutaneous_pressure,left_sensation,right_sensation,left_score_sensation,right_score_sensation,data_entry_timestamp,user_signature,professional_id from limb_assessment;
update limb_adv_assessment set left_foot_deformities=(select left_foot_deformities from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc limit 1);
update components set field_name='left_api_score' where field_name='left_abi_score_ankle_brachial';
update components set field_name='right_api_score' where field_name='right_abi_score_ankle_brachial';
update limb_adv_assessment set right_foot_deformities=(select right_foot_deformities from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc limit 1);
update limb_adv_assessment set left_foot_skin=(select left_foot_skin from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc limit 1);
update limb_adv_assessment set right_foot_skin=(select right_foot_skin from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc limit 1);
update limb_adv_assessment set left_foot_toes=(select left_foot_toes from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc limit 1);
update limb_adv_assessment set right_foot_toes=(select right_foot_toes from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc limit 1);
alter table limb_assessment drop column doppler_lab;
alter table limb_assessment drop column left_dorsalis_pedis_doppler;
alter table limb_assessment drop column right_dorsalis_pedis_doppler;
alter table limb_assessment drop column left_posterior_tibial_doppler;
alter table limb_assessment drop column right_posterior_tibial_doppler;
alter table limb_assessment drop column left_interdigitial_doppler;
alter table limb_assessment drop column right_interdigitial_doppler;
alter table limb_assessment drop column ankle_brachial_lab;
alter table limb_assessment drop column ankle_brachial_date;
alter table limb_assessment drop column left_tibial_pedis_ankle_brachial;
update components set hide_flowchart=1,hide_gui=1 where field_name='left_more_capillary' or field_name='right_more_capillary';
alter table limb_assessment drop column right_tibial_pedis_ankle_brachial;
alter table limb_assessment drop column left_dorsalis_pedis_ankle_brachial;
alter table limb_assessment drop column right_dorsalis_pedis_ankle_brachial;
alter table limb_assessment drop column left_ankle_brachial;
alter table limb_assessment drop column right_ankle_brachial;
alter table limb_assessment drop column toe_brachial_lab;
alter table limb_assessment drop column toe_brachial_date;
alter table limb_assessment drop column left_toe_pressure;
alter table limb_assessment drop column right_toe_pressure;
alter table limb_assessment drop column left_brachial_pressure;
alter table limb_assessment drop column right_brachial_pressure;
alter table foot_assessment drop column left_foot_deformities;
alter table foot_assessment drop column right_foot_deformities;
alter table foot_assessment drop column left_foot_skin;
alter table foot_assessment drop column right_foot_skin;
alter table foot_assessment drop column left_foot_toes;
alter table foot_assessment drop column right_foot_toes;
alter table limb_assessment drop column transcutaneous_lab;
alter table limb_assessment drop column transcutaneous_date;
alter table limb_assessment drop column left_transcutaneous_pressure;
alter table limb_assessment drop column right_transcutaneous_pressure;
alter table limb_assessment drop column left_sensation;
alter table limb_assessment drop column right_sensation;
alter table limb_assessment drop column left_score_sensation;
alter table limb_assessment drop column right_score_sensation;
RENAME TABLE limb_assessment TO limb_basic_assessment;
update components set flowchart=52 where field_name='doppler_lab';
update components set flowchart=52 where field_name='left_dorsalis_pedis_doppler';
update components set flowchart=52 where field_name='right_dorsalis_pedis_doppler';
update components set flowchart=52 where field_name='left_posterior_tibial_doppler';
update components set flowchart=52 where field_name='right_posterior_tibial_doppler';
update components set flowchart=52 where field_name='left_interdigitial_doppler';
update components set flowchart=52 where field_name='right_interdigitial_doppler';
update components set flowchart=52 where field_name='ankle_brachial_lab';
update components set flowchart=52 where field_name='ankle_brachial_date';
update components set flowchart=52 where field_name='left_tibial_pedis_ankle_brachial';
update components set flowchart=52 where field_name='right_tibial_pedis_ankle_brachial';
update components set flowchart=52 where field_name='left_dorsalis_pedis_ankle_brachial';
update components set flowchart=52 where field_name='right_dorsalis_pedis_ankle_brachial';
update components set flowchart=52 where field_name='left_ankle_brachial';
update components set flowchart=52 where field_name='right_ankle_brachial';
update components set flowchart=52 where field_name='toe_brachial_lab';
update components set flowchart=52 where field_name='toe_brachial_date';
update components set flowchart=52 where field_name='left_toe_pressure';
update components set flowchart=52 where field_name='right_toe_pressure';
update components set flowchart=52 where field_name='left_brachial_pressure';
update components set flowchart=52 where field_name='right_brachial_pressure';
update components set flowchart=52 where field_name='left_foot_deformities';
update components set flowchart=52 where field_name='right_foot_deformities';
update components set flowchart=52 where field_name='left_foot_skin';
update components set flowchart=52 where field_name='right_foot_skin';
update components set flowchart=52 where field_name='left_foot_toes';
update components set flowchart=52 where field_name='right_foot_toes';
update components set flowchart=52,hide_flowchart=1,hide_gui=1 where field_name='transcutaneous_lab';
update components set flowchart=52,hide_flowchart=1,hide_gui=1 where field_name='transcutaneous_date';
update components set flowchart=52,hide_flowchart=1,hide_gui=1 where field_name='left_transcutaneous_pressure';
update components set flowchart=52,hide_flowchart=1,hide_gui=1 where field_name='right_transcutaneous_pressure';
update components set flowchart=52 where field_name='left_sensation';
update components set flowchart=52 where field_name='right_sensation';
update components set flowchart=52 where field_name='left_score_sensation';
update components set flowchart=52 where field_name='right_score_sensation';
update components set flowchart=52 where field_name='left_tbi_score';
update components set flowchart=52 where field_name='right_tbi_score';
update components set flowchart=52 where field_name='left_abi_score_ankle_brachial';
update components set flowchart=52 where field_name='right_abi_score_ankle_brachial';
update components set flowchart=52 where field_name='left_foot_deformities';
update components set flowchart=52 where field_name='left_foot_skin';
update components set flowchart=52 where field_name='left_foot_toes';
update components set flowchart=52 where field_name='right_foot_deformities';
update components set flowchart=52 where field_name='right_foot_skin';
update components set flowchart=52 where field_name='right_foot_toes';
update components set flowchart=52 where field_name='left_tbi_score';
update components set flowchart=52 where field_name='right_tbi_score';
update components set flowchart=52 where field_name='left_abi_score_ankle_brachial';
update components set flowchart=52 where field_name='right_abi_score_ankle_brachial';
insert into components (resource_id,field_name,field_type,component_type,flowchart,label_key,table_id,orderby,required,enabled_close,info_popup_id,allow_edit,validation,allow_required,validation_message,hide_flowchart,ignore_info_popup,hide_gui) select resource_id,field_name,field_type,component_type,53,label_key,table_id,orderby,required,enabled_close,info_popup_id,allow_edit,validation,allow_required,validation_message,hide_flowchart,ignore_info_popup,hide_gui from components where flowchart=11 and (field_name='maintenance');
insert into components (resource_id,field_name,field_type,component_type,flowchart,label_key,table_id,orderby,required,enabled_close,info_popup_id,allow_edit,validation,allow_required,validation_message,hide_flowchart,ignore_info_popup,hide_gui) select resource_id,field_name,field_type,component_type,53,label_key,table_id,orderby,required,enabled_close,info_popup_id,allow_edit,validation,allow_required,validation_message,hide_flowchart,ignore_info_popup,hide_gui from components where flowchart=11;
CREATE TABLE limb_upper_assessment LIKE limb_basic_assessment;
alter table limb_upper_assessment change column left_skin_colour_leg left_skin_colour_arm int(11);
alter table limb_upper_assessment change column right_skin_colour_leg right_skin_colour_arm int(11);
alter table limb_upper_assessment change column left_skin_colour_foot left_skin_colour_hand int(11);
alter table limb_upper_assessment change column right_skin_colour_foot right_skin_colour_hand int(11);
alter table limb_upper_assessment change column left_skin_colour_toes left_skin_colour_fingers int(11);
alter table limb_upper_assessment change column right_skin_colour_toes right_skin_colour_fingers int(11);
alter table limb_upper_assessment change column left_temperature_leg left_temperature_arm int(11);
alter table limb_upper_assessment change column right_temperature_leg right_temperature_arm int(11);
alter table limb_upper_assessment change column left_temperature_foot left_temperature_hand int(11);
alter table limb_upper_assessment change column right_temperature_foot right_temperature_hand int(11);
alter table limb_upper_assessment change column left_temperature_toes left_temperature_fingers int(11);
alter table limb_upper_assessment change column right_temperature_toes right_temperature_fingers int(11);
alter table limb_upper_assessment change column left_ankle_cm left_wrist_cm int(11);
alter table limb_upper_assessment change column left_ankle_mm left_wrist_mm int(11);
alter table limb_upper_assessment change column right_ankle_cm right_wrist_cm int(11);
alter table limb_upper_assessment change column right_ankle_mm right_wrist_mm int(11);
alter table limb_upper_assessment change column left_midcalf_cm left_elbow_cm int(11);
alter table limb_upper_assessment change column left_midcalf_mm left_elbow_mm int(11);
alter table limb_upper_assessment change column right_midcalf_cm right_elbow_cm int(11);
alter table limb_upper_assessment change column right_midcalf_mm right_elbow_mm int(11);
insert into resources (id,name) values (188,'Missing Upper Limbs');
insert into resources (id,name) values (189,'Upper Limb Edema Location');
update resources set name='Limb Edema Location' where id=20;
update components set label_key='pixalere.patientprofile.form.left_wrist',field_name='left_wrist' where field_name='left_ankle' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_wrist',field_name='right_wrist' where field_name='right_ankle' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.left_elbow',field_name='left_elbow' where field_name='left_midcalf' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_elbow',field_name='right_elbow' where field_name='right_midcalf' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.left_missing_limb_upper',resource_id=188 where flowchart=53 and field_name='left_missing_limbs' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_missing_limb_upper',resource_id=188 where flowchart=53 and field_name='right_missing_limbs' and flowchart=53;
update components set field_name='left_skin_colour_arm',label_key='pixalere.patientprofile.form.left_skin_colour_arm' where field_name='left_skin_colour_leg' and flowchart=53;
update components set field_name='right_skin_colour_arm',label_key='pixalere.patientprofile.form.right_skin_colour_arm' where field_name='right_skin_colour_leg' and flowchart=53;
update components set field_name='left_skin_colour_hand',label_key='pixalere.patientprofile.form.left_skin_colour_hand' where field_name='left_skin_colour_foot' and flowchart=53;
update components set field_name='right_skin_colour_hand',label_key='pixalere.patientprofile.form.right_skin_colour_hand' where field_name='right_skin_colour_foot' and flowchart=53;
update components set field_name='left_skin_colour_fingers',label_key='pixalere.patientprofile.form.left_skin_colour_fingers' where field_name='left_skin_colour_toes' and flowchart=53;
update components set field_name='right_skin_colour_fingers',label_key='pixalere.patientprofile.form.right_skin_colour_fingers' where field_name='right_skin_colour_toes' and flowchart=53;
update components set field_name='left_temperature_arm',label_key='pixalere.patientprofile.form.left_temperature_arm' where field_name='left_temperature_leg' and flowchart=53;
update components set field_name='right_temperature_arm',label_key='pixalere.patientprofile.form.right_temperature_arm' where field_name='right_temperature_leg' and flowchart=53;
update components set field_name='left_temperature_hand',label_key='pixalere.patientprofile.form.left_temperature_hand' where field_name='left_temperature_foot' and flowchart=53;
update components set field_name='right_temperature_hand',label_key='pixalere.patientprofile.form.right_temperature_hand' where field_name='right_temperature_foot' and flowchart=53;
update components set field_name='left_temperature_fingers',label_key='pixalere.patientprofile.form.left_temperature_fingers' where field_name='left_temperature_toes' and flowchart=53;
update components set field_name='right_temperature_fingers',label_key='pixalere.patientprofile.form.right_temperature_fingers' where field_name='right_temperature_toes' and flowchart=53;
delete from components where (field_name='pain_comments' or field_name='left_intedigitial_doppler' or  field_name='right_intedigitial_doppler' ) and ( flowchart=11 or flowchart=53 or flowchart=52);
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'No amputations',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Arm above elbow',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Arm below elbow',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Hand partial',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Hand all',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Thumb',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Second finger',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Third finger',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Fourth finger',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Fifth finger',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'No Edema',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Hand',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Up to Wrist',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Up to Elbow',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Up to Shoulder',resource_id from lookup order by id desc limit 1;
alter table limb_basic_assessment add left_range_motion_knee int(11);
alter table limb_basic_assessment add left_range_motion_ankle int(11);
alter table limb_basic_assessment add left_range_motion_toe int(11);
alter table limb_basic_assessment add right_range_motion_knee int(11);
alter table limb_basic_assessment add right_range_motion_ankle int(11);
alter table limb_basic_assessment add right_range_motion_toe int(11);
insert into resources (id,name) values (190,'Range of Motion');
insert into lookup (active,resource_id) values(1,190);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Normal',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,190);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Decreased',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'left_range_motion_ankle', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.left_range_motion_ankle', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'left_range_motion_knee', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.left_range_motion_knee', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'left_range_motion_toe', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.left_range_motion_toe', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'right_range_motion_ankle', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.right_range_motion_ankle', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'right_range_motion_knee', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.right_range_motion_knee', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'right_range_motion_toe', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.right_range_motion_toe', NULL,  67, 1,  0, 0, 1, NULL, 1);
alter table limb_upper_assessment add left_range_motion_arm int(11);
alter table limb_upper_assessment add left_range_motion_hand int(11);
alter table limb_upper_assessment add left_range_motion_fingers int(11);
alter table limb_upper_assessment add right_range_motion_arm int(11);
alter table limb_upper_assessment add right_range_motion_hand int(11);
alter table limb_upper_assessment add right_range_motion_fingers int(11);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'left_range_motion_arm', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.left_range_motion_arm', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'left_range_motion_hand', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.left_range_motion_hand', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'left_range_motion_fingers', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.left_range_motion_fingers', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'right_range_motion_arm', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.right_range_motion_arm', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'right_range_motion_hand', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.right_range_motion_hand', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 190, 'right_range_motion_fingers', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.right_range_motion_fingers', NULL,  67, 1,  0, 0, 1, NULL, 1);
alter table limb_adv_assessment add left_stemmers int(11);
alter table limb_adv_assessment add right_stemmers int(11);
alter table limb_adv_assessment add left_homans int(11);
alter table limb_adv_assessment add right_homans int(11);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 0, 'left_stemmers', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.left_stemmers', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 0, 'right_stemmers', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.right_stemmers', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 0, 'left_homans', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.left_homans', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES( 0, 'right_homans', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.right_homans', NULL,  67, 1,  0, 0, 1, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (111,'left_pain_assessment','STRING','CHKB',52,'pixalere.patientprofile.form.left_pain_assessment',NULL,22,0,0,46,1,NULL,NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (111,'right_pain_assessment','STRING','CHKB',52,'pixalere.patientprofile.form.right_pain_assessment',NULL,23,0,0,0,1,NULL,NULL);
INSERT INTO components (resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (19,'left_temperature_leg','INTEGER','DRP',52,'pixalere.patientprofile.form.left_temperature_leg',NULL,28,0,0,0,1,NULL,NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (19,'right_temperature_leg','INTEGER','DRP',52,'pixalere.patientprofile.form.right_temperature_leg',NULL,29,0,0,0,1,NULL,NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (19,'left_temperature_foot','INTEGER','DRP',52,'pixalere.patientprofile.form.left_temperature_foot',NULL,30,0,0,0,1,NULL,NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (19,'right_temperature_foot','INTEGER','DRP',52,'pixalere.patientprofile.form.right_temperature_foot',NULL,31,0,0,0,1,NULL,NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (19,'left_temperature_toes','INTEGER','DRP',52,'pixalere.patientprofile.form.left_temperature_toes',NULL,32,0,0,0,1,NULL,NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (19,'right_temperature_toes','INTEGER','DRP',52,'pixalere.patientprofile.form.right_temperature_toes',NULL,33,0,0,0,1,NULL,NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message,  ignore_info_popup) VALUES (0,'limb_comments','STRING','TXT',52,'pixalere.patientprofile.form.limb_comments',NULL,35,0,0,0,1,NULL,NULL);

delete from components where id=92;
insert into lookup (active,resource_id) values(1,6);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Not Adherent to Care Plan',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'referral_vascular_assessment', 'INTEGER', 'YN',11, 'pixalere.limb.form.referral_for_vascular_assessment', NULL,  70, 1, 0,  0, 0, NULL, 1);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'allowReferralByPositions','',' ','CHKB','',' ','1','0');
DROP TABLE IF EXISTS ccac_report;
CREATE TABLE ccac_report (
  id int(11) NOT NULL AUTO_INCREMENT,
  active int(11) NOT NULL DEFAULT '0',
  patient_id int(11) NOT NULL,
patient_account_id int(11) NOT NULL,
  professional_id int(11) NOT NULL,
  patient_profile_id int(11) NOT NULL,
  wound_profile_id int(11) NOT NULL,
  limb_assessment_id int(11) ,
  alpha_id int(11) NOT NULL,
  assessment_id int(11) NOT NULL,
  wound_id int(11) NOT NULL ,
  type_of_report varchar(25) default '',
  week int(11)  default '0',
  wound_status varchar(25) default '',
  discharge varchar(25) default '',
    suitable_transfer varchar(25) default '',
change_in_wound varchar(25) default '',
wound_change_comments varchar(255) default '',
wound_progress varchar(25) default '',
  date_of_transfer date ,
    already_retrieved int(11) default '0',
  correct_pathway_confirmed varchar(5) default '',
  comments text ,
  time_stamp datetime,
  PRIMARY KEY (id),
  KEY id (id),
  KEY patient_id (patient_id),
  KEY professional_id (professional_id),
  CONSTRAINT ccac_ibfk_1 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id),
  CONSTRAINT ccac_ibfk_4 FOREIGN KEY (patient_account_id) REFERENCES patient_accounts (id),
  CONSTRAINT ccac_ibfk_2 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT ccac_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
alter table professional_accounts add access_ccac_reporting int default 0;
update professional_accounts set access_ccac_reporting=1 where id=7;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'allowCCACReporting','',' ','CHKB','',' ','1','0');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacClientIndependent','allowCCACReporting==1',' ','INPT','',' ','3275','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacLongTermCompressionSystem','allowCCACReporting==1',' ','INPT','',' ','3274','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacCompressionTherapyInitiated','allowCCACReporting==1',' ','INPT','',' ','3273','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacPressureRedistributionInitiated','allowCCACReporting==1',' ','INPT','',' ','3272','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacAdheringPressureRedistribution','allowCCACReporting==1',' ','INPT','',' ','3271','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacReferralLongTermPressure','allowCCACReporting==1',' ','INPT','',' ','3270','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacWoundTherapyReassessed','allowCCACReporting==1',' ','INPT','',' ','3269','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacWoundTherapyInitiated','allowCCACReporting==1',' ','INPT','',' ','3268','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacArterialLegUlcer','allowCCACReporting==1',' ','INPT','',' ','134,138','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacDiabeticFootUlcer','allowCCACReporting==1',' ','INPT','',' ','3268','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacVenousLegUlcer','allowCCACReporting==1',' ','INPT','',' ','318,404','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacPressureUlcer','allowCCACReporting==1',' ','INPT','',' ','142,1273,1274,1275,1276','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacMalignant','allowCCACReporting==1',' ','INPT','',' ','135','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacTrauma','allowCCACReporting==1',' ','INPT','',' ','139','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacMaintenance','allowCCACReporting==1',' ','INPT','',' ','1082','1');
update lookup set locked=1 where id=135;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacRedistributionMeasures','allowCCACReporting==1',' ','INPT','',' ','3278','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacWoundRelatedSymptoms','allowCCACReporting==1',' ','INPT','',' ','3279','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacNotAdherentCarePlan','allowCCACReporting==1',' ','INPT','',' ','3280','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacClinical','allowCCACReporting==1',' ','INPT','',' ','3281,3282,3283,3284','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacSystemBarriers','allowCCACReporting==1',' ','INPT','',' ','3285,3286','1');
update lookup set locked=1 where id=134;
update lookup set locked=1 where id=182;
update lookup set locked=1 where id=139;
update lookup set locked=1 where id=138;
update lookup set locked=1 where id=1273;
update lookup set locked=1 where id=1274;
update lookup set locked=1 where id=1275;
update lookup set locked=1 where id=1276;
update lookup set locked=1 where id=318;
update lookup set locked=1 where id=404;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Pressure Redistribution Measures Initiated: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Wound Related Symptoms Managed: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Tissue Breakdown',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Allergy',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Amputation',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Infection',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,6);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Lack of supply/equipment',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,6);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Delay in available community support',resource_id from lookup order by id desc limit 1;
update resources set name='Patient Limitations' where id=89;
CREATE TABLE external_referrals (
  id int(11) NOT NULL AUTO_INCREMENT,
  patient_profile_id int(11) NOT NULL,
    external_referral_id int(11) NOT NULL,
    arranged_id int(11) NOT NULL,

  PRIMARY KEY (id),
  KEY id (id),
  CONSTRAINT extreferral_ibfk_4 FOREIGN KEY (patient_profile_id) REFERENCES patient_profiles (id)ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
insert into resources (id,name) values (195,'External Referrals');
insert into lookup (active,resource_id) values(1,195);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'RD',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,195);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'SW',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,195);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'PSW',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,195);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Chiropody',resource_id from lookup order by id desc limit 1;
insert into resources (id,name) values (196,'Arranged');
insert into lookup (active,resource_id) values(1,196);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'C.M. to arrange',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,196);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Nurse to arrange',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,196);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Client to arrange',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'target_discharge_date', 'DATE', 'DATE',3, 'pixalere.woundassessment.form.target_discharge_date', NULL,  1,  1, 0, 0, 0, NULL,  1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, ignore_info_popup) VALUES( 0, 'discharge_planning_initiated', 'INTEGER', 'YN',1, 'pixalere.nursingcareplan.form.discharge_planning_initiated', NULL,  64,  1, 0,  0, 0, NULL,  1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'chronic_disease_initiated', 'INTEGER', 'YN',1, 'pixalere.nursingcareplan.form.chronic_disease_initiated', NULL,  66, 1, 0,  0, 0, NULL,  1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 191, 'ccac_questions', 'STRING', 'CHKB',3, 'pixalere.nursingcareplan.form.ccac_questions', 27,  37,  1, 0,  0, 0, NULL,  1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'quality_of_life_addressed', 'INTEGER', 'YN',1, 'pixalere.nursingcareplan.form.quality_of_life_addressed', NULL, 68, 60,  1,  0, 0, NULL,  1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'barriers_addressed', 'INTEGER', 'YNWHY',1, 'pixalere.nursingcareplan.form.barriers_addressed', NULL,  70,  1, 0,  0, 0, NULL,  1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'root_cause_addressed', 'INTEGER', 'YNWHY',2, 'pixalere.nursingcareplan.form.root_cause_addressed', NULL,  45,  1, 0,  0, 0, NULL,  1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'system_barriers', 'INTEGER', 'YNWHY',1, 'pixalere.nursingcareplan.form.system_barriers', NULL,  72,  1, 0,  0, 0, NULL,  1);
 INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'allowMultipleGoals','',' ','CHKB','',' ','0','0');
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Wound Therapy Initiated: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Wound Therapy Reassessed: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Referral Initiated for Long-Term Pressure: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Client has obtained and is adhering to pressure redistribution system: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Pressure Redistribution Initiated: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Compression Therapy Initiated: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Referral Initiated for Long-Term Compression System: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Client is independent with Long-Term Compression System by 14 weeks: Yes',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,193);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Peripheral',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,193);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select 1,id,'Central Venous',resource_id from lookup order by id desc limit 1;
alter table patient_profiles add discharge_planning_initiated int default 0;
alter table patient_profiles add chronic_disease_initiated int default 0;
alter table nursing_care_plan add compression_therapy_initiated int default 0;
alter table nursing_care_plan add client_independent int default 0;
alter table nursing_care_plan add pressure_redistribution_measures int default 0;
alter table nursing_care_plan add wound_related_symptoms int default 0;
alter table patient_profiles add barriers_addressed int default 0;
alter table patient_profiles add barriers_addressed_comments text;
alter table wound_profiles add root_cause_addressed int default 0;
alter table wound_profiles add root_cause_addressed_comments text;
alter table patient_profiles add quality_of_life_addressed int default 0;
alter table patient_profiles add quality_of_life_addressed_comments text;
alter table patient_profiles add system_barriers_comments text;
alter table referrals_tracking add assigned_to int default -1;
alter table limb_adv_assessment add referral_vascular_assessment int default 0;
delete from components where id=92;
alter table assessment_wound add target_discharge_date date;
alter table assessment_ostomy add target_discharge_date date;
alter table assessment_drain add target_discharge_date date;
alter table assessment_skin add target_discharge_date date;
alter table assessment_incision add target_discharge_date date;
alter table assessment_burn add target_discharge_date date;
alter table user_position add orderby int default 0;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'allowReferralByPositions','',' ','CHKB','',' ','1','0');
DROP TABLE IF EXISTS ccac_report;
CREATE TABLE ccac_report (
  id int(11) NOT NULL AUTO_INCREMENT,
active int(11) NOT NULL DEFAULT '0',
  patient_id int(11) NOT NULL,
patient_account_id int(11) NOT NULL,
  professional_id int(11) NOT NULL,
  patient_profile_id int(11) NOT NULL,
  wound_profile_id int(11) NOT NULL,
  limb_assessment_id int(11),
  alpha_id int(11) NOT NULL,
  assessment_id int(11) NOT NULL,
  wound_id int(11) NOT NULL ,
  type_of_report varchar(25) default '',
  week int(11)  default '0',
  wound_status varchar(25) default '',
  discharge varchar(25) default '',
    suitable_transfer varchar(25) default '',
change_in_wound varchar(25) default '',
wound_change_comments varchar(255) default '',
wound_progress varchar(25) default '',
  date_of_transfer date ,
    already_retrieved int(11) default '0',
  correct_pathway_confirmed varchar(5) default '',
  comments text,
  time_stamp datetime,
  PRIMARY KEY (id),
  KEY id (id),
  KEY patient_id (patient_id),
  KEY professional_id (professional_id),
  CONSTRAINT ccac_ibfk_1 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id),
  CONSTRAINT ccac_ibfk_4 FOREIGN KEY (patient_account_id) REFERENCES patient_accounts (id),
  CONSTRAINT ccac_ibfk_2 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
CONSTRAINT ccac_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
update lookup set locked=1 where id=135;
update lookup set locked=1 where id=134;
update lookup set locked=1 where id=182;
update lookup set locked=1 where id=139;
update lookup set locked=1 where id=138;
update lookup set locked=1 where id=1273;
update lookup set locked=1 where id=1274;
update lookup set locked=1 where id=1275;
update lookup set locked=1 where id=1276;
update lookup set locked=1 where id=318;
update lookup set locked=1 where id=404;
update resources set name='Patient Limitations' where id=89;
update referrals_tracking set assigned_to=-1;
insert into resources (id,name) values (198,'Advanced Pain Assessment');
insert into resources (id,name) values (199,'Advanced Limb Temperature');
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui) VALUES( 0, 'right_digit_total', 'INTEGER', 'TXT', 52, 'pixalere.patientprofile.form.right_digit_total', NULL, 86, 0, 0, 0, 0, NULL, 0, NULL, 0, 1, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui) VALUES( 0, 'left_digit_total', 'INTEGER', 'TXT', 52, 'pixalere.patientprofile.form.left_digit_total', NULL, 87, 0, 0, 0, 0, NULL, 0, NULL, 0, 1, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby, required, enabled_close,  info_popup_id, allow_edit, validation,  ignore_info_popup) VALUES( 0, 'external_referrals', 'STRING', 'MDRP',1, 'pixalere.patientprofile.form.external_referral', 61,  75,  1, 0,  0, 0, NULL,  1);
update configuration set component_type='CHKB' where component_type='CHK';
update components set flowchart=60 where field_name='blood_sugar' OR field_name='blood_sugar_date' or field_name='blood_sugar_meal' or field_name='investigation' or field_name='albumin' or field_name='pre_albumin' or field_name='albumin_date' or field_name='prealbumin_date' or field_name='hga1c';
update components set orderby=2 where field_name='right_missing_limbs' and flowchart=11;
update components set orderby=4 where field_name='left_missing_limbs' and flowchart=11;
update components set orderby=6 where field_name='right_skin_colour_leg' and flowchart=11;
update components set orderby=8 where field_name='left_skin_colour_leg' and flowchart=11;
update components set orderby=10 where field_name='right_skin_colour_foot' and flowchart=11;
update components set orderby=12 where field_name='left_skin_colour_foot' and flowchart=11;
update components set orderby=14 where field_name='right_skin_colour_toes' and flowchart=11;
update components set orderby=16 where field_name='left_skin_colour_toes' and flowchart=11;
update components set orderby=18 where field_name='right_temperature_leg' and flowchart=11;
update components set orderby=20 where field_name='left_temperature_leg' and flowchart=11;
update components set orderby=22 where field_name='right_temperature_foot' and flowchart=11;
update components set orderby=24 where field_name='left_temperature_foot' and flowchart=11;
update components set orderby=26 where field_name='right_temperature_toes' and flowchart=11;
update components set orderby=28 where field_name='left_temperature_toes' and flowchart=11;
update components set orderby=29 where field_name='right_dorsalis_pedis_palpation' and flowchart=11;
update components set orderby=30 where field_name='left_dorsalis_pedis_palpation' and flowchart=11;
update components set orderby=31 where field_name='right_posterior_tibial_palpation' and flowchart=11;
update components set orderby=32 where field_name='left_posterior_tibial_palpation' and flowchart=11;
update components set orderby=33 where field_name='right_less_capillary' and flowchart=11;
update components set orderby=34 where field_name='left_less_capillary' and flowchart=11;
update components set orderby=35 where field_name='right_more_capillary' and flowchart=11;
update components set orderby=36 where field_name='left_more_capillary' and flowchart=11;
update components set orderby=38 where field_name='right_range_motion_knee' and flowchart=11;
update components set orderby=40 where field_name='left_range_motion_knee' and flowchart=11;
update components set orderby=42 where field_name='right_range_motion_ankle' and flowchart=11;
update components set orderby=44 where field_name='left_range_motion_ankle' and flowchart=11;
update components set orderby=46 where field_name='right_range_motion_toe' and flowchart=11;
update components set orderby=48 where field_name='left_range_motion_toe' and flowchart=11;
update components set orderby=50 where field_name='right_edema_location' and flowchart=11;
update components set orderby=52 where field_name='left_edema_location' and flowchart=11;
update components set orderby=54 where field_name='right_edema_severity' and flowchart=11;
update components set orderby=56 where field_name='left_edema_severity' and flowchart=11;
update components set orderby=79 where field_name='sleep_positions' and flowchart=11;
update components set orderby=60 where field_name='right_ankle' and flowchart=11;
update components set orderby=62 where field_name='left_ankle' and flowchart=11;
update components set orderby=64 where field_name='right_midcalf' and flowchart=11;
update components set orderby=66 where field_name='left_midcalf' and flowchart=11;
update components set orderby=68 where field_name='right_skin_assessment' and flowchart=11;
update components set orderby=70 where field_name='left_skin_assessment' and flowchart=11;
update components set orderby=72 where field_name='right_sensory' and flowchart=11;
update components set orderby=74 where field_name='left_sensory' and flowchart=11;
update components set orderby=76 where field_name='right_pain_assessment' and flowchart=11;
update components set orderby=78 where field_name='left_pain_assessment' and flowchart=11;
update components set orderby=2 where field_name='right_missing_limbs' and flowchart=53;
update components set orderby=4 where field_name='left_missing_limbs' and flowchart=53;
update components set orderby=6 where field_name='right_skin_colour_arm' and flowchart=53;
update components set orderby=8 where field_name='left_skin_colour_arm' and flowchart=53;
update components set orderby=10 where field_name='right_skin_colour_hand' and flowchart=53;
update components set orderby=12 where field_name='left_skin_colour_hand' and flowchart=53;
update components set orderby=14 where field_name='right_skin_colour_fingers' and flowchart=53;
update components set orderby=16 where field_name='left_skin_colour_fingers' and flowchart=53;
update components set orderby=18 where field_name='right_skin_temperature_arm' and flowchart=53;
update components set orderby=20 where field_name='left_skin_temperature_arm' and flowchart=53;
update components set orderby=22 where field_name='right_skin_temperature_hand' and flowchart=53;
update components set orderby=24 where field_name='left_skin_temperature_hand' and flowchart=53;
update components set orderby=26 where field_name='right_skin_temperature_fingers' and flowchart=53;
update components set orderby=28 where field_name='left_skin_temperature_fingers' and flowchart=53;
update components set orderby=30 where field_name='right_less_capillary' and flowchart=53;
update components set orderby=32 where field_name='left_less_capillary' and flowchart=53;
update components set orderby=34 where field_name='right_more_capillary' and flowchart=53;
update components set orderby=36 where field_name='left_more_capillary' and flowchart=53;
update components set orderby=38 where field_name='right_range_motion_arm' and flowchart=53;
update components set orderby=40 where field_name='left_range_motion_arm' and flowchart=53;
update components set orderby=42 where field_name='right_range_motion_hand' and flowchart=53;
update components set orderby=44 where field_name='left_range_motion_hand' and flowchart=53;
update components set orderby=46 where field_name='right_range_motion_fingers' and flowchart=53;
update components set orderby=48 where field_name='left_range_motion_fingers' and flowchart=53;
update components set orderby=50 where field_name='right_edema_location' and flowchart=53;
update components set orderby=52 where field_name='left_edema_location' and flowchart=53;
update components set orderby=54 where field_name='right_edema_severity' and flowchart=53;
update components set orderby=56 where field_name='left_edema_severity' and flowchart=53;
update components set orderby=58 where field_name='sleep_positions' and flowchart=53;
update components set orderby=60 where field_name='right_elbow' and flowchart=53;
update components set orderby=62 where field_name='left_elbow' and flowchart=53;
update components set orderby=64 where field_name='right_wrist' and flowchart=53;
update components set orderby=66 where field_name='left_wrist' and flowchart=53;
update components set orderby=68 where field_name='right_skin_assessment' and flowchart=53;
update components set orderby=70 where field_name='left_skin_assessment' and flowchart=53;
update components set orderby=72 where field_name='right_sensory' and flowchart=53;
update components set orderby=74 where field_name='left_sensory' and flowchart=53;
update components set orderby=76 where field_name='right_pain_assessment' and flowchart=53;
update components set orderby=78 where field_name='left_pain_assessment' and flowchart=53;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui) VALUES ( 0, 'limb_comments', 'STRING', 'TXT', 11, 'pixalere.patientprofile.form.limb_comments', NULL, 85, 0, 0, 0, 1, NULL, NULL, NULL, 0, NULL, 0);
update components set flowchart=28 where field_name='left_api_score' and flowchart=11;
update components set flowchart=26 where field_name='right_api_score' and flowchart=11;
delete from components where field_name='left_api_score' and flowchart=52;
delete from components  where field_name='right_api_score' and flowchart=52;
update components set orderby=2 where field_name='right_dorsalis_pedis_doppler' and flowchart=52;
update components set orderby=4 where field_name='left_dorsalis_pedis_doppler' and flowchart=52;
update components set orderby=6 where field_name='right_tibial_pedis_doppler' and flowchart=52;
update components set orderby=8 where field_name='left_tibial_pedis_doppler' and flowchart=52;
update components set orderby=10 where field_name='ankle_brachial_lab' and flowchart=52;
update components set orderby=12 where field_name='ankle_brachial_date' and flowchart=52;
update components set orderby=14 where field_name='right_dorsalis_pedis_ankle_brachial' and flowchart=52;
update components set orderby=16 where field_name='left_dorsalis_pedis_ankle_brachial' and flowchart=52;
update components set orderby=18 where field_name='right_tibial_pedis_ankle_brachial' and flowchart=52;
update components set orderby=20 where field_name='left_tibial_pedis_ankle_brachial' and flowchart=52;
update components set orderby=22 where field_name='right_ankle_brachial' and flowchart=52;
update components set orderby=24 where field_name='left_ankle_brachial' and flowchart=52;
update components set orderby=26 where field_name='right_api_score' and flowchart=52;
update components set orderby=28 where field_name='left_api_score' and flowchart=52;
update components set orderby=30,flowchart=52 where field_name='referral_vascular_assessment' and flowchart=11;
update components set orderby=32 where field_name='toe_brachial_lab' and flowchart=52;
update components set orderby=34 where field_name='toe_brachial_date' and flowchart=52;
update components set orderby=36 where field_name='right_toe_pressure' and flowchart=52;
update components set orderby=38 where field_name='left_toe_pressure' and flowchart=52;
update components set orderby=40 where field_name='right_brachial_pressure' and flowchart=52;
update components set orderby=42 where field_name='left_brachial_pressure' and flowchart=52;
update components set orderby=44 where field_name='right_tbi_score' and flowchart=52;
update components set orderby=46 where field_name='left_tbi_score' and flowchart=52;
update components set orderby=44 where field_name='right_sensation' and flowchart=52;
update components set orderby=50 where field_name='left_sensation' and flowchart=52;
update components set orderby=52 where field_name='right_score_sensation' and flowchart=52;
update components set orderby=54 where field_name='left_score_sensation' and flowchart=52;
update components set orderby=56 where field_name='right_digit_total' and flowchart=52;
update components set orderby=58 where field_name='left_digit_total' and flowchart=52;
update components set orderby=60 where field_name='right_stemmers' and flowchart=52;
update components set orderby=62 where field_name='left_stemmers' and flowchart=52;
update components set orderby=64 where field_name='right_homans' and flowchart=52;
update components set orderby=66 where field_name='left_homans' and flowchart=52;
update components set orderby=68 where field_name='right_foot_deformities' and flowchart=52;
update components set orderby=70 where field_name='left_foot_deformities' and flowchart=52;
update components set orderby=72 where field_name='right_foot_skin' and flowchart=52;
update components set orderby=74 where field_name='left_foot_skin' and flowchart=52;
update components set orderby=76 where field_name='right_pain_assessment' and flowchart=52;
update components set orderby=78 where field_name='left_pain_assessment' and flowchart=52;
delete from components where field_name='doppler_lab' and flowchart=52;
delete from components where field_name='left_interdigitial_doppler' and flowchart=52;
delete from components where field_name='right_interdigitial_doppler' and flowchart=52;
update components set orderby=6,flowchart=12 where field_name='right_foot_toes' and flowchart=52;
update components set orderby=8,flowchart=12 where field_name='left_foot_toes' and flowchart=52;
delete from components where field_name='transcutaneous_lab' and flowchart=52;
delete from components where field_name='transcutaneous_date' and flowchart=52;
delete from components where field_name='right_transcutaneous_pressure' and flowchart=52;
delete from components where field_name='left_transcutaneous_pressure' and flowchart=52;
update components set orderby=2 where field_name='right_foot_toes' and flowchart=12;
update components set orderby=4 where field_name='left_foot_toes' and flowchart=12;
update components set orderby=6 where field_name='weight_bearing' and flowchart=12;
update components set orderby=8 where field_name='balance' and flowchart=12;
update components set orderby=10 where field_name='calf_muscle_pump' and flowchart=12;
update components set orderby=12 where field_name='calf_muscle_comments' and flowchart=12;
update components set orderby=14 where field_name='mobility_aids' and flowchart=12;
update components set orderby=16 where field_name='mobility_aids_comments' and flowchart=12;
update components set orderby=20 where field_name='gait_pattern_comments' and flowchart=12;
update components set orderby=22 where field_name='walking_distance_comments' and flowchart=12;
update components set orderby=24 where field_name='walking_endurance_comments' and flowchart=12;
update components set orderby=26 where field_name='indoor_footwear' and flowchart=12;
update components set orderby=28 where field_name='indoor_footwear_comments' and flowchart=12;
update components set orderby=30 where field_name='outdoor_footwear' and flowchart=12;
update components set orderby=32 where field_name='outdoor_footwear_comments' and flowchart=12;
update components set orderby=34 where field_name='orthotics' and flowchart=12;
update components set orderby=36 where field_name='orthotics_comments' and flowchart=12;
update components set orderby=38 where field_name='muscle_tone_functional' and flowchart=12;
update components set orderby=40 where field_name='arches_foot_structure' and flowchart=12;
update components set orderby=42 where field_name='supination_foot_structure' and flowchart=12;
update components set orderby=44 where field_name='pronation_foot_structure' and flowchart=12;
update components set orderby=46 where field_name='dorsiflexion_active' and flowchart=12;
update components set orderby=48 where field_name='dorsiflexion_passive' and flowchart=12;
update components set orderby=50 where field_name='plantarflexion_active' and flowchart=12;
update components set orderby=52 where field_name='plantarflexion_passive' and flowchart=12;
update components set orderby=54 where field_name='greattoe_active' and flowchart=12;
update components set orderby=56 where field_name='greattoe_passive' and flowchart=12;
update components set orderby=58 where field_name='right_proprioception' and flowchart=12;
update components set orderby=70 where field_name='left_proprioception' and flowchart=12;
update components set orderby=72 where field_name='foot_comments' and flowchart=12;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,0,'referral_required','','','CHKB','','','1',0);
update account_assignments set time_stamp=FROM_UNIXTIME(time_stamp);
alter table account_assignments change column time_stamp created_on  datetime ;
update assessment_burn set last_update=FROM_UNIXTIME(last_update);
alter table assessment_burn change column  last_update created_on datetime after wound_profile_type_id;
update assessment_wound set last_update=FROM_UNIXTIME(last_update);
alter table assessment_wound change column  last_update created_on datetime after wound_profile_type_id;
update assessment_drain set last_update=FROM_UNIXTIME(last_update);
alter table assessment_drain change column  last_update created_on datetime after wound_profile_type_id;
update assessment_incision set last_update=FROM_UNIXTIME(last_update);
alter table assessment_incision change column  last_update created_on datetime after wound_profile_type_id;
update assessment_ostomy set last_update=FROM_UNIXTIME(last_update);
alter table assessment_ostomy change column  last_update created_on datetime after wound_profile_type_id;
update assessment_skin set last_update=FROM_UNIXTIME(last_update);
alter table assessment_skin change column  last_update created_on datetime after wound_profile_type_id;
alter table assessment_burn add lastmodified_on datetime  default NULL after created_on;
alter table assessment_wound add lastmodified_on datetime default NULL  after created_on;
alter table assessment_drain add lastmodified_on datetime  default NULL after created_on;
alter table assessment_incision add lastmodified_on datetime  default NULL after created_on;
alter table assessment_ostomy add lastmodified_on datetime  default NULL after created_on;
alter table assessment_skin add lastmodified_on datetime default NULL  after created_on;
alter table assessment_comments change column time_stamp time_stamp varchar(255);
update assessment_comments set time_stamp=FROM_UNIXTIME(time_stamp);
alter table assessment_comments change column  time_stamp created_on  datetime ;
alter table assessment_comments change column user_signature user_signature varchar(255) after created_on;
alter table assessment_comments add deleted_on datetime  default NULL after created_on;
alter table assessment_burn add deleted_on datetime  default NULL after created_on;
alter table assessment_antibiotics change column start_date start_date date;
alter table assessment_antibiotics change column end_date end_date date;
alter table assessment_wound add deleted_on datetime  default NULL after created_on;
alter table assessment_drain add deleted_on datetime  default NULL after created_on;
alter table assessment_incision add deleted_on datetime default NULL  after created_on;
alter table assessment_ostomy add deleted_on datetime  default NULL after created_on;
alter table assessment_skin add deleted_on datetime default NULL  after created_on;
update braden set last_update=FROM_UNIXTIME(last_update);
alter table braden change column  last_update created_on datetime after current_flag;
alter table braden add deleted_on datetime after created_on;
update braden set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table braden change column  data_entry_timestamp lastmodified_on  datetime after created_on;
update diagnostic_investigation set creation_date=FROM_UNIXTIME(creation_date);
alter table diagnostic_investigation change column  creation_date created_on  datetime after patient_id;
update diagnostic_investigation set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table diagnostic_investigation change column  data_entry_timestamp lastmodified_on  datetime after created_on;
alter table diagnostic_investigation add deleted_on datetime default NULL  after created_on;
update dressing_change_frequency set time_stamp=FROM_UNIXTIME(time_stamp);
alter table dressing_change_frequency change column  time_stamp created_on  datetime after wound_profile_type_id;
alter table dressing_change_frequency add deleted_on datetime  default NULL after created_on;
alter table dressing_change_frequency change column dcf dcf int after deleted_on;
update foot_assessment set last_update=FROM_UNIXTIME(last_update);
alter table foot_assessment change column  last_update created_on   datetime after patient_id;
update foot_assessment set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table foot_assessment change column data_entry_timestamp lastmodified_on  datetime after created_on;
alter table foot_assessment add deleted_on datetime  default NULL after lastmodified_on;
update limb_adv_assessment set last_update=FROM_UNIXTIME(last_update);
alter table limb_adv_assessment change column  last_update created_on  datetime after patient_id;
update limb_adv_assessment set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table limb_adv_assessment change column  data_entry_timestamp lastmodified_on datetime after created_on;
alter table limb_adv_assessment add deleted_on datetime  default NULL after lastmodified_on;
update limb_basic_assessment set last_update=FROM_UNIXTIME(last_update);
alter table limb_basic_assessment change column  last_update created_on datetime after patient_id;
update limb_basic_assessment set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table limb_basic_assessment change column  data_entry_timestamp lastmodified_on datetime after created_on;
alter table limb_basic_assessment add deleted_on datetime default NULL  after lastmodified_on ;;
update limb_upper_assessment set last_update=FROM_UNIXTIME(last_update);
alter table limb_upper_assessment change column  last_update created_on datetime after patient_id;
update limb_upper_assessment set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table limb_upper_assessment change column  data_entry_timestamp lastmodified_on datetime after created_on;
alter table limb_upper_assessment add deleted_on datetime default NULL  after lastmodified_on;
update nursing_care_plan set time_stamp=FROM_UNIXTIME(time_stamp);
alter table nursing_care_plan change column  time_stamp created_on datetime after wound_profile_type_id;
alter table nursing_care_plan add deleted_on datetime default NULL  after created_on;
update patient_profiles set last_update=FROM_UNIXTIME(last_update);
alter table patient_profiles change column  last_update created_on datetime after patient_id;
update patient_profiles set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table patient_profiles change column  data_entry_timestamp lastmodified_on datetime after created_on;
alter table patient_profiles add deleted_on datetime  default NULL after lastmodified_on;
update wound_profiles set last_update=FROM_UNIXTIME(last_update);
alter table wound_profiles change column  last_update created_on datetime after patient_id;
update wound_profiles set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table wound_profiles change column  data_entry_timestamp lastmodified_on datetime after created_on;
alter table wound_profiles add deleted_on datetime  default NULL after lastmodified_on;
update physical_exam set last_update=FROM_UNIXTIME(last_update);
alter table physical_exam change column  last_update created_on  datetime after patient_id;
alter table physical_exam add deleted_on datetime  default NULL after created_on;
update referrals_tracking set time_stamp=FROM_UNIXTIME(time_stamp);
alter table referrals_tracking change column  time_stamp created_on datetime after patient_id;
update treatment_comments set time_stamp=FROM_UNIXTIME(time_stamp);
alter table treatment_comments change column  time_stamp created_on datetime after wound_profile_type_id;
alter table treatment_comments add deleted_on datetime  default NULL after created_on;
update wound_assessment set time_stamp=FROM_UNIXTIME(time_stamp);
alter table wound_assessment change column  time_stamp created_on datetime after professional_id;
update wound_assessment set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table wound_assessment change column  data_entry_timestamp lastmodified_on datetime after created_on;
update wound_assessment_location set data_entry_timestamp=FROM_UNIXTIME(data_entry_timestamp);
alter table wound_assessment_location change column  data_entry_timestamp lastmodified_on datetime after wound_profile_type_id;
alter table wound_profile_type add deleted_on datetime  default NULL after wound_id;
update wound_profile_type set closed_date=FROM_UNIXTIME(closed_date);
alter table wound_assessment_location add deleted_on datetime  default NULL after wound_id;
alter table wound_profile_type change column closed_date closed_date  datetime ;
alter table wounds add deleted_on datetime after patient_id;
alter table assessment_burn change column active active int after lastmodified_on;
alter table assessment_wound change column active active int after lastmodified_on;
alter table assessment_drain change column active active int after lastmodified_on;
alter table assessment_incision change column active active int after lastmodified_on;
alter table assessment_ostomy change column active active int after lastmodified_on;
alter table assessment_skin change column active active int after lastmodified_on;
alter table assessment_comments change column referral_id referral_id int after professional_id;
alter table assessment_comments change column position_id position_id int after professional_id;
alter table assessment_comments change column body body text after comment_type;
alter table assessment_wound change column status status int after lastmodified_on;
alter table assessment_ostomy change column status status int after lastmodified_on;
alter table assessment_drain change column status status int after lastmodified_on;
alter table assessment_incision change column status status int after lastmodified_on;
alter table assessment_skin change column status status int after lastmodified_on;
alter table diagnostic_investigation drop column last_modified_date;
alter table assessment_antibiotics change column start_date start_date  datetime ;
alter table assessment_antibiotics change column end_date end_date  datetime ;
update assessment_burn set closed_date=FROM_UNIXTIME(closed_date);
alter table assessment_burn change column closed_date closed_date date;
update assessment_burn set wound_date=FROM_UNIXTIME(wound_date);
alter table assessment_burn change column wound_date wound_date date;
update assessment_drain set tubes_changed_date=FROM_UNIXTIME(tubes_changed_date);
alter table assessment_drain change column tubes_changed_date tubes_changed_date date;
update assessment_drain set additional_days_date1=FROM_UNIXTIME(additional_days_date1);
alter table assessment_drain change column additional_days_date1 additional_days_date1 date;
update assessment_drain set additional_days_date2=FROM_UNIXTIME(additional_days_date2);
alter table assessment_drain change column additional_days_date2 additional_days_date2 date;
update assessment_drain set additional_days_date3=FROM_UNIXTIME(additional_days_date3);
alter table assessment_drain change column additional_days_date3 additional_days_date3 date;
update assessment_drain set additional_days_date4=FROM_UNIXTIME(additional_days_date4);
alter table assessment_drain change column additional_days_date4 additional_days_date4 date;
update assessment_drain set additional_days_date5=FROM_UNIXTIME(additional_days_date5);
alter table assessment_drain change column additional_days_date5 additional_days_date5 date;
update assessment_incision set clinical_path_date=FROM_UNIXTIME(clinical_path_date);
alter table assessment_incision change column clinical_path_date clinical_path_date date;
update assessment_incision set surgery_date=FROM_UNIXTIME(surgery_date);
alter table assessment_incision change column surgery_date surgery_date date;
alter table assessment_npwt change column vac_start_date vac_start_date date;
alter table assessment_npwt change column vac_end_date vac_end_date date;
update assessment_ostomy set clinical_path_date=FROM_UNIXTIME(clinical_path_date);
alter table assessment_ostomy change column clinical_path_date clinical_path_date date;
update assessment_skin set wound_date=FROM_UNIXTIME(wound_date);
alter table assessment_skin change column wound_date wound_date date;
alter table assessment_treatment change column cs_date cs_date date;
update assessment_drain set closed_date=FROM_UNIXTIME(closed_date);
alter table assessment_drain change column closed_date closed_date date;
update assessment_incision set closed_date=FROM_UNIXTIME(closed_date);
alter table assessment_incision change column closed_date closed_date date;
update assessment_ostomy set closed_date=FROM_UNIXTIME(closed_date);
alter table assessment_ostomy change column closed_date closed_date date;
update assessment_wound set closed_date=FROM_UNIXTIME(closed_date);
alter table assessment_wound change column closed_date closed_date date;
update assessment_wound set wound_date=FROM_UNIXTIME(wound_date);
alter table assessment_wound change column wound_date wound_date date;
update diagnostic_investigation set albumin_date=FROM_UNIXTIME(albumin_date);
alter table diagnostic_investigation change column albumin_date albumin_date date;
update diagnostic_investigation set prealbumin_date=FROM_UNIXTIME(prealbumin_date);
alter table diagnostic_investigation change column prealbumin_date prealbumin_date date;
update limb_adv_assessment set ankle_brachial_date=FROM_UNIXTIME(ankle_brachial_date);
alter table limb_adv_assessment change column ankle_brachial_date ankle_brachial_date date;
update limb_adv_assessment set toe_brachial_date=FROM_UNIXTIME(toe_brachial_date);
alter table limb_adv_assessment change column toe_brachial_date toe_brachial_date date;
update limb_adv_assessment set transcutaneous_date=FROM_UNIXTIME(transcutaneous_date);
alter table limb_adv_assessment change column transcutaneous_date transcutaneous_date date;
alter table patient_investigations change column investigation_date investigation_date date;
update patient_profiles set start_date=FROM_UNIXTIME(start_date);
alter table patient_profiles change column start_date start_date date;
update wound_assessment_location set open_time_stamp=FROM_UNIXTIME(open_time_stamp);
alter table wound_assessment_location change column open_time_stamp open_time_stamp  datetime ;
update wound_assessment_location set close_time_stamp=FROM_UNIXTIME(close_time_stamp);
alter table wound_assessment_location change column close_time_stamp close_time_stamp  datetime ;
update wound_profiles set date_surgery=FROM_UNIXTIME(date_surgery);
alter table wound_profiles change column date_surgery date_surgery date;
update wound_profiles set final_tbsa=FROM_UNIXTIME(final_tbsa);
alter table wound_profiles change column final_tbsa final_tbsa date;
update wound_profiles set preliminary_tbsa=FROM_UNIXTIME(preliminary_tbsa);
alter table wound_profiles change column preliminary_tbsa preliminary_tbsa date;
alter table report_queue change column timestamp created_on datetime;
alter table report_queue change column start_date start_date date;
alter table report_queue change column end_date end_date date;
update logs_access set time_stamp=FROM_UNIXTIME(time_stamp);
alter table logs_access change column time_stamp created_on  datetime ;
update patient_accounts set time_stamp=FROM_UNIXTIME(time_stamp);
alter table patient_accounts change column time_stamp created_on  datetime ;
update password_history set time_stamp=FROM_UNIXTIME(time_stamp);
alter table password_history change column time_stamp created_on  datetime ;
update patients set time_stamp=FROM_UNIXTIME(time_stamp);
alter table patients change column time_stamp created_on  datetime ;
update nursing_fixes set time_stamp=FROM_UNIXTIME(time_stamp);
alter table nursing_fixes change column time_stamp created_on  datetime ;
update components set field_type='DATE' where component_type='DATE';
update components set field_name='created_on' where field_name='creation_date';
update components set field_name='lastmodified_on' where field_name='last_modified_date';
alter table professional_accounts change column creation_date created_on  datetime ;
alter table professional_accounts change column updated_date lastmodified_on  datetime ;
alter table professional_accounts change column deletion_date deleted_on  datetime ;
alter table components add num_columns int default NULL;
update components set num_columns=1 where component_type='CHKB' or component_type='RADO';
update components set num_columns=3 where field_name='appearance' or field_name='pouching' or field_name='profile' or field_name='stool_quantity' or field_name='urine_quantity';
update components set num_columns=2 where field_name='interfering_meds' or field_name='patient_limitations' or field_name='medications_comments' or field_name='drain_characteristics' or field_name='peri_drain_area' or field_name='drain_site' or   field_name='incision_status' or field_name='incision_closure_methods' or  field_name='peri_incisional' or field_name='incision_exudate' or  field_name='postop_management' or field_name='exuduate_type' or field_name='exudate_amount' or field_name='wound_edge' or field_name='periwound_skin' or field_name='nutritional_status' or field_name='mucocutaneous_margin' or field_name='urine_colour' or field_name='periostomy_skin' ;
update components set num_columns=-1 where  field_name='construction' or field_name='shape' or field_name='devices';
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'ncpName','',' ','DRP','pixalere.treatment.form.nursing_care_plan,pixalere.treatment.form.treatment_plan','pixalere.treatment.form.nursing_care_plan,pixalere.treatment.form.treatment_plan','pixalere.treatment.form.nursing_care_plan','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'ncpTemplate','',' ','TEXT','',' ','','0');
alter table lookup change column other other int(1) default 0;
update lookup set other=0 where other IS NULL;
insert into resources (id,name) values (165,'Medications that Affect Wound Healing');
alter table wounds change column closed_date closed_date  datetime;
alter table wound_profiles add visited_on datetime;
update wound_profiles set visited_on=created_on;
alter table wound_assessment add visited_on datetime;
update wound_assessment set visited_on=created_on;
alter table assessment_wound add visited_on datetime;
update assessment_wound set visited_on=created_on;
alter table assessment_drain add visited_on datetime;
update assessment_drain set visited_on=created_on;
alter table assessment_incision add visited_on datetime;
update assessment_incision set visited_on=created_on;
alter table assessment_ostomy add visited_on datetime;
update assessment_ostomy set visited_on=created_on;
alter table patient_profiles add visited_on datetime;
update patient_profiles set visited_on=created_on;
alter table limb_adv_assessment add visited_on datetime;
update limb_adv_assessment set visited_on=created_on;
alter table limb_basic_assessment add visited_on datetime;
update limb_basic_assessment set visited_on=created_on;
alter table limb_upper_assessment add visited_on datetime;
update limb_upper_assessment set visited_on=created_on;
alter table physical_exam add visited_on datetime;
update physical_exam set visited_on=created_on;
alter table diagnostic_investigation add visited_on datetime;
update diagnostic_investigation set visited_on=created_on;
alter table braden add visited_on datetime;
update braden set visited_on=created_on;
alter table nursing_care_plan add visited_on datetime;
update nursing_care_plan set visited_on=created_on;
alter table assessment_comments add visited_on datetime;
update assessment_comments set visited_on=created_on;
update configuration set current_value=0 where config_setting='allowReferralByPositions';
update configuration set current_value=0 where config_setting='allowCCACReporting';
alter table assessment_skin add visited_on datetime;
update assessment_skin set visited_on=created_on;
alter table assessment_burn add visited_on datetime;
update assessment_burn set visited_on=created_on;
alter table treatment_comments add visited_on datetime;
update treatment_comments set visited_on=created_on;
alter table foot_assessment add visited_on datetime;
update foot_assessment set visited_on=created_on;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',3, 'pixalere.common.visited_on', NULL,  5,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',4, 'pixalere.common.visited_on', NULL,  6,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',5, 'pixalere.common.visited_on', NULL,  3,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',6, 'pixalere.common.visited_on', NULL,  3,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',17, 'pixalere.common.visited_on', NULL,  5,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',50, 'pixalere.common.visited_on', NULL,  5,  0, 0, 0, 0, NULL, 0);
alter table assessment_antibiotics drop column alpha_id;
update components set allow_edit=1 where field_name='professionals';
update components set allow_edit=1 where field_name='surgical_history';
update components set allow_edit=1 where field_name='external_referrals';
update components set allow_edit=1 where field_name='co_morbidities';
update lookup_l10n set name='Training Area' where name='Sample Area';
update lookup_l10n set name='Training Location' where name='Sample Location';
update lookup set locked=1 where id=990 or id=612;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,0,'webservice_password','','','INPT','','','p1x.Srv!@');
update lookup_l10n set name='+1 Trace' where name='+1 Trace 2mm pitting';
update lookup_l10n set name='+2 Trace' where name='+2 Trace 4mm pitting';
update lookup_l10n set name='+3 Trace' where name='+3 Trace 6mm pitting';
update lookup_l10n set name='+4 Trace' where name='+4 Trace 8mm pitting';
delete from components where field_name='left_homans' or field_name='right_homans';
update components set label_key='pixalere.limbassessment.form.referral_for_vascular_assessment' where label_key='pixalere.limb.form.referral_for_vascular_assessment';
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 198, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id, 'No pain', 198 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 198, 0, 12, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id, 'Non verbal response', 198 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES 	( 1, 198, 0, 11, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id,'With deep palpation', 198 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES 	( 1, 198, 0, 10, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id, 'Intermittent claudication', 198 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES 	( 1, 198, 0, 9, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id, 'Intermittent', 198 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES 	( 1, 198, 0, 8, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id, 'Continuous', 198 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES 	( 1, 198, 0, 7, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id, 'Knife-like', 198 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES 	( 1, 198, 0, 6, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select 1 ,id, 'Ache', 198 from lookup order by id desc limit 1;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'from_email','',' ','INPT','',' ','sendmail@pixalere.com','0');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'userStats','',' ','INPT','',' ','0','0');
update components set allow_edit=1 where field_name='visited_on';
delete from components where flowchart=52 and field_name LIKE '%_temperature_%';
update components set field_type='INTEGER' where field_name='right_ankle_brachial' or field_name='left_ankle_brachial';
update components set allow_edit=1 where field_name='referral_vascular_assessment';
update components set resource_id=111 where field_name='left_pain_assessment' and flowchart=52;
update components set resource_id=111 where field_name='right_pain_assessment' and flowchart=52;
delete from components where flowchart=12 and field_name='left_foot_toes';
delete from components where flowchart=12 and field_name='right_foot_toes';
alter table limb_adv_assessment add limb_comments text;
update components set label_key='pixalere.limbassessment.form.referral_for_vascular_assessment' where flowchart=52 and field_name='referral_vascular_assessment';
insert into resources (id,name) values (200,'Limb Shape');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 200, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Champagne-bottle shaped leg', 200 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 200, 0, 2, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Wasted calf muscle', 200 from lookup order by id desc  limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 200, 0, 3, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Not applicable', 200 from lookup order by id desc  limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 200, 'left_limb_shape', 'STRING', 'CHKB',52, 'pixalere.patientprofile.form.left_limb_shape', NULL,  85,  0, 0, 0, 1, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 200, 'right_limb_shape', 'STRING', 'CHKB',52, 'pixalere.patientprofile.form.right_limb_shape', NULL,  86,  0, 0, 0, 1, NULL, 0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'hidePatientsFromSearch','','','CHKB','','','0');
update wound_assessment_location set close_time_stamp=(select visited_on from assessment_wound where alpha_id=wound_assessment_location.id and status=(select current_value from configuration where config_setting='woundActive') order by visited_on desc limit 1) where discharge=1 and exists(select id from assessment_wound where alpha_id=wound_assessment_location.id and status=(select current_value from configuration where config_setting='woundActive') and visited_on>close_time_stamp order by visited_on desc limit 1);
update configuration set selfadmin=1 where config_setting='passwordExpiring' or config_setting='passwordExpiring';
alter table limb_adv_assessment add left_limb_shape text;
alter table limb_adv_assessment add right_limb_shape text;
alter table assessment_npwt  add vendor_name int;
alter table assessment_npwt add machine_acquirement int;
update wounds set status='Closed' where active=1 and status IS NULL and NOT EXISTS(select id from wound_profile_type where closed!=1 and wound_id=wounds.id) and EXISTs (select id from wound_profile_type where wound_id=wounds.id);
update assessment_wound set closed_date=NULL where closed_date='1970-01-01';
update components set required=0 where field_name='pain_comments';
insert into resources (id,name) values (201,'NPWT Vendor');
insert into lookup (active,resource_id) values(1,201);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select  1,id,'KCI',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,201);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select  1,id,'S&N',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0,1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 201, 'vendor_name', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.vendor_name', 51,  67, 1,  0, 0, 1, NULL);
insert into resources (id,name) values (202,'NPWT Machine');
insert into lookup (active,resource_id) values(1,202);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select  1,id,'Owned',resource_id from lookup order by id desc limit 1;
insert into lookup (active,resource_id) values(1,202);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select  1,id,'Rented',resource_id from lookup order by id desc limit 1;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',3, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',4, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',5, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',6, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',17, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0,1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 202, 'machine_acquirement', 'INTEGER', 'RADO',50, 'pixalere.treatment.form.machine_acquirement', 51,  68, 1,  0, 0, 1, NULL);
delete from components where field_name='left_api_score' and flowchart=53;
delete from components where field_name='right_api_score' and flowchart=53;
update components set flowchart=52 where field_name='left_api_score' OR field_name='right_api_score';
update components set orderby=26 where field_name='right_api_score';
update components set orderby=28 where field_name='right_api_score';
update components set orderby=45 where field_name='right_tbi_score';
update components set orderby=32 where field_name='right_temperature_fingers';
update components set orderby=34 where field_name='left_less_capillary';
update components set orderby=33 where field_name='right_less_capillary';
update components set orderby=35 where field_name='right_more_capillary';
update configuration set category=2,orderby=35 where config_setting='webservice_password';
update configuration set category=2,orderby=34,component_options=',PCC Interface,HL7 Web Service,Flat File (Paris/Procura),XML Web Service',component_values=',pcc_webservice,hl7_webservice,flatfile,xml_webservice' where config_setting='integration';
update components set hide_flowchart=1,hide_gui=1 where field_name='interfering_meds_other'  OR field_name='hga1c';
update components set allow_edit=1 where field_name='referral_vascular_assessment' or field_name='wound_acquired' OR field_name='np_connector' OR field_name='initiated_by' or field_name='reason_for_ending' or field_name='goal_of_therapy';
update components set validation='var c =0;var perc=0;for(i=0;i<document.form.woundbase.length;i++){if(document.form.woundbase[i].checked==true){if(document.getElementById("woundbase_percentage_"+(i+1)).selectedIndex == -1 || document.getElementById("woundbase_percentage_"+(i+1)).selectedIndex == 0){error=1;}var percentage = document.getElementById("woundbase_percentage_"+(i+1)).options[document.getElementById("woundbase_percentage_"+(i+1)).selectedIndex].value; if(c == 0){perc=""+percentage;}else{perc=perc+","+percentage; }c++;}}document.form.woundbase_percentage.value=perc;if(calcTotalWoundBed() != 100 && calcTotalWoundBed() != 0){themessage=themessage + "pixalere.woundassessment.error.percentage,";error=1;}' where field_name='wound_base';
update components set validation_message='wound_date_year: \'Valid Date only\'',validation='wound_date_year : {canadianDate : {data: ["#wound_date_day","#wound_date_month","#wound_date_year"]}},wound_date_day:"required",wound_date_month:"required"' where field_name='wound_date';
update components set validation_message='date_surgery_year: \'Valid Date only\'',validation='date_surgery_year : {canadianDate : {data: ["#date_surgery_day","#date_surgery_month","#date_surgery_year"]}}' where field_name='date_surgery';
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (0,'limb_comments','STRING','TXT',53,'pixalere.patientprofile.form.limb_comments',NULL,90,0,0,0,1,NULL, 0);
INSERT INTO components (resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns) VALUES ( 117, 'right_foot_toes', 'STRING', 'CHKB', 52, 'pixalere.patientprofile.form.right_toes', NULL, 74, 0, 0, 0, 1, NULL, 0, NULL, 0, 1, 0, 1),(117, 'left_foot_toes', 'STRING', 'CHKB', 52, 'pixalere.patientprofile.form.left_toes', NULL, 75, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, 1);
update components set orderby=73 where field_name='left_foot_skin' and flowchart=52;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 169, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Treatment Complete', 169 from lookup order by id desc limit 1;
update components set validation='time:{time:true},visited_on_year : {canadianDate : {data: ["#visited_on_day","#visited_on_month","#visited_on_year"]}},visited_on_day:"required",visited_on_month:"required"',validation_message='time : "Time must be in format 23:59",visited_on_year : "Visited date must be valid"' where field_name='visited_on';
alter table diagnostic_investigation add purs_score int default 0;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart,validation_message) VALUES (0,'purs_score','INTEGER','TXT',60,'pixalere.patientprofile.form.purs_score',NULL,16,1,0,0,0,NULL,0,'purs_score: "pixalere.patientprofile.form.purs_score.error"');
update components set label_key='pixalere.patientprofile.form.health_care_professional_contact' where field_name='professionals';
update configuration set current_value=0 where config_setting='deletePatientsOnSync';
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 99, 35, 1, 'deletePatientOnUpload', '', '', 'CHKB', '', '', '0');
update lookup_l10n set name='PGR Referral 1-7 days' where name='PAN Referral 1-7 days';
update professional_accounts set firstname='Pixalere',lastname='Test',user_name='pixalere' where id=7;
update components set field_name='system_barriers_comments',field_type='STRING',component_type='TXTA' where field_name='system_barriers';
delete from components where field_name='treatment_location_id' or field_name='gender' or field_name='dob' or field_name='phn';
update components set hide_gui=0, hide_flowchart=0 where field_name LIKE '%more_capillary%';
INSERT INTO components (resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns)VALUES( 0, 'antibiotics', 'STRING', 'MDDA', 4, 'pixalere.treatment.form.antibiotics', 38, 59, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL);
INSERT INTO components (resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns)VALUES( 17, 'adjunctive', 'STRING', 'CHKB', 4, 'pixalere.woundassessment.form.adjunctive', 27, 61, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, 1);
INSERT INTO components (resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns)VALUES( 17, 'adjunctive', 'STRING', 'CHKB', 5, 'pixalere.woundassessment.form.adjunctive', 27, 61, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, 1);
INSERT INTO components (resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns)VALUES( 17, 'adjunctive', 'STRING', 'CHKB', 6, 'pixalere.woundassessment.form.adjunctive', 27, 61, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns)VALUES( 0, 'vac_start_date', 'DATE', 'DATE', 4, 'pixalere.woundassessment.form.vacstart', 51, 70, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL),( 0, 'vac_end_date', 'DATE', 'DATE', 4, 'pixalere.woundassessment.form.vacend', 51, 71, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL),( 98, 'therapy_setting', 'INTEGER', 'RADO', 4, 'pixalere.woundassessment.form.reading_group', 51, 72, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, 1),( 40, 'pressure_reading', 'INTEGER', 'DRP', 4, 'pixalere.woundassessment.form.pressure_reading', 51, 73, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns)VALUES( 0, 'vac_start_date', 'DATE', 'DATE', 5, 'pixalere.woundassessment.form.vacstart', 51, 70, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL),( 0, 'vac_end_date', 'DATE', 'DATE', 5, 'pixalere.woundassessment.form.vacend', 51, 71, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL),( 98, 'therapy_setting', 'INTEGER', 'RADO', 5, 'pixalere.woundassessment.form.reading_group', 51, 72, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, 1),( 40, 'pressure_reading', 'INTEGER', 'DRP', 5, 'pixalere.woundassessment.form.pressure_reading', 51, 73, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, allow_required, validation_message, hide_flowchart, ignore_info_popup, hide_gui, num_columns)VALUES( 0, 'vac_start_date', 'DATE', 'DATE', 6, 'pixalere.woundassessment.form.vacstart', 51, 70, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL),( 0, 'vac_end_date', 'DATE', 'DATE', 6, 'pixalere.woundassessment.form.vacend', 51, 71, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL),( 98, 'therapy_setting', 'INTEGER', 'RADO', 6, 'pixalere.woundassessment.form.reading_group', 51, 72, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, 1),( 40, 'pressure_reading', 'INTEGER', 'DRP', 6, 'pixalere.woundassessment.form.pressure_reading', 51, 73, 0, 0, 0, 1, NULL, 0, NULL, 0, 0, 0, NULL);
update components set label_key='pixalere.treatment.form.bandages_in_viewer' where field_name='bandages_in';
update components set label_key='pixalere.treatment.form.bandages_out_viewer' where field_name='bandages_out';
update lookup lk set report_value='2' where exists (select id from lookup_l10n l where l.lookup_id=lk.id and l.name='Paged Referral (within 2 hours)');
update lookup lk set report_value='48' where exists (select id from lookup_l10n l where l.lookup_id=lk.id and l.name='Urgent (24 - 48hrs)');
update lookup lk set report_value='168' where exists (select id from lookup_l10n l where l.lookup_id=lk.id and l.name LIKE '%Clinical Review (1 - 7 days)%');
update lookup lk set report_value='168' where exists (select id from lookup_l10n l where l.lookup_id=lk.id and l.name='PAN Referral 1-7 days');
update lookup lk set report_value='48' where exists (select id from lookup_l10n l where l.lookup_id=lk.id and l.name='Cardinal ET (24 - 48hrs)');
update components set orderby=4 where field_name='root_cause_addressed';
update components set orderby=9 where field_name='date_surgery' and flowchart=2;
update components set orderby=8 where field_name='surgeon';
update components set orderby=7 where field_name='external_referrals';
update components set orderby=6 where field_name='funding_source';
update components set orderby=8 where field_name='professionals';
update components set orderby=9 where field_name='co_morbidities';
update components set orderby=10 where field_name='surgical_history';
update components set orderby=15 where field_name='discharge_planning_initiated';
update components set orderby=18 where field_name='chronic_disease_initiated';
update components set orderby=21 where field_name='quality_of_life_addressed';
update components set orderby=24 where field_name='barriers_addressed';
update components set orderby=27 where field_name='system_barriers_comments';
update components set orderby=30 where field_name='interfering_factors';
update components set orderby=31 where field_name='interfering_factors_other';
update components set orderby=34 where field_name='medication_comments';
update components set orderby=37 where field_name='patient_limitations';
update components set orderby=67 where field_name='kci_num';
delete from components where field_name='purs_score' and flowchart=1;
update components set orderby=1 where field_name='temperature';
update components set orderby=15 where field_name='height';
update components set orderby=12 where field_name='weight';
update components set orderby=6 where field_name='pulse';
update components set orderby=9 where field_name='resp_rate';
update components set orderby=3 where field_name='blood_pressure';
update components set orderby=58 where field_name='sleep_positions' and flowchart=11;
update components set orderby=99 where field_name='limb_comments' and flowchart=52;
update components set orderby=43 where field_name='right_tbi_score' and flowchart=52;
update components set orderby=44 where field_name='left_tbi_score' and flowchart=52;
update components set orderby=64 where field_name='left_limb_shape' and flowchart=52;
update components set orderby=63 where field_name='right_limb_shape' and flowchart=52;
update components set label_key='pixalere.patientprofile.form.right_sensory_upper',orderby=33 where field_name='right_sensory' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.left_sensory_upper',orderby=34 where field_name='left_sensory' and flowchart=53;
update components set orderby=39 where field_name='right_less_capillary' and flowchart=53;
update components set orderby=40 where field_name='left_less_capillary' and flowchart=53;
update components set orderby=41 where field_name='right_more_capillary' and flowchart=53;
update components set orderby=42 where field_name='left_more_capillary' and flowchart=53;
update components set orderby=35 where field_name='right_dorsalis_pedis_palpation' and flowchart=53;
update components set orderby=36 where field_name='left_dorsalis_pedis_palpation' and flowchart=53;
update components set orderby=37 where field_name='right_posterior_tibial_palpation' and flowchart=53;
update components set orderby=38 where field_name='left_posterior_tibial_palpation' and flowchart=53;
update components set orderby=43 where field_name='right_range_motion_arm' and flowchart=53;
update components set orderby=44 where field_name='left_range_motion_arm' and flowchart=53;
update components set orderby=45 where field_name='right_range_motion_hand' and flowchart=53;
update components set orderby=46 where field_name='left_range_motion_hand' and flowchart=53;
update components set orderby=47 where field_name='right_range_motion_fingers' and flowchart=53;
update components set orderby=48 where field_name='left_range_motion_fingers' and flowchart=53;
update components set orderby=64 where field_name='right_elbow' and flowchart=53;
update components set orderby=65 where field_name='left_elbow' and flowchart=53;
update components set orderby=62 where field_name='right_wrist' and flowchart=53;
update components set orderby=63 where field_name='left_wrist' and flowchart=53;
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 35, 1, 'allowReferralEmail', '', '', 'CHKB', '', '', '0');
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 36, 1, 'woundDischargeReason', '', '', 'TXT', '', '', '431');
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 36, 1, 'patientDischargeReasonComplete', '', '', 'TXT', '', '', '3507');
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 36, 1, 'patientDischargeReasonSelfCare', '', '', 'TXT', '', '', '1603');
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 36, 1, 'patientDischargeReasonOther', '', '', 'TXT', '', '', '3201');
alter table report_queue add extra_filter varchar(255);
alter table assessment_antibiotics modify start_date varchar(50);
alter table assessment_antibiotics modify end_date varchar(50);
alter table assessment_burn modify wound_date varchar(50);
alter table assessment_burn modify closed_date varchar(50);
alter table assessment_burn modify target_discharge_date varchar(50);
alter table assessment_drain modify tubes_changed_date varchar(50);
alter table assessment_drain modify additional_days_date1 varchar(50);
alter table assessment_drain modify additional_days_date2 varchar(50);
alter table assessment_drain modify additional_days_date3 varchar(50);
alter table assessment_drain modify additional_days_date4 varchar(50);
alter table assessment_drain modify additional_days_date5 varchar(50);
alter table assessment_drain modify closed_date datetime;
alter table assessment_drain modify target_discharge_date varchar(50);
alter table assessment_incision modify clinical_path_date varchar(50);
alter table assessment_incision modify surgery_date varchar(50);
alter table assessment_incision modify closed_date varchar(50);
alter table assessment_incision modify target_discharge_date varchar(50);
alter table assessment_npwt modify vac_start_date varchar(50);
alter table assessment_npwt modify vac_end_date varchar(50);
alter table assessment_ostomy modify clinical_path_date varchar(50);
alter table assessment_ostomy modify closed_date varchar(50);
alter table assessment_ostomy modify target_discharge_date varchar(50);
alter table assessment_skin modify wound_date varchar(50);
alter table assessment_skin modify target_discharge_date varchar(50);
alter table assessment_treatment modify cs_date varchar(50);
alter table assessment_wound modify closed_date varchar(50);
alter table assessment_wound modify wound_date varchar(50);
alter table assessment_wound modify target_discharge_date varchar(50);
alter table diagnostic_investigation modify blood_sugar_date varchar(50);
alter table diagnostic_investigation modify albumin_date varchar(50);
alter table diagnostic_investigation modify prealbumin_date varchar(50);
alter table limb_adv_assessment modify ankle_brachial_date varchar(50);
alter table limb_adv_assessment modify toe_brachial_date varchar(50);
alter table limb_adv_assessment modify transcutaneous_date varchar(50);
alter table patient_investigations modify investigation_date varchar(50);
alter table patient_profiles modify start_date varchar(50);
alter table wound_profiles modify date_surgery varchar(50);
alter table wound_profiles modify final_tbsa varchar(50);
alter table wound_profiles modify preliminary_tbsa varchar(50);
alter table wounds modify start_date varchar(50);
alter table assessment_wound add heal_rate float;
alter table wound_assessment_location add current_heal_rate float;
update wound_assessment_location wal set current_heal_rate =(select (((cast(concat(ae2.length_cm,'.',ae2.length_mm) as decimal) * cast(concat(ae2.width_cm,'.',ae2.width_mm) as decimal))-(cast(concat(ae1.length_cm,'.',ae1.length_mm) as decimal) * cast(concat(ae1.width_cm,'.',ae1.width_mm) as decimal)))/(cast(concat(ae1.length_cm,'.',ae1.length_mm) as decimal) * cast(concat(ae1.width_cm,'.',ae1.width_mm) as decimal))) from assessment_wound ae2 left join assessment_wound ae1 on ae1.alpha_id=ae2.alpha_id and ae2.visited_on<ae1.visited_on where ae2.full_assessment=1 and ae1.full_assessment=1  and wal.id=ae1.alpha_id and ae1.id IS NOT NULL and ae2.id IS NOT NULL  order by ae2.visited_on desc limit 1);
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 36, 1, 'useWMPForSummaryReport', '', '', 'CHKB', '', '', '1');
update components set hide_gui=1,hide_flowchart=1 where field_name='referral_vascular_assessment';
update resources set lock_add_n_del=0 where lock_add_n_del IS NULL;
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 36, 1, 'showExamTab', '', '', 'CHKB', '', '', '1');
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 0, 36, 1, 'applyEncounterHistory', '', '', 'CHKB', '', '', '1');
update lookup set active=0 where resource_id=7 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Uncertain');
update lookup set active=0 where resource_id=7  and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Uncertain');
update lookup set active=0 where resource_id=51 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Training');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'OT', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'PT', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Dietitian', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Social Worker', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Financial Worker', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Diabetic Education', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Vascular Surgery', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Plastic Surgery', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Orthopedic Surgery', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Podiatrist', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 195, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Chiropodist', 195 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 196, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'GP', 196 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 196, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'NP', 196 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 196, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Nurse', 196 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 196, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Client', 196 from lookup order by id desc limit 1 ;
update lookup set active=0 where resource_id=6 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Lack of supply/equipment');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 6, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Delay in available community support', 6 from lookup order by id desc limit 1 ;
update lookup set active=0 where resource_id=6 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Not Adherent to Care Plan');
update lookup set active=0 where resource_id=6 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Allergy');
update lookup set active=0 where resource_id=6 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Amputaton');
update lookup set active=0 where resource_id=6 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Infection');
update lookup set active=0 where resource_id=6 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Lack of supply/equipment');
update lookup set active=0 where resource_id=6 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Delay in available community support');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 6, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Inability to Participate in care plan', 6 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 6, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Delay in equipment', 6 from lookup order by id desc limit 1 ;
update lookup_l10n set name='Non-palpable' where resource_id=23 and name='Absent';
update lookup_l10n set name='Non-palpable' where resource_id=22 and name='Absent';
update lookup set active=0 where resource_id=169 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Discharged - Healed');
update lookup set active=0 where resource_id=169 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Discharged - Self Care');
update lookup set active=0 where resource_id=169 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Discharged - Other');
update lookup set active=0 where resource_id=169 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Service no longer required');
update lookup set active=0 where resource_id=169 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Treatment Complete');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 169, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'S&W care no longer required', 169 from lookup order by id desc limit 1 ;
update lookup_l10n set name='Transferred - Non Pixalere Site' where name='Transfered - Non Pix Site';
update lookup set active=0 where   exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='+ Homans Sign');
update lookup set active=0 where  exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='+ Stemmers Sign');
update lookup_l10n set name='No visible edema' where resource_id=20 and name='No Edema';
update lookup_l10n set name='Up to ankle' where resource_id=20 and name='Up to Ankle';
update lookup_l10n set name='Up to midcalf' where resource_id=20 and name='Up to Midcalf';
update lookup_l10n set name='Up to knee' where resource_id=20 and name='Up to Knee';
update lookup_l10n set name='Up to groin' where resource_id=20 and name='Up to Groin';
update lookup_l10n set name='None noted' where resource_id=21 and name='0 None';
update lookup_l10n set name='+1 Trace 2mm' where resource_id=21 and name='+1 Trace';
update lookup_l10n set name='+2 Moderate 4mm' where resource_id=21 and name='+2 Moderate';
update lookup_l10n set name='+3 Deep 6mm' where resource_id=21 and name='+3 Deep';
update lookup_l10n set name='+4 Very Deep 8mm' where resource_id=21 and name='+4 Very Deep';
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 21, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Non-pitting', 21 from lookup order by id desc limit 1 ;
update lookup_l10n set name='Inability to participate in care plan' where name='Non-adherence to treatment plan';
update lookup set active=1 where id=820 and exists (select id from lookup_l10n where lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Non-adherence to treatment plan');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 116, 0, 10, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Itchy', 116 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 116, 0, 11, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Rash present', 116 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 116, 0, 12, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Inflammation', 116 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 116, 0, 13, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Healed wound/scar', 116 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 116, 0, 14, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Blister(s) present', 116 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 116, 0, 15, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Wound(s) present', 116 from lookup order by id desc limit 1 ;
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=116 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Varicosities');
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=116 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Hemosiderin staining');
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=116 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Woody/fiborous');
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=116 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Stasis dermatitis');
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=116 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Dependent rubor');
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=116 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Blanching on elevation');
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=126 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Continuous');
update lookup set active=1,resource_id=112 where   exists (select id from lookup_l10n where lookup.resource_id=126 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Intermittent');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 112, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Venous dermatitis', 112 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 112, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Ankle flare', 112 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 112, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Hyperkeratosis', 112 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 112, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Papillomatosis', 112 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 112, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'None of the above', 112 from lookup order by id desc limit 1 ;
update lookup_l10n set name='Champagne-bottle shaped' where name='Champagne-bottled shaped leg';
update lookup_l10n set name='None of the above' where resource_id=200 and name='Not applicable';
update lookup_l10n set name='None of the above' where resource_id=115 and name='Not applicable';
update lookup_l10n set name='None of the above' where resource_id=117 and name='Not applicable';
update lookup_l10n set name='Incorrect length short' where resource_id=117 and name='Nails too short';
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 115, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Cracks between toes', 115 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 115, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Abnormal skin dryness', 115 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 117, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Thin', 117 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 117, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Ridged', 117 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 117, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Ram''s Horn formation', 117 from lookup order by id desc limit 1 ;
update lookup_l10n set name='Incorrect length-long' where resource_id=117 and name='Nails too long';
update lookup_l10n set name='Ingrown' where resource_id=117 and name='Ingrown (nails)';
update lookup_l10n set name='Involuted' where resource_id=117 and name='Involuted (nails)';
update lookup_l10n set name='Discoloured' where resource_id=117 and name='Yellow coloured';
update lookup_l10n set name='Thickened' where resource_id=117 and name='Thickened (nails)';
update components set required=0 where field_name='medications_comments';
update lookup_l10n set resource_id=198 where name='Pain at rest' and resource_id=111;
update lookup_l10n set resource_id=198 where name='Pain at night' and resource_id=111;
update lookup_l10n set resource_id=198 where name='Decrease with elevation' and resource_id=111;
update lookup_l10n set resource_id=198 where name='Increase with elevation' and resource_id=111;
update lookup_l10n set resource_id=198 where name='Intermittent claudication' and resource_id=111;
update lookup_l10n set resource_id=198 where name='With deep palpation' and resource_id=111;
update lookup set active=1,resource_id=198 where   exists (select id from lookup_l10n where lookup.resource_id=111 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Pain at rest');
update lookup set active=1,resource_id=198 where   exists (select id from lookup_l10n where lookup.resource_id=111 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Pain at night');
update lookup set active=1,resource_id=198 where   exists (select id from lookup_l10n where lookup.resource_id=111 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Decrease with elevation');
update lookup set active=1,resource_id=198 where   exists (select id from lookup_l10n where lookup.resource_id=111 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Increase with elevation');
update lookup set active=1,resource_id=198 where   exists (select id from lookup_l10n where lookup.resource_id=111 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Intermittent claudication');
update lookup set active=1,resource_id=198 where   exists (select id from lookup_l10n where lookup.resource_id=111 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='With deep palpation');
update components set resource_id=198 where resource_id=111 and flowchart=52;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 117, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Relieved with rest', 198 from lookup order by id desc limit 1 ;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 117, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'None of the above', 198 from lookup order by id desc limit 1 ;
update lookup_l10n set name='Relieved w/ elevation' where name='Decrease with elevation' and resource_id=198;
update lookup_l10n set name='Relieved w/ dependent position' where name='Increase with elevation' and resource_id=198;
update components set resource_id=112 where flowchart=52 and resource_id=116;
update components set resource_id=116 where flowchart=11 and resource_id=112;
update lookup_l10n set resource_id=198 where name='Pain at rest' and resource_id=112;
update lookup set orderby=1 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Intact');
update lookup set orderby=10 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Dry');
update lookup set orderby=20 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Rash');
update lookup set orderby=30 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Fragile');
update lookup set orderby=40 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Excoriated/denuded');
update lookup set orderby=50 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Weepy');
update lookup set orderby=60 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Blister');
update lookup set orderby=70 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Bruised');
update lookup set orderby=80 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Macerated');
update lookup set orderby=90 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Not Assessed');
update lookup set orderby=5 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Erythema 2cm or >');
update lookup set orderby=15 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Indurated < 2cm');
update lookup set orderby=25 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Erythema < 2cm');
update lookup set orderby=35 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Indurated 2cm and >');
update lookup set orderby=45 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Increased Warmth');
update lookup set orderby=55 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Boggy');
update lookup set orderby=65 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Edema');
update lookup set orderby=75 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Tape Tear');
update lookup set orderby=85 where   exists (select id from lookup_l10n where lookup.resource_id=37 and lookup.id=lookup_l10n.lookup_id and language_id=1 and name='Callused');
update lookup set category_id=0 where category_id IS NULL and resource_id=50;
update configuration set current_value=0 where config_setting='applyEncounterHistory';
update lookup set active=0 where exists (select id from lookup_l10n where name='Amputation' and resource_id=6 and lookup_id=lookup.id) and active=1;
update lookup set active=0 where exists (select id from lookup_l10n where name='Inability to Participate in care plan' and resource_id=6 and lookup_id=lookup.id) and active=1;
update lookup set orderby=20 where exists (select id from lookup_l10n where name='Delay in equipment' and resource_id=6 and lookup_id=lookup.id) and active=1;
update lookup set orderby=21 where exists (select id from lookup_l10n where name='Inability to Particpate in care plan' and resource_id=6 and lookup_id=lookup.id) and active=1;
update lookup set orderby=0 where exists (select id from lookup_l10n where name='N/A' and resource_id=6 and lookup_id=lookup.id) and active=1;
update lookup set orderby=9 where exists (select id from lookup_l10n where name='No visible edema' and resource_id=20 and lookup_id=lookup.id) and active=1;
update lookup set orderby=1 where exists (select id from lookup_l10n where name='Foot' and resource_id=20 and lookup_id=lookup.id) and active=1;
update lookup set active=0 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Normal' and resource_id=126);
update lookup set orderby=8 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='None of the Above' and resource_id=126);
update lookup set orderby=13 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='No Pain' and resource_id=111);
update lookup_l10n set name='Champagne-bottle shaped' where name='Champagne-bottled shaped leg';
update lookup set orderby=4 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Thin' and resource_id=117);
update lookup set orderby=7 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Ridged' and resource_id=117);
update lookup set orderby=10 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Ram''s Horn' and resource_id=117);
update lookup set orderby=1 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Incorrect length short' and resource_id=117);
update lookup set orderby=2 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Incorrect length-long' and resource_id=117);
update lookup set orderby=3 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Ingrown nail(s)' and resource_id=117);
update lookup set orderby=9 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Involuted nail(s)' and resource_id=117);
update lookup set orderby=8 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Discoloured' and resource_id=117);
update lookup set orderby=6 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Thickened nail(s)' and resource_id=117);
update lookup set orderby=5 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Brittle' and resource_id=117);
update lookup set orderby=11 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Fungal Infection' and resource_id=117);
update lookup set orderby=12 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='None of the above' and resource_id=117);
update lookup set orderby=1 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Bunion(s)' and resource_id=115);
update lookup set orderby=2 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Callus(s)' and resource_id=115);
update lookup set orderby=3 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Corn(s)' and resource_id=115);
update lookup set orderby=4 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Plantar wart(s)' and resource_id=115);
update lookup set orderby=5 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Dropped MTH(s)' and resource_id=115);
update lookup set orderby=6 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Hammertoe(s)' and resource_id=115);
update lookup set orderby=7 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Crossed toes' and resource_id=115);
update lookup set orderby=8 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Fissures' and resource_id=115);
update lookup set orderby=9 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Cracks between toes' and resource_id=115);
update lookup set orderby=10 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Abnormal skin dryness' and resource_id=115);
update lookup set orderby=11 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Acute Charcot' and resource_id=115);
update lookup set orderby=12 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Chronic Charcot' and resource_id=115);
update lookup set orderby=13 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='None of the above' and resource_id=115);
update lookup set orderby=5 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Venous Dermatitis' and resource_id=112);
update lookup set orderby=8 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Ankle Flare' and resource_id=112);
update lookup set orderby=10 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Hyperkeratosis' and resource_id=112);
update lookup set orderby=11 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Papillomatosis' and resource_id=112);
update lookup set orderby=12 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='None of the above' and resource_id=112);
update lookup set orderby=9 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Varicosities' and resource_id=112);
update lookup set orderby=3 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Hemosiderin staining' and resource_id=112);
update lookup set orderby=4 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Woody fibrosis' and resource_id=112);
update lookup set orderby=6 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Atrophie blanche' and resource_id=112);
update lookup set orderby=2 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Dependent rubor' and resource_id=112);
update lookup set orderby=1 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Blanching on elevation' and resource_id=112);
update lookup_l10n set name='Contact dermatitis/puritis' where name='Stasis Dermatitis';
update lookup set orderby=7 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Contact dermatitis/puritis' and resource_id=112);
update lookup set orderby=1 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Pain at rest' and resource_id=198);
update lookup set orderby=2 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Pain at night' and resource_id=198);
update lookup set orderby=3 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Decrease elevation' and resource_id=198);
update lookup set orderby=4 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Decrease with dependent position' and resource_id=198);
update lookup set orderby=5 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='Intermittent claudication' and resource_id=198);
update lookup set orderby=6 where exists(select id from lookup_l10n l where l.lookup_id=lookup.id and name='With deep palpation' and resource_id=198);
update nursing_care_plan set deleted=0 where deleted IS NULL;
update treatment_comments set deleted=0 where deleted IS NULL;
update limb_adv_assessment set ankle_brachial_date=null where ankle_brachial_date=0;
update limb_adv_assessment set toe_brachial_date=null where toe_brachial_date=0;
update limb_adv_assessment set transcutaneous_date=null where transcutaneous_date=0;
update lookup set active=0 where id=990 or id=612;
update components set label_key='pixalere.patientprofile.form.left_radial_palpation' where field_name='left_dorsalis_pedis_palpation' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_radial_palpation' where field_name='right_dorsalis_pedis_palpation' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.left_brachial_palpation' where field_name='left_posterior_tibial_palpation' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_brachial_palpation' where field_name='right_posterior_tibial_palpation' and flowchart=53;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'applyPasswordReset','','','CHKB','','','1');
CREATE TABLE password_reset (password_reset_id  int(11) NOT NULL auto_increment,primary key(password_reset_id), professional_id int,created_on date,token varchar(255)) ENGINE=InnoDB;
alter table event add reason varchar(255);
alter table professional_accounts add question_one int;
alter table professional_accounts add question_two int;
alter table professional_accounts add answer_one varchar(255);
alter table professional_accounts add answer_two varchar(255);
alter table professional_accounts add reset_password_method int(1);
insert into resources (id,name) values (204,'Password Questions');
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 204, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'In what city did you meet your spouse/significant other in?', 204 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 204, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'What is your mothers maiden name?', 204 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 204, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'What street did you live on in the 3rd grade?', 204 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 204, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'What are the last 5 digits of your drivers license number?', 204 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 204, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'What was your dream job as a child?', 204 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 204, 0, 1, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'What is the first name of the first boy/girl you kissed?', 204 from lookup order by id desc limit 1;
alter  table logs_access add description varchar(255);
update components set label_key='pixalere.patientprofile.form.left_sensory_upper' where field_name='left_sensory' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_sensory_upper' where field_name='right_sensory' and flowchart=53;
update components set orderby=33 where flowchart=53 and field_name='left_temperature_fingers';
update components set orderby=32 where flowchart=53 and field_name='right_temperature_fingers';
update components set orderby=37 where flowchart=53 and field_name='right_dorsalis_pedis_palpation';
update components set orderby=38 where flowchart=53 and field_name='left_dorsalis_pedis_palpation';
update components set orderby=39 where flowchart=53 and field_name='right_posterior_tibial_palpation';
update components set orderby=40 where flowchart=53 and field_name='left_posterior_tibial_palpation';
update components set orderby=41 where flowchart=53 and field_name='right_range_motion_arm';
update components set orderby=42 where flowchart=53 and field_name='left_range_motion_arm';
update components set orderby=43 where flowchart=53 and field_name='right_range_motion_hand';
update components set orderby=44 where flowchart=53 and field_name='left_range_motion_hand';
update components set orderby=45 where flowchart=53 and field_name='right_range_motion_fingers';
update components set orderby=46 where flowchart=53 and field_name='left_range_motion_fingers';
update wound_profiles set current_flag=1 where wound_id=52 and patient_id=66;
update wound_profiles set current_flag=1 where wound_id=221 and patient_id=3277;
update wound_profiles set current_flag=1 where wound_id=3278 and patient_id=1933;
update wound_profiles set current_flag=1 where wound_id=3279 and patient_id=1933;
update wound_profiles set current_flag=1 where wound_id=5016 and patient_id=132;
update lookup_l10n set name='Transgender' where name='Transgendered';
update components set validation='etiology: {alphaElementRequired: { alphas: "etiology_alphas",field: "etiology"}}',validation_message='etiology: "Etiology are required"' where field_name='etiology';
update components set validation='goals: {alphaElementRequired: { alphas: "goals_alphas",field: "goals"}}',validation_message='goals: "Goals are required"' where field_name='goals';
update patient_accounts set treatment_location_id=201 where treatment_location_id=612;
update user_account_regions set treatment_location_id=201 where treatment_location_id=612;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 8, 0, 15, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Hemorrhoid (Internal)', 8 from lookup order by id desc limit 1;
INSERT INTO lookup ( active, resource_id, other, orderby, title, solobox, update_access, report_value, category_id, icd9, icd10, cpt, locked, clinic, modified_date) VALUES ( 1, 8, 0, 16, 0, 0, '0', NULL, 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO lookup_l10n ( language_id, lookup_id, name, resource_id) select  1 ,id, 'Hemorrhoid (External)', 8 from lookup order by id desc limit 1;
update wound_assessment_location wal set close_time_stamp = (select closed_date from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by visited_on desc limit 1) where wal.discharge=1 and exists(select closed_date from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by visited_on desc limit 1);
update components set field_type='INTEGER' where field_name='bandages_in';
update components set field_type='INTEGER' where field_name='bandages_out';
alter table report_queue add active int;
update report_queue set active=1;
alter table components add page_column int;
update components set component_type='YNWHY' where field_name='quality_of_life_addressed';
update components set page_column = 1 where field_name='professionals' or field_name='external_referrals' or field_name='co_morbidities';
update components set hide_gui=1 where field_name='interfering_factors_other' or field_name='age' or field_name='funding_source';
update components set page_column = 2 where field_name='interfering_factors' or field_name='interfering_factors_other'  or field_name='system_barriers_comments' or field_name='surgical_history' or field_name='medications_comments';
update components set page_column = 3 where field_name='maintenance' or  field_name='discharge_planning_initiated' or field_name='patient_limitations'  or field_name='discharge_planning_initiated' or field_name='chronic_disease_initiated' or field_name='quality_of_life_addressed' or field_name='barriers_addressed' or field_name='system_barriers_comments';
update components set page_column=1 where field_name='pain' or field_name='pain_comments' or field_name='length' or field_name='width' or field_name='depth' or field_name='undermining' or field_name='sinus_tracts';
update components set page_column=2 where field_name='wound_base' or field_name='exudate_type';
update components set page_column=3 where field_name='exudate_amount' or field_name='exudate_odour' or field_name='wound_edge' or field_name='periwound_skin';
