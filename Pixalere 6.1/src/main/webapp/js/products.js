//uncheck the "other product" checkbox, and clear the text field
function clearOtherProduct() {
	document.form.chkOtherProducts.checked = false;
	document.form.otherProduct.value = "";
}
function verifyQuantity() {
    return floatOnly(document.form.quantity);
}

function showQuantity(products){
    var choose=products;
	var cholen=choose.length;

	for(i=0;i<cholen;i++){
		if(choose!=null&&choose.options[i].selected==true){
			var product_splice = choose.options[i].value;
			var array = product_splice.split("|");
            var quantity=array[0];
            document.form.change_quantity.value=quantity;
            }
	   }
}

function changeQTY(){
    if(document.form.chkOtherProducts.checked == false){
        if(document.form.products.selectedIndex!=-1){
       showProduct(document.form.products.options[document.form.products.selectedIndex].text);
        }
    }

    else{
	document.form.quantity.value="1";
	}
}
 function trim(str)
{	while(str.charAt(0) == (" ") )
	{	str = str.substring(1);
	}
	while(str.charAt(str.length-1) == " " )
	{	str = str.substring(0,str.length-1);
	}
	return str;
}
function addProduct(){
    var products=document.form.products;
    var choosen=document.form.current_products;
    var prodlen=products.length;
    var cholen=choosen.length;
    var quant=document.form.quantity.value;
    var max=document.form.maxquant.value;
    var amount=quant;
    var wounds = document.form.wound_ass.options;

    if(document.form.chkOtherProducts.checked){
        amount=quant;
    }
    var wasselected=false;
    if(document.form.chkOtherProducts.checked == false){
            for ( i=0; i<prodlen ; i++){
                if (products.options[i].selected == true){
                    wasselected=true;
                    var numselected=0;
                    for(x=0;x<document.form.wound_ass.length;x++){
                        if(document.form.wound_ass.options[x].selected==true){
                            numselected++;
                        }
                    }
            
                    for(x=0;x<document.form.wound_ass.length;x++){
                        cholen=choosen.length;
                        if(document.form.wound_ass.options[x].selected==true){
                            if(document.form.wound_ass[x].value=="all"){
                                for(j=0;j<wounds.length;j++){
                                    cholen=choosen.length;
                                    if(wounds[j].value!="all"){
                                        choosen.options[cholen]= new Option(amount+':'+wounds[j].text+':'+products.options[i].text,amount+'|'+wounds[j].value+'|'+products.options[i].value);
                                        }
                                    }
                                }
                            else{

                                var newamount=amount/numselected;
                                if(document.form.chkOtherProducts.checked){
                                    choosen.options[cholen]= new Option((Math.round(newamount*100)/100)+':'+document.form.wound_ass[x].value+':Other;'+document.form.otherProduct.value,(Math.round(newamount*100)/100)+'|'+document.form.wound_ass[x].value+'|Other;'+document.form.otherProduct.value);
                                }
                                else{
                                     choosen.options[cholen]= new Option((Math.round(newamount*100)/100)+':'+document.form.wound_ass[x].text+':'+products.options[i].text,(Math.round(newamount*100)/100)+'|'+document.form.wound_ass[x].value+'|'+products.options[i].value);
                                }
                            }
                        }
                    }
                }
            }
    }
    if(wasselected==false){
            var numselected=0;
            for(x=0;x<document.form.wound_ass.length;x++){
                if(document.form.wound_ass.options[x].selected==true){
                    numselected++;
                }
            }
            for(x=0;x<document.form.wound_ass.length;x++){
                cholen=choosen.length;
                if(document.form.wound_ass.options[x].selected==true){
                    if(document.form.wound_ass[x].value=="all"){
                   
                        for(j=0;j<wounds.length;j++){
                            cholen=choosen.length;
                            if(document.form.chkOtherProducts.checked){
                                if(wounds[j].value!="all"){
                                    choosen.options[cholen]= new Option(amount+':'+wounds[j].text+':Other;'+document.form.otherProduct.value,amount+'|'+wounds[j].value+'|Other;'+document.form.otherProduct.value);
                                }
                            }
                        }
                    }
                    else{
                        var newamount=amount/numselected;
                        if(document.form.chkOtherProducts.checked){
                                choosen.options[cholen]= new Option((Math.round(newamount*100)/100)+':'+document.form.wound_ass[x].text+':Other;'+document.form.otherProduct.value,(Math.round(newamount*100)/100)+'|'+document.form.wound_ass[x].value+'|Other;'+document.form.otherProduct.value);
                        }
                    }
                }
            }
    }
    $("#products option:selected").removeAttr("selected");
    document.form.current_products.selectedIndex=-1;
    //resort list.
    //
    //sortProducts(document.form.current_products);
    var selectList = $('#current_products option');

    selectList.sort(function(a,b){
        a = a.value;
        b = b.value;

        return a-b;
    });
    
    $('#current_products').eq(0).html(selectList);
}

function setButtons()
{
    var intCount=0;
	for(i=0;i<document.form.current_products.length;i++){
		if(document.form.current_products.options[i].selected==true){
            intCount++;
            }
	   }

    document.form.btn_update.disabled=(document.form.chkOtherProducts.checked==false && (intCount!=1 || document.form.products.selectedIndex==-1));
}

function removeProduct(){
    document.form.btn_update.disabled=true;
    document.form.removeAlpha.disabled=true;

    var choose=document.form.current_products;
    for(i=choose.length;i>-1;i--){
    	if(choose.options[i]!=null && choose.options[i].selected==true){
    		choose.options[i]=null;
    		choose.value[i]=null;
            }
    	}
}

function MM_openBrWindowLink(where,winName,features) {
			window.open(where,'product_info','resizable=yes,scrollbars=yes,width=680,height=500');
}
function updateProductValues(){
    if(document.form.btn_update.disabled == false){
//should we lock down current_products to prevent reselection?
//also need to think of way to unupdate.. ie what if they change their mind? SV: In that case, too bad...
        removeProduct();
        addProduct();
        document.form.addAlpha.disabled=false;
        document.form.removeAlpha.disabled=true;
        document.form.btn_update.disabled=true;
    }
}
function popUpdateProduct(objSelect,objQty,objAlpha)
{
    document.form.removeAlpha.disabled=false;
    if(objSelect.selectedIndex>-1)
        {
        var strProductDetails=objSelect.options[objSelect.selectedIndex].value;
		var strQuantity=strProductDetails.substring(0,strProductDetails.indexOf("|"));
        strProductDetails=strProductDetails.substring(strProductDetails.indexOf("|")+1);
        var alpha = strProductDetails.substring(0,strProductDetails.indexOf("|"));
        strProductDetails=strProductDetails.substring(strProductDetails.indexOf("|")+1);
        var product = strProductDetails.substring(0,strProductDetails.length);
        if(product.indexOf("Other;")>-1)
            // Product is "Other"
            {
            document.form.otherProduct.value=product.substring(6);
            document.form.chkOtherProducts.checked = true;
            }
        else
            {
        	document.form.chkOtherProducts.checked = false;
        	document.form.otherProduct.value = "";

            // Product comes from the list, so first select category "All products"
            document.form.category_products.options[0].selected=true;
            set_child_listbox(document.form.category_products,document.form.products,Product,'');
            for (var i=0; i < document.form.products.length; i++)
                {
                document.form.products[i].selected = false;
                if (document.form.products[i].value == product)
                    {
                    document.form.products[i].selected = true;
                    }
                }
        }
        objQty.value=strQuantity;
    
        for (var i=0; i < objAlpha.length; i++)
            {
            objAlpha[i].selected = false;
            if (objAlpha[i].value == alpha)
                {
                objAlpha[i].selected = true;
                }
            }
    }
    setButtons();    
}



function autoComplete (field, select, property, forcematch) {

	var found = false;
	for (var i = 0; i < select.options.length; i++) {
	if (select.options[i][property].toUpperCase().indexOf(field.value.toUpperCase()) == 0) {
		found=true; break;
		}
	}

	if (found) { select.selectedIndex = i; }

	else { select.selectedIndex = -1; }

	if (field.createTextRange) {

		if (forcematch && !found) {
			field.value=field.value.substring(0,field.value.length-1);
			return;
			}

		var cursorKeys ="8;46;37;38;39;40;33;34;35;36;45;";

		if (cursorKeys.indexOf(event.keyCode+";") == -1) {

			var r1 = field.createTextRange();
			var oldValue = r1.text;
			var newValue = found ? select.options[i][property] : oldValue;
			if (newValue != field.value) {
				field.value = newValue;
				var rNew = field.createTextRange();
				rNew.moveStart('character', oldValue.length) ;
				rNew.select();
				showProduct(select.options[select.selectedIndex].text);
				}
			}
		}
	}

function filterProducts(objSelect,strFilter)
{
    set_child_listbox(document.form.category_products,document.form.products,Product,'');
    
    if(strFilter.length>0){
        strFilter=strFilter.toUpperCase();
   
        var i = 0;
        while ( i < objSelect.options.length){
            if (objSelect.options[i].text.toUpperCase().indexOf(strFilter) == -1){
               objSelect.options[i]=null;
            }
            else{
                i++; 
            }
        }
    } 
}

function showHideVac(obj){
	obj2=document.getElementById('vacDiv').style
	if(obj.value==1){
		obj2.display='block';
	}else{
	document.form.pressure_reading.options[0].selected=true;
	    document.form.reading_group[0].checked=false;
	    document.form.reading_group[1].checked=false;
		obj2.display='none';
	}
}
function verify() {
        
	if(document.getElementById("cancel1") && document.getElementById("cancel1").checked == true  ){
            document.form.submit();
        }else{
            
            var $tabs = $('#tabs').tabs(); // first tab selected
            $tabs.tabs('option','active', 0);
            
            $("#treatment_form").submit();
        }

}
function isChangedOne(){
    var isChanged = 0;
    
    if( $('#initiated_by1').val() != "") {isChanged = 1;}
    
    if(   $('#vendor_name1').val()  != "") {isChanged = 1;}
    if(  typeof $("input[name=machine_acquirement1]:checked").val() != "undefined" ) {isChanged = 1;}
    if(   $('#serial_num1').val() != "") {isChanged = 1;}
    
    if(   $('#kci_num1').val()  != "") {isChanged = 1;}
    if(   $('#pressure_reading1').val() != "" ) {isChanged = 1;}
    if(   $('#goal_of_therapy1').val() != "" ) {isChanged = 1;}
    if(   $('#vacstart1_day').val()  != "") {isChanged = 1;}
    if(   $('#vacstart1_month').val() != "" ) {isChanged = 1;}
    if(   $('#vacstart1_year').val()  != "") {isChanged = 1;}
    
    if(   typeof  $("input[name=therapy_setting1]:checked").val() != "undefined" ) {isChanged = 1;}
    
    if(   $('#np_connector1').val() != "" ) {isChanged = 1;}
    if(   $('#reason_for_ending1').val()  != "") {isChanged = 1;}
    if(   $('#vacend1_day').val()  != "") {isChanged = 1;}
    if(   $('#vacend1_month').val() != "" ) {isChanged = 1;}
    
    if(   $('#vacend1_year').val()  != "") {isChanged = 1;}
    
    return isChanged;
}
function isChangedTwo(){
    var isChanged = 0;
    if(  $('#initiated_by2').val()  != '') {isChanged = 1;}
    if(   $('#vendor_name2').val() != "" ) {isChanged = 1;}
    if(  typeof   $("input[name=machine_acquirement2]:checked").val() != "undefined" ) {isChanged = 1;}
    
    if(   $('#serial_num2').val()  != "") {isChanged = 1;}
    if(   $('#kci_num2').val() != "") {isChanged = 1;}
    if(   $('#pressure_reading2').val() != "" ) {isChanged = 1;}
    if(   $('#goal_of_therapy2').val() != "" ) {isChanged = 1;}
    if(   $('#vacstart2_day').val()  != "") {isChanged = 1;}
    if(   $('#vacstart2_month').val() != "" ) {isChanged = 1;}
    if(   $('#vacstart2_year').val()  != "") {isChanged = 1;}
    
    if(   typeof  $("input[name=therapy_setting2]:checked").val() != "undefined" ) {isChanged = 1;}
    if(   $('#np_connector2').val()  != "") {isChanged = 1;}
    if(   $('#reason_for_ending2').val() != "" ) {isChanged = 1;}
    
    if(   $('#vacend2_day').val()  != "") {isChanged = 1;}
    if(   $('#vacend2_month').val()  != "") {isChanged = 1;}
    
    if( $('#vacend2_year').val()  != "") {isChanged = 1;}
    
    return isChanged;
}

var DomYes=document.getElementById?1:0;

function set_child_listbox(parentObject,childObject,childArray,spanToHide) {
	//Clear child listbox
	for(var i=(childObject.length-1);i>=0;i--) {
		childObject.options[i] = null;
	}
    var sel_index = parentObject.options[parentObject.selectedIndex].value;
    //childObject.options[0] = new Option("All Products","all");
    for (var i = (childObject.options.length-1); i >= 0; i--){
         childObject.options[i]=null;
    }
    if(sel_index != "all"){
	    //alert(parentObject.options[parentObject.selectedIndex].value)
	    if (sel_index == "") {
		    childObject.disabled = true;
	    } else {
		    childObject.disabled = false;
		    var childIndex = 0;
		    for (i = 0; i < childArray.length; i++) {
		        //alert(childArray[i][6]);
			    if (childArray[i][6] == sel_index) {
				    childObject.options[childIndex] = new Option(childArray[i][0], childArray[i][7]);
				    childIndex++;
			    }
		    }
	    }
	 }else{
	      childObject.disabled = false;
		    var childIndex = 0;
		    for (i = 0; i < childArray.length; i++) {
		       childObject.options[childIndex] = new Option(childArray[i][0], childArray[i][7]);
			   childIndex++;

		    }
	 }
	//Select first option
	//childObject.selectedIndex = 0;

	//Hide dependent grid
	if (spanToHide != "") {
		if (DomYes) {
  			document.getElementById(spanToHide).style.display="none";
		} else {
			document.all[spanToHide].style.display="none";
		}
	}
	document.form.maxquant.value="";
	document.form.quantity.value="";
} 

function selectAll(){
    var ops = document.form.current_products.options;
	for (i = 0; i < ops.length; i++)
	{
		ops[i].selected = true;
	}
        if(document.form.antibiotics != undefined){
            var ops2 = document.form.antibiotics.options;
            for (i = 0; i < ops2.length; i++)
            {
                    ops2[i].selected = true;
            }
        }
        if(document.form.wound_ccac_questions != undefined){
            var ops4 = document.form.wound_ccac_questions.options;
            for (i = 0; i < ops4.length; i++){
                    ops4[i].selected = true;
            }
        }
        if(document.form.csresult != undefined){
        var ops3 = document.form.csresult.options;
            for (i = 0; i < ops3.length; i++)
            {
                    ops3[i].selected = true;
            }
        }
        if(document.form.bandages != undefined){
               var ops4 = document.form.bandages.options;
                for (i = 0; i < ops4.length; i++)
                {
                    ops4[i].selected = true;
                }
        }
        if(document.form.dressing_change_frequency != undefined){
            for(i = 0; i < document.form.dressing_change_frequency.length; i++){
                 document.form.dressing_change_frequency.options[i].selected=true;
            }
        }
}