function calculateWoundArea(){
    var length = (document.form.length.value==-1?0:document.form.length.value)+"."+(document.form.length_milli.value==-1?0:document.form.length_milli.value);
    var width = (document.form.width.value==-1?0:document.form.width.value)+"."+(document.form.width_milli.value==-1?0:document.form.width_milli.value);
  
    if(length != "0.0" && width != "0.0"){
        var area =  length * width;
        if(area==""){
             document.getElementById("wound_area_label").innerHTML="N/A";
        }else{
            document.getElementById("wound_area_label").innerHTML=area+"cm<sup>2</sup>";
        }
    }
}
function selectAll(){
	var perc="";
        var wb_fail=0;
        var c=0;
    	for(i=0;i<document.form.woundbase.length;i++){
		if(document.form.woundbase[i].checked==true){
		    var percentage = document.getElementById("woundbase_percentage_"+(i+1)).options[document.getElementById("woundbase_percentage_"+(i+1)).selectedIndex].value;
                    if(c == 0){
                        perc=""+percentage;
                    }
                    else{
                        perc=perc+","+percentage;
                    }
                    fail_check=0;
                    c++;
                }
    	}
        document.form.woundbase_percentage.value=perc;
}
function verifyPopulate(){
    var radio_buttons = document.populate.assessment_type;
      var value = -1;
      for(var k=0; k<radio_buttons.length; k++){
            if(radio_buttons[k].checked == true){
               value = radio_buttons[k].value;
            }
      }
      document.populate.full.value = value;
    $("#populate_form").submit();
}


function addItem(){

    var products=document.form.interventions;
    var choosen=document.form.current_interventions;
    var prodlen=products.length;
    var cholen=choosen.length
    for ( i=0; i<prodlen ; i++){
        if (products.options[i].selected == true){
            cholen=choosen.length;
            var text="";
            if(products.options[i].value.lastIndexOf("-- ")!=-1){
                text=products.options[i].value.substr(products.options[i].value.lastIndexOf("-- ")+3);
            }else{
                text=products.options[i].value;
            }
            choosen.options[cholen]= new Option(text);
        }
    }
}

	

	function removeItem(){
		var choose=document.form.current_interventions;
		var cholen=choose.length;
		for(i=0;i<cholen;i++){

			if(choose.options[i].selected==true){
				choose.options[i]=null;
				choose.selectedIndex=-1;break;

			} 
		}	

	

	}

function readytoprocess2(){

	var selectedproducts = "";				



    m2 = document.form.current_interventions;



	for (i=0; i<m2.length; i++)

	{ 

		if (i > 0) { 

			selectedproducts += "||";

		}

		selectedproducts += m2.options[i].text;					

		m2.options[i].selected = true; 

	}

	

	document.form.interventions_string.value=selectedproducts;



}







function MM_jumpMenu(targ,selObj,restore){ //v3.0

  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");

  if (restore) selObj.selectedIndex=0;

}



function MM_openBrWindow(theURL,winName,features) { //v2.0

  window.open('popups/'+theURL,winName,features);

}

/*function readytoprocess(){

	var selectedproducts = "";				



    m2 = document.assessmentform.assessments;



	for (i=0; i<m2.length; i++)

	{ 

		if (i > 0) { 

			selectedproducts += "||";

		}

		selectedproducts += m2.options[i].value;					

		m2.options[i].selected = true; 

	}

	document.form.assess_string.value=selectedproducts;

}*/

function perRound(num, precision) {
	var precision = 0; //default value if not passed from caller, change if desired
	// remark if passed from caller
	precision = parseInt(precision); // make certain the decimal precision is an integer
    var result1 = num * Math.pow(10, precision);
    var result2 = Math.round(result1);
    var result3 = result2 / Math.pow(10, precision);
    return zerosPad(result3, precision);
}

function zerosPad(rndVal, decPlaces) {
    var valStrg = rndVal.toString(); // Convert the number to a string
    var decLoc = valStrg.indexOf("."); // Locate the decimal point
    // check for a decimal
    if (decLoc == -1) {
        decPartsecnum = 0; // If no decimal, then all decimal places will be padded with 0s
        // If decPlaces is greater than zero, add a decimal point
        valStrg += decPlaces > 0 ? "." : "";
    }
    else {
        decPartsecnum = valStrg.secnumgth - decLoc - 1; // If there is a decimal already, only the needed decimal places will be padded with 0s
    }
     var totalPad = decPlaces - decPartsecnum;    // Calculate the number of decimal places that need to be padded with 0s
    if (totalPad > 0) {
        // Pad the string with 0s
        for (var cntrVal = 1; cntrVal <= totalPad; cntrVal++)
            valStrg += "0";
        }
    return valStrg;
}
function round(number,X) {

// rounds number to X decimal places, defaults to 2

    X = (!X ? 2 : X);

    return Math.round(number*Math.pow(10,X))/Math.pow(10,X);

}
function setExudateAmountNil(amount_nil,type_nil){
    var type = document.form.exudate_type;
    var amount = document.form.exudate_amount;
    var bool = 0;
    for (i=0; i<type.length; i++){
        if(type[i].checked == true && type[i].value == type_nil){
            for (x=0; x<amount.length; x++){
                if(amount[x].value == amount_nil){
                    amount[x].checked=true;
                }
            }
           bool=1;
        }


    }
    if(bool == 0){
        for(x=0;x<type.length;x++){
                type[x].disabled=false;
        }
    }
}
function setExudateTypeNil(amount_nil,type_nil){
    var amount = document.form.exudate_amount;
    var type = document.form.exudate_type;
    var bool = 0;
    for (i=0; i<amount.length; i++){

        if(amount[i].checked == true && amount[i].value == amount_nil){
            for (x=0; x<type.length; x++){
                if(type[x].value == type_nil){
                    type[x].checked=true;
                }else{
                    type[x].checked=false;
                    type[x].disabled=true;
                }
            }
            bool=1;
        }
    }
    if(bool == 0){
        for(x=0;x<type.length;x++){
                //type[x].disabled=false;
        }
    }
}


