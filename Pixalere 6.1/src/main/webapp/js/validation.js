    $.validator.addMethod("time", function(value, element) {

         return value == "" || /^(([0-1][0-9])|([2][0-3]))([0-5][0-9])$/i.test(value);
    });
    $.validator.addMethod("dateRange", function(){
        
        return $("#start_date").val() < $("#end_date").val();
    });
    $.validator.addMethod("numberslash", function(value,element,params){
      
        return value.match(/^\d\d?\d?\/\d\d?\d?$/);
    });


    $.validator.addMethod("passwordValidation",function(value,element,params){
        var strength = 0;
        var result = false;
        if(params.criteria == "alpha"){
            result = this.optional(element) || /^(?=.*[A-Za-z]).{4,16}$/i.test(value);
        }else if(params.criteria == "alpha-numeric"){
            result =  this.optional(element) || /^(?=.*\d)(?=.*[A-Za-z]).{4,16}$/i.test(value);
        }else if(params.criteria == "alpha-numeric-uppercase"){
            result =  this.optional(element) || /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z]).{4,16}$/i.test(value);
        }else if(params.criteria == "2alpha-2numeric-uppercase"){
            result =  this.optional(element) || /^(?=.*\d)(?=.*\d)(?=.*[A-Za-z])(?=.*[A-Z])(?=.*[a-z]).{4,16}$/i.test(value);
        }else if(params.criteria == "alpha-numeric-uppercase-specialcharacter"){
            result =  this.optional(element) || /^(?=.*\d)(?=.*[A-Z])(?=.*[@#$%.])(?=.*[a-z]).{4,16}$/i.test(value);
        }
        return result;
    });
    
    $.validator.addMethod("phnRequired",function(value,element,params){
        var phn="";
        for(var x = 0; x<params.fmt.length;x++){
            phn = phn + $(params.phn[x]).val();
        }
        
        if((phn =="" || phn == "undefined") && $(params.not_available).val() == "0"){
            return false;
        }else if(($(params.other_phn).val() =="" || $(params.other_phn).val() == "undefined") && $(params.not_available).val() == "2"){
            return false;
        }else if($(params.not_available).val() == "1"){
            return true;
        }

        return true;
    });

    $.validator.addMethod("phnLength",function(value,element,params){

        if ($(params.which).val() == "0"){
            for(var x = 0; x<params.fmt.length;x++){
                if($(params.phn[x]).val().length != $(params.fmt[x]).val()){
                    return false;
                }
            }
           
         }
         return true;
    });
    
    $.validator.addMethod("validatePHN",function(value,element,params){
        if (params.customer=="BC"){

            var strPHN = $(params.phn[0]).val()+$(params.phn[1]).val()+$(params.phn[2]).val();
            var result = (strPHN.length==10 && strPHN.charAt(0)=="9" && parseInt(strPHN.charAt(9))==11-(((parseInt(strPHN.charAt(1))*2)%11+(parseInt(strPHN.charAt(2))*4)%11+(parseInt(strPHN.charAt(3))*8)%11+(parseInt(strPHN.charAt(4))*5)%11+(parseInt(strPHN.charAt(5))*10)%11+(parseInt(strPHN.charAt(6))*9)%11+(parseInt(strPHN.charAt(7))*7)%11+(parseInt(strPHN.charAt(8))*3)%11)%11));
             
             return result;
            

         }return true;
    });
    $.validator.addMethod("equalToMD5",function(value,element,params){
       
       if(params.oldP == params.newP){
           return true;
       }else{return false;}
    });
    
    $.validator.addMethod("canadianDate",function(value, element, params) {
               
                var $month = $(params.data[1]);
                var $year = $(params.data[2]);
                var $day = $(params.data[0]);
                var month = $month.val();
                var year = $year.val();
                var day = $day.val();
                var value2 = day+"/"+month+"/"+year;
      
                if(value2 == "//" ){
                    return true;
                }
                if ((month==4 || month == 6 || month == 9 || month == 11) && day == 31){ return false;}
                else if (month == 2){
                    var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                    if (day> 29 || (day ==29 && !isleap)){return false;}
                }

                var currdate = new Date();
                var date=new Date();
                date.setFullYear(year,month-1,day);
                var _now=new Date();
                if(date.getTime()>_now.getTime()){
              
                  return false;
                }else if(year<1880){
                    return false;

                }
                 var JDate = new Date(parseInt(year), parseInt(month) -1, parseInt(day))

                valid = (parseInt(month) - 1 == JDate.getMonth() && parseInt(day) == JDate.getDate() && parseInt(year) == JDate.getFullYear())

                if(valid == false){return false;}
                return value2.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    });
    $.validator.addMethod("canadianDateClosed",function(value, element, params) {
               
                var $month = $(params.data[1]);
                var $year = $(params.data[2]);
                var $day = $(params.data[0]);
                
                var month = $month.val();
                var year = $year.val();
                var day = $day.val();
                var value2 = day+"/"+month+"/"+year;
                if(value2 == "//" ){
                    return false;
                }
                if ((month==4 || month == 6 || month == 9 || month == 11) && day == 31){ return false;}
                else if (month == 2){
                    var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                    if (day> 29 || (day ==29 && !isleap)){return false;}
                }

                var currdate = new Date();
                var date=new Date();
                date.setFullYear(year,month-1,day);
                var _now=new Date();
                if(date.getTime()>_now.getTime()){
              
                  return false;
                }else if(year<1880){
                    return false;

                }
                 var JDate = new Date(parseInt(year), parseInt(month) -1, parseInt(day))

                valid = (parseInt(month) - 1 == JDate.getMonth() && parseInt(day) == JDate.getDate() && parseInt(year) == JDate.getFullYear())

                if(valid == false){return false;}
                //ensure the date is past last assessment date
                
                return value2.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    });
    $.validator.addMethod("canadianDateIgnoreFuture",function(value, element, params) {
              
                var $month = $(params.data[1]);
                var $year = $(params.data[2]);
                var $day = $(params.data[0]);
                var month = $month.val();
                var year = $year.val();
                var day = $day.val();
                var value2 = day+"/"+month+"/"+year;

                if(value2 == "//" ){
                    return true;
                }
                var currdate = new Date();
                
                 var JDate = new Date(parseInt(year), parseInt(month) -1, parseInt(day))

                valid = (parseInt(month) - 1 == JDate.getMonth() && parseInt(day) == JDate.getDate() && parseInt(year) == JDate.getFullYear())

                if(valid == false){return false;}
                return value2.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    });
    $.validator.addMethod("checkLocation",function (value,element){
       if ((document.getElementById("blueman").background && document.getElementById("blueman").background.indexOf("empty_bluemodel.jpg")>0 )||
                value==null || value =="0" || value =="") {
                return false;
            }else{
                return true;
            }
    });
    $.validator.addMethod("measurementsRequired",function(value,element,params){
        if($(params.status).val() != params.closed){
            for(var x = 0; x<params.data.length;x++){
                    if($(params.data[x]).val() != "" && $(params.data[x]).val() != "0"){return true;}
            }
        }else{return true;}
        return false;
    });
    $.validator.addMethod("ifNotClosed", function(value,element,params){
        if($(params.status).val() != params.closed){
             return false;
        }
        return true;
    });
    $.validator.addMethod("ifClosed", function(value,element,params){

        if($(params.status).val() == params.closed){
            return true;
        }
        return false;
    });
    $.validator.addMethod("woundbasePercentageRequired", function(value,element){
       if(calcTotalWoundBed() != 100){
            return false;
        }
        return true;
    });
    $.validator.addMethod("npwtAlpha1Required", function(value,element){
        if(isChangedOne() == 1 && !$('.npwt_alphas1').is(':checked')){
            return false;
        }
        
        return true;
    });
    $.validator.addMethod("npwtAlpha2Required", function(value,element){
        if(isChangedTwo() == 1 && !$('.npwt_alphas2').is(':checked')){
            return false;
        }
        return true;
    });
    $.validator.addMethod("bradenCheck", function(value,element){
        if(document.form.braden_mobility.value == "" && document.form.braden_activity.value == "" && document.form.braden_nutrition.value == "" && document.form.braden_moisture.value == "" && document.form.braden_friction.value == "" && document.form.braden_sensory.value == ""){
            return true;
        }else if(document.form.braden_mobility.value == "" || document.form.braden_activity.value == "" || document.form.braden_nutrition.value == "" || document.form.braden_moisture.value == "" || document.form.braden_friction.value == "" || document.form.braden_sensory.value == ""){
            return false;
        }else{
            return true;
        }
    });
    $.validator.addMethod("bradenRequired", function(value,element){
        if(document.form.braden_mobility.value == "" || document.form.braden_activity.value == "" || document.form.braden_nutrition.value == "" || document.form.braden_moisture.value == "" || document.form.braden_friction.value == "" || document.form.braden_sensory.value == ""){
            return false;
        }else{
            return true;
        }
    });
    $.validator.addMethod("checkboxRequired", function(value,element){
       var selector_checked = $("input[@id=selector]:checked").length;
    });
    $.validator.addMethod("enforceLargerLength", function(value,element,params){
        var good=true;
        if(params.data.length==4){
          var length = "";
            var width = "";
            if ($(params.data[0]).val() != -1){
                length = $(params.data[0]).val()+"."+$(params.data[1]).val();
            }
            if ($(params.data[2]).val() != -1){
                width = $(params.data[2]).val()+"."+$(params.data[3]).val();
            }
            if(parseFloat(width)>parseFloat(length)){
                good=false;
            }
        }
        return good;
    });
    $.validator.addMethod("alphaElementRequired",function (value,element,params){
      var alphaLength = $("select[id='"+params.alphas+"'] option").length;
       if(element.options.length == 0 && alphaLength!=0){ 
           return false;
       }else {
            var length = 0;
            $("select[id='"+params.alphas+"'] option").each(function () {
                var val = $(this).text();
                
                $("select[id='"+params.field+"'] option:selected").each(function (){
                    
                    if($(this).text().indexOf(val) != -1){
                        length++;
                    }
                });

            });
           
            if(alphaLength>length){
                return false;
            }
            return true;
             
       }
        

    });
    $.validator.addMethod("fieldLength", function (value,element){
       return element.length>0;
    });
    $.validator.addMethod("maxWords", function(value, element, params) {
        return this.optional(element) || value.match(/\b\w+\b/g).length < params;
    }, $.validator.format("Please enter {0} words or less."));
    
    $.validator.addMethod("minWords", function(value, element, params) {
        return this.optional(element) || value.match(/\b\w+\b/g).length >= params;
    }, $.validator.format("Please enter at least {0} words."));

    $.validator.addMethod("rangeWords", function(value, element, params) {
        return this.optional(element) || value.match(/\b\w+\b/g).length >= params[0] && value.match(/bw+b/g).length < params[1];
    }, $.validator.format("Please enter between {0} and {1} words."));


    $.validator.addMethod("letterswithbasicpunc", function(value, element) {
            return this.optional(element) || /^[a-z-.,()'\"\s]+$/i.test(value);
    }, "Letters or punctuation only please");

    $.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^\w+$/i.test(value);
    }, "Letters, numbers, spaces or underscores only please");

    $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Letters only please");
    $.validator.addMethod("numbersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Numbers only please");
    $.validator.addMethod('numericOnly', function (value, element) {
       return this.optional(element) || /^[0-9]+$/.test(value);
    }, 'Please only enter numeric values (0-9)');
    $.validator.addMethod("nowhitespace", function(value, element) {
            return this.optional(element) || /^\S+$/i.test(value);
    }, "No white space please");
    $.validator.addMethod("bloodpressure", function(value,element){
        return this.optional(element) || value.match(/^\d\d\d?\/\d\d\d?$/);
    },"Blood Pressure must be format ###/###");
    $.validator.addMethod('IP4Checker', function(value) {
            var ip = "^(?:(?:25[0-5]2[0-4][0-9][01]?[0-9][0-9]?)\.){3}" +
                "(?:25[0-5]2[0-4][0-9][01]?[0-9][0-9]?)$";
                return value.match(ip);
            }, 'Invalid IP address');