/**
* global.js - 3rd party js functions
*
*/


/***********************************************
* Show Hint script- ??? Dynamic Drive (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for this script and 100s more.
***********************************************/

var horizontal_offset="9px";



var vertical_offset="0";
var ie=document.all;
var ns6=document.getElementById&&!document.all;
function clickThru(table,id,trigger_id){
    //call ajax to log evet
    $.ajax({
        url: 'LogEvent?table='+table+'&prod_id='+id+'&trigger_id='+trigger_id,
        error: function(){
               
        },
        success: function(response){
           
        }
    });
}
function showDiv(div){
    if($('#'+div).is(':visible')){
        $('#'+div).hide();
        $('#'+div+'_titlepanel').addClass('bg_pink');
        $('#'+div+'_uiicon').addClass('ui-title-panel-open');
        $('#'+div+'_uiicon').removeClass('ui-title-panel-close');
    }else{
       $('#'+div).show({effect : 'slide',easing : 'easeOutQuart',direction : 'down',duration : 400}); 
       $('#'+div+'_titlepanel').removeClass('bg_pink');
        $('#'+div+'_uiicon').addClass('ui-title-panel-close');
        $('#'+div+'_uiicon').removeClass('ui-title-panel-open');
    }
}
function moveOnMax(field,nextFieldID){
    
    if(field.value.length >= field.maxLength){
        setTimeout(function() { document.getElementById(nextFieldID).focus(); }, 100);
        document.getElementById(nextFieldID).focus();

    }
}
function deselect(select){
    if(select){
        for (var i=0; i<select.options.length; i++) {
            select.options[i].selected = false;
        }
    }
}

function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}
function parseUrl(data) {
    var e=/^((http|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+\.[^#?\s]+)(#[\w\-]+)?$/;

    if (data.match(e)) {
        return  {url: RegExp['$&'],
                protocol: RegExp.$2,
                host:RegExp.$3,
                path:RegExp.$4,
                file:RegExp.$6,
                hash:RegExp.$7};
    }
    else {
        return  {url:"", protocol:"",host:"",path:"",file:"",hash:""};
    }
}
function getposOffset(what, offsettype){
    var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
    var parentEl=what.offsetParent;
    while (parentEl!=null){
        totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
        parentEl=parentEl.offsetParent;
    }
    return totaloffset;
}
function loadWindow(sUrl,width,height){
    large_fix_win = window.open(sUrl, 'large_fix_win', 'width='+width+',height='+height+',scrollbars=yes,resizable=yes,status=no,location=no,menubar=no');
    large_fix_win.focus();
}
function iecompattest(){
    return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function clearbrowseredge(obj, whichedge){
    var edgeoffset=(whichedge=="rightedge")? parseInt(horizontal_offset)*-1 : parseInt(vertical_offset)*-1
    if (whichedge=="rightedge"){
        var windowedge=ie && !window.opera? iecompattest().scrollLeft+iecompattest().clientWidth-30 : window.pageXOffset+window.innerWidth-40
        dropmenuobj.contentmeasure=dropmenuobj.offsetWidth
        if (windowedge-dropmenuobj.x < dropmenuobj.contentmeasure)
            edgeoffset=dropmenuobj.contentmeasure+obj.offsetWidth+parseInt(horizontal_offset)
    }
    else{
        var windowedge=ie && !window.opera? iecompattest().scrollTop+iecompattest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
        dropmenuobj.contentmeasure=dropmenuobj.offsetHeight
        if (windowedge-dropmenuobj.y < dropmenuobj.contentmeasure)
            edgeoffset=dropmenuobj.contentmeasure-obj.offsetHeight
    }
    return edgeoffset
}

function showhint(menucontents, obj, e, tipwidth){
    if ((ie||ns6) && document.getElementById("hintbox")){
        dropmenuobj=document.getElementById("hintbox")
        dropmenuobj.innerHTML=menucontents
        dropmenuobj.style.left=dropmenuobj.style.top=-500
        if (tipwidth!=""){
            dropmenuobj.widthobj=dropmenuobj.style
            dropmenuobj.widthobj.width=tipwidth
        }
        dropmenuobj.x=getposOffset(obj, "left")
        dropmenuobj.y=getposOffset(obj, "top")
        dropmenuobj.style.left=dropmenuobj.x-clearbrowseredge(obj, "rightedge")+obj.offsetWidth+"px"
        dropmenuobj.style.top=dropmenuobj.y-clearbrowseredge(obj, "bottomedge")+"px"
        dropmenuobj.style.visibility="visible"
        obj.onmouseout=hidetip
    }
}


function hidetip(e){
    dropmenuobj.style.visibility="hidden"
    dropmenuobj.style.left="-500px"
}

function createhintbox(){
    var divblock=document.createElement("div")
    divblock.setAttribute("id", "hintbox")
    document.body.appendChild(divblock)
}

if (window.addEventListener)
    window.addEventListener("load", createhintbox, false)
else if (window.attachEvent)
    window.attachEvent("onload", createhintbox)
else if (document.getElementById)
    window.onload=createhintbox




/* DON"T CHA?NGE ANYThING BELOW! */

function f_clientWidth() {
    return f_filterResults (
        window.innerWidth ? window.innerWidth : 0,
        document.documentElement ? document.documentElement.clientWidth : 0,
        document.body ? document.body.clientWidth : 0
        );
}
function f_scrollLeft() {
    return f_filterResults (
        window.pageXOffset ? window.pageXOffset : 0,
        document.documentElement ? document.documentElement.scrollLeft : 0,
        document.body ? document.body.scrollLeft : 0
        );
}

function f_scrollTop() {
    return f_filterResults (
        window.pageYOffset ? window.pageYOffset : 0,
        document.documentElement ? document.documentElement.scrollTop : 0,
        document.body ? document.body.scrollTop : 0
        );
}

function f_filterResults(n_win, n_docel, n_body) {
    var n_result = n_win ? n_win : 0;
    if (n_docel && (!n_result || (n_result > n_docel)))
        n_result = n_docel;
    return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}
function MM_swapImgRestore() { 
    var i,x,a=document.MM_sr;
    for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() {
    var d=document;
    if(d.images){
        if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
        for(i=0; i<a.length; i++){
            if (a[i].indexOf("#")!=0){
                d.MM_p[j]=new Image;
                d.MM_p[j++].src=a[i];
            }
        }
    }
}

function MM_findObj(n, d) {
    var p,i,x;
    if(!d) d=document;
    if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document;
        n=n.substring(0,p);
    }
    if(!(x=d[n])&&d.all) x=d.all[n];
    for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
    for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
    if(!x && d.getElementById) x=d.getElementById(n);
    return x;
}
function disableField(box,value){
    if(box.checked == false){
        value.disabled=true;
        value.value="";
    }else{
        value.disabled=false;
    }
}
function MM_swapImage() {
    var i,j=0,x,a=MM_swapImage.arguments;
    document.MM_sr=new Array;
    for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){
            document.MM_sr[j++]=x;
            if(!x.oSrc) x.oSrc=x.src;
            x.src=a[i+2];
        }
}


/**
*
* MD5 (Message-Digest Algorithm)
* http://www.webtoolkit.info/
*
**/

var MD5 = function (string) {

    function RotateLeft(lValue, iShiftBits) {
        return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
    }

    function AddUnsigned(lX,lY) {
        var lX4,lY4,lX8,lY8,lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
    }

    function F(x,y,z) {
        return (x & y) | ((~x) & z);
    }
    function G(x,y,z) {
        return (x & z) | (y & (~z));
    }
    function H(x,y,z) {
        return (x ^ y ^ z);
    }
    function I(x,y,z) {
        return (y ^ (x | (~z)));
    }

    function FF(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function GG(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function HH(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function II(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function ConvertToWordArray(string) {
        var lWordCount;
        var lMessageLength = string.length;
        var lNumberOfWords_temp1=lMessageLength + 8;
        var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
        var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
        var lWordArray=Array(lNumberOfWords-1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while ( lByteCount < lMessageLength ) {
            lWordCount = (lByteCount-(lByteCount % 4))/4;
            lBytePosition = (lByteCount % 4)*8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount-(lByteCount % 4))/4;
        lBytePosition = (lByteCount % 4)*8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
        lWordArray[lNumberOfWords-2] = lMessageLength<<3;
        lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
        return lWordArray;
    };

    function WordToHex(lValue) {
        var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
        for (lCount = 0;lCount<=3;lCount++) {
            lByte = (lValue>>>(lCount*8)) & 255;
            WordToHexValue_temp = "0" + lByte.toString(16);
            WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
        }
        return WordToHexValue;
    };

    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    };

    var x=Array();
    var k,AA,BB,CC,DD,a,b,c,d;
    var S11=7, S12=12, S13=17, S14=22;
    var S21=5, S22=9 , S23=14, S24=20;
    var S31=4, S32=11, S33=16, S34=23;
    var S41=6, S42=10, S43=15, S44=21;

    string = Utf8Encode(string);

    x = ConvertToWordArray(string);

    a = 0x67452301;
    b = 0xEFCDAB89;
    c = 0x98BADCFE;
    d = 0x10325476;

    for (k=0;k<x.length;k+=16) {
        AA=a;
        BB=b;
        CC=c;
        DD=d;
        a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
        d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
        c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
        b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
        a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
        d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
        c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
        b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
        a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
        d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
        c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
        b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
        a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
        d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
        c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
        b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
        a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
        d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
        c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
        b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
        a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
        d=GG(d,a,b,c,x[k+10],S22,0x2441453);
        c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
        b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
        a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
        d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
        c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
        b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
        a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
        d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
        c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
        b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
        a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
        d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
        c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
        b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
        a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
        d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
        c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
        b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
        a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
        d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
        c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
        b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
        a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
        d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
        c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
        b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
        a=II(a,b,c,d,x[k+0], S41,0xF4292244);
        d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
        c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
        b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
        a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
        d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
        c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
        b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
        a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
        d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
        c=II(c,d,a,b,x[k+6], S43,0xA3014314);
        b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
        a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
        d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
        c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
        b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
        a=AddUnsigned(a,AA);
        b=AddUnsigned(b,BB);
        c=AddUnsigned(c,CC);
        d=AddUnsigned(d,DD);
    }

    var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

    return temp.toLowerCase();
}

function generatePHN(objForm)
{

    var intRandom=Math.random()*100000000;
    while (intRandom<9999998)
    {
        intRandom=intRandom*11;
    }
    var intCheck=-1;
    while(intCheck==-1 || intCheck>9)
    {
        intRandom++;
        if(intRandom>99999999)
        {
            intRandom=10000000;
        }
        var strPHN=(intRandom+"0000000000").substring(0,8);
        intCheck=1;
        intCheck=11-(((parseInt(strPHN.charAt(0))*2)%11+(parseInt(strPHN.charAt(1))*4)%11+(parseInt(strPHN.charAt(2))*8)%11+(parseInt(strPHN.charAt(3))    *5)%11+(parseInt(strPHN.charAt(4))*10)%11+(parseInt(strPHN.charAt(5))*9)%11+(parseInt(strPHN.charAt(6))*7)%11+(parseInt(strPHN.charAt(7))*3)%11)%11);
    }
    strPHN="9"+strPHN+intCheck;
    
    if(objForm.phn1_1 == undefined){
        objForm.editphn1_1.value=(strPHN+"").substring(0,4);
        objForm.editphn1_2.value=(strPHN+"").substring(4,7);
        objForm.editphn1_3.value=(strPHN+"").substring(7,10);
    }else{
        objForm.phn1_1.value=(strPHN+"").substring(0,4);
        objForm.phn1_2.value=(strPHN+"").substring(4,7);
        objForm.phn1_3.value=(strPHN+"").substring(7,10);
    }
}

function setFocus1(field,tofield){

    if(field.value.length==4){
        tofield.focus();
    }
}
function setFocus2(field,tofield){
    if(field.value.length==3){
        tofield.focus();
    }
}
function showDatepicker(id) {
    $('#' + id).val($('#' + id + '_day').val() + '/' + $('#' + id + '_month').val() + '/' + $('#' + id + '_year').val());
    $('#' + id).datepicker('show');
}
function popDate(date){
    var day = $('#' + date.id + '_day').val();
    var month = $('#' + date.id + '_month').val();
    var year =  $('#' + date.id + '_year').val();
    $('#'+date.id).val(day+'/'+month+'/'+year);
}
function dateRequired(name) {
    return $("#"+name+"_day")[0].selectedIndex > 0 &&
    $("#"+name+"_month")[0].selectedIndex > 0 && $("#"+name+"_year").val().length;
}
function setDate(text, control) {
    var dt = text.split('/');
    $('#' + control.id + '_day').val(dt[0]);
    $('#' + control.id + '_month').val(dt[1]);
    $('#' + control.id + '_year').val(dt[2]);
}

function id(id){
    var el =  document.getElementById(id);
    return el;
}
function addByElement(html){
    if(html.indexOf()!=-1){

}
}
function removeByElement(arrayName,arrayElement)
{
    for(var i=0; i<arrayName.length;i++ )
    {
        if(arrayName[i]==arrayElement)
            arrayName.splice(i,1);
    }
}
function getPos(id) {
    var el = document.getElementById(id);
    for (var lx=0, ly=0;
        el != null;
        lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {
        x: lx,
        y: ly
    };
}
function allowMove(){
    $("#$!{al}box").draggable({
        scroll: false,
        collide: 'block',
        snap: true,
        snapMode: 'both',
        opacity: 0.35,
        cursorAt: {
            left: 5
        },
        cursor: 'move',
        revert: "invalid"
    });
}
function addAlphaToEtiology(text,alpha){
    var et = document.getElementById('etiology_alphas');
    if(alpha == ""){
        et.options[et.length] = new Option(text,text);
    }else{
        et.options[et.length] = new Option(text+' '+alpha.toUpperCase(),text+' '+alpha.toUpperCase());
    }
}
function checkBurnFromAlpha(){
    var et = document.getElementById('etiology_alphas');
    for(var i = 0;i<et.length;i++){
        var al = et[i].text;
        if(al == "Burn"){
            return true;
        }
    }
    return false;
}
function removeAlphafromEtiology(text,alpha,alpha_type){
    var et = document.getElementById('etiology_alphas');
    var etiology = document.getElementById('etiology');
    for(var i = 0;i<et.length;i++){
        var al = et[i].value;
        if(al.indexOf(text+' '+alpha)!=-1 && alpha_type == 'A'){
            et.remove(i);
            for(var x = 0;x<etiology.length;x++){
                if(etiology[x].value.indexOf(text+' '+alpha)!=-1){
                    etiology.remove(x);
                }
            }
        }else if(al.indexOf(text+' '+alpha.substring(2,alpha.length))!=-1 && alpha_type == 'D'){
            et.remove(i);
            for(var x = 0;x<etiology.length;x++){
                if(etiology[x].value.indexOf(text+' '+alpha.substring(2,alpha.length))!=-1){
                    etiology.remove(x);
                }
            }
        } else if(al.indexOf(text+' '+alpha.substring(1,alpha.length))!=-1 && alpha_type == 'S'){
            et.remove(i);
            for(var x = 0;x<etiology.length;x++){
                if(etiology[x].value.indexOf(text+' '+alpha.substring(1,alpha.length))!=-1){
                    etiology.remove(x);
                }
            }
        } else if(al.indexOf(text+' '+alpha.substring(3,alpha.length))!=-1 && alpha_type == 'T'){
            et.remove(i);
            for(var x = 0;x<etiology.length;x++){
                if(etiology[x].value.indexOf(text+' '+alpha.substring(3,alpha.length))!=-1){
                    etiology.remove(x);
                }
            }
        }else if(al.indexOf(text)!=-1 && alpha_type == 'O'){
            et.remove(i);
            for(var x = 0;x<etiology.length;x++){
                if(etiology[x].value.indexOf(text)!=-1){
                    etiology.remove(x);
                }
            }
        }
    }

}
function removeAlphafromAcquired(text,alpha,alpha_type){
    var et = document.getElementById('wound_acquired_alphas');
    var obtained = document.getElementById('wound_acquired');
    for(var i = 0;i<et.length;i++){
        var al = et[i].value;
        if(al.indexOf(text+' '+alpha)!=-1 && alpha_type == 'A'){
            et.remove(i);
            for(var x = 0;x<obtained.length;x++){
                if(obtained[x].value.indexOf(text+' '+alpha)!=-1){
                    obtained.remove(x);
                }
            }
        }else if(al.indexOf(text+' '+alpha.substring(2,alpha.length))!=-1 && alpha_type == 'D'){
            et.remove(i);
            for(var x = 0;x<obtained.length;x++){
                if(obtained[x].value.indexOf(text+' '+alpha.substring(2,alpha.length))!=-1){
                    obtained.remove(x);
                }
            }
        } else if(al.indexOf(text+' '+alpha.substring(3,alpha.length))!=-1 && alpha_type == 'T'){
            et.remove(i);
            for(var x = 0;x<obtained.length;x++){
                if(obtained[x].value.indexOf(text+' '+alpha.substring(3,alpha.length))!=-1){
                    obtained.remove(x);
                }
            }
        }else if(al.indexOf(text+' '+alpha.substring(1,alpha.length))!=-1 && alpha_type == 'S'){
            et.remove(i);
            for(var x = 0;x<obtained.length;x++){
                if(obtained[x].value.indexOf(text+' '+alpha.substring(1,alpha.length))!=-1){
                    obtained.remove(x);
                }
            }
        }else if(al.indexOf(text)!=-1 && alpha_type == 'O'){
            et.remove(i);
            for(var x = 0;x<obtained.length;x++){
                if(obtained[x].value.indexOf(text)!=-1){
                    obtained.remove(x);
                }
            }
        }
    }

}
function removeAlphafromGoals(text,alpha,alpha_type){
    var et = document.getElementById('goals_alphas');
    var goals = document.getElementById('goals');
    for(var i = 0;i<et.length;i++){
        var al = et[i].value;
        
        if(al.indexOf(text+' '+alpha)!=-1 && alpha_type == 'A'){
            et.remove(i);
            for(var x = 0;x<goals.length;x++){
                if(goals[x].value.indexOf(text+' '+alpha)!=-1){
                    goals.remove(x);
                }
            }
        }else if(al.indexOf(text+' '+alpha.substring(2,alpha.length))!=-1 && alpha_type == 'D'){

            et.remove(i);
            for(var x = 0;x<goals.length;x++){
                if(goals[x].value.indexOf(text+' '+alpha.substring(2,alpha.length))!=-1){
                    goals.remove(x);
                }
            }
        } else if(al.indexOf(text+' '+alpha.substring(1,alpha.length))!=-1 && alpha_type == 'S'){

            et.remove(i);
            for(var x = 0;x<goals.length;x++){
                if(goals[x].value.indexOf(text+' '+alpha.substring(1,alpha.length))!=-1){
                    goals.remove(x);
                }
            }
        } else if(al.indexOf(text+' '+alpha.substring(3,alpha.length))!=-1 && alpha_type == 'T'){
            et.remove(i);
            for(var x = 0;x<goals.length;x++){
                if(goals[x].value.indexOf(text+' '+alpha.substring(3,alpha.length))!=-1){
                    goals.remove(x);
                }
            }
        }else if(al.indexOf(text)!=-1 && alpha_type == 'O'){
            et.remove(i);
            for(var x = 0;x<goals.length;x++){
                if(goals[x].value.indexOf(text)!=-1){
                    goals.remove(x);
                }
            }
        }
    }

}
function addAlphaToGoals(text,alpha){
    var g = document.getElementById('goals_alphas');
    if(alpha == ""){
        g.options[g.length] = new Option(text,text);
    }else{
        g.options[g.length] = new Option(text+' '+alpha.toUpperCase(),text+' '+alpha.toUpperCase());
    }
}
function addAlphaToObtained(text,alpha){
    var g = document.getElementById('wound_acquired_alphas');
    if(g){
        if(alpha == ""){
            g.options[g.length] = new Option(text,text);
        }else{
            g.options[g.length] = new Option(text+' '+alpha.toUpperCase(),text+' '+alpha.toUpperCase());
        }
    }
}
function addItem(field1, field2,text){
    var products=field1;
    var choosen=field2;
    var prodlen=products.length;
    var cholen=choosen.length;
    for (var i=0; i<prodlen ; i++){
        if (products.options[i].selected == true ){
            cholen=choosen.length;
            var there=0;
            for(var x=0;x<cholen;x++){
                if(choosen.options[x].value==products.options[i].value){
                    there=1;
                }
            }
            if(there==0){
                choosen.options[cholen]= new Option(products.options[i].text,products.options[i].value);
            }
            else{
        }
        }
    }
}
/*function addItem(field1,field2,tofield){
   var len = tofield.length;
   var already_exists=0;
   var item= field1+":"+field2;
   for(var i = 0;i<len;i++){
       if(item == tofield[i].value){
           already_exists=1;
       }

   }
   if(already_exists == 0){
       tofield.options[len]= new Option(item,item);
   }else{
       alert("$text.get('"+text+"')");
   }

}*/

function removeAll(objSelect){
    for (i = 0; i < objSelect.length; i++) {
        objSelect.options[i].selected=true;
    }
    removeItem(objSelect);
}
function removeItem(objSelect){
    for(i=objSelect.length;i>-1;i--){
        if(objSelect.options[i]!=null && objSelect.options[i].selected==true){
            objSelect.options[i]=null;
        }
    }
}

function fillTreatmentDropdown(category,treatObj){
    for(var i=(treatObj.length-1);i>=0;i--) {
        treatObj.options[i] = null;
    }
    var count=1;
    treatObj.options[0] = new Option("","");
    for (i = 0; i < treatments.length; i++) {
        if(category.options[category.selectedIndex].value == treatments[i][0]){
            treatObj.options[count] = new Option(treatments[i][2], treatments[i][1]);
            count++;
        }
    }
    if (count==2)
    {
        treatObj.selectedIndex=1;
    }
    treatObj.disabled=count==1;
}
function verifySearch(patient_id){
    var fail=0;
    var Answer;
    if(patient_id){
        if(patient_id == document.searchN.search.value){
            return true;
        }else{

            $( "#chgpat_confirm" ).dialog( "open" );
        }
    }
    else{
        return true;
    }
    return false;

}
function catchBS() {
    if (window.event && window.event.keyCode == 8) {
        var strElement = window.event.srcElement.type;
        if (strElement!="text" && strElement!="textarea" && strElement!="password" && strElement!="file"){
            window.event.cancelBubble = true;
            window.event.returnValue = false;
            return false;
        }
    }
    disableCtrlKeyCombination(event);
}

function setRequired(strAsterisk)
{
    var intObj=1;
    objTemp=document.getElementById('req'+intObj);
    while(objTemp)
    {
        objTemp.innerText = strAsterisk;
        intObj++;
        objTemp=document.getElementById('req'+intObj);
    }
}

function checkPopupBlocker(objPopup){
    if (objPopup==null || typeof(objPopup)=="undefined") {
        showErrorInline("$text.get('pixalere.login.form.popups_blocked')");
    }
    
}

function reverseToggleDiv(divName,componentName){
    thisDiv = document.getElementById(divName);
    var radios = document.getElementsByName(componentName);
    for (i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
            if( radios[i].value == 0){
                thisDiv.style.display = "block";
            }else{
                thisDiv.style.display = "none";
            }
        }
    }
}
function toggleDiv(divName){
    thisDiv = document.getElementById(divName+"Div");
    var radios = document.getElementsByName(divName+"_show");
    for (i = 0; i < radios.length; i++) {
        if (radios[i].checked) {
            if( radios[i].value == 2){
                thisDiv.style.display = "block";
            }else{
                thisDiv.style.display = "none";
            }
        }
    }
    
}
function submitForm(){
    document.form.submit();
}
function calculateCheckboxScore(c, t){
    var textfield = document.getElementById(t);

    var score=0;
    if(textfield.value!=""){
        score=parseInt(textfield.value);
    }
    
    if(c.checked)
    {
        score=score+1;
    }else{
        score=score-1;
    }
    textfield.value = score;
}
function transition(redirect){
    document.form.page.value=redirect;
    selectAll();
    verify();
}
/**
 * Resort listbox
 */
function fixForMultiply(intInput)
{
    if(intInput==0)
    {
        return 1;
    }  
    else
    {
        return intInput;
    }
}
function fixDecimal(intInput)
{
    strResult=intInput+"";
    if(strResult.indexOf(".")==-1)
    {
        strResult=strResult+".0"; 
    }
    return strResult;
}

function topOfSelect(obj) { 
    obj = (typeof obj == "string") ? document.getElementById(obj) : obj;
    if (obj.tagName.toLowerCase() != "select" && obj.length < 2)
        return false;
    var elements = new Array();
    for (var i=0; i<obj.length; i++) {
        if (obj[i].selected) {
            elements[elements.length] = new Array((document.body.innerHTML ? obj[i].innerHTML : obj[i].text), obj[i].value, obj[i].style.color, obj[i].style.backgroundColor, obj[i].className, obj[i].id, obj[i].selected);
        }
    }
    for (i=0; i<obj.length; i++) {
        if (!obj[i].selected) {
            elements[elements.length] = new Array((document.body.innerHTML ? obj[i].innerHTML : obj[i].text), obj[i].value, obj[i].style.color, obj[i].style.backgroundColor, obj[i].className, obj[i].id, obj[i].selected);
        }
    }
    for (i=0; i<obj.length; i++) {
        if (document.body.innerHTML) obj[i].innerHTML = elements[i][0];
        else obj[i].text = elements[i][0];
        obj[i].value = elements[i][1];
        obj[i].style.color = elements[i][2];
        obj[i].style.backgroundColor = elements[i][3];
        obj[i].className = elements[i][4];
        obj[i].id = elements[i][5];
        obj[i].selected = elements[i][6];
    }
}

/**
 * Only allow float in textfield
 */
function floatOnly(i){
    value = i.value;
    if ((undefined === value) || (null === value)) {
        return false;
    }
    if (typeof value == 'number') {
        return true;
    }

    return !isNaN(value - 0);
}
/**
 * verify checkbox control, enable/disable fieldtype
 */
function verifyCheckboxOther(controlenabled,other){

    if(controlenabled == true){

        other.disabled=false;
        //document.getElementById(other).style.border="1px solid #6666FF";
    }else{

        other.value="";
        other.disabled=true;
        //document.getElementById(other).style.border="1px solid #ACA899";
    }
}


function disableCheckboxes(control,field){
    var i;
    
    for (i=0;i<field.length;i++)
    {
        
        if(field[i].id!=control.id)
        {
            if(control.checked)
            {
                field[i].checked=false;
                if(field[i].id.indexOf("other")!=-1)
                {
                    
                    verifyCheckboxOther(false,"other"+field[i].value)
              
                }
            }
            field[i].disabled=control.checked;
        }
    }
}

function dateHeight(dateStr){
        if (trim(dateStr) != '') {
                var frDate = trim(dateStr).split(' ');
                var frTime = frDate[1].split('h');
                var frDateParts = frDate[0].split('/');
                var day = frDateParts[0] * 60 * 24;
                var monthTxt = frDateParts[1];
                var month=12;
                if(monthTxt=="jan" || monthTxt=="Jan"){month=01;}
                else if(monthTxt=="feb" || monthTxt=="Feb"){month=02;}
                else if(monthTxt=="mar" || monthTxt=="Mar"){month=03;}
                else if(monthTxt=="apr" || monthTxt=="Apr"){month=04;}
                else if(monthTxt=="may" || monthTxt=="May"){month=05;}
                else if(monthTxt=="jun" || monthTxt=="Jun"){month=06;}
                else if(monthTxt=="jul" || monthTxt=="Jul"){month=07;}
                else if(monthTxt=="aug" || monthTxt=="Aug"){month=08;}
                else if(monthTxt=="sep" || monthTxt=="Sep"){month=09;}
                else if(monthTxt=="oct" || monthTxt=="Oct"){month=10;}
                else if(monthTxt=="nov" || monthTxt=="Nov"){month=11;} 
           
                var year = frDateParts[2] * 60 * 24 * 366;
                //var hour = frTime[0];// * 60;
                //var minutes = frTime[1];
                var x = day+month+year//+hour;//+minutes;
        } else {
                var x = 99999999999999999; //GoHorse!
        }
        return x;
}


/**
 * Trim whitespace
 */
function trim(str)
{
    return str.replace(/^\s*|\s*$/g,"");
}
function showErrorInline(themessage){
    document.getElementById("errorAlert").text=themessage;
    document.getElementById('error').style.display='block';
}
/**
 * Display errors in popup by type
 * log is used as the exception trace for exception types, and the title for others.
 */
function showError(themessage,type,log){

    var  description="";
    var title="";
    var desc2="";
    if(type == "exception"){

        description=log;
        title=themessage;
    }else if(type == "confirm"){
        title="Confirmation";
        description=themessage;
        desc2=log;
    }else if(type == "saved"){
        title="";
        description=themessage;
    }else{
        title="Validation Failed";
        description=themessage;
    }
    new_window = open("InformationPopupSetup.do?field=0&type="+type+"&title="+title+"&description="+description+"&desc2="+desc2,"infowindow","width=550,height=380,left=40,top=40,scrollbars=1");
    if(!new_window.opener)
        new_window.opener = self;
    if(window.focus)
        new_window.focus();
}
function showWebHelp(language){
    var lang = "";
    if(language == "es"){lang="ES";}
    new_window = open("https://pixalere.atlassian.net/wiki/display/DOC"+lang+"/Reference+Guide+6.0","webhelp","width=860,height=700,left=40,top=40,scrollbars=1");
    if(!new_window.opener)
        new_window.opener = self;
    if(window.focus)
        new_window.focus();
}
/**
 * Display info in popup by type
 */
function showInfoPopup(field){

    new_window = open("InformationPopupSetup.do?field="+field,"infowindow","width=1050,height=680,left=40,top=40,scrollbars=yes");
    if(!new_window.opener)
        new_window.opener = self;
    if(window.focus)
        new_window.focus();
}
function loadLibrary(){
    new_window_library = open("LibrarySetup.do","librarywindow","width=800,height=520,left=40,top=40,resizable=yes,scrollbars=yes");
    if(!new_window_library.opener)
        new_window_library.opener = self;
    if(window.focus)
        new_window_library.focus();
}


/**
 * Set background image for bluemodel/alphas
 */
function setBackgroundImage (id, imageURL) {
    if (document.layers){
        document[id].background.src = imageURL == 'none'? null : imageURL;
    }
    else if (document.all){
        if(document.all[id]){
            document.all[id].style.backgroundImage = imageURL == 'none'? 'none'
            : 'url(' + imageURL + ')';
        }
    }
    else if (document.getElementById ){
        if(document.getElementById(id)){
            document.getElementById(id).style.backgroundImage = imageURL ==
            'none'? 'none' : 'url(' + imageURL + ')';
        }
    }
}
/**
 * Gets the alpha text from the alpha key
 */
function getAlpha(alpha_name, alpha_type){
    var alpha="";
    if(alpha_name.indexOf("ostu") !=-1){
        alpha = "Urostomy";
    }else if(alpha_name.indexOf("ostf") != -1){
        alpha = "Fecal Stoma";
    }else if(alpha_type == "A"){
        alpha = "Wound "+alpha_name.toUpperCase();
    }else if(alpha_type == "D"){
        alpha = "Tube/Drain "+alpha_name.substring(2);
    }else if(alpha_type == "T"){
        alpha = "Incision "+alpha_name.substring(3);
    }else if(alpha_type == "B"){
        alpha = "Burn";
    }
    return alpha;
}


function showBluemodelTitleFromImage(strImage,strTitleLeft,strTitleRight,strBiLateral) {
    if (strImage.indexOf("_l.jpg")!= -1 || strImage.indexOf("_left_")!= -1)
    {
        showBluemodelTitle(strTitleLeft);
    }
    if (strImage.indexOf("_r.jpg")!= -1 || strImage.indexOf("_right_")!= -1)
    {
        showBluemodelTitle(strTitleRight);
    }
    if(strImage.indexOf("_bil_")!= -1){
        showBluemodelTitle(strBiLateral);
    }
}

function showBluemodelTitle(strTitle) {
    if (strTitle!="")
    {
        document.getElementById('bluemodeltitle').innerHTML="<img src='images/bluemodel/bullet_red.gif'>&nbsp;"+strTitle+"&nbsp;<img src='images/bluemodel/bullet_red.gif'>";
    }
    else
    {    
        document.getElementById('bluemodeltitle').innerHTML="";
    }
}
function verifyNCP(){

}
function verifyHome(patient_id,ref){
    if(patient_id){
        document.searchN.search.value='';
        document.searchN.referralPop.value=ref;
        $( "#chgpat_confirm" ).dialog( "open" );
        
    }else{
        document.searchN.search.value='';
        document.searchN.referralPop.value=ref;
        document.searchN.submit();
    }
}



function validateDate(allowempty,year,month,day){
    var right_now=new Date();
    var invalid_date=0;
    if((year == undefined || year.value == "") && (month == undefined || month.selectedIndex == 0) && (day == undefined ||  day.selectedIndex == 0)  && allowempty == 1){
        invalid_date=0;
    }else{
        if(year.value>0 && (year.value < 1890 || year.value > (right_now.getFullYear()))){
            invalid_date=1;
        }
        if(invalid_date == 0 && year.value == right_now.getFullYear() && month.selectedIndex > right_now.getMonth()+1){
            invalid_date=1;
        }
        if(invalid_date == 0 && year.value == right_now.getFullYear() && month.selectedIndex == right_now.getMonth()+1 && day.selectedIndex> right_now.getDate()){
            invalid_date=1;
        }
        if(invalid_date == 0 && month.selectedIndex <1 || day.selectedIndex <1 || year.value == ""){
            invalid_date=1;
        }

        if (invalid_date == 0 && (year.value<1890 || year.value>2050) ||
            (day.selectedIndex>=30 && month.selectedIndex==2) ||
            (day.selectedIndex==31 && (month.selectedIndex==4 ||
                month.selectedIndex==6 || month.selectedIndex==9 ||
                month.selectedIndex==11))) {
            invalid_date=1;
        }

        if (invalid_date == 0 && (day.selectedIndex==29 && month.selectedIndex==2) &&
            (year.value % 4 > 0 || (year.value % 100 == 0 && year.value % 400 > 0))) {
            invalid_date=1;
        }
    }
    
    return invalid_date;
}
function bloodPressureOnly(e){
    var keynum=-1;
    var numcheck;
    if(e.which) 
    {
        keynum = e.which;
    }
    else if(window.event)
    {
        keynum = e.keyCode;
    }
    numcheck = /\d/;
    
    return (numcheck.test(String.fromCharCode(keynum)) || keynum==8 || keynum==-1 || keynum==47);
}


function intOnly(e) {
    var keynum=-1;
    var numcheck;
    if(e.which) 
    {
        keynum = e.which;
    }
    else if(window.event) 
    {
        keynum = e.keyCode;
    }
    numcheck = /\d/;
    
    return (numcheck.test(String.fromCharCode(keynum)) || keynum==8 || keynum==-1);
}

function decimalsOnly(e) {
    var keynum=-1;
    var numcheck;
    if(window.event) 
    {
        keynum = e.keyCode;
    }
    else if(e.which) 
    {
        keynum = e.which;
    }
    numcheck = /\d/;
    return (numcheck.test(String.fromCharCode(keynum)) || keynum==46 || keynum==8 || keynum==-1)
}
function intAlphaOnly(e){
    return true;
}
function alphaOnly(e) {
    var keynum=-1;
    var numcheck;
    if(window.event) 
    {
        keynum = e.keyCode;
    }
    else if(e.which) 
    {
        keynum = e.which;
    }
    numcheck = /\d/;
    return (!numcheck.test(String.fromCharCode(keynum)) );
}
function stripBackSlashes(value){
    return value.replace(/[\\]/g,'/');
}
function verifyWound(wound_id){
    var fail=0;
    var Answer;
    if(document.form.anterior){
        $( "chgwnd_confirm" ).dialog( "open" );
    }else{
        return true;
    }
    return false;
	
}
function displayage(dobYear, dobMonth, dobDay, errormsg,label){

    

    var today=new Date();
    var age=today.getFullYear()-dobYear;

    if (dobDay<=0 || dobMonth<=0 || dobYear<=0 || dobYear.length<4) age=-1; 

    if (age>-1) {
        if (today.getMonth()+1<dobMonth) age=age-1; 
        if (today.getMonth()+1==dobMonth && today.getDate()<dobDay) age=age-1; 

        if (today.getFullYear()==dobYear && dobMonth>today.getMonth()+1) age=-1;
        if (today.getFullYear()==dobYear && dobMonth==today.getMonth()+1 && dobDay>today.getDate()) age=-1;

        if ((dobDay>=30 && dobMonth==2) || (dobDay==31 && (dobMonth==4 || dobMonth==6 || dobMonth==9 || dobMonth==11))) age=-1;
        if ((dobDay==29 && dobMonth==2) && (dobYear % 4 > 0 || (dobYear % 100 == 0 && dobYear % 400 > 0))) age=-1;
        if (age<0) {
            document.getElementById(label).innerHTML=errormsg; 
        }
        else if(age == 0){
            var months=today.getMonth() - dobMonth;
            if(dobDay<=today.getDate())
            {
                months++;
            }
            if(dobYear<today.getFullYear())
            {
                months=today.getMonth()+12 - dobMonth;
            }
            document.getElementById(label).innerHTML=months+" months";
        }else{
            document.getElementById(label).innerHTML=age; 
        }
    }
    else{
        document.getElementById(label).innerHTML=""; 
    }
}
/**
 * Calculate patients age from DOB
 */
function calcAge(dob_year, dob_month, dob_day, error,label) {
    displayage(dob_year.value,dob_month.value,dob_day.value, error,label);
}
function MM_openBrWindow(theURL,winName,features) {
    window.open('popups/'+theURL,winName,features);
}
function popup(theURL,winName,features){
    window.open(theURL,winName,features);
}

function SelectPatient(width,height)
{
    large_image_win = window.open('SearchSetup.do', 'searchPopup', 'width='+width+',height='+height+',scrollbars=yes,resizable=yes,status=no,location=no,menubar=no');
    large_image_win.focus();
}
function addDropDateItem(month,day,year,field_list,field,themessage){
    var cns_smonth=month.options[month.selectedIndex].value;
    var cns_sday=day.options[day.selectedIndex].value;
    var cns_syear=year.value;

if(cns_smonth.length  == 1){
		cns_smonth='0'+cns_smonth;
	}
	if(cns_sday.length  == 1){
		cns_sday='0'+cns_sday;
	}

    var error=0;

    if(!isDate(cns_syear+"-"+cns_smonth+"-"+cns_sday,"yyyy-mm-dd")){
        error=1;
    }

    var cnsname=field_list;
    if(error == 0){
        var cholen = field.length;
        for(i=0;i<cholen;i++){
            field.options[i].selected=false;
        }
        if(cnsname.selectedIndex!=-1 && cnsname.selectedIndex != 0){
            field.options[cholen]= new Option(cnsname.options[cnsname.selectedIndex].text+" : "+cns_sday+"/"+month.options[month.selectedIndex].text+"/"+cns_syear,cnsname.options[cnsname.selectedIndex].value+" : "+cns_sday+"/"+month.options[month.selectedIndex].text+"/"+cns_syear);
            field.options[cholen].selected = true;
        }
        field_list.selectedIndex=-1;
        year.value="";
        month.options[0].selected=true;
        day.options[0].selected=true;

        topOfSelect(field);
    }else{
        showError(themessage,'warn');
    }
}
function removeListItem(choose){
    for(i=choose.length;i>-1;i--){
        if(choose.options[i]!=null && choose.options[i].selected==true){
            choose.options[i]=null;
        }
    }
}
function blockRemoveOldValues(field, alpha){
    for(i=0;i<field.length;i++)
    {
        if(field.options[i].selected==true)
        {
            blnExist=false;
            for(j=0;j<alpha.length;j++)
            {
                if(field.options[i].text.indexOf("Wound "+alpha.options[j].text+": ")!= -1)
                {
                    blnExist=true;
                }
            }
            if(!blnExist)
            {
                field.options[i].selected=false;
            }
        }
    }
}


function addByAlpha(field, alpha, list, multiselect){
    if(list.options[list.selectedIndex].text!="")
    {
        for(intAlpha=0;intAlpha<alpha.length;intAlpha++){
            if(alpha.options[intAlpha].selected==true){
                var intFieldLength = field.length;
                var strNewOption=alpha.options[intAlpha].text+": "+list.options[list.selectedIndex].text;
                var strNewValueOption=alpha.options[intAlpha].value+": "+list.options[list.selectedIndex].value;
                var strAlpha=alpha.options[intAlpha].text+": ";
                var blnExist=false;
                var blnAlphaExist=false;
                for(i=0;i<intFieldLength;i++){
                    if(field.options[i].text==strNewOption || (multiselect=="0" && field.options[i].text.indexOf(strAlpha)!= -1))
                    {
                        blnExist=true;
                        field.options[i].value=strNewValueOption;
                        field.options[i].text=strNewOption;
                        field.options[i].selected=true;
                    }
                    else
                    {
                        field.options[i].selected=false;
                    }
                }
                if(  !blnExist && (list.selectedIndex != 0 || list.selectedIndex != -1)){
                    field.options[intFieldLength]= new Option(strNewOption,strNewValueOption);
                    field.options[intFieldLength].selected = true;
                }
            }
        }
    }
}

function addByAlphaBandages(field, alpha, list,list2, multiselect){
    
    if(list.value!="")
    {
        for(intAlpha=0;intAlpha<alpha.length;intAlpha++){
            if(alpha.options[intAlpha].selected==true){
                var intFieldLength = field.length;
                var strNewOption=alpha.options[intAlpha].text+": "+list.value+"/"+list2.value;
                var strAlpha=alpha.options[intAlpha].text+": ";
                var blnExist=false;
                var blnAlphaExist=false;
                for(i=0;i<intFieldLength;i++){
                    if(field.options[i].text==strNewOption || (multiselect=="0" && field.options[i].text.indexOf(strAlpha)!= -1))
                    {
                        blnExist=true;
                        field.options[i].value=strNewOption;
                        field.options[i].text=strNewOption;
                        field.options[i].selected=true;
                    }
                    else
                    {
                        field.options[i].selected=false;
                    }
                }
                if(  !blnExist && (list.value != "" && list2.value != "")){
                    field.options[intFieldLength]= new Option(strNewOption,strNewOption);
                    field.options[intFieldLength].selected = true;
                }
            }
        }
    }
}
function sortProducts(lb) {

    arrSort = new Array();
  
    for(i=0; i<lb.length; i++)  {
        strAlpha=lb.options[i].text;
        strAlpha=strAlpha.substring(strAlpha.indexOf(":")+1);
        strAlpha=strAlpha.substring(0,strAlpha.indexOf(":"));
        arrSort[i] = strAlpha+"|"+lb.options[i].text+"|"+lb.options[i].value;
    }

    arrSort.sort();

    for(i=0; i<lb.length; i++)  {
        strOption=arrSort[i].substring(arrSort[i].indexOf("|")+1);
        lb.options[i].text = strOption.substring(0,strOption.indexOf("|"));
        lb.options[i].value = strOption.substring(strOption.indexOf("|")+1);
    }
}

function addByAlphaValue(field, alpha, list, multiselect){

    if(list.options[list.selectedIndex].text!="")
    {    
        for(intAlpha=0;intAlpha<alpha.length;intAlpha++){
            if(alpha.options[intAlpha].selected==true){
                var intFieldLength = field.length;
                var strNewOption=alpha.options[intAlpha].text+": "+list.options[list.selectedIndex].text;
                var strNewOptionValue = alpha.options[intAlpha].text+": "+list.options[list.selectedIndex].value;
                var strAlpha=alpha.options[intAlpha].text+": ";
                var blnExist=false;
                for(i=0;i<intFieldLength;i++){
                    if(field.options[i].text==strNewOption || (multiselect=="0" && field.options[i].text.indexOf(strAlpha)!= -1))
                    {
                        blnExist=true;  
                        field.options[i].value=strNewOptionValue;
                        field.options[i].text=strNewOption;
                        field.options[i].selected=true;
                    }
                    else
                    {
                        field.options[i].selected=false;
                    }
                }
                if(!blnExist && (list.selectedIndex != 0 || list.selectedIndex != -1)){
                    field.options[intFieldLength]= new Option(strNewOption,strNewOptionValue);
                    field.options[intFieldLength].selected = true;
                }
            }
        }
    
    }
}

function addByList(field, alpha, list, multiselect){

    if(list.options[list.selectedIndex].text!="")
    {    
        for(intAlpha=0;intAlpha<alpha.length;intAlpha++){
            if(alpha.options[intAlpha].selected==true){
                var intFieldLength = field.length;
                var strNewOption=alpha.options[intAlpha].text+": "+list.options[list.selectedIndex].text;
                var strAlpha=alpha.options[intAlpha].text+": ";
                var blnExist=false;
                for(i=0;i<intFieldLength;i++){
                    if(field.options[i].text==strNewOption || (multiselect=="0" && field.options[i].text.indexOf(strAlpha)!= -1))
                    {
                        blnExist=true;  
                        field.options[i].value=strNewOption;
                        field.options[i].text=strNewOption;
                        field.options[i].selected=true;
                    }
                    else
                    {
                        field.options[i].selected=false;
                    }
                }
                if(!blnExist && (list.selectedIndex != 0 || list.selectedIndex != -1)){
                    field.options[intFieldLength]= new Option(strNewOption,strNewOption);
                    field.options[intFieldLength].selected = true;
                }
            }
        }
    }
}
function removeDropDateItem(field){
    var choose=field;
    var cholen=choose.length;
    for(i=0;i<cholen;i++){
        if(choose.options[i]!=null && choose.options[i].selected==true){
			
            choose.options[i]=null;
			
        }
    }
}
function validateDates(one_month, one_day,one_year, two_month, two_day, two_year){
         
    if (Date.parse(one_month.options[one_month.selectedIndex].value+'/'+one_day.options[one_day.selectedIndex].value+'/'+one_year.value) > Date.parse(two_month.options[two_month.selectedIndex].value+'/'+two_day.options[two_day.selectedIndex].value+'/'+two_year.value)) {
            
        return true;
    }
    return false;
}

function disableCtrlKeyCombination(e)
{
    var forbiddenKeys = new Array('n');
    var key;
    var isCtrl;

    if(window.event)
    {
        key = window.event.keyCode;     
        if(window.event.ctrlKey)
            isCtrl = true;
        else
            isCtrl = false;
    }
    else
    {
        key = e.which;
        if(e.ctrlKey)
            isCtrl = true;
        else
            isCtrl = false;
    }

    if(isCtrl)
    {
        for(i=0; i< forbiddenKeys.length; i++)
        {
            if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
            {
                alert("We're sorry, Ctrl N has been disabled in Pixalere.");
                return false;
            }
        }
    }
    return true;
}

/**
 * Calendar Popup.
 *
 */


function getAnchorPosition(anchorname) {
    var useWindow=false;
    var coordinates=new Object();
    var x=0,y=0;
    var use_gebi=false, use_css=false, use_layers=false;
    if (document.getElementById) {
        use_gebi=true;
    }
    else if (document.all) {
        use_css=true;
    }
    else if (document.layers) {
        use_layers=true;
    }
    if (use_gebi && document.all) {
        x=AnchorPosition_getPageOffsetLeft(document.all[anchorname]);
        y=AnchorPosition_getPageOffsetTop(document.all[anchorname]);
    }
    else if (use_gebi) {
        var o=document.getElementById(anchorname);
        x=AnchorPosition_getPageOffsetLeft(o);
        y=AnchorPosition_getPageOffsetTop(o);
    }
    else if (use_css) {
        x=AnchorPosition_getPageOffsetLeft(document.all[anchorname]);
        y=AnchorPosition_getPageOffsetTop(document.all[anchorname]);
    }
    else if (use_layers) {
        var found=0;
        for (var i=0; i<document.anchors.length; i++) {
            if (document.anchors[i].name==anchorname) {
                found=1;
                break;
            }
        }
        if (found==0) {
            coordinates.x=0;
            coordinates.y=0;
            return coordinates;
        }
        x=document.anchors[i].x;
        y=document.anchors[i].y;
    }
    else {
        coordinates.x=0;
        coordinates.y=0;
        return coordinates;
    }
    coordinates.x=x;
    coordinates.y=y;
    return coordinates;
}

function getAnchorWindowPosition(anchorname) {
    var coordinates=getAnchorPosition(anchorname);
    var x=0;
    var y=0;
    if (document.getElementById) {
        if (isNaN(window.screenX)) {
            x=coordinates.x-document.body.scrollLeft+window.screenLeft;
            y=coordinates.y-document.body.scrollTop+window.screenTop;
        }
        else {
            x=coordinates.x+window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset;
            y=coordinates.y+window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset;
        }
    }
    else if (document.all) {
        x=coordinates.x-document.body.scrollLeft+window.screenLeft;
        y=coordinates.y-document.body.scrollTop+window.screenTop;
    }
    else if (document.layers) {
        x=coordinates.x+window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset;
        y=coordinates.y+window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset;
    }
    coordinates.x=x;
    coordinates.y=y;
    return coordinates;
}

function AnchorPosition_getPageOffsetLeft (el) {
    var ol=el.offsetLeft;
    while ((el=el.offsetParent) != null) {
        ol += el.offsetLeft;
    }
    return ol;
}
function AnchorPosition_getWindowOffsetLeft (el) {
    return AnchorPosition_getPageOffsetLeft(el)-document.body.scrollLeft;
}
function AnchorPosition_getPageOffsetTop (el) {
    var ot=el.offsetTop;
    while((el=el.offsetParent) != null) {
        ot += el.offsetTop;
    }
    return ot;
}
function AnchorPosition_getWindowOffsetTop (el) {
    return AnchorPosition_getPageOffsetTop(el)-document.body.scrollTop;
}


var MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');
function LZ(x) {
    return(x<0||x>9?"":"0")+x
    }

function isDate(val,format) {
    var date=getDateFromFormat(val,format);
    if (date==0) {
        return false;
    }
    return true;
}

function compareDates(date1,dateformat1,date2,dateformat2) {
    var d1=getDateFromFormat(date1,dateformat1);
    var d2=getDateFromFormat(date2,dateformat2);
    if (d1==0 || d2==0) {
        return -1;
    }
    else if (d1 > d2) {
        return 1;
    }
    return 0;
}

function formatDate(date,format) {
    format=format+"";
    var result="";
    var i_format=0;
    var c="";
    var token="";
    var y=date.getYear()+"";
    var M=date.getMonth()+1;
    var d=date.getDate();
    var E=date.getDay();
    var H=date.getHours();
    var m=date.getMinutes();
    var s=date.getSeconds();
    var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
    var value=new Object();
    if (y.length < 4) {
        y=""+(y-0+1900);
    }
    value["y"]=""+y;
    value["yyyy"]=y;
    value["yy"]=y.substring(2,4);
    value["M"]=M;
    value["MM"]=LZ(M);
    value["MMM"]=MONTH_NAMES[M-1];
    value["NNN"]=MONTH_NAMES[M+11];
    value["d"]=d;
    value["dd"]=LZ(d);
    value["E"]=DAY_NAMES[E+7];
    value["EE"]=DAY_NAMES[E];
    value["H"]=H;
    value["HH"]=LZ(H);
    if (H==0){
        value["h"]=12;
    }
    else if (H>12){
        value["h"]=H-12;
    }
    else {
        value["h"]=H;
    }
    value["hh"]=LZ(value["h"]);
    if (H>11){
        value["K"]=H-12;
    } else {
        value["K"]=H;
    }
    value["k"]=H+1;
    value["KK"]=LZ(value["K"]);
    value["kk"]=LZ(value["k"]);
    if (H > 11) {
        value["a"]="PM";
    }
    else {
        value["a"]="AM";
    }
    value["m"]=m;
    value["mm"]=LZ(m);
    value["s"]=s;
    value["ss"]=LZ(s);
    while (i_format < format.length) {
        c=format.charAt(i_format);
        token="";
        while ((format.charAt(i_format)==c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        if (value[token] != null) {
            result=result + value[token];
        }
        else {
            result=result + token;
        }
    }
    return result;
}


function _isInteger(val) {
    var digits="1234567890";
    for (var i=0; i < val.length; i++) {
        if (digits.indexOf(val.charAt(i))==-1) {
            return false;
        }
    }
    return true;
}
function _getInt(str,i,minlength,maxlength) {
    for (var x=maxlength; x>=minlength; x--) {
        var token=str.substring(i,i+x);
        if (token.length < minlength) {
            return null;
        }
        if (_isInteger(token)) {
            return token;
        }
    }
    return null;
}


function getDateFromFormat(val,format) {
    val=val+"";
    format=format+"";
    var i_val=0;
    var i_format=0;
    var c="";
    var token="";
    var token2="";
    var x,y;
    var now=new Date();
    var year=now.getYear();
    var month=now.getMonth()+1;
    var date=1;
    var hh=now.getHours();
    var mm=now.getMinutes();
    var ss=now.getSeconds();
    var ampm="";

    while (i_format < format.length) {
        c=format.charAt(i_format);
        token="";
        while ((format.charAt(i_format)==c) && (i_format < format.length)) {
            token += format.charAt(i_format++);
        }
        if (token=="yyyy" || token=="yy" || token=="y") {
            if (token=="yyyy") {
                x=4;
                y=4;
            }
            if (token=="yy")   {
                x=2;
                y=2;
            }
            if (token=="y")    {
                x=2;
                y=4;
            }
            year=_getInt(val,i_val,x,y);
            if (year==null) {
                return 0;
            }
            i_val += year.length;
            if (year.length==2) {
                if (year > 70) {
                    year=1900+(year-0);
                }
                else {
                    year=2000+(year-0);
                }
            }
        }
        else if (token=="MMM"||token=="NNN"){
            month=0;
            for (var i=0; i<MONTH_NAMES.length; i++) {
                var month_name=MONTH_NAMES[i];
                if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
                    if (token=="MMM"||(token=="NNN"&&i>11)) {
                        month=i+1;
                        if (month>12) {
                            month -= 12;
                        }
                        i_val += month_name.length;
                        break;
                    }
                }
            }
            if ((month < 1)||(month>12)){
                return 0;
            }
        }
        else if (token=="EE"||token=="E"){
            for (var i=0; i<DAY_NAMES.length; i++) {
                var day_name=DAY_NAMES[i];
                if (val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()) {
                    i_val += day_name.length;
                    break;
                }
            }
        }
        else if (token=="MM"||token=="M") {
            month=_getInt(val,i_val,token.length,2);
            if(month==null||(month<1)||(month>12)){
                return 0;
            }
            i_val+=month.length;
        }
        else if (token=="dd"||token=="d") {
            date=_getInt(val,i_val,token.length,2);
            if(date==null||(date<1)||(date>31)){
                return 0;
            }
            i_val+=date.length;
        }
        else if (token=="hh"||token=="h") {
            hh=_getInt(val,i_val,token.length,2);
            if(hh==null||(hh<1)||(hh>12)){
                return 0;
            }
            i_val+=hh.length;
        }
        else if (token=="HH"||token=="H") {
            hh=_getInt(val,i_val,token.length,2);
            if(hh==null||(hh<0)||(hh>23)){
                return 0;
            }
            i_val+=hh.length;
        }
        else if (token=="KK"||token=="K") {
            hh=_getInt(val,i_val,token.length,2);
            if(hh==null||(hh<0)||(hh>11)){
                return 0;
            }
            i_val+=hh.length;
        }
        else if (token=="kk"||token=="k") {
            hh=_getInt(val,i_val,token.length,2);
            if(hh==null||(hh<1)||(hh>24)){
                return 0;
            }
            i_val+=hh.length;
            hh--;
        }
        else if (token=="mm"||token=="m") {
            mm=_getInt(val,i_val,token.length,2);
            if(mm==null||(mm<0)||(mm>59)){
                return 0;
            }
            i_val+=mm.length;
        }
        else if (token=="ss"||token=="s") {
            ss=_getInt(val,i_val,token.length,2);
            if(ss==null||(ss<0)||(ss>59)){
                return 0;
            }
            i_val+=ss.length;
        }
        else if (token=="a") {
            if (val.substring(i_val,i_val+2).toLowerCase()=="am") {
                ampm="AM";
            }
            else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {
                ampm="PM";
            }
            else {
                return 0;
            }
            i_val+=2;
        }
        else {
            if (val.substring(i_val,i_val+token.length)!=token) {
                return 0;
            }
            else {
                i_val+=token.length;
            }
        }
    }
    if (i_val != val.length) {
        return 0;
    }
    if (month==2) {
        if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { 
            if (date > 29){
                return 0;
            }
        }
        else {
            if (date > 28) {
                return 0;
            }
        }
}
if ((month==4)||(month==6)||(month==9)||(month==11)) {
    if (date > 30) {
        return 0;
    }
}
if (hh<12 && ampm=="PM") {
    hh=hh-0+12;
}
else if (hh>11 && ampm=="AM") {
    hh-=12;
}
var newdate=new Date(year,month-1,date,hh,mm,ss);
return newdate.getTime();
}


function parseDate(val) {
    var preferEuro=(arguments.length==2)?arguments[1]:false;
    generalFormats=new Array('y-M-d','MMM d, y','MMM d,y','y-MMM-d','d-MMM-y','MMM d');
    monthFirst=new Array('M/d/y','M-d-y','M.d.y','MMM-d','M/d','M-d');
    dateFirst =new Array('d/M/y','d-M-y','d.M.y','d-MMM','d/M','d-M');
    var checkList=new Array('generalFormats',preferEuro?'dateFirst':'monthFirst',preferEuro?'monthFirst':'dateFirst');
    var d=null;
    for (var i=0; i<checkList.length; i++) {
        var l=window[checkList[i]];
        for (var j=0; j<l.length; j++) {
            d=getDateFromFormat(val,l[j]);
            if (d!=0) {
                return new Date(d);
            }
        }
    }
    return null;
}

function PopupWindow_getXYPosition(anchorname) {
    var coordinates;
    if (this.type == "WINDOW") {
        coordinates = getAnchorWindowPosition(anchorname);
    }
    else {
        coordinates = getAnchorPosition(anchorname);
    }
    this.x = coordinates.x;
    this.y = coordinates.y;
}
function PopupWindow_setSize(width,height) {
    this.width = width;
    this.height = height;
}
function PopupWindow_populate(contents) {
    this.contents = contents;
    this.populated = false;
}
function PopupWindow_setUrl(url) {
    this.url = url;
}
function PopupWindow_setWindowProperties(props) {
    this.windowProperties = props;
}
function PopupWindow_refresh() {
    if (this.divName != null) {
        if (this.use_gebi) {
            document.getElementById(this.divName).innerHTML = this.contents;
        }
        else if (this.use_css) {
            document.all[this.divName].innerHTML = this.contents;
        }
        else if (this.use_layers) {
            var d = document.layers[this.divName];
            d.document.open();
            d.document.writeln(this.contents);
            d.document.close();
        }
    }
    else {
        if (this.popupWindow != null && !this.popupWindow.closed) {
            if (this.url!="") {
                this.popupWindow.location.href=this.url;
            }
            else {
                this.popupWindow.document.open();
                this.popupWindow.document.writeln(this.contents);
                this.popupWindow.document.close();
            }
            this.popupWindow.focus();
        }
    }
}
function PopupWindow_showPopup(anchorname) {
    this.getXYPosition(anchorname);
    this.x += this.offsetX;
    this.y += this.offsetY;
    if (!this.populated && (this.contents != "")) {
        this.populated = true;
        this.refresh();
    }
    if (this.divName != null) {
        if (this.use_gebi) {
            document.getElementById(this.divName).style.left = this.x + "px";
            document.getElementById(this.divName).style.top = this.y + "px";
            document.getElementById(this.divName).style.visibility = "visible";
        }
        else if (this.use_css) {
            document.all[this.divName].style.left = this.x;
            document.all[this.divName].style.top = this.y;
            document.all[this.divName].style.visibility = "visible";
        }
        else if (this.use_layers) {
            document.layers[this.divName].left = this.x;
            document.layers[this.divName].top = this.y;
            document.layers[this.divName].visibility = "visible";
        }
    }
    else {
        if (this.popupWindow == null || this.popupWindow.closed) {
            if (this.x<0) {
                this.x=0;
            }
            if (this.y<0) {
                this.y=0;
            }
            if (screen && screen.availHeight) {
                if ((this.y + this.height) > screen.availHeight) {
                    this.y = screen.availHeight - this.height;
                }
            }
            if (screen && screen.availWidth) {
                if ((this.x + this.width) > screen.availWidth) {
                    this.x = screen.availWidth - this.width;
                }
            }
            var avoidAboutBlank = window.opera || ( document.layers && !navigator.mimeTypes['*'] ) || navigator.vendor == 'KDE' || ( document.childNodes && !document.all && !navigator.taintEnabled );
            this.popupWindow = window.open(avoidAboutBlank?"":"about:blank","window_"+anchorname,this.windowProperties+",width="+this.width+",height="+this.height+",screenX="+this.x+",left="+this.x+",screenY="+this.y+",top="+this.y+"");
        }
        this.refresh();
    }
}
function PopupWindow_hidePopup() {
    if (this.divName != null) {
        if (this.use_gebi) {
            document.getElementById(this.divName).style.visibility = "hidden";
        }
        else if (this.use_css) {
            document.all[this.divName].style.visibility = "hidden";
        }
        else if (this.use_layers) {
            document.layers[this.divName].visibility = "hidden";
        }
    }
    else {
        if (this.popupWindow && !this.popupWindow.closed) {
            this.popupWindow.close();
            this.popupWindow = null;
        }
    }
}
function PopupWindow_isClicked(e) {
    if (this.divName != null) {
        if (this.use_layers) {
            var clickX = e.pageX;
            var clickY = e.pageY;
            var t = document.layers[this.divName];
            if ((clickX > t.left) && (clickX < t.left+t.clip.width) && (clickY > t.top) && (clickY < t.top+t.clip.height)) {
                return true;
            }
            else {
                return false;
            }
        }
        else if (document.all) { 
            var t = window.event.srcElement;
            while (t.parentElement != null) {
                if (t.id==this.divName) {
                    return true;
                }
                t = t.parentElement;
            }
            return false;
        }
        else if (this.use_gebi && e) {
            var t = e.originalTarget;
            while (t.parentNode != null) {
                if (t.id==this.divName) {
                    return true;
                }
                t = t.parentNode;
            }
            return false;
        }
        return false;
    }
    return false;
}

function PopupWindow_hideIfNotClicked(e) {
    if (this.autoHideEnabled && !this.isClicked(e)) {
        this.hidePopup();
    }
}
function PopupWindow_autoHide() {
    this.autoHideEnabled = true;
}
function PopupWindow_hidePopupWindows(e) {
    for (var i=0; i<popupWindowObjects.length; i++) {
        if (popupWindowObjects[i] != null) {
            var p = popupWindowObjects[i];
            p.hideIfNotClicked(e);
        }
    }
}
function PopupWindow_attachListener() {
    if (document.layers) {
        document.captureEvents(Event.MOUSEUP);
    }
    window.popupWindowOldEventListener = document.onmouseup;
    if (window.popupWindowOldEventListener != null) {
        document.onmouseup = new Function("window.popupWindowOldEventListener(); PopupWindow_hidePopupWindows();");
    }
    else {
        document.onmouseup = PopupWindow_hidePopupWindows;
    }
}
function PopupWindow() {
    if (!window.popupWindowIndex) {
        window.popupWindowIndex = 0;
    }
    if (!window.popupWindowObjects) {
        window.popupWindowObjects = new Array();
    }
    if (!window.listenerAttached) {
        window.listenerAttached = true;
        PopupWindow_attachListener();
    }
    this.index = popupWindowIndex++;
    popupWindowObjects[this.index] = this;
    this.divName = null;
    this.popupWindow = null;
    this.width=0;
    this.height=0;
    this.populated = false;
    this.visible = false;
    this.autoHideEnabled = false;

    this.contents = "";
    this.url="";
    this.windowProperties="toolbar=no,location=no,status=no,menubar=no,scrollbars=auto,resizable,alwaysRaised,dependent,titlebar=no";
    if (arguments.length>0) {
        this.type="DIV";
        this.divName = arguments[0];
    }
    else {
        this.type="WINDOW";
    }
    this.use_gebi = false;
    this.use_css = false;
    this.use_layers = false;
    if (document.getElementById) {
        this.use_gebi = true;
    }
    else if (document.all) {
        this.use_css = true;
    }
    else if (document.layers) {
        this.use_layers = true;
    }
    else {
        this.type = "WINDOW";
    }
    this.offsetX = 0;
    this.offsetY = 0;
    this.getXYPosition = PopupWindow_getXYPosition;
    this.populate = PopupWindow_populate;
    this.setUrl = PopupWindow_setUrl;
    this.setWindowProperties = PopupWindow_setWindowProperties;
    this.refresh = PopupWindow_refresh;
    this.showPopup = PopupWindow_showPopup;
    this.hidePopup = PopupWindow_hidePopup;
    this.setSize = PopupWindow_setSize;
    this.isClicked = PopupWindow_isClicked;
    this.autoHide = PopupWindow_autoHide;
    this.hideIfNotClicked = PopupWindow_hideIfNotClicked;
}
