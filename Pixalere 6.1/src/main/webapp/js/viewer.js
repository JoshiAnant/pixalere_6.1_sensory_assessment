function show(div,nest){
    obj=bw.dom?document.getElementById(div).style:bw.ie4?document.all[div].style:bw.ns4?nest?document[nest].document[div]:document[div]:0;
    obj.visibility='visible';
}
function ShowLarge(sUrl,width,height,id){
  large_image_win = window.open(sUrl, 'large_image_win'+id, 'width='+width+',height='+height+',scrollbars=yes,resizable=yes,status=no,location=no,menubar=no');
}
function ShowLarge2(sUrl,width,height,id){
    large_image_win2 = window.open(sUrl, 'large_image_win2'+id, 'width='+width+',height='+height+',scrollbars=yes,resizable=yes,status=no,location=no,menubar=no');
}
function refreshComments(sURL){
    location.href=sURL;
    location.reload();
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
    window.open('../popups/'+theURL,winName,features);
}
function MM_openBrWindowWound(theURL,winName,features) { //v2.0
    window.open(''+theURL,winName,features);
}
function Is() {
    agent  = navigator.userAgent.toLowerCase();
    this.major = parseInt(navigator.appVersion);
    this.minor = parseFloat(navigator.appVersion);
    this.ns = ((agent.indexOf('mozilla') != -1) &&
        (agent.indexOf('spoofer') == -1) && (agent.indexOf('compatible') == -1) &&
        (agent.indexOf('opera') == -1) &&
        (agent.indexOf('webtv') == -1));
   this.ns2   = (this.ns && (this.major      ==    3));
    this.ns3   = (this.ns && (this.major      ==    3));
    this.ns4   = (this.ns && (this.major      ==    4));
    this.ns6   = (this.ns && (this.major      >=    5));
    this.ie    = (agent.indexOf("msie")       !=   -1);
    this.ie3   = (this.ie && (this.major      < 4));
    this.ie4   = (this.ie && (this.major      ==    4) && (agent.indexOf("msie 5.0")   ==   -1));
    this.ie5   = (this.ie && (this.major      ==    4) && (agent.indexOf("msie 5.0")   !=   -1));
    this.ieX = (this.ie && !this.ie3 && !this.ie4);
}
var is = new Is();
function readytoprocess(){
    var selectedproducts = "";
    m2 = document.assessmentform.assessments;
    for (i=0; i<m2.length; i++)   {
            if (i > 0) {
                selectedproducts += "||";
            }
            selectedproducts += m2.options[i].value;
            m2.options[i].selected = true;
        }
   document.form.assess_string.value=selectedproducts;
}

function checkBrowser(){
    this.ver=navigator.appVersion;
    this.dom=document.getElementById?1:0;
    this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0;
    this.ie4=(document.all && !this.dom)?1:0;
    this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0;
    this.ns4=(document.layers && !this.dom)?1:0;
    this.bw=(this.ie5 || this.ie4 || this.ns4 || this.ns5);
    return this;
}
bw=new checkBrowser();
function get_image_pos(imgname){
   var img = document.images[imgname];
    if(!img) return {
        x:0,
        y:0
    };
   var iX = (document.layers) ? img.x : img.offsetLeft;
    var iY = (document.layers) ? img.y : img.offsetTop;
    if(document.all || parseInt(navigator.appVersion)>=5){
     var elm = img.offsetParent;
        while(elm && elm!=null)       {
           iX += elm.offsetLeft;
           iY += elm.offsetTop;
           elm = elm.offsetParent;
       }
    }
    return {
        x:iX,
        y:iY
    };

}
function showComp(div,text){
    var nest="";
    obj=bw.dom?document.getElementById(div).style:bw.ie4?document.all[div].style:bw.ns4?nest?document[nest].document[div]:document[div]:0;
    var source;
    if(text=="Discharged"){
    obj.visibility='visible';
     }
    else{
    obj.visibility='hidden';
    }
}
function selectAssessmentForViewer(formname,formvalue,objSelectedThumb,box){

    formvalue.value=box.options[box.selectedIndex].value;
    objSelectedThumb.value=1;
    verify();
}
function browseAssessment(addindex,formname,formvalue,objSelectedThumb,box){
	formvalue.value=box.options[box.selectedIndex+addindex].value;
    objSelectedThumb.value=1;
    verify();
}
