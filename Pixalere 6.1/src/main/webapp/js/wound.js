var IE = document.all ? true : false;
function touchScreen(event, config) {
    var positions = getPos('blueman_grid');

    var left = event.offsetX ? (event.offsetX) : event.pageX - positions.x;
    var top = event.offsetY ? (event.offsetY) : event.pageY - positions.y;
    //alert(left+' '+top);
    if (document.getElementById("alpha_move").value != "") {

        var cloned = $("#" + $("#alpha_move").val()).clone().appendTo($("#blueman_grid"));
        cloned.removeClass("touchScreen");
        placeAlpha($("#alpha_move").val(), top, left, cloned, config);

    }

}


function placeAlpha(wound_type, top, left, cloned, config, text, name) {

    var positions = getPos('blueman_grid');
    if (wound_type == "dragA") {
        if (woundAlphas.length == 0) {
            document.getElementById("alert-message").innerHTML = text;
            $("#alert-message").dialog("open");
            cloned.remove();
        } else {
            var newAlpha = woundAlphas[0];//grab the first alpha available.
            cloned.offset({'top': positions.y + top, 'left': positions.x + left});

            cloned.addClass('Atrash');
            cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move', revert: "invalid"});
            cloned.html(newAlpha + '<input type="hidden" name="' + newAlpha + '_coords"  id="' + newAlpha + '_coords" value="' + newAlpha + '|' + left + '|' + (top - 10) + '|-1"/>');
            removeByElement(woundAlphas, newAlpha);//remove alpha from array so it can't be used later.
            addAlphaToEtiology(name, newAlpha);
            addAlphaToGoals(name, newAlpha);
            addAlphaToObtained(name, newAlpha);
            if (document.getElementById("showForWound")) {
                document.getElementById("showForWound").style.display = 'block';
            }
        }
    } else if (wound_type == "dragSKIN") {
        if (skinAlphas.length == 0) {
            document.getElementById("alert-message").innerHTML = text;
            $("#alert-message").dialog("open");
            cloned.remove();

        } else {
            var newAlpha = skinAlphas[0];//grab the first alpha available.
            cloned.offset({'top': positions.y + top, 'left': positions.x + left});

            cloned.addClass('skintrash');
            cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
            cloned.html('<img src="images/Alphas/' + newAlpha + '_red.gif"/><input type="hidden" name="' + newAlpha + '_coords" id="' + newAlpha + '_coords"  value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
            removeByElement(skinAlphas, newAlpha);//remove alpha from array so it can't be used later.
            newAlpha = newAlpha.substring(1, newAlpha.length);
            addAlphaToEtiology(name, newAlpha);
            addAlphaToGoals(name, newAlpha);
            addAlphaToObtained(name, newAlpha);
        }
    } else if (wound_type == "dragTD") {
        if (drainAlphas.length == 0) {
            document.getElementById("alert-message").innerHTML = text;
            $("#alert-message").dialog("open");
            cloned.remove();

        } else {
            var newAlpha = drainAlphas[0];//grab the first alpha available.
            cloned.offset({'top': positions.y + top, 'left': positions.x + left});

            cloned.addClass('tdtrash');
            cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
            cloned.html('<img src="images/Alphas/' + newAlpha + '_red.gif"/><input type="hidden" name="' + newAlpha + '_coords" id="' + newAlpha + '_coords"  value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
            removeByElement(drainAlphas, newAlpha);//remove alpha from array so it can't be used later.
            newAlpha = newAlpha.substring(2, newAlpha.length);
            addAlphaToEtiology(name, newAlpha);
            addAlphaToGoals(name, newAlpha);
            addAlphaToObtained(name, newAlpha);
            document.getElementById("showForOstomyPostOp").style.display = 'block';
        }
    } else if (wound_type == "dragF" || wound_type == "dragU") {

        if (ostAlphas.length == 0 || cloned.html().indexOf('ostf') != -1 && ostAlphas[0].indexOf('ostf') == -1) {
            cloned.remove();
            document.getElementById("alert-message").innerHTML = text;
            $("#alert-message").dialog("open");
        }
        else if (ostAlphas.length == 0 || cloned.html().indexOf('ostu') != -1 && ostAlphas[ostAlphas.length - 1] == -1) {
            cloned.remove();
            document.getElementById("alert-message").innerHTML = text;
            $("#alert-message").dialog("open");
        }
        else {
            var newAlpha = (cloned.html().indexOf('ostf') != -1 ? ostAlphas[0] : ostAlphas[ostAlphas.length - 1]);//grab the first alpha available.

            cloned.offset({'top': positions.y + top, 'left': positions.x + left});


            cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
            cloned.html('<img src="images/Alphas/' + newAlpha + '_red.gif"/><input type="hidden" name="' + newAlpha + '_coords"  id="' + newAlpha + '_coords" value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
            removeByElement(ostAlphas, newAlpha);//remove alpha from array so it can't be used later.
            if (cloned.html().indexOf('ostf') != -1) {
                cloned.addClass('ostftrash');
                addAlphaToEtiology(name, '');
                addAlphaToGoals(name, '');
                addAlphaToObtained(name, '');
            } else if (cloned.html().indexOf('ostu') != -1) {
                cloned.addClass('ostutrash');
                addAlphaToEtiology(name, '');
                addAlphaToGoals(name, '');
                addAlphaToObtained(name, '');
            }
            document.getElementById("showForOstomyPostOp").style.display = 'block';
            document.getElementById("showForOstomy").style.display = 'block';
        }
    } else if (wound_type == "dragTAG") {
        if (tagAlphas.length == 0) {
            document.getElementById("alert-message").innerHTML = text;
            $("#alert-message").dialog("open");
            cloned.remove();
        } else {
            var newAlpha = tagAlphas[0];//grab the first alpha available.
            cloned.offset({'top': positions.y + top, 'left': positions.x + left});

            cloned.addClass('tagtrash');//duplicate with below?
            cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
            cloned.html(newAlpha.substring(3, newAlpha.length) + '<input type="hidden" name="' + newAlpha + '_coords"  id="' + newAlpha + '_coords" value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
            removeByElement(tagAlphas, newAlpha);//remove alpha from array so it can't be used later.
            addAlphaToEtiology(name, newAlpha.substring(3, newAlpha.length));
            addAlphaToGoals(name, newAlpha.substring(3, newAlpha.length));
            addAlphaToObtained(name, newAlpha.substring(3, newAlpha.length));
            document.getElementById("showForOstomyPostOp").style.display = 'block';
            if (tagAlphas.length != 9) {
                $("div.incsdraggable").show();
                $("div.incsdraggable").prop('disabled', false);
            } else {
                $("div.incsdraggable").hide();
            }
        }
    } else if (wound_type == "dragBURNO") {
        var index1 = cloned.html().indexOf('src="images/Alphas/');
        var index2 = cloned.html().indexOf('.gif');
        var newAlpha = cloned.html().substring(index1 + 19, index2);

        cloned.offset({'top': positions.y + top, 'left': positions.x + left});

        cloned.addClass('burntrash');
        cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
        da = new Date();
        ep = da.getTime();
        cloned.html('<img src="images/Alphas/' + newAlpha + '.gif"  class="opacity30"/><input type="hidden" name="burn_coords"  id="' + newAlpha + '_coords" value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
        if (checkBurnFromAlpha() == false) {
            addAlphaToEtiology("Burn", "");
            addAlphaToGoals("Burn", "");
            addAlphaToObtained("Burn", "");
        }
        document.getElementById("showForBurn").style.display = 'block';
    } else if (wound_type == "dragBURNR") {
        var index1 = cloned.html().indexOf('src="images/Alphas/');
        var index2 = cloned.html().indexOf('.gif');
        var newAlpha = cloned.html().substring(index1 + 19, index2);

        cloned.offset({'top': positions.y + top, 'left': positions.x + left});

        cloned.addClass('burntrash');
        cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
        da = new Date();
        ep = da.getTime();
        cloned.html('<img src="images/Alphas/' + newAlpha + '.gif"  class="opacity30"/><input type="hidden" name="burn_coords"  id="' + newAlpha + '_coords" value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
        if (checkBurnFromAlpha() == false) {
            addAlphaToEtiology("Burn", "");
            addAlphaToGoals("Burn", "");
            addAlphaToObtained("Burn", "");
        }
        document.getElementById("showForBurn").style.display = 'block';

    } else if (wound_type == "dragBURNY") {
        var index1 = cloned.html().indexOf('src="images/Alphas/');
        var index2 = cloned.html().indexOf('.gif');
        var newAlpha = cloned.html().substring(index1 + 19, index2);

        cloned.offset({'top': positions.y + top, 'left': positions.x + left});

        cloned.addClass('burntrash');
        cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
        da = new Date();
        ep = da.getTime();
        cloned.html('<img src="images/Alphas/' + newAlpha + '.gif"  class="opacity30"/><input type="hidden" name="burn_coords"  id="' + newAlpha + '_coords" value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
        if (checkBurnFromAlpha() == false) {
            addAlphaToEtiology("Burn", "");
            addAlphaToGoals("Burn", "");
            addAlphaToObtained("Burn", "");
        }
        document.getElementById("showForBurn").style.display = 'block';


    } else if (wound_type.length == 8) {

        var index1 = cloned.html().indexOf('src="images/Alphas/');
        var index2 = cloned.html().indexOf('.gif');
        var newAlpha = cloned.html().substring(index1 + 19, index2);
        cloned.offset({'top': positions.y + top, 'left': positions.x + left});

        cloned.addClass('incstrash');
        cloned.draggable({scroll: false, collide: 'block', opacity: 0.35, cursorAt: {left: 5}, cursor: 'move'});
        da = new Date();
        ep = da.getTime();
        cloned.html('<img src="images/Alphas/' + newAlpha + '.gif"/><input type="hidden" name="incs_coords"  id="' + newAlpha + ep + '_coords" value="' + newAlpha + '|' + left + '|' + top + '|-1"/>');
    }
}
function checkWound(obj) {
    var themessage = "You are required to complete the following fields: ";
    var fail = 0;
    if (document.form.location_region.value == "") {
        themessage = themessage + " - Wound Location";
        fail = 1;
    }
    if (fail == 1) {
        alert(themessage);
    }
}

function assignEpsValue() {
    document.form.program_stay.value = document.form.eps.options[document.form.eps.selectedIndex].value;
}


function verify() {
    if (fail == 0) {
        if (((document.form.wound_id_p.value != "" && document.form.wound_id_p.value != 0 && document.form.wound_id_p.value != "[wound_id_p]" && document.form.which.value != 1) && (document.form.wound_id_p.value != document.form.wound_id.value)) && document.form.page.value == "woundprofile") {
            var themessage = "You have selected the option to change to a different wound profile. Selecting OK will cause you to lose any data changes that you have made during this session. Select Cancel if you would like to upload your changes before switching wound profiles.";
            showError(themessage, "confirm", "woundform");

        }
    }
}


var done = true;


function showComp(div, text) {
    var nest = "";
    obj = document.getElementById(div).style;
    var source;
    if (text == "Discharged") {
        obj.visibility = 'visible';
    }
    else {
        obj.visibility = 'hidden';
    }
}

//Shows the div
function show(div, nest) {
    obj = document.getElementById(div).style;
    obj.visibility = 'visible';
}

//Removes alpha from wound profile.
function removeFromList(oldValue) {

    alpha.unshift(oldValue);
    var selected;
    var inc = -1;
    var assess_string = document.form.assess_string.value.split("||");

    for (var x = 0; x < assess_string.length; x++) {
        if (x == 0) {
            if (assess_string.length == 1) {
                document.form.assess_string.value = "";
            }
            else {
                document.form.assess_string.value = assess_string[x];
            }
        }

        if (x > 0 && x != assess_string.length - 1) {
            document.form.assess_string.value = document.form.assess_string.value + "||" + assess_string[x];
        }
    }

    document.getElementById(oldValue).value = 0;
    document.getElementById(oldValue).style.visibility = 'hidden';
    if (tagAlphas.length != 9) {
        $("div.incsdraggable").show();
        $("div.incsdraggable").prop('disabled', false);
    } else {
        $("div.incsdraggable").hide();
    }
}
function showProduct(prodName){

    if(document.prevForm.quantity.value=="" || document.prevForm.current_products.selectedIndex==-1){
        document.prevForm.quantity.value="1";       
    }
        for (i = 0; i < Product.length; i++) {
                        if (Product[i][0] == prodName) {
                            var description=Product[i][2];
                            var url=Product[i][3];
                            var image=Product[i][1];
                            var title=Product[i][0];
                            document.getElementById('type').innerText=Product[i][5]+" "+Product[i][4];
                            if(url.indexOf("pdf")!=-1){
                                document.prevForm.url.value=Product[i][3];
                                document.getElementById("product_info_img").src="images/pdf.png";
                            }else if(url!=''){
                                document.prevForm.url.value=Product[i][3];
                                document.getElementById("product_info_img").src="images/url.png";
                            }
                        }
                }
        

        

        //document.getElementById('description').innerText=description;
        //document.getElementById('title').innerText=title;
        //document['image'].src="images/logos/"+image;
        //document.getElementById('url').href="Javascript:MM_openBrWindowLink('"+url+"','"+title+"','resizable=yes,scrollbars=yes,width=680,height=500')";
}
function selectAll() {
    
    for (i = 0; i < document.form.etiology.length; i++) {
        document.form.etiology.options[i].selected = true;
    }
    for (i = 0; i < document.form.etiology_alphas.length; i++) {
        document.form.etiology_alphas.options[i].selected = true;
    }
    for (i = 0; i < document.form.goals.length; i++) {
        document.form.goals.options[i].selected = true;
    }
    if (document.form.wound_acquired) {
        for (i = 0; i < document.form.wound_acquired.length; i++) {
            document.form.wound_acquired.options[i].selected = true;
        }
    }
}

function setLocationRadios(objRadios, objDropdown, arrValues, blnDisabled, strShowFull, strShowLeftRight, strShowMidline, strShowLeftRight) {
// When user selects a location in the dropdown with bluemodel images, the radios for Left/Right might need to be enabled

    if (objDropdown.selectedIndex > 0)
    {
        document.form.location.value = arrValues[objDropdown.options[objDropdown.selectedIndex].value][1];
        document.form.location_detailed.value = arrValues[objDropdown.options[objDropdown.selectedIndex].value][2];
        document.form.blockedboxes.value = arrValues[objDropdown.options[objDropdown.selectedIndex].value][10];
        document.form.slices.value = arrValues[objDropdown.options[objDropdown.selectedIndex].value][11];

        var blnFull = arrValues[objDropdown.options[objDropdown.selectedIndex].value][6] != "";

        var blnLeft = arrValues[objDropdown.options[objDropdown.selectedIndex].value][7] != "";
        var blnMidline = arrValues[objDropdown.options[objDropdown.selectedIndex].value][8] != "";
        var blnRight = arrValues[objDropdown.options[objDropdown.selectedIndex].value][9] != "";

        if (strShowFull == "1") {
            objRadios[$intRadioFull].disabled = (!blnFull || blnDisabled == true);
            objRadios[$intRadioFull].checked = (blnFull && !blnLeft && !blnMidline && !blnRight);
        }
        if (strShowLeftRight == "1") {
            objRadios[$intRadioLeft].disabled = (!blnLeft || blnDisabled == true);
            objRadios[$intRadioLeft].checked = false;
        }
        if (strShowMidline == "1") {
            objRadios[$intRadioMidline].disabled = (!blnMidline || blnDisabled == true);
            objRadios[$intRadioMidline].checked = (!blnFull && !blnLeft && blnMidline && !blnRight);
        }
        if (strShowLeftRight == "1") {
            objRadios[$intRadioRight].disabled = (!blnRight || blnDisabled == true);
            objRadios[$intRadioRight].checked = false;
        }

        if (blnFull && !blnLeft && !blnMidline && !blnRight)
        {

            setBluemodel(objDropdown, '', 'F', 0, arrValues, false);
        }
        if (!blnFull && !blnLeft && blnMidline && !blnRight)
        {
            setBluemodel(objDropdown, '', 'M', 2, arrValues, false);
        }
    }
    else
    {
        if (strShowFull == "1") {
            objRadios[$intRadioFull].disabled = true;
            objRadios[$intRadioFull].checked = false;
        }
        if (strShowLeftRight == "1") {
            objRadios[$intRadioLeft].disabled = true;
            objRadios[$intRadioLeft].checked = false;
        }
        if (strShowMidline == "1") {
            objRadios[$intRadioMidline].disabled = true;
            objRadios[$intRadioMidline].checked = false;
        }
        if (strShowLeftRight == "1") {
            objRadios[$intRadioRight].disabled = true;
            objRadios[$intRadioRight].checked = false;
        }
    }
}

function enableWoundLocationDropdowns(showAnterior, showLateral, showOther, showPosterior, woundActive) {

    if (document.getElementById('lock').src.indexOf("images/lock_icon.png") != -1) {
        document.getElementById('lock').src = "images/open_lock_icon.png";
        fillAllLocationsDropdowns(woundActive);
        if (showAnterior == "1") {
            document.form.anterior.disabled = false;
            setLocationRadios(document.form.rad_anterior, document.form.anterior, arr_anterior, true);
        }
        if (showPosterior == "1") {
            document.form.posterior.disabled = false;
        }
        if (showLateral == "1") {
            document.form.lateral.disabled = false;
        }
        if (showOther == "1") {
            document.form.other.disabled = false;
        }
    } else {
        document.getElementById('lock').src = "images/lock_icon.png"
        if (showAnterior == "1") {
            document.form.anterior.disabled = true;
        }
        if (showPosterior == "1") {
            document.form.posterior.disabled = true;
        }
        if (showLateral == "1") {
            document.form.lateral.disabled = true;
        }
        if (showOther == "1") {
            document.form.other.disabled = true;
        }
    }
}
function resetDropdowns(strActiveDropdown, intItemsCount, showAnterior, showPosterior, showLateral, showOther) {
    document.getElementById("blueman").background = "images/bluemodel/empty_bluemodel.jpg";
    document.form.bluemodel_image.value = "";
    document.form.bluemodel_image_id.value = 0;
    document.form.location.value = "";
    document.form.location_detailed.value = "";
    document.form.view.value = "";
    document.form.position.value = "";
    showBluemodelTitle('');



    if (showAnterior == "1") {
        if (strActiveDropdown != "anterior")
        {
            if (document.form.anterior.options.length == 2 && intItemsCount == 1)
            {
                document.form.anterior.selectedIndex = 1;
                document.form.anterior.disabled = true;
                setLocationRadios(document.form.rad_anterior, document.form.anterior, arr_anterior, true);
            }
            else
            {
                document.form.anterior.selectedIndex = 0;
                setLocationRadios(document.form.rad_anterior, document.form.anterior, '', false);
            }
        }
        else
        {
            document.form.view.value = "A";
        }
    }
    if (showPosterior == "1") {
        if (strActiveDropdown != "posterior")
        {
            if (document.form.posterior.options.length == 2 && intItemsCount == 1)
            {
                document.form.posterior.selectedIndex = 1;
                document.form.posterior.disabled = true;
                setLocationRadios(document.form.rad_posterior, document.form.posterior, arr_posterior, true);
            }
            else
            {
                document.form.posterior.selectedIndex = 0;
                setLocationRadios(document.form.rad_posterior, document.form.posterior, '', false);
            }
        }
        else
        {
            document.form.view.value = "P";
        }
    }
    if (showLateral == "1") {
        if (strActiveDropdown != "lateral")
        {
            if (document.form.lateral.options.length == 2 && intItemsCount == 1)
            {
                document.form.lateral.selectedIndex = 0;
                document.form.lateral.disabled = true;
                setLocationRadios(document.form.rad_lateral, document.form.lateral, arr_lateral, true);
            }
            else
            {
                document.form.lateral.selectedIndex = 0;
                setLocationRadios(document.form.rad_lateral, document.form.lateral, '', false);
            }
        }
        else
        {
            document.form.view.value = "L";
        }
    }
    if (showOther == "1") {
        if (strActiveDropdown != "other")
        {
            if (document.form.other.options.length == 2 && intItemsCount == 1)
            {
                document.form.other.selectedIndex = 1;
                document.form.other.disabled = true;
                setLocationRadios(document.form.rad_other, document.form.other, arr_other, true);
            }
            else
            {
                document.form.other.selectedIndex = 0;
                setLocationRadios(document.form.rad_other, document.form.other, '', false);
            }
        }
        else
        {
            document.form.view.value = "O";
        }
    }

}

function fillLocationsDropDown(objDropdown, arrValues, strSelected, woundActive) {
    for (var i = (objDropdown.length - 1); i >= 0; i--) {
        objDropdown.options[i] = null;
    }

    objDropdown.options[0] = new Option("", "");

    var intItems = 1;
    for (var i = 0; i < arrValues.length; i++) {
        if ((arrValues[i][13] != "")
                || (arrValues[i][14] != "")
                || (arrValues[i][15] != "")
                || (arrValues[i][16] != "")
                || (arrValues[i][17] != ""))
        {
            objDropdown.options[intItems] = new Option(arrValues[i][1], i);
            if (arrValues[i][1] == strSelected)
            {
                objDropdown.options[intItems].selected = true;
            }
            intItems++;
        }
    }

    objDropdown.disabled = (intItems == 1 || woundActive == "1");
    return intItems - 1;
}

function fillAllLocationsDropdowns(woundActive, showAnterior, showPosterior, showLateral, showOther) {

    var intItemsCount = 0

    if (showAnterior == "1") {
        intItemsCount = intItemsCount + fillLocationsDropDown(document.form.anterior, arr_anterior, "", woundActive);
    }
    if (showPosterior == "1") {
        intItemsCount = intItemsCount + fillLocationsDropDown(document.form.posterior, arr_posterior, "", woundActive);
    }
    if (showLateral == "1") {
        intItemsCount = intItemsCount + fillLocationsDropDown(document.form.lateral, arr_lateral, "", woundActive);
    }
    if (showOther == "1") {
        intItemsCount = intItemsCount + fillLocationsDropDown(document.form.other, arr_other, "", woundActive);
    }

    return intItemsCount;
}
function addProduct() {
    var products = document.prevForm.products;
    var choosen = document.prevForm.current_products;
    var prodlen = products.length;
    var cholen = choosen.length;
    var quant = document.prevForm.quantity.value;
    var max = document.prevForm.maxquant.value;
    var amount = quant;


    for (i = 0; i < prodlen; i++) {
        
        if (products.options[i].selected == true) {
            cholen = choosen.length;
            choosen.options[cholen] = new Option(amount + ':' + products.options[i].text, amount + '|' + products.options[i].value);
        }
    }

    document.prevForm.current_products.selectedIndex = -1;
    
}

function removeProduct() {
    document.prevForm.btn_update.disabled = true;
    document.prevForm.removeAlpha.disabled = true;

    var choose = document.prevForm.current_products;
    for (i = choose.length; i > -1; i--) {
        if (choose.options[i] != null && choose.options[i].selected == true) {
            choose.options[i] = null;
            choose.value[i] = null;
        }
    }
}
function updateProductValues() {
    if (document.prevForm.btn_update.disabled == false) {
//should we lock down current_products to prevent reselection?
//also need to think of way to unupdate.. ie what if they change their mind? SV: In that case, too bad...
        removeProduct();
        addProduct();
        document.prevForm.addAlpha.disabled = false;
        document.prevForm.removeAlpha.disabled = true;
        document.prevForm.btn_update.disabled = true;
    }
}

function filterProducts(objSelect, strFilter)
{
    set_child_listbox(document.prevForm.products, Product, '');
    if (strFilter.length > 0)
    {
        strFilter = strFilter.toUpperCase();
        var i = 0;
        while (i < objSelect.options.length)
        {
            if (objSelect.options[i].text.toUpperCase().indexOf(strFilter) == -1)
            {
                objSelect.options[i] = null;
            }
            else
            {
                i++;
            }
        }
    }
}
function verifyQuantity() {
    return floatOnly(document.prevForm.quantity);
}

function showQuantity(products){
    var choose=products;
	var cholen=choose.length;

	for(i=0;i<cholen;i++){
		if(choose!=null&&choose.options[i].selected==true){
			var product_splice = choose.options[i].value;
			var array = product_splice.split("|");
            var quantity=array[0];
            document.prevForm.change_quantity.value=quantity;
            }
	   }
}
var DomYes = document.getElementById ? 1 : 0;

function set_child_listbox(childObject, childArray, spanToHide) {

    //Clear child listbox
    for (var i = (childObject.length - 1); i >= 0; i--) {
        childObject.options[i] = null;
    }
    //childObject.options[0] = new Option("All Products","all");
    for (var i = (childObject.options.length - 1); i >= 0; i--) {
        childObject.options[i] = null;
    }

    childObject.disabled = false;
    var childIndex = 0;
    for (i = 0; i < childArray.length; i++) {
        childObject.options[childIndex] = new Option(childArray[i][0], childArray[i][7]);
        childIndex++;

    }

    //Select first option
    //childObject.selectedIndex = 0;

    //Hide dependent grid
    if (spanToHide != "") {
        if (DomYes) {
            document.getElementById(spanToHide).style.display = "none";
        } else {
            document.all[spanToHide].style.display = "none";
        }
    }
    document.prevForm.maxquant.value = "";
    document.prevForm.quantity.value = "";
} 