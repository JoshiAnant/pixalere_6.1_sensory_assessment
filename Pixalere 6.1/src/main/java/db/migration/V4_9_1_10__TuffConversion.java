package db.migration;

import com.pixalere.assessment.bean.AssessmentMucocSeperationsVO;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import com.pixalere.assessment.bean.AssessmentAntibioticVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentSinustractsVO;
import com.pixalere.assessment.bean.AssessmentUnderminingVO;
import com.pixalere.assessment.bean.AssessmentWoundBedVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.guibeans.LocationDepthVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.service.WoundServiceImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Difficult Database migrations.
 *
 * @since 5.1
 * @author travis
 */
public class V4_9_1_10__TuffConversion implements JdbcMigration {

    private String execution_log = "";

    public void migrate(Connection connection) throws Exception {
        DataSource ds = null;
        System.out.println("Migrate from Java");
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
        } catch (NamingException ne) {
            throw new RuntimeException("Unable to aquire data source", ne);

        }

        try {

            String[] locations = {"_live"};

            ProfessionalServiceImpl uBD = new ProfessionalServiceImpl();

            ListServiceImpl lservice = new ListServiceImpl(1);

            AssessmentServiceImpl aservice = new AssessmentServiceImpl(1);

            WoundServiceImpl wservice = new WoundServiceImpl(1);

            WoundAssessmentServiceImpl waservice = new WoundAssessmentServiceImpl();

            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();

            PatientProfileServiceImpl pservice = new PatientProfileServiceImpl(1);

            TreatmentCommentsServiceImpl tservice = new TreatmentCommentsServiceImpl();

            for (String location : locations) {

                WoundServiceImpl w = new WoundServiceImpl(1);

                Connection con;

                con = ds.getConnection();

                //mucocutaneous
                PreparedStatement state5 = con.prepareStatement("select id,alpha_id,mucocutaneous_margin_array from assessment_ostomy where mucocutaneous_margin_array!='' OR  mucocutaneous_margin_array!='a:0:{}' ");

                ResultSet result5 = state5.executeQuery();

                int cnt = 0;

                System.out.println("Mucocutaneous: " + PDate.getDateTime(new java.util.Date()));

                while (result5.next()) {

                    cnt++;

                    if (cnt % 1000 == 0) {

                        System.out.println(cnt + " @ " + PDate.getDateTime(new java.util.Date()));

                    }

                    int id = result5.getInt("id");

                    int alpha_id = result5.getInt("alpha_id");

                    String sinus = result5.getString("mucocutaneous_margin_array");

                    AssessmentOstomyVO a = new AssessmentOstomyVO();

                    setMucocutaneous_margin_array(sinus);

                    Vector sinuss = getMucocutaneousMargin();

                    for (int x = 0; x < sinuss.size(); x++) {

                        LocationDepthVO d = (LocationDepthVO) sinuss.get(x);

                        AssessmentMucocSeperationsVO s = new AssessmentMucocSeperationsVO();

                        s.setAlpha_id(alpha_id);

                        s.setAssessment_ostomy_id(id);

                        s.setDepth_cm(d.getDepth_cm());

                        s.setDepth_mm(d.getDepth_mm());

                        s.setLocation_start(d.getLocation_start());

                        s.setLocation_end(d.getLocation_end());

                        aservice.saveSeperation(s);

                    }

                }
                //drainage

                /*for (int x = 1; x < 6; x++) {

                 PreparedStatement state15 = con.prepareStatement("select id,alpha_id,additional_days_date" + x + ",drainage_amount" + x + "_start,drainage_amount" + x + "_end,drainage_amount_mls" + x + ",drainage_amount_hrs" + x + ",drain_characteristics" + x + " from assessment_drain where drainage_num>=" + x);

                 ResultSet result15 = state15.executeQuery();

                 cnt = 0;

                 System.out.println("Drainage Page: " + PDate.getDateTime(new java.util.Date()));

                 while (result15.next()) {

                 cnt++;

                 if (cnt % 1000 == 0) {

                 System.out.println(cnt + " @ " + PDate.getDateTime(new java.util.Date()));

                 }

                 int id = result15.getInt("id");

                 int alpha_id = result15.getInt("alpha_id");

                 String date = result15.getString("additional_days_date" + x);

                 int start = result15.getInt("drainage_amount" + x + "_start");

                 int end = result15.getInt("drainage_amount" + x + "_end");

                 int hrs = result15.getInt("drainage_amount_hrs" + x);

                 int mls = result15.getInt("drainage_amount_mls" + x);

                 String characteristics = result15.getString("drain_characteristics" + x);





                 AssessmentDrainageVO s = new AssessmentDrainageVO();

                 s.setAlpha_id(alpha_id);

                 s.setAssessment_drain_id(id);

                 Date d = null;

                 if (!date.equals("")) {

                 d = new Date(Long.parseLong(date));

                 }

                 s.setAdditional_days_date(d);

                 s.setDrainage_amount_end(end);

                 s.setDrainage_amount_start(start);

                 s.setDrainage_amount_hrs(hrs);

                 s.setDrainage_amount_mls(mls);

                 s.setCharacteristics(characteristics);



                 aservice.saveDrainage(s);





                 }*/
                //convert serialized fields from php format to pipes
                Statement stmt = con.createStatement();

                String sql = "update assessment_drain set drain_site='' where drain_site='a:0:{}' OR drain_site='a:1:{}'";

                stmt.executeUpdate(sql);

                //array increment
                System.out.println("Starting deserialization: " + PDate.getDateTime(new java.util.Date()));

                for (int x = 1; x < 12; x++) {

                    //string length
                    System.out.println("Increment: " + PDate.getDateTime(new java.util.Date()));

                    for (int y = 1; y <= 45; y++) {
                        //String sql_tmp = "update assessment_treatment set antibiotics=replace(antibiotics,'a:" + x + ":{i:0;s:" + y + ":\",'')";
                        //String sql_tmp2 = "update assessment_treatment set antibiotics=replace(antibiotics,'\";i:" + x + ";s:" + y + ":\",'|')";
                        //stmt.executeUpdate(sql_tmp);
                        //stmt.executeUpdate(sql_tmp2);
                        //parse, reinsert....
                        //sql_tmp = "update wound_profiles set goals=replace(goals,'a:" + x + ":{i:0;s:" + y + ":\",'')";
                        //sql_tmp2 = "update wound_profiles set goals=replace(goals,'\";i:" + x + ";s:" + y + ":\",'|')";
                        //stmt.executeUpdate(sql_tmp);
                        //stmt.executeUpdate(sql_tmp2);
                    }

                }

                LookupVO lis = new LookupVO();

                lis.setResourceId(lis.WOUND_BED_PERCENT);

                Collection<LookupVO> lists = null;

                lists = lservice.getListsWithInactive(lis);

               
                System.out.println("Assessment Getting: " + PDate.getDateTime(new java.util.Date()));

                PreparedStatement state = con.prepareStatement("select wound_assessment.patient_id as patient_id,professional_id,time_stamp,assessment_wound.wound_profile_type_id,assessment_wound.assessment_id,assessment_wound.id as id,wound_assessment.wound_id as wound_id,assessment_treatment.id as treatment_id ,antibiotics,assessment_wound.alpha_id,sinus_tract_array,undermining_array,wound_base,wound_base_percentage,wound_assessment.user_signature as user_signature from assessment_wound LEFT JOIN wound_assessment ON wound_assessment.id=assessment_wound.assessment_id left join assessment_treatment on assessment_wound.alpha_id=assessment_treatment.alpha_id where assessment_treatment.assessment_id=assessment_wound.assessment_id and wound_assessment.active=1 and assessment_wound.id<831165 order by id desc", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet result = state.executeQuery();
                result.setFetchSize(100);
                cnt = 0;
                System.out.println("Assessment Start: " + PDate.getDateTime(new java.util.Date()));
                sql = "insert into treatment_comments (patient_id,wound_id,active,professional_id,user_signature,body,time_stamp,assessment_id,wound_profile_type_id) values ";
                String woundbed_sql = "insert into assessment_woundbed (alpha_id,assessment_wound_id,wound_bed,percentage) values ";

                String under_sql = "insert into assessment_undermining (alpha_id,assessment_wound_id,depth_cm,depth_mm,location_start,location_end) values ";
                String sinus_sql = "insert into assessment_sinustracts (alpha_id,assessment_wound_id,depth_cm,depth_mm,location_start) values ";
                String sql_inner = "";
                String sql_under_inner = "";
                String sql_sinus_inner = "";
                String sql_woundbed_inner = "";
                int anti_cnt = 0;
                int sinus_cnt = 0;
                int under_cnt = 0;
                int woundbed_cnt = 0;
                while (result.next()) {

                    cnt++;
                    if (cnt % 1000 == 0) {

                        System.out.println(cnt + " @ " + PDate.getDateTime(new java.util.Date()));
                    }

                    int id = result.getInt("id");

                    int patient_id = result.getInt("patient_id");

                    int assessment_id = result.getInt("assessment_id");

                    int alpha_id = result.getInt("alpha_id");

                    int professional_id = result.getInt("professional_id");

                    String time_stamp = result.getString("time_stamp");

                    int wound_profile_type_id = result.getInt("wound_profile_type_id");
                    int treatment_id = result.getInt("treatment_id");

                    int wound_id = result.getInt("wound_id");

                    String sinus = result.getString("sinus_tract_array");

                    String under = result.getString("undermining_array");

                    /*String anti = result.getString("antibiotics");
                    String user_signature = result.getString("user_signature");
                    if (anti != null && !anti.equals("") && !anti.equals("a:0:{}")) {
                        anti_cnt++;
                        Vector<String> antibiotics = Serialize.unserializeLegacy(anti);
                        //String antibiotic = Serialize.commaIze(anti);
                        //a:1:{i:0;s:45:"Cipro and Keflex (?) : 23/5/2008 to 31/5/2008";}
                        for (String antibiotic : antibiotics) {

                            try {

                                //Find out if the wound has only 1 alpha, then assign antibiotic, otherwise it goes in treatment comments.
                                
                                PreparedStatement stateA = con.prepareStatement("select id from wound_assessment_location where wound_id="+wound_id+" and active=1", ResultSet.TYPE_SCROLL_INSENSITIVE,
                                        ResultSet.CONCUR_READ_ONLY);

                                ResultSet resultAnti = stateA.executeQuery();
                                resultAnti.setFetchSize(100);
                               //while (resultAnti.next()) {
                                int s = resultAnti.getFetchSize();
                                
                                if (s == 1) {

                                    int index = antibiotic.indexOf(" : ");

                                    String name = antibiotic.substring(0, index);

                                    try {

                                        String tmp = antibiotic.substring(index + 3, antibiotic.length());

                                        int index2 = tmp.indexOf(" to ");

                                        String date1 = tmp.substring(0, index2);

                                        String date2 = tmp.substring(index2 + 4, tmp.length());

                                        SimpleDateFormat sf = new SimpleDateFormat("d/MM/yyyy");

                                        java.util.Date start_date = sf.parse(date1);

                                        java.util.Date end_date = sf.parse(date2);

                                        if (start_date != null && !name.equals("")) {

                             //PreparedStatement states = con.prepareStatement("select id from assessment_treatment where assessment_id=" + assessment_id + " AND alpha_id=" + alpha_id + " order by id desc");
                                            //ResultSet results = states.executeQuery();
                                            //int treatment_id = -1;
                                            //while (result.next()) {
                                            //  treatment_id = results.getInt("id");
                                            // }
                                            AssessmentAntibioticVO a = new AssessmentAntibioticVO();

                                            a.setAssessment_treatment_id(treatment_id);

                                            a.setStart_date(start_date);

                                            if (end_date != null) {//may not have end_date

                                                a.setEnd_date(end_date);

                                            }

                                            a.setAntibiotic(name);

                                            aservice.saveAntibiotic(a);

                                        }

                                    } catch (StringIndexOutOfBoundsException e) {

                                        System.out.println(antibiotic + " " + e.getMessage());

                                        //e.printStackTrace();
                                    } catch (Exception e) {
                                        System.out.println(antibiotic + " " + e.getMessage());
                                        //e.printStackTrace();
                                    }

                                } else if (s > 1) {

                                    long time = Long.parseLong(time_stamp);
                                    sql_inner = sql_inner + "(" + patient_id + "," + wound_id + ",1," + professional_id + ",'" + user_signature + "','Antibiotic: " + antibiotic + "','" + time_stamp + "'," + assessment_id + "," + wound_profile_type_id + "),";
                                    if (anti_cnt % 1000 == 0) {

                                        if (sql_inner.length() > 0) {
                                            sql_inner = sql_inner.substring(0, sql_inner.length() - 1);
                                            //System.out.println("Antibiotics: " + sql + sql_inner);
                                            stmt.executeUpdate(sql + sql_inner);
                                        }
                                        sql_inner = "";
                                    }
                                    //tservice.saveTreatmentComment(tc);

                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                                if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                    con = ds.getConnection();
                                    stmt = con.createStatement();
                                }

                            }

                        }
                        // System.out.println("Saved Antibiotics: " + PDate.getDateTime(new java.util.Date()));
                    }*/

                    setSinus_tract_array(sinus);

                    Vector sinuss = getSinus();

                    for (int x = 0; x < sinuss.size(); x++) {
                        sinus_cnt++;

                        LocationDepthVO d = (LocationDepthVO) sinuss.get(x);
                        sql_sinus_inner = sql_sinus_inner + "(" + alpha_id + "," + id + "," + d.getDepth_cm() + "," + d.getDepth_mm() + "," + d.getLocation_start() + "),";
                        if (sinus_cnt % 1000 == 0) {

                            if (sql_sinus_inner.length() > 0) {
                                sql_sinus_inner = sql_sinus_inner.substring(0, sql_sinus_inner.length() - 1);
                                //System.out.println("Sinus: " + sinus_sql + sql_sinus_inner);
                                try {
                                    stmt.executeUpdate(sinus_sql + sql_sinus_inner);
                                } catch (Exception e) {

                                    e.printStackTrace();
                                    if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                        con = ds.getConnection();
                                        stmt = con.createStatement();
                                    }

                                }
                            }
                            sql_sinus_inner = "";
                        }

                    }

                    setUndermining_array(under);

                    Vector u = getUndermining();

                    for (int x = 0; x < u.size(); x++) {
                        under_cnt++;
                        LocationDepthVO d = (LocationDepthVO) u.get(x);

                        sql_under_inner = sql_under_inner + "(" + alpha_id + "," + id + "," + d.getDepth_cm() + "," + d.getDepth_mm() + "," + d.getLocation_start() + "," + d.getLocation_end() + "),";
                        if (under_cnt % 1000 == 0) {

                            if (sql_under_inner.length() > 0) {
                                sql_under_inner = sql_under_inner.substring(0, sql_under_inner.length() - 1);
                                //System.out.println("Under: " + under_sql + sql_under_inner);
                                try {
                                    stmt.executeUpdate(under_sql + sql_under_inner);
                                } catch (Exception e) {

                                    e.printStackTrace();
                                    if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                        con = ds.getConnection();
                                        stmt = con.createStatement();
                                    }

                                }
                            }
                            sql_under_inner = "";
                        }
                    }

                    String wb = result.getString("wound_base");

                    String woundbasep = result.getString("wound_base_percentage");

                    AssessmentEachwoundVO a = new AssessmentEachwoundVO();

                    a.setWound_base(wb);

                    a.setWound_base_percentage(woundbasep);

                    boolean found = false;
                    for (LookupVO l : lists) {

                        if (a.ifWoundBaseExists(wb, l.getName(1))) {
                            woundbed_cnt++;
                            int percentage = a.getPercentagesByItem(woundbasep, l.getName(1));
                            found = true;
                            sql_woundbed_inner = sql_woundbed_inner + "('" + alpha_id + "','" + id + "','" + l.getId() + "'," + percentage + "),";
//      
                            if (woundbed_cnt % 1000 == 0) {

                                if (sql_woundbed_inner.length() > 0) {
                                    sql_woundbed_inner = sql_woundbed_inner.substring(0, sql_woundbed_inner.length() - 1);
                                    //System.out.println("WoundBed: " + woundbed_sql + sql_woundbed_inner);
                                    try {
                                        stmt.executeUpdate(woundbed_sql + sql_woundbed_inner);
                                    } catch (Exception e) {

                                        e.printStackTrace();
                                        if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                            con = ds.getConnection();
                                            stmt = con.createStatement();
                                        }

                                    }
                                }
                                sql_woundbed_inner = "";
                            }

                        }
                    }

                    if (result.isLast()) {
                        if (woundbed_cnt > 0) {
                            sql_woundbed_inner = sql_woundbed_inner.substring(0, sql_woundbed_inner.length() - 1);
                            //System.out.println("WoundBed: " + woundbed_sql + sql_woundbed_inner);
                            try {
                                stmt.executeUpdate(woundbed_sql + sql_woundbed_inner);
                            } catch (Exception e) {

                                e.printStackTrace();
                                if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                    con = ds.getConnection();
                                    stmt = con.createStatement();
                                }

                            }
                        }
                        if (sinus_cnt > 0) {
                            sql_sinus_inner = sql_sinus_inner.substring(0, sql_sinus_inner.length() - 1);
                            //System.out.println("WoundBed: " + woundbed_sql + sql_woundbed_inner);
                            try {
                                stmt.executeUpdate(sinus_sql + sql_sinus_inner);
                            } catch (Exception e) {

                                e.printStackTrace();
                                if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                    con = ds.getConnection();
                                    stmt = con.createStatement();
                                }

                            }
                        }
                        if (under_cnt > 0) {
                            sql_under_inner = sql_under_inner.substring(0, sql_under_inner.length() - 1);
                            //System.out.println("WoundBed: " + woundbed_sql + sql_woundbed_inner);
                            try {
                                stmt.executeUpdate(under_sql + sql_under_inner);
                            } catch (Exception e) {

                                e.printStackTrace();
                                if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                    con = ds.getConnection();
                                    stmt = con.createStatement();
                                }

                            }
                        }
                        if (anti_cnt > 0 && sql_inner.length() != 0) {
                            sql_inner = sql_inner.substring(0, sql_inner.length() - 1);
                            System.out.println("Antibiotic: " + sql + sql_inner);
                            try {
                                stmt.executeUpdate(sql + sql_inner);
                            } catch (Exception e) {

                                e.printStackTrace();
                                if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                    con = ds.getConnection();
                                    stmt = con.createStatement();
                                }

                            }
                        }
                    }
                }
                con.close();
                stmt.close();
                result.close();
                state.close();
            }

        } catch (Exception e) {

            e.printStackTrace();
            throw new RuntimeException("Exception: ", e);

        }

    }

    /**
     * Executes each line of the upgrade script.
     *
     * @param query the sql query to execute
     * @return boolean if there's an error return false
     */
    public boolean executeQuery(String query) {

        DataSource ds = null;
        try {

            Context initContext = new InitialContext();

            Context envContext = (Context) initContext.lookup("java:/comp/env");

            ds = (DataSource) envContext.lookup("jdbc/pixDS");

        } catch (NamingException ne) {

            throw new RuntimeException("Unable to aquire data source", ne);

        }

        try {

            Connection con;

            con = ds.getConnection();

            Statement stmt = con.createStatement();

            stmt.executeUpdate(query);

            con.close();
            stmt.close();

            return true;

        } catch (Exception e) {

            e.printStackTrace();

            execution_log = e.getMessage();

            return false;

        }

    }
    /* legacy */
    private Integer mucocutaneous_margin_num;
    private String mucocutaneous_margin_array;
    /**
     *
     *
     *
     * nullable persistent field
     *
     *
     *
     */
    private Integer num_undermining;
    private String undermining_array;
    /**
     *
     *
     *
     * nullable persistent field
     *
     *
     *
     */
    private String sinus_tract_array;
    private static LocationDepthVO sinusTract = new LocationDepthVO();
    private static Vector sinus = new Vector();
    private static LocationDepthVO undermining = new LocationDepthVO();
    private static Vector underminingV = new Vector();
    /**
     *
     *
     *
     * nullable persistent field
     *
     *
     *
     */
    private Integer num_sinus_tracts;

    public Integer getNum_undermining() {

        return num_undermining;

    }

    public void setNum_undermining(Integer num_undermining) {

        this.num_undermining = num_undermining;

    }

    public String getUndermining_array() {

        return undermining_array;

    }

    public void setUndermining_array(String undermining_array) {

        this.undermining_array = undermining_array;

    }

    public String getSinus_tract_array() {

        return this.sinus_tract_array;

    }

    public void setSinus_tract_array(String sinus_tract_array) {

        this.sinus_tract_array = sinus_tract_array;

    }

    public Integer getNum_sinus_tracts() {

        return this.num_sinus_tracts;

    }

    public void setNum_sinus_tracts(Integer num_sinus_tracts) {

        this.num_sinus_tracts = num_sinus_tracts;

    }

    public Integer getMucocutaneous_margin_num() {

        return mucocutaneous_margin_num;

    }

    public void setMucocutaneous_margin_num(Integer mucocutaneous_margin_num) {

        this.mucocutaneous_margin_num = mucocutaneous_margin_num;

    }

    public String getMucocutaneous_margin_array() {

        return mucocutaneous_margin_array;

    }

    public void setMucocutaneous_margin_array(String mucocutaneous_margin_array) {

        this.mucocutaneous_margin_array = mucocutaneous_margin_array;

    }
    private static LocationDepthVO mm = new LocationDepthVO();

    private void populateSinus(int state, int row, String tmp) {

        if (tmp.equals("-1")) {

            tmp = "0";

        }

        if (row == 1) {

            if (state == 1) {

                sinusTract = new LocationDepthVO();

                sinusTract.setDepth_cm(new Integer(tmp));

            } else if (state == 2) {

                sinusTract.setDepth_mm(new Integer(tmp));

            } else if (state == 3) {

                sinusTract.setLocation_start(new Integer(tmp));

            } else if (state == 4) {

                sinusTract.setLocation_end(new Integer(tmp));

                sinus.add(sinusTract);

            }

        } else {

            if (state == 1) {

                sinusTract = new LocationDepthVO();

                sinusTract.setDepth_cm(new Integer(tmp));

            } else if (state == 2) {

                sinusTract.setDepth_mm(new Integer(tmp));

            } else if (state == 3) {

                sinusTract.setLocation_start(new Integer(tmp));

            } else if (state == 4) {

                sinusTract.setLocation_end(new Integer(tmp));

                sinus.add(sinusTract);

            }

        }

    }

    public Vector getSinus() {

        String sinusArray = getSinus_tract_array();

        sinus = new Vector();

        if (sinusArray != null) {

            int count = 0;

            int state = 0;

            int row = 0;

            do {

                if (sinusArray.indexOf(":\"") == -1) {

                    break;

                }

                int index = sinusArray.indexOf(":\"");

                sinusArray = sinusArray.substring(index + 2, sinusArray.length());

                int index2 = sinusArray.indexOf("\";");

                String prod = sinusArray.substring(0, index2);

                if (state >= 1 && state < 5) {

                    populateSinus(state, row, prod);

                    state = 0;

                }

                if (prod.equals("sinus_tract_depth")) {

                    row++;

                    state = 1;

                } else if (prod.equals("sinus_tract_depth_mini")) {

                    state = 2;

                } else if (prod.equals("sinus_tract_start")) {

                    state = 3;

                } else if (prod.equals("sinus_tract_end")) {

                    state = 4;

                } else {

                    count++;

                }

            } while (true);

        }

        return sinus;

    }

    private void populateUndermining(int state, int row, String tmp) {

        if (tmp.equals("-1")) {

            tmp = "0";

        }

        if (row == 1) {

            if (state == 1) {

                undermining = new LocationDepthVO();

                undermining.setDepth_cm(new Integer(tmp));

            } else if (state == 2) {

                undermining.setDepth_mm(new Integer(tmp));

            } else if (state == 3) {

                undermining.setLocation_start(new Integer(tmp));

            } else if (state == 4) {

                undermining.setLocation_end(new Integer(tmp));

                underminingV.add(undermining);

            }

        } else {

            if (state == 1) {

                undermining = new LocationDepthVO();

                undermining.setDepth_cm(new Integer(tmp));

            } else if (state == 2) {

                undermining.setDepth_mm(new Integer(tmp));

            } else if (state == 3) {

                undermining.setLocation_start(new Integer(tmp));

            } else if (state == 4) {

                undermining.setLocation_end(new Integer(tmp));

                underminingV.add(undermining);

            }

        }

    }

    public Vector getUndermining_depth() {

        return getUndermining();

    }

    public Vector getUndermining_location() {

        return getUndermining();

    }

    public Vector getSinus_depth() {

        return getSinus();

    }

    public Vector getSinus_location() {

        return getSinus();

    }

    public Vector getUndermining() {

        String underArray = getUndermining_array();

        underminingV = new Vector();

        if (underArray != null) {

            int count = 0;

            int state = 0;

            int row = 0;

            do {

                if (underArray.indexOf(":\"") == -1) {

                    break;

                }

                int index = underArray.indexOf(":\"");

                underArray = underArray.substring(index + 2, underArray.length());

                int index2 = underArray.indexOf("\";");

                String prod = underArray.substring(0, index2);

                if (state >= 1 && state < 5) {

                    populateUndermining(state, row, prod);

                    state = 0;

                }

                if (prod.equals("undermining_depth")) {

                    row++;

                    state = 1;

                } else if (prod.equals("undermining_depth_mini")) {

                    state = 2;

                } else if (prod.equals("undermining_start")) {

                    state = 3;

                } else if (prod.equals("undermining_end")) {

                    state = 4;

                } else {

                    count++;

                }

            } while (true);

        }

        return underminingV;

    }
    private static Vector mV = new Vector();

    private void populateMucocutaneousMargin(int state, int row, String tmp) {

        if (tmp.equals("-1")) {

            tmp = "0";

        }

        if (row == 1) {

            if (state == 1) {

                mm = new LocationDepthVO();

                mm.setDepth_cm(new Integer(tmp));

            } else if (state == 2) {

                mm.setDepth_mm(new Integer(tmp));

            } else if (state == 3) {

                mm.setLocation_start(new Integer(tmp));

            } else if (state == 4) {

                mm.setLocation_end(new Integer(tmp));

                mV.add(mm);

            }

        } else {

            if (state == 1) {

                mm = new LocationDepthVO();

                mm.setDepth_cm(new Integer(tmp));

            } else if (state == 2) {

                mm.setDepth_mm(new Integer(tmp));

            } else if (state == 3) {

                mm.setLocation_start(new Integer(tmp));

            } else if (state == 4) {

                mm.setLocation_end(new Integer(tmp));

                mV.add(mm);

            }

        }

    }

    public Vector getMucocutaneous_margin_depth() {

        return getMucocutaneousMargin();

    }

    public Vector getMucocutaneous_margin_location() {

        return getMucocutaneousMargin();

    }

    public Vector getMucocutaneousMargin() {

        String mArray = getMucocutaneous_margin_array();

        mV = new Vector();

        if (mArray != null) {

            int count = 0;

            int state = 0;

            int row = 0;

            do {

                if (mArray.indexOf(":\"") == -1) {

                    break;

                }

                int index = mArray.indexOf(":\"");

                mArray = mArray.substring(index + 2, mArray.length());

                int index2 = mArray.indexOf("\";");

                String prod = mArray.substring(0, index2);

                if (state >= 1 && state < 5) {

                    populateMucocutaneousMargin(state, row, prod);

                    state = 0;

                }

                if (prod.equals("mucocutaneous_margin_depth")) {

                    row++;

                    state = 1;

                } else if (prod.equals("mucocutaneous_margin_depth_mini")) {

                    state = 2;

                } else if (prod.equals("mucocutaneous_margin_start")) {

                    state = 3;

                } else if (prod.equals("mucocutaneous_margin_end")) {

                    state = 4;

                } else {

                    count++;

                }

            } while (true);

        }

        return mV;

    }

    /**
     * @deprecated @param vecData
     * @param alpha
     * @return
     */
    public String getAlphaValue(Vector<String> vecData, String alpha) {

        Vector vecResult = new Vector();

        for (String strDataOption : vecData) {

            int s = strDataOption.indexOf(alpha + ":");

            s = s + (alpha.length() + 1);

            if (strDataOption.indexOf(alpha + ":") != -1) {

                String t = strDataOption.substring(s, strDataOption.length());

                return t.trim();

            }

        }

        return "";

    }
}
