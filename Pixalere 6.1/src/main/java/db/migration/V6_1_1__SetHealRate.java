package db.migration;

import com.pixalere.assessment.bean.AssessmentMucocSeperationsVO;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import com.pixalere.assessment.bean.AssessmentAntibioticVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentSinustractsVO;
import com.pixalere.assessment.bean.AssessmentUnderminingVO;
import com.pixalere.assessment.bean.AssessmentWoundBedVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.guibeans.LocationDepthVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.service.WoundServiceImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Difficult Database migrations.
 *
 * @since 5.1
 * @author travis
 */
public class V6_1_1__SetHealRate implements JdbcMigration {

    private String execution_log = "";

    public void migrate(Connection connection) throws Exception {
        DataSource ds = null;
        System.out.println("Migrate from Java");
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
        } catch (NamingException ne) {
            throw new RuntimeException("Unable to aquire data source", ne);

        }

        String[] locations = {"_live"};

        ProfessionalServiceImpl uBD = new ProfessionalServiceImpl();

        ListServiceImpl lservice = new ListServiceImpl(1);

        AssessmentServiceImpl aservice = new AssessmentServiceImpl(1);

        WoundServiceImpl wservice = new WoundServiceImpl(1);

        WoundAssessmentServiceImpl waservice = new WoundAssessmentServiceImpl();

        WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();

        PatientProfileServiceImpl pservice = new PatientProfileServiceImpl(1);

        TreatmentCommentsServiceImpl tservice = new TreatmentCommentsServiceImpl();

        for (String location : locations) {

            WoundServiceImpl w = new WoundServiceImpl(1);

            Connection con;
            System.out.println("conn: ");
            con = ds.getConnection();
            System.out.println("conn: " + con);
            Statement stmt = con.createStatement();
            System.out.println("stmt: " + stmt);

            String mssql = "";
            String mysql = "";
            if (checkIsMySQL(con)) {
                mysql = " limit 1";
            } else {
                mssql = " top 1 ";
            }

            //Backdate assessments.
            System.out.println("MySql: " + checkIsMySQL(con));
            
                PreparedStatement stateback = con.prepareStatement("select id from wound_assessment_location", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet resultback = stateback.executeQuery();
                resultback.setFetchSize(100);
                while (resultback.next()) {
                    try {

                        int alpha_id = resultback.getInt("id");
                        double length=0.0;
                        double width=0.0;
                        double last_length=0.0;
                        double last_width=0.0;
                        PreparedStatement stateback3 = con.prepareStatement("select "+mssql+" length,width from assessment_wound where alpha_id="+alpha_id+" and full_assessment=1 order by visited_on asc "+mysql, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                ResultSet.CONCUR_READ_ONLY);

                        ResultSet resultback3 = stateback3.executeQuery();
                        resultback3.setFetchSize(100);
                        while (resultback3.next()) {
                            try {
                                length=resultback3.getDouble("length");
                                width=resultback3.getDouble("width");
                                
                            } catch (Exception e) {e.printStackTrace();
                            }
                        }
                        
                        PreparedStatement stateback34 = con.prepareStatement("select "+mssql+" length,width from assessment_wound where alpha_id="+alpha_id+" and full_assessment=1 order by visited_on desc "+mysql, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                ResultSet.CONCUR_READ_ONLY);

                        ResultSet resultback34 = stateback3.executeQuery();
                        resultback34.setFetchSize(100);
                        while (resultback34.next()) {
                            try {
                                last_length=resultback34.getDouble("length");
                                last_width=resultback34.getDouble("width");
                                
                            } catch (Exception e) {e.printStackTrace();
                            }
                        }
                        if(length>0 || width>0){
                            double first = length*width;
                            double second = last_length*last_width;
                            double result= ((first-second) * first) * 100;
                            String update2 = "update wound_assessment_location set current_heal_rate=" + result + " where id=" + alpha_id;
                            
                            stmt.executeUpdate(update2);
                        }
                        resultback34.close();
                        stateback34.close();
                        resultback3.close();
                        stateback3.close();
                        
                    } catch (Exception e) {e.printStackTrace();
                        
                    }
                }
                resultback.close();
                stateback.close();
                con.close();
        }
        
    }

    private boolean checkIsMySQL(Connection con) {

        //select query with top 1... if it fails.. its mysql.
        boolean mysql = false;
        try {
            PreparedStatement state = con.prepareStatement("select top 1 from configuration", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
            result.close();
            state.close();
        } catch (com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException e) {
            //e.printStackTrace();
            mysql = true;
        } catch (Exception e) {
            //e.printStackTrace();
            if (e.getMessage().indexOf("MySQL") != -1) {
                mysql = true;
            }
        }
        return mysql;
    }
}
