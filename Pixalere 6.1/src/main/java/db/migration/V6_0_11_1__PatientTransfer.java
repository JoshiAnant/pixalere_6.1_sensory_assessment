/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package db.migration;

import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import com.pixalere.auth.service.ProfessionalServiceImpl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Difficult Database migrations.
 *
 * @since 6.0
 * @author travis
 */
public class V6_0_11_1__PatientTransfer implements JdbcMigration {

    private String execution_log = "";

    public void migrate(Connection connection) throws Exception {
        DataSource ds = null;
        System.out.println("Migrate from Java");
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
        } catch (NamingException ne) {
            throw new RuntimeException("Unable to aquire data source", ne);

        }

        String[] locations = {"_live"};

        ProfessionalServiceImpl uBD = new ProfessionalServiceImpl();

        for (String location : locations) {

            Connection con;
            System.out.println("conn: ");
            con = ds.getConnection();
            System.out.println("conn: " + con);
            
            String mssql = "";
            String mysql = "";
            if (checkIsMySQL(con)) {
                mysql = " limit 1";
            } else {
                mssql = " top 1 ";
            }

            //Backdate assessments.
            System.out.println("MySql: " + checkIsMySQL(con));

            // Insert Query
            String insertTransferQuery = "insert into patient_transfers "
                                + "(patient_id, professional_id, transfer_on, source_location_id, destination_location_id) "
                                + "values "
                                + "(?,?,?,?,?)";
            PreparedStatement insert_stmt = con.prepareStatement(insertTransferQuery);

            // Get unique patients
            System.out.println("select patient_id from patient_accounts where patient_id IS NOT NULL order by id asc");
            PreparedStatement patients_stmt = con.prepareStatement(
                    "select patient_id "
                            + "from patient_accounts "
                            + "where patient_id IS NOT NULL "
                            + "order by id asc", 
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            
            // Avoid duplication of inserts
            Set<Integer> recordedAssIds = new HashSet<Integer>();

            ResultSet patients_rset = patients_stmt.executeQuery();
            patients_rset.setFetchSize(100);
            while (patients_rset.next()) {
                try {

                    int patient_id = patients_rset.getInt("patient_id");
                    int previous_treatment_location_id = 0;
                    int previous_professional_id = 0;
                    Timestamp previous_visited_on = null;

                    // Get assessments for this patient
                    PreparedStatement wounds_assessments_stmt
                            = con.prepareStatement(
                                    "select "
                                    + "id, "
                                    + "treatment_location_id, "
                                    + "professional_id, "
                                    + "visited_on "
                                    + "from wound_assessment "
                                    + "where patient_id =" + patient_id + " "
                                    + "and active=1 "
                                    + "and reset_visit=1"
                                    + "order by id asc",
                                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                                    ResultSet.CONCUR_READ_ONLY);

                    ResultSet wounds_assessments_rset = wounds_assessments_stmt.executeQuery();
                    wounds_assessments_rset.setFetchSize(100);

                    while (wounds_assessments_rset.next()) {
                        int assessment_id = wounds_assessments_rset.getInt("id");
                        int treatment_location_id = wounds_assessments_rset.getInt("treatment_location_id");
                        int professional_id = wounds_assessments_rset.getInt("professional_id");
                        Timestamp visited_on = wounds_assessments_rset.getTimestamp("visited_on");

                        previous_treatment_location_id = (previous_treatment_location_id == 0)
                                ? treatment_location_id
                                : previous_treatment_location_id;
                        previous_visited_on = (previous_visited_on == null)
                                ? visited_on
                                : previous_visited_on;

                        if (previous_treatment_location_id != treatment_location_id
                                && !recordedAssIds.contains(assessment_id)) {
                            System.out.println(" Transfer patient migration: " + patient_id + " location change: " + previous_treatment_location_id + " -> " + treatment_location_id);
                            recordedAssIds.add(assessment_id);

                            insert_stmt.setInt(1, patient_id);
                        // It's more likely that the previous professional 
                            // was the one that did the transfer
                            insert_stmt.setInt(2, previous_professional_id);
                            insert_stmt.setTimestamp(3, previous_visited_on);
                            insert_stmt.setInt(4, previous_treatment_location_id);
                            insert_stmt.setInt(5, treatment_location_id);

                            insert_stmt.executeUpdate();
                        }

                        // Save previous entry data
                        previous_treatment_location_id = treatment_location_id;
                        previous_professional_id = professional_id;
                        previous_visited_on = visited_on;
                    }

                    wounds_assessments_rset.close();
                    wounds_assessments_stmt.close();

                   
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
            
            }
            patients_rset.close();
            patients_stmt.close();
            insert_stmt.close();

            con.close();

        }
    }

    private boolean checkIsMySQL(Connection con) {

        //select query with top 1... if it fails.. its mysql.
        boolean mysql = false;
        try {
            PreparedStatement state = con.prepareStatement("select top 1 from configuration", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
        } catch (com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException e) {
            //e.printStackTrace();
            mysql = true;
        } catch (Exception e) {
            //e.printStackTrace();
            if (e.getMessage().indexOf("MySQL") != -1) {
                mysql = true;
            }
        }
        return mysql;
    }
}
