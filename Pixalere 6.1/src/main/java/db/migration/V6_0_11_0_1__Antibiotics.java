package db.migration;

import com.pixalere.assessment.bean.AssessmentMucocSeperationsVO;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import com.pixalere.assessment.bean.AssessmentAntibioticVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentSinustractsVO;
import com.pixalere.assessment.bean.AssessmentUnderminingVO;
import com.pixalere.assessment.bean.AssessmentWoundBedVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.guibeans.LocationDepthVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.service.WoundServiceImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Difficult Database migrations.
 *
 * @since 5.1
 * @author travis
 */
public class V6_0_11_0_1__Antibiotics implements JdbcMigration {

    private String execution_log = "";

    public void migrate(Connection connection) throws Exception {
        DataSource ds = null;
        System.out.println("Migrate from Java");
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
        } catch (NamingException ne) {
            throw new RuntimeException("Unable to aquire data source", ne);

        }

        String[] locations = {"_live"};

        ProfessionalServiceImpl uBD = new ProfessionalServiceImpl();

        ListServiceImpl lservice = new ListServiceImpl(1);

        AssessmentServiceImpl aservice = new AssessmentServiceImpl(1);

        WoundServiceImpl wservice = new WoundServiceImpl(1);

        WoundAssessmentServiceImpl waservice = new WoundAssessmentServiceImpl();

        WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();

        PatientProfileServiceImpl pservice = new PatientProfileServiceImpl(1);

        TreatmentCommentsServiceImpl tservice = new TreatmentCommentsServiceImpl();

        for (String location : locations) {

            WoundServiceImpl w = new WoundServiceImpl(1);

            Connection con;
            System.out.println("conn: ");
            con = ds.getConnection();
            System.out.println("conn: " + con);
            Statement stmt = con.createStatement();
            System.out.println("stmt: " + stmt);

            String mssql = "";
            String mysql = "";
            if (checkIsMySQL(con)) {
                mysql = " limit 1";
            } else {
                mssql = " top 1 ";
            }

            //Backdate assessments.
            System.out.println("MySql: " + checkIsMySQL(con));
            if (checkIsMySQL(con) == false) {
                System.out.println("select eachwound.visited_on as last_update,eachwound.alpha_id as alpha_id,eachwound.patient_id as patient_id,aclose.id as id,aclose.closed_date as closed_date  from wound_assessment_location wal outer apply (select top 1 * from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundActive') order by visited_on desc) eachwound  outer apply (select top 1 * from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') and (discharge_reason=431 or discharge_reason=435 or discharge_reason=788 or discharge_reason=799 or discharge_reason=854 or discharge_reason=1305) order by visited_on desc) aclose where (DATEADD(dd, 14, eachwound.visited_on)<aclose.closed_date) and wal.discharge=1 and aclose.id IS NOT NULL");
                PreparedStatement stateback = con.prepareStatement("select eachwound.visited_on as last_update,eachwound.alpha_id as alpha_id,eachwound.patient_id as patient_id,aclose.id as id,aclose.closed_date as closed_date  from wound_assessment_location wal outer apply (select " + mssql + " * from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundActive') order by visited_on desc " + mysql + ") eachwound  outer apply (select " + mssql + " * from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') and (discharge_reason=435 or discharge_reason=431 or discharge_reason=788 or discharge_reason=799 or discharge_reason=854 or discharge_reason=1305) order by visited_on desc " + mysql + ") aclose where (DATEADD(dd, 14, eachwound.visited_on)<aclose.closed_date) and wal.discharge=1 and aclose.id IS NOT NULL", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet resultback = stateback.executeQuery();
                resultback.setFetchSize(100);
                while (resultback.next()) {
                    try {
                        int patient_id = resultback.getInt("patient_id");
                        int alpha_id = resultback.getInt("alpha_id");
                        int id = resultback.getInt("id");
                        String closed_date = resultback.getString("closed_date");
                        String last_update = resultback.getString("last_update");
                        String update = "update assessment_wound set closed_date=DateAdd(dd,14,'" + last_update + "') where alpha_id=" + alpha_id + " and status=(select current_value from configuration where config_setting='woundClosed')";
                        String update2 = "update wound_assessment_location set close_time_stamp=DateAdd(dd,14,'" + last_update + "') where id=" + alpha_id;
                        String insert = "insert into nursing_fixes (patient_id,row_id,professional_id,time_stamp,initials,field,error,delete_reason) values (" + patient_id + "," + id + ",71,'" + PDate.getEpochTime() + "','2300h 15/Feb/2013 WOCN#71 SH',4,'Error: Inappropriate Closure Date : " + closed_date + "','')";
                        stmt.executeUpdate(update);
                        stmt.executeUpdate(update2);
                        stmt.executeUpdate(insert);
                    } catch (Exception e) {
                    }
                }
                resultback.close();
                stateback.close();

                /*System.out.println("select eachwound.last_update as last_update,eachwound.alpha_id as alpha_id,eachwound.patient_id as patient_id,aclose.id as id,aclose.drain_removed_date as drain_removed_date  from wound_assessment_location wal outer apply (select top 1 * from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainActive') order by last_update desc) eachwound  outer apply (select top 1 * from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainClosed') and discharge_reason=(select * from lookup_l10n where name LIKE 'Drain removed%') order by last_update desc) aclose where (convert(int,eachwound.last_update)+1209600)<convert(int,aclose.drain_removed_date) and wal.discharge=1 and aclose.id IS NOT NULL");
                 PreparedStatement stateback2 = con.prepareStatement("select eachwound.last_update as last_update,eachwound.alpha_id as alpha_id,eachwound.patient_id as patient_id,aclose.id as id,aclose.drain_removed_date as drain_removed_date  from wound_assessment_location wal outer apply (select top 1 * from assessment_drain ae where ae.alpha_id=wal.id and status=(select top 1 current_value from configuration where config_setting='drainActive') order by last_update desc) eachwound  outer apply (select top 1 * from assessment_drain ae where ae.alpha_id=wal.id and status=(select top 1 current_value from configuration where config_setting='drainClosed') and discharge_reason=(select lookup_id from lookup_l10n where name LIKE 'Drain removed%') order by last_update desc) aclose where (convert(int,eachwound.last_update)+1209600)<convert(int,aclose.drain_removed_date) and wal.discharge=1 and aclose.id IS NOT NULL", ResultSet.TYPE_SCROLL_INSENSITIVE,
                 ResultSet.CONCUR_READ_ONLY);

                 ResultSet resultback2 = stateback2.executeQuery();
                 resultback2.setFetchSize(100);
                 while (resultback2.next()) {
                 try {
                 int patient_id = resultback2.getInt("patient_id");
                 int alpha_id = resultback2.getInt("alpha_id");
                 int id = resultback2.getInt("id");
                 String closed_date = resultback2.getString("drain_removed_date");
                 String last_update = resultback2.getString("last_update");
                 String update = "update assessment_drain set drain_removed_date=" + (Integer.parseInt(last_update) + 1209600) + " where alpha_id=" + alpha_id + " and status=(select current_value from configuration where config_setting='drainClosed')";
                 String insert = "insert into nursing_fixes (patient_id,row_id,professional_id,time_stamp,initials,field,error,delete_reason) values (" + patient_id + "," + id + ",71,'" + PDate.getEpochTime() + "','2300h 15/Feb/2013 WOCN#71 SH',4,'Error: Inappropriate Closure Date : " + PDate.getFormattedDateFromEpoch(PDate.getDate(closed_date), "yyyy mmm dd") + "','')";
                 stmt.executeUpdate(update);
                 stmt.executeUpdate(insert);
                 } catch (Exception e) {
                 }
                 }
                 resultback2.close();
                 stateback2.close();*/
                PreparedStatement stateback3 = con.prepareStatement("select eachwound.visited_on as last_update,eachwound.alpha_id as alpha_id,eachwound.patient_id as patient_id,aclose.id as id,aclose.closed_date as closed_date  from wound_assessment_location wal outer apply (select top 1 * from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionActive') order by visited_on desc) eachwound  outer apply (select top 1 * from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionClosed') and (discharge_reason=1546 or discharge_reason=1547 or discharge_reason=1593 or discharge_reason=1714 or discharge_reason=1548) order by visited_on desc) aclose where (dateadd(dd,14,eachwound.visited_on)<aclose.closed_date) and wal.discharge=1 and aclose.id IS NOT NULL", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet resultback3 = stateback3.executeQuery();
                resultback3.setFetchSize(100);
                while (resultback3.next()) {
                    try {
                        int patient_id = resultback3.getInt("patient_id");
                        int alpha_id = resultback3.getInt("alpha_id");
                        int id = resultback3.getInt("id");
                        String closed_date = resultback3.getString("closed_date");
                        String last_update = resultback3.getString("last_update");
                        String update = "update assessment_incision set closed_date=DateAdd(dd,14,'" + last_update + "') where alpha_id=" + alpha_id + " and status=(select current_value from configuration where config_setting='incisionClosed')";
                        String update2 = "update wound_assessment_location set close_time_stamp=DateAdd(dd,14,'" + last_update + "') where id=" + alpha_id;
                        //System.out.println(update2);
                        String insert = "insert into nursing_fixes (patient_id,row_id,professional_id,time_stamp,initials,field,error,delete_reason) values (" + patient_id + "," + id + ",71,'" + PDate.getEpochTime() + "','2300h 15/Feb/2013 WOCN#71 SH',4,'Error: Inappropriate Closure Date : " + closed_date + "','')";
                        stmt.executeUpdate(update);
                        stmt.executeUpdate(update2);
                        stmt.executeUpdate(insert);
                    } catch (Exception e) {
                    }
                }
                resultback3.close();
                stateback3.close();

                System.out.println("Assessment Getting: " + PDate.getDateTime(new java.util.Date()));

                PreparedStatement state = con.prepareStatement("select  wound_assessment.patient_id as patient_id,wound_assessment.professional_id as professional_id,assessment_wound.visited_on as visited_on,assessment_wound.wound_profile_type_id,assessment_wound.assessment_id,assessment_wound.id as id,wound_assessment.wound_id as wound_id,assessment_treatment.id as treatment_id ,antibiotics,assessment_wound.alpha_id,wound_assessment.user_signature as user_signature from assessment_wound LEFT JOIN wound_assessment ON wound_assessment.id=assessment_wound.assessment_id left join assessment_treatment on assessment_wound.alpha_id=assessment_treatment.alpha_id where assessment_treatment.assessment_id=assessment_wound.assessment_id and wound_assessment.active=1 and assessment_wound.id<1000000 and antibiotics IS NOT NULL and antibiotics != 'a:0:{}' order by id desc", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet result = state.executeQuery();
                result.setFetchSize(100);

                System.out.println("Assessment Start: " + PDate.getDateTime(new java.util.Date()));
                String sql = "insert into treatment_comments (patient_id,wound_id,active,professional_id,user_signature,body,visited_on,created_on,assessment_id,wound_profile_type_id) values ";
                String sql_inner = "";

                int anti_cnt = 0;
                int cnt = 0;
                while (result.next()) {

                    cnt++;
                    if (cnt % 1000 == 0) {

                        System.out.println(cnt + " @ " + PDate.getDateTime(new java.util.Date()));
                    }

                    int id = result.getInt("id");

                    int patient_id = result.getInt("patient_id");

                    int assessment_id = result.getInt("assessment_id");

                    int alpha_id = result.getInt("alpha_id");

                    int professional_id = result.getInt("professional_id");

                    String time_stamp = result.getString("visited_on");

                    int wound_profile_type_id = result.getInt("wound_profile_type_id");
                    int treatment_id = result.getInt("treatment_id");

                    int wound_id = result.getInt("wound_id");

                    String anti = result.getString("antibiotics");
                    String user_signature = result.getString("user_signature");
                    if (anti != null && !anti.equals("") && !anti.equals("a:0:{}")) {
                        anti_cnt++;
                        Vector<String> antibiotics = Serialize.unserializeLegacy(anti);
                    //String antibiotic = Serialize.commaIze(anti);
                        //a:1:{i:0;s:45:"Cipro and Keflex (?) : 23/5/2008 to 31/5/2008";}
                        for (String antibiotic : antibiotics) {

                            try {

                                //Find out if the wound has only 1 alpha, then assign antibiotic, otherwise it goes in treatment comments.
                                PreparedStatement stateA = con.prepareStatement("select id from wound_assessment_location where wound_id=" + wound_id + " and active=1", ResultSet.TYPE_SCROLL_INSENSITIVE,
                                        ResultSet.CONCUR_READ_ONLY);

                                ResultSet resultAnti = stateA.executeQuery();
                                resultAnti.setFetchSize(100);
                                //while (resultAnti.next()) {
                                int s = resultAnti.getFetchSize();

                                if (s == 1) {

                                    int index = antibiotic.indexOf(" : ");

                                    String name = antibiotic.substring(0, index);

                                    try {

                                        String tmp = antibiotic.substring(index + 3, antibiotic.length());

                                        int index2 = tmp.indexOf(" to ");

                                        String date1 = tmp.substring(0, index2);

                                        String date2 = tmp.substring(index2 + 4, tmp.length());

                                        SimpleDateFormat sf = new SimpleDateFormat("d/MM/yyyy");

                                        java.util.Date start_date = sf.parse(date1);

                                        java.util.Date end_date = sf.parse(date2);

                                        if (start_date != null && !name.equals("")) {

                                        //PreparedStatement states = con.prepareStatement("select id from assessment_treatment where assessment_id=" + assessment_id + " AND alpha_id=" + alpha_id + " order by id desc");
                                            //ResultSet results = states.executeQuery();
                                            //int treatment_id = -1;
                                            //while (result.next()) {
                                            //  treatment_id = results.getInt("id");
                                            // }
                                            AssessmentAntibioticVO a = new AssessmentAntibioticVO();

                                            a.setAssessment_treatment_id(treatment_id);

                                            a.setStart_date(start_date);

                                            if (end_date != null) {//may not have end_date

                                                a.setEnd_date(end_date);

                                            }

                                            a.setAntibiotic(name);

                                            aservice.saveAntibiotic(a);

                                        }

                                    } catch (StringIndexOutOfBoundsException e) {
                                        if (cnt % 1000 == 0) {
                                            System.out.println(antibiotic + " " + e.getMessage());
                                        }
                                        //e.printStackTrace();
                                    } catch (Exception e) {
                                        if (cnt % 1000 == 0) {
                                            System.out.println(antibiotic + " " + e.getMessage());
                                        }
                                        //e.printStackTrace();
                                    }

                                } else if (s > 1) {

                                    //long time = Long.parseLong(time_stamp);
                                    sql_inner = sql_inner + "(" + patient_id + "," + wound_id + ",1," + professional_id + ",'" + user_signature + "','Antibiotic: " + antibiotic + "','" + time_stamp + "','" + time_stamp + "'," + assessment_id + "," + wound_profile_type_id + "),";

                                    //tservice.saveTreatmentComment(tc);
                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                                if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                    con = ds.getConnection();
                                    stmt = con.createStatement();
                                }

                            }

                        }
                        //System.out.println("Saved Antibiotics: " + PDate.getDateTime(new java.util.Date()));
                    }
                    if (anti_cnt % 250 == 0) {

                        if (sql_inner.length() > 0) {
                            sql_inner = sql_inner.substring(0, sql_inner.length() - 1);
                            //System.out.println("Antibiotics: " + sql + sql_inner);
                            try {
                                stmt.executeUpdate(sql + sql_inner);
                            } catch (Exception e) {

                                e.printStackTrace();
                                if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                                    con = ds.getConnection();
                                    stmt = con.createStatement();
                                }

                            }
                            sql_inner = "";
                        }
                    }
                    /*if (anti_cnt > 0 && sql_inner.length() != 0) {
                     sql_inner = sql_inner.substring(0, sql_inner.length() - 1);
                     //System.out.println("Antibiotic: " + sql + sql_inner);
                     try {
                     // stmt.executeUpdate(sql + sql_inner);
                     } catch (Exception e) {

                     e.printStackTrace();
                     if (e.getMessage().indexOf("Operation timed out") != -1 || e.getMessage().indexOf("Connection reset") != -1 || e.getMessage().indexOf("Statement object is closed") != -1) {

                     con = ds.getConnection();
                     stmt = con.createStatement();
                     }

                     }
                     }*/

                }

                con.close();
                stmt.close();
                result.close();
                state.close();
            }
        }
    }

    private boolean checkIsMySQL(Connection con) {

        //select query with top 1... if it fails.. its mysql.
        boolean mysql = false;
        try {
            PreparedStatement state = con.prepareStatement("select top 1 from configuration", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
        } catch (com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException e) {
            //e.printStackTrace();
            mysql = true;
        } catch (Exception e) {
            //e.printStackTrace();
            if (e.getMessage().indexOf("MySQL") != -1) {
                mysql = true;
            }
        }
        return mysql;
    }
}
