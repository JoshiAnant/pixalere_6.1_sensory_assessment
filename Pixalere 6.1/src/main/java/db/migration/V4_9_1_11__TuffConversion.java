package db.migration;

import java.util.Hashtable;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import com.pixalere.assessment.bean.AssessmentAntibioticVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentSinustractsVO;
import com.pixalere.assessment.bean.AssessmentUnderminingVO;
import com.pixalere.assessment.bean.AssessmentWoundBedVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.guibeans.LocationDepthVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.service.WoundServiceImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Difficult Database migrations.
 *
 * @since 5.1
 * @author travis
 */
public class V4_9_1_11__TuffConversion implements JdbcMigration {

    private String execution_log = "";

    public void migrate(Connection connection) throws Exception {
        DataSource ds = null;
        System.out.println("Migrate from Java");
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
        } catch (NamingException ne) {
            throw new RuntimeException("Unable to aquire data source", ne);

        }



        try {

            String[] locations = {"_live"};

            ProfessionalServiceImpl uBD = new ProfessionalServiceImpl();

            ListServiceImpl lservice = new ListServiceImpl(1);

            AssessmentServiceImpl aservice = new AssessmentServiceImpl(1);

            WoundServiceImpl wservice = new WoundServiceImpl(1);

            WoundAssessmentServiceImpl waservice = new WoundAssessmentServiceImpl();

            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();

            PatientProfileServiceImpl pservice = new PatientProfileServiceImpl(1);

            TreatmentCommentsServiceImpl tservice = new TreatmentCommentsServiceImpl();

            for (String location : locations) {

                WoundServiceImpl w = new WoundServiceImpl(1);

                Connection con;



                con = ds.getConnection();




                //convert serialized fields from php format to pipes

                com.pixalere.assessment.dao.WoundAssessmentLocationDAO locationDAO = new com.pixalere.assessment.dao.WoundAssessmentLocationDAO();


                Statement stmt = con.createStatement();
                int[] cycle = {65000, 50000, 35000,20000,10000, 0};
                
                int before = 180000;
                for (int c : cycle) {
                    PreparedStatement state2 = con.prepareStatement("select id,wound_id,etiology, goals from wound_profiles where active=1 and etiology!='' and goals!='' and id>" + c + " and id<="+before+" order by id desc");
                    System.out.println("SQL QUERY: === select id,wound_id,etiology, goals from wound_profiles where active=1 and etiology!='' and goals!='' and id>" + c + " and id<="+before+" order by id desc");
                    before=c;
                    ResultSet result2 = state2.executeQuery();
                    result2.setFetchSize(1000);
                    PreparedStatement goals_state = con.prepareStatement("select name,lookup_id from lookup_l10n where  language_id=1 and (resource_id=" + LookupVO.WOUND_GOALS + " OR resource_id=" + LookupVO.OSTOMY_GOALS + " OR resource_id=" + LookupVO.TUBEDRAIN_GOALS + " OR resource_id=" + LookupVO.INCISION_GOALS + ")", ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);
                    ResultSet resultGoal = goals_state.executeQuery();
                    PreparedStatement etiology_state = con.prepareStatement("select name,lookup_id from lookup_l10n where   language_id=1 and (resource_id=" + LookupVO.ETIOLOGY + " OR resource_id=" + LookupVO.OSTOMY_ETIOLOGY + " OR resource_id=" + LookupVO.DRAINS_ETIOLOGY + " OR resource_id=" + LookupVO.POSTOP_ETIOLOGY + ")", ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);
                    ResultSet resultEtiology = etiology_state.executeQuery();
                    Hashtable etiology = new Hashtable();
                    Hashtable goalsH = new Hashtable();
                    while (resultEtiology.next()) {
                        String name = resultEtiology.getString("name");
                        String id = resultEtiology.getString("lookup_id");
                        etiology.put(name.trim(), id);
                    }
                    while (resultGoal.next()) {
                        String name = resultGoal.getString("name");
                        String id = resultGoal.getString("lookup_id");
                        goalsH.put(name.trim(), id);
                    }
                    int cnt = 0;

                    System.out.println("Wound Profile: " + PDate.getDateTime(new java.util.Date()));
                    int etiology_cnt = 0;
                    int goals_cnt = 0;
                    String etiology_sql = "insert into wound_etiology (wound_profile_id,alpha_id,lookup_id) values ";
                    String goals_sql = "insert into wound_goals (wound_profile_id,alpha_id,lookup_id) values ";
                    String inner_etiology_sql = "";
                    String inner_goals_sql = "";
                    while (result2.next()) {
                        String legacy_etiologies="Old Etiologies: ";
                        String legacy_goals = "Old Goals: ";
                        try {
                            Vector justAlphas = new Vector();

                            int wound_id = result2.getInt("wound_id");

                            int id = result2.getInt("id");

                            String wound_etiology = result2.getString("etiology");

                            String goals = result2.getString("goals");

                            Vector etiologies = Serialize.unserializeLegacy(wound_etiology);

                            Vector goals_vec = Serialize.unserializeLegacy(goals);


                            PreparedStatement state3 = con.prepareStatement("select id,alpha from wound_assessment_location where wound_id=" + wound_id + " order by id desc", ResultSet.TYPE_SCROLL_INSENSITIVE,
                                    ResultSet.CONCUR_READ_ONLY);

                            ResultSet result3 = state3.executeQuery();
                            result3.setFetchSize(100);

                            while (result3.next()) {
                                String alpha = result3.getString("alpha");
                                int alpha_id = result3.getInt("id");
                                // justAlphas = alphas/icons

                                if (!alpha.equals("incs")) {

                                    String value = getAlphaValue(etiologies, Common.getAlphaText(alpha, "en"));
                                    if (!value.equals("")) {
                                        String lookup_id = (String) etiology.get(value.trim());
                                        if (lookup_id != null) {
                                            etiology_cnt++;
                                            inner_etiology_sql = inner_etiology_sql + "(" + id + "," + alpha_id + "," + lookup_id + "),";
                                            if (etiology_cnt % 1000 == 0) {

                                                if (inner_etiology_sql.length() > 0) {
                                                    System.out.println("Etiology: " + etiology_cnt + " @ " + PDate.getDateTime(new java.util.Date()));
                                                    inner_etiology_sql = inner_etiology_sql.substring(0, inner_etiology_sql.length() - 1);
                                                    //System.out.println("Etiology: " + etiology_sql + inner_etiology_sql);
                                                    stmt.executeUpdate(etiology_sql + inner_etiology_sql);
                                                    System.out.println("Saved Etiology ");
                                                    inner_etiology_sql = "";
                                                }

                                            }

                                        } else {

                                            if (wound_etiology.indexOf(Common.getAlphaText(alpha, "en")) != -1) {
                                                //no etiology found
                                                legacy_etiologies = legacy_etiologies + wound_etiology;
                                                //System.out.println(wound_etiology + " Could find Etiology: " + alpha);
                                            }

                                        }

                                    } else {

                                        if (wound_etiology.indexOf(Common.getAlphaText(alpha, "en")) != -1) {
                                            //no alpha found
                                            legacy_etiologies = legacy_etiologies + wound_etiology;
                                            //System.out.println(wound_etiology + " Could find Etiology2: " + alpha);
                                        }

                                    }

                                    String value2 = getAlphaValue(goals_vec, Common.getAlphaText(alpha, "en"));

                                    if (!value2.equals("")) {
                                        String lookup_id = (String) goalsH.get(value2.trim());

                                        if (lookup_id != null) {
                                            goals_cnt++;
                                            inner_goals_sql = inner_goals_sql + "(" + id + "," + alpha_id + "," + lookup_id + "),";
                                            if (goals_cnt % 1000 == 0) {
                                                System.out.println("Goals: " + goals_cnt + " @ " + PDate.getDateTime(new java.util.Date()));
                                                if (inner_goals_sql.length() > 0) {
                                                    inner_goals_sql = inner_goals_sql.substring(0, inner_goals_sql.length() - 1);
                                                    //System.out.println("Goals: " + goals_sql + inner_goals_sql);
                                                    stmt.executeUpdate(goals_sql + inner_goals_sql);
                                                    System.out.println("Saved Goal: " );
                                                    inner_goals_sql = "";
                                                }

                                            }



                                        } else {

                                            if (goals.indexOf(Common.getAlphaText(alpha, "en")) != -1) {
                                                legacy_goals = legacy_goals + goals;
                                            }

                                        }

                                    } else {

                                        if (goals.indexOf(Common.getAlphaText(alpha, "en")) != -1) {
                                            //System.out.println(goals + " Could find Goals: " + alpha);
                                            legacy_goals = legacy_goals + goals;
                                        }

                                    }

                                }

                            }
                            if(!legacy_etiologies.equals("Old Etiologies: ") || !legacy_goals.equals("Old Goals: ")){
                                String combined = "";
                               if(!legacy_etiologies.equals("Old Etiologies: ")){
                                   combined=combined + legacy_etiologies;
                               }
                               if(!legacy_goals.equals("Old Goals: ")){
                                   combined=combined + legacy_goals;
                               }
                               System.out.println("update wound_profiles set root_cause_addressed_comments='"+combined+"' where id="+id);
                                stmt.executeUpdate("update wound_profiles set root_cause_addressed_comments='"+combined+"' where id="+id);
                            }
                            result3.close();
                            state3.close();


                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("Exception: " + goals_sql + inner_goals_sql);
                            if (e.getMessage().indexOf("Connection reset") != -1) {
                                con = ds.getConnection();
                            }
                        }

                    }
                    //insert remaining
                    System.out.println("End Game: " + etiology_sql + inner_etiology_sql);
                    System.out.println("End Game: " + goals_sql + inner_goals_sql);
                    try {
                        inner_goals_sql = inner_goals_sql.substring(0, inner_goals_sql.length() - 1);
                        inner_etiology_sql = inner_etiology_sql.substring(0, inner_etiology_sql.length() - 1);
                        stmt.executeUpdate(etiology_sql + inner_etiology_sql);
                        stmt.executeUpdate(goals_sql + inner_goals_sql);
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception: " + goals_sql + inner_goals_sql);
                        if (e.getMessage().indexOf("Connection reset") != -1) {
                            con = ds.getConnection();
                        }
                
                    }
                    result2.close();
                    state2.close();
                }
                executeQuery("alter table wound_profiles drop column etiology");

                executeQuery("alter table wound_profiles drop column goals");
                con.close();
            }

        } catch (Exception e) {

            e.printStackTrace();
            throw new RuntimeException("Exception: ", e);


        }


    }

    /**
     * Executes each line of the upgrade script.
     *
     * @param query the sql query to execute
     * @return boolean if there's an error return false
     */
    public boolean executeQuery(String query) {

        DataSource ds = null;
        try {

            Context initContext = new InitialContext();

            Context envContext = (Context) initContext.lookup("java:/comp/env");

            ds = (DataSource) envContext.lookup("jdbc/pixDS");

        } catch (NamingException ne) {

            throw new RuntimeException("Unable to aquire data source", ne);

        }



        try {

            Connection con;



            con = ds.getConnection();

            Statement stmt = con.createStatement();

            stmt.executeUpdate(query);
            stmt.close();
            con.close();



            return true;

        } catch (Exception e) {

            e.printStackTrace();

            execution_log = e.getMessage();

            return false;

        }



    }

    /**
     * @deprecated @param vecData
     * @param alpha
     * @return
     */
    public String getAlphaValue(Vector<String> vecData, String alpha) {

        Vector vecResult = new Vector();

        for (String strDataOption : vecData) {

            int s = strDataOption.indexOf(alpha + ":");

            s = s + (alpha.length() + 1);



            if (strDataOption.indexOf(alpha + ":") != -1) {

                String t = strDataOption.substring(s, strDataOption.length());

                return t.trim();

            }



        }

        return "";

    }

    public String getString(ResultSet set, String field) {
        try {
            if (set != null) {
                set.beforeFirst();
                while (set.next()) {
                    String s = set.getString(field);
                    if (s == null) {
                        s = "";
                    }

                    return s;
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
            e.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }
        return "";
    }
}
