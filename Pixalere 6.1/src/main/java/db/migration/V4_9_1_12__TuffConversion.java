package db.migration;
import java.util.List;
import java.util.ArrayList;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.service.*;
import com.pixalere.common.service.*;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import com.pixalere.assessment.bean.AssessmentAntibioticVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentSinustractsVO;
import com.pixalere.assessment.bean.AssessmentUnderminingVO;
import com.pixalere.assessment.bean.AssessmentWoundBedVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.guibeans.LocationDepthVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.service.WoundServiceImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Difficult Database migrations.
 *
 * @since 5.1
 * @author travis
 */
public class V4_9_1_12__TuffConversion implements JdbcMigration {

    private String execution_log = "";

    public void migrate(Connection connection) throws Exception {
        DataSource ds = null;
        System.out.println("Migrate from Java");
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
        } catch (NamingException ne) {
            throw new RuntimeException("Unable to aquire data source", ne);

        }



        try {

            String[] locations = {"_live"};

            ProfessionalServiceImpl uBD = new ProfessionalServiceImpl();

            ListServiceImpl lservice = new ListServiceImpl(1);

            AssessmentServiceImpl aservice = new AssessmentServiceImpl(1);

            WoundServiceImpl wservice = new WoundServiceImpl(1);

            WoundAssessmentServiceImpl waservice = new WoundAssessmentServiceImpl();

            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();

            PatientProfileServiceImpl pservice = new PatientProfileServiceImpl(1);

            TreatmentCommentsServiceImpl tservice = new TreatmentCommentsServiceImpl();

            for (String location : locations) {

                WoundServiceImpl w = new WoundServiceImpl(1);

                Connection con;



                con = ds.getConnection();
                Statement stmt = con.createStatement();

                PreparedStatement state3 = con.prepareStatement("select id,investigation from patient_profiles where investigation IS NOT NULL and investigation!='' order by id desc");

                ResultSet result3 = state3.executeQuery();
                result3.setFetchSize(1000);
                int cnt = 0;
                String invest_sql = "insert into patient_investigations (patient_profile_id,lookup_id,investigation_date) values ";
                String inner_invest_sql = "";

                System.out.println("Investigation: " + PDate.getDateTime(new java.util.Date()));

                while (result3.next()) {
                    try {
                        int id = result3.getInt("id");

                        String investigations = result3.getString("investigation");

                        //System.out.println("Invest: " + investigations);

                        Vector<String> unserialize = Serialize.unserializeStraight(investigations);

                        if (unserialize != null) {

                            for (String un : unserialize) {

                                int index = un.indexOf(": ");

                                if (index != -1) {

                                    String value = un.substring(0, index);



                                    String date = un.substring(index + 3, un.length());

                                    date = date.trim();
                                    //System.out.println("Invest value: "+value);
                                    LookupVO l = lservice.getListItem(value);

                                    if (l != null) {
                                        cnt++;

                                        SimpleDateFormat f = new SimpleDateFormat("d/MMMMM/yyyy");

                                        java.util.Date dates = null;

                                        if (!date.equals("//")) {

                                            try {

                                                dates = f.parse(date);

                                            } catch (ParseException e) {
                                            }

                                        }
                                        SimpleDateFormat ff = new SimpleDateFormat("yyyy-MM-dd");
                                        if (dates != null) {
                                            inner_invest_sql = inner_invest_sql + "(" + id + "," + l.getId() + ",'" + ff.format(dates) + "'),";
                                        } else {
                                        }//System.out.println("Date is NULL");}
                                        if (cnt % 1000 == 0) {

                                            if (inner_invest_sql.length() > 0) {
                                                inner_invest_sql = inner_invest_sql.substring(0, inner_invest_sql.length() - 1);
                                                //System.out.println("Invest: " +  inner_invest_sql);
                                                executeQuery(invest_sql + inner_invest_sql);
                                            }
                                            inner_invest_sql = "";
                                        }

                                    } else {

                                        System.out.println(value + " Could not find Investigation: " + id + " Value: " + investigations);

                                    }

                                } else {

                                    System.out.println("Failed: " + un);

                                }

                            }

                        } else {
                            //System.out.println("Could not Find Invstigation"+id);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (e.getMessage().indexOf("Connection reset") != -1) {
                            con = ds.getConnection();
                        }
                    }

                }
                //insert remaining
                System.out.println("End Game: " + invest_sql + inner_invest_sql);


                try {
                    inner_invest_sql = inner_invest_sql.substring(0, inner_invest_sql.length() - 1);
                    stmt.executeUpdate(invest_sql + inner_invest_sql);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (e.getMessage().indexOf("Connection reset") != -1) {
                        con = ds.getConnection();
                    }
                }
                int prev = 0;
                int count = 0;
                while (count <= 28000) {
                    count = count + 1000;
                    PreparedStatement state4 = con.prepareStatement("select patient_id from patient_accounts where current_flag=1 and patient_id>" + prev + " and patient_id<=" + count + " and patient_id IS NOT NULL order by id desc");


                    System.out.println("Count: " + count + ": select patient_id from patient_accounts where current_flag=1 and patient_id>" + prev + " and patient_id<=" + count + " and patient_id IS NOT NULL order by id desc");
                    prev = count;
                    ResultSet result4 = state4.executeQuery();
                    result4.setFetchSize(10000);
                    //cnt = 0;

                    //System.out.println("Investigation: " + PDate.getDateTime(new Date()));
                    int blah = 0;
                    
                    while (result4.next()) {

                        try {
                            int id = result4.getInt("patient_id");
                            PreparedStatement state5 = con.prepareStatement("select id,patient_id,account_status from patient_accounts where patient_id=" + id + " order by id asc");

                            ResultSet result5 = state5.executeQuery();
                            result5.setFetchSize(10000);
                            boolean discharged = false;
                            int c = 0;
                            while (result5.next()) {

                                int pid = result5.getInt("patient_id");
                                int as = result5.getInt("account_status");
                                int i = result5.getInt("id");
                                if (cnt != 0) {//all fields but the first one.
                                    //update the user_signature!

                                    executeQuery("update patient_accounts set user_signature=(select top 1 user_signature from patient_accounts where flag=1 and id<" + i + ") where id=" + i);



                                }
                                if (discharged == true && as == 0) {
                                    // do nothing
                                } else if (discharged == false && as == 1) {
                                    if (c == 0) {

                                        executeQuery("update patient_accounts set action='ADMIT' where id=" + i);
                                    }
                                    //do nothing
                                } else if (as == 0) {
                                    discharged = true;
                                    executeQuery("update patient_accounts set action='DISCHARGE' where id=" + i);
                                } else if (as == 1) {
                                    discharged = false;
                                    executeQuery("update patient_accounts set action='ADMIT' where id=" + i);

                                }
                                c++;

                            }
                            blah++;
                            if (blah % 25 == 0) {
                                System.out.print(".");
                                if (blah == 250) {
                                    System.out.print("25%");
                                }
                                if (blah == 500) {
                                    System.out.print("50%");
                                }
                                if (blah == 750) {
                                    System.out.print("75%");
                                }
                            }
                            if (blah % 1000 == 0) {
                                System.out.println("Patient Count: " + blah + " ON " + id);
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                            if (e.getMessage().indexOf("Connection reset") != -1) {
                                con = ds.getConnection();

                                stmt = con.createStatement();
                                System.out.println("Reset Connection ===========");
                            }

                        }

                    }
                    result4.close();
                    state4.close();
                }
               // executeQuery("update patient_accounts set sig_temp=user_signature;update patient_accounts set sig_temp=substring(sig_temp,6,len(sig_temp))");




                con.close();
            }
            
        } catch (Exception e) {

            e.printStackTrace();
            throw new RuntimeException("Exception: ", e);


        }


    }

    /**
     * Executes each line of the upgrade script.
     *
     * @param query the sql query to execute
     * @return boolean if there's an error return false
     */
    public boolean executeQuery(String query) {

        DataSource ds = null;
        try {

            Context initContext = new InitialContext();

            Context envContext = (Context) initContext.lookup("java:/comp/env");

            ds = (DataSource) envContext.lookup("jdbc/pixDS");

        } catch (NamingException ne) {

            throw new RuntimeException("Unable to aquire data source", ne);

        }



        try {

            Connection con;



            con = ds.getConnection();

            Statement stmt = con.createStatement();

            stmt.executeUpdate(query);

            con.close();
            stmt.close();


            return true;

        } catch (Exception e) {

            e.printStackTrace();

            execution_log = e.getMessage();

            return false;

        }



    }

    /**
     * @deprecated @param vecData
     * @param alpha
     * @return
     */
    public String getAlphaValue(Vector<String> vecData, String alpha) {

        Vector vecResult = new Vector();

        for (String strDataOption : vecData) {

            int s = strDataOption.indexOf(alpha + ":");

            s = s + (alpha.length() + 1);



            if (strDataOption.indexOf(alpha + ":") != -1) {

                String t = strDataOption.substring(s, strDataOption.length());

                return t.trim();

            }



        }

        return "";

    }

}
