package com.pixalere.wound.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 *     This is the LocationImagesVO  bean for the location_images  (DB table)
 *     Refer to {@link com.pixalere.wound.service.WoundServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class LocationImagesVO extends ValueObject implements Serializable {

    /** identifier field */
    private Integer id;
    private Integer orderby;
    private String location;
    private String wound_name;
    private String view;
    private String gender;
    private Integer limb;
    private Integer lower_limb;
    private Integer foot;
    private String filename_full;
    private String filename_midline;
    private String filename_left;
    private String filename_right;
    private String blocked_boxes;
    private String slices;
    private Integer rows;
    private Integer woundcare;
    private Integer incision;
    private Integer drain;
    private Integer stoma;
    private Integer burn;
    
    /** default constructor */
    public LocationImagesVO() {
    }
    
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getOrderby() {
        return this.orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

    public String getLocation() {
        return this.location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getWound_name() {
        return wound_name;
    }

    public void setWound_name(String wound_name) {
        this.wound_name = wound_name;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getLimb() {
        return limb;
    }

    public void setLimb(Integer limb) {
        this.limb = limb;
    }

    public Integer getLower_limb() {
        return lower_limb;
    }

    public void setLower_limb(Integer lower_limb) {
        this.lower_limb = lower_limb;
    }
    
    public Integer getFoot() {
        return foot;
    }

    public void setFoot(Integer foot) {
        this.foot = foot;
    }

    public String getFilename_full() {
        return filename_full;
    }

    public void setFilename_full(String filename_full) {
        this.filename_full = filename_full;
    }

    public String getFilename_midline() {
        return filename_midline;
    }

    public void setFilename_midline(String filename_midline) {
        this.filename_midline = filename_midline;
    }

    public String getFilename_left() {
        return filename_left;
    }

    public void setFilename_left(String filename_left) {
        this.filename_left = filename_left;
    }

    public String getFilename_right() {
        return filename_right;
    }

    public void setFilename_right(String filename_right) {
        this.filename_right = filename_right;
    }

    public String getBlocked_boxes() {
        return blocked_boxes;
    }

    public void setBlocked_boxes(String blocked_boxes) {
        this.blocked_boxes = blocked_boxes;
    }

    public String getSlices() {
        return slices;
    }

    public void setSlices(String slices) {
        this.slices = slices;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getWoundcare() {
        return woundcare;
    }

    public void setWoundcare(Integer woundcare) {
        this.woundcare = woundcare;
    }

    public Integer getStoma() {
        return stoma;
    }

    public void setStoma(Integer stoma) {
        this.stoma = stoma;
    }
    
    public Integer getIncision() {
        return incision;
    }

    public void setIncision(Integer incision) {
        this.incision = incision;
    }
    
    public Integer getDrain() {
        return drain;
    }

    public void setDrain(Integer drain) {
        this.drain = drain;
    }
    
    public Integer getBurn() {
        return burn;
    }

    public void setBurn(Integer burn) {
        this.burn = burn;
    }
    
}
