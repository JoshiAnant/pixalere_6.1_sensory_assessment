package com.pixalere.wound.bean;

import com.pixalere.common.bean.LookupVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.annotation.ComparableField;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Mar 11, 2010
 * Time: 11:01:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class EtiologyVO extends ArrayValueObject implements Serializable {
   
    @ComparableField
    private Integer lookup_id;
    private Integer alpha_id;
    private Integer wound_profile_id;
   
    private WoundAssessmentLocationVO alpha;

    public EtiologyVO() {
    }

    public EtiologyVO(Integer wound_profile_id, Integer alpha_id, Integer lookup_id) {
        this.wound_profile_id = wound_profile_id;
        this.alpha_id = alpha_id;
        this.lookup_id = lookup_id;
    }


    public Integer getLookup_id() {
        return lookup_id;
    }

    public void setLookup_id(Integer lookup_id) {
        this.lookup_id = lookup_id;
    }

    public Integer getAlpha_id() {
        return alpha_id;
    }

    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }

    public Integer getWound_profile_id() {
        return wound_profile_id;
    }

    public void setWound_profile_id(Integer wound_profile_id) {
        this.wound_profile_id = wound_profile_id;
    }


    public WoundAssessmentLocationVO getAlpha() {
        return alpha;
    }

    public void setAlpha(WoundAssessmentLocationVO alpha) {
        this.alpha = alpha;
    }

    public String getPrintable(Integer language) {
        String print = "";
        
        if (alpha != null && getLookup() != null) {
            String alpha_str = alpha.getAlpha();
            if (alpha_str.indexOf(Constants.OSTOMY_ALPHAS[0]) != -1) {
                print = "Urostomy";//'+alpha.substring(4,alpha.length());

            }

            if (alpha_str.indexOf(Constants.OSTOMY_ALPHAS[1]) != -1) {
                print = "Fecal Stoma";//+alpha.substring(4,alpha.length());

            }
            for (String w : Constants.ALPHAS) {
                if (w.equals(alpha_str)) {
                    print = "Wound " + alpha_str;
                }
            }

            //check incision
            for (String inc : Constants.INCISION_ALPHA) {
                if (inc.equals(alpha_str)) {
                    String num = inc.substring(3, alpha_str.length());
                    print = "Incision " + num;
                }
            }

            //check tube or drain
            for (String inc : Constants.DRAIN_ALPHAS) {
                if (inc.equals(alpha_str)) {
                    String num = inc.substring(2, alpha_str.length());
                    print = "Tube/Drain " + num;
                }
            }
        for (String inc : Constants.SKIN_ALPHAS) {
            if (inc.equals(alpha_str)) {
                String num = inc.substring(1, alpha_str.length());
                print = "Skin " + num;
            }
        }
            print = print + ": " + getLookup().getName(language);
        }
        
        return print;
    }
    public static String getAlphaText(String display_alpha, Integer language) {
        String locale = Common.getLanguageLocale(language);
        return getAlphaText(display_alpha, locale);
    }
    public static String getAlphaText(String display_alpha, String locale) {
        if (display_alpha != null) {
            if (display_alpha.length() > 0) {

                if (display_alpha.length() == 1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.woundcare", locale) + " " + display_alpha;
                } else if (display_alpha.indexOf("ostu") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.urinalstoma", locale);
                } else if (display_alpha.indexOf("ostf") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.fecalstoma", locale);
                } else if (display_alpha.indexOf("s") == 0) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.skin", locale) + " " + display_alpha.substring(1);
                }else if (display_alpha.indexOf("td") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.drain", locale) + " " + display_alpha.substring(2);
                } else if (display_alpha.indexOf("tag") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.incision_tag", locale) + " " + display_alpha.substring(3);
                } else if (display_alpha.indexOf("burn") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.burn", locale);
                }

            }

        }

        return display_alpha;
    }
    private Integer current_flag;
    public Integer getCurrent_flag(){
        return current_flag;
    }
    public void setCurrent_flag(Integer current_flag){
        this.current_flag=current_flag;
    }
    
}
