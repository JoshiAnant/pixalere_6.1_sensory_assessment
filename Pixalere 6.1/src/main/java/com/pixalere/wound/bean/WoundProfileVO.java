package com.pixalere.wound.bean;
import java.lang.reflect.Field;
import com.pixalere.annotation.ComparableField;

import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Vector;
import com.pixalere.utils.Common;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.PDate;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Collection;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import com.pixalere.utils.Constants;
/**
 *     This is the WoundProfileVO  bean for the wound_profiles  (DB table)
 *     Refer to {@link com.pixalere.wound.service.WoundServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundVO
 *
 */
public class WoundProfileVO extends ValueObject implements Serializable {
    @ComparableField
    private String type_of_ostomy;
    private ProfessionalVO professional;
    private WoundVO wound;

    private Integer active;
    /** identifier field */
    private Collection<GoalsVO> goals;
    private Collection<EtiologyVO> etiologies;
    private Collection<WoundAcquiredVO> wound_acquired;
 private Date lastmodified_on;
    private Integer id;
    private Integer review_done;

    private Integer offline_flag;
    private Integer wound_id;
    private Integer current_flag;
    private Integer patient_id;
    private Integer patient_account_id;
    @ComparableField
    private Integer pressure_ulcer;
    @ComparableField
    private String operative_procedure_comments;
    @ComparableField
    private Date date_surgery;
    @ComparableField
    private String surgeon;
    @ComparableField
    private Integer marking_prior_surgery;
    @ComparableField
    private String patient_limitations;
    private String burn_areas;
    private Date preliminary_tbsa;
    private Date final_tbsa;
    private String tbsa_percentage;
    @ComparableField
    private Integer teaching_goals;
    @ComparableField
    private String prev_prods;
    private Integer referral;
    private Date created_on;
    @ComparableField 
    private String reason_for_care;
    private Integer professional_id;
    private String user_signature;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
    private String root_cause_addressed_comments;
    private Integer root_cause_addressed;
     public Integer getDeleted() {
         return deleted;
     }

     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }

     public String getDeleted_reason() {
         return deleted_reason;
     }

     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }

     public String getDelete_signature() {
         return delete_signature;
     }

     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    /** default constructor */
    public WoundProfileVO() {
    }
    public Integer getPatient_account_id() {
        return patient_account_id;
    }
    
    public void setPatient_account_id(Integer patient_account_id) {
        this.patient_account_id = patient_account_id;
    }
    /**
     * @method getProfessional_id
     * @field professional_id - Integer
     * @return Returns the professional_id.
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    
    /**
     * @method setProfessional_id
     * @field professional_id - Integer
     * @param professional_id The professional_id to set.
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    
    public Integer getActive() {
        return active;
    }
    
    
    public void setActive(Integer active) {
        this.active = active;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getPatient_id() {
        return this.patient_id;
    }
    
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    
    
    public String getReason_for_care() {
        return reason_for_care;
    }
    
    public void setReason_for_care(String reason_for_care) {
        this.reason_for_care = reason_for_care;
    }
    
   
    
    public Integer getPressure_ulcer() {
        return this.pressure_ulcer;
    }
    
    public void setPressure_ulcer(Integer pressure_ulcer) {
        this.pressure_ulcer = pressure_ulcer;
    }
   
    
    public Integer getWound_id() {
        return this.wound_id;
    }
    
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    
    public String getPrev_prods() {
        return this.prev_prods;
    }
    
    public void setPrev_prods(String prev_prods) {
        this.prev_prods = prev_prods;
    }
    
    
    public Integer getReferral() {
        return this.referral;
    }
    
    public void setReferral(Integer referral) {
        this.referral = referral;
    }
    
    public Date getCreated_on() {
        return this.created_on;
    }
    
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    
    public Integer getCurrent_flag() {
        return this.current_flag;
    }
    
    public void setCurrent_flag(Integer current_flag) {
        this.current_flag = current_flag;
    }
    
    public String getUser_signature() {
        return this.user_signature;
    }
    
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    
    
    public ProfessionalVO getProfessional(){
        return professional;
    }
    public void setProfessional(ProfessionalVO professional){
        this.professional=professional;
    }
   
    
    public void setWound(WoundVO wound){
        this.wound=wound;
    }
    public WoundVO getWound(){
        return wound;
    }
    
    public Integer getOffline_flag() {
        return offline_flag;
    }
    
    public void setOffline_flag(Integer offline_flag) {
        this.offline_flag = offline_flag;
    }
    
    
    public String getSurgeon() {
        return surgeon;
    }
    
    public void setSurgeon(String surgeon) {
        this.surgeon = surgeon;
    }
    
    public String getOperative_procedure_comments() {
        return operative_procedure_comments;
    }
    
    public void setOperative_procedure_comments(String operative_procedure_comments) {
        this.operative_procedure_comments = operative_procedure_comments;
    }
    
    public Date getDate_surgery() {
        return date_surgery;
    }
    
    public void setDate_surgery(Date date_surgery) {
        this.date_surgery = date_surgery;
    }
    
    public Integer getMarking_prior_surgery() {
        return marking_prior_surgery;
    }
    
    public void setMarking_prior_surgery(Integer marking_prior_surgery) {
        this.marking_prior_surgery = marking_prior_surgery;
    }
    
    public String getPatient_limitations() {
        return patient_limitations;
    }
    
    public void setPatient_limitations(String patient_limitations) {
        this.patient_limitations = patient_limitations;
    }
    
    public String getBurn_areas() {
        return burn_areas;
    }
    
    public void setBurn_areas(String burn_areas) {
        this.burn_areas = burn_areas;
    }
    
    public Date getPreliminary_tbsa() {
        return preliminary_tbsa;
    }

    public void setPreliminary_tbsa(Date preliminary_tbsa) {
        this.preliminary_tbsa = preliminary_tbsa;
    }

    public Date getFinal_tbsa() {
        return final_tbsa;
    }

    public void setFinal_tbsa(Date final_tbsa) {
        this.final_tbsa = final_tbsa;
    }

    public String getTbsa_percentage() {
        return tbsa_percentage;
    }

    public void setTbsa_percentage(String tbsa_percentage) {
        this.tbsa_percentage = tbsa_percentage;
    }

    public Integer getTeaching_goals() {
        return teaching_goals;
    }
    
    public void setTeaching_goals(Integer teaching_goals) {
        this.teaching_goals = teaching_goals;
    }
    
    

    public Vector getDate_surgery_formvalues() {
        Vector<String> dates = new Vector<String>();
        try {

            if (getDate_surgery() == null) {
                return new Vector();
            }

            GregorianCalendar calendar = new GregorianCalendar();
            
            calendar.setTime(getDate_surgery());

            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
        }

    public Vector getPreliminary_tbsa_formvalues() {
        Vector<String> dates = new Vector<String>();
        try {

            if (getPreliminary_tbsa() == null) {
                return new Vector();
            }

            GregorianCalendar calendar = new GregorianCalendar();
            
            calendar.setTime(getPreliminary_tbsa());

            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
        }

    public Vector getFinal_tbsa_formvalues() {
        Vector<String> dates = new Vector<String>();
        try {

            if (getFinal_tbsa() == null) {
                return new Vector();
            }

            GregorianCalendar calendar = new GregorianCalendar();
            
            calendar.setTime(getFinal_tbsa());

            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
        }


    public String getType_of_ostomy() {
        return type_of_ostomy;
    }

    public void setType_of_ostomy(String type_of_ostomy) {
        this.type_of_ostomy = type_of_ostomy;
    }
    public Integer getReview_done(){
        return review_done;
    }
    public void setReview_done(Integer review_done){
        this.review_done=review_done;
    }

    /**
     * @return the patient_visit_timestamp
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }

    /**
     * @param patient_visit_timestamp the patient_visit_timestamp to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
/**
     * @return the goals
     */
    public Collection<GoalsVO> getGoals() {
        return goals;
    }

    /**
     * @param goals the goals to set
     */
    public void setGoals(Collection<GoalsVO> goals) {
        this.goals = goals;
    }

    /**
     * @return the etiologies
     */
    public Collection<EtiologyVO> getEtiologies() {
        return etiologies;
    }

    /**
     * @param etiologies the etiologies to set
     */
    public void setEtiologies(Collection<EtiologyVO> etiologies) {
        this.etiologies = etiologies;
    }
    public <WoundProfileVO> boolean compare(Object t, Object t2) throws IllegalArgumentException,
                                          IllegalAccessException {//<WoundProfileVO>
        WoundProfileVO obj1 = (WoundProfileVO)t;
        WoundProfileVO obj2 = (WoundProfileVO)t2;
        Field[] fields = obj1.getClass().getDeclaredFields();
        boolean differ=false;
        if (fields != null) {
            for (Field field : fields) {
                if (field.isAnnotationPresent(ComparableField.class)) {
                    field.setAccessible(true);
                   
                    if((field == null || obj1 == null || field.get(obj1) == null) || (field == null || obj2 == null || field.get(obj2) == null)){
                        if((field == null || obj1 == null || field.get(obj1) == null) && (field == null || obj2 == null || field.get(obj2) == null)){
                            
                        }else{
                            differ=true;
                        }
                    }else if ( !(field.get(obj1)).equals(field.get(obj2)) ){
                        differ=true;
                    }else{
                        
                    }
                    field.setAccessible(false);
                }
            }
        }
        return differ;
    }

    /**
     * @return the wound_acquired
     */
    public Collection<WoundAcquiredVO> getWound_acquired() {
        return wound_acquired;
    }

    /**
     * @param wound_acquired the wound_acquired to set
     */
    public void setWound_acquired(Collection<WoundAcquiredVO> wound_acquired) {
        this.wound_acquired = wound_acquired;
    }
 /**
     * @return the root_caused_addressed_comments
     */
    public String getRoot_cause_addressed_comments() {
        return root_cause_addressed_comments;
    }

    /**
     * @param root_caused_addressed_comments the root_caused_addressed_comments to set
     */
    public void setRoot_cause_addressed_comments(String root_caused_addressed_comments) {
        this.root_cause_addressed_comments = root_caused_addressed_comments;
    }
        /**
     * @return the root_caused_addressed
     */
    public Integer getRoot_cause_addressed() {
        return root_cause_addressed;
    }

    /**
     * @param root_caused_addressed the root_caused_addressed to set
     */
    public void setRoot_cause_addressed(Integer root_caused_addressed) {
        this.root_cause_addressed = root_caused_addressed;
    }
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }

    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
  private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }

    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }

 
}
