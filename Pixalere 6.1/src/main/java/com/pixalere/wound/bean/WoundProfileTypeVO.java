/*
 * WoundProfileTypeVO.java
 *
 * Created on January 10, 2008, 11:08 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.wound.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;
/**
 *     This is the WoundProfileTypeVO  bean for the wound_profile_type  (DB table)
 *     Refer to {@link com.pixalere.wound.service.WoundServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     
 */
public class WoundProfileTypeVO extends ValueObject implements Serializable {
    private Integer id;
    private Integer patient_id;
    private Integer wound_id;
    private Integer active;
    private String alpha_type;
    private Integer closed;
    private Integer professional_id;
    private Date closed_date;
    /** Creates a new instance of WoundProfileTypeVO */
    public WoundProfileTypeVO() {
    }
private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
     public Integer getDeleted() {
         return deleted;
     }

     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }

     public String getDeleted_reason() {
         return deleted_reason;
     }

     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }

     public String getDelete_signature() {
         return delete_signature;
     }

     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public Integer getWound_id() {
        return wound_id;
    }

    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }

    public String getAlpha_type() {
        return alpha_type;
    }

    public void setAlpha_type(String alpha_type) {
        this.alpha_type = alpha_type;
    }

    public Integer getClosed() {
        return closed;
    }

    public void setClosed(Integer closed) {
        this.closed = closed;
    }

    public Date getClosed_date() {
        return closed_date;
    }

    public void setClosed_date(Date closed_date) {
        this.closed_date = closed_date;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }

    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }

    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }

    
}
