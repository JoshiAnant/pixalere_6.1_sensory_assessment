package com.pixalere.wound.bean;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.wound.bean.LocationImagesVO;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;
/**
 *     This is the WoundVO  bean for the wounds  (DB table)
 *     Refer to {@link com.pixalere.wound.service.WoundServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.patient.bean.PatientAccountVO
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.LocationImagesVO
 *     @has 1..1 Has 1..* com.pixalere.wound.bean.WoundProfileTypeVO
 *     @has 1..1 Has 1..* com.pixalere.assessment.bean.WoundAssessmentLocationVO
 */
public class WoundVO extends ValueObject implements Serializable {

    /** identifier field */
    private Integer id;
    private Integer patient_id;
    private Date closed_date;
    private String wound_location;
    private String wound_location_detailed;
    private String wound_location_position;
    private String image_name;
    private Integer location_image_id;
    private Date start_date;
    private String discharge;
    private String status;
    private String startdate_signature;
    private Integer active;
    private Integer professional_id;
    private WoundAssessmentLocationVO[] alphas;
    private LocationImagesVO location_image;
    private WoundProfileTypeVO[] wound_profile_types;
    /** default constructor */
    public WoundVO() {
    }
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
     public Integer getDeleted() {
         return deleted;
     }

     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }

     public String getDeleted_reason() {
         return deleted_reason;
     }

     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }

     public String getDelete_signature() {
         return delete_signature;
     }

     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    public String getStartdate_signature() {
        return this.startdate_signature;
    }
    
    public void setStartdate_signature(String startdate_signature) {
        this.startdate_signature = startdate_signature;
    }
    
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    
    public Integer getPatient_id() {
        return this.patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public Date getClosed_date() {
        return closed_date;
    }

    public void setClosed_date(Date closed_date) {
        this.closed_date = closed_date;
    }

    public String getWound_location() {
        return wound_location;
    }

    public void setWound_location(String wound_location) {
        this.wound_location = wound_location;
    }
   
    public String getWound_location_detailed() {
        return wound_location_detailed;
    }

    public void setWound_location_detailed(String wound_location_detailed) {
        this.wound_location_detailed = wound_location_detailed;
    }

    public String getWound_location_position() {
        return wound_location_position;
    }

    public void setWound_location_position(String wound_location_position) {
        this.wound_location_position = wound_location_position;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public Integer getLocation_image_id() {
        return location_image_id;
    }

    public void setLocation_image_id(Integer location_image_id) {
        this.location_image_id = location_image_id;
    }
    
    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public String getDischarge() {
        return discharge;
    }

    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WoundAssessmentLocationVO[] getAlphas() {
        return alphas;
    }

    public void setAlphas(WoundAssessmentLocationVO[] alphas) {
        this.alphas = alphas;
    }

    public LocationImagesVO getLocation_image() {
        return location_image;
    }

    public void setLocation_image(LocationImagesVO location_image) {
        this.location_image = location_image;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getProfessional_id() {
        return professional_id;
    }

    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }

    public WoundProfileTypeVO[] getWound_profile_types() {
        return wound_profile_types;
    }

    public void setWound_profile_types(WoundProfileTypeVO[] wound_profile_types) {
        this.wound_profile_types = wound_profile_types;
    }

    /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }

    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }

  

   
}
