package com.pixalere.wound.dao;
import java.util.Date;
import com.pixalere.common.*;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.guibeans.WoundProfileItem;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.bean.LocationImagesVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.utils.*;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.ojb.broker.query.ReportQueryByCriteria;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.WoundAcquiredVO;
import java.util.Collection;
import com.pixalere.utils.Common;
import java.util.Iterator;
import java.util.Vector;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  wound_profiles    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class WoundProfileDAO implements DataAccessObject {

    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(WoundProfileDAO.class);

    public WoundProfileDAO() {
    }

    /**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<WoundProfileVO> findWoundProfileFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<WoundProfileVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on", PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));

                query = QueryFactory.newQuery(WoundProfileVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }

    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findWoundProfileBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        WoundProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date != null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(WoundProfileVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (WoundProfileVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }

    /**
     * Finds all wound_profiles records by criteria.  If no record is found a null value is returned.
     *
     * @param patient_id of the wound_profile
     * @param isUploader if user is in uploader, rather then viewer (don't show healed wounds).
     * @param professional_id of current logged in user. This is for retrieveinactive records.
     * @todo needs refactorig
     * @since 3.0
     */
    public Vector getWounds(int patient_id, boolean isUploader, int professional_id,Integer language) {
        // If professional_id = 0, it will return active (saved) wound profiles only (used in Viewer, and maybe in Reports too)
        Vector woundProfiles = new Vector();
        logs.info("##$$##$$##-----Entering WoundProfileDAO.getWounds(" + patient_id + "," + isUploader + ")-----##$$##$$##");
        String locale = Common.getLanguageLocale(language);
        
        try {
            WoundVO woundVO = new WoundVO();
            woundVO.setPatient_id(new Integer(patient_id));


            Collection<WoundVO> all = findAllWoundsByCriteria(woundVO, 0);
            AssessmentServiceImpl am = new AssessmentServiceImpl(language);
            WoundAssessmentLocationServiceImpl walm = new WoundAssessmentLocationServiceImpl();
            for (WoundVO profileVO : all) {
                try {
                    WoundProfileItem tmpItem = new WoundProfileItem();
                    // Refactor this later to use a Collection sort on the objects.
                    if (profileVO.getActive().equals(new Integer(1)) || profileVO.getProfessional_id().equals(professional_id)) {
                        tmpItem.setId(profileVO.getId().intValue());
                        tmpItem.setStatus(profileVO.getStatus());

                        boolean isNew = false;
                        if (profileVO.getActive().equals(new Integer(0))) {
                            isNew = true;
                        }
                        tmpItem.setIsNew(isNew);
                        if (isUploader == false) {
                            WoundProfileTypeVO type = new WoundProfileTypeVO();
                            type.setWound_id(profileVO.getId());
                            type.setAlpha_type(Constants.WOUND_PROFILE_TYPE);
                            WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
                            wal.setWound_id(profileVO.getId());
                            wal.setActive(1);
                            WoundAssessmentLocationVO walocation = walm.getAlpha(wal, true, true, Constants.WOUND_PROFILE_TYPE);
                            if (walocation != null) {
                                Collection<WoundProfileTypeVO> col = findAllByCriteria(type, -1);
                                for (WoundProfileTypeVO t : col) {
                                    if (!t.getAlpha_type().equals("I")) {

                                        tmpItem = new WoundProfileItem();
                                        tmpItem.setDeleted(t.getDeleted());
                                        tmpItem.setId(t.getId());
                                        tmpItem.setStatus((t.getClosed().equals(new Integer(1))) ? "Closed" : "");
                                        tmpItem.setName(profileVO.getWound_location_detailed() + " - " + Common.getWoundProfileType(t.getAlpha_type(),locale));
                                        woundProfiles.add(tmpItem);
                                    }
                                }
                            }
                            type.setWound_id(profileVO.getId());
                            type.setAlpha_type(Constants.INCISIONTAG_PROFILE_TYPE);
                            walocation = walm.getAlpha(wal, true, true, Constants.INCISIONTAG_PROFILE_TYPE);
                            if (walocation != null) {
                                Collection<WoundProfileTypeVO> col = findAllByCriteria(type, -1);
                                for (WoundProfileTypeVO t : col) {
                                    if (!t.getAlpha_type().equals("I")) {

                                        tmpItem = new WoundProfileItem();
                                        tmpItem.setDeleted(t.getDeleted());
                                        tmpItem.setId(t.getId());
                                        tmpItem.setStatus((t.getClosed().equals(new Integer(1))) ? "Closed" : "");
                                        tmpItem.setName(profileVO.getWound_location_detailed() + " - " + Common.getWoundProfileType(t.getAlpha_type(),locale));
                                        woundProfiles.add(tmpItem);
                                    }
                                }
                            }
                            type.setWound_id(profileVO.getId());
                            type.setAlpha_type(Constants.TUBESANDDRAINS_PROFILE_TYPE);
                            walocation = walm.getAlpha(wal, true, true, Constants.TUBESANDDRAINS_PROFILE_TYPE);
                            if (walocation != null) {
                                Collection<WoundProfileTypeVO> col = findAllByCriteria(type, -1);
                                for (WoundProfileTypeVO t : col) {
                                    if (!t.getAlpha_type().equals("I")) {

                                        tmpItem = new WoundProfileItem();
                                        tmpItem.setDeleted(t.getDeleted());
                                        tmpItem.setId(t.getId());
                                        tmpItem.setStatus((t.getClosed().equals(new Integer(1))) ? "Closed" : "");
                                        tmpItem.setName(profileVO.getWound_location_detailed() + " - " + Common.getWoundProfileType(t.getAlpha_type(),locale));
                                        woundProfiles.add(tmpItem);
                                    }
                                }
                            }
                            type.setWound_id(profileVO.getId());
                            type.setAlpha_type(Constants.OSTOMY_PROFILE_TYPE);
                            walocation = walm.getAlpha(wal, true, true, Constants.OSTOMY_PROFILE_TYPE);
                            if (walocation != null) {
                                Collection<WoundProfileTypeVO> col = findAllByCriteria(type, -1);
                                for (WoundProfileTypeVO t : col) {
                                    if (!t.getAlpha_type().equals("I")) {
                                        tmpItem = new WoundProfileItem();
                                        tmpItem.setDeleted(t.getDeleted());
                                        tmpItem.setId(t.getId());
                                        tmpItem.setStatus((t.getClosed().equals(new Integer(1))) ? "Closed" : "");
                                        tmpItem.setName(profileVO.getWound_location_detailed() + " - " + Common.getWoundProfileType(t.getAlpha_type(),locale));
                                        woundProfiles.add(tmpItem);
                                    }
                                }
                            }
                            type.setWound_id(profileVO.getId());
                            type.setAlpha_type(Constants.BURN_PROFILE_TYPE);
                            walocation = walm.getAlpha(wal, true, true, Constants.BURN_PROFILE_TYPE);
                            if (walocation != null) {
                                Collection<WoundProfileTypeVO> col = findAllByCriteria(type, -1);
                                for (WoundProfileTypeVO t : col) {
                                    if (!t.getAlpha_type().equals("I")) {

                                        tmpItem = new WoundProfileItem();
                                        tmpItem.setDeleted(t.getDeleted());
                                        tmpItem.setId(t.getId());
                                        tmpItem.setStatus((t.getClosed().equals(new Integer(1))) ? "Closed" : "");
                                        tmpItem.setName(profileVO.getWound_location_detailed() + " - " + Common.getWoundProfileType(t.getAlpha_type(),locale));
                                        woundProfiles.add(tmpItem);
                                    }
                                }
                            }
                            type.setWound_id(profileVO.getId());
                            type.setAlpha_type(Constants.SKIN_PROFILE_TYPE);
                            walocation = walm.getAlpha(wal, true, true, Constants.SKIN_PROFILE_TYPE);
                            //if (walocation != null) {
                                Collection<WoundProfileTypeVO> col = findAllByCriteria(type, -1);
                                for (WoundProfileTypeVO t : col) {
                                    if (!t.getAlpha_type().equals("I")) {

                                        tmpItem = new WoundProfileItem();
                                        tmpItem.setDeleted(t.getDeleted());
                                        tmpItem.setId(t.getId());
                                        tmpItem.setStatus((t.getClosed().equals(new Integer(1))) ? "Closed" : "");
                                        tmpItem.setName(profileVO.getWound_location_detailed() + " - " + Common.getWoundProfileType(t.getAlpha_type(),locale));
                                        woundProfiles.add(tmpItem);
                                    }
                                }
                            //}
                        } else {
                            if (profileVO.getDeleted() == null || profileVO.getDeleted().equals(0)) {


                                tmpItem.setName(profileVO.getWound_location_detailed());
                                tmpItem.setImage_name(profileVO.getImage_name());
                                woundProfiles.add(tmpItem);
                            }
                        }
                    }

                }
                catch (NullPointerException exc) {
                    exc.printStackTrace();
                    //New woundprofile after upload...
                }

            }
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.findAllTMPByCriteria(): " + e.toString(), e);
            System.out.println("DataAccessException in WoundProfileDAO; getWounds: " + e.toString());
            e.printStackTrace();

        }
        logs.info("***********************Leaving WoundProfileDAO.getWoundProfiles()***********************");
        return woundProfiles;
        //cycle array, compare to, if equals ignore.  Return Vector of WounProfileItems.

    }

    public Vector getLocationImages(String strGender, String strView,Integer language) {
        logs.info("##$$##$$##-----Entering WoundProfileDAO.getLocationImages(" + strGender + ")-----##$$##$$##");
        Vector results = new Vector();
        try {
            ListServiceImpl listBD = new ListServiceImpl(language);
            String strMale = "";
            String strFemale = "";
            boolean blnWoundCare = Common.getConfig("allowWoundCare").equals("1");
            boolean blnStoma = Common.getConfig("allowStoma").equals("1");
            boolean blnDrain = Common.getConfig("allowDrain").equals("1");
            boolean blnIncision = Common.getConfig("allowIncision").equals("1");
            boolean blnBurn = Common.getConfig("allowBurn").equals("1");
            try {
                LookupVO m = listBD.getListItem(Integer.parseInt(Common.getConfig("male")));
                strMale = m.getName(language);

                LookupVO f = listBD.getListItem(Integer.parseInt(Common.getConfig("female")));
                strFemale = f.getName(language);

            } catch (Exception e) {
            }


            LocationImagesVO locationImagesVO = new LocationImagesVO();
            if (!strView.equals("")) {
                locationImagesVO.setView(strView);
            }
            
            Collection<LocationImagesVO> locationImages = findAllLocationImagesByCriteria(locationImagesVO);
            String locale = Common.getLanguageLocale(language);
            for (LocationImagesVO locImg : locationImages) {
                // i18n
                String localizedLocation = Common.getLocalizedString(locImg.getLocation(), locale);
                locImg.setLocation(localizedLocation);
                String localizedName = Common.getLocalizedString(locImg.getWound_name(), locale);
                locImg.setWound_name(localizedName);
                
                if (((blnWoundCare && locImg.getWoundcare() == 1) || (blnStoma && locImg.getStoma() == 1)
                        || (blnDrain && locImg.getDrain() == 1) || (blnBurn && locImg.getBurn() == 1) || (blnIncision && locImg.getIncision() == 1)) && ((locImg.getGender().indexOf("B") != -1) || (strGender == "B") || (locImg.getGender().indexOf(strGender) != -1))) {

                    if (strGender == "B") {
                        if (locImg.getGender().indexOf("M") != -1) {
                            locImg.setLocation(locImg.getLocation() + " (" + strMale + ")");
                        }
                        if (locImg.getGender().indexOf("F") != -1) {
                            locImg.setLocation(locImg.getLocation() + " (" + strFemale + ")");
                        }
                    }

                    results.add(locImg);
                }
            }

        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.getLocationImages: " + e.toString(), e);

        }
        logs.info("***********************Leaving WoundProfileDAO.getLocationImages()***********************");
        return results;

    }

    /**
     * Finds a all wound_profiles records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(WoundProfileVO woundVO, int limit) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        WoundProfileVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundProfileVO();
            if (!Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }

            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("created_on");

            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds a all wound_profile_type records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param woundProfileTypeVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(WoundProfileTypeVO woundVO, int limit) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        WoundProfileTypeVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundProfileTypeVO();
            if (!Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");

            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds a all wounds records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public Collection findAllWoundsByCriteria(WoundVO woundVO, int limit) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        WoundVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundVO();
            Criteria crit = new Criteria();
                QueryByCriteria query = null;
                if(woundVO.getActive()!=null){
                    crit.addEqualTo("active", woundVO.getActive());
                }
                if (!Common.getConfig("showDeleted").equals("1")) {
                    crit.addEqualTo("deleted", new Integer(0));
                }
                crit.addEqualTo("patient_id", woundVO.getPatient_id());
                if(woundVO.getStatus() != null && woundVO.getStatus().equals("")){
                    //System.out.println("NOT EQUALS CLOSED");
                    crit.addIsNull("status");//,"Closed");
                }else if(woundVO.getStatus() != null && woundVO.getStatus().equals("Closed")){
                    crit.addEqualTo("status","Closed");
                }
            
            
            query = QueryFactory.newQuery(WoundVO.class, crit);
            query.addOrderByDescending("id");

            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
            //System.out.println("RESULTS "+results.size());
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds an wound_profiles record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllLocationImagesByCriteria(LocationImagesVO locationImagesVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();

            QueryByCriteria query = new QueryByCriteria(locationImagesVO);
            query.addOrderByAscending("orderby");

            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return results;
    }

    public ValueObject findByCriteria(WoundProfileTypeVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;

        WoundProfileTypeVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            QueryByCriteria query = new QueryByCriteria(woundVO);

            query.addOrderByDescending("id");
            returnVO = (WoundProfileTypeVO) broker.getObjectByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }

    public ValueObject findByCriteria(WoundProfileVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;

        WoundProfileVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            woundVO.setDeleted(0);
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (WoundProfileVO) broker.getObjectByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }

    public ValueObject findByCriteria(WoundProfileVO woundVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;

        WoundProfileVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && !Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (WoundProfileVO) broker.getObjectByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Finds an wound record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public ArrayValueObject findEtiologyByCriteria(EtiologyVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;

        EtiologyVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();


            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (EtiologyVO) broker.getObjectByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }
/**
     * Finds an wound record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public ArrayValueObject findAcquiredByCriteria(WoundAcquiredVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;

        WoundAcquiredVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();


            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (WoundAcquiredVO) broker.getObjectByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an wound record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public ArrayValueObject findGoalByCriteria(GoalsVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;

        GoalsVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();


            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (GoalsVO) broker.getObjectByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Finds an wound record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public ValueObject findWoundByCriteria(WoundVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;

        WoundVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            woundVO.setDeleted(0);
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (WoundVO) broker.getObjectByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Finds a single wound_profiles record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the wound_profiles table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findById(int id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        WoundProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            patientVO = new WoundProfileVO();
            patientVO.setId(new Integer(id));

            Query query = new QueryByCriteria(patientVO);
            patientVO = (WoundProfileVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }

    /**
     * Finds a single wound_profiles record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id of the Wound_profile
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int wound_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        WoundProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            patientVO = new WoundProfileVO();
            patientVO.setWound_id(new Integer(wound_id));
            patientVO.setCurrent_flag(new Integer(1));
            patientVO.setActive(new Integer(1));
            Query query = new QueryByCriteria(patientVO);
            patientVO = (WoundProfileVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }

    /**
     * Finds a single wound_profile_tmp record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param wound_id of the Wound_profile
     * @param professional_id of teh current logged in user
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findTMPByPK(String wound_id, int professional_id) throws DataAccessException {
        logs.info("********* Entering the WoundProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        WoundProfileVO woundVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            woundVO = new WoundProfileVO();
            if (wound_id != null) {
                woundVO.setWound_id(new Integer(wound_id));
                woundVO.setProfessional_id(new Integer(professional_id));
                woundVO.setActive(new Integer(0));
                Query query = new QueryByCriteria(woundVO);
                woundVO = (WoundProfileVO) broker.getObjectByQuery(query);

            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in woundProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in woundProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the woundProfileDAO.findByPK");

        return woundVO;
    }

    /**
     * Inserts a single woundVO) object into the wounds table.  The value object passed
     * in has to be of type WoundVO.
     * @return int the wound_id of the newly created wound.
     * @param insertRecord fields in ValueObject represent fields in the wounds record.
     *
     * @since 3.0
     */
    public int insertNewWound(WoundVO wound) {
        logs.info("************Entering WoundProfileDAO.insert()************");
        PersistenceBroker broker = null;
        WoundVO returnVO = null;
        try {
            if(wound!=null && wound.getDeleted() == null){
                wound.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(wound);
            broker.commitTransaction();
            WoundVO tmp = new WoundVO();
            tmp.setPatient_id(wound.getPatient_id());

            QueryByCriteria query = new QueryByCriteria(tmp);
            query.addOrderByDescending("id");
            returnVO = (WoundVO) broker.getObjectByQuery(query);

        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundtProfileDAO.insert(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insert()*************");
        if(returnVO!=null){
            return returnVO.getId();
        }else{ return -1;}

    }
 public void dropWoundProfileTypeOffline(WoundProfileTypeVO delete) {
        logs.info("************Entering WoundProfileDAO.deleteTMPByObject()************");
        PersistenceBroker broker = null;
        try {
            
            Collection result = findAllByCriteria(((WoundProfileTypeVO) delete), 0);
            Iterator iter = result.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                WoundProfileTypeVO tmpResultVO = (WoundProfileTypeVO) iter.next();


                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.deleteTMPByObject(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.deleteTMPByObject(): " + e.toString(), e);

        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMPByObject(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.delete()*************");
    }
    /**
     * Inserts a single woundVO) object into the wounds table.  The value object passed
     * in has to be of type WoundVO.
     * @return int the wound_id of the newly created wound.
     * @param insertRecord fields in ValueObject represent fields in the wounds record.
     *
     * @since 3.0
     */
    public void saveWound(WoundVO wound) {
        logs.info("************Entering WoundProfileDAO.insert()************");
        PersistenceBroker broker = null;
        WoundVO returnVO = null;
        try {
            if(wound!=null && wound.getDeleted() == null){
                wound.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(wound);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundtProfileDAO.insert(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insert()*************");



    }

    public void insertLocationImage(LocationImagesVO img) {
        logs.info("************Entering WoundProfileDAO.insert()************");
        PersistenceBroker broker = null;

        try {

            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(img);
            broker.commitTransaction();

        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundtProfileDAO.insert(): " + e.toString(), e);

        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insert()*************");



    }

    /**
     * Inserts a single woundProfileTypeVO) object into the wound_Profile_type table.  The value object passed
     * in has to be of type WoundProfileTypeVO.
     * @param insertRecord fields in ValueObject represent fields in the wounds record.
     *
     * @since 3.0
     */
    public void saveWoundProfileType(WoundProfileTypeVO wound) {
        PersistenceBroker broker = null;
        WoundProfileTypeVO returnVO = null;
        try {
            if(wound!=null && wound.getDeleted() == null){
                wound.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(wound);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundtProfileDAO.insert(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insert()*************");



    }

    /**
     * Inserts a single WoundProfilesVO object into the wound_profiles table.  The value object passed
     * in has to be of type WoundProfilesVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the wound_profiles record.
     *
     * @since 3.0
     */
    public void update(ValueObject insert) throws DataAccessException {
        logs.info("************Entering WoundProfileDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            WoundProfileVO patient = (WoundProfileVO) insert;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            WoundProfileVO vo = new WoundProfileVO();
            vo.setWound_id(patient.getWound_id());
            vo.setCurrent_flag(new Integer("1"));
            Collection<WoundProfileVO> wounds = findAllByCriteria(vo, 0);
            Iterator iter = wounds.iterator();
            broker = ConnectionLocator.getInstance().findBroker();
            for (WoundProfileVO currentWound : wounds) {
                currentWound.setCurrent_flag(new Integer("0"));
                broker.beginTransaction();
                broker.store(currentWound);
                broker.commitTransaction();
                
            }
            WoundProfileVO duplicate = (WoundProfileVO) findByCriteria((WoundProfileVO) insert);
            if (duplicate == null) {
                patient.setCurrent_flag(new Integer(1));
                broker.beginTransaction();
                broker.store(patient);
                broker.commitTransaction();
                
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundtProfileDAO.insert(): " + e.toString(), e);

        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with woundProfileDAO.insert()*************");


    }

    /**
     * Inserts a single WoundProfilesVO object into the wound_profiles table.  The value object passed
     * in has to be of type WoundProfilesVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the wound_profiles record.
     *
     * @since 3.0
     */
    public void insert(ValueObject update) throws DataAccessException {
        logs.info("************Entering WoundProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            WoundProfileVO patient = (WoundProfileVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            int deleted = (patient.getDeleted()==null?0:patient.getDeleted());
            broker = ConnectionLocator.getInstance().findBroker();
            WoundProfileVO duplicate = (WoundProfileVO) findByCriteria((WoundProfileVO) update);
            if (duplicate == null) {
                patient.setDeleted(deleted);
                broker.beginTransaction();
                broker.store(patient);
                broker.commitTransaction();
                
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.update()*************");


    }

    public void deleteByPatient(String patient_id) {
        logs.info("************Entering WoundProfileDAO.deleteTMP()************");
        WoundProfileVO tmpVO = new WoundProfileVO();
        tmpVO.setPatient_id(new Integer(patient_id));
        tmpVO.setActive(new Integer(0));
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();


            broker.beginTransaction();
            broker.delete(tmpVO);
            broker.commitTransaction();
            

        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.deleteTMP()*************");

    }

    public void deleteTMPByObject(WoundProfileVO delete) {
        logs.info("************Entering WoundProfileDAO.deleteTMPByObject()************");
        PersistenceBroker broker = null;
        try {
            //Integer wound_id=((WoundProfileVO)delete).getId();
            delete.setActive(new Integer(0));//ensure it only deletes tmp records

            Collection result = findAllByCriteria(((WoundProfileVO) delete), 0);
            Iterator iter = result.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                WoundProfileVO tmpResultVO = (WoundProfileVO) iter.next();
                //tmpResultVO.setId(wound_id);
                WoundProfileTypeVO wpt = new WoundProfileTypeVO();
                wpt.setPatient_id(tmpResultVO.getPatient_id());
                wpt.setWound_id(tmpResultVO.getWound_id());

                broker.beginTransaction();
                broker.delete(wpt);
                broker.delete(tmpResultVO);
                broker.commitTransaction();

            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.deleteTMPByObject(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.deleteTMPByObject(): " + e.toString(), e);

        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.delete()*************");
    }

    public Collection findAllWoundsByCriteria(WoundVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        WoundVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundVO();
            if (!Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");

            results = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return results;
    }

    public List<Integer> deleteWounds(ValueObject obj, Date purge_days) {
        logs.info("************Entering WoundProfileDAO.deleteWounds()************");
        WoundVO tmpVO = (WoundVO) obj;
        List<Integer> deleted_ids = new ArrayList<Integer>();
        PersistenceBroker broker = null;
        try {
            Collection<WoundVO> result = findAllWoundsByCriteria(tmpVO);
            broker = ConnectionLocator.getInstance().findBroker();
            for (WoundVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getStart_date();
                

                if (purge_days == null || last_update.before(purge_days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                   
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);

        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;

    }

    public void deleteTMP(String patient_id, int professional_id) {
        logs.info("************Entering WoundProfileDAO.deleteTMP()************");
        WoundProfileVO tmpVO = new WoundProfileVO();
        tmpVO.setPatient_id(new Integer(patient_id));
        tmpVO.setProfessional_id(new Integer(professional_id));
        tmpVO.setActive(new Integer(0));
        PersistenceBroker broker = null;
        try {
            Collection<WoundProfileVO> result = findAllByCriteria(tmpVO, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (WoundProfileVO tmpResultVO : result) {
                WoundProfileTypeVO wpt = new WoundProfileTypeVO();
                wpt.setPatient_id(tmpResultVO.getPatient_id());
                wpt.setWound_id(tmpResultVO.getWound_id());
                //first delete any etiologies/goals assigned to it
                //Delete Etiology / Goals
                EtiologyVO etiology = new EtiologyVO();
                etiology.setWound_profile_id(wpt.getId());
                deleteEtiology(etiology);
                GoalsVO goals = new GoalsVO();
                goals.setWound_profile_id(wpt.getId());
                deleteGoal(goals);
                WoundAcquiredVO wa = new WoundAcquiredVO();
                wa.setWound_profile_id(wpt.getId());
                deleteAcquired(wa);
                
                //wpt.setActive(0);
                //Collection<WoundProfileTypeVO> result2 = findAllByCriteria(wpt,0);
                //for(WoundProfileTypeVO w : result2){

                //broker.beginTransaction();
                //broker.delete(w);
                //broker.commitTransaction();
                //}
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();


            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);

        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.deleteTMP()*************");

    }

    public Vector getAllWoundProfileSignatures(int wound_id, int limit_start) throws DataAccessException {

        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("wound_id", wound_id + "");
            crit.addEqualTo("active", 1);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(WoundProfileVO.class, crit);
            q.addOrderByDescending("id");
            q.setStartAtIndex(limit_start);
            //q.setEndAtIndex(limit);
            q.setAttributes(new String[]{"user_signature", "id"});

            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in WoundProfileVO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in WoundProfileVO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        return v;

    }

    public List<Integer> delete(ValueObject delete, Date purge_days) {
        logs.info("************Entering WoundProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            //Integer wound_id=((WoundProfileVO)delete).getId();
            Collection<WoundProfileVO> result = findAllByCriteria(((WoundProfileVO) delete), 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (WoundProfileVO tmpResultVO : result) {
                
                WoundProfileTypeVO wpt = new WoundProfileTypeVO();
                wpt.setPatient_id(tmpResultVO.getPatient_id());
                wpt.setWound_id(tmpResultVO.getWound_id());
                //wpt.setActive(0);
               /*Collection<WoundProfileTypeVO> result2 = findAllByCriteria(wpt,0);
                for(WoundProfileTypeVO w : result2){
                broker.beginTransaction();
                broker.delete(w);
                broker.commitTransaction();
                }*/
                boolean nfe = false;
                Date last_update = null;
                if (tmpResultVO.getCreated_on()== null) {
                    nfe = true;
                } else {
                    last_update = tmpResultVO.getCreated_on();
                }
                //int days = Integer.parseInt(purge_days + "");
                if (purge_days == null || (nfe == false && last_update.before(purge_days))) {
                    GoalsVO g = new GoalsVO();
                    g.setWound_profile_id(tmpResultVO.getId());
                    deleteGoal(g);
                    EtiologyVO et = new EtiologyVO();
                    et.setWound_profile_id(tmpResultVO.getId());
                    deleteEtiology(et);
                    WoundAcquiredVO at = new WoundAcquiredVO();
                    at.setWound_profile_id(tmpResultVO.getId());
                    deleteAcquired(at);
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);

                    broker.commitTransaction();

                    deleted_ids.add(tmpResultVO.getId());
                    
                }
            }
        } catch (PersistenceBrokerException e) {
            if (broker != null) {
                broker.abortTransaction();
            }
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.delete(): " + e.toString(), e);

        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;


    }

    public int[] getWoundCount(int treatment_id) throws com.pixalere.common.DataAccessException {
        PersistenceBroker broker = null;
        int[] countArray = {0, 0, 0, 0};
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            com.pixalere.patient.dao.PatientDAO bd = new com.pixalere.patient.dao.PatientDAO();
            PatientAccountVO vo = new PatientAccountVO();
            vo.setTreatment_location_id(Integer.parseInt(treatment_id + ""));
            vo.setCurrent_flag(new Integer(1));
            Collection<PatientAccountVO> patients = bd.findAllByCriteria(vo, true);
            for (PatientAccountVO p : patients) {
                countArray[3] = (p.getVisits() == null ? 0 : p.getVisits().intValue());
                WoundProfileVO wp = new WoundProfileVO();
                wp.setPatient_id(p.getPatient_id());
                wp.setCurrent_flag(new Integer(1));
                wp.getWound().setStatus("Closed");
                QueryByCriteria query = new QueryByCriteria(wp);
                countArray[2] = broker.getCount(query);
                Criteria crit = new Criteria();
                QueryByCriteria query2 = null;
                crit.addNotEqualTo("status", "Closed");
                crit.addEqualTo("patient_id", p.getPatient_id());
                crit.addEqualTo("current", new Integer(1));
                query2 = QueryFactory.newQuery(WoundProfileVO.class, crit);

                countArray[1] = broker.getCount(query2);
            }
            countArray[0] = countArray[1] + countArray[2];
            return countArray;
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
    }

    public int getPressureCount(int treatment_id) throws com.pixalere.common.DataAccessException {
        PersistenceBroker broker = null;
        int count = 0;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            com.pixalere.patient.dao.PatientDAO bd = new com.pixalere.patient.dao.PatientDAO();
            PatientAccountVO vo = new PatientAccountVO();
            vo.setTreatment_location_id(Integer.parseInt(treatment_id + ""));
            vo.setCurrent_flag(new Integer(1));
            Collection<PatientAccountVO> patients = bd.findAllByCriteria(vo, true);
            for (PatientAccountVO p : patients) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                //crit.addNotEqualTo("status", "Closed");
                crit.addEqualTo("patient_id", p.getPatient_id());
                crit.addEqualTo("current", new Integer(1));
                crit.addLike("etiology", "Pressure");
                query = QueryFactory.newQuery(WoundProfileVO.class, crit);

                int c = broker.getCount(query);
                if (c > 0) {
                    count++;
                }
            }

            return count;
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
    }

    /**
     * PlaceHolder
     *
     */
    public void delete(ValueObject delete) {
        PersistenceBroker broker = null;
        try {
            // NOT Used
        } catch (Exception ex) {

            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");


    }

    public Vector findAllWoundBySearch() throws DataAccessException {
        System.out.println("in findAllWoundBySearch in woundprofiledao");
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //crit.addEqualTo("patient_id", new Integer(patient_id));
            //crit.addEqualTo("wound_id", new Integer(wound_id));
            //crit.addEqualTo("visit", 1);

            //crit.addNotEqualTo("status", "Closed");

            ReportQueryByCriteria q = QueryFactory.newReportQuery(WoundVO.class, crit);
            q.setAttributes(new String[]{"id", "active", "patient_id", "wound_location_detailed", "status"});
            q.addOrderByDescending("patient_id");
            q.addOrderByDescending("id");
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            System.out.println("Exception in ReportDAO: " + e);
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        return v;
    }

    public Vector findAllWoundProfilesBySearch() throws DataAccessException {
        System.out.println("in findAllWoundProfilesBySearch in woundprofiledao");
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //crit.addEqualTo("patient_id", new Integer(patient_id));
            //crit.addEqualTo("wound_id", new Integer(wound_id));
            //crit.addEqualTo("visit", 1);

            crit.addEqualTo("current_flag", "1");

            ReportQueryByCriteria q = QueryFactory.newReportQuery(WoundProfileVO.class, crit);
            q.setAttributes(new String[]{"id", "patient_id", "wound_id", "goals"});
            q.addOrderByDescending("patient_id");
            q.addOrderByDescending("wound_id");
            q.addOrderByDescending("id");
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            System.out.println("Exception in ReportDAO: " + e);
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        return v;
    }

    /*public List<WoundVO> findAllWoundProfileTypesBySearch(int patient_id, int care_type_status, String[] caretypes, int wound_goal, int ostomy_goal, int tubedrain_goal, int incision_goal) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        List<WoundVO> wounds = new ArrayList();
        WoundServiceImpl wservice = new WoundServiceImpl();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //get all care types for this patient_id
            crit.addEqualTo("patient_id", new Integer(patient_id));
            //crit.addEqualTo("active","1");
            Criteria c = new Criteria();
            if (caretypes != null) {
                //retrieve all care_types allowed based on criteria (all or specifics)
                for (String caretype : caretypes) {
                    //if All isn't select, get specific care types, if all, no need for criteria
                    //commented out as it returns no rows if it doesn't match the criteria... but 
                    //we only assume the patient doesn't have any wounds rather
                    //then it was filtered out... we need to filter out the patient as well. Filtering will occur later
                    if (!caretype.equals("all")) {
                        //Criteria cc = new Criteria();
                        //cc.addEqualTo("alpha_type", caretype);
                        //c.addOrCriteria(cc);
                    }
                }
                if (!Arrays.asList(caretypes).contains("all")) {
                    //crit.addAndCriteria(c);
                }
            }
            if (care_type_status != -1) {
                //crit.addEqualTo("closed", care_type_status);
            }
            ReportQueryByCriteria q = QueryFactory.newReportQuery(WoundProfileTypeVO.class, crit,true);
            q.setAttributes(new String[]{"wound_id"});
            q.addOrderByDescending("wound_id");
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                Object[] obj = (Object[])iter.next();
                Integer wound_id = (Integer)obj[0];
                WoundVO wound = wservice.getWound(wound_id);
                if(wound!=null)wounds.add(wound);
            }
        } catch (ConnectionLocatorException e) {
            System.out.println("Exception in ReportDAO: " + e);
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        }catch (ApplicationException e) {
            System.out.println("Exception in ReportDAO: " + e);
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return wounds;
    }*/


    /**
     * Deletes a single EtiologyVO object into the etiology table.  The value object passed
     * in has to be of type EtiologyVO.
     *
     * @param delete fields in ValueObject represent fields in the investigations record.
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deleteEtiology(EtiologyVO delete) {
        logs.info("************Entering patientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            EtiologyVO s = (EtiologyVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getWound_profile_id() != null)) {
                Collection<EtiologyVO> result = findAllByCriteria(s);


                if (result != null) {
                    for (EtiologyVO prodResultVO : result) {
                        broker.beginTransaction();
                        broker.delete(prodResultVO);
                        broker.commitTransaction();
                        
                    }
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.delete()*************");
    }
/**
     * Deletes a single WoundAcquiredVO object into the wound_acquired table.  The value object passed
     * in has to be of type WoundAcquiredVO.
     *
     * @param delete fields in ValueObject represent fields in the investigations record.
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deleteAcquired(WoundAcquiredVO delete) {
        logs.info("************Entering patientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            WoundAcquiredVO s = (WoundAcquiredVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getWound_profile_id() != null)) {
                Collection<WoundAcquiredVO> result = findAllByCriteria(s);


                if (result != null) {
                    for (WoundAcquiredVO prodResultVO : result) {
                        broker.beginTransaction();
                        broker.delete(prodResultVO);
                        broker.commitTransaction();

                    }
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.delete()*************");
    }
    /**
     * Updates a single EtiologyVO object into the etiology table.  The value object passed
     * in has to be of type EtiologyVO.
     *
     * @param update fields in ValueObject represent fields in the patient_profiles_arrays record.
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 4.1.2
     */
    public void saveEtiology(EtiologyVO update) throws DataAccessException {
        logs.info("************Entering patientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            EtiologyVO assessment = (EtiologyVO) update;


            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            throw new DataAccessException("patientProfileDAO throwing SQLException: " + e.getMessage());


        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.update()*************");


    }
/**
     * Updates a single WoundAcquiredVO object into the wound_acquired table.  The value object passed
     * in has to be of type WoundAcquiredVO.
     *
     * @param update fields in ValueObject represent fields in the patient_profiles_arrays record.
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 4.1.2
     */
    public void saveAcquired(WoundAcquiredVO update) throws DataAccessException {
        logs.info("************Entering patientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            WoundAcquiredVO assessment = (WoundAcquiredVO) update;


            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();

        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            throw new DataAccessException("patientProfileDAO throwing SQLException: " + e.getMessage());


        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.update()*************");


    }
    public Collection findAllByCriteria(EtiologyVO crit) throws
            DataAccessException {
        logs.info("***********************Entering patientProfileDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundProfileDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving patientProfileDAO.findAllByCriteria()***********************");
        return results;
    }
    public Collection findAllByCriteria(WoundAcquiredVO crit) throws
            DataAccessException {
        logs.info("***********************Entering patientProfileDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundProfileDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving patientProfileDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Deletes a single GoalsVO object into the goals table.  The value object passed
     * in has to be of type GoalsVO.
     *
     * @param delete fields in ValueObject represent fields in the investigations record.
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deleteGoal(GoalsVO delete) {
        logs.info("************Entering patientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            GoalsVO s = (GoalsVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getWound_profile_id() != null)) {
                Collection<GoalsVO> result = findAllByCriteria(s);


                if (result != null) {
                    for (GoalsVO prodResultVO : result) {

                        broker.beginTransaction();
                        broker.delete(prodResultVO);
                        broker.commitTransaction();
                        
                    }
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.delete()*************");
    }

    /**
     * Updates a single GoalsVO object into the goals table.  The value object passed
     * in has to be of type GoalsVO.
     *
     * @param update fields in ValueObject represent fields in the patient_profiles_arrays record.
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 4.1.2
     */
    public void saveGoal(GoalsVO update) throws DataAccessException {
        logs.info("************Entering patientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            GoalsVO assessment = (GoalsVO) update;


            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            throw new DataAccessException("patientProfileDAO throwing SQLException: " + e.getMessage());


        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);

        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.update()*************");


    }

    public Collection findAllByCriteria(GoalsVO crit) throws
            DataAccessException {
        logs.info("***********************Entering patientProfileDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundProfileDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        logs.info("***********************Leaving patientProfileDAO.findAllByCriteria()***********************");
        return results;
    }
}
