package com.pixalere.wound.service;

import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.wound.bean.LocationImagesVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;
import com.pixalere.wound.dao.*;
import com.pixalere.utils.*;
import com.pixalere.common.*;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.WoundAcquiredVO;
import java.util.Collection;
import com.pixalere.integration.dao.IntegrationDAO;
import com.pixalere.integration.bean.EventVO;

/**
 * This is the wound profile service implementation Refer to {@link com.pixalere.wound.service.WoundProfileService
 * } to see if there are any web services available.
 *
 * <img src="WoundServiceImpl.png"/>
 *
 *
 * @view WoundServiceImpl
 *
 * @match class com.pixalere.wound.
 *
 * @opt hide
 * @match class
 * com.pixalere.wound\.(dao.WoundProfileDAO|bean.WoundProfileVO|bean.WoundVO|service.WoundService)
 * @opt !hide
 *
 */
public class WoundServiceImpl implements WoundService {
    // Create Log4j category instance for logging

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    private WoundProfileDAO dao = null;
    private Integer language = 1;

    public WoundServiceImpl(Integer language) {
        this.language = language;
        dao = new WoundProfileDAO();
    }

    public WoundServiceImpl() {
        dao = new WoundProfileDAO();
    }

    /**
     * Get all wound profiles since a date.
     *
     * @param patient_id
     * @param date
     * @param professional_id
     * @return wound_profiles
     * @throws ApplicationException
     */
    public Collection<WoundProfileVO> getWoundProfileFromDate(int patient_id, Date date, int professional_id) throws ApplicationException {
        try {
            WoundProfileDAO patientDAO = new WoundProfileDAO();
            return patientDAO.findWoundProfileFromDate(patient_id, date, professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: " + e.getMessage());
        }
    }

    /**
     * Get a wound profile before a date
     *
     * @param patient_id
     * @param date
     * @return wound profile
     * @throws ApplicationException
     */
    public WoundProfileVO getWoundProfileBeforeDate(int patient_id, Date date) throws ApplicationException {
        try {
            WoundProfileVO tmp = (WoundProfileVO) dao.findWoundProfileBeforeDate(patient_id, date);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }

    /**
     * Get the wound.id of the newly created wound profile
     *
     * @param wound
     * @return wound.id
     */
    public int getWoundId(WoundVO wound) {
        if (wound != null && wound.getDeleted() == null) {
            wound.setDeleted(0);
        }
        int int_wound = dao.insertNewWound(wound);
        WoundVO newwound = new WoundVO();
        newwound.setId(int_wound);
        try {
            com.pixalere.admin.service.LogAuditServiceImpl bd = new com.pixalere.admin.service.LogAuditServiceImpl();
            bd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(wound.getProfessional_id(), wound.getPatient_id(), com.pixalere.utils.Constants.CREATING_RECORD, "wounds", newwound));
        } catch (Exception e) {
        }
        return int_wound;
    }

    /**
     * Check if a temporary wound profile exists by a certain professional.
     *
     * @param profile
     * @param professional_id
     * @return true if a wound profile exists
     * @throws ApplicationException
     */
    public boolean doesExist(WoundProfileVO profile, int professional_id) throws ApplicationException {
        boolean exists = false;
        try {

            WoundProfileVO wProfile = (WoundProfileVO) dao.findByCriteria(profile);
            if (wProfile != null && wProfile.getProfessional_id().intValue() == professional_id) {
                exists = true;
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
        return exists;
    }

    /**
     * Get a list of wound_profile_types
     *
     * @param crit
     * @param limit
     * @return collection of wound_profile_types
     * @throws ApplicationException
     */
    public Collection<WoundProfileTypeVO> getWoundProfileTypes(WoundProfileTypeVO crit, int limit) throws ApplicationException {
        Collection<WoundProfileTypeVO> results = null;
        try {
            results = dao.findAllByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
        return results;
    }

    /**
     * Get a list of wound_profiles
     *
     * @param crit
     * @param limit
     * @return collection of wound_profiles
     * @throws ApplicationException
     */
    public Collection<WoundProfileVO> getWoundProfiles(WoundProfileVO crit, int limit) throws ApplicationException {
        Collection<WoundProfileVO> results = null;
        try {
            results = dao.findAllByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
        return results;
    }

    public void mergeWoundProfiles(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        WoundProfileVO i = new WoundProfileVO();
        IntegrationDAO integrationDAO = new IntegrationDAO();
        i.setPatient_id(patient_from);
        Collection<WoundProfileVO> many = getWoundProfiles(i, -1);
        for (WoundProfileVO a : many) {
            a.setPatient_id(patient_to);
            saveWoundProfile(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("wound_profiles");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
            WoundVO w = a.getWound();
            w.setPatient_id(patient_to);
            saveWound(w);

            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("wounds");
            event.setRow_id(w.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Get all active and inactive open wound profiles. This method will grab
     * all all active open wound profiles regardless of who did them, and all
     * inactive (not committed yet) wound profiles dont by the professional_id
     * passed in.
     *
     * @param patient_id
     * @param isUploader
     * @param professional_id
     * @return
     */
    public Vector getWoundProfilesDropdown(int patient_id, boolean isUploader, int professional_id) {

        try {
            return dao.getWounds(patient_id, isUploader, professional_id, language);

        } catch (Exception e) {
            log.error("An application exception has been raised in WoundManagerBD.perform(): " + e.toString());
            Vector results = new Vector();

            return results;
        }

    }

    public void mergeWoundProfileTypes(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        WoundProfileTypeVO i = new WoundProfileTypeVO();
        IntegrationDAO integrationDAO = new IntegrationDAO();
        i.setPatient_id(patient_from);
        Collection<WoundProfileTypeVO> many = getWoundProfileTypes(i, -1);
        for (WoundProfileTypeVO a : many) {
            a.setPatient_id(patient_to);
            saveWoundProfileType(a);

            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("wound_profile_type");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void dropWoundProfileTypeOffline(WoundProfileTypeVO wp) {

        dao.dropWoundProfileTypeOffline(wp);
    }
    /*
     * Get All Wound Profiles for Flowchart. Format and design based on components table results.  Components table
     * is used to state what fields, type of fields, and where they display on each given page.
     * This is our earlier attempt at getting Pixalere automated. We're half way there.
     * @param wound_id the wounds.id
     * @param rec_id the wound_profiles.id
     * @param database the database we should call
     * @param limit how many we want
     * @param id the wound_profiles.id
     * @param ProfessionalVO the professional logged in
     * @param blnEmbedCode shoudl we include the html in the result.
     * @return rowdata the array of rows/columns to dispaly in the flowcharts.
     */

    public RowData[] getAllWoundProfilesForFlowchart(Collection<WoundProfileVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode,boolean showNursingFixes) throws ApplicationException {
        RowData[] rows = null;

        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);

        String locale = Common.getLanguageLocale(language);
        
        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.WOUND_PROFILE);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        //Temporary until removes hard coded in JS/prob move to db later or use Constants
        String WND = "0";
        String OST = "1";
        String profile_title = "Wound Profile";

        for (ComponentsVO component : components) {
            if (component.getHide_flowchart() == 0) {
                String title = Common.getLocalizedString(component.getLabel_key(), locale);
                int extend_count = 0;
                int count = 0;
                for (WoundProfileVO wound : results) {

                    WoundVO w = wound.getWound();

                    ProfessionalVO userVO = null;
                    userVO = wound.getProfessional();
                    if (cnt == 2) {
                        List<FieldValues> fields = new ArrayList();
                        FieldValues f = new FieldValues();
                        f.setTitle("id");
                        String valuet2 = wound.getId() + "";
                        f.setValue(valuet2);

                        fields.add(f);
                        FieldValues f2 = new FieldValues();
                        f2.setTitle(profile_title);
                        String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + wound.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (wound.getId() != null ? Common.showNursingFixes(am.getNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", "backdate"), blnEmbedCode, locale) : "");
                        f2.setValue(valuet22);

                        fields.add(f2);
                        rows[count] = new RowData();
                        rows[count].setUser_signature(wound.getUser_signature());
                        rows[count].setId(wound.getId() != null ? wound.getId().intValue() : 0);
                        rows[count].setFields(fields);
                        rows[count].setHeader("pixalere.woundprofile.form.pageheader");
                        rows[count].setDeleted(wound.getDeleted());
                        rows[count].setDelete_signature(wound.getDelete_signature());
                        rows[count].setDelete_reason(wound.getDeleted_reason());
                    }
                    List<FieldValues> fields = rows[count].getFields();
                    //Get all columns for each component
                    String value = "";
                    if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.WOUND))) {

                        FieldValues field = new FieldValues();
                        Hashtable v = Common.getPrintableValue(w, userVO, component, blnEmbedCode, count, language);
                        Vector va = (Vector) v.get("value");
                        value = (String) va.get(0);
                        field.setDb_value((String) v.get("db_value"));
                        value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(w.getId() != null ? w.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale):"");
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ETIOLOGY_SECTION)) {
                        FieldValues field = new FieldValues();
                        List<String> list_items = new ArrayList();
                        Collection<EtiologyVO> arrays = wound.getEtiologies();
                        if (arrays != null) {
                            for (EtiologyVO array : arrays) {

                                list_items.add(array.getPrintable(language));
                            }
                        }

                        value = Serialize.serialize(list_items, ",");
                        field.setDb_value(value);
                        value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale):"");
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ACQUIRED_SECTION)) {

                        FieldValues field = new FieldValues();
                        List<String> list_items = new ArrayList();
                        Collection<WoundAcquiredVO> arrays = wound.getWound_acquired();
                        if (arrays != null) {
                            for (WoundAcquiredVO array : arrays) {
                                list_items.add(array.getPrintable(language));
                            }
                        }

                        value = Serialize.serialize(list_items, ",");
                        field.setDb_value(value);
                        value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale):"");
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.GOALS_SECTION)) {
                        FieldValues field = new FieldValues();
                        List<String> list_items = new ArrayList();
                        Collection<GoalsVO> arrays = wound.getGoals();
                        if (arrays != null) {
                            for (GoalsVO array : arrays) {
                                list_items.add(array.getPrintable(language));
                            }
                        }

                        value = Serialize.serialize(list_items, ",");
                        field.setDb_value(value);
                        value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale):"");
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);

                    } else {
                        FieldValues field = new FieldValues();
                        Hashtable v = Common.getPrintableValue(wound, userVO, component, blnEmbedCode, count, language);
                        Vector va = (Vector) v.get("value");
                        if (va.size() > 0) {
                            value = (String) va.get(0);
                        }
                        field.setDb_value((String) v.get("db_value"));
                        value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale):"");
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() == true ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    }
                    rows[count].setFields(fields);
                    count++;
                }
                cnt++;
                cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }

        return rows;

    }
    /*
     * getFlowchartRow returns an entire row for the fl owchart (all 3 columns).
     * This allows us to dynamically create the flowcharts.
     */

    private Vector getFlowchartRow(int column, int row, String value, Vector vector) throws ApplicationException {
        if (value == null) {
            value = "";
        }
        if (column == 0) {
            Hashtable t = new Hashtable();
            t.put("title", value);
            vector.add(row, t);
        } else if (column > 0) {
            Hashtable t = (Hashtable) vector.get(row);
            t.put("profile" + column, value);
            vector.set(row, t);
        }
        return vector;
    }

    /**
     * Get all the wounds for a patient
     *
     * @param patient_id
     * @return wounds
     */
    public Collection getWounds(int patient_id) {

        try {
            WoundVO w = new WoundVO();
            w.setPatient_id(patient_id);
            return dao.findAllWoundsByCriteria(w, -1);

        } catch (Exception e) {
            log.error("An application exception has been raised in WoundManagerBD.perform(): " + e.toString());
            System.out.println("An application exception has been raised in WoundManagerBD.perform(): " + e.toString());
            Vector results = new Vector();
            System.out.println("getWoundProfilesDropdown: returning empty results");
            return results;
        }

    }

    /**
     * Get all the wounds for a patient
     *
     * @param patient_id
     * @return wounds
     */
    public Collection getWounds(WoundVO w) {

        try {

            return dao.findAllWoundsByCriteria(w, -1);

        } catch (Exception e) {
            log.error("An application exception has been raised in WoundManagerBD.perform(): " + e.toString());
            System.out.println("An application exception has been raised in WoundManagerBD.perform(): " + e.toString());
            Vector results = new Vector();
            System.out.println("getWoundProfilesDropdown: returning empty results");
            return results;
        }

    }

    public Collection<LocationImagesVO> getAllLocationImages() {
        try {
            WoundProfileDAO dao = new WoundProfileDAO();
            LocationImagesVO img = new LocationImagesVO();
            return dao.findAllLocationImagesByCriteria(img);

        } catch (Exception e) {
            log.error("An application exception has been raised in WoundManagerBD.perform(): " + e.toString());
            Vector results = new Vector();
            return results;
        }
    }

    public Vector getWoundLocationsImages(String strGender, String strView) {
        try {
            WoundProfileDAO dao = new WoundProfileDAO();
            return dao.getLocationImages(strGender, strView, language);

        } catch (Exception e) {

            log.error("An application exception has been raised in WoundManagerBD.perform(): " + e.toString());
            Vector results = new Vector();
            return results;
        }

    }

    /**
     * drop all temporary (inactive) wound profiles, and their dependencies, if
     * purge_days is greater then 0, delete all inactive wound profiles older
     * then purge_days.
     *
     * @param patient_id
     * @param professional_id
     * @param purge_days
     */
    public void dropTMP(String patient_id, int professional_id, Date purge_days) {
        try {
            WoundProfileVO wp = new WoundProfileVO();
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            wp.setActive(new Integer(0));//ensure it deletes only tmp
            if (professional_id > 0) {
                wp.setProfessional_id(professional_id);
            }
            wp.setPatient_id(new Integer(patient_id));
            WoundVO w = new WoundVO();//should cascade and delete all children
            w.setActive(new Integer(0));
            if (professional_id > 0) {
                w.setProfessional_id(professional_id);
            }
            w.setPatient_id(new Integer(patient_id));

            List<Integer> deleted_ids = dao.delete(wp, purge_days);
            WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
            wal.setPatient_id(new Integer(patient_id));
            wal.setProfessional_id(new Integer(professional_id));
            wal.setActive(0);
            walManager.removeAlpha(wal, purge_days);
            List<Integer> deleted_ids2 = dao.deleteWounds(w, purge_days);
            for (int id : deleted_ids) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new Integer(professional_id), new Integer(patient_id), com.pixalere.utils.Constants.DROP_TMP_RECORD, "wound_profiles", id));

            }
            for (int id : deleted_ids2) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new Integer(professional_id), new Integer(patient_id), com.pixalere.utils.Constants.DROP_TMP_RECORD, "wounds", id));

            }
        } catch (ApplicationException e) {
            log.error("Update Wound Profile failed" + e.getMessage());

        }
    }

    /**
     * drop all active/inactive wound profiles.
     *
     * @param patient_id
     * @param wound_id
     * @param professional_id
     */
    public void dropTMP(String patient_id, String wound_id, String professional_id) {
        try {
            WoundProfileVO wp = new WoundProfileVO();
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            wp.setActive(new Integer(0));//ensure it deletes only tmp
            if (wound_id != null) {
                wp.setWound_id(new Integer(wound_id));
            }

            wp.setProfessional_id(new Integer(professional_id));
            wp.setPatient_id(new Integer(patient_id));
            WoundVO w = new WoundVO();//should cascade and delete all children
            w.setActive(new Integer(0));
            w.setProfessional_id(new Integer(professional_id));
            w.setPatient_id(new Integer(patient_id));
            w.setId(new Integer(wound_id));
            List<Integer> deleted_ids = dao.delete(wp, null);
            WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
            wal.setPatient_id(new Integer(patient_id));
            wal.setWound_id(new Integer(wound_id));
            wal.setProfessional_id(new Integer(professional_id));
            wal.setActive(0);
            walManager.removeAlpha(wal, null);
            List<Integer> deleted_ids2 = dao.deleteWounds(w, null);
            for (int id : deleted_ids) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new Integer(professional_id), new Integer(patient_id), com.pixalere.utils.Constants.DROP_TMP_RECORD, "wound_profiles", id));

            }
            for (int id : deleted_ids2) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new Integer(professional_id), new Integer(patient_id), com.pixalere.utils.Constants.DROP_TMP_RECORD, "wounds", id));

            }
        } catch (ApplicationException e) {
            log.error("Update Wound Profile failed" + e.getMessage());

        }
    }

    public void drop(WoundProfileVO wp) {
        try {
            List<Integer> deleted_ids = dao.delete(wp, null);
            for (int id : deleted_ids) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(wp.getProfessional_id(), wp.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "wound_profiles", id));

            }
        } catch (ApplicationException e) {
            log.error("Update Wound Profile failed" + e.getMessage());

        }
    }
    //@todo refactor these two methods

    public void dropOffline(WoundProfileVO wp) {
        WoundVO w = new WoundVO();//should cascade and delete all children

        w.setPatient_id(wp.getPatient_id());
        dao.delete(wp, null);
        if (wp != null && wp.getPatient_id() != null) {
            dao.deleteWounds(w, null);
        }
    }

    public void updateWoundProfile(WoundProfileVO accountVO) throws ApplicationException {
        try {
            dao.update(accountVO);
            ValueObject new_id = dao.findByCriteria(accountVO);
            if (new_id != null && accountVO != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(accountVO.getProfessional_id(), accountVO.getPatient_id(), (accountVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wound_profiles", new_id));
            }
        } catch (DataAccessException e) {
            log.error("Update Wound Profile failed" + e.getMessage());
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }

    public void saveWound(WoundVO vo) throws ApplicationException {
        try {
            dao.saveWound(vo);
            ValueObject new_id = dao.findWoundByCriteria(vo);
            if (vo != null && new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wounds", new_id));
            }
        } catch (DataAccessException e) {
            log.error("Update Wound Profile failed" + e.getMessage());
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }

    public void saveLocationImage(LocationImagesVO vo) throws ApplicationException {
        dao.insertLocationImage(vo);
    }

    public void saveWoundProfileType(WoundProfileTypeVO vo) throws ApplicationException {
        try {
            dao.saveWoundProfileType(vo);
            ValueObject new_id = dao.findByCriteria(vo);
            if (vo != null && new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wound_profile_type", new_id));
            }
        } catch (DataAccessException e) {
            log.error("Update Wound Profile failed" + e.getMessage());
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }

    public void saveWoundProfile(WoundProfileVO vo) throws ApplicationException {
        try {
            if (vo != null) {
                dao.insert(vo);
                ValueObject new_id = dao.findByCriteria(vo);
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wound_profiles", new_id));
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }

    public WoundProfileTypeVO getWoundProfileType(WoundProfileTypeVO wound) throws ApplicationException {
        try {
            WoundProfileTypeVO tmp = (WoundProfileTypeVO) dao.findByCriteria(wound);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfileType(): " + e.toString(), e);
        }
    }

    public WoundProfileVO getWoundProfile(WoundProfileVO wound) throws ApplicationException {
        try {
            WoundProfileVO tmp = (WoundProfileVO) dao.findByCriteria(wound);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }

    public WoundProfileVO getWoundProfile(WoundProfileVO wound, boolean showDeleted) throws ApplicationException {
        try {
            WoundProfileVO tmp = (WoundProfileVO) dao.findByCriteria(wound, showDeleted);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }

    public Vector getAllSignatures(int wound_id, int limit_start) throws ApplicationException {
        try {
            return dao.getAllWoundProfileSignatures(wound_id, limit_start);
        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve etiology 
     * 
     * @param etiology the object containing the criteria for retrieving record
     * @return Etiology null if not exists
     * @throws ApplicationException 
     */
    public EtiologyVO getEtiology(EtiologyVO et) throws ApplicationException {
        try {
            EtiologyVO tmp = (EtiologyVO) dao.findEtiologyByCriteria(et);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve goal 
     * 
     * @param goal the object containing the criteria for retrieving record
     * @return Goals null if not exists
     * @throws ApplicationException 
     */
    public GoalsVO getGoal(GoalsVO et) throws ApplicationException {
        try {
            GoalsVO tmp = (GoalsVO) dao.findGoalByCriteria(et);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public WoundVO getWound(WoundVO wound) throws ApplicationException {
        try {
            WoundVO tmp = (WoundVO) dao.findWoundByCriteria(wound);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }

    public WoundVO getWound(int wound_id) throws ApplicationException {
        try {
            WoundVO t = new WoundVO();
            t.setId(new Integer(wound_id));
            WoundVO tmp = (WoundVO) dao.findWoundByCriteria(t);
            return tmp;

        } catch (DataAccessException e) {

            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }

    /**
     * Check if patient has any open wound profiles.
     *
     * @param patient_id
     * @return true if all wounds are healed
     */
    public boolean checkForOpenProfiles(int patient_id) {
        try {
            WoundVO t = new WoundVO();
            t.setPatient_id(new Integer(patient_id));
            t.setActive(1);

            Collection<WoundVO> tmp = dao.findAllWoundsByCriteria(t, 0);
            boolean healed = true;
            for (WoundVO wnd : tmp) {
                if (wnd.getStatus() == null || !wnd.getStatus().equals("Closed")) {
                    healed = false;
                }
            }
            return healed;

        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Reopen a wound which was previously closed.
     *
     * @param intWound
     */
    public void reopenWound(int intWound) {
        try {
            WoundVO searchWnd = new WoundVO();
            searchWnd.setId(new Integer(intWound));
            WoundVO wnd = (WoundVO) dao.findWoundByCriteria(searchWnd);
            if ((wnd.getStatus() != null && wnd.getStatus().equals("Closed"))) {
                wnd.setClosed_date(null);
                wnd.setStatus(null);
                saveWound(wnd);
                WoundVO new_id = getWound(wnd);
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(wnd.getProfessional_id(), wnd.getPatient_id(), (wnd.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wounds", new_id));
                }
            }
        } catch (Exception e) {
            log.error("An application exception has been raised in WoundServiceImpl.reopenWound: " + e.toString());
            System.out.println("An application exception has been raised in WoundServiceImpl.reopenWound: " + e.toString());
        }
    }

    public void reopenWoundType(int intWoundType) {
        try {
            WoundProfileTypeVO searchWpt = new WoundProfileTypeVO();
            searchWpt.setId(new Integer(intWoundType));
            WoundProfileTypeVO wpt = (WoundProfileTypeVO) dao.findByCriteria(searchWpt);
            if (wpt.getClosed() == 1) {
                wpt.setClosed_date(null);
                wpt.setClosed(0);
                saveWoundProfileType(wpt);
                WoundProfileTypeVO new_id = getWoundProfileType(wpt);
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(wpt.getProfessional_id(), wpt.getPatient_id(), (wpt.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wound_profile_type", new_id));
                }
                reopenWound(wpt.getWound_id());
            }
        } catch (Exception e) {
            log.error("An application exception has been raised in WoundServiceImpl.reopenWoundType: " + e.toString());
            System.out.println("An application exception has been raised in WoundServiceImpl.reopenWoundType: " + e.toString());
        }
    }

    public void saveEtiology(EtiologyVO vo) throws ApplicationException {
        try {
            WoundProfileDAO ppDAO = new WoundProfileDAO();
            ppDAO.saveEtiology(vo);

        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public void removeEtiology(EtiologyVO p) {
        WoundProfileDAO dao = new WoundProfileDAO();
        dao.deleteEtiology(p);
    }

    public void saveAcquired(WoundAcquiredVO vo) throws ApplicationException {
        try {
            WoundProfileDAO ppDAO = new WoundProfileDAO();
            ppDAO.saveAcquired(vo);

        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public void removeAcquired(WoundAcquiredVO p) {
        WoundProfileDAO dao = new WoundProfileDAO();
        dao.deleteAcquired(p);
    }

    public Collection getAllGoals(GoalsVO assess) throws ApplicationException {
        try {
            WoundProfileDAO ppDAO = new WoundProfileDAO();

            return ppDAO.findAllByCriteria(assess);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);

        }

    }
public Collection getAllAcquired(WoundAcquiredVO assess) throws ApplicationException {
        try {
            WoundProfileDAO ppDAO = new WoundProfileDAO();

            return ppDAO.findAllByCriteria(assess);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);

        }

    }
    public Collection getAllEtiologies(EtiologyVO assess) throws ApplicationException {
        try {
            WoundProfileDAO ppDAO = new WoundProfileDAO();

            return ppDAO.findAllByCriteria(assess);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);

        }

    }

    public void saveGoal(GoalsVO vo) throws ApplicationException {
        try {
            WoundProfileDAO ppDAO = new WoundProfileDAO();
            ppDAO.saveGoal(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public void removeGoal(GoalsVO p) {
        WoundProfileDAO dao = new WoundProfileDAO();
        dao.deleteGoal(p);
    }
}
