package com.pixalere.wound.service;

import com.pixalere.common.ApplicationException;
import com.pixalere.wound.bean.*;
import com.pixalere.wound.bean.LocationImagesVO;
import javax.jws.WebService;
import javax.jws.WebParam;
import java.util.Collection;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface WoundService {
    public WoundProfileVO getWoundProfile(@WebParam(name="crit") WoundProfileVO crit) throws ApplicationException;
    public void updateWoundProfile(@WebParam(name="updateRecord") WoundProfileVO updateRecord) throws ApplicationException;
    public Collection<WoundProfileVO> getWoundProfiles(@WebParam(name="crit") WoundProfileVO crit, @WebParam(name="limit") int limit) throws ApplicationException;
    public void saveWound(@WebParam(name="updateRecord") WoundVO updateRecord) throws ApplicationException;
    public WoundVO getWound(@WebParam(name="crit") WoundVO crit) throws ApplicationException;
    public void saveWoundProfileType(@WebParam(name="updateRecord") WoundProfileTypeVO updateRecord) throws ApplicationException;
    public WoundProfileTypeVO getWoundProfileType(@WebParam(name="crit") WoundProfileTypeVO crit) throws ApplicationException;
    public Collection<LocationImagesVO> getAllLocationImages() throws ApplicationException;
     public void saveGoal(@WebParam(name="updateRecord") GoalsVO updateRecord) throws ApplicationException;
    public void saveEtiology(@WebParam(name="updateRecord") EtiologyVO updateRecord) throws ApplicationException;
    public void saveAcquired(@WebParam(name="updateRecord") WoundAcquiredVO updateRecord) throws ApplicationException;
}