package com.pixalere.struts.summary;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;


import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;



/**
 * @author
 *
 */
public class SummaryForm
    extends ActionForm {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.
      getLogger(SummaryForm.class);
  public ActionErrors validate(ActionMapping mapping,
                               HttpServletRequest request) {
	
    ActionErrors errors = new ActionErrors();
    
    return errors;

  }

  public void reset(ActionMapping mapping, HttpServletRequest request) {
  }

  /**
   * Returns the password.
   * @return String
   */
  

private String body_referral_drain;
  private String body_referral_ostomy;
  private String body_referral_incision;
  private String body_referral_wound;
  private String body_referral_skin;
  private String body_referral_burn;
  private int auto_referral_type;
  private int backdated_year;
  private int backdated_month;
  private int backdated_day;
  private String backdated_time;
  private String auto_referral;

  private String body_wound;
  private String referral_wound;
  private int priority_wound;
private int wound_referral_for;
  private int skin_referral_for;
  private int drain_referral_for;
  private int incision_referral_for;
  private int ostomy_referral_for;
  private int burn_referral_for;
  private int priority_burn;

  private String body_skin;
  private String referral_skin;
  private int priority_skin;

  private String body_incision;
  private String referral_incision;
  private int priority_incision;

  private String body_drain;
  private String referral_drain;
  private int priority_drain;
  private String body_burn;
  private String referral_burn;
  private String body_ostomy;
  private String referral_ostomy;
  private int priority_ostomy;
  private int send_message_ostomy;
  private int send_message_skin;
  private int send_message_incision;
  private int send_message_wound;
  private int send_message_drain;
  private int send_message_burn;
  public int getAuto_referral_type(){
	  return auto_referral_type;
  }
  public void setAuto_referral_type(int auto_referral_type){
	  this.auto_referral_type=auto_referral_type;
  }
    public int getContinued() {
        return continued;
    }

    public void setContinued(int continued) {
        this.continued = continued;
    }

    private int continued;


    public String getBody_wound() {
        return body_wound;
    }

    public void setBody_wound(String body_wound) {
        this.body_wound = body_wound;
    }

    public String getReferral_wound() {
        return referral_wound;
    }

    public void setReferral_wound(String referral_wound) {
        this.referral_wound = referral_wound;
    }

    public int getPriority_wound() {
        return priority_wound;
    }

    public void setPriority_wound(int priority_wound) {
        this.priority_wound = priority_wound;
    }

 

    public String getBody_incision() {
        return body_incision;
    }

    public void setBody_incision(String body_incision) {
        this.body_incision = body_incision;
    }

    public String getReferral_incision() {
        return referral_incision;
    }

    public void setReferral_incision(String referral_incision) {
        this.referral_incision = referral_incision;
    }

    public int getPriority_incision() {
        return priority_incision;
    }

    public void setPriority_incision(int priority_incision) {
        this.priority_incision = priority_incision;
    }

 

    public String getBody_drain() {
        return body_drain;
    }

    public void setBody_drain(String body_drain) {
        this.body_drain = body_drain;
    }

    public String getReferral_drain() {
        return referral_drain;
    }

    public void setReferral_drain(String referral_drain) {
        this.referral_drain = referral_drain;
    }

    public int getPriority_drain() {
        return priority_drain;
    }

    public void setPriority_drain(int priority_drain) {
        this.priority_drain = priority_drain;
    }


    public String getBody_ostomy() {
        return body_ostomy;
    }

    public void setBody_ostomy(String body_ostomy) {
        this.body_ostomy = body_ostomy;
    }

    public String getReferral_ostomy() {
        return referral_ostomy;
    }

    public void setReferral_ostomy(String referral_ostomy) {
        this.referral_ostomy = referral_ostomy;
    }

    public int getPriority_ostomy() {
        return priority_ostomy;
    }

    public void setPriority_ostomy(int priority_ostomy) {
        this.priority_ostomy = priority_ostomy;
    }

    public int getSend_message_ostomy() {
        return send_message_ostomy;
    }

    public void setSend_message_ostomy(int send_message_ostomy) {
        this.send_message_ostomy = send_message_ostomy;
    }

    public int getSend_message_incision() {
        return send_message_incision;
    }

    public void setSend_message_incision(int send_message_incision) {
        this.send_message_incision = send_message_incision;
    }

    public int getSend_message_wound() {
        return send_message_wound;
    }

    public void setSend_message_wound(int send_message_wound) {
        this.send_message_wound = send_message_wound;
    }

    public int getSend_message_drain() {
        return send_message_drain;
    }

    public void setSend_message_drain(int send_message_drain) {
        this.send_message_drain = send_message_drain;
    }

    public String getAuto_referral() {
        return auto_referral;
    }

    public void setAuto_referral(String auto_referral) {
        this.auto_referral = auto_referral;
    }


    /**
     * @return the backdated_year
     */
    public int getBackdated_year() {
        return backdated_year;
    }

    /**
     * @param backdated_year the backdated_year to set
     */
    public void setBackdated_year(int backdated_year) {
        this.backdated_year = backdated_year;
    }

    /**
     * @return the backdated_month
     */
    public int getBackdated_month() {
        return backdated_month;
    }

    /**
     * @param backdated_month the backdated_month to set
     */
    public void setBackdated_month(int backdated_month) {
        this.backdated_month = backdated_month;
    }

    /**
     * @return the backdated_day
     */
    public int getBackdated_day() {
        return backdated_day;
    }

    /**
     * @param backdated_day the backdated_day to set
     */
    public void setBackdated_day(int backdated_day) {
        this.backdated_day = backdated_day;
    }

    /**
     * @return the backdated_time
     */
    public String getBackdated_time() {
        return backdated_time;
    }

    /**
     * @param backdated_time the backdated_time to set
     */
    public void setBackdated_time(String backdated_time) {
        this.backdated_time = backdated_time;
    }


    /**

     * @return the body_skin

     */

    public String getBody_skin() {
        return body_skin;

    }

    /**

     * @param body_skin the body_skin to set

     */

    public void setBody_skin(String body_skin) {
        this.body_skin = body_skin;

    }

    /**

     * @return the referral_skin

     */

    public String getReferral_skin() {
        return referral_skin;

    }

    /**

     * @param referral_skin the referral_skin to set

     */

    public void setReferral_skin(String referral_skin) {
        this.referral_skin = referral_skin;

    }

    /**

     * @return the priority_skin

     */

    public int getPriority_skin() {
        return priority_skin;

    }

    /**

     * @param priority_skin the priority_skin to set

     */

    public void setPriority_skin(int priority_skin) {
        this.priority_skin = priority_skin;

    }

    /**

     * @return the send_message_skin

     */

    public int getSend_message_skin() {
        return send_message_skin;

    }

    /**

     * @param send_message_skin the send_message_skin to set

     */

    public void setSend_message_skin(int send_message_skin) {
        this.send_message_skin = send_message_skin;

    }

    /**
     * @return the priority_burn
     */
    public int getPriority_burn() {
        return priority_burn;
    }

    /**
     * @param priority_burn the priority_burn to set
     */
    public void setPriority_burn(int priority_burn) {
        this.priority_burn = priority_burn;
    }

 

    /**
     * @return the body_burn
     */
    public String getBody_burn() {
        return body_burn;
    }

    /**
     * @param body_burn the body_burn to set
     */
    public void setBody_burn(String body_burn) {
        this.body_burn = body_burn;
    }

    /**
     * @return the referral_burn
     */
    public String getReferral_burn() {
        return referral_burn;
    }

    /**
     * @param referral_burn the referral_burn to set
     */
    public void setReferral_burn(String referral_burn) {
        this.referral_burn = referral_burn;
    }

    /**
     * @return the send_message_burn
     */
    public int getSend_message_burn() {
        return send_message_burn;
    }

    /**
     * @param send_message_burn the send_message_burn to set
     */
    public void setSend_message_burn(int send_message_burn) {
        this.send_message_burn = send_message_burn;
    }

    /**
     * @return the body_referral_drain
     */
    public String getBody_referral_drain() {
        return body_referral_drain;
    }

    /**
     * @param body_referral_drain the body_referral_drain to set
     */
    public void setBody_referral_drain(String body_referral_drain) {
        this.body_referral_drain = body_referral_drain;
    }

    /**
     * @return the body_referral_ostomy
     */
    public String getBody_referral_ostomy() {
        return body_referral_ostomy;
    }

    /**
     * @param body_referral_ostomy the body_referral_ostomy to set
     */
    public void setBody_referral_ostomy(String body_referral_ostomy) {
        this.body_referral_ostomy = body_referral_ostomy;
    }

    /**
     * @return the body_referral_incision
     */
    public String getBody_referral_incision() {
        return body_referral_incision;
    }

    /**
     * @param body_referral_incision the body_referral_incision to set
     */
    public void setBody_referral_incision(String body_referral_incision) {
        this.body_referral_incision = body_referral_incision;
    }

    /**
     * @return the body_referral_wound
     */
    public String getBody_referral_wound() {
        return body_referral_wound;
    }

    /**
     * @param body_referral_wound the body_referral_wound to set
     */
    public void setBody_referral_wound(String body_referral_wound) {
        this.body_referral_wound = body_referral_wound;
    }

    /**
     * @return the body_referral_skin
     */
    public String getBody_referral_skin() {
        return body_referral_skin;
    }

    /**
     * @param body_referral_skin the body_referral_skin to set
     */
    public void setBody_referral_skin(String body_referral_skin) {
        this.body_referral_skin = body_referral_skin;
    }

    /**
     * @return the body_referral_burn
     */
    public String getBody_referral_burn() {
        return body_referral_burn;
    }

    /**
     * @param body_referral_burn the body_referral_burn to set
     */
    public void setBody_referral_burn(String body_referral_burn) {
        this.body_referral_burn = body_referral_burn;
    }

    /**
     * @return the wound_referral_for
     */
    public int getWound_referral_for() {
        return wound_referral_for;
    }

    /**
     * @param wound_referral_for the wound_referral_for to set
     */
    public void setWound_referral_for(int wound_referral_for) {
        this.wound_referral_for = wound_referral_for;
    }

    /**
     * @return the skin_referral_for
     */
    public int getSkin_referral_for() {
        return skin_referral_for;
    }

    /**
     * @param skin_referral_for the skin_referral_for to set
     */
    public void setSkin_referral_for(int skin_referral_for) {
        this.skin_referral_for = skin_referral_for;
    }

    /**
     * @return the drain_referral_for
     */
    public int getDrain_referral_for() {
        return drain_referral_for;
    }

    /**
     * @param drain_referral_for the drain_referral_for to set
     */
    public void setDrain_referral_for(int drain_referral_for) {
        this.drain_referral_for = drain_referral_for;
    }

    /**
     * @return the incision_referral_for
     */
    public int getIncision_referral_for() {
        return incision_referral_for;
    }

    /**
     * @param incision_referral_for the incision_referral_for to set
     */
    public void setIncision_referral_for(int incision_referral_for) {
        this.incision_referral_for = incision_referral_for;
    }

    /**
     * @return the ostomy_referral_for
     */
    public int getOstomy_referral_for() {
        return ostomy_referral_for;
    }

    /**
     * @param ostomy_referral_for the ostomy_referral_for to set
     */
    public void setOstomy_referral_for(int ostomy_referral_for) {
        this.ostomy_referral_for = ostomy_referral_for;
    }

    /**
     * @return the burn_referral_for
     */
    public int getBurn_referral_for() {
        return burn_referral_for;
    }

    /**
     * @param burn_referral_for the burn_referral_for to set
     */
    public void setBurn_referral_for(int burn_referral_for) {
        this.burn_referral_for = burn_referral_for;
    }

}