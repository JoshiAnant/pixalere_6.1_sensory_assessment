package com.pixalere.struts.summary;

import com.pixalere.auth.bean.PositionVO;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.admin.service.LogAuditServiceImpl;

import java.util.Date;

import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.patient.bean.DiagnosticInvestigationVO;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.pixalere.patient.bean.PhysicalExamVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import com.pixalere.guibeans.RowData;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbUpperAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.bean.SummaryVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.wound.bean.GoalsVO;

import java.util.*;

import com.pixalere.utils.PDate;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.auth.bean.ProfessionalVO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.bean.OfflineVO;
import com.pixalere.patient.bean.PatientAccountVO;

public class SummarySetupAction extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SummarySetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            //Generate Token - prevent multiple posts
            saveToken(request);
            Hashtable alphaStore = new Hashtable();
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            PatientAccountVO pa = (PatientAccountVO) session.getAttribute("patientAccount");
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            request.setAttribute("page", "summary");
            //check if an assessment has been completed with products/treatment.
            AssessmentServiceImpl eachBD = new AssessmentServiceImpl(language);
            AssessmentImagesServiceImpl imagesBD = new AssessmentImagesServiceImpl();
            WoundAssessmentLocationServiceImpl locBD = new WoundAssessmentLocationServiceImpl();
            AssessmentCommentsServiceImpl cmanager = new AssessmentCommentsServiceImpl();
            WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
            NursingCarePlanServiceImpl ncpmanager = new NursingCarePlanServiceImpl();

            LogAuditServiceImpl logservice = new LogAuditServiceImpl();
            WoundServiceImpl woundBD = new WoundServiceImpl(language);
            ListServiceImpl listmanager = new ListServiceImpl(language);

            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            /*if(session.getAttribute("backdated") == null){
             session.setAttribute("backdated",new Date());
             }*/
            Date now = new Date();
            request.setAttribute("backdated_year", pdate.getYear(now));
            request.setAttribute("backdated_day", pdate.getDay(now));
            request.setAttribute("backdated_month", pdate.getMonthNum(now));
            request.setAttribute("backdated_minutes", pdate.getMin(now));
            request.setAttribute("backdated_hours", pdate.getHour(now));
            String assessment_id = null;
            if (wound_id != null && !wound_id.equals("")) {
                WoundAssessmentVO atmp = new WoundAssessmentVO();
                atmp.setWound_id(new Integer(wound_id));
                atmp.setActive(0);
                atmp.setProfessional_id(userVO.getId());
                WoundAssessmentVO tmpAssess = wamanager.getAssessment(atmp);
                if (tmpAssess != null) {
                    assessment_id = tmpAssess.getId() + "";
                    request.setAttribute("assessment_id", assessment_id);
                }
            }
            int wp_id = -1;
            if (session.getAttribute("wound_id") != null && !((String) session.getAttribute("wound_id")).equals("")) {
                wp_id = new Integer((String) session.getAttribute("wound_id"));
            }
            int assess_id = -1;
            if (assessment_id != null && !((String) assessment_id).equals("")) {
                assess_id = new Integer((String) assessment_id);
            }
            if (assess_id != -1 && eachBD.verifyTreatmentCompletion(assess_id) == false && locBD.isWoundHealed(wp_id, userVO.getId().intValue()) == false) {
                session.setAttribute("treatment_completion_error", "1");
                return (mapping.findForward("uploader.go.treatment"));
            }
            //Getting Patient Profile data
            PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
            PatientServiceImpl manager = new PatientServiceImpl(language);
            ReportBuilder reportService = new ReportBuilder(language);
            PatientProfileVO tmpProfile = pmanager.getTemporaryPatientProfile(Integer.parseInt(patient_id), userVO.getId());
            DiagnosticInvestigationVO ttt = new DiagnosticInvestigationVO();
            ttt.setPatient_id(new Integer(patient_id));
            ttt.setActive(0);
            ttt.setProfessional_id(userVO.getId());
            DiagnosticInvestigationVO tmpDiag = pmanager.getDiagnosticInvestigation(ttt);
            BradenVO tmpBraden = pmanager.getTemporaryBraden(Integer.parseInt(patient_id), userVO.getId());
            PhysicalExamVO tmpExam = pmanager.getTemporaryExam(Integer.parseInt(patient_id), userVO.getId());
            
            FootAssessmentVO tmpFoot = pmanager.getTemporaryFoot(Integer.parseInt(patient_id), userVO.getId());
            SensoryAssessmentVO tmpSensory = pmanager.getTemporarySensory(Integer.parseInt(patient_id), userVO.getId());
            
            LimbBasicAssessmentVO tmpLimb = pmanager.getTemporaryLimbBasic(Integer.parseInt(patient_id), userVO.getId());
            LimbAdvAssessmentVO tmpLimbAdv = pmanager.getTemporaryLimbAdv(Integer.parseInt(patient_id), userVO.getId());
            LimbUpperAssessmentVO tmpLimbUp = pmanager.getTemporaryLimbUpper(Integer.parseInt(patient_id), userVO.getId());

            List<FieldValues> comparedbraden = null;
            Collection<BradenVO> resultsBraden = new ArrayList();
            if (tmpBraden != null) {
                resultsBraden.add(tmpBraden);
                RowData[] faRecord = pmanager.getAllBradenForFlowchart(resultsBraden, userVO, false);
                comparedbraden = Common.compareGlobal(faRecord);
            }

            //check physicalexam.
            List<FieldValues> comparedexam = null;
            Collection<PhysicalExamVO> resultsExam = new ArrayList();
            if (tmpExam != null) {
                resultsExam.add(tmpExam);
                RowData[] exRecord = pmanager.getAllExamsForFlowchart(resultsExam, userVO, false);
                comparedexam = Common.compareGlobal(exRecord);
            }
            //check diagnostic.
            List<FieldValues> compareddiag = null;
            Collection<DiagnosticInvestigationVO> resultsDiag = new ArrayList();
            if (tmpDiag != null) {
                resultsDiag.add(tmpDiag);
                RowData[] exRecord = pmanager.getAllDiagnosticAssessmentsForFlowchart(resultsDiag, userVO, false);
                compareddiag = Common.compareGlobal(exRecord);
            }
            List<FieldValues> comparedfoot = null;
            List<FieldValues> comparedsensory = null; // created for sensory
            List<FieldValues> comparedlimb = null;
            List<FieldValues> comparedlimbadv = null;
            List<FieldValues> comparedlimbu = null;

            Collection<FootAssessmentVO> resultsFA = new ArrayList();
            Collection<SensoryAssessmentVO> resultsSA = new ArrayList(); // created for sensory
            Collection<LimbBasicAssessmentVO> resultsLA = new ArrayList();
            Collection<LimbAdvAssessmentVO> resultsLAdv = new ArrayList();
            Collection<LimbUpperAssessmentVO> resultsLAU = new ArrayList();
            ;
            if (tmpFoot != null) {
            	
            	resultsFA.add(tmpFoot);
                RowData[] faRecord = pmanager.getAllFootAssessmentsForFlowchart(resultsFA, userVO, false);
                comparedfoot = Common.compareGlobal(faRecord);
                
            }
            // created for sensory
            if (tmpSensory != null) {
            	resultsSA.add(tmpSensory);
                RowData[] saRecord = pmanager.getAllSensoryAssessmentsForFlowchart(resultsSA, userVO, false);
                comparedsensory = Common.compareGlobal(saRecord);
            }
            if (tmpLimb != null) {
                resultsLA.add(tmpLimb);
                RowData[] laRecord = pmanager.getAllBasicLimbAssessmentsForFlowchart(resultsLA, userVO, false);
                comparedlimb = Common.compareGlobal(laRecord);
            }
            if (tmpLimbAdv != null) {
                resultsLAdv.add(tmpLimbAdv);
                RowData[] lavRecord = pmanager.getAllAdvLimbAssessmentsForFlowchart(resultsLAdv, userVO, false);
                comparedlimbadv = Common.compareGlobal(lavRecord);
            }
            if (tmpLimbUp != null) {
                resultsLAU.add(tmpLimbUp);
                RowData[] lauRecord = pmanager.getAllUpperLimbAssessmentsForFlowchart(resultsLAU, userVO, false);
                comparedlimbu = Common.compareGlobal(lauRecord);
            }
            Collection resultsPP = new ArrayList();
            List<FieldValues> comparedppdata = null;
            if (tmpProfile != null) {
                resultsPP.add(tmpProfile);
                RowData[] ppRecord = pmanager.getAllPatientProfilesForFlowchart(resultsPP, userVO, false, true);
                comparedppdata = Common.compareGlobal(ppRecord);//Getting flowchart data.
            }

            //if Offline... we need to set offlin record with changed fields.
            OfflineVO offline = null;
            if (Common.isOffline() == true) {

                if (session.getAttribute("offlineVO") == null) {
                    offline = new OfflineVO();
                } else {
                    offline = (OfflineVO) session.getAttribute("offlineVO");
                }

            }
            if (tmpBraden != null) {
                session.setAttribute("braden_changed", "true");
                if (offline != null) {
                    offline.setBraden_changed(1);
                }
            } else {
                session.setAttribute("braden_changed", "false");
            }
            if (tmpExam != null) {
                session.setAttribute("exam_changed", "true");
                if (offline != null) {
                    offline.setExam_changed(1);
                }
            } else {
                session.setAttribute("exam_changed", "false");
            }
            if (tmpDiag != null) {
                session.setAttribute("diag_changed", "true");
                if (offline != null) {
                    offline.setDiagnostic_changed(1);
                }
            } else {
                session.setAttribute("diag_changed", "false");
            }
            if (Common.recordChanged(comparedppdata) == true && tmpProfile != null) {
                session.setAttribute("patient_changed", "true");
                if (offline != null) {
                    offline.setPatientprofile_changed(1);
                }
            } else {
                session.setAttribute("patient_changed", "false");
            }
           if (tmpFoot != null) {
            	
                session.setAttribute("foot_changed", "true");
            	if (offline != null) {
                    offline.setFoot_changed(1);
                }
            } else {
                session.setAttribute("foot_changed", "false");
            }
            /***************************************************/
            if (tmpSensory != null) {
                session.setAttribute("sensory_changed", "true");
            	
            	if (offline != null) {
                    offline.setSensory_changed(1);
                }
            } else {
                session.setAttribute("sensory_changed", "false");
            }
            
            /*******************************************************/
            if (tmpLimb != null) {
                session.setAttribute("limb_changed", "true");
                if (offline != null) {
                    offline.setLimb_changed(1);
                }
            } else {
                session.setAttribute("limb_changed", "false");
            }
            if (tmpLimbAdv != null) {
                session.setAttribute("limb_adv_changed", "true");
                if (offline != null) {
                    offline.setLimb_adv_changed(1);
                }
            } else {
                session.setAttribute("limb_adv_changed", "false");
            }
            if (tmpLimbUp != null) {
                session.setAttribute("limb_upper_changed", "true");
                if (offline != null) {
                    offline.setLimb_upper_changed(1);
                }
            } else {
                session.setAttribute("limb_upper_changed", "false");
            }
            request.setAttribute("patientProfile", reportService.organizeSummary(comparedppdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.patientprofile", locale)));
            request.setAttribute("braden", reportService.organizeSummary(comparedbraden, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.braden", locale)));
            request.setAttribute("foot", reportService.organizeSummary(comparedfoot, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.foot", locale)));
            
            /******************************************/
            request.setAttribute("sensory", reportService.organizeSummary(comparedsensory, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.sensory", locale)));

            /*********************************************/
            request.setAttribute("limb", reportService.organizeSummary(comparedlimb, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.limb_basic", locale)));
            request.setAttribute("limb_adv", reportService.organizeSummary(comparedlimbadv, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.limb_adv", locale)));
            request.setAttribute("limb_upper", reportService.organizeSummary(comparedlimbu, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.limb_adv", locale)));
            request.setAttribute("exam", reportService.organizeSummary(comparedexam, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.exam", locale)));
            request.setAttribute("investigation", reportService.organizeSummary(compareddiag, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.diagnostic", locale)));

          
            /*SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
             Date date = new Date();
             request.setAttribute("time",sf.format(date));*/
            Collection<GoalsVO> woundGoals = null;
            // Display Wound Profile on page
            if (wound_id != null && !wound_id.equals("")) {
                WoundProfileVO wp1 = new WoundProfileVO();
                wp1.setWound_id(new Integer(wound_id));
                wp1.setProfessional_id(userVO.getId());
                wp1.setActive(new Integer(0));
                WoundProfileVO tmpWP = woundBD.getWoundProfile(wp1);
                WoundProfileVO wp2 = new WoundProfileVO();
                wp2.setWound_id(new Integer(wound_id));
                wp2.setCurrent_flag(new Integer(1));
                WoundProfileVO liveWP = woundBD.getWoundProfile(wp2);
                if (tmpWP != null) {
                    woundGoals = tmpWP.getGoals();
                    request.setAttribute("wound_profile", tmpWP.getWound());
                } else if (liveWP != null) {
                    woundGoals = liveWP.getGoals();
                    request.setAttribute("wound_profile", liveWP.getWound());
                }
                //find out if there's inactive incisions
                if (userVO != null && userVO.getId() != null) {
                    WoundAssessmentLocationVO alphaInc = new WoundAssessmentLocationVO();
                    alphaInc.setWound_id(new Integer(wound_id));
                    alphaInc.setActive(0);
                    alphaInc.setProfessional_id(userVO.getId());
                    alphaInc.setAlpha("incs");
                    WoundAssessmentLocationVO wal = locBD.getAlpha(alphaInc);
                    if (wal != null) {
                        session.setAttribute("wound_changed", "true");
                    }
                }
                WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
                WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
                tloco.setWound_id(new Integer((String) wound_id));
                tloco.setDischarge(0); // Only open alphas
                Vector<WoundAssessmentLocationVO> allalphas = walservice.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, true);//4th parameter (extra sort) is ignored

                request.setAttribute("allalphas", allalphas);
                Collection resultsWP = new ArrayList();
                if (liveWP != null) {
                    resultsWP.add(liveWP);
                }
                if (tmpWP != null) {
                    resultsWP.add(tmpWP);
                } else if (liveWP != null) {
                    // Add same data, to make compare think nothing's changed so it won't show in blue on summary page
                    resultsWP.add(liveWP);
                }

                RowData[] tmpRecord = woundBD.getAllWoundProfilesForFlowchart(resultsWP, userVO, false, true);
                List<FieldValues> comparedwpdata = Common.compareGlobal(tmpRecord);
                if (Common.recordChanged(comparedwpdata) == true && tmpWP != null) {
                    session.setAttribute("wound_changed", "true");
                    if (offline != null) {
                        offline.setWound_changed(1);
                    }
                } else {
                    session.setAttribute("wound_changed", "false");
                }
                request.setAttribute("woundProfile", reportService.organizeSummary(comparedwpdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.woundprofile", locale)));

                if (assessment_id != null) {
                    //Getting all Assessment data
                    RowData[] woundAssessments;
                    RowData[] ostomyAssessments;
                    RowData[] incisionAssessments;
                    RowData[] drainAssessments;
                    RowData[] burnAssessments;
                    RowData[] skinAssessments;
                    AssessmentEachwoundVO assWound = new AssessmentEachwoundVO();
                    assWound.setAssessment_id(new Integer(assessment_id));
                    AssessmentOstomyVO assOst = new AssessmentOstomyVO();
                    assOst.setAssessment_id(new Integer(assessment_id));
                    AssessmentIncisionVO assIn = new AssessmentIncisionVO();
                    assIn.setAssessment_id(new Integer(assessment_id));
                    AssessmentDrainVO assDr = new AssessmentDrainVO();
                    assDr.setAssessment_id(new Integer(assessment_id));
                    AssessmentSkinVO assSk = new AssessmentSkinVO();
                    assSk.setAssessment_id(new Integer(assessment_id));
                    AssessmentBurnVO assBn = new AssessmentBurnVO();
                    assBn.setAssessment_id(new Integer(assessment_id));
                    Collection<AssessmentEachwoundVO> ae = eachBD.getAllAssessments(assWound, -1, Constants.ASC_ORDER);
                    Collection<AssessmentOstomyVO> oe = eachBD.getAllAssessments(assOst, -1, Constants.ASC_ORDER);
                    Collection<AssessmentIncisionVO> ie = eachBD.getAllAssessments(assIn, -1, Constants.ASC_ORDER);
                    Collection<AssessmentDrainVO> de = eachBD.getAllAssessments(assDr, -1, Constants.ASC_ORDER);
                    Collection<AssessmentBurnVO> be = eachBD.getAllAssessments(assBn, -1, Constants.ASC_ORDER);

                    Collection<AssessmentSkinVO> sk = eachBD.getAllAssessments(assSk, -1, Constants.ASC_ORDER);

                    woundAssessments = eachBD.getAllAssessmentsForFlowchart(ae, userVO, false);
                    ostomyAssessments = eachBD.getAllOstomyAssessmentsForFlowchart(oe, userVO, false);
                    incisionAssessments = eachBD.getAllIncisionAssessmentsForFlowchart(ie, userVO, false);
                    drainAssessments = eachBD.getAllDrainAssessmentsForFlowchart(de, userVO, false);
                    burnAssessments = eachBD.getAllBurnAssessmentsForFlowchart(be, userVO, false);

                    skinAssessments = eachBD.getAllSkinAssessmentsForFlowchart(sk, userVO, false);
                    Hashtable ncp = new Hashtable();

                    //@todo Next 6 for statements should be refactored into 1
                    for (AbstractAssessmentVO t : ae) {
                        if (session.getAttribute("alpha_id") == null) {
                            session.setAttribute("alpha_id", t.getAlpha_id() + "");
                        }
                        WoundAssessmentLocationVO alpha = t.getAlphaLocation();

                        NursingCarePlanVO n = new NursingCarePlanVO();
                        n.setAssessment_id(new Integer(assessment_id));
                        n.setWound_profile_type_id(t.getWound_profile_type_id());
                        NursingCarePlanVO nn = ncpmanager.getNursingCarePlan(n);
                        if (nn != null) {
                            ncp.put("ncp" + t.getAlpha_id(), nn.getBody());
                            if (nn.getAs_needed() != null) {
                                ncp.put("as" + t.getAlpha_id(), (nn.getAs_needed() == 2 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale)));
                            }
                        }
                        if (nn != null && nn.getVisit_frequency() != null) {
                            ncp.put("vf" + t.getAlpha_id(), Common.getListValue(nn.getVisit_frequency().intValue(), language));
                        }
                        DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                        d.setAssessment_id(t.getAssessment_id());
                        d.setAlpha_id(t.getAlpha_id());
                        DressingChangeFrequencyVO dd = ncpmanager.getDressingChangeFrequency(d);
                        if (dd != null && dd.getDcf() != null) {
                            ncp.put("dcf" + t.getAlpha_id(), Common.getListValue(dd.getDcf().intValue(), language));
                        }

                        alphaStore.put(alpha.getAlpha(), alpha);
                    }
                    for (AbstractAssessmentVO t : sk) {
                        if (session.getAttribute("alpha_id") == null) {
                            session.setAttribute("alpha_id", t.getAlpha_id() + "");
                        }
                        WoundAssessmentLocationVO alpha = t.getAlphaLocation();
                        NursingCarePlanVO n = new NursingCarePlanVO();
                        n.setAssessment_id(new Integer(assessment_id));
                        n.setWound_profile_type_id(t.getWound_profile_type_id());
                        NursingCarePlanVO nn = ncpmanager.getNursingCarePlan(n);
                        if (nn != null) {
                            ncp.put("ncp" + t.getAlpha_id(), nn.getBody());
                            if (nn.getAs_needed() != null) {
                                ncp.put("as" + t.getAlpha_id(), (nn.getAs_needed() == 2 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale)));
                            }
                        }
                        if (nn != null && nn.getVisit_frequency() != null) {
                            ncp.put("vf" + t.getAlpha_id(), Common.getListValue(nn.getVisit_frequency().intValue(), language));
                        }
                        DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                        d.setAssessment_id(t.getAssessment_id());
                        d.setAlpha_id(t.getAlpha_id());
                        DressingChangeFrequencyVO dd = ncpmanager.getDressingChangeFrequency(d);
                        if (dd != null && dd.getDcf() != null) {
                            ncp.put("dcf" + t.getAlpha_id(), Common.getListValue(dd.getDcf().intValue(), language));
                        }

                        alphaStore.put(alpha.getAlpha(), alpha);
                    }
                    for (AbstractAssessmentVO t : ie) {
                        if (session.getAttribute("alpha_id") == null) {
                            session.setAttribute("alpha_id", t.getAlpha_id() + "");
                        }
                        WoundAssessmentLocationVO alpha = t.getAlphaLocation();
                        alphaStore.put(alpha.getAlpha(), alpha);
                        NursingCarePlanVO n = new NursingCarePlanVO();
                        n.setAssessment_id(new Integer(assessment_id));
                        n.setWound_profile_type_id(t.getWound_profile_type_id());
                        NursingCarePlanVO nn = ncpmanager.getNursingCarePlan(n);
                        if (nn != null) {
                            ncp.put("ncp" + t.getAlpha_id(), nn.getBody());
                            if (nn.getAs_needed() != null) {
                                ncp.put("as" + t.getAlpha_id(), (nn.getAs_needed() == 2 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale)));
                            }
                            if (nn.getVisit_frequency() != null) {
                                ncp.put("vf" + t.getAlpha_id(), Common.getListValue(nn.getVisit_frequency().intValue(), language));
                            }
                        }
                        DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                        d.setAssessment_id(t.getAssessment_id());
                        d.setAlpha_id(t.getAlpha_id());
                        DressingChangeFrequencyVO dd = ncpmanager.getDressingChangeFrequency(d);
                        if (dd != null && dd.getDcf() != null) {
                            ncp.put("dcf" + t.getAlpha_id(), Common.getListValue(dd.getDcf().intValue(), language));
                        }
                    }
                    for (AbstractAssessmentVO t : de) {
                        if (session.getAttribute("alpha_id") == null) {
                            session.setAttribute("alpha_id", t.getAlpha_id() + "");
                        }
                        WoundAssessmentLocationVO alpha = t.getAlphaLocation();
                        alphaStore.put(alpha.getAlpha(), alpha);
                        NursingCarePlanVO n = new NursingCarePlanVO();
                        n.setAssessment_id(new Integer(assessment_id));
                        n.setWound_profile_type_id(t.getWound_profile_type_id());
                        NursingCarePlanVO nn = ncpmanager.getNursingCarePlan(n);
                        if (nn != null) {
                            ncp.put("ncp" + t.getAlpha_id(), nn.getBody());
                            if (nn.getAs_needed() != null) {
                                ncp.put("as" + t.getAlpha_id(), (nn.getAs_needed() == 2 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale)));
                            }
                            if (nn.getVisit_frequency() != null) {
                                ncp.put("vf" + t.getAlpha_id(), Common.getListValue(nn.getVisit_frequency().intValue(), language));
                            }
                        }
                        DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                        d.setAssessment_id(t.getAssessment_id());
                        d.setAlpha_id(t.getAlpha_id());
                        DressingChangeFrequencyVO dd = ncpmanager.getDressingChangeFrequency(d);
                        if (dd != null && dd.getDcf() != null) {
                            ncp.put("dcf" + t.getAlpha_id(), Common.getListValue(dd.getDcf().intValue(), language));
                        }
                    }
                    for (AbstractAssessmentVO t : oe) {
                        if (session.getAttribute("alpha_id") == null) {
                            session.setAttribute("alpha_id", t.getAlpha_id() + "");
                        }
                        WoundAssessmentLocationVO alpha = t.getAlphaLocation();
                        alphaStore.put(alpha.getAlpha(), alpha);
                        NursingCarePlanVO n = new NursingCarePlanVO();
                        n.setAssessment_id(new Integer(assessment_id));
                        n.setWound_profile_type_id(t.getWound_profile_type_id());
                        NursingCarePlanVO nn = ncpmanager.getNursingCarePlan(n);
                        if (nn != null) {
                            ncp.put("ncp" + t.getAlpha_id(), nn.getBody());
                            if (nn.getAs_needed() != null) {
                                ncp.put("as" + t.getAlpha_id(), (nn.getAs_needed() == 2 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale)));
                            }
                            if (nn.getVisit_frequency() != null) {
                                ncp.put("vf" + t.getAlpha_id(), Common.getListValue(nn.getVisit_frequency().intValue(), language));
                            }
                        }
                        DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                        d.setAssessment_id(t.getAssessment_id());
                        d.setAlpha_id(t.getAlpha_id());
                        DressingChangeFrequencyVO dd = ncpmanager.getDressingChangeFrequency(d);
                        if (dd != null && dd.getDcf() != null) {
                            ncp.put("dcf" + t.getAlpha_id(), Common.getListValue(dd.getDcf().intValue(), language));
                        }
                    }
                    for (AbstractAssessmentVO t : be) {
                        if (session.getAttribute("alpha_id") == null) {
                            session.setAttribute("alpha_id", t.getAlpha_id() + "");
                        }
                        WoundAssessmentLocationVO alpha = t.getAlphaLocation();
                        alphaStore.put(alpha.getAlpha(), alpha);
                        NursingCarePlanVO n = new NursingCarePlanVO();
                        n.setAssessment_id(new Integer(assessment_id));
                        n.setWound_profile_type_id(t.getWound_profile_type_id());
                        NursingCarePlanVO nn = ncpmanager.getNursingCarePlan(n);
                        if (nn != null) {
                            ncp.put("ncp" + t.getAlpha_id(), nn.getBody());
                            if (nn.getAs_needed() != null) {
                                ncp.put("as" + t.getAlpha_id(), (nn.getAs_needed() == 2 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale)));
                            }
                            if (nn.getVisit_frequency() != null) {
                                ncp.put("vf" + t.getAlpha_id(), Common.getListValue(nn.getVisit_frequency().intValue(), language));
                            }
                        }
                        DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                        d.setAssessment_id(t.getAssessment_id());
                        d.setAlpha_id(t.getAlpha_id());
                        DressingChangeFrequencyVO dd = ncpmanager.getDressingChangeFrequency(d);
                        if (dd != null && dd.getDcf() != null) {
                            ncp.put("dcf" + t.getAlpha_id(), Common.getListValue(dd.getDcf().intValue(), language));
                        }
                    }
                    request.setAttribute("ncp", ncp);

                    if (Common.getConfig("allowCCACReporting").equals("1")) {
                        Hashtable ccac_checks = new Hashtable();
                        //cycle assessments
                        for (AssessmentEachwoundVO each : ae) {
                            AssessmentEachwoundVO assestmp = new AssessmentEachwoundVO();
                            assestmp.setActive(1);
                            assestmp.setAlpha_id(each.getAlpha_id());
                            assestmp.setAssessment_id(each.getAssessment_id());
                            AssessmentEachwoundVO assessment = (AssessmentEachwoundVO) eachBD.getAssessment(assestmp);

                            assestmp.setStatus(new Integer(Common.getConfig("woundActive")));
                            AssessmentEachwoundVO lastAssessment = (AssessmentEachwoundVO) eachBD.getAssessment(assestmp);
                            if (lastAssessment != null) {
                                double before = lastAssessment.getLength() * lastAssessment.getLength();
                                double after = each.getLength() * each.getLength();
                                if (after > before) {
                                    ccac_checks.put("alphaid_" + each.getAlpha_id(), "increased");
                                } else if (before > after) {
                                    ccac_checks.put("alphaid_" + each.getAlpha_id(), "decreased");
                                } else if (before == after) {
                                    ccac_checks.put("alphaid_" + each.getAlpha_id(), "same");
                                }
                            }
                        }
                        request.setAttribute("ccac_checks", ccac_checks);
                        /**
                         * String[] maintenance =
                         * Common.getConfig("ccacMaintenance").split(",");
                         * String[] trauma =
                         * Common.getConfig("ccacTrauma").split(","); String[]
                         * malignant =
                         * Common.getConfig("ccacMalignant").split(",");
                         * String[] venous =
                         * Common.getConfig("ccacVenousLegUlcer").split(",");
                         * String[] pressure =
                         * Common.getConfig("ccacPressureUlcer").split(",");
                         * String[] arterial =
                         * Common.getConfig("ccacArterial").split(","); String[]
                         * diabetic =
                         * Common.getConfig("ccacDiabetic").split(",");
                         *
                         * //for (AbstractAssessmentVO abs : assessCollection) {
                         * //ensure this assessment was a full assessment for
                         * each alpha.. if they select create CCAC Report, they
                         * //are required to go back and full out a full
                         * assessment for that alpha. Hashtable ccac_hash = new
                         * Hashtable(); Hashtable perms = new Hashtable(); for
                         * (AssessmentEachwoundVO abs : ae) { if
                         * (abs.getFull_assessment().equals(1)) {
                         * perms.put("full_assessment", "1"); } else {
                         * perms.put("full_assessment", "0"); } WoundVO wound =
                         * woundBD.getWound(wp_id); //check if etiology is
                         * arterial, diabetic, venous, or pressuler ulcer,
                         * ensure these sections fields are filled out
                         * EtiologyVO etTMP = new EtiologyVO();
                         * etTMP.setAlpha_id(abs.getAlpha_id());
                         * Collection<EtiologyVO> etiologies =
                         * woundBD.getAllEtiologies(etTMP); for (EtiologyVO
                         * etiology : etiologies) { perms.put("arterial","0");
                         * perms.put("venous","0"); perms.put("diabetic","0");
                         * perms.put("pressure","0");
                         * perms.put("malignant","0"); perms.put("trauma","0");
                         * perms.put("maintenance","0"); if
                         * (etiology.getAlpha_id().equals(abs.getAlpha_id()) &&
                         * wound.getLocation_image().getLower_limb().equals(1))
                         * { perms.put("lower_limb", "1");
                         * put("arterial",arterial,perms,etiology);
                         * put("venous",venous,perms,etiology);
                         * put("diabetic",diabetic,perms,etiology);
                         *
                         * }
                         * put("pressure",pressure,perms,etiology);
                         * put("malignant",malignant,perms,etiology);
                         * put("maintenance",maintenance,perms,etiology);
                         * put("trauma",trauma,perms,etiology);
                         *
                         * if(perms.get("arterial").equals(1) &&
                         * perms.get("lower_limb").equals("1")){
                         * for(LimbAssessmentVO limb : resultsLA){
                         * if(limb==null){ //check for prior assessments.
                         * perms.put("lower_limb","0"); }else{
                         * if(limb.getReferral_vascular_assessment()==null){
                         * perms.put("referral_vascular","0");
                         * }else{perms.put("referral_vascular","1"); }
                         * if(limb.getLeft_ankle_brachial()!=null ||
                         * limb.getRight_ankle_brachial()!=null ){
                         * perms.put("abpi_score","1");
                         * }else{perms.put("abpi_score","0");}
                         * perms.put("lower_limb","1"); } } } }
                         *
                         * //if malignant ensure fields are filled out.
                         *
                         *
                         * ccac_hash.put(abs.getAlphaLocation().getAlpha(),
                         * perms);
                         *
                         * }
                         * session.setAttribute("ccac_hash",ccac_hash);
                         */
                    }

                    ReferralsTrackingServiceImpl referrals = new ReferralsTrackingServiceImpl(language);

                    String strAutoReferral = "0";
                    String strAutoReferralReason = "";

                    // Check previous assessments to see if an auto referral is necessary
                    Date lngTimestampNow = new Date();
                    String referral_num = "";
                    if (Common.isOffline() != true && (Common.getConfig("makeAutoReferralsHealrate").equals("1"))) {
                        strAutoReferralReason = wamanager.checkHealRateAutoReferral(lngTimestampNow, woundGoals, wound_id, userVO, -1, language);
                        if (!strAutoReferralReason.equals("")) {
                            strAutoReferral = "1";
                            referral_num = referral_num + "6";//legacy..
                        }
                    }

                    if (Common.isOffline() != true && Common.getConfig("makeAutoReferralsDressing").equals("1")) {

                        String reason = wamanager.checkDcfAutoReferral(tmpWP, liveWP, wound_id, woundGoals, userVO, -1, language);
                        if (!reason.equals("")) {
                            strAutoReferral = "1";
                            strAutoReferralReason = strAutoReferralReason + "; " + reason;
                            referral_num = referral_num + "7";//legacy.. 

                        }

                    }
                    if (Common.isOffline() != true && Common.getConfig("makeAutoReferralAntimicrobial").equals("1")) {
                        String reason = wamanager.checkAntimicrobialAutoReferral(tmpWP, liveWP, woundGoals, wound_id, userVO, -1, language);
                        if (!reason.equals("")) {
                            strAutoReferral = "1";
                            strAutoReferralReason = strAutoReferralReason + "; " + reason;
                            referral_num = referral_num + "8";//legacy..
                        }
                    }
                    if (!referral_num.equals("")) {
                        request.setAttribute("auto_referral", referral_num);
                    }

                    if (!strAutoReferralReason.equals("")) {
                        strAutoReferralReason = Common.getLocalizedString("pixalere.summary.form.autoreferral_comment", locale).trim() + strAutoReferralReason + ".";
                    }
                    LookupVO lookupVO = new LookupVO();
                    request.setAttribute("priority", listmanager.getLists(LookupVO.PRIORITY));
                    //check if referral positions requires images
                    Collection<PositionVO> positions = userBD.getReferralPositions();

                    request.setAttribute("autoreferral", strAutoReferral);

                    request.setAttribute("common", new Common());
                    request.setAttribute("woundassessments", woundAssessments);
                    request.setAttribute("ostomyassessments", ostomyAssessments);
                    request.setAttribute("incisionassessments", incisionAssessments);
                    request.setAttribute("drainassessments", drainAssessments);
                    request.setAttribute("burnassessments", burnAssessments);
                    request.setAttribute("skinassessments", skinAssessments);
                    session.setAttribute("wpti_drain", "");
                    session.setAttribute("wpti_incision", "");
                    session.setAttribute("wpti_wound", "");
                    session.setAttribute("wpti_ostomy", "");
                    session.setAttribute("wpti_burn", "");
                    ReferralsTrackingVO ref = new ReferralsTrackingVO();
                    ref.setAssessment_id(new Integer(assessment_id));
                    ref.setEntry_type(0); // "Referral"
                    ref.setCurrent(new Integer(1));
                    Hashtable refs = (Hashtable) session.getAttribute("referrals");
                    if (refs == null) {
                        refs = new Hashtable();
                    }
                    if (woundAssessments.length > 0) {

                        WoundProfileTypeVO wptv = new WoundProfileTypeVO();
                        wptv.setPatient_id(new Integer(patient_id));
                        wptv.setWound_id(new Integer(wound_id));
                        wptv.setAlpha_type(Constants.WOUND_PROFILE_TYPE);
                        WoundProfileTypeVO t = woundBD.getWoundProfileType(wptv);
                        if (t != null) {
                            ref.setWound_profile_type_id(t.getId());
                            ReferralsTrackingVO referral = (ReferralsTrackingVO) refs.get(t.getId() + "");
                            if (referral != null) {
                                request.setAttribute("wound_referral", referral);
                            }
                            session.setAttribute("wpti_wound", t.getId() + "");
                            //Remove afer vlad effect is fixed.
                            LogAuditVO logaudit2 = new LogAuditVO();
                            logaudit2.setAction("INFO");
                            logaudit2.setCreated_on(new Date());
                            logaudit2.setDescription("On Summary Setup: Wound_profile_type_id: " + session.getAttribute("wpti_wound") + " Wound_ID: " + wound_id + " Session OBJ: " + session + "\n\n" + (String) request.getHeader("User-Agent") + "\n\n");
                            logaudit2.setIp_address((String) request.getRemoteAddr());
                            logaudit2.setPatientId(new Integer(patient_id));
                            logaudit2.setProfessionalId(userVO.getId());
                            logservice.saveLogAudit(logaudit2);
                        }
                        //Getting Comments
                        AssessmentCommentsVO comment = null;
                        AssessmentCommentsVO comm = new AssessmentCommentsVO();
                        comm.setAssessment_id(new Integer(assessment_id));
                        comm.setWound_profile_type_id(t.getId());
                        comm.setComment_type(Constants.COMMENT_TYPE_REFERRALNOTE);
                        comment = cmanager.getComment(comm);
                        if (comment == null) {
                            comment = new AssessmentCommentsVO();
                            comment.setBody(strAutoReferralReason);
                        } else if (comment.getBody().indexOf("Pixalere Generated Referral") != -1) {
                            comment.setBody(strAutoReferralReason);//reset referral so we don't keep an old one.
                        } else if (!strAutoReferralReason.equals("")) {
                            comment.setBody(strAutoReferralReason + " \n" + comment.getBody());//if comment exists, and auto referral, append.
                        }
                        request.setAttribute("wound_referral_comment", comment);
                        comm.setComment_type(Constants.COMMENT_TYPE_NOTE);
                        comment = cmanager.getComment(comm);
                        request.setAttribute("wound_comment", comment);
                    }
                    if (ostomyAssessments.length > 0) {
                        WoundProfileTypeVO wptv = new WoundProfileTypeVO();
                        wptv.setPatient_id(new Integer(patient_id));
                        wptv.setWound_id(new Integer(wound_id));
                        wptv.setAlpha_type(Constants.OSTOMY_PROFILE_TYPE);
                        WoundProfileTypeVO t = woundBD.getWoundProfileType(wptv);
                        session.setAttribute("wpti_ostomy", t.getId() + "");
                        ref.setWound_profile_type_id(t.getId());
                        ReferralsTrackingVO referral = (ReferralsTrackingVO) refs.get(t.getId() + "");
                        if (referral != null) {
                            request.setAttribute("ostomy_referral", referral);
                        }
                        //Getting Comments
                        AssessmentCommentsVO comment = null;
                        AssessmentCommentsVO comm = new AssessmentCommentsVO();
                        comm.setAssessment_id(new Integer(assessment_id));
                        comm.setWound_profile_type_id(t.getId());
                        comm.setComment_type(Constants.COMMENT_TYPE_REFERRALNOTE);
                        comment = cmanager.getComment(comm);
                        if (comment == null) {
                            comment = new AssessmentCommentsVO();
                            // comment.setBody(strAutoReferralReason);
                        }
                        request.setAttribute("ostomy_referral_comment", comment);
                        comm.setComment_type(Constants.COMMENT_TYPE_NOTE);
                        comment = cmanager.getComment(comm);
                        request.setAttribute("ostomy_comment", comment);
                    }
                    if (skinAssessments.length > 0) {
                        WoundProfileTypeVO wptv = new WoundProfileTypeVO();
                        wptv.setPatient_id(new Integer(patient_id));
                        wptv.setWound_id(new Integer(wound_id));
                        wptv.setAlpha_type(Constants.SKIN_PROFILE_TYPE);
                        WoundProfileTypeVO t = woundBD.getWoundProfileType(wptv);
                        session.setAttribute("wpti_skin", t.getId() + "");
                        ref.setWound_profile_type_id(t.getId());
                        ReferralsTrackingVO referral = (ReferralsTrackingVO) refs.get(t.getId() + "");
                        if (referral != null) {
                            request.setAttribute("skin_referral", referral);
                        }
                        //Getting Comments
                        AssessmentCommentsVO comment = null;
                        AssessmentCommentsVO comm = new AssessmentCommentsVO();
                        comm.setAssessment_id(new Integer(assessment_id));
                        comm.setWound_profile_type_id(t.getId());
                        comm.setComment_type(Constants.COMMENT_TYPE_REFERRALNOTE);
                        comment = cmanager.getComment(comm);
                        if (comment == null) {
                            comment = new AssessmentCommentsVO();
                            //comment.setBody(strAutoReferralReason);
                        }
                        request.setAttribute("skin_referral_comment", comment);
                        comm.setComment_type(Constants.COMMENT_TYPE_NOTE);
                        comment = cmanager.getComment(comm);
                        request.setAttribute("skin_comment", comment);
                    }
                    if (incisionAssessments.length > 0) {
                        WoundProfileTypeVO wptv = new WoundProfileTypeVO();
                        wptv.setPatient_id(new Integer(patient_id));
                        wptv.setWound_id(new Integer(wound_id));
                        wptv.setAlpha_type(Constants.INCISIONTAG_PROFILE_TYPE);
                        WoundProfileTypeVO t = woundBD.getWoundProfileType(wptv);
                        if (t != null) {
                            session.setAttribute("wpti_incision", t.getId() + "");
                            ref.setWound_profile_type_id(t.getId());
                            ReferralsTrackingVO referral = (ReferralsTrackingVO) refs.get(t.getId() + "");
                            if (referral != null) {
                                request.setAttribute("incision_referral", referral);
                            }
                        }
                        //Getting Comments
                        AssessmentCommentsVO comment = null;
                        AssessmentCommentsVO comm = new AssessmentCommentsVO();
                        comm.setAssessment_id(new Integer(assessment_id));
                        comm.setWound_profile_type_id(t.getId());
                        comm.setComment_type(Constants.COMMENT_TYPE_REFERRALNOTE);
                        comment = cmanager.getComment(comm);
                        if (comment == null) {
                            comment = new AssessmentCommentsVO();
                            //comment.setBody(strAutoReferralReason);
                        }
                        request.setAttribute("incision_referral_comment", comment);
                        comm.setComment_type(Constants.COMMENT_TYPE_NOTE);
                        comment = cmanager.getComment(comm);
                        request.setAttribute("incision_comment", comment);
                    }
                    if (drainAssessments.length > 0) {
                        WoundProfileTypeVO wptv = new WoundProfileTypeVO();
                        wptv.setPatient_id(new Integer(patient_id));
                        wptv.setWound_id(new Integer(wound_id));
                        wptv.setAlpha_type(Constants.TUBESANDDRAINS_PROFILE_TYPE);
                        WoundProfileTypeVO t = woundBD.getWoundProfileType(wptv);
                        if (t != null) {
                            session.setAttribute("wpti_drain", t.getId() + "");
                            ref.setWound_profile_type_id(t.getId());
                            ReferralsTrackingVO referral = (ReferralsTrackingVO) refs.get(t.getId() + "");
                            if (referral != null) {
                                request.setAttribute("drain_referral", referral);
                            }
                        }
                        //Getting Comments
                        AssessmentCommentsVO comment = null;
                        AssessmentCommentsVO comm = new AssessmentCommentsVO();
                        comm.setAssessment_id(new Integer(assessment_id));
                        comm.setWound_profile_type_id(t.getId());
                        comm.setComment_type(Constants.COMMENT_TYPE_REFERRALNOTE);
                        comment = cmanager.getComment(comm);
                        if (comment == null) {
                            comment = new AssessmentCommentsVO();
                            //comment.setBody(strAutoReferralReason);
                        }
                        request.setAttribute("drain_referral_comment", comment);
                        comm.setComment_type(Constants.COMMENT_TYPE_NOTE);
                        comment = cmanager.getComment(comm);
                        request.setAttribute("drain_comment", comment);
                    }
                    if (burnAssessments.length > 0) {
                        WoundProfileTypeVO wptv = new WoundProfileTypeVO();
                        wptv.setPatient_id(new Integer(patient_id));
                        wptv.setWound_id(new Integer(wound_id));
                        wptv.setAlpha_type(Constants.BURN_PROFILE_TYPE);
                        WoundProfileTypeVO t = woundBD.getWoundProfileType(wptv);
                        if (t != null) {
                            session.setAttribute("wpti_burn", t.getId() + "");
                            ref.setWound_profile_type_id(t.getId());
                            ReferralsTrackingVO referral = referrals.getReferralsTracking(ref);
                            request.setAttribute("burn_referral", referral);
                        }
                        //Getting Comments
                        AssessmentCommentsVO comment = null;
                        AssessmentCommentsVO comm = new AssessmentCommentsVO();
                        comm.setAssessment_id(new Integer(assessment_id));
                        comm.setWound_profile_type_id(t.getId());
                        comm.setComment_type(Constants.COMMENT_TYPE_REFERRALNOTE);
                        comment = cmanager.getComment(comm);
                        if (comment == null) {
                            comment = new AssessmentCommentsVO();
                            // comment.setBody(strAutoReferralReason);
                        }
                        request.setAttribute("burn_referral_comment", comment);
                        comm.setComment_type(Constants.COMMENT_TYPE_NOTE);
                        comment = cmanager.getComment(comm);
                        request.setAttribute("burn_comment", comment);
                    }

                    request.setAttribute("summary_alphas", alphaStore);

                    //list of unfinished alphas ("A", "B", "C",...)
                    AssessmentEachwoundVO ass = new AssessmentEachwoundVO();
                    ass.setAssessment_id(new Integer(assessment_id));
                    ass.setWound_id(new Integer(wound_id));
                    List<String> unfinishedAlphas = locBD.computeUncompletedAlphas(ass, userVO.getId());

                    StringBuilder unfinishedAlphasString = new StringBuilder();
                    if (unfinishedAlphas != null) {
                        for (String unfinishedAlpha : unfinishedAlphas) {
                            unfinishedAlphasString.append('\"');
                            unfinishedAlphasString.append(unfinishedAlpha);
                            unfinishedAlphasString.append('\"');
                            unfinishedAlphasString.append(',');
                        }
                    }
                    if (unfinishedAlphas != null && unfinishedAlphas.size() > 0) {
                        request.setAttribute("unfinishedAlphas", unfinishedAlphasString.substring(0, unfinishedAlphasString.length() - 1));
                    } else {
                        request.setAttribute("unfinishedAlphas", "");
                    }

                    WoundAssessmentLocationVO selected_alpha = null;
                    int alcnt = 0;
                    for (Enumeration e = alphaStore.keys(); e.hasMoreElements();) {
                        if (alcnt == 0) {
                            String k = (String) e.nextElement();
                            request.setAttribute("default_alpha", k);
                            selected_alpha = (WoundAssessmentLocationVO) alphaStore.get(k);
                            break;
                        }
                        alcnt++;
                    }
                    request.setAttribute("selected_alpha", selected_alpha);

                    if (assessment_id != null) {
                        AssessmentImagesVO ig = new AssessmentImagesVO();
                        ig.setAssessment_id(new Integer(assessment_id));
                        Collection<AssessmentImagesVO> imagesVO = imagesBD.getAllImages(ig);
                        if (imagesVO != null && imagesVO.size() != 0) {
                            request.setAttribute("images", Common.mergeImages(imagesVO, true, locale));
                        }
                        if (Common.getConfig("allowReferralByPositions").equals("1")) {
                            //check each of the images
                            //if a position requires images, remove them from list.
                            List<PositionVO> no_img_positions = new ArrayList();
                            String positions_removed = "";
                            List<PositionVO> positionsWithAccess = new ArrayList();
                            //checking if the professional with the correct position
                            //has access to this patient.
                            for (PositionVO pos : positions) {
                                Collection<ProfessionalVO> po = userBD.getProfessionalsByAccess(pos.getId(), pa.getTreatment_location_id());
                                if (po != null && po.size() > 0) {
                                    positionsWithAccess.add(pos);
                                }
                            }
                            for (PositionVO pos : positionsWithAccess) {
                                if (pos.getEnforce_images() == null || !pos.getEnforce_images().equals(new Integer(1))) {
                                    no_img_positions.add(pos);
                                } else {
                                    positions_removed = positions_removed + " " + pos.getTitle() + ", ";
                                }
                            }
                            if (positionsWithAccess.size() != no_img_positions.size()) {
                                if (positions_removed.length() > 1) {
                                    positions_removed = positions_removed.substring(0, positions_removed.length() - 1);
                                }
                                request.setAttribute("positions_removed", positions_removed + Common.getLocalizedString("pixalere.summary.imgs_required_by_referral", locale));
                            }
                            if (imagesVO == null || imagesVO.size() == 0) {
                                request.setAttribute("referral_position_wound", no_img_positions);
                                request.setAttribute("referral_position_skin", no_img_positions);
                                request.setAttribute("referral_position_drain", no_img_positions);
                                request.setAttribute("referral_position_incision", no_img_positions);
                                request.setAttribute("referral_position_ostomy", no_img_positions);
                                request.setAttribute("no_images_wound", "true");
                                request.setAttribute("no_images_skin", "true");
                                request.setAttribute("no_images_ostomy", "true");
                                request.setAttribute("no_images_drain", "true");
                                request.setAttribute("no_images_incision", "true");
                            } else {
                                //cycle through and check each care type for imgs
                                for (AssessmentImagesVO img : imagesVO) {
                                    boolean img_check = false;
                                    if (woundAssessments.length > 0) {
                                        //if image is of type "Wound", no_images=false
                                        if (((String) session.getAttribute("wpti_wound")).equals(img.getWound_profile_type_id() + "")) {
                                            img_check = true;
                                        }
                                    }
                                    if (img_check) {
                                        request.setAttribute("referral_position_wound", positionsWithAccess);
                                    } else {

                                        request.setAttribute("referral_position_wound", no_img_positions);
                                    }
                                    img_check = false;
                                    if (ostomyAssessments.length > 0) {
                                        //if image is of type "Wound", no_images=false
                                        if (((String) session.getAttribute("wpti_ostomy")).equals(img.getWound_profile_type_id() + "")) {
                                            img_check = true;
                                        }
                                    }
                                    if (img_check) {
                                        request.setAttribute("referral_position_ostomy", positions);
                                    } else {
                                        request.setAttribute("no_images_ostomy", "true");
                                        request.setAttribute("referral_position_ostomy", no_img_positions);
                                    }
                                    img_check = false;
                                    if (drainAssessments.length > 0) {
                                        //if image is of type "Wound", no_images=false
                                        if (((String) session.getAttribute("wpti_drain")).equals(img.getWound_profile_type_id() + "")) {
                                            img_check = true;
                                        }
                                    }
                                    if (img_check) {
                                        request.setAttribute("referral_position_drain", positions);
                                    } else {
                                        request.setAttribute("no_images_drain", "true");
                                        request.setAttribute("referral_position_drain", no_img_positions);
                                    }
                                    img_check = false;
                                    if (incisionAssessments.length > 0) {
                                        //if image is of type "Wound", no_images=false
                                        if (((String) session.getAttribute("wpti_incision")).equals(img.getWound_profile_type_id() + "")) {
                                            img_check = true;
                                        }
                                    }
                                    if (img_check) {
                                        request.setAttribute("referral_position_incision", positions);
                                    } else {
                                        request.setAttribute("no_images_incision", "true");
                                        request.setAttribute("referral_position_incision", no_img_positions);
                                    }
                                    img_check = false;
                                    if (skinAssessments.length > 0) {
                                        //if image is of type "Wound", no_images=false
                                        if (((String) session.getAttribute("wpti_skin")).equals(img.getWound_profile_type_id() + "")) {
                                            img_check = true;
                                        }
                                    }
                                    if (img_check) {
                                        request.setAttribute("referral_position_skin", positions);
                                    } else {
                                        request.setAttribute("no_images_skin", "true");
                                        request.setAttribute("referral_position_skin", no_img_positions);
                                    }
                                    img_check = false;
                                }
                            }
                        }
                    }
                }
            } else {
                session.setAttribute("wound_changed", "false");
            }
            if (Common.isOffline() == true) {
                session.setAttribute("offlineVO", offline);
            }
        } catch (Exception e) {
            //Integer patient_id = (Integer) session.getAttribute("patient_id");
            log.error("An application exception has been raised in SummarySetupAction.perform(): for Patient_ID:" + session.getAttribute("patient_id") + " " + e.toString());
            e.printStackTrace();
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward(
                "uploader.summary.success"));

    }

    public Hashtable put(String key, String[] array, Hashtable hash, EtiologyVO etiology) {
    	System.out.println("in hashtable put mathod");
        hash.put(key, "0");
        for (String art : array) {
            if (art != null && !art.equals("")) {
                if (etiology.getLookup_id().equals(new Integer(art))) {
                    hash.put(key, "1");
                }
            }
        }
        return hash;
    }
}
