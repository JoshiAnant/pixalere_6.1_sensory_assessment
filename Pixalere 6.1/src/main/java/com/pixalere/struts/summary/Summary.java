package com.pixalere.struts.summary;

import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;

import javax.servlet.ServletContext;

import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.reporting.bean.CCACReportVO;
import com.pixalere.reporting.service.ReportingServiceImpl;

import java.util.Date;

import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.patient.bean.DiagnosticInvestigationVO;
import com.pixalere.assessment.bean.AssessmentProductVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.common.bean.PatientReportVO;
import com.pixalere.common.service.PatientReportService;
import com.pixalere.common.ApplicationException;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.pixalere.common.bean.OfflineVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.mail.SendMailUsingAuthentication;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.patient.bean.LimbUpperAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PhysicalExamVO;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.*;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.bean.WoundVO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Collection;
import java.util.Hashtable;


public class Summary extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Summary.class);


    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

        SummaryForm summaryForm = (SummaryForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        ServletContext context = session.getServletContext();
        OfflineVO offline = new OfflineVO();
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        PatientAccountVO patientAccount = (PatientAccountVO) session.getAttribute("patientAccount");
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");

        if (isTokenValid(request)) {
            resetToken(request);
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
                
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            boolean isOffline = Common.isOffline();
            AssessmentServiceImpl amanager = new AssessmentServiceImpl(language);
            LogAuditServiceImpl logservice = new LogAuditServiceImpl();
            NursingCarePlanServiceImpl nmanager = new NursingCarePlanServiceImpl();
            WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
            WoundAssessmentLocationServiceImpl walmanager = new WoundAssessmentLocationServiceImpl();
            PatientProfileServiceImpl managerp = new PatientProfileServiceImpl(language);
            WoundServiceImpl managerw = new WoundServiceImpl(language);
            PatientServiceImpl managerpa = new PatientServiceImpl(language);
            ReferralsTrackingServiceImpl refBD = new ReferralsTrackingServiceImpl(language);
            TreatmentCommentsServiceImpl tcBD = new TreatmentCommentsServiceImpl();
            ListServiceImpl lservice = new ListServiceImpl(language);
            ReportingServiceImpl rservice = new ReportingServiceImpl();
             
                                
            String assessment_id = null;
            if (wound_id != null && !wound_id.equals("")) {
                WoundAssessmentVO atmp = new WoundAssessmentVO();
                atmp.setWound_id(new Integer(wound_id));
                atmp.setActive(0);
                atmp.setProfessional_id(userVO.getId());
                try {
                    WoundAssessmentVO tmpAssess = wamanager.getAssessment(atmp);
                    if (tmpAssess != null) {
                        assessment_id = tmpAssess.getId() + "";
                    }
                    //Remove afer vlad effect is fixed.
             LogAuditVO logaudit2 = new LogAuditVO();
                                logaudit2.setAction("INFO");
                                logaudit2.setCreated_on(new Date());
                                logaudit2.setDescription("On Summary Save: Wound_profile_type_id:"+session.getAttribute("wpti_wound")+" Wound_ID: "+wound_id+" Session OBJ: "+session+"\n\n"+(String)request.getHeader("User-Agent"));
                                logaudit2.setIp_address((String)request.getRemoteAddr());
                                logaudit2.setPatientId(new Integer(patient_id));
                                logaudit2.setProfessionalId(userVO.getId());
                                logservice.saveLogAudit(logaudit2);  
                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in Summary.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
            }
            if (isOffline == true && session.getAttribute("offlineVO") != null) {
                offline = (OfflineVO) session.getAttribute("offlineVO");
            }
            if (((String) request.getParameter("entries_submitted")).equals("1")) {


                try {
                    int hours = 0;
                    int minutes = 0;
                    try {
                        if (summaryForm.getBackdated_time() != null && !summaryForm.getBackdated_time().equals("")) {
                            String timeage = summaryForm.getBackdated_time();

                            if (timeage!=null && !timeage.equals("") && timeage.length()==4) {
                                hours = Integer.parseInt(timeage.substring(0,2));
                                minutes = Integer.parseInt(timeage.substring(2,4));
                            } else {
                                minutes = Integer.parseInt(pdate.getMin(new Date()));
                                hours = Integer.parseInt(pdate.getHour(new Date()));
                            }

                        }

                    } catch (NumberFormatException exxe) {
                    }



                    Date backdated_timestamp = PDate.getDate(summaryForm.getBackdated_month(), summaryForm.getBackdated_day(), summaryForm.getBackdated_year(), hours, minutes);

                    String user_signature = "";
                    if (backdated_timestamp == null) {
                        backdated_timestamp = new Date();
                        try {
                            user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
                        } catch (ApplicationException e) {
                            e.printStackTrace();
                        }

                    } else {
                        session.setAttribute("backdate", backdated_timestamp);
                        try {
                            user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
                        } catch (ApplicationException e) {
                            e.printStackTrace();
                        }

                    }
                    if (patient_id == null || patient_id.equals("")) {
                        patient_id = "0";
                    }

                    //PatientAccountVO patientVO = managerpa.getPatient(Integer.parseInt(patient_id));
                    Date closed_date = null;


                    //if (Common.getPatientChanged() == true)
                    if (session.getAttribute("foot_changed") != null && session.getAttribute("foot_changed").equals("true")) {
                        FootAssessmentVO f = new FootAssessmentVO();
                        f.setPatient_id(new Integer((String) patient_id));
                        f.setProfessional_id(userVO.getId());
                        f.setActive(new Integer(0));
                        if (isOffline == true) {
                            offline.setFoot_changed(1);
                        }
                        FootAssessmentVO foot = managerp.getFootAssessment(f);
                        if (foot != null) {
                            foot.setActive(1);
                            foot.setVisited_on(backdated_timestamp);
                            foot.setCreated_on(new Date());
                            foot.setLastmodified_on(new Date());


                            foot.setUser_signature(user_signature);
                            managerp.saveFoot(foot);
                        }
                    }
                    
                    if (session.getAttribute("sensory_changed") != null && session.getAttribute("sensory_changed").equals("true")) {
                        SensoryAssessmentVO sensoryAssessmentVO = new SensoryAssessmentVO();
                        sensoryAssessmentVO.setPatient_id(new Integer((String) patient_id));
                        sensoryAssessmentVO.setProfessional_id(userVO.getId());
                        sensoryAssessmentVO.setActive(new Integer(0));
                        if (isOffline == true) {
                            offline.setSensory_changed(1);
                        }
                        SensoryAssessmentVO sensoryAssesmentVO = managerp.getSensoryAssessment(sensoryAssessmentVO);
                        if (sensoryAssesmentVO != null) {
                            sensoryAssesmentVO.setActive(1);
                            sensoryAssesmentVO.setVisited_on(backdated_timestamp);
                            sensoryAssesmentVO.setCreated_on(new Date());
                            sensoryAssesmentVO.setLastmodified_on(new Date());
                            sensoryAssesmentVO.setUser_signature(user_signature);
                            managerp.saveSensory(sensoryAssesmentVO);
                        }
                    }
                    if (session.getAttribute("diag_changed") != null && session.getAttribute("diag_changed").equals("true")) {
                        DiagnosticInvestigationVO f = new DiagnosticInvestigationVO();
                        f.setPatient_id(new Integer((String) patient_id));
                        f.setProfessional_id(userVO.getId());
                        f.setActive(new Integer(0));
                        if (isOffline == true) {
                            offline.setDiagnostic_changed(1);
                        }
                        DiagnosticInvestigationVO foot = managerp.getDiagnosticInvestigation(f);
                        if (foot != null) {
                            foot.setActive(1);
                            foot.setVisited_on(backdated_timestamp);
                            foot.setCreated_on(new Date());
                            foot.setLastmodified_on(new Date());


                            foot.setUser_signature(user_signature);
                            managerp.saveDiagnosticInvestigation(foot);
                        }
                    }
                    if (session.getAttribute("exam_changed") != null && session.getAttribute("exam_changed").equals("true")) {
                        PhysicalExamVO f = new PhysicalExamVO();
                        f.setPatient_id(new Integer((String) patient_id));
                        f.setProfessional_id(userVO.getId());
                        f.setActive(new Integer(0));
                        if (isOffline == true) {
                            offline.setExam_changed(1);
                        }
                        PhysicalExamVO exam = managerp.getExam(f);
                        if (exam != null) {
                            exam.setActive(1);

                            exam.setVisited_on(backdated_timestamp);
                            exam.setCreated_on(new Date());
                            exam.setLastmodified_on(new Date());
                            exam.setCurrent_flag(1);

                            exam.setUser_signature(user_signature);
                            managerp.updatePhysicalExam(exam);
                        }
                    }
                    if (session.getAttribute("limb_changed") != null && session.getAttribute("limb_changed").equals("true")) {
                        LimbBasicAssessmentVO l = new LimbBasicAssessmentVO();
                        l.setPatient_id(new Integer((String) patient_id));
                        l.setProfessional_id(userVO.getId());
                        l.setActive(new Integer(0));
                        LimbBasicAssessmentVO limb = managerp.getLimbBasicAssessment(l);
                        if (isOffline == true) {
                            offline.setLimb_changed(1);
                        }
                        if (limb != null) {
                            limb.setActive(1);
                            limb.setVisited_on(backdated_timestamp);
                            limb.setCreated_on(new Date());

                            limb.setLastmodified_on(new Date());


                            limb.setUser_signature(user_signature);
                            managerp.saveBasicLimb(limb);
                        }
                    }
                    if (session.getAttribute("limb_adv_changed") != null && session.getAttribute("limb_adv_changed").equals("true")) {
                        LimbAdvAssessmentVO l = new LimbAdvAssessmentVO();
                        l.setPatient_id(new Integer((String) patient_id));
                        l.setProfessional_id(userVO.getId());
                        l.setActive(new Integer(0));
                        LimbAdvAssessmentVO limb = managerp.getLimbAdvAssessment(l);
                        if (isOffline == true) {
                            offline.setLimb_adv_changed(1);
                        }
                        if (limb != null) {
                            limb.setActive(1);
                            limb.setVisited_on(backdated_timestamp);
                            limb.setCreated_on(new Date());
                            limb.setLastmodified_on(new Date());


                            limb.setUser_signature(user_signature);
                            managerp.saveAdvLimb(limb);
                        }
                    }
                    if (session.getAttribute("limb_upper_changed") != null && session.getAttribute("limb_upper_changed").equals("true")) {
                        LimbUpperAssessmentVO l = new LimbUpperAssessmentVO();
                        l.setPatient_id(new Integer((String) patient_id));
                        l.setProfessional_id(userVO.getId());
                        l.setActive(new Integer(0));
                        LimbUpperAssessmentVO limb = managerp.getLimbUpperAssessment(l);
                        if (isOffline == true) {
                            offline.setLimb_upper_changed(1);
                        }
                        if (limb != null) {
                            limb.setActive(1);
                            limb.setVisited_on(backdated_timestamp);
                            limb.setCreated_on(new Date());

                            limb.setLastmodified_on(new Date());


                            limb.setUser_signature(user_signature);
                            managerp.saveUpperLimb(limb);
                        }
                    }
                    if (session.getAttribute("braden_changed") != null && session.getAttribute("braden_changed").equals("true")) {
                        BradenVO l = new BradenVO();
                        l.setPatient_id(new Integer((String) patient_id));
                        l.setProfessional_id(userVO.getId());
                        l.setActive(new Integer(0));
                        BradenVO b = managerp.getBraden(l);
                        if (isOffline == true) {
                            offline.setBraden_changed(1);
                        }
                        if (b != null) {
                            b.setActive(1);
                            b.setVisited_on(backdated_timestamp);
                            b.setCreated_on(new Date());
                            b.setLastmodified_on(new Date());

                            b.setUser_signature(user_signature);
                            managerp.saveBraden(b);
                        }
                    }
                    if (session.getAttribute("patient_changed") != null && session.getAttribute("patient_changed") == "true") {
                        PatientProfileVO tmp = new PatientProfileVO();
                        tmp.setPatient_id(new Integer((String) patient_id));
                        tmp.setProfessional_id(userVO.getId());
                        tmp.setActive(new Integer(0));
                        PatientProfileVO patient = managerp.getPatientProfile(tmp);
                        if (isOffline == true) {
                            offline.setPatientprofile_changed(1);
                        }
                        if (patient != null) {
                            patient.setActive(new Integer(1));
                            patient.setOffline_flag(isOffline == true ? 1 : 0);
                            patient.setUser_signature(user_signature);
                            patient.setVisited_on(backdated_timestamp);
                            patient.setCreated_on(new Date());
                            patient.setLastmodified_on(new Date());


                            managerp.updatePatientProfile(patient);
                        }

                    }
                    if (isOffline == true) {
                        offline.setPatient_id(new Integer(patient_id + ""));
                    }
                    Collection<AbstractAssessmentVO> assessCollection = null;
                    if (assessment_id != null) {
                        assessCollection = amanager.getAllAssessments(Integer.parseInt(assessment_id));
                    }
                    if (wound_id != null && !wound_id.equals("")) {
                        //need to set closed alphas to discharge in locationsdao here. cycle through currently
                        //assessed alphas and if status == closed then set that alphas to discharge = 1 in location
                        //-----discharging alphas in wound_assessment_location
                        if (assessment_id != null) {
                            //flip active flag for ncp
                            NursingCarePlanVO ncpt = new NursingCarePlanVO();
                            ncpt.setAssessment_id(new Integer(assessment_id));
                            Collection<NursingCarePlanVO> ncprecords = nmanager.getAllNursingCarePlans(ncpt, 0);
                            for (NursingCarePlanVO ncprecord : ncprecords) {
                                if (ncprecord != null && ncprecord.getBody() != null) {
                                    ncprecord.setActive(new Integer(1));
                                    ncprecord.setVisited_on(backdated_timestamp);
                                    ncprecord.setCreated_on(new Date());

                                    ncprecord.setUser_signature(user_signature);
                                    nmanager.saveNursingCarePlan(ncprecord);
                                }
                            }
                            //Treatment Comments
                            TreatmentCommentVO tc = new TreatmentCommentVO();
                            tc.setAssessment_id(new Integer(assessment_id));
                            Collection<TreatmentCommentVO> treats = tcBD.getAllTreatmentComments(tc, 0);
                            for (TreatmentCommentVO t : treats) {
                                if (t != null && t.getBody() != null) {
                                    t.setActive(1);
                                    t.setCreated_on(backdated_timestamp);

                                    t.setUser_signature(user_signature);
                                    tcBD.saveTreatmentComment(t);
                                }
                            }
                            //DressingChangeFrequency
                            DressingChangeFrequencyVO dcf = new DressingChangeFrequencyVO();
                            dcf.setAssessment_id(new Integer(assessment_id));
                            Collection<DressingChangeFrequencyVO> dcfs = nmanager.getAllDressingChangeFrequencies(dcf, 0);
                            for (DressingChangeFrequencyVO t : dcfs) {
                                if (t != null && t.getDcf() != null) {
                                    t.setActive(1);
                                    t.setCreated_on(backdated_timestamp);

                                    t.setUser_signature(user_signature);
                                    nmanager.saveDressingChangeFrequency(t);
                                }
                            }

                            //TODO: change code to handle ostomy in collection.
                            //Collection<AbstractAssessmentVO> assessCollection = amanager.getAllAssessments(Integer.parseInt(assessment_id));
                            for (AbstractAssessmentVO assessVO : assessCollection) {
                                //Get closure_date of alpha for closure_of_woundprofile
                                try {
                                    if (assessVO.getClosed_date() != null && (closed_date == null || closed_date.before(assessVO.getClosed_date()))) {
                                        closed_date = assessVO.getClosed_date();
                                    }
                                } catch (NullPointerException e) {
                                    log.error("NumberFormatException:  Summary: " + e.getMessage());
                                } catch (NumberFormatException e) {
                                    log.error("NumberFormatException:  Summary: " + e.getMessage());
                                }
                                assessVO.setActive(new Integer(1));
                                assessVO.setVisited_on(backdated_timestamp);
                                assessVO.setCreated_on(new Date());
                                assessVO.setLastmodified_on(new Date());

                                if (assessVO.getWound_profile_type().getAlpha_type().equals("A")) {
                                    assessVO.setPriority(new Integer(summaryForm.getPriority_wound()));
                                } else if (assessVO.getWound_profile_type().getAlpha_type().equals("D")) {
                                    assessVO.setPriority(new Integer(summaryForm.getPriority_drain()));
                                } else if (assessVO.getWound_profile_type().getAlpha_type().equals("I")) {
                                    assessVO.setPriority(new Integer(summaryForm.getPriority_incision()));
                                } else if (assessVO.getWound_profile_type().getAlpha_type().equals("O")) {
                                    assessVO.setPriority(new Integer(summaryForm.getPriority_ostomy()));

                                } else if (assessVO.getWound_profile_type().getAlpha_type().equals("B")) {
                                    assessVO.setPriority(new Integer(summaryForm.getPriority_burn()));

                                } else if (assessVO.getWound_profile_type().getAlpha_type().equals("S")) {
                                    assessVO.setPriority(new Integer(summaryForm.getPriority_skin()));
                                }
                                //amanager.addAssessmentEachwound(assessVO);
                                AssessmentProductVO tp = new AssessmentProductVO();
                                tp.setAssessment_id(assessVO.getAssessment_id());
                                tp.setAlpha_id(assessVO.getAlpha_id());
                                Collection<AssessmentProductVO> products = amanager.getAllProducts(tp);

                                if (products != null) {
                                    for (AssessmentProductVO product : products) {
                                        product.setActive(new Integer(1));
                                        amanager.saveProduct(product);
                                    }
                                }
                                amanager.saveAssessment(assessVO);

                                if ((assessVO instanceof AssessmentIncisionVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("incisionClosed"))) || (assessVO instanceof AssessmentOstomyVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("ostomyClosed"))) || (assessVO instanceof AssessmentEachwoundVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("woundClosed"))) || (assessVO instanceof AssessmentDrainVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("drainClosed"))) || (assessVO instanceof AssessmentSkinVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("skinClosed")))) {
                                    log.info("##$$##$$##-----ALPHA IS MARKED AS CLOSED-----##$$##$$##");
                                    WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                                    locateVO.setPatient_id(new Integer((String) patient_id));
                                    locateVO.setWound_id(new Integer((String) wound_id));
                                    locateVO.setId(assessVO.getAlpha_id());
                                    WoundAssessmentLocationVO resultVO = new WoundAssessmentLocationVO();
                                    resultVO = walmanager.getAlpha(locateVO, true, false, null);

                                    resultVO.setActive(new Integer(1));
                                    resultVO.setDischarge(new Integer("1"));
                                    resultVO.setClose_timestamp(assessVO.getClosed_date());


                                    walmanager.saveAlpha(resultVO);
                                    //WoundAssessmentLocationVO resultVO2 = walmanager.getAlpha(resultVO, true, false, null);

                                } else if (assessVO instanceof AssessmentEachwoundVO) {
                                }
                            }
                        }
                        //-----end discharge


                    }

                    if (wound_id != null && !(wound_id).equals("")) {
                        //need to set closed alphas to discharge in locationsdao here. cycle through currently
                        //assessed alphas and if status == closed then set that alphas to discharge = 1 in location
                        //-----discharging alphas in wound_assessment_location
                        WoundAssessmentLocationVO loco3 = new WoundAssessmentLocationVO();

                        loco3.setWound_id(new Integer((String) wound_id));
                        loco3.setActive(new Integer(0));
                        loco3.setProfessional_id(userVO.getId());
                        Collection<WoundAssessmentLocationVO> locations3 = walmanager.getAllAlphas(loco3);

                        for (WoundAssessmentLocationVO locationVO : locations3) {

                            locationVO.setActive(new Integer("1"));
                            locationVO.setOpen_timestamp(backdated_timestamp);

                            walmanager.saveAlpha(locationVO);
                            WoundProfileTypeVO t = new WoundProfileTypeVO();
                            t.setId(locationVO.getWound_profile_type_id());
                            t.setActive(0);
                            t.setProfessional_id(userVO.getId());
                            WoundProfileTypeVO typeVO = managerw.getWoundProfileType(t);
                            if (typeVO != null) {
                                t.setActive(1);
                                managerw.saveWoundProfileType(t);
                            }

                            if(assessCollection!=null){
                            for (AbstractAssessmentVO assessVO : assessCollection) {


                                    if ((assessVO instanceof AssessmentIncisionVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("incisionClosed"))) || (assessVO instanceof AssessmentOstomyVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("ostomyClosed"))) || (assessVO instanceof AssessmentEachwoundVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("woundClosed"))) || (assessVO instanceof AssessmentDrainVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("drainClosed"))) || (assessVO instanceof AssessmentSkinVO && Integer.toString(assessVO.getStatus()).equals(Common.getConfig("skinClosed")))) {

                                        log.info("##$$##$$##-----ALPHA IS MARKED AS CLOSED-----##$$##$$##");
                                        WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                                        locateVO.setPatient_id(new Integer((String) patient_id));
                                        locateVO.setWound_id(new Integer((String) wound_id));
                                        locateVO.setId(assessVO.getAlpha_id());
                                        WoundAssessmentLocationVO resultVO = new WoundAssessmentLocationVO();
                                        resultVO = walmanager.getAlpha(locateVO, true, false, null);
                                        resultVO.setDischarge(new Integer("1"));
                                        resultVO.setClose_timestamp(backdated_timestamp);
                                        resultVO.setActive(new Integer("1"));
                                        walmanager.saveAlpha(resultVO);
                                    } else if (assessVO instanceof AssessmentEachwoundVO) {
                                        AssessmentEachwoundVO e = (AssessmentEachwoundVO) assessVO;
                                        if (e.getHeal_rate() != null) {
                                            WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                                            locateVO.setPatient_id(new Integer((String) patient_id));
                                            locateVO.setWound_id(new Integer((String) wound_id));
                                            locateVO.setId(assessVO.getAlpha_id());
                                            WoundAssessmentLocationVO resultVO = new WoundAssessmentLocationVO();
                                            resultVO = walmanager.getAlpha(locateVO, true, false, null);
                                            resultVO.setCurrent_heal_rate(e.getHeal_rate());
                                            walmanager.saveAlpha(resultVO);
                                        }
                                    }
                                }
                            }
                            
                        }


                        WoundAssessmentLocationVO loco = new WoundAssessmentLocationVO();
                        loco.setWound_id(new Integer((String) wound_id));
                        loco.setActive(new Integer(0));
                        loco.setProfessional_id(userVO.getId());
                        Collection<WoundAssessmentLocationVO> locations = walmanager.getAllAlphas(loco);
                        for (WoundAssessmentLocationVO locationVO : locations) {
                            locationVO.setActive(new Integer("1"));
                            walmanager.saveAlpha(locationVO);

                        }

                        log.info("##$$##$$##-----WOUND_CHANGED IN THE SESSION IS == " + session.getAttribute("wound_changed") + "-----##$$##$$##");
                        //checking as well if the wound profile is healed, since there could be no edits on it.
                        WoundProfileVO tmp = new WoundProfileVO();
                        tmp.setWound_id(new Integer((String) wound_id));
                        tmp.setActive(new Integer(0));
                        tmp.setProfessional_id(userVO.getId());
                        WoundProfileVO woundprofile = managerw.getWoundProfile(tmp);
                        if (woundprofile != null) {
                            woundprofile.setVisited_on(backdated_timestamp);
                            woundprofile.setCreated_on(new Date());

                            woundprofile.setUser_signature(user_signature);
                            woundprofile.setActive(new Integer(1));
                        }
                        String update_flag = "false";
                        WoundVO wound = managerw.getWound(Integer.parseInt((String) wound_id));
                        // if (wound != null && woundprofile != null) {
                        if (wound != null) {
                            
                            wound.setActive(new Integer(1));
                            managerw.saveWound(wound);


                            if (isOffline == false && session.getAttribute("wound_changed") != null && session.getAttribute("wound_changed") == "true") {
                                log.info("##$$##$$##-----updating the wound profile because it is different then the past record-----##$$##$$##");
                                //WoundProfileTMPVO wound = managerw.retrieveWoundProfileTMP((String) wound_id);
                                if (isOffline == true) {
                                    offline.setWoundprofile_changed(1);
                                }
                                managerw.saveWound(wound);
                                if (woundprofile != null) {
                                    woundprofile.setOffline_flag(Common.isOffline() == true ? 1 : 0);
                                    managerw.updateWoundProfile(woundprofile);
                                }
                                update_flag = "true";
                            }
                            if (walmanager.isWoundHealed(wound.getId().intValue(), userVO.getId().intValue()) == true) {
                                //update the woundprofile to close it since wound has no more open alphas

                                wound.setClosed_date(closed_date);
                                wound.setStatus("Closed");
                                managerw.saveWound(wound);

                                if (woundprofile != null) {
                                    woundprofile.setOffline_flag(isOffline == true ? 1 : 0);
                                    managerw.updateWoundProfile(woundprofile);
                                    update_flag = "true";
                                }
                            }
                            if (isOffline == true) {
                                managerw.saveWound(wound);
                                if (woundprofile != null) {
                                    woundprofile.setOffline_flag(isOffline == true ? 1 : 0);
                                    managerw.updateWoundProfile(woundprofile);
                                }
                                offline.setWound_id(wound.getId());
                                offline.setWound_location(wound.getWound_location_detailed());
                            }

                            //Cycle the different wound_types, to see if the wound_type is closed.
                            WoundProfileTypeVO[] types = wound.getWound_profile_types();
                            for (WoundProfileTypeVO type : types) {
                                WoundAssessmentLocationVO loco2 = new WoundAssessmentLocationVO();
                                loco2.setWound_id(new Integer((String) wound_id));
                                loco2.setWound_profile_type_id(type.getId());
                                Collection<WoundAssessmentLocationVO> allLocations = walmanager.getAllAlphas(loco2);
                                loco2.setActive(new Integer(1));
                                loco2.setDischarge(new Integer(0));

                                Collection<WoundAssessmentLocationVO> allActiveLocations = walmanager.getAllAlphas(loco2);

                                boolean healed = Common.isWoundProfileTypeHealed(userVO.getId(), allLocations, allActiveLocations);
                                //System.out.println("Healed: "+healed);
                                if (healed == true) {
                                    type.setClosed(1);
                                    type.setClosed_date(backdated_timestamp);
                                    managerw.saveWoundProfileType(type);
                                }
                                if (healed == false && type.getClosed().equals(1)) // If wound type was closed but new alpha is created
                                {
                                     type.setClosed(0);
                                    type.setClosed_date(null);
                                    managerw.saveWoundProfileType(type);
                                }

                                
                                /*if(type.getActive().equals(new Integer(0))){
                                 type.setActive(1);
                                 }
                                 managerw.saveWoundProfileType(type);*/
                            }
                        }
                    }
                    if (assessment_id != null) {
                        WoundAssessmentVO wat = new WoundAssessmentVO();
                        wat.setId(Integer.parseInt((String) assessment_id));
                        WoundAssessmentVO assessVO = wamanager.getAssessment(wat);
                        WoundAssessmentVO activeAssess = new WoundAssessmentVO();
                        activeAssess.setPatient_id(new Integer((String) patient_id));
                        activeAssess.setActive(1);
                        activeAssess.setVisit(1);//get last assessment which counts as a visit.
                        WoundAssessmentVO lastActiveAssessment = wamanager.getAssessment(activeAssess);
                        //Collection assessCollection = amanager.getAllAssessments(Integer.parseInt(assessment_id));
                        if (assessVO != null && assessCollection.size() > 0) {
                            assessVO.setActive(new Integer("1"));
                            assessVO.setReset_visit(0);
                            assessVO.setReset_reason("");
                            try {
                                //check if last assessment was reset due to healed..
                                //if it was.. it means this one must be a new profile..
                                //and we need to check if its iwthin the 24hr timeframe,
                                //which means we reset the reset :P and continue counting.
                                if (lastActiveAssessment != null && new Date().before(PDate.addDays(lastActiveAssessment.getCreated_on(), 1)) && (lastActiveAssessment.getReset_visit() != null && lastActiveAssessment.getReset_visit() == 1) && (lastActiveAssessment.getReset_reason() != null && lastActiveAssessment.getReset_reason().equals("All Wounds Healed"))) {
                                    //last assessment was reset due to healing.. and falls within 24hr timeframe.
                                    //we need to revert the reset.
                                    lastActiveAssessment.setReset_visit(0);
                                    lastActiveAssessment.setReset_reason("");
                                    wamanager.saveAssessment(lastActiveAssessment);

                                }
                                if (lastActiveAssessment == null || (PDate.addHours(lastActiveAssessment.getCreated_on(), 2).before(new Date()))) {
                                    assessVO.setVisit(1);
                                    //check if any wound_profiles are still open.. if not reset.
                                    boolean heal = managerw.checkForOpenProfiles(new Integer(patient_id));
                                    if (heal == true) {
                                        assessVO.setReset_visit(1);
                                        assessVO.setReset_reason(Constants.WOUNDS_HEALED);
                                    } else {
                                        if (lastActiveAssessment != null && !lastActiveAssessment.getTreatment_location_id().equals(assessVO.getTreatment_location_id())) {
                                            //current assessment is a new location so we need to reset on last assessment.
                                            lastActiveAssessment.setReset_visit(1);
                                            lastActiveAssessment.setReset_reason(Constants.LOCATION_MOVE);
                                            lastActiveAssessment.setLastmodified_on(new Date());
                                            System.out.println("Location Move:::::::::::::::::"+lastActiveAssessment.getCreated_on());
                                            lastActiveAssessment.setCreated_on(lastActiveAssessment.getVisited_on());
                                            wamanager.saveAssessment(lastActiveAssessment);
                                        }
                                    }
                                }
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            //managerpa.saveVisit(new Integer((String) patient_id).intValue(),assessVO.getTreatment_location_id());
                            assessVO.setOffline_flag(Common.isOffline() == true ? 1 : 0);
                            assessVO.setVisited_on(backdated_timestamp);
                            assessVO.setCreated_on(new Date());

                            assessVO.setUser_signature(user_signature);
                            wamanager.saveAssessment(assessVO);

                        }

                        if (isOffline == true) {
                            offline.setAssessment_id(assessVO.getId());
                        }
                    }

                    //TODO:  Insert referrals.
                    //for WOUND type
                    if (assessment_id != null) {


                        ReferralsTrackingVO referralVOCrit = new ReferralsTrackingVO();
                        referralVOCrit.setProfessional_id(userVO.getId());
                        referralVOCrit.setAssessment_id(new Integer((String) assessment_id));
                        referralVOCrit.setWound_id(new Integer((String) wound_id));
                        referralVOCrit.setEntry_type(0); // "Referral"
                        //referralVOCrit.setCurrent(1);
                        ReferralsTrackingVO referral_id = null;
                        String wpti_wound = "-1";
                        String wpti_drain = "-1";
                        String wpti_incision = "-1";
                        String wpti_skin = "-1";
                        String wpti_ostomy = "-1";
                        String wpti_burn = "-1";
                        if(session.getAttribute("wpti_wound")!=null){
                            wpti_wound = (String)session.getAttribute("wpti_wound");
                        }
                        if(session.getAttribute("wpti_ostomy")!=null){
                            wpti_ostomy = (String)session.getAttribute("wpti_ostomy");
                        }
                        if(session.getAttribute("wpti_drain")!=null){
                            wpti_drain = (String)session.getAttribute("wpti_drain");
                        }
                        if(session.getAttribute("wpti_skin")!=null){
                            wpti_skin = (String)session.getAttribute("wpti_skin");
                        }
                        if(session.getAttribute("wpti_burn")!=null){
                            wpti_burn = (String)session.getAttribute("wpti_burn");
                        }
                        if(session.getAttribute("wpti_incision")!=null){
                            wpti_incision = (String)session.getAttribute("wpti_incision");
                        }
                        if ((summaryForm.getReferral_wound() != null && summaryForm.getReferral_wound().equals("y")) || (summaryForm.getSend_message_wound() == 1)) {
                            WoundProfileTypeVO t = new WoundProfileTypeVO();
                            t.setId(new Integer(wpti_wound));
                            WoundProfileTypeVO check = managerw.getWoundProfileType(t);
                            if(check!=null && !check.getWound_id().equals(new Integer(wound_id))){
                                LogAuditVO logaudit = new LogAuditVO();
                                logaudit.setAction("ERROR");
                                logaudit.setCreated_on(new Date());
                                logaudit.setDescription("WPTI is wrong! Wound_ID: "+wound_id+" Session OBJ: "+session);
                                logaudit.setIp_address((String)request.getRemoteAddr());
                                logaudit.setPatientId(new Integer(patient_id));
                                logaudit.setProfessionalId(userVO.getId());
                                logaudit.setRecord_id(new Integer(wpti_wound));
                                logservice.saveLogAudit(logaudit);
                            }
                            ReferralsTrackingVO referralVO = referralVOCrit;
                            referralVO.setWound_profile_type_id(new Integer(wpti_wound));
                            ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                            if (r != null && r.getWound_id() == Integer.parseInt(wound_id)) {
                                referralVO = r;
                            }
                            referralVO.setCreated_on(new Date());
                            if (summaryForm.getSend_message_wound() == 1) {
                                referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                            } else {
                                referralVO.setEntry_type(0);
                            }
                            referralVO.setTop_priority(summaryForm.getPriority_wound());
                            if (Common.getConfig("allowReferralByPositions") != null && Common.getConfig("allowReferralByPositions").equals("1")) {
                                if (summaryForm.getWound_referral_for() > 0) {
                                    referralVO.setAssigned_to(summaryForm.getWound_referral_for());
                                }

                            } 
                            referralVO.setCurrent(1);
                            referralVO.setActive(1);

                            refBD.saveReferralsTracking(referralVO, Integer.parseInt(patient_id));

                            if (Common.isOffline()==false && referralVO.getEntry_type().equals(new Integer(0)) &&  Common.getConfig("allowReferralEmails") != null && Common.getConfig("allowReferralEmails").equals("1")) {
                                //send email
                                int assigned_to = -1;
                                if(referralVO.getAssigned_to() != null){assigned_to=referralVO.getAssigned_to();}
                                LookupVO priority = lservice.getListItem(referralVO.getTop_priority());
                                if(assigned_to > 0){
                                    sendReferralEmail(patientAccount, assigned_to, (priority == null ? "" : priority.getName(language)), locale, request);
                                }
                            }
                            referral_id = refBD.getReferralsTracking(referralVO);
                            //referralVO.setAuto_referral(null);

                        }
                        if ((summaryForm.getReferral_drain() != null && summaryForm.getReferral_drain().equals("y")) || (summaryForm.getSend_message_drain() == 1)) {
                            ReferralsTrackingVO referralVO = referralVOCrit;
                            referralVO.setWound_profile_type_id(new Integer(wpti_drain));
                            ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                            if (r != null) {
                                referralVO = r;
                            }
                            referralVO.setCreated_on(new Date());
                            if (summaryForm.getSend_message_drain() == 1) {
                                referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                            } else {
                                referralVO.setEntry_type(0);
                            }
                            referralVO.setTop_priority(summaryForm.getPriority_drain());
                            if (Common.getConfig("allowReferralByPositions") != null && Common.getConfig("allowReferralByPositions").equals("1")) {
                                if (summaryForm.getDrain_referral_for() > 0) {
                                    referralVO.setAssigned_to(summaryForm.getDrain_referral_for());
                                }
                            }

                            referralVO.setCurrent(1);
                            referralVO.setActive(1);

                            refBD.saveReferralsTracking(referralVO, Integer.parseInt(patient_id));
                            if (Common.isOffline()==false && referralVO.getEntry_type().equals(new Integer(0)) &&  Common.getConfig("allowReferralEmails") != null && Common.getConfig("allowReferralEmails").equals("1")) {
                                //send email
                                int assigned_to = -1;
                                if(referralVO.getAssigned_to() != null){assigned_to=referralVO.getAssigned_to();}
                                LookupVO priority = lservice.getListItem(referralVO.getTop_priority());
                                if(assigned_to > 0){
                                    sendReferralEmail(patientAccount, assigned_to, (priority == null ? "" : priority.getName(language)), locale, request);
                                }
                            }
                            referral_id = refBD.getReferralsTracking(referralVO);
                            //referralVO.setAuto_referral(null);

                        }

                        if ((summaryForm.getReferral_incision() != null && summaryForm.getReferral_incision().equals("y")) || (summaryForm.getSend_message_incision() == 1)) {
                            ReferralsTrackingVO referralVO = referralVOCrit;
                            referralVO.setWound_profile_type_id(new Integer(wpti_incision));
                            ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                            if (r != null) {
                                referralVO = r;
                            }
                            referralVO.setCreated_on(new Date());
                            if (summaryForm.getSend_message_incision() == 1) {
                                referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                            } else {
                                referralVO.setEntry_type(0);
                            }
                            referralVO.setTop_priority(summaryForm.getPriority_incision());
                            if (Common.getConfig("allowReferralByPositions") != null && Common.getConfig("allowReferralByPositions").equals("1")) {
                                if (summaryForm.getIncision_referral_for() > 0) {
                                    referralVO.setAssigned_to(summaryForm.getIncision_referral_for());
                                }
                            }

                            referralVO.setCurrent(1);
                            referralVO.setActive(1);

                            refBD.saveReferralsTracking(referralVO, Integer.parseInt(patient_id));
                            if (Common.isOffline()==false && referralVO.getEntry_type().equals(new Integer(0)) &&   Common.getConfig("allowReferralEmails") != null && Common.getConfig("allowReferralEmails").equals("1")) {
                                //send email
                                int assigned_to = -1;
                                if(referralVO.getAssigned_to() != null){assigned_to=referralVO.getAssigned_to();}
                                LookupVO priority = lservice.getListItem(referralVO.getTop_priority());
                                if(assigned_to > 0){
                                    sendReferralEmail(patientAccount, assigned_to, (priority == null ? "" : priority.getName(language)), locale, request);
                                }
                            }
                            referral_id = refBD.getReferralsTracking(referralVO);
                            //referralVO.setAuto_referral(null);

                        }
                        if ((summaryForm.getReferral_ostomy() != null && summaryForm.getReferral_ostomy().equals("y")) || (summaryForm.getSend_message_ostomy() == 1)) {
                            ReferralsTrackingVO referralVO = referralVOCrit;
                            referralVO.setWound_profile_type_id(new Integer(wpti_ostomy));
                            ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                            if (r != null) {
                                referralVO = r;
                            }
                            referralVO.setCreated_on(new Date());
                            if (summaryForm.getSend_message_ostomy() == 1) {
                                referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                            } else {
                                referralVO.setEntry_type(0);
                            }
                            referralVO.setTop_priority(summaryForm.getPriority_ostomy());
                            if (Common.getConfig("allowReferralByPositions") != null && Common.getConfig("allowReferralByPositions").equals("1")) {
                                if (summaryForm.getOstomy_referral_for() > 0) {
                                    referralVO.setAssigned_to(summaryForm.getOstomy_referral_for());
                                }
                            } 
                            referralVO.setCurrent(1);
                            referralVO.setActive(1);

                            refBD.saveReferralsTracking(referralVO, Integer.parseInt(patient_id));
                            if (Common.isOffline()==false &&  referralVO.getEntry_type().equals(new Integer(0)) &&  Common.getConfig("allowReferralEmails") != null && Common.getConfig("allowReferralEmails").equals("1")) {
                                //send email
                                int assigned_to = -1;
                                if(referralVO.getAssigned_to() != null){assigned_to=referralVO.getAssigned_to();}
                                LookupVO priority = lservice.getListItem(referralVO.getTop_priority());
                                if(assigned_to > 0){
                                    sendReferralEmail(patientAccount, assigned_to, (priority == null ? "" : priority.getName(language)), locale, request);
                                }
                            }
                            referral_id = refBD.getReferralsTracking(referralVO);
                            //referralVO.setAuto_referral(null);

                        }
                        if ((summaryForm.getReferral_skin() != null && summaryForm.getReferral_skin().equals("y")) || (summaryForm.getSend_message_skin() == 1)) {
                            ReferralsTrackingVO referralVO = referralVOCrit;
                            referralVO.setWound_profile_type_id(new Integer(wpti_skin));
                            ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                            if (r != null) {
                                referralVO = r;
                            }
                            referralVO.setCreated_on(new Date());
                            if (summaryForm.getSend_message_ostomy() == 1) {
                                referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                            } else {
                                referralVO.setEntry_type(0);
                            }
                            referralVO.setTop_priority(summaryForm.getPriority_skin());
                            if (Common.getConfig("allowReferralByPositions") != null && Common.getConfig("allowReferralByPositions").equals("1")) {
                                if (summaryForm.getSkin_referral_for() > 0) {
                                    referralVO.setAssigned_to(summaryForm.getSkin_referral_for());
                                }

                            } 
                            referralVO.setCurrent(1);
                            referralVO.setActive(1);

                            refBD.saveReferralsTracking(referralVO, Integer.parseInt(patient_id));


                            if (Common.isOffline()==false &&  referralVO.getEntry_type().equals(new Integer(0)) &&  Common.getConfig("allowReferralEmails") != null && Common.getConfig("allowReferralEmails").equals("1")) {
                                //send email
                                int assigned_to = -1;
                                if(referralVO.getAssigned_to() != null){assigned_to=referralVO.getAssigned_to();}
                                LookupVO priority = lservice.getListItem(referralVO.getTop_priority());
                                if(assigned_to > 0){
                                    sendReferralEmail(patientAccount, assigned_to, (priority == null ? "" : priority.getName(language)), locale, request);
                                }
                            }
                            referral_id = refBD.getReferralsTracking(referralVO);
                            //referralVO.setAuto_referral(null);

                        }
                        if ((summaryForm.getReferral_burn() != null && summaryForm.getReferral_burn().equals("y")) || (summaryForm.getSend_message_burn() == 1)) {
                            ReferralsTrackingVO referralVO = referralVOCrit;
                            referralVO.setWound_profile_type_id(new Integer(wpti_burn));
                            ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                            if (r != null) {
                                referralVO = r;
                            }
                            referralVO.setCreated_on(new Date());
                            if (summaryForm.getSend_message_burn() == 1) {
                                referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                            } else {
                                referralVO.setEntry_type(0);
                            }
                            referralVO.setTop_priority(summaryForm.getPriority_burn());
                            if (Common.getConfig("allowReferralByPositions") != null && Common.getConfig("allowReferralByPositions").equals("1")) {
                                if (summaryForm.getBurn_referral_for() > 0) {
                                    referralVO.setAssigned_to(summaryForm.getBurn_referral_for());
                                }
                            }

                            referralVO.setCurrent(1);
                            referralVO.setActive(1);

                            refBD.saveReferralsTracking(referralVO, Integer.parseInt(patient_id));
                            if (Common.isOffline()==false && referralVO.getEntry_type().equals(new Integer(0)) &&  Common.getConfig("allowReferralEmails") != null && Common.getConfig("allowReferralEmails").equals("1")) {
                                //send email
                                int assigned_to = -1;
                                if(referralVO.getAssigned_to() != null){assigned_to=referralVO.getAssigned_to();}
                                LookupVO priority = lservice.getListItem(referralVO.getTop_priority());
                                if(assigned_to > 0){
                                    sendReferralEmail(patientAccount, assigned_to, (priority == null ? "" : priority.getName(language)), locale, request);
                                }
                            }
                            referral_id = refBD.getReferralsTracking(referralVO);
                            //referralVO.setAuto_referral(null);

                        }
                        saveComment(summaryForm, summaryForm.getBody_wound(), wpti_wound, referral_id, Constants.COMMENT_TYPE_NOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_referral_wound(), wpti_wound, referral_id, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_incision(), wpti_incision, referral_id, Constants.COMMENT_TYPE_NOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_referral_incision(), wpti_incision, referral_id, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_ostomy(), wpti_ostomy, referral_id, Constants.COMMENT_TYPE_NOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_referral_ostomy(), wpti_ostomy, referral_id, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_drain(), wpti_drain, referral_id, Constants.COMMENT_TYPE_NOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_referral_drain(), wpti_drain, referral_id, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_skin(), wpti_skin, referral_id, Constants.COMMENT_TYPE_NOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_referral_skin(), wpti_skin, referral_id, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_burn(), wpti_burn, referral_id, Constants.COMMENT_TYPE_NOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        saveComment(summaryForm, summaryForm.getBody_referral_burn(), wpti_burn, referral_id, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, user_signature, patient_id, wound_id, assessment_id, backdated_timestamp);
                        if (Common.getConfig("allowCCACReporting").equals("1")) {
                            for (AbstractAssessmentVO abs : assessCollection) {
                                String create_report = (String) request.getParameter("show_ccac_report_" + abs.getAlpha_id());
                                if (create_report != null && create_report.equals("1")) {
                                    //get wound profile
                                    WoundProfileVO wp = new WoundProfileVO();
                                    wp.setWound_id(new Integer(wound_id));
                                    wp.setActive(1);
                                    wp.setCurrent_flag(1);
                                    WoundProfileVO wound_profile = managerw.getWoundProfile(wp);

                                    //get limb_assessment
                                    LimbBasicAssessmentVO ltmp = new LimbBasicAssessmentVO();
                                    ltmp.setActive(1);
                                    ltmp.setPatient_id(new Integer(patient_id));
                                    LimbBasicAssessmentVO limb = managerp.getLimbBasicAssessment(ltmp);

                                    //get patient profile
                                    PatientProfileVO ptmp = new PatientProfileVO();
                                    ptmp.setPatient_id(new Integer(patient_id));
                                    ptmp.setActive(1);
                                    ptmp.setCurrent_flag(1);
                                    PatientProfileVO patient_profile = managerp.getPatientProfile(ptmp);

                                    if (wound_profile != null && patient_profile != null) {
                                        CCACReportVO report = new CCACReportVO();
                                        report.setAssessment_id(abs.getAssessment_id());
                                        report.setAlpha_id(abs.getAlpha_id());
                                        report.setProfessional_id(userVO.getId());
                                        report.setPatient_id(new Integer((String) patient_id));
                                        report.setWound_id(new Integer(wound_id));
                                        report.setWound_profile_id(wound_profile.getId());
                                        report.setPatient_profile_id(patient_profile.getId());
                                        String suitable = (String) request.getParameter("suitable_transfer_" + abs.getAlpha_id());
                                        if (suitable != null && suitable.equals("Yes")) {
                                            report.setSuitable_transfer(suitable);
                                            try {
                                                int year = Integer.parseInt((String) request.getParameter("date_transfer_" + abs.getAlpha_id() + "_year"));
                                                int month = Integer.parseInt((String) request.getParameter("date_transfer_" + abs.getAlpha_id() + "_month"));
                                                int day = Integer.parseInt((String) request.getParameter("date_transfer_" + abs.getAlpha_id() + "_day"));
                                                report.setDate_of_transfer(PDate.getDate(month, day, year));
                                            } catch (NumberFormatException e) {
                                            }

                                        }
                                        if (limb != null) {
                                            report.setLimb_assessment_id(limb.getId());
                                        }
                                        if (request.getParameter("wound_progress" + abs.getAlpha_id()) != null) {
                                            report.setChange_in_wound((String) request.getParameter("change_in_wound" + abs.getAlpha_id()));
                                            report.setWound_change_comments((String) request.getParameter("wound_change_reason" + abs.getAlpha_id()));
                                            report.setWound_progress((String) request.getParameter("wound_progress" + abs.getAlpha_id()));
                                        }
                                        report.setActive(1);
                                        report.setComments((String) request.getParameter("comments_" + abs.getAlpha_id()));
                                        report.setDischarge((String) request.getParameter("discharge_" + abs.getAlpha_id()));
                                        report.setType_of_report((String) request.getParameter("type_of_report_" + abs.getAlpha_id()));
                                        report.setWound_status((String) request.getParameter("change_of_status_" + abs.getAlpha_id()));
                                        report.setCorrect_pathway_confirmed((String) request.getParameter("correct_pathway_" + abs.getAlpha_id()));
                                        try {
                                            report.setWeek(new Integer((String) request.getParameter("week_" + abs.getAlpha_id())));
                                        } catch (NumberFormatException e) {
                                        }
                                        report.setTime_stamp(new Date());
                                        if (patientAccount != null) {
                                            report.setPatient_account_id(patientAccount.getId());
                                        }
                                        report.setAlready_retrieved(0);
                                        rservice.saveCCACReport(report);
                                    }
                                }

                            }
                        }
                    }
                    if (isOffline == true) {
                        offline.setProfessional_id(userVO.getId());
                        managerp.saveOffline(offline);
                    } else {
                        //update timestamp for patient.. used to generate report for PCI ( or authority's document library)

                        PatientReportService prservice = new PatientReportService();
                        if (patient_id != null && !patient_id.equals("")) {
                            PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                            if (savedPatient != null && savedPatient.getActive() == 0) {//if patient exists and hasn't been generated yet
                                savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                savedPatient.setActive(1);
                                prservice.savePatientReportUpdate(savedPatient);
                            } else {
                                savedPatient = new PatientReportVO();
                                savedPatient.setPatient_id(new Integer(patient_id));
                                savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                savedPatient.setActive(1);
                                prservice.savePatientReportUpdate(savedPatient);
                            }
                        }
                    }
                    //drop old temp files
                    //no reason to drop old tmp files. >> SV: Maybe drop old tmp files from other professionals here?
                    //managerp.dropTMP((String) patient_id,userVO.getId()+"");
                    //only drop from WP_id
                    if (wound_id != null && !((String) wound_id).equals("")) {
                        //managerw.dropTMP((String) patient_id,(String) wound_id,userVO.getId()+"");
                        //amanager.dropTMP((String) patient_id,(String) wound_id,userVO.getId().intValue());
                    }
                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in Summary.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
            } else {
                if (assessment_id != null && !((String) assessment_id).equals("")) {

                    try {
                        /*if(Common.getConfig("allowCCACReporting").equals("1")){
                         CCACReportVO report = new CCACReportVO();
                         report.setAssessment_id(new Integer(assessment_id));
                         //temp
                        
                         report.setAlpha_id(13);
                         report.setPatient_id(new Integer((String)patient_id));
                         report.setWound_id(new Integer(wound_id));
                         report.setActive(0);
                         report.setComments(summaryForm.getComments());
                         report.setDischarge(summaryForm.getDischarge());
                         report.setType_of_report(summaryForm.getType_of_report());
                         report.setWound_status(summaryForm.getChange_of_status());
                         report.setProfessional_id(userVO.getId());
                         report.setWeek(summaryForm.getWeek());
                         //report.setAlready_retrieved(summaryForm);
                         if(patientAccount!=null){
                         report.setPatient_account_id(patientAccount.getId());
                         }
                         report.setAlready_retrieved(0);
                         rservice.saveCCACReport(report);
                         }*/

                        //user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
                        String wpti_wound = "-1";
                        String wpti_drain = "-1";
                        String wpti_incision = "-1";
                        String wpti_skin = "-1";
                        String wpti_ostomy = "-1";
                        String wpti_burn = "-1";
                        if(session.getAttribute("wpti_wound")!=null){
                            try{wpti_wound = (String)session.getAttribute("wpti_wound");}catch(NumberFormatException ee){}
                        }
                        if(session.getAttribute("wpti_ostomy")!=null){
                            try{wpti_ostomy = (String)session.getAttribute("wpti_ostomy");}catch(NumberFormatException ee){}
                        }
                        if(session.getAttribute("wpti_drain")!=null){
                            try{wpti_drain = (String)session.getAttribute("wpti_drain");}catch(NumberFormatException ee){}
                        }
                        if(session.getAttribute("wpti_skin")!=null){
                            try{wpti_skin = (String)session.getAttribute("wpti_skin");}catch(NumberFormatException ee){}
                        }
                        if(session.getAttribute("wpti_incision")!=null){
                            try{wpti_incision = (String)session.getAttribute("wpti_incision");}catch(NumberFormatException ee){}
                        }

                        Collection<AbstractAssessmentVO> assessCollection = amanager.getAllAssessments(Integer.parseInt(assessment_id));
                        for (AbstractAssessmentVO assessVO : assessCollection) {
                            if (assessVO.getWound_profile_type().getAlpha_type().equals("A")) {
                                assessVO.setPriority(new Integer(summaryForm.getPriority_wound()));
                            } else if (assessVO.getWound_profile_type().getAlpha_type().equals("D")) {
                                assessVO.setPriority(new Integer(summaryForm.getPriority_drain()));
                            } else if (assessVO.getWound_profile_type().getAlpha_type().equals("I")) {
                                assessVO.setPriority(new Integer(summaryForm.getPriority_incision()));
                            } else if (assessVO.getWound_profile_type().getAlpha_type().equals("O")) {
                                assessVO.setPriority(new Integer(summaryForm.getPriority_ostomy()));

                            } else if (assessVO.getWound_profile_type().getAlpha_type().equals("B")) {
                                assessVO.setPriority(new Integer(summaryForm.getPriority_burn()));

                            } else if (assessVO.getWound_profile_type().getAlpha_type().equals("S")) {
                                assessVO.setPriority(new Integer(summaryForm.getPriority_skin()));

                            }
                            amanager.saveAssessment(assessVO);
                        }

                        saveComment(summaryForm, summaryForm.getBody_wound(), wpti_wound, null, Constants.COMMENT_TYPE_NOTE, userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_referral_wound(), wpti_wound, null, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_incision(), wpti_incision, null, Constants.COMMENT_TYPE_NOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_referral_incision(), wpti_incision, null, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_ostomy(), wpti_ostomy, null, Constants.COMMENT_TYPE_NOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_referral_ostomy(), wpti_ostomy, null, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_drain(), wpti_drain, null, Constants.COMMENT_TYPE_NOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_referral_drain(), wpti_drain, null, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_skin(), wpti_skin, null, Constants.COMMENT_TYPE_NOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        saveComment(summaryForm, summaryForm.getBody_referral_skin(), wpti_skin, null, Constants.COMMENT_TYPE_REFERRALNOTE,userVO, "", patient_id, wound_id, assessment_id, new Date());
                        //saveComment(summaryForm,summaryForm.getBody_burn(),summaryForm.getWpti_burn(),Constants.COMMENT_TYPE_NOTE);
                        //saveComment(summaryForm,summaryForm.getBody_referral_burn(),summaryForm.getWpti_burn(),Constants.COMMENT_TYPE_REFERRALNOTE);

                        //temp saving referral/priority
                        Hashtable refs = new Hashtable();
                        if (assessment_id != null) {




                            if ((summaryForm.getReferral_wound() != null && summaryForm.getReferral_wound().equals("y")) || (summaryForm.getSend_message_wound() == 1)) {
                                ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                                referralVO.setProfessional_id(userVO.getId());
                                referralVO.setAssessment_id(new Integer((String) assessment_id));
                                referralVO.setWound_id(new Integer((String) wound_id));
                                referralVO.setCurrent(new Integer("1"));
                                referralVO.setWound_profile_type_id(new Integer(wpti_wound));
                                ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                                if (r != null) {
                                    referralVO.setId(r.getId());
                                }
                                referralVO.setCreated_on(new Date());
                                if (summaryForm.getSend_message_wound() == 1) {
                                    referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                                } else {
                                    referralVO.setEntry_type(Constants.REFERRAL);
                                }
                                referralVO.setTop_priority(summaryForm.getPriority_wound());
                                refs.put(wpti_wound + "", referralVO);
                            } else {
                                refs.remove(wpti_wound + "");
                            }
                            if ((summaryForm.getReferral_drain() != null && summaryForm.getReferral_drain().equals("y")) || (summaryForm.getSend_message_drain() == 1)) {
                                ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                                referralVO.setProfessional_id(userVO.getId());
                                referralVO.setAssessment_id(new Integer((String) assessment_id));
                                referralVO.setWound_id(new Integer((String) wound_id));
                                referralVO.setCurrent(new Integer("1"));
                                referralVO.setWound_profile_type_id(new Integer(wpti_drain));
                                ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                                if (r != null) {
                                    referralVO.setId(r.getId());
                                }
                                referralVO.setCreated_on(new Date());
                                if (summaryForm.getSend_message_drain() == 1) {
                                    referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                                } else {
                                    referralVO.setEntry_type(Constants.REFERRAL);
                                }
                                referralVO.setTop_priority(summaryForm.getPriority_drain());
                                refs.put(wpti_drain + "", referralVO);

                            } else {
                                refs.remove(wpti_drain + "");

                            }
                            if ((summaryForm.getReferral_skin() != null && summaryForm.getReferral_skin().equals("y")) || (summaryForm.getSend_message_skin() == 1)) {
                                ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                                referralVO.setProfessional_id(userVO.getId());
                                referralVO.setAssessment_id(new Integer((String) assessment_id));
                                referralVO.setWound_id(new Integer((String) wound_id));
                                referralVO.setCurrent(new Integer("1"));
                                referralVO.setWound_profile_type_id(new Integer(wpti_skin));
                                ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                                if (r != null) {
                                    referralVO.setId(r.getId());
                                }
                                referralVO.setCreated_on(new Date());
                                if (summaryForm.getSend_message_skin() == 1) {
                                    referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                                } else {
                                    referralVO.setEntry_type(Constants.REFERRAL);
                                }
                                referralVO.setTop_priority(summaryForm.getPriority_skin());
                                refs.put(wpti_skin + "", referralVO);

                            } else {
                                refs.remove(wpti_skin + "");

                            }
                            if ((summaryForm.getReferral_incision() != null && summaryForm.getReferral_incision().equals("y")) || (summaryForm.getSend_message_incision() == 1)) {
                                ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                                referralVO.setProfessional_id(userVO.getId());
                                referralVO.setAssessment_id(new Integer((String) assessment_id));
                                referralVO.setWound_id(new Integer((String) wound_id));
                                referralVO.setCurrent(new Integer("1"));
                                referralVO.setWound_profile_type_id(new Integer(wpti_incision));
                                ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                                if (r != null) {
                                    referralVO.setId(r.getId());
                                }
                                referralVO.setCreated_on(new Date());
                                if (summaryForm.getSend_message_incision() == 1) {
                                    referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                                } else {
                                    referralVO.setEntry_type(Constants.REFERRAL);
                                }
                                referralVO.setTop_priority(summaryForm.getPriority_incision());
                                refs.put(wpti_incision + "", referralVO);
                            } else {
                                refs.remove(wpti_incision + "");
                            }
                            if ((summaryForm.getReferral_ostomy() != null && summaryForm.getReferral_ostomy().equals("y")) || (summaryForm.getSend_message_ostomy() == 1)) {
                                ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                                referralVO.setProfessional_id(userVO.getId());
                                referralVO.setAssessment_id(new Integer((String) assessment_id));
                                referralVO.setWound_id(new Integer((String) wound_id));
                                referralVO.setCurrent(new Integer("1"));
                                referralVO.setWound_profile_type_id(new Integer(wpti_ostomy));
                                ReferralsTrackingVO r = refBD.getReferralsTracking(referralVO);
                                if (r != null) {
                                    referralVO.setId(r.getId());
                                }
                                referralVO.setCreated_on(new Date());
                                if (summaryForm.getSend_message_ostomy() == 1) {
                                    referralVO.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                                } else {
                                    referralVO.setEntry_type(Constants.REFERRAL);
                                }
                                referralVO.setTop_priority(summaryForm.getPriority_ostomy());
                                refs.put(wpti_ostomy + "", referralVO);

                            } else {
                                refs.remove(wpti_ostomy + "");
                            }


                            session.setAttribute("referrals", refs);

                        }


                    } catch (ApplicationException e) {
                        log.error("Summary.perform: Deleting Assessment Error");
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception", "y");
                        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                        errors.add("exception", new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);
                        return (mapping.findForward("pixalere.error"));
                    }
                }


            }

            request.setAttribute("assessment_id", assessment_id);

        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            log.info("############$$$$$$$$$$$$################# Page: " + request.getParameter("page"));
            return (mapping.findForward("uploader.go.admin"));
        } else {
            if (summaryForm.getContinued() == 1) {
                Common.invalidateWoundSession(session);
                session.removeAttribute("referrals");
                request.setAttribute("saved", "yes");
                return (mapping.findForward("uploader.go.redirectwp"));
            } else {
                Common.invalidatePatientSession(session);
                session.removeAttribute("referrals");//refactor in 4.2 (moved to schema)
                request.setAttribute("saved", "yes");
                request.setAttribute("page", "home");
                return (mapping.findForward("uploader.go.home"));
            }
        }
    }

    public void saveComment(SummaryForm summaryForm, String body, String wound_profile_type_id, ReferralsTrackingVO referral_id, String comment_type,ProfessionalVO userVO,String user_signature,String patient_id,String wound_id,String assessment_id,Date backdated_timestamp) throws ApplicationException {
        AssessmentCommentsServiceImpl cmanager = new AssessmentCommentsServiceImpl();
        if ((body != null && !body.equals("")) && wound_profile_type_id!=null && !wound_profile_type_id.equals("")) {
            AssessmentCommentsVO c = new AssessmentCommentsVO();
            c.setAssessment_id(new Integer((String) assessment_id));
            c.setWound_profile_type_id(new Integer(wound_profile_type_id));
            c.setComment_type(comment_type);
            AssessmentCommentsVO comments = cmanager.getComment(c);
            if (comments == null) {
                comments = new AssessmentCommentsVO();
            }
            if (assessment_id == null || assessment_id.equals("")) {
                comments.setAssessment_id(-1);
            } else {
                comments.setAssessment_id(new Integer((String) assessment_id));
            }
            comments.setPatient_id(new Integer((String) patient_id));
            comments.setWound_id(new Integer((String) wound_id));
            comments.setProfessional_id(userVO.getId());
            comments.setVisited_on(backdated_timestamp);
            comments.setCreated_on(new Date());
            comments.setComment_type(comment_type);
            if (referral_id != null) {
                comments.setReferral_id(referral_id.getId());
            }
            comments.setPosition_id(userVO.getPosition_id());
            comments.setUser_signature(user_signature);
            comments.setBody(body);
            comments.setWound_profile_type_id(new Integer(wound_profile_type_id));
            if (body != null && !body.equals("")) {
                cmanager.saveComment(comments);
            }

        }
    }

    private void assignProfessional(PatientAccountVO patientAccount, int position_id,ProfessionalVO userVO,String wound_id) {
        try {
            ProfessionalServiceImpl usermanager = new ProfessionalServiceImpl();
            ProfessionalVO profTMP = new ProfessionalVO();
            profTMP.setPosition_id(position_id);
            Collection<ProfessionalVO> professionals_w_position = usermanager.getProfessionals(profTMP);
            if (professionals_w_position != null) {
                for (ProfessionalVO prof : professionals_w_position) {
                    boolean assigned = false;
                    //find out if this professional already has access to the patent
                    Collection<UserAccountRegionsVO> regions = prof.getRegions();
                    if (regions != null) {

                        for (UserAccountRegionsVO region : regions) {
                            if (region != null) {
                                if (region.getTreatment_location_id().equals(patientAccount.getTreatment_location_id())) {
                                    assigned = true;
                                }
                            }
                        }
                    }
                    //if assigned == false, assign access through account_assignments
                    AssignPatientsServiceImpl aservice = new AssignPatientsServiceImpl();
                    AssignPatientsVO assign = new AssignPatientsVO();
                    assign.setAccountType("Patient");
                    assign.setAssignedBy(userVO.getId());
                    assign.setCreated_on(new Date());
                    assign.setPatientId(patientAccount.getPatient_id());
                    assign.setProfessionalId(prof.getId());
                    aservice.saveAssignedPatient(assign);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends an email to professionals who have access to th Referrals popup.  if position_id
     * != -1 it will send to only professionals with access to popup and with
     * designatd position.
     * @param patient
     * @param position_id
     * @param priority
     * @param locale
     * @param request 
     */
    public void sendReferralEmail(PatientAccountVO patient, int position_id, String priority, String locale, HttpServletRequest request) {
        try {
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            SendMailUsingAuthentication mailservice = new SendMailUsingAuthentication();
            if (patient != null) {
                //send an email to all clincians assigned to this
                Collection<ProfessionalVO> professionals = pservice.getProfessionalsByAccess(position_id, patient.getTreatment_location_id());
                
                for (ProfessionalVO prof : professionals) {
                    if (prof != null && prof.getEmail() != null && !prof.getEmail().equals("")) {
                    //send email about referral to Clinician
                        String url = "";
                        if (request.getRequestURL() != null) {
                            url = request.getRequestURL().toString();
                            if(url.lastIndexOf("/")>0){
                                url = url.substring(0,url.lastIndexOf("/"));
                            }
                        }
                        String subject = Common.getLocalizedString("pixalere.referral.email.subject", locale) + ": " + Common.getConfig("customer_name") +" on "+PDate.getDate(new Date(), true);
                        String body1 = Common.getLocalizedString("pixalere.referral.email.body1", locale) + " "+ (priority!=""?priority:"N/A") + " "+Common.getLocalizedString("pixalere.for",locale)+" "+Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale)+":"+patient.getPatient_id()+".\n";
                        
                        mailservice.postMail(prof.getEmail(), subject, body1, null);

                    }
                }
            }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}