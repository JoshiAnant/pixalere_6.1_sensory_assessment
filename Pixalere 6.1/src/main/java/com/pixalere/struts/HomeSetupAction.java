package com.pixalere.struts;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Date;
import com.pixalere.common.service.ConfigurationServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Common;
import java.util.Hashtable;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Constants;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.manager.LastAssessments;
import java.util.Vector;
import com.pixalere.utils.PDate;

public class HomeSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HomeSetupAction.class);

    /** TODO: Look into passing patientAcocuntVO throw submit (edit) **/
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        if (userVO == null) {//session is invalid.. return to login
            request.setAttribute("session_invalid", "yes");
            return (mapping.findForward("system.logoff"));
        }
        if (request.getAttribute("passwordExpiring") != null) {
            request.setAttribute("passwordExpiring", request.getAttribute("passwordExpiring"));
        }
        String locale = Common.getLanguageLocale(language);
        String sort = "";
        /*String request_sort = (String) request.getParameter("sort");
        //Sets the proper sort order for the list
        if (request_sort == null || request_sort.equals("")) {
            sort = "created_on";
        } else {
            sort = request_sort;
        }*/
        int offset = 0;
        if (request.getParameter("offset") != null) {
            offset = new Integer((String) request.getParameter("offset")).intValue();
        }
        ReferralsTrackingServiceImpl dao = new ReferralsTrackingServiceImpl(language);
        try{
            if (userVO.getPosition().getReferral_popup().equals(1) &&  request.getParameter("page") == null) {
                request.setAttribute("wocn", "yes");
                request.setAttribute("referralsList", dao.getAllReferrals(Constants.REFERRAL, userVO));
            } else if (userVO.getPosition().getRecommendation_popup().equals(1) && request.getParameter("page") == null) {
                request.setAttribute("referralsList", dao.getAllReferrals(Constants.RECOMMENDATION, userVO));
            }
        }catch(ApplicationException e){
            e.printStackTrace();
        }
        //Get all patients with assessments due.
        if(userVO!=null && !Common.isOffline() && (Common.getConfig("reportLastAssessments").equals("1" )) && request.getParameter("page") == null){
           ReportBuilder rb = new ReportBuilder(language);
           java.util.List<LastAssessments> report =  rb.getLastAssessmentReport(userVO,2450);
           Vector<String[]> patients = new Vector<String[]>();
           //why is this done, and not just passing obj
           for(LastAssessments r : report){
               String[] patient = new String[7];
               patient[0] = r.getPatientAccountId()+"";
               patient[1] = r.getWoundProfile();
               patient[2] = PDate.getDaysBetweenDates(getStartOfDay(PDate.convertTimezoneDate(new Date())), getStartOfDay(PDate.convertTimezoneDate(r.getLastAssessmentTimeStamp())))+" "+Common.getLocalizedString("pixalere.reporting.lastassessment.daysago",locale) + "<br/>("+PDate.getDateTime(PDate.convertTimezoneDate(r.getLastAssessmentTimeStamp()))+")";
               patient[3] = r.getWoundAlpha();
               if(r.getTreatmentLocation()!=null){
                patient[4] = r.getTreatmentLocation();
               }
               patient[5] = r.getPatientNameFull();
               patient[6] = PDate.getDaysBetweenDates(getStartOfDay(PDate.convertTimezoneDate(new Date())), getStartOfDay(PDate.convertTimezoneDate(r.getLastAssessmentTimeStamp())))+"";
               patients.add(patient);
           }
           request.setAttribute("report",patients);
        }
        // Although the configuration was already loaded in the config screen, it might need
        // a reload here because maybe user is using the Training database.
        // As soon as the Listdata, Components, Resources etc are shared for Live and Training,
        // the following code for loading the config can be removed.
        ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
        Hashtable config = new Hashtable();
        try {
            config = manager.getConfiguration();
            Common.CONFIG = config;  // Load into a hashtable to make values accessible from java classes
        } catch (ApplicationException e) {
            log.error("DataAccessException in retrieveConfiguration: " + e.toString());
            System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());
            e.printStackTrace();
        }
        session.setAttribute("config", config); // Load into the session to make values accessible for vm's

        if (session.getAttribute("patient_id") != null && request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            if (session.getAttribute("viewerOnly") != "") {
                return (mapping.findForward("uploader.go.viewer"));
            } else {
                return (mapping.findForward("uploader.go.patient"));
            }
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            if (session.getAttribute("viewerOnly") != "") {
                return (mapping.findForward("uploader.go.viewer"));
            } else {
                return (mapping.findForward("uploader.go.profile"));
            }
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            if (session.getAttribute("viewerOnly") != "") {
                return (mapping.findForward("uploader.go.viewer"));
            } else {
                return (mapping.findForward("uploader.go.treatment"));
            }
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            if (session.getAttribute("viewerOnly") != "") {
                return (mapping.findForward("uploader.go.viewer"));
            } else {
                return (mapping.findForward("uploader.go.summary"));
            }
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("uploader.go.home"));
        }

    }
    private static Date getStartOfDay(Date date) {
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);//the date we want to set initially.
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            
            return c.getTime();

        }
        return null;
    }
}
