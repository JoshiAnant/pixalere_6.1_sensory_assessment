package com.pixalere.struts.wound;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpSession;
public class WoundHome extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundProfile.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");

        if (request.getParameter("page")!=null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.woundhome.patient"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.woundhome.profile"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.woundhome.assess"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess_ostomy")) {
            return (mapping.findForward("uploader.woundhome.assess_ostomy"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess_incision")) {
            return (mapping.findForward("uploader.woundhome.assess_incision"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess_drain")) {
            return (mapping.findForward("uploader.woundhome.assess_drain"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess_burn")) {
            return (mapping.findForward("uploader.woundhome.assess_burn"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.woundhome.treatment"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.woundhome.viewer"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.woundhome.summary"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.woundhome.reporting"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("admin")) {
            log.info("############$$$$$$$$$$$$################# Page: " + request.getParameter("page"));
            return (mapping.findForward("uploader.woundhome.admin"));
        } else {
            return (mapping.findForward("uploader.woundhome.profile"));
        }
    }
}

