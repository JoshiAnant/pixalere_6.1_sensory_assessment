package com.pixalere.struts.wound;

import com.pixalere.assessment.bean.NursingFixesVO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.WoundAcquiredVO;
import java.util.Date;

import com.pixalere.assessment.bean.NursingFixesVO;

import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.*;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.bean.OfflineVO;
import java.util.Iterator;

public class WoundProfile extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundProfile.class);
// Alpha-string:
// 0=ID
// 1=alpha_type
// 2=alpha
// 3=box

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        WoundProfileForm wPForm = (WoundProfileForm) form;
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        boolean tokenValid = isTokenValid(request);
        try {
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            //String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");

            if (tokenValid) {
                resetToken(request);
                WoundServiceImpl bd = new WoundServiceImpl(language);
                if (((String) request.getParameter("cancel") == null) || !((String) request.getParameter("cancel")).equals("yes")) {
                    ListServiceImpl lbd = new ListServiceImpl(language);
                    ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();

                    WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
                    WoundAssessmentLocationServiceImpl walmanager = new WoundAssessmentLocationServiceImpl();
                    AssessmentServiceImpl aservice = new AssessmentServiceImpl();
                    PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                    String user_signature = pdate.getProfessionalSignature(new Date(), userVO, locale);
                    String startdate_signature = "";
                    Date today = new Date();
                    int wound_profile_id = 0;
                    WoundProfileVO woundProfileVO = wPForm.getFormData(language);
                    Date start_date = null;
                    if (wound_id == null || wound_id.equals("0")) {
                        WoundVO woundVO = woundProfileVO.getWound();
                        woundVO.setPatient_id(new Integer((String) patient_id));
                        start_date = new Date();
                        startdate_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
                        woundVO.setStart_date(start_date);
                        woundVO.setStartdate_signature(startdate_signature);
                        woundVO.setProfessional_id(userVO.getId());
                        woundVO.setActive(new Integer(0));
                        woundVO.setDeleted(0);
                        wound_profile_id = bd.getWoundId(woundVO);
                        woundVO.setId(new Integer(wound_profile_id));
                        //System.out.println("Setting wound_id: " + wound_profile_id);
                        wound_id = wound_profile_id + "";
                        session.setAttribute("wound_id", wound_profile_id + "");
                        if (Common.isOffline() == true) {
                            OfflineVO offline = null;
                            if (session.getAttribute("offlineVO") == null) {
                                offline = new OfflineVO();
                            } else {
                                offline = (OfflineVO) session.getAttribute("offlineVO");
                            }
                            offline.setWoundprofile_new(new Integer(1));
                            session.setAttribute("offlineVO", offline);
                        }

                    } else {
                        wound_profile_id = Integer.parseInt((String) wound_id);
                        //wound already exists, grab record and store in wound_profiles to ensure its updated.
                        WoundVO wound = bd.getWound(wound_profile_id);
                        boolean chg = false;
                        boolean needAudit = false;
                        //System.out.println(wound_profile_id+"OUnd: "+wound);
                        if (wound != null) {

                            if (!wound.getWound_location_detailed().equals(woundProfileVO.getWound().getWound_location_detailed())) {
                                chg = true;
                                needAudit = true;
                            }
                            if (!wound.getWound_location_position().equals(woundProfileVO.getWound().getWound_location_position())) {
                                chg = true;
                                needAudit = true;
                            }
                            if (!wound.getLocation_image_id().equals(woundProfileVO.getWound().getLocation_image_id())) {
                                chg = true;
                                needAudit = true;
                            }
                            if (!wound.getWound_location().equals(woundProfileVO.getWound().getWound_location())) {
                                chg = true;
                                needAudit = true;
                            }
                        } else {
                            //wound = new WoundVO();
                            chg = true;
                            wound.setPatient_id(new Integer((String) patient_id));
                            start_date = new Date();
                            startdate_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
                            wound.setStart_date(start_date);
                            wound.setStartdate_signature(startdate_signature);
                            wound.setProfessional_id(userVO.getId());
                            wound.setActive(new Integer(0));
                            wound.setDeleted(0);
                            wound_profile_id = bd.getWoundId(wound);
                            wound.setId(new Integer(wound_profile_id));
                            session.setAttribute("wound_id", wound_profile_id + "");
                        }
                        String location = wound.getWound_location_detailed();
                        wound.setWound_location_detailed(woundProfileVO.getWound().getWound_location_detailed());
                        wound.setWound_location_position(woundProfileVO.getWound().getWound_location_position());
                        wound.setLocation_image_id(woundProfileVO.getWound().getLocation_image_id());
                        wound.setImage_name(woundProfileVO.getWound().getImage_name());
                        wound.setWound_location(woundProfileVO.getWound().getWound_location());
                        woundProfileVO.setWound(wound);
                        woundProfileVO.setOffline_flag(Common.isOffline() == true ? 1 : 0);
                        if (chg == true) {
                            if (Common.isOffline() == true) {
                                OfflineVO offline = null;
                                if (session.getAttribute("offlineVO") == null) {
                                    offline = new OfflineVO();
                                } else {
                                    offline = (OfflineVO) session.getAttribute("offlineVO");
                                }
                                offline.setWound_changed(1);
                                session.setAttribute("offlineVO", offline);
                            }
                            wound.setDeleted(0);
                            bd.saveWound(wound);

                            if (needAudit) {
                                NursingFixesVO fixes = new NursingFixesVO();
                                fixes.setPatient_id(new Integer((String) patient_id));
                                fixes.setWound_id(woundProfileVO.getWound().getId());
                                fixes.setProfessional_id(userVO.getId());
                                fixes.setRow_id(woundProfileVO.getWound().getId());
                                fixes.setDelete_reason("");
                                fixes.setCreated_on(new Date());
                                fixes.setInitials(pdate.getProfessionalSignature(new Date(), userVO, locale));
                                fixes.setField("98");
                                fixes.setError(Common.getLocalizedString("pixalere.woundprofile.fixes.wound_location", locale) + " " + woundProfileVO.getWound().getWound_location_detailed() + ".");
                                aservice.saveNursingFixes(fixes);
                                session.setAttribute("simulateSave", "yes");
                            }

                        }
                    }

                    //Get a Temporary record
                    WoundProfileVO crit = new WoundProfileVO();
                    crit.setWound_id(new Integer(wound_profile_id));
                    crit.setProfessional_id(userVO.getId());
                    crit.setActive(new Integer(0));
                    WoundProfileVO currentTMPRecord = bd.getWoundProfile(crit);
                    if (currentTMPRecord != null) {
                        woundProfileVO.setId(currentTMPRecord.getId());
                    }
                    woundProfileVO.setWound_id(new Integer(wound_profile_id));
                    woundProfileVO.setProfessional_id(userVO.getId());
                    woundProfileVO.setUser_signature(user_signature);

                    woundProfileVO.setCreated_on(today);
                    woundProfileVO.setLastmodified_on(today);
                    WoundProfileVO currentWP = new WoundProfileVO();
                    currentWP.setPatient_id(new Integer(patient_id));
                    currentWP.setWound_id(new Integer(wound_id));
                    currentWP.setCurrent_flag(new Integer(1));
                    WoundProfileVO lastWoundProfile = bd.getWoundProfile(currentWP);

                    // Save the alphas
                    AssessmentServiceImpl eachwoundManager = new AssessmentServiceImpl(language);
                    String[] woundAlphas = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
                    String[] drainAlphas = {"td1", "td2", "td3", "td4", "td5", "td6", "td7", "td8", "td9"};
                    String[] incisionAlphas = {"tag1", "tag2", "tag3", "tag4", "tag5", "tag6", "tag7", "tag8", "tag9"};
                    String[] ostomyAlphas = {"ostf1", "ostu1"};
                    String[] burnAlphas = {"burn_draw_o", "burn_draw_y", "burn_draw_r"};
                    String[] skinAlphas = {"s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "s12", "s13", "s14", "s15", "s16", "s17", "s18", "s19", "s20"};
                    Vector<Integer> tmpAlphas = new Vector<Integer>();
                    if (Common.getConfig("allowWoundCare").equals("1")) {
                        for (String w : woundAlphas) {
                            if (request.getParameter(w + "_coords") != null) {
                                String tok = (String) request.getParameter(w + "_coords");
                                //System.out.println("Wound Coords: " + tok);
                                tmpAlphas = saveAlpha(tok, patient_id, wound_id, "A", tmpAlphas, userVO, locale);
                            }
                        }
                    }
                    if (Common.getConfig("allowStoma").equals("1")) {
                        for (String w : ostomyAlphas) {
                            if (request.getParameter(w + "_coords") != null) {
                                String tok = (String) request.getParameter(w + "_coords");
                                tmpAlphas = saveAlpha(tok, patient_id, wound_id, "O", tmpAlphas, userVO, locale);
                            }
                        }
                    }
                    if (Common.getConfig("allowIncision").equals("1")) {
                        for (String w : incisionAlphas) {
                            if (request.getParameter(w + "_coords") != null) {
                                System.out.println(w + "_coords: ");
                                String tok = (String) request.getParameter(w + "_coords");
                                tmpAlphas = saveAlpha(tok, patient_id, wound_id, "T", tmpAlphas, userVO, locale);
                            }
                        }
                        String[] details = wPForm.getIncs_coords();
                        if (details != null) {
                            if (details.length > 0) {
                                saveAlpha("incs|0|0", patient_id, wound_id, "I", tmpAlphas, userVO, locale);
                            }
                            for (String detail : details) {
                                saveAlpha(detail, patient_id, wound_id, "I", tmpAlphas, userVO, locale);
                            }
                        }
                    }
                    if (Common.getConfig("allowDrain").equals("1")) {
                        for (String w : drainAlphas) {
                            if (request.getParameter(w + "_coords") != null) {
                                String tok = (String) request.getParameter(w + "_coords");
                                tmpAlphas = saveAlpha(tok, patient_id, wound_id, "D", tmpAlphas, userVO, locale);

                            }

                        }
                    }
                    if (Common.getConfig("allowSkin").equals("1")) {
                        for (String w : skinAlphas) {
                            if (request.getParameter(w + "_coords") != null) {
                                String tok = (String) request.getParameter(w + "_coords");
                                tmpAlphas = saveAlpha(tok, patient_id, wound_id, "S", tmpAlphas, userVO, locale);

                            }

                        }
                    }
                    if (Common.getConfig("allowBurn").equals("1")) {
                        String[] details = wPForm.getBurn_coords();
                        if (details != null) {
                            if (details.length > 0) {
                                saveAlpha("burn|0|0", patient_id, wound_id, "B", tmpAlphas, userVO, locale);
                            }
                            for (String detail : details) {
                                saveAlpha(detail, patient_id, wound_id, "B", tmpAlphas, userVO, locale);
                            }
                        }
                    }

                    //refactor to retrieveAlphas...
                    Vector<WoundAssessmentLocationVO> allLiveAlphas = walmanager.retrieveLocationsForWound(userVO.getId(), new Integer((String) wound_id).intValue(), false, false);
                    //find out if some alphas were removed

                    for (WoundAssessmentLocationVO dbAlpha : allLiveAlphas) {
                        boolean removed = true;
                        boolean removedIncision = true;
                        boolean removedBurn = true;
                        int cnt = 0;
                        for (Integer formAlpha : tmpAlphas) {
                            //if its an inactive alpha, check if its removed
                            if (dbAlpha.getId().equals(formAlpha) && dbAlpha.getActive().equals(new Integer(0))) { //&& dbAlpha.getId().equals(new Integer(tmpAlphasIds.get(cnt)))) {
                                removed = false;
                            }
                            cnt++;
                        }

                        if (removed == true && dbAlpha.getActive().equals(new Integer(0))) {
                            eachwoundManager.dropTMPAssessmentByAlpha(new Integer((String) patient_id).intValue(), dbAlpha.getId().intValue());
                            walmanager.removeAlpha(dbAlpha.getId().intValue(), userVO.getId().intValue());
                            session.removeAttribute("alpha_id");
                            //remove associated assessments/images/comments

                        } else if (dbAlpha.getAlpha().equals(Constants.INCISION_ALPHA_GLOBAL)) {
                            WoundLocationDetailsVO wld = new WoundLocationDetailsVO();
                            wld.setAlpha_id(dbAlpha.getId());
                            Collection<WoundLocationDetailsVO> allDetails = walmanager.getAllAlphaDetails(wld);
                            for (WoundLocationDetailsVO wldvo : allDetails) {

                                if (removedIncision == true) {
                                    session.removeAttribute("alpha_id");
                                    wldvo.setDeleted(1);
                                    walmanager.saveAlphaDetail(wldvo);
                                }
                                removedIncision = true;

                            }

                        } else if (dbAlpha.getAlpha().equals(Constants.BURN_ALPHA_GLOBAL)) {
                            WoundLocationDetailsVO wld = new WoundLocationDetailsVO();
                            wld.setAlpha_id(dbAlpha.getId());
                            Collection<WoundLocationDetailsVO> allDetails = walmanager.getAllAlphaDetails(wld);
                            for (WoundLocationDetailsVO wldvo : allDetails) {

                                if (removedBurn == true) {
                                    session.removeAttribute("alpha_id");
                                    wldvo.setDeleted(1);
                                    walmanager.saveAlphaDetail(wldvo);
                                }
                                removedBurn = true;

                            }

                        }
                    }

                    List<EtiologyVO> etiology_arrays = new ArrayList();
                    List<WoundAcquiredVO> acquired_arrays = new ArrayList();
                    List<GoalsVO> goals_arrays = new ArrayList();

                    Vector<WoundAssessmentLocationVO> al = allLiveAlphas;
                    for (WoundAssessmentLocationVO alpha : al) {
                        //Get all alphas to check for changed data.. but remove 
                        //any temp alphas not created by logged in user.
                        if (alpha.getActive() == 0 && !alpha.getProfessional_id().equals(userVO.getId())) {
                            al.remove(alpha);
                        }
                    }
                    //only needed for the before/after comparison to decide if anything changed.

                    for (WoundAssessmentLocationVO alpha : al) {
                        try {

                            Vector<String> list_ids = Common.parseSelectByAlpha(alpha.getAlpha(), wPForm.getGoals(), locale);

                            if (list_ids != null && list_ids.size() > 0) {
                                for (String list_id : list_ids) {
                                    if (list_id != null && !list_id.equals("")) {
                                        GoalsVO goa = new GoalsVO(null, alpha.getId(), new Integer(list_id.trim()));
                                        LookupVO lo = lbd.getListItem(new Integer(list_id.trim()));
                                        goa.setLookup(lo);
                                        goals_arrays.add(goa);
                                    }
                                }
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            Vector<String> list_ids = Common.parseSelectByAlpha(alpha.getAlpha(), wPForm.getWound_acquired(), locale);
                            //System.out.println("LIST_ID: "+list_ids.size());
                            if (list_ids != null && list_ids.size() > 0) {

                                for (String list_id : list_ids) {
                                    //System.out.println("TEst: "+list_id);
                                    if (list_id != null && !list_id.equals("")) {
                                        WoundAcquiredVO goa = new WoundAcquiredVO(null, alpha.getId(), new Integer(list_id.trim()));
                                        LookupVO lo = lbd.getListItem(new Integer(list_id.trim()));
                                        goa.setLookup(lo);
                                        acquired_arrays.add(goa);
                                    }
                                }
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {

                            Vector<String> list_ids = Common.parseSelectByAlpha(alpha.getAlpha(), wPForm.getEtiology(), locale);
                            if (list_ids != null && list_ids.size() > 0) {
                                for (String list_id : list_ids) {
                                    if (list_id != null && !list_id.equals("")) {
                                        EtiologyVO eti = new EtiologyVO(null, alpha.getId(), new Integer(list_id.trim()));
                                        LookupVO lo = lbd.getListItem(new Integer(list_id.trim()));
                                        eti.setLookup(lo);
                                        etiology_arrays.add(eti);
                                    }
                                }
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    boolean changed = woundProfileVO.compare(woundProfileVO, lastWoundProfile);

                    if (changed == false && lastWoundProfile != null) {

                        if (lastWoundProfile.getGoals() == null || goals_arrays.size() != lastWoundProfile.getGoals().size()) {
                            changed = true;
                        } else if (lastWoundProfile.getEtiologies() == null || etiology_arrays.size() != lastWoundProfile.getEtiologies().size()) {
                            changed = true;
                        } else if (lastWoundProfile.getWound_acquired() == null || acquired_arrays.size() != lastWoundProfile.getWound_acquired().size()) {
                            changed = true;
                        } else {
                            for (EtiologyVO et1 : etiology_arrays) {
                                for (EtiologyVO et2 : lastWoundProfile.getEtiologies()) {
                                    if (!et1.getLookup().getId().equals(et2.getLookup().getId())) {
                                        changed = true;
                                    }
                                }
                            }
                            for (GoalsVO et1 : goals_arrays) {
                                for (GoalsVO et2 : lastWoundProfile.getGoals()) {
                                    if (!et1.getLookup().getId().equals(et2.getLookup().getId())) {
                                        changed = true;
                                    }
                                }
                            }
                            for (WoundAcquiredVO et1 : acquired_arrays) {
                                for (WoundAcquiredVO et2 : lastWoundProfile.getWound_acquired()) {
                                    if (!et1.getLookup().getId().equals(et2.getLookup().getId())) {
                                        changed = true;
                                    }
                                }
                            }
                        }
                    }

                    //bd.saveGoal(
                    if (changed == true) {

                        bd.saveWoundProfile(woundProfileVO);
                        WoundProfileVO wtmp = new WoundProfileVO();
                        wtmp.setLastmodified_on(today);
                        wtmp.setPatient_id(new Integer(patient_id));
                        WoundProfileVO currentTMPWP = bd.getWoundProfile(wtmp);
                        if (currentTMPWP != null) {
                            //remove old ones.

                            EtiologyVO eDel = new EtiologyVO();
                            eDel.setWound_profile_id(currentTMPWP.getId());
                            bd.removeEtiology(eDel);
                            GoalsVO gDel = new GoalsVO();
                            gDel.setWound_profile_id(currentTMPWP.getId());
                            bd.removeGoal(gDel);
                            WoundAcquiredVO aDel = new WoundAcquiredVO();
                            aDel.setWound_profile_id(currentTMPWP.getId());
                            bd.removeAcquired(aDel);

                            for (GoalsVO goal : goals_arrays) {
                                goal.setWound_profile_id(currentTMPWP.getId());
                                bd.saveGoal(goal);
                            }
                            for (EtiologyVO et : etiology_arrays) {
                                et.setWound_profile_id(currentTMPWP.getId());
                                bd.saveEtiology(et);
                            }
                            for (WoundAcquiredVO a : acquired_arrays) {
                                a.setWound_profile_id(currentTMPWP.getId());
                                bd.saveAcquired(a);
                            }
                        }
                    }
                   
                } else {
                    WoundAssessmentServiceImpl waservice = new WoundAssessmentServiceImpl();
                    WoundAssessmentVO wavo = new WoundAssessmentVO();
                    wavo.setPatient_id(new Integer(patient_id));
                    wavo.setProfessional_id(userVO.getId());
                    wavo.setWound_id(new Integer(wound_id));
                    wavo.setActive(new Integer(0));
                    Collection coll = waservice.getAllAssessments(wavo);
                    Iterator iter = coll.iterator();
                    while (iter.hasNext()) {
                        WoundAssessmentVO wwvo = (WoundAssessmentVO) iter.next();
                        // Remove record from wound_assessment
                        waservice.removeAssessment(wwvo, null);
                    }
                    bd.dropTMP(patient_id, wound_id, userVO.getId() + "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("An application exception has been raised in WoundProfile.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") == null && userVO != null) {
            ActionErrors errors = new ActionErrors();
            errors.add("errors", new ActionError("pixalere.common.page.error"));
            saveErrors(request, errors);
            return (mapping.findForward("uploader.woundprofile.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess_ostomy")) {
            return (mapping.findForward("uploader.go.assess_ostomy"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess_incision")) {
            return (mapping.findForward("uploader.go.assess_incision"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess_drain")) {
            return (mapping.findForward("uploader.go.assess_drain"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess_burn")) {
            return (mapping.findForward("uploader.go.assess_burn"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {

            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("uploader.go.profile"));
        }
    }

    public Vector saveAlpha(String token, String patient_id, String wound_profile_id, String alpha_type, Vector<Integer> tmpAlphas, ProfessionalVO userVO, String locale) {
        String alpha_id;
        int x;
        int y;
        String alpha;
        StringTokenizer st = new StringTokenizer(token, "|");
        alpha = st.nextToken();
        //alpha_id = st.nextToken();
        String strx = st.nextToken();
        x = Math.round(new Float(strx));
        String stry = st.nextToken();
        y = Math.round(new Float(stry));
        boolean newAlpha = false;
        String id = "-1";
        if (st.hasMoreTokens()) {
            id = st.nextToken();
        }
        // check if exists.. update if it does.
        WoundServiceImpl bd = new WoundServiceImpl(Constants.ENGLISH);
        WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
        WoundAssessmentLocationServiceImpl walmanager = new WoundAssessmentLocationServiceImpl();
        WoundAssessmentLocationVO tmploc = new WoundAssessmentLocationVO();
        WoundProfileTypeVO wpt = new WoundProfileTypeVO();
        try {
            wpt.setId(null);
            wpt.setPatient_id(new Integer((String) patient_id));
            wpt.setWound_id(new Integer(wound_profile_id));
            wpt.setAlpha_type(alpha_type);

            WoundProfileTypeVO existsWPT = bd.getWoundProfileType(wpt);
            if (existsWPT == null) {
                //wpt.setActive(0);
                wpt.setClosed(0);
                wpt.setDeleted(0);
                wpt.setClosed_date(null);
                bd.saveWoundProfileType(wpt);
            } else {
                //no need to update it 
                /*existsWPT.setClosed(0);
                 wpt.setDeleted(0);
                 existsWPT.setClosed_date(null);
                 bd.saveWoundProfileType(existsWPT);*/
            }
            // saving the new wound profile type
            wpt.setId(null);
            WoundProfileTypeVO newWPT = bd.getWoundProfileType(wpt);
            tmploc.setWound_id(new Integer(wound_profile_id));
            tmploc.setWound_profile_type_id(newWPT.getId());
            ////insert type records.
            WoundAssessmentLocationVO incisionexist = null;//(WoundAssessmentLocationVO) amanager.retrieveAlpha(tmploc);

            if (alpha_type.indexOf(Constants.INCISION_PROFILE_TYPE) == 0) {
                tmploc.setAlpha(Constants.INCISION_ALPHA_GLOBAL);
                WoundLocationDetailsVO det = new WoundLocationDetailsVO();
                incisionexist = (WoundAssessmentLocationVO) walmanager.getAlpha(tmploc, true, false, null);
                // tmpAlphas.add((String)alphas.get(2)+"-"+alphas.get(3));
            } else if (alpha_type.indexOf(Constants.BURN_PROFILE_TYPE) == 0) {
                tmploc.setAlpha(Constants.BURN_ALPHA_GLOBAL);
                WoundLocationDetailsVO det = new WoundLocationDetailsVO();
                incisionexist = (WoundAssessmentLocationVO) walmanager.getAlpha(tmploc, true, false, null);

            } else {
                tmploc.setAlpha(alpha);
            }

            //tmploc.setActive(new Integer(0));
            WoundAssessmentLocationVO alphaexist = (WoundAssessmentLocationVO) walmanager.getAlpha(tmploc, true, false, null);
            WoundAssessmentLocationVO location = new WoundAssessmentLocationVO();

            if (alphaexist == null) {//|| ((String) alphas.get(1))!="I"){
                location.setOpen_timestamp(new Date());
                newAlpha = true;
            } else {
                location.setId(alphaexist.getId());
                location.setOpen_timestamp(alphaexist.getOpen_timestamp());
            }
            location.setPatient_id(new Integer((String) patient_id));
            location.setWound_id(new Integer(wound_profile_id));

            if ((alpha_type).indexOf(Constants.INCISION_PROFILE_TYPE) == 0) {
                location.setAlpha(Constants.INCISION_ALPHA_GLOBAL);
            } else if ((alpha_type).indexOf(Constants.BURN_PROFILE_TYPE) == 0) {
                location.setAlpha(Constants.BURN_ALPHA_GLOBAL);
            } else {
                location.setAlpha(alpha);
            }

            location.setProfessional_id(userVO.getId());
            location.setActive(new Integer(0));
            if (alphaexist != null && alphaexist.getActive().equals(new Integer(1))) {
                location.setActive(new Integer(1));
            }
            location.setDischarge(new Integer(0));
            if (alphaexist != null && alphaexist.getDischarge().equals(new Integer(1))) {
                location.setDischarge(1);
            }
            location.setReactivate(new Integer(0));//not used yet

            location.setClose_timestamp(null);
            location.setWound_profile_type_id(newWPT.getId());//adding alpha_types id

            location.setX(x);
            location.setY(y);
            location.setDeleted(0);
            if (incisionexist == null) {
                if (newAlpha) {

                    walmanager.saveAlpha(location);
                } else {
                    //System.out.println("Updating Alpha");
                    //walmanager.updateAlpha(location);
                }

            }

            WoundAssessmentLocationVO returnAlpha = (WoundAssessmentLocationVO) walmanager.getAlpha(tmploc, true, false, null);
            WoundLocationDetailsVO detailed = new WoundLocationDetailsVO();

            if (returnAlpha == null && (incisionexist != null)) {//if it doesnt find a temp saved incision/burn salpha, get an active one

                tmploc.setActive(1);
                returnAlpha = (WoundAssessmentLocationVO) walmanager.getAlpha(tmploc, true, false, null);
            }
            if (returnAlpha != null) {
                tmpAlphas.add(returnAlpha.getId());
            }
            if (incisionexist != null) {

                detailed.setId(new Integer(id));
                //detailed.setBox(new Integer(alphas.get(3) + ""));
                WoundLocationDetailsVO d = (WoundLocationDetailsVO) walmanager.getAlphaDetail(detailed);

                if (d != null) {

                    detailed.setId(d.getId());
                }
                detailed.setAlpha_id(incisionexist.getId());
                detailed.setImage(alpha);
                detailed.setX(x);
                detailed.setY(y);
                detailed.setDeleted(0);
                //detailed.setBox(new Integer(alphas.get(3) + ""));
                if (!alpha.equals("incs")) {
                    walmanager.saveAlphaDetail(detailed);
                }
            }

            if (alphaexist != null) {
                alphaMoved(patient_id, alpha, alphaexist.getId(), wound_profile_id, x, y, userVO, locale);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("An application exception has been raised in WoundProfile.perform(): " + e.toString());

        }
        return tmpAlphas;
    }

    public void alphaMoved(String patient_id, String alpha, Integer alpha_id, String wound_profile_id, int x, int y, ProfessionalVO userVO, String locale) {
        // Alpha might be moved
        WoundServiceImpl bd = new WoundServiceImpl(Constants.ENGLISH);
        WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
        WoundAssessmentLocationServiceImpl walmanager = new WoundAssessmentLocationServiceImpl();
        WoundAssessmentLocationVO exist = new WoundAssessmentLocationVO();
        try {

            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            exist.setId(alpha_id);
            WoundAssessmentLocationVO alphaexist = (WoundAssessmentLocationVO) walmanager.getAlpha(exist);
            if (alphaexist != null) {
                // Check if alpha has moved position
                String intOldPos = alphaexist.getX() + "-" + alphaexist.getY();
                String intNewPos = x + "-" + y;
                if (!intOldPos.equals(intNewPos) && !alphaexist.getWound_profile_type().getAlpha_type().equals(Constants.INCISION_PROFILE_TYPE)) {

                    //Get record ID of last saved wp
                    WoundProfileVO savedWP = new WoundProfileVO();
                    savedWP.setWound_id(new Integer(wound_profile_id));
                    savedWP.setActive(new Integer(1));
                    WoundProfileVO savedWPRecord = bd.getWoundProfile(savedWP);
                    if (savedWPRecord != null) {
                        com.pixalere.assessment.dao.AssessmentDAO dao = new com.pixalere.assessment.dao.AssessmentDAO();
                        com.pixalere.assessment.bean.NursingFixesVO fixes = new com.pixalere.assessment.bean.NursingFixesVO();

                        String strMoveDetails = "";
                        if (y > alphaexist.getY().intValue()) {

                        } else if (y < alphaexist.getY().intValue()) {

                        }

                        if (x > alphaexist.getX().intValue()) {

                        } else if (x < alphaexist.getX().intValue()) {

                        }
                        alphaexist.setX(x);
                        alphaexist.setY(y);

                        fixes.setPatient_id(new Integer((String) patient_id));
                        fixes.setWound_id(new Integer((String) wound_profile_id));
                        fixes.setProfessional_id(userVO.getId());
                        fixes.setRow_id(savedWPRecord.getId());
                        fixes.setDelete_reason("");
                        fixes.setCreated_on(new Date());
                        fixes.setInitials(pdate.getProfessionalSignature(new Date(), userVO, locale));
                        fixes.setField("226");

                        fixes.setError(Common.getLocalizedString("pixalere.woundprofile.fixes.alpha_moved", locale) + " " + Common.getAlphaText(alpha, locale) + " " + Common.getLocalizedString("pixalere.woundprofile.fixes.moved_on_blue_model", locale) + ": " + strMoveDetails + ".");

                        dao.insertNursingFixes(fixes);

                        //alphaexist.setBox(new Integer((String) alphas.get(3)));
                        walmanager.saveAlpha(alphaexist);
                    } else {
                        alphaexist.setX(x);
                        alphaexist.setY(y);
                        walmanager.saveAlpha(alphaexist);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("An application exception has been raised in WoundProfile.perform(): " + e.toString());

        }
    }
}
