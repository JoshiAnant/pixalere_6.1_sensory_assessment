package com.pixalere.struts.wound;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.bean.LocationImagesVO;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import java.util.*;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.utils.*;

// initializes the wound profile page.
public class WoundProfileSetupAction extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundProfileSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public Vector getOpenWoundsData(Vector vecData, Vector vecOpenWounds) {
        Vector vecResult = new Vector();
        for (int intData = 0; intData < vecData.size(); intData++) {
            String strDataOption = (String) vecData.get(intData);
            for (int x = 0; x < vecOpenWounds.size(); x++) {
                String alpha = (String) vecOpenWounds.get(x);
                if (strDataOption.indexOf(alpha + ":") != -1) {
                    vecResult.add(strDataOption);
                }
            }
        }
        return vecResult;
    }

    public String getShowRadio(Collection<LocationImagesVO> locationImages, String strType) {
        String strResult = "0";
        for (LocationImagesVO locImg : locationImages) {
            if ((strType.equals("Full") && locImg.getFilename_full().trim().length() > 0 && Common.getLocalizedString("pixalere.hideFull", "en").equals("0")) || (strType.equals("Full") && locImg.getFilename_full().trim().length() > 0 && (locImg.getFilename_left().trim().length() > 0 || locImg.getFilename_right().trim().length() > 0)) || (strType.equals("Midline") && locImg.getFilename_midline().trim().length() > 0) || (strType.equals("LeftRight") && (locImg.getFilename_left().trim().length() > 0 || locImg.getFilename_right().trim().length() > 0))) {
                strResult = "1";
            }
        }
        return strResult;
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        request.setAttribute("page", "woundprofile");
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);

        PatientAccountVO pa = (PatientAccountVO) session.getAttribute("patientAccount");
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        if (request.getParameter("wound_id") != null && !request.getParameter("wound_id").equals("")) {
            //changing wound profile.. verify session

            session.setAttribute("wound_id", request.getParameter("wound_id"));
            wound_id = (String) request.getParameter("wound_id");

        } else {
            //   session.removeAttribute("wound_id");
        }
        

        WoundServiceImpl woundBD = new WoundServiceImpl(language);
        if (wound_id == null || wound_id.equals("")) {

            Vector<WoundVO> woundProfilesDrop = woundBD.getWoundProfilesDropdown(Integer.parseInt((String) patient_id),
                    true, userVO.getId().intValue());
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);
            return mapping.findForward("uploader.woundhome.success");
        }
        //Generate Token - prevent multiple posts
        saveToken(request);

        try {
            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            comp.setFlowchart(com.pixalere.utils.Constants.WOUND_PROFILE);
            Collection components = gui.getAllComponents(comp);
            java.util.Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);

            AssessmentServiceImpl aservice = new AssessmentServiceImpl(language);
    
            String strGender = "B";
            int intAge = 21;
            if (pa != null && pa.getDob() != null) {
                String strAge = PDate.getAge(pa.getYear(), pa.getMonth(), pa.getDay());

                if (!strAge.trim().equals("")) {
                    //intAge=Integer.parseInt(strAge);
                }
                if (pa.getGender() != null && pa.getGender() == Integer.parseInt(Common.getConfig("male"))) {
                    strGender = "M";
                }
                if (pa.getGender() != null && pa.getGender() == Integer.parseInt(Common.getConfig("female"))) {
                    strGender = "F";
                }
            }

            if (Common.getConfig("allowBurn").equals("1")) {
                String strAgeCat = Common.getLocalizedString("pixalere.burn.ages.base_cat", locale);

                // for example: pixalere.burn.ages=teen:0,14;adult:15,>;
                String strAgeCats = Common.getLocalizedString("pixalere.burn.ages.categories", locale);
                while (strAgeCats.length() > 1) {
                    int intPosColon = strAgeCats.indexOf(":");
                    int intPosComma = strAgeCats.indexOf(",");
                    int intPosSemiColon = strAgeCats.indexOf(";");
                    if (intPosColon > -1 && intPosComma > -1 && intPosSemiColon > -1) {
                        String strCat = strAgeCats.substring(0, strAgeCats.indexOf(":"));
                        String strMin = strAgeCats.substring(intPosColon + 1, intPosComma);
                        String strMax = strAgeCats.substring(intPosComma + 1, intPosSemiColon);
                        strAgeCats = strAgeCats.substring(intPosSemiColon + 1);
                        if ((strMax.equals(">") && intAge >= Integer.parseInt(strMin))
                                || (intAge >= Integer.parseInt(strMin) && intAge <= Integer.parseInt(strMax))) {
                            strAgeCat = strCat;
                            strAgeCats = "";
                        }
                    } else {
                        strAgeCats = "";
                    }
                }
                request.setAttribute("slicesPercentages", Common.getLocalizedString("pixalere.burn.perc." + strAgeCat, locale));
            } else {
                request.setAttribute("slicesPercentages", "");
            }

            Vector<LocationImagesVO> woundLocationsImagesAnterior = woundBD.getWoundLocationsImages(strGender, "A");
            Vector<LocationImagesVO> woundLocationsImagesPosterior = woundBD.getWoundLocationsImages(strGender, "P");
            Vector<LocationImagesVO> woundLocationsImagesLateral = woundBD.getWoundLocationsImages(strGender, "L");
            Vector<LocationImagesVO> woundLocationsImagesOther = woundBD.getWoundLocationsImages(strGender, "O");

            request.setAttribute("woundLocationsImagesAnterior", woundLocationsImagesAnterior);
            request.setAttribute("woundLocationsImagesPosterior", woundLocationsImagesPosterior);
            request.setAttribute("woundLocationsImagesLateral", woundLocationsImagesLateral);
            request.setAttribute("woundLocationsImagesOther", woundLocationsImagesOther);

            Vector<LocationImagesVO> woundLocationsImagesAll = woundBD.getWoundLocationsImages(strGender, "");
            String strShowFull = getShowRadio(woundLocationsImagesAll, "Full");
            String strShowLeftRight = getShowRadio(woundLocationsImagesAll, "LeftRight");
            String strShowMidline = getShowRadio(woundLocationsImagesAll, "Midline");

            // if a wound is selected from the wound profiles dropdown
            if (request.getParameter("wound_id") != null) {
                // set the session wound id to the current wound profile

                session.removeAttribute("alpha_id");
                session.removeAttribute("nursing_care_plan");
                session.removeAttribute("treatment_comments");
                session.removeAttribute("referrals");
                session.removeAttribute("referral");
                session.removeAttribute("wpti_wound");
                session.removeAttribute("wpti_skin");
                session.removeAttribute("wpti_incision");
                session.removeAttribute("wpti_drain");
                session.removeAttribute("wpti_ostomy");
                session.removeAttribute("wpti_burn");
                session.removeAttribute("patientcare");

                session.removeAttribute("offlineVO");
                //clear temp table
                //woundBD.dropTMP((String)patient_id,userVO.getId()+"");
            } else if (request.getParameter("wound_id") == null || request.getParameter("wound_id").equals("0") || request.getParameter("wound_id").equals("")) {
                // if no wound profile is selected.
            }
            // ------------initialize all the page controls--------------//
            // get all the currently uploaded and active wound profiles for the
            // current patient
            // check for null untill strange woundprofiles problem sorted out
            // *****************
            if (patient_id != null) {
                Vector<WoundVO> woundProfilesDrop = woundBD.getWoundProfilesDropdown(Integer.parseInt((String) patient_id),
                        true, userVO.getId().intValue());

                request.setAttribute("woundProfilesDrop", woundProfilesDrop);
                if(woundProfilesDrop == null || woundProfilesDrop.size()==0){
                     //check if user has done a patient profile first.
                    PatientProfileServiceImpl pservice = new PatientProfileServiceImpl();
                    PatientProfileVO ptmp = new PatientProfileVO();
                    ptmp.setPatient_id(new Integer(patient_id));
                    PatientProfileVO new_prof = (PatientProfileVO)pservice.getPatientProfile(ptmp);
                    if(new_prof==null){
                      return mapping.findForward("uploader.woundhome.success");  
                    }
                }
                // ---------------//
            }

            // Load dropdown lists/checkboxes
            Vector yearVect = new Vector();
            for (int z = 2150; z > 1900; z--) {
                String currYear = pdate.getYear(new Date());
                int curr = Integer.parseInt(currYear);
                if (curr >= z) {
                    yearVect.add(z + "");
                }
            }
            
            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("wound_acquired", listBD.getLists(LookupVO.WOUND_ACQUIRED));
            request.setAttribute("lab_done", listBD.getLists(LookupVO.LAB_DONE));
            request.setAttribute("type_of_ostomy", listBD.getLists(LookupVO.TYPE_OF_OSTOMY));
            //resort to alphabetical
            ArrayList list = new ArrayList(listBD.getLists(LookupVO.ETIOLOGY));
            Collections.sort(list,new LookupVO(language));
            request.setAttribute("list_wound", list);
            list = new ArrayList(listBD.getLists(LookupVO.SKIN_ETIOLOGY));
            Collections.sort(list,new LookupVO(language));
            request.setAttribute("list_skin", list);
            list = new ArrayList(listBD.getLists(LookupVO.OSTOMY_ETIOLOGY));
            Collections.sort(list,new LookupVO(language));
            request.setAttribute("list_ostomy", list);
            list = new ArrayList(listBD.getLists(LookupVO.POSTOP_ETIOLOGY));
            Collections.sort(list,new LookupVO(language));
            request.setAttribute("list_postop", list);
            list = new ArrayList(listBD.getLists(LookupVO.DRAINS_ETIOLOGY));
            Collections.sort(list,new LookupVO(language));
            request.setAttribute("list_drains", list);
            list = new ArrayList(listBD.getLists(LookupVO.BURN_ETIOLOGY));
            Collections.sort(list,new LookupVO(language));
            request.setAttribute("list_burn", list);
            request.setAttribute("pressure_ulcer", listBD.getLists(LookupVO.PRESSURE_ULCER_STAGE));
            request.setAttribute("wound_goals", listBD.getLists(LookupVO.WOUND_GOALS));
            request.setAttribute("incision_goals", listBD.getLists(LookupVO.INCISION_GOALS));
            request.setAttribute("skin_goals", listBD.getLists(LookupVO.SKIN_GOALS));
            request.setAttribute("drain_goals", listBD.getLists(LookupVO.TUBEDRAIN_GOALS));
            request.setAttribute("burn_goals", listBD.getLists(LookupVO.BURN_GOALS));
            request.setAttribute("ostomy_goals", listBD.getLists(LookupVO.OSTOMY_GOALS));
            request.setAttribute("patient_limitations", listBD.getLists(LookupVO.PATIENT_LIMITATIONS));
            request.setAttribute("teaching_goals", listBD.getLists(LookupVO.TEACHING_GOALS));

            //request.setAttribute("como_list", listBD_BD.findAllResourcesWithCategories(LookupVO.COMORBIDITIES));
            // ----------------------end the page control
            // check to see if there is a existing temp wound profile record if
            // so grab it.
            WoundProfileVO woundTMPVO = null;
            Vector justAlphas = new Vector();//getting alphas for csresults component
            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
            
            Vector<WoundAssessmentLocationVO> allalphas = null;
            if (wound_id != null && ((String) wound_id).length() > 0) {
                allalphas=walservice.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, true);//4th parameter (extra sort) is ignored
                
                woundTMPVO = new WoundProfileVO();
                woundTMPVO.setWound_id(new Integer((String) wound_id));
                woundTMPVO.setActive(new Integer(0));
                woundTMPVO.setProfessional_id(userVO.getId());
                woundTMPVO = (WoundProfileVO) woundBD.getWoundProfile(woundTMPVO);

                Hashtable alphaMap = new Hashtable();
                boolean burnAlpha = false;

                for (WoundAssessmentLocationVO v : allalphas) {
                    alphaMap.put(v.getAlpha(), v);
                    if (v.getWound_profile_type().getAlpha_type().equals("O")) {
                        request.setAttribute("showOstomyPostOpJS", "document.getElementById('showForOstomyPostOp').style.display='block';");
                        request.setAttribute("showOstomyJS", "document.getElementById('showForOstomy').style.display='block';");
                    } else if (v.getWound_profile_type().getAlpha_type().equals("D")) {
                        request.setAttribute("showOstomyPostOpJS", "document.getElementById('showForOstomyPostOp').style.display='block';");
                    } else if (v.getWound_profile_type().getAlpha_type().equals("T")) {
                        request.setAttribute("showOstomyPostOpJS", "document.getElementById('showForOstomyPostOp').style.display='block';");
                    } else if (v.getWound_profile_type().getAlpha_type().equals("B")) {
                        request.setAttribute("showBurnJS", "document.getElementById('showForBurn').style.display='block';");
                    } else if (v.getWound_profile_type().getAlpha_type().equals("A")) {
                        request.setAttribute("showWoundJS", "document.getElementById('showForWound').style.display='block';");
                    }
                    // justAlphas = alphas/icons
                    if (!v.getAlpha().equals("incs") && v.getDischarge().equals(0)) {
                        if (v.getAlpha().indexOf("burn") == -1 || burnAlpha == false) {
                            justAlphas.add(Common.getAlphaText(v.getAlpha(), locale));
                        } else if (v.getAlpha().indexOf("burn") > -1) {
                            burnAlpha = true;
                        }
                    }

                }

                request.setAttribute("allalphas", alphaMap);
                request.setAttribute("just_alphas", justAlphas);

                WoundAssessmentLocationVO tloco2 = new WoundAssessmentLocationVO();
                tloco2.setWound_id(new Integer((String) wound_id));
                tloco2.setDischarge(1); // Only closed alphas
                // tloco2.setAlpha_details().setDeleted(0); // No deleted incisions
                WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
                Collection<WoundAssessmentLocationVO> allalphas2 = locationDAO.findAllByCriteria(tloco2, -1, false, "");//4th parameter (extra sort) is ignored
                request.setAttribute("healed_alphas", allalphas2);

            }
            // find out the location of the wound to select the correct
            // location.

            if (request.getAttribute("error") == null) {

                if (woundTMPVO != null) {

                    if (woundTMPVO.getWound().getWound_location_position().equals("F") && Common.getLocalizedString("pixalere.hideFull", "en").equals("0")) {
                        strShowFull = "1";
                    }
                    if (woundTMPVO.getWound().getWound_location_position().equals("M")) {
                        strShowMidline = "1";
                    }
                    if (woundTMPVO.getWound().getWound_location_position().equals("L") || woundTMPVO.getWound().getWound_location_position().equals("R")) {
                        strShowLeftRight = "1";
                    }

                    // if the wound profile exists in the temp table and has not
                    // been uploaded
                    // populate the page with it.
                    // Because the gender might have changed on the Patient Profile, check fort the
                    // blue model image gender to be still matching (for unsaved, new wp's only)
                    if (woundTMPVO.getWound().getActive() == 0) {
                        if (strGender != "B") {
                            if (strGender == "F" && woundTMPVO.getWound().getImage_name().indexOf("m_") == 0) {
                                woundTMPVO.getWound().setImage_name("f_" + woundTMPVO.getWound().getImage_name().substring(2));
                            } else {
                                if (strGender == "M" && woundTMPVO.getWound().getImage_name().indexOf("f_") == 0) {
                                    woundTMPVO.getWound().setImage_name("m_" + woundTMPVO.getWound().getImage_name().substring(2));
                                }
                            }
                        }
                    }

                    request.setAttribute("woundProfile", woundTMPVO);

                    // request.setAttribute("woundLocationImage", woundTMPVO.getWound().getLocation_image());
                    WoundProfileVO woundVO = new WoundProfileVO();
                    woundVO.setWound_id(new Integer((String) wound_id));
                    woundVO.setCurrent_flag(new Integer(1));
                    woundVO.setDeleted(0);
                    woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO);
                    if (woundVO != null) {
                        request.setAttribute("user_signature", woundVO.getUser_signature());

                        ProfessionalVO userVO2 = woundVO.getProfessional();
                        if (userVO2 != null) {
                            request.setAttribute("profName", userVO2.getFullName());
                        }
                        request.setAttribute("start_date", woundVO.getWound().getStart_date());
                        request.setAttribute("startdate_signature", woundVO.getWound().getStartdate_signature());
                        // this is not a new wound profile so we need to disable
                        // the blue model.
                    }

                } else {
                    // the wound profile being worked on is already been
                    // uploaded so grab it
                    // from the existing wound profile table

                    WoundProfileVO woundVO = null;
                    if (wound_id != null && ((String) wound_id).length() > 0) {
                        woundVO = new WoundProfileVO();
                        woundVO.setWound_id(new Integer((String) wound_id));
                        woundVO.setCurrent_flag(new Integer(1));
                        woundVO.setDeleted(0);
                        woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO);
                    }
                    if (woundVO != null) {
                       
                        request.setAttribute("user_signature", woundVO.getUser_signature());
                        request.setAttribute("profName", (com.pixalere.utils.Common.getProfessionalsName((String) request.getAttribute("user_signature"), locale)));

                        request.setAttribute("start_date", woundVO.getWound().getStart_date());
                        request.setAttribute("startdate_signature", woundVO.getWound().getStartdate_signature());

                        // unserialize checkboxes from live tableSerialize.arrayIze(currentProfile.getCo_morbidities())
                        request.setAttribute("unserializePatientLimitations", Serialize.arrayIze(woundVO.getPatient_limitations()));
                        request.setAttribute("unserializeTypeOfOstomy", Serialize.arrayIze(woundVO.getType_of_ostomy()));

                        if (woundVO.getWound().getWound_location_position().equals("F") && Common.getLocalizedString("pixalere.hideFull", locale).equals("0")) {
                            strShowFull = "1";
                        }
                        if (woundVO.getWound().getWound_location_position().equals("M")) {
                            strShowMidline = "1";
                        }
                        if (woundVO.getWound().getWound_location_position().equals("L") || woundVO.getWound().getWound_location_position().equals("R")) {
                            strShowLeftRight = "1";
                        }
                        woundVO.setReview_done(null);
                        if (Common.getConfig("3rdWeekEtiologyAlert").equals("1")) {
                            //Check Alpha
                            boolean etiologyAlert = false;

                            try {
                                for (WoundAssessmentLocationVO alpha : allalphas) {
                                    if (alpha.getActive().equals(1) && alpha.getDischarge().equals(0)) {
                                        
                                            AbstractAssessmentVO tmp = null;
                                            if (alpha.getWound_profile_type().getAlpha_type().equals(Constants.WOUND_PROFILE_TYPE)) {
                                                tmp = new AssessmentEachwoundVO();tmp.setDeleted(0);
                                                tmp.setAlpha_id(alpha.getId());tmp.setActive(1);
                                            } else if (alpha.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                                                tmp = new AssessmentDrainVO();tmp.setDeleted(0);
                                                tmp.setAlpha_id(alpha.getId());tmp.setActive(1);
                                            } else if (alpha.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE)) {
                                                tmp = new AssessmentOstomyVO();tmp.setDeleted(0);
                                                tmp.setAlpha_id(alpha.getId());tmp.setActive(1);
                                            } else if (alpha.getWound_profile_type().getAlpha_type().equals(Constants.INCISION_PROFILE_TYPE)) {
                                                tmp = new AssessmentIncisionVO();tmp.setDeleted(0);
                                                tmp.setAlpha_id(alpha.getId());tmp.setActive(1);
                                            }else if (alpha.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE)) {
                                                tmp = new AssessmentSkinVO();tmp.setDeleted(0);
                                                tmp.setAlpha_id(alpha.getId());tmp.setActive(1);
                                            }
                                            AbstractAssessmentVO assess = aservice.getAssessment(tmp, false);
                                                    
                                             if(assess == null){}
                                            else if ((assess.getWoundAssessment()!=null && (assess.getWoundAssessment().getCreated_on().before(PDate.addDays(alpha.getOpen_timestamp(), 21))) && PDate.addDays(alpha.getOpen_timestamp(), 21).before(new Date()))) {
                                                etiologyAlert = true;
                                            }
                                        
                                    }

                                }
                            } catch (NullPointerException e) {
                                //ignore check.
                            }
                            if (etiologyAlert == true) {
                                request.setAttribute("etiology_warning", "1");
                            }
                        }
                    }

                    // populate the page with the wound profile
                    request.setAttribute("woundProfile", woundVO);
                    // request.setAttribute("woundLocationImage", woundVO.getWound().getLocation_image());
                }

                if (woundTMPVO != null) {
                    // unserialize checkboxes from temp table
                    request.setAttribute("unserializePatientLimitations", Serialize.arrayIze(woundTMPVO.getPatient_limitations()));
                    request.setAttribute("unserializeTypeOfOstomy", Serialize.arrayIze(woundTMPVO.getType_of_ostomy()));

                }

            }

            request.setAttribute("strShowFull", strShowFull);
            request.setAttribute("strShowLeftRight", strShowLeftRight);
            request.setAttribute("strShowMidline", strShowMidline);
        } catch (com.pixalere.common.DataAccessException e) {
            log.error("An application exception has been raised in WoundProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in WoundProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (NullPointerException e) {
            log.error("An application exception has been raised in WoundProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        // populated wound profile page.
        return (mapping.findForward("uploader.wound.success"));

    }
}
