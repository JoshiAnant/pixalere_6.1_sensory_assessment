package com.pixalere.struts.wound;

import javax.servlet.http.HttpServletRequest;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import com.pixalere.wound.bean.WoundVO;
import java.util.Vector;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpSession;

//initializes the wound profile page.
public class WoundHomeSetupAction extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundHomeSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);

        WoundServiceImpl woundBD = new WoundServiceImpl(language);

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
        request.setAttribute("page", "woundprofile");

        Vector<WoundVO> woundProfilesDrop = woundBD.getWoundProfilesDropdown(Integer.parseInt(patient_id),
                true, userVO.getId().intValue());

        request.setAttribute("woundProfilesDrop", woundProfilesDrop);
        return (mapping.findForward("uploader.woundhome.success"));
    }
}
