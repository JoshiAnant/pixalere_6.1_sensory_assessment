package com.pixalere.struts.wound;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.wound.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;

public class WoundProfileForm extends ActionForm {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundProfileForm.class);
    HttpSession session = null;
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        session = request.getSession();
        return errors;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
//nothing
    }
    
    
    private int cancel;
    public String getLocation() {
        return location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getLocation_detailed() {
        return location_detailed;
    }
    
    public void setLocation_detailed(String location_detailed) {
        this.location_detailed = location_detailed;
    }
  
    
    public String getBluemodel_image() {
        return bluemodel_image;
    }
    
    public void setBluemodel_image(String bluemodel_image) {
        this.bluemodel_image = bluemodel_image;
    }
    
    public int getBluemodel_image_id() {
        return bluemodel_image_id;
    }
    
    public void setBluemodel_image_id(int bluemodel_image_id) {
        this.bluemodel_image_id = bluemodel_image_id;
    }
    
    public int getLimb() {
        return limb;
    }
    
    public void setLimb(int limb) {
        this.limb = limb;
    }
    
    public int getLowerlimb() {
        return lowerlimb;
    }
    
    public void setLowerlimb(int lowerlimb) {
        this.lowerlimb = lowerlimb;
    }
    
    public String getPosition() {
        return position;
    }
    
    public void setPosition(String position) {
        this.position = position;
    }
    
    public String getView() {
        return view;
    }
    
    public void setView(String view) {
        this.view = view;
    }
    
    public String getStart_date() {
        return start_date;
    }
    
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }
    
    public String getStartdate_signature() {
        return startdate_signature;
    }
    
    public void setStartdate_signature(String startdate_signature) {
        this.startdate_signature = startdate_signature;
    }
    
    
    public String getPrev_prods_comments() {
        return prev_prods_comments;
    }
    
    public void setPrev_prods_comments(String prev_prods_comments) {
        this.prev_prods_comments = prev_prods_comments;
    }
  
    
    public void setReason_for_care(String reason_for_care){
        this.reason_for_care=reason_for_care;
    }
    public String getReason_for_care(){
        return reason_for_care;
    }
  
    public void setEtiology(String[] etiology){
        this.etiology=etiology;
    }
    public String[] getEtiology(){
        return etiology;
    }
    
    public void setGoals(String[] goals){
        this.goals=goals;
    }
    public String[] getGoals(){
        return goals;
    }
    public void setPressure_ulcer(int pressure_ulcer){
        this.pressure_ulcer=pressure_ulcer;
    }
    public int getPressure_ulcer(){
        return pressure_ulcer;
    }

    /**
     * @method getCnsd_day
     * @field cnsd_day - int
     * @return Returns the cnsd_day.
     */
    
    
    public String[] getAntibiotics() {
        return antibiotics;
    }
    
    public void setAntibiotics(String[] antibiotics) {
        this.antibiotics=antibiotics;
    }
    
    public String getCns_name() {
        return cns_name;
    }
    
    public void setCns_name(String cns_name) {
        this.cns_name=cns_name;
    }
    
    
    public WoundProfileVO getFormData(int language) throws ApplicationException{
        String locale = Common.getLanguageLocale(language);

        WoundProfileVO woundProfileVO = new WoundProfileVO();
        WoundVO woundGrp=new WoundVO();
        
        woundGrp.setWound_location(getLocation());
        woundGrp.setImage_name(getBluemodel_image());
        woundGrp.setLocation_image_id(getBluemodel_image_id());
        woundGrp.setWound_location_position(getPosition());
        woundGrp.setStart_date(PDate.getDate(start_date));
        woundGrp.setStartdate_signature(startdate_signature);
        String strExtended = "";
        String location = getLocation_detailed();
        location = location.replaceAll(Common.getLocalizedString("pixalere.woundprofile.form.left",locale)+" ","");
        location = location.replaceAll(Common.getLocalizedString("pixalere.woundprofile.form.right",locale)+" ","");
        
        if (getPosition().indexOf("L") != -1) {
            strExtended = Common.getLocalizedString("pixalere.woundprofile.form.left",locale)+" ";
        } else if (getPosition().indexOf("R") != -1) {
            strExtended = Common.getLocalizedString("pixalere.woundprofile.form.right",locale)+" ";
        }
        strExtended = strExtended + location;
        
        woundGrp.setWound_location_detailed(strExtended);
        woundProfileVO.setActive(new Integer(0));
        woundProfileVO.setCurrent_flag(new Integer(0));
        woundProfileVO.setPatient_id(new Integer((String)session.getAttribute("patient_id")));
        woundProfileVO.setReview_done(getReview_done());
       
        woundProfileVO.setPressure_ulcer(new Integer(getPressure_ulcer()));
        woundProfileVO.setPrev_prods(Common.stripHTML(getPrev_prods_comments()));
        woundProfileVO.setType_of_ostomy(Serialize.serialize(getType_of_ostomy()));
        woundProfileVO.setReason_for_care(getReason_for_care());
        woundProfileVO.setDate_surgery(PDate.getDate(getDate_surgery_month(),getDate_surgery_day(),getDate_surgery_year()));
        woundProfileVO.setMarking_prior_surgery(getMarking_prior_surgery()==null?-1:getMarking_prior_surgery());
        woundProfileVO.setOperative_procedure_comments(getOperative_procedure_comments());
        woundProfileVO.setSurgeon(getSurgeon());
        woundProfileVO.setTeaching_goals(getTeaching_goals());
        woundProfileVO.setBurn_areas(getBurn_areas());
        woundProfileVO.setPreliminary_tbsa(PDate.getDate(getPreliminary_date_month(),getPreliminary_date_day(),getPreliminary_date_year()));
        woundProfileVO.setFinal_tbsa(PDate.getDate(getFinal_date_month(),getFinal_date_day(),getFinal_date_year()));
        woundProfileVO.setTbsa_percentage(getTbsa_percentage());
        woundProfileVO.setPatient_limitations(Serialize.serialize(getPatient_limitations()));
        woundProfileVO.setDeleted(0);
        
        woundProfileVO.setRoot_cause_addressed(root_cause_addressed);
        woundProfileVO.setRoot_cause_addressed_comments(getRoot_cause_addressed_comments());
        woundProfileVO.setWound(woundGrp);
        return woundProfileVO;
    }
    
    private String prev_prods_comments;

    private String startdate_signature;
    
    private int is_skin;
    private String operative_procedure_comments;
    private int date_surgery_day;
    private int date_surgery_month;
    private int date_surgery_year;
    private String surgeon;
    private Integer marking_prior_surgery;
    private String[] patient_limitations;
    private Integer teaching_goals;
    private String location;
    private String location_detailed;
    private String bluemodel_image;
    private int bluemodel_image_id;
    private int limb;
    private int lowerlimb;
    private String position;
    private String view;
    private String burn_areas;
    private int preliminary_date_day;
    private int preliminary_date_month;
    private int preliminary_date_year;
    private int final_date_day;
    private int final_date_month;
    private int final_date_year;
    private String tbsa_percentage;
    private String start_date;
    private String[] antibiotics;
    private String[] incs_coords;
    private String[] burn_coords;
    private String cns_name;
    private String[] etiology;
    private String[] wound_acquired;
    private String[] goals;
    private String[] type_of_ostomy;
    private int pressure_ulcer;
    private String reason_for_care;
    private int review_done;
    private int root_cause_addressed;
    private String root_cause_addressed_comments;
    public String getOperative_procedure_comments() {
        return operative_procedure_comments;
    }

    public void setOperative_procedure_comments(String operative_procedure_comments) {
        this.operative_procedure_comments = operative_procedure_comments;
    }

    

    public String getSurgeon() {
        return surgeon;
    }

    public void setSurgeon(String surgeon) {
        this.surgeon = surgeon;
    }

    public Integer getMarking_prior_surgery() {
        return marking_prior_surgery;
    }

    public void setMarking_prior_surgery(Integer marking_prior_surgery) {
        this.marking_prior_surgery = marking_prior_surgery;
    }

    public String[] getPatient_limitations() {
        return patient_limitations;
    }

    public void setPatient_limitations(String[] patient_limitations) {
        this.patient_limitations = patient_limitations;
    }

    public Integer getTeaching_goals() {
        return teaching_goals;
    }

    public void setTeaching_goals(Integer teaching_goals) {
        this.teaching_goals = teaching_goals;
    }

    public int getDate_surgery_day() {
        return date_surgery_day;
    }

    public void setDate_surgery_day(int date_surgery_day) {
        this.date_surgery_day = date_surgery_day;
    }

    public int getDate_surgery_month() {
        return date_surgery_month;
    }

    public void setDate_surgery_month(int date_surgery_month) {
        this.date_surgery_month = date_surgery_month;
    }

    public int getDate_surgery_year() {
        return date_surgery_year;
    }

    public void setDate_surgery_year(int date_surgery_year) {
        this.date_surgery_year = date_surgery_year;
    }

    public String[] getType_of_ostomy() {
        return type_of_ostomy;
    }

    public void setType_of_ostomy(String[] type_of_ostomy) {
        this.type_of_ostomy = type_of_ostomy;
    }
    

    public String getBurn_areas() {
        return burn_areas;
    }

    public void setBurn_areas(String burn_areas) {
        this.burn_areas = burn_areas;
    }

    public int getPreliminary_date_day() {
        return preliminary_date_day;
    }

    public void setPreliminary_date_day(int preliminary_date_day) {
        this.preliminary_date_day = preliminary_date_day;
    }

    public int getPreliminary_date_month() {
        return preliminary_date_month;
    }

    public void setPreliminary_date_month(int preliminary_date_month) {
        this.preliminary_date_month = preliminary_date_month;
    }

    public int getPreliminary_date_year() {
        return preliminary_date_year;
    }

    public void setPreliminary_date_year(int preliminary_date_year) {
        this.preliminary_date_year = preliminary_date_year;
    }

    public int getFinal_date_day() {
        return final_date_day;
    }

    public void setFinal_date_day(int final_date_day) {
        this.final_date_day = final_date_day;
    }

    public int getFinal_date_month() {
        return final_date_month;
    }

    public void setFinal_date_month(int final_date_month) {
        this.final_date_month = final_date_month;
    }

    public int getFinal_date_year() {
        return final_date_year;
    }

    public void setFinal_date_year(int final_date_year) {
        this.final_date_year = final_date_year;
    }

    public String getTbsa_percentage() {
        return tbsa_percentage;
    }

    public void setTbsa_percentage(String tbsa_percentage) {
        this.tbsa_percentage = tbsa_percentage;
    }


    public int getReview_done(){
        return review_done;
    }
    public void setReview_done(int review_done){
        this.review_done=review_done;
    }

    /**
     * @return the incs_coords
     */
    public String[] getIncs_coords() {
        return incs_coords;
    }

    /**
     * @param incs_coords the incs_coords to set
     */
    public void setIncs_coords(String[] incs_coords) {
        this.incs_coords = incs_coords;
    }

    /**
     * @return the burn_coords
     */
    public String[] getBurn_coords() {
        return burn_coords;
    }

    /**
     * @param burn_coords the burn_coords to set
     */
    public void setBurn_coords(String[] burn_coords) {
        this.burn_coords = burn_coords;
    }

    /**
     * @return the Wound_obtained
     */
    public String[] getWound_acquired() {
        return wound_acquired;
    }

    /**
     * @param quadrant the quadrant to set
     */
    public void setWound_acquired(String[] wound_acquired) {
        this.wound_acquired = wound_acquired;
    }

    /**
     * @return the cancel
     */
    public int getCancel() {
        return cancel;
    }

    /**
     * @param cancel the cancel to set
     */
    public void setCancel(int cancel) {
        this.cancel = cancel;
    }

         /**
     * @return the root_caused_addressed
     */
    public Integer getRoot_cause_addressed() {
        return root_cause_addressed;
    }

    /**
     * @param root_caused_addressed the root_caused_addressed to set
     */
    public void setRoot_cause_addressed(Integer root_caused_addressed) {
        this.root_cause_addressed = root_caused_addressed;
    }

    /**
     * @return the root_cause_addressed_comments
     */
    public String getRoot_cause_addressed_comments() {
        return root_cause_addressed_comments;
    }

    /**
     * @param root_cause_addressed_comments the root_cause_addressed_comments to set
     */
    public void setRoot_cause_addressed_comments(String root_cause_addressed_comments) {
        this.root_cause_addressed_comments = root_cause_addressed_comments;
    }

}
