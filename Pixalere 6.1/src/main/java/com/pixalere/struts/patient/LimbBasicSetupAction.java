package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;

import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.*;
import com.pixalere.auth.bean.ProfessionalVO;

public class LimbBasicSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LimbBasicSetupAction.class);
 
    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        PatientServiceImpl pamanager = new PatientServiceImpl(language);
        log.error("Session: PP: "+session);
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        log.error("SEssion Patient_ID:"+session.getAttribute("patient_id"));
        String patient_id = (String) session.getAttribute("patient_id");
        
        //Generate Token - prevent multiple posts
        saveToken(request);
        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\nSetting Session: "+request.getParameter("form"));
        try {
            request.setAttribute("page", "patient");

            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            LimbBasicAssessmentVO pp = new LimbBasicAssessmentVO();
            pp.setPatient_id(new Integer(patient_id));
            pp.setActive(new Integer(1));
            
            pp.setDeleted(new Integer(0));
            LimbBasicAssessmentVO currentProfile = manager.getLimbBasicAssessment(pp);
            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na",locale)));

            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
      

            comp.setFlowchart(com.pixalere.utils.Constants.LIMB_ASSESSMENT_BASIC);
            java.util.Collection components3 = gui.getAllComponents(comp);
            java.util.Hashtable hashized3 = Common.hashComponents(components3);
            request.setAttribute("componentLimb", hashized3);

           
            if (currentProfile != null) {

                ProfessionalVO userVO2 = currentProfile.getProfessional();
                if (userVO2 != null) {
                    request.setAttribute("profName", userVO2.getFullName());
                }
            }
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");

            LimbBasicAssessmentVO ltmp = new LimbBasicAssessmentVO();
     
            ltmp.setActive(0);
            ltmp.setPatient_id(new Integer(patient_id));
            ltmp.setProfessional_id(userVO.getId());
            
            LimbBasicAssessmentVO limb = manager.getLimbBasicAssessment(ltmp);
          
            if (limb == null && request.getParameter("pop")!=null && ((String)request.getParameter("pop")).equals("1")) {
                limb = currentProfile;
            } else if(limb !=null ) {
                request.setAttribute("limb_comments", Common.convertCarriageReturns(limb.getLimb_comments(), true));

            }
            
            if (limb != null) {
                 request.setAttribute("unserializeLeftMissingLimbs", Serialize.arrayIze(limb.getLeft_missing_limbs()));
                request.setAttribute("unserializeRightMissingLimbs", Serialize.arrayIze(limb.getRight_missing_limbs()));
                request.setAttribute("unserializeLeftPainAssessment", Serialize.arrayIze(limb.getLeft_pain_assessment()));
                request.setAttribute("unserializeRightPainAssessment", Serialize.arrayIze(limb.getRight_pain_assessment()));
                request.setAttribute("unserializeLeftSkinAssessment", Serialize.arrayIze(limb.getLeft_skin_assessment()));
                request.setAttribute("unserializeRightSkinAssessment", Serialize.arrayIze(limb.getRight_skin_assessment()));
                request.setAttribute("unserializeRightSensory", Serialize.arrayIze(limb.getRight_sensory()));
                request.setAttribute("unserializeLeftSensory", Serialize.arrayIze(limb.getLeft_sensory()));

            }
          
            if (limb != null) {
                if (Common.getConfig("defaultHideOnPP").equals("1")) {
                    limb.setLimb_show(1);
                }
            }
            
            request.setAttribute("limb", limb);
            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("doppler", listBD.getLists(LookupVO.DOPPLER));
            request.setAttribute("palpation", listBD.getLists(LookupVO.PALPATION));
            request.setAttribute("missing_limbs", listBD.getLists(LookupVO.MISSING_LIMBS));
            request.setAttribute("missing_limbs_upper", listBD.getLists(LookupVO.MISSING_UPPER_LIMB));
            request.setAttribute("pain_assessment", listBD.getLists(LookupVO.PAIN_ASSESSMENT));
            request.setAttribute("sensory", listBD.getLists(LookupVO.SENSORY));
            request.setAttribute("sensation", listBD.getLists(LookupVO.SENSATION));
            request.setAttribute("deformities", listBD.getLists(LookupVO.DEFORMITIES));
            request.setAttribute("skin", listBD.getLists(LookupVO.SKIN_ASSESSMENT));
           
            request.setAttribute("range_of_motion", listBD.getLists(LookupVO.RANGE_OF_MOTION));
            request.setAttribute("toes", listBD.getLists(LookupVO.TOES));
            request.setAttribute("limbColor", listBD.getLists(LookupVO.LIMB_COLOR));
            request.setAttribute("limbTemperature", listBD.getLists(LookupVO.LIMB_TEMPERATURE));
            request.setAttribute("limbEdema", listBD.getLists(LookupVO.LIMB_EDEMA));
            request.setAttribute("upperLimbEdema", listBD.getLists(LookupVO.UPPER_LIMB_EDEMA_LOCATION));
            request.setAttribute("limbEdemaSeverity", listBD.getLists(LookupVO.LIMB_EDEMA_SEVERITY));
            request.setAttribute("transcutaneous_pressures", listBD.getLists(LookupVO.TRANSCUTANEOUS_PRESSURES));
            request.setAttribute("lab_done", listBD.getLists(LookupVO.LAB_DONE));
            
        }  catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (patient_id == null) {
            return (mapping.findForward("uploader.patienthome.success"));
        } else {
            return (mapping.findForward("uploader.limbbasic.success"));
        }
    }
}
