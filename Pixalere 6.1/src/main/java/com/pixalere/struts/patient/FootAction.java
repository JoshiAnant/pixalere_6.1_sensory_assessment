package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import javax.servlet.http.HttpServletRequest;

import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.utils.Constants;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;

import java.util.*;

import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.guibeans.RowData;
import com.pixalere.guibeans.FieldValues;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class FootAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FootAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
   	 

    	FootForm patientProfileForm = (FootForm) form;
        HttpSession session = request.getSession();
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
      
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        boolean tokenValid = isTokenValid(request);
        try {
            PatientServiceImpl pamanager = new PatientServiceImpl(language);
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }

            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
           
            if (tokenValid) {
                resetToken(request);
                PatientProfileServiceImpl managerBD = new PatientProfileServiceImpl(language);

                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                ListServiceImpl lservice = new ListServiceImpl(language);
                String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);

                if (((String) request.getParameter("cancel") == null) || !((String) request.getParameter("cancel")).equals("yes")) {


                    FootAssessmentVO foot = patientProfileForm.getFootData(userVO);
                    if (foot != null) {
                    	
                      
                    	foot.setPatient_id(new Integer(patient_id));
                    	foot.setProfessional_id(userVO.getId());
                    	foot.setUser_signature(user_signature);
                        foot.setCreated_on(new Date());
                        foot.setLastmodified_on(new Date());
                   
                        FootAssessmentVO tmpFA = new FootAssessmentVO();
                        tmpFA.setPatient_id(new Integer(patient_id));
                        tmpFA.setActive(0);
                        tmpFA.setProfessional_id(userVO.getId());

                        FootAssessmentVO tmpFootAssessment = managerBD.getFootAssessment(tmpFA);

                        if (tmpFootAssessment != null) {
                            foot.setId(tmpFootAssessment.getId());
                        }

                        if (foot != null) {
                            foot.setDeleted(0);
                            foot.setProfessional(userVO);
                        }

                            foot.setDeleted(0);
                            managerBD.saveFoot(foot);
                        
                    }
                } else {
                    if (patient_id != null && userVO != null) {
                        managerBD.dropTMPFoot(patient_id, userVO.getId() + "", null);
                    }
                }


            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfile.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));

        }

        if (request.getParameter("page") == null && userVO != null) {
            ActionErrors errors = new ActionErrors();
            errors.add("errors", new ActionError("pixalere.common.page.error"));
            saveErrors(request, errors);
            return (mapping.findForward("uploader.patientprofile.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("exam")) {
            return (mapping.findForward("uploader.go.physicalexam"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("sensory")) {
	           return (mapping.findForward("uploader.go.sensoryassessment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("braden")) {
            return (mapping.findForward("uploader.go.braden"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("foot")) {
            return (mapping.findForward("uploader.go.foot"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbbasic")) {
            return (mapping.findForward("uploader.go.limbbasic"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbadv")) {
            return (mapping.findForward("uploader.go.limbadv"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbupper")) {
            return (mapping.findForward("uploader.go.limbupper"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("investigations")) {
            return (mapping.findForward("uploader.go.investigations"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.woundprofiles"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("uploader.go.patient"));
        }

    }
}
