package com.pixalere.struts.patient;

import com.pixalere.patient.bean.DiagnosticInvestigationVO;
import com.pixalere.patient.bean.InvestigationsVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author
 *
 */
public class InvestigationsForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InvestigationsForm.class);
    HttpSession session = null;

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping,
            HttpServletRequest request) {
    }
    private String blood_sugar_meal;
    private double blood_sugar;
    private int bloodsugar_month;
    private int bloodsugar_year;
    private int bloodsugar_day;
    private int albumin_day;
    private int albumin_month;
    private int albumin_year;
    private int prealbumin_year;
    private int prealbumin_day;
    private int prealbumin_month;
    private double albumin;
    private double pre_albumin;
    private String[] investigation;
    private int hga1c;
    private int purs_score;
    
    public DiagnosticInvestigationVO getInvestFormData() throws ApplicationException {
        DiagnosticInvestigationVO d = new DiagnosticInvestigationVO();
        d.setActive(0);
        d.setAlbumin_date(PDate.getDate(getAlbumin_month(), getAlbumin_day(), getAlbumin_year()));
        d.setAlbumin((getAlbumin() <= 0.0 ? null : getAlbumin()));
        d.setPre_albumin((getPre_albumin() <= 0.0 ? null : getPre_albumin()));
        d.setPrealbumin_date(PDate.getDate(getPrealbumin_month(), getPrealbumin_day(), getPrealbumin_year()));
        d.setBlood_sugar_date(PDate.getDate(getBloodsugar_month(), getBloodsugar_day(), getBloodsugar_year()));
        d.setBlood_sugar(getBlood_sugar() == 0 ? null : getBlood_sugar());
        d.setBlood_sugar_meal(getBlood_sugar_meal().equals("") ? "" : getBlood_sugar_meal());
        d.setHga1c(getHga1c());
        d.setPurs_score(getPurs_score());
        return d;

    }

    public List<InvestigationsVO> getFormData(ProfessionalVO userVO, HttpServletRequest request) throws ApplicationException {
        List<InvestigationsVO> in_arrays = new ArrayList();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {
            if (investigation != null) {

                SimpleDateFormat sf = new SimpleDateFormat("dd/MMMMM/yyyy");
                for (String item : investigation) {

                    int index = item.indexOf(" : ");
                    String id = item.substring(0, index);

                    String date = item.substring(index + 3, item.length());
                    Date d = sf.parse(date);
                    InvestigationsVO i = new InvestigationsVO();
                    i.setInvestigation_date(d);
                    i.setLookup_id(new Integer(id));

                    in_arrays.add(i);
                }
            }
        } catch (ParseException e) {
        }
        return in_arrays;

    }

    public int getAlbumin_day() {
        return albumin_day;
    }

    public void setAlbumin_day(int albumin_day) {
        this.albumin_day = albumin_day;
    }

    public int getAlbumin_month() {
        return albumin_month;
    }

    public void setAlbumin_month(int albumin_month) {
        this.albumin_month = albumin_month;
    }

    public int getAlbumin_year() {
        return albumin_year;
    }

    public void setAlbumin_year(int albumin_year) {
        this.albumin_year = albumin_year;
    }

    public int getPrealbumin_year() {
        return prealbumin_year;
    }

    public void setPrealbumin_year(int prealbumin_year) {
        this.prealbumin_year = prealbumin_year;
    }

    public int getPrealbumin_day() {
        return prealbumin_day;
    }

    public void setPrealbumin_day(int prealbumin_day) {
        this.prealbumin_day = prealbumin_day;
    }

    public int getPrealbumin_month() {
        return prealbumin_month;
    }

    public void setPrealbumin_month(int prealbumin_month) {
        this.prealbumin_month = prealbumin_month;
    }

    public double getAlbumin() {
        return albumin;
    }

    public void setAlbumin(double albumin) {
        this.albumin = albumin;
    }

    public double getPre_albumin() {
        return pre_albumin;
    }

    public void setPre_albumin(double pre_albumin) {
        this.pre_albumin = pre_albumin;
    }

    /**
     * @return the blood_sugar_meal
     */
    public String getBlood_sugar_meal() {
        return blood_sugar_meal;
    }

    /**
     * @param blood_sugar_meal the blood_sugar_meal to set
     */
    public void setBlood_sugar_meal(String blood_sugar_meal) {
        this.blood_sugar_meal = blood_sugar_meal;
    }

    /**
     * @return the blood_sugar
     */
    public double getBlood_sugar() {
        return blood_sugar;
    }

    /**
     * @param blood_sugar the blood_sugar to set
     */
    public void setBlood_sugar(double blood_sugar) {
        this.blood_sugar = blood_sugar;
    }

    /**
     * @return the bloodsugar_month
     */
    public int getBloodsugar_month() {
        return bloodsugar_month;
    }

    /**
     * @param bloodsugar_month the bloodsugar_month to set
     */
    public void setBloodsugar_month(int bloodsugar_month) {
        this.bloodsugar_month = bloodsugar_month;
    }

    /**
     * @return the bloodsugar_year
     */
    public int getBloodsugar_year() {
        return bloodsugar_year;
    }

    /**
     * @param bloodsugar_year the bloodsugar_year to set
     */
    public void setBloodsugar_year(int bloodsugar_year) {
        this.bloodsugar_year = bloodsugar_year;
    }

    /**
     * @return the bloodsugar_day
     */
    public int getBloodsugar_day() {
        return bloodsugar_day;
    }

    /**
     * @param bloodsugar_day the bloodsugar_day to set
     */
    public void setBloodsugar_day(int bloodsugar_day) {
        this.bloodsugar_day = bloodsugar_day;
    }

    public String[] getInvestigation() {
        return investigation;
    }

    public void setInvestigation(String[] investigation) {
        this.investigation = investigation;
    }

    /**
     * @return the hga1c
     */
    public int getHga1c() {
        return hga1c;
    }

    /**
     * @param hga1c the hga1c to set
     */
    public void setHga1c(int hga1c) {
        this.hga1c = hga1c;
    }

    /**
     * @return the purs_score
     */
    public int getPurs_score() {
        return purs_score;
    }

    /**
     * @param purs_score the purs_score to set
     */
    public void setPurs_score(int purs_score) {
        this.purs_score = purs_score;
    }
}