package com.pixalere.struts.patient;

import com.pixalere.patient.bean.ExternalReferralVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.PatientProfileVO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author
 *
 */
public class PatientProfileForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientProfileForm.class);
    HttpSession session = null;

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping,
            HttpServletRequest request) {
    }

    public Integer[] getIm() {
        return im;
    }

    public void setIm(Integer[] im) {
        this.im = im;
    }

    public String getIm_other() {
        return im_other;
    }

    public void setIm_other(String im_other) {
        this.im_other = im_other;
    }

    public Integer[] getMedication_comments() {
        return medication_comments;
    }

    public void setMedication_comments(Integer[] medication_comments) {
        this.medication_comments = medication_comments;
    }

    public Integer[] getCurrent_comorbidities() {
        return current_comorbidities;
    }

    public void setCurrent_comorbidities(Integer[] current_comorbidities) {
        this.current_comorbidities = current_comorbidities;
    }

    public int getReferral() {
        return referral;
    }

    public void setReferral(int referral) {
        this.referral = referral;
    }

    public int getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public String getProfessionals() {
        return professionals;
    }

    public void setProfessionals(String professionals) {
        this.professionals = professionals;
    }

    private String professionals;
    private Integer[] im;
    private String im_other;
    private Integer[] medication_comments;
    private Integer[] current_comorbidities;
    private String[] external_referrals;
    private int referral;
    private int patient_id;
    private int purs_score;
    private String surgical_history;
    private String blood_sugar_meal = "";
    private double blood_sugar;
    private int bloodsugar_month;
    private int bloodsugar_year;
    private int bloodsugar_day;
    private int hga1c;

    private Integer[] patient_limitations;
    private int discharge_planning_initiated;
    private int chronic_disease_initiated;
    private int client_redistribution_system;
    private int referral_longterm_pressure;
    private int barriers_addressed;
    private String barriers_addressed_comments;
    private String system_barriers_comments;
    private int quality_of_life_addressed;
    private String quality_of_life_addressed_comments;
    private Integer pressure_redistribution_initiated;

    public PatientProfileVO getFormData(ProfessionalVO userVO, HttpServletRequest request) throws ApplicationException {
        PatientProfileVO profileTMP = new PatientProfileVO();
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);

        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        PatientProfileVO profileVO = new PatientProfileVO(new Integer(0), new Integer(0), new Integer((String) session.getAttribute("patient_id")),
                "",
                new Integer(getReferral()),
                getProfessionals(),
                new Date(), new Date(),
                Common.stripHTML(getSurgical_history()), getReview_done(), getPurs_score());
        //profileVO.setBlood_sugar_date(PDate.getDate(bloodsugar_year,bloodsugar_month,bloodsugar_day));
        //profileVO.setBlood_sugar(blood_sugar==0?null:blood_sugar);
        //profileVO.setBlood_sugar_meal(blood_sugar_meal.equals("")?"":blood_sugar_meal);

        if (barriers_addressed > 0) {
            profileVO.setBarriers_addressed(barriers_addressed);
        }
        if (barriers_addressed_comments != null && !barriers_addressed_comments.equals("")) {
            profileVO.setBarriers_addressed_comments(Common.stripHTML(barriers_addressed_comments));
        }
        if (chronic_disease_initiated > 0) {
            profileVO.setChronic_disease_initiated(chronic_disease_initiated);
        }
        if (discharge_planning_initiated > 0) {
            profileVO.setDischarge_planning_initiated(discharge_planning_initiated);
        }
        if (quality_of_life_addressed > 0) {
            profileVO.setQuality_of_life_addressed(quality_of_life_addressed);
        }
        if (quality_of_life_addressed_comments != null && !quality_of_life_addressed_comments.equals("")) {
            profileVO.setQuality_of_life_addressed_comments(Common.stripHTML(quality_of_life_addressed_comments));
        }
        if (system_barriers_comments != null && !system_barriers_comments.equals("")) {
            profileVO.setSystem_barriers_comments(Common.stripHTML(system_barriers_comments));
        }
        //if(getHga1c()>=0){profileVO.setHga1c(getHga1c());}

        List pp_arrays = new ArrayList();
        List er_arrays = new ArrayList();
        if (im != null) {
            for (Integer item : im) {
                String other = "";
                if (request.getParameter("other" + item) != null) {
                    other = (String) request.getParameter("other" + item);
                }
                PatientProfileArraysVO it = new PatientProfileArraysVO(null, item, other);
                pp_arrays.add(it);
            }
        }
        if (medication_comments != null) {
            for (Integer item : medication_comments) {
                String other = "";
                if (request.getParameter("other" + item) != null) {
                    other = (String) request.getParameter("other" + item);
                }
                PatientProfileArraysVO it = new PatientProfileArraysVO(null, item, other);
                pp_arrays.add(it);
            }
        }
        if (external_referrals != null) {
            for (String item : external_referrals) {
                String[] split = item.split(" : ");
                String one = "";
                String two = "";
                if (split.length == 2) {
                    one = split[0];
                    two = split[1];
                }

                if (one != null && two != null && !one.equals("") && !two.equals("")) {
                    try {
                        ExternalReferralVO it = new ExternalReferralVO(null, Integer.parseInt(one), Integer.parseInt(two));
                        er_arrays.add(it);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (current_comorbidities != null) {
            for (Integer item : current_comorbidities) {
                String other = "";

                PatientProfileArraysVO it = new PatientProfileArraysVO(null, item, other);
                System.out.println("item "+item);
                pp_arrays.add(it);
            }
        }

        if (patient_limitations != null) {
            for (Integer item : patient_limitations) {
                String other = "";
                if (request.getParameter("other" + item) != null) {
                    other = (String) request.getParameter("other" + item);
                }
                PatientProfileArraysVO it = new PatientProfileArraysVO(null, item, other);
                pp_arrays.add(it);
            }
        }
        profileVO.setPatient_profile_arrays(pp_arrays);
        profileVO.setExternalReferrals(er_arrays);

        return profileVO;

    }

    public String[] getExternal_referrals() {
        return external_referrals;
    }

    public void setExternal_referrals(String[] external_referrals) {
        this.external_referrals = external_referrals;
    }

    public String getSurgical_history() {
        return surgical_history;
    }

    public void setSurgical_history(String surgical_history) {
        this.surgical_history = surgical_history;
    }

    private int review_done;

    public int getReview_done() {
        return review_done;
    }

    public void setReview_done(int review_done) {
        this.review_done = review_done;
    }

    /**
     * @return the purs_score
     */
    public int getPurs_score() {
        return purs_score;
    }

    /**
     * @param purs_score the purs_score to set
     */
    public void setPurs_score(int purs_score) {
        this.purs_score = purs_score;
    }

    /**
     * @return the patient_limitations
     */
    public Integer[] getPatient_limitations() {
        return patient_limitations;
    }

    /**
     * @param patient_limitations the patient_limitations to set
     */
    public void setPatient_limitations(Integer[] patient_limitations) {
        this.patient_limitations = patient_limitations;
    }

    /**
     * @return the discharge_planning_initiated
     */
    public Integer getDischarge_planning_initiated() {
        return discharge_planning_initiated;
    }

    /**
     * @param discharge_planning_initiated the discharge_planning_initiated to
     * set
     */
    public void setDischarge_planning_initiated(Integer discharge_planning_initiated) {
        this.discharge_planning_initiated = discharge_planning_initiated;
    }

    /**
     * @return the chronic_disease_initiated
     */
    public Integer getChronic_disease_initiated() {
        return chronic_disease_initiated;
    }

    /**
     * @param chronic_disease_initiated the chronic_disease_initiated to set
     */
    public void setChronic_disease_initiated(Integer chronic_disease_initiated) {
        this.chronic_disease_initiated = chronic_disease_initiated;
    }

    /**
     * @return the barriers_addressed
     */
    public Integer getBarriers_addressed() {
        return barriers_addressed;
    }

    /**
     * @param barriers_addressed the barriers_addressed to set
     */
    public void setBarriers_addressed(Integer barriers_addressed) {
        this.barriers_addressed = barriers_addressed;
    }

    /**
     * @return the barriers_addressed_comments
     */
    public String getBarriers_addressed_comments() {
        return barriers_addressed_comments;
    }

    /**
     * @param barriers_addressed_comments the barriers_addressed_comments to set
     */
    public void setBarriers_addressed_comments(String barriers_addressed_comments) {
        this.barriers_addressed_comments = barriers_addressed_comments;
    }

    /**
     * @return the system_barriers_comments
     */
    public String getSystem_barriers_comments() {
        return system_barriers_comments;
    }

    /**
     * @param system_barriers_comments the system_barriers_comments to set
     */
    public void setSystem_barriers_comments(String system_barriers_comments) {
        this.system_barriers_comments = system_barriers_comments;
    }

    /**
     * @return the quality_of_life_addressed
     */
    public Integer getQuality_of_life_addressed() {
        return quality_of_life_addressed;
    }

    /**
     * @param quality_of_life_addressed the quality_of_life_addressed to set
     */
    public void setQuality_of_life_addressed(Integer quality_of_life_addressed) {
        this.quality_of_life_addressed = quality_of_life_addressed;
    }

    /**
     * @return the quality_of_life_addressed_comments
     */
    public String getQuality_of_life_addressed_comments() {
        return quality_of_life_addressed_comments;
    }

    /**
     * @param quality_of_life_addressed_comments the
     * quality_of_life_addressed_comments to set
     */
    public void setQuality_of_life_addressed_comments(String quality_of_life_addressed_comments) {
        this.quality_of_life_addressed_comments = quality_of_life_addressed_comments;
    }

    /**
     * @return the client_redistribution_system
     */
    public int getClient_redistribution_system() {
        return client_redistribution_system;
    }

    /**
     * @param client_redistribution_system the client_redistribution_system to
     * set
     */
    public void setClient_redistribution_system(int client_redistribution_system) {
        this.client_redistribution_system = client_redistribution_system;
    }

    /**
     * @return the referral_longterm_pressure
     */
    public int getReferral_longterm_pressure() {
        return referral_longterm_pressure;
    }

    /**
     * @param referral_longterm_pressure the referral_longterm_pressure to set
     */
    public void setReferral_longterm_pressure(int referral_longterm_pressure) {
        this.referral_longterm_pressure = referral_longterm_pressure;
    }

    /**
     * @return the pressure_redistribution_initiated
     */
    public Integer getPressure_redistribution_initiated() {
        return pressure_redistribution_initiated;
    }

    /**
     * @param pressure_redistribution_initiated the
     * pressure_redistribution_initiated to set
     */
    public void setPressure_redistribution_initiated(Integer pressure_redistribution_initiated) {
        this.pressure_redistribution_initiated = pressure_redistribution_initiated;
    }

    /**
     * @return the blood_sugar_meal
     */
    public String getBlood_sugar_meal() {
        return blood_sugar_meal;
    }

    /**
     * @param blood_sugar_meal the blood_sugar_meal to set
     */
    public void setBlood_sugar_meal(String blood_sugar_meal) {
        this.blood_sugar_meal = blood_sugar_meal;
    }

    /**
     * @return the blood_sugar
     */
    public double getBlood_sugar() {
        return blood_sugar;
    }

    /**
     * @param blood_sugar the blood_sugar to set
     */
    public void setBlood_sugar(double blood_sugar) {
        this.blood_sugar = blood_sugar;
    }

    /**
     * @return the bloodsugar_month
     */
    public int getBloodsugar_month() {
        return bloodsugar_month;
    }

    /**
     * @param bloodsugar_month the bloodsugar_month to set
     */
    public void setBloodsugar_month(int bloodsugar_month) {
        this.bloodsugar_month = bloodsugar_month;
    }

    /**
     * @return the bloodsugar_year
     */
    public int getBloodsugar_year() {
        return bloodsugar_year;
    }

    /**
     * @param bloodsugar_year the bloodsugar_year to set
     */
    public void setBloodsugar_year(int bloodsugar_year) {
        this.bloodsugar_year = bloodsugar_year;
    }

    /**
     * @return the bloodsugar_day
     */
    public int getBloodsugar_day() {
        return bloodsugar_day;
    }

    /**
     * @param bloodsugar_day the bloodsugar_day to set
     */
    public void setBloodsugar_day(int bloodsugar_day) {
        this.bloodsugar_day = bloodsugar_day;
    }

    /**
     * @return the hga1c
     */
    public int getHga1c() {
        return hga1c;
    }

    /**
     * @param hga1c the hga1c to set
     */
    public void setHga1c(int hga1c) {
        this.hga1c = hga1c;
    }
}
