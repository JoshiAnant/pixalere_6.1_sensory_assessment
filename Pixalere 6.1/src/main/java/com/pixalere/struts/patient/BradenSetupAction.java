package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.BradenVO;

import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.*;
import com.pixalere.auth.bean.ProfessionalVO;

public class BradenSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BradenSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        PatientServiceImpl pamanager = new PatientServiceImpl(language);
        log.error("Session: PP: " + session);
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        log.error("Session Patient_ID:" + session.getAttribute("patient_id"));
        String patient_id = (String) session.getAttribute("patient_id");

        //Generate Token - prevent multiple posts
        saveToken(request);
       
        try {
            request.setAttribute("page", "patient");
            
            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            PatientProfileVO pp = new PatientProfileVO();
            pp.setPatient_id(new Integer(patient_id));
            pp.setCurrent_flag(new Integer(1));
            pp.setActive(new Integer(1));
            
            pp.setDeleted(new Integer(0));
            PatientProfileVO currentProfile = manager.getPatientProfile(pp);
            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na", locale)));

            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            comp.setFlowchart(com.pixalere.utils.Constants.PATIENT_PROFILE);
            java.util.Collection components = gui.getAllComponents(comp);
            java.util.Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);

            comp.setFlowchart(com.pixalere.utils.Constants.BRADEN_SCORE_SECTION);
            java.util.Collection components2 = gui.getAllComponents(comp);
            java.util.Hashtable hashized2 = Common.hashComponents(components2);
            request.setAttribute("componentBraden", hashized2);

           
            if (currentProfile != null) {

                ProfessionalVO userVO2 = currentProfile.getProfessional();
                if (userVO2 != null) {
                    request.setAttribute("profName", userVO2.getFullName());
                }
            }
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");


            //retieve braden assessments.
            BradenVO btmp = new BradenVO();
            btmp.setActive(0);
            btmp.setPatient_id(new Integer(patient_id));
            btmp.setProfessional_id(userVO.getId());

            BradenVO braden = manager.getBraden(btmp);
            /*if (braden == null) {
                btmp.setActive(1);
                btmp.setProfessional_id(null);
                btmp.setDeleted(0);
                braden = manager.getBraden(btmp);
            }*/
            if (braden != null) {
                request.setAttribute("braden", braden);
            }

            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("braden_sensory", listBD.getLists(LookupVO.BRADEN_SENSORY));
            request.setAttribute("braden_moisture", listBD.getLists(LookupVO.BRADEN_MOISTURE));
            request.setAttribute("braden_mobility", listBD.getLists(LookupVO.BRADEN_MOBILITY));
            request.setAttribute("patient_limitations", listBD.getLists(LookupVO.PATIENT_LIMITATIONS));

            request.setAttribute("braden_activity", listBD.getLists(LookupVO.BRADEN_ACTIVITY));
            request.setAttribute("braden_nutrition", listBD.getLists(LookupVO.BRADEN_NUTRITION));
            request.setAttribute("braden_friction", listBD.getLists(LookupVO.BRADEN_FRICTION));

        }  catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (patient_id == null) {
            return (mapping.findForward("uploader.patienthome.success"));
        } else {
            return (mapping.findForward("uploader.braden.success"));
        }
    }
}
