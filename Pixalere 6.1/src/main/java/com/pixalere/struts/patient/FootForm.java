package com.pixalere.struts.patient;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.FootAssessmentVO;
import java.util.Date;

/**
 * @author
 *
 */
public class FootForm extends ActionForm {
	
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FootForm.class);

    HttpSession session = null;
    HttpServletRequest request = null;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        this.request=request;
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
   
    private int left_proprioception;
    private int right_proprioception;
    private int weight_bearing;
    private int balance;
    private int calf_muscle_pump;
    private int mobility_aids;
    private String calf_muscle_comments;
    private String mobility_aids_comments;
    private String gait_pattern_comments;
    private String walking_distance_comments;
    private int indoor_footwear;
    private String foot_comments;
    private int outdoor_footwear;
    private String indoor_footwear_comments;
    private String outdoor_footwear_comments;
    private int orthotics;
    private String orthotics_comments;
    private int muscle_tone_functional;
    private int arches_foot_structure;
    private int supination_foot_structure;
    private int pronation_foot_structure;
    private String[] dorsiflexion_active;
    private String[] dorsiflexion_passive;
    private String[] plantarflexion_active;
    private String[] plantarflexion_passive;
    private String[] greattoe_active;
    private String[] greattoe_passive;
    private String walking_endurance_comments;
  
    
    public FootAssessmentVO getFootData(ProfessionalVO userVO) throws ApplicationException {
    
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        boolean save=false;
        FootAssessmentVO  foot = new FootAssessmentVO();
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        foot.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale));
        foot.setCreated_on(new Date());
        foot.setLastmodified_on(new Date());
        foot.setActive(0);
        
        if(weight_bearing>0){
        save=true;
        }foot.setWeight_bearing(weight_bearing);
        if(balance>0){
        save=true;
        }foot.setBalance(balance);
        if(calf_muscle_pump>0){
        save=true;
        }foot.setCalf_muscle_pump(calf_muscle_pump);
        if(mobility_aids>0){
        save=true;}
        foot.setMobility_aids(mobility_aids);
        if(calf_muscle_comments!=null && !calf_muscle_comments.equals("")){
        save=true;
        }foot.setCalf_muscle_comments(Common.stripHTML(calf_muscle_comments));
        if(mobility_aids_comments!=null && !mobility_aids_comments.equals("")){
        save=true;}foot.setMobility_aids_comments(Common.stripHTML(mobility_aids_comments));
        if(gait_pattern_comments!=null && !gait_pattern_comments.equals("")){
        save=true;}foot.setGait_pattern_comments(Common.stripHTML(gait_pattern_comments));
        if(walking_distance_comments!=null && !walking_distance_comments.equals("")){
        save=true;}foot.setWalking_distance_comments(Common.stripHTML(walking_distance_comments));
        if(walking_endurance_comments!=null && !walking_endurance_comments.equals("")){
        save=true;}foot.setWalking_endurance_comments(Common.stripHTML(walking_endurance_comments));
        if(right_proprioception>0){
        save=true;}foot.setRight_proprioception(right_proprioception);
        if(left_proprioception>0){
        save=true;}foot.setLeft_proprioception(left_proprioception);
        if(indoor_footwear>0){
        save=true;}foot.setIndoor_footwear(indoor_footwear);
        if(outdoor_footwear>0){
        save=true;}foot.setOutdoor_footwear(outdoor_footwear);
        if(indoor_footwear_comments!=null && !indoor_footwear_comments.equals("")){
        save=true;}foot.setIndoor_footwear_comments(Common.stripHTML(indoor_footwear_comments));
        if(outdoor_footwear_comments!=null && !outdoor_footwear_comments.equals("")){
        save=true;}foot.setOutdoor_footwear_comments(Common.stripHTML(outdoor_footwear_comments));
        if(orthotics>0){
        save=true;}foot.setOrthotics(orthotics);
        if(orthotics_comments!=null && !orthotics_comments.equals("")){
        save=true;}foot.setOrthotics_comments(Common.stripHTML(getOrthotics_comments()));
        if(muscle_tone_functional>0){
        save=true;}foot.setMuscle_tone_functional(muscle_tone_functional);
        if(arches_foot_structure>0){
        save=true;}foot.setArches_foot_structure(arches_foot_structure);
        if(supination_foot_structure>0){
        save=true;}foot.setSupination_foot_structure(supination_foot_structure);
        if(pronation_foot_structure>0){
        save=true;}foot.setPronation_foot_structure(pronation_foot_structure);
        if(dorsiflexion_active!=null){
    	save=true;}foot.setDorsiflexion_active(Serialize.serialize(dorsiflexion_active));
        if(dorsiflexion_passive!=null){
    	save=true;}foot.setDorsiflexion_passive(Serialize.serialize(dorsiflexion_passive));
        if(plantarflexion_active!=null){
    	save=true;}foot.setPlantarflexion_active(Serialize.serialize(plantarflexion_active));
        if(plantarflexion_passive!=null){
    	save=true;}foot.setPlantarflexion_passive(Serialize.serialize(plantarflexion_passive));
        if(greattoe_active!=null){
    	save=true;}foot.setGreattoe_active(Serialize.serialize(greattoe_active));
        if(greattoe_passive!=null){
    	save=true;}foot.setGreattoe_passive(Serialize.serialize(greattoe_passive));
        if(foot_comments!=null && !foot_comments.equals("")){save=true;}foot.setFoot_comments(Common.stripHTML(Common.convertCarriageReturns(foot_comments, false)));
        if(save){
        
        return foot;
        
        }else{
        return null;}
    }
    

   

    public int getWeight_bearing() {
        return weight_bearing;
    }

    public void setWeight_bearing(int weight_bearing) {
        this.weight_bearing = weight_bearing;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getCalf_muscle_pump() {
        return calf_muscle_pump;
    }
   public int getRight_proprioception() {
        return right_proprioception;
    }

    public void setRight_proprioception(int right_proprioception) {
        this.right_proprioception = right_proprioception;
    }
    public int getLeft_proprioception() {
        return left_proprioception;
    }

    public void setLeft_proprioception(int left_proprioception) {
        this.left_proprioception = left_proprioception;
    }
    public void setCalf_muscle_pump(int calf_muscle_pump) {
        this.calf_muscle_pump = calf_muscle_pump;
    }

    public int getMobility_aids() {
        return mobility_aids;
    }

    public void setMobility_aids(int mobility_aids) {
        this.mobility_aids = mobility_aids;
    }

    public String getCalf_muscle_comments() {
        return calf_muscle_comments;
    }

    public void setCalf_muscle_comments(String calf_muscle_comments) {
        this.calf_muscle_comments = calf_muscle_comments;
    }

    public String getMobility_aids_comments() {
        return mobility_aids_comments;
    }

    public void setMobility_aids_comments(String mobility_aids_comments) {
        this.mobility_aids_comments = mobility_aids_comments;
    }

    public String getGait_pattern_comments() {
        return gait_pattern_comments;
    }

    public void setGait_pattern_comments(String gait_pattern_comments) {
        this.gait_pattern_comments = gait_pattern_comments;
    }

    public String getWalking_distance_comments() {
        return walking_distance_comments;
    }

    public void setWalking_distance_comments(String walking_distance_comments) {
        this.walking_distance_comments = walking_distance_comments;
    }

    

    public int getIndoor_footwear() {
        return indoor_footwear;
    }

    public void setIndoor_footwear(int indoor_footwear) {
        this.indoor_footwear = indoor_footwear;
    }

    public int getOutdoor_footwear() {
        return outdoor_footwear;
    }

    public void setOutdoor_footwear(int outdoor_footwear) {
        this.outdoor_footwear = outdoor_footwear;
    }

    public int getOrthotics() {
        return orthotics;
    }

    public void setOrthotics(int orthotics) {
        this.orthotics = orthotics;
    }

     public String getFoot_comments() {
        return foot_comments;
    }

    public void setFoot_comments(String foot_comments) {
        this.foot_comments = foot_comments;
    }

    public int getMuscle_tone_functional() {
        return muscle_tone_functional;
    }

    public void setMuscle_tone_functional(int muscle_tone_functional) {
        this.muscle_tone_functional = muscle_tone_functional;
    }

    public int getArches_foot_structure() {
        return arches_foot_structure;
    }

    public void setArches_foot_structure(int arches_foot_structure) {
        this.arches_foot_structure = arches_foot_structure;
    }

    public int getSupination_foot_structure() {
        return supination_foot_structure;
    }

    public void setSupination_foot_structure(int supination_foot_structure) {
        this.supination_foot_structure = supination_foot_structure;
    }

    public int getPronation_foot_structure() {
        return pronation_foot_structure;
    }

    public void setPronation_foot_structure(int pronation_foot_structure) {
        this.pronation_foot_structure = pronation_foot_structure;
    }

    public String[] getDorsiflexion_active() {
        return dorsiflexion_active;
    }

    public void setDorsiflexion_active(String[] dorsiflexion_active) {
        this.dorsiflexion_active = dorsiflexion_active;
    }

    public String[] getDorsiflexion_passive() {
        return dorsiflexion_passive;
    }

    public void setDorsiflexion_passive(String[] dorsiflexion_passive) {
        this.dorsiflexion_passive = dorsiflexion_passive;
    }

    public String[] getPlantarflexion_active() {
        return plantarflexion_active;
    }

    public void setPlantarflexion_active(String[] plantarflexion_active) {
        this.plantarflexion_active = plantarflexion_active;
    }

    public String[] getPlantarflexion_passive() {
        return plantarflexion_passive;
    }

    public void setPlantarflexion_passive(String[] plantarflexion_passive) {
        this.plantarflexion_passive = plantarflexion_passive;
    }

    public String[] getGreattoe_active() {
        return greattoe_active;
    }

    public void setGreattoe_active(String[] greattoe_active) {
        this.greattoe_active = greattoe_active;
    }

    public String[] getGreattoe_passive() {
        return greattoe_passive;
    }

    public void setGreattoe_passive(String[] greattoe_passive) {
        this.greattoe_passive = greattoe_passive;
    }

    public String getWalking_endurance_comments() {
        return walking_endurance_comments;
    }

    public void setWalking_endurance_comments(String walking_endurance_comments) {
        this.walking_endurance_comments = walking_endurance_comments;
    }

    public String getIndoor_footwear_comments() {
        return indoor_footwear_comments;
    }

    public void setIndoor_footwear_comments(String indoor_footwear_comments) {
        this.indoor_footwear_comments = indoor_footwear_comments;
    }

    public String getOutdoor_footwear_comments() {
        return outdoor_footwear_comments;
    }

    public void setOutdoor_footwear_comments(String outdoor_footwear_comments) {
        this.outdoor_footwear_comments = outdoor_footwear_comments;
    }

    public String getOrthotics_comments() {
        return orthotics_comments;
    }

    public void setOrthotics_comments(String orthotics_comments) {
        this.orthotics_comments = orthotics_comments;
    }

}