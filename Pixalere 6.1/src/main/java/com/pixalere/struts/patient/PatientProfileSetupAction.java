package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import java.util.List;
import java.util.ArrayList;
import com.pixalere.guibeans.DropdownBean;
import com.pixalere.patient.bean.ExternalReferralVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.dao.ListsDAO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.*;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Iterator;
import java.util.Collection;

public class PatientProfileSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientProfileSetupAction.class);
 
    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        PatientServiceImpl pamanager = new PatientServiceImpl(language);
        log.error("Session: PP: "+session);
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        log.error("SEssion Patient_ID:"+session.getAttribute("patient_id"));
        String patient_id = (String) session.getAttribute("patient_id");
        
        //Generate Token - prevent multiple posts
        saveToken(request);
        try {
            request.setAttribute("page", "patient");
            
            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            ListServiceImpl listBD = new ListServiceImpl(language);
            PatientProfileVO pp = new PatientProfileVO();
            pp.setPatient_id(new Integer(patient_id));
            pp.setCurrent_flag(new Integer(1));
            pp.setActive(new Integer(1));
            pp.setDeleted(new Integer(0));
            PatientProfileVO currentProfile = manager.getPatientProfile(pp);
            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na",locale)));

            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
			comp.setFlowchart(com.pixalere.utils.Constants.PATIENT_PROFILE);
            java.util.Collection components = gui.getAllComponents(comp);
            java.util.Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);
            
            
            if (currentProfile != null) {

                ProfessionalVO userVO2 = currentProfile.getProfessional();
                if (userVO2 != null) {
                    request.setAttribute("profName", userVO2.getFullName());
                }
            }
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");


            
            PatientProfileVO tmpProfile = manager.getTemporaryPatientProfile(Integer.parseInt(patient_id), userVO.getId());
            if ((request.getAttribute("error")) == null) {
                if (tmpProfile == null) {
                    if (currentProfile == null) {
                        currentProfile = new PatientProfileVO();
                    }

                    currentProfile.setReview_done(null);
                    request.setAttribute("patient", currentProfile);
                    if (currentProfile.getPatient_profile_arrays() != null) {
                        request.setAttribute("unserializeComo", Common.filterArrayObjects2Lookup( currentProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[currentProfile.getPatient_profile_arrays().size()]), LookupVO.COMORBIDITIES));
                        request.setAttribute("unserializePatientLimitations", Common.filterArrayObjects(currentProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[currentProfile.getPatient_profile_arrays().size()]), LookupVO.PATIENT_LIMITATIONS));
                        //FIXME: this is really "interfering factors"
                        request.setAttribute("unserializeInter", Common.filterArrayObjects(currentProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[currentProfile.getPatient_profile_arrays().size()]), LookupVO.INTERFERING_MEDS));
                        //request.setAttribute("unserializeInvestigations", Serialize.unserializeStraight(currentProfile.getInvestigation()));
                        List<DropdownBean> refs = new ArrayList();
                        for(ExternalReferralVO ref : currentProfile.getExternalReferrals()){
                            LookupVO ext = listBD.getListItem(ref.getExternal_referral_id());
                            LookupVO arr = listBD.getListItem(ref.getArranged_id());
                            refs.add(new DropdownBean(ref.getExternal_referral_id()+" : "+ref.getArranged_id(),ext.getName(language)+" : "+arr.getName(language)));
                        }
                        request.setAttribute("unserializedReferrals",refs);
                        //FIXME: this is really "interfering meds"
                        request.setAttribute("unserializeComments", Common.filterArrayObjects(currentProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[currentProfile.getPatient_profile_arrays().size()]), LookupVO.MED_COMMENTS));
                    }
                } else {
                    request.setAttribute("patient", tmpProfile);
                    //request.setAttribute("unserializeInvestigations", Serialize.unserializeStraight(tmpProfile.getInvestigation()));
                    if (tmpProfile.getPatient_profile_arrays() != null) {
                        request.setAttribute("unserializeComo", Common.filterArrayObjects2Lookup(tmpProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[tmpProfile.getPatient_profile_arrays().size()]), LookupVO.COMORBIDITIES));
                        request.setAttribute("unserializePatientLimitations", Common.filterArrayObjects(tmpProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[tmpProfile.getPatient_profile_arrays().size()]), LookupVO.PATIENT_LIMITATIONS));
                        //FIXME: this is really "interfering factors"
                        request.setAttribute("unserializeInter", Common.filterArrayObjects(tmpProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[tmpProfile.getPatient_profile_arrays().size()]), LookupVO.INTERFERING_MEDS));
                        //request.setAttribute("unserializeInvestigations", Serialize.unserializeStraight(currentProfile.getInvestigation()));
                        List<DropdownBean> refs = new ArrayList();
                       for(ExternalReferralVO ref : tmpProfile.getExternalReferrals()){
                          LookupVO ext = listBD.getListItem(ref.getExternal_referral_id());
                            LookupVO arr = listBD.getListItem(ref.getArranged_id());
                            refs.add(new DropdownBean(ref.getExternal_referral_id()+" : "+ref.getArranged_id(),ext.getName(language)+" : "+arr.getName(language)));
                        }
                        request.setAttribute("unserializedReferrals",refs);
                        //FIXME: this is really "interfering meds"
                        request.setAttribute("unserializeComments", Common.filterArrayObjects(tmpProfile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[tmpProfile.getPatient_profile_arrays().size()]), LookupVO.MED_COMMENTS));
                    }
                }
            }
            PatientAccountVO patientAcc = pamanager.getPatient(Integer.parseInt(patient_id));

            if(patientAcc!=null && patientAcc.getFunding_source()!=null){
                LookupVO fs = listBD.getListItem(patientAcc.getFunding_source());
                if(fs!=null){
                    request.setAttribute("funding_source",fs.getName(language));
                }
            }
            String phn_ = patientAcc.getPhn();
            if (!patientAcc.getOutofprovince().equals(new Integer(1))) {
                request.setAttribute("phn", phn_);
            } else {
                request.setAttribute("phn", phn_);
            }

            request.setAttribute("treatment_location", patientAcc.getTreatmentLocation().getName(language));
            /*patientAcc.setFirstName();
            patientAcc.setLastName();
            patientAcc.setTreatment_location_id();*/
            LookupVO lookupVO = new LookupVO();
            lookupVO.setResourceId(new Integer(LookupVO.FUNDING_SOURCE));
            ListsDAO listDAO = new ListsDAO();

            
            request.setAttribute("patientResidence", listDAO.findAllByCriteria(lookupVO));
            lookupVO.setResourceId(new Integer(LookupVO.REFERRAL_SOURCE));
            request.setAttribute("referralSource", listDAO.findAllByCriteria(lookupVO));
            lookupVO.setResourceId(new Integer(LookupVO.INTERFERING_MEDS));
            request.setAttribute("im_list", listDAO.findAllByCriteria(lookupVO));

            lookupVO.setResourceId(LookupVO.MED_COMMENTS);
            request.setAttribute("med_comments_list", listDAO.findAllByCriteria(lookupVO));
            request.setAttribute("como_list", listBD.getLists(LookupVO.COMORBIDITIES, null, true));
            request.setAttribute("patient_limitations", listBD.getLists(LookupVO.PATIENT_LIMITATIONS));
            request.setAttribute("external_referral", listBD.getLists(LookupVO.EXTERNAL_REFERRAL));
            request.setAttribute("arranged", listBD.getLists(LookupVO.ARRANGED));
            // SETUP NEW STATIC DATA (GENDER)
            Collection<LookupVO> co = null;

            lookupVO.setResourceId(new Integer(LookupVO.GENDER));
            if(patientAcc.getGender()!=null){
                lookupVO.setId(patientAcc.getGender());
                co = listDAO.findAllByCriteria(lookupVO);
                for (Iterator itera = co.iterator(); itera.hasNext();) {
                    LookupVO colookupVO = (LookupVO) itera.next();
                    request.setAttribute("gender", colookupVO.getName(language));
                }
            }
            // SETUP NEW STATIC DATA (PATIENT RESIDENCE)

            lookupVO.setResourceId(new Integer(LookupVO.PATIENT_RESIDENCE));
            lookupVO.setId(patientAcc.getPatient_residence());
            co = listDAO.findAllByCriteria(lookupVO);
            for (Iterator itera = co.iterator(); itera.hasNext();) {
                LookupVO colookupVO = (LookupVO) itera.next();
                request.setAttribute("patient_residence", colookupVO.getName(language));
            }

            // SETUP NEW STATIC DATA (DOB)
            try {



                PDate pd = new PDate();
                //request.setAttribute("age", pd.getCalcAge(patientAcc.getDate().getTime()) );
            } catch (NumberFormatException e) {

                request.setAttribute("age", "");
            }



            request.setAttribute("phn2", patientAcc.getPhn2());
            request.setAttribute("phn3", patientAcc.getPhn3());
        } catch (com.pixalere.common.DataAccessException e) {
            log.error("An application exception has been raised in LogAuditAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (patient_id == null) {
            return (mapping.findForward("uploader.patienthome.success"));
        } else {
            return (mapping.findForward("uploader.patient.success"));
        }
    }
}