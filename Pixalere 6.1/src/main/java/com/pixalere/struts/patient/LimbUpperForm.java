package com.pixalere.struts.patient;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.LimbUpperAssessmentVO;
import java.util.Date;

/**
 * @author
 *
 */
public class LimbUpperForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LimbUpperForm.class);

    HttpSession session = null;
    HttpServletRequest request = null;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        this.request=request;
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    private String limb_comments_upper;
    private String[] left_missing_limbs_upper;
    private String[] right_missing_limbs_upper;
    private String[] left_pain_assessment_upper;
    private String[] right_pain_assessment_upper;
    private String[] left_skin_assessment_upper;
    private String[] right_skin_assessment_upper;
    private int left_dorsalis_pedis_palpation_upper;
    private int right_dorsalis_pedis_palpation_upper;
    private int left_posterior_tibial_palpation_upper;
    private int right_posterior_tibial_palpation_upper;
    private int left_edema_severity_upper;
    private int right_edema_severity_upper;
    private int left_edema_location_upper;
    private int right_edema_location_upper;
    
    private String sleep_positions_upper;
    
    private String[] left_sensory_upper;
    private String[] right_sensory_upper;
    
    private int left_capillary_less_upper;
    private int right_capillary_less_upper;
    private int left_capillary_more_upper;
    private int right_capillary_more_upper;
    private Integer left_range_motion_arm;
    private Integer right_range_motion_arm;
    private Integer left_range_motion_hand;
    private Integer right_range_motion_hand;
    private Integer left_range_motion_fingers;
    private Integer right_range_motion_fingers;
    private String left_wrist_cm;
    private String left_wrist_mm;
    private String right_wrist_cm;
    private String right_wrist_mm;
    private String left_elbow_cm;
    private String left_elbow_mm;
    private String right_elbow_cm;
    private String right_elbow_mm;
    private int left_temperature_arm;
    private int right_temperature_arm;
    private int left_temperature_hand;
    private int right_temperature_hand;
    private int left_temperature_fingers;
    private int right_temperature_fingers;
    private int left_skin_colour_arm;
    private int right_skin_colour_arm;
    private int left_skin_colour_hand;
    private int right_skin_colour_hand;
    private int left_skin_colour_fingers;
    private int right_skin_colour_fingers;

    public LimbUpperAssessmentVO getLimbUpperData(ProfessionalVO userVO) throws ApplicationException {
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        boolean save=false;
        LimbUpperAssessmentVO limb = new LimbUpperAssessmentVO();
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        limb.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale));
        limb.setCreated_on(new Date());
        limb.setLastmodified_on(new Date());
        limb.setActive(0);

        if(left_missing_limbs_upper!=null){save=true;}limb.setLeft_missing_limbs(Serialize.serialize(left_missing_limbs_upper));
        if(right_missing_limbs_upper!=null){save=true;}limb.setRight_missing_limbs(Serialize.serialize(right_missing_limbs_upper));
        if(left_pain_assessment_upper!=null){save=true;}limb.setLeft_pain_assessment(Serialize.serialize(left_pain_assessment_upper));
        if(right_pain_assessment_upper!=null){save=true;}limb.setRight_pain_assessment(Serialize.serialize(right_pain_assessment_upper));
        //limb.setPain_comments(pain_comments);
        if(getLeft_range_motion_arm()!=null){save=true;}limb.setLeft_range_motion_arm(getLeft_range_motion_arm());
        if(getLeft_range_motion_hand()!=null){save=true;}limb.setLeft_range_motion_hand(getLeft_range_motion_hand());
        if(getLeft_range_motion_fingers()!=null){save=true;}limb.setLeft_range_motion_fingers(getLeft_range_motion_fingers());
        if(getRight_range_motion_arm()!=null){save=true;} limb.setRight_range_motion_arm(getRight_range_motion_arm());
        if(getRight_range_motion_hand()!=null){save=true;}limb.setRight_range_motion_hand(getRight_range_motion_hand());
        if(getRight_range_motion_fingers()!=null){save=true;}limb.setRight_range_motion_fingers(getRight_range_motion_fingers());

        if(left_skin_assessment_upper!=null){save=true;}limb.setLeft_skin_assessment(Serialize.serialize(left_skin_assessment_upper));
        if(right_skin_assessment_upper!=null){save=true;}limb.setRight_skin_assessment(Serialize.serialize(right_skin_assessment_upper));

        if(left_temperature_arm>0){save=true;}limb.setLeft_temperature_arm(left_temperature_arm);
        if(right_temperature_arm>0){save=true;}limb.setRight_temperature_arm(right_temperature_arm);
        if(left_temperature_hand>0){save=true;}limb.setLeft_temperature_hand(left_temperature_hand);
        if(right_temperature_hand>0){save=true;}limb.setRight_temperature_hand(right_temperature_hand);
        if(left_temperature_fingers>0){save=true;}limb.setLeft_temperature_fingers(left_temperature_fingers);
        if(right_temperature_fingers>0){save=true;}limb.setRight_temperature_fingers(right_temperature_fingers);

        if(left_skin_colour_arm>0){save=true;}limb.setLeft_skin_colour_arm(left_skin_colour_arm);
        if(right_skin_colour_arm>0){save=true;}limb.setRight_skin_colour_arm(right_skin_colour_arm);
        if(left_skin_colour_hand>0){save=true;}limb.setLeft_skin_colour_hand(left_skin_colour_hand);
        if(right_skin_colour_hand>0){save=true;}limb.setRight_skin_colour_hand(right_skin_colour_hand);
        if(left_skin_colour_fingers>0){save=true;}limb.setLeft_skin_colour_fingers(left_skin_colour_fingers);
        if(right_skin_colour_fingers>0){save=true;}limb.setRight_skin_colour_fingers(right_skin_colour_fingers);


        if(left_dorsalis_pedis_palpation_upper>0){save=true;}limb.setLeft_dorsalis_pedis_palpation(left_dorsalis_pedis_palpation_upper);
        if(right_dorsalis_pedis_palpation_upper>0){save=true;}limb.setRight_dorsalis_pedis_palpation(right_dorsalis_pedis_palpation_upper);
        if(left_posterior_tibial_palpation_upper>0){save=true;}limb.setLeft_posterior_tibial_palpation(left_posterior_tibial_palpation_upper);
        if(right_posterior_tibial_palpation_upper>0){save=true;}limb.setRight_posterior_tibial_palpation(right_posterior_tibial_palpation_upper);
        if(left_capillary_less_upper>0){save=true;limb.setLeft_more_capillary(1);}limb.setLeft_less_capillary(left_capillary_less_upper);
        if(right_capillary_less_upper>0){save=true;limb.setRight_more_capillary(1);}limb.setRight_less_capillary(right_capillary_less_upper);
        if(left_capillary_more_upper>0){save=true;}limb.setLeft_more_capillary(left_capillary_more_upper);
        if(right_capillary_more_upper>0){save=true;}limb.setRight_more_capillary(right_capillary_more_upper);
        if(left_edema_severity_upper>0){save=true;}limb.setLeft_edema_severity(left_edema_severity_upper);
        if(right_edema_severity_upper>0){save=true;}limb.setRight_edema_severity(right_edema_severity_upper);

        if(left_edema_location_upper>0){save=true;}limb.setLeft_edema_location(left_edema_location_upper);
        if(right_edema_location_upper>0){save=true;}limb.setRight_edema_location(right_edema_location_upper);

        if(sleep_positions_upper!=null && !sleep_positions_upper.equals("")){save=true;}limb.setSleep_positions(Common.stripHTML(sleep_positions_upper));
     if(!left_wrist_cm.equals("")){save=true;}limb.setLeft_wrist_cm(left_wrist_cm == null || left_wrist_cm.equals("") ? null : new Integer(left_wrist_cm));
        //System.out.println("Measurement: "+left_wrist_cm);
       if(!left_wrist_mm.equals("")){save=true;} limb.setLeft_wrist_mm(left_wrist_mm == null || left_wrist_mm.equals("") ? null : new Integer(left_wrist_mm));
        if(!right_wrist_cm.equals("")){save=true;}limb.setRight_wrist_cm(right_wrist_cm == null || right_wrist_cm.equals("") ? null : new Integer(right_wrist_cm));
        if(!right_wrist_mm.equals("")){save=true;}limb.setRight_wrist_mm(right_wrist_mm == null || right_wrist_mm.equals("") ? null : new Integer(right_wrist_mm));
        if(!left_elbow_cm.equals("")){save=true;}limb.setLeft_elbow_cm(left_elbow_cm == null || left_elbow_cm.equals("") ? null : new Integer(left_elbow_cm));
        if(!left_elbow_mm.equals("")){save=true;}limb.setLeft_elbow_mm(left_elbow_mm == null || left_elbow_mm.equals("") ? null : new Integer(left_elbow_mm));
        if(!right_elbow_cm.equals("")){save=true;}limb.setRight_elbow_cm(right_elbow_cm == null || right_elbow_cm.equals("") ? null : new Integer(right_elbow_cm));
        if(!right_elbow_mm.equals("")){save=true;}limb.setRight_elbow_mm(right_elbow_mm == null || right_elbow_mm.equals("") ? null : new Integer(right_elbow_mm));

        if(left_sensory_upper!=null){save=true;}limb.setLeft_sensory(Serialize.serialize(left_sensory_upper));
        if(right_sensory_upper!=null){save=true;}limb.setRight_sensory(Serialize.serialize(right_sensory_upper));
        if(!limb_comments_upper.equals("")){save=true;}limb.setLimb_comments(Common.stripHTML(Common.convertCarriageReturns(getLimb_comments_upper(), false)));
        if(save){return limb;}else{return null;}
    }
    /**
     * @return the limb_comments_upper
     */
    public String getLimb_comments_upper() {
        return limb_comments_upper;
    }

    /**
     * @param limb_comments_upper the limb_comments_upper to set
     */
    public void setLimb_comments_upper(String limb_comments_upper) {
        this.limb_comments_upper = limb_comments_upper;
    }
    

    /**
     * @return the left_missing_limbs_upper
     */
    public String[] getLeft_missing_limbs_upper() {
        return left_missing_limbs_upper;
    }

    /**
     * @param left_missing_limbs_upper the left_missing_limbs_upper to set
     */
    public void setLeft_missing_limbs_upper(String[] left_missing_limbs_upper) {
        this.left_missing_limbs_upper = left_missing_limbs_upper;
    }

    /**
     * @return the right_missing_limbs_upper
     */
    public String[] getRight_missing_limbs_upper() {
        return right_missing_limbs_upper;
    }

    /**
     * @param right_missing_limbs_upper the right_missing_limbs_upper to set
     */
    public void setRight_missing_limbs_upper(String[] right_missing_limbs_upper) {
        this.right_missing_limbs_upper = right_missing_limbs_upper;
    }

    /**
     * @return the left_pain_assessment_upper
     */
    public String[] getLeft_pain_assessment_upper() {
        return left_pain_assessment_upper;
    }

    /**
     * @param left_pain_assessment_upper the left_pain_assessment_upper to set
     */
    public void setLeft_pain_assessment_upper(String[] left_pain_assessment_upper) {
        this.left_pain_assessment_upper = left_pain_assessment_upper;
    }

    /**
     * @return the right_pain_assessment_upper
     */
    public String[] getRight_pain_assessment_upper() {
        return right_pain_assessment_upper;
    }

    /**
     * @param right_pain_assessment_upper the right_pain_assessment_upper to set
     */
    public void setRight_pain_assessment_upper(String[] right_pain_assessment_upper) {
        this.right_pain_assessment_upper = right_pain_assessment_upper;
    }

    /**
     * @return the left_skin_assessment_upper
     */
    public String[] getLeft_skin_assessment_upper() {
        return left_skin_assessment_upper;
    }

    /**
     * @param left_skin_assessment_upper the left_skin_assessment_upper to set
     */
    public void setLeft_skin_assessment_upper(String[] left_skin_assessment_upper) {
        this.left_skin_assessment_upper = left_skin_assessment_upper;
    }

    /**
     * @return the right_skin_assessment_upper
     */
    public String[] getRight_skin_assessment_upper() {
        return right_skin_assessment_upper;
    }

    /**
     * @param right_skin_assessment_upper the right_skin_assessment_upper to set
     */
    public void setRight_skin_assessment_upper(String[] right_skin_assessment_upper) {
        this.right_skin_assessment_upper = right_skin_assessment_upper;
    }



    /**
     * @return the left_dorsalis_pedis_palpation_upper
     */
    public int getLeft_dorsalis_pedis_palpation_upper() {
        return left_dorsalis_pedis_palpation_upper;
    }

    /**
     * @param left_dorsalis_pedis_palpation_upper the left_dorsalis_pedis_palpation_upper to set
     */
    public void setLeft_dorsalis_pedis_palpation_upper(int left_dorsalis_pedis_palpation_upper) {
        this.left_dorsalis_pedis_palpation_upper = left_dorsalis_pedis_palpation_upper;
    }

    /**
     * @return the right_dorsalis_pedis_palpation_upper
     */
    public int getRight_dorsalis_pedis_palpation_upper() {
        return right_dorsalis_pedis_palpation_upper;
    }

    /**
     * @param right_dorsalis_pedis_palpation_upper the right_dorsalis_pedis_palpation_upper to set
     */
    public void setRight_dorsalis_pedis_palpation_upper(int right_dorsalis_pedis_palpation_upper) {
        this.right_dorsalis_pedis_palpation_upper = right_dorsalis_pedis_palpation_upper;
    }

    /**
     * @return the left_posterior_tibial_palpation_upper
     */
    public int getLeft_posterior_tibial_palpation_upper() {
        return left_posterior_tibial_palpation_upper;
    }

    /**
     * @param left_posterior_tibial_palpation_upper the left_posterior_tibial_palpation_upper to set
     */
    public void setLeft_posterior_tibial_palpation_upper(int left_posterior_tibial_palpation_upper) {
        this.left_posterior_tibial_palpation_upper = left_posterior_tibial_palpation_upper;
    }

    /**
     * @return the right_posterior_tibial_palpation_upper
     */
    public int getRight_posterior_tibial_palpation_upper() {
        return right_posterior_tibial_palpation_upper;
    }

    /**
     * @param right_posterior_tibial_palpation_upper the right_posterior_tibial_palpation_upper to set
     */
    public void setRight_posterior_tibial_palpation_upper(int right_posterior_tibial_palpation_upper) {
        this.right_posterior_tibial_palpation_upper = right_posterior_tibial_palpation_upper;
    }

    /**
     * @return the left_edema_severity_upper
     */
    public int getLeft_edema_severity_upper() {
        return left_edema_severity_upper;
    }

    /**
     * @param left_edema_severity_upper the left_edema_severity_upper to set
     */
    public void setLeft_edema_severity_upper(int left_edema_severity_upper) {
        this.left_edema_severity_upper = left_edema_severity_upper;
    }

    /**
     * @return the right_edema_severity_upper
     */
    public int getRight_edema_severity_upper() {
        return right_edema_severity_upper;
    }

    /**
     * @param right_edema_severity_upper the right_edema_severity_upper to set
     */
    public void setRight_edema_severity_upper(int right_edema_severity_upper) {
        this.right_edema_severity_upper = right_edema_severity_upper;
    }

    /**
     * @return the left_edema_location_upper
     */
    public int getLeft_edema_location_upper() {
        return left_edema_location_upper;
    }

    /**
     * @param left_edema_location_upper the left_edema_location_upper to set
     */
    public void setLeft_edema_location_upper(int left_edema_location_upper) {
        this.left_edema_location_upper = left_edema_location_upper;
    }

    /**
     * @return the right_edema_location_upper
     */
    public int getRight_edema_location_upper() {
        return right_edema_location_upper;
    }

    /**
     * @param right_edema_location_upper the right_edema_location_upper to set
     */
    public void setRight_edema_location_upper(int right_edema_location_upper) {
        this.right_edema_location_upper = right_edema_location_upper;
    }


    /**
     * @return the left_sensory_upper
     */
    public String[] getLeft_sensory_upper() {
        return left_sensory_upper;
    }

    /**
     * @param left_sensory_upper the left_sensory_upper to set
     */
    public void setLeft_sensory_upper(String[] left_sensory_upper) {
        this.left_sensory_upper = left_sensory_upper;
    }

    /**
     * @return the right_sensory_upper
     */
    public String[] getRight_sensory_upper() {
        return right_sensory_upper;
    }

    /**
     * @param right_sensory_upper the right_sensory_upper to set
     */
    public void setRight_sensory_upper(String[] right_sensory_upper) {
        this.right_sensory_upper = right_sensory_upper;
    }

    /**
     * @return the sleep_positions_upper
     */
    public String getSleep_positions_upper() {
        return sleep_positions_upper;
    }

    /**
     * @param sleep_positions_upper the sleep_positions_upper to set
     */
    public void setSleep_positions_upper(String sleep_positions_upper) {
        this.sleep_positions_upper = sleep_positions_upper;
    }

   
    /**
     * @return the left_capillary_less_upper
     */
    public int getLeft_capillary_less_upper() {
        return left_capillary_less_upper;
    }

    /**
     * @return the right_capillary_less_upper
     */
    public int getRight_capillary_less_upper() {
        return right_capillary_less_upper;
    }

    /**
     * @return the left_capillary_more_upper
     */
    public int getLeft_capillary_more_upper() {
        return left_capillary_more_upper;
    }

    /**
     * @return the right_capillary_more_upper
     */
    public int getRight_capillary_more_upper() {
        return right_capillary_more_upper;
    }

    
    /**
     * @return the left_wrist_cm
     */
    public String getLeft_wrist_cm() {
        return left_wrist_cm;
    }

    /**
     * @param left_wrist_cm the left_wrist_cm to set
     */
    public void setLeft_wrist_cm(String left_wrist_cm) {
        this.left_wrist_cm = left_wrist_cm;
    }

    /**
     * @return the left_wrist_mm
     */
    public String getLeft_wrist_mm() {
        return left_wrist_mm;
    }

    /**
     * @param left_wrist_mm the left_wrist_mm to set
     */
    public void setLeft_wrist_mm(String left_wrist_mm) {
        this.left_wrist_mm = left_wrist_mm;
    }

    /**
     * @return the right_wrist_cm
     */
    public String getRight_wrist_cm() {
        return right_wrist_cm;
    }

    /**
     * @param right_wrist_cm the right_wrist_cm to set
     */
    public void setRight_wrist_cm(String right_wrist_cm) {
        this.right_wrist_cm = right_wrist_cm;
    }

    /**
     * @return the right_wrist_mm
     */
    public String getRight_wrist_mm() {
        return right_wrist_mm;
    }

    /**
     * @param right_wrist_mm the right_wrist_mm to set
     */
    public void setRight_wrist_mm(String right_wrist_mm) {
        this.right_wrist_mm = right_wrist_mm;
    }

    /**
     * @return the left_elbow_cm
     */
    public String getLeft_elbow_cm() {
        return left_elbow_cm;
    }

    /**
     * @param left_elbow_cm the left_elbow_cm to set
     */
    public void setLeft_elbow_cm(String left_elbow_cm) {
        this.left_elbow_cm = left_elbow_cm;
    }

    /**
     * @return the left_elbow_mm
     */
    public String getLeft_elbow_mm() {
        return left_elbow_mm;
    }

    /**
     * @param left_elbow_mm the left_elbow_mm to set
     */
    public void setLeft_elbow_mm(String left_elbow_mm) {
        this.left_elbow_mm = left_elbow_mm;
    }

    /**
     * @return the right_elbow_cm
     */
    public String getRight_elbow_cm() {
        return right_elbow_cm;
    }

    /**
     * @param right_elbow_cm the right_elbow_cm to set
     */
    public void setRight_elbow_cm(String right_elbow_cm) {
        this.right_elbow_cm = right_elbow_cm;
    }

    /**
     * @return the right_elbow_mm
     */
    public String getRight_elbow_mm() {
        return right_elbow_mm;
    }

    /**
     * @param right_elbow_mm the right_elbow_mm to set
     */
    public void setRight_elbow_mm(String right_elbow_mm) {
        this.right_elbow_mm = right_elbow_mm;
    }

    /**
     * @return the left_temperature_arm
     */
    public int getLeft_temperature_arm() {
        return left_temperature_arm;
    }

    /**
     * @param left_temperature_arm the left_temperature_arm to set
     */
    public void setLeft_temperature_arm(int left_temperature_arm) {
        this.left_temperature_arm = left_temperature_arm;
    }

    /**
     * @return the right_temperature_arm
     */
    public int getRight_temperature_arm() {
        return right_temperature_arm;
    }

    /**
     * @param right_temperature_arm the right_temperature_arm to set
     */
    public void setRight_temperature_arm(int right_temperature_arm) {
        this.right_temperature_arm = right_temperature_arm;
    }

    /**
     * @return the left_temperature_hand
     */
    public int getLeft_temperature_hand() {
        return left_temperature_hand;
    }

    /**
     * @param left_temperature_hand the left_temperature_hand to set
     */
    public void setLeft_temperature_hand(int left_temperature_hand) {
        this.left_temperature_hand = left_temperature_hand;
    }

    /**
     * @return the right_temperature_hand
     */
    public int getRight_temperature_hand() {
        return right_temperature_hand;
    }

    /**
     * @param right_temperature_hand the right_temperature_hand to set
     */
    public void setRight_temperature_hand(int right_temperature_hand) {
        this.right_temperature_hand = right_temperature_hand;
    }

    /**
     * @return the left_temperature_fingers
     */
    public int getLeft_temperature_fingers() {
        return left_temperature_fingers;
    }

    /**
     * @param left_temperature_fingers the left_temperature_fingers to set
     */
    public void setLeft_temperature_fingers(int left_temperature_fingers) {
        this.left_temperature_fingers = left_temperature_fingers;
    }

    /**
     * @return the right_temperature_fingers
     */
    public int getRight_temperature_fingers() {
        return right_temperature_fingers;
    }

    /**
     * @param right_temperature_fingers the right_temperature_fingers to set
     */
    public void setRight_temperature_fingers(int right_temperature_fingers) {
        this.right_temperature_fingers = right_temperature_fingers;
    }

    /**
     * @return the left_skin_colour_arm
     */
    public int getLeft_skin_colour_arm() {
        return left_skin_colour_arm;
    }

    /**
     * @param left_skin_colour_arm the left_skin_colour_arm to set
     */
    public void setLeft_skin_colour_arm(int left_skin_colour_arm) {
        this.left_skin_colour_arm = left_skin_colour_arm;
    }

    /**
     * @return the right_skin_colour_arm
     */
    public int getRight_skin_colour_arm() {
        return right_skin_colour_arm;
    }

    /**
     * @param right_skin_colour_arm the right_skin_colour_arm to set
     */
    public void setRight_skin_colour_arm(int right_skin_colour_arm) {
        this.right_skin_colour_arm = right_skin_colour_arm;
    }

    /**
     * @return the left_skin_colour_hand
     */
    public int getLeft_skin_colour_hand() {
        return left_skin_colour_hand;
    }

    /**
     * @param left_skin_colour_hand the left_skin_colour_hand to set
     */
    public void setLeft_skin_colour_hand(int left_skin_colour_hand) {
        this.left_skin_colour_hand = left_skin_colour_hand;
    }

    /**
     * @return the right_skin_colour_hand
     */
    public int getRight_skin_colour_hand() {
        return right_skin_colour_hand;
    }

    /**
     * @param right_skin_colour_hand the right_skin_colour_hand to set
     */
    public void setRight_skin_colour_hand(int right_skin_colour_hand) {
        this.right_skin_colour_hand = right_skin_colour_hand;
    }

    /**
     * @return the left_skin_colour_fingers
     */
    public int getLeft_skin_colour_fingers() {
        return left_skin_colour_fingers;
    }

    /**
     * @param left_skin_colour_fingers the left_skin_colour_fingers to set
     */
    public void setLeft_skin_colour_fingers(int left_skin_colour_fingers) {
        this.left_skin_colour_fingers = left_skin_colour_fingers;
    }

    /**
     * @return the right_skin_colour_fingers
     */
    public int getRight_skin_colour_fingers() {
        return right_skin_colour_fingers;
    }

    /**
     * @param right_skin_colour_fingers the right_skin_colour_fingers to set
     */
    public void setRight_skin_colour_fingers(int right_skin_colour_fingers) {
        this.right_skin_colour_fingers = right_skin_colour_fingers;
    }

    /**
     * @return the left_range_motion_arm
     */
    public Integer getLeft_range_motion_arm() {
        return left_range_motion_arm;
    }

    /**
     * @param left_range_motion_arm the left_range_motion_arm to set
     */
    public void setLeft_range_motion_arm(Integer left_range_motion_arm) {
        this.left_range_motion_arm = left_range_motion_arm;
    }

    /**
     * @return the right_range_motion_arm
     */
    public Integer getRight_range_motion_arm() {
        return right_range_motion_arm;
    }

    /**
     * @param right_range_motion_arm the right_range_motion_arm to set
     */
    public void setRight_range_motion_arm(Integer right_range_motion_arm) {
        this.right_range_motion_arm = right_range_motion_arm;
    }

    /**
     * @return the left_range_motion_hand
     */
    public Integer getLeft_range_motion_hand() {
        return left_range_motion_hand;
    }

    /**
     * @param left_range_motion_hand the left_range_motion_hand to set
     */
    public void setLeft_range_motion_hand(Integer left_range_motion_hand) {
        this.left_range_motion_hand = left_range_motion_hand;
    }

    /**
     * @return the right_range_motion_hand
     */
    public Integer getRight_range_motion_hand() {
        return right_range_motion_hand;
    }

    /**
     * @param right_range_motion_hand the right_range_motion_hand to set
     */
    public void setRight_range_motion_hand(Integer right_range_motion_hand) {
        this.right_range_motion_hand = right_range_motion_hand;
    }

    /**
     * @return the left_range_motion_fingers
     */
    public Integer getLeft_range_motion_fingers() {
        return left_range_motion_fingers;
    }

    /**
     * @param left_range_motion_fingers the left_range_motion_fingers to set
     */
    public void setLeft_range_motion_fingers(Integer left_range_motion_fingers) {
        this.left_range_motion_fingers = left_range_motion_fingers;
    }

    /**
     * @return the right_range_motion_fingers
     */
    public Integer getRight_range_motion_fingers() {
        return right_range_motion_fingers;
    }

    /**
     * @param right_range_motion_fingers the right_range_motion_fingers to set
     */
    public void setRight_range_motion_fingers(Integer right_range_motion_fingers) {
        this.right_range_motion_fingers = right_range_motion_fingers;
    }

    /**
     * @param left_capillary_less_upper the left_capillary_less_upper to set
     */
    public void setLeft_capillary_less_upper(int left_capillary_less_upper) {
        this.left_capillary_less_upper = left_capillary_less_upper;
    }

    /**
     * @param right_capillary_less_upper the right_capillary_less_upper to set
     */
    public void setRight_capillary_less_upper(int right_capillary_less_upper) {
        this.right_capillary_less_upper = right_capillary_less_upper;
    }

    /**
     * @param left_capillary_more_upper the left_capillary_more_upper to set
     */
    public void setLeft_capillary_more_upper(int left_capillary_more_upper) {
        this.left_capillary_more_upper = left_capillary_more_upper;
    }

    /**
     * @param right_capillary_more_upper the right_capillary_more_upper to set
     */
    public void setRight_capillary_more_upper(int right_capillary_more_upper) {
        this.right_capillary_more_upper = right_capillary_more_upper;
    }

}