package com.pixalere.struts.patient;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;

public class SensoryAssessmentAction extends Action{

	 private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SensoryAssessmentAction.class);
	 
	    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
	    	
	   
	    	SensoryAssessmentForm patientProfileForm = (SensoryAssessmentForm) form;
	    	HttpSession session = request.getSession();
	        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
	        
	        Integer language = Common.getLanguageIdFromSession(session);
	        String locale = Common.getLanguageLocale(language);
	        
	        boolean tokenValid = isTokenValid(request);
	        try {
	            PatientServiceImpl pamanager = new PatientServiceImpl(language);
	            if (session.getAttribute("patient_id") == null) {
	                return (mapping.findForward("uploader.null.patient"));
	            }

	            String patient_id = (String) session.getAttribute("patient_id");
	            String wound_id = (String) session.getAttribute("wound_id");
	           
	            if (tokenValid) {
	            	resetToken(request);
	                PatientProfileServiceImpl managerBD = new PatientProfileServiceImpl(language);

	                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
	                ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
	                ListServiceImpl lservice = new ListServiceImpl(language);
	                String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
	                
	                if (((String) request.getParameter("cancel") == null) || !((String) request.getParameter("cancel")).equals("yes")) {


	                    SensoryAssessmentVO sensory = patientProfileForm.getSensoryData(userVO);
	                    if (sensory != null) {
	                    	sensory.setPatient_id(new Integer(patient_id));

	                        sensory.setProfessional_id(userVO.getId());
	                        sensory.setUser_signature(user_signature);
	                        sensory.setCreated_on(new Date());
	                        sensory.setLastmodified_on(new Date());

	                        SensoryAssessmentVO tmpSA = new SensoryAssessmentVO();
	                        tmpSA.setPatient_id(new Integer(patient_id));
	                        tmpSA.setActive(0);
	                        tmpSA.setProfessional_id(userVO.getId());

	                        SensoryAssessmentVO tmpSensoryAssessment = managerBD.getSensoryAssessment(tmpSA);

	                        if (tmpSensoryAssessment != null) {
	                            sensory.setId(tmpSensoryAssessment.getId());
	                        }

	                        if (sensory != null) {
	                        	sensory.setDeleted(0);
	                        	sensory.setProfessional(userVO);
	                        }

	                        sensory.setDeleted(0);
	                           managerBD.saveSensory(sensory);
	                        
	                    }
	                }else {
	                    if (patient_id != null && userVO != null) {
	                        managerBD.dropTMPSensory(patient_id, userVO.getId() + "", null);
	                    }
	                }


	            }
	            
	            
	        } catch (ApplicationException e) {
	            ActionErrors errors = new ActionErrors();
	            request.setAttribute("exception", "y");
	            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
	            errors.add("exception", new ActionError("pixalere.common.error"));
	            saveErrors(request, errors);
	            return (mapping.findForward("pixalere.error"));

	        }

	        if (request.getParameter("page") == null && userVO != null) {
	            ActionErrors errors = new ActionErrors();
	            errors.add("errors", new ActionError("pixalere.common.page.error"));
	            saveErrors(request, errors);
	            return (mapping.findForward("uploader.patientprofile.error"));
	        }
	        
	        
	      
	                
	                 
	                 if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
	                     return (mapping.findForward("uploader.go.patient"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("exam")) {
	                     return (mapping.findForward("uploader.go.physicalexam"));
	                 }
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("sensory")) {
	         	           return (mapping.findForward("uploader.go.sensoryassessment"));
	                 }
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("braden")) {
	                     return (mapping.findForward("uploader.go.braden"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("foot")) {
	                     return (mapping.findForward("uploader.go.foot"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("limbbasic")) {
	                     return (mapping.findForward("uploader.go.limbbasic"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("limbadv")) {
	                     return (mapping.findForward("uploader.go.limbadv"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("limbupper")) {
	                     return (mapping.findForward("uploader.go.limbupper"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("investigations")) {
	                     return (mapping.findForward("uploader.go.investigations"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
	                     return (mapping.findForward("uploader.go.woundprofiles"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
	                     return (mapping.findForward("uploader.go.reporting"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
	                     return (mapping.findForward("uploader.go.assess"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
	                     return (mapping.findForward("uploader.go.treatment"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
	                     return (mapping.findForward("uploader.go.viewer"));
	                 }
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
	                     return (mapping.findForward("uploader.go.summary"));
	                 } 
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
	                     return (mapping.findForward("uploader.go.reporting"));
	                 }
	                 else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
	                     return (mapping.findForward("uploader.go.admin"));
	                 }
	                else {
	                     return (mapping.findForward("uploader.go.patient"));
	                 }

	             }
}