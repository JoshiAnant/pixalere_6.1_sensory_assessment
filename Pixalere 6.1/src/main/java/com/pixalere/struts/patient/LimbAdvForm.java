package com.pixalere.struts.patient;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import java.util.Date;

/**
 * @author
 *
 */
public class LimbAdvForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LimbAdvForm.class);
    
    HttpSession session = null;
    HttpServletRequest request = null;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        this.request=request;
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    private Integer right_digit_total;
    private Integer left_digit_total;
    private Double left_api_score;
    private Double right_api_score;
    private String[] left_pain_assessment;
    private String[] right_pain_assessment;
    private String[] left_limb_shape;
    private String[] right_limb_shape;
    private int left_temperature_leg;
    private int right_temperature_leg;
    private int left_temperature_foot;
    private int right_temperature_foot;
    private int left_temperature_toes;
    private int right_temperature_toes;
    private int left_stemmers;
    private int right_stemmers;
    private String limb_comments;
    private int left_homans;
    private int right_homans;
    private String left_toe_pressure;
    private String right_toe_pressure;
    private String left_brachial_pressure;
    private String right_brachial_pressure;
    private int left_transcutaneous_pressure;
    private int right_transcutaneous_pressure;
    private String left_tibial_pedis_ankle_brachial;
    private String right_tibial_pedis_ankle_brachial;
    private String left_dorsalis_pedis_ankle_brachial;
    private String right_dorsalis_pedis_ankle_brachial;
    private String left_ankle_brachial;
    private String right_ankle_brachial;
    private String[] left_dorsalis_pedis_doppler;
    private String[] right_dorsalis_pedis_doppler;
    private String[] left_posterior_tibial_doppler;
    private String[] right_posterior_tibial_doppler;
    private String[] left_interdigitial_doppler;
    private String[] right_interdigitial_doppler;
    private int doppler_lab;
    private int ankle_brachial_lab;
    private int ankle_brachial_day;
    private int ankle_brachial_month;
    private int ankle_brachial_year;
    private int toe_brachial_lab;
    private int toe_brachial_day;
    private int toe_brachial_month;
    private int toe_brachial_year;
    private String[] left_foot_deformities;
    private String[] right_foot_deformities;
    private String[] left_foot_skin;
    private String[] right_foot_skin;
    private String[] left_foot_toes;
    private String[] right_foot_toes;
    private String[] right_sensation;
    private String right_score_sensation;
    private String[] left_sensation;
    private String left_score_sensation;
    private int transcutaneous_lab;
    private int transcutaneous_month;
    private int transcutaneous_day;
    private int transcutaneous_year;
    private int referral_vascular_assessment;

    public LimbAdvAssessmentVO getLimbAdvData(ProfessionalVO userVO ) throws ApplicationException {
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        boolean save = false;
        LimbAdvAssessmentVO limb = new LimbAdvAssessmentVO();
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        limb.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale));
        limb.setCreated_on(new Date());
        limb.setLastmodified_on(new Date());
        limb.setActive(0);
        if(left_stemmers>0){save=true;}limb.setLeft_stemmers(left_stemmers);
        if(right_stemmers>0){save=true;}limb.setRight_stemmers(right_stemmers);
        if(left_homans>0){save=true;}limb.setLeft_homans(left_homans);
        if(right_homans>0){save=true;}limb.setRight_homans(right_homans);
        if(left_temperature_leg>0){save=true;}limb.setLeft_temperature_leg(left_temperature_leg);
        if(right_temperature_leg>0){save=true;}limb.setRight_temperature_leg(right_temperature_leg);
        if(left_temperature_foot>0){save=true;}limb.setLeft_temperature_foot(left_temperature_foot);
        if(right_temperature_foot>0){save=true;}limb.setRight_temperature_foot(right_temperature_foot);
        if(left_temperature_toes>0){save=true;}limb.setLeft_temperature_toes(left_temperature_toes);
        if(right_temperature_toes>0){save=true;}limb.setRight_temperature_toes(right_temperature_toes);
        if(!limb_comments.equals("")){save=true;}limb.setLimb_comments(Common.stripHTML(Common.convertCarriageReturns(limb_comments, false)));
        if(left_pain_assessment!=null){save=true;}limb.setLeft_pain_assessment(Serialize.serialize(left_pain_assessment));
        if(right_pain_assessment!=null){save=true;}limb.setRight_pain_assessment(Serialize.serialize(right_pain_assessment));
        if(referral_vascular_assessment>0){save=true;}limb.setReferral_vascular_assessment(getReferral_vascular_assessment());
        if(left_foot_deformities!=null){save=true;}limb.setLeft_foot_deformities(Serialize.serialize(left_foot_deformities));
        if(left_digit_total!=null && left_digit_total>0){save=true;}limb.setLeft_digit_total(left_digit_total);
        if(right_digit_total!=null && left_digit_total>0){save=true;}limb.setRight_digit_total(right_digit_total);
        if(left_api_score!=null){save=true;}limb.setLeft_api_score(left_api_score);
        if(right_api_score!=null){save=true;}limb.setRight_api_score(right_api_score);
        if(left_foot_skin!=null){save=true;}limb.setLeft_foot_skin(Serialize.serialize(left_foot_skin));
        if(left_foot_toes!=null){save=true;}limb.setLeft_foot_toes(Serialize.serialize(left_foot_toes));
        if(right_foot_deformities!=null){save=true;}limb.setRight_foot_deformities(Serialize.serialize(right_foot_deformities));
        if(right_foot_skin!=null){save=true;}limb.setRight_foot_skin(Serialize.serialize(right_foot_skin));
        if(right_foot_toes!=null){save=true;}limb.setRight_foot_toes(Serialize.serialize(right_foot_toes));
        if(left_limb_shape!=null){save=true;}limb.setLeft_limb_shape(Serialize.serialize(left_limb_shape));
        if(right_limb_shape!=null){save=true;}limb.setRight_limb_shape(Serialize.serialize(right_limb_shape));
        if(doppler_lab>0){save=true;}limb.setDoppler_lab(doppler_lab);
        if(left_dorsalis_pedis_doppler!=null){save=true;}limb.setLeft_dorsalis_pedis_doppler(Serialize.serialize(left_dorsalis_pedis_doppler));
        if(right_dorsalis_pedis_doppler!=null){save=true;}limb.setRight_dorsalis_pedis_doppler(Serialize.serialize(right_dorsalis_pedis_doppler));
        if(left_posterior_tibial_doppler!=null){save=true;}limb.setLeft_posterior_tibial_doppler(Serialize.serialize(left_posterior_tibial_doppler));
        if(right_posterior_tibial_doppler!=null){save=true;}limb.setRight_posterior_tibial_doppler(Serialize.serialize(right_posterior_tibial_doppler));
        if(left_interdigitial_doppler!=null){save=true;}limb.setLeft_interdigitial_doppler(Serialize.serialize(left_interdigitial_doppler));
        if(right_interdigitial_doppler!=null){save=true;}limb.setRight_interdigitial_doppler(Serialize.serialize(right_interdigitial_doppler));
        if(referral_vascular_assessment>0){save=true;}limb.setReferral_vascular_assessment(referral_vascular_assessment);
        if(ankle_brachial_lab>0){save=true;}limb.setAnkle_brachial_lab(ankle_brachial_lab);
        limb.setAnkle_brachial_date(pdate.getDate(getAnkle_brachial_month(), getAnkle_brachial_day(), getAnkle_brachial_year()));
        if(limb.getAnkle_brachial_date()!=null){save=true;}
        if(!left_tibial_pedis_ankle_brachial.equals("")){save=true;}limb.setLeft_tibial_pedis_ankle_brachial(left_tibial_pedis_ankle_brachial == null || left_tibial_pedis_ankle_brachial.equals("") ? null : new Integer(left_tibial_pedis_ankle_brachial));
        if(!right_tibial_pedis_ankle_brachial.equals("")){save=true;}limb.setRight_tibial_pedis_ankle_brachial(right_tibial_pedis_ankle_brachial == null || right_tibial_pedis_ankle_brachial.equals("") ? null : new Integer(right_tibial_pedis_ankle_brachial));
        if(!left_dorsalis_pedis_ankle_brachial.equals("")){save=true;}limb.setLeft_dorsalis_pedis_ankle_brachial(left_dorsalis_pedis_ankle_brachial == null || left_dorsalis_pedis_ankle_brachial.equals("") ? null : new Integer(left_dorsalis_pedis_ankle_brachial));
        if(!right_dorsalis_pedis_ankle_brachial.equals("")){save=true;}limb.setRight_dorsalis_pedis_ankle_brachial(right_dorsalis_pedis_ankle_brachial == null || right_dorsalis_pedis_ankle_brachial.equals("") ? null : new Integer(right_dorsalis_pedis_ankle_brachial));
        if(!left_ankle_brachial.equals("")){save=true;}limb.setLeft_ankle_brachial(left_ankle_brachial == null || left_ankle_brachial.equals("") ? null : new Integer(left_ankle_brachial));
        if(!right_ankle_brachial.equals("")){save=true;}limb.setRight_ankle_brachial(right_ankle_brachial == null || right_ankle_brachial.equals("") ? null : new Integer(right_ankle_brachial));

        if(toe_brachial_lab>0){save=true;}limb.setToe_brachial_lab(toe_brachial_lab);
        limb.setToe_brachial_date(pdate.getDate(getToe_brachial_month(), getToe_brachial_day(), getToe_brachial_year()));
        if(limb.getToe_brachial_date()!=null){save=true;}
        if(!left_toe_pressure.equals("")){save=true;}limb.setLeft_toe_pressure(left_toe_pressure);
        if(!right_toe_pressure.equals("")){save=true;}limb.setRight_toe_pressure(right_toe_pressure);
        if(!left_brachial_pressure.equals("")){save=true;}limb.setLeft_brachial_pressure(left_brachial_pressure);
        if(!right_brachial_pressure.equals("")){save=true;}limb.setRight_brachial_pressure(right_brachial_pressure);

        if(transcutaneous_lab>0){save=true;}limb.setTranscutaneous_lab(transcutaneous_lab);
        limb.setTranscutaneous_date(PDate.getDate(getTranscutaneous_month(), getTranscutaneous_day(), getTranscutaneous_year()));
        if(limb.getTranscutaneous_date()!=null){save=true;}
        if(left_transcutaneous_pressure>0){save=true;}limb.setLeft_transcutaneous_pressure(left_transcutaneous_pressure);
        if(right_transcutaneous_pressure>0){save=true;}limb.setRight_transcutaneous_pressure(right_transcutaneous_pressure);

        if(left_sensation!=null){save=true;}limb.setLeft_sensation(Serialize.serialize(left_sensation));
        if(!left_score_sensation.equals("")){save=true;}limb.setLeft_score_sensation(left_score_sensation);
        if(right_sensation!=null){save=true;}limb.setRight_sensation(Serialize.serialize(right_sensation));
        if(!right_score_sensation.equals("")){save=true;}limb.setRight_score_sensation(right_score_sensation);
        
        if(save){return limb;}else{return null;}
    }

    public int getDoppler_lab() {
        return doppler_lab;
    }

    public void setDoppler_lab(int doppler_lab) {
        this.doppler_lab = doppler_lab;
    }

    public String[] getLeft_dorsalis_pedis_doppler() {
        return left_dorsalis_pedis_doppler;
    }

    public void setLeft_dorsalis_pedis_doppler(String[] left_dorsalis_pedis_doppler) {
        this.left_dorsalis_pedis_doppler = left_dorsalis_pedis_doppler;
    }

    public String[] getRight_dorsalis_pedis_doppler() {
        return right_dorsalis_pedis_doppler;
    }

    public void setRight_dorsalis_pedis_doppler(String[] right_dorsalis_pedis_doppler) {
        this.right_dorsalis_pedis_doppler = right_dorsalis_pedis_doppler;
    }
    public String[] getRight_limb_shape() {
        return right_limb_shape;
    }

    public void setRight_limb_shape(String[] right_limb_shape) {
        this.right_limb_shape = right_limb_shape;
    }
    public String[] getLeft_limb_shape() {
        return left_limb_shape;
    }

    public void setLeft_limb_shape(String[] left_limb_shape) {
        this.left_limb_shape = left_limb_shape;
    }
    public String[] getLeft_posterior_tibial_doppler() {
        return left_posterior_tibial_doppler;
    }

    public void setLeft_posterior_tibial_doppler(String[] left_posterior_tibial_doppler) {
        this.left_posterior_tibial_doppler = left_posterior_tibial_doppler;
    }

    public String[] getRight_posterior_tibial_doppler() {
        return right_posterior_tibial_doppler;
    }

    public void setRight_posterior_tibial_doppler(String[] right_posterior_tibial_doppler) {
        this.right_posterior_tibial_doppler = right_posterior_tibial_doppler;
    }

    public String[] getLeft_interdigitial_doppler() {
        return left_interdigitial_doppler;
    }

    public void setLeft_interdigitial_doppler(String[] left_interdigitial_doppler) {
        this.left_interdigitial_doppler = left_interdigitial_doppler;
    }

    public String[] getRight_interdigitial_doppler() {
        return right_interdigitial_doppler;
    }

    public void setRight_interdigitial_doppler(String[] right_interdigitial_doppler) {
        this.right_interdigitial_doppler = right_interdigitial_doppler;
    }

    public int getAnkle_brachial_lab() {
        return ankle_brachial_lab;
    }

    public void setAnkle_brachial_lab(int ankle_brachial_lab) {
        this.ankle_brachial_lab = ankle_brachial_lab;
    }

    public String getLeft_tibial_pedis_ankle_brachial() {
        return left_tibial_pedis_ankle_brachial;
    }

    public void setLeft_tibial_pedis_ankle_brachial(String left_tibial_pedis_ankle_brachial) {
        this.left_tibial_pedis_ankle_brachial = left_tibial_pedis_ankle_brachial;
    }

    public String getRight_tibial_pedis_ankle_brachial() {
        return right_tibial_pedis_ankle_brachial;
    }

    public void setRight_tibial_pedis_ankle_brachial(String right_tibial_pedis_ankle_brachial) {
        this.right_tibial_pedis_ankle_brachial = right_tibial_pedis_ankle_brachial;
    }

    public String getLeft_dorsalis_pedis_ankle_brachial() {
        return left_dorsalis_pedis_ankle_brachial;
    }

    public void setLeft_dorsalis_pedis_ankle_brachial(String left_dorsalis_pedis_ankle_brachial) {
        this.left_dorsalis_pedis_ankle_brachial = left_dorsalis_pedis_ankle_brachial;
    }

    public String getRight_dorsalis_pedis_ankle_brachial() {
        return right_dorsalis_pedis_ankle_brachial;
    }

    public void setRight_dorsalis_pedis_ankle_brachial(String right_dorsalis_pedis_ankle_brachial) {
        this.right_dorsalis_pedis_ankle_brachial = right_dorsalis_pedis_ankle_brachial;
    }

    public String getLeft_ankle_brachial() {
        return left_ankle_brachial;
    }

    public void setLeft_ankle_brachial(String left_ankle_brachial) {
        this.left_ankle_brachial = left_ankle_brachial;
    }

    public String getRight_ankle_brachial() {
        return right_ankle_brachial;
    }

    public void setRight_ankle_brachial(String right_ankle_brachial) {
        this.right_ankle_brachial = right_ankle_brachial;
    }

    public int getToe_brachial_lab() {
        return toe_brachial_lab;
    }

    public void setToe_brachial_lab(int toe_brachial_lab) {
        this.toe_brachial_lab = toe_brachial_lab;
    }

    public String getLeft_toe_pressure() {
        return left_toe_pressure;
    }

    public void setLeft_toe_pressure(String left_toe_pressure) {
        this.left_toe_pressure = left_toe_pressure;
    }

    public String getRight_toe_pressure() {
        return right_toe_pressure;
    }

    public void setRight_toe_pressure(String right_toe_pressure) {
        this.right_toe_pressure = right_toe_pressure;
    }

    public String getLeft_brachial_pressure() {
        return left_brachial_pressure;
    }

    public void setLeft_brachial_pressure(String left_brachial_pressure) {
        this.left_brachial_pressure = left_brachial_pressure;
    }

    public String getRight_brachial_pressure() {
        return right_brachial_pressure;
    }

    public void setRight_brachial_pressure(String right_brachial_pressure) {
        this.right_brachial_pressure = right_brachial_pressure;
    }

    public int getLeft_transcutaneous_pressure() {
        return left_transcutaneous_pressure;
    }

    public void setLeft_transcutaneous_pressure(int left_transcutaneous_pressure) {
        this.left_transcutaneous_pressure = left_transcutaneous_pressure;
    }

    public int getRight_transcutaneous_pressure() {
        return right_transcutaneous_pressure;
    }

    public void setRight_transcutaneous_pressure(int right_transcutaneous_pressure) {
        this.right_transcutaneous_pressure = right_transcutaneous_pressure;
    }

    public String[] getLeft_sensation() {
        return left_sensation;
    }

    public void setLeft_sensation(String[] left_sensation) {
        this.left_sensation = left_sensation;
    }

    public String getLeft_score_sensation() {
        return left_score_sensation;
    }

    public void setLeft_score_sensation(String left_score_sensation) {
        this.left_score_sensation = left_score_sensation;
    }

 
    public String[] getRight_sensation() {
        return right_sensation;
    }

    public void setRight_sensation(String[] right_sensation) {
        this.right_sensation = right_sensation;
    }

    public String getRight_score_sensation() {
        return right_score_sensation;
    }

    public void setRight_score_sensation(String right_score_sensation) {
        this.right_score_sensation = right_score_sensation;
    }

 

    public String[] getLeft_foot_deformities() {
        return left_foot_deformities;
    }

    public void setLeft_foot_deformities(String[] left_foot_deformities) {
        this.left_foot_deformities = left_foot_deformities;
    }

    public String[] getRight_foot_deformities() {
        return right_foot_deformities;
    }

    public void setRight_foot_deformities(String[] right_foot_deformities) {
        this.right_foot_deformities = right_foot_deformities;
    }

    public String[] getLeft_foot_skin() {
        return left_foot_skin;
    }

    public void setLeft_foot_skin(String[] left_foot_skin) {
        this.left_foot_skin = left_foot_skin;
    }

    public String[] getRight_foot_skin() {
        return right_foot_skin;
    }

    public void setRight_foot_skin(String[] right_foot_skin) {
        this.right_foot_skin = right_foot_skin;
    }

    public String[] getLeft_foot_toes() {
        return left_foot_toes;
    }

    public void setLeft_foot_toes(String[] left_foot_toes) {
        this.left_foot_toes = left_foot_toes;
    }

    public String[] getRight_foot_toes() {
        return right_foot_toes;
    }

    public void setRight_foot_toes(String[] right_foot_toes) {
        this.right_foot_toes = right_foot_toes;
    }

    public int getToe_brachial_day() {
        return toe_brachial_day;
    }

    public void setToe_brachial_day(int toe_brachial_day) {
        this.toe_brachial_day = toe_brachial_day;
    }

    public int getToe_brachial_month() {
        return toe_brachial_month;
    }

    public void setToe_brachial_month(int toe_brachial_month) {
        this.toe_brachial_month = toe_brachial_month;
    }

    public int getToe_brachial_year() {
        return toe_brachial_year;
    }

    public void setToe_brachial_year(int toe_brachial_year) {
        this.toe_brachial_year = toe_brachial_year;
    }

    public int getAnkle_brachial_day() {
        return ankle_brachial_day;
    }

    public void setAnkle_brachial_day(int ankle_brachial_day) {
        this.ankle_brachial_day = ankle_brachial_day;
    }

    public int getAnkle_brachial_month() {
        return ankle_brachial_month;
    }

    public void setAnkle_brachial_month(int ankle_brachial_month) {
        this.ankle_brachial_month = ankle_brachial_month;
    }

    public int getAnkle_brachial_year() {
        return ankle_brachial_year;
    }

    public void setAnkle_brachial_year(int ankle_brachial_year) {
        this.ankle_brachial_year = ankle_brachial_year;
    }

    public int getTranscutaneous_lab() {
        return transcutaneous_lab;
    }

    public void setTranscutaneous_lab(int transcutaneous_lab) {
        this.transcutaneous_lab = transcutaneous_lab;
    }

    public int getTranscutaneous_month() {
        return transcutaneous_month;
    }

    public void setTranscutaneous_month(int transcutaneous_month) {
        this.transcutaneous_month = transcutaneous_month;
    }

    public int getTranscutaneous_day() {
        return transcutaneous_day;
    }

    public void setTranscutaneous_day(int transcutaneous_day) {
        this.transcutaneous_day = transcutaneous_day;
    }

    public int getTranscutaneous_year() {
        return transcutaneous_year;
    }

    public void setTranscutaneous_year(int transcutaneous_year) {
        this.transcutaneous_year = transcutaneous_year;
    }

    /**
     * @return the left_stemmers
     */
    public int getLeft_stemmers() {
        return left_stemmers;
    }

    /**
     * @param left_stemmers the left_stemmers to set
     */
    public void setLeft_stemmers(int left_stemmers) {
        this.left_stemmers = left_stemmers;
    }

    /**
     * @return the right_stemmers
     */
    public int getRight_stemmers() {
        return right_stemmers;
    }

    /**
     * @param right_stemmers the right_stemmers to set
     */
    public void setRight_stemmers(int right_stemmers) {
        this.right_stemmers = right_stemmers;
    }

    /**
     * @return the left_homans
     */
    public int getLeft_homans() {
        return left_homans;
    }

    /**
     * @param left_homans the left_homans to set
     */
    public void setLeft_homans(int left_homans) {
        this.left_homans = left_homans;
    }

    /**
     * @return the right_homans
     */
    public int getRight_homans() {
        return right_homans;
    }

    /**
     * @param right_homans the right_homans to set
     */
    public void setRight_homans(int right_homans) {
        this.right_homans = right_homans;
    }

    /**
     * @return the referral_vascular_assessment
     */
    public int getReferral_vascular_assessment() {
        return referral_vascular_assessment;
    }

    /**
     * @param referral_vascular_assessment the referral_vascular_assessment to
     * set
     */
    public void setReferral_vascular_assessment(int referral_vascular_assessment) {
        this.referral_vascular_assessment = referral_vascular_assessment;
    }

    /**
     * @return the left_pain_assessment
     */
    public String[] getLeft_pain_assessment() {
        return left_pain_assessment;
    }

    /**
     * @param left_pain_assessment the left_pain_assessment to set
     */
    public void setLeft_pain_assessment(String[] left_pain_assessment) {
        this.left_pain_assessment = left_pain_assessment;
    }

    /**
     * @return the right_pain_assessment
     */
    public String[] getRight_pain_assessment() {
        return right_pain_assessment;
    }

    /**
     * @param right_pain_assessment the right_pain_assessment to set
     */
    public void setRight_pain_assessment(String[] right_pain_assessment) {
        this.right_pain_assessment = right_pain_assessment;
    }

    /**
     * @return the left_temperature_leg
     */
    public int getLeft_temperature_leg() {
        return left_temperature_leg;
    }

    /**
     * @param left_temperature_leg the left_temperature_leg to set
     */
    public void setLeft_temperature_leg(int left_temperature_leg) {
        this.left_temperature_leg = left_temperature_leg;
    }

    /**
     * @return the right_temperature_leg
     */
    public int getRight_temperature_leg() {
        return right_temperature_leg;
    }

    /**
     * @param right_temperature_leg the right_temperature_leg to set
     */
    public void setRight_temperature_leg(int right_temperature_leg) {
        this.right_temperature_leg = right_temperature_leg;
    }

    /**
     * @return the left_temperature_foot
     */
    public int getLeft_temperature_foot() {
        return left_temperature_foot;
    }

    /**
     * @param left_temperature_foot the left_temperature_foot to set
     */
    public void setLeft_temperature_foot(int left_temperature_foot) {
        this.left_temperature_foot = left_temperature_foot;
    }

    /**
     * @return the right_temperature_foot
     */
    public int getRight_temperature_foot() {
        return right_temperature_foot;
    }

    /**
     * @param right_temperature_foot the right_temperature_foot to set
     */
    public void setRight_temperature_foot(int right_temperature_foot) {
        this.right_temperature_foot = right_temperature_foot;
    }

    /**
     * @return the left_temperature_toes
     */
    public int getLeft_temperature_toes() {
        return left_temperature_toes;
    }

    /**
     * @param left_temperature_toes the left_temperature_toes to set
     */
    public void setLeft_temperature_toes(int left_temperature_toes) {
        this.left_temperature_toes = left_temperature_toes;
    }

    /**
     * @return the right_temperature_toes
     */
    public int getRight_temperature_toes() {
        return right_temperature_toes;
    }

    /**
     * @param right_temperature_toes the right_temperature_toes to set
     */
    public void setRight_temperature_toes(int right_temperature_toes) {
        this.right_temperature_toes = right_temperature_toes;
    }

    /**
     * @return the limb_comments
     */
    public String getLimb_comments() {
        return limb_comments;
    }

    /**
     * @param limb_comments the limb_comments to set
     */
    public void setLimb_comments(String limb_comments) {
        this.limb_comments = limb_comments;
    }

    /**
     * @return the right_digit_total
     */
    public Integer getRight_digit_total() {
        return right_digit_total;
    }

    /**
     * @param right_digit_total the right_digit_total to set
     */
    public void setRight_digit_total(Integer right_digit_total) {
        this.right_digit_total = right_digit_total;
    }

    /**
     * @return the left_digit_total
     */
    public Integer getLeft_digit_total() {
        return left_digit_total;
    }

    /**
     * @param left_digit_total the left_digit_total to set
     */
    public void setLeft_digit_total(Integer left_digit_total) {
        this.left_digit_total = left_digit_total;
    }

    /**
     * @return the left_api_score
     */
    public Double getLeft_api_score() {
        return left_api_score;
    }

    /**
     * @param left_api_score the left_api_score to set
     */
    public void setLeft_api_score(Double left_api_score) {
        this.left_api_score = left_api_score;
    }

    /**
     * @return the right_api_score
     */
    public Double getRight_api_score() {
        return right_api_score;
    }

    /**
     * @param right_api_score the right_api_score to set
     */
    public void setRight_api_score(Double right_api_score) {
        this.right_api_score = right_api_score;
    }
}