package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;

import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.*;

public class LimbAdvSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LimbAdvSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);

        PatientServiceImpl pamanager = new PatientServiceImpl(language);
        log.error("Session: PP: " + session);
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        log.error("SEssion Patient_ID:" + session.getAttribute("patient_id"));
        String patient_id = (String) session.getAttribute("patient_id");

        //Generate Token - prevent multiple posts
        saveToken(request);
        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\nSetting Session: " + request.getParameter("form"));
        try {
            request.setAttribute("page", "patient");
            String locale = Common.getLanguageLocale(language);

            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            LimbAdvAssessmentVO pp = new LimbAdvAssessmentVO();
            pp.setPatient_id(new Integer(patient_id));
            pp.setActive(new Integer(1));

            pp.setDeleted(new Integer(0));
            LimbAdvAssessmentVO currentProfile = manager.getLimbAdvAssessment(pp);
            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na", locale)));

            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();

            comp.setFlowchart(com.pixalere.utils.Constants.LIMB_ASSESSMENT_ADVANCED);
            java.util.Collection components33 = gui.getAllComponents(comp);
            java.util.Hashtable hashized33 = Common.hashComponents(components33);
            request.setAttribute("componentLimbAdv", hashized33);

            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
            LimbAdvAssessmentVO latmp = new LimbAdvAssessmentVO();
            latmp.setActive(0);
            latmp.setPatient_id(new Integer(patient_id));
            latmp.setProfessional_id(userVO.getId());

            LimbAdvAssessmentVO limbadv = manager.getLimbAdvAssessment(latmp);

            if (limbadv == null && request.getParameter("pop") != null && ((String) request.getParameter("pop")).equals("1")) {
                limbadv = currentProfile;
            } else if (limbadv != null) {
                request.setAttribute("limb_comments", Common.convertCarriageReturns(limbadv.getLimb_comments(), true));

            }

            if (limbadv != null) {
                request.setAttribute("unserializeLeftFootDeformities", Serialize.arrayIze(limbadv.getLeft_foot_deformities()));
                request.setAttribute("unserializeLeftPainAssessment", Serialize.arrayIze(limbadv.getLeft_pain_assessment()));
                request.setAttribute("unserializeRightPainAssessment", Serialize.arrayIze(limbadv.getRight_pain_assessment()));
                request.setAttribute("unserializeLeftFootSkin", Serialize.arrayIze(limbadv.getLeft_foot_skin()));
                request.setAttribute("unserializeLeftFootToes", Serialize.arrayIze(limbadv.getLeft_foot_toes()));
                request.setAttribute("unserializeRightFootDeformities", Serialize.arrayIze(limbadv.getRight_foot_deformities()));
                request.setAttribute("unserializeRightFootSkin", Serialize.arrayIze(limbadv.getRight_foot_skin()));
                request.setAttribute("unserializeRightFootToes", Serialize.arrayIze(limbadv.getRight_foot_toes()));
                request.setAttribute("unserializeLeftDorsalisDoppler", Serialize.arrayIze(limbadv.getLeft_dorsalis_pedis_doppler()));
                request.setAttribute("unserializeLeftPosteriorDoppler", Serialize.arrayIze(limbadv.getLeft_posterior_tibial_doppler()));
                request.setAttribute("unserializeLeftInterdigitialDoppler", Serialize.arrayIze(limbadv.getLeft_interdigitial_doppler()));
                request.setAttribute("unserializeRightDorsalisDoppler", Serialize.arrayIze(limbadv.getRight_dorsalis_pedis_doppler()));
                request.setAttribute("unserializeRightPosteriorDoppler", Serialize.arrayIze(limbadv.getRight_posterior_tibial_doppler()));
                request.setAttribute("unserializeRightInterdigitialDoppler", Serialize.arrayIze(limbadv.getRight_interdigitial_doppler()));
                request.setAttribute("unserializeLeftSensation", Serialize.arrayIze(limbadv.getLeft_sensation()));
                request.setAttribute("unserializeRightSensation", Serialize.arrayIze(limbadv.getRight_sensation()));
                request.setAttribute("unserializeLeftLimbShape", Serialize.arrayIze(limbadv.getLeft_limb_shape()));
                request.setAttribute("unserializeRightLimbShape", Serialize.arrayIze(limbadv.getRight_limb_shape()));

            }

            request.setAttribute("limbadv", limbadv);
            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("doppler", listBD.getLists(LookupVO.DOPPLER));
            request.setAttribute("palpation", listBD.getLists(LookupVO.PALPATION));
            request.setAttribute("pain_assessment", listBD.getLists(LookupVO.PAIN_ADV_ASSESSMENT));
            request.setAttribute("sensation", listBD.getLists(LookupVO.SENSATION));
            request.setAttribute("deformities", listBD.getLists(LookupVO.DEFORMITIES));
            request.setAttribute("skin", listBD.getLists(LookupVO.SKIN_ADV_ASSESSMENT));

            request.setAttribute("range_of_motion", listBD.getLists(LookupVO.RANGE_OF_MOTION));
            request.setAttribute("toes", listBD.getLists(LookupVO.TOES));
            request.setAttribute("limbColor", listBD.getLists(LookupVO.LIMB_COLOR));
            request.setAttribute("limb_shape", listBD.getLists(LookupVO.LIMB_SHAPE));
            request.setAttribute("limbTemperature", listBD.getLists(LookupVO.LIMB_ADV_TEMPERATURE));
            request.setAttribute("limbEdema", listBD.getLists(LookupVO.LIMB_EDEMA));
            request.setAttribute("limbEdemaSeverity", listBD.getLists(LookupVO.LIMB_EDEMA_SEVERITY));
            request.setAttribute("transcutaneous_pressures", listBD.getLists(LookupVO.TRANSCUTANEOUS_PRESSURES));
            request.setAttribute("lab_done", listBD.getLists(LookupVO.LAB_DONE));

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (patient_id == null) {
            return (mapping.findForward("uploader.patienthome.success"));
        } else {
            return (mapping.findForward("uploader.limbadv.success"));
        }
    }
}
