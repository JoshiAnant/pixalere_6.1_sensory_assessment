package com.pixalere.struts.patient;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.PhysicalExamVO;
import java.util.Date;

/**
 * @author
 *
 */
public class PhysicalExamForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PhysicalExamForm.class);
    
    HttpSession session = null;
    HttpServletRequest request = null;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        this.request=request;
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    private int bloodsugar_month;
    private int bloodsugar_year;
    private int bloodsugar_day;
    private String blood_sugar_meal;
    private double blood_sugar;
    private double height;
    private double weight;
    private int resp_rate;
    private double temperature;
    private int hga1c;
    private int pulse;
    private String blood_pressure;

    public PhysicalExamVO getExam(ProfessionalVO userVO) throws ApplicationException {
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        PhysicalExamVO ex = new PhysicalExamVO();
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        ex.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale));
        ex.setActive(0);
        System.out.println("Height"+height+" "+(height <= 0.0 ? null : height));
        ex.setHeight(height <= 0.0 ? null : height);
        ex.setWeight(weight <= 0.0 ? null : weight);
        ex.setTemperature(temperature == 0.0 ? null : temperature);
        ex.setPulse(pulse == 0 ? null : pulse);
        ex.setResp_rate(resp_rate == 0 ? null : resp_rate);
        ex.setBlood_pressure(getBlood_pressure().equals("") ? null : getBlood_pressure());
        ex.setCreated_on(new Date());
        return ex;
    }

    public int getBloodsugar_month() {
        return bloodsugar_month;
    }

    public void setBloodsugar_month(int bloodsugar_month) {
        this.bloodsugar_month = bloodsugar_month;
    }

    public int getBloodsugar_year() {
        return bloodsugar_year;
    }

    public void setBloodsugar_year(int bloodsugar_year) {
        this.bloodsugar_year = bloodsugar_year;
    }

    public int getBloodsugar_day() {
        return bloodsugar_day;
    }

    public void setBloodsugar_day(int bloodsugar_day) {
        this.bloodsugar_day = bloodsugar_day;
    }

    public String getBlood_sugar_meal() {
        return blood_sugar_meal;
    }

    public void setBlood_sugar_meal(String blood_sugar_meal) {
        this.blood_sugar_meal = blood_sugar_meal;
    }

    public double getBlood_sugar() {
        return blood_sugar;
    }

    public void setBlood_sugar(double blood_sugar) {
        this.blood_sugar = blood_sugar;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @return the resp_rate
     */
    public int getResp_rate() {
        return resp_rate;
    }

    /**
     * @param resp_rate the resp_rate to set
     */
    public void setResp_rate(int resp_rate) {
        this.resp_rate = resp_rate;
    }

    /**
     * @return the temperature
     */
    public double getTemperature() {
        return temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the pulse
     */
    public int getPulse() {
        return pulse;
    }

    /**
     * @param pulse the pulse to set
     */
    public void setPulse(int pulse) {
        this.pulse = pulse;
    }
 /**
     * @return the hga1c
     */
    public int getHga1c() {
        return hga1c;
    }

    /**
     * @param hga1c the hga1c to set
     */
    public void setHga1c(int hga1c) {
        this.hga1c = hga1c;
    }
    
    /**
     * @return the blood_pressure
     */
    public String getBlood_pressure() {
        return blood_pressure;
    }

    /**
     * @param blood_pressure the blood_pressure to set
     */
    public void setBlood_pressure(String blood_pressure) {
        this.blood_pressure = blood_pressure;
    }
}