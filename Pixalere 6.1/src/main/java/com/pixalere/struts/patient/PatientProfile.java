package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import java.util.*;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.Common;
import com.pixalere.guibeans.RowData;
import com.pixalere.guibeans.FieldValues;
import java.util.Date;
import org.apache.struts.action.Action;
import com.pixalere.patient.bean.ExternalReferralVO;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PatientProfile extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientProfile.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        PatientProfileForm patientProfileForm = (PatientProfileForm) form;
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        boolean tokenValid = isTokenValid(request);
        try {
            PatientServiceImpl pamanager = new PatientServiceImpl(language);
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }

            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            
            if (tokenValid) {
                resetToken(request);
                PatientProfileServiceImpl managerBD = new PatientProfileServiceImpl(language);

                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);

                ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                ListServiceImpl lservice = new ListServiceImpl(language);
                String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
                if (((String) request.getParameter("cancel") == null) || !((String) request.getParameter("cancel")).equals("yes")) {

                    PatientProfileVO profileVO = patientProfileForm.getFormData(userVO, request);
                    PatientProfileVO currentTMPRecord = managerBD.getTemporaryPatientProfile(Integer.parseInt(patient_id), userVO.getId());
                    if (currentTMPRecord != null) {
                        profileVO.setStart_date(currentTMPRecord.getStart_date());
                        profileVO.setId(currentTMPRecord.getId());

                    }
                    profileVO.setProfessional_id(userVO.getId());
                    profileVO.setUser_signature(user_signature);
                    Date today = new Date();
                    profileVO.setCreated_on(today);
                    profileVO.setLastmodified_on(today);
                    profileVO.setOffline_flag(Common.isOffline() == true ? 1 : 0);
                    PatientAccountVO patientAcc = (PatientAccountVO) session.getAttribute("patientAccount");
                    if (patientAcc != null) {
                        profileVO.setTreatment_location_id(patientAcc.getTreatment_location_id());
                    }

                    PatientProfileVO currentPP = new PatientProfileVO();
                    currentPP.setPatient_id(new Integer(patient_id));
                    currentPP.setCurrent_flag(new Integer(1));
                    PatientProfileVO lastPatientProfile = managerBD.getPatientProfile(currentPP);
                    if(lastPatientProfile!=null){
                        profileVO.setStart_date(lastPatientProfile.getStart_date());
                    }
                    PatientProfileVO tmp2 = new PatientProfileVO();
                    tmp2.setPatient_id(new Integer(patient_id));
                    tmp2.setProfessional_id(userVO.getId());
                    tmp2.setActive(new Integer(0));
                    PatientProfileVO tmpProfile = managerBD.getPatientProfile(tmp2);
                    Collection<PatientProfileArraysVO> arrays = profileVO.getPatient_profile_arrays();
                    Collection resultsPP = new ArrayList();
                    if (lastPatientProfile != null) {
                        resultsPP.add(lastPatientProfile);
                    }
                    if (profileVO != null) {
                        profileVO.setProfessional(userVO);
                        List<PatientProfileArraysVO> new_arrays = new ArrayList();
                        for (PatientProfileArraysVO array : arrays) {
                            LookupVO item = lservice.getListItem(array.getLookup_id());
                            array.setLookup(item);
                            new_arrays.add(array);
                        }

                        profileVO.setPatient_profile_arrays(new_arrays);
                        profileVO.setDeleted(0);
                        resultsPP.add(profileVO);
                    } else {
                        // Add same data, to make compare think nothing's changed so it won't show in blue on summary page
                        resultsPP.add(lastPatientProfile);
                    }

                    Collection<ExternalReferralVO> er_arrays = profileVO.getExternalReferrals();
                    //The purpose of getting the temp object, and last submitted object and comparing them is to find out if anything changed, if nothing
                    //changed then there's no reason to save a  new patient profile.
                    RowData[] ppRecord = managerBD.getAllPatientProfilesForFlowchart(resultsPP, userVO, false,true);
                    List<FieldValues> comparedppdata = Common.compareGlobal(ppRecord);
                    if (tmpProfile != null) {
                        profileVO.setId(tmpProfile.getId());
                    }
                    if (Common.recordChanged(comparedppdata) == true) {
                        profileVO.setDeleted(0);
                        managerBD.savePatientProfile(profileVO);
                        PatientProfileVO initPP = new PatientProfileVO();
                        initPP.setPatient_id(new Integer(patient_id));
                        initPP.setActive(0);
                        initPP.setLastmodified_on(today);
                        PatientProfileVO vo = (PatientProfileVO) managerBD.getPatientProfile(initPP);

                        if (vo != null && arrays != null) {
                            //remove old tmp arrays
                            PatientProfileArraysVO ptmp = new PatientProfileArraysVO();
                            ptmp.setPatient_profile_id(vo.getId());
                            managerBD.removePatientArray(ptmp);
                            for (PatientProfileArraysVO a : arrays) {

                                a.setPatient_profile_id(vo.getId());
                                managerBD.savePatientProfileArray(a);
                            }
                        }
                        if (vo != null && er_arrays != null) {
                            //remove old tmp arrays
                            ExternalReferralVO ptmp = new ExternalReferralVO();
                            ptmp.setPatient_profile_id(vo.getId());
                            managerBD.removeExternalReferral(ptmp);
                            for (ExternalReferralVO i : er_arrays) {
                                i.setPatient_profile_id(vo.getId());
                                managerBD.saveExternalReferral(i);
                            }
                        }

                    }
                } else {
                    if (patient_id != null && userVO != null) {
                        managerBD.dropTMP(patient_id, userVO.getId() + "", null);
                    }
                }


            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfile.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));

        }

        if (request.getParameter(
                "page") == null && userVO != null) {
            System.out.println("Page parameter is null for " + userVO.getId() + ": " + request.getParameter("page") + " Token : " + tokenValid);
            ActionErrors errors = new ActionErrors();
            errors.add("errors", new ActionError("pixalere.common.page.error"));
            saveErrors(request, errors);
            return (mapping.findForward("uploader.patientprofile.error"));
        }

        if (request.getParameter(
                "page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("exam")) {
            return (mapping.findForward("uploader.go.physicalexam"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("braden")) {
            return (mapping.findForward("uploader.go.braden"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("foot")) {
            return (mapping.findForward("uploader.go.foot"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("limbbasic")) {
            return (mapping.findForward("uploader.go.limbbasic"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("limbadv")) {
            return (mapping.findForward("uploader.go.limbadv"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("limbupper")) {
            return (mapping.findForward("uploader.go.limbupper"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("investigations")) {
            return (mapping.findForward("uploader.go.investigations"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.woundprofiles"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else if (request.getParameter(
                "page") != null && request.getParameter("page").equals("sensory")) {
					return (mapping.findForward("uploader.go.sensoryassessment"));					
		} else {
            return (mapping.findForward("uploader.go.patient"));
        }
    }
}