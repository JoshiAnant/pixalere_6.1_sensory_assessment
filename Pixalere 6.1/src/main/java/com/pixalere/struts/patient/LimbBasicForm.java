package com.pixalere.struts.patient;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import java.util.Date;

/**
 * @author
 *
 */
public class LimbBasicForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LimbBasicForm.class);

    HttpSession session = null;
    HttpServletRequest request = null;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        this.request=request;
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    private int left_dorsalis_pedis_palpation;
    private int right_dorsalis_pedis_palpation;
    private int left_posterior_tibial_palpation;
    private int right_posterior_tibial_palpation;
    private String[] left_missing_limbs;
    private String[] right_missing_limbs;
    
    private String[] left_pain_assessment;
    private String[] right_pain_assessment;
    private String limb_comments;
    private String[] left_skin_assessment;
    private String[] right_skin_assessment;
    private Integer left_range_motion_ankle;
    private Integer right_range_motion_ankle;
    private Integer left_range_motion_knee;
    private Integer right_range_motion_knee;
    private Integer left_range_motion_toes;
    private Integer right_range_motion_toes;
    private String left_ankle_cm;
    private String left_ankle_mm;
    private String right_ankle_cm;
    private String right_ankle_mm;
    private String left_midcalf_cm;
    private String left_midcalf_mm;
    private String right_midcalf_cm;
    private String right_midcalf_mm;
    private String[] left_sensory;
    
    private String[] right_sensory;
    private int left_capillary_less;
    private int right_capillary_less;
    private int left_capillary_more;
    private int right_capillary_more;
    private int left_temperature_leg;
    private int right_temperature_leg;
    private int left_temperature_foot;
    private int right_temperature_foot;
    private int left_temperature_toes;
    private int right_temperature_toes;
    private int left_skin_colour_leg;
    private int right_skin_colour_leg;
    private int left_skin_colour_foot;
    private int right_skin_colour_foot;
    private int left_skin_colour_toes;
    private int right_skin_colour_toes;
    private String sleep_positions;
    private int left_edema_severity;
    private int right_edema_severity;
    private int left_edema_location;
    private int right_edema_location;
    private String pain_comments;
    
    public LimbBasicAssessmentVO getLimbBasicData(ProfessionalVO userVO) throws ApplicationException {
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        LimbBasicAssessmentVO limb = new LimbBasicAssessmentVO();
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        limb.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale));
        limb.setCreated_on(new Date());
        limb.setLastmodified_on(new Date());
        limb.setActive(0);
        
        boolean save = false;
        if(getLeft_range_motion_knee()!=null){save=true;}limb.setLeft_range_motion_knee(getLeft_range_motion_knee());
        if(getLeft_range_motion_ankle()!=null){save=true;}limb.setLeft_range_motion_ankle(getLeft_range_motion_ankle());
        if(getLeft_range_motion_toes()!=null){save=true;}limb.setLeft_range_motion_toe(getLeft_range_motion_toes());
        if(getRight_range_motion_knee()!=null){save=true;}limb.setRight_range_motion_knee(getRight_range_motion_knee());
        if(getRight_range_motion_ankle()!=null){save=true;}limb.setRight_range_motion_ankle(getRight_range_motion_ankle());
        if(getRight_range_motion_toes()!=null){save=true;}limb.setRight_range_motion_toe(getRight_range_motion_toes());
        if(left_missing_limbs!=null){save=true;}limb.setLeft_missing_limbs(Serialize.serialize(left_missing_limbs));
        if(right_missing_limbs!=null){save=true;}limb.setRight_missing_limbs(Serialize.serialize(right_missing_limbs));

        if(left_pain_assessment!=null){save=true;}limb.setLeft_pain_assessment(Serialize.serialize(left_pain_assessment));
        if(right_pain_assessment!=null){save=true;}limb.setRight_pain_assessment(Serialize.serialize(right_pain_assessment));
        if(pain_comments!=null && !pain_comments.equals("")){save=true;}limb.setPain_comments(pain_comments);

        if(left_skin_assessment!=null){save=true;}limb.setLeft_skin_assessment(Serialize.serialize(left_skin_assessment));
        if(right_skin_assessment!=null){save=true;}limb.setRight_skin_assessment(Serialize.serialize(right_skin_assessment));

        if(left_temperature_leg>0){save=true;}limb.setLeft_temperature_leg(left_temperature_leg);
        if(right_temperature_leg>0){save=true;}limb.setRight_temperature_leg(right_temperature_leg);
        if(left_temperature_foot>0){save=true;}limb.setLeft_temperature_foot(left_temperature_foot);
        if(right_temperature_foot>0){save=true;}limb.setRight_temperature_foot(right_temperature_foot);
        if(left_temperature_toes>0){save=true;}limb.setLeft_temperature_toes(left_temperature_toes);
        if(right_temperature_toes>0){save=true;}limb.setRight_temperature_toes(right_temperature_toes);

        if(left_skin_colour_leg>0){save=true;}limb.setLeft_skin_colour_leg(left_skin_colour_leg);
        if(right_skin_colour_leg>0){save=true;}limb.setRight_skin_colour_leg(right_skin_colour_leg);
        if(left_skin_colour_foot>0){save=true;}limb.setLeft_skin_colour_foot(left_skin_colour_foot);
        if(right_skin_colour_foot>0){save=true;}limb.setRight_skin_colour_foot(right_skin_colour_foot);
        if(left_skin_colour_toes>0){save=true;}limb.setLeft_skin_colour_toes(left_skin_colour_toes);
        if(right_skin_colour_toes>0){save=true;}limb.setRight_skin_colour_toes(right_skin_colour_toes);


        if(left_dorsalis_pedis_palpation>0){save=true;}limb.setLeft_dorsalis_pedis_palpation(left_dorsalis_pedis_palpation);
        if(right_dorsalis_pedis_palpation>0){save=true;}limb.setRight_dorsalis_pedis_palpation(right_dorsalis_pedis_palpation);
        if(left_posterior_tibial_palpation>0){save=true;}limb.setLeft_posterior_tibial_palpation(left_posterior_tibial_palpation);
        if(right_posterior_tibial_palpation>0){save=true;}limb.setRight_posterior_tibial_palpation(right_posterior_tibial_palpation);
        if(left_capillary_less>0){save=true;}limb.setLeft_less_capillary(left_capillary_less);
        if(right_capillary_less>0){save=true;}limb.setRight_less_capillary(right_capillary_less);
        if(left_capillary_more>0){save=true;}limb.setLeft_more_capillary(left_capillary_more);
        if(right_capillary_more>0){save=true;}limb.setRight_more_capillary(right_capillary_more);
        if(left_edema_severity>0){save=true;}limb.setLeft_edema_severity(left_edema_severity);
        if(right_edema_severity>0){save=true;}limb.setRight_edema_severity(right_edema_severity);

        if(left_edema_location>0){save=true;}limb.setLeft_edema_location(left_edema_location);
        if(right_edema_location>0){save=true;}limb.setRight_edema_location(right_edema_location);

        if(!sleep_positions.equals("")){save=true;}limb.setSleep_positions(Common.stripHTML(sleep_positions));
        if(!left_ankle_cm.equals("")){save=true;}limb.setLeft_ankle_cm(left_ankle_cm == null || left_ankle_cm.equals("") ? null : new Integer(left_ankle_cm));
        //System.out.println("Measurement: "+left_ankle_cm);
        if(!left_ankle_mm.equals("")){save=true;}limb.setLeft_ankle_mm(left_ankle_mm == null || left_ankle_mm.equals("") ? null : new Integer(left_ankle_mm));
        if(!right_ankle_cm.equals("")){save=true;}limb.setRight_ankle_cm(right_ankle_cm == null || right_ankle_cm.equals("") ? null : new Integer(right_ankle_cm));
        if(!right_ankle_mm.equals("")){save=true;}limb.setRight_ankle_mm(right_ankle_mm == null || right_ankle_mm.equals("") ? null : new Integer(right_ankle_mm));
        if(!left_midcalf_cm.equals("")){save=true;}limb.setLeft_midcalf_cm(left_midcalf_cm == null || left_midcalf_cm.equals("") ? null : new Integer(left_midcalf_cm));
        if(!left_midcalf_mm.equals("")){save=true;}limb.setLeft_midcalf_mm(left_midcalf_mm == null || left_midcalf_mm.equals("") ? null : new Integer(left_midcalf_mm));
        if(!right_midcalf_cm.equals("")){save=true;}limb.setRight_midcalf_cm(right_midcalf_cm == null || right_midcalf_cm.equals("") ? null : new Integer(right_midcalf_cm));
        if(!right_midcalf_mm.equals("")){save=true;}limb.setRight_midcalf_mm(right_midcalf_mm == null || right_midcalf_mm.equals("") ? null : new Integer(right_midcalf_mm));

         if(left_missing_limbs!=null){save=true;}limb.setLeft_sensory(Serialize.serialize(left_sensory));
        if(left_missing_limbs!=null){save=true;}limb.setRight_sensory(Serialize.serialize(right_sensory));
        if(limb_comments!=null && !limb_comments.equals("")){save=true;}limb.setLimb_comments(Common.stripHTML(Common.convertCarriageReturns(limb_comments, false)));
        if(save){return limb;}else{return null;}    
    }
    
    public String getLeft_ankle_cm() {
        return left_ankle_cm;
    }

    public void setLeft_ankle_cm(String left_ankle_cm) {
        this.left_ankle_cm = left_ankle_cm;
    }

    public String getLeft_ankle_mm() {
        return left_ankle_mm;
    }

    public void setLeft_ankle_mm(String left_ankle_mm) {
        this.left_ankle_mm = left_ankle_mm;
    }


    public int getLeft_dorsalis_pedis_palpation() {
        return left_dorsalis_pedis_palpation;
    }

    public void setLeft_dorsalis_pedis_palpation(int left_dorsalis_pedis_palpation) {
        this.left_dorsalis_pedis_palpation = left_dorsalis_pedis_palpation;
    }

    public int getRight_dorsalis_pedis_palpation() {
        return right_dorsalis_pedis_palpation;
    }

    public void setRight_dorsalis_pedis_palpation(int right_dorsalis_pedis_palpation) {
        this.right_dorsalis_pedis_palpation = right_dorsalis_pedis_palpation;
    }

    public int getLeft_posterior_tibial_palpation() {
        return left_posterior_tibial_palpation;
    }

    public void setLeft_posterior_tibial_palpation(int left_posterior_tibial_palpation) {
        this.left_posterior_tibial_palpation = left_posterior_tibial_palpation;
    }

    public int getRight_posterior_tibial_palpation() {
        return right_posterior_tibial_palpation;
    }

    public void setRight_posterior_tibial_palpation(int right_posterior_tibial_palpation) {
        this.right_posterior_tibial_palpation = right_posterior_tibial_palpation;
    }

    public int getLeft_edema_severity() {
        return left_edema_severity;
    }

    public void setLeft_edema_severity(int left_edema_severity) {
        this.left_edema_severity = left_edema_severity;
    }

    public int getRight_edema_severity() {
        return right_edema_severity;
    }

    public void setRight_edema_severity(int right_edema_severity) {
        this.right_edema_severity = right_edema_severity;
    }

    public int getLeft_edema_location() {
        return left_edema_location;
    }

    public void setLeft_edema_location(int left_edema_location) {
        this.left_edema_location = left_edema_location;
    }

    public int getRight_edema_location() {
        return right_edema_location;
    }

    public void setRight_edema_location(int right_edema_location) {
        this.right_edema_location = right_edema_location;
    }

    
    public String getRight_ankle_cm() {
        return right_ankle_cm;
    }

    public void setRight_ankle_cm(String right_ankle_cm) {
        this.right_ankle_cm = right_ankle_cm;
    }

    public String getRight_ankle_mm() {
        return right_ankle_mm;
    }

    public void setRight_ankle_mm(String right_ankle_mm) {
        this.right_ankle_mm = right_ankle_mm;
    }

    public String getLeft_midcalf_cm() {
        return left_midcalf_cm;
    }

    public void setLeft_midcalf_cm(String left_midcalf_cm) {
        this.left_midcalf_cm = left_midcalf_cm;
    }

    public String getLeft_midcalf_mm() {
        return left_midcalf_mm;
    }

    public void setLeft_midcalf_mm(String left_midcalf_mm) {
        this.left_midcalf_mm = left_midcalf_mm;
    }

    public String getRight_midcalf_cm() {
        return right_midcalf_cm;
    }

    public void setRight_midcalf_cm(String right_midcalf_cm) {
        this.right_midcalf_cm = right_midcalf_cm;
    }

    public String getRight_midcalf_mm() {
        return right_midcalf_mm;
    }

    public void setRight_midcalf_mm(String right_midcalf_mm) {
        this.right_midcalf_mm = right_midcalf_mm;
    }

    public String[] getLeft_missing_limbs() {
        return left_missing_limbs;
    }

    public void setLeft_missing_limbs(String[] left_missing_limbs) {
        this.left_missing_limbs = left_missing_limbs;
    }

    public String[] getRight_missing_limbs() {
        return right_missing_limbs;
    }

    public void setRight_missing_limbs(String[] right_missing_limbs) {
        this.right_missing_limbs = right_missing_limbs;
    }

   

    public String[] getLeft_pain_assessment() {
        return left_pain_assessment;
    }

    public void setLeft_pain_assessment(String[] left_pain_assessment) {
        this.left_pain_assessment = left_pain_assessment;
    }

    public String[] getRight_pain_assessment() {
        return right_pain_assessment;
    }

    public void setRight_pain_assessment(String[] right_pain_assessment) {
        this.right_pain_assessment = right_pain_assessment;
    }

    public String getPain_comments() {
        return pain_comments;
    }

    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }

    

    public String[] getLeft_skin_assessment() {
        return left_skin_assessment;
    }

    public void setLeft_skin_assessment(String[] left_skin_assessment) {
        this.left_skin_assessment = left_skin_assessment;
    }

    public String[] getRight_skin_assessment() {
        return right_skin_assessment;
    }

    public void setRight_skin_assessment(String[] right_skin_assessment) {
        this.right_skin_assessment = right_skin_assessment;
    }

  

    public int getLeft_temperature_leg() {
        return left_temperature_leg;
    }

    public void setLeft_temperature_leg(int left_temperature_leg) {
        this.left_temperature_leg = left_temperature_leg;
    }

    public int getRight_temperature_leg() {
        return right_temperature_leg;
    }

    public void setRight_temperature_leg(int right_temperature_leg) {
        this.right_temperature_leg = right_temperature_leg;
    }

    public int getLeft_temperature_foot() {
        return left_temperature_foot;
    }

    public void setLeft_temperature_foot(int left_temperature_foot) {
        this.left_temperature_foot = left_temperature_foot;
    }

    public int getRight_temperature_foot() {
        return right_temperature_foot;
    }

    public void setRight_temperature_foot(int right_temperature_foot) {
        this.right_temperature_foot = right_temperature_foot;
    }

    public int getLeft_temperature_toes() {
        return left_temperature_toes;
    }

    public void setLeft_temperature_toes(int left_temperature_toes) {
        this.left_temperature_toes = left_temperature_toes;
    }

    public int getRight_temperature_toes() {
        return right_temperature_toes;
    }

    public void setRight_temperature_toes(int right_temperature_toes) {
        this.right_temperature_toes = right_temperature_toes;
    }

    

    public int getLeft_skin_colour_leg() {
        return left_skin_colour_leg;
    }

    public void setLeft_skin_colour_leg(int left_skin_colour_leg) {
        this.left_skin_colour_leg = left_skin_colour_leg;
    }

    public int getRight_skin_colour_leg() {
        return right_skin_colour_leg;
    }

    public void setRight_skin_colour_leg(int right_skin_colour_leg) {
        this.right_skin_colour_leg = right_skin_colour_leg;
    }

    public int getLeft_skin_colour_foot() {
        return left_skin_colour_foot;
    }

    public void setLeft_skin_colour_foot(int left_skin_colour_foot) {
        this.left_skin_colour_foot = left_skin_colour_foot;
    }

    public int getRight_skin_colour_foot() {
        return right_skin_colour_foot;
    }

    public void setRight_skin_colour_foot(int right_skin_colour_foot) {
        this.right_skin_colour_foot = right_skin_colour_foot;
    }

    public int getLeft_skin_colour_toes() {
        return left_skin_colour_toes;
    }

    public void setLeft_skin_colour_toes(int left_skin_colour_toes) {
        this.left_skin_colour_toes = left_skin_colour_toes;
    }

    public int getRight_skin_colour_toes() {
        return right_skin_colour_toes;
    }

    public void setRight_skin_colour_toes(int right_skin_colour_toes) {
        this.right_skin_colour_toes = right_skin_colour_toes;
    }
public String getSleep_positions() {
        return sleep_positions;
    }

    public void setSleep_positions(String sleep_positions) {
        this.sleep_positions = sleep_positions;
    }
    

   

    public String[] getLeft_sensory() {
        return left_sensory;
    }

    public void setLeft_sensory(String[] left_sensory) {
        this.left_sensory = left_sensory;
    }

    

    public String[] getRight_sensory() {
        return right_sensory;
    }

    public void setRight_sensory(String[] right_sensory) {
        this.right_sensory = right_sensory;
    }
    

    public int getLeft_capillary_less() {
        return left_capillary_less;
    }

    public void setLeft_capillary_less(int left_capillary_less) {
        this.left_capillary_less = left_capillary_less;
    }

    public int getRight_capillary_less() {
        return right_capillary_less;
    }

    public void setRight_capillary_less(int right_capillary_less) {
        this.right_capillary_less = right_capillary_less;
    }

    public int getLeft_capillary_more() {
        return left_capillary_more;
    }

    public void setLeft_capillary_more(int left_capillary_more) {
        this.left_capillary_more = left_capillary_more;
    }

    public int getRight_capillary_more() {
        return right_capillary_more;
    }

    public void setRight_capillary_more(int right_capillary_more) {
        this.right_capillary_more = right_capillary_more;
    }
       public String getLimb_comments() {
        return limb_comments;
    }

    public void setLimb_comments(String limb_comments) {
        this.limb_comments = limb_comments;
    }

    /**
     * @return the left_range_motion_ankle
     */
    public Integer getLeft_range_motion_ankle() {
        return left_range_motion_ankle;
    }

    /**
     * @param left_range_motion_ankle the left_range_motion_ankle to set
     */
    public void setLeft_range_motion_ankle(Integer left_range_motion_ankle) {
        this.left_range_motion_ankle = left_range_motion_ankle;
    }

    /**
     * @return the right_range_motion_ankle
     */
    public Integer getRight_range_motion_ankle() {
        return right_range_motion_ankle;
    }

    /**
     * @param right_range_motion_ankle the right_range_motion_ankle to set
     */
    public void setRight_range_motion_ankle(Integer right_range_motion_ankle) {
        this.right_range_motion_ankle = right_range_motion_ankle;
    }

    /**
     * @return the left_range_motion_knee
     */
    public Integer getLeft_range_motion_knee() {
        return left_range_motion_knee;
    }

    /**
     * @param left_range_motion_knee the left_range_motion_knee to set
     */
    public void setLeft_range_motion_knee(Integer left_range_motion_knee) {
        this.left_range_motion_knee = left_range_motion_knee;
    }

    /**
     * @return the right_range_motion_knee
     */
    public Integer getRight_range_motion_knee() {
        return right_range_motion_knee;
    }

    /**
     * @param right_range_motion_knee the right_range_motion_knee to set
     */
    public void setRight_range_motion_knee(Integer right_range_motion_knee) {
        this.right_range_motion_knee = right_range_motion_knee;
    }

    /**
     * @return the left_range_motion_toes
     */
    public Integer getLeft_range_motion_toes() {
        return left_range_motion_toes;
    }

    /**
     * @param left_range_motion_toes the left_range_motion_toes to set
     */
    public void setLeft_range_motion_toes(Integer left_range_motion_toes) {
        this.left_range_motion_toes = left_range_motion_toes;
    }

    /**
     * @return the right_range_motion_toes
     */
    public Integer getRight_range_motion_toes() {
        return right_range_motion_toes;
    }

    /**
     * @param right_range_motion_toes the right_range_motion_toes to set
     */
    public void setRight_range_motion_toes(Integer right_range_motion_toes) {
        this.right_range_motion_toes = right_range_motion_toes;
    }
    

  
}