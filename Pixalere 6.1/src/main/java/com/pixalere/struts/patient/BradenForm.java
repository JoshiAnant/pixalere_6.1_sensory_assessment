package com.pixalere.struts.patient;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.BradenVO;
import java.util.Date;
/**
 * @author
 *
 */
public class BradenForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BradenForm.class);

    HttpSession session = null;
    HttpServletRequest request = null;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        this.request=request;
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    public BradenVO getBradenData(ProfessionalVO userVO) {
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        BradenVO braden = new BradenVO();
        braden.setActive(0);
        braden.setCurrent_flag(0);
        try{
        braden.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale));
        }catch(ApplicationException e){}
        braden.setCreated_on(new Date());
        braden.setLastmodified_on(new Date());
        braden.setBraden_sensory(getBraden_sensory());
        braden.setBraden_moisture(getBraden_moisture());
        braden.setBraden_activity(getBraden_activity());
        braden.setBraden_nutrition(getBraden_nutrition());
        braden.setBraden_friction(getBraden_friction());
        braden.setBraden_mobility(getBraden_mobility());
        if(braden_moisture<=0 && braden_sensory<=0 && braden_activity<=0 && braden_nutrition<=0 && braden_friction<=0 && braden_mobility<=0){
            return null;
        }else{
            return braden;
        }

    }
    private int braden_moisture;
    private int braden_friction;
    private int braden_nutrition;
    private int braden_sensory;
    private int braden_activity;
    private int braden_mobility;

    public int getBraden_moisture() {
        return braden_moisture;
    }

    public void setBraden_moisture(int braden_moisture) {
        this.braden_moisture = braden_moisture;
    }

    public int getBraden_friction() {
        return braden_friction;
    }

    public void setBraden_friction(int braden_friction) {
        this.braden_friction = braden_friction;
    }

    public int getBraden_nutrition() {
        return braden_nutrition;
    }

    public void setBraden_nutrition(int braden_nutrition) {
        this.braden_nutrition = braden_nutrition;
    }

    public int getBraden_sensory() {
        return braden_sensory;
    }

    public void setBraden_sensory(int braden_sensory) {
        this.braden_sensory = braden_sensory;
    }

    public int getBraden_activity() {
        return braden_activity;
    }

    public void setBraden_activity(int braden_activity) {
        this.braden_activity = braden_activity;
    }

    public int getBraden_mobility() {
        return braden_mobility;
    }

    public void setBraden_mobility(int braden_mobility) {
        this.braden_mobility = braden_mobility;
    }
}