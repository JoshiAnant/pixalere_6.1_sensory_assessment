package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.pixalere.utils.*;

public class FootSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FootSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        PatientServiceImpl pamanager = new PatientServiceImpl(language);
        log.error("Session: PP: " + session);
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        log.error("SEssion Patient_ID:" + session.getAttribute("patient_id"));
        String patient_id = (String) session.getAttribute("patient_id");
        //Generate Token - prevent multiple posts
        saveToken(request);
        try {
        	 request.setAttribute("page", "patient");

            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            FootAssessmentVO pp = new FootAssessmentVO();
            pp.setPatient_id(new Integer(patient_id));
            pp.setActive(new Integer(1));
            pp.setDeleted(new Integer(0));
            FootAssessmentVO currentProfile = manager.getFootAssessment(pp);
	           System.out.println("CurrentProfile:"+currentProfile);

            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na", locale)));

            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();

            comp.setFlowchart(com.pixalere.utils.Constants.FOOT_ASSESSMENT);
            java.util.Collection components3 = gui.getAllComponents(comp);
            java.util.Hashtable hashized3 = Common.hashComponents(components3);
            request.setAttribute("componentFoot", hashized3);

            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");

            FootAssessmentVO ltmp = new FootAssessmentVO();
            ltmp.setActive(0);
            ltmp.setPatient_id(new Integer(patient_id));
            ltmp.setProfessional_id(userVO.getId());

            FootAssessmentVO foot = manager.getFootAssessment(ltmp);
            if (foot == null && request.getParameter("pop") != null && ((String) request.getParameter("pop")).equals("1")) {
 	           System.out.println("CurrentProfile1:"+currentProfile);

                foot = currentProfile;
            } else if (foot != null) {
		           System.out.println("CurrentProfile2:"+currentProfile);

                request.setAttribute("foot_comments", Common.convertCarriageReturns(foot.getFoot_comments(), true));

            }
            request.setAttribute("foot", foot);

            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("weight_bearing_status", listBD.getLists(LookupVO.WEIGHT_BEARING_STATUS));
            request.setAttribute("balance", listBD.getLists(LookupVO.BALANCE));
            request.setAttribute("calf_muscle_pump", listBD.getLists(LookupVO.CALF_MUSCLE_PUMP_FUNCTION));
            request.setAttribute("mobility_aids", listBD.getLists(LookupVO.MOBILITY_AIDS));
            request.setAttribute("muscle_tone", listBD.getLists(LookupVO.MUSCLE_TONE));
            request.setAttribute("arches", listBD.getLists(LookupVO.ARCHES));
            request.setAttribute("foot_active", listBD.getLists(LookupVO.FOOT_ACTIVE));
            request.setAttribute("foot_passive", listBD.getLists(LookupVO.FOOT_PASSIVE));
            request.setAttribute("deformities", listBD.getLists(LookupVO.DEFORMITIES));
            request.setAttribute("skin", listBD.getLists(LookupVO.SKIN_ASSESSMENT));
            request.setAttribute("toes", listBD.getLists(LookupVO.TOES));
            request.setAttribute("proprioception", listBD.getLists(LookupVO.PROPRIOCEPTION));
            if (foot != null) {
                request.setAttribute("unserializeDorsiflexionActive", Serialize.arrayIze(foot.getDorsiflexion_active()));
                request.setAttribute("unserializeDorsiflexionPassive", Serialize.arrayIze(foot.getDorsiflexion_passive()));
                request.setAttribute("unserializeGreattoeActive", Serialize.arrayIze(foot.getGreattoe_active()));
                request.setAttribute("unserializeGreattoePassive", Serialize.arrayIze(foot.getGreattoe_passive()));
                request.setAttribute("unserializePlantarflexionPassive", Serialize.arrayIze(foot.getPlantarflexion_passive()));
                request.setAttribute("unserializePlantarflexionActive", Serialize.arrayIze(foot.getPlantarflexion_active()));
            }

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
     
        if (patient_id == null) {
            return (mapping.findForward("uploader.patienthome.success"));
        } else {
            return (mapping.findForward("uploader.foot.success"));
        }
    }
}
