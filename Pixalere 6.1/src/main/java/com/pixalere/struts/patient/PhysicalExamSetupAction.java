package com.pixalere.struts.patient;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PhysicalExamVO;

import com.pixalere.patient.service.PatientProfileServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.*;

public class PhysicalExamSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PhysicalExamSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        PatientServiceImpl pamanager = new PatientServiceImpl(language);

        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");

        //Generate Token - prevent multiple posts
        saveToken(request);

        try {
            request.setAttribute("page", "patient");
            
            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            PhysicalExamVO pp = new PhysicalExamVO();
            pp.setPatient_id(new Integer(patient_id));
            pp.setActive(new Integer(1));
            
            pp.setDeleted(new Integer(0));
            PhysicalExamVO currentProfile = manager.getExam(pp);
            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na",locale)));
            
            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            
            comp.setFlowchart(com.pixalere.utils.Constants.PHYSICAL_EXAM);
            java.util.Collection components5 = gui.getAllComponents(comp);
            java.util.Hashtable hashized5 = Common.hashComponents(components5);
            request.setAttribute("componentExam", hashized5);
            
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");

            //only inactive physical exams are retrieved, nurse should update fresh each visit.
            PhysicalExamVO extmp = new PhysicalExamVO();
            extmp.setActive(0);
            extmp.setPatient_id(new Integer(patient_id));
            
            extmp.setDeleted(new Integer(0));
            extmp.setProfessional_id(userVO.getId());

            PhysicalExamVO exam = manager.getExam(extmp);
            
            request.setAttribute("exam", exam);
            
        } catch (com.pixalere.common.ApplicationException e) {
            log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (patient_id == null) {
            return (mapping.findForward("uploader.patienthome.success"));
        } else {
            return (mapping.findForward("uploader.exam.success"));
        }
    }
}
