package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.bean.DiagnosticInvestigationVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.*;

public class InvestigationsSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InvestigationsSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");

        PatientServiceImpl pamanager = new PatientServiceImpl(language);
        log.error("Session: PP: " + session);
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        log.error("SEssion Patient_ID:" + session.getAttribute("patient_id"));
        String patient_id = (String) session.getAttribute("patient_id");

        //Generate Token - prevent multiple posts
        saveToken(request);
        try {
            request.setAttribute("page", "patient");
            
            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            DiagnosticInvestigationVO pp = new DiagnosticInvestigationVO();
            pp.setPatient_id(new Integer(patient_id));
            pp.setActive(new Integer(1));
            
            pp.setDeleted(new Integer(0));
            DiagnosticInvestigationVO currentProfile = manager.getDiagnosticInvestigation(pp);
            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na", locale)));
            DiagnosticInvestigationVO lutmp = new DiagnosticInvestigationVO();

            lutmp.setActive(0);
            lutmp.setPatient_id(new Integer(patient_id));
            lutmp.setProfessional_id(userVO.getId());
            DiagnosticInvestigationVO patient = manager.getDiagnosticInvestigation(lutmp);

            if (patient == null) {
                patient = currentProfile;
                //if inactive == false, clear previously active blood sugar values (force repop)
                /*patient.setBlood_sugar(null);
                patient.setBlood_sugar_date(null);
                patient.setBlood_sugar_meal("");*/
            }
            request.setAttribute("patient", patient);
            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("investigation", listBD.getLists(LookupVO.INVESTIGATIONS));
            request.setAttribute("albumin", listBD.getLists(LookupVO.ALBUMIN));
            request.setAttribute("pre_albumin", listBD.getLists(LookupVO.PREALBUMIN));

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (patient_id == null) {
            return (mapping.findForward("uploader.patienthome.success"));
        } else {
            return (mapping.findForward("uploader.investigations.success"));
        }
    }
}
