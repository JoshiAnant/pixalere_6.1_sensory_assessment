package com.pixalere.struts.patient;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import com.pixalere.patient.bean.PhysicalExamVO;
import java.util.*;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.utils.Common;

import com.pixalere.guibeans.RowData;
import com.pixalere.guibeans.FieldValues;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PhysicalExamAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PhysicalExamAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        PhysicalExamForm patientProfileForm = (PhysicalExamForm) form;
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        boolean tokenValid = isTokenValid(request);
        try {
            PatientServiceImpl pamanager = new PatientServiceImpl(language);
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }

            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            if (tokenValid) {
                resetToken(request);
                PatientProfileServiceImpl managerBD = new PatientProfileServiceImpl(language);

                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                ListServiceImpl lservice = new ListServiceImpl(language);
                String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);

                if (((String) request.getParameter("cancel") == null) || !((String) request.getParameter("cancel")).equals("yes")) {
                    //check physicalexam.
                    PhysicalExamVO exam = patientProfileForm.getExam(userVO);
                    exam.setProfessional_id(userVO.getId());
                    exam.setUser_signature(user_signature);
                    exam.setPatient_id(new Integer(patient_id));
                    
                    PhysicalExamVO exTMP = new PhysicalExamVO();
                    exTMP.setPatient_id(new Integer(patient_id));
                    exTMP.setActive(0);
                    exTMP.setProfessional_id(userVO.getId());

                    PhysicalExamVO tmpExam = managerBD.getExam(exTMP);
                    
                    if (exam != null) {
                        exam.setDeleted(0);
                        exam.setProfessional(userVO);
                    }


                    if (tmpExam != null) {
                        exam.setId(tmpExam.getId());
                    }
                    
                        exam.setDeleted(0);
                        managerBD.savePhysicalExam(exam);
                    
                } else {
                    if (patient_id != null && userVO != null) {
                        managerBD.dropTMPExam(patient_id, userVO.getId() + "", null);
                    }
                }
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientProfile.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));

        }

        if (request.getParameter("page") == null && userVO != null) {
            System.out.println("Page parameter is null for " + userVO.getId() + ": " + request.getParameter("page") + " Token : " + tokenValid);
            ActionErrors errors = new ActionErrors();
            errors.add("errors", new ActionError("pixalere.common.page.error"));
            saveErrors(request, errors);
            return (mapping.findForward("uploader.patientprofile.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("exam")) {
            return (mapping.findForward("uploader.go.physicalexam"));
        }else if (request.getParameter("page") != null && request.getParameter("page").equals("sensory")) {
	        return (mapping.findForward("uploader.go.sensoryassessment"));
        }else if (request.getParameter("page") != null && request.getParameter("page").equals("braden")) {
            return (mapping.findForward("uploader.go.braden"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("foot")) {
            return (mapping.findForward("uploader.go.foot"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbbasic")) {
            return (mapping.findForward("uploader.go.limbbasic"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbadv")) {
            return (mapping.findForward("uploader.go.limbadv"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbupper")) {
            return (mapping.findForward("uploader.go.limbupper"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("investigations")) {
            return (mapping.findForward("uploader.go.investigations"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.woundprofiles"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("uploader.go.patient"));
        }

    }
}
