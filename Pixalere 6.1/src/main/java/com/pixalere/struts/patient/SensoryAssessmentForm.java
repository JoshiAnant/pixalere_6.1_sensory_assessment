package com.pixalere.struts.patient;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;

public class SensoryAssessmentForm extends ActionForm {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(SensoryAssessmentForm.class);
	HttpSession session = null;
	HttpServletRequest request = null;

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		session = request.getSession();
		ActionErrors errors = new ActionErrors();
		return errors;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
	}

	private String user_signature;

	private int skin_leftfoot;
	private int skin_rightfoot;
	private String skin_comments;

	private int nails_leftfoot;
	private int nails_rightfoot;
	private String nails_comments;

	private int deformity_leftfoot;
	private int deformity_rightfoot;
	private String deformity_comments;

	private int footwear_leftfoot;
	private int footwear_rightfoot;
	private String footwear_comments;

	private int tempcold_leftfoot;
	private int tempcold_rightfoot;
	private String tempcold_comments;

	private int temphot_leftfoot;
	private int temphot_rightfoot;
	private String temphot_comments;

	private int rangeofmotion_leftfoot;
	private int rangeofmotion_rightfoot;
	private String rangeofmotion_comments;

	private int sensation_monofilament_leftfoot;
	private int sensation_monofilament_rightfoot;
	private String sensation_monofilament_comments;

	private int sensation_question_leftfoot;
	private int sensation_question_rightfoot;
	private String sensation_question_comments;

	private int pedal_pulse_leftfoot;
	private int pedal_pulse_rightfoot;
	private String pedal_pulse_comments;

	private int dependent_rubor_leftfoot;
	private int dependent_rubor_rightfoot;
	private String dependent_rubor_comments;

	private int erythema_leftfoot;
	private int erythema_rightfoot;
	private String erythema_comments;
	private int leftfoot_score;
	private int rightfoot_score;
	private int leftfoot_image_score;
	private int rightfoot_image_score;
	

	

	public int getLeftfoot_score() {
		return leftfoot_score;
	}

	public void setLeftfoot_score(int leftfoot_score) {
		this.leftfoot_score = leftfoot_score;
	}

	public int getRightfoot_score() {
		return rightfoot_score;
	}

	public void setRightfoot_score(int rightfoot_score) {
		this.rightfoot_score = rightfoot_score;
	}

	private Date created_on;
	private Integer professional_id;
	private ProfessionalVO professional;
	private Integer active;
	private Integer id;
	private Date lastmodified_on;

	public SensoryAssessmentVO getSensoryData(ProfessionalVO userVO)
			throws ApplicationException {

		Integer language = Common.getLanguageIdFromSession(session);
		String locale = Common.getLanguageLocale(language);

		boolean save = false;
		SensoryAssessmentVO sensory = new SensoryAssessmentVO();
		PDate pdate = new PDate(userVO != null ? userVO.getTimezone()
				: Constants.TIMEZONE);
		sensory.setUser_signature(pdate.getProfessionalSignature(new Date(),
				(ProfessionalVO) session.getAttribute("userVO"), locale));
		sensory.setCreated_on(new Date());
		sensory.setLastmodified_on(new Date());
		sensory.setActive(0);

		if (skin_leftfoot > 0) {
			save = true;
		}
		sensory.setSkin_leftfoot(skin_leftfoot);
		if (skin_rightfoot > 0) {
			save = true;
		}
		sensory.setSkin_rightfoot(skin_rightfoot);
		
		if (skin_comments != null && !skin_comments.equals("")) {
			save = true;
		}
		sensory.setSkin_comments(Common.stripHTML(skin_comments));

		if (nails_leftfoot > 0) {
			save = true;
		}
		sensory.setNails_leftfoot(nails_leftfoot);
		if (nails_rightfoot > 0) {
			save = true;
		}
		sensory.setNails_rightfoot(nails_rightfoot);
		if (nails_comments != null && !nails_comments.equals("")) {
			save = true;
		}
		sensory.setNails_comments(Common.stripHTML(nails_comments));

		if (deformity_leftfoot > 0) {
			save = true;
		}
		sensory.setDeformity_leftfoot(deformity_leftfoot);
		if (deformity_rightfoot > 0) {
			save = true;
		}
		sensory.setDeformity_rightfoot(deformity_rightfoot);
		if (deformity_comments != null && !deformity_comments.equals("")) {
			save = true;
		}
		sensory.setDeformity_comments(Common.stripHTML(deformity_comments));

		if (footwear_leftfoot > 0) {
			save = true;
		}
		sensory.setFootwear_leftfoot(footwear_leftfoot);
		if (footwear_rightfoot > 0) {
			save = true;
		}
		sensory.setFootwear_rightfoot(footwear_rightfoot);
		if (footwear_comments != null && !footwear_comments.equals("")) {
			save = true;
		}
		sensory.setFootwear_comments(Common.stripHTML(footwear_comments));

		if (tempcold_leftfoot > 0) {
			save = true;
		}
		sensory.setTempcold_leftfoot(tempcold_leftfoot);
		if (tempcold_rightfoot > 0) {
			save = true;
		}
		sensory.setTempcold_rightfoot(tempcold_rightfoot);
		if (tempcold_comments != null && !tempcold_comments.equals("")) {
			save = true;
		}
		sensory.setTempcold_comments(Common.stripHTML(tempcold_comments));

		if (temphot_leftfoot > 0) {
			save = true;
		}
		sensory.setTemphot_leftfoot(temphot_leftfoot);
		if (temphot_rightfoot > 0) {
			save = true;
		}
		sensory.setTemphot_rightfoot(temphot_rightfoot);
		if (temphot_comments != null && !temphot_comments.equals("")) {
			save = true;
		}
		sensory.setTemphot_comments(Common.stripHTML(temphot_comments));

		if (rangeofmotion_leftfoot > 0) {
			save = true;
		}
		sensory.setRangeofmotion_leftfoot(rangeofmotion_leftfoot);
		if (rangeofmotion_rightfoot > 0) {
			save = true;
		}
		sensory.setRangeofmotion_rightfoot(rangeofmotion_rightfoot);
		if (rangeofmotion_comments != null
				&& !rangeofmotion_comments.equals("")) {
			save = true;
		}
		sensory.setRangeofmotion_comments(Common
				.stripHTML(rangeofmotion_comments));

		if (sensation_monofilament_leftfoot > 0) {
			
			save = true;
		}
		sensory.setSensation_monofilament_leftfoot(sensation_monofilament_leftfoot);
		
		if (sensation_monofilament_rightfoot > 0) {
			save = true;
		}
		sensory.setSensation_monofilament_rightfoot(sensation_monofilament_rightfoot);
		if (sensation_monofilament_comments != null
				&& !sensation_monofilament_comments.equals("")) {
			save = true;
		}
		sensory.setSensation_monofilament_comments(Common
				.stripHTML(sensation_monofilament_comments));

		if (sensation_question_leftfoot > 0) {
			save = true;
		}
		sensory.setSensation_question_leftfoot(sensation_question_leftfoot);
		if (sensation_question_rightfoot > 0) {
			save = true;
		}
		sensory.setSensation_question_rightfoot(sensation_question_rightfoot);
		if (sensation_question_comments != null
				&& !sensation_monofilament_comments.equals("")) {
			save = true;
		}
		sensory.setSensation_question_comments(Common
				.stripHTML(sensation_question_comments));
		if (pedal_pulse_leftfoot > 0) {
			save = true;
		}
		sensory.setPedal_pulse_leftfoot(pedal_pulse_leftfoot);
		if (pedal_pulse_rightfoot > 0) {
			save = true;
		}
		sensory.setPedal_pulse_rightfoot(pedal_pulse_rightfoot);
		if (pedal_pulse_comments != null && !pedal_pulse_comments.equals("")) {
			save = true;
		}
		sensory.setPedal_pulse_comments(Common.stripHTML(pedal_pulse_comments));
		if (dependent_rubor_leftfoot > 0) {
			save = true;
		}
		sensory.setDependent_rubor_leftfoot(dependent_rubor_leftfoot);
		if (dependent_rubor_rightfoot > 0) {
			save = true;
		}
		sensory.setDependent_rubor_rightfoot(dependent_rubor_rightfoot);
		if (dependent_rubor_comments != null
				&& !dependent_rubor_comments.equals("")) {
			save = true;
		}
		sensory.setDependent_rubor_comments(Common
				.stripHTML(pedal_pulse_comments));
		if (erythema_leftfoot > 0) {
			save = true;
		}
		sensory.setErythema_leftfoot(erythema_leftfoot);
		if (erythema_rightfoot > 0) {
			save = true;
		}
		sensory.setErythema_rightfoot(erythema_rightfoot);
		if (erythema_comments != null && !erythema_comments.equals("")) {
			save = true;
		}
		sensory.setErythema_comments(Common.stripHTML(erythema_comments));
		
		sensory.setLeftfoot_score(leftfoot_score);
		if (leftfoot_score > 0){
		
			save = true;
		}
		sensory.setRightfoot_score(rightfoot_score);
		if  (rightfoot_score > 0){
			save = true;
		}
		sensory.setLeftfoot_image_score(leftfoot_image_score);
		if (leftfoot_image_score > 0){
			
			save = true;
		}
		sensory.setRightfoot_image_score(rightfoot_image_score);
		if  (rightfoot_image_score > 0){
			save = true;
		}
		else {
			return null;
		}
		return sensory;

	}

	public Date getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public Integer getProfessional_id() {
		return professional_id;
	}

	public void setProfessional_id(Integer professional_id) {
		this.professional_id = professional_id;
	}

	public ProfessionalVO getProfessional() {
		return professional;
	}

	public void setProfessional(ProfessionalVO professional) {
		this.professional = professional;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getLastmodified_on() {
		return lastmodified_on;
	}

	public void setLastmodified_on(Date lastmodified_on) {
		this.lastmodified_on = lastmodified_on;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public int getSkin_leftfoot() {
		return skin_leftfoot;
	}

	public void setSkin_leftfoot(int skin_leftfoot) {
		this.skin_leftfoot = skin_leftfoot;
	}

	public int getSkin_rightfoot() {
		return skin_rightfoot;
	}

	public void setSkin_rightfoot(int skin_rightfoot) {
		this.skin_rightfoot = skin_rightfoot;
	}

	public String getSkin_comments() {
		return skin_comments;
	}

	public void setSkin_comments(String skin_comments) {
		this.skin_comments = skin_comments;
	}

	public int getNails_leftfoot() {
		return nails_leftfoot;
	}

	public void setNails_leftfoot(int nails_leftfoot) {
		this.nails_leftfoot = nails_leftfoot;
	}

	public int getNails_rightfoot() {
		return nails_rightfoot;
	}

	public void setNails_rightfoot(int nails_rightfoot) {
		this.nails_rightfoot = nails_rightfoot;
	}

	public String getNails_comments() {
		return nails_comments;
	}

	public void setNails_comments(String nails_comments) {
		this.nails_comments = nails_comments;
	}

	public int getDeformity_leftfoot() {
		return deformity_leftfoot;
	}

	public void setDeformity_leftfoot(int deformity_leftfoot) {
		this.deformity_leftfoot = deformity_leftfoot;
	}

	public int getDeformity_rightfoot() {
		return deformity_rightfoot;
	}

	public void setDeformity_rightfoot(int deformity_rightfoot) {
		this.deformity_rightfoot = deformity_rightfoot;
	}

	public String getDeformity_comments() {
		return deformity_comments;
	}

	public void setDeformity_comments(String deformity_comments) {
		this.deformity_comments = deformity_comments;
	}

	public int getFootwear_leftfoot() {
		return footwear_leftfoot;
	}

	public void setFootwear_leftfoot(int footwear_leftfoot) {
		this.footwear_leftfoot = footwear_leftfoot;
	}

	public int getFootwear_rightfoot() {
		return footwear_rightfoot;
	}

	public void setFootwear_rightfoot(int footwear_rightfoot) {
		this.footwear_rightfoot = footwear_rightfoot;
	}

	public String getFootwear_comments() {
		return footwear_comments;
	}

	public void setFootwear_comments(String footwear_comments) {
		this.footwear_comments = footwear_comments;
	}

	public int getTempcold_leftfoot() {
		return tempcold_leftfoot;
	}

	public void setTempcold_leftfoot(int tempcold_leftfoot) {
		this.tempcold_leftfoot = tempcold_leftfoot;
	}

	public int getTempcold_rightfoot() {
		return tempcold_rightfoot;
	}

	public void setTempcold_rightfoot(int tempcold_rightfoot) {
		this.tempcold_rightfoot = tempcold_rightfoot;
	}

	public String getTempcold_comments() {
		return tempcold_comments;
	}

	public void setTempcold_comments(String tempcold_comments) {
		this.tempcold_comments = tempcold_comments;
	}

	public int getRangeofmotion_leftfoot() {
		return rangeofmotion_leftfoot;
	}

	public void setRangeofmotion_leftfoot(int rangeofmotion_leftfoot) {
		this.rangeofmotion_leftfoot = rangeofmotion_leftfoot;
	}

	public int getRangeofmotion_rightfoot() {
		return rangeofmotion_rightfoot;
	}

	public void setRangeofmotion_rightfoot(int rangeofmotion_rightfoot) {
		this.rangeofmotion_rightfoot = rangeofmotion_rightfoot;
	}

	public String getRangeofmotion_comments() {
		return rangeofmotion_comments;
	}

	public void setRangeofmotion_comments(String rangeofmotion_comments) {
		this.rangeofmotion_comments = rangeofmotion_comments;
	}

	public int getSensation_monofilament_leftfoot() {
		return sensation_monofilament_leftfoot;
	}

	public void setSensation_monofilament_leftfoot(
			int sensation_monofilament_leftfoot) {
		this.sensation_monofilament_leftfoot = sensation_monofilament_leftfoot;
	}

	public int getSensation_monofilament_rightfoot() {
		return sensation_monofilament_rightfoot;
	}

	public void setSensation_monofilament_rightfoot(
			int sensation_monofilament_rightfoot) {
		this.sensation_monofilament_rightfoot = sensation_monofilament_rightfoot;
	}

	public String getSensation_monofilament_comments() {
		return sensation_monofilament_comments;
	}

	public void setSensation_monofilament_comments(
			String sensation_monofilament_comments) {
		this.sensation_monofilament_comments = sensation_monofilament_comments;
	}

	public int getSensation_question_leftfoot() {
		return sensation_question_leftfoot;
	}

	public void setSensation_question_leftfoot(int sensation_question_leftfoot) {
		this.sensation_question_leftfoot = sensation_question_leftfoot;
	}

	public int getSensation_question_rightfoot() {
		return sensation_question_rightfoot;
	}

	public void setSensation_question_rightfoot(int sensation_question_rightfoot) {
		this.sensation_question_rightfoot = sensation_question_rightfoot;
	}

	public String getSensation_question_comments() {
		return sensation_question_comments;
	}

	public void setSensation_question_comments(
			String sensation_question_comments) {
		this.sensation_question_comments = sensation_question_comments;
	}

	public int getPedal_pulse_leftfoot() {
		return pedal_pulse_leftfoot;
	}

	public void setPedal_pulse_leftfoot(int pedal_pulse_leftfoot) {
		this.pedal_pulse_leftfoot = pedal_pulse_leftfoot;
	}

	public int getPedal_pulse_rightfoot() {
		return pedal_pulse_rightfoot;
	}

	public void setPedal_pulse_rightfoot(int pedal_pulse_rightfoot) {
		this.pedal_pulse_rightfoot = pedal_pulse_rightfoot;
	}

	public String getPedal_pulse_comments() {
		return pedal_pulse_comments;
	}

	public void setPedal_pulse_comments(String pedal_pulse_comments) {
		this.pedal_pulse_comments = pedal_pulse_comments;
	}

	public int getTemphot_rightfoot() {
		return temphot_rightfoot;
	}

	public void setTemphot_rightfoot(int temphot_rightfoot) {
		this.temphot_rightfoot = temphot_rightfoot;
	}

	public int getTemphot_leftfoot() {
		return temphot_leftfoot;
	}

	public void setTemphot_leftfoot(int temphot_leftfoot) {
		this.temphot_leftfoot = temphot_leftfoot;
	}

	public String getTemphot_comments() {
		return temphot_comments;
	}

	public void setTemphot_comments(String temphot_comments) {
		this.temphot_comments = temphot_comments;
	}

	public int getDependent_rubor_leftfoot() {
		return dependent_rubor_leftfoot;
	}

	public void setDependent_rubor_leftfoot(int dependent_rubor_leftfoot) {
		this.dependent_rubor_leftfoot = dependent_rubor_leftfoot;
	}

	public int getDependent_rubor_rightfoot() {
		return dependent_rubor_rightfoot;
	}

	public void setDependent_rubor_rightfoot(int dependent_rubor_rightfoot) {
		this.dependent_rubor_rightfoot = dependent_rubor_rightfoot;
	}

	public String getDependent_rubor_comments() {
		return dependent_rubor_comments;
	}

	public void setDependent_rubor_comments(String dependent_rubor_comments) {
		this.dependent_rubor_comments = dependent_rubor_comments;
	}

	public int getErythema_leftfoot() {
		return erythema_leftfoot;
	}

	public void setErythema_leftfoot(int erythema_leftfoot) {
		this.erythema_leftfoot = erythema_leftfoot;
	}

	public int getErythema_rightfoot() {
		return erythema_rightfoot;
	}

	public void setErythema_rightfoot(int erythema_rightfoot) {
		this.erythema_rightfoot = erythema_rightfoot;
	}

	public String getErythema_comments() {
		return erythema_comments;
	}

	public void setErythema_comments(String erythema_comments) {
		this.erythema_comments = erythema_comments;
	}

	public static org.apache.log4j.Logger getLog() {
		return log;
	}

	public String getUser_signature() {
		return user_signature;
	}

	public void setUser_signature(String user_signature) {
		this.user_signature = user_signature;
	}
	public int getLeftfoot_image_score() {
		return leftfoot_image_score;
	}

	public void setLeftfoot_image_score(int leftfoot_image_score) {
		this.leftfoot_image_score = leftfoot_image_score;
	}

	public int getRightfoot_image_score() {
		return rightfoot_image_score;
	}

	public void setRightfoot_image_score(int rightfoot_image_score) {
		this.rightfoot_image_score = rightfoot_image_score;
	}

	/***************************************************/

}
