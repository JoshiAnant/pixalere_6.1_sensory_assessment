package com.pixalere.struts.patient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.DiagnosticInvestigationVO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;

public class SensoryAssessmentSetupAction extends Action{

	 private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SensoryAssessmentSetupAction.class);

	    /**
	     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
	     */
	    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
	    		        HttpSession session = request.getSession();
	       
		    
			Integer language = Common.getLanguageIdFromSession(session);
			String locale = Common.getLanguageLocale(language);
						
			PatientServiceImpl pmanager=new PatientServiceImpl(language);
			 
			 if (session.getAttribute("patient_id") == null) {
		      return (mapping.findForward("uploader.null.patient"));
			 }

		        String patient_id = (String) session.getAttribute("patient_id");
		        
				saveToken(request);
				try{
			            request.setAttribute("page", "patient");

			            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
			            SensoryAssessmentVO pp = new SensoryAssessmentVO();
			            pp.setPatient_id(new Integer(patient_id));   
			            pp.setActive(new Integer(1));
			            pp.setDeleted(new Integer(0)); 
			           SensoryAssessmentVO currentProfile = manager.getSensoryAssessment(pp);
			            request.setAttribute("user_signature", (currentProfile != null ? currentProfile.getUser_signature() : Common.getLocalizedString("pixalere.na", locale)));

			            //get components records for this table
			            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
			            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();

			            comp.setFlowchart(com.pixalere.utils.Constants.SENSORY_ASSESSMENT);
			            java.util.Collection components3 = gui.getAllComponents(comp);
			           
			            java.util.Hashtable hashized3 = Common.hashComponents(components3);
			            request.setAttribute("componentFoot", hashized3);

			            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");

			            SensoryAssessmentVO ltmp = new SensoryAssessmentVO();
			            ltmp.setActive(0);
			          
			            ltmp.setPatient_id(new Integer(patient_id));
			            ltmp.setProfessional_id(userVO.getId());
			            
			           SensoryAssessmentVO sensory = manager.getSensoryAssessment(ltmp);
			            if (sensory == null && request.getParameter("pop") != null && ((String) request.getParameter("pop")).equals("1")) {
					          

			            	sensory = currentProfile;
			            } else if (sensory != null) {
			                request.setAttribute("sensory_comments", Common.convertCarriageReturns(sensory.getSensory_comments(), true));

			            }
			            request.setAttribute("sensory", sensory);
			            
			            ListServiceImpl listBD = new ListServiceImpl(language);
			            request.setAttribute("skin_leftfoot", listBD.getLists(LookupVO.SKIN_LEFTFOOT));
			            request.setAttribute("skin_rightfoot", listBD.getLists(LookupVO.SKIN_RIGHTFOOT));
			            request.setAttribute("nails_leftfoot", listBD.getLists(LookupVO.NAILS_LEFTFOOT));
			            request.setAttribute("nails_rightfoot",listBD.getLists(LookupVO.NAILS_RIGHTFOOT));
			            request.setAttribute("deformity_leftfoot",listBD.getLists(LookupVO.DEFORMITY_LEFTFOOT));
			            request.setAttribute("deformity_rightfoot",listBD.getLists(LookupVO.DEFORMITY_RIGHTFOOT));
			            request.setAttribute("footwear_leftfoot",listBD.getLists(LookupVO.FOOTWEAR_LEFTFOOT));
			            request.setAttribute("footwear_rightfoot",listBD.getLists(LookupVO.FOOTWEAR_RIGHTFOOT));
			           
			            request.setAttribute("tempcold_leftfoot",listBD.getLists(LookupVO.TEMPCOLD_LEFTFOOT));
			            request.setAttribute("tempcold_rightfoot",listBD.getLists(LookupVO.TEMPCOLD_RIGHTFOOT));
			            request.setAttribute("temphot_leftfoot",listBD.getLists(LookupVO.TEMPHOT_LEFTFOOT));
			            request.setAttribute("temphot_rightfoot",listBD.getLists(LookupVO.TEMPHOT_RIGHTFOOT));
			            request.setAttribute("rangeofmotion_leftfoot",listBD.getLists(LookupVO.RANGEOFMOTION_LEFTFOOT));
			            request.setAttribute("rangeofmotion_rightfoot",listBD.getLists(LookupVO.RANGEOFMOTION_RIGHTFOOT));
			            request.setAttribute("sensation_monofilament_leftfoot",listBD.getLists(LookupVO.SENSATION_MONOFILAMENT_LEFTFOOT));
			            request.setAttribute("sensation_monofilament_rightfoot",listBD.getLists(LookupVO.SENSATION_MONOFILAMENT_RIGHTFOOT));
			            request.setAttribute("sensation_question_leftfoot",listBD.getLists(LookupVO.SENSATION_QUESTION_LEFTFOOT));
			            request.setAttribute("sensation_question_rightfoot",listBD.getLists(LookupVO.SENSATION_QUESTION_RIGHTFOOT));
			            request.setAttribute("pedal_pulse_leftfoot",listBD.getLists(LookupVO.PEDAL_PULSE_LEFTFOOT));
			            request.setAttribute("pedal_pulse_rightfoot",listBD.getLists(LookupVO.PEDAL_PULSE_RIGHTFOOT));
			            request.setAttribute("dependent_rubor_leftfoot",listBD.getLists(LookupVO.DEPENDENT_RUBOR_LEFTFOOT));
			            request.setAttribute("dependent_rubor_rightfoot",listBD.getLists(LookupVO.DEPENDENT_RUBOR_RIGHTFOOT));
			            request.setAttribute("erythema_leftfoot",listBD.getLists(LookupVO.ERYTHEMA_LEFTFOOT));
			            request.setAttribute("erythema_rightfoot",listBD.getLists(LookupVO.ERYTHEMA_RIGHTFOOT));
					
				}
				catch(ApplicationException e)
				{
					 log.error("An application exception has been raised in PatientProfileSetupAction.perform(): " + e.toString());
			            ActionErrors errors = new ActionErrors();
			            request.setAttribute("exception", "y");
			            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
			            errors.add("exception", new ActionError("pixalere.common.error"));
			            saveErrors(request, errors);
			            return (mapping.findForward("pixalere.error"));
				}
			
				
				 if (patient_id == null) {
			            return (mapping.findForward("uploader.patienthome.success"));
			        } else {
			            return (mapping.findForward("uploader.sensoryassessment.success"));
			        }
				
				
				
			/**********************************************************************************/	
				
				
				
           // return (mapping.findForward("uploader.sensoryassessment.success"));
        
	    }
	
}
