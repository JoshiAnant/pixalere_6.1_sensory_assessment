/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.viewer;

import com.pixalere.utils.Common;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
/**
 * An overview of the entire patient chart for this wound profile.
 * @version 5.1
 * @since 5.1
 * @author travis
 */
public class SummaryReportSetupAction  extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SummaryReportSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        /*try{
            
        }catch(ApplicationException e){
            
        }*/
        return mapping.findForward("");
    }
}
