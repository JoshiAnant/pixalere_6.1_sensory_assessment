package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AssessmentFlowchart extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentFlowchart.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

        AssessmentFlowchartForm viewerForm = (AssessmentFlowchartForm) form;
        HttpSession session = request.getSession();
        
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
        request.setAttribute("alpha_id", viewerForm.getAlpha_id());
        request.setAttribute("page", "assessflowchart");
        if (request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            }else if (request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            }else if (request.getParameter("page").equals("patientflowchart")) {
            return (mapping.findForward("uploader.go.patientflowchart"));
        } else if (request.getParameter("page").equals("woundflowchart")) {
            return (mapping.findForward("uploader.go.woundflowchart"));
        } else if (request.getParameter("page").equals("assessflowchart")) {
            return (mapping.findForward("uploader.go.assessflowchart"));
        } else if (request.getParameter("page").equals("mdiagram")) {
            return (mapping.findForward("uploader.go.mdiagram"));
        } else if (request.getParameter("page").equals("comments")) {
            return (mapping.findForward("uploader.go.comments"));
        } else if (request.getParameter("page").equals("producthistory")) {
            return (mapping.findForward("uploader.go.prodhistory"));
        } else if (request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
        }else if (request.getParameter("page").equals("nursingcareplan")) {
            return (mapping.findForward("uploader.go.nursingcareplan"));
        } else if (request.getParameter("page").equals("admin")) {

            return (mapping.findForward("uploader.go.admin"));
        } else if (request.getParameter("page").equals("bradenflowchart")) {
            return (mapping.findForward("uploader.go.bradenflowchart"));
        } else if (request.getParameter("page").equals("limbflowchart")) {
            return (mapping.findForward("uploader.go.limbflowchart"));
        } else if (request.getParameter("page").equals("footflowchart")) {
            return (mapping.findForward("uploader.go.footflowchart"));
        } else {
            return (mapping.findForward("uploader.aflowchart.success"));
        }
    }
}
