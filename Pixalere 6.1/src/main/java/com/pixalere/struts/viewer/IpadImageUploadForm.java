package com.pixalere.struts.viewer;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.upload.FormFile;

public class IpadImageUploadForm extends ActionForm {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(IpadImageUploadForm.class);

  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    
    return errors;
  }

  public void reset(ActionMapping mapping, HttpServletRequest request) {
  }
  
  
    public FormFile getFile0_1(){
        return file0_1;
    }
    public void setFile0_1(FormFile file0_1){
        this.file0_1=file0_1;
    }
    public FormFile getFile0_0(){
        return file0_0;
    }
    public void setFile0_0(FormFile file0_0){
        this.file0_0=file0_0;
    }
    public FormFile getImageName(){
        return imageName;
    }
    public void setImageName(FormFile imageName){
        this.imageName=imageName;
    }
  private int assessment_id;
  private int alpha_id;
  private int user_id;
  private int PackageFileCount;
  private FormFile file0_1;
  private FormFile imageName;
 private FormFile file0_0;
 private String action;

    /**
     * @return the assessment_id
     */
    public int getAssessment_id() {
        return assessment_id;
    }

    /**
     * @param assessment_id the assessment_id to set
     */
    public void setAssessment_id(int assessment_id) {
        this.assessment_id = assessment_id;
    }

    /**
     * @return the alpha_id
     */
    public int getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(int alpha_id){
        this.alpha_id=alpha_id;
    }
    /**
     * @return the user_id
     */
    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id){
        this.user_id=user_id;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

  

}