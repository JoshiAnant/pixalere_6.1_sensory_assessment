package com.pixalere.struts.viewer;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.PatientReportVO;
import com.pixalere.common.service.PatientReportService;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.utils.PDate;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;

public class NursingCarePlan extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NursingCarePlan.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        NursingCarePlanForm nform = (NursingCarePlanForm) form;
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
        if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {

            return (mapping.findForward("uploader.go.viewer"));
        }
        try {
            if (nform.getPage() != null && nform.getPage().equals("delete_ncp")) {
                if (nform.getId() != 0) {
                    String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale);
                    NursingCarePlanServiceImpl ncpBD = new NursingCarePlanServiceImpl();
                    NursingCarePlanVO c = new NursingCarePlanVO();
                    c.setId(nform.getId());
                    NursingCarePlanVO comment = ncpBD.getNursingCarePlan(c);
                    comment.setDeleted(1);
                    comment.setDelete_signature(user_signature);
                    comment.setDeleted_reason(nform.getDelete_reason());
                    ncpBD.saveNursingCarePlan(comment);

                }
            } else {
                WoundAssessmentLocationServiceImpl walBD = new WoundAssessmentLocationServiceImpl();
                NursingCarePlanServiceImpl ncpBD = new NursingCarePlanServiceImpl();
                WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
                walocation.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                walocation.setDischarge(0);
                Vector<WoundAssessmentLocationVO> locations = walBD.getAllAlphas(walocation);

                Hashtable ncpHash = new Hashtable();
                //cycle alphas.
                int wound_id_int = 0;
                ArrayList<DressingChangeFrequencyVO> dcfs = new ArrayList<DressingChangeFrequencyVO>();
                for (WoundAssessmentLocationVO location : locations) {
                    wound_id_int = location.getWound_id();
                    Vector dcf = Common.parseSelectByAlpha(location.getAlpha(), nform.getDressing_change_frequency(), locale);
                    int dcf_access = 0;
                    if (dcf != null && dcf.size() != 0) {

                        dcf_access = Integer.parseInt(dcf.get(0) + "");
                    }
                    if (dcf_access != 0) {

                        DressingChangeFrequencyVO f = new DressingChangeFrequencyVO();
                        f.setActive(1);
                        f.setWound_profile_type_id(location.getWound_profile_type_id());
                        f.setAssessment_id(-1);
                        f.setPatient_id(new Integer(patient_id));
                        f.setWound_id(location.getWound_id());
                        f.setProfessional_id(userVO.getId());
                        f.setDcf(new Integer(dcf_access));
                        f.setAlpha_id(location.getId());
                        f.setCreated_on(new Date());
                        f.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                        if (request.getParameter("commit") != null && !request.getParameter("commit").equals("")) {
                            //update timestamp for patient.. used to generate report for PCI ( or authority's document library)
                            PatientReportService prservice = new PatientReportService();
                            if (patient_id != null && !patient_id.equals("")) {
                                PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                                if (savedPatient != null) {
                                    savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                    savedPatient.setActive(1);
                                    prservice.savePatientReportUpdate(savedPatient);
                                } else {
                                    savedPatient = new PatientReportVO();
                                    savedPatient.setPatient_id(new Integer(patient_id));
                                    savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                    savedPatient.setActive(1);
                                    prservice.savePatientReportUpdate(savedPatient);
                                }
                            }
                            ncpBD.saveDressingChangeFrequency(f);
                        } else {
                            dcfs.add(f);

                        }
                    }
                }
                if (request.getParameter("commit") != null && !request.getParameter("commit").equals("")) {
                } else {

                    ncpHash.put("dcf", dcfs);

                }
                NursingCarePlanVO oldNCP = new NursingCarePlanVO();
                oldNCP.setWound_profile_type_id(new Integer(wound_profile_type_id));
                oldNCP.setActive(new Integer(1));
                oldNCP.setDeleted(0);
                oldNCP = ncpBD.getNursingCarePlan(oldNCP);
                NursingCarePlanVO newncp = new NursingCarePlanVO();

                newncp.setActive(1);
                newncp.setAssessment_id(new Integer(-1));
                newncp.setPatient_id(new Integer((String) patient_id));
                newncp.setWound_profile_type_id(walocation.getWound_profile_type_id());
                newncp.setWound_id(wound_id_int);
                newncp.setAs_needed(nform.getAs_needed());
                newncp.setProfessional_id(userVO.getId());
                newncp.setCreated_on(new Date());
                newncp.setVisit_frequency(nform.getVisit_freq());
                newncp.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                newncp.setBody(Common.convertCarriageReturns(nform.getBody(), false));
                //if(){adng ncp_id session var
                //  newncp.setId(nform.getId());
                //}

                if ((nform.getBody() != null && !(nform.getBody().replaceAll("<br>", "")).equals("") && (oldNCP == null || oldNCP.getBody() == null)) || (nform.getBody() != null && oldNCP != null && oldNCP.getBody() != null && !oldNCP.getBody().equals(nform.getBody()))) {
                    if (!(nform.getBody().replaceAll("<br>", "")).equals("")) {
                        if (request.getParameter("commit") != null && !request.getParameter("commit").equals("")) {
                            ncpBD.saveNursingCarePlan(newncp);
                            //update timestamp for patient.. used to generate report for PCI ( or authority's document library)
                            PatientReportService prservice = new PatientReportService();
                            if (patient_id != null && !patient_id.equals("")) {
                                PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                                if (savedPatient != null) {
                                    savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                    savedPatient.setActive(1);
                                    prservice.savePatientReportUpdate(savedPatient);
                                } else {
                                    savedPatient = new PatientReportVO();
                                    savedPatient.setPatient_id(new Integer(patient_id));
                                    savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                    savedPatient.setActive(1);
                                    prservice.savePatientReportUpdate(savedPatient);
                                }
                            }
                        } else {
                            ncpHash.put("ncp", newncp.getBody());
                            ncpHash.put("vf", newncp.getVisit_frequency());
                            ncpHash.put("as", newncp.getAs_needed());
                        }
                    } else if ((nform.getVisit_freq() != 0 && oldNCP == null) || (nform.getVisit_freq() != 0 && oldNCP != null && nform.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue())) || (nform.getAs_needed() != 0 && oldNCP != null && nform.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                        newncp.setBody(oldNCP.getBody());
                        if (request.getParameter("commit") != null && !request.getParameter("commit").equals("")) {
                            ncpBD.saveNursingCarePlan(newncp);
                            //update timestamp for patient.. used to generate report for PCI ( or authority's document library)
                            PatientReportService prservice = new PatientReportService();
                            if (patient_id != null && !patient_id.equals("")) {
                                PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                                if (savedPatient != null) {
                                    savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                    savedPatient.setActive(1);
                                    prservice.savePatientReportUpdate(savedPatient);
                                } else {
                                    savedPatient = new PatientReportVO();
                                    savedPatient.setPatient_id(new Integer(patient_id));
                                    savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                    savedPatient.setActive(1);
                                    prservice.savePatientReportUpdate(savedPatient);
                                }
                            }
                        } else {
                            ncpHash.put("ncp", newncp.getBody());
                            ncpHash.put("vf", newncp.getVisit_frequency());
                            ncpHash.put("as", newncp.getAs_needed());
                        }

                    }

                } else if ((nform.getVisit_freq() != 0 && oldNCP == null) || (nform.getVisit_freq() != 0 && oldNCP != null && nform.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue())) || (nform.getAs_needed() != 0 && oldNCP != null && nform.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                    if (nform.getBody().equals("") && oldNCP != null) {
                        newncp.setBody(oldNCP.getBody());
                    }
                    if (request.getParameter("commit") != null && !request.getParameter("commit").equals("")) {
                        ncpBD.saveNursingCarePlan(newncp);
                        //update timestamp for patient.. used to generate report for PCI ( or authority's document library)
                        PatientReportService prservice = new PatientReportService();
                        if (patient_id != null && !patient_id.equals("")) {
                            PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                            if (savedPatient != null) {
                                savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                savedPatient.setActive(1);
                                prservice.savePatientReportUpdate(savedPatient);
                            } else {
                                savedPatient = new PatientReportVO();
                                savedPatient.setPatient_id(new Integer(patient_id));
                                savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                savedPatient.setActive(1);
                                prservice.savePatientReportUpdate(savedPatient);
                            }
                        }
                    } else {
                        ncpHash.put("ncp", newncp.getBody());
                        ncpHash.put("vf", newncp.getVisit_frequency());
                        ncpHash.put("as", newncp.getAs_needed());
                    }

                }
                if (request.getParameter("commit") != null && !request.getParameter("commit").equals("")) {
                    session.removeAttribute("ncp");
                } else {

                    session.setAttribute("ncp", ncpHash);
                }
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in TreatmentSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        if (request.getParameter("page") != null && request.getParameter("page").
                equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("visitsflowchart")) {
            return (mapping.findForward("uploader.go.visits"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page").equals("examflowchart")) {
            return (mapping.findForward("uploader.go.examflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("diagflowchart")) {
            return (mapping.findForward("uploader.go.diagflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("patientflowchart")) {
            return (mapping.findForward("uploader.go.patientflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("patientflowchart")) {
            return (mapping.findForward("uploader.go.patientflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("woundflowchart")) {
            return (mapping.findForward("uploader.go.woundflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("assessflowchart")) {
            return (mapping.findForward("uploader.go.assessflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("mdiagram")) {
            return (mapping.findForward("uploader.go.mdiagram"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("comments")) {
            return (mapping.findForward("uploader.go.comments"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("producthistory")) {
            return (mapping.findForward("uploader.go.prodhistory"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("nursingcareplan")) {
            return (mapping.findForward("uploader.go.nursingcareplan"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("bradenflowchart")) {
            return (mapping.findForward("uploader.go.bradenflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("limbflowchart")) {
            return (mapping.findForward("uploader.go.limbflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").
                equals("footflowchart")) {
            return (mapping.findForward("uploader.go.footflowchart"));
        } else {
            return (mapping.findForward("viewer.ncp.success"));
        }
    }
}
