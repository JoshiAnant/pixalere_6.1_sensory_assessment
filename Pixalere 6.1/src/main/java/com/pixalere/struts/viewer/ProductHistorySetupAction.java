package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.assessment.bean.AssessmentProductVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.utils.*;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.service.WoundServiceImpl;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.Vector;
import java.util.Collection;
import java.util.ArrayList;

public class ProductHistorySetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductHistorySetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {
            request.setAttribute("page", "producthistory");
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {
                return (mapping.findForward("uploader.go.viewer"));
            }
            WoundAssessmentLocationServiceImpl wal = new WoundAssessmentLocationServiceImpl();
            AssessmentServiceImpl waDAO = new AssessmentServiceImpl(language);
            com.pixalere.wound.service.WoundServiceImpl woundBD = new com.pixalere.wound.service.WoundServiceImpl(language);
            
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

            //Display wound profile on page
            WoundProfileVO woundVO = null;
            WoundServiceImpl manager = new WoundServiceImpl(language);
            WoundProfileTypeVO wpt = new WoundProfileTypeVO();
            wpt.setId(new Integer((String) wound_profile_type_id));
            WoundProfileTypeVO location = (WoundProfileTypeVO) manager.getWoundProfileType(wpt);

            String showalphas = "";
            if ((String) wound_profile_type_id != null && ((String) wound_profile_type_id).length() > 0) {
                WoundProfileVO criteria = new WoundProfileVO();
                criteria.setWound_id(location.getWound_id());
                criteria.setActive(new Integer(1));
                criteria.setCurrent_flag(new Integer(1));
                woundVO = (WoundProfileVO) woundBD.getWoundProfile(criteria);
                if(woundVO!=null){
                    request.setAttribute("wound_profile", woundVO.getWound().getWound_location_detailed());
                }
            }

            String alpha_id = (String) request.getParameter("alpha_id");
            String alpha_type = (String) request.getParameter("alpha_type");
            WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
            walocation.setPatient_id(new Integer((String) patient_id));
            walocation.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
            if (alpha_id == null) {
                WoundAssessmentLocationVO location2 = (WoundAssessmentLocationVO) wal.getAlpha(walocation, false, false, null);
                if (location != null && location2 !=null) {
                    alpha_id = location2.getId().toString();
                    alpha_type = location2.getWound_profile_type().getAlpha_type();
                    request.setAttribute("selected_alpha", location2.getAlpha());
                }
            }
            if (alpha_id != null) {
                WoundAssessmentLocationVO wal2 = new WoundAssessmentLocationVO();
                wal2.setId(new Integer(alpha_id));
                WoundAssessmentLocationVO loco = (WoundAssessmentLocationVO) wal.getAlpha(wal2);
                WoundAssessmentLocationVO tmp = new WoundAssessmentLocationVO();
                tmp.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                Collection alphas = wal.getAllAlphas(tmp);

                ArrayList alphaslist = new ArrayList();
                alphaslist.addAll(alphas);

                String strAlphaName = "";
                if (loco.getWound_profile_type().getAlpha_type().indexOf("A") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare",locale) + " " + loco.getAlpha();
                }
                if (loco.getWound_profile_type().getAlpha_type().indexOf("D") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_drain",locale) + " " + loco.getAlpha();
                }
                if (loco.getWound_profile_type().getAlpha_type().indexOf("I") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_incisiontag",locale);
                }
                if (loco.getWound_profile_type().getAlpha_type().indexOf("O") != -1) {
                    if (loco.getAlpha().indexOf("ostu") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_urinalstoma",locale);
                    }
                    if (loco.getAlpha().indexOf("ostf") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_fecalstoma",locale);
                    }
                }
                if (loco.getWound_profile_type().getAlpha_type().indexOf("B") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_burn",locale);
                }
                if (loco.getWound_profile_type().getAlpha_type().indexOf("S") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_skin",locale);
                }
                request.setAttribute("alpha_str", strAlphaName);
                request.setAttribute("alphas", Common.getSortedAlphaList(alphaslist));
                request.setAttribute("selected_alpha", loco.getAlpha());

            }
            request.setAttribute("alpha_type", alpha_type);
            request.setAttribute("alpha_id", alpha_id);

            request.setAttribute("populate_alphas", showalphas);


            //show all product history in history list box*************************
            //AssessmentEachwoundDAO assessmentDAO = new AssessmentEachwoundDAO();
            ArrayList productHistoryList = new ArrayList();
            Collection<AbstractAssessmentVO> assessmentHistory = null;
            if (alpha_id != null && (String) wound_profile_type_id != null && ((String) wound_profile_type_id).length() > 0) {
                AbstractAssessmentVO ab = new AbstractAssessmentVO();
                ab.setAlpha_id(new Integer(alpha_id));
                ab.setWound_profile_type_id(new Integer(wound_profile_type_id));
                ab.setDeleted(0);
                assessmentHistory = (Collection) waDAO.findAllPastAssessmentsByCriteria(ab);
                ProfessionalServiceImpl profBD = new ProfessionalServiceImpl();
                Integer intBasicProductCategory = new Integer(Common.getConfig("basicProductCategory"));
                for (AbstractAssessmentVO e : assessmentHistory) {
                    WoundAssessmentVO waVO = e.getWoundAssessment();
                    ProfessionalVO profVO = waVO.getProfessional();
                    PDate pdate = new PDate(profVO != null ? profVO.getTimezone() : Constants.TIMEZONE);
                    String timestamp = pdate.getDateStringWithHour(waVO.getCreated_on(),Common.getLanguageLocale(language));
                    AssessmentProductVO tp2 = new AssessmentProductVO();
                    tp2.setAssessment_id(e.getAssessment_id());
                    tp2.setAlpha_id(e.getAlpha_id());
                    Collection<AssessmentProductVO> product_history = waDAO.getAllProducts(tp2);
                    int firstProduct = 0;
                    for (AssessmentProductVO prod : product_history) {
                        ProductsVO item = prod.getProduct();
                        if (!prod.getProduct_id().equals(new Integer(-1)) && item != null && (Common.getConfig("showAllProductsInHistory").equals("1") || !prod.getProduct().getCategory_id().equals(intBasicProductCategory))) {
                            if (firstProduct == 0) {
                                if (profVO == null) {
                                    productHistoryList.add(Common.getLocalizedString("pixalere.login.form.assess", locale)+": " + timestamp + " :");
                                } else {
                                    productHistoryList.add(Common.getLocalizedString("pixalere.login.form.assess", locale)+": " + timestamp + " :" + profVO.getPosition().getTitle() + " #" + profVO.getId());
                                }
                            }
                            productHistoryList.add("" + prod.getQuantity() + " " + item.getTitle());
                            firstProduct++;
                        } else if (prod.getProduct() == null ||  (Common.getConfig("showAllProductsInHistory").equals("1") || !prod.getProduct().getCategory_id().equals(intBasicProductCategory))) {
                            //product is an OTHER so need to parse that out and add it.
                            if (firstProduct == 0) {
                                if (profVO == null) {
                                    productHistoryList.add(Common.getLocalizedString("pixalere.login.form.assess", locale)+": " + timestamp + " :");
                                } else {
                                    productHistoryList.add(Common.getLocalizedString("pixalere.login.form.assess", locale)+": " + timestamp + " :" + profVO.getPosition().getTitle() + " #" + profVO.getId());
                                }
                            }
                            productHistoryList.add("" + prod.getQuantity() + " " + prod.getOther());
                            firstProduct++;
                        }
                    }
                }
                request.setAttribute("product_history", productHistoryList);
            }

            // populate wound profile dropdown
            
            Vector woundProfilesDrop = manager.getWoundProfilesDropdown(Integer.parseInt((String) patient_id), false, userVO.getId().intValue());
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);
            if(woundVO!=null){
                request.setAttribute("bluemodel", woundVO.getWound().getImage_name());
            
            if (woundVO!=null && woundVO.getWound() == null || woundVO.getWound() ==null) {
                request.setAttribute("bluemodelrows", 12);
            } else {
                request.setAttribute("bluemodelrows", woundVO.getWound().getLocation_image().getRows());
            }
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ProductHistorySetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        log.info("**** TreatmentSetupAction.perform: End of Treatment");
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("visitsflowchart")) {
            return (mapping.findForward("uploader.go.visits"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            }else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        }else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("patientflowchart")) {
            return (mapping.findForward("uploader.go.patientflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundflowchart")) {
            return (mapping.findForward("uploader.go.woundflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assessflowchart")) {
            return (mapping.findForward("uploader.go.assessflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("mdiagram")) {
            return (mapping.findForward("uploader.go.mdiagram"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("comments")) {
            return (mapping.findForward("uploader.go.comments"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("nursingcareplan")) {
            return (mapping.findForward("uploader.go.nursingcareplan"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("bradenflowchart")) {
            return (mapping.findForward("uploader.go.bradenflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbflowchart")) {
            return (mapping.findForward("uploader.go.limbflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("footflowchart")) {
            return (mapping.findForward("uploader.go.footflowchart"));
        } else {
            return (mapping.findForward("viewer.treatment.success"));
        }
    }
}

