package com.pixalere.struts.viewer.edit;

import com.pixalere.utils.*;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.assessment.service.*;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.assessment.bean.*;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

public class AssessmentAmend extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentAmend.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        AssessmentAmendForm assessmentAmendForm = (AssessmentAmendForm) form;
        Collection assessments_per_caretype = null;        
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

        try {
            PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
            String strAuditLog = assessmentAmendForm.getAudit();
            int intId = Integer.parseInt(assessmentAmendForm.getId());
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            GUIServiceImpl guiservice = new GUIServiceImpl();
            WoundAssessmentLocationServiceImpl location_manager = new WoundAssessmentLocationServiceImpl();
            String deleted_reason = assessmentAmendForm.getDeleted_reason();
            
            AbstractAssessmentVO assessment_details = null;
            if (assessmentAmendForm.getAction().equals("delete")) {

                if (assessmentAmendForm.getType().equals(Constants.WOUND_ASSESSMENT)) {
                    AssessmentEachwoundVO searchVO1 = new AssessmentEachwoundVO();
                    searchVO1.setId(intId);
                    assessment_details =  manager.getAssessment(searchVO1);
                    if(assessment_details!=null){
                        AssessmentEachwoundVO searchVO2 = new AssessmentEachwoundVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                        if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("woundClosed"))) {
                            location_manager.reopenAlpha(assessment_details.getAlpha_id());

                        }
                        assessment_details.setDeleted(1);
                        assessment_details.setDeleted_reason(deleted_reason);
                        assessment_details.setDelete_signature(pdate.getProfessionalSignature(new Date(),userVO,locale));
                        manager.saveAssessment(assessment_details);
                    }
                }
                if (assessmentAmendForm.getType().equals(Constants.OSTOMY_ASSESSMENT)) {
                    AssessmentOstomyVO searchVO1 = new AssessmentOstomyVO();
                    searchVO1.setId(intId);
                    assessment_details =  manager.getAssessment(searchVO1);


                    if(assessment_details!=null){
                        AssessmentEachwoundVO searchVO2 = new AssessmentEachwoundVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                        if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("woundClosed"))) {
                            location_manager.reopenAlpha(assessment_details.getAlpha_id());
                        }
                        assessment_details.setDeleted(1);
                        assessment_details.setDeleted_reason(deleted_reason);
                        assessment_details.setDelete_signature(pdate.getProfessionalSignature(new Date(),userVO,locale));
                        manager.saveAssessment(assessment_details);
                    }
                }
                if (assessmentAmendForm.getType().equals(Constants.INCISION_ASSESSMENT)) {
                    AssessmentIncisionVO searchVO1 = new AssessmentIncisionVO();
                    searchVO1.setId(intId);
                    assessment_details =  manager.getAssessment(searchVO1);
                    if(assessment_details!=null){
                        AssessmentIncisionVO searchVO2 = new AssessmentIncisionVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                        if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("incisionClosed"))) {
                            location_manager.reopenAlpha(assessment_details.getAlpha_id());

                        }
                        assessment_details.setDeleted(1);
                        assessment_details.setDeleted_reason(deleted_reason);
                        assessment_details.setDelete_signature(pdate.getProfessionalSignature(new Date(),userVO,locale));
                        manager.saveAssessment(assessment_details);
                    }
                }
                if (assessmentAmendForm.getType().equals(Constants.DRAIN_ASSESSMENT)) {
                    AssessmentDrainVO searchVO1 = new AssessmentDrainVO();
                    searchVO1.setId(intId);
                    assessment_details =  manager.getAssessment(searchVO1);
                    if(assessment_details!=null){
                        AssessmentDrainVO searchVO2 = new AssessmentDrainVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                        if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("drainClosed"))) {
                            location_manager.reopenAlpha(assessment_details.getAlpha_id());
                        }
                        assessment_details.setDeleted(1);
                        assessment_details.setDeleted_reason(deleted_reason);
                        assessment_details.setDelete_signature(pdate.getProfessionalSignature(new Date(),userVO,locale));
                        manager.saveAssessment(assessment_details);
                    }
                }
                if (assessmentAmendForm.getType().equals(Constants.SKIN_ASSESSMENT)) {
                    AssessmentSkinVO searchVO1 = new AssessmentSkinVO();
                    searchVO1.setId(intId);
                    assessment_details =  manager.getAssessment(searchVO1);


                    if(assessment_details!=null){
                        AssessmentSkinVO searchVO2 = new AssessmentSkinVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                        if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("skinClosed"))) {
                            location_manager.reopenAlpha(assessment_details.getAlpha_id());
                        }
                        assessment_details.setDeleted(1);
                        assessment_details.setDeleted_reason(deleted_reason);
                        assessment_details.setDelete_signature(pdate.getProfessionalSignature(new Date(),userVO,locale));
                        manager.saveAssessment(assessment_details);
                    }
                }
                if (assessmentAmendForm.getType().equals(Constants.BURN_ASSESSMENT)) {
                    AssessmentBurnVO searchVO1 = new AssessmentBurnVO();
                    searchVO1.setId(intId);
                    assessment_details =  manager.getAssessment(searchVO1);


                    if(assessment_details!=null){
                        AssessmentBurnVO searchVO2 = new AssessmentBurnVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                        if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("burnClosed"))) {
                            location_manager.reopenAlpha(assessment_details.getAlpha_id());
                        }
                        assessment_details.setDeleted(1);
                        assessment_details.setDeleted_reason(deleted_reason);
                        assessment_details.setDelete_signature(pdate.getProfessionalSignature(new Date(),userVO,locale));
                        manager.saveAssessment(assessment_details);
                    }
                }

                if(assessment_details!=null){

                    if (assessments_per_caretype.size() == 1) {
                        // There's only one assessment for this caretype, so delete the comments etc too
                        AssessmentCommentsServiceImpl comments_manager = new AssessmentCommentsServiceImpl();
                        AssessmentCommentsVO searchVO4 = new AssessmentCommentsVO();
                        searchVO4.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO4.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        AssessmentCommentsVO assessment_comments = (AssessmentCommentsVO) comments_manager.getComment(searchVO4);
                        if(assessment_comments!=null){
                            assessment_comments.setDeleted(1);
                            assessment_comments.setDelete_reason(deleted_reason);
                            assessment_comments.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));

                            comments_manager.saveComment(assessment_comments);
                        }
                        
                        ReferralsTrackingServiceImpl referrals_manager = new ReferralsTrackingServiceImpl(language);
                        ReferralsTrackingVO searchVO6 = new ReferralsTrackingVO();
                        searchVO6.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO6.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        Collection<ReferralsTrackingVO> assessment_referrals = referrals_manager.getAllReferralsByCriteria(searchVO6);
                        if (assessment_referrals != null) {
                            for (Iterator referrals = assessment_referrals.iterator(); referrals.hasNext();) {
                                ReferralsTrackingVO assessment_referral = (ReferralsTrackingVO) referrals.next();
                                if(assessment_referral!=null){
                                    assessment_referral.setActive(0);
                                    assessment_referral.setCurrent(0);
                                    referrals_manager.saveReferralsTracking(assessment_referral);
                                }
                            }
                        }

                        TreatmentCommentsServiceImpl treatmentcomment_manager = new TreatmentCommentsServiceImpl();
                        TreatmentCommentVO searchVO7 = new TreatmentCommentVO();
                        searchVO7.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO7.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        TreatmentCommentVO assessment_treatmentcomments = (TreatmentCommentVO) treatmentcomment_manager.getTreatmentComment(searchVO7);

                        if (assessment_treatmentcomments != null) {
                            assessment_treatmentcomments.setDeleted(1);
                            assessment_treatmentcomments.setDeleted_reason(deleted_reason);
                            assessment_treatmentcomments.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            treatmentcomment_manager.saveTreatmentComment(assessment_treatmentcomments);

                        }


                        NursingCarePlanServiceImpl ncp_manager = new NursingCarePlanServiceImpl();
                        NursingCarePlanVO searchVO8 = new NursingCarePlanVO();
                        searchVO8.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO8.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        NursingCarePlanVO assessment_ncp = (NursingCarePlanVO) ncp_manager.getNursingCarePlan(searchVO8);
                        
                        if (assessment_ncp != null) {
                            assessment_ncp.setDeleted(1);
                            assessment_ncp.setDeleted_reason(deleted_reason);
                            assessment_ncp.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            ncp_manager.saveNursingCarePlan(assessment_ncp);
                        }


                        DressingChangeFrequencyVO search09 = new DressingChangeFrequencyVO();
                        search09.setAssessment_id(assessment_details.getAssessment_id());
                        search09.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        DressingChangeFrequencyVO assessment_df = (DressingChangeFrequencyVO) ncp_manager.getDressingChangeFrequency(search09);
                        
                        /*if (assessment_df != null) {
                            assessment_df.setDeleted(1);
                            assessment_df.setDeleted_reason(deleted_reason);
                            assessment_df.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO));
                            ncp_manager.saveDressingChangeFrequency(assessment_df);
                        }*/

                    }
                }
                


                /*Collection col = manager.getAllAssessments(assessment_details.getAssessment_id());
                    if(col.size()==0) // If no more assessment records left, delete wound_assessment as well
                        {
                        WoundAssessmentServiceImpl woundassessment_manager = new WoundAssessmentServiceImpl();
                        woundassessment_manager.removeAssessment(assessment_details.getWoundAssessment(),-1);
                        } */

            }else if(assessmentAmendForm.getAction().equals("move")){
                if(assessmentAmendForm.getAlpha_id_to()>0){
                    manager.moveAssessment(assessmentAmendForm.getAlpha_id_to(), assessmentAmendForm.getType(), assessmentAmendForm.getMoved_reason(), intId,userVO);
                }
            }else if(assessmentAmendForm.getAction().equals("backdate")){
                if (assessmentAmendForm.getType().equals(Constants.WOUND_PROFILE_TYPE)) {
                    AssessmentEachwoundVO searchVO1 = new AssessmentEachwoundVO();
                    searchVO1.setId(intId);
                     assessment_details =  manager.getAssessment(searchVO1);
                    if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("woundClosed"))) {
                        strAuditLog = strAuditLog + "Backdating an Assessment";
                    }
                    //assessment_details.setBackdated(1);
                    //assessment_details.setBackdated_creation_date(assessment_details.getLast_update());
                    //assessment_details.setBackdated_reason(deleted_reason);
                    //set date
                    assessment_details.setCreated_on(pdate.getDate(assessmentAmendForm.getLastupdate_month(),assessmentAmendForm.getLastupdate_day(),assessmentAmendForm.getLastupdate_year()));
                    manager.saveAssessment(assessment_details);
                }else if (assessmentAmendForm.getType().equals(Constants.OSTOMY_PROFILE_TYPE)) {
                    AssessmentOstomyVO searchVO1 = new AssessmentOstomyVO();
                    searchVO1.setId(intId);
                    assessment_details = manager.getAssessment(searchVO1);
                    if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("ostomyClosed"))) {
                        strAuditLog = strAuditLog + "Backdating an Assessment";
                    }
                    //assessment_details.setBackdated(1);
                    //assessment_details.setBackdated_creation_date(assessment_details.getLast_update());
                    //assessment_details.setBackdated_reason(deleted_reason);
                    //set date
                    assessment_details.setCreated_on(pdate.getDate(assessmentAmendForm.getLastupdate_month(),assessmentAmendForm.getLastupdate_day(),assessmentAmendForm.getLastupdate_year()));
                    manager.saveAssessment(assessment_details);
                }else if (assessmentAmendForm.getType().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                    AssessmentDrainVO searchVO1 = new AssessmentDrainVO();
                    searchVO1.setId(intId);
                     assessment_details = manager.getAssessment(searchVO1);
                    if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("drainClosed"))) {
                        strAuditLog = strAuditLog + "Backdating an Assessment";
                    }
                    //assessment_details.setBackdated(1);
                    //assessment_details.setBackdated_creation_date(assessment_details.getLast_update());
                    //assessment_details.setBackdated_reason(deleted_reason);
                    //set date
                    assessment_details.setCreated_on(pdate.getDate(assessmentAmendForm.getLastupdate_month(),assessmentAmendForm.getLastupdate_day(),assessmentAmendForm.getLastupdate_year()));
                    manager.saveAssessment(assessment_details);
                }else if (assessmentAmendForm.getType().equals(Constants.INCISION_PROFILE_TYPE)) {
                    AssessmentIncisionVO searchVO1 = new AssessmentIncisionVO();
                    searchVO1.setId(intId);
                     assessment_details = manager.getAssessment(searchVO1);
                    if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("incisionClosed"))) {
                        strAuditLog = strAuditLog + "Backdating an Assessment";
                    }
                    //assessment_details.setBackdated(1);
                    //assessment_details.setBackdated_creation_date(assessment_details.getLast_update());
                    //assessment_details.setBackdated_reason(deleted_reason);
                    //set date
                    assessment_details.setCreated_on(pdate.getDate(assessmentAmendForm.getLastupdate_month(),assessmentAmendForm.getLastupdate_day(),assessmentAmendForm.getLastupdate_year()));
                    manager.saveAssessment(assessment_details);
                }else if (assessmentAmendForm.getType().equals(Constants.BURN_PROFILE_TYPE)) {
                    AssessmentBurnVO searchVO1 = new AssessmentBurnVO();
                    searchVO1.setId(intId);
                    assessment_details = manager.getAssessment(searchVO1);
                    if (Integer.toString(assessment_details.getStatus()).equals(Common.getConfig("burnClosed"))) {
                        strAuditLog = strAuditLog + "Backdating an Assessment";
                    }
                    //assessment_details.setBackdated(1);
                    //assessment_details.setBackdated_creation_date(assessment_details.getLast_update());
                    //assessment_details.setBackdated_reason(deleted_reason);
                    //set date
                    assessment_details.setCreated_on(pdate.getDate(assessmentAmendForm.getLastupdate_month(),assessmentAmendForm.getLastupdate_day(),assessmentAmendForm.getLastupdate_year()));
                    manager.saveAssessment(assessment_details);
                }
            }

        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (Exception e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        return (mapping.findForward("uploader.go.assessflowchart"));

    }
}