/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.viewer;

import com.pixalere.struts.admin.*;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.struts.reporting.ParameterAction;
import com.pixalere.struts.reporting.ParameterForm;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author User
 */
public class PatientConsentRevokeAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParameterAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String patient_id = (String) session.getAttribute("patient_id");
        
        if (patient_id == null) {
            return (mapping.findForward("uploader.patientconsent.error"));
        }
        
        try {
            PatientServiceImpl patientService = new PatientServiceImpl(language);
            PatientAccountVO patient = patientService.getPatient(new Integer((String) patient_id));

            if (patient == null) {
                return (mapping.findForward("uploader.patientconsent.error"));
            }

            // Clear up all the consent fields
            patient.setConsent_information(0);
            patient.setConsent_services(0);
            patient.setConsent_verbal(null);
            patient.setPatient_consent_name(null);
            patient.setPatient_consent_sig(null);
            patient.setPatient_consent_blurb(null);
            patient.setConsent_date(null);

            // Update patient
            patientService.updatePatient(patient);
        } catch (Exception e) {
            System.out.println("Error revoking consent:" + e.toString());
            return (mapping.findForward("uploader.patientconsent.error"));
        }
        
        return (mapping.findForward("uploader.patientconsent.revoked"));
    }
    
}
