package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.dao.PatientProfileDAO;
import com.pixalere.patient.bean.PhysicalExamVO;
import com.pixalere.guibeans.RowData;
import com.pixalere.utils.Constants;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Collection;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;

public class ExamFlowchartSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ExamFlowchartSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        
        try {
            ProfessionalVO currentProfessional = (ProfessionalVO) session.getAttribute("userVO");
            request.setAttribute("page", "examflowchart");
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if (request.getParameter("page") != null && request.getParameter("page").equals("comments")) {
                return (mapping.findForward("uploader.go.comments"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("patientflowchart")) {
                return (mapping.findForward("uploader.go.patientflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            } else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            }else if (request.getParameter("page") != null && request.getParameter("page").equals("woundflowchart")) {
                return (mapping.findForward("uploader.go.woundflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("assessflowchart")) {
                return (mapping.findForward("uploader.go.assessflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("mdiagram")) {
                return (mapping.findForward("uploader.go.mdiagram"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("producthistory")) {
                return (mapping.findForward("uploader.go.prodhistory"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("nursingcareplan")) {
                return (mapping.findForward("uploader.go.nursingcareplan"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("bradenflowchart")) {
                return (mapping.findForward("uploader.go.bradenflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbflowchart")) {
                return (mapping.findForward("uploader.go.limbflowchart"));
            }
            PhysicalExamVO vo = new PhysicalExamVO();
            vo.setPatient_id(new Integer((String) patient_id));
            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            String id = "";
            request.setAttribute("act", "ExamFlowchartSetup.do");
            int limit = 3;
            if (request.getParameter("limit") != null) {
                limit = Integer.parseInt((String) request.getParameter("limit"));
            }
            PhysicalExamVO crit = new PhysicalExamVO();
            crit.setPatient_id(new Integer((String) patient_id));
            crit.setActive(1);
            Collection<PhysicalExamVO> items = null;
            if (limit != 0) {
                items = manager.getAllExams(crit, limit);
            }
            //if item selected from dropwn pretend to list
            if (request.getParameter("id") != null) {
                id = (String) request.getParameter("id");
                request.setAttribute("selectedid", (String) request.getParameter("id"));
                PhysicalExamVO criteria = new PhysicalExamVO();
                criteria.setId(new Integer(id));
                PhysicalExamVO item = manager.getExam(criteria, Constants.SHOW_DELETED);
                Collection<PhysicalExamVO> tmp = new ArrayList();
                //if(tmp!=null){tmp.add(item);}
                int count = 0;
                for (PhysicalExamVO pp : items) {
                    count++;
                    if (count != items.size()) {
                        tmp.add(pp);
                    }
                }
               if(items.size()>2){
                    tmp.add(item);
                    items = tmp;
                }

            }

            RowData[] results = manager.getAllExamsForFlowchart(items, currentProfessional, true);
            request.setAttribute("results", results);

            request.setAttribute("error_class", "AssessmentErrorSetup");
            PatientProfileDAO dao = new PatientProfileDAO();
            request.setAttribute("dropdown", dao.getAllPhysicalExamsSignatures(new Integer((String) patient_id).intValue(), limit));

            // populate wound profile dropdown
            com.pixalere.wound.service.WoundServiceImpl wp = new com.pixalere.wound.service.WoundServiceImpl(language);
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
            Vector woundProfilesDrop = wp.getWoundProfilesDropdown(Integer.parseInt((String) patient_id), false, userVO.getId().intValue());
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);
            // request.setAttribute("page", "patientflowchart");

        } catch (com.pixalere.common.DataAccessException e) {
            log.error("An application exception has been raised in LogAuditAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientFlowchartSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null) {
            if (request.getParameter("page").equals("patient")) {
                return (mapping.findForward("uploader.go.patient"));
            } else if (request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            } else if (request.getParameter("page").equals("woundprofiles")) {
                return (mapping.findForward("uploader.go.profile"));
            } else if (request.getParameter("page").equals("assess")) {
                return (mapping.findForward("uploader.go.assess"));
            } else if (request.getParameter("page").equals("treatment")) {
                return (mapping.findForward("uploader.go.treatment"));
            } else if (request.getParameter("page").equals("viewer")) {
                return (mapping.findForward("uploader.go.viewer"));
            } else if (request.getParameter("page").equals("summary")) {
                return (mapping.findForward("uploader.go.summary"));
            } else if (request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            } else if (request.getParameter("page").equals("reporting")) {
                return (mapping.findForward("uploader.go.reporting"));
            } else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            }else if (request.getParameter("page").equals("limbflowchart")) {
                return (mapping.findForward("uploader.go.limbflowchart"));
            } else if (request.getParameter("page").equals("patientflowchart")) {
                return (mapping.findForward("uploader.go.patientflowchart"));
            } else if (request.getParameter("page").equals("woundflowchart")) {
                return (mapping.findForward("uploader.go.woundflowchart"));
            } else if (request.getParameter("page").equals("assessflowchart")) {
                return (mapping.findForward("uploader.go.assessflowchart"));
            } else if (request.getParameter("page").equals("mdiagram")) {
                return (mapping.findForward("uploader.go.mdiagram"));
            } else if (request.getParameter("page").equals("comments")) {
                return (mapping.findForward("uploader.go.comments"));
            } else if (request.getParameter("page").equals("producthistory")) {
                return (mapping.findForward("uploader.go.prodhistory"));
            } else if (request.getParameter("page").equals("nursingcareplan")) {
                return (mapping.findForward("uploader.go.nursingcareplan"));
            } else if (request.getParameter("page").equals("admin")) {
                log.info("############$$$$$$$$$$$$################# Page: " + request.getParameter("page"));
                return (mapping.findForward("uploader.go.admin"));
            } else {
                return (mapping.findForward("uploader.examflowchart.success"));
            }
        } else {
            return (mapping.findForward("uploader.examflowchart.success"));
        }
    }
}
