/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.viewer;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.Constants;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.ReportingManager;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
/**
 *
 * @author travis
 */
public class PdfSummarySetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PdfSummarySetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        String id = (String) request.getParameter("id");
        String report_type = (String) request.getParameter("report_type");
        
        ProfessionalVO userVO = null;
        if (session.getAttribute("userVO") == null) {
            request.setAttribute("session_invalid", "yes");
            return (mapping.findForward("system.logoff"));
        }else{
            userVO = (ProfessionalVO)session.getAttribute("userVO");
        }

        System.out.println("Report T_YPE: "+report_type);
        if (report_type != null && !report_type.equals("") && id != null) {
            try {
                ReportingManager manager = new ReportingManager(
                        userVO, 
                        null, 
                        null, 
                        null, 
                        null, 
                        "report_wound_management_plan", 
                        null, null, null, null, null, null, null, null, 
                        id, session.getServletContext(), language);
                manager.callReport(request, response);
            } catch (Exception e) {
                log.error("An application exception has been raised in ParameterAction.perform(): " + e.toString());
                e.printStackTrace();
                mapping.findForward("uploader.patient.failure");
            }
        }
        else if (id != null) {
            ReportBuilder rservice = new ReportBuilder(userVO, Constants.SUMMARY_REPORT, request, response, language);
            rservice.generatePDFSummary(id);
        }
        return (mapping.findForward("pdf.success"));

    }
}
