package com.pixalere.struts.viewer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.utils.Constants;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.utils.PDate;

import java.util.Hashtable;

import com.pixalere.auth.bean.ProfessionalVO;

import com.pixalere.utils.Common;

public class Viewer extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Viewer.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        log.info("##$$##$$##-----IN VIEWER.perform()-----##$$##$$##");
        ViewerForm viewerForm = (ViewerForm) form;
        HttpSession session = request.getSession();
        
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
        if (request.getParameter("wound_id") != null && !request.getParameter("wound_id").equals("")) {


            session.setAttribute("wound_id", request.getParameter("wound_id"));
            wound_id = (String) request.getParameter("wound_id");

        } else {
            //   session.removeAttribute("wound_id");
        }
        if (viewerForm.getAssessment_select1() != null && !viewerForm.getAssessment_select1().equals("")) {
            request.setAttribute("assessment_select1", viewerForm.getAssessment_select1());
        }
        if (viewerForm.getAssessment_select2() != null && !viewerForm.getAssessment_select2().equals("")) {
            request.setAttribute("assessment_select2", viewerForm.getAssessment_select2());
        }
        if (viewerForm.getThumbnail_select1() != null && !viewerForm.getThumbnail_select1().equals("")) {
            request.setAttribute("thumbnail_select1", viewerForm.getThumbnail_select1());
        }
        if (viewerForm.getThumbnail_select2() != null && !viewerForm.getThumbnail_select2().equals("")) {
            request.setAttribute("thumbnail_select2", viewerForm.getThumbnail_select2());
        }
        if (request.getParameter("page")!=null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("visitsflowchart")) {
            return (mapping.findForward("uploader.go.visits"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            }else if (request.getParameter("page")!=null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("patientflowchart")) {
            return (mapping.findForward("uploader.go.patientflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundflowchart")) {
            return (mapping.findForward("uploader.go.woundflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assessflowchart")) {
            return (mapping.findForward("uploader.go.assessflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("mdiagram")) {
            return (mapping.findForward("uploader.go.mdiagram"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("producthistory")) {
            return (mapping.findForward("uploader.go.prodhistory"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("comments")) {
            return (mapping.findForward("uploader.go.comments"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("nursingcareplan")) {
            return (mapping.findForward("uploader.go.nursingcareplan"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            }else if (request.getParameter("page")!=null && request.getParameter("page").equals("bradenflowchart")) {
            return (mapping.findForward("uploader.go.bradenflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("limbflowchart")) {
            return (mapping.findForward("uploader.go.limbflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("footflowchart")) {
            return (mapping.findForward("uploader.go.footflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("sensoryflowchart")) {
            return (mapping.findForward("uploader.go.sensoryflowchart"));
        } else {
            return (mapping.findForward("uploader.go.viewer"));
        }
    }
}
