package com.pixalere.struts.viewer;

import com.pixalere.common.DataAccessException;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.*;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Serialize;
import com.pixalere.common.bean.InformationPopupVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.io.*;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;

public class QuickPikSuccessSetupAction extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(IpadImageUpload.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

        System.out.println("In ImageUploader for the iPad");
        if(request.getParameter("success")!=null && ((String)request.getParameter("success")).equals("1")){
            return (mapping.findForward("quick.success"));
        }else{
             return (mapping.findForward("quick.failed"));
        }


    }

}
