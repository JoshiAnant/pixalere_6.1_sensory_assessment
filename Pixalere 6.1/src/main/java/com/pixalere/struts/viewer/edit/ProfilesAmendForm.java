package com.pixalere.struts.viewer.edit;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Hashtable;

public class ProfilesAmendForm extends ActionForm {

	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfilesAmendForm.class);

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        //nothing
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

 
    private String fwd;
    private String action;
  	private String id;
  	private int type;
    private String deleted_reason;
    private int lastupdate_day;
    private int lastupdate_month;
    private int lastupdate_year;
    public String getDeleted_reason() {
        return deleted_reason;
    }

    public void setDeleted_reason(String deleted_reason) {
        this.deleted_reason = deleted_reason;
    }

    public int getLastupdate_day() {
        return lastupdate_day;
    }

    public void setLastupdate_day(int lastupdate_day) {
        this.lastupdate_day = lastupdate_day;
    }

    public int getLastupdate_month() {
        return lastupdate_month;
    }

    public void setLastupdate_month(int lastupdate_month) {
        this.lastupdate_month = lastupdate_month;
    }

    public int getLastupdate_year() {
        return lastupdate_year;
    }

    public void setLastupdate_year(int lastupdate_year) {
        this.lastupdate_year = lastupdate_year;
    }

    /**
     * @return the fwd
     */
    public String getFwd() {
        return fwd;
    }

    /**
     * @param fwd the fwd to set
     */
    public void setFwd(String fwd) {
        this.fwd = fwd;
    }
}
