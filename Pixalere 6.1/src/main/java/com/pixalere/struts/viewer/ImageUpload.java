package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Serialize;
import com.pixalere.common.bean.InformationPopupVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.io.*;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;

public class ImageUpload extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImageUpload.class);
   
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        String imageArray = "";
        String imageArray2 = "";
        String imageArray3 = "";
        ImageUploadForm assessmentEachwoundForm = (ImageUploadForm) form;
        Common common = new Common();
        String absolute_path = common.getPhotosPath();
        AssessmentImagesServiceImpl managerBD = new AssessmentImagesServiceImpl();
        WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
        String assessment_id = assessmentEachwoundForm.getAssessment_id() + "";
        
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
        try {
            request.setAttribute("assess_id", assessmentEachwoundForm.getAssessment_id());
            request.setAttribute("assessment_id", assessmentEachwoundForm.getAssessment_id());
            
            request.setAttribute("alpha_id", assessmentEachwoundForm.getAlpha_id());

            if (!(assessmentEachwoundForm.getDeleteImage()).equals("image") && !(assessmentEachwoundForm.getDeleteImage()).equals("imageExtra")) {
                com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
                PDate pdate  =new PDate();
                String user_signature = pdate.getProfessionalSignature(new java.util.Date(), userVO,locale);
                WoundAssessmentLocationVO location = (WoundAssessmentLocationVO) dao.findByPK(new Integer(assessmentEachwoundForm.getAlpha_id()).intValue());

                com.pixalere.utils.ImageManipulation image = new com.pixalere.utils.ImageManipulation((String) session.getAttribute("patient_id"), user_signature, absolute_path, assessmentEachwoundForm.getAssessment_id() + "");

                if (request.getParameter("image_array") != null) {
                    imageArray = (String) request.getParameter("image_array");

                }
                if (request.getParameter("image_array2") != null) {
                    imageArray2 = (String) request.getParameter("image_array2");
                }
                if (request.getParameter("image_array3") != null) {
                    imageArray3 = (String) request.getParameter("image_array3");
                }
                //Get Image from Form, and create serialized array for DB (legacy reasons)
                long currenttime = pdate.getEpochTime();
                FormFile image1 = assessmentEachwoundForm.getImage_1();

                if (imageArray.equals("") && image1 != null && !image1.getFileName().equals("")) {
                    imageArray =  currenttime + "_" + location.getAlpha() + "-1.jpg";
                    String error = image.processUploadedFile(image1, "" + currenttime + "_" + location.getAlpha() + "-1.jpg", userVO.getId().intValue());
                    if(!error.equals("")){
                                ActionErrors errors = new ActionErrors();
                                request.setAttribute("exception", "y");

                                String exception = error;
                                request.setAttribute("exception_log", exception);
                                errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                                saveErrors(request, errors);
                                //return (mapping.findForward("pixalere.error"));
                                }
                } 

                try {
                    AssessmentImagesVO ig = new AssessmentImagesVO();
                    ig.setAssessment_id(new Integer(assessment_id));
                    ig.setAlpha_id(new Integer(assessmentEachwoundForm.getAlpha_id()));
                    ig.setWhichImage(Constants.IMAGE_1);
                    AssessmentImagesVO imagesVO = managerBD.getImage(ig);
                    if(session.getAttribute("wound_profile_type_id") !=null){
                        Integer wpt = new Integer((String)session.getAttribute("wound_profile_type_id"));
                        if(wpt != location.getWound_profile_type_id()){
                            // this popup is stale.
                            
                        }
                    }
                    if (imagesVO == null) {
                        imagesVO = new AssessmentImagesVO();
                        imagesVO.setAssessment_id(new Integer(assessmentEachwoundForm.getAssessment_id() + ""));
                        imagesVO.setAlpha_id(new Integer(assessmentEachwoundForm.getAlpha_id() + ""));
                        imagesVO.setWound_profile_type_id(new Integer((String)session.getAttribute("wound_profile_type_id")));
                        imagesVO.setActive(new Integer(1));
                        imagesVO.setWound_id(location.getWound_id());
                        imagesVO.setWhichImage(Constants.IMAGE_1);
                        imagesVO.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    }
                    imagesVO.setImages(imageArray);
                    if(!imageArray.equals("")){
                        managerBD.saveImage(imagesVO);
                    }
                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }

                FormFile image2 = assessmentEachwoundForm.getImage_2();
                if (imageArray2.equals("") && image2 != null && !image2.getFileName().equals("")) {
                    imageArray2 =  currenttime + "_" + location.getAlpha() + "-2.jpg";
                    String error = image.processUploadedFile(image2, "" + currenttime + "_" + location.getAlpha() + "-2.jpg", userVO.getId().intValue());
                    if(!error.equals("")){
                                ActionErrors errors = new ActionErrors();
                                request.setAttribute("exception", "y");

                                String exception = error;
                                request.setAttribute("exception_log", exception);
                                errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                                saveErrors(request, errors);
                                //return (mapping.findForward("pixalere.error"));
                                }
                } 

                try {
                    AssessmentImagesVO ig = new AssessmentImagesVO();
                    ig.setAssessment_id(new Integer(assessment_id));
                    ig.setAlpha_id(new Integer(assessmentEachwoundForm.getAlpha_id()));
                    ig.setWhichImage(Constants.IMAGE_2);
                    AssessmentImagesVO imagesVO2 = managerBD.getImage(ig);
                    if (imagesVO2 == null) {
                        imagesVO2 = new AssessmentImagesVO();
                        imagesVO2.setAssessment_id(new Integer(assessmentEachwoundForm.getAssessment_id() + ""));
                        imagesVO2.setAlpha_id(new Integer(assessmentEachwoundForm.getAlpha_id() + ""));
                        imagesVO2.setWound_profile_type_id(new Integer((String)session.getAttribute("wound_profile_type_id")));
                        imagesVO2.setActive(new Integer(1));
                        imagesVO2.setWhichImage(Constants.IMAGE_2);
                        imagesVO2.setWound_id(location.getWound_id());
                        imagesVO2.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    }
                    imagesVO2.setImages(imageArray2);
                    if(!imageArray2.equals("")){
                        managerBD.saveImage(imagesVO2);
                    }

                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
                FormFile image3 = assessmentEachwoundForm.getImage_3();

                if (imageArray3.equals("") && image3 != null && !image3.getFileName().equals("")) {
                    imageArray3 =  currenttime + "_" + location.getAlpha() + "-3.jpg";
                    String error = image.processUploadedFile(image3, "" + currenttime + "_" + location.getAlpha() + "-3.jpg", userVO.getId().intValue());
                    if(!error.equals("")){
                                ActionErrors errors = new ActionErrors();
                                request.setAttribute("exception", "y");

                                String exception = error;
                                request.setAttribute("exception_log", exception);
                                errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                                saveErrors(request, errors);
                                //return (mapping.findForward("pixalere.error"));
                                }
                }

                try {
                    AssessmentImagesVO ig = new AssessmentImagesVO();
                    ig.setAssessment_id(new Integer(assessment_id));
                    ig.setAlpha_id(new Integer(assessmentEachwoundForm.getAlpha_id()));
                    ig.setWhichImage(Constants.IMAGE_3);
                    AssessmentImagesVO imagesVO = managerBD.getImage(ig);
                    if (imagesVO == null) {
                        imagesVO = new AssessmentImagesVO();
                        imagesVO.setAssessment_id(new Integer(assessmentEachwoundForm.getAssessment_id() + ""));
                        imagesVO.setAlpha_id(new Integer(assessmentEachwoundForm.getAlpha_id() + ""));
                        imagesVO.setWound_profile_type_id(new Integer((String)session.getAttribute("wound_profile_type_id")));
                        imagesVO.setActive(new Integer(1));
                        imagesVO.setWound_id(location.getWound_id());
                        imagesVO.setWhichImage(Constants.IMAGE_3);
                        imagesVO.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    }
                    imagesVO.setImages(imageArray3);
                    if(!imageArray3.equals("")){
                        managerBD.saveImage(imagesVO);
                    }
                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
            }
            AssessmentImagesServiceImpl manager = new AssessmentImagesServiceImpl();
            AssessmentImagesVO ig = new AssessmentImagesVO();
            ig.setAssessment_id(new Integer((String) request.getParameter("assessment_id")));
            ig.setAlpha_id(new Integer((String) request.getParameter("alpha_id")));
            ig.setWhichImage(Constants.IMAGE_1);
            com.pixalere.assessment.bean.AssessmentImagesVO image_ = manager.getImage(ig);
            ig.setWhichImage(Constants.IMAGE_2);
            com.pixalere.assessment.bean.AssessmentImagesVO extraImage = manager.getImage(ig);
ig.setWhichImage(Constants.IMAGE_3);
            com.pixalere.assessment.bean.AssessmentImagesVO extraImage3 = manager.getImage(ig);

            if (image_ != null && image_.getImages() != null) {

                request.setAttribute("image_array", (Serialize.arrayIze(image_.getImages()).size() > 0) ? Serialize.arrayIze(
                        image_.getImages()).get(0) : "");


                if ((assessmentEachwoundForm.getDeleteImage()).equals("image")) {

                    request.setAttribute("image_array", "");
                    manager.removeImage(image_);
                }

            }
            if (extraImage != null && extraImage.getImages() != null) {
                request.setAttribute("image_array2", (Serialize.arrayIze(extraImage.getImages()).size() > 0) ? Serialize.arrayIze(
                        extraImage.getImages()).get(0) : "");

                if ((assessmentEachwoundForm.getDeleteImage()).equals("imageExtra")) {
                                            
                    request.setAttribute("image_array2", "");
                    manager.removeImage(extraImage);
                }

            }
if (extraImage3 != null && extraImage3.getImages() != null) {
                request.setAttribute("image_array3", (Serialize.arrayIze(extraImage3.getImages()).size() > 0) ? Serialize.arrayIze(
                        extraImage3.getImages()).get(0) : "");

                if ((assessmentEachwoundForm.getDeleteImage()).equals("imageExtra2")) {

                    request.setAttribute("image_array3", "");
                    manager.removeImage(extraImage3);
                }

            }
        } catch (com.pixalere.common.DataAccessException e) {
            log.error("An application exception has been raised in LogAuditAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        InformationPopupVO info = new InformationPopupVO();
        info.setTitle("Image Upload");
        info.setDescription("Successfully updated patient record.");
        request.setAttribute("info", info);
        request.setAttribute("type", "saved");
        request.setAttribute("field", "success");
        return (mapping.findForward("uploader.assesserror.success"));

    }

    public void processUploadedFile(FormFile file, String fileName, String patient_id, String assessment_id, String path_absolute) {
        try {

            InputStream stream = file.getInputStream();
            OutputStream bos = new FileOutputStream(path_absolute + "/live/" + patient_id + "/" + assessment_id + "/" + fileName);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }

            bos.close();

            // close the stream
            stream.close();

        } catch (Exception ex) {
            log.error("ImageUpload.perform: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}