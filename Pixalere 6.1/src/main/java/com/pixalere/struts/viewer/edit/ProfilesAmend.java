package com.pixalere.struts.viewer.edit;

import com.pixalere.utils.*;

import java.util.Vector;

import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.wound.service.*;
import com.pixalere.assessment.service.*;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.assessment.bean.*;
import com.pixalere.wound.bean.*;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PhysicalExamVO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.pixalere.common.ApplicationException;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.pixalere.common.bean.*;
import com.pixalere.common.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.*;

public class ProfilesAmend extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfilesAmend.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        ProfilesAmendForm assessmentAmendForm = (ProfilesAmendForm) form;
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

        try {
            PDate pdate = new PDate(userVO.getTimezone());
            int intId = Integer.parseInt(assessmentAmendForm.getId());

            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            String deleted_reason = assessmentAmendForm.getDeleted_reason();


            if (assessmentAmendForm.getAction().equals("delete")) {
                //System.out.println(assessmentAmendForm.getAction() + " " + assessmentAmendForm.getType() + " " + assessmentAmendForm.getId());
                if (assessmentAmendForm.getType() == Constants.PATIENT_PROFILE) {
                    PatientProfileServiceImpl service = new PatientProfileServiceImpl(language);
                    PatientProfileVO ptmp = new PatientProfileVO();
                    ptmp.setId(intId);
                    PatientProfileVO p = service.getPatientProfile(ptmp);
                    //System.out.println("P");
                    if (p != null) {
                        p.setDeleted(1);
                        p.setCurrent_flag(0);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        service.savePatientProfile(p);
                        PatientProfileVO tmp = new PatientProfileVO();
                        tmp.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        tmp.setActive(1);
                        PatientProfileVO newCurrent = service.getPatientProfile(tmp);
                        if (newCurrent != null) {
                            newCurrent.setCurrent_flag(1);
                            service.savePatientProfile(newCurrent);
                        }
                    }
                }if (assessmentAmendForm.getType() == Constants.PHYSICAL_EXAM) {
                    PatientProfileServiceImpl service = new PatientProfileServiceImpl(language);
                    PhysicalExamVO ptmp = new PhysicalExamVO();
                    ptmp.setId(intId);
                    PhysicalExamVO p = service.getExam(ptmp);
                    //System.out.println("P");
                    if (p != null) {
                        p.setDeleted(1);
                        p.setCurrent_flag(0);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        service.savePhysicalExam(p);
                        PhysicalExamVO tmp = new PhysicalExamVO();
                        tmp.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        tmp.setActive(1);
                        PhysicalExamVO newCurrent = service.getExam(tmp);
                        if (newCurrent != null) {
                            newCurrent.setCurrent_flag(1);
                            service.savePhysicalExam(newCurrent);
                        }
                    }
                }  else if (assessmentAmendForm.getType() == Constants.WOUND_PROFILE) {
                    WoundServiceImpl service = new WoundServiceImpl(language);
                    NursingCarePlanServiceImpl nservice = new NursingCarePlanServiceImpl();
                    AssessmentImagesServiceImpl iservice = new AssessmentImagesServiceImpl();
                    WoundAssessmentLocationServiceImpl aservice = new WoundAssessmentLocationServiceImpl();
                    AssessmentCommentsServiceImpl acservice = new AssessmentCommentsServiceImpl();
                    WoundProfileVO ptmp = new WoundProfileVO();
                    ptmp.setId(intId);
                    WoundProfileVO p = service.getWoundProfile(ptmp);

                    if (p != null) {
                        int wound_id = p.getWound_id();
                        p.setDeleted(1);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        p.setCurrent_flag(0);
                        //System.out.println("WP: "+p.getDeleted());
                        service.saveWoundProfile(p);
                        WoundProfileVO tmp = new WoundProfileVO();
                        tmp.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        tmp.setWound_id(wound_id);
                        tmp.setActive(1);
                        WoundProfileVO newCurrent = service.getWoundProfile(tmp);
                        //System.out.println("Should be Null");
                        if (newCurrent != null) {
                            //System.out.println(newCurrent.getId()+"its not: "+newCurrent.getDeleted());
                            newCurrent.setCurrent_flag(1);
                            service.saveWoundProfile(newCurrent);
                        }
                        WoundProfileVO t = new WoundProfileVO();
                        t.setWound_id(wound_id);
                        t.setDeleted(0);
                        t.setActive(1);
                        WoundProfileVO profile = service.getWoundProfile(t);
                        if (profile == null) {
                            //Remove NCP    
                            NursingCarePlanVO n1 = new NursingCarePlanVO();
                            n1.setWound_id(wound_id);
                            Collection<NursingCarePlanVO> ncp = nservice.getAllNursingCarePlans(n1, -1);
                            for(NursingCarePlanVO n : ncp){
                                n.setDeleted(1);
                                n.setDeleted_reason(deleted_reason);
                                n.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                nservice.saveNursingCarePlan(n);
                                
                            }
                            //Remove Comments
                            AssessmentCommentsVO c1 = new AssessmentCommentsVO();
                            c1.setWound_id(wound_id);
                            Collection<AssessmentCommentsVO> comments = acservice.getAllComments(c1);
                            for(AssessmentCommentsVO comm : comments){
                                comm.setDeleted(1);
                                comm.setDelete_reason(deleted_reason);
                                comm.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                acservice.saveComment(comm);
                            }
                            //There are no wound profiles left which are not deleted, so we must recursively delete everything!
                            ReferralsTrackingServiceImpl referrals_manager = new ReferralsTrackingServiceImpl(language);
                            ReferralsTrackingVO searchVO6 = new ReferralsTrackingVO();
                            searchVO6.setWound_id(wound_id);
                            Collection<ReferralsTrackingVO> assessment_referrals = referrals_manager.getAllReferralsByCriteria(searchVO6);
                            if (assessment_referrals != null) {
                                for (Iterator referrals = assessment_referrals.iterator(); referrals.hasNext();) {
                                    ReferralsTrackingVO assessment_referral = (ReferralsTrackingVO) referrals.next();
                                    assessment_referral.setCurrent(0);
                                    referrals_manager.saveReferralsTracking(assessment_referral);
                                }
                            }
                            //Remove Assessments,
                            AssessmentEachwoundVO v1 = new AssessmentEachwoundVO();
                            v1.setWound_id(wound_id);
                            Collection<AssessmentEachwoundVO> assessment_details = manager.getAllAssessments(v1, 0, false);
                            if (assessment_details != null) {
                                for(AssessmentEachwoundVO a : assessment_details){

                                    a.setDeleted(1);
                                    a.setDeleted_reason(deleted_reason);
                                    a.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                    manager.saveAssessment(a);
                                    AssessmentImagesVO img = new AssessmentImagesVO();
                                    img.setAlpha_id(a.getAlpha_id());
                                    img.setAssessment_id(a.getAssessment_id());
                                    iservice.removeImage(img);
                                }
                            }
                            AssessmentOstomyVO v2 = new AssessmentOstomyVO();
                            v2.setWound_id(wound_id);
                            Collection<AssessmentOstomyVO> assessment_details2 = manager.getAllAssessments(v2, 0, false);
                            if (assessment_details2 != null) {
                                for(AssessmentOstomyVO a : assessment_details2){

                                    a.setDeleted(1);
                                    a.setDeleted_reason(deleted_reason);
                                    a.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                    manager.saveAssessment(a);
                                    AssessmentImagesVO img = new AssessmentImagesVO();
                                    img.setAlpha_id(a.getAlpha_id());
                                    img.setAssessment_id(a.getAssessment_id());
                                    iservice.removeImage(img);
                                }
                            }
                            AssessmentDrainVO v3 = new AssessmentDrainVO();
                            v3.setWound_id(wound_id);
                            Collection<AssessmentDrainVO> assessment_details3 = manager.getAllAssessments(v3, 0, false);
                            if (assessment_details3 != null) {
                                for(AssessmentDrainVO a : assessment_details3){

                                    a.setDeleted(1);
                                    a.setDeleted_reason(deleted_reason);
                                    a.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                    manager.saveAssessment(a);
                                    AssessmentImagesVO img = new AssessmentImagesVO();
                                    img.setAlpha_id(a.getAlpha_id());
                                    img.setAssessment_id(a.getAssessment_id());
                                    iservice.removeImage(img);
                                }
                            }
                            AssessmentIncisionVO v4 = new AssessmentIncisionVO();
                            v4.setWound_id(wound_id);
                            Collection<AssessmentIncisionVO> assessment_details4 = manager.getAllAssessments(v4, 0, false);
                            if (assessment_details4 != null) {
                                for(AssessmentIncisionVO a : assessment_details4){

                                    a.setDeleted(1);
                                    a.setDeleted_reason(deleted_reason);
                                    a.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                    manager.saveAssessment(a);
                                    AssessmentImagesVO img = new AssessmentImagesVO();
                                    img.setAlpha_id(a.getAlpha_id());
                                    img.setAssessment_id(a.getAssessment_id());
                                    iservice.removeImage(img);
                                }
                            }
                            AssessmentBurnVO v5 = new AssessmentBurnVO();
                            v5.setWound_id(wound_id);
                            Collection<AssessmentBurnVO> assessment_details5 = manager.getAllAssessments(v5, 0, false);
                            if (assessment_details5 != null) {
                                for(AssessmentBurnVO a : assessment_details5){

                                    a.setDeleted(1);
                                    a.setDeleted_reason(deleted_reason);
                                    a.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                    manager.saveAssessment(a);
                                    AssessmentImagesVO img = new AssessmentImagesVO();
                                    img.setAlpha_id(a.getAlpha_id());
                                    img.setAssessment_id(a.getAssessment_id());
                                    iservice.removeImage(img);
                                }
                            }
                            AssessmentSkinVO v6 = new AssessmentSkinVO();
                            v6.setWound_id(wound_id);
                            Collection<AssessmentSkinVO> assessment_details6 = manager.getAllAssessments(v6, 0, false);
                            if (assessment_details6 != null) {
                                for(AssessmentSkinVO a : assessment_details6){

                                    a.setDeleted(1);
                                    a.setDeleted_reason(deleted_reason);
                                    a.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                    manager.saveAssessment(a);
                                    AssessmentImagesVO img = new AssessmentImagesVO();
                                    img.setAlpha_id(a.getAlpha_id());
                                    img.setAssessment_id(a.getAssessment_id());
                                    iservice.removeImage(img);
                                }
                            }
                            //Remove Alphas
                            WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
                            wal.setWound_id(wound_id);
                            Vector<WoundAssessmentLocationVO> alphas = aservice.getAllAlphas(wal);
                            for(WoundAssessmentLocationVO alpha : alphas){
                                alpha.setDeleted(1);
                                    alpha.setDeleted_reason(deleted_reason);
                                    alpha.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                    aservice.saveAlpha(alpha);
                            }
                            //Remove WoundProfileTypeVO
                            WoundProfileTypeVO wpt = new WoundProfileTypeVO();
                            wpt.setWound_id(wound_id);
                            
                            Collection<WoundProfileTypeVO> wpts = service.getWoundProfileTypes(wpt, -1);
                            for(WoundProfileTypeVO wptt : wpts){
                                wptt.setDeleted(1);
                                    wptt.setDeleted_reason(deleted_reason);
                                    wptt.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                service.saveWoundProfileType(wptt);
                            }
                            //Remove WoundVO
                            WoundVO wnd = service.getWound(wound_id);
                            if(wnd!=null){
                                wnd.setDeleted(1);
                                    wnd.setDeleted_reason(deleted_reason);
                                    wnd.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                                service.saveWound(wnd);
                            }
                        }
                    }

                } else if (assessmentAmendForm.getType() == Constants.FOOT_ASSESSMENT) {
                    PatientProfileServiceImpl service = new PatientProfileServiceImpl(language);
                    FootAssessmentVO ptmp = new FootAssessmentVO();
                    ptmp.setId(intId);
                    FootAssessmentVO p = service.getFootAssessment(ptmp);
                    if (p != null) {
                        p.setDeleted(1);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        service.saveFoot(p);

                    }

                } else if (assessmentAmendForm.getType() == Constants.SENSORY_ASSESSMENT) {
                    PatientProfileServiceImpl service = new PatientProfileServiceImpl(language);
                    SensoryAssessmentVO ptmp = new SensoryAssessmentVO();
                    ptmp.setId(intId);
                    SensoryAssessmentVO p = service.getSensoryAssessment(ptmp);
                    if (p != null) {
                        p.setDeleted(1);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        service.saveSensory(p);

                    }

                } else if (assessmentAmendForm.getType() == Constants.LIMB_ASSESSMENT_BASIC) {
                    PatientProfileServiceImpl service = new PatientProfileServiceImpl(language);
                    LimbBasicAssessmentVO ptmp = new LimbBasicAssessmentVO();
                    ptmp.setId(intId);
                    LimbBasicAssessmentVO p = service.getLimbBasicAssessment(ptmp);
                    if (p != null) {
                        p.setDeleted(1);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        service.saveBasicLimb(p);

                    }

                } else if (assessmentAmendForm.getType() == Constants.LIMB_ASSESSMENT_ADVANCED) {
                    PatientProfileServiceImpl service = new PatientProfileServiceImpl(language);
                    LimbAdvAssessmentVO ptmp = new LimbAdvAssessmentVO();
                    ptmp.setId(intId);
                    LimbAdvAssessmentVO p = service.getLimbAdvAssessment(ptmp);
                    if (p != null) {
                        p.setDeleted(1);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        service.saveAdvLimb(p);

                    }

                } else if (assessmentAmendForm.getType() == Constants.BRADEN_SCORE_SECTION) {
                    PatientProfileServiceImpl service = new PatientProfileServiceImpl(language);
                    BradenVO ptmp = new BradenVO();
                    ptmp.setId(intId);
                    BradenVO p = service.getBraden(ptmp);
                    if (p != null) {
                        p.setDeleted(1);
                        p.setDeleted_reason(deleted_reason);
                        p.setDelete_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        service.saveBraden(p);
                        BradenVO tmp = new BradenVO();
                        tmp.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        tmp.setActive(1);
                        BradenVO newCurrent = service.getBraden(tmp);
                        if (newCurrent != null) {
                            newCurrent.setCurrent_flag(1);
                            service.saveBraden(newCurrent);
                        }
                    }

                }

            }
            return (mapping.findForward(assessmentAmendForm.getFwd()));
        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (Exception e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }



    }
}
