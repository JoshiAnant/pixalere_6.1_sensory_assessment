package com.pixalere.struts.viewer;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.upload.FormFile;
public class ImageUploadForm extends ActionForm {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImageUploadForm.class);

  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    return errors;
  }

  public void reset(ActionMapping mapping, HttpServletRequest request) {
  }
  public void setImage_1(FormFile image_1){
	this.image_1=image_1;
  }
  public FormFile getImage_1(){
	return image_1;
  }

  public FormFile getImage_2(){
	return image_2;
  }
  public void setImage_2(FormFile image_2){
	this.image_2=image_2;
  }
  public FormFile getImage_3(){
	return image_3;
  }
  public void setImage_3(FormFile image_3){
	this.image_3=image_3;
  }

  public Integer getAssessment_id(){
	return assessment_id;
}
public void setAssessment_id(Integer assessment_id){
	this.assessment_id=assessment_id;
}
    public String getImage_array() {
        return image_array;
    }

    public void setImage_array(String image_array) {
        this.image_array = image_array;
    }

    public String getImage_array2() {
        return image_array2;
    }

    public void setImage_array2(String image_array2) {
        this.image_array2 = image_array2;
    }
public String getImage_array3() {
        return image_array3;
    }

    public void setImage_array3(String image_array3) {
        this.image_array3 = image_array3;
    }
    public String getAlpha_id() {
        return alpha_id;
    }

    public void setAlpha_id(String alpha_id) {
        this.alpha_id = alpha_id;
    }
    
    private String deleteImage;
    private String image_array;
  private Integer assessment_id;
  private FormFile image_1;
  private FormFile image_2;
  private FormFile image_3;
  private String alpha_id;
    private String image_array2;
    private String image_array3;

	/**
	 * @method getDelete
	 * @field delete - String
	 * @return Returns the delete.
	 */
	public String getDeleteImage() {
		return deleteImage;
	}

	/**
	 * @method setDelete
	 * @field delete - String
	 * @param delete The delete to set.
	 */
	public void setDeleteImage(String deleteImage) {
		this.deleteImage = deleteImage;
	}

}