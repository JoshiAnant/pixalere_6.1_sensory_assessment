package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;

import com.pixalere.utils.Common;
import org.apache.struts.action.ActionForm;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.utils.PDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import com.pixalere.utils.Constants;

public class MeasurementsDiagramSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MeasurementsDiagramSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {
            String alpha_id = (String) request.getParameter("alpha_id");
            String alpha_type = (String) request.getParameter("alpha_type");
            System.out.println("*******************ALPHA ID:" + alpha_id);
            request.setAttribute("page", "mdiagram");

            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {

                return (mapping.findForward("uploader.go.viewer"));
            }
            request.removeAttribute("populate_alphas"); // Needs to be removed to prevent from js error  SV 080508

            /*String id = "";
             int limit = 3;
             if (request.getParameter("id") != null) {
             id = (String) request.getParameter("id");
             request.setAttribute("selectedid", (String) request.getParameter("id"));
             
             }*/
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            WoundAssessmentLocationServiceImpl wal = new WoundAssessmentLocationServiceImpl();
            WoundServiceImpl wp = new WoundServiceImpl(language);


            // default to assessment history on alpha A if first time
            // accessing.
            // we need to get the alpha_id for A as it is the default
            WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
            walocation.setPatient_id(new Integer((String) patient_id));
            walocation.setWound_profile_type_id(new Integer((String) wound_profile_type_id));

            if (alpha_id == null) {

                WoundAssessmentLocationVO activeAlpha = wal.getAlpha(walocation, false, Constants.HIDE_INCISIONS, Constants.WOUND_PROFILE_TYPE);
                if (activeAlpha == null) {
                    activeAlpha = wal.getAlpha(walocation, false, Constants.HIDE_INCISIONS, Constants.INCISIONTAG_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = wal.getAlpha(walocation, false, Constants.HIDE_INCISIONS, Constants.TUBESANDDRAINS_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = wal.getAlpha(walocation, false, Constants.HIDE_INCISIONS, Constants.OSTOMY_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = wal.getAlpha(walocation, false, Constants.HIDE_INCISIONS, Constants.BURN_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = wal.getAlpha(walocation, false, Constants.HIDE_INCISIONS, Constants.SKIN_PROFILE_TYPE);
                }
                if (activeAlpha != null) {
                    alpha_id = activeAlpha.getId() + "";
                    alpha_type = activeAlpha.getWound_profile_type().getAlpha_type();
                    wound_id = activeAlpha.getWound_id() + "";
                }

            }
            //WoundAssessmentLocationVO loco = (WoundAssessmentLocationVO) manager.retrieveAlphaByPK(new Integer(alpha_id).intValue());
            WoundAssessmentLocationVO tmp = new WoundAssessmentLocationVO();
            tmp.setPatient_id(new Integer((String) patient_id));
            tmp.setWound_profile_type_id(new Integer((String) wound_profile_type_id));

            Collection alphas = wal.getAllAlphas(tmp);

            ArrayList alphaslist = new ArrayList();
            alphaslist.addAll(alphas);

            request.setAttribute("alphas", Common.getSortedAlphaList(alphaslist));


            request.setAttribute("flowchart_text", "woundassessment");
            request.setAttribute("error_class", "AssessmentErrorSetup");

            request.setAttribute("alpha_type", alpha_type);
            request.setAttribute("alpha_id", alpha_id);


            if (alpha_id != null && alpha_type != null) {
                Collection<AbstractAssessmentVO> results = null;
                
                if (alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {
                    AssessmentEachwoundVO assess = new AssessmentEachwoundVO();
                    assess.setAlpha_id(new Integer(alpha_id));
                    assess.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                    assess.setActive(new Integer(1));
                    assess.setDeleted(0);

                    results = manager.getAllAssessments(assess,"visited_on", 0, Constants.DESC_ORDER);
                    
                } else if (alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {
                    AssessmentSkinVO assess = new AssessmentSkinVO();
                    assess.setAlpha_id(new Integer(alpha_id));
                    assess.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                    assess.setActive(new Integer(1));
                    assess.setDeleted(0);
                    results = manager.getAllAssessments(assess, "visited_on",0, Constants.DESC_ORDER);
                }
                Vector vecLength = new Vector();
                Vector vecWidth = new Vector();
                Vector vecDepth = new Vector();
                Vector vecDay = new Vector();
                Vector vecSignature = new Vector();
                String startDateLabel = "";
                String endDateLabel = "";
                String strStartDate = (String) request.getParameter("start_date");
                if (strStartDate == null) {
                    strStartDate = "-1";
                }
                String strEndDate = (String) request.getParameter("end_date");
                if (strEndDate == null) {
                    strEndDate = "-1";
                }
                
                if (results != null) {
                    for (AbstractAssessmentVO healVO : results) {
                        if (healVO != null) {
                            if(healVO.getWoundAssessment().getId()>Integer.parseInt(strEndDate)){
                                strEndDate = healVO.getWoundAssessment().getId()+"";
                            }
                        }
                    }
                    int cnt=0;
                    for (AbstractAssessmentVO healVO : results) {
                        if (healVO != null) {
                     
                            
                            double length = -1;
                            double width = -1;
                            double depth = -1;

                            if (healVO instanceof AssessmentEachwoundVO) {
                                AssessmentEachwoundVO e = (AssessmentEachwoundVO) healVO;
                                if(e.getLength()!=null)length = e.getLength();
                                if(e.getWidth()!=null)width = e.getWidth();
                                if(e.getDepth()!=null)depth = e.getDepth();
                            } else if (healVO instanceof AssessmentSkinVO) {
                                AssessmentSkinVO e = (AssessmentSkinVO) healVO;
                                if(e.getLength()!=null)length = e.getLength();
                                if(e.getWidth()!=null)width = e.getWidth();
                                if(e.getDepth()!=null)depth = e.getDepth();
                            }
                            
                                if ((strStartDate.equals("-1") || strEndDate.equals("-1") || ((Integer.parseInt(strStartDate)<=healVO.getAssessment_id()) && Integer.parseInt(strEndDate)>=healVO.getAssessment_id())) && (width > 0
                                        || length > 0
                                        || depth > 0)) // maybe there were two or more on the startdate
                                {
                                    cnt++;
                                    if(cnt == 1){
                                        startDateLabel = healVO.getWoundAssessment().getUser_signature();
                                    }
                                    vecLength.add((length == -1 ? 0 : length));
                                    vecWidth.add((width == -1 ? 0 : width));
                                    vecDepth.add((depth == -1 ? 0 : depth));
                                 
                                    vecDay.add(healVO.getWoundAssessment().getVisited_on());
                                    vecSignature.add(healVO.getWoundAssessment());
                               
                                    
                                }
                            
                        }
                    }
                }
                
                request.setAttribute("startDateLabel",startDateLabel);
                request.setAttribute("endDateLabel",endDateLabel);
                
                request.setAttribute("length_list", vecLength);
                request.setAttribute("width_list", vecWidth);
                request.setAttribute("depth_list", vecDepth);
                request.setAttribute("day_list", vecDay);
                request.setAttribute("date_list", vecSignature);
                request.setAttribute("start_date", strStartDate);
                request.setAttribute("end_date", strEndDate);

                if (wound_id != null && !wound_id.equals("")) {
                    WoundVO tmpVO = new WoundVO();
                    tmpVO.setId(new Integer(wound_id));
                    WoundVO wpvo = (WoundVO) wp.getWound(tmpVO);
                    if(wpvo!=null){
                        request.setAttribute("woundprofile", wpvo.getWound_location_detailed());
                    }
                }
            }
            //populate wound profile dropdown
            Vector woundProfilesDrop = wp.getWoundProfilesDropdown(Integer.parseInt((String) patient_id), false, userVO.getId().intValue());

            request.setAttribute("woundProfilesDrop", woundProfilesDrop);
            //  request.setAttribute("page", "mdiagram");
            //  request.setAttribute("page_sub", "mdiagram");

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in AssessmentFlowchartSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null) {
            if (request.getParameter("page").equals("patient")) {
                return (mapping.findForward("uploader.go.patient"));
            } else if (request.getParameter("page").equals("woundprofiles")) {
                return (mapping.findForward("uploader.go.profile"));
            } else if (request.getParameter("page").equals("assess")) {
                return (mapping.findForward("uploader.go.assess"));
            } else if (request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            } else if (request.getParameter("page").equals("treatment")) {
                return (mapping.findForward("uploader.go.treatment"));
            } else if (request.getParameter("page").equals("viewer")) {
                return (mapping.findForward("uploader.go.viewer"));
            } else if (request.getParameter("page").equals("summary")) {
                return (mapping.findForward("uploader.go.summary"));
            } else if (request.getParameter("page").equals("reporting")) {
                return (mapping.findForward("uploader.go.reporting"));
            } else if (request.getParameter("page").equals("patientflowchart")) {
                return (mapping.findForward("uploader.go.patientflowchart"));
            } else if (request.getParameter("page").equals("woundflowchart")) {
                return (mapping.findForward("uploader.go.woundflowchart"));
            } else if (request.getParameter("page").equals("assessflowchart")) {
                return (mapping.findForward("uploader.go.assessflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            } else if (request.getParameter("page").equals("comments")) {
                return (mapping.findForward("uploader.go.comments"));
            } else if (request.getParameter("page").equals("producthistory")) {
                return (mapping.findForward("uploader.go.prodhistory"));
            } else if (request.getParameter("page").equals("limbflowchart")) {
                return (mapping.findForward("uploader.go.limbflowchart"));
            } else if (request.getParameter("page").equals("footflowchart")) {
                return (mapping.findForward("uploader.go.footflowchart"));
            } else if (request.getParameter("page").equals("nursingcareplan")) {
                return (mapping.findForward("uploader.go.nursingcareplan"));
            } else if (request.getParameter("page").equals("admin")) {
                log.info("############$$$$$$$$$$$$################# Page: " + request.getParameter("page"));
                return (mapping.findForward("uploader.go.admin"));
            } else {
                return (mapping.findForward("uploader.mdiagram.success"));
            }
        } else {
            return (mapping.findForward("uploader.mdiagram.success"));
        }
    }
}