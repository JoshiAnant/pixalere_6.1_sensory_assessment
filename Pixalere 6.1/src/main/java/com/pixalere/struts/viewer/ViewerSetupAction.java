package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Collection;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;

import com.pixalere.guibeans.AssessmentListVO;
import com.pixalere.guibeans.ImagesListVO;

import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;

public class ViewerSetupAction extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ViewerSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        request.setAttribute("page", "viewer");
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_profile_type_id = null;

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        WoundServiceImpl woundBD = new WoundServiceImpl(language);
        AssessmentImagesServiceImpl aBD = new AssessmentImagesServiceImpl();
        WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
        WoundAssessmentServiceImpl waManager = new WoundAssessmentServiceImpl();
        String alphas = "";
        try {
            if (request.getParameter("wound_profile_type_id") != null && !((String) request.getParameter("wound_profile_type_id")).equals("")) {

                session.removeAttribute("savedComments");
                session.removeAttribute("ncp");
                session.setAttribute("wound_profile_type_id", request.getParameter("wound_profile_type_id"));

            }
            wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if (wound_profile_type_id != null && !wound_profile_type_id.equals("")) {
                //populate alphas on bluemodel
                WoundAssessmentLocationVO wavo = new WoundAssessmentLocationVO();
                wavo.setWound_profile_type_id(Integer.parseInt((String) wound_profile_type_id));
                wavo.setActive(new Integer(1));
                WoundProfileTypeVO wptvo = new WoundProfileTypeVO();
                wptvo.setId(wavo.getWound_profile_type_id());
                wptvo = woundBD.getWoundProfileType(wptvo);
                if (wptvo != null) {
                    wavo.setWound_profile_type_id(null);
                    wavo.setWound_id(wptvo.getWound_id());

                    WoundVO wndvo = new WoundVO();
                    wndvo.setId(wptvo.getWound_id());
                    WoundVO woundVO = (WoundVO) woundBD.getWound(wndvo);
                    String strBlueModel = "";
                    if (woundVO != null) {
                        strBlueModel = woundVO.getImage_name();
                    }
                }
                /*Vector locations = walManager.getAllAlphas(wavo); // Descending order
                 for (Iterator i = locations.iterator(); i.hasNext();) {
                 WoundAssessmentLocationVO locationVO = (WoundAssessmentLocationVO) i.next();
                 WoundLocationDetailsVO[] details = locationVO.getAlpha_details();
                 for (WoundLocationDetailsVO detail : details) {

                 //if(alphas.indexOf("showWound(document.img" + detail.getBox() + ",") == -1 && (detail.getDeleted() == null || detail.getDeleted().equals(new Integer(0)))) // Skip old alphas that are reactivated   and deleted.
                 //{
                 //alphas = alphas + "showWound(document.img" + detail.getBox() + "," + detail.getBox() + ",'" + detail.getImage() + "','" + locationVO.getWound_profile_type().getAlpha_type() + "','" + strBlueModel + "'," + locationVO.getDischarge() + "," + locationVO.getActive() + ",1," + locationVO.getId() + ");";
                 //}
                 }
                 }*/
            } else {
                //redirect to patient profile flowchart.
                return (mapping.findForward("uploader.go.patientflowchart"));
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ViewerSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        Vector assess = null;
        try {
            //populate wound profile dropdown (saved wounds only)
            Vector woundProfilesDrop = woundBD.getWoundProfilesDropdown(Integer.parseInt((String) patient_id), false, 0);
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);

            if (wound_profile_type_id != null && !wound_profile_type_id.equals("")) {
                //if  woundprofile is already selected, populate assessment dropdown
                log.info("ViewerSetupAction.perform: getting Assessment data." + wound_profile_type_id);
                AssessmentImagesServiceImpl manager = new AssessmentImagesServiceImpl();
                WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
                walocation.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                WoundAssessmentLocationVO location = (WoundAssessmentLocationVO) walManager.getAlpha(walocation, true, false, null);
                if (location != null) {
                    assess = manager.retrieveAssessmentDates((String) wound_profile_type_id, location.getWound_id() + "");
                    //request.setAttribute("assessments_list", assess);
                    request.setAttribute("populate_alphas", alphas);

                    WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
                    WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
                    tloco.setWound_profile_type_id(location.getWound_id());
                    tloco.setDischarge(0); // Only open alphas
                    tloco.setDeleted(0);
                    Vector<WoundAssessmentLocationVO> allalphas = walservice.retrieveLocationsForWound(userVO.getId(), location.getWound_id(), false, true);//4th parameter (extra sort) is ignored
                    Hashtable alphaMap = new Hashtable();
                    for (WoundAssessmentLocationVO v : allalphas) {
                        alphaMap.put(v.getAlpha(), v);
                    }
                    request.setAttribute("allalphas", alphaMap);
                    request.setAttribute("page", "viewer");
                }
            } else if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals("")) && (request.getAttribute("wound_profile_type_id") == null || ((String) request.getAttribute("wound_profile_type_id")).equals(""))) {

                return (mapping.findForward("uploader.viewer.success"));
            }

            log.info("ViewerSetupAction.perform: check if past image assessment drop was selected.");

            //check if woundprofile has been selected
            if (wound_profile_type_id != null && !wound_profile_type_id.equals("")) {
                AssessmentImagesServiceImpl manager = new AssessmentImagesServiceImpl();
                WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
                walocation.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                WoundAssessmentLocationVO location = (WoundAssessmentLocationVO) walManager.getAlpha(walocation, true, false, null);
                if (location != null) {
                    String strWoundProfileTypeID = (String) wound_profile_type_id;
                    request.setAttribute("assessment_dates", manager.retrieveAssessmentDates(strWoundProfileTypeID, location.getWound_id() + ""));
                    WoundProfileVO tmpVO = new WoundProfileVO();
                    tmpVO.setWound_id(location.getWound_id());
                    WoundProfileVO woundVO = (WoundProfileVO) woundBD.getWoundProfile(tmpVO, true);
                    //show bluemodel for wound
                    if (woundVO != null) {
                        request.setAttribute("wound_profile", woundVO.getWound());

                    }
                    if (assess != null && assess.size() != 0) {
                        AssessmentListVO lastAssessmentVO = (AssessmentListVO) assess.get(0);

                        String strAssessment_select1 = "";
                        if (request.getAttribute("assessment_select1") != null) {
                            //show images for past assessment
                            strAssessment_select1 = (String) request.getAttribute("assessment_select1");
                        } else if (assess != null && assess.size() >= 1) {
                            //show images for last assessment
                            strAssessment_select1 = lastAssessmentVO.getAssessment_id() + "";
                        }
                        String strThumbnail_select1 = "";
                        if (request.getAttribute("thumbnail_select1") != null) {
                            //show first thumbnail
                            strThumbnail_select1 = (String) request.getAttribute("thumbnail_select1");
                        } else {
                            //show first thumbnail
                            strThumbnail_select1 = "1";
                        }
                        Collection<ImagesListVO> ImagesListVO1 = manager.getImageList(strAssessment_select1, walocation.getWound_profile_type_id(), language);

                        request.setAttribute("assessment_select1", strAssessment_select1);
                        request.setAttribute("thumbnail_select1", strThumbnail_select1);
                        request.setAttribute("images_set1", ImagesListVO1);
                        String strAssessment_select2 = "";
                        if (request.getAttribute("assessment_select2") != null) {
                            //show images for past assessment
                            strAssessment_select2 = (String) request.getAttribute("assessment_select2");
                        } else if (assess != null && assess.size() >= 1) {
                            //show images for last assessment
                            strAssessment_select2 = lastAssessmentVO.getAssessment_id() + "";
                        }
                        String strThumbnail_select2 = "";
                        if (request.getAttribute("thumbnail_select2") != null) {
                            //show first thumbnail
                            strThumbnail_select2 = (String) request.getAttribute("thumbnail_select2");
                        } else {
                            //show first thumbnail
                            strThumbnail_select2 = "1";
                        }
                        Collection<ImagesListVO> ImagesListVO2 = manager.getImageList(strAssessment_select2, walocation.getWound_profile_type_id(), language);
                        request.setAttribute("assessment_select2", strAssessment_select2);
                        request.setAttribute("thumbnail_select2", strThumbnail_select2);
                        request.setAttribute("images_set2", ImagesListVO2);
                    }
                }
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ViewerSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        try {

//			 show patient name
            com.pixalere.patient.service.PatientServiceImpl managerp = new com.pixalere.patient.service.PatientServiceImpl(language);
            com.pixalere.patient.bean.PatientAccountVO patientVO = (com.pixalere.patient.bean.PatientAccountVO) managerp.getPatient(Integer.parseInt((String) patient_id));
            request.setAttribute("patientAccount", patientVO);
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ViewerSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {

            request.removeAttribute("populate_alphas");
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {

            request.removeAttribute("populate_alphas");
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("examflowchart")) {
            return (mapping.findForward("uploader.go.examflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {

            request.removeAttribute("populate_alphas");
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("visitsflowchart")) {
            return (mapping.findForward("uploader.go.visits"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {

            request.removeAttribute("populate_alphas");
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {

            request.removeAttribute("populate_alphas");
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {

            request.removeAttribute("populate_alphas");
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {

            request.removeAttribute("populate_alphas");
            return (mapping.findForward("uploader.go.admin"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("bradenflowchart")) {
            return (mapping.findForward("uploader.go.bradenflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbflowchart")) {
            return (mapping.findForward("uploader.go.limbflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("diagflowchart")) {
            return (mapping.findForward("uploader.go.diagflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("footflowchart")) {
            return (mapping.findForward("uploader.go.footflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("sensoryflowchart")) {
            return (mapping.findForward("uploader.go.sensoryflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("nursing_care_plan")) {
            return (mapping.findForward("uploader.go.nursing_care_plan"));
        } else {

            return (mapping.findForward("uploader.viewer.success"));
        }
    }
}
