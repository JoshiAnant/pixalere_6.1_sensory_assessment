package com.pixalere.struts.viewer;

import com.pixalere.common.DataAccessException;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.*;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import org.apache.struts.upload.MultipartRequestHandler;
import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.io.*;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;

public class IpadImageUpload extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(IpadImageUpload.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        String imageArray = "";
        String imageArray2 = "";

        IpadImageUploadForm assessmentEachwoundForm = (IpadImageUploadForm) form;

        Common common = new Common();
        String absolute_path = common.getPhotosPath();
        AssessmentImagesServiceImpl managerBD = new AssessmentImagesServiceImpl();
        WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
        
        try {
            if (assessmentEachwoundForm.getAction() != null && assessmentEachwoundForm.getAction().equals("infopop")) {
                //save image,, return pathname;
                FormFile image1 = (FormFile) assessmentEachwoundForm.getImageName();
                PDate pdate = new PDate();
                com.pixalere.utils.ImageManipulation image = new com.pixalere.utils.ImageManipulation( absolute_path);
                        if (image1 != null && !image1.getFileName().equals("")) {
                    
                    String error = image.processUploadedFile(image1, image1.getFileName(),assessmentEachwoundForm.getAction());
                    PrintWriter out = response.getWriter();
                    if (!error.equals("")) {
                        out.write("error: "+error);
                        //return (mapping.findForward("pixalere.error"));
                    }

                    out.write("<div id=\"image\">/photos/"+Common.getConfig("customer_id") +"/infopop/"+image1.getFileName()+"</div>");
                        }
                return null;
                } else {
                    int assessment_id = assessmentEachwoundForm.getAssessment_id();
                    int alpha_id = assessmentEachwoundForm.getAlpha_id();
                    int professional_id = assessmentEachwoundForm.getUser_id();

                    WoundAssessmentServiceImpl aservice = new WoundAssessmentServiceImpl();
                    ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
                    WoundAssessmentVO ass = new WoundAssessmentVO();
                    ass.setId(assessment_id);
                    WoundAssessmentVO assessment = aservice.getAssessment(ass);
                    if (assessment != null) {
                        int patient_id = assessment.getPatient_id();
                        int wound_id = assessment.getWound_id();


                        ProfessionalVO userVO = pservice.getProfessional(professional_id);
                        PDate pdate = new PDate();
                        String user_signature = pdate.getProfessionalSignature(new java.util.Date(), userVO, locale);
                        WoundAssessmentLocationVO location = (WoundAssessmentLocationVO) dao.findByPK(alpha_id);

                        com.pixalere.utils.ImageManipulation image = new com.pixalere.utils.ImageManipulation(patient_id + "", user_signature, absolute_path, assessment_id + "");
                        if (location != null) {
                            Enumeration e = request.getParameterNames();
                            while (e.hasMoreElements()) {
                                String name = (String) e.nextElement();
                                
                            }
                            int package_count = Integer.parseInt((String) request.getParameter("PackageFileCount"));
                            //Get Image from Form, and create serialized array for DB (legacy reasons)
                            long currenttime = pdate.getEpochTime();
                            MultipartRequestHandler handler = assessmentEachwoundForm.getMultipartRequestHandler();
                            Hashtable file = handler.getFileElements();
                            Enumeration files = file.keys();
                            int x = 0;
                            while (files.hasMoreElements()) {
                                String key = (String) files.nextElement();
                                FormFile image1 = (FormFile) file.get(key);
                                if (image1 != null) {


                                    if (image1 != null && !image1.getFileName().equals("")) {
                                        imageArray = currenttime + "_" + location.getAlpha() + "-" + (x + 1) + ".jpg";
                                        String error = image.processUploadedFile(image1, "" + currenttime + "_" + location.getAlpha() + "-" + (x + 1) + ".jpg", userVO.getId().intValue());
                                        if (!error.equals("")) {
                                            ActionErrors errors = new ActionErrors();
                                            request.setAttribute("exception", "y");

                                            String exception = error;
                                            request.setAttribute("exception_log", exception);
                                            errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                                            saveErrors(request, errors);
                                            //return (mapping.findForward("pixalere.error"));
                                        }
                                    }

                                    AssessmentImagesVO ig = new AssessmentImagesVO();
                                    ig.setAssessment_id(new Integer(assessment_id));
                                    ig.setAlpha_id(alpha_id);
                                    ig.setWhichImage(x + 1);
                                    AssessmentImagesVO imagesVO = managerBD.getImage(ig);
                                    if (imagesVO == null) {
                                        imagesVO = new AssessmentImagesVO();
                                        imagesVO.setAssessment_id(assessment_id);
                                        imagesVO.setAlpha_id(alpha_id);
                                        imagesVO.setWound_profile_type_id(location.getWound_profile_type_id());
                                        imagesVO.setActive(new Integer(1));
                                        imagesVO.setWound_id(location.getWound_id());
                                        imagesVO.setWhichImage(x + 1);
                                        imagesVO.setPatient_id(patient_id);
                                    }
                                    imagesVO.setImages(imageArray);
                                    if (!imageArray.equals("")) {
                                        managerBD.saveImage(imagesVO);
                                    }
                                }

                                x++;
                            }
                        }
                    }
                }
            }  catch (DataAccessException e) {
            e.printStackTrace();
            log.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            e.printStackTrace();
            log.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }catch (IOException e) {
            e.printStackTrace();
            log.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("uploader.success"));

    }

    public void processUploadedFile(FormFile file, String fileName, String patient_id, String assessment_id, String path_absolute) {
        try {

            InputStream stream = file.getInputStream();
            OutputStream bos = new FileOutputStream(path_absolute + "/live/" + patient_id + "/" + assessment_id + "/" + fileName);

            int bytesRead = 0;

            byte[] buffer = new byte[8192];

            while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);


            }

            bos.close();

            // close the stream
            stream.close();

        } catch (Exception ex) {
            log.error("ImageUpload.perform: " + ex.getMessage());
            ex.printStackTrace();


        }
    }
}
