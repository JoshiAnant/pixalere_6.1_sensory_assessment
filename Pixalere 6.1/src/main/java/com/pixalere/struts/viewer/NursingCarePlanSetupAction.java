package com.pixalere.struts.viewer;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.utils.*;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.assessment.bean.NursingCarePlanVO;

import java.util.Vector;
import java.util.Collection;
import java.util.Hashtable;

public class NursingCarePlanSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NursingCarePlanSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
           if(session.getAttribute("patient_id") == null) {
              return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {
              return (mapping.findForward("uploader.go.viewer"));
            }
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp2 = new com.pixalere.common.bean.ComponentsVO();
            comp2.setFlowchart(com.pixalere.utils.Constants.NURSING_CARE_PLAN);
            Collection components2 = gui.getAllComponents(comp2);
            java.util.Hashtable hashized2 = Common.hashComponents(components2);
            request.setAttribute("componentsTreatment", hashized2);

            
            NursingCarePlanServiceImpl eachwoundBD = new NursingCarePlanServiceImpl();
            AssessmentServiceImpl assessmentBD = new AssessmentServiceImpl(language);
           
            WoundAssessmentLocationServiceImpl walBD = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
            walocation.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
            WoundAssessmentLocationVO location = walBD.getAlpha(walocation);
            walocation.setDischarge(0);
            walocation.setActive(1);
            Vector<WoundAssessmentLocationVO> alphas = walBD.getAllAlphas(walocation);

            int start_date = -1;
            int end_date = -1;
            if (request.getParameter("start_date") != null) {
              start_date = Integer.parseInt((String) request.getParameter("start_date"));
            }
            if (request.getParameter("end_date") != null) {
              end_date = Integer.parseInt((String)  request.getParameter("end_date"));
            }
            request.setAttribute("start_date", start_date);
            request.setAttribute("end_date", end_date);

            request.setAttribute("page", "nursingcareplan");
            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("bath",listBD.getLists(LookupVO.BATH));
            request.setAttribute("dressing_change_frequency", listBD.getLists(LookupVO.DRESSING_CHANGE_FREQUENCY));
            request.setAttribute("nursing_visit_frequency", listBD.getLists(LookupVO.NURSING_VISIT_FREQUENCY));
            DressingChangeFrequencyVO dcftmp = new DressingChangeFrequencyVO();
            dcftmp.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
            dcftmp.setActive(1);
            Collection<DressingChangeFrequencyVO> lastDcfs = eachwoundBD.getAllDressingChangeFrequencies(dcftmp, 0);
            request.setAttribute("dcfs", lastDcfs);
            
            NursingCarePlanVO ncp = new NursingCarePlanVO();
            ncp.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
            ncp.setActive(1);
            Collection<NursingCarePlanVO> lastNursing = eachwoundBD.getAllNursingCarePlans(ncp, 0);
            Vector newcarePlans = new Vector();
            if (start_date>0 || end_date>0) {
              for (NursingCarePlanVO n : lastNursing) {
                try {
                   if (n.getId()>=start_date && n.getId()<=end_date) {
                       newcarePlans.add(n);
                   }
                } catch (NumberFormatException e) {
                    request.setAttribute("ncps", lastNursing);
                }
             }
             request.setAttribute("ncps", newcarePlans);
            } else {
                request.setAttribute("ncps", lastNursing);
            }
            request.setAttribute("assessment_list", lastNursing);

            DressingChangeFrequencyVO dcf = new DressingChangeFrequencyVO();
            dcf.setWound_profile_type_id(new Integer(wound_profile_type_id));
            //cycling current alphas.. (active).. get last dcf for each.
            Vector allDCFResults = new Vector();
            Vector allDCFValueResults = new Vector();
            Vector justAlphas = new Vector();

            for (WoundAssessmentLocationVO alpha : alphas) {
                String display_alpha = alpha.getAlpha();
                display_alpha = Common.getAlphaText(display_alpha,locale);
                justAlphas.add(display_alpha);

                if (session.getAttribute("ncp") == null) {
                    dcf.setAlpha_id(alpha.getId());
                    DressingChangeFrequencyVO newDCF = eachwoundBD.getDressingChangeFrequency(dcf);
                    if (newDCF != null) {
                        LookupVO l = listBD.getListItem(newDCF.getDcf());
                        if (l != null) {
                            allDCFResults.add(Common.getAlphaText(newDCF.getAlphaLocation().getAlpha(),locale) + ": " + l.getName(language));
                            allDCFValueResults.add(Common.getAlphaText(newDCF.getAlphaLocation().getAlpha(),locale) + ": " + newDCF.getDcf());
                        }
                    }
                }
            }
            if (session.getAttribute("ncp") != null) {
                Hashtable ncpHash = (Hashtable) session.getAttribute("ncp");
                if (ncpHash != null && ncpHash.get("dcf") != null) {
                    ArrayList<DressingChangeFrequencyVO> arrayList = (ArrayList) ncpHash.get("dcf");
                    for (DressingChangeFrequencyVO d : arrayList) {
                        WoundAssessmentLocationVO aTMP = new WoundAssessmentLocationVO();
                        aTMP.setId(d.getAlpha_id());
                        WoundAssessmentLocationVO alpha = walBD.getAlpha(aTMP);
                        LookupVO dlist = listBD.getListItem(d.getDcf());
                        allDCFResults.add(Common.getAlphaText(alpha.getAlpha(),locale) + ": " + dlist.getName(language));
                        allDCFValueResults.add(Common.getAlphaText(alpha.getAlpha(),locale) + ": " + d.getDcf());
                    }
                }
            }
            request.setAttribute("justAlpha", justAlphas);
            request.setAttribute("unserializedDCFresults", allDCFResults);
            request.setAttribute("unserializedDCFValueresults", allDCFValueResults);
            if (session.getAttribute("ncp") == null) {
                for (NursingCarePlanVO n : lastNursing) {
                    request.setAttribute("visit_frequency", n.getVisit_frequency());
                    request.setAttribute("as_needed",n.getAs_needed());
                    if (request.getParameter("populate") != null && ((String) request.getParameter("populate")).equals("1")) {
                        request.setAttribute("popNCP", Common.convertCarriageReturns(n.getBody(), true));
                    }
                    break;// break after grabbing first NCP;
                }
            } else {
                Hashtable ncpHash = (Hashtable) session.getAttribute("ncp");
                if (ncpHash != null) {
                    request.setAttribute("popNCP", Common.convertCarriageReturns((String)ncpHash.get("ncp"),true));
                    request.setAttribute("visit_frequency", ncpHash.get("vf"));
                    request.setAttribute("as_needed",ncpHash.get("as_needed"));
                }
            }
            if(location != null){
                //NursingCarePlanVO body = eachwoundBD.getNursingCarePlan(ncp);
                com.pixalere.wound.service.WoundServiceImpl wp = new com.pixalere.wound.service.WoundServiceImpl(language);
                WoundProfileVO wpVO = new WoundProfileVO();
                wpVO.setWound_id(location.getWound_id());
                com.pixalere.wound.bean.WoundProfileVO wpvo = (com.pixalere.wound.bean.WoundProfileVO) wp.getWoundProfile(wpVO);
                if(wpvo!=null){
                    request.setAttribute("woundprofile", wpvo.getWound().getWound_location_detailed());
                }
            }
            // populate wound profile dropdown
            com.pixalere.wound.service.WoundServiceImpl manager = new com.pixalere.wound.service.WoundServiceImpl(language);
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");

            Vector woundProfilesDrop = manager.getWoundProfilesDropdown(Integer.parseInt((String) patient_id), false, userVO.getId().intValue());
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in TreatmentSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("viewer.ncp.success"));
    }
}
