package com.pixalere.struts.viewer;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

public class MeasurementsDiagramForm
    extends ActionForm {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.
      getLogger(MeasurementsDiagramForm.class);
  public ActionErrors validate(ActionMapping mapping,
                               HttpServletRequest request) {
	
    ActionErrors errors = new ActionErrors();
    
    return errors;

  }

  public void reset(ActionMapping mapping, HttpServletRequest request) {
  }

  public String getAlpha_id() {
    return alpha_id;
  }

  public void setAlpha_id(String alpha_id) {
    this.alpha_id = alpha_id;
  }

  public String getStartDate() {
    return start_date;
  }

  public void setStartDate(String start_date) {
    this.start_date = start_date;
  }

  public String getEndDate() {
    return end_date;
  }

  public void setEndDate(String end_date) {
    this.end_date = end_date;
  }

  private String alpha_id;
  private String alpha_type;
  private String start_date;
  private String end_date;

    public String getAlpha_type() {
        return alpha_type;
    }

    public void setAlpha_type(String alpha_type) {
        this.alpha_type = alpha_type;
    }

}
