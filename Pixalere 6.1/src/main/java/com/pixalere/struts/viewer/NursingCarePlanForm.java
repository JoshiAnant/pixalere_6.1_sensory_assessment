package com.pixalere.struts.viewer;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

public class NursingCarePlanForm extends ActionForm {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NursingCarePlanForm.class);

  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    return errors;
  }

  public void reset(ActionMapping mapping, HttpServletRequest request) {
  }
  private String[] dressing_change_frequency;
  private int visit_freq;
  private int as_needed;
  private String body;
  private int deleted;
    private int id;
    private String page;
    private String delete_reason;
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
public String[] getDressing_change_frequency() {
        return dressing_change_frequency;
    }
public String getPage(){
        return page;
    }
    public void setPage(String page){
        this.page=page;
    }
    public void setDressing_change_frequency(String[] dressing_change_frequency) {
        this.dressing_change_frequency = dressing_change_frequency;
    }
        public int getVisit_freq() {
        return visit_freq;
    }

    public void setVisit_freq(int visit_freq) {
        this.visit_freq = visit_freq;
    }
  /**
     * @return the deleted
     */
    public int getDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    /**
     * @return the delete_reason
     */
    public String getDelete_reason() {
        return delete_reason;
    }

    /**
     * @param delete_reason the delete_reason to set
     */
    public void setDelete_reason(String delete_reason) {
        this.delete_reason = delete_reason;
    }

    /**
     * @return the as_needed
     */
    public int getAs_needed() {
        return as_needed;
    }

    /**
     * @param as_needed the as_needed to set
     */
    public void setAs_needed(int as_needed) {
        this.as_needed = as_needed;
    }
}