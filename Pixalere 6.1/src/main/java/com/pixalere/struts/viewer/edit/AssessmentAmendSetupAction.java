package com.pixalere.struts.viewer.edit;

import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.util.Vector;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

public class AssessmentAmendSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentAmendSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {

            String strId = (String) request.getParameter("id");
            String strAction = (String) request.getParameter("action");
            String strType = (String) request.getParameter("type");
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            
            if (strId != null && strId != "" && strAction != null && strAction != "" && strType != null && strType != "") {

                request.setAttribute("action", strAction);
                request.setAttribute("id", strId);
                request.setAttribute("type", strType);
                if (strAction.equals("delete")) {
                    request.setAttribute("title", "DELETE ASSESSMENT");

                    Vector vecActionDetails = new Vector();
                    AbstractAssessmentVO assessment_details = new AbstractAssessmentVO();
                    Collection assessments_per_caretype = null;

                    if (strType.equals(Constants.WOUND_PROFILE_TYPE)) {
                        AssessmentEachwoundVO searchVO1 = new AssessmentEachwoundVO();
                        searchVO1.setId(Integer.parseInt(strId));
                        assessment_details = (AbstractAssessmentVO) manager.getAssessment(searchVO1);
                        AssessmentEachwoundVO searchVO2 = new AssessmentEachwoundVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                    }
                    if (strType.equals(Constants.OSTOMY_PROFILE_TYPE)) {
                        AssessmentOstomyVO searchVO1 = new AssessmentOstomyVO();
                        searchVO1.setId(Integer.parseInt(strId));
                        assessment_details = (AbstractAssessmentVO) manager.getAssessment(searchVO1);

                        AssessmentOstomyVO searchVO2 = new AssessmentOstomyVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                    }
                    if (strType.equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                        AssessmentIncisionVO searchVO1 = new AssessmentIncisionVO();
                        searchVO1.setId(Integer.parseInt(strId));
                        assessment_details = (AbstractAssessmentVO) manager.getAssessment(searchVO1);

                        AssessmentIncisionVO searchVO2 = new AssessmentIncisionVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                    }
                    if (strType.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                        AssessmentDrainVO searchVO1 = new AssessmentDrainVO();
                        searchVO1.setId(Integer.parseInt(strId));
                        assessment_details = (AbstractAssessmentVO) manager.getAssessment(searchVO1);

                        AssessmentDrainVO searchVO2 = new AssessmentDrainVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                    }
                    if (strType.equals(Constants.BURN_PROFILE_TYPE)) {
                        AssessmentBurnVO searchVO1 = new AssessmentBurnVO();
                        searchVO1.setId(Integer.parseInt(strId));
                        assessment_details = (AbstractAssessmentVO) manager.getAssessment(searchVO1);

                        AssessmentBurnVO searchVO2 = new AssessmentBurnVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                    }
                    if (strType.equals(Constants.SKIN_PROFILE_TYPE)) {
                        AssessmentSkinVO searchVO1 = new AssessmentSkinVO();
                        searchVO1.setId(Integer.parseInt(strId));
                        assessment_details = (AbstractAssessmentVO) manager.getAssessment(searchVO1);

                        AssessmentSkinVO searchVO2 = new AssessmentSkinVO();
                        searchVO2.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO2.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        assessments_per_caretype = manager.getAllAssessments(searchVO2, 0, false);
                    }

                    request.setAttribute("assessmentid", assessment_details.getAssessment_id());

                    vecActionDetails.add(Common.getLocalizedString("pixalere.alert.assessment_treatment",locale) + " " + Common.getAlphaText(assessment_details.getAlphaLocation().getAlpha(),locale) + " " + assessment_details.getWoundAssessment().getUser_signature());

                    AssessmentImagesServiceImpl images_manager = new AssessmentImagesServiceImpl();
                    AssessmentImagesVO searchVO3 = new AssessmentImagesVO();
                    searchVO3.setAssessment_id(assessment_details.getAssessment_id());
                    searchVO3.setAlpha_id(assessment_details.getAlpha_id());
                    Collection assessment_images = images_manager.getAllImages(searchVO3);
                    if (assessment_images != null && assessment_images.size() > 0) {
                        vecActionDetails.add(assessment_images.size() + " " + Common.getLocalizedString("pixalere.attached_images",locale));
                    }

                    if (assessments_per_caretype.size() == 1) {
                        // There's only one assessment for this caretype, so deal with comments etc too
                        request.setAttribute("caretypeid", assessment_details.getWound_profile_type_id());


                        AssessmentCommentsServiceImpl comments_manager = new AssessmentCommentsServiceImpl();
                        AssessmentCommentsVO searchVO4 = new AssessmentCommentsVO();
                        searchVO4.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO4.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        AssessmentCommentsVO assessment_comments = (AssessmentCommentsVO) comments_manager.getComment(searchVO4);
                        if (assessment_comments != null) {
                            vecActionDetails.add(Common.getLocalizedString("pixalere.viewer.form.comment_body",locale) + " " + assessment_comments.getUser_signature());
                        }
                        
                        ReferralsTrackingServiceImpl referrals_manager = new ReferralsTrackingServiceImpl(language);
                        ReferralsTrackingVO searchVO6 = new ReferralsTrackingVO();
                        searchVO6.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO6.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        searchVO6.setCurrent(new Integer(1));
                        Collection<ReferralsTrackingVO> assessment_referrals = referrals_manager.getAllReferralsByCriteria(searchVO6);
                        if (assessment_referrals != null) {
                            int pending_referral = 0;
                            int pending_recommendation = 0;
                            int acknowledged_recommendation = 0;
                            for (Iterator referrals = assessment_referrals.iterator(); referrals.hasNext();) {
                                ReferralsTrackingVO assessment_referral = (ReferralsTrackingVO) referrals.next();
                                if (assessment_referral.getEntry_type().equals(Constants.REFERRAL)) pending_referral++;
                                if (assessment_referral.getEntry_type().equals(Constants.RECOMMENDATION))
                                    pending_recommendation++;
                                if (assessment_referral.getEntry_type().equals(Constants.ACKNOWLEDGE_RECOMMENDATION))
                                    acknowledged_recommendation++;
                            }
                            if (pending_referral > 0)
                                vecActionDetails.add(pending_referral + " " + Common.getLocalizedString("pixalere.amendments.form.pending_referral",locale));
                            if (pending_recommendation > 0)
                                vecActionDetails.add(pending_referral + " " + Common.getLocalizedString("pixalere.amendments.form.pending_recommendation",locale));
                            if (acknowledged_recommendation > 0)
                                vecActionDetails.add(pending_referral + " " + Common.getLocalizedString("pixalere.amendments.form.acknowledged_recommendation",locale));
                        }
                        TreatmentCommentsServiceImpl treatmentcomment_manager = new TreatmentCommentsServiceImpl();
                        TreatmentCommentVO searchVO7 = new TreatmentCommentVO();
                        searchVO7.setAssessment_id(assessment_details.getAssessment_id());
                        searchVO7.setWound_profile_type_id(assessment_details.getWound_profile_type_id());
                        TreatmentCommentVO assessment_treatmentcomments = (TreatmentCommentVO) treatmentcomment_manager.getTreatmentComment(searchVO7);
                        if (assessment_treatmentcomments != null) {
                            vecActionDetails.add(Common.getLocalizedString("pixalere.treatment.form.treatment_comments",locale) + " " + assessment_treatmentcomments.getUser_signature());
                        }
                    } else {
                        request.setAttribute("caretypeid", 0); // Do not remove details per caretype
                    }
                    request.setAttribute("actiondetails", vecActionDetails);
                    String strAuditLog = "";
                    for (int intDetails = 0; intDetails < vecActionDetails.size(); intDetails++) {
                        if (strAuditLog != "") strAuditLog = strAuditLog + ", ";
                        strAuditLog = strAuditLog + vecActionDetails.get(intDetails);
                    }
                    request.setAttribute("auditlog", strAuditLog);
                } else if (strAction.equals("backdate")) {
                    Vector vecActionDetails = new Vector();
                    AbstractAssessmentVO assessment_details = new AbstractAssessmentVO();
                    AssessmentEachwoundVO searchVO1 = new AssessmentEachwoundVO();
                    searchVO1.setId(Integer.parseInt(strId));
                    assessment_details = (AbstractAssessmentVO) manager.getAssessment(searchVO1);
                    vecActionDetails.add(Common.getLocalizedString("pixalere.login.form.assess", locale)+": "+assessment_details.getWoundAssessment().getUser_signature());

                    request.setAttribute("actiondetails", vecActionDetails);
                    String strAuditLog = Common.getLocalizedString("pixalere.login.form.assess", locale)+": "+assessment_details.getWoundAssessment().getUser_signature();

                    request.setAttribute("auditlog", strAuditLog);
                }


            }

        } catch (Exception e) {
            log.error("An application exception has been raised in AssessmentAmendSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("uploader.assessmentamend.success"));
    }

}