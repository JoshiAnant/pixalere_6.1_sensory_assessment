package com.pixalere.struts.viewer;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Collection;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Common;
import com.pixalere.guibeans.RowData;


public class WoundFlowchartSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundFlowchartSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        try {
            ProfessionalVO currentProfessional = (ProfessionalVO) session.getAttribute("userVO");
            request.setAttribute("page", "woundflowchart");
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {

                return (mapping.findForward("uploader.go.viewer"));
            }
            request.setAttribute("act", "WoundFlowchartSetup.do");
            WoundAssessmentLocationServiceImpl wal = new WoundAssessmentLocationServiceImpl();
            AssessmentServiceImpl amanager = new AssessmentServiceImpl(language);
            WoundServiceImpl manager = new WoundServiceImpl(language);
            WoundProfileTypeVO walocation = new WoundProfileTypeVO();
            walocation.setId(new Integer((String) wound_profile_type_id));
            WoundProfileTypeVO location = (WoundProfileTypeVO) manager.getWoundProfileType(walocation);

            WoundProfileVO vo = new WoundProfileVO();
            vo.setWound_id(location.getWound_id());
            
            String id = "";
            int limit = 3;
            if (request.getParameter("limit") != null) {
                limit = Integer.parseInt((String) request.getParameter("limit"));
            }
            WoundProfileVO crit = new WoundProfileVO();
            crit.setWound_id(location.getWound_id());
            crit.setActive(1);
            Collection<WoundProfileVO> items = null;
            if (limit != 0) {
                items = manager.getWoundProfiles(crit, limit);
            }
            //if item selected from dropdown pretend to list
            if (request.getParameter("id") != null) {
                id = (String) request.getParameter("id");
                request.setAttribute("selectedid", (String) request.getParameter("id"));
                WoundProfileVO criteria = new WoundProfileVO();
                criteria.setId(new Integer(id));
                WoundProfileVO item = manager.getWoundProfile(criteria, Constants.SHOW_DELETED);
                Collection<WoundProfileVO> tmp = new ArrayList();
                //if(item!=null){tmp.add(item);}
                int count = 0;
                for (WoundProfileVO wp : items) {
                    count++;
                    if (count != items.size()) {
                        tmp.add(wp);
                    }
                }
                if(items.size()>2){
                    tmp.add(item);
                    items = tmp;
                }

            }
            //get primary type for components
            WoundProfileVO primary = manager.getWoundProfile(crit);

            RowData[] results = manager.getAllWoundProfilesForFlowchart(items, currentProfessional, true,true);
            request.setAttribute("results", results);
            request.setAttribute("error_class", "AssessmentErrorSetup");
            request.setAttribute("dropdown", manager.getAllSignatures(location.getWound_id().intValue(), limit));

            // populate wound profile dropdown
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
            Vector woundProfilesDrop = manager.getWoundProfilesDropdown(Integer.parseInt((String) patient_id), false, userVO.getId().intValue());
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);
            request.setAttribute("page", "woundflowchart");

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientFlowchartSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null) {
            if (request.getParameter("page").equals("patient")) {
                return (mapping.findForward("uploader.go.patient"));
            } else if (request.getParameter("page").equals("woundprofiles")) {
                return (mapping.findForward("uploader.go.profile"));
            } else if (request.getParameter("page").equals("assess")) {
                return (mapping.findForward("uploader.go.assess"));
            } else if (request.getParameter("page").equals("treatment")) {
                return (mapping.findForward("uploader.go.treatment"));
            } else if (request.getParameter("page").equals("viewer")) {
                return (mapping.findForward("uploader.go.viewer"));
            } else if (request.getParameter("page").equals("summary")) {
                return (mapping.findForward("uploader.go.summary"));
            } else if (request.getParameter("page").equals("reporting")) {
                return (mapping.findForward("uploader.go.reporting"));
            } else if (request.getParameter("page").equals("patientflowchart")) {
                return (mapping.findForward("uploader.go.patientflowchart"));
            } else if (request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            } else if (request.getParameter("page").equals("assessflowchart")) {
                return (mapping.findForward("uploader.go.assessflowchart"));
            } else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            }else if (request.getParameter("page").equals("mdiagram")) {
                return (mapping.findForward("uploader.go.mdiagram"));
            } else if (request.getParameter("page").equals("bradenflowchart")) {
                return (mapping.findForward("uploader.go.bradenflowchart"));
            } else if (request.getParameter("page").equals("limbflowchart")) {
                return (mapping.findForward("uploader.go.limbflowchart"));
            } else if (request.getParameter("page").equals("footflowchart")) {
                return (mapping.findForward("uploader.go.footflowchart"));
            } else if (request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            } else if (request.getParameter("page").equals("comments")) {
                return (mapping.findForward("uploader.go.comments"));
            } else if (request.getParameter("page").equals("admin")) {
                log.info("############$$$$$$$$$$$$################# Page: " + request.getParameter("page"));
                return (mapping.findForward("uploader.go.admin"));
            } else if (request.getParameter("page").equals("nursingcareplan")) {
                return (mapping.findForward("uploader.go.nursingcareplan"));
            } else {
                return (mapping.findForward("uploader.wpflowchart.success"));
            }
        } else {
            return (mapping.findForward("uploader.wpflowchart.success"));
        }

    }
}
