package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.bean.ReferralsTrackingVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Vector;
import com.pixalere.utils.Common;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.wound.service.WoundServiceImpl;

public class CommentsSetupAction extends Action {    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CommentsSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        
        try {
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {

                return (mapping.findForward("uploader.go.viewer"));
            }
            WoundAssessmentServiceImpl manager = new WoundAssessmentServiceImpl();
            AssessmentCommentsServiceImpl cmanager = new AssessmentCommentsServiceImpl();
            WoundAssessmentLocationServiceImpl walmanager = new WoundAssessmentLocationServiceImpl();
            request.setAttribute("page","comments");
            com.pixalere.wound.service.WoundServiceImpl wp = new com.pixalere.wound.service.WoundServiceImpl(language);
            WoundProfileVO wpVO=new WoundProfileVO();
            WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
            walocation.setWound_profile_type_id(new Integer(wound_profile_type_id));
            WoundAssessmentLocationVO location = (WoundAssessmentLocationVO) walmanager.getAlpha(walocation, true, false, null);
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            boolean isWCC = Common.isSpecialist(userVO.getPosition());

            if(isWCC == true){
                request.setAttribute("isSpecialist","1");
            }else{
                request.setAttribute("isSpecialist","0");
            }
            //Generate Token - prevent multiple posts
            saveToken(request);
            wpVO.setWound_id(location.getWound_id());
            com.pixalere.wound.bean.WoundProfileVO wpvo = (com.pixalere.wound.bean.WoundProfileVO) wp.getWoundProfile(wpVO);
            ReferralsTrackingServiceImpl ref = new ReferralsTrackingServiceImpl(language);
            
            String start_date = "";
            String end_date = "";
            if(patient_id==null){
                return (mapping.findForward("uploader.null.patient"));
            }
            
            if (request.getParameter("start_date")!=null) {
                start_date = (String) request.getParameter("start_date");
            }
            if (request.getParameter("end_date")!=null) {
                end_date = (String) request.getParameter("end_date");
            }

            WoundAssessmentVO wat = new WoundAssessmentVO();
            wat.setWound_id(location.getWound_id());
            Collection<WoundAssessmentVO> assess = manager.getAllAssessments(wat);
            if (Common.getConfig("showLast4MonthsOfComments").equals("1") && request.getParameter("start_date") == null) {
                for (WoundAssessmentVO w : assess) {
                    if (w.getCreated_on().before(PDate.subtractDays(new java.util.Date(), 120))) {
                        start_date = w.getId() + "";
                    }
                    end_date = w.getId() + "";
                }
                //start_date = (pdate.getEpochTime() - Constants.FOUR_MONTHS) + "";
                //end_date = pdate.getEpochTime() + "";
            }
            request.setAttribute("start_date", start_date);
            request.setAttribute("end_date", end_date);
            


            /*while(iter.hasNext()){
                WoundAssessmentVO w = (WoundAssessmentVO)iter.next();
                AssessmentCommentsVO ctmp = new AssessmentCommentsVO();
                AssessmentRecommendationsVO rtmp=new AssessmentRecommendationsVO();
                ctmp.setAssessment_id(w.getId());
                rtmp.setAssessment_id(w.getId());
                //AssessmentCommentsVO c = cmanager.getComment(ctmp);
                //AssessmentRecommendationsVO r = rmanager.getRecommendation(rtmp);
                //if(c != null || r != null){
                    

                        comms.add(w);

                //}
            } */
            AbstractAssessmentVO vo = new AbstractAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setWound_profile_type_id(new Integer(wound_profile_type_id));
            //vo.setAssessment_id(new Integer((String)request.getAttribute("assessment_id")));

            Vector assessments = cmanager.getAllComments(assess,vo, (String) session.getAttribute("professional_id"), start_date, end_date,Common.getLanguageLocale(language));
            request.setAttribute("comments", assessments);


            Hashtable hash = ref.getAllReferralsTracking(location.getWound_id().intValue(),new Integer(wound_profile_type_id).intValue());

            request.setAttribute("referrals", hash);
            if(wpvo!=null){
                request.setAttribute("woundprofile", wpvo.getWound().getWound_location_detailed());
            }
            //populate dropdowns on top
            //Collection assessComments=manager.retrieveAllWoundAssessments(waVO);
            request.setAttribute("assessments_list", assess);
            
            WoundAssessmentVO woundAssess = manager.getAssessment(wat);
            if(woundAssess!=null){
                request.setAttribute("woundAssessment",woundAssess);
                request.setAttribute("woundTimestamp",pdate.getDateStringWithHour(woundAssess.getCreated_on(),Common.getLanguageLocale(language)));
                ReferralsTrackingVO refVO = new ReferralsTrackingVO();
                refVO.setCurrent(new Integer(1));
                refVO.setEntry_type(com.pixalere.utils.Constants.REFERRAL); 
                refVO.setWound_profile_type_id(new Integer(wound_profile_type_id));
                if(Common.getConfig("allowReferralByPositions").equals("1")){
                    refVO.setAssigned_to(userVO.getPosition_id());
                }
                ReferralsTrackingServiceImpl rrmanager = new ReferralsTrackingServiceImpl(language);
                request.setAttribute("refs", rrmanager.getAllReferralsByCriteria(refVO));
                request.setAttribute("assessment_id", woundAssess.getId());
            }
            
            //comments found in session need to be restored
            if(session.getAttribute("savedComments")!=null){
                Hashtable tmpH=(Hashtable)session.getAttribute("savedComments");
                request.setAttribute("viewerComment",tmpH.get("comment"));
                request.setAttribute("viewerRecommendation",tmpH.get("recommendation"));
                request.setAttribute("viewerReferral",tmpH.get("referral"));
            }
            WoundAssessmentLocationVO wavo = new WoundAssessmentLocationVO();
            wavo.setWound_profile_type_id(Integer.parseInt(wound_profile_type_id));
            wavo.setActive(new Integer(1));
            // populate wound profile dropdown
            
            Vector woundProfilesDrop = wp.getWoundProfilesDropdown(Integer.parseInt(patient_id), false, userVO
                    .getId().intValue());
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);
            
            Vector locations = walmanager.getAllAlphas(wavo);
            request.setAttribute("alphas", locations);
            request.setAttribute("page", "comments");
            
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in CommentsSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        if (request.getParameter("page")!=null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("patientflowchart")) {
            return (mapping.findForward("uploader.go.patientflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            }else if (request.getParameter("page")!=null && request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundflowchart")) {
            return (mapping.findForward("uploader.go.woundflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            }else if (request.getParameter("page")!=null && request.getParameter("page").equals("assessflowchart")) {
            return (mapping.findForward("uploader.go.assessflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("mdiagram")) {
            return (mapping.findForward("uploader.go.mdiagram"));
        }else if (request.getParameter("page")!=null && request.getParameter("page").equals("comments")) {
            return (mapping.findForward("uploader.go.comments"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("producthistory")) {
            return (mapping.findForward("uploader.go.prodhistory"));
        }else if (request.getParameter("page")!=null && request.getParameter("page").equals("nursingcareplan")) {
            return (mapping.findForward("uploader.go.nursingcareplan"));
        }else if (request.getParameter("page")!=null && request.getParameter("page").equals("bradenflowchart")) {
            return (mapping.findForward("uploader.go.bradenflowchart"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("limbflowchart")) {
            return (mapping.findForward("uploader.go.limbflowchart"));
        }else if (request.getParameter("page")!=null && request.getParameter("page").equals("footflowchart")) {
            return (mapping.findForward("uploader.go.footflowchart"));
        }else if (request.getParameter("page")!=null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        }else{
            return (mapping.findForward("uploader.comments.success"));
        }
    }
    
    
}