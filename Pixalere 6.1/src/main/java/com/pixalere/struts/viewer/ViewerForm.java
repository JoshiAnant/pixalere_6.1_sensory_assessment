package com.pixalere.struts.viewer;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

public class ViewerForm extends ActionForm {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ViewerForm.class);

  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    return errors;
  }

  public void reset(ActionMapping mapping, HttpServletRequest request) {
  }

  
  private String assessment_select1;
  private String assessment_select2;
  private String thumbnail_select1;
  private String thumbnail_select2;
  
  public void setAssessment_select1(String assessment_select1){
	this.assessment_select1=assessment_select1;
     }
 public String getAssessment_select1(){
	return assessment_select1;
    }
    public void setAssessment_select2(String assessment_select2){
	   this.assessment_select2=assessment_select2;
    }
    public String getAssessment_select2(){
    	return assessment_select2;
    }
    public void setThumbnail_select1(String thumbnail_select1){
	   this.thumbnail_select1=thumbnail_select1;
     }
    public String getThumbnail_select1(){
	   return thumbnail_select1;
    }
    public void setThumbnail_select2(String thumbnail_select2){
	   this.thumbnail_select2=thumbnail_select2;
    }
    public String getThumbnail_select2(){
    	return thumbnail_select2;
    }

}
