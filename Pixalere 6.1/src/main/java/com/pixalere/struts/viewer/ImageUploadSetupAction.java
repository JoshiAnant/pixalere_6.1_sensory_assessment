package com.pixalere.struts.viewer;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.utils.Serialize;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ImageUploadSetupAction extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImageUploadSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        try {
            AssessmentImagesServiceImpl manager = new AssessmentImagesServiceImpl();
            WoundAssessmentLocationServiceImpl almanager = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
            if (request.getParameter("alpha_id") != null) {
                wal.setId(new Integer(request.getParameter("alpha_id")));
                WoundAssessmentLocationVO alpha = almanager.getAlpha(wal);
                request.setAttribute("assess_id", request.getParameter("assess_id"));
                request.setAttribute("assessment_id", request.getParameter("assess_id"));

                request.setAttribute("alpha_id", request.getParameter("alpha_id"));
                AssessmentImagesVO ig = new AssessmentImagesVO();
                ig.setAssessment_id(new Integer((String) request.getParameter("assess_id")));
                ig.setAlpha_id(new Integer((String) request.getParameter("alpha_id")));
                ig.setWhichImage(Constants.IMAGE_1);
                com.pixalere.assessment.bean.AssessmentImagesVO image = manager.getImage(ig);
                ig.setWhichImage(Constants.IMAGE_2);
                com.pixalere.assessment.bean.AssessmentImagesVO extraImage = manager.getImage(ig);
                
                ig.setWhichImage(Constants.IMAGE_3);
                com.pixalere.assessment.bean.AssessmentImagesVO extraImage2 = manager.getImage(ig);
                if (alpha != null) {
                    request.setAttribute("selected_alpha", alpha.getAlpha());
                }
                if (image != null && image.getImages() != null) {

                    request.setAttribute("image_array", image.getImages());
                    if (request.getParameter("delete") != null) {

                        if (((String) request.getParameter("delete")).equals("image")) {
                            image.setImages("");
                            request.setAttribute("image_array", "");
                            manager.saveImage(image);
                        }
                    }
                }
                if (extraImage != null && extraImage.getImages() != null) {
                    request.setAttribute("image_array2", extraImage.getImages());
                    if (request.getParameter("delete") != null) {
                        if (((String) request.getParameter("delete")).equals("imageExtra")) {
                            extraImage.setImages("");
                            request.setAttribute("image_array2", "");
                            manager.saveImage(extraImage);
                        }
                    }
                }
                if (extraImage2 != null && extraImage2.getImages() != null) {
                    request.setAttribute("image_array3", extraImage2.getImages());
                    if (request.getParameter("delete") != null) {
                        if (((String) request.getParameter("delete")).equals("imageExtra2")) {
                            extraImage2.setImages("");
                            request.setAttribute("image_array3", "");
                            manager.saveImage(extraImage2);
                        }
                    }
                }

            }



        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("viewer.image.success"));
    }
}
