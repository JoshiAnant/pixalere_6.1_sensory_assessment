package com.pixalere.struts.viewer;

import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import com.pixalere.guibeans.RowData;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Common;
import org.apache.struts.action.ActionForm;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.reporting.bean.SummaryVO;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;
import java.util.TreeMap;

public class AssessmentFlowchartSetupAction extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentFlowchartSetupAction.class);
    //private final String OSTOMY="O";
    //private final String WOUND="A";

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
            String alpha_id = (String) request.getParameter("alpha_id");
            String alpha_type = (String) request.getParameter("alpha_type");
            log.info("*******************ALPHA ID:" + alpha_id);
            request.setAttribute("page", "assessflowchart");

            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {

                return (mapping.findForward("uploader.go.viewer"));
            }
            if (request.getParameter("page") != null && request.getParameter("page").equals("comments")) {
                return (mapping.findForward("uploader.go.comments"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("patientflowchart")) {
                return (mapping.findForward("uploader.go.patientflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundflowchart")) {
                return (mapping.findForward("uploader.go.woundflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("mdiagram")) {
                return (mapping.findForward("uploader.go.mdiagram"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("bradenflowchart")) {
                return (mapping.findForward("uploader.go.bradenflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbflowchart")) {
                return (mapping.findForward("uploader.go.limbflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("footflowchart")) {
                return (mapping.findForward("uploader.go.footflowchart"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("producthistory")) {
                return (mapping.findForward("uploader.go.prodhistory"));
            } else if (request.getParameter("page") != null && request.getParameter("page").equals("nursingcareplan")) {
                return (mapping.findForward("uploader.go.nursingcareplan"));
            }
            request.setAttribute("act", "AssessmentFlowchartSetup.do");
            String id = "0";
            int limit = 3;
            if (request.getParameter("limit") != null) {
                limit = Integer.parseInt((String) request.getParameter("limit"));
                request.setAttribute("limit", limit);
            }
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            WoundServiceImpl wp = new WoundServiceImpl(language);
            WoundAssessmentLocationServiceImpl walmanager = new WoundAssessmentLocationServiceImpl();
            RowData[] results = null;

            // default to assessment history on alpha A if first time
            // accessing.
            // we need to get the alpha_id for A as it is the default
            WoundAssessmentLocationVO walocation = new WoundAssessmentLocationVO();
            walocation.setPatient_id(new Integer(patient_id));
            walocation.setWound_profile_type_id(new Integer((String) session.getAttribute("wound_profile_type_id")));
            walocation.setActive(1);
            if (alpha_id == null) {
                WoundAssessmentLocationVO location = (WoundAssessmentLocationVO) walmanager.getAlpha(walocation, false, false, null);
                if (location != null) {
                    alpha_id = location.getId().toString();
                    alpha_type = location.getWound_profile_type().getAlpha_type();
                }
            }
            if (alpha_id != null) {
                WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
                wal.setId(new Integer(alpha_id));
                WoundAssessmentLocationVO loco = (WoundAssessmentLocationVO) walmanager.getAlpha(wal);
                WoundAssessmentLocationVO tmp = new WoundAssessmentLocationVO();
                tmp.setWound_profile_type_id(new Integer((String) session.getAttribute("wound_profile_type_id")));
                tmp.setActive(1);
                Collection<WoundAssessmentLocationVO> alphas = walmanager.getAllAlphas(tmp);

                if (loco != null) {
                    alpha_type = loco.getWound_profile_type().getAlpha_type();
                    ArrayList alphaslist = new ArrayList();
                    alphaslist.addAll(alphas);

                    String strAlphaName = "";
                    if (loco.getWound_profile_type().getAlpha_type().indexOf("A") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare", locale) + " " + loco.getAlpha();
                    }
                    if (loco.getWound_profile_type().getAlpha_type().indexOf("D") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_drain", locale) + " " + loco.getAlpha().substring(2);
                    }
                    if (loco.getWound_profile_type().getAlpha_type().indexOf("T") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_incisiontag", locale) + " " + loco.getAlpha().substring(3);
                    }
                    if (loco.getWound_profile_type().getAlpha_type().indexOf("O") != -1) {
                        if (loco.getAlpha().indexOf("ostu") != -1) {
                            strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_urinalstoma", locale) + " " + loco.getAlpha().substring(4);
                        }
                        if (loco.getAlpha().indexOf("ostf") != -1) {
                            strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_fecalstoma", locale) + " " + loco.getAlpha().substring(4);
                        }
                    }
                    if (loco.getWound_profile_type().getAlpha_type().indexOf("B") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_burn", locale);
                    }
                    if (loco.getWound_profile_type().getAlpha_type().indexOf("S") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_skin", locale);
                    }

                    request.setAttribute("alpha_str", strAlphaName);
                    request.setAttribute("alphas", Common.getSortedAlphaList(alphaslist));
                }
                TreeMap alphaMap = new TreeMap();
                //Getting alphas

                for (WoundAssessmentLocationVO v : alphas) {
                    alphaMap.put(v.getAlpha(), v);

                }

                request.setAttribute("allalphas", alphaMap);
            }

            Vector dropdown = null;

            if (alpha_type != null && alpha_type.equals(Constants.OSTOMY_PROFILE_TYPE)) {
                AssessmentOstomyVO crit = new AssessmentOstomyVO();
                crit.setAlpha_id(new Integer(alpha_id));
                crit.setActive(new Integer(1));
                if (request.getParameter("full_assessment") != null && ((String) request.getParameter("full_assessment")).equals("1")) {
                    crit.setFull_assessment(1);
                    request.setAttribute("full_assessment", 1);
                }
                Collection<AssessmentOstomyVO> items = null;
                if (limit != 0) {
                    items = manager.getAllAssessments(crit, limit, Constants.DESC_ORDER);
                }
                //if item selected from dropwn pretend to list
                if (request.getParameter("id") != null) {
                    id = (String) request.getParameter("id");
                    request.setAttribute("selectedid", (String) request.getParameter("id"));
                    AssessmentOstomyVO criteria = new AssessmentOstomyVO();
                    criteria.setId(new Integer(id));
                    AssessmentOstomyVO item = (AssessmentOstomyVO) manager.getAssessment(criteria, Constants.SHOW_DELETED);
                    Collection<AssessmentOstomyVO> tmp = new ArrayList();

                    int count = 0;
                    for (AssessmentOstomyVO pp : items) {
                        count++;
                        if (count != items.size()) {
                            tmp.add(pp);
                        }
                    }
                    if (items.size() > 2) {
                        tmp.add(item);
                        items = tmp;
                    }

                }
                results = manager.getAllOstomyAssessmentsForFlowchart(items, userVO, true);
                dropdown = manager.getAllAssessmentSignaturesByAlpha(crit, limit - 2);
                request.setAttribute("flowchart_text", "ostomyassessment");
                request.setAttribute("error_class", "AssessmentErrorSetup");
            } else if (alpha_type != null && alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {
                AssessmentEachwoundVO crit = new AssessmentEachwoundVO();
                crit.setAlpha_id(new Integer(alpha_id));
                crit.setActive(new Integer(1));
                if (request.getParameter("full_assessment") != null && ((String) request.getParameter("full_assessment")).equals("1")) {
                    crit.setFull_assessment(1);
                    request.setAttribute("full_assessment", 1);
                }
                Collection<AssessmentEachwoundVO> items = null;
                if (limit != 0) {
                    items = manager.getAllAssessments(crit, limit, Constants.DESC_ORDER);
                }
                //if item selected from dropwn pretend to list
                if (request.getParameter("id") != null) {

                    id = (String) request.getParameter("id");
                    request.setAttribute("selectedid", (String) request.getParameter("id"));
                    AssessmentEachwoundVO criteria = new AssessmentEachwoundVO();
                    criteria.setId(new Integer(id));
                    AssessmentEachwoundVO item = (AssessmentEachwoundVO) manager.getAssessment(criteria, Constants.SHOW_DELETED);
                    Collection<AssessmentEachwoundVO> tmp = new ArrayList();

                    int count = 0;

                    for (AssessmentEachwoundVO pp : items) {
                        count++;
                        if (count != items.size()) {
                            tmp.add(pp);
                        }

                    }
                    if (item != null) {
                        tmp.add(item);
                    }
                    items = tmp;

                }

                results = manager.getAllAssessmentsForFlowchart(items, userVO, true);
                dropdown = manager.getAllAssessmentSignaturesByAlpha(crit, limit - 2);
                request.setAttribute("flowchart_text", "woundassessment");
                request.setAttribute("error_class", "AssessmentErrorSetup");
            } else if (alpha_type != null && alpha_type.equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                AssessmentIncisionVO crit = new AssessmentIncisionVO();
                crit.setAlpha_id(new Integer(alpha_id));
                crit.setActive(new Integer(1));
                if (request.getParameter("full_assessment") != null && ((String) request.getParameter("full_assessment")).equals("1")) {
                    crit.setFull_assessment(1);
                    request.setAttribute("full_assessment", 1);
                }
                Collection<AssessmentIncisionVO> items = null;
                if (limit != 0) {
                    items = manager.getAllAssessments(crit, limit, Constants.DESC_ORDER);
                }
                //if item selected from dropwn pretend to list
                if (request.getParameter("id") != null) {
                    id = (String) request.getParameter("id");
                    request.setAttribute("selectedid", (String) request.getParameter("id"));
                    AssessmentIncisionVO criteria = new AssessmentIncisionVO();
                    criteria.setId(new Integer(id));
                    AssessmentIncisionVO item = (AssessmentIncisionVO) manager.getAssessment(criteria, Constants.SHOW_DELETED);
                    Collection<AssessmentIncisionVO> tmp = new ArrayList();

                    int count = 0;
                    for (AssessmentIncisionVO pp : items) {
                        count++;
                        if (count != items.size()) {
                            tmp.add(pp);
                        }
                    }
                    tmp.add(item);
                    items = tmp;

                }
                results = manager.getAllIncisionAssessmentsForFlowchart(items, userVO, true);
                dropdown = manager.getAllAssessmentSignaturesByAlpha(crit, limit - 2);
                request.setAttribute("flowchart_text", "incisionassessment");
                request.setAttribute("error_class", "AssessmentErrorSetup");
            } else if (alpha_type != null && alpha_type.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                AssessmentDrainVO crit = new AssessmentDrainVO();
                crit.setAlpha_id(new Integer(alpha_id));
                crit.setActive(new Integer(1));
                if (request.getParameter("full_assessment") != null && ((String) request.getParameter("full_assessment")).equals("1")) {
                    crit.setFull_assessment(1);
                    request.setAttribute("full_assessment", 1);
                }
                Collection<AssessmentDrainVO> items = null;
                if (limit != 0) {
                    items = manager.getAllAssessments(crit, limit, Constants.DESC_ORDER);
                }
                //if item selected from dropwn pretend to list
                if (request.getParameter("id") != null) {
                    id = (String) request.getParameter("id");
                    request.setAttribute("selectedid", (String) request.getParameter("id"));
                    AssessmentDrainVO criteria = new AssessmentDrainVO();
                    criteria.setId(new Integer(id));
                    AssessmentDrainVO item = (AssessmentDrainVO) manager.getAssessment(criteria, Constants.SHOW_DELETED);
                    Collection<AssessmentDrainVO> tmp = new ArrayList();

                    int count = 0;
                    for (AssessmentDrainVO pp : items) {
                        count++;
                        if (count != items.size()) {
                            tmp.add(pp);
                        }
                    }
                    tmp.add(item);
                    items = tmp;

                }
                results = manager.getAllDrainAssessmentsForFlowchart(items, userVO, true);
                dropdown = manager.getAllAssessmentSignaturesByAlpha(crit, limit - 2);
                request.setAttribute("flowchart_text", "drainassessment");
                request.setAttribute("error_class", "AssessmentErrorSetup");
            } else if (alpha_type != null && alpha_type.equals(Constants.BURN_PROFILE_TYPE)) {
                AssessmentBurnVO crit = new AssessmentBurnVO();
                crit.setAlpha_id(new Integer(alpha_id));
                crit.setActive(new Integer(1));
                if (request.getParameter("full_assessment") != null && ((String) request.getParameter("full_assessment")).equals("1")) {
                    crit.setFull_assessment(1);
                    request.setAttribute("full_assessment", 1);
                }
                Collection<AssessmentBurnVO> items = null;
                if (limit != 0) {
                    items = manager.getAllAssessments(crit, limit, Constants.DESC_ORDER);
                }
                //if item selected from dropwn pretend to list
                if (request.getParameter("id") != null) {
                    id = (String) request.getParameter("id");
                    request.setAttribute("selectedid", (String) request.getParameter("id"));
                    AssessmentBurnVO criteria = new AssessmentBurnVO();
                    criteria.setId(new Integer(id));
                    AssessmentBurnVO item = (AssessmentBurnVO) manager.getAssessment(criteria, Constants.SHOW_DELETED);
                    Collection<AssessmentBurnVO> tmp = new ArrayList();

                    int count = 0;
                    for (AssessmentBurnVO pp : items) {
                        count++;
                        if (count != items.size()) {
                            tmp.add(pp);
                        }
                    }
                    tmp.add(item);
                    items = tmp;

                }
                results = manager.getAllBurnAssessmentsForFlowchart(items, userVO, true);
                dropdown = manager.getAllAssessmentSignaturesByAlpha(crit, limit - 2);
                request.setAttribute("flowchart_text", "burnassessment");
                request.setAttribute("error_class", "AssessmentErrorSetup");
            } else if (alpha_type != null && alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {
                AssessmentSkinVO crit = new AssessmentSkinVO();
                crit.setAlpha_id(new Integer(alpha_id));
                crit.setActive(new Integer(1));
                if (request.getParameter("full_assessment") != null && ((String) request.getParameter("full_assessment")).equals("1")) {
                    crit.setFull_assessment(1);
                    request.setAttribute("full_assessment", 1);
                }
                Collection<AssessmentSkinVO> items = null;
                if (limit != 0) {
                    items = manager.getAllAssessments(crit, limit, Constants.DESC_ORDER);
                }
                //if item selected from dropwn pretend to list
                if (request.getParameter("id") != null) {
                    id = (String) request.getParameter("id");
                    request.setAttribute("selectedid", (String) request.getParameter("id"));
                    AssessmentSkinVO criteria = new AssessmentSkinVO();
                    criteria.setId(new Integer(id));
                    AssessmentSkinVO item = (AssessmentSkinVO) manager.getAssessment(criteria, Constants.SHOW_DELETED);
                    Collection<AssessmentSkinVO> tmp = new ArrayList();

                    int count = 0;
                    for (AssessmentSkinVO pp : items) {
                        count++;
                        if (count != items.size()) {
                            tmp.add(pp);
                        }
                    }
                    tmp.add(item);
                    items = tmp;

                }
                results = manager.getAllSkinAssessmentsForFlowchart(items, userVO, true);
                dropdown = manager.getAllAssessmentSignaturesByAlpha(crit, limit - 2);
                request.setAttribute("flowchart_text", "skinssessment");
                request.setAttribute("error_class", "AssessmentErrorSetup");
            }

            request.setAttribute("dropdown", dropdown);
            request.setAttribute("results", results);
            request.setAttribute("alpha_type", alpha_type);
            request.setAttribute("alpha_id", alpha_id);
            if(alpha_id != null){
                walocation.setId(new Integer(alpha_id));
            }
            WoundAssessmentLocationVO location = (WoundAssessmentLocationVO) walmanager.getAlpha(walocation, true, false, null);
            if (location != null) {
                WoundVO tmpVO = new WoundVO();
                tmpVO.setId(location.getWound_id());
                WoundVO wpvo = (WoundVO) wp.getWound(tmpVO);
                WoundProfileVO wpt = new WoundProfileVO();
                wpt.setWound_id(location.getWound_id());
                wpt.setCurrent_flag(1);

                WoundProfileVO profile = wp.getWoundProfile(wpt);
                if (profile!=null && profile.getEtiologies() != null) {
                    //System.out.println("Etiology"+profile.getEtiologies());
                    for (EtiologyVO e : profile.getEtiologies()) {
                        //System.out.println("e"+e.getAlpha_id()+" "+location.getId());
                        if (e.getAlpha_id().equals(location.getId())) {
                            request.setAttribute("etiology", e.getPrintable(language));

                        }
                    }
                }
                request.setAttribute("wound_profile", wpvo);
            }
            //populate wound profile dropdown
            Vector woundProfilesDrop = wp.getWoundProfilesDropdown(Integer.parseInt(patient_id), false, userVO.getId().intValue());
            request.setAttribute("woundProfilesDrop", woundProfilesDrop);
            request.setAttribute("page", "assessflowchart");
            request.setAttribute("page_sub", "aflowchart");

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in AssessmentFlowchartSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null) {
            if (request.getParameter("page").equals("patient")) {
                return (mapping.findForward("uploader.go.patient"));
            } else if (request.getParameter("page").equals("woundprofiles")) {
                return (mapping.findForward("uploader.go.profile"));
            } else if (request.getParameter("page").equals("assess")) {
                return (mapping.findForward("uploader.go.assess"));
            } else if (request.getParameter("page").equals("visitsflowchart")) {
                return (mapping.findForward("uploader.go.visits"));
            } else if (request.getParameter("page").equals("treatment")) {
                return (mapping.findForward("uploader.go.treatment"));
            } else if (request.getParameter("page").equals("viewer")) {
                return (mapping.findForward("uploader.go.viewer"));
            } else if (request.getParameter("page").equals("summary")) {
                return (mapping.findForward("uploader.go.summary"));
            } else if (request.getParameter("page").equals("reporting")) {
                return (mapping.findForward("uploader.go.reporting"));
            } else if (request.getParameter("page").equals("admin")) {
                log.info("############$$$$$$$$$$$$################# Page: " + request.getParameter("page"));
                return (mapping.findForward("uploader.go.admin"));
            } else {
                return (mapping.findForward("uploader.aflowchart.success"));
            }
        } else {
            return (mapping.findForward("uploader.aflowchart.success"));
        }
    }
}
