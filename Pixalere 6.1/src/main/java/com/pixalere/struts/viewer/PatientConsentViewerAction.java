package com.pixalere.struts.viewer;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PatientConsentViewerAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientFlowchartSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        boolean consentEnabled = Common.getConfig("patientConsentEnabled").equals("1");
    
        try {
            if (session.getAttribute("patient_id") == null || !consentEnabled) {
                return (mapping.findForward("uploader.patientconsent.error"));
            }

            String patient_id = (String) session.getAttribute("patient_id");
            // Patient Consent information
            PatientServiceImpl patientService = new PatientServiceImpl(language);
            PatientAccountVO patient = patientService.getPatient(new Integer((String) patient_id));
            
            if (patient == null || !patient.isPatientConsent()) {
                return (mapping.findForward("uploader.patientconsent.error"));
            }
            
            request.setAttribute("contract", patient.getPatient_consent_blurb());
            request.setAttribute("patient_name", patient.getPatient_consent_name());
            
            // Verbal
            if (patient.getConsent_verbal() != null){
                ListsDAO listDAO = new ListsDAO();
                
                LookupVO lookupVO = new LookupVO();
                lookupVO.setResourceId(LookupVO.VERBAL_PATIENT_CONSENT);
                lookupVO.setId(patient.getConsent_verbal());
                Collection<LookupVO> list = listDAO.findAllByCriteria(lookupVO);
                
                for (Iterator itera = list.iterator(); itera.hasNext();) {
                    LookupVO el = (LookupVO) itera.next();
                    request.setAttribute("consent_verbal", el.getName(language));
                }
            }
            
            // Signature
            request.setAttribute("signature", patient.getPatient_consent_sig());
            
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy", Common.getLocaleFromLocaleString(locale));
            String consent_date = df.format(patient.getConsent_date());
            request.setAttribute("consent_date", consent_date);
            
            
        } catch (com.pixalere.common.DataAccessException e) {
            log.error("A data exception has been raised in PatientConsentViewerAction: " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientConsentViewerAction: " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("uploader.patientconsent.success"));
    }
}
