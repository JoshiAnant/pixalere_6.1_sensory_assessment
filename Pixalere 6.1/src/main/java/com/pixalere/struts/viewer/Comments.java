package com.pixalere.struts.viewer;

import java.util.Date;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.service.PatientReportService;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.PDate;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.bean.PatientReportVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import java.util.Hashtable;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;

public class Comments extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Comments.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        CommentsForm commentsForm = (CommentsForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
        if ((session.getAttribute("wound_profile_type_id") == null || ((String) session.getAttribute("wound_profile_type_id")).equals(""))) {

            return (mapping.findForward("uploader.go.viewer"));
        }
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
        ReferralsTrackingVO trackingVO = new ReferralsTrackingVO();
        request.setAttribute("page", "comments");

        if (isTokenValid(request)) {
            resetToken(request);
            trackingVO.setId(commentsForm.getReferral_id());

            ReferralsTrackingServiceImpl refDAO = new ReferralsTrackingServiceImpl(language);
            PatientServiceImpl pbd = new PatientServiceImpl(language);
            WoundServiceImpl wbd = new WoundServiceImpl(language);
            AssessmentCommentsServiceImpl cmanager = new AssessmentCommentsServiceImpl();
            try {
                if (commentsForm.getPage() != null && commentsForm.getPage().equals("delete_comment")) {
                    if (commentsForm.getId() != 0) {
                        String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale);

                        AssessmentCommentsVO c = new AssessmentCommentsVO();
                        c.setId(commentsForm.getId());
                        AssessmentCommentsVO comment = cmanager.getComment(c);
                        comment.setDeleted(1);
                        comment.setDelete_signature(user_signature);
                        comment.setDelete_reason(commentsForm.getDelete_reason());
                        cmanager.saveComment(comment);
                    }
                } else if (commentsForm.getPage() != null && commentsForm.getPage().equals("delete_recommendation")) {
                    if (commentsForm.getId() != 0) {
                        String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale);

                        AssessmentCommentsVO c = new AssessmentCommentsVO();
                        c.setId(commentsForm.getId());
                        AssessmentCommentsVO comment = cmanager.getComment(c);
                        comment.setDeleted(1);
                        comment.setDelete_reason(commentsForm.getDelete_reason());
                        comment.setDelete_signature(user_signature);
                        cmanager.saveComment(comment);
                        //remove recommendation..
                        //renable referral?
                    }
                } else {
                    int patient_account_id = 0;
                    ReferralsTrackingVO referral = null;
                    PatientAccountVO v = new PatientAccountVO();
                    v.setPatient_id(new Integer(patient_id));
                    v.setCurrent_flag(1);
                    PatientAccountVO p = pbd.getPatient(v);
                    if (p != null) {
                        patient_account_id = p.getId();
                    }
                    if (commentsForm.getAssessment_id() == 0) {
                        commentsForm.setAssessment_id(-1);
                    }

                    if (commentsForm.getReferral_id() != null) {
                        if (commentsForm.getReferral_id() == 0) {
                            trackingVO.setWound_profile_type_id(new Integer(wound_profile_type_id));
                            trackingVO.setCurrent(1);
                            trackingVO.setAssessment_id(-1);
                        }
                       
                        ReferralsTrackingVO returnVO = (ReferralsTrackingVO) refDAO.getReferralsTracking(trackingVO);
                        if (returnVO != null) {
                            if (session.getAttribute("wound_profile_type_id") != null) {
                                log.info("Viewer.perform(): User Type:" + userVO.getPosition() + " matches.");
                                
                                returnVO.setCurrent(new Integer(0));
                                log.info("PixalereSearch.perform: Setting Old referral to current =0");
                                refDAO.saveReferralsTracking(returnVO);
                                
                                log.info("PixalereSearch.perform: Inserting new referral (ackknowledgement)");
                                returnVO.setCurrent(new Integer("1"));
                                returnVO.setEntry_type(Constants.ACKNOWLEDGE_RECOMMENDATION); // "Acknowledge Recommendation"
                                returnVO.setCreated_on(new Date());
                                returnVO.setProfessional_id(userVO.getId());
                                returnVO.setId(null);
                                refDAO.saveReferralsTracking(returnVO);
                                

                            }
                        }
                    } else if (commentsForm.getComments_body() != null || commentsForm.getComments_recommendations() != null) {

                        String user_signature = pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"),locale);
                        if (wound_profile_type_id != null && !wound_profile_type_id.equals("")) {

                            WoundProfileTypeVO wpt = new WoundProfileTypeVO();
                            wpt.setId(new Integer((String) wound_profile_type_id));
                            WoundProfileTypeVO wpid = wbd.getWoundProfileType(wpt);
                            AssessmentCommentsVO assessmentCommentsVO = new AssessmentCommentsVO();
                            assessmentCommentsVO.setPatient_id(new Integer(patient_id));
                            assessmentCommentsVO.setWound_id(wpid.getWound_id());
                            assessmentCommentsVO.setWound_profile_type_id(new Integer(wound_profile_type_id));
                            assessmentCommentsVO.setAssessment_id(commentsForm.getAssessment_id());
                            assessmentCommentsVO.setComment_type(Constants.COMMENT_TYPE_NOTE);
                            assessmentCommentsVO.setPosition_id(userVO.getPosition_id());
    
                            assessmentCommentsVO.setProfessional_id(userVO.getId());

                            assessmentCommentsVO.setBody(commentsForm.getComments_body());
                            assessmentCommentsVO.setCreated_on(new Date());
                            assessmentCommentsVO.setUser_signature(user_signature);

                            AssessmentCommentsVO assessmentRecommendationsVO = new AssessmentCommentsVO();
                            assessmentRecommendationsVO.setPatient_id(new Integer(patient_id));
                            assessmentRecommendationsVO.setWound_id(wpid.getWound_id());
                            assessmentRecommendationsVO.setWound_profile_type_id(new Integer(wound_profile_type_id));
                            assessmentRecommendationsVO.setAssessment_id(commentsForm.getAssessment_id());
                            assessmentRecommendationsVO.setProfessional_id(userVO.getId());
                            assessmentRecommendationsVO.setNcp_changed(commentsForm.getNcp_changed());
                            assessmentRecommendationsVO.setWcc_visit_required(commentsForm.getWcc_visit_required());
                            assessmentRecommendationsVO.setComment_type(Constants.COMMENT_TYPE_RECOMMENDATION);//NEEDS TO BE RECOMMENDATION!
                            assessmentRecommendationsVO.setPosition_id(userVO.getPosition_id());
                            assessmentRecommendationsVO.setBody(commentsForm.getComments_recommendations());
                            assessmentRecommendationsVO.setCreated_on(new Date());
                            assessmentRecommendationsVO.setUser_signature(user_signature);
                            Hashtable commentSession = new Hashtable();
                            if (request.getParameter("leftViewerSaveComments") == null || !request.getParameter("leftViewerSaveComments").equals("1")) {
                                if (!(commentsForm.getComments_body().trim()).equals("")) {
                                    //update timestamp for patient.. used to generate report for PCI ( or authority's document library)
                                    PatientReportService prservice = new PatientReportService();
                                    if (patient_id != null && !patient_id.equals("")) {
                                        PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                                        if (savedPatient != null) {
                                            savedPatient.setActive(1);
                                            savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                            prservice.savePatientReportUpdate(savedPatient);
                                        } else {
                                            savedPatient = new PatientReportVO();
                                            savedPatient.setPatient_id(new Integer(patient_id));
                                            savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                            savedPatient.setActive(1);
                                            prservice.savePatientReportUpdate(savedPatient);
                                        }
                                    }
                                    
                                }
                                if ((commentsForm.getComments_recommendations() != null && !(commentsForm.getComments_recommendations().trim()).equals("")) || !(commentsForm.getComments_body().trim()).equals("")) {
                                    ReferralsTrackingVO trackingVO2 = new ReferralsTrackingVO();
                                    trackingVO2.setCurrent(new Integer(1));
                                    trackingVO2.setEntry_type(Constants.REFERRAL); // "Referral"
                                    trackingVO2.setAssessment_id(commentsForm.getAssessment_id());
                                    trackingVO2.setWound_profile_type_id(new Integer(wound_profile_type_id));

                                    ReferralsTrackingVO returnVO = refDAO.getReferralsTracking(trackingVO2);

                                    if (userVO.getPosition().getReferral_popup().equals(1)) {
                                        if (returnVO != null) {

                                            returnVO.setCurrent(new Integer(0));
                                            log.info("PixalereSearch.perform: Setting Old referral to current =0");
                                            refDAO.saveReferralsTracking(returnVO);
                                            log.info("PixalereSearch.perform: Inserting new referral (ackknowledgement)");
                                            returnVO.setWound_profile_type_id(new Integer(wound_profile_type_id));
                                            returnVO.setPatient_account_id(patient_account_id);

                                            //if(commentsForm.getSend_message() == 1){
                                            //    returnVO.setAssessment_id(-1);
                                            //    returnVO.setEntry_type(Constants.MESSAGE_FROM_CLINICIAN);
                                            //}
                                            returnVO.setCurrent(new Integer("1"));
                                            returnVO.setEntry_type(Constants.RECOMMENDATION); // "Recommendation Complete"
                                            returnVO.setCreated_on(new Date());
                                            returnVO.setProfessional_id(userVO.getId());
                                            returnVO.setId(null);
                                            refDAO.saveReferralsTracking(returnVO);
                                            referral = refDAO.getReferralsTracking(returnVO);
                                        } else if (commentsForm.getAssessment_id() != 0) {
                                            ReferralsTrackingVO refVO = new ReferralsTrackingVO();
                                            //if(commentsForm.getSend_message() == 1){
                                            //    refVO.setAssessment_id(null);
                                            //    refVO.setEntry_type(Constants.MESSAGE_FROM_CLINICIAN);
                                            //}else{
                                            refVO.setAssessment_id(commentsForm.getAssessment_id());
                                            //}
                                            refVO.setWound_id(wpid.getWound_id());
                                            refVO.setWound_profile_type_id(new Integer(wound_profile_type_id));
                                            refVO.setCurrent(new Integer("1"));
                                            refVO.setEntry_type(Constants.RECOMMENDATION); // "Recommendation Complete"
                                            refVO.setCreated_on(new Date());
                                            refVO.setProfessional_id(userVO.getId());
                                            refVO.setId(null);
                                            refVO.setPatient_account_id(patient_account_id);
                                            refVO.setPatient_id(new  Integer(patient_id));
                                            //remove old unassigned referrals if this one is unassigned
                                            if (commentsForm.getAssessment_id() == -1) {
                                                ReferralsTrackingVO tTMP = new ReferralsTrackingVO();
                                                tTMP.setAssessment_id(-1);
                                                tTMP.setWound_profile_type_id(new Integer(wound_profile_type_id));
                                                tTMP.setCurrent(1);
                                                tTMP.setEntry_type(Constants.RECOMMENDATION);
                                                ReferralsTrackingVO returnVO2 = refDAO.getReferralsTracking(tTMP);
                                                if (returnVO2 != null) {
                                                    refVO.setCurrent(0);
                                                    refDAO.saveReferralsTracking(returnVO2);
                                                }
                                            }
                                            refVO.setActive(1);
                                            refDAO.saveReferralsTracking(refVO);
                                            referral = refDAO.getReferralsTracking(refVO);
                                        }
                                    } else {//is a nurse entering a comment.
                                        if (commentsForm.getSend_message() == 1) {
                                            ReferralsTrackingVO refVO = new ReferralsTrackingVO();
                                            refVO.setAssessment_id(-1);
                                            refVO.setWound_id(wpid.getWound_id());
                                            refVO.setWound_profile_type_id(new Integer(wound_profile_type_id));
                                            refVO.setCurrent(new Integer("1"));
                                            refVO.setEntry_type(Constants.MESSAGE_FROM_NURSE); // "Recommendation Complete"
                                            refVO.setCreated_on(new Date());
                                            refVO.setProfessional_id(userVO.getId());
                                            refVO.setId(null);
                                            refVO.setPatient_account_id(patient_account_id);
                                            refVO.setPatient_id(new  Integer(patient_id));
                                            refVO.setActive(1);
											refDAO.saveReferralsTracking(refVO);
                                            referral = refDAO.getReferralsTracking(refVO);
                                        }
                                    }

                                    if (commentsForm.getComments_recommendations() != null && !(commentsForm.getComments_recommendations().trim()).equals("")) {
                                        //update timestamp for patient.. used to generate report for PCI ( or authority's document library)
                                        PatientReportService prservice = new PatientReportService();
                                        if (patient_id != null && !patient_id.equals("")) {
                                            PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                                            if (savedPatient != null) {
                                                savedPatient.setActive(1);
                                                savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                                prservice.savePatientReportUpdate(savedPatient);
                                            } else {
                                                savedPatient = new PatientReportVO();
                                                savedPatient.setPatient_id(new Integer(patient_id));
                                                savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                                                savedPatient.setActive(1);
                                                prservice.savePatientReportUpdate(savedPatient);
                                            }
                                        }
                                        if(referral!=null){assessmentRecommendationsVO.setReferral_id(referral.getId());}
                                        cmanager.saveComment(assessmentRecommendationsVO);
                                    }
                                    if (!(commentsForm.getComments_body().trim()).equals("")) {
                                        if(referral!=null){assessmentCommentsVO.setReferral_id(referral.getId());}
                                        cmanager.saveComment(assessmentCommentsVO);
                                    }
                                }
                                session.removeAttribute("savedComments");
                                
                            } else {
                                commentSession.put("comment", assessmentCommentsVO.getBody());
                                commentSession.put("recommendation", assessmentRecommendationsVO.getBody());
                                commentSession.put("referral", commentsForm.getAssessment_id());
                                session.setAttribute("savedComments", commentSession);
                            }
                        }
                    }
                }
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in LogAuditAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("visitsflowchart")) {
            return (mapping.findForward("uploader.go.visits"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("diagflowchart")) {
                return (mapping.findForward("uploader.go.diagflowchart"));
            }else if (request.getParameter("page") != null && request.getParameter("page").equals("patientflowchart")) {
            return (mapping.findForward("uploader.go.patientflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundflowchart")) {
            return (mapping.findForward("uploader.go.woundflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assessflowchart")) {
            return (mapping.findForward("uploader.go.assessflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("mdiagram")) {
            return (mapping.findForward("uploader.go.mdiagram"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("comments")) {
            return (mapping.findForward("uploader.go.comments"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("producthistory")) {
            return (mapping.findForward("uploader.go.prodhistory"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("examflowchart")) {
                return (mapping.findForward("uploader.go.examflowchart"));
            }else if (request.getParameter("page") != null && request.getParameter("page").equals("nursingcareplan")) {
            return (mapping.findForward("uploader.go.nursingcareplan"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("bradenflowchart")) {
            return (mapping.findForward("uploader.go.bradenflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("limbflowchart")) {
            return (mapping.findForward("uploader.go.limbflowchart"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("footflowchart")) {
            return (mapping.findForward("uploader.go.footflowchart"));
        } else {
            return (mapping.findForward("viewer.comments.success"));
        }
    }
}