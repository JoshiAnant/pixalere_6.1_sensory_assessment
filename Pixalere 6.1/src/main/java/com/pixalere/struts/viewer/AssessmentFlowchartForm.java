package com.pixalere.struts.viewer;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

public class AssessmentFlowchartForm
    extends ActionForm {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.
      getLogger(AssessmentFlowchartForm.class);
  public ActionErrors validate(ActionMapping mapping,
                               HttpServletRequest request) {
	
    ActionErrors errors = new ActionErrors();
    
    return errors;

  }

  public void reset(ActionMapping mapping, HttpServletRequest request) {
  }

  public String getAlpha_id() {
    return alpha_id;
  }

  public void setAlpha_id(String alpha_id) {
    this.alpha_id = alpha_id;
  }

  private String alpha_id;

}