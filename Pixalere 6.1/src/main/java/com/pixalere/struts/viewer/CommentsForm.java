package com.pixalere.struts.viewer;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

public class CommentsForm extends ActionForm {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CommentsForm.class);

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    private int ncp_changed;
    private int wcc_visit_required;
    private Integer referral_id;
    private int deleted;
    private int id;
    private String delete_reason;
    private String page;
    /*private Integer start_date;
    private Integer end_date;

    public Integer getStart_date() {
        return start_date;
    }

    public void setStart_date(Integer start_date) {
        this.start_date = start_date;
    }

    public Integer getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Integer end_date) {
        this.end_date = end_date;
    } */

    public void setReferral_id(Integer referral_id) {
        this.referral_id = referral_id;
    }

    public Integer getReferral_id() {
        return referral_id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
    private String comments_header;
  private String comments_body;
  private String comments_recommendations;
  private int assessment_id;
  private Integer message;
  private int send_message;
    public String getComments_header() {
        return comments_header;
    }

    public void setComments_header(String comments_header) {
        this.comments_header = comments_header;
    }

    public String getComments_body() {
        return comments_body;
    }

    public void setComments_body(String comments_body) {
        this.comments_body = comments_body;
    }
    public String getPage(){
        return page;
    }
    public void setPage(String page){
        this.page=page;
    }
    public String getComments_recommendations() {
        return comments_recommendations;
    }

    public void setComments_recommendations(String comments_recommendations) {
        this.comments_recommendations = comments_recommendations;
    }

    public int getAssessment_id() {
        return assessment_id;
    }

    public void setAssessment_id(int assessment_id) {
        this.assessment_id = assessment_id;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public int getSend_message() {
        return send_message;
    }

    public void setSend_message(int send_message) {
        this.send_message = send_message;
    }

    public int getNcp_changed() {
        return ncp_changed;
    }

    public void setNcp_changed(int ncp_changed) {
        this.ncp_changed = ncp_changed;
    }

    public int getWcc_visit_required() {
        return wcc_visit_required;
    }

    public void setWcc_visit_required(int wcc_visit_required) {
        this.wcc_visit_required = wcc_visit_required;
    }

    /**
     * @return the deleted
     */
    public int getDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    /**
     * @return the delete_reason
     */
    public String getDelete_reason() {
        return delete_reason;
    }

    /**
     * @param delete_reason the delete_reason to set
     */
    public void setDelete_reason(String delete_reason) {
        this.delete_reason = delete_reason;
    }
}
