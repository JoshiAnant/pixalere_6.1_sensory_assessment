package com.pixalere.struts.viewer.edit;

import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.utils.PDate;
import com.pixalere.common.ArrayValueObject;
import java.text.SimpleDateFormat;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.service.AssessmentNPWTServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.assessment.bean.AssessmentProductVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.bean.*;
import com.pixalere.utils.Serialize;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.guibeans.DropdownBean;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

public class AssessmentErrorSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentErrorSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {
            String id = (String) request.getParameter("id");

            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            TreatmentCommentsServiceImpl tc = new TreatmentCommentsServiceImpl();
            NursingCarePlanServiceImpl ncp = new NursingCarePlanServiceImpl();
            AssessmentNPWTServiceImpl nmanager = new AssessmentNPWTServiceImpl();
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            PatientProfileServiceImpl pp = new PatientProfileServiceImpl(language);
            WoundServiceImpl wp = new WoundServiceImpl(language);
            
            if(request.getParameter("type")!=null){
                request.setAttribute("type",request.getParameter("type"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String wound_profile_type_id = (String) session.getAttribute("wound_profile_type_id");
            if (id != null && userVO != null) {
                String field = (String) request.getParameter("field");
                request.setAttribute("field", field);
                LookupVO lookupVO = new LookupVO();
                //load user-signature
                if (request.getParameter("act") != null) {
                    request.setAttribute("act", request.getParameter("act"));
                }
                if (request.getParameter("alpha_id") != null) {
                    request.setAttribute("alpha_id", request.getParameter("alpha_id"));
                }
                ListServiceImpl listBD = new ListServiceImpl(language);
                //Get Field data
                ComponentsVO crit = new ComponentsVO();
                GUIServiceImpl gui = new GUIServiceImpl();
                crit.setId(new Integer(field));
                ComponentsVO component = gui.getComponent(crit);
                component = Common.populateLocalizedTitles(component, locale);
                request.setAttribute("component", component);
                request.setAttribute("title", Common.getLocalizedString(component.getLabel_key(), locale));
                Object assess = null;
                if (component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT)) {
                    AssessmentOstomyVO ost = new AssessmentOstomyVO();
                    ost.setId(new Integer(id));
                    assess = (AbstractAssessmentVO) manager.getAssessment(ost);
                    
                    if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                        AssessmentOstomyVO obj = (AssessmentOstomyVO) assess;
                        assess = (WoundAssessmentVO) obj.getWoundAssessment();
                        request.setAttribute("assess_each_id",obj.getId());
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT) || component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION) || component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_ANTIBIOTICS)) {
                        AssessmentOstomyVO obj = (AssessmentOstomyVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                        at.setAssessment_id(obj.getAssessment_id());
                        at.setAlpha_id(obj.getAlpha_id());
                        AssessmentTreatmentVO atreat = tc.getAssessmentTreatment(at);
                        assess = atreat;
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        AssessmentOstomyVO obj = (AssessmentOstomyVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentNPWTVO atreat = nmanager.getNegativePressure(obj.getAssessment_id(), obj.getAlpha_id());
                        assess = atreat;
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        AssessmentOstomyVO obj = (AssessmentOstomyVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        TreatmentCommentVO t1 = new TreatmentCommentVO();
                        t1.setAssessment_id(obj.getAssessment_id());
                        t1.setWound_profile_type_id(obj.getWound_profile_type_id());
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        if (t2 != null) {
                            assess = t2;
                        } else {
                            request.setAttribute("is_null", "1");
                        }
                    }
                    
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) {
                    AssessmentEachwoundVO ost = new AssessmentEachwoundVO();
                    ost.setId(new Integer(id));
                    assess = (AbstractAssessmentVO) manager.getAssessment(ost);
                    if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                        AssessmentEachwoundVO obj = (AssessmentEachwoundVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        assess = (WoundAssessmentVO) obj.getWoundAssessment();
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        AssessmentEachwoundVO obj = (AssessmentEachwoundVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentNPWTVO atreat = nmanager.getNegativePressure(obj.getAssessment_id(), obj.getAlpha_id());
                        if(atreat != null){
                            assess = atreat;
                            
                        }else{
                            request.setAttribute("is_null","1");
                        }
                        
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT) || component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION) || component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_ANTIBIOTICS)) {
                        AssessmentEachwoundVO obj = (AssessmentEachwoundVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                        at.setAssessment_id(obj.getAssessment_id());
                        at.setAlpha_id(obj.getAlpha_id());
                        AssessmentTreatmentVO atreat = tc.getAssessmentTreatment(at);
                        assess = atreat;
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        AssessmentEachwoundVO obj = (AssessmentEachwoundVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        TreatmentCommentVO t1 = new TreatmentCommentVO();
                        t1.setAssessment_id(obj.getAssessment_id());
                        t1.setWound_profile_type_id(obj.getWound_profile_type_id());
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        if (t2 != null) {
                            assess = t2;
                        } else {
                            request.setAttribute("is_null", "1");
                        }
                    }
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.INCISION_ASSESSMENT)) {
                    AssessmentIncisionVO ost = new AssessmentIncisionVO();
                    ost.setId(new Integer(id));
                    assess = (AbstractAssessmentVO) manager.getAssessment(ost);
                    if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                        AssessmentIncisionVO obj = (AssessmentIncisionVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        assess = (WoundAssessmentVO) obj.getWoundAssessment();
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        AssessmentIncisionVO obj = (AssessmentIncisionVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentNPWTVO atreat = nmanager.getNegativePressure(obj.getAssessment_id(), obj.getAlpha_id());
                        if(atreat != null){
                            assess = atreat;
                            
                        }else{
                            request.setAttribute("is_null","1");
                        }
                        
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT) || component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION) || component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_ANTIBIOTICS)) {
                        AssessmentIncisionVO obj = (AssessmentIncisionVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                        at.setAssessment_id(obj.getAssessment_id());
                        at.setAlpha_id(obj.getAlpha_id());
                        AssessmentTreatmentVO atreat = tc.getAssessmentTreatment(at);
                        assess = atreat;
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        AssessmentIncisionVO obj = (AssessmentIncisionVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        TreatmentCommentVO t1 = new TreatmentCommentVO();
                        t1.setAssessment_id(obj.getAssessment_id());
                        t1.setWound_profile_type_id(obj.getWound_profile_type_id());
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        if (t2 != null) {
                            assess = t2;
                        } else {
                            request.setAttribute("is_null", "1");
                        }
                    }
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT)) {
                    AssessmentDrainVO ost = new AssessmentDrainVO();
                    ost.setId(new Integer(id));
                    assess = (AbstractAssessmentVO) manager.getAssessment(ost);
                    if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                        AssessmentDrainVO obj = (AssessmentDrainVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        assess = (WoundAssessmentVO) obj.getWoundAssessment();
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        AssessmentDrainVO obj = (AssessmentDrainVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentNPWTVO atreat = nmanager.getNegativePressure(obj.getAssessment_id(), obj.getAlpha_id());
                        if(atreat != null){
                            assess = atreat;
                            
                        }else{
                            request.setAttribute("is_null","1");
                        }
                        
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT) || component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION) || component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_ANTIBIOTICS)) {
                        AssessmentDrainVO obj = (AssessmentDrainVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                        at.setAssessment_id(obj.getAssessment_id());
                        at.setAlpha_id(obj.getAlpha_id());
                        AssessmentTreatmentVO atreat = tc.getAssessmentTreatment(at);
                        assess = atreat;
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        AssessmentDrainVO obj = (AssessmentDrainVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        TreatmentCommentVO t1 = new TreatmentCommentVO();
                        t1.setAssessment_id(obj.getAssessment_id());
                        t1.setWound_profile_type_id(obj.getWound_profile_type_id());
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        if (t2 != null) {
                            assess = t2;
                        } else {
                            request.setAttribute("is_null", "1");
                        }
                    }
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.SKIN_ASSESSMENT)) {
                    AssessmentSkinVO ost = new AssessmentSkinVO();
                    ost.setId(new Integer(id));
                    assess = (AbstractAssessmentVO) manager.getAssessment(ost);
                    if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                        AssessmentSkinVO obj = (AssessmentSkinVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        assess = (WoundAssessmentVO) obj.getWoundAssessment();
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        AssessmentSkinVO obj = (AssessmentSkinVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentNPWTVO atreat = nmanager.getNegativePressure(obj.getAssessment_id(), obj.getAlpha_id());
                        if(atreat != null){
                            assess = atreat;
                            
                        }else{
                            request.setAttribute("is_null","1");
                        }
                        
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT) || component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION) || component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_ANTIBIOTICS)) {
                        AssessmentSkinVO obj = (AssessmentSkinVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                        at.setAssessment_id(obj.getAssessment_id());
                        at.setAlpha_id(obj.getAlpha_id());
                        AssessmentTreatmentVO atreat = tc.getAssessmentTreatment(at);
                        assess = atreat;
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        AssessmentSkinVO obj = (AssessmentSkinVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        TreatmentCommentVO t1 = new TreatmentCommentVO();
                        t1.setAssessment_id(obj.getAssessment_id());
                        t1.setWound_profile_type_id(obj.getWound_profile_type_id());
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        if (t2 != null) {
                            assess = t2;
                        } else {
                            request.setAttribute("is_null", "1");
                        }
                    }
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.BURN_ASSESSMENT)) {
                    AssessmentBurnVO ost = new AssessmentBurnVO();
                    ost.setId(new Integer(id));
                    assess = (AbstractAssessmentVO) manager.getAssessment(ost);
                    if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                        AssessmentBurnVO obj = (AssessmentBurnVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        assess = (WoundAssessmentVO) obj.getWoundAssessment();
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        AssessmentBurnVO obj = (AssessmentBurnVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentNPWTVO atreat = nmanager.getNegativePressure(obj.getAssessment_id(), obj.getAlpha_id());
                       if(atreat != null){
                            assess = atreat;
                            
                        }else{
                            request.setAttribute("is_null","1");
                        }
                        
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT) || component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION) || component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_ANTIBIOTICS)) {
                        AssessmentBurnVO obj = (AssessmentBurnVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                        at.setAssessment_id(obj.getAssessment_id());
                        at.setAlpha_id(obj.getAlpha_id());
                        AssessmentTreatmentVO atreat = tc.getAssessmentTreatment(at);
                        assess = atreat;
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        AssessmentBurnVO obj = (AssessmentBurnVO) assess;
                        request.setAttribute("assess_each_id",obj.getId());
                        TreatmentCommentVO t1 = new TreatmentCommentVO();
                        t1.setAssessment_id(obj.getAssessment_id());
                        t1.setWound_profile_type_id(obj.getWound_profile_type_id());
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        if (t2 != null) {
                            assess = t2;
                        } else {
                            request.setAttribute("is_null", "1");
                        }
                    }
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.WOUND_PROFILE)) {
                    WoundProfileVO ost = new WoundProfileVO();
                    ost.setId(new Integer(id));
                    assess = wp.getWoundProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.PHYSICAL_EXAM)) {
                    PhysicalExamVO ost = new PhysicalExamVO();
                    ost.setId(new Integer(id));
                    assess = pp.getExam(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.OSTOMY_PROFILE)) {
                    WoundProfileVO ost = new WoundProfileVO();
                    ost.setId(new Integer(id));
                    assess = wp.getWoundProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.POSTOP_PROFILE)) {
                    WoundProfileVO ost = new WoundProfileVO();
                    ost.setId(new Integer(id));
                    assess = wp.getWoundProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.TUBES_DRAINS_PROFILE)) {
                    WoundProfileVO ost = new WoundProfileVO();
                    ost.setId(new Integer(id));
                    assess = wp.getWoundProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.PATIENT_PROFILE)) {
                    PatientProfileVO ost = new PatientProfileVO();
                    ost.setId(new Integer(id));
                    assess = pp.getPatientProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.DIAGNOSTIC_INVESTIGATION)) {
                    DiagnosticInvestigationVO ost = new DiagnosticInvestigationVO();
                    ost.setId(new Integer(id));
                    assess = pp.getDiagnosticInvestigation(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.BRADEN_SCORE_SECTION)) {
                    BradenVO ost = new BradenVO();
                    ost.setId(new Integer(id));
                    assess = pp.getBraden(ost);
                    //PatientProfileVO ppvo = pp.retrievePatientProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.FOOT_ASSESSMENT)) {
                    FootAssessmentVO ost = new FootAssessmentVO();
                    ost.setId(new Integer(id));
                    assess = pp.getFootAssessment(ost);
                    //PatientProfileVO ppvo = pp.retrievePatientProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.SENSORY_ASSESSMENT)) {
                    SensoryAssessmentVO ost = new SensoryAssessmentVO();
                    ost.setId(new Integer(id));
                    assess = pp.getSensoryAssessment(ost);
                    //PatientProfileVO ppvo = pp.retrievePatientProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_BASIC)) {
                    LimbBasicAssessmentVO ost = new LimbBasicAssessmentVO();
                    ost.setId(new Integer(id));
                    assess = pp.getLimbBasicAssessment(ost);
                    //PatientProfileVO ppvo = pp.retrievePatientProfile(ost);
                    request.setAttribute("assess", assess);
                } else if (component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_ADVANCED)) {
                    LimbAdvAssessmentVO ost = new LimbAdvAssessmentVO();
                    ost.setId(new Integer(id));
                    assess = pp.getLimbAdvAssessment(ost);
                    //PatientProfileVO ppvo = pp.retrievePatientProfile(ost);
                    request.setAttribute("assess", assess);
                }else if (component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_UPPER)) {
                    LimbUpperAssessmentVO ost = new LimbUpperAssessmentVO();
                    ost.setId(new Integer(id));
                    assess = pp.getLimbUpperAssessment(ost);
                    //PatientProfileVO ppvo = pp.retrievePatientProfile(ost);
                    request.setAttribute("assess", assess);
                }

                request.setAttribute("assessment", assess);
                request.setAttribute("list", listBD.getLists(component.getResource_id().intValue()));

                //load usersignature

                if (component.getTable_id() != null && component.getTable_id().equals(Constants.INVESTIGATION_SECTION)) {
                    DiagnosticInvestigationVO obj = (DiagnosticInvestigationVO) assess;
                    Collection<InvestigationsVO> in = obj.getInvestigations();
                    request.setAttribute("unserializedItem", in);
                    request.setAttribute("list", listBD.getLists(LookupVO.INVESTIGATIONS));
                    //PatientProfileVO ppvo = pp.retrievePatientProfile(ost);

                } else if (component.getField_name().equals("interfering_meds")  || component.getField_name().equals("patient_limitations") || component.getField_name().equals("medications_comments") || component.getField_name().equals("co_morbidities")) {//new checkbox_component.. eventually we'll merge with the next one.
                    if (component.getField_name().equals("interfering_meds") ||  component.getField_name().equals("patient_limitations") || component.getField_name().equals("medications_comments")) {
                        request.setAttribute("list", listBD.getLists(component.getResource_id()));
                    } else {
                        request.setAttribute("list", listBD.getLists(LookupVO.COMORBIDITIES, null, true));
                    }
                    PatientProfileVO pp2 = (PatientProfileVO) assess;
                    Collection<PatientProfileArraysVO> aa = pp2.getPatient_profile_arrays();
                    if(component.getField_name().equals("interfering_meds")){
                        List<ArrayValueObject> t = Common.filterArrayObjects(aa.toArray(new PatientProfileArraysVO[aa.size()]), LookupVO.INTERFERING_MEDS);
                         request.setAttribute("unserializedItem",t);
                    }else if(component.getField_name().equals("medications_comments")){
                        request.setAttribute("unserializedItem", Common.filterArrayObjects(aa.toArray(new PatientProfileArraysVO[aa.size()]), LookupVO.MED_COMMENTS));
                    }else if(component.getField_name().equals("patient_limitations")){
                        request.setAttribute("unserializedItem", Common.filterArrayObjects(aa.toArray(new PatientProfileArraysVO[aa.size()]), LookupVO.PATIENT_LIMITATIONS));
                    }else if(component.getField_name().equals("co_morbidities")){    
                        request.setAttribute("unserializedItem", Common.filterArrayObjects2Lookup(aa.toArray(new PatientProfileArraysVO[aa.size()]), LookupVO.COMORBIDITIES));
                    }
               } else if (component.getField_name().equals("adjunctive") || component.getField_name().equals("csresult")) {//new checkbox_component.. eventually we'll merge with the next one.
                    request.setAttribute("list", listBD.getLists(component.getResource_id()));
                    AssessmentTreatmentVO pp2 = (AssessmentTreatmentVO) assess;
                    if (pp2 != null) {
                        Collection<ArrayValueObject> aa = pp2.getAssessment_treatment_arrays();
                        request.setAttribute("unserializedItem", Common.filterArrayObjects(aa.toArray(new AssessmentTreatmentArraysVO[aa.size()]), component.getResource_id()));
                    }
                } else if(component.getField_name().equals("external_referrals")){
                    PatientProfileVO pp2 = (PatientProfileVO) assess;
                    request.setAttribute("list1",listBD.getLists(LookupVO.EXTERNAL_REFERRAL));
                    request.setAttribute("list2",listBD.getLists(LookupVO.ARRANGED));
                     List<DropdownBean> refs = new ArrayList();
                       for(ExternalReferralVO ref : pp2.getExternalReferrals()){
                          LookupVO ext = listBD.getListItem(ref.getExternal_referral_id());
                            LookupVO arr = listBD.getListItem(ref.getArranged_id());
                            refs.add(new DropdownBean(ref.getExternal_referral_id()+" : "+ref.getArranged_id(),ext.getName(language)+" : "+arr.getName(language)));
                        }
                        request.setAttribute("unserializedItem",refs);
                }else if (component.getComponent_type().equals(Constants.CHECKBOX_COMPONENT_TYPE)) {
                    String other = (String) Common.getFieldValue(component.getField_name() + "_other", assess);
                    if (other != null) {
                        request.setAttribute("otherText", other);
                    }
                    request.setAttribute("unserializedItem", Serialize.arrayIze((String) Common.getFieldValue(component.getField_name(), assess)));
                } else if (component.getComponent_type().equals(Constants.CLOCK_COMPONENT_TYPE)) {
                } else if (component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_GOALS_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_MULTIPLELIST_COMPONENT_TYPE)) {
                    if (component.getField_name().equals("goals")) {
                        request.setAttribute("list_wound", listBD.getLists(LookupVO.WOUND_GOALS));
                        request.setAttribute("list_ostomy", listBD.getLists(LookupVO.OSTOMY_GOALS));
                        request.setAttribute("list_postop", listBD.getLists(LookupVO.INCISION_GOALS));
                        request.setAttribute("list_drains", listBD.getLists(LookupVO.TUBEDRAIN_GOALS));
                        request.setAttribute("list_skin", listBD.getLists(LookupVO.SKIN_GOALS));
                        request.setAttribute("list_burn", listBD.getLists(LookupVO.BURN_GOALS));
                    } else if (component.getField_name().equals("wound_acquired")) {
                        request.setAttribute("list_wound", listBD.getLists(LookupVO.WOUND_ACQUIRED));
                        request.setAttribute("list_ostomy", listBD.getLists(LookupVO.WOUND_ACQUIRED));
                        request.setAttribute("list_postop", listBD.getLists(LookupVO.WOUND_ACQUIRED));
                        request.setAttribute("list_drains", listBD.getLists(LookupVO.WOUND_ACQUIRED));
                        request.setAttribute("list_skin", listBD.getLists(LookupVO.WOUND_ACQUIRED));
                        request.setAttribute("list_burn", listBD.getLists(LookupVO.WOUND_ACQUIRED));
                    } else {
                        request.setAttribute("list_wound", listBD.getLists(LookupVO.ETIOLOGY));
                        request.setAttribute("list_ostomy", listBD.getLists(LookupVO.OSTOMY_ETIOLOGY));
                        request.setAttribute("list_postop", listBD.getLists(LookupVO.POSTOP_ETIOLOGY));
                        request.setAttribute("list_drains", listBD.getLists(LookupVO.DRAINS_ETIOLOGY));
                        request.setAttribute("list_skin", listBD.getLists(LookupVO.SKIN_ETIOLOGY));
                        request.setAttribute("list_burn", listBD.getLists(LookupVO.BURN_ETIOLOGY));
                    }
                    WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
                    tloco.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                    tloco.setDischarge(0); // Only open alphas
                    com.pixalere.assessment.dao.WoundAssessmentLocationDAO locationDAO = new com.pixalere.assessment.dao.WoundAssessmentLocationDAO();
                    Collection<WoundAssessmentLocationVO> allalphas = locationDAO.findAllByCriteria(tloco, userVO.getId(), false, "");//4th parameter (extra sort) is ignored
                    Vector justAlphas = new Vector();//getting alphas for csresults component
                    for (WoundAssessmentLocationVO v : allalphas) {
                        justAlphas.add(Common.getAlphaText(v.getAlpha(), locale));
                    }
                    request.setAttribute("list2", justAlphas);
                    if (component.getTable_id().equals(Constants.ETIOLOGY_SECTION)) {
                        WoundProfileVO w = (WoundProfileVO) assess;
                        request.setAttribute("unserializedItem", w.getEtiologies());
                    } else if (component.getTable_id().equals(Constants.GOALS_SECTION)) {
                        WoundProfileVO w = (WoundProfileVO) assess;
                        request.setAttribute("unserializedItem", w.getGoals());
                    } else if (component.getTable_id().equals(Constants.ACQUIRED_SECTION)) {
                        WoundProfileVO w = (WoundProfileVO) assess;
                        request.setAttribute("unserializedItem", w.getWound_acquired());
                    } else {
                        request.setAttribute("unserializedItem", Serialize.arrayIze((String) Common.getFieldValue(component.getField_name(), assess)));
                    }
                    WoundAssessmentLocationVO tloco2 = new WoundAssessmentLocationVO();
                    tloco2.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                    tloco2.setDischarge(1); // Only closed alphas
                    // tloco2.setAlpha_details().setDeleted(0); // No deleted incisions
                    Collection<WoundAssessmentLocationVO> allalphas2 = locationDAO.findAllByCriteria(tloco2, -1, false, "");//4th parameter (extra sort) is ignored
                    request.setAttribute("list3", allalphas2);
                } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_COMPONENT_TYPE)) {
                    request.setAttribute("unserializedItem", Serialize.unserializeStraight((String) Common.getFieldValue(component.getField_name(), assess)));
                    request.setAttribute("num_items", (String) Common.getFieldValue("num_" + component.getField_name(), assess));
                } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {//antibiotics
                    AssessmentTreatmentVO at = (AssessmentTreatmentVO) assess;
                    if (at != null) {
                        Collection<AssessmentAntibioticVO> antibiotics = at.getAntibiotics();
                        List<String> antis = new ArrayList();
                        SimpleDateFormat sf = new SimpleDateFormat("d/M/yyyy");
                        for (AssessmentAntibioticVO a : antibiotics) {
                            String startdate = "";
                            String enddate = "";
                            if (a.getStart_date() != null && !a.getStart_date().equals("")) {
                                startdate = sf.format(a.getStart_date());
                            }
                            if (a.getEnd_date() != null && !a.getEnd_date().equals("")) {
                                enddate = sf.format(a.getEnd_date());
                            }
                            String antibiotic = a.getAntibiotic() + " : " + startdate + " to " + enddate;
                            antis.add(antibiotic);
                        }
                        request.setAttribute("unserializedItem", antis);
                    }
                    /*WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
                     tloco.setWound_profile_type_id(new Integer((String) wound_profile_type_id));
                     com.pixalere.assessment.dao.WoundAssessmentLocationDAO locationDAO = new com.pixalere.assessment.dao.WoundAssessmentLocationDAO();
                     Collection<WoundAssessmentLocationVO> allalphas = locationDAO.findAllByCriteria(tloco, userVO.getId(), false, "");//4th parameter (extra sort) is ignored
                     */
                    Vector justAlphas = new Vector();//getting alphas for csresults component
                    //for (WoundAssessmentLocationVO v : allalphas) {
                    justAlphas.add(Common.getAlphaText(at.getAlphaLocation().getAlpha(), locale));
                    // }
                    request.setAttribute("just_alphas", justAlphas);
                } else if (component.getComponent_type().equals(Constants.DROPDOWN_DATE_COMPONENT_TYPE)) {
                    request.setAttribute("unserializedItem", Serialize.unserializeStraight((String) Common.getFieldValue(component.getField_name(), assess)));
                } else if (component.getComponent_type().equals(Constants.MULTILIST_COMPONENT_TYPE)) {
                    request.setAttribute("fld_name", "current_" + component.getField_name());
                    request.setAttribute("unserializedItem", Serialize.arrayIzeWithList((String) Common.getFieldValue(component.getField_name(), assess), language));
                } else if (component.getComponent_type().equals(Constants.DROPDOWN_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.RADIO_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.YN_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.YNN_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.Y_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.YN_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.ONOFFNA_COMPONENT_TYPE)) {
                    request.setAttribute("unserializedItem", (Integer) Common.getFieldValue(component.getField_name(), assess));
                } else if (component.getComponent_type().equals(Constants.TEXTAREA_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.TEXTFIELD_COMPONENT_TYPE)  || component.getComponent_type().equals(Constants.NUMBER_COMPONENT_TYPE)) {
                    try {
                        request.setAttribute("unserializedItem", (String) Common.getFieldValue(component.getField_name(), assess));
                    } catch (ClassCastException e) {
                        try {
                            request.setAttribute("unserializedItem", (Integer) Common.getFieldValue(component.getField_name(), assess));
                        } catch (Exception ex) {
                            try {
                                request.setAttribute("unserializedItem", (Double) Common.getFieldValue(component.getField_name(), assess));
                            } catch (Exception exx) {
                            }
                        }
                    }
                } else if (component.getComponent_type().equals(Constants.BRADEN_SCORE_COMPONENT_TYPE)) {
                    BradenVO b = (BradenVO) assess;
                    request.setAttribute("unserializedItem", (Integer) Common.getFieldValue(component.getField_name(), assess));
                }  else if (component.getComponent_type().equals(Constants.DATE_COMPONENT_TYPE)) {
                    if (Common.getFieldValue(component.getField_name(), assess) instanceof Date) {
                        Date epoch = (Date) Common.getFieldValue(component.getField_name(), assess);
                        Vector date = PDate.getDateInVector(epoch);
                        request.setAttribute("unserializedItem", date);
                    } else {
                        Date epoch = (Date) Common.getFieldValue(component.getField_name(), assess);
                        Vector date = PDate.getDateInVector(epoch);
                        request.setAttribute("unserializedItem", date);
                    }
                }else if (component.getComponent_type().equals(Constants.DATETIME_COMPONENT_TYPE)) {
                    if (Common.getFieldValue(component.getField_name(), assess) instanceof Date) {
                        Date epoch = (Date) Common.getFieldValue(component.getField_name(), assess);
                        Vector date = PDate.getDateTimeInVector(epoch);
                        request.setAttribute("unserializedItem", date);
                    } else {
                        Date epoch = (Date) Common.getFieldValue(component.getField_name(), assess);
                        Vector date = PDate.getDateInVector(epoch);
                        request.setAttribute("unserializedItem", date);
                    }
                } else if (component.getComponent_type().equals(Constants.MULTIPLE_TEXTAREA_COMPONENT_TYPE)) {
                    Vector v = Serialize.unserializeStraight((String) Common.getFieldValue(component.getField_name(), assess));
                    request.setAttribute("unserializedItem", v);
                }  else if (component.getComponent_type().equals(Constants.MEASUREMENT_COMPONENT_TYPE)) {
                    String[] s = new String[2];
                    s[0] = ((Integer) Common.getFieldValue(component.getField_name() + "_cm", assess)) + "";
                    s[1] = ((Integer) Common.getFieldValue(component.getField_name() + "_mm", assess)) + "";
                    request.setAttribute("unserializedItem", s);
                }else if (component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {
                    if (component.getField_name().equals("undermining_location")) {
                        AssessmentEachwoundVO eachwound = (AssessmentEachwoundVO) assess;
                        request.setAttribute("unserializedItem", eachwound.getUnderminings());
                    } else if (component.getField_name().equals("sinus_location")) {
                        AssessmentEachwoundVO eachwound = (AssessmentEachwoundVO) assess;
                        request.setAttribute("unserializedItem", eachwound.getSinustracts());
                    } else if (component.getField_name().equals("mucocutaneous_margin_location")) {
                        AssessmentOstomyVO eachwound = (AssessmentOstomyVO) assess;
                        request.setAttribute("unserializedItem", eachwound.getSeperations());
                    }
                    // Vector mmas = (Vector) Common.getFieldValue(component.getField_name(), assess);
                    //request.setAttribute("unserializedItem", mmas);
                } else if (component.getComponent_type().equals(Constants.DOB_COMPONENT_TYPE)) {
                    String[] s = new String[3];
                    PatientProfileVO tv = new PatientProfileVO();
                    s[0] = (String) Common.getFieldValue(component.getField_name() + "_month", assess);
                    s[1] = (String) Common.getFieldValue(component.getField_name() + "_day", assess);
                    s[2] = (String) Common.getFieldValue(component.getField_name() + "_year", assess);
                    s[0] = s[0];
                    s[1] = s[1];
                    request.setAttribute("unserializedItem", s);
                } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                    Vector currentProducts = new Vector();
                    Vector currentProductsId = new Vector();
                    com.pixalere.patient.service.PatientServiceImpl pBD = new com.pixalere.patient.service.PatientServiceImpl(language);
                    ProductsVO productsVO = new ProductsVO();
                    ProductsServiceImpl productsDAO = new ProductsServiceImpl();
                    ProductCategoryVO productCategoryVO = new ProductCategoryVO();
                    ProductCategoryServiceImpl productCategoryDAO = new ProductCategoryServiceImpl();
                    WoundAssessmentVO waVO = new WoundAssessmentVO();
                    productsVO.setActive(new Integer(1));
                    AbstractAssessmentVO sa = (AbstractAssessmentVO) assess;
                    PatientAccountVO patient = pBD.getPatient(sa.getPatient_id().intValue());
                    request.setAttribute("product_list", productsDAO.getAllProducts(userVO));
                    productCategoryVO.setActive(new Integer(1));
                    request.setAttribute("category_list", productCategoryDAO.getAllProductCategoriesByCriteria(productCategoryVO));
                    //show all product history in history list box*************************
                    //@todo Switch to BD
                    Vector current_alphas = new Vector();
                    AssessmentServiceImpl assessmentDAO = new AssessmentServiceImpl(language);
                    current_alphas.add(sa.getAlphaLocation());
                    //Populate Previous products
                    
                        
                        
                        if (sa != null) {
                            log.info("##$$##$$##-----LAST ASSESSMENT IS NOT NULL-----##$$##$$##");
                            // System.out.println("##$$##$$##-----LAST ASSESSMENT COLLECTION SIZE == " + lastAssessmentCol.size() + "-----##$$##$$##");
                            //for (AbstractAssessmentVO lastAssessment : lastAssessmentCol) {
                                int flag = 0;
                                log.info("##$$##$$##-----NEXT ASSESSMENT-----##$$##$$##");
                                // unserialize the products from the last assessment and pass it into the
                                // vm template to show product history.
                                //get the appropriate alpha for the assesment to be passed to getProductsUnserialized.
                                AssessmentProductVO tp2 = new AssessmentProductVO();
                                if(sa instanceof AssessmentEachwoundVO){
                                    tp2.setWound_assessment_id(sa.getId());
                                }else if(sa instanceof AssessmentIncisionVO){
                                    tp2.setIncision_assessment_id(sa.getId());
                                }else if(sa instanceof AssessmentDrainVO){
                                    tp2.setDrain_assessment_id(sa.getId());
                                }else if(sa instanceof AssessmentOstomyVO){
                                    tp2.setOstomy_assessment_id(sa.getId());
                                }else if(sa instanceof AssessmentSkinVO){
                                    tp2.setSkin_assessment_id(sa.getId());
                                }else if(sa instanceof AssessmentBurnVO){
                                    tp2.setBurn_assessment_id(sa.getId());
                                }else{
                                    tp2.setAssessment_id(sa.getAssessment_id());
                                }
                                tp2.setAlpha_id(sa.getAlpha_id());
                                Collection<AssessmentProductVO> products = assessmentDAO.getAllProducts(tp2);
                                //this is where I will replace the prduct name with the product Id.
                                for (AssessmentProductVO product : products) {
                                    ProductsVO item = product.getProduct();
                                    if (product.getProduct_id().equals(new Integer("-1"))) {
                                        currentProducts.addElement(product.getQuantity() + ":" + Common.getAlphaText(sa.getAlphaLocation().getAlpha(), locale) + ":" + product.getOther());
                                        currentProductsId.addElement(product.getQuantity() + "|" + product.getAlpha_id() + "|" + product.getOther());
                                    } else {
                                        currentProducts.addElement(product.getQuantity() + ":" + Common.getAlphaText(sa.getAlphaLocation().getAlpha(), locale) + ":" + item.getTitle());
                                        currentProductsId.addElement(product.getQuantity() + "|" + product.getAlpha_id() + "|" + product.getProduct_id());
                                    }
                                }
                           // }
                        }
                    
                    request.setAttribute("historyWithIds", currentProductsId);
                    request.setAttribute("history", currentProducts);
                    //Collections.sort(current_alphas);
                    //if (current_alphas.size() > 1 && !current_alphas.contains("All")) {
                    //    current_alphas.add(0, "All");
                    //}
                    request.setAttribute("alpha_list", current_alphas);
                }
            }
        } catch (DataAccessException e) {
            log.error("An application exception has been raised in AssessmentFlowchartSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in AssessmentFlowchartSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("uploader.assessmenterror.success"));
    }
}
