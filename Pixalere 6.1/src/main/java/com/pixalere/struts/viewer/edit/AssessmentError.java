package com.pixalere.struts.viewer.edit;

import com.pixalere.assessment.service.AssessmentNPWTServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.assessment.bean.*;
import com.pixalere.auth.bean.ProfessionalVO;
import java.text.ParseException;
import com.pixalere.utils.*;
import com.pixalere.common.bean.InformationPopupVO;

import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.service.PatientReportService;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.bean.*;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.bean.PatientReportVO;
import com.pixalere.wound.bean.*;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @todo: This class needs major refactoring!
 * @author travis
 */
public class AssessmentError extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentError.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        AssessmentErrorForm viewerForm = (AssessmentErrorForm) form;

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        String id = request.getParameter("id");
        String field_name = request.getParameter("field");
        String patient_id = "0";

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        //@todo create method in BD
        com.pixalere.assessment.dao.AssessmentDAO dao = new com.pixalere.assessment.dao.AssessmentDAO();
        com.pixalere.assessment.bean.NursingFixesVO fixes = new com.pixalere.assessment.bean.NursingFixesVO();
        TreatmentCommentsServiceImpl tc = new TreatmentCommentsServiceImpl();
        WoundAssessmentServiceImpl wabd = new WoundAssessmentServiceImpl();
        AssessmentServiceImpl bd = new AssessmentServiceImpl(language);
        AssessmentNPWTServiceImpl nc = new AssessmentNPWTServiceImpl();
        PatientProfileServiceImpl pbd = new PatientProfileServiceImpl(language);
        WoundServiceImpl wbd = new WoundServiceImpl(language);
        WoundAssessmentLocationServiceImpl lbd = new WoundAssessmentLocationServiceImpl();
        Object eachVO = null;
        String is_null = (String) request.getParameter("is_null");
        String title = "";
        try {
            ComponentsVO comp = new ComponentsVO();
            comp.setId(new Integer(field_name));
            GUIServiceImpl gui = new GUIServiceImpl();
            ComponentsVO component = gui.getComponent(comp);
            String value = null;
            title = Common.getLocalizedString(component.getLabel_key(), locale);
            Hashtable fields = new Hashtable();
            int array_size = 0;
            //prepare updated data to be written depending on component_type

            if (component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_GOALS_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_MULTIPLELIST_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.CHECKBOX_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.MULTILIST_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.MULTI_DROPDOWN_COMPONENT_TYPE)) {
                String f_name = component.getField_name();


                String[] t = (String[]) request.getParameterValues(f_name);
                if (t != null) {
                    array_size = t.length;
                    value = Serialize.serialize(t);
                }
                String other = (String) request.getParameter(component.getField_name() + "_other");
                if (other == null || other.equals("")) {
                    other = "";
                }
                ComponentsVO tvc = new ComponentsVO();
                tvc.setField_name(component.getField_name() + "_other");
                ComponentsVO cv = gui.getComponent(tvc);
                if (cv != null) {
                    fields.put(component.getField_name() + "_other", other);
                }
                if (value == null) {
                    value = "";
                }
                fields.put(component.getField_name(), value);

            } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                //defer to later... below
            } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {
                String[] t = (String[]) request.getParameterValues(component.getField_name());

                value = Serialize.serialize(t);
                fields.put(component.getField_name(), value);
            }  else if (component.getComponent_type().equals(Constants.MEASUREMENT_COMPONENT_TYPE)) {

                String cm = request.getParameter(component.getField_name() + "_cm");
                String mm = request.getParameter(component.getField_name() + "_mm");
                fields.put(component.getField_name() + "_cm", cm);
                fields.put(component.getField_name() + "_mm", mm);
             }else if (component.getComponent_type().equals(Constants.DATE_COMPONENT_TYPE)) {
                String day = request.getParameter(component.getField_name() + "_day");
                String month = request.getParameter(component.getField_name() + "_month");
                String year = request.getParameter(component.getField_name() + "_year");
                
                if (!day.equals("") && !month.equals("") && !year.equals("")) {
                    Date date = PDate.getDate(Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(year));
                    fields.put(component.getField_name(), date.getTime()+"");
                     System.out.println("Date: "+date.getTime());
                }
                if (value == null) {
                   //fields.put(component.getField_name(), "");
                }
                //fields.put(component.getField_name(), value);
            }else if (component.getComponent_type().equals(Constants.DATETIME_COMPONENT_TYPE)) {
                String day = request.getParameter(component.getField_name() + "_day");
                String month = request.getParameter(component.getField_name() + "_month");
                String year = request.getParameter(component.getField_name() + "_year");
                String timeage = request.getParameter("time");
                int hour = 0;
                int minute = 0;
                if(timeage!=null){
                        
                        if (timeage!=null && !timeage.equals("") && timeage.length()==4) {
                                hour = Integer.parseInt(timeage.substring(0,2));
                                minute = Integer.parseInt(timeage.substring(2,4));
                            } else {
                                minute = Integer.parseInt(pdate.getMin(new Date()));
                                hour = Integer.parseInt(pdate.getHour(new Date()));
                            }
                }
                if (!day.equals("") && !month.equals("") && !year.equals("")) {
                    Date date = PDate.getDate(Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(year), hour, minute);
                    if (date == null) {
                        fields.put(component.getField_name(), "");
                    }else{
                        fields.put(component.getField_name(), date.getTime()+"");
                    }
                }
                
                //fields.put(component.getField_name(), value);
            } else if (component.getComponent_type().equals(Constants.NEWDATE_COMPONENT_TYPE)) {
                String day = request.getParameter(component.getField_name() + "_day");
                String month = request.getParameter(component.getField_name() + "_month");
                String year = request.getParameter(component.getField_name() + "_year");
                if (!day.equals("") && !month.equals("") && !year.equals("")) {//vPDate.getDate(bloodsugar_year,bloodsugar_month,bloodsugar_day)
                    value = year + "-" + month + "-" + day;
                }
                if (value == null) {
                    value = "";
                }
                fields.put(component.getField_name(), value);
            } else if (component.getComponent_type().equals(Constants.DOB_COMPONENT_TYPE)) {
                PatientProfileVO tv = new PatientProfileVO();
                String day = request.getParameter(component.getField_name() + "_day");
                String month = request.getParameter(component.getField_name() + "_month");
                String year = request.getParameter(component.getField_name() + "_year");

                fields.put(component.getField_name() + "_day", day + "");
                fields.put(component.getField_name() + "_month", month + "");
                fields.put(component.getField_name() + "_year", year);
            } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_COMPONENT_TYPE)) {
                int num_items = new Integer((String) request.getParameter("num_" + component.getField_name())).intValue();
                Vector items = new Vector();
                for (int x = 1; x <= num_items; x++) {
                    String item = request.getParameter(component.getField_name() + "_" + x);
                    items.add(item);
                }
                fields.put(component.getField_name(), Serialize.serializeStraight(items.toArray()));
            } else if (component.getComponent_type().equals(Constants.MULTIPLE_TEXTAREA_COMPONENT_TYPE)) {
                int num = Integer.parseInt((String) request.getParameter("num_" + component.getField_name()));
                Vector fistulas = new Vector();
                for (int x = 1; x <= num; x++) {
                    String tx = ((String) request.getParameter(component.getField_name() + "_" + x) == null ? "" : (String) request.getParameter(component.getField_name() + "_" + x));
                    fistulas.add(tx);
                }
                String val = Serialize.serializeStraight(fistulas.toArray());
                fields.put(component.getField_name(), val);
            } else {


                value = request.getParameter(component.getField_name());//Get value from form.
                if (value == null) {
                    value = "";
                }
                fields.put(component.getField_name(), value);
            }

            //Grab the last saved object from the specific table
            if (component.getTable_id() != null && (component.getTable_id().equals(new Integer(Constants.ASSESSMENT_TREATMENT)) || component.getTable_id().equals(new Integer(Constants.TREATMENT_ARRAY_SECTION)) || component.getTable_id().equals(new Integer(Constants.ASSESSMENT_ANTIBIOTICS)))) {
                AssessmentTreatmentVO t1 = new AssessmentTreatmentVO();
                t1.setId(new Integer(id));
                AssessmentTreatmentVO t2 = tc.getAssessmentTreatment(t1);
                eachVO = t2;
            } else if (component.getFlowchart().equals(new Integer(Constants.WOUND_ASSESSMENT))) {
                AssessmentEachwoundVO ost = new AssessmentEachwoundVO();
                if (id != null && !id.equals("")) {
                    ost.setId(new Integer(id));
                    eachVO = bd.getAssessment(ost);
                }
                if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.ASSESSMENT))) {
                    AssessmentEachwoundVO a = (AssessmentEachwoundVO) eachVO;
                    eachVO = a.getWoundAssessment();
                } else if (component.getTable_id() != null && (component.getTable_id().equals(new Integer(Constants.NPWT_SECTION)))) {
                    AssessmentNPWTVO t1 = new AssessmentNPWTVO();
                    //a negative pressure exists.
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        AssessmentNPWTVO t2 = nc.getNegativePressure(t1);
                        eachVO = t2;
                    } else {//no negative pressure exists.. create one.
                        AssessmentEachwoundVO each2 = (AssessmentEachwoundVO) eachVO;
                        AssessmentNPWTVO t2 = new AssessmentNPWTVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setProfessional_id(userVO.getId());
                        AssessmentNPWTAlphaVO alpha = new AssessmentNPWTAlphaVO();
                        alpha.setAlpha_id(each2.getAlpha_id());
                        Collection<AssessmentNPWTAlphaVO> alphas = new ArrayList();
                        alphas.add(alpha);
                        t2.setAlphas(alphas);
                        eachVO = t2;
                    }
                } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.TREATMENT_COMMENTS_SECTION))) {
                    TreatmentCommentVO t1 = new TreatmentCommentVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        eachVO = t2;
                    } else {
                        AssessmentEachwoundVO each2 = (AssessmentEachwoundVO) eachVO;
                        TreatmentCommentVO t2 = new TreatmentCommentVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setWound_profile_type_id(each2.getWound_profile_type_id());
                        t2.setActive(1);
                        t2.setProfessional_id(userVO.getId());
                        t2.setCreated_on(new Date());
                        t2.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                        eachVO = t2;
                    }

                }
            } else if (component.getFlowchart().equals(new Integer(Constants.OSTOMY_ASSESSMENT))) {
                AssessmentOstomyVO ost = new AssessmentOstomyVO();
                if (id != null && !id.equals("")) {
                    ost.setId(new Integer(id));
                    eachVO = bd.getAssessment(ost);
                }
                if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.ASSESSMENT))) {
                    AssessmentOstomyVO a = (AssessmentOstomyVO) eachVO;
                    eachVO = a.getWoundAssessment();
                } else if (component.getTable_id() != null && (component.getTable_id().equals(new Integer(Constants.NPWT_SECTION)))) {
                    AssessmentNPWTVO t1 = new AssessmentNPWTVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        AssessmentNPWTVO t2 = nc.getNegativePressure(t1);
                        eachVO = t2;
                    } else {
                        AssessmentOstomyVO each2 = (AssessmentOstomyVO) eachVO;
                        AssessmentNPWTVO t2 = new AssessmentNPWTVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setProfessional_id(userVO.getId());
                        AssessmentNPWTAlphaVO alpha = new AssessmentNPWTAlphaVO();
                        alpha.setAlpha_id(each2.getAlpha_id());
                        Collection<AssessmentNPWTAlphaVO> alphas = new ArrayList();
                        alphas.add(alpha);
                        t2.setAlphas(alphas);
                        eachVO = t2;
                    }
                } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.TREATMENT_COMMENTS_SECTION))) {
                    TreatmentCommentVO t1 = new TreatmentCommentVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        eachVO = t2;
                    } else {
                        AssessmentOstomyVO each2 = (AssessmentOstomyVO) eachVO;
                        TreatmentCommentVO t2 = new TreatmentCommentVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setWound_profile_type_id(each2.getWound_profile_type_id());
                        t2.setActive(1);
                        t2.setProfessional_id(userVO.getId());
                        t2.setCreated_on(new Date());
                        t2.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                        eachVO = t2;
                    }
                }
            } else if (component.getFlowchart().equals(new Integer(Constants.SKIN_ASSESSMENT))) {
                AssessmentSkinVO ost = new AssessmentSkinVO();
                if (id != null && !id.equals("")) {
                    ost.setId(new Integer(id));
                    eachVO = bd.getAssessment(ost);
                }
                if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.ASSESSMENT))) {
                    AssessmentSkinVO a = (AssessmentSkinVO) eachVO;
                    eachVO = a.getWoundAssessment();
                } else if (component.getTable_id() != null && (component.getTable_id().equals(new Integer(Constants.NPWT_SECTION)))) {
                    
                    AssessmentNPWTVO t1 = new AssessmentNPWTVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        AssessmentNPWTVO t2 = nc.getNegativePressure(t1);
                        eachVO = t2;
                    } else {
                        AssessmentSkinVO each2 = (AssessmentSkinVO) eachVO;
                        AssessmentNPWTVO t2 = new AssessmentNPWTVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setProfessional_id(userVO.getId());
                        AssessmentNPWTAlphaVO alpha = new AssessmentNPWTAlphaVO();
                        alpha.setAlpha_id(each2.getAlpha_id());
                        Collection<AssessmentNPWTAlphaVO> alphas = new ArrayList();
                        alphas.add(alpha);
                        t2.setAlphas(alphas);
                        eachVO = t2;
                    }
                } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.TREATMENT_COMMENTS_SECTION))) {
                    TreatmentCommentVO t1 = new TreatmentCommentVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        eachVO = t2;
                    } else {
                        AssessmentSkinVO each2 = (AssessmentSkinVO) eachVO;
                        TreatmentCommentVO t2 = new TreatmentCommentVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setWound_profile_type_id(each2.getWound_profile_type_id());
                        t2.setActive(1);
                        t2.setProfessional_id(userVO.getId());
                        t2.setCreated_on(new Date());
                        t2.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                        eachVO = t2;
                    }
                }
            } else if (component.getFlowchart().equals(new Integer(Constants.INCISION_ASSESSMENT))) {
                AssessmentIncisionVO ost = new AssessmentIncisionVO();
                if (id != null && !id.equals("")) {
                    ost.setId(new Integer(id));
                    eachVO = bd.getAssessment(ost);
                }
                if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.ASSESSMENT))) {
                    AssessmentIncisionVO a = (AssessmentIncisionVO) eachVO;
                    eachVO = a.getWoundAssessment();
                } else if (component.getTable_id() != null && (component.getTable_id().equals(new Integer(Constants.NPWT_SECTION)))) {
                    AssessmentNPWTVO t1 = new AssessmentNPWTVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        AssessmentNPWTVO t2 = nc.getNegativePressure(t1);
                        eachVO = t2;
                    } else {
                        AssessmentIncisionVO each2 = (AssessmentIncisionVO) eachVO;
                        AssessmentNPWTVO t2 = new AssessmentNPWTVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setProfessional_id(userVO.getId());
                        AssessmentNPWTAlphaVO alpha = new AssessmentNPWTAlphaVO();
                        alpha.setAlpha_id(each2.getAlpha_id());
                        Collection<AssessmentNPWTAlphaVO> alphas = new ArrayList();
                        alphas.add(alpha);
                        t2.setAlphas(alphas);
                        eachVO = t2;
                    }
                } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.TREATMENT_COMMENTS_SECTION))) {
                    TreatmentCommentVO t1 = new TreatmentCommentVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        eachVO = t2;
                    } else {
                        AssessmentIncisionVO each2 = (AssessmentIncisionVO) eachVO;
                        TreatmentCommentVO t2 = new TreatmentCommentVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setWound_profile_type_id(each2.getWound_profile_type_id());
                        t2.setActive(1);
                        t2.setProfessional_id(userVO.getId());
                        t2.setCreated_on(new Date());
                        t2.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                        eachVO = t2;
                    }
                }
            } else if (component.getFlowchart().equals(new Integer(Constants.DRAIN_ASSESSMENT))) {
                AssessmentDrainVO ost = new AssessmentDrainVO();
                if (id != null && !id.equals("")) {
                    ost.setId(new Integer(id));
                    eachVO = bd.getAssessment(ost);
                }
                if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.ASSESSMENT))) {
                    AssessmentDrainVO a = (AssessmentDrainVO) eachVO;
                    eachVO = a.getWoundAssessment();
                } else if (component.getTable_id() != null && (component.getTable_id().equals(new Integer(Constants.NPWT_SECTION)))) {
                    AssessmentNPWTVO t1 = new AssessmentNPWTVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        AssessmentNPWTVO t2 = nc.getNegativePressure(t1);
                        eachVO = t2;
                    } else {
                        AssessmentDrainVO each2 = (AssessmentDrainVO) eachVO;
                        AssessmentNPWTVO t2 = new AssessmentNPWTVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setProfessional_id(userVO.getId());
                        AssessmentNPWTAlphaVO alpha = new AssessmentNPWTAlphaVO();
                        alpha.setAlpha_id(each2.getAlpha_id());
                        Collection<AssessmentNPWTAlphaVO> alphas = new ArrayList();
                        alphas.add(alpha);
                        t2.setAlphas(alphas);
                        eachVO = t2;
                    }
                } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.TREATMENT_COMMENTS_SECTION))) {
                    TreatmentCommentVO t1 = new TreatmentCommentVO();
                    if (is_null == null || !is_null.equals("1")) {
                        t1.setId(new Integer(id));
                        TreatmentCommentVO t2 = tc.getTreatmentComment(t1);
                        eachVO = t2;
                    } else {
                        AssessmentDrainVO each2 = (AssessmentDrainVO) eachVO;
                        TreatmentCommentVO t2 = new TreatmentCommentVO();
                        t2.setPatient_id(each2.getPatient_id());
                        t2.setWound_id(each2.getWound_id());
                        t2.setAssessment_id(each2.getAssessment_id());
                        t2.setWound_profile_type_id(each2.getWound_profile_type_id());
                        t2.setActive(1);
                        t2.setProfessional_id(userVO.getId());
                        t2.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                        t2.setCreated_on(new Date());
                        eachVO = t2;
                    }
                }
            } else if (component.getFlowchart().equals(new Integer(Constants.PATIENT_PROFILE))) {
                PatientProfileVO ost = new PatientProfileVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getPatientProfile(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.PHYSICAL_EXAM))) {
                PhysicalExamVO ost = new PhysicalExamVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getExam(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.BRADEN_SCORE_SECTION))) {
                BradenVO ost = new BradenVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getBraden(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.FOOT_ASSESSMENT))) {
                FootAssessmentVO ost = new FootAssessmentVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getFootAssessment(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.SENSORY_ASSESSMENT))) {
                SensoryAssessmentVO ost = new SensoryAssessmentVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getSensoryAssessment(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.LIMB_ASSESSMENT_BASIC))) {
                LimbBasicAssessmentVO ost = new LimbBasicAssessmentVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getLimbBasicAssessment(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.LIMB_ASSESSMENT_ADVANCED))) {
                LimbAdvAssessmentVO ost = new LimbAdvAssessmentVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getLimbAdvAssessment(ost);
            }else if (component.getFlowchart().equals(new Integer(Constants.DIAGNOSTIC_INVESTIGATION))) {
                DiagnosticInvestigationVO ost = new DiagnosticInvestigationVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getDiagnosticInvestigation(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.LIMB_ASSESSMENT_UPPER))) {
                LimbUpperAssessmentVO ost = new LimbUpperAssessmentVO();
                ost.setId(new Integer(id));
                eachVO = pbd.getLimbUpperAssessment(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.WOUND_PROFILE))) {
                WoundProfileVO ost = new WoundProfileVO();
                ost.setId(new Integer(id));
                eachVO = wbd.getWoundProfile(ost);

            } else if (component.getFlowchart().equals(new Integer(Constants.OSTOMY_PROFILE))) {
                WoundProfileVO ost = new WoundProfileVO();
                ost.setId(new Integer(id));
                eachVO = wbd.getWoundProfile(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.POSTOP_PROFILE))) {
                WoundProfileVO ost = new WoundProfileVO();
                ost.setId(new Integer(id));
                eachVO = wbd.getWoundProfile(ost);
            } else if (component.getFlowchart().equals(new Integer(Constants.TUBES_DRAINS_PROFILE))) {
                WoundProfileVO ost = new WoundProfileVO();
                ost.setId(new Integer(id));
                eachVO = wbd.getWoundProfile(ost);
            }

            if (eachVO != null) {
                fixes.setProfessional_id(userVO.getId());
                fixes.setCreated_on(new Date());

                fixes.setInitials(pdate.getProfessionalSignature(new Date(), userVO, locale));

                //save the updated objects
                if (component != null) {
                    Hashtable hash = Common.getPrintableValue(eachVO, null, component, false, 0, language);

                    Vector<String> printage = (Vector) hash.get("value");
                    Vector<String> field = (Vector) hash.get("field");
                    //@todo needs refactoring..
                    //System.out.println("Component_TABLEID "+component.getTable_id());
                    if (component.getField_name().equals("products")) {
                        //remove old products first.
                        AssessmentProductVO productVO = new AssessmentProductVO();
                        Collection<AssessmentProductVO> prods = null;
                        AbstractAssessmentVO updateAssessmentVO = (AbstractAssessmentVO) eachVO;
                        fixes.setPatient_id(updateAssessmentVO.getPatient_id());
                        fixes.setRow_id(updateAssessmentVO.getId());
                        if (eachVO instanceof AssessmentEachwoundVO) {
                            AssessmentEachwoundVO a = (AssessmentEachwoundVO) eachVO;
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setWound_assessment_id(a.getId());
                            prods = bd.getAllProducts(p);
                        } else if (eachVO instanceof AssessmentIncisionVO) {
                            AssessmentIncisionVO a = (AssessmentIncisionVO) eachVO;
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setIncision_assessment_id(a.getId());
                            prods = bd.getAllProducts(p);
                        } else if (eachVO instanceof AssessmentOstomyVO) {
                            AssessmentOstomyVO a = (AssessmentOstomyVO) eachVO;
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setOstomy_assessment_id(a.getId());
                            prods = bd.getAllProducts(p);
                        } else if (eachVO instanceof AssessmentDrainVO) {
                            AssessmentDrainVO a = (AssessmentDrainVO) eachVO;
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setDrain_assessment_id(a.getId());
                            prods = bd.getAllProducts(p);
                        }else if (eachVO instanceof AssessmentSkinVO) {
                            AssessmentSkinVO a = (AssessmentSkinVO) eachVO;
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setSkin_assessment_id(a.getId());
                            prods = bd.getAllProducts(p);
                        }
                        
                        String v = Common.printVector(Common.printProducts(prods), true);
                        //insert nursing fixf or products

                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        //for (String v : printage) {
                        NursingFixesVO fv = fixes;
                        if (v.equals("")) {
                            v = "...";
                        }
                        fv.setError(v);
                        fv.setDelete_reason("");
                        fv.setField(component.getId() + "");
                        dao.insertNursingFixes(fv);
                        //cnt++;
                        //}
                        if (updateAssessmentVO instanceof AssessmentEachwoundVO) {
                            productVO.setWound_assessment_id(updateAssessmentVO.getId());
                            productVO.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        } else if (updateAssessmentVO instanceof AssessmentOstomyVO) {
                            productVO.setOstomy_assessment_id(updateAssessmentVO.getId());
                            productVO.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        } else if (updateAssessmentVO instanceof AssessmentIncisionVO) {
                            productVO.setIncision_assessment_id(updateAssessmentVO.getId());
                            productVO.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        } else if (updateAssessmentVO instanceof AssessmentDrainVO) {
                            productVO.setDrain_assessment_id(updateAssessmentVO.getId());
                            productVO.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        } else if (updateAssessmentVO instanceof AssessmentBurnVO) {
                            productVO.setBurn_assessment_id(updateAssessmentVO.getId());
                            productVO.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        } else if (updateAssessmentVO instanceof AssessmentSkinVO) {
                            productVO.setSkin_assessment_id(updateAssessmentVO.getId());
                            productVO.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        }
                        bd.removeProducts(productVO);

                        //
                        String[] products = (String[]) request.getParameterValues("current_products");
                        if (products != null) {
                            for (String product : products) {
                                StringTokenizer st = new StringTokenizer(product, "|");
                                String quantity = st.nextToken();
                                String alpha = st.nextToken();
                                String prod_id = st.nextToken();

                                AssessmentProductVO prod = new AssessmentProductVO();
                                prod.setActive(1);
                                prod.setPatient_id(updateAssessmentVO.getPatient_id());
                                prod.setWound_id(updateAssessmentVO.getWound_id());
                                prod.setAssessment_id(updateAssessmentVO.getAssessment_id());
                                if (updateAssessmentVO instanceof AssessmentEachwoundVO) {
                                    prod.setWound_assessment_id(updateAssessmentVO.getId());

                                } else if (updateAssessmentVO instanceof AssessmentOstomyVO) {
                                    prod.setOstomy_assessment_id(updateAssessmentVO.getId());
                                } else if (updateAssessmentVO instanceof AssessmentIncisionVO) {
                                    prod.setIncision_assessment_id(updateAssessmentVO.getId());
                                } else if (updateAssessmentVO instanceof AssessmentDrainVO) {
                                    prod.setDrain_assessment_id(updateAssessmentVO.getId());
                                } else if (updateAssessmentVO instanceof AssessmentBurnVO) {
                                    prod.setBurn_assessment_id(updateAssessmentVO.getId());
                                } else if (updateAssessmentVO instanceof AssessmentSkinVO) {
                                    prod.setSkin_assessment_id(updateAssessmentVO.getId());
                                }

                                prod.setAlpha_id(updateAssessmentVO.getAlpha_id());
                                prod.setProduct_id(prod_id.indexOf("Other;") == -1 ? new Integer(prod_id) : new Integer(-1));
                                if(!Common.isNumeric(quantity)){
                                   quantity="0";
                                }
                                prod.setQuantity(quantity);
                                prod.setOther(prod_id.indexOf("Other;") != -1 ? prod_id : "");
                                
                                bd.saveProduct(prod);

                            }
                        }
                    } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.SEPERATION_SECTION))) {
                        AssessmentOstomyVO each2 = (AssessmentOstomyVO) eachVO;
                        fixes.setPatient_id(each2.getPatient_id());
                        fixes.setRow_id(each2.getId());
                        fixes.setField(component.getId() + "");
                        Collection<AssessmentMucocSeperationsVO> m = each2.getSeperations();
                        String location = Common.parseMultipleSeperations(m, false, locale);//location
                        String depth = Common.parseMultipleSeperations(m, true, locale);//depth
                        if (location.equals("")) {
                            location = "...";
                        }
                        fixes.setError(location+" "+depth);
                        fixes.setDelete_reason("");
                        dao.insertNursingFixes(fixes);
                        if (depth.equals("")) {
                            depth = "...";
                        }
                        /*fixes.setId(null);
                        fixes.setField(component.getId() + "");
                        fixes.setError(depth);
                        fixes.setDelete_reason("");
                        dao.insertNursingFixes(fixes);*/
                        AssessmentMucocSeperationsVO t = new AssessmentMucocSeperationsVO();
                        t.setAssessment_ostomy_id(each2.getId());
                        t.setAlpha_id(each2.getAlpha_id());
                        bd.removeSeperation(t);
                        List<AssessmentMucocSeperationsVO> underminingList = getSeperation(request);
                        for (AssessmentMucocSeperationsVO u : underminingList) {
                            u.setAlpha_id(each2.getAlpha_id());
                            u.setAssessment_ostomy_id(each2.getId());
                            bd.saveSeperation(u);
                        }
                        each2.setLastmodified_on(new Date());
 bd.saveAssessment(each2);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.UNDERMINING_SECTION))) {
                        AssessmentEachwoundVO each2 = (AssessmentEachwoundVO) eachVO;
                        fixes.setPatient_id(each2.getPatient_id());
                        fixes.setRow_id(each2.getId());
                        fixes.setField(component.getId() + "");
                        Collection<AssessmentUnderminingVO> undermining = each2.getUnderminings();
                        String location = Common.parseMultipleUndermining(undermining, false, locale);//location
                        String depth = Common.parseMultipleUndermining(undermining, true, locale);//depth
                        if (location.equals("")) {
                            location = "...";
                        }
                        fixes.setError(location);
                        fixes.setDelete_reason("");
                        dao.insertNursingFixes(fixes);
                        if (depth.equals("")) {
                            depth = "...";
                        }
                        fixes.setId(null);
                        fixes.setField(component.getId() + "");
                        fixes.setError(depth);
                        fixes.setDelete_reason("");
                        dao.insertNursingFixes(fixes);
                        AssessmentUnderminingVO t = new AssessmentUnderminingVO();
                        t.setAssessment_wound_id(each2.getId());
                        t.setAlpha_id(each2.getAlpha_id());
                        bd.removeUndermining(t);

                        List<AssessmentUnderminingVO> underminingList = getUndermining(request);
                        //System.out.println("saved under: "+underminingList);
                        for (AssessmentUnderminingVO u : underminingList) {
                            u.setAlpha_id(each2.getAlpha_id());
                            u.setAssessment_wound_id(each2.getId());
                            bd.saveUndermining(u);
                        }
                        each2.setLastmodified_on(new Date());
 bd.saveAssessment(each2);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.SINUS_TRACTS_SECTION))) {
                        AssessmentEachwoundVO each2 = (AssessmentEachwoundVO) eachVO;
                        fixes.setPatient_id(each2.getPatient_id());
                        fixes.setRow_id(each2.getId());
                        fixes.setField(component.getId() + "");
                        Collection<AssessmentSinustractsVO> sinus = each2.getSinustracts();
                        String location = Common.parseMultipleTracts(sinus, false);//location
                        String depth = Common.parseMultipleTracts(sinus, true);//depth
                        if (location.equals("")) {
                            location = "...";
                        }
                        fixes.setError(location);
                        fixes.setDelete_reason("");
                        dao.insertNursingFixes(fixes);
                        if (depth.equals("")) {
                            depth = "...";
                        }
                        fixes.setId(null);
                        fixes.setField(component.getId() + "");
                        fixes.setError(depth);
                        fixes.setDelete_reason("");
                        dao.insertNursingFixes(fixes);
                        AssessmentSinustractsVO t = new AssessmentSinustractsVO();
                        t.setAssessment_wound_id(each2.getId());
                        t.setAlpha_id(each2.getAlpha_id());
                        bd.removeSinustract(t);

                        List<AssessmentSinustractsVO> sinusList = getSinus(request);
                        for (AssessmentSinustractsVO u : sinusList) {
                            u.setAlpha_id(each2.getAlpha_id());
                            u.setAssessment_wound_id(each2.getId());
                            bd.saveSinustract(u);
                        }
                        each2.setLastmodified_on(new Date());
 bd.saveAssessment(each2);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(new Integer(Constants.WOUNDBED_SECTION))) {
                        AssessmentEachwoundVO each2 = (AssessmentEachwoundVO) eachVO;
                        fixes.setPatient_id(each2.getPatient_id());
                        fixes.setRow_id(each2.getId());
                        fixes.setField(component.getId() + "");
                        Collection<AssessmentWoundBedVO> wb = each2.getWoundbed();
                        String location = Common.parseWoundBed(wb, language);

                        if (location.equals("")) {
                            location = "...";
                        }
                        fixes.setError(location);
                        fixes.setDelete_reason("");
                        dao.insertNursingFixes(fixes);
                        AssessmentWoundBedVO t = new AssessmentWoundBedVO();
                        t.setAssessment_wound_id(each2.getId());
                        t.setAlpha_id(each2.getAlpha_id());
                        bd.removeWoundBed(t);

                        List<AssessmentWoundBedVO> wbList = getWoundBed(request, locale);
                        for (AssessmentWoundBedVO u : wbList) {
                            u.setAlpha_id(each2.getAlpha_id());
                            u.setAssessment_wound_id(each2.getId());
                            bd.saveWoundBed(u);
                        }
                        pushScale(each2.getAssessment_id(), language);
                        each2.setLastmodified_on(new Date());
 bd.saveAssessment(each2);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.INVESTIGATION_SECTION)) {
                        DiagnosticInvestigationVO tt = (DiagnosticInvestigationVO) eachVO;
                        fixes.setPatient_id(tt.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");
                        Collection<InvestigationsVO> et = tt.getInvestigations();
                        List<String> it = new ArrayList();
                        for (InvestigationsVO ppa : et) {
                            //remove old items.
                            it.add(ppa.getLookup().getName(language) + ": " + ppa.getDate());
                            pbd.removeInvestigation(ppa);

                        }

                        SimpleDateFormat sf = new SimpleDateFormat("dd/MMMMM/yyyy");
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        for (String item : t) {

                            int index = item.indexOf(" : ");
                            String id2 = item.substring(0, index);
                            try {
                                String date = item.substring(index + 3, item.length());
                                Date d = sf.parse(date);
                                InvestigationsVO i = new InvestigationsVO();
                                i.setInvestigation_date(d);
                                i.setDiagnostic_id(tt.getId());
                                i.setLookup_id(new Integer(id2));
                                pbd.saveInvestigation(i);
                            } catch (ParseException e) {
                            }
                        }

                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        //fv.setField(field.get(0));
                        dao.insertNursingFixes(fv);
                        tt.setLastmodified_on(new Date());
                        pbd.saveDiagnosticInvestigation(tt);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION)) {
                        AssessmentTreatmentVO tt = (AssessmentTreatmentVO) eachVO;
                        WoundAssessmentVO wtmp = new WoundAssessmentVO();
                        //System.out.println("Saving Treatment ARray");
                        wtmp.setId(tt.getAssessment_id());
                        WoundAssessmentVO w = wabd.getAssessment(wtmp);
                        fixes.setPatient_id(w.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");
                        Collection<ArrayValueObject> et = tt.getAssessment_treatment_arrays();
                        List<String> it = new ArrayList();
                        for (ArrayValueObject ppa : et) {
                            if (ppa.getLookup().getResourceId().equals(component.getResource_id())) {
                                //remove old items.
                                it.add(ppa.getLookup().getName(language) + (ppa.getLookup().getOther() == 1 ? ";" + ppa.getOther() : ""));
                                AssessmentTreatmentArraysVO atrr = (AssessmentTreatmentArraysVO) ppa;
                                tc.removeTreatmentArray(atrr);
                            }
                        }
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        if(t == null){
                            log.error("NullPointer Possibility: "+component.getField_name());
                        }else{
                            for (String item : t) {
                                if (item != null && !item.equals("")) {
                                    try {
                                        AssessmentTreatmentArraysVO ppanew = new AssessmentTreatmentArraysVO();
                                        ppanew.setAssessment_treatment_id(tt.getId());
                                        ppanew.setLookup_id(new Integer(item));
                                        ppanew.setAlpha_id(tt.getAlpha_id());
                                        ppanew.setResource_id(component.getResource_id());
                                        String other = "";

                                        if (request.getParameter("other" + item) != null) {
                                            other = (String) request.getParameter("other" + item);
                                        }
                                        ppanew.setOther(other);
                                        tc.saveTreatmentArray(ppanew);

                                    } catch (NumberFormatException e) {
                                    }
                                }
                            }
                        }
                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        fixes.setField(component.getId() + "");
                        dao.insertNursingFixes(fv);
                        w.setLastmodified_on(new Date());
                        wabd.saveAssessment(w);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_ANTIBIOTICS)) {
                        AssessmentTreatmentVO tt = (AssessmentTreatmentVO) eachVO;
                        WoundAssessmentVO wtmp = new WoundAssessmentVO();
                        //System.out.println("Saving Treatment ARray");
                        wtmp.setId(tt.getAssessment_id());
                        WoundAssessmentVO w = wabd.getAssessment(wtmp);
                        fixes.setPatient_id(w.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");
                        Collection<AssessmentAntibioticVO> et = tt.getAntibiotics();
                        List<String> it = new ArrayList();
                        for (AssessmentAntibioticVO ppa : et) {
                            it.add(ppa.getPrintable(locale));
                            bd.removeAntibiotic(ppa);
                        }
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        
                        if (t != null && tt != null) {
                           
                            for (String antibiotic : t) {
                                try {
                                    int index = antibiotic.indexOf(" : ");
                                    String name = antibiotic.substring(0, index);
                                    String tmp = antibiotic.substring(index + 3, antibiotic.length());

                                    int index3 = tmp.indexOf(" to ");
                                    String date1 = tmp.substring(0, index3);
                                    String date2 = "";
                                    try {

                                        date2 = tmp.substring(index3 + 4, tmp.length());
                                    } catch (StringIndexOutOfBoundsException e) {
                                    }
                                    SimpleDateFormat sf = new SimpleDateFormat("d/M/yyyy");
                                    Date start_date = sf.parse(date1);
                                    Date end_date = null;
                                    if (date2 != null && !date2.equals("") && !date2.equals("//")) {
                                        end_date = sf.parse(date2);
                                    }
                                    if (start_date != null && !name.equals("") && !name.equals("")) {
                                        AssessmentAntibioticVO a = new AssessmentAntibioticVO();
                                        a.setAssessment_treatment_id(tt.getId());
                                        a.setStart_date(start_date);
                                        if (end_date != null) {//may not have end_date
                                            a.setEnd_date(end_date);
                                        }
                                        a.setAntibiotic(name);
                                        bd.saveAntibiotic(a);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        fixes.setField(component.getId() + "");
                        dao.insertNursingFixes(fv);
                        w.setLastmodified_on(new Date());
                        wabd.saveAssessment(w);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.PP_ARRAY_SECTION)) {
                        PatientProfileVO tt = (PatientProfileVO) eachVO;
                        fixes.setPatient_id(tt.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");

                        Collection<PatientProfileArraysVO> et = tt.getPatient_profile_arrays();
                        List<String> it = new ArrayList();
                        for (PatientProfileArraysVO ppa : et) {

                            if (ppa.getLookup().getResourceId().equals(component.getResource_id())) {
                                //remove old items.
                                if(ppa!=null && ppa.getLookup()!=null){
                                    it.add(ppa.getLookup().getName(language) + (ppa.getLookup().getOther() == 1 ? ";" + ppa.getOther() : ""));

                                    pbd.removePatientArray(ppa);
                                }
                            } else {
                                for (int i : LookupVO.COMORBIDITIES) {
                                    if (i == ppa.getLookup().getResourceId() && component.getField_name().equals("co_morbidities")) {
                                        //remove old items.
                                        it.add(ppa.getLookup().getName(language) + (ppa.getLookup().getOther() == 1 ? ";" + ppa.getOther() : ""));
                                        pbd.removePatientArray(ppa);
                                    }
                                }
                            }
                        }
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        if(component.getField_name().equals("co_morbidities")){
                            t = (String[])request.getParameterValues("current_co_morbidities");
                        }
                        if(t!=null){
                            for (String item : t) {
                                if (item != null && !item.equals("")) {
                                    try {
                                        PatientProfileArraysVO ppanew = new PatientProfileArraysVO();
                                        ppanew.setPatient_profile_id(tt.getId());
                                        ppanew.setLookup_id(new Integer(item));
                                        String other = "";

                                        if (request.getParameter("other" + item) != null) {
                                            other = (String) request.getParameter("other" + item);
                                        }
                                        ppanew.setOther(other);
                                        pbd.savePatientProfileArray(ppanew);

                                    } catch (NumberFormatException e) {
                                    }
                                }
                            }
                        }
                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        fixes.setField(field.get(0));
                        dao.insertNursingFixes(fv);
                        
                        tt.setLastmodified_on(new Date());
                        pbd.savePatientProfile(tt);
                    } else if(component.getTable_id()!=null && component.getTable_id().equals(Constants.EXTERNAL_REFERRALS_SECTION)){
                        PatientProfileVO tt = (PatientProfileVO) eachVO;
                        ListServiceImpl lservice = new ListServiceImpl(language);
                        fixes.setPatient_id(tt.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");

                        Collection<ExternalReferralVO> et = tt.getExternalReferrals();
                        List<String> it = new ArrayList();
                        for (ExternalReferralVO ppa : et) {
                            LookupVO ex = lservice.getListItem(ppa.getExternal_referral_id());
                            LookupVO arr = lservice.getListItem(ppa.getArranged_id());
                            if(ex!=null && arr!=null ){
                                it.add(ex.getName(language)+" : "+arr.getName(language));
                                pbd.removeExternalReferral(ppa);
                            }
                            
                            
                        }
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        if(t!=null){
                        for (String item : t) {
                            String[] split = item.split(" : ");
                            String one = "";
                            String two = "";
                            if(split.length == 2){
                                one = split[0];
                                two = split[1];
                            }

                            if(one!=null && two!=null && !one.equals("") && !two.equals("")){
                                try{
                                ExternalReferralVO ext = new ExternalReferralVO(tt.getId(),Integer.parseInt(one),Integer.parseInt(two));
                                pbd.saveExternalReferral(ext);
                                }catch(NumberFormatException e){e.printStackTrace();}
                            }
                            
                        }
                        }
                       
                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        fixes.setField(field.get(0));
                        dao.insertNursingFixes(fv);
                        tt.setLastmodified_on(new Date());
                        pbd.savePatientProfile(tt);
                    }else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ETIOLOGY_SECTION)) {
                        WoundProfileVO tt = (WoundProfileVO) eachVO;
                        fixes.setPatient_id(tt.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");
                        Collection<EtiologyVO> et = tt.getEtiologies();
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        List<String> it = new ArrayList();
                        for (EtiologyVO e : et) {
                            WoundAssessmentLocationVO alpha = e.getAlpha();
                            Vector<String> list_ids = Common.parseSelectByAlpha(alpha.getAlpha(), t,locale);
                            it.add(e.getPrintable(language));
                            wbd.removeEtiology(e);
                            if (list_ids != null && list_ids.size() > 0) {

                                for (String list_id : list_ids) {
                                    //System.out.println("TEst: "+list_id);
                                    if (list_id != null && !list_id.equals("")) {
                                        e.setLookup_id(new Integer(list_id.trim()));
                                        e.setId(null);
                                        wbd.saveEtiology(e);

                                    }
                                }
                            }
                        }
                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        fixes.setField(component.getId() + "");
                        dao.insertNursingFixes(fv);
                        tt.setLastmodified_on(new Date());
                        wbd.saveWoundProfile(tt);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ACQUIRED_SECTION)) {
                        WoundProfileVO tt = (WoundProfileVO) eachVO;
                        fixes.setPatient_id(tt.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");
                        Collection<WoundAcquiredVO> et = tt.getWound_acquired();
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        List<String> it = new ArrayList();
                        for (WoundAcquiredVO e : et) {
                            WoundAssessmentLocationVO alpha = e.getAlpha();
                            Vector<String> list_ids = Common.parseSelectByAlpha(alpha.getAlpha(), t,locale);
                            it.add(e.getPrintable(language));
                            wbd.removeAcquired(e);
                            if (list_ids != null && list_ids.size() > 0) {

                                for (String list_id : list_ids) {
                                    //System.out.println("TEst: "+list_id);
                                    if (list_id != null && !list_id.equals("")) {
                                        e.setLookup_id(new Integer(list_id.trim()));
                                        e.setId(null);
                                        wbd.saveAcquired(e);

                                    }
                                }
                            }
                        }
                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        fixes.setField(component.getId() + "");
                        dao.insertNursingFixes(fv);
                        tt.setLastmodified_on(new Date());
                        wbd.saveWoundProfile(tt);

                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.GOALS_SECTION)) {
                        WoundProfileVO tt = (WoundProfileVO) eachVO;
                        fixes.setPatient_id(tt.getPatient_id());
                        fixes.setRow_id(tt.getId());
                        fixes.setField(component.getId() + "");
                        Collection<GoalsVO> et = tt.getGoals();
                        String[] t = (String[]) request.getParameterValues(component.getField_name());
                        List<String> it = new ArrayList();
                        for (GoalsVO e : et) {
                            WoundAssessmentLocationVO alpha = e.getAlpha();
                            Vector<String> list_ids = Common.parseSelectByAlpha(alpha.getAlpha(), t,locale);
                            it.add(e.getPrintable(language));
                            wbd.removeGoal(e);
                            if (list_ids != null && list_ids.size() > 0) {

                                for (String list_id : list_ids) {
                                    //System.out.println("TEst: "+list_id);
                                    if (list_id != null && !list_id.equals("")) {
                                        e.setLookup_id(new Integer(list_id.trim()));
                                        e.setId(null);
                                        wbd.saveGoal(e);

                                    }
                                }
                            }
                        }

                        String items = Serialize.serialize(it, ", ");

                        NursingFixesVO fv = fixes;
                        if (items.equals("")) {
                            items = "...";
                        }
                        fv.setDelete_reason("");
                        fv.setError(items);
                        fixes.setField(component.getId() + "");
                        dao.insertNursingFixes(fv);
                        tt.setLastmodified_on(new Date());
                        wbd.saveWoundProfile(tt);


                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {

                        WoundAssessmentVO t = (WoundAssessmentVO) eachVO;
                        //cycle through fields to update object
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (WoundAssessmentVO) Common.setFieldValue(component, t, vname);

                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setDelete_reason("");
                            fv.setError(v);
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());

                        wabd.saveAssessment(t);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        TreatmentCommentVO t = null;

                        t = (TreatmentCommentVO) eachVO;

                        //cycle through fields to update object
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            //System.out.println(fname + " Treatment Comment - Edit Save " + vname);
                            t = (TreatmentCommentVO) Common.setFieldValue(component, t, vname);

                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        if (t.getId() != null) {
                            for (String v : printage) {

                                NursingFixesVO fv = fixes;
                                if (v.equals("")) {
                                    v = "...";
                                }
                                fv.setDelete_reason("");
                                fv.setError(v);
                                fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                                dao.insertNursingFixes(fv);

                                cnt++;
                            }
                        }
                        tc.saveTreatmentComment(t);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT)) {
                        AssessmentTreatmentVO t = (AssessmentTreatmentVO) eachVO;
                        //cycle through fields to update object
                        //System.out.println("saving assessment_treatment"+component.getField_name());
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            //System.out.println("Saving:???"+vname);
                            t = (AssessmentTreatmentVO) Common.setFieldValue(component, t, vname);
                        }
                        WoundAssessmentVO wtmp = new WoundAssessmentVO();
                        wtmp.setId(t.getAssessment_id());
                        WoundAssessmentVO w = wabd.getAssessment(wtmp);
                        fixes.setPatient_id(w.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        w.setLastmodified_on(new Date());
                        wabd.saveAssessment(w);

                        tc.saveAssessmentTreatment(t);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        AssessmentNPWTVO t = (AssessmentNPWTVO) eachVO;

                        //cycle through fields to update object
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (AssessmentNPWTVO) Common.setFieldValue(component, t, vname);
                        }
                        WoundAssessmentVO wtmp = new WoundAssessmentVO();
                        wtmp.setId(t.getAssessment_id());
                        WoundAssessmentVO w = wabd.getAssessment(wtmp);


                        nc.saveNegativePressure(t);
                        AssessmentNPWTVO retrieve = nc.getNegativePressure(t);
                        if (retrieve != null) {
                            for (AssessmentNPWTAlphaVO alpha : t.getAlphas()) {
                                AssessmentNPWTAlphaVO talpha = new AssessmentNPWTAlphaVO();
                                talpha.setAlpha_id(alpha.getAlpha_id());
                                talpha.setAssessment_npwt_id(retrieve.getId());
                                nc.saveNegativePressureAlpha(talpha);
                            }
                            fixes.setPatient_id(w.getPatient_id());
                            fixes.setRow_id(retrieve.getId());
                            patient_id = fixes.getPatient_id() + "";
                            int cnt = 0;
                            for (String v : printage) {
                                NursingFixesVO fv = fixes;
                                if (v.equals("")) {
                                    v = "...";
                                }
                                fv.setError(v);
                                fv.setDelete_reason("");
                                fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));

                                dao.insertNursingFixes(fv);

                                cnt++;
                            }
                        }
                        w.setLastmodified_on(new Date());
                        wabd.saveAssessment(w);
                    } else if (component != null && component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) {
                        AssessmentEachwoundVO t = (AssessmentEachwoundVO) eachVO;
                        //cycle through fields to update object
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (AssessmentEachwoundVO) Common.setFieldValue(component, t, vname);
                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        if (component.getField_name().equals("status") && Integer.toString(t.getStatus()).equals(Common.getConfig("woundActive"))) {
                            t.setClosed_date(null);
                            t.setDischarge_other(null);
                            t.setDischarge_reason(0);
                            lbd.reopenAlpha(t.getAlpha_id());
                        } else if (component.getField_name().equals("closed_date")) {
                            WoundAssessmentLocationVO alphaT = new WoundAssessmentLocationVO();
                            alphaT.setId(t.getAlpha_id());
                            WoundAssessmentLocationVO updateAlpha = lbd.getAlpha(alphaT);
                            updateAlpha.setClose_timestamp(new Date(Long.parseLong((String)fields.get("closed_date"))));
                            lbd.saveAlpha(updateAlpha);

                        }
                        t.setLastmodified_on(new Date());
                        bd.saveAssessment(t);
                        
                        pushScale(t.getAssessment_id(), language);
                    } else if (component.getFlowchart().equals(Constants.INCISION_ASSESSMENT)) {
                        AssessmentIncisionVO t = (AssessmentIncisionVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (AssessmentIncisionVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        if (component.getField_name().equals("status") && Integer.toString(t.getStatus()).equals(Common.getConfig("incisionActive"))) {
                            t.setClosed_date(null);
                            t.setDischarge_other(null);
                            t.setDischarge_reason(0);
                            lbd.reopenAlpha(t.getAlpha_id());
                        } else if (component.getField_name().equals("closed_date")) {
                            WoundAssessmentLocationVO alphaT = new WoundAssessmentLocationVO();
                            alphaT.setId(t.getAlpha_id());
                            WoundAssessmentLocationVO updateAlpha = lbd.getAlpha(alphaT);
                            updateAlpha.setClose_timestamp(new Date(Long.parseLong((String)fields.get("closed_date"))));
                            lbd.saveAlpha(updateAlpha);

                        }
                        t.setLastmodified_on(new Date());
                        bd.saveAssessment(t);
                        
                    } else if (component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT)) {
                        AssessmentDrainVO t = (AssessmentDrainVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (AssessmentDrainVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        if (component.getField_name().equals("status") && Integer.toString(t.getStatus()).equals(Common.getConfig("drainActive"))) {
                            t.setClosed_date(null);
                            t.setDischarge_other(null);
                            t.setDischarge_reason(0);
                            t.setDrain_removed_intact(0);
                            lbd.reopenAlpha(t.getAlpha_id());
                        } else if (component.getField_name().equals("closed_date")) {
                            WoundAssessmentLocationVO alphaT = new WoundAssessmentLocationVO();
                            alphaT.setId(t.getAlpha_id());
                            WoundAssessmentLocationVO updateAlpha = lbd.getAlpha(alphaT);
                            updateAlpha.setClose_timestamp(new Date(Long.parseLong((String)fields.get("closed_date"))));
                            lbd.saveAlpha(updateAlpha);

                        }
                        t.setLastmodified_on(new Date());
                        bd.saveAssessment(t);
                    } else if (component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT)) {
                        AssessmentOstomyVO t = (AssessmentOstomyVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (AssessmentOstomyVO) Common.setFieldValue(component, t, vname);

                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        if (component.getField_name().equals("status") && Integer.toString(t.getStatus()).equals(Common.getConfig("ostomyActive"))) {
                            t.setClosed_date(null);
                            t.setDischarge_other(null);
                            t.setDischarge_reason(0);
                            lbd.reopenAlpha(t.getAlpha_id());
                        } else if (component.getField_name().equals("closed_date")) {
                            WoundAssessmentLocationVO alphaT = new WoundAssessmentLocationVO();
                            alphaT.setId(t.getAlpha_id());
                            WoundAssessmentLocationVO updateAlpha = lbd.getAlpha(alphaT);
                            updateAlpha.setClose_timestamp(new Date(Long.parseLong((String)fields.get("closed_date"))));
                            lbd.saveAlpha(updateAlpha);

                        }
                        t.setLastmodified_on(new Date());
                        bd.saveAssessment(t);
                    } else if (component.getFlowchart().equals(Constants.SKIN_ASSESSMENT)) {
                        AssessmentSkinVO t = (AssessmentSkinVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (AssessmentSkinVO) Common.setFieldValue(component, t, vname);

                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        if (component.getField_name().equals("status") && Integer.toString(t.getStatus()).equals(Common.getConfig("skinActive"))) {
                            t.setClosed_date(null);
                            t.setDischarge_other(null);
                            t.setDischarge_reason(0);
                            lbd.reopenAlpha(t.getAlpha_id());
                        } else if (component.getField_name().equals("closed_date")) {
                            WoundAssessmentLocationVO alphaT = new WoundAssessmentLocationVO();
                            alphaT.setId(t.getAlpha_id());
                            WoundAssessmentLocationVO updateAlpha = lbd.getAlpha(alphaT);
                            updateAlpha.setClose_timestamp(new Date(Long.parseLong((String)fields.get("closed_date"))));
                            lbd.saveAlpha(updateAlpha);

                        }
                        t.setLastmodified_on(new Date());

                        bd.saveAssessment(t);
                    } else if (component.getFlowchart().equals(Constants.BURN_ASSESSMENT)) {
                        AssessmentBurnVO t = (AssessmentBurnVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (AssessmentBurnVO) Common.setFieldValue(component, t, vname);

                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        if (component.getField_name().equals("status") && Integer.toString(t.getStatus()).equals(Common.getConfig("burnActive"))) {
                            t.setClosed_date(null);
                            t.setDischarge_other(null);
                            t.setDischarge_reason(0);
                            lbd.reopenAlpha(t.getAlpha_id());
                        } else if (component.getField_name().equals("closed_date")) {
                            WoundAssessmentLocationVO alphaT = new WoundAssessmentLocationVO();
                            alphaT.setId(t.getAlpha_id());
                            WoundAssessmentLocationVO updateAlpha = lbd.getAlpha(alphaT);
                            updateAlpha.setClose_timestamp(new Date(Long.parseLong((String)fields.get("closed_date"))));
                            lbd.saveAlpha(updateAlpha);

                        }
                        t.setLastmodified_on(new Date());

                        bd.saveAssessment(t);
                    } else if (component.getFlowchart().equals(Constants.BRADEN_SCORE_SECTION)) {
                        BradenVO t = (BradenVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (BradenVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }

                        t.setLastmodified_on(new Date());
                        pbd.saveBraden(t);

                    } else if (component.getFlowchart().equals(Constants.PATIENT_PROFILE)) {
                        PatientProfileVO t = (PatientProfileVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (PatientProfileVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        pbd.savePatientProfile(t);

                    }else if (component.getFlowchart().equals(Constants.DIAGNOSTIC_INVESTIGATION)) {
                        DiagnosticInvestigationVO t = (DiagnosticInvestigationVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (DiagnosticInvestigationVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        pbd.saveDiagnosticInvestigation(t);

                    } else if (component.getFlowchart().equals(Constants.PHYSICAL_EXAM)) {
                        PhysicalExamVO t = (PhysicalExamVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (PhysicalExamVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        pbd.savePhysicalExam(t);

                    } else if (component.getFlowchart().equals(Constants.FOOT_ASSESSMENT)) {
                        FootAssessmentVO t = (FootAssessmentVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (FootAssessmentVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        pbd.saveFoot(t);
                        
                        
                    } else if (component.getFlowchart().equals(Constants.SENSORY_ASSESSMENT)) {
                        SensoryAssessmentVO t = (SensoryAssessmentVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (SensoryAssessmentVO) Common.setFieldValue(component, t, vname);

                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        pbd.saveSensory(t);


                    } else if (component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_ADVANCED)) {
                        LimbAdvAssessmentVO t = (LimbAdvAssessmentVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (LimbAdvAssessmentVO) Common.setFieldValue(component, t, vname);
                            if (component.getField_name().equals("left_sensation")) {
                                t.setLeft_score_sensation(array_size + "");
                            } else if (component.getField_name().equals("right_sensation")) {
                                t.setRight_score_sensation(array_size + "");
                            }
                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        pbd.saveAdvLimb(t);

                    } else if (component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_BASIC)) {
                        LimbBasicAssessmentVO t = (LimbBasicAssessmentVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (LimbBasicAssessmentVO) Common.setFieldValue(component, t, vname);

                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        pbd.saveBasicLimb(t);

                    } else if (component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_UPPER)) {
                        LimbUpperAssessmentVO t = (LimbUpperAssessmentVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            String vname = (String) fields.get(fname);
                            component.setField_name(fname);
                            t = (LimbUpperAssessmentVO) Common.setFieldValue(component, t, vname);

                        }

                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));
                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        pbd.saveUpperLimb(t);

                    } else if (component.getFlowchart().equals(Constants.WOUND_PROFILE) || component.getFlowchart().equals(Constants.OSTOMY_PROFILE) || component.getFlowchart().equals(Constants.POSTOP_PROFILE) || component.getFlowchart().equals(Constants.TUBES_DRAINS_PROFILE)) {


                        WoundProfileVO t = (WoundProfileVO) eachVO;
                        for (Enumeration e = fields.keys(); e.hasMoreElements();) {
                            String fname = (String) e.nextElement();
                            
                                String vname = (String) fields.get(fname);
                                component.setField_name(fname);
                                t = (WoundProfileVO) Common.setFieldValue(component, t, vname);
                            
                        }
                        fixes.setPatient_id(t.getPatient_id());
                        fixes.setRow_id(t.getId());
                        patient_id = fixes.getPatient_id() + "";
                        int cnt = 0;
                        for (String v : printage) {
                            NursingFixesVO fv = fixes;
                            if (v.equals("")) {
                                v = "...";
                            }
                            fv.setError(v);
                            fv.setDelete_reason("");
                            fv.setField(component.getId()+"");//fixes.setField(field.get(cnt));

                            dao.insertNursingFixes(fv);
                            cnt++;
                        }
                        t.setLastmodified_on(new Date());
                        wbd.saveWoundProfile(t);
                    }
                } else {
                    ///ERROR.. let user know!
                }
                //update timestamp for patient.. used to generate report for PCI ( or authority's document library)
                PatientReportService prservice = new PatientReportService();
                if (patient_id != null && !patient_id.equals("") && !patient_id.equals("0")) {
                    PatientReportVO savedPatient = prservice.getPatientReportUpdate(Integer.parseInt(patient_id));
                    if (savedPatient != null) {
                        savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                        savedPatient.setActive(1);
                        prservice.savePatientReportUpdate(savedPatient);
                    } else {
                        savedPatient = new PatientReportVO();
                        savedPatient.setPatient_id(new Integer(patient_id));
                        savedPatient.setTime_stamp(pdate.getEpochTime() + "");
                        savedPatient.setActive(1);
                        prservice.savePatientReportUpdate(savedPatient);
                    }
                }
                InformationPopupVO info = new InformationPopupVO();
                info.setTitle(title);
                info.setDescription("Successfully updated patient record.");
                request.setAttribute("info", info);
                request.setAttribute("type", "saved");
                request.setAttribute("field", "success");

            }
        } catch (DataAccessException e) {
            e.printStackTrace();
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            e.printStackTrace();
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        return (mapping.findForward(
                "uploader.assesserror.success"));

    }

    private AssessmentProductVO[] getProducts(String[] products, AbstractAssessmentVO updateAssessmentVO) {


        //Gather products
        List list = new LinkedList();

        int cnt = 0;
        for (String product : products) {
            StringTokenizer st = new StringTokenizer(product, "|");
            String quantity = st.nextToken();
            String alpha = st.nextToken();
            String prod_id = st.nextToken();
            if (alpha.equals(updateAssessmentVO.getAlpha_id() + "")) {
                AssessmentProductVO prod = new AssessmentProductVO();
                prod.setPatient_id(updateAssessmentVO.getPatient_id());
                prod.setWound_id(updateAssessmentVO.getWound_id());
                prod.setAssessment_id(updateAssessmentVO.getAssessment_id());
                prod.setOstomy_assessment_id(updateAssessmentVO.getId());
                prod.setAlpha_id(updateAssessmentVO.getAlpha_id());
                prod.setProduct_id(prod_id.indexOf("Other;") == -1 ? new Integer(prod_id) : new Integer(-1));
                prod.setQuantity(quantity);
                prod.setOther(prod_id.indexOf("Other;") != -1 ? prod_id : "");
                list.add(prod);
            }


        }
        //convert back to an array to be stored in the assessmnt object
        AssessmentProductVO[] prods = (AssessmentProductVO[]) list.toArray(new AssessmentProductVO[0]);
        return prods;

    }
    /*
     * Check if an assessment_eachwound serialized field contains a specified item
     *
     * @return boolean true if it exists false if it doesnt
     * @param serial is the serialized field in assessment_eachwound
     * @param item is the needle in the haystack we need to find.
     *
     * @since 3.2
     */

    public boolean ifExists(String serial, String item) {
        if (serial.indexOf(item) == -1) {
            return false;
        } else {
            return true;
        }
    }

    public List getSinus(HttpServletRequest request) {

        List v = new ArrayList();
        try {
            String num_sinus_tracts_str = (String) request.getParameter("num_sinus_location");
            int num_sinus_tracts = Integer.parseInt(num_sinus_tracts_str);
            for (int x = 0; x < num_sinus_tracts; x++) {
                int cnt = x + 1;
                AssessmentSinustractsVO u = new AssessmentSinustractsVO();
                String dcm = (String) request.getParameter("sinus_location_depth_cm_" + cnt);
                String dmm = (String) request.getParameter("sinus_location_depth_mm_" + cnt);
                String dstart = (String) request.getParameter("sinus_location_start_" + cnt);

                u.setDepth_cm(new Integer(dcm));
                u.setDepth_mm(new Integer(dmm));
                u.setLocation_start(new Integer(dstart));
                v.add(u);
            }
        } catch (Exception e) {
            return new ArrayList();
        }
        return v;

    }

    public List getWoundBed(HttpServletRequest request, String locale) {
        List v = new ArrayList();
        try {
            //String woundbase_percentage = (String) request.getParameter("woundbase_percentage");
            String[] woundbase = (String[]) request.getParameterValues("woundbase");
            /*if (woundbase != null) {
             //Vector wndbase_percentage = Serialize.unserialize(woundbase_percentage, ",");

                
             for (int x = 0; x < woundbase.length; x++) {
             System.out.println("perc: "+request.getParameter("woundbase_percentage_"+x));
             if(request.getParameter("woundbase_percentage_"+x)!=null){
             AssessmentWoundBedVO w = new AssessmentWoundBedVO();
             w.setPercentage(new Integer((String)request.getParameter("woundbase_percentage_"+x)));
             w.setWound_bed(woundbase[x]);
             v.add(w);
             }
             }
                

             }*/
            String woundbase_percentage = null;
            if (request.getParameter("woundbase_percentage") != null) {
                woundbase_percentage = (String) request.getParameter("woundbase_percentage");
            }
            if (woundbase_percentage != null) {
                Vector wndbase_percentage = Serialize.unserialize(woundbase_percentage, ",");
                if (woundbase != null && !woundbase_percentage.equals("")) {
                    for (int x = 0; x < woundbase.length; x++) {
                        AssessmentWoundBedVO w = new AssessmentWoundBedVO();
                        if (wndbase_percentage == null || wndbase_percentage.get(x).equals("")) {
                            w.setPercentage(new Integer("0"));
                        } else {
                            w.setPercentage(new Integer(wndbase_percentage.get(x) + ""));
                        }
                        w.setWound_bed(Integer.parseInt(woundbase[x]));
                        v.add(w);
                    }
                }

            }
            return v;
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            return new ArrayList();//this is not valid likely because of undermining because selected (no validation)
        } catch (Exception e) {
            return new ArrayList();
        }
    }

    public int pushScale(int assessment_id, int language) {
        int score = 0;
        if (Common.getConfig("pushScale").equals("1")) {
            AssessmentServiceImpl aservice = new AssessmentServiceImpl();
            ListServiceImpl lservice = new ListServiceImpl(language);
            AssessmentEachwoundVO a = new AssessmentEachwoundVO();
            a.setAssessment_id(assessment_id);
            a.setActive(1);
            try {
                Collection<AbstractAssessmentVO> assess = aservice.getAllAssessments(a, -1, true);
                for (AbstractAssessmentVO as : assess) {
                    if (as instanceof AssessmentEachwoundVO) {
                        AssessmentEachwoundVO assessment = (AssessmentEachwoundVO) as;
                        double area = assessment.getLength() / assessment.getWidth();
                        if (area < 0.3) {
                            score = score + 1;
                        } else if (area >= 0.3 && area <= 0.6) {
                            score = score + 2;
                        } else if (area >= 0.7 && area <= 1.0) {
                            score = score + 3;
                        } else if (area >= 1.1 && area <= 2.0) {
                            score = score + 4;
                        } else if (area >= 2.1 && area <= 3.0) {
                            score = score + 5;
                        } else if (area >= 3.1 && area <= 4.0) {
                            score = score + 6;
                        } else if (area >= 4.1 && area <= 8.0) {
                            score = score + 7;
                        } else if (area >= 8.1 && area <= 12.0) {
                            score = score + 8;
                        } else if (area >= 12.1 && area <= 24.0) {
                            score = score + 9;
                        } else if (area > 24.0) {
                            score = score + 10;
                        }

                        LookupVO ea = lservice.getListItem(assessment.getExudate_amount());

                        Collection<AssessmentWoundBedVO> wb = assessment.getWoundbed();
                        int wb_score = 0;
                        for (AssessmentWoundBedVO w : wb) {
                            /* if(w.)
                             #foreach($ea in $wound_base_percentage)
                             if($ea.id == value && $ea.report_value> bed_score){
                             bed_score = $ea.report_value;
                             score = score + bed_score;
                             }
                             #end*/
                        }
                    }
                }
            } catch (ApplicationException e) {
            }

        }
        return score;
    }

    public List getUndermining(HttpServletRequest request) {
        List v = new ArrayList();
        try {
            String num_undermining_str = (String) request.getParameter("num_undermining_location");
            int num_undermining = Integer.parseInt(num_undermining_str);
            for (int x = 0; x < num_undermining; x++) {
                int cnt = x + 1;
                AssessmentUnderminingVO u = new AssessmentUnderminingVO();
                String dcm = (String) request.getParameter("undermining_location_depth_cm_" + cnt);
                String dmm = (String) request.getParameter("undermining_location_depth_mm_" + cnt);
                String dstart = (String) request.getParameter("undermining_location_start_" + cnt);
                String dend = (String) request.getParameter("undermining_location_end_" + cnt);
                u.setDepth_cm(new Integer(dcm));
                u.setDepth_mm(new Integer(dmm));
                u.setLocation_start(new Integer(dstart));
                u.setLocation_end(new Integer(dend));
                v.add(u);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    public List getSeperation(HttpServletRequest request) {
        List v = new ArrayList();
        try {
            String num_undermining_str = (String) request.getParameter("num_mucocutaneous_margin_location");
            int num_undermining = Integer.parseInt(num_undermining_str);
            for (int x = 0; x < num_undermining; x++) {
                int cnt = x + 1;
                AssessmentMucocSeperationsVO u = new AssessmentMucocSeperationsVO();
                String dcm = (String) request.getParameter("mucocutaneous_margin_location_depth_cm_" + cnt);
                String dmm = (String) request.getParameter("mucocutaneous_margin_location_depth_mm_" + cnt);
                String dstart = (String) request.getParameter("mucocutaneous_margin_location_start_" + cnt);
                String dend = (String) request.getParameter("mucocutaneous_margin_location_end_" + cnt);
                u.setDepth_cm(new Integer(dcm));
                u.setDepth_mm(new Integer(dmm));
                u.setLocation_start(new Integer(dstart));
                u.setLocation_end(new Integer(dend));
                v.add(u);
            }
        } catch (Exception e) {
        }
        return v;
    }
}
