package com.pixalere.struts.login;

import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.service.OpenSAMLService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Common;

public class Logout extends Action {

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

        // Check configuration to start SSO
        if (Common.getConfig("enableSSO") != null && Common.getConfig("enableSSO").equals("1")){
            // Valid and enabled SSO conf
            OpenSAMLService openSAMLService = new OpenSAMLService();
            SSOProviderVO ssopvo = openSAMLService.getSsoConfig();
            
            if (ssopvo != null && ssopvo.getActive().equals(1)){
                //If SSO is operational redirect to SSOLogout 
                return (mapping.findForward("system.logoffsso"));
            }
        } 
        
        /*Removing the ProfessionalVO from the session, the filter should pick
         this up and lookup the default record.*/
        HttpSession session = request.getSession();
        session.removeAttribute("referral");
        session.removeAttribute("config");
        session.removeAttribute("userVO");
        Common.invalidatePatientSession(session);
        session.invalidate();
        if (request.getAttribute("session_invalid") != null) {
            request.setAttribute("session_invalid", request.getAttribute("session_invalid"));
        }

        return (mapping.findForward("logout.success"));
        
    }
}
