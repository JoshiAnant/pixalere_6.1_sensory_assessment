package com.pixalere.struts.login;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.DataAccessException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.patient.dao.PatientProfileDAO;
import com.pixalere.common.bean.OfflineVO;
public class UploadSetupAction extends Action {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UploadSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        HttpSession session=request.getSession();
        
        PatientProfileDAO dao=new PatientProfileDAO();
        try{

            request.setAttribute("offline",dao.findAllOfflineByCriteria(new OfflineVO(),(ProfessionalVO)session.getAttribute("userVO"),false));

        }catch(DataAccessException e){
            
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        
        
        return (mapping.findForward("upload.success"));
        
    }
}
