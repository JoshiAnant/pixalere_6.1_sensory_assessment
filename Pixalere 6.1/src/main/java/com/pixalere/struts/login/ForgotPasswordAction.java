/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.login;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.Action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.Common;
import com.pixalere.utils.MD5;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Sms;
import com.twilio.sdk.resource.list.SmsList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author travis
 */
public class ForgotPasswordAction extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.struts.login.ForgotPasswordAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        ForgotPasswordForm lform = (ForgotPasswordForm) form;

        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        ProfessionalVO ptmp = new ProfessionalVO();
        ptmp.setId(lform.getId());
        try {
            ProfessionalVO retrieved_prof = pservice.getProfessional(ptmp);
            
            if (retrieved_prof != null && retrieved_prof.getReset_password_method().equals(0) && retrieved_prof.getAnswer_one().equals(MD5.hash(lform.getAnswer1())) && retrieved_prof.getAnswer_two().equals(MD5.hash(lform.getAnswer2()))) {
                //forward to change password screen, pass md5 for verification and id.
                request.setAttribute("reset_password_method",0);
                request.setAttribute("id",retrieved_prof.getId());
                request.setAttribute("token",MD5.hash(lform.getAnswer1())+MD5.hash(lform.getAnswer2()));
                return (mapping.findForward("change_pass"));
            } else if (retrieved_prof != null && retrieved_prof.getReset_password_method().equals(1)) {
                //verify token.
                PasswordResetVO reset = new PasswordResetVO();
                reset.setProfessional_id(lform.getId());
                PasswordResetVO r = pservice.getPasswordReset(reset);
                if (r != null && r.getToken().equals(MD5.hash(lform.getToken()))) {
                    request.setAttribute("id",retrieved_prof.getId());
                    request.setAttribute("token",r.getToken());
                    request.setAttribute("reset_password_method",1);
                    ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                
                    return (mapping.findForward("change_pass"));
                    //forward to change password screen, pass md5 for verification and id
                }else{
                    request.setAttribute("error","incorrect_token");
                    return (mapping.findForward("go.login"));
                }
            } else {
                //forward toe rror page (wrong answr or token)
                return (mapping.findForward("go.login"));
            }

        } catch (ApplicationException e) {
            e.printStackTrace();
            //errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));

        }
        return (mapping.findForward("pixalere.login"));
    }
}
