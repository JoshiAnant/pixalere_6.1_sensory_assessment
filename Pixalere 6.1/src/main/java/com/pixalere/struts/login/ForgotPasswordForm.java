/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.login;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.Common;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.MD5;
/**
 * 
 * @author travis
 */
public class ForgotPasswordForm  extends ActionForm {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LoginForm.class);
    private Integer id;
    private String token;
    private String answer1;
    private String answer2;
    private String action;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        Common common = new Common();
        try {
            //return as success regardless of username existing or not.  This way we won't have someone guessing usernames.
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            ProfessionalVO ptmp = new ProfessionalVO();
            ptmp.setId(getId());
            ProfessionalVO retrieved_prof = pservice.getProfessional(ptmp);
            if(retrieved_prof!=null && (retrieved_prof.getPager()==null || retrieved_prof.getPager().equals("")) && (retrieved_prof.getAnswer_one()!=null || retrieved_prof.getAnswer_two()!=null)){
                //throw warning stating their account isn't capable of being reset as it lacks a mobile #
                //errors.add("pass", new ActionError("forgot_password_nocellnum"));
                //return (mapping.findForward("forgot_password_nocellnum"));
            }else if(retrieved_prof == null){
                //redirect to login page, throw no warnings
                //eturn (mapping.findForward("forgot_password_token"));
            }
                //Heart.468
        }catch (ApplicationException e) {
            errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));

        }
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setId(null);
        setAction("");
    }



    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the answer1
     */
    public String getAnswer1() {
        return answer1;
    }

    /**
     * @param answer1 the answer1 to set
     */
    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    /**
     * @return the answer2
     */
    public String getAnswer2() {
        return answer2;
    }

    /**
     * @param answer2 the answer2 to set
     */
    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

}
