package com.pixalere.struts.login;

import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.PasswordHistoryVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.auth.client.ProfessionalClient;
import com.pixalere.auth.service.*;
import com.pixalere.utils.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import java.util.*;

import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.common.service.LicenseServiceImpl;

import com.pixalere.auth.bean.UserAccountRegionsVO;

public class LoginForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LoginForm.class);
    
    private Integer language = -1;
    private String userId = "";
    private String password = "";
    private String act;
    private String license;
    private Integer question_one;
    private Integer question_two;
    private String answer_one;
    private String answer_two;
    private String phone;

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        Common common = new Common();
        try {
            if (act != null && act.equals("LicenseSave")) {
                try {
                    LicenseServiceImpl lservice = new LicenseServiceImpl();
                    System.out.println("License" + license);
                    lservice.saveLicense(license);
                    //return mapping.findForward("login.failure");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String locale = Common.getLanguageLocale(language);
            
            if (request.getParameter("change_password") != null && (Integer.parseInt(request.getParameter("change_password"))) == 1) {
                //check old password and that new passwords match then write info to professionals table
                //set new_user = 0 and forward back to login for relogin using new password
                String old_password = request.getParameter("password");
                String new_password = request.getParameter("newpassword");
                String confirm_password = request.getParameter("confirmpassword");
                HttpSession session = request.getSession();

                ProfessionalVO tmpuserVO = null;
                ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
                ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                if (userBD.passwordHistoryCheck(userVO.getId() + "", confirm_password) == true) {
                    //password already exists
                    errors.add("samepass", new ActionError("pixalere.login.error.samepass"));
                } else {
                    userVO.setPassword((MD5.hash(new_password)).toLowerCase());
                    userVO.setNew_user(new Integer(0));
                    if(getPhone()!= null && !getPhone().equals("")){
                        userVO.setPager(getPhone());
                    }
                    //if phone # exists, use this for password resets
                    if (getPhone() != null && !getPhone().equals("")) {
                        userVO.setReset_password_method(1);
                    } else {
                        userVO.setReset_password_method(0);
                    }
                    if (answer_one != null && !answer_one.equals("")) {
                        userVO.setAnswer_one(MD5.hash(answer_one));
                        userVO.setQuestion_one(question_one);
                    }
                    if (answer_two != null && !answer_two.equals("")) {
                        userVO.setAnswer_two(MD5.hash(answer_two));
                        userVO.setQuestion_two(question_two);
                    }
                    //update professional
                    userBD.saveProfessional(userVO, userVO.getId());//for offline need to send back to server.
                    //save password to prevent user from using it in the future.
                    PasswordHistoryVO historyVO = new PasswordHistoryVO();
                    historyVO.setProfessional_id(userVO.getId());
                    historyVO.setCreated_on(new Date());
                    historyVO.setPassword(MD5.hash(new_password));

                    userBD.insertPasswordForHistory(historyVO);
                    request.setAttribute("newuser_error", "yes");
                }
            } else if (userId.equals("") || password.equals("")) {
                if (Common.getConfig("useUserNames").equals("1")) {
                    errors.add("username", new ActionError("pixalere.login.error.userName"));
                } else {
                    errors.add("username", new ActionError("pixalere.login.error.userNumber"));
                }
                //errors.add("password",new ActionError("pixalere.login.error.password"));
            } else {
                log.info("##$$##$$##-----LOGIN USER-----##$$##$$##");

                ProfessionalVO userVO = null;
                HttpSession session = request.getSession();

                if (Common.isOffline() == true) {
                    session.setAttribute("offline", "1");
                }
                if (Common.isOffline() == true && common.checkConnection() == true) {
                    //call new Service
                    ProfessionalClient pc = new ProfessionalClient();
                    ProfessionalService remoteService = pc.createProfessionalClient();
                    userVO = remoteService.validateUserId(userId, password);
                    
                    //userVO =login.validateLogin(userId+"", password);

                } else {
                    ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                    userVO = userBD.validateUserId(userId, password);
                    if (userVO != null && Common.getConfig("passwordExpiring").equals("1")) {
                        if (!Common.getConfig("passwordlockout").equals("0") && userVO.getLocked() == 1) {
                            errors.add("user.locked", new ActionError("pixalere.login.error.lockedout"));

                        }
                        ProfessionalServiceImpl.PasswordStatus passwordStatus = userBD.passwordExpired(userVO.getId() + "");
                        //System.out.println("PassworExpiry: " + passwordStatus);

                        switch (passwordStatus) {
                            case EXPIRED:
                                setChangePasswordFlag(1);
                                request.setAttribute("passwordExpiring", Common.getLocalizedString("pixalere.login.form.password_expired", locale));
                                //errors.add("invalid.login", new ActionError("pixalere.login.error.passwordExpired"));
                                break;
                            case ONE_DAY_LEFT:
                                request.setAttribute("passwordExpiring", Common.getLocalizedString("pixalere.login.form.password_expiring_before", locale) + " " + passwordStatus + " " + Common.getLocalizedString("pixalere.login.form.password_expiring_after", locale));
                            case TWO_DAYS_LEFT:
                                request.setAttribute("passwordExpiring", Common.getLocalizedString("pixalere.login.form.password_expiring_before", locale) + " " + passwordStatus + " " + Common.getLocalizedString("pixalere.login.form.password_expiring_after", locale));
                            case THREE_DAYS_LEFT:
                                request.setAttribute("passwordExpiring", Common.getLocalizedString("pixalere.login.form.password_expiring_before", locale) + " " + passwordStatus + " " + Common.getLocalizedString("pixalere.login.form.password_expiring_after", locale));
                            case FOUR_DAYS_LEFT:
                                request.setAttribute("passwordExpiring", Common.getLocalizedString("pixalere.login.form.password_expiring_before", locale) + " " + passwordStatus + " " + Common.getLocalizedString("pixalere.login.form.password_expiring_after", locale));
                            case FIVE_DAYS_LEFT:
                                //TODO: externalise this string
                                request.setAttribute("passwordExpiring", Common.getLocalizedString("pixalere.login.form.password_expiring_before", locale) + " " + passwordStatus + " " + Common.getLocalizedString("pixalere.login.form.password_expiring_after", locale));
                                break;
                        }

                    }
                }

                if (userVO == null) {
                    ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                    if (Common.getConfig("useUserNames").equals("1")) {

                        ProfessionalVO u = userBD.getProfessionalByUsernameOrId(userId);
                        //System.out.println("lock check" + u);
                        if (u != null && !Common.getConfig("passwordlockout").equals("0")) {
                            u.setInvalid_password_count(u.getInvalid_password_count() + 1);

                            if (u.getInvalid_password_count().equals(new Integer(Common.getConfig("passwordlockout")))) {
                                u.setLocked(1);
                                userBD.saveProfessional(u, u.getId());
                                //audit log the lockout
                                LogAuditServiceImpl bd = new LogAuditServiceImpl();

                                LogAuditVO vo = new LogAuditVO(u.getId(), new Integer(0), Constants.LOCKED_OUT, request.getRemoteAddr());
                                bd.saveLogAudit(vo);
                                if(Common.getConfig("logAuditEmails") !=null && !Common.getConfig("logAuditEmails").equals("")){
                                    try{
                                    String emails = Common.getConfig("logAuditEmails");
                                    com.pixalere.mail.SendMailUsingAuthentication sm = new com.pixalere.mail.SendMailUsingAuthentication();
                                    sm.postMail(emails, "Pixalere Nofication: User lock out", "Pixalere Nofication: "+ u.getFullName()+" is locked out\nUser ID: "+u.getId(), "");
                                    }catch(Exception e){
                                        log.error("Error Sending Notification Email: ",e);
                                    }
                                }
                            } else {
                                userBD.saveProfessional(u, u.getId());
                            }
                        }
                        errors.add("invalid.login", new ActionError("pixalere.login.error.invalidLoginUserName", Common.getConfig("wrongpassword_info")));
                    } else {
                        ProfessionalVO u = userBD.getProfessionalByUsernameOrId(userId);
                        //System.out.println("lock check" + u);
                        if (u != null && !Common.getConfig("passwordlockout").equals("0")) {
                            u.setInvalid_password_count(u.getInvalid_password_count() + 1);
                            if (u.getInvalid_password_count().equals(new Integer(Common.getConfig("passwordlockout")))) {
                                u.setLocked(1);
                                userBD.saveProfessional(u, u.getId());
                                if(Common.getConfig("logAuditEmails") !=null && !Common.getConfig("logAuditEmails").equals("")){
                                    try{
                                    String emails = Common.getConfig("logAuditEmails");
                                    com.pixalere.mail.SendMailUsingAuthentication sm = new com.pixalere.mail.SendMailUsingAuthentication();
                                    sm.postMail(emails, "Pixalere Nofication: User lock out", "Pixalere Nofication: "+ u.getFullName()+" is locked out\nUser ID: "+u.getId(), "");
                                    }catch(Exception e){
                                        log.error("Error Sending Notification Email: ",e);
                                    }
                                }
                            } else {
                                //System.out.println("lock asdfasdfcheck"+u.getInvalid_password_count());
                                userBD.saveProfessional(u, u.getId());
                            }
                        }
                        errors.add("invalid.login", new ActionError("pixalere.login.error.invalidLoginUserNumber", Common.getConfig("wrongpassword_info")));
                    }

                } else {
                    if (!Common.getConfig("passwordlockout").equals("0") && userVO.getLocked() == 1) {
                        errors.add("user.locked", new ActionError("pixalere.login.error.lockedout"));

                    } else {
                        String ip_address = (String)request.getRemoteAddr();
                        //Everything looks good, now verify that this user is allowed to login from IP address
                        boolean login_allowed=false;
                        if(Common.isOffline() == false && userVO.getAllow_remote_access()!=null && userVO.getAllow_remote_access().equals(new Integer(1))){
                            login_allowed=true;
                        }else if(Common.isOffline() == false){
                            //check this user's facilities and ensure IP address matches.
                            //as they aren't allowed remote logins
                            
                            Collection<UserAccountRegionsVO> regions = userVO.getRegions();
                            for(UserAccountRegionsVO region : regions){
                                String ip_addr = region.getTreatmentLocation().getReport_value();
                                //if(ip_addr!=null){
                                    //for(String ip : ip_addr.split(",")){
                                        if(ip_addr!=null && !ip_addr.equals("") && ip_addr.indexOf(ip_address)!=-1){
                                            login_allowed=true;
                                        }
                                    //}
                                //}
                            }
                            
                        }else if(Common.isOffline() == true){
                            login_allowed=true;
                        }
                        if(login_allowed == false){
                            errors.add("user.disallowed", new ActionError("pixalere.login.error.ip_address",ip_address));
                        }
                        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                        session.setAttribute("userVO", userVO);
                        session.setAttribute(Common.SESSION_LANGUAGE, getLanguage());
                        //ListServiceImpl lservice = new ListServiceImpl();
                        //LanguageVO lang = lservice.getLanguage(getLanguage());
                        session.setAttribute(org.apache.struts.Globals.LOCALE_KEY, Common.getLocaleFromLocaleString(locale));
                        
                        userVO.setLocked(0);
                        userVO.setInvalid_password_count(0);
                        userBD.saveProfessional(userVO, userVO.getId());
                        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);

                        LogAuditServiceImpl bd = new LogAuditServiceImpl();

                        LogAuditVO vo = new LogAuditVO(userVO.getId(), new Integer(0), Constants.LOGGED_IN, request.getRemoteAddr());
                        vo.setDescription(request.getHeader("User-Agent"));
                        bd.saveLogAudit(vo);


                        // DON'T REMOVE THE FOLLOWING LINE PLEASE, WE NEED IT IN THE LOGIN
                        System.out.println(pdate.getDateStringWithHourAndSeconds(new Date(),locale)+ " -- Logon to Live by user " + getUserId());
                        if (Common.isOffline() == false && (!(userVO.getNew_user()).equals(new Integer(1)) && !Common.getConfig("password_reset").equals("none") && ((userVO.getPager() == null || userVO.getPager().equals("")) && ((userVO.getAnswer_one() == null || userVO.getAnswer_one().equals("")) && (userVO.getAnswer_two() == null || userVO.getAnswer_two().equals("")))))) {
                            //This user doesn't have a pager or answer set.  Ask them to enter the password reset information
                            request.setAttribute("set_password_reset", "yes");
                        }
                    }
                }
            }

        }/*catch(java.io.IOException e){
         errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));


         } */ catch (ApplicationException e) {
            System.out.println("ApplicationException - Check");
            errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));

        }
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setUserId("");
        setPassword("");
    }

    /**
     * Returns the password.
     *
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Returns the userId.
     *
     * @return String
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the password.
     *
     * @param password The password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets the userId.
     *
     * @param userId The userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    private Integer changePasswordFlag;

    public Integer getChangePasswordFlag() {
        return changePasswordFlag;
    }

    public void setChangePasswordFlag(Integer changePasswordFlag) {
        this.changePasswordFlag = changePasswordFlag;
    }

    /**
     * @return the language
     */
    public Integer getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(Integer language) {
        this.language = language;
    }

    /**
     * @return the act
     */
    public String getAct() {
        return act;
    }

    /**
     * @param act the act to set
     */
    public void setAct(String act) {
        this.act = act;
    }

    /**
     * @return the license
     */
    public String getLicense() {
        return license;
    }

    /**
     * @param license the license to set
     */
    public void setLicense(String license) {
        this.license = license;
    }

    /**
     * @return the question_one
     */
    public Integer getQuestion_one() {
        return question_one;
    }

    /**
     * @param question_one the question_one to set
     */
    public void setQuestion_one(Integer question_one) {
        this.question_one = question_one;
    }

    /**
     * @return the question_two
     */
    public Integer getQuestion_two() {
        return question_two;
    }

    /**
     * @param question_two the question_two to set
     */
    public void setQuestion_two(Integer question_two) {
        this.question_two = question_two;
    }

    /**
     * @return the answer_one
     */
    public String getAnswer_one() {
        return answer_one;
    }

    /**
     * @param answer_one the answer_one to set
     */
    public void setAnswer_one(String answer_one) {
        this.answer_one = answer_one;
    }

    /**
     * @return the answer_two
     */
    public String getAnswer_two() {
        return answer_two;
    }

    /**
     * @param answer_two the answer_two to set
     */
    public void setAnswer_two(String answer_two) {
        this.answer_two = answer_two;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
}
