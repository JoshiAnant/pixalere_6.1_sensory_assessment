/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.login;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 *
 * @author travis
 */
public class SaveAnswersForm extends ActionForm {
        private Integer question_one;
    private Integer question_two;
    private String answer_one;
    private String answer_two;
    private String phone;
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LoginForm.class);

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    /**
     * @return the question_one
     */
    public Integer getQuestion_one() {
        return question_one;
    }

    /**
     * @param question_one the question_one to set
     */
    public void setQuestion_one(Integer question_one) {
        this.question_one = question_one;
    }

    /**
     * @return the question_two
     */
    public Integer getQuestion_two() {
        return question_two;
    }

    /**
     * @param question_two the question_two to set
     */
    public void setQuestion_two(Integer question_two) {
        this.question_two = question_two;
    }

    /**
     * @return the answer_one
     */
    public String getAnswer_one() {
        return answer_one;
    }

    /**
     * @param answer_one the answer_one to set
     */
    public void setAnswer_one(String answer_one) {
        this.answer_one = answer_one;
    }

    /**
     * @return the answer_two
     */
    public String getAnswer_two() {
        return answer_two;
    }

    /**
     * @param answer_two the answer_two to set
     */
    public void setAnswer_two(String answer_two) {
        this.answer_two = answer_two;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
}