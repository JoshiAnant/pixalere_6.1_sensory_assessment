package com.pixalere.struts.login;

import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.service.ConfigurationServiceImpl;
import java.util.Hashtable;
import com.pixalere.utils.PDate;
import java.util.Date;
import com.pixalere.utils.Common;
import com.pixalere.common.service.LicenseServiceImpl;
import java.util.Locale;

public class LoginSetup extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LoginSetup.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        Common common = new Common();

        
        HttpSession session = request.getSession();
        if (session != null) {
            if (session.getAttribute("patientsync") != null) {
                request.setAttribute("patientsync", session.getAttribute("patientsync"));
                session.removeAttribute("patientsync");
            }
            if (session.getAttribute("syncsuccess") != null) {
                request.setAttribute("syncsuccess", session.getAttribute("syncsuccess"));
                session.removeAttribute("syncsuccess");
            }
        }
        // Display Login in Default language
        String locale = Common.getConfig(Common.DEFAULT_LANGUAGE_CONF);
        
        session.setAttribute(org.apache.struts.Globals.LOCALE_KEY, Common.getLocaleFromLocaleString(locale));
        request.setAttribute("default_language", locale);
        
        /*System.out.println("Version Mismatch!");
        if(!Common.isDBPatched()){
            System.out.println("Version Mismatch!"+Common.isDBPatched());
            request.setAttribute("status", Common.migrationStatus());
            return (mapping.findForward("version.mismatch"));
        }*/
        
        if (Common.isOffline() == true && common.checkConnection() == true) {
            if (request.getParameter("frmlnd") != null && ((String) request.getParameter("frmlnd")).equals("1")) {

                request.setAttribute("frmlnd", "1");
            }
            log.error("Connection is Active ============================");
            request.setAttribute("offline_online", "1");
        } else if (Common.isOffline() == true) {
            log.error("Connection is Dead ============================");
            request.setAttribute("offline_online", "0");
        }
        if (request.getAttribute("session_invalid") != null) {
            request.setAttribute("session_invalid", request.getAttribute("session_invalid"));
        }

        ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
        Hashtable config = new Hashtable();
        try {
            config = manager.getConfiguration();
            Common.CONFIG = config;
        } catch (ApplicationException e) {
            log.error("DataAccessException in retrieveConfiguration: " + e.toString());
            System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());

            e.printStackTrace();
        }
        if (!Common.isOffline()){
            LicenseServiceImpl lservice = new LicenseServiceImpl();
            /*Date date = PDate.getLicenseDate(lservice.getValidUntil());
            System.out.println("date: "+date);
            if(date == null){
                request.setAttribute("license_expired","true");
                return (mapping.findForward("pixalere.error"));
            } else if (PDate.getDaysBetweenDates( date,new Date()) < 30 && PDate.getDaysBetweenDates(date,new Date()) > 0) {
                request.setAttribute("expiry_soon", "true");
            } else if (PDate.getDaysBetweenDates( date,new Date()) < 1) {
                request.setAttribute("license_expired","true");
                return (mapping.findForward("pixalere.error"));
            }*/
        }
        /*try {
        ListClient listClient = new ListClient();
        ListService remotelistService = listClient.createListClient();

        Collection<LookupVO> lists = remotelistService.getAllListsForOffline();


        } catch (ApplicationException e) {
        ActionErrors errors = new ActionErrors();
        request.setAttribute("exception", "y");
        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
        errors.add("exception", new ActionError("pixalere.common.error"));
        saveErrors(request, errors);
        return (mapping.findForward("pixalere.error"));
        } catch (Exception e) {
        e.printStackTrace();
        }*/
        //don't set it in the session until after they login... wasting resources.
        //session.setAttribute("config", config);
        request.setAttribute("config",config);

        return (mapping.findForward("login.success"));

    }
}
