/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.login;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.Action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.Common;
import com.pixalere.utils.MD5;

import java.util.HashMap;
import java.util.Map;

/**
 * @since 6.0
 * @version 6.0
 * @author travis
 */
public class SaveAnswersAction extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.struts.login.SaveAnswersAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        SaveAnswersForm answersform = (SaveAnswersForm) form;
        try {
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            if (userVO == null) {//session is invalid.. return to login
                request.setAttribute("session_invalid", "yes");
                return (mapping.findForward("system.logoff"));
            } else {
                if (answersform.getPhone() != null && !answersform.getPhone().equals("")) {
                    userVO.setPager(answersform.getPhone());
                }
                if (answersform.getPhone() != null && !answersform.getPhone().equals("")) {
                    userVO.setReset_password_method(1);
                } else {
                    userVO.setReset_password_method(0);
                }
                if (answersform.getAnswer_one() != null && !answersform.getAnswer_one().equals("")) {
                    userVO.setAnswer_one(MD5.hash(answersform.getAnswer_one()));
                    userVO.setQuestion_one(answersform.getQuestion_one());
                }
                if (answersform.getAnswer_two() != null && !answersform.getAnswer_two().equals("")) {
                    userVO.setAnswer_two(MD5.hash(answersform.getAnswer_two()));
                    userVO.setQuestion_two(answersform.getQuestion_two());
                }
                pservice.saveProfessional(userVO,userVO.getId());
                session.setAttribute("userVO",userVO);
            }
        } catch (ApplicationException e) {
            e.printStackTrace();
            //errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));

        }
        request.setAttribute("reset_answered_saved","true");
        return (mapping.findForward("login.success"));
    }
}
