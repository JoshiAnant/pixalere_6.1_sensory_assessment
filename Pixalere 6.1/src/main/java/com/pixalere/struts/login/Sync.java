package com.pixalere.struts.login;

import com.pixalere.auth.client.ProfessionalClient;
import com.pixalere.auth.service.ProfessionalService;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.service.*;
import com.pixalere.common.client.*;
import com.pixalere.common.bean.*;
import com.pixalere.assessment.client.*;
import com.pixalere.assessment.service.*;
import com.pixalere.assessment.bean.*;
import com.pixalere.auth.bean.PositionVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.*;
import com.pixalere.utils.Common;
import com.pixalere.patient.bean.*;
import com.pixalere.patient.service.*;
import com.pixalere.patient.client.*;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.common.*;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.admin.client.ListClient;
import com.pixalere.common.service.ListService;
import com.pixalere.admin.service.ResourcesServiceImpl;
import com.pixalere.admin.client.ResourcesClient;
import com.pixalere.admin.service.ResourcesService;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.wound.bean.*;
import com.pixalere.wound.client.*;
import com.pixalere.wound.service.*;
import java.util.*;
import java.util.TimeZone;
import com.pixalere.patient.bean.BradenVO;

public class Sync extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Sync.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        log.error("Sync.class starting...");

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        if(userVO == null){
             request.setAttribute("session_invalid", "yes");
            return (mapping.findForward("system.logoff"));
        }
        
        Integer language = Common.getLanguageIdFromSession(session);

        if (Common.isOffline() == true) {
            try {
                //Initialize Local Services
                PatientProfileServiceImpl localProfileService = new PatientProfileServiceImpl(language);
                PatientServiceImpl localPatientService = new PatientServiceImpl(language);
                WoundServiceImpl localWoundService = new WoundServiceImpl(language);
                AssessmentServiceImpl localAssessmentService = new AssessmentServiceImpl(language);
                AssessmentNPWTServiceImpl localNPWTService = new AssessmentNPWTServiceImpl();
                AssessmentImagesServiceImpl localImagesService = new AssessmentImagesServiceImpl();
                AssessmentCommentsServiceImpl localCommentsService = new AssessmentCommentsServiceImpl();
                WoundAssessmentServiceImpl localWoundAssessmentService = new WoundAssessmentServiceImpl();
                TreatmentCommentsServiceImpl localTcService = new TreatmentCommentsServiceImpl();
                WoundAssessmentLocationServiceImpl localAlphaService = new WoundAssessmentLocationServiceImpl();
                ListServiceImpl localListService = new ListServiceImpl(language);
                ResourcesServiceImpl localResourceService = new ResourcesServiceImpl();
                GUIServiceImpl localGUIService = new GUIServiceImpl();
                NursingCarePlanServiceImpl localNcpService = new NursingCarePlanServiceImpl();

                ConfigurationServiceImpl localConfigurationService = new ConfigurationServiceImpl();
                ProfessionalServiceImpl localProfessionalService = new ProfessionalServiceImpl();
                ProductsServiceImpl localProductsService = new ProductsServiceImpl();
                ProductCategoryServiceImpl localProductCategoryService = new ProductCategoryServiceImpl();
                LibraryServiceImpl localPdfService = new LibraryServiceImpl();

                ProfessionalClient apc = new ProfessionalClient();
                ProfessionalService remoteProfessionalService = apc.createProfessionalClient();

                //setup remote services.
                ConfigurationClient cc = new ConfigurationClient();
                ConfigurationService remoteConfigurationService = cc.createConfigurationClient();


                AssessmentNPWTClient npwtc = new AssessmentNPWTClient();
                AssessmentNPWTService remoteNPWTService = npwtc.createNPWTClient();

                NursingCarePlanClient ncpc = new NursingCarePlanClient();
                NursingCarePlanService remoteNcpService = ncpc.createNursingCarePlanClient();


                TreatmentCommentsClient tcc = new TreatmentCommentsClient();
                TreatmentCommentsService remoteTcService = tcc.createTreatmentCommentsClient();


                LibraryClient lc = new LibraryClient();
                LibraryService remoteLibraryService = lc.createLibraryClient();

                WoundClient wc = new WoundClient();
                WoundService remoteWoundService = wc.createWoundClient();

                AssessmentClient ac = new AssessmentClient();
                AssessmentService remoteAssessmentService = ac.createAssessmentClient();

                GUIClient guiClient = new GUIClient();
                GUIService remoteGUIService = guiClient.createGUIClient();
                WoundAssessmentLocationClient walc = new WoundAssessmentLocationClient();
                WoundAssessmentLocationService remoteAlphaService = walc.createWoundAssessmentLocationClient();


                WoundAssessmentClient wac = new WoundAssessmentClient();
                WoundAssessmentService remoteWoundAssessmentService = wac.createWoundAssessmentClient();


                AssessmentImagesClient aic = new AssessmentImagesClient();
                AssessmentImagesService remoteImagesService = aic.createAssessmentImagesClient();


                AssessmentCommentsClient acc = new AssessmentCommentsClient();
                AssessmentCommentsService remoteCommentsService = acc.createAssessmentCommentsClient();



                WoundAssessmentVO wvo = new WoundAssessmentVO();
                WoundAssessmentLocationVO lvo = new WoundAssessmentLocationVO();


                TableUpdatesVO updatesServer = remoteConfigurationService.getTableUpdates();
                TableUpdatesVO updatesLocal = localConfigurationService.getTableUpdates();
                //test

                //if config.. clean patient on sync...
                //localProfileService.removeOffline(off);
                if (Common.getConfig("deletePatientsOnSync").equals("1")) {
                    //remove all data pp, wp, wa, ass,im,pro,comm,recomm,ncp,tc,dcf
                    try {
                        //off.setPatient_id(new Integer(patientid + ""));
                        PatientAccountVO pa = new PatientAccountVO();
                        Collection<PatientAccountVO> patients = localPatientService.getAllPatients(pa);
                        if (patients != null) {
                            for (PatientAccountVO patient : patients) {
                                log.error("Patient deletion before.. " + patient.getPatient_id());
                                localProfileService.removePatientProfile(patient.getPatient_id() + "");

                                localWoundAssessmentService.removeAssessmentOffline(patient.getPatient_id(), patient.getId());

                                WoundAssessmentLocationVO wlvo = new WoundAssessmentLocationVO();
                                wlvo.setPatient_id(patient.getPatient_id());
                                localAlphaService.removeAlpha(wlvo, null);

                                WoundProfileTypeVO wptvo = new WoundProfileTypeVO();
                                wptvo.setPatient_id(patient.getPatient_id());
                                localWoundService.dropWoundProfileTypeOffline(wptvo);

                                WoundProfileVO tw = new WoundProfileVO();
                                tw.setPatient_id(patient.getPatient_id());
                                localWoundService.dropOffline(tw);

                                localProfileService.removeFootAssessment(patient.getPatient_id() + "");
                                localProfileService.removeLimbBasicAssessment(patient.getPatient_id() + "");
                                localProfileService.removeLimbAdvAssessment(patient.getPatient_id() + "");
                                localProfileService.removeLimbUpperAssessment(patient.getPatient_id() + "");
                                localProfileService.removeExam(patient.getPatient_id() + "");
                                localProfileService.removeDiagnosticInvestigation(patient.getPatient_id() + "");
                                OfflineVO off = new OfflineVO();
                                off.setPatient_id(patient.getPatient_id());
                                localProfileService.removeOffline(off);
                                localPatientService.removePatient(patient.getPatient_id() + "");
                            }
                        }
                    } catch (ApplicationException e) {
                        e.printStackTrace();
                    }
                    try {
                        OfflineVO off = new OfflineVO();
                        localProfileService.removeOffline(off);
                    } catch (ApplicationException e) {
                    }
                }
                if (updatesLocal == null || updatesLocal.getListdata() == null || updatesServer.getListdata().intValue() > updatesLocal.getListdata().intValue()) {
                    //Retreive remote Services
                    ListClient listClient = new ListClient();
                    ListService remotelistService = listClient.createListClient();

                    Collection<LookupVO> lists = remotelistService.getAllListsForOffline();
                    Collection<LanguageVO> languages = remotelistService.getLanguages();
                    PositionVO p = new PositionVO();
                    Collection<PositionVO> positions = remoteProfessionalService.getPositions(p);
                    if (languages != null) {
                        for (LanguageVO lang : languages) {
                            localListService.saveLanguage(lang);
                        }
                    }
                    if (lists != null) {

                        // LookupVO l3 = localListService.getListItem(1572);
                        // LookupVO l4 = localListService.getListItem(962);

                        for (LookupVO lookupVO : lists) {
                            
                            localListService.saveListItemForOffline(lookupVO);
                            if (lookupVO != null && lookupVO.getNames() != null) {
                                
                                for (LookupLocalizationVO l : lookupVO.getNames()) {
                                   
                                    localListService.saveLocalization(l);
                                }
                            } else {
                            }
                        }

                        // l3 = localListService.getListItem(1572);
                        // l4 = localListService.getListItem(962);
                        // System.out.println(l3.getName()+" -: "+l3+" List After "+l4+" :- "+l4.getName());

                    }

                    if (positions != null) {
                        for (PositionVO position : positions) {
                            localProfessionalService.savePosition(position);
                        }
                    }
                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setListdata(updatesServer.getListdata());
                    localConfigurationService.saveTableUpdates(newVO);
                    updatesLocal = localConfigurationService.getTableUpdates();//we need to update the local object so its not overwritten later.
                }
                /*if (updatesLocal == null || updatesLocal.getPdf() == null || updatesServer.getPdf().intValue() > updatesLocal.getPdf().intValue()) {

                 Vector<LibraryVO> pdfs = remoteLibraryService.getAllPdfFiles();
                 //delete all old files.
                 File pathoff = new File(Common.getDownloadsPath() );
                 if (!pathoff.exists()) {
                 try {
                 pathoff.mkdir();
                 } catch (Exception e) {
                 throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);

                 }
                 }
                 if (pdfs != null) {
                 for (LibraryVO pdf : pdfs) {
                 localPdfService.saveLibrary(pdf);
                 DataHandler dh = pdf.getPdfFile();
                 String filename = Common.getDownloadsPath() +""+ pdf.getPdf();
                 (dh+"PDF "+filename);
                 Common.saveFile(dh, filename);
                 }
                 }
                 TableUpdatesVO newVO = new TableUpdatesVO();
                 if (updatesLocal != null) {
                 newVO = updatesLocal;
                 }
                 newVO.setPdf(updatesServer.getPdf());

                 localConfigurationService.saveTableUpdates(newVO);
                 } */
                if (updatesLocal == null || updatesLocal.getLocation_images() == null || updatesServer.getLocation_images().intValue() > updatesLocal.getLocation_images().intValue()) {
                    Collection<LocationImagesVO> img = remoteWoundService.getAllLocationImages();

                    if (img != null) {
                        for (LocationImagesVO info : img) {
                            localWoundService.saveLocationImage(info);
                        }
                    }
                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setLocation_images(updatesServer.getLocation_images());

                    localConfigurationService.saveTableUpdates(newVO);
                    //updatesLocal =localConfigurationService.getTableUpdates();
                }
                if (updatesLocal == null || updatesLocal.getInformation_popup() == null || updatesServer.getInformation_popup().intValue() > updatesLocal.getInformation_popup().intValue()) {
                    Collection<InformationPopupVO> img = remoteGUIService.getAllInformationPopups();

                    if (img != null) {
                        for (InformationPopupVO info : img) {
                            localGUIService.saveInformationPopup(info);
                        }
                    }
                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setInformation_popup(updatesServer.getInformation_popup());

                    localConfigurationService.saveTableUpdates(newVO);
                    //updatesLocal =localConfigurationService.getTableUpdates();
                }
                if (updatesLocal == null || updatesLocal.getComponents() == null || updatesServer.getComponents().intValue() > updatesLocal.getComponents().intValue()) {
                    ComponentsVO crit = new ComponentsVO();
                    Collection<ComponentsVO> components = remoteGUIService.getAllComponents(crit);
                    if (components != null) {
                        for (ComponentsVO componentsVO : components) {
                            localGUIService.saveComponent(componentsVO);
                        }
                    }

                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setComponents(updatesServer.getComponents());

                    localConfigurationService.saveTableUpdates(newVO);
                }
                
                if (updatesLocal == null || updatesLocal.getConfiguration() == null || updatesServer.getConfiguration().intValue() > updatesLocal.getConfiguration().intValue()) {
                    
                    Collection<ConfigurationVO> configuration = remoteConfigurationService.getAllConfigurations();
                    if (configuration != null) {
                        for (ConfigurationVO configurationVO : configuration) {
                            if (!configurationVO.getComponent_type().equals("CAT")) {
                                localConfigurationService.saveConfiguration(configurationVO);
                            }
                        }
                    }

                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setConfiguration(updatesServer.getConfiguration());
                    localConfigurationService.saveTableUpdates(newVO);
                }
                String serverZone = remoteConfigurationService.getServerTimeZone();
             
                TimeZone.setDefault(TimeZone.getTimeZone(serverZone));
                ConfigurationVO configTimezone = new ConfigurationVO();
                configTimezone.setConfig_setting("timezone");
                ConfigurationVO currentTimezone = localConfigurationService.getConfiguration(configTimezone);
                if(currentTimezone!=null){currentTimezone.setCurrent_value(serverZone);localConfigurationService.saveConfiguration(configTimezone);}
                if (updatesLocal == null || updatesLocal.getResources() == null || updatesServer.getResources().intValue() > updatesLocal.getResources().intValue()) {
                    ResourcesClient rClient = new ResourcesClient();
                    ResourcesService remoteRService = rClient.createResourceClient();

                    Collection<ResourcesVO> rlists = remoteRService.getAllResources();
                    if (rlists != null) {

                        for (ResourcesVO lookupVO : rlists) {
                            localResourceService.saveResource(lookupVO);
                        }
                    }

                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setResources(updatesServer.getResources());
                    localConfigurationService.saveTableUpdates(newVO);
                    //updatesLocal =localConfigurationService.getTableUpdates();
                }

                //Vector elists = dservice.downloadEmail();
                ProductsClient prodc = new ProductsClient();
                ProductsService remoteProductsService = prodc.createProductClient();


                ProductCategoryClient pcc = new ProductCategoryClient();
                ProductCategoryService remoteProductCategoryService = pcc.createProductCategoryClient();


                if (updatesLocal == null || updatesLocal.getProducts() == null || updatesServer.getProducts().intValue() > updatesLocal.getProducts().intValue()) {
                    Collection<ProductsVO> products = remoteProductsService.getAllProducts();
                    if (products != null) {
                        for (ProductsVO lookupVO : products) {
                            localProductsService.saveProduct(lookupVO);
                            Collection<ProductsByFacilityVO> fac = lookupVO.getFacilities();
                            if(fac!=null){
                                for(ProductsByFacilityVO f : fac){
                                    localProductsService.saveProductsByFacility(f);
                                }
                            }
                        }
                    }
                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setProducts(updatesServer.getProducts());
                    localConfigurationService.saveTableUpdates(newVO);
                    //updatesLocal =localConfigurationService.getTableUpdates();
                }
                if (updatesLocal == null || updatesLocal.getProducts_categories() == null || updatesServer.getProducts_categories().intValue() > updatesLocal.getProducts_categories().intValue()) {
                    Collection<ProductCategoryVO> categories = remoteProductCategoryService.getAllProductCategoriesByCriteria(new ProductCategoryVO());
                    if (categories != null) {
                        for (ProductCategoryVO lookupVO : categories) {
                            localProductCategoryService.saveProductCategory(lookupVO);
                        }
                    }
                    TableUpdatesVO newVO = new TableUpdatesVO();
                    if (updatesLocal != null) {
                        newVO = updatesLocal;
                    }
                    newVO.setProducts_categories(updatesServer.getProducts_categories());
                    localConfigurationService.saveTableUpdates(newVO);
                    //updatesLocal =localConfigurationService.getTableUpdates();
                }
                ProfessionalVO userVO2 = (ProfessionalVO) remoteProfessionalService.getProfessional(((ProfessionalVO) session.getAttribute("userVO")).getId());
                localProfessionalService.saveProfessional(userVO2, userVO2.getId());
                if (userVO2 != null) {
                    Collection<UserAccountRegionsVO> regions = userVO2.getRegions();
                    if (regions != null) {
                        for (UserAccountRegionsVO region : regions) {
                            localProfessionalService.saveTreatmentLocation(region);
                        }
                    }
                }
                // Remove any old data for the patients
                for (int x = 0; x < 10; x++) {
                    if (request.getParameter("patient" + (x + 1)) != null && !((String) request.getParameter("patient" + (x + 1))).equals("")) {
                        String search = ((String) request.getParameter("patient" + (x + 1)));
                        try{
                        // PatientAccountVO paco = (PatientAccountVO) piter.next();
                        Integer patientid = new Integer(search);
                        PatientAccountVO pavo = new PatientAccountVO();
                        wvo.setPatient_id(patientid);
                        pavo.setPatient_id(patientid);
                        lvo.setPatient_id(patientid);

                        OfflineVO off = new OfflineVO();
                        off.setPatient_id(new Integer(patientid + ""));

                        localProfileService.removeOffline(off);


                        WoundAssessmentVO a = new WoundAssessmentVO();
                        a.setPatient_id(patientid);
                        localWoundAssessmentService.removeAssessment(a, null);

                        localAlphaService.removeAlpha(lvo, null);

                        WoundProfileVO tw = new WoundProfileVO();
                        tw.setPatient_id(new Integer(patientid + ""));
                        localWoundService.dropOffline(tw);

                        localProfileService.removeOffline(off);
                        localProfileService.removeFootAssessment(patientid + "");
                        localProfileService.removeLimbBasicAssessment(patientid + "");
                        localProfileService.removeLimbAdvAssessment(patientid + "");
                        localProfileService.removeDiagnosticInvestigation(patientid + "");
                        localProfileService.removePatientProfile(patientid + "");
                        localPatientService.removePatient(patientid + "");
                        }catch(NumberFormatException e){
                            //ignore
                        }
                    }

                }

                String patient_status = "";
                for (int x = 0; x < 10; x++) {

                    log.error("Patient ID: " + patient_status);
                    String search = ((String) request.getParameter("patient" + (x + 1)));

                    boolean error = false;
                    if (request.getParameter("patient" + (x + 1)) != null && !((String) request.getParameter("patient" + (x + 1))).equals("") && isNumber((String) request.getParameter("patient" + (x + 1)))) {
                        // Check if number is valid
                        try {
                            Integer number = new Integer(search);
                        } catch (NumberFormatException e) {
                            error = true;
                            patient_status = patient_status + "Patient " + ((String) request.getParameter("patient" + (x + 1))) + " must be a valid number.<br/>";

                        }
                        PatientClient patc = new PatientClient();
                        PatientService remotePatientService = patc.createPatientClient();


                        PatientAccountVO account = (PatientAccountVO) remotePatientService.getPatient(new Integer(search));

                        if (account != null) {
                            int err = remotePatientService.validatePatientId(((ProfessionalVO) session.getAttribute("userVO")).getId().intValue(), new Integer(search));

                            if (err == 3 || err == 2) {
                                error = true;
                                patient_status = patient_status + "Patient " + ((String) request.getParameter("patient" + (x + 1))) + " is not in your treatment location, or you are not assigned to this patient.<br/>";

                            }
                        } else {
                            error = true;
                            patient_status = patient_status + "Patient " + ((String) request.getParameter("patient" + (x + 1))) + " does not exist.<br/>";

                        }
                        if (error == false) {

                            localPatientService.updatePatient(account);
                            PatientProfileClient ppc = new PatientProfileClient();
                            PatientProfileService remoteProfileService = ppc.createPatientProfileClient();
                            
                            PatientProfileVO pcrit = new PatientProfileVO();
                            pcrit.setActive(1);
                            pcrit.setCurrent_flag(1);
                            pcrit.setPatient_id(new Integer(search));
                            PatientProfileVO vo = (PatientProfileVO) remoteProfileService.getPatientProfile(pcrit);
                            if (vo != null) {
                                vo.setOffline_flag(new Integer(0));
                                localProfileService.savePatientProfile(vo);
                                Collection<PatientProfileArraysVO> parrays = vo.getPatient_profile_arrays();
                                if (parrays != null) {
                                    for (PatientProfileArraysVO p : parrays) {
                                        localProfileService.savePatientProfileArray(p);
                                    }
                                }
                                Collection<ExternalReferralVO> referrals = vo.getExternalReferrals();
                                if(referrals!=null){
                                    for(ExternalReferralVO xt:  referrals){
                                        
                                        localProfileService.saveExternalReferral(xt);
                                    }
                                }
                            }
                            
                            DiagnosticInvestigationVO dcrit = new DiagnosticInvestigationVO();
                            dcrit.setActive(1);
                            dcrit.setPatient_id(new Integer(search));
                            DiagnosticInvestigationVO vo5 = (DiagnosticInvestigationVO) remoteProfileService.getDiagnosticInvestigation(dcrit);
                            if (vo5 != null) {
                                localProfileService.saveDiagnosticInvestigation(vo5);

                                Collection<InvestigationsVO> investigations = vo5.getInvestigations();
                                if (investigations != null) {
                                    for (InvestigationsVO investigation : investigations) {
                                        localProfileService.saveInvestigation(investigation);
                                    }
                                }
                            }
                            FootAssessmentVO fcrit = new FootAssessmentVO();
                            fcrit.setActive(1);
                            fcrit.setPatient_id(new Integer(search));
                            FootAssessmentVO foot = (FootAssessmentVO) remoteProfileService.getFootAssessment(fcrit);
                            if (foot != null) {
                                localProfileService.saveFoot(foot);
                            }
                            LimbBasicAssessmentVO lcrit = new LimbBasicAssessmentVO();
                            lcrit.setActive(1);
                            lcrit.setPatient_id(new Integer(search));
                            LimbBasicAssessmentVO limb = (LimbBasicAssessmentVO) remoteProfileService.getLimbBasicAssessment(lcrit);
                            if (limb != null) {
                                localProfileService.saveBasicLimb(limb);
                            }
                            LimbAdvAssessmentVO lacrit = new LimbAdvAssessmentVO();
                            lacrit.setActive(1);
                            lacrit.setPatient_id(new Integer(search));
                            LimbAdvAssessmentVO limba = (LimbAdvAssessmentVO) remoteProfileService.getLimbAdvAssessment(lacrit);
                            if (limba != null) {
                                localProfileService.saveAdvLimb(limba);
                            }
                            LimbUpperAssessmentVO lucrit = new LimbUpperAssessmentVO();
                            lucrit.setActive(1);
                            lucrit.setPatient_id(new Integer(search));
                            LimbUpperAssessmentVO limbu = (LimbUpperAssessmentVO) remoteProfileService.getLimbUpperAssessment(lucrit);
                            if (limbu != null) {
                                localProfileService.saveUpperLimb(limbu);
                            }
                            BradenVO bcrit = new BradenVO();
                            bcrit.setActive(1);
                            bcrit.setPatient_id(new Integer(search));
                            BradenVO braden = (BradenVO) remoteProfileService.getBraden(bcrit);
                            if (braden != null) {
                                localProfileService.saveBraden(braden);
                            }
                            PhysicalExamVO excrit = new PhysicalExamVO();
                            excrit.setActive(1);
                            excrit.setPatient_id(new Integer(search));
                            PhysicalExamVO exam = (PhysicalExamVO) remoteProfileService.getExam(excrit);
                            if (exam != null) {
                                localProfileService.savePhysicalExam(exam);
                            }
                            WoundProfileVO wcrit = new WoundProfileVO();
                            wcrit.setPatient_id(new Integer(search));
                            wcrit.setActive(1);
                            wcrit.setCurrent_flag(1);
                            Collection<WoundProfileVO> wve = remoteWoundService.getWoundProfiles(wcrit, 0);
                            if (wve != null) {
                                for (WoundProfileVO woundVO : wve) {
                                    if (woundVO != null && woundVO.getWound() != null) {
                                        WoundVO wVO = woundVO.getWound();
                                        if (wVO.getStatus() == null || !wVO.getStatus().equals("Closed")) {
                                            int last_assessment_id = 0;
                                            
                                            localWoundService.saveWound(wVO);
                                            woundVO.setOffline_flag(new Integer(0));
                                            localWoundService.saveWoundProfile(woundVO);


                                            WoundProfileTypeVO[] types = wVO.getWound_profile_types();
                                            if (types != null) {
                                                for (WoundProfileTypeVO type : types) {
                                                    localWoundService.saveWoundProfileType(type);
                                                }
                                            }

                                            log.info("Sync.perform(): Inserting WoundProfile " + woundVO.getWound().getWound_location());
                                            WoundAssessmentLocationVO a = new WoundAssessmentLocationVO();
                                            a.setWound_id(wVO.getId());
                                            a.setActive(1);
                                            Collection<WoundAssessmentLocationVO> alphas = remoteAlphaService.getAllAlphas(a);
                                            if (alphas != null) {
                                                for (WoundAssessmentLocationVO eachVO : alphas) {
                                                    WoundLocationDetailsVO[] details = eachVO.getAlpha_details();

                                                    localAlphaService.saveAlpha(eachVO);
                                                    if (details != null) {
                                                        for (WoundLocationDetailsVO detail : details) {
                                                            detail.setImage(detail.getImage().trim());
                                                            localAlphaService.saveAlphaDetail(detail);
                                                        }
                                                    }




                                                    AbstractAssessmentVO ab = new AbstractAssessmentVO();
                                                    ab.setAlpha_id(eachVO.getId());
                                                    ab.setActive(1);
                                                    ab.setWound_id(eachVO.getWound_id());

                                                    Vector vecAssessments = new Vector();
                                                    Collection<AssessmentEachwoundVO> colWoundAssessments = remoteAssessmentService.getWoundAssessmentsForOffline(ab);
                                                    if (colWoundAssessments != null) {
                                                        Iterator itrWoundAssessments = colWoundAssessments.iterator();
                                                        while (itrWoundAssessments.hasNext()) {
                                                            AssessmentEachwoundVO assessmentEachwoundVO = (AssessmentEachwoundVO) itrWoundAssessments.next();
                                                            vecAssessments.add(assessmentEachwoundVO.getAssessment_id().intValue());
                                                        }
                                                    }

                                                    Collection<AssessmentDrainVO> colDrainAssessments = remoteAssessmentService.getDrainAssessmentsForOffline(ab);
                                                    if (colDrainAssessments != null) {
                                                        Iterator itrDrainAssessments = colDrainAssessments.iterator();
                                                        while (itrDrainAssessments.hasNext()) {
                                                            AssessmentDrainVO assessmentDrainVO = (AssessmentDrainVO) itrDrainAssessments.next();
                                                            vecAssessments.add(assessmentDrainVO.getAssessment_id().intValue());
                                                        }
                                                    }
                                                    Collection<AssessmentIncisionVO> colIncisionAssessments = remoteAssessmentService.getIncisionAssessmentsForOffline(ab);
                                                    if (colIncisionAssessments != null) {
                                                        Iterator itrIncisionAssessments = colIncisionAssessments.iterator();
                                                        while (itrIncisionAssessments.hasNext()) {
                                                            AssessmentIncisionVO assessmentIncisionVO = (AssessmentIncisionVO) itrIncisionAssessments.next();
                                                            vecAssessments.add(assessmentIncisionVO.getAssessment_id().intValue());
                                                        }
                                                    }
                                                    Collection<AssessmentOstomyVO> colOstomyAssessments = remoteAssessmentService.getOstomyAssessmentsForOffline(ab);
                                                    if (colOstomyAssessments != null) {
                                                        Iterator itrOstomyAssessments = colOstomyAssessments.iterator();
                                                        while (itrOstomyAssessments.hasNext()) {
                                                            AssessmentOstomyVO assessmentOstomyVO = (AssessmentOstomyVO) itrOstomyAssessments.next();
                                                            vecAssessments.add(assessmentOstomyVO.getAssessment_id().intValue());
                                                        }
                                                    }
                                                    Collection<AssessmentSkinVO> colSkinAssessments = remoteAssessmentService.getSkinAssessmentsForOffline(ab);
                                                    if (colSkinAssessments != null) {
                                                        Iterator itrAssessments = colSkinAssessments.iterator();
                                                        while (itrAssessments.hasNext()) {
                                                            AssessmentSkinVO assessmentVO = (AssessmentSkinVO) itrAssessments.next();
                                                            vecAssessments.add(assessmentVO.getAssessment_id().intValue());
                                                        }
                                                    }
                                                    /*Collection<AssessmentBurnVO> colBurnAssessments = remoteAssessmentService.getBurnAssessmentsForOffline(ab);
                                                    if (colBurnAssessments != null) {
                                                        Iterator itrAssessments = colOstomyAssessments.iterator();
                                                        while (itrAssessments.hasNext()) {
                                                            AssessmentBurnVO assessmentVO = (AssessmentBurnVO) itrAssessments.next();
                                                            vecAssessments.add(assessmentVO.getAssessment_id().intValue());
                                                        }
                                                    }*/
                                                    for (int intAssessments = 0; intAssessments < vecAssessments.size(); intAssessments++) {
                                                        AbstractAssessmentVO abstractAssessment = remoteAssessmentService.retrieveAssessment((Integer) vecAssessments.get(intAssessments), eachVO.getId(), eachVO.getWound_profile_type().getAlpha_type());
                                                        if (abstractAssessment != null) {



                                                            WoundAssessmentVO assessVO = abstractAssessment.getWoundAssessment();

                                                            if (assessVO != null) {
                                                                assessVO.setOffline_flag(new Integer(0));
                                                                localWoundAssessmentService.saveAssessment(assessVO);
                                                                // abstractAssessment.setId(null);
                                                                AssessmentProductVO tp = new AssessmentProductVO();
                                                                tp.setAssessment_id(abstractAssessment.getAssessment_id());
                                                                tp.setAlpha_id(abstractAssessment.getAlpha_id());
                                                                Collection<AssessmentProductVO> products = remoteAssessmentService.getAllProducts(tp);
                                                                if (products != null) {
                                                                    for (AssessmentProductVO product : products) {
                                                                        localAssessmentService.saveProduct(product);
                                                                    }
                                                                }
                                                                localAssessmentService.saveAssessment(abstractAssessment);
                                                                AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                                                                at.setAssessment_id(abstractAssessment.getAssessment_id());
                                                                at.setAlpha_id(abstractAssessment.getAlpha_id());

                                                                AssessmentTreatmentVO c = remoteTcService.getAssessmentTreatment(at);
                                                                if (c != null) {
                                                                    localTcService.saveAssessmentTreatment(c);

                                                                    Collection<ArrayValueObject> arrays = c.getAssessment_treatment_arrays();
                                                                    if (arrays != null) {
                                                                        for (ArrayValueObject obj : arrays) {
                                                                            AssessmentTreatmentArraysVO atv = (AssessmentTreatmentArraysVO) obj;
                                                                            localTcService.saveTreatmentArray(atv);
                                                                        }
                                                                    }
                                                                    Collection<AssessmentAntibioticVO> antibiotics = c.getAntibiotics();
                                                                    if (antibiotics != null) {
                                                                        for (AssessmentAntibioticVO antibiotic : antibiotics) {
                                                                            localAssessmentService.saveAntibiotic(antibiotic);
                                                                        }
                                                                    }
                                                                }

                                                                AssessmentNPWTVO nptmp = new AssessmentNPWTVO();
                                                                nptmp.setAssessment_id(abstractAssessment.getAssessment_id());
                                                                AssessmentNPWTVO npwt = remoteNPWTService.getNegativePressure(nptmp);
                                                                if (npwt != null) {
                                                                    localNPWTService.saveNegativePressure(npwt);
                                                                    if(npwt.getAlphas()!=null){
                                                                        for (AssessmentNPWTAlphaVO alpha : npwt.getAlphas()) {
                                                                            localNPWTService.saveNegativePressureAlpha(alpha);
                                                                        }
                                                                    }
                                                                }
                                                                TreatmentCommentVO tctmp = new TreatmentCommentVO();
                                                                tctmp.setAssessment_id(abstractAssessment.getAssessment_id());
                                                                tctmp.setWound_profile_type_id(abstractAssessment.getWound_profile_type_id());
                                                                TreatmentCommentVO tc = remoteTcService.getTreatmentComment(tctmp);
                                                                if (tc != null) {
                                                                    localTcService.saveTreatmentComment(tc);
                                                                }
                                                                
                                                                if (abstractAssessment instanceof AssessmentEachwoundVO) {
                                                                    AssessmentEachwoundVO u = (AssessmentEachwoundVO) abstractAssessment;
                                                                    Collection<AssessmentUnderminingVO> undermining = u.getUnderminings();
                                                                    if (undermining != null) {
                                                                        for (AssessmentUnderminingVO au : undermining) {
                                                                            localAssessmentService.saveUndermining(au);
                                                                        }
                                                                    }
                                                                    Collection<AssessmentSinustractsVO> sinus = u.getSinustracts();
                                                                    if (sinus != null) {
                                                                        for (AssessmentSinustractsVO au : sinus) {

                                                                            localAssessmentService.saveSinustract(au);
                                                                        }
                                                                    }
                                                                    Collection<AssessmentWoundBedVO> m = u.getWoundbed();

                                                                    if (m != null) {
                                                                        for (AssessmentWoundBedVO au : m) {
                                                                            localAssessmentService.saveWoundBed(au);
                                                                        }
                                                                    }

                                                                }
                                                                if (abstractAssessment instanceof AssessmentOstomyVO) {
                                                                    AssessmentOstomyVO u = (AssessmentOstomyVO) abstractAssessment;
                                                                    Collection<AssessmentMucocSeperationsVO> s = u.getSeperations();
                                                                    if (s != null) {
                                                                        for (AssessmentMucocSeperationsVO au : s) {

                                                                            localAssessmentService.saveSeperation(au);
                                                                        }
                                                                    }


                                                                }
                                                                AssessmentImagesVO assessmentImages = new AssessmentImagesVO();
                                                                assessmentImages.setAssessment_id(assessVO.getId());
                                                                assessmentImages.setAlpha_id(eachVO.getId());
                                                                Vector<AssessmentImagesVO> images = remoteImagesService.getAllImageFiles(assessmentImages);
                                                                
                                                                if (images != null) {
                                                                  
                                                                    for (AssessmentImagesVO imgVO : images) {


                                                                        localImagesService.saveImage(imgVO);

                                                                    }
                                                                }
                                                                AssessmentCommentsVO assessmentComments = new AssessmentCommentsVO();
                                                                assessmentComments.setAssessment_id(assessVO.getId());
                                                                Collection<AssessmentCommentsVO> comments = remoteCommentsService.getAllComments(assessmentComments);
                                                                if (comments != null) {
                                                                    for (AssessmentCommentsVO comment : comments) {
                                                                        localCommentsService.saveComment(comment);
                                                                    }
                                                                }

                                                            }

                                                        }

                                                    }
                                                    NursingCarePlanVO ncptmp = new NursingCarePlanVO();
                                                    ncptmp.setWound_id(eachVO.getWound_id());
                                                    ncptmp.setWound_profile_type_id(eachVO.getWound_profile_type_id());
                                                    NursingCarePlanVO ncp = remoteNcpService.getNursingCarePlan(ncptmp);
                                                    if(ncp!=null && ncp.getBody()!=null){
                                                        ncp.setBody(Common.convertCarriageReturns(ncp.getBody(),false));
                                                    }
                                                    localNcpService.saveNursingCarePlan(ncp);

                                                    DressingChangeFrequencyVO dcftmp = new DressingChangeFrequencyVO();
                                                    dcftmp.setAlpha_id(eachVO.getId());
                                                    
                                                        DressingChangeFrequencyVO dcf = remoteNcpService.getDressingChangeFrequency(dcftmp);
                                                        localNcpService.saveDressingChangeFrequency(dcf);
                                                    
                                                }
                                            }
                                            Collection<EtiologyVO> earrays = woundVO.getEtiologies();
                                            Collection<WoundAcquiredVO> warrays = woundVO.getWound_acquired();
                                            Collection<GoalsVO> garrays = woundVO.getGoals();
                                            if (earrays != null) {
                                                for (EtiologyVO e : earrays) {
                                                    localWoundService.saveEtiology(e);
                                                }
                                            }
                                            if (warrays != null) {
                                                for (WoundAcquiredVO e : warrays) {
                                                    localWoundService.saveAcquired(e);
                                                }
                                            }
                                            if (garrays != null) {
                                                for (GoalsVO g : garrays) {
                                                    localWoundService.saveGoal(g);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (error == false) {
                            patient_status = patient_status + "Patient " + ((String) request.getParameter("patient" + (x + 1))) + "  was downloaded.<br/>";

                        }
                    } else {
                        if (!search.equals("")) {
                            error = true;
                            patient_status = patient_status + "Patient " + ((String) request.getParameter("patient" + (x + 1))) + " must be a valid number.<br/>";

                            session.setAttribute("patientsync_fail", "1");

                        }
                    }


                }
                session.setAttribute("patientsync", patient_status);
                session.setAttribute("syncsuccess", "1");

                if (!Common.exceptionsMsg.isEmpty()) {
                    saveErrors(request, Common.exceptionsMsg);
                    Common.exceptionsMsg = new ActionErrors();

                    return (mapping.findForward("sync.failure"));
                } else {
                    System.out.println("Redirecting to Login");
                    return (mapping.findForward("sync.success"));
                }

            } catch (ApplicationException e) {
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            } catch (Exception e) {
                e.printStackTrace();
                ActionErrors errors = new ActionErrors();
                String[] info = {com.pixalere.utils.Common.buildException(e)};
                errors.add("exception", new ActionMessage("pixalere.common.error2", info));
                saveErrors(request, errors);
            }
        }
        return (mapping.findForward("sync.failure"));
    }

    private boolean isNumber(String n) {
        try {
            double d = Double.valueOf(n).doubleValue();
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
