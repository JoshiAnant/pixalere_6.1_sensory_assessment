/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.login;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author travismorris
 */
public class UploadForm extends org.apache.struts.action.ActionForm {
    
   private String[] uploadlist;


   /**
    *
    */
   public UploadForm() {
       super();
       // TODO Auto-generated constructor stub
   }

   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
       ActionErrors errors = new ActionErrors();
       
       return errors;
   }

    public String[] getUploadlist() {
        return uploadlist;
    }

    public void setUploadlist(String[] uploadlist) {
        this.uploadlist = uploadlist;
    }
}
