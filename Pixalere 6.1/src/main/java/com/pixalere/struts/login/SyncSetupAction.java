package com.pixalere.struts.login;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMapping;
public class SyncSetupAction extends Action {

  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SyncSetupAction.class);

       public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
				log.info("SyncSetupAction.perform():  Redirecting to sync.vm");
                                HttpSession session = request.getSession();
                                if(session!=null){
                                    session.removeAttribute("syncsuccess");
                                    session.removeAttribute("patientsync");
                                }
				
				return (mapping.findForward("sync.success"));
		  
	}
}
