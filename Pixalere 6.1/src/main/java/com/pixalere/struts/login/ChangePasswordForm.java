/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.login;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.Common;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.MD5;
/**
/**
 *
 * @author travis
 */
public class ChangePasswordForm  extends ActionForm {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangePasswordForm.class);
    private Integer id;
    private String token;
    private String newpassword;
    private String confirmpassword;
    private Integer reset_password_method;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        Common common = new Common();
        try {
            //return as success regardless of username existing or not.  This way we won't have someone guessing usernames.
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            ProfessionalVO ptmp = new ProfessionalVO();
            ptmp.setId(getId());
            ProfessionalVO retrieved_prof = pservice.getProfessional(ptmp);
            
        }catch (ApplicationException e) {
            errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));

        }
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setId(null);
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the newpassword
     */
    public String getNewpassword() {
        return newpassword;
    }

    /**
     * @param newpassword the newpassword to set
     */
    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    /**
     * @return the confirmpassword
     */
    public String getConfirmpassword() {
        return confirmpassword;
    }

    /**
     * @param confirmpassword the confirmpassword to set
     */
    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }

    /**
     * @return the reset_password_method
     */
    public Integer getReset_password_method() {
        return reset_password_method;
    }

    /**
     * @param reset_password_method the reset_password_method to set
     */
    public void setReset_password_method(Integer reset_password_method) {
        this.reset_password_method = reset_password_method;
    }

}