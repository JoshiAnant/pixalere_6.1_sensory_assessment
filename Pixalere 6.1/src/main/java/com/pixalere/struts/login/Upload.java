package com.pixalere.struts.login;

import java.util.Collection;
import java.util.Vector;
import org.apache.struts.action.ActionError;
import com.pixalere.common.ArrayValueObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import java.util.Hashtable;
import com.pixalere.assessment.bean.AssessmentAntibioticVO;
import com.pixalere.assessment.service.*;
import com.pixalere.assessment.client.*;
import com.pixalere.assessment.bean.*;
import com.pixalere.common.bean.OfflineVO;
import com.pixalere.common.bean.*;
import com.pixalere.common.client.*;
import com.pixalere.common.service.*;
import com.pixalere.patient.bean.*;
import com.pixalere.patient.service.*;
import com.pixalere.patient.client.*;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import com.pixalere.wound.bean.*;
import com.pixalere.wound.client.*;

import com.pixalere.wound.service.*;
import java.util.TimeZone;

import javax.activation.*;

public class Upload extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Upload.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        UploadForm uploadForm = (UploadForm) form;
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        if(userVO == null){
             request.setAttribute("session_invalid", "yes");
            return (mapping.findForward("system.logoff"));
        }
        String[] items = uploadForm.getUploadlist();
        //DownloadService dwn=new DownloadService();
        Common common = new Common();
        String absolute_path = Common.getPhotosPath();

        //Setup local and remote services.
        AssessmentServiceImpl localAssessService = new AssessmentServiceImpl(language);
        PatientProfileServiceImpl localProfileService = new PatientProfileServiceImpl(language);
        AssessmentClient ac = new AssessmentClient();
        AssessmentService remoteAssessmentService = ac.createAssessmentClient();
        //setup remote services.
        ConfigurationClient cc = new ConfigurationClient();
        ConfigurationService remoteConfigurationService = cc.createConfigurationClient();
        AssessmentNPWTClient an = new AssessmentNPWTClient();
        AssessmentNPWTService remoteNPWTService = an.createNPWTClient();
        AssessmentNPWTServiceImpl localNPWTService = new AssessmentNPWTServiceImpl();

        PatientProfileClient pc = new PatientProfileClient();
        PatientProfileService remotePatientProfileService = pc.createPatientProfileClient();


        PatientServiceImpl localPatientService = new PatientServiceImpl(language);

        WoundAssessmentLocationServiceImpl localAlphaService = new WoundAssessmentLocationServiceImpl();
        WoundAssessmentLocationClient alc = new WoundAssessmentLocationClient();
        WoundAssessmentLocationService remoteAlphaService = alc.createWoundAssessmentLocationClient();


        WoundAssessmentServiceImpl localWoundAssessService = new WoundAssessmentServiceImpl();
        WoundAssessmentClient walc = new WoundAssessmentClient();
        WoundAssessmentService remoteWoundAssessService = walc.createWoundAssessmentClient();


        TreatmentCommentsServiceImpl localTreatmentService = new TreatmentCommentsServiceImpl();
        TreatmentCommentsClient tlc = new TreatmentCommentsClient();
        TreatmentCommentsService remoteTreatmentService = tlc.createTreatmentCommentsClient();


        NursingCarePlanServiceImpl localNursingCarePlanService = new NursingCarePlanServiceImpl();
        NursingCarePlanClient nlc = new NursingCarePlanClient();
        NursingCarePlanService remoteNursingCarePlanService = nlc.createNursingCarePlanClient();


        AssessmentCommentsServiceImpl localAssessmentCommentsService = new AssessmentCommentsServiceImpl();
        AssessmentCommentsClient aclc = new AssessmentCommentsClient();
        AssessmentCommentsService remoteAssessmentCommentsService = aclc.createAssessmentCommentsClient();


        AssessmentImagesServiceImpl localAssessmentImagesService = new AssessmentImagesServiceImpl();
        AssessmentImagesClient ailc = new AssessmentImagesClient();
        AssessmentImagesService remoteAssessmentImagesService = ailc.createAssessmentImagesClient();


        ReferralsTrackingServiceImpl localReferralsService = new ReferralsTrackingServiceImpl(language);
        ReferralsTrackingClient reflc = new ReferralsTrackingClient();
        ReferralsTrackingService remoteReferralsService = reflc.createReferralsTrackingClient();


        WoundServiceImpl localWoundProfileService = new WoundServiceImpl(language);
        WoundClient wplc = new WoundClient();
        WoundService remoteWoundProfileService = wplc.createWoundClient();
        try{
            String serverZone = remoteConfigurationService.getServerTimeZone();
                
                TimeZone.setDefault(TimeZone.getTimeZone(serverZone));
        }catch(Exception e){log.error(e.getMessage());}
        //End setup of local and remote services.

        try {

            if (items != null) {
                for (int x = 0; x < items.length; x++) {
                    OfflineVO offline = new OfflineVO();
                    String item = items[x];
                    offline.setPatient_id(new Integer(item.trim()));

                    Vector<OfflineVO> coll2 = localProfileService.getAllOffline(offline, (ProfessionalVO) session.getAttribute("userVO"), true);

                    String wound_id = "";
                    String assessment_id = "";
                    //String alpha_id = "";
                    String patient_id = "";

                    
                    Hashtable wound_ids = new Hashtable();
                    for (OfflineVO off : coll2) {
                        if (off.getPatient_id() != null) {
                            log.info("Upload.perform():  OfflineVO is not NULL");
                            PatientProfileVO tmp = new PatientProfileVO();
                            log.info("Upload.perform():  Patient is " + off.getPatient_id());
                            tmp.setPatient_id(off.getPatient_id());
                            patient_id = off.getPatient_id() + "";
                            tmp.setCurrent_flag(new Integer(1));
                            PatientProfileVO vo2 = (PatientProfileVO) localProfileService.getPatientProfile(tmp);
                            
                            if (vo2 != null && off.getPatientprofile_changed() != null && off.getPatientprofile_changed().equals(new Integer(1))) {
                                
                                vo2.setId(null);
                                remotePatientProfileService.updatePatientProfile(vo2);
                                PatientProfileVO t = remotePatientProfileService.getPatientProfile(vo2);
                                if(t!=null){
                                    Collection<PatientProfileArraysVO> pparrays = vo2.getPatient_profile_arrays();
                                    for (PatientProfileArraysVO array : pparrays) {
                                                                                array.setId(null);
                                        array.setPatient_profile_id(t.getId());
                                        remotePatientProfileService.savePatientProfileArray(array);
                                    }
                                    Collection<ExternalReferralVO> external_refs = vo2.getExternalReferrals();
                                    for(ExternalReferralVO ext : external_refs)
                                    {
                                        ext.setId(null);
                                        ext.setPatient_profile_id(t.getId());
                                        remotePatientProfileService.saveExternalReferral(ext);
                                    }
                                }
                            }
                            DiagnosticInvestigationVO dtmp = new DiagnosticInvestigationVO();
                            log.info("Upload.perform():  Patient is " + off.getPatient_id());
                            dtmp.setPatient_id(off.getPatient_id());
                            patient_id = off.getPatient_id() + "";
                            dtmp.setActive(new Integer(1));
                            DiagnosticInvestigationVO vo25 = (DiagnosticInvestigationVO) localProfileService.getDiagnosticInvestigation(dtmp);
                            
                            if (vo25 != null && off.getDiagnostic_changed() != null && off.getDiagnostic_changed().equals(new Integer(1))) {
                                vo25.setId(null);
                                remotePatientProfileService.saveDiagnosticInvestigation(vo25);
                                DiagnosticInvestigationVO newD = new DiagnosticInvestigationVO();
                                newD.setPatient_id(vo25.getPatient_id());
                                newD.setVisited_on(vo25.getVisited_on());
                                newD.setActive(1);
                                DiagnosticInvestigationVO t = remotePatientProfileService.getDiagnosticInvestigation(newD);
                                
                                Collection<InvestigationsVO> investigations = vo25.getInvestigations();
                                if(t!=null){
                                    for (InvestigationsVO i : investigations) {
                                        i.setId(null);
                                        i.setDiagnostic_id(t.getId());
                                        remotePatientProfileService.saveInvestigation(i);
                                    }
                                }
                            }
                            BradenVO b1 = new BradenVO();
                            b1.setPatient_id(new Integer(patient_id));
                            b1.setActive(1);
                            b1.setCurrent_flag(1);
                            BradenVO braden = (BradenVO) localProfileService.getBraden(b1);
                            if (braden != null && off.getBraden_changed() != null && off.getBraden_changed().equals(new Integer(1))) {
                                braden.setId(null);
                                remotePatientProfileService.saveBraden(braden);
                            }
                            PhysicalExamVO x1 = new PhysicalExamVO();
                            x1.setPatient_id(new Integer(patient_id));
                            x1.setActive(1);
                            x1.setCurrent_flag(1);
                            PhysicalExamVO exam = (PhysicalExamVO) localProfileService.getExam(x1);
                            
                            if (exam != null && off.getExam_changed() != null && off.getExam_changed().equals(new Integer(1))) {
                                exam.setId(null);
                                remotePatientProfileService.savePhysicalExam(exam);
                            }
                            FootAssessmentVO f1 = new FootAssessmentVO();
                            f1.setPatient_id(new Integer(patient_id));
                            f1.setActive(1);
                            FootAssessmentVO foot = (FootAssessmentVO) localProfileService.getFootAssessment(f1);
                            if (foot != null && off.getFoot_changed() != null && off.getFoot_changed().equals(new Integer("1"))) {
                                foot.setId(null);
                                remotePatientProfileService.saveFoot(foot);
                            }
                            LimbAdvAssessmentVO l1 = new LimbAdvAssessmentVO();
                            l1.setPatient_id(new Integer(patient_id));
                            l1.setActive(1);
                            LimbAdvAssessmentVO limb = (LimbAdvAssessmentVO) localProfileService.getLimbAdvAssessment(l1);
                            
                            if (limb != null && off.getLimb_adv_changed() != null && off.getLimb_adv_changed().equals(new Integer("1"))) {
                                limb.setId(null);
                                remotePatientProfileService.saveAdvLimb(limb);
                            }
                            LimbBasicAssessmentVO l1b = new LimbBasicAssessmentVO();
                            l1b.setPatient_id(new Integer(patient_id));
                            l1b.setActive(1);
                            LimbBasicAssessmentVO limbb = (LimbBasicAssessmentVO) localProfileService.getLimbBasicAssessment(l1b);
                            if (limbb != null && off.getLimb_changed() != null && off.getLimb_changed().equals(new Integer("1"))) {
                                limbb.setId(null);
                                remotePatientProfileService.saveBasicLimb(limbb);
                            }
                            LimbUpperAssessmentVO lub = new LimbUpperAssessmentVO();
                            lub.setPatient_id(new Integer(patient_id));
                            lub.setActive(1);
                            LimbUpperAssessmentVO limbu = (LimbUpperAssessmentVO) localProfileService.getLimbUpperAssessment(lub);
                            
                            if (limbu != null && off.getLimb_upper_changed() != null && off.getLimb_upper_changed().equals(new Integer("1"))) {
                                limbu.setId(null);
                                remotePatientProfileService.saveUpperLimb(limbu);
                            }
                        }
                        Hashtable alpha_ids = new Hashtable();
                        Hashtable wound_profile_type_ids = new Hashtable();
                        //check if wound profile was completed
                        if (off.getWound_id() != null) {

                            WoundProfileVO wvo = new WoundProfileVO();
                            wvo.setPatient_id(off.getPatient_id());
                            wvo.setWound_id(off.getWound_id());
                            wvo.setActive(1);
                            wvo.setCurrent_flag(1);
                            WoundProfileVO vo = (WoundProfileVO) localWoundProfileService.getWoundProfile(wvo);
                            Collection<GoalsVO> goals = vo.getGoals();
                            Collection<EtiologyVO> etiologies = vo.getEtiologies();

                            Collection<WoundAcquiredVO> acquired = vo.getWound_acquired();

                            WoundVO wcrit = new WoundVO();
                            wcrit.setId(off.getWound_id());
                            wcrit.setPatient_id(off.getPatient_id());

                            wcrit.setActive(1);
                            WoundVO woundvo = (WoundVO) localWoundProfileService.getWound(wcrit);
                            wound_id = "";

                            if (vo != null) {
                                patient_id = vo.getPatient_id() + "";
                                vo.setId(null);//make wound profile id null to insert new record
                                wcrit.setStartdate_signature(woundvo.getStartdate_signature());
                                wcrit.setWound_location(woundvo.getWound_location());
                                //System.out.println(woundvo.getStart_date()+" start_date:Offline:location "+woundvo.getWound_location());
                                wcrit.setId(null);
                                WoundVO checkForNew = remoteWoundProfileService.getWound(wcrit);
                                
                                if (checkForNew == null) {

                                    vo.setWound_id(null);
                                    woundvo.setId(null);
                                    remoteWoundProfileService.saveWound(woundvo);
                                    //Get the ID of the Wound
                                    WoundVO t = remoteWoundProfileService.getWound(woundvo);
                                    if (t != null) {
                                        wound_id = t.getId() + "";
                                        wound_ids.put(off.getWound_id() + "", wound_id);

                                        vo.setWound_id(new Integer(wound_id));
                                    } else {
                                    }
                                } else {
                                    wound_id = checkForNew.getId() + "";
                                    if (woundvo != null && woundvo.getStatus() != null && woundvo.getStatus().equals("Closed")) {
                                        remoteWoundProfileService.saveWound(woundvo);
                                    }
                                }

                                if(off.getWound_changed()!=null && off.getWound_changed().equals(1)){
                                    //if changed
                                    vo.setWound_id(new Integer(wound_id));
                                    remoteWoundProfileService.updateWoundProfile(vo);
                                }

                            }

                            //get all local wound assesment location alphas.
                            Vector<WoundAssessmentLocationVO> coll = null;
                            if (off.getWound_id() != null && !wound_id.equals("")) {

                                WoundAssessmentLocationVO wavo = new WoundAssessmentLocationVO();
                                wavo.setWound_id(off.getWound_id());
                                wavo.setActive(new Integer(1));
                                //wavo.setDischarge(new Integer(1));
                                coll = localAlphaService.getAllAlphas(wavo, false);
                                wavo.setWound_id(new Integer(wound_id));


                                for (WoundAssessmentLocationVO alpha : coll) {
                                    wavo.setAlpha(alpha.getAlpha());
                                    wavo.setWound_id(new Integer(wound_id));
                                    //wavo.setOpen_timestamp(alpha.getOpen_timestamp());
                                    WoundAssessmentLocationVO remoteAlpha = remoteAlphaService.getAlpha(wavo);
                                    WoundProfileTypeVO wptTMP = new WoundProfileTypeVO();
                                    wptTMP.setId(alpha.getWound_profile_type_id());
                                    WoundProfileTypeVO localWPT = localWoundProfileService.getWoundProfileType(wptTMP);

                                    WoundProfileTypeVO wptCrit = new WoundProfileTypeVO();
                                    wptCrit.setAlpha_type(localWPT.getAlpha_type());
                                    wptCrit.setWound_id(new Integer(wound_id));
                                    WoundProfileTypeVO remoteWPT = remoteWoundProfileService.getWoundProfileType(wptCrit);
                                    int wound_profile_type_id = -1;
                                    int tmpWPT = localWPT.getId();
                                    if (remoteWPT == null) {
                                        localWPT.setId(null);
                                        localWPT.setWound_id(new Integer(wound_id));
                                        remoteWoundProfileService.saveWoundProfileType(localWPT);
                                        WoundProfileTypeVO lat = remoteWoundProfileService.getWoundProfileType(localWPT);
                                        wound_profile_type_id = lat.getId().intValue();
                                        wound_profile_type_ids.put(tmpWPT + "", wound_profile_type_id);
                                    } else {
                                        localWPT.setId(remoteWPT.getId());
                                        localWPT.setWound_id(remoteWPT.getWound_id());
                                        remoteWoundProfileService.saveWoundProfileType(localWPT);
                                        wound_profile_type_id = remoteWPT.getId().intValue();
                                        wound_profile_type_ids.put(tmpWPT + "", wound_profile_type_id);
                                    }
                                    alpha.setWound_profile_type_id(wound_profile_type_id);
                                    alpha.setWound_id(new Integer(wound_id));
                                    //check if current alpha is already in Live.
                                    String aid = alpha.getId() + "";
                                    if (remoteAlpha == null) {
                                        alpha.setId(null);

                                        remoteAlphaService.saveAlpha(alpha);
                                        WoundAssessmentLocationVO atemp = remoteAlphaService.getAlpha(alpha);
                                        alpha_ids.put(aid, atemp.getId() + "");//store real alpha_id to use later.
                                        WoundLocationDetailsVO d = new WoundLocationDetailsVO();
                                        d.setAlpha_id(new Integer(aid));
                                        Collection<WoundLocationDetailsVO> details = localAlphaService.getAllAlphaDetails(d);
                                        for (WoundLocationDetailsVO detail : details) {
                                            detail.setId(null);
                                            detail.setAlpha_id(atemp.getId());
                                            remoteAlphaService.saveAlphaDetail(detail);
                                        }
                                    } else {//not new
                                        alpha.setId(remoteAlpha.getId());
                                        alpha.setWound_id(remoteAlpha.getWound_id());
                                        alpha.setWound_profile_type_id(remoteAlpha.getWound_profile_type_id());
                                        remoteAlphaService.saveAlpha(alpha);
                                        alpha_ids.put(aid, remoteAlpha.getId());
                                        WoundLocationDetailsVO[] details = alpha.getAlpha_details();
                                        for (WoundLocationDetailsVO detail : details) {
                                            detail.setAlpha_id(remoteAlpha.getId());
                                            remoteAlphaService.saveAlphaDetail(detail);
                                        }

                                    }//else
                                }//for
                            }//if
                            if (off.getWound_changed() != null && off.getWound_changed().equals(1)) {
                                //for
                                WoundProfileVO wpNewTMP = new WoundProfileVO();
                                wpNewTMP.setCreated_on(vo.getCreated_on());
                                wpNewTMP.setWound_id(vo.getWound_id());
                                wpNewTMP.setProfessional_id(vo.getProfessional_id());
                                WoundProfileVO wpNew = remoteWoundProfileService.getWoundProfile(wpNewTMP);
                                if(wpNew!=null){
                                    for (GoalsVO goal : goals) {
                                        java.util.Enumeration enumer = alpha_ids.keys();
                                        while (enumer.hasMoreElements()) {
                                            String aid = enumer.nextElement() + "";

                                        }
                                        String alp_id = alpha_ids.get(goal.getAlpha_id() + "") + "";
                                        goal.setWound_profile_id(wpNew.getId());
                                        goal.setId(null);
                                        goal.setAlpha_id(new Integer(alp_id));
                                        remoteWoundProfileService.saveGoal(goal);

                                    }
                                    for (EtiologyVO etiology : etiologies) {
                                        String alp_id = alpha_ids.get(etiology.getAlpha_id() + "") + "";
                                        etiology.setId(null);
                                        etiology.setWound_profile_id(wpNew.getId());
                                        etiology.setAlpha_id(new Integer(alp_id));
                                        remoteWoundProfileService.saveEtiology(etiology);
                                    }
                                    for (WoundAcquiredVO acq : acquired) {
                                        String alp_id = alpha_ids.get(acq.getAlpha_id() + "") + "";
                                        acq.setId(null);
                                        acq.setWound_profile_id(wpNew.getId());
                                        acq.setAlpha_id(new Integer(alp_id));
                                        remoteWoundProfileService.saveAcquired(acq);
                                    }
                                }
                            }
                        }//if

                        if (off.getAssessment_id() != null && wound_id != "" && off.getAssessment_id() != -1) {
                            WoundAssessmentVO wa = new WoundAssessmentVO();
                            wa.setId(off.getAssessment_id());
                            wa.setActive(1);
                            
                            WoundAssessmentVO vo = localWoundAssessService.getAssessment(wa);
                            
                            if (vo != null) {
                                patient_id = vo.getPatient_id() + "";
                                vo.setId(null);
                                vo.setWound_id(new Integer(wound_id));

                                remoteWoundAssessService.saveAssessment(vo);
                                WoundAssessmentVO woundass = remoteWoundAssessService.getAssessment(vo);
                                assessment_id = woundass.getId() + "";
                                Collection<AbstractAssessmentVO> assessments = localAssessService.getAllAssessments(off.getAssessment_id().intValue());
                                //Vector assess = new Vector();

                                AssessmentNPWTVO npwttmp = new AssessmentNPWTVO();
                                npwttmp.setWound_id(off.getWound_id());
                                npwttmp.setAssessment_id(off.getAssessment_id());
                                Collection<AssessmentNPWTVO> nps = localNPWTService.getAllNegativePressure(npwttmp, -1);
                                for (AssessmentNPWTVO ncp : nps) {
                                    ncp.setWound_id(new Integer(wound_id));
                                    ncp.setAssessment_id(new Integer(assessment_id));
                                    ncp.setId(null);

                                    remoteNPWTService.saveNegativePressure(ncp);
                                    AssessmentNPWTVO npwtoto = remoteNPWTService.getNegativePressure(ncp);
                                    if (ncp.getAlphas() != null && npwtoto != null) {
                                        for (AssessmentNPWTAlphaVO alpha : ncp.getAlphas()) {
                                            alpha.setAssessment_npwt_id(npwtoto.getId());
                                            remoteNPWTService.saveNegativePressureAlpha(alpha);
                                        }
                                    }
                                }

                                NursingCarePlanVO ncptmp = new NursingCarePlanVO();
                                ncptmp.setWound_id(off.getWound_id());
                                ncptmp.setAssessment_id(off.getAssessment_id());
                                Collection<NursingCarePlanVO> ncps = localNursingCarePlanService.getAllNursingCarePlans(ncptmp, -1);
                                for (NursingCarePlanVO ncp : ncps) {
                                    ncp.setWound_id(new Integer(wound_id));
                                    ncp.setAssessment_id(new Integer(assessment_id));
                                    ncp.setId(null);
                                    String wpt = wound_profile_type_ids.get(ncp.getWound_profile_type_id() + "") + "";
                                    ncp.setWound_profile_type_id(new Integer(wpt));
                                    remoteNursingCarePlanService.saveNursingCarePlan(ncp);
                                }
                                ReferralsTrackingVO tmpRefVO = new ReferralsTrackingVO();
                                tmpRefVO.setAssessment_id(off.getAssessment_id());
                                Collection<ReferralsTrackingVO> refs = localReferralsService.getAllReferralsByCriteria(tmpRefVO);
                                Hashtable refs_hash = new Hashtable();
                                for (ReferralsTrackingVO refvo : refs) {
                                    if (refvo != null) {
                                        refvo.setId(null);
                                        refvo.setWound_id(new Integer(wound_id));
                                        refvo.setAssessment_id(new Integer(assessment_id));
                                        String wpt = wound_profile_type_ids.get(refvo.getWound_profile_type_id() + "") + "";
                                        refvo.setWound_profile_type_id(new Integer(wpt));
                                        remoteReferralsService.saveReferralsTracking(refvo);
                                        ReferralsTrackingVO ref = remoteReferralsService.getReferralsTracking(refvo);
                                        if(ref!=null){
                                            refs_hash.put(wpt, ref.getId());
                                        }
                                    }
                                }
                                AssessmentCommentsVO actmp = new AssessmentCommentsVO();
                                actmp.setWound_id(off.getWound_id());
                                actmp.setAssessment_id(off.getAssessment_id());
                                Collection<AssessmentCommentsVO> comments = localAssessmentCommentsService.getAllComments(actmp);
                                for (AssessmentCommentsVO comment : comments) {
                                    try {
                                        if (comment != null) {
                                            comment.setId(null);
                                            comment.setWound_id(new Integer(wound_id));
                                            comment.setAssessment_id(new Integer(assessment_id));
                                            String wpt = wound_profile_type_ids.get(comment.getWound_profile_type_id() + "") + "";
                                            comment.setWound_profile_type_id(new Integer(wpt));
                                            comment.setReferral_id((Integer) refs_hash.get(wpt));
                                            remoteAssessmentCommentsService.saveComment(comment);
                                        }
                                    } catch (NumberFormatException e) {
                                    }
                                }
                                TreatmentCommentVO ttmp = new TreatmentCommentVO();
                                ttmp.setWound_id(off.getWound_id());
                                ttmp.setAssessment_id(off.getAssessment_id());
                                Collection<TreatmentCommentVO> tcomments = localTreatmentService.getAllTreatmentComments(ttmp, -1);
                                for (TreatmentCommentVO comment : tcomments) {
                                    if (comment != null) {
                                        comment.setId(null);
                                        comment.setWound_id(new Integer(wound_id));
                                        comment.setAssessment_id(new Integer(assessment_id));
                                        String wpt = wound_profile_type_ids.get(comment.getWound_profile_type_id() + "") + "";
                                        comment.setWound_profile_type_id(new Integer(wpt));
                                        remoteTreatmentService.saveTreatmentComment(comment);
                                    }
                                }

                                AssessmentProductVO pv = new AssessmentProductVO();
                                pv.setAssessment_id(off.getAssessment_id());


                                int wpt_autoreferral = 0;
                                for (AbstractAssessmentVO vo2 : assessments) {
                                    AssessmentProductVO tp = new AssessmentProductVO();
                                    tp.setAssessment_id(vo2.getAssessment_id());
                                    tp.setAlpha_id(vo2.getAlpha_id());
                                    Collection<AssessmentProductVO> products = localAssessService.getAllProducts(tp);
                                    String alpha_id = (alpha_ids.get(vo2.getAlpha_id() + "") + "");
                                    String wpt = wound_profile_type_ids.get(vo2.getWound_profile_type_id() + "") + "";
                                    DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                                    d.setAssessment_id(off.getAssessment_id());
                                    d.setAlpha_id(vo2.getAlpha_id());
                                    DressingChangeFrequencyVO dcf = localNursingCarePlanService.getDressingChangeFrequency(d);
                                    if (dcf != null) {
                                        dcf.setAlpha_id(new Integer(alpha_id));
                                        dcf.setAssessment_id(new Integer(assessment_id));
                                        dcf.setWound_id(new Integer(wound_id));
                                        dcf.setWound_profile_type_id(new Integer(wpt));
                                        dcf.setId(null);
                                        remoteNursingCarePlanService.saveDressingChangeFrequency(dcf);
                                    }
                                    AssessmentTreatmentVO atmp = new AssessmentTreatmentVO();
                                    atmp.setAssessment_id(off.getAssessment_id());
                                    atmp.setAlpha_id(vo2.getAlpha_id());
                                    AssessmentTreatmentVO atreat = localTreatmentService.getAssessmentTreatment(atmp);
                                    if (atreat != null) {
                                        atreat.setAssessment_id(new Integer(assessment_id));
                                        atreat.setWound_profile_type_id(new Integer(wpt));
                                        atreat.setAlpha_id(new Integer(alpha_id));
                                        atreat.setId(null);
                                        remoteTreatmentService.saveAssessmentTreatment(atreat);
                                       
                                        AssessmentTreatmentVO remoteAT = remoteTreatmentService.getAssessmentTreatment(atreat);
                                        

                                        Collection<ArrayValueObject> atreatarray = atreat.getAssessment_treatment_arrays();
                                        
                                        for (ArrayValueObject arra : atreatarray) {
                                            AssessmentTreatmentArraysVO array = (AssessmentTreatmentArraysVO)arra;
                                            array.setAssessment_treatment_id(remoteAT.getId());
                                            array.setAlpha_id(new Integer(alpha_id));
                                            array.setId(null);
                                            remoteTreatmentService.saveTreatmentArray(array);
                                        }
                                       Collection<AssessmentAntibioticVO> antis= atreat.getAntibiotics();
                                       if(antis !=null){
                                           for(AssessmentAntibioticVO a : antis){
                                               a.setAssessment_treatment_id(remoteAT.getId());
                                               a.setId(null);
                                               remoteAssessmentService.saveAntibiotic(a);
                                           }
                                       }
                                       
                                    }
                                    AssessmentImagesVO imgCrit = new AssessmentImagesVO();
                                    imgCrit.setAssessment_id(off.getAssessment_id());
                                    imgCrit.setAlpha_id(vo2.getAlpha_id());

                                    Collection<AssessmentImagesVO> images = localAssessmentImagesService.getAllImages(imgCrit);
                                    //Integer local_alpha = vo2.getAlpha_id();

                                    vo2.setAlpha_id(new Integer(alpha_id));
                                    vo2.setId(null);
                                    vo2.setAssessment_id(new Integer(assessment_id));
                                    vo2.setWound_id(new Integer(wound_id));
                                    vo2.setWound_profile_type_id(new Integer(wpt));
                                    for (AssessmentImagesVO image : images) {
                                        //assess.add(vo2);
                                        Common.imagesFailedUpload = false;
                                        if (image != null) {

                                            image.setId(null);
                                            image.setWound_id(new Integer(wound_id));
                                            image.setWound_profile_type_id(new Integer(wpt));
                                            image.setAlpha_id(new Integer(alpha_id));
                                            image.setWhichImage(image.getWhichImage());
                                            if (Common.imagesFailedUpload == true) {
                                                image.setImages("a:0:{}");
                                            }
                                            String filename = image.getImages();

                                            if (filename != null && !filename.equals("")) {
                                                DataSource ds = new FileDataSource(new File(Common.getPhotosPath() + "/" + image.getPatient_id() + "/" + image.getAssessment_id() + "/" + filename));
                                                image.setImagefile(new DataHandler(ds));
                                            }
                                            image.setAssessment_id(new Integer(assessment_id));

                                            remoteAssessmentImagesService.saveImage(image);

                                        }
                                    }


                                    remoteAssessmentService.saveAssessment(vo2);

                                    AbstractAssessmentVO justsaved = null;
                                    if (vo2 instanceof AssessmentEachwoundVO) {
                                        AssessmentEachwoundVO tjustsaved = new AssessmentEachwoundVO();
                                        tjustsaved.setAssessment_id(new Integer(assessment_id));
                                        tjustsaved.setAlpha_id(new Integer(alpha_id));
                                        justsaved = remoteAssessmentService.getAssessment(tjustsaved);
                                        if (justsaved != null) {
                                            AssessmentEachwoundVO ass2 = (AssessmentEachwoundVO) vo2;
                                            Collection<AssessmentUnderminingVO> u = ass2.getUnderminings();
                                            for (AssessmentUnderminingVO tmpA : u) {
                                                tmpA.setAssessment_wound_id(justsaved.getId());
                                                tmpA.setAlpha_id(new Integer(alpha_id));
                                                tmpA.setId(null);
                                                remoteAssessmentService.saveUndermining(tmpA);
                                            }
                                            Collection<AssessmentSinustractsVO> s = ass2.getSinustracts();
                                            for (AssessmentSinustractsVO tmpA : s) {
                                                tmpA.setAssessment_wound_id(justsaved.getId());
                                                tmpA.setAlpha_id(new Integer(alpha_id));
                                                tmpA.setId(null);
                                                remoteAssessmentService.saveSinustract(tmpA);
                                            }
                                            Collection<AssessmentWoundBedVO> wb = ass2.getWoundbed();
                                            for (AssessmentWoundBedVO tmpA : wb) {
                                                tmpA.setAssessment_wound_id(justsaved.getId());
                                                tmpA.setAlpha_id(new Integer(alpha_id));
                                                tmpA.setId(null);
                                                remoteAssessmentService.saveWoundBed(tmpA);
                                            }

                                        }
                                        wpt_autoreferral = new Integer(wpt);
                                    } else if (vo2 instanceof AssessmentOstomyVO) {
                                        AssessmentOstomyVO tjustsaved = new AssessmentOstomyVO();
                                        tjustsaved.setAssessment_id(new Integer(assessment_id));
                                        tjustsaved.setAlpha_id(new Integer(alpha_id));
                                        justsaved = remoteAssessmentService.getAssessment(tjustsaved);

                                        if (justsaved != null) {
                                            AssessmentOstomyVO ass2 = (AssessmentOstomyVO) vo2;
                                            Collection<AssessmentMucocSeperationsVO> u = ass2.getSeperations();
                                            for (AssessmentMucocSeperationsVO tmpA : u) {
                                                tmpA.setAssessment_ostomy_id(justsaved.getId());
                                                tmpA.setAlpha_id(new Integer(alpha_id));
                                                tmpA.setId(null);
                                                remoteAssessmentService.saveSeperation(tmpA);
                                            }

                                        }
                                    } else if (vo2 instanceof AssessmentDrainVO) {
                                        AssessmentDrainVO tjustsaved = new AssessmentDrainVO();
                                        tjustsaved.setAssessment_id(new Integer(assessment_id));
                                        tjustsaved.setAlpha_id(new Integer(alpha_id));
                                        justsaved = remoteAssessmentService.getAssessment(tjustsaved);
                                    } else if (vo2 instanceof AssessmentSkinVO) {
                                        AssessmentSkinVO tjustsaved = new AssessmentSkinVO();
                                        tjustsaved.setAssessment_id(new Integer(assessment_id));
                                        tjustsaved.setAlpha_id(new Integer(alpha_id));
                                        justsaved = remoteAssessmentService.getAssessment(tjustsaved);
                                    } else if (vo2 instanceof AssessmentIncisionVO) {
                                        AssessmentIncisionVO tjustsaved = new AssessmentIncisionVO();
                                        tjustsaved.setAssessment_id(new Integer(assessment_id));
                                        tjustsaved.setAlpha_id(new Integer(alpha_id));
                                        justsaved = remoteAssessmentService.getAssessment(tjustsaved);
                                    }
                                    if (justsaved != null) {
                                        for (AssessmentProductVO p : products) {
                                            p.setAlpha_id(new Integer(alpha_id));
                                            p.setAssessment_id(new Integer(assessment_id));
                                            p.setWound_id(new Integer(wound_id));
                                            if (justsaved instanceof AssessmentEachwoundVO) {
                                                p.setWound_assessment_id(justsaved.getId());
                                            }
                                            if (justsaved instanceof AssessmentOstomyVO) {
                                                p.setOstomy_assessment_id(justsaved.getId());
                                            }
                                            if (justsaved instanceof AssessmentDrainVO) {
                                                p.setDrain_assessment_id(justsaved.getId());
                                            }
                                            if (justsaved instanceof AssessmentIncisionVO) {
                                                p.setIncision_assessment_id(justsaved.getId());
                                            }
                                            if (justsaved instanceof AssessmentBurnVO) {
                                                p.setBurn_assessment_id(justsaved.getId());
                                            }
                                            if (justsaved instanceof AssessmentSkinVO) {
                                                p.setSkin_assessment_id(justsaved.getId());
                                            }
                                            p.setId(null);
                                            remoteAssessmentService.saveProduct(p);
                                        }
                                    }

                                }
                                //Call Auto Referral check if one of assessments was of wound type
                                if (wpt_autoreferral != 0) {
                                    remoteWoundAssessService.checkAutoReferralsForOffline(new Integer(assessment_id), wpt_autoreferral, language);
                                }
                            }//if vo check


                            ///also dont forget throw exceptions to user.


                        }//if
                        //remove patient data.
                        ActionErrors errors = Common.exceptionsMsg;
                        if (Common.exceptionsMsg.isEmpty()) {
                            localProfileService.removeOffline(off);

                        } else if (errors.size() == 1 && errors.get("updFiles") != null) {
                        }


                    }//for
                    if (Common.exceptionsMsg.isEmpty()) {

                        if (Common.getConfig("deletePatientOnUpload").equals("1") && Common.isOffline() == true) {
                            //remove all data pp, wp, wa, ass,im,pro,comm,recomm,ncp,tc,dcf
                            PatientAccountVO pavo = new PatientAccountVO();
                            WoundAssessmentLocationVO lvo = new WoundAssessmentLocationVO();
                            //wvo.setPatient_id(patient_id);
                            pavo.setPatient_id(new Integer(patient_id));
                            lvo.setPatient_id(new Integer(patient_id));

                            //OfflineVO off = new OfflineVO();
                            //off.setPatient_id(new Integer(patientid + ""));

                            localProfileService.removePatientProfile(patient_id + "");
                            

                            WoundProfileVO tw = new WoundProfileVO();
                            tw.setPatient_id(new Integer(patient_id + ""));
                            localWoundProfileService.dropOffline(tw);

                            WoundAssessmentVO a = new WoundAssessmentVO();
                            a.setPatient_id(new Integer(patient_id));
                            localWoundAssessService.removeAssessment(a, null);
                            
                            localAlphaService.removeAlpha(lvo, null);

                            localProfileService.removeFootAssessment(patient_id + "");
                            localProfileService.removeLimbBasicAssessment(patient_id + "");
                            localProfileService.removeLimbAdvAssessment(patient_id + "");
                            localProfileService.removeLimbUpperAssessment(patient_id + "");
                            localProfileService.removeExam(patient_id + "");
                            localProfileService.removeDiagnosticInvestigation(patient_id + "");
                            localPatientService.removePatient(patient_id + "");

                        }
                    }

                }//for
            }

        } catch (Exception e) {
            e.printStackTrace();
            ActionErrors errors = Common.exceptionsMsg;
            errors.add("upderror", new ActionMessage("errors.msg", e.getMessage()));
            request.setAttribute("errors.msg",e.getMessage());
            Common.exceptionsMsg = errors;
            log.error("Unknown exception uploading data" + e.getMessage());
            //PatientProfileDAO dao=new PatientProfileDAO();
            try {
                request.setAttribute("offline", localProfileService.getAllOffline(new OfflineVO(), (ProfessionalVO) session.getAttribute("userVO"), false));
            } catch (Exception ex) {

                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(ex));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        }
        if (!Common.exceptionsMsg.isEmpty()) {
            saveErrors(request, Common.exceptionsMsg);
            Common.exceptionsMsg = new ActionErrors();
            request.setAttribute("upload_error", "1");
            return (mapping.findForward("upload.failure"));
        } else {
            //purge database if empty
            /*PatientProfileDAO bd=new PatientProfileDAO();
             //get all offline items regardless of user logged in
             try{
             Vector coll2 = bd.findAllOfflineByCriteria(new OfflineVO(),new ProfessionalVO());
             //if none returned.. purge database.
            
             if(coll2.size()==0 && Common.getIsOffline()==true){
             //get IHA purge functions, then check to ensure it cycles.
             purgeDatabase();
             }
             }catch(com.pixalere.common.DataAccessException e){
             ActionErrors errors = new ActionErrors();
             request.setAttribute("exception","y");
             request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
             errors.add("exception",new ActionError("pixalere.common.error"));
             saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
             }catch(ApplicationException e){
             ActionErrors errors = new ActionErrors();
             request.setAttribute("exception","y");
             request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
             errors.add("exception",new ActionError("pixalere.common.error"));
             saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
             }*/
            request.setAttribute("uploadsuccess", "1");
            return (mapping.findForward("upload.success"));
        }

    }    //public void purgeDatabase() throws ApplicationException {
    // }
}
