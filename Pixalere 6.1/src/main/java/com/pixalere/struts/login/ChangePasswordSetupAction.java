/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.login;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ConfigurationServiceImpl;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.Action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.Common;
import java.util.Hashtable;

/**
 *
 * @author travis
 */
public class ChangePasswordSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangePasswordSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        request.setAttribute("id",request.getAttribute("id"));
        request.setAttribute("token",request.getAttribute("token"));
        
        request.setAttribute("reset_password_method",request.getAttribute("reset_password_method"));
        
            if (!Common.isOffline()) {
                Hashtable config = new Hashtable();
                ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
                try {
                    config = manager.getConfiguration();
                    Common.CONFIG = config;
                    request.setAttribute("config",config);
                } catch (ApplicationException e) {
                    log.error("DataAccessException in retrieveConfiguration: " + e.toString());
                    System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());

                    e.printStackTrace();
                }
            return mapping.findForward("change_pass");
        }
        return mapping.findForward("go.login");
    }

}
