/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.login;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Sms;
import java.util.Collection;
import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.MD5;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.Action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author travis
 */
public class ForgotPasswordSetupAction extends Action {
    private static final String ACCOUNT_SID = "AC4abf45eef89867af39038f72bb78ef02";
    private static final String AUTH_TOKEN = "941d50aa1e2c3024ef8376268144b05e";

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ForgotPasswordSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);

        if (!Common.isOffline() && request.getParameter("username") != null) {
            Hashtable config = new Hashtable();
                ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
                try {
                    config = manager.getConfiguration();
                    Common.CONFIG = config;
                    request.setAttribute("config",config);
                } catch (ApplicationException e) {
                    log.error("DataAccessException in retrieveConfiguration: " + e.toString());
                    System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());

                    e.printStackTrace();
                }
            //Get questions for user
            try {
                ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
                ProfessionalVO ptmp = new ProfessionalVO();
                ptmp.setUser_name((String) request.getParameter("username"));
                ListServiceImpl lservice = new ListServiceImpl(language);
                
                ProfessionalVO prof = pservice.getProfessional(ptmp);
                if (prof != null) {
                    if (prof.getReset_password_method()!=null && prof.getReset_password_method().equals(1)) {
                        //get token
                        try {
                          
                                request.setAttribute("action","getToken");
                                //Send random token ID to Cell phone.
                                TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

                                int random_num = Common.getRandomNumber(999999);
                                PasswordResetVO tm = new PasswordResetVO();
                                tm.setProfessional_id(prof.getId());
                                PasswordResetVO t = pservice.getPasswordReset(tm);
                                if (t == null) {
                                    t = new PasswordResetVO();
                                }
                                t.setProfessional_id(prof.getId());
                                t.setCreated_on(new java.util.Date());
                                t.setToken(MD5.hash(random_num + ""));
                                pservice.savePasswordReset(t);
                                System.out.println(prof.getPager() + " TOKEN: " + random_num);
                                // Build a filter for the SmsList
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Body", random_num + "");
                                params.put("To", "+1"+prof.getPager());
                                params.put("From", "+16043325556");
                                SmsFactory messageFactory = client.getAccount().getSmsFactory();
                                Sms message = messageFactory.create(params);
                                System.out.println(message.getSid());
                                request.setAttribute("id",prof.getId());
                                request.setAttribute("reset_password_method","1");
                                return (mapping.findForward("submit.token"));
                            
                        } catch (com.twilio.sdk.TwilioRestException e) {
                            e.printStackTrace();
                            //errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));

                        }
                    } else if(prof.getReset_password_method()!=null && prof.getReset_password_method().equals(0)){
                        request.setAttribute("action","getAnswers");
                        request.setAttribute("id",prof.getId());
                        Collection<LookupVO> lists = lservice.getLists(LookupVO.PASSWORD_QUESTIONS);
                        request.setAttribute("question_list",lists);
                        if(prof.getQuestion_one()!=null){
                            request.setAttribute("question1", lservice.getListItem(prof.getQuestion_one()).getName(1));
                        }else {return (mapping.findForward("go.login"));}
                        if(prof.getQuestion_two()!=null){
                        request.setAttribute("question2", lservice.getListItem(prof.getQuestion_two()).getName(1));
                        }else {return (mapping.findForward("go.login"));}
                        request.setAttribute("reset_password_method","0");
                        return (mapping.findForward("submit.token"));
                    }else{
                        //redirect to login..
                        return (mapping.findForward("go.login"));
                    }
                    //return mapping.findForward("submit.token");
                }else{
                    //redirect to login
                }
            } catch (ApplicationException e) {
                
            }
        }
        return mapping.findForward("go.login");
    }

}
