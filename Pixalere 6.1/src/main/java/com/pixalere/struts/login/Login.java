package com.pixalere.struts.login;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import com.pixalere.common.bean.InformationPopupVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ConfigurationServiceImpl;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;

public class Login extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.struts.login.LoginForm.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        LoginForm lform = (LoginForm) form;

        ProfessionalVO userVO = null;
        if (session.getAttribute("userVO") != null) {
            userVO = (ProfessionalVO) session.getAttribute("userVO");

        }

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
        Hashtable config = new Hashtable();
        try {
            config = manager.getConfiguration();
            Common.CONFIG = config;

        } catch (ApplicationException e) {
            log.error("DataAccessException in retrieveConfiguration: " + e.toString());
            System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());

            e.printStackTrace();
        }
        com.pixalere.common.service.ListServiceImpl lservice = new com.pixalere.common.service.ListServiceImpl(language);

        try {
            //if password reset has been initiated, get logged in user's questions
            if (request.getAttribute("set_password_reset") != null) {
                Collection<LookupVO> lists = lservice.getLists(LookupVO.PASSWORD_QUESTIONS);
                request.setAttribute("question_list", lists);
                request.setAttribute("config",config);
                return (mapping.findForward("get_answers"));
            }
        } catch (ApplicationException e) {
        }
        //if this user is flagged as new, transition to change password screen.
        if (Common.isOffline() == false && userVO != null && ((userVO.getNew_user()).equals(new Integer(1)) || (lform.getChangePasswordFlag() != null && lform.getChangePasswordFlag().equals(1)))) {

            try {

                Collection<LookupVO> lists = lservice.getLists(LookupVO.PASSWORD_QUESTIONS);
                request.setAttribute("question_list", lists);
            } catch (ApplicationException e) {
                log.error("DataAccessException in retrieveConfiguration: " + e.toString());
                System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());

                e.printStackTrace();
            }

            request.setAttribute("config", config);
            //if this is a training user, and change password flag was set, don't allow them to change the password.
            if (userVO.getTraining_flag() != null && userVO.getTraining_flag() == 1 && lform.getChangePasswordFlag() != null && lform.getChangePasswordFlag() == 1) {
                InformationPopupVO info = new InformationPopupVO();
                //request.setAttribute("type", "warning");
                request.setAttribute("type", "error");
                request.setAttribute("field", "success");
                info.setTitle("Change Password Denied");
                info.setDescription(Common.getLocalizedString("pixalere.login.message.password_change_notallowed", locale));
                request.setAttribute("info", info);
                return (mapping.findForward("login.logout"));
            }
            return (mapping.findForward("login.newuser"));
        } else if (request.getParameter("change_password") != null && (Integer.parseInt(request.getParameter("change_password"))) == 1) {
            log.info("##$$##$$##-----IN SESSION INVALIDATE-----##$$##$$##");
            session.removeAttribute("userVO");
            session.setAttribute("userVO", null);
            session.invalidate();
            InformationPopupVO info = new InformationPopupVO();
            //request.setAttribute("type", "warning");
            request.setAttribute("type", "saved");
            request.setAttribute("field", "success");
            info.setTitle("Password changed");
            info.setDescription(Common.getLocalizedString("pixalere.login.message.password_changed", locale));
            request.setAttribute("info", info);
            return (mapping.findForward("login.logout"));
        }
        session.setAttribute("config", config);
        log.info("##$$##$$##-----MAPPING LOGIN.SUCCESS-----##$$##$$##");
        if (request.getAttribute("newuser_error") != null) {
            return (mapping.findForward("login.newuser"));
        } else {

            boolean sync = true;//temp
            log.info("Login.perform(); Option->" + request.getParameter("offlineoption"));
            if (request.getParameter("offlineoption") != null && ((String) request.getParameter("offlineoption")).equals("sync")) {
                return (mapping.findForward("sync.success"));
            } else if (request.getParameter("offlineoption") != null && ((String) request.getParameter("offlineoption")).equals("upload")) {
                return (mapping.findForward("upload.success"));
            }
            if (userVO != null && userVO.getPosition() != null && userVO.getPosition().getId().equals(new Integer(Common.getConfig("patient_position_id")))) {
                return (mapping.findForward("uploader.go.patientupload"));
            }
            return (mapping.findForward("login.success"));
        }

    }
}
