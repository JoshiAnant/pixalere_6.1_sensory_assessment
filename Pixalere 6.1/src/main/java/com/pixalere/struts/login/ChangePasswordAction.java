/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.login;

import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.MD5;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.Action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author travis
 */
public class ChangePasswordAction extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.struts.login.ChangePasswordAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        ChangePasswordForm lform = (ChangePasswordForm) form;

        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        ProfessionalVO ptmp = new ProfessionalVO();
        ptmp.setId(lform.getId());
        try {
            ProfessionalVO retrieved_prof = pservice.getProfessional(ptmp);
            
            if (lform.getReset_password_method().equals(1) && retrieved_prof != null && lform.getNewpassword()!=null) {
                //verify token.
                PasswordResetVO reset = new PasswordResetVO();
                reset.setProfessional_id(lform.getId());
                PasswordResetVO r = pservice.getPasswordReset(reset);
         
                if (r != null && r.getToken().equals(lform.getToken())) {
               
                    retrieved_prof.setPassword((MD5.hash(lform.getNewpassword())).toLowerCase());
                    retrieved_prof.setNew_user(new Integer(0));
                    pservice.saveProfessional(retrieved_prof, retrieved_prof.getId());
                    return (mapping.findForward("change_pass"));
                    //forward to change password screen, pass md5 for verification and id
                } else {

                    request.setAttribute("error", "incorrect_token");
                    return (mapping.findForward("go.login"));
                }
            } 
            else if (lform.getReset_password_method().equals(0) && retrieved_prof != null && lform.getNewpassword()!=null) {
                if (retrieved_prof != null && (retrieved_prof.getAnswer_one()+retrieved_prof.getAnswer_two()).equals(lform.getToken())) {
               
                    retrieved_prof.setPassword((MD5.hash(lform.getNewpassword())).toLowerCase());
                    retrieved_prof.setNew_user(new Integer(0));
                    pservice.saveProfessional(retrieved_prof, retrieved_prof.getId());
                    return (mapping.findForward("change_pass"));
                    //forward to change password screen, pass md5 for verification and id
                } else {

                    return (mapping.findForward("go.login"));
                }
            }
            
            else {
                //forward toe rror page (wrong answr or token)
                return (mapping.findForward("go_login"));
            }

        } catch (ApplicationException e) {
            e.printStackTrace();
            //errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(e)));

        }
        return (mapping.findForward("pixalere.login"));
    }
}
