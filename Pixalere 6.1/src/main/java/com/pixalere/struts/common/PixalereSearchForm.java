package com.pixalere.struts.common;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.utils.*;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.wound.service.WoundServiceImpl;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Date;

/**
 * @author
 */
public class PixalereSearchForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PixalereSearchForm.class);
    
    private String searchItem;
    private String ack;
    private String referralPop;
    private String search;

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        HttpSession session = request.getSession();
        clearSession(session);
        
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        if (request.getParameter("search") != null && !((String) request.getParameter("search")).equals("") && session.getAttribute("userVO") != null) {
            this.searchItem = (String) request.getParameter("search");
            try {
                PatientServiceImpl manager = new PatientServiceImpl(language);
                String patient_search = request.getParameter("search");
                patient_search = patient_search.trim();
                if (patient_search.length() > 6) {
                    patient_search = "-1";
                }
                int errorNum = manager.validatePatientId(((ProfessionalVO) session.getAttribute("userVO")).getId().intValue(), Integer.parseInt(patient_search));
                String purgeDays = Common.getConfig("purgeTMPRecordsAfterXDays");
                //purge all tmp records older then configured PurgeDays.
                if (!purgeDays.equals("") && !purgeDays.equals("-1")) {//if purging is allowed

                    try {
                        int patient_id = Integer.parseInt(searchItem);
                        int days = Integer.parseInt(purgeDays);
                        //days = days * 60 * 60 * 24;//days # of days by 60 seconds, 60 minutes, 24 hrs.
                        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                        Date purge_days = new Date();
                       
                        purge_days = PDate.subtractDays(purge_days, days);
                        //purge pp

                        PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
                        pmanager.dropTMP(searchItem, Constants.IGNORE_STR, purge_days);

                        pmanager.dropTMPFoot(searchItem, Constants.IGNORE_STR, purge_days);
                        pmanager.dropTMPBasicLimb(searchItem, Constants.IGNORE_STR, purge_days);
                        pmanager.dropTMPAdvLimb(searchItem, Constants.IGNORE_STR, purge_days);
                        pmanager.dropTMPBraden(searchItem, Constants.IGNORE_STR, purge_days);
                        pmanager.dropTMPExam(searchItem, Constants.IGNORE_STR, purge_days);
                        //purge wound_assessments, each assessments, images, products
                        WoundAssessmentServiceImpl amanager = new WoundAssessmentServiceImpl();
                        WoundAssessmentLocationServiceImpl lmanager = new WoundAssessmentLocationServiceImpl();
                        WoundAssessmentVO wassess = new WoundAssessmentVO();
                        wassess.setActive(0);
                        wassess.setPatient_id(new Integer(searchItem));
                        amanager.removeAssessment(wassess, purge_days);

                        
                        //get Alphas
                        WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
                        wal.setPatient_id(new Integer(searchItem));
                        wal.setActive(0);
                        lmanager.removeAlpha(wal, purge_days);

                        //purge WP
                        WoundServiceImpl wmanager = new WoundServiceImpl(language);
                        wmanager.dropTMP(searchItem, -1, purge_days);
                        
                    } catch (NumberFormatException e) {
                        e.printStackTrace();

                    }
                }

                session.setAttribute("viewerOnly", "");
                request.setAttribute("suppress_popup", "yes");
                if (errorNum == 1) {
                    if (Common.getLocalizedString("pixalere.offline", "en").indexOf("1") == -1) {
                        errors.add("Patient Search:", new ActionError("pixalere.patientsearch.error.patientnotexist_online"));
                    } else {
                        errors.add("Patient Search:", new ActionError("pixalere.patientsearch.error.patientnotexist_offline"));
                    }
                } else if (errorNum == 2 || errorNum == 3) {
                    PatientAccountVO patientLoc = manager.getPatient(new Integer((String) request.getParameter("search")).intValue());
                    errors.add("Patient Search:", new ActionError("<li>" + Common.getLocalizedString("pixalere.patientsearch.error.assign", locale) + " " + patientLoc.getTreatmentLocation().getName(language) +" "+Common.getLocalizedString("pixalere.patientsearch.error.assign2", locale)+ "</li>"));
                } else if (errorNum == 4) {
                    //	errors.add("Patient Search:", new ActionError("pixalere.patientsearch.error.patient_inactive"));
                    session.setAttribute("viewerOnly", "inactive");
                    session.setAttribute("patient_id", request.getParameter("search"));
                } else if(errorNum == 6){
                    errors.add("training_only:",new ActionError("patient.access.not_training"));
                } 
                else {
                    PatientAccountVO patient = manager.getPatient(new Integer((String) request.getParameter("search")).intValue());
                    session.setAttribute("patient_id", request.getParameter("search"));
                    session.setAttribute("patientAccount", patient);
                }

            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error in PixalereSearchForm(): " + e.toString(), e);
                clearSession(session);

                if (Common.getLocalizedString("pixalere.offline", "en").equals("1")) {
                    errors.add("Patient Search:", new ActionError("pixalere.patientsearch.error.patientnotexist_offline"));
                } else {
                    errors.add("Patient Search:", new ActionError("pixalere.patientsearch.error.patientnotexist_online"));
                }
            }
        } else {
        }
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setSearchItem("");
    }

    public void setReferralPop(String referralPop) {
        this.referralPop = referralPop;
    }

    public String getReferralPop() {
        return referralPop;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public String getAck() {
        return ack;
    }

    public void setSearchItem(String searchItem) {
        this.searchItem = searchItem;
    }

    public String getSearchItem() {
        return searchItem;
    }

    public void clearSession(HttpSession session) {
        Common.invalidatePatientSession(session);

    }

    /**
     * @return the search
     */
    public String getSearch() {
        return search;
    }

    /**
     * @param search the search to set
     */
    public void setSearch(String search) {
        this.search = search;
    }
}
