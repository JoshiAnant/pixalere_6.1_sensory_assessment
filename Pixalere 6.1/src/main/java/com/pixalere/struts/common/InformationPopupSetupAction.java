package com.pixalere.struts.common;
import com.pixalere.common.bean.ComponentsVO;
import java.util.Collection;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.InformationPopupVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

public class InformationPopupSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InformationPopupSetupAction.class);

    /** TODO: Look into passing patientAcocuntVO throw submit (edit) **/
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        String field = (String) request.getParameter("field");
        String description = "";
        if (request.getParameter("description") != null) {
            description = (String) request.getParameter("description").replace("@", "%");
        }

        String title = (String) request.getParameter("title");
        String desc2 = (String) request.getParameter("desc2");
        if (desc2 == null) {
            desc2 = "";
        }
        String type = (String) request.getParameter("type");
        String fromFormValidation = (String) request.getParameter("errorAdd");
        try {
            request.setAttribute("form", request.getParameter("desc2"));
            if (type != null && !type.equals("")) {
                request.setAttribute("type", type);
                if (type.equals("warn") && (fromFormValidation == null || !fromFormValidation.equals("1"))) {
                    String error_string = "";
                    StringTokenizer st = new StringTokenizer(description, ",");
                    while (st.hasMoreTokens()) {
                        String key = st.nextToken();
                        // If text contains "<li>", this means that it should show as it is without getting localized string
                        if (key.indexOf("<li>") != -1) {
                            error_string = error_string + key;
                        } else {
                            error_string = error_string + Common.getLocalizedString(key, locale);
                        }
                    }
                    description = error_string;
                }
            } else {
                request.setAttribute("type", "info");
            }

            if (field != null && !field.equals("") && !field.equals("0")) {
                GUIServiceImpl gui = new GUIServiceImpl();
                ListServiceImpl lservice = new ListServiceImpl();
                InformationPopupVO t = new InformationPopupVO();
                t.setId(Integer.parseInt(field));
                InformationPopupVO info = gui.getInformationPopup(t);
                if (info != null) {
                    request.setAttribute("info", info);
                    ComponentsVO comp = gui.getComponent(info.getComponent_id());
                    if(comp!=null){
                        Collection<LookupVO> lookups = lservice.getLists(comp.getResource_id());
                        request.setAttribute("definitions",lookups);
                    }
                }
            } else {
                InformationPopupVO info = new InformationPopupVO();
                info.setTitle(title);
                if (type != null && (type.equals("warn") || type.equals("warn2") || type.equals("confirm"))) {
                    info.setDescription(description);
                    if (!desc2.equals("")) {
                        request.setAttribute("form_name", request.getParameter("desc2"));
                    }
                } else {
                    if (desc2.equals("")) {
                        info.setDescription(" " + description);
                    } else {
                        info.setDescription("New Value: " + description + "<br/>Old Value: " + desc2);
                    }
                }
                request.setAttribute("info", info);
            }

            request.setAttribute("page", "infopopup");
        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("common.help.success"));



    }
}

