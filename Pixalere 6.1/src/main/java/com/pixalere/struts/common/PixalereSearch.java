package com.pixalere.struts.common;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.*;
import com.pixalere.guibeans.RowData;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.assessment.dao.WoundAssessmentDAO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PixalereSearch extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PixalereSearch.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        //log.error("Session: " + session);
        
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        if (userVO == null) {//session is invalid.. return to login
            request.setAttribute("session_invalid", "yes");
            return (mapping.findForward("system.logoff"));
        }
        PixalereSearchForm searchForm = (PixalereSearchForm) form;
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
        
        boolean go_home = false;
        if (!searchForm.getSearchItem().equals("")) {
            try {
                LogAuditServiceImpl bd = new LogAuditServiceImpl();

                if (request.getParameter("wound_profile_type_id") != null && !((String) request.getParameter("wound_profile_type_id")).equals("")) {
                    session.setAttribute("wound_profile_type_id", (String) request.getParameter("wound_profile_type_id"));

                }
                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                LogAuditVO vo = new LogAuditVO(userVO.getId(), new Integer(((String) session.getAttribute("patient_id"))), Constants.ACCESSED_PATIENT);
                bd.saveLogAudit(vo);
            } catch (ApplicationException e) {
                log.error("PixalereSearch: Exception in perform: " + e.getMessage());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
            
            if (searchForm.getAck() != null && !searchForm.getAck().equals("")) {
                if (searchForm.getReferralPop().equals("referral")) {
                    request.setAttribute("ack", searchForm.getAck());
                    Hashtable commentSession = new Hashtable();
                    commentSession.put("comment", "");
                    commentSession.put("recommendation", "");
                    commentSession.put("referral", searchForm.getAck());
                    session.setAttribute("savedComments", commentSession);
                    return (mapping.findForward("viewer.comments.success"));

                } else if (searchForm.getReferralPop().equals("recommendation")) {
                    request.setAttribute("ack", searchForm.getAck());
                    Hashtable commentSession = new Hashtable();
                    commentSession.put("comment", "");
                    commentSession.put("recommendation", "");
                    commentSession.put("referral", searchForm.getAck());
                    session.setAttribute("savedComments", commentSession);
                    return (mapping.findForward("viewer.comments.success"));
                } else if (searchForm.getReferralPop().equals("msg_nurse") || searchForm.getReferralPop().equals("msg_nurse")) {
                    request.setAttribute("ack", searchForm.getAck());
                    Hashtable commentSession = new Hashtable();
                    commentSession.put("comment", "");
                    commentSession.put("recommendation", "");
                    commentSession.put("referral", "");
                    session.setAttribute("savedComments", commentSession);
                    ReferralsTrackingServiceImpl ref = new ReferralsTrackingServiceImpl(language);
                    ReferralsTrackingVO refVO = new ReferralsTrackingVO();
                    try {
                        if (!searchForm.getAck().equals("")) {
                            refVO.setAssessment_id(new Integer(searchForm.getAck()));
                            if (searchForm.getReferralPop().equals("msg_nurse")) {
                                refVO.setEntry_type(new Integer(Constants.MESSAGE_FROM_NURSE));
                            } else if (searchForm.getReferralPop().equals("msg_clinican")) {
                                refVO.setEntry_type(new Integer(Constants.MESSAGE_FROM_CLINICIAN));
                            }
                            refVO.setCurrent(1);
                            refVO.setWound_profile_type_id(new Integer(request.getParameter("wound_profile_type_id") + ""));
                            ReferralsTrackingVO r = ref.getReferralsTracking(refVO);
                            if (r != null) {
                                r.setCurrent(0);
                                ref.saveReferralsTracking(r);
                            }
                        }
                    } catch (ApplicationException ext) {
                        ext.printStackTrace();
                        log.error("Error saving Message: " + ext.getMessage());
                    }
                    return (mapping.findForward("viewer.comments.success"));

                }
            }
        } else {
            go_home = true;
        }
        if (userVO != null && userVO.getAccess_uploader().intValue() == 1) {

            if (go_home == true) {

                Common.invalidatePatientSession(session);
                if (searchForm.getReferralPop() != null && ((String) searchForm.getReferralPop()).equals("yes")) {
                    return (mapping.findForward("error.nopatient.access"));
                } else if (searchForm.getReferralPop() != null && ((String) searchForm.getReferralPop()).equals("no")) {
                    request.setAttribute("suppress_popup", "yes");
                    return (mapping.findForward("error.nopatient.access"));
                } else {
                    request.setAttribute("suppress_popup", "yes");
                    return (mapping.findForward("error.nopatient.access"));
                }
            } else {                                
                try {
                    // Before doing anything validate Patient Consent
                    boolean consentEnabled = Common.getConfig("patientConsentEnabled").equals("1");
                    boolean consent;
                    if (consentEnabled) {
                        PatientServiceImpl patientService = new PatientServiceImpl(language);
                        PatientAccountVO patient = patientService.getPatient(new Integer((String) session.getAttribute("patient_id")));
                        consent = patient.isPatientConsent(); 
                    } else {
                        consent = true;
                    }

                    if (!consent) {
                        // If not then go to Patient Admin
                        return (mapping.findForward("uploader.go.admin"));
                    }
                    
                    String strUnsavedData = "";
                    List<FieldValues> comparedppdata = null;
                    List<FieldValues> comparedbdata = null;
                    List<FieldValues> comparedwpdata = null;
                    List<FieldValues> comparedfadata = null;
                    List<FieldValues> comparedladata = null;
                    List<FieldValues> comparedladvdata = null;
                    Collection<PatientProfileVO> resultsPP = new ArrayList();
                    Collection<BradenVO> resultsB = new ArrayList();
                    Collection<WoundProfileVO> resultsWP = new ArrayList();
                    Collection<FootAssessmentVO> resultsFA = new ArrayList();
                    Collection<LimbBasicAssessmentVO> resultsLA = new ArrayList();
                    Collection<LimbAdvAssessmentVO> resultsLAdv = new ArrayList();

                    PatientProfileVO unsavedPP = new PatientProfileVO();
                    unsavedPP.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    unsavedPP.setProfessional_id(userVO.getId());
                    unsavedPP.setActive(new Integer(0));
                    PatientProfileVO tmpPP = pmanager.getPatientProfile(unsavedPP);
                    if (tmpPP != null) {
                        PatientProfileVO currentPP = new PatientProfileVO();
                        currentPP.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        currentPP.setCurrent_flag(new Integer(1));
                        PatientProfileVO currentPatientProfile = pmanager.getPatientProfile(currentPP);
                        resultsPP.add(tmpPP);
                        boolean blnFoundUnsavedPP = false;
                        if (currentPatientProfile == null) {
                            blnFoundUnsavedPP = true;
                        } else {
                            resultsPP.add(currentPatientProfile);
                            //only do a compare if a current record actulaly exists.. no need otherwise.
                            RowData[] ppRecord = pmanager.getAllPatientProfilesForFlowchart(resultsPP, userVO, false,true);
                            comparedppdata = Common.compareGlobal(ppRecord);
                            blnFoundUnsavedPP = Common.recordChanged(comparedppdata);
                        }
                        if (blnFoundUnsavedPP == true) {
                            strUnsavedData = strUnsavedData + Common.getLocalizedString("pixalere.alert.patientprofile", locale) + ": " + tmpPP.getUser_signature() + "<br/>";
                        }
                    }

                    //Braden check
                    BradenVO unsavedB = new BradenVO();
                    unsavedB.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    unsavedB.setProfessional_id(userVO.getId());
                    unsavedB.setActive(new Integer(0));
                    BradenVO tmpB = pmanager.getBraden(unsavedB);
                    if (tmpB != null) {
                        BradenVO currentB = new BradenVO();
                        currentB.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        currentB.setCurrent_flag(new Integer(1));
                        BradenVO currentBraden = pmanager.getBraden(currentB);
                        resultsB.add(tmpB);
                        boolean blnFoundUnsavedB = false;
                        if (currentBraden == null) {
                            blnFoundUnsavedB = true;
                        } else {
                            resultsB.add(currentBraden);
                            //only do a compare if a current record actulaly exists.. no need otherwise.
                            RowData[] bRecord = pmanager.getAllBradenForFlowchart(resultsB, userVO, false);
                            comparedbdata = Common.compareGlobal(bRecord);
                            blnFoundUnsavedB = Common.recordChanged(comparedbdata);
                        }
                        if (blnFoundUnsavedB == true) {
                            strUnsavedData = strUnsavedData + Common.getLocalizedString("pixalere.alert.braden", locale) + ": " + tmpB.getUser_signature() + "<br/>";
                        }
                    }

                    FootAssessmentVO unsavedFA = new FootAssessmentVO();
                    unsavedFA.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    unsavedFA.setProfessional_id(userVO.getId());
                    unsavedFA.setActive(new Integer(0));
                    FootAssessmentVO tmpFA = pmanager.getFootAssessment(unsavedFA);
                    if (tmpFA != null) {
                        FootAssessmentVO currentFA = new FootAssessmentVO();
                        currentFA.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        currentFA.setActive(new Integer(1));
                        FootAssessmentVO currentFootAssessment = pmanager.getFootAssessment(currentFA);
                        resultsFA.add(tmpFA);
                        boolean blnFoundUnsavedFA = false;
                        if (currentFootAssessment == null) {
                            blnFoundUnsavedFA = true;
                        } else {
                            resultsFA.add(currentFootAssessment);
                            //only do a compare if a current record actulaly exists.. no need otherwise.
                            RowData[] faRecord = pmanager.getAllFootAssessmentsForFlowchart(resultsFA, userVO, false);
                            comparedfadata = Common.compareGlobal(faRecord);
                            blnFoundUnsavedFA = Common.recordChanged(comparedfadata);
                        }
                        if (blnFoundUnsavedFA == true) {
                            strUnsavedData = strUnsavedData + Common.getLocalizedString("pixalere.alert.foot", locale) + ": " + tmpFA.getUser_signature() + "<br/>";
                        }
                    }
                    LimbBasicAssessmentVO unsavedLA = new LimbBasicAssessmentVO();
                    unsavedLA.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    unsavedLA.setProfessional_id(userVO.getId());
                    unsavedLA.setActive(new Integer(0));
                    LimbBasicAssessmentVO tmpLA = pmanager.getLimbBasicAssessment(unsavedLA);
                    if (tmpLA != null) {
                        LimbBasicAssessmentVO currentLA = new LimbBasicAssessmentVO();
                        currentLA.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        currentLA.setActive(new Integer(1));
                        LimbBasicAssessmentVO currentLimbAssessment = pmanager.getLimbBasicAssessment(currentLA);
                        resultsLA.add(tmpLA);
                        boolean blnFoundUnsavedLA = false;
                        if (currentLimbAssessment == null) {
                            blnFoundUnsavedLA = true;
                        } else {
                            resultsLA.add(currentLimbAssessment);
                            //only do a compare if a current record actulaly exists.. no need otherwise.
                            RowData[] laRecord = pmanager.getAllBasicLimbAssessmentsForFlowchart(resultsLA, userVO, false);
                            comparedladata = Common.compareGlobal(laRecord);
                            blnFoundUnsavedLA = Common.recordChanged(comparedladata);
                        }
                        if (blnFoundUnsavedLA == true) {
                            strUnsavedData = strUnsavedData + Common.getLocalizedString("pixalere.alert.limb", locale) + ": " + tmpLA.getUser_signature() + "<br/>";
                        }
                    }
                     LimbAdvAssessmentVO unsavedLAdv = new LimbAdvAssessmentVO();
                    unsavedLAdv.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    unsavedLAdv.setProfessional_id(userVO.getId());
                    unsavedLAdv.setActive(new Integer(0));
                    LimbAdvAssessmentVO tmpLAdv = pmanager.getLimbAdvAssessment(unsavedLAdv);
                    if (tmpLAdv != null) {
                        LimbAdvAssessmentVO currentLA = new LimbAdvAssessmentVO();
                        currentLA.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        currentLA.setActive(new Integer(1));
                        LimbAdvAssessmentVO currentLimbAssessment = pmanager.getLimbAdvAssessment(currentLA);
                        resultsLAdv.add(tmpLAdv);
                        boolean blnFoundUnsavedLA = false;
                        if (currentLimbAssessment == null) {
                            blnFoundUnsavedLA = true;
                        } else {
                            resultsLAdv.add(currentLimbAssessment);
                            //only do a compare if a current record actulaly exists.. no need otherwise.
                            RowData[] laRecord = pmanager.getAllAdvLimbAssessmentsForFlowchart(resultsLAdv, userVO, false);
                            comparedladvdata = Common.compareGlobal(laRecord);
                            blnFoundUnsavedLA = Common.recordChanged(comparedladvdata);
                        }
                        if (blnFoundUnsavedLA == true) {
                            strUnsavedData = strUnsavedData + Common.getLocalizedString("pixalere.alert.limb", locale) + ": " + tmpLAdv.getUser_signature() + "<br/>";
                        }
                    }

                    WoundServiceImpl wmanager = new WoundServiceImpl(language);
                    WoundProfileDAO wpdao = new WoundProfileDAO();
                    com.pixalere.wound.bean.WoundProfileVO critUnsavedWP = new com.pixalere.wound.bean.WoundProfileVO();
                    com.pixalere.wound.bean.WoundProfileVO currentWP = null;
                    critUnsavedWP.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    critUnsavedWP.setProfessional_id(userVO.getId());
                    critUnsavedWP.setActive(new Integer(0));
                    boolean blnUnsavedWPs = false;
                    Collection collUnsavedWP = null;
                    collUnsavedWP = wpdao.findAllByCriteria(critUnsavedWP, 0);
                    Iterator iterUnsavedWP = collUnsavedWP.iterator();
                    while (iterUnsavedWP.hasNext()) {
                        com.pixalere.wound.bean.WoundProfileVO unsavedWP = (com.pixalere.wound.bean.WoundProfileVO) iterUnsavedWP.next();
                        com.pixalere.wound.bean.WoundProfileVO critCurrentWP = new com.pixalere.wound.bean.WoundProfileVO();
                        critCurrentWP.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                        critCurrentWP.setCurrent_flag(new Integer(1));
                        critCurrentWP.setWound_id(unsavedWP.getWound_id());
                        currentWP = (com.pixalere.wound.bean.WoundProfileVO) wpdao.findByCriteria(critCurrentWP);
                        boolean blnFoundUnsavedWP = false;
                        if (currentWP == null) // New, unsaved wp
                        {
                            blnFoundUnsavedWP = true;
                        } else {
                            resultsWP.add(currentWP);
                            resultsWP.add(unsavedWP);
                            RowData[] tmpRecord = wmanager.getAllWoundProfilesForFlowchart(resultsWP, userVO, false,true);
                            comparedwpdata = Common.compareGlobal(tmpRecord);
                            blnFoundUnsavedWP = Common.recordChanged(comparedwpdata);
                        }
                        if (blnFoundUnsavedWP && unsavedWP.getWound() != null) {
                            strUnsavedData = strUnsavedData + Common.getLocalizedString("pixalere.alert.woundprofile", locale) + " " + unsavedWP.getWound().getWound_location_detailed() + ": " + unsavedWP.getUser_signature() + "<br/>";
                        }
                    }
                    WoundAssessmentDAO wasDAO = new WoundAssessmentDAO();
                    //shouldn't be using database layer in this layer.. need to be using managers... tsk tsk :P
                    com.pixalere.assessment.bean.WoundAssessmentVO critWoundAssessment = new com.pixalere.assessment.bean.WoundAssessmentVO();
                    critWoundAssessment.setActive(0);
                    critWoundAssessment.setProfessional_id(userVO.getId());
                    critWoundAssessment.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    Collection collWoundAssessment = null;
                    collWoundAssessment = wasDAO.findAllByCriteria(critWoundAssessment);
                    Iterator iterWoundAssessment = collWoundAssessment.iterator();
                    while (iterWoundAssessment.hasNext()) {
                        WoundAssessmentVO unsavedWoundAssessment = (WoundAssessmentVO) iterWoundAssessment.next();
                        WoundVO wvo = new WoundVO();
                        wvo.setId(unsavedWoundAssessment.getWound_id());
                        WoundVO wound = wmanager.getWound(wvo);
                        strUnsavedData = strUnsavedData + (wound != null ? Common.getLocalizedString("pixalere.alert.woundprofile", locale) + " " + wound.getWound_location_detailed() : "") + "; " + Common.getLocalizedString("pixalere.alert.assessment_treatment",locale) + ": " + unsavedWoundAssessment.getUser_signature() + "<br/>";
                    }

                    Vector unsavedAlphas = null;
                    WoundAssessmentLocationDAO locDAO = new WoundAssessmentLocationDAO();
                    WoundAssessmentLocationVO critUnsavedAlphas = new WoundAssessmentLocationVO();
                    critUnsavedAlphas.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    critUnsavedAlphas.setProfessional_id(userVO.getId());
                    critUnsavedAlphas.setActive(new Integer(0));
                    unsavedAlphas = locDAO.findAllByCriteria(critUnsavedAlphas, userVO.getId().intValue(), false, "");//4th parameter (extra sort) is ignored

                    if (unsavedAlphas != null && unsavedAlphas.size() != 0) {
                        strUnsavedData = strUnsavedData + Common.getLocalizedString("pixalere.alert.alphas_detected", locale) + "<br/>";
                    }
                    
                    if (session.getAttribute("viewerOnly").equals("TIP")) {
                        return (mapping.findForward("uploader.go.viewer"));
                    } else if (strUnsavedData.length() > 0) {   //check if anything changed (diff live/tmp record)
                        request.setAttribute("unsaved_data", strUnsavedData);
                        return (mapping.findForward("uploader.alerttemp.success"));
                    }

                    PatientProfileVO p = new PatientProfileVO();

                    p.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
                    p.setActive(1);
                    PatientProfileVO prof = null;
                    try {
                        prof = pmanager.getPatientProfile(p);
                    } catch (Exception e) {
                    }
                    if (prof != null || !session.getAttribute("viewerOnly").equals("")) {
                        return (mapping.findForward("uploader.go.viewer"));
                    } else {
                        return (mapping.findForward("admin.patientsearch.success"));
                    }

                } catch (NumberFormatException e) {
                    System.out.println("Professional: " + (userVO != null ? userVO.getId() : "0") + " @ Date: " + new PDate().getEpochTime() + " An NumberFormatException exception has been raised in PixalereSearch.perform(): " + e.getMessage());
                    e.printStackTrace();
                    return (mapping.findForward("error.nopatient.access"));
                } catch (com.pixalere.common.DataAccessException e) {
                    e.printStackTrace();
                    log.error("An application exception has been raised in PixalereSearch.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                } catch (ApplicationException e) {
                    e.printStackTrace();
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }


            }
        } else if (userVO != null && userVO.getAccess_viewer().intValue() == 1) {
            return (mapping.findForward("viewer.success"));
        } else {
            System.out.println("No Patient Access");
            request.setAttribute("suppress_popup", "yes");
            return (mapping.findForward("error.nopatient.access"));
        }

    }
}
