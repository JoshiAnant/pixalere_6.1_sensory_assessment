package com.pixalere.struts.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class SessionTimeout extends Action {

        static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SessionTimeout.class);

        public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
			request.setAttribute("sessiontimeout","1");
			HttpSession session=request.getSession();
					            session.removeAttribute("userVO");
          session.invalidate();
          return (mapping.findForward("session.timeout.success"));

        }

}
