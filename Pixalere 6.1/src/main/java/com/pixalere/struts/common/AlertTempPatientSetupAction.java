/*
 * Copyright (c) 2006 Your Corporation. All Rights Reserved.
 */
package com.pixalere.struts.common;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
/**
 * Created by IntelliJ IDEA.
 * User: travismorris
 * Date: Jan 12, 2006
 * Time: 7:17:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlertTempPatientSetupAction extends Action {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AlertTempPatientSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            HttpSession session = request.getSession();
        
            request.setAttribute("patient_id",request.getAttribute("patient_id"));
            request.setAttribute("wound_profile_type_id",request.getAttribute("wound_profile_type_id"));
            request.setAttribute("unsaved_data",request.getAttribute("unsaved_data"));
        } catch (Exception e) {
            log.error("AlertTempPatientSetupAction.perform error: " + e.getMessage());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("uploader.alerttemp.success"));
        
    }
    
}
