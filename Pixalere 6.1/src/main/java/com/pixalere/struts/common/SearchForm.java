package com.pixalere.struts.common;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 * @author
 */
public class SearchForm extends ActionForm {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SearchForm.class);

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
//reset code
    }

    /**
     * Returns the password.
     *
     * @return String
     */
    public int getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public int getProfessional_id() {
        return professional_id;
    }

    public void setProfessional_id(int professional_id) {
        this.professional_id = professional_id;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getRegions() {
        return regions;
    }

    public void setRegions(int regions) {
        this.regions = regions;
    }

    public String getPhn1() {
        return phn1;
    }

    public void setPhn1(String phn1) {
        this.phn1 = phn1;
    }




    private String phn1;
    private int regions;
    private int patient_id;
    private int professional_id;
    private String last_name;
    private int dob_year;
    private int dob_month;
    private int dob_day;

	/**
	 * @method getDob_day
	 * @field dob_day - int
	 * @return Returns the dob_day.
	 */
	public int getDob_day() {
		return dob_day;
	}

	/**
	 * @method setDob_day
	 * @field dob_day - int
	 * @param dob_day The dob_day to set.
	 */
	public void setDob_day(int dob_day) {
		this.dob_day = dob_day;
	}

	/**
	 * @method getDob_month
	 * @field dob_month - int
	 * @return Returns the dob_month.
	 */
	public int getDob_month() {
		return dob_month;
	}

	/**
	 * @method setDob_month
	 * @field dob_month - int
	 * @param dob_month The dob_month to set.
	 */
	public void setDob_month(int dob_month) {
		this.dob_month = dob_month;
	}

	/**
	 * @method getDob_year
	 * @field dob_year - int
	 * @return Returns the dob_year.
	 */
	public int getDob_year() {
		return dob_year;
	}

	/**
	 * @method setDob_year
	 * @field dob_year - int
	 * @param dob_year The dob_year to set.
	 */
	public void setDob_year(int dob_year) {
		this.dob_year = dob_year;
	}


}
