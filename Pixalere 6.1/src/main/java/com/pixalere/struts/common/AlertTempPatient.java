package com.pixalere.struts.common;

import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.service.WoundServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Iterator;
import com.pixalere.assessment.dao.WoundAssessmentDAO;
import com.pixalere.assessment.dao.AssessmentDAO;
import com.pixalere.assessment.dao.AssessmentCommentsDAO;
import com.pixalere.assessment.dao.NursingCarePlanDAO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.utils.Common;

public class AlertTempPatient extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AlertTempPatient.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        AlertTempPatientForm formtemp = (AlertTempPatientForm) form;
        if (request.getParameter("patient_id") != null
                && !request.getParameter("patient_id").equals("")) {
            session.setAttribute("patient_id", request.getParameter("patient_id"));
        }
        if (request.getParameter("wound_profile_type_id") != null
                && !request.getParameter("wound_profile_type_id").equals("")) {
            session.setAttribute("wound_profile_type_id", request.getParameter("wound_profile_type_id"));
        }
        if ((formtemp.getAccept()).equals("no")) {
            // drop unsaved data
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
            WoundServiceImpl wmanager = new WoundServiceImpl(language);
            AssessmentCommentsDAO comDAO = new AssessmentCommentsDAO();
            NursingCarePlanDAO ncpdao = new NursingCarePlanDAO();
            ReferralsTrackingServiceImpl refbd = new ReferralsTrackingServiceImpl(language);
            com.pixalere.assessment.service.AssessmentServiceImpl amanager = new com.pixalere.assessment.service.AssessmentServiceImpl(language);
            if (request.getParameter("patient_id") != null
                    && !request.getParameter("patient_id").equals("")) {
                try {
                    pmanager.dropTMP((String) request.getParameter("patient_id"), userVO.getId() + "", null);
                    pmanager.dropTMPFoot((String) request.getParameter("patient_id"), userVO.getId() + "", null);
                    pmanager.dropTMPExam((String) request.getParameter("patient_id"), userVO.getId() + "", null);
                    pmanager.dropTMPBasicLimb((String) request.getParameter("patient_id"), userVO.getId() + "", null);
                    pmanager.dropTMPAdvLimb((String) request.getParameter("patient_id"), userVO.getId() + "", null);
                    pmanager.dropTMPUpperLimb((String) request.getParameter("patient_id"), userVO.getId() + "", null);
                    pmanager.dropTMPBraden((String) request.getParameter("patient_id"), userVO.getId() + "", null);
                    pmanager.dropTMPInvestigation((String) request.getParameter("patient_id"), userVO.getId() + "", null);

                    AssessmentDAO adao = new AssessmentDAO();

                    WoundAssessmentDAO wadao = new WoundAssessmentDAO();
                    WoundAssessmentVO wavo = new WoundAssessmentVO();
                    wavo.setPatient_id(new Integer((String) request.getParameter("patient_id")));
                    wavo.setProfessional_id(userVO.getId());
                    wavo.setActive(new Integer(0));
                    Collection coll = wadao.findAllByCriteria(wavo);
                    Iterator iter = coll.iterator();
                    while (iter.hasNext()) {

                        WoundAssessmentVO wwvo = (WoundAssessmentVO) iter.next();
                        // Remove record from wound_assessment
                        wadao.delete(wwvo, null);
                    }

                    

                    wmanager.dropTMP((String) request.getParameter("patient_id"), userVO.getId(), null);
                    return (mapping.findForward("uploader.alerttemp_viewer.success"));
                } catch (com.pixalere.common.DataAccessException e) {
                    log.error("An application exception has been raised in AlertTempPatient.perform(): " + e.toString());
                    System.out.println("Error: " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in AlertTempPatient.perform(): " + e.toString());
                    System.out.println("Error: " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
            }

        } else {
            //Set session variable.
            WoundAssessmentServiceImpl waservice = new WoundAssessmentServiceImpl();
            WoundAssessmentVO wavo = new WoundAssessmentVO();
            wavo.setPatient_id(new Integer((String) request.getParameter("patient_id")));
            wavo.setProfessional_id(userVO.getId());
            wavo.setActive(new Integer(0));
            try{
            WoundAssessmentVO assessment = waservice.getAssessment(wavo);
            if (assessment != null) {
                session.setAttribute("wound_id", assessment.getWound_id()+"");
                session.setAttribute("assessment_id", assessment.getId()+"");
            } else {
                WoundServiceImpl wservice = new WoundServiceImpl(language);
                WoundProfileVO wpvo = new WoundProfileVO();
                wpvo.setPatient_id(new Integer((String) request.getParameter("patient_id")));
                wpvo.setProfessional_id(userVO.getId());
                wpvo.setActive(new Integer(0));
                WoundProfileVO wound = wservice.getWoundProfile(wpvo);
                if (wound != null) {
                    session.setAttribute("wound_id", wound.getWound_id()+"");
                }
            }
            return (mapping.findForward("uploader.alerttemp.success"));
            }catch(Exception e){}
            
        }
        return (mapping.findForward("uploader.alerttemp.success"));
    }
}
