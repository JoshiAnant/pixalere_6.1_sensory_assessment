/*
 * Copyright (c) 2006 Your Corporation. All Rights Reserved.
 */
package com.pixalere.struts.common;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
/**
 * Created by IntelliJ IDEA.
 * User: travismorris
 * Date: Jan 12, 2006
 * Time: 7:17:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class BradenScaleSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BradenScaleSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        try {
            ListServiceImpl bd = new ListServiceImpl(language);
            ProfessionalVO userVO = (ProfessionalVO)session.getAttribute("userVO");
            request.setAttribute("braden_sensory",bd.getLists(LookupVO.BRADEN_SENSORY));
            request.setAttribute("braden_moisture",bd.getLists(LookupVO.BRADEN_MOISTURE));
            request.setAttribute("braden_mobility",bd.getLists(LookupVO.BRADEN_MOBILITY));
            request.setAttribute("braden_activity",bd.getLists(LookupVO.BRADEN_ACTIVITY));
            request.setAttribute("braden_nutrition",bd.getLists(LookupVO.BRADEN_NUTRITION));
            request.setAttribute("braden_friction",bd.getLists(LookupVO.BRADEN_FRICTION));
            PatientProfileServiceImpl manager = new PatientProfileServiceImpl(language);
            PatientProfileVO pp=new PatientProfileVO();
            pp.setPatient_id(new Integer((String)session.getAttribute("patient_id")));
            pp.setCurrent_flag(new Integer(1));
            PatientProfileVO currentProfile = manager.getPatientProfile(pp);
            PatientProfileVO tmpProfile = manager.getTemporaryPatientProfile(Integer.parseInt((String)session.getAttribute("patient_id")),userVO.getId());
            if(tmpProfile!=null){
                request.setAttribute("pp",tmpProfile);
            }else{
                request.setAttribute("pp",currentProfile);
            }
        } catch (ApplicationException e) {
            log.error("AlertTempPatientSetupAction.perform error: " + e.getMessage());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("uploader.bradenscale.success"));
        
    }
    
}
