package com.pixalere.struts.common;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;


public class HelpSetupAction extends Action {
	
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HelpSetupAction.class);
	
	
	/** TODO: Look into passing patientAcocuntVO throw submit (edit) **/
	
	public ActionForward execute(ActionMapping mapping,
								 
								 ActionForm     form,
								 
								 HttpServletRequest request,
								 
								 HttpServletResponse response){
				HttpSession session=request.getSession();				
				request.setAttribute("page","help"); 
			return (mapping.findForward("common.help.success"));
		

		
	}
	
	
	
}

