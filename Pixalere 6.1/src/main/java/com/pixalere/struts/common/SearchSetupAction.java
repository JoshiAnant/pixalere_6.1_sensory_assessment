package com.pixalere.struts.common;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.Common;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.Vector;

public class SearchSetupAction extends Action {    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SearchSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        HttpSession session=request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        try{
            LookupVO vo = new LookupVO();
            com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
            com.pixalere.auth.bean.ProfessionalVO userVO=(com.pixalere.auth.bean.ProfessionalVO)session.getAttribute("userVO");
            Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION,userVO,false);
            
            for (int intSort1 = 0; intSort1 < treatmentVO.size(); intSort1++) {
                for (int intSort2 = 0; intSort2 < treatmentVO.size() - intSort1 - 1; intSort2++) {
                    LookupVO sortRec1 = (LookupVO) treatmentVO.get(intSort2);
                    LookupVO sortRec2 = (LookupVO) treatmentVO.get(intSort2 + 1);
                    if ((sortRec1.getName(language).toUpperCase()).compareTo(sortRec2.getName(language).toUpperCase()) > 0) {
                        treatmentVO.setElementAt(sortRec2, intSort2);
                        treatmentVO.setElementAt(sortRec1, intSort2 + 1);
                    }
                }
            }
            
            request.setAttribute("treatmentList",treatmentVO);
        }catch(ApplicationException e){
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("uploader.search.success"));
        
    }
    
}

