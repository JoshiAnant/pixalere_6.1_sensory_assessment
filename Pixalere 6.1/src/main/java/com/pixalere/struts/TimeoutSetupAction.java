package com.pixalere.struts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TimeoutSetupAction extends Action {
	//static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HomeSetupAction.class);

	/** TODO: Look into passing patientAcocuntVO throw submit (edit) **/
	public ActionForward execute(ActionMapping mapping,
								 ActionForm     form,
								 HttpServletRequest request,
								 HttpServletResponse response){
				//HttpSession session=request.getSession();
          request.setAttribute("close",request.getParameter("close"));
          request.setAttribute("counter",request.getParameter("counter"));
        return (mapping.findForward("timeout.go.home"));
	}

}
