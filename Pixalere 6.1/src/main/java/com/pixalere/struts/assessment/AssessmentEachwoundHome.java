package com.pixalere.struts.assessment;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
public class AssessmentEachwoundHome extends Action {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentEachwoundHome.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        log.info("##$$##$$##-----PAGE = "+request.getParameter("page")+"-----##$$##$$##");
        HttpSession session=request.getSession();
 
        if(request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.assesshome.patient"));
        } else if(request.getParameter("page").equals("woundprofiles")){
            return (mapping.findForward("uploader.assesshome.profile"));
        } else if(request.getParameter("page").equals("assess")){
            return (mapping.findForward("uploader.assesshome.assess"));
        } else if(request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.assesshome.treatment"));
        } else if(request.getParameter("page").equals("viewer")){
            return (mapping.findForward("uploader.assesshome.viewer"));
        } else if(request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.assesshome.summary"));
        } else if(request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.assesshome.reporting"));
        } else if(request.getParameter("page").equals("admin")){
            log.info("############$$$$$$$$$$$$################# Page: "+request.getParameter("page"));
            return (mapping.findForward("uploader.assesshome.admin"));
        } else{
            return (mapping.findForward("uploader.assesshome.assess"));
        }
    }
}