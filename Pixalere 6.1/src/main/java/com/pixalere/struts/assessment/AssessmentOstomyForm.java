package com.pixalere.struts.assessment;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.guibeans.LocationDepthVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentMucocSeperationsVO;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.Common;
import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Date;
public class AssessmentOstomyForm extends ActionForm{

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentEachwoundForm.class);
    private int clinical_path;
    private int mucocutaneous_margin_num;
    private int mucocutaneous_margin_start_1;
    private int mucocutaneous_margin_end_1;
    private int mucocutaneous_margin_cm_1;
    private int mucocutaneous_margin_mm_1;
    private String image_array3;
    private int mucocutaneous_margin_start_10;
    private int mucocutaneous_margin_end_10;
    private int mucocutaneous_margin_cm_10;
    private int mucocutaneous_margin_mm_10;
    private int mucocutaneous_margin_start_2;
    private int mucocutaneous_margin_end_2;
    private int mucocutaneous_margin_cm_2;
    private int mucocutaneous_margin_mm_2;
    private int mucocutaneous_margin_start_3;
    private int mucocutaneous_margin_end_3;
    private int mucocutaneous_margin_cm_3;
    private int mucocutaneous_margin_mm_3;
    private int mucocutaneous_margin_start_4;
    private int mucocutaneous_margin_end_4;
    private int mucocutaneous_margin_cm_4;
    private int mucocutaneous_margin_mm_4;
    private int mucocutaneous_margin_start_5;
    private int mucocutaneous_margin_end_5;
    private int mucocutaneous_margin_cm_5;
    private int mucocutaneous_margin_mm_5;
    private int mucocutaneous_margin_start_6;
    private int mucocutaneous_margin_end_6;
    private int mucocutaneous_margin_cm_6;
    private int mucocutaneous_margin_mm_6;
    private int mucocutaneous_margin_start_7;
    private int mucocutaneous_margin_end_7;
    private int mucocutaneous_margin_cm_7;
    private int mucocutaneous_margin_mm_7;
    private int mucocutaneous_margin_start_8;
    private int mucocutaneous_margin_end_8;
    private int mucocutaneous_margin_cm_8;
    private int mucocutaneous_margin_mm_8;
    private int mucocutaneous_margin_start_9;
    private int mucocutaneous_margin_end_9;
    private int mucocutaneous_margin_cm_9;
    private int mucocutaneous_margin_mm_9;
    private String drainage_comments;
    private int phone_visit;
    private int length_mm;
    private int width_mm;
    private int nutritional_status;
    private int clinical_path_date_month;
    private int clinical_path_date_day;
    private int clinical_path_date_year;
    private String[] periostomy_skin;
    private int devices;
    private FormFile image_3;
    public FormFile getImage_3() {
        return image_3;
    }

    public void setImage_3(FormFile image_3) {
        this.image_3 = image_3;
    }
    private String[] drainage;
    private int mucous_fistula_present_show;
    private String[] mucocutaneous_margin;
    private String mucocutaneous_margin_comments;
    private String[] perifistula_skin;
    private String[] profile;
    private int circumference_mm;
    private int flange_pouch;
    private String flange_pouch_comments;
    
    private int construction;
    
    private String ostomy_other;
    
    private String training_comments;  
    
    //Peristomal Skin
    private String[] pouching;
    private String[] appearance;
    private String[] contour;
    private int full_assessment;
    //Stoma
    private String[] self_care_progress;
    private String self_care_progress_comments;
    private int shape;
    private int postop_days;

    
    private int status;
    
    //Elimination
    private String[] urine_colour;
    private String[] urine_type;
    private int urine_quantity;
    
    private String[] stool_colour;
    private String[] stool_consistency;
    private int stool_quantity;
    private String nutritional_status_other;

    

    //Closed Date
    private int closed_day;
    private int closed_month;
    private int closed_year;
    private int cs_date_day;
    private int cs_date_month;
    private int cs_date_year;
    private int cs_date_show;
    
    //images
    private String image_array;    
    private String image_array2;
    private FormFile image_1;
    private FormFile image_2;
    
    
    private int visit_freq;
    private int discharge_reason;

    HttpSession session = null;
    
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        
        ActionErrors errors = new ActionErrors();
        
        if (image_1!=null && image_1.getFileName().length()>0 && image_1.getFileSize() == 0) {
            errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
        }

       if (image_2!=null && image_2.getFileName().length()>0 && image_2.getFileSize() == 0) {
            errors.add("file2",new ActionError("pixalere.woundassessment.error.filemissing2"));
        }
       
        return errors;
        
    }
    
    public AssessmentOstomyVO getFormData(ProfessionalVO userVO)  throws ApplicationException{
        PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
    	AssessmentOstomyVO assessmentOstomyVO = new AssessmentOstomyVO();
    	assessmentOstomyVO.setActive(0);
        assessmentOstomyVO.setDeleted(0);
    	assessmentOstomyVO.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
    	assessmentOstomyVO.setWound_id(new Integer((String) session.getAttribute("wound_id")));
    	assessmentOstomyVO.setCircumference_mm(getCircumference_mm() );
        assessmentOstomyVO.setLength_mm(getLength_mm());
        assessmentOstomyVO.setWidth_mm(getWidth_mm());
        assessmentOstomyVO.setSelf_care_progress(Serialize.serialize(getSelf_care_progress()));
        assessmentOstomyVO.setSelf_care_progress_comments(Common.convertCarriageReturns(getSelf_care_progress_comments(),false));
        assessmentOstomyVO.setDrainage(Serialize.serialize(getDrainage()));
        assessmentOstomyVO.setShape(new Integer(getShape()));      
        assessmentOstomyVO.setDrainage_comments(Common.convertCarriageReturns(getDrainage_comments(),false));
        //flange
        
        //assessmentOstomyVO.setCs_date(PDate.getEpochFromDate(cs_date_month,cs_date_day,cs_date_year));
        //radio button
        if( full_assessment == -1){ full_assessment = 1; }
        assessmentOstomyVO.setFull_assessment(full_assessment);
        //multi-selection
        assessmentOstomyVO.setFlange_pouch(getFlange_pouch());
        assessmentOstomyVO.setFlange_pouch_comments(Common.convertCarriageReturns(getFlange_pouch_comments(),false));
        assessmentOstomyVO.setDevices(getDevices());
        assessmentOstomyVO.setStool_colour(Serialize.serialize(getStool_colour()));
        assessmentOstomyVO.setStool_consistency(Serialize.serialize(getStool_consistency()));
        assessmentOstomyVO.setProfile(Serialize.serialize(getProfile()));
        assessmentOstomyVO.setPouching(Serialize.serialize(getPouching()));
        assessmentOstomyVO.setAppearance(Serialize.serialize(getAppearance()));
        assessmentOstomyVO.setContour(Serialize.serialize(getContour()));
        assessmentOstomyVO.setPeriostomy_skin(Serialize.serialize(getPeriostomy_skin()));
        assessmentOstomyVO.setMucocutaneous_margin_comments(Common.convertCarriageReturns(getMucocutaneous_margin_comments(),false));
        assessmentOstomyVO.setPerifistula_skin(Serialize.serialize(getPerifistula_skin()));
        assessmentOstomyVO.setMucocutaneous_margin(Serialize.serialize(getMucocutaneous_margin()));
        //date
        assessmentOstomyVO.setClosed_date(pdate.getDate(getClosed_month(),getClosed_day(),getClosed_year()));
        try{
            if(assessmentOstomyVO.getClosed_date()==null && assessmentOstomyVO.getStatus().equals(Integer.parseInt(Common.getConfig("ostomyClosed")))){
               assessmentOstomyVO.setClosed_date(new Date());
            }
        }catch(NullPointerException e){
            
        }catch(NumberFormatException e){
            
        }
        assessmentOstomyVO.setClinical_path(new Integer(getClinical_path()));
        assessmentOstomyVO.setClinical_path_date(pdate.getDate(getClinical_path_date_month(),getClinical_path_date_day(),getClinical_path_date_year()));
                //assessmentOstomyVO.setWound_date(PDate.getEpochFromDate(getWound_month(), getWound_day(), getWound_year()));
        assessmentOstomyVO.setCreated_on(new Date());
        //dropdown
        assessmentOstomyVO.setStool_quantity(new Integer(getStool_quantity()));
        assessmentOstomyVO.setStatus(new Integer(getStatus() + ""));
        assessmentOstomyVO.setDischarge_reason(new Integer(getDischarge_reason()));
        assessmentOstomyVO.setUrine_type(Serialize.serialize(getUrine_type()));
        assessmentOstomyVO.setUrine_colour(Serialize.serialize(getUrine_colour()));
        assessmentOstomyVO.setConstruction(getConstruction());
        assessmentOstomyVO.setUrine_quantity(new Integer(getUrine_quantity()));
        if(getPostop_days()>0){
       assessmentOstomyVO.setPostop_days(getPostop_days());
        }
        //other 
        assessmentOstomyVO.setStool_quantity_other(getStool_quantity_other());
        assessmentOstomyVO.setNutritional_status(getNutritional_status());
        assessmentOstomyVO.setNutritional_status_other(Common.convertCarriageReturns(getNutritional_status_other(),false));
        // text
        //set for all alphas incase some do not goto treatment.
        assessmentOstomyVO.setSeperations(getSeperations());
        
        assessmentOstomyVO.setStatus(new Integer(getStatus() + ""));
        
        
        return assessmentOstomyVO;
    }
    
    
    
    public int getVisit_freq() {
        return visit_freq;
    }
    
    public void setVisit_freq(int visit_freq) {
        this.visit_freq = visit_freq;
    }
    

    public int getDischarge_reason() {
        return discharge_reason;
    }
    
    public void setDischarge_reason(int discharge_reason) {
        this.discharge_reason = discharge_reason;
    }
    
    public void setClosed_day(int closed_day){
        this.closed_day=closed_day;
    }
    public int getClosed_day(){
        return closed_day;
    }
    public void setClosed_month(int closed_month){
        this.closed_month=closed_month;
    }
    public int getClosed_month(){
        return closed_month;
    }
    public void setClosed_year(int closed_year){
        this.closed_year=closed_year;
    }
    public int getClosed_year(){
        return closed_year;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    
    public int getDevices() {
        return devices;
    }
    
    public void setDevices(int devices) {
        this.devices = devices;
    }
    
  
    
    public String[] getUrine_colour(){
    	return urine_colour;
    }
    
    public void setUrine_colour(String[] urine_colour){
    	this.urine_colour = urine_colour;
    }
    
    public String[] getUrine_type(){
    	return urine_type;
    }
    
    public void setUrine_type(String[] urine_type){
    	this.urine_type = urine_type;
    }
   
    public int getUrine_quantity(){
    	return urine_quantity;
    }
    
    public void setUrine_quantity(int urine_quantity){
    	this.urine_quantity = urine_quantity;
    }
    
  
    
    public String[] getStool_colour(){
    	return stool_colour;
    }
    
    public void setStool_colour(String[] stool_colour){
    	this.stool_colour = stool_colour;
    }
   
    public String[] getStool_consistency(){
    	return stool_consistency;
    }
    
    public void setStool_consistency(String[] stool_consistency){
    	this.stool_consistency = stool_consistency;
    }
 
    public int getStool_quantity(){
    	return stool_quantity;
    }
    
    public void setStool_quantity(int stool_quantity){
    	this.stool_quantity = stool_quantity;
    }

    public String getStool_quantity_other(){
    	return getNutritional_status_other();
    }
    
    public void setStool_quantity_other(String stool_quantity_other){
    	this.setNutritional_status_other(stool_quantity_other);
    }
   
    

    
    public String[] getAppearance(){
    	return appearance;
    }
    
    public void setAppearance (String[] appearance){
    	this.appearance = appearance;
    }
    
    public String[] getContour(){
    	return contour;
    }
    
    public void setContour (String[] contour){
    	this.contour = contour;
    }

    
  
    
    public int getShape(){
    	return shape;
    }
    
    public void setShape(int shape){
    	this.shape = shape;
    }
    
    
    public String getImage_array() {
        return image_array;
    }
    
    public void setImage_array(String image_array) {
        this.image_array = image_array;
    }
    
    public String getImage_array2() {
        return image_array2;
    }
    
    public void setImage_array2(String image_array2) {
        this.image_array2 = image_array2;
    }
    
    public FormFile getImage_1() {
        return image_1;
    }
    
    public void setImage_1(FormFile image_1) {
        this.image_1 = image_1;
    }

    public FormFile getImage_2() {
        return image_2;
    }
    
    public void setImage_2(FormFile image_2) {
        this.image_2 = image_2;
    }
    
    public int getStatus() {
        return status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
  

 

    public int getClinical_path() {
        return clinical_path;
    }

    public void setClinical_path(int clinical_path) {
        this.clinical_path = clinical_path;
    }

    

    public int getClinical_path_date_month() {
        return clinical_path_date_month;
    }

    public void setClinical_path_date_month(int clinical_path_date_month) {
        this.clinical_path_date_month = clinical_path_date_month;
    }

    public int getClinical_path_date_day() {
        return clinical_path_date_day;
    }

    public void setClinical_path_date_day(int clinical_path_date_day) {
        this.clinical_path_date_day = clinical_path_date_day;
    }

    public int getClinical_path_date_year() {
        return clinical_path_date_year;
    }

    public void setClinical_path_date_year(int clinical_path_date_year) {
        this.clinical_path_date_year = clinical_path_date_year;
    }

    

    public String[] getSelf_care_progress() {
        return self_care_progress;
    }

    public void setSelf_care_progress(String[] self_care_progress) {
        this.self_care_progress = self_care_progress;
    }

    public int getFlange_pouch() {
        return flange_pouch;
    }

    public void setFlange_pouch(int flange_pouch) {
        this.flange_pouch = flange_pouch;
    }

    public String[] getProfile() {
        return profile;
    }

    public void setProfile(String[] profile) {
        this.profile = profile;
    }

    public String[] getPouching() {
        return pouching;
    }

    public void setPouching(String[] pouching) {
        this.pouching = pouching;
    }

    public String[] getPeriostomy_skin() {
        return periostomy_skin;
    }

    public void setPeriostomy_skin(String[] periostomy_skin) {
        this.periostomy_skin = periostomy_skin;
    }

    public int getCs_date_day() {
        return cs_date_day;
    }

    public void setCs_date_day(int cs_date_day) {
        this.cs_date_day = cs_date_day;
    }

    public int getCs_date_month() {
        return cs_date_month;
    }

    public void setCs_date_month(int cs_date_month) {
        this.cs_date_month = cs_date_month;
    }

    public int getCs_date_year() {
        return cs_date_year;
    }

    public void setCs_date_year(int cs_date_year) {
        this.cs_date_year = cs_date_year;
    }

    public int getCs_date_show() {
        return cs_date_show;
    }

    public void setCs_date_show(int cs_date_show) {
        this.cs_date_show = cs_date_show;
    }

    public String getFlange_pouch_comments() {
        return flange_pouch_comments;
    }

    public void setFlange_pouch_comments(String flange_pouch_comments) {
        this.flange_pouch_comments = flange_pouch_comments;
    }

    public String getSelf_care_progress_comments() {
        return self_care_progress_comments;
    }

    public void setSelf_care_progress_comments(String self_care_progress_comments) {
        this.self_care_progress_comments = self_care_progress_comments;
    }

    public int getConstruction() {
        return construction;
    }

    public void setConstruction(int construction) {
        this.construction = construction;
    }

    public String[] getDrainage() {
        return drainage;
    }

    public void setDrainage(String[] drainage) {
        this.drainage = drainage;
    }

    public Integer getMucous_fistula_present_show() {
        return mucous_fistula_present_show;
    }

    public void setMucous_fistula_present_show(Integer mucous_fistula_present_show) {
        this.mucous_fistula_present_show = mucous_fistula_present_show;
    }



    public String[] getMucocutaneous_margin() {
        return mucocutaneous_margin;
    }

    public void setMucocutaneous_margin(String[] mucocutaneous_margin) {
        this.mucocutaneous_margin = mucocutaneous_margin;
    }

    public String getMucocutaneous_margin_comments() {
        return mucocutaneous_margin_comments;
    }

    public void setMucocutaneous_margin_comments(String mucocutaneous_margin_comments) {
        this.mucocutaneous_margin_comments = mucocutaneous_margin_comments;
    }

    public String[] getPerifistula_skin() {
        return perifistula_skin;
    }

    public void setPerifistula_skin(String[] perifistula_skin) {
        this.perifistula_skin = perifistula_skin;
    }

    public int getNutritional_status() {
        return nutritional_status;
    }

    public void setNutritional_status(int nutritional_status) {
        this.nutritional_status = nutritional_status;
    }

    public int getCircumference_mm() {
        return circumference_mm;
    }

    public void setCircumference_mm(int circumference_mm) {
        this.circumference_mm = circumference_mm;
    }

    public int getPhone_visit() {
        return phone_visit;
    }

    public void setPhone_visit(int phone_visit) {
        this.phone_visit = phone_visit;
    }

    public String getDrainage_comments() {
        return drainage_comments;
    }

    public void setDrainage_comments(String drainage_comments) {
        this.drainage_comments = drainage_comments;
    }

    public int getLength_mm() {
        return length_mm;
    }

    public void setLength_mm(int length_mm) {
        this.length_mm = length_mm;
    }

    public int getWidth_mm() {
        return width_mm;
    }

    public void setWidth_mm(int width_mm) {
        this.width_mm = width_mm;
    }

    public int getMucocutaneous_margin_num() {
        return mucocutaneous_margin_num;
    }

    public void setMucocutaneous_margin_num(int mucocutaneous_margin_num) {
        this.mucocutaneous_margin_num = mucocutaneous_margin_num;
    }

    public List getSeperations() {
       List v = new ArrayList();
       try {

           for (int x = 0; x < getMucocutaneous_margin_num(); x++) {
               int cnt = x + 1;
               AssessmentMucocSeperationsVO u = new AssessmentMucocSeperationsVO();
               Integer dcm = (Integer) Common.getFieldValue("mucocutaneous_margin_cm_" + cnt, this);
               Integer dmm = (Integer) Common.getFieldValue("mucocutaneous_margin_mm_" + cnt, this);
               Integer dstart = (Integer) Common.getFieldValue("mucocutaneous_margin_start_" + cnt, this);
               Integer dend = (Integer) Common.getFieldValue("mucocutaneous_margin_end_" + cnt, this);
               if (dstart.equals(-1)) {
                   dstart = 0;
               }
               if (dend.equals(-1)) {
                   dend = 0;
               }
               u.setDepth_cm(dcm);
               u.setDepth_mm(dmm);
               u.setLocation_start(dstart);
               u.setLocation_end(dend);
               v.add(u);
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return v;
   }
    

    public int getPostop_days() {
        return postop_days;
    }

    public void setPostop_days(int postop_days) {
        this.postop_days = postop_days;
    }

    public String getNutritional_status_other() {
        return nutritional_status_other;
    }

    public void setNutritional_status_other(String nutritional_status_other) {
        this.nutritional_status_other = nutritional_status_other;
    }

    public int getMucocutaneous_margin_start_1() {
        return mucocutaneous_margin_start_1;
    }

    public void setMucocutaneous_margin_start_1(int mucocutaneous_margin_start_1) {
        this.mucocutaneous_margin_start_1 = mucocutaneous_margin_start_1;
    }

    public int getMucocutaneous_margin_end_1() {
        return mucocutaneous_margin_end_1;
    }

    public void setMucocutaneous_margin_end_1(int mucocutaneous_margin_end_1) {
        this.mucocutaneous_margin_end_1 = mucocutaneous_margin_end_1;
    }

    public int getMucocutaneous_margin_cm_1() {
        return mucocutaneous_margin_cm_1;
    }

    public void setMucocutaneous_margin_cm_1(int mucocutaneous_margin_cm_1) {
        this.mucocutaneous_margin_cm_1 = mucocutaneous_margin_cm_1;
    }

    public int getMucocutaneous_margin_mm_1() {
        return mucocutaneous_margin_mm_1;
    }

    public void setMucocutaneous_margin_mm_1(int mucocutaneous_margin_mm_1) {
        this.mucocutaneous_margin_mm_1 = mucocutaneous_margin_mm_1;
    }

    public int getMucocutaneous_margin_start_10() {
        return mucocutaneous_margin_start_10;
    }

    public void setMucocutaneous_margin_start_10(int mucocutaneous_margin_start_10) {
        this.mucocutaneous_margin_start_10 = mucocutaneous_margin_start_10;
    }

    public int getMucocutaneous_margin_end_10() {
        return mucocutaneous_margin_end_10;
    }

    public void setMucocutaneous_margin_end_10(int mucocutaneous_margin_end_10) {
        this.mucocutaneous_margin_end_10 = mucocutaneous_margin_end_10;
    }

    public int getMucocutaneous_margin_cm_10() {
        return mucocutaneous_margin_cm_10;
    }

    public void setMucocutaneous_margin_cm_10(int mucocutaneous_margin_cm_10) {
        this.mucocutaneous_margin_cm_10 = mucocutaneous_margin_cm_10;
    }

    public int getMucocutaneous_margin_mm_10() {
        return mucocutaneous_margin_mm_10;
    }

    public void setMucocutaneous_margin_mm_10(int mucocutaneous_margin_mm_10) {
        this.mucocutaneous_margin_mm_10 = mucocutaneous_margin_mm_10;
    }

    public int getMucocutaneous_margin_start_2() {
        return mucocutaneous_margin_start_2;
    }

    public void setMucocutaneous_margin_start_2(int mucocutaneous_margin_start_2) {
        this.mucocutaneous_margin_start_2 = mucocutaneous_margin_start_2;
    }

    public int getMucocutaneous_margin_end_2() {
        return mucocutaneous_margin_end_2;
    }

    public void setMucocutaneous_margin_end_2(int mucocutaneous_margin_end_2) {
        this.mucocutaneous_margin_end_2 = mucocutaneous_margin_end_2;
    }

    public int getMucocutaneous_margin_cm_2() {
        return mucocutaneous_margin_cm_2;
    }

    public void setMucocutaneous_margin_cm_2(int mucocutaneous_margin_cm_2) {
        this.mucocutaneous_margin_cm_2 = mucocutaneous_margin_cm_2;
    }

    public int getMucocutaneous_margin_mm_2() {
        return mucocutaneous_margin_mm_2;
    }

    public void setMucocutaneous_margin_mm_2(int mucocutaneous_margin_mm_2) {
        this.mucocutaneous_margin_mm_2 = mucocutaneous_margin_mm_2;
    }

    public int getMucocutaneous_margin_start_3() {
        return mucocutaneous_margin_start_3;
    }

    public void setMucocutaneous_margin_start_3(int mucocutaneous_margin_start_3) {
        this.mucocutaneous_margin_start_3 = mucocutaneous_margin_start_3;
    }

    public int getMucocutaneous_margin_end_3() {
        return mucocutaneous_margin_end_3;
    }

    public void setMucocutaneous_margin_end_3(int mucocutaneous_margin_end_3) {
        this.mucocutaneous_margin_end_3 = mucocutaneous_margin_end_3;
    }

    public int getMucocutaneous_margin_cm_3() {
        return mucocutaneous_margin_cm_3;
    }

    public void setMucocutaneous_margin_cm_3(int mucocutaneous_margin_cm_3) {
        this.mucocutaneous_margin_cm_3 = mucocutaneous_margin_cm_3;
    }

    public int getMucocutaneous_margin_mm_3() {
        return mucocutaneous_margin_mm_3;
    }

    public void setMucocutaneous_margin_mm_3(int mucocutaneous_margin_mm_3) {
        this.mucocutaneous_margin_mm_3 = mucocutaneous_margin_mm_3;
    }

    public int getMucocutaneous_margin_start_4() {
        return mucocutaneous_margin_start_4;
    }

    public void setMucocutaneous_margin_start_4(int mucocutaneous_margin_start_4) {
        this.mucocutaneous_margin_start_4 = mucocutaneous_margin_start_4;
    }

    public int getMucocutaneous_margin_end_4() {
        return mucocutaneous_margin_end_4;
    }

    public void setMucocutaneous_margin_end_4(int mucocutaneous_margin_end_4) {
        this.mucocutaneous_margin_end_4 = mucocutaneous_margin_end_4;
    }

    public int getMucocutaneous_margin_cm_4() {
        return mucocutaneous_margin_cm_4;
    }

    public void setMucocutaneous_margin_cm_4(int mucocutaneous_margin_cm_4) {
        this.mucocutaneous_margin_cm_4 = mucocutaneous_margin_cm_4;
    }

    public int getMucocutaneous_margin_mm_4() {
        return mucocutaneous_margin_mm_4;
    }

    public void setMucocutaneous_margin_mm_4(int mucocutaneous_margin_mm_4) {
        this.mucocutaneous_margin_mm_4 = mucocutaneous_margin_mm_4;
    }

    public int getMucocutaneous_margin_start_5() {
        return mucocutaneous_margin_start_5;
    }

    public void setMucocutaneous_margin_start_5(int mucocutaneous_margin_start_5) {
        this.mucocutaneous_margin_start_5 = mucocutaneous_margin_start_5;
    }

    public int getMucocutaneous_margin_end_5() {
        return mucocutaneous_margin_end_5;
    }

    public void setMucocutaneous_margin_end_5(int mucocutaneous_margin_end_5) {
        this.mucocutaneous_margin_end_5 = mucocutaneous_margin_end_5;
    }

    public int getMucocutaneous_margin_cm_5() {
        return mucocutaneous_margin_cm_5;
    }

    public void setMucocutaneous_margin_cm_5(int mucocutaneous_margin_cm_5) {
        this.mucocutaneous_margin_cm_5 = mucocutaneous_margin_cm_5;
    }

    public int getMucocutaneous_margin_mm_5() {
        return mucocutaneous_margin_mm_5;
    }

    public void setMucocutaneous_margin_mm_5(int mucocutaneous_margin_mm_5) {
        this.mucocutaneous_margin_mm_5 = mucocutaneous_margin_mm_5;
    }

    public int getMucocutaneous_margin_start_6() {
        return mucocutaneous_margin_start_6;
    }

    public void setMucocutaneous_margin_start_6(int mucocutaneous_margin_start_6) {
        this.mucocutaneous_margin_start_6 = mucocutaneous_margin_start_6;
    }

    public int getMucocutaneous_margin_end_6() {
        return mucocutaneous_margin_end_6;
    }

    public void setMucocutaneous_margin_end_6(int mucocutaneous_margin_end_6) {
        this.mucocutaneous_margin_end_6 = mucocutaneous_margin_end_6;
    }

    public int getMucocutaneous_margin_cm_6() {
        return mucocutaneous_margin_cm_6;
    }

    public void setMucocutaneous_margin_cm_6(int mucocutaneous_margin_cm_6) {
        this.mucocutaneous_margin_cm_6 = mucocutaneous_margin_cm_6;
    }

    public int getMucocutaneous_margin_mm_6() {
        return mucocutaneous_margin_mm_6;
    }

    public void setMucocutaneous_margin_mm_6(int mucocutaneous_margin_mm_6) {
        this.mucocutaneous_margin_mm_6 = mucocutaneous_margin_mm_6;
    }

    public int getMucocutaneous_margin_start_7() {
        return mucocutaneous_margin_start_7;
    }

    public void setMucocutaneous_margin_start_7(int mucocutaneous_margin_start_7) {
        this.mucocutaneous_margin_start_7 = mucocutaneous_margin_start_7;
    }

    public int getMucocutaneous_margin_end_7() {
        return mucocutaneous_margin_end_7;
    }

    public void setMucocutaneous_margin_end_7(int mucocutaneous_margin_end_7) {
        this.mucocutaneous_margin_end_7 = mucocutaneous_margin_end_7;
    }

    public int getMucocutaneous_margin_cm_7() {
        return mucocutaneous_margin_cm_7;
    }

    public void setMucocutaneous_margin_cm_7(int mucocutaneous_margin_cm_7) {
        this.mucocutaneous_margin_cm_7 = mucocutaneous_margin_cm_7;
    }

    public int getMucocutaneous_margin_mm_7() {
        return mucocutaneous_margin_mm_7;
    }

    public void setMucocutaneous_margin_mm_7(int mucocutaneous_margin_mm_7) {
        this.mucocutaneous_margin_mm_7 = mucocutaneous_margin_mm_7;
    }

    public int getMucocutaneous_margin_start_8() {
        return mucocutaneous_margin_start_8;
    }

    public void setMucocutaneous_margin_start_8(int mucocutaneous_margin_start_8) {
        this.mucocutaneous_margin_start_8 = mucocutaneous_margin_start_8;
    }

    public int getMucocutaneous_margin_end_8() {
        return mucocutaneous_margin_end_8;
    }

    public void setMucocutaneous_margin_end_8(int mucocutaneous_margin_end_8) {
        this.mucocutaneous_margin_end_8 = mucocutaneous_margin_end_8;
    }

    public int getMucocutaneous_margin_cm_8() {
        return mucocutaneous_margin_cm_8;
    }

    public void setMucocutaneous_margin_cm_8(int mucocutaneous_margin_cm_8) {
        this.mucocutaneous_margin_cm_8 = mucocutaneous_margin_cm_8;
    }

    public int getMucocutaneous_margin_mm_8() {
        return mucocutaneous_margin_mm_8;
    }

    public void setMucocutaneous_margin_mm_8(int mucocutaneous_margin_mm_8) {
        this.mucocutaneous_margin_mm_8 = mucocutaneous_margin_mm_8;
    }

    public int getMucocutaneous_margin_start_9() {
        return mucocutaneous_margin_start_9;
    }

    public void setMucocutaneous_margin_start_9(int mucocutaneous_margin_start_9) {
        this.mucocutaneous_margin_start_9 = mucocutaneous_margin_start_9;
    }

    public int getMucocutaneous_margin_end_9() {
        return mucocutaneous_margin_end_9;
    }

    public void setMucocutaneous_margin_end_9(int mucocutaneous_margin_end_9) {
        this.mucocutaneous_margin_end_9 = mucocutaneous_margin_end_9;
    }

    public int getMucocutaneous_margin_cm_9() {
        return mucocutaneous_margin_cm_9;
    }

    public void setMucocutaneous_margin_cm_9(int mucocutaneous_margin_cm_9) {
        this.mucocutaneous_margin_cm_9 = mucocutaneous_margin_cm_9;
    }

    public int getMucocutaneous_margin_mm_9() {
        return mucocutaneous_margin_mm_9;
    }

    public void setMucocutaneous_margin_mm_9(int mucocutaneous_margin_mm_9) {
        this.mucocutaneous_margin_mm_9 = mucocutaneous_margin_mm_9;
    }

    public int getFull_assessment() {
        return full_assessment;
    }

    public void setFull_assessment(int full_assessment) {
        this.full_assessment = full_assessment;
    }

    /**
     * @return the image_array3
     */
    public String getImage_array3() {
        return image_array3;
    }

    /**
     * @param image_array3 the image_array3 to set
     */
    public void setImage_array3(String image_array3) {
        this.image_array3 = image_array3;
    }

}
