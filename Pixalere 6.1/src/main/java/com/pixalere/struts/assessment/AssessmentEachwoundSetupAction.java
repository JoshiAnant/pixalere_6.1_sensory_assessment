package com.pixalere.struts.assessment;

import java.util.Hashtable;
import java.util.List;
import java.util.Date;
import java.util.Vector;
import com.pixalere.utils.PDate;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.bean.SummaryVO;
import java.util.Collection;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Common;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;

import java.util.ArrayList;

public class AssessmentEachwoundSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentEachwoundSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        log.info("=-=-=-=-=-=-=-=-=-=-=-- AssessmentEachwoundSetupAction - Starting =-=-=-=-=-=-=-=-=-=-=-- ");
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {

            
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String alpha_id = (String) session.getAttribute("alpha_id");

            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            //Generate Token - prevent multiple posts
            saveToken(request);
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            
            ReportBuilder reportService = new ReportBuilder(language);
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentServiceImpl waManager = new WoundAssessmentServiceImpl();
            AssessmentImagesServiceImpl imanager = new AssessmentImagesServiceImpl();
            PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
            PatientProfileVO tmpProfile = pmanager.getTemporaryPatientProfile(Integer.parseInt(patient_id), userVO.getId());
            if(tmpProfile==null){
                PatientProfileVO pp = new PatientProfileVO();
                pp.setPatient_id(new Integer(patient_id));
                pp.setCurrent_flag(new Integer(1));
                pp.setActive(new Integer(1));
                pp.setDeleted(new Integer(0));
                tmpProfile = pmanager.getPatientProfile(pp);
            }
                Collection resultsPP = new ArrayList();
                List<FieldValues> comparedppdata = null;
                if (tmpProfile != null) {
                    resultsPP.add(tmpProfile);
                    RowData[] ppRecord = pmanager.getAllPatientProfilesForFlowchart(resultsPP, userVO, false,false);
                    comparedppdata = Common.compareGlobal(ppRecord);//Getting flowchart data.
                }
                request.setAttribute("patient_profile",reportService.organizeSummary(comparedppdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.patientprofile", locale)));
            
            
            String assessment_id = null;
            WoundAssessmentVO atmp = new WoundAssessmentVO();
            atmp.setWound_id(new Integer(wound_id));
            atmp.setActive(0);
            atmp.setProfessional_id(userVO.getId());
            WoundAssessmentVO tmpAssess = waManager.getAssessment(atmp);
            if (tmpAssess != null) {
                assessment_id = tmpAssess.getId() + "";
            }
            //String alphaid = (String) session.getAttribute("alpha_id");
            request.setAttribute("page", "assess");
            // shouldnt be here if we havent gone to wound profile first.
            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            comp.setFlowchart(com.pixalere.utils.Constants.WOUND_ASSESSMENT);
            Collection components = gui.getAllComponents(comp);
            Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);
            if (wound_id == null || ((String) wound_id).length() == 0) {
                ActionErrors errors = new ActionErrors();
                errors.add("nowound_id", new ActionError("pixalere.assessment.form.error.nowound_id"));
                saveErrors(request, errors);
                return (mapping.findForward("uploader.assessment.failure"));
            }
            request.setAttribute("offline",
                    (com.pixalere.utils.Common.isOffline() == true ? "1" : "0"));
            //Vector alphas = (Vector) request.getAttribute("alphas");
            WoundServiceImpl woundBD = new WoundServiceImpl(language);

            AssessmentEachwoundVO vo = new AssessmentEachwoundVO();
            // User selected Populate previous
            if (request.getParameter("pop") != null) {
                AssessmentEachwoundVO assess = new AssessmentEachwoundVO();
                assess.setAlpha_id(new Integer(alpha_id));
                assess.setWound_id(new Integer((String) wound_id));
                AssessmentEachwoundVO fullVO = null;
                if (request.getParameter("full") != null && ((String) request.getParameter("full")).equals("1")) {
                    fullVO = (AssessmentEachwoundVO) manager.retrieveLastActiveAssessment(assess, Constants.WOUND_PROFILE_TYPE, 1);
                    
                }
                vo = (AssessmentEachwoundVO) manager.retrieveLastActiveAssessment(assess, Constants.WOUND_PROFILE_TYPE, -1);
                
                
                
                if (fullVO != null && vo != null) {
                    vo.setFull_assessment(new Integer((String) request.getParameter("full")));
                    vo.setLength(fullVO.getLength());
                    vo.setWidth(fullVO.getWidth());
                    vo.setDepth(fullVO.getDepth());
                    vo.setSinustracts(fullVO.getSinustracts());
                    vo.setUnderminings(fullVO.getUnderminings());
                } else if (request.getParameter("full") != null && ((String) request.getParameter("full")).equals("0") && vo != null) {
                    vo.setFull_assessment(new Integer((String) request.getParameter("full")));
                    vo.setLength(null);
                    vo.setWidth(null);
                    vo.setDepth(null);
                    vo.setSinustracts(null);
                    vo.setUnderminings(null);
                }
                if (vo != null) {
                    // dimensions should not be part of populate previous
                    // set assessment object to form
                    request.setAttribute("assessment", vo);
                    request.setAttribute("sinustracts", vo.getSinustracts());
                    request.setAttribute("undermining", vo.getUnderminings());
                    request.setAttribute("pain_comments", Common.convertCarriageReturns(vo.getPain_comments(), true));
                    request.setAttribute("unserializedColour", Serialize.arrayIze(vo.getExudate_colour()));
                    request.setAttribute("unserializedPeriwound", Serialize.arrayIze(vo.getPeriwound_skin()));
                    request.setAttribute("unserializedWoundEdge", Serialize.arrayIze(vo.getWound_edge()));
                    request.setAttribute("unserializedExudateType", Serialize.arrayIze(vo.getExudate_type()));
                    request.setAttribute("unserializedFistulas", Serialize.unserializeStraight(Common.convertCarriageReturns(vo.getFistulas(), true)));
                }
                if (assessment_id != null) {
                    AssessmentImagesVO imgt = new AssessmentImagesVO();
                    imgt.setAssessment_id(new Integer(assessment_id));
                    imgt.setAlpha_id(new Integer(alpha_id));
                    imgt.setWhichImage(Constants.IMAGE_1);
                    AssessmentImagesVO image = imanager.getImage(imgt);
                    imgt.setWhichImage(Constants.IMAGE_2);
                    AssessmentImagesVO extraImage = imanager.getImage(imgt);
                    imgt.setWhichImage(Constants.IMAGE_3);
                    AssessmentImagesVO extraImage2 = imanager.getImage(imgt);
                    if (image != null && image.getImages() != null) {
                        request.setAttribute("image_array", image.getImages());
                    }
                    if (extraImage != null && extraImage.getImages() != null) {
                        request.setAttribute("image_array2", extraImage.getImages());
                    }
                    
                    if (extraImage2 != null && extraImage2.getImages() != null) {
                        request.setAttribute("image_array3", extraImage2.getImages());
                    }
                }
            } else {
                // User is returning from another page,.. need to populate
                // assessment with current work
                com.pixalere.assessment.bean.AssessmentImagesVO image = null;
                com.pixalere.assessment.bean.AssessmentImagesVO extraImage = null;
                AssessmentImagesVO extraImage2 = null;
                vo = null;
                try {
                    if (assessment_id != null) {
                        AssessmentEachwoundVO assessVO = new AssessmentEachwoundVO();
                        assessVO.setAssessment_id(new Integer(assessment_id));
                        assessVO.setAlpha_id(new Integer(alpha_id));
                        vo = (AssessmentEachwoundVO) manager.getAssessment(assessVO);
                        AssessmentImagesVO ig = new AssessmentImagesVO();
                        ig.setAssessment_id(new Integer(assessment_id));
                        ig.setAlpha_id(new Integer(alpha_id));
                        ig.setWhichImage(Constants.IMAGE_1);
                        image = imanager.getImage(ig);
                        ig.setWhichImage(Constants.IMAGE_2);
                        extraImage = imanager.getImage(ig);
                    ig.setWhichImage(Constants.IMAGE_3);
                    extraImage2 = imanager.getImage(ig);
                    }
                } catch (ApplicationException ex) {
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(ex));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
                if (image != null && image.getImages() != null) {
                    request.setAttribute("image_array", image.getImages());
                }
                if (extraImage != null && extraImage.getImages() != null) {
                    request.setAttribute("image_array2", extraImage.getImages());
                }
                    if (extraImage2 != null && extraImage2.getImages() != null) {
                        request.setAttribute("image_array3", extraImage2.getImages());
                    }
                if (vo != null) {
                    log.info("##$$##$$##-----THERE IS PREVIOUS ASSESSMENT INFORMATION THAT HAS NOT BEEN COMMITTED IN THIS SESSION AND POP IS NULL-----##$$##$$##");
                    request.setAttribute("assessment", vo);
                    request.setAttribute("date_of_onset", vo.getAgeofWoundDate());
                    request.setAttribute("pain_comments", Common.convertCarriageReturns(vo.getPain_comments(), true));
                    request.setAttribute("sinustracts", vo.getSinustracts());
                    request.setAttribute("undermining", vo.getUnderminings());
                    request.setAttribute("unserializedColour", Serialize.arrayIze(vo.getExudate_colour()));
                    request.setAttribute("unserializedPeriwound", Serialize.arrayIze(vo.getPeriwound_skin()));
                    request.setAttribute("unserializedWoundEdge", Serialize.arrayIze(vo.getWound_edge()));
                    request.setAttribute("unserializedExudateType", Serialize.arrayIze(vo.getExudate_type()));
                    request.setAttribute("unserializedFistulas", Serialize.unserializeStraight(vo.getFistulas()));
                }
            }
            request.setAttribute("isRequired", "1");
            log.info("##$$##$$##-----GRABBING THE USER SIGNATURE-----##$$##$$##");
            AssessmentEachwoundVO assess = new AssessmentEachwoundVO();
            assess.setAlpha_id(new Integer(alpha_id));
            assess.setWound_id(new Integer(wound_id));
            //assess.setFull_assessment(new Integer("-1"));
            assess.setActive(1);
            AssessmentEachwoundVO voUpdate = (AssessmentEachwoundVO) manager.getAssessment(assess);
            if (voUpdate != null) {
                AssessmentCommentsServiceImpl cservice = new AssessmentCommentsServiceImpl();
                //Find outstanding recommendations.
                ReferralsTrackingServiceImpl rservice=new ReferralsTrackingServiceImpl(language);
                if(((String)request.getParameter("action"))!=null &&((String)request.getParameter("action")).equals("acknowledge") ){
                    ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                    rtmp.setWound_profile_type_id(voUpdate.getWound_profile_type_id());
                    rtmp.setEntry_type(Constants.RECOMMENDATION);
                    rtmp.setCurrent(Constants.ACTIVE);
                    Collection<ReferralsTrackingVO> referrals = rservice.getAllReferralsByCriteria(rtmp);
                    for(ReferralsTrackingVO referral : referrals){
                        if (referral != null) {
                                referral.setCurrent(new Integer(0));
                                rservice.saveReferralsTracking(referral);
                                log.info("PixalereSearch.perform: Inserting new referral (ackknowledgement)");
                                referral.setCurrent(new Integer("1"));
                                referral.setEntry_type(Constants.ACKNOWLEDGE_RECOMMENDATION); // "Acknowledge Recommendation"
                                referral.setCreated_on(new Date());
                                referral.setProfessional_id(userVO.getId());
                                referral.setId(null);
                                rservice.saveReferralsTracking(referral);

                            
                        }
                    }
                }
                ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                rtmp.setWound_profile_type_id(voUpdate.getWound_profile_type_id());
                rtmp.setEntry_type(Constants.RECOMMENDATION);
                rtmp.setCurrent(Constants.ACTIVE);
                Collection<ReferralsTrackingVO> referrals = rservice.getAllReferralsByCriteria(rtmp);
                Vector comments = new Vector();
                for(ReferralsTrackingVO referral : referrals){
                    //get recommendations/comments
                    AssessmentCommentsVO ctmp = new AssessmentCommentsVO();
                    if(!referral.getAssessment_id().equals(-1)){
                    ctmp.setAssessment_id(referral.getAssessment_id());
                    }else{
                        ctmp.setReferral_id(referral.getId());
                    }
                    Collection<AssessmentCommentsVO> comments2 = cservice.getAllComments(ctmp);
                    for(AssessmentCommentsVO comment : comments2){
                        comments.add(comment);
                    }
                }
                request.setAttribute("comments",comments);


                log.info("##$$##$$##-----THERE HAS BEEN A PREVIOUS ASSESSMENT WITH A SIGNATURE-----##$$##$$##");
                WoundAssessmentVO assessVO = voUpdate.getWoundAssessment();
                if (assessVO != null) {
                    request.setAttribute("date_of_onset", voUpdate.getAgeofWoundDate());
                    request.setAttribute("user_signature", assessVO.getUser_signature());
                    request.setAttribute("lastassess_date",assessVO.getVisited_on());
                    ProfessionalVO userVO2 = assessVO.getProfessional();
                    if (userVO2 != null) {
                        request.setAttribute("profName", userVO2.getFullName());
                    }
                } else {
                    request.setAttribute("user_signature", Common.getLocalizedString("pixalere.na",locale));
                }
            } else {
                log.info("##$$##$$##-----NO PREVIOUS ASSESSMENT, SIGNATURE IS N/A-----##$$##$$##");
                request.setAttribute("user_signature", Common.getLocalizedString("pixalere.na",locale));
            }
            // Populate Lists
            log.info("##$$##$$##-----INITIALIZE ALL PAGE CONTROLS-----##$$##$$##");
            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("pain", listBD.getLists(LookupVO.PAIN));
            request.setAttribute("exudate_type", listBD.getLists(LookupVO.EXUDATE_TYPE));
            request.setAttribute("exudate_amount", listBD.getLists(LookupVO.EXUDATE_AMOUNT));
            request.setAttribute("odour", listBD.getLists(LookupVO.ODOUR));
            request.setAttribute("wound_edge", listBD.getLists(LookupVO.WOUND_EDGE));
            request.setAttribute("wound_status", listBD.getLists(LookupVO.WOUND_STATUS));
            request.setAttribute("discharge_reason", listBD.getLists(LookupVO.WOUND_DISCHARGE_REASON));
            request.setAttribute("wound_bed", listBD.getLists(LookupVO.WOUND_BED));
            request.setAttribute("wound_base_percentage", listBD.getLists(LookupVO.WOUND_BED_PERCENT));
            request.setAttribute("colour", listBD.getLists(LookupVO.COLOUR));
            request.setAttribute("periwound_skin", listBD.getLists(LookupVO.PERIWOUND_SKIN));
            request.setAttribute("fistulas", listBD.getLists(LookupVO.FISTULA));
            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
            tloco.setWound_id(new Integer((String) wound_id));
            tloco.setDischarge(0); // Only open alphas
            Vector<WoundAssessmentLocationVO> allalphas = walservice.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, true);//4th parameter (extra sort) is ignored
            //TreeMap alphaMap = new TreeMap();
            //for (WoundAssessmentLocationVO v : allalphas) {
            //    alphaMap.put(v.getAlpha(), v);
            //}
            request.setAttribute("allalphas", allalphas);
            if (assessment_id != null && assessment_id.length() > 0) {

                //list of unfinished alphas ("A", "B", "C",...)
                AssessmentEachwoundVO t = new AssessmentEachwoundVO();
                t.setWound_id(new Integer(wound_id));
                t.setAssessment_id(new Integer(assessment_id));
                List<String> unfinishedAlphas = walManager.computeUncompletedAlphas(t, userVO.getId());
                StringBuilder unfinishedAlphasString = new StringBuilder();
                List<WoundAssessmentLocationVO> completedAssess = new java.util.ArrayList();
                for(WoundAssessmentLocationVO a : allalphas){completedAssess.add(a);}
                //get completed assessements for checkmark
                for(int i = completedAssess.size()-1;i>=0;i--){
                    WoundAssessmentLocationVO v = completedAssess.get(i);
                    for (String unfinishedAlpha : unfinishedAlphas) {
                        if(v.getAlpha().equals(unfinishedAlpha)){
                            completedAssess.remove(i);//remove alpha from completed list, as its unfinished
                        }
                    }
                }
                
              
                request.setAttribute("completed_assess",completedAssess);
                if (unfinishedAlphas != null) {
                    for (String unfinishedAlpha : unfinishedAlphas) {
                        unfinishedAlphasString.append('\"');
                        unfinishedAlphasString.append(unfinishedAlpha);
                        unfinishedAlphasString.append('\"');
                        unfinishedAlphasString.append(',');
                        
                    }
                }
                
                if (unfinishedAlphas != null && unfinishedAlphas.size() > 0) {
                    request.setAttribute("unfinishedAlphas", unfinishedAlphasString.substring(0, unfinishedAlphasString.length() - 1));
                } else {
                    request.setAttribute("unfinishedAlphas", "");
                }
            } else {
                //a null assessment id means that no assessments have been completed
                Vector<WoundAssessmentLocationVO> allAlphas = walManager.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, false);
                StringBuilder unfinishedAlphasString = new StringBuilder();
                for (WoundAssessmentLocationVO loc : allAlphas) {
                    unfinishedAlphasString.append('\"');
                    unfinishedAlphasString.append(loc.getAlpha());
                    unfinishedAlphasString.append('\"');
                    unfinishedAlphasString.append(',');
                    if (allAlphas.size() > 0) {
                        //cut off the trailing comma
                        request.setAttribute("unfinishedAlphas", unfinishedAlphasString.substring(0, unfinishedAlphasString.length() - 1));
                    } else {
                        request.setAttribute("unfinishedAlphas", "");
                    }
                }
            }
            //get bluemodel image
            if (wound_id != null && wound_id.length() > 0) {
                WoundProfileVO woundVO2 = new WoundProfileVO();
                woundVO2.setWound_id(new Integer(wound_id));
                woundVO2.setActive(new Integer(0));
                woundVO2.setProfessional_id(userVO.getId());
                WoundProfileVO woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                if (woundVO == null) {
                    woundVO2 = new WoundProfileVO();
                    woundVO2.setWound_id(new Integer(wound_id));
                    woundVO2.setCurrent_flag(new Integer(1));
                    woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                }
                //get Goal of alpha
                GoalsVO gotmp = new GoalsVO();
                gotmp.setWound_profile_id(woundVO.getId());
                gotmp.setAlpha_id(new Integer(alpha_id));
                GoalsVO goal = woundBD.getGoal(gotmp);
                request.setAttribute("goal",goal);
                request.setAttribute("wound_profile", woundVO.getWound());
                Collection resultsWP = new ArrayList();
                List<FieldValues> comparedwpdata = null;
                if (woundVO != null) {
                    resultsWP.add(woundVO);
                    RowData[] wpRecord = woundBD.getAllWoundProfilesForFlowchart(resultsWP, userVO, false,false);
                    comparedwpdata = Common.compareGlobal(wpRecord);//Getting flowchart data.
                }
                request.setAttribute("wound_profile_readonly",reportService.organizeSummary(comparedwpdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.woundprofile", locale)));
            }

            if (assessment_id != null) {
                request.setAttribute("assessment_id", assessment_id);
            } else {
                //create one
                WoundAssessmentVO assess2 = new WoundAssessmentVO();
                assess2.setPatient_id(new Integer(patient_id));
                assess2.setWound_id(new Integer(wound_id));
                assess2.setProfessional_id(new Integer(userVO.getId() + ""));
                assess2.setCreated_on(new Date());
                assess2.setActive(new Integer(0));
                assess2.setOffline_flag(Common.isOffline() == true ? 1 : 0);
                String user_signature = pdate.getProfessionalSignature(new Date(), userVO,locale);
                assess2.setUser_signature(user_signature);
                PatientAccountVO patientAcc = (PatientAccountVO) session.getAttribute("patientAccount");
                if(patientAcc!=null){
                    assess2.setTreatment_location_id(patientAcc.getTreatment_location_id());
                    waManager.saveAssessment(assess2);
                    WoundAssessmentVO wat = new WoundAssessmentVO();
                    wat.setWound_id(new Integer(wound_id));
                    wat.setProfessional_id(userVO.getId());
                    WoundAssessmentVO assessmentObject = waManager.getAssessment(wat);
                    if (assessmentObject != null) {
                        request.setAttribute("assessment_id", assessmentObject.getId());
                    }
                }
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in AssessmentEachwoundSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        String anchor = (String) request.getAttribute("anchor");
        request.setAttribute("anchor", anchor);
        log.info(
                "=-=-=-=-=-=-=-=-=-=-=-- AssessmentEachwoundSetupAction - Ending =-=-=-=-=-=-=-=-=-=-=-- ");

        if (anchor != null && anchor.equals(
                "anchor_fistula")) {
            return (mapping.findForward("uploader.assessment.fistula"));
        } else if (anchor != null && anchor.equals(
                "anchor_undermining")) {
            return (mapping.findForward("uploader.assessment.undermining"));
        } else if (anchor != null && anchor.equals(
                "anchor_sinus")) {
            return (mapping.findForward("uploader.assessment.sinus"));
        } else {
            return (mapping.findForward("uploader.assessment.success"));
        }
    }
}
