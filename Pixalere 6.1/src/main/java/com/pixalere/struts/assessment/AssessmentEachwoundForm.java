package com.pixalere.struts.assessment;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentWoundBedVO;
import com.pixalere.assessment.bean.AssessmentUnderminingVO;
import com.pixalere.assessment.bean.AssessmentSinustractsVO;
import com.pixalere.utils.*;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Constants;
import java.util.Date;
/**
 * @author
 */
public class AssessmentEachwoundForm extends ActionForm {
    private int phone_visit;
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentEachwoundForm.class);
    private int full_assessment;
    private int undermining_end_4;
    private String laser;
    private int undermining_end_5;
    private String pain_comments;
    private int undermining_end_6;
    private int cs_date_day;
    private int cs_date_month;
    private int cs_date_year;
    private int cs_date_show;
    private int undermining_end_7;
    private Integer push_scale;
    private double skin_temperature;
    
    private int undermining_end_8;
    
    private int undermining_end_9;
    
    private int undermining_end_10;
    private String fistulas_1;
    private String fistulas_2;
    private String fistulas_3;
    private String fistulas_4;
    private String fistulas_5;
    private String fistulas_6;
    private String fistulas_7;
    private String fistulas_8;
    private String fistulas_9;
    private String fistulas_10;
    private int num_fistulas;
    private int wound_date_day;
    private int wound_date_month;
    private int wound_date_year;
    private int num_under;
    
    private int num_sinus_tracts;
    
    private int undermining_start;
    
    private int undermining_end;
    
    private String[] wound_edge;
    
    private String[] periwound;
    
    private String wound_depth;
    
    private int exudate_amount;
    
    private String[] exudate_colour;
    
    private Integer exudate_odour;
    
    
    
    private String discharge_other;
    
    private int status;
    private int closed_day;
    private int closed_month;
    private int closed_year;
    private int target_discharge_date_day;
    private int target_discharge_date_month;
    private int target_discharge_date_year;
    private String[] exudate_type;
    
    private String healrate_timestamp;
    
    private int healrate_percent;
    
    private int discharge_reason;
    
    private String image_array;
    private String image_array2;
    private String image_array3;
    private FormFile image_1;
    private FormFile image_2;
    private FormFile image_3;
    public FormFile getImage_3() {
        return image_3;
    }
    public void setImage_3(FormFile image_3) {
        this.image_3 = image_3;
    }
    
    
    private int pain;
    
    private double length;
    
    private double width;
    
    private double depth;
    

    
    private int undermining_depth;
    
    private int undermining_depth_milli;
    
    private String[] woundbase;
    
    private String woundbase_percentage;
    
    
    private String[] colour;
    
    private Integer recurrent;
    
    private int odour;
    
    private int sinusdepth_1;
    
    private int sinusdepth_2;
    
    private int sinusdepth_3;
    
    private int sinusdepth_4;
    
    private int sinusdepth_5;
    
    private int sinusdepth_6;
    
    private int sinusdepth_7;
    
    private int sinusdepth_8;
    
    private int sinusdepth_9;
    
    private int sinusdepth_10;
    
    private int sinusdepth_mini_1;
    
    private int sinusdepth_mini_2;
    
    private int sinusdepth_mini_3;
    
    private int sinusdepth_mini_4;
    
    private int sinusdepth_mini_5;
    
    private int sinusdepth_mini_6;
    
    private int sinusdepth_mini_7;
    
    private int sinusdepth_mini_8;
    
    private int sinusdepth_mini_9;
    
    private int sinusdepth_mini_10;
    
    private int sinustract_start_1;
    
    private int sinustract_start_2;
    
    private int sinustract_start_3;
    
    private int sinustract_start_4;
    
    private int sinustract_start_5;
    
    private int sinustract_start_6;
    
    private int sinustract_start_7;
    
    private int sinustract_start_8;
    
    private int sinustract_start_9;
    
    private int sinustract_start_10;
    
    private int sinustract_end_1;
    
    private int sinustract_end_2;
    
    private int sinustract_end_3;
    
    private int sinustract_end_4;
    
    private int sinustract_end_5;
    private int sinustract_end_6;
    
    private int sinustract_end_7;
    
    private int sinustract_end_8;
    
    private int sinustract_end_9;
    
    private int sinustract_end_10;
    
    private int underminingdepth_1;
    
    private int underminingdepth_2;
    
    private int underminingdepth_3;
    
    private int underminingdepth_4;
    
    private int underminingdepth_5;
    
    private int underminingdepth_6;
    
    private int underminingdepth_7;
    
    private int underminingdepth_8;
    
    private int underminingdepth_9;
    
    private int underminingdepth_10;
    
    private int underminingdepth_mini_1;
    
    private int underminingdepth_mini_2;
    
    private int underminingdepth_mini_3;
    
    private int underminingdepth_mini_4;
    
    private int underminingdepth_mini_5;
    
    private int underminingdepth_mini_6;
    
    private int underminingdepth_mini_7;
    
    private int underminingdepth_mini_8;
    
    private int underminingdepth_mini_9;
    
    private int underminingdepth_mini_10;
    
    private int undermining_start_1;
    
    private int undermining_start_2;
    
    private int undermining_start_3;
    
    private int undermining_start_4;
    
    private int undermining_start_5;
    
    private int undermining_start_6;
    
    private int undermining_start_7;
    
    private int undermining_start_8;
    
    private int undermining_start_9;
    
    private int undermining_start_10;
    
    private int undermining_end_1;
    
    private int undermining_end_2;
    
    private int undermining_end_3;
    
    private String vac;
    private int vacstart_day;
    private int vacstart_month;
    private int vacstart_year;
    private int vacend_day;
    private int vacend_month;
    private int vacend_year;
    private String[] adjunctive;
    private String adjunctive_other;
    private int pressure_reading;
    private String reading_group;
    
    
    
    HttpSession session = null;
  
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
       
        ActionErrors errors = new ActionErrors();
        if (image_1!=null && image_1.getFileName().length()>0 && image_1.getFileSize() == 0) {
            
            errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
        }
        
        if (image_2!=null && image_2.getFileName().length()>0 && image_2.getFileSize() == 0) {
            
            errors.add("file2",new ActionError("pixalere.woundassessment.error.filemissing2"));
        }
        return errors;
        
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    
    /**
     * Returns the password.
     *
     * @return String
     */
    public int getPain() {
        return pain;
    }
    
    public void setPain(int pain) {
        this.pain = pain;
    }
    
    public void setClosed_day(int closed_day){
        this.closed_day=closed_day;
    }
    public int getClosed_day(){
        return closed_day;
    }
    public void setClosed_month(int closed_month){
        this.closed_month=closed_month;
    }
    public int getClosed_month(){
        return closed_month;
    }
    public void setClosed_year(int closed_year){
        this.closed_year=closed_year;
    }
    public int getClosed_year(){
        return closed_year;
    }
    /**
     * @method getFull_assessment
     * @field full_assessment - int
     * @return Returns the full_assessment.
     */
    public int getFull_assessment() {
        return full_assessment;
    }
    
    /**
     * @method setFull_assessment
     * @field full_assessment - int
     * @param full_assessment The full_assessment to set.
     */
    public void setFull_assessment(int full_assessment) {
        this.full_assessment = full_assessment;
    }
    
    
    public int getUndermining_depth() {
        return undermining_depth;
    }
    
    public void setUndermining_depth(int undermining_depth) {
        this.undermining_depth = undermining_depth;
    }
    
    public int getUndermining_depth_milli() {
        return undermining_depth_milli;
    }
    
    public void setUndermining_depth_milli(int undermining_depth_milli) {
        this.undermining_depth_milli = undermining_depth_milli;
    }
    
    public String[] getWoundbase() {
        return woundbase;
    }
    
    public void setWoundbase(String[] woundbase) {
        this.woundbase = woundbase;
    }
    
    public int getSinusdepth_1() {
        return sinusdepth_1;
    }
    
    public void setSinusdepth_1(int sinusdepth_1) {
        this.sinusdepth_1 = sinusdepth_1;
    }
    
    public int getSinusdepth_2() {
        return sinusdepth_2;
    }
    
    public void setSinusdepth_2(int sinusdepth_2) {
        this.sinusdepth_2 = sinusdepth_2;
    }
    
    public int getSinusdepth_3() {
        return sinusdepth_3;
    }
    
    public void setSinusdepth_3(int sinusdepth_3) {
        this.sinusdepth_3 = sinusdepth_3;
    }
    
    public int getSinusdepth_4() {
        return sinusdepth_4;
    }
    
    public void setSinusdepth_4(int sinusdepth_4) {
        this.sinusdepth_4 = sinusdepth_4;
    }
    
    public int getSinusdepth_5() {
        return sinusdepth_5;
    }
    
    public void setSinusdepth_5(int sinusdepth_5) {
        this.sinusdepth_5 = sinusdepth_5;
    }
    
    public int getSinusdepth_6() {
        return sinusdepth_6;
    }
    
    public void setSinusdepth_6(int sinusdepth_6) {
        this.sinusdepth_6 = sinusdepth_6;
    }
    
    public int getSinusdepth_7() {
        return sinusdepth_7;
    }
    
    public void setSinusdepth_7(int sinusdepth_7) {
        this.sinusdepth_7 = sinusdepth_7;
    }
    
    public int getSinusdepth_8() {
        return sinusdepth_8;
    }
    
    public void setsinusdepth_8(int sinusdepth_8) {
        this.sinusdepth_8 = sinusdepth_8;
    }
    
    public int getSinusdepth_9() {
        return sinusdepth_9;
    }
    
    public void setSinusdepth_9(int sinusdepth_9) {
        this.sinusdepth_9 = sinusdepth_9;
    }
    
    public int getSinusdepth_10() {
        return sinusdepth_10;
    }
    
    public void setSinusdepth_10(int sinusdepth_10) {
        this.sinusdepth_10 = sinusdepth_10;
    }
    
    // *****************
    public int getSinusdepth_mini_1() {
        return sinusdepth_mini_1;
    }
    
    public void setSinusdepth_mini_1(int sinusdepth_mini_1) {
        this.sinusdepth_mini_1 = sinusdepth_mini_1;
    }
    
    public int getSinusdepth_mini_2() {
        return sinusdepth_mini_2;
    }
    
    public void setSinusdepth_mini_2(int sinusdepth_mini_2) {
        this.sinusdepth_mini_2 = sinusdepth_mini_2;
    }
    
    public int getSinusdepth_mini_3() {
        return sinusdepth_mini_3;
    }
    
    public void setSinusdepth_mini_3(int sinusdepth_mini_3) {
        this.sinusdepth_mini_3 = sinusdepth_mini_3;
    }
    
    public int getSinusdepth_mini_4() {
        return sinusdepth_mini_4;
    }
    
    public void setSinusdepth_mini_4(int sinusdepth_mini_4) {
        this.sinusdepth_mini_4 = sinusdepth_mini_4;
    }
    
    public int getSinusdepth_mini_5() {
        return sinusdepth_mini_5;
    }
    
    public void setSinusdepth_mini_5(int sinusdepth_mini_5) {
        this.sinusdepth_mini_5 = sinusdepth_mini_5;
    }
    
    public int getSinusdepth_mini_6() {
        return sinusdepth_mini_6;
    }
    
    public void setSinusdepth_mini_6(int sinusdepth_mini_6) {
        this.sinusdepth_mini_6 = sinusdepth_mini_6;
    }
    
    public int getSinusdepth_mini_7() {
        return sinusdepth_mini_7;
    }
    
    public void setSinusdepth_mini_7(int sinusdepth_mini_7) {
        this.sinusdepth_mini_7 = sinusdepth_mini_7;
    }
    
    public int getSinusdepth_mini_8() {
        return sinusdepth_mini_8;
    }
    
    public void setsinusdepth_mini_8(int sinusdepth_mini_8) {
        this.sinusdepth_mini_8 = sinusdepth_mini_8;
    }
    
    public int getSinusdepth_mini_9() {
        return sinusdepth_mini_9;
    }
    
    public void setSinusdepth_mini_9(int sinusdepth_mini_9) {
        this.sinusdepth_mini_9 = sinusdepth_mini_9;
    }
    
    public int getSinusdepth_mini_10() {
        return sinusdepth_mini_10;
    }
    
    public void setSinusdepth_mini_10(int sinusdepth_mini_10) {
        this.sinusdepth_mini_10 = sinusdepth_mini_10;
    }
    
    // *********************************
    
    // *****************
    public int getSinustract_start_1() {
        return sinustract_start_1;
    }
    
    public void setSinustract_start_1(int sinustract_start_1) {
        this.sinustract_start_1 = sinustract_start_1;
    }
    
    public int getSinustract_start_2() {
        return sinustract_start_2;
    }
    
    public void setSinustract_start_2(int sinustract_start_2) {
        this.sinustract_start_2 = sinustract_start_2;
    }
    
    public int getSinustract_start_3() {
        return sinustract_start_3;
    }
    
    public void setSinustract_start_3(int sinustract_start_3) {
        this.sinustract_start_3 = sinustract_start_3;
    }
    
    public int getSinustract_start_4() {
        return sinustract_start_4;
    }
    
    public void setSinustract_start_4(int sinustract_start_4) {
        this.sinustract_start_4 = sinustract_start_4;
    }
    
    public int getSinustract_start_5() {
        return sinustract_start_5;
    }
    
    public void setSinustract_start_5(int sinustract_start_5) {
        this.sinustract_start_5 = sinustract_start_5;
    }
    
    public int getSinustract_start_6() {
        return sinustract_start_6;
    }
    
    public void setSinustract_start_6(int sinustract_start_6) {
        this.sinustract_start_6 = sinustract_start_6;
    }
    
    public int getSinustract_start_7() {
        return sinustract_start_7;
    }
    
    public void setSinustract_start_7(int sinustract_start_7) {
        this.sinustract_start_7 = sinustract_start_7;
    }
    
    public int getSinustract_start_8() {
        return sinustract_start_8;
    }
    
    public void setSinustract_start_8(int sinustract_start_8) {
        this.sinustract_start_8 = sinustract_start_8;
    }
    
    public int getSinustract_start_9() {
        return sinustract_start_9;
    }
    
    public void setSinustract_start_9(int sinustract_start_9) {
        this.sinustract_start_9 = sinustract_start_9;
    }
    
    public int getSinustract_start_10() {
        return sinustract_start_10;
    }
    
    public void setSinustract_start_10(int sinustract_start_10) {
        this.sinustract_start_10 = sinustract_start_10;
    }
    
    // *********************************
    
    // *****************
    public int getSinustract_end_1() {
        return sinustract_end_1;
    }
    
    public void setSinustract_end_1(int sinustract_end_1) {
        this.sinustract_end_1 = sinustract_end_1;
    }
    
    public int getSinustract_end_2() {
        return sinustract_end_2;
    }
    
    public void setSinustract_end_2(int sinustract_end_2) {
        this.sinustract_end_2 = sinustract_end_2;
    }
    
    public int getSinustract_end_3() {
        return sinustract_end_3;
    }
    
    public void setSinustract_end_3(int sinustract_end_3) {
        this.sinustract_end_3 = sinustract_end_3;
    }
    
    public int getSinustract_end_4() {
        return sinustract_end_4;
    }
    
    public void setSinustract_end_4(int sinustract_end_4) {
        this.sinustract_end_4 = sinustract_end_4;
    }
    
    public int getSinustract_end_5() {
        return sinustract_end_5;
    }
    
    public void setSinustract_end_5(int sinustract_end_5) {
        this.sinustract_end_5 = sinustract_end_5;
    }
    
    public int getSinustract_end_6() {
        return sinustract_end_6;
    }
    
    public void setSinustract_end_6(int sinustract_end_6) {
        this.sinustract_end_6 = sinustract_end_6;
    }
    
    public int getSinustract_end_7() {
        return sinustract_end_7;
    }
    
    public void setSinustract_end_7(int sinustract_end_7) {
        this.sinustract_end_7 = sinustract_end_7;
    }
    
    public int getSinustract_end_8() {
        return sinustract_end_8;
    }
    
    public void setSinustract_end_8(int sinustract_end_8) {
        this.sinustract_end_8 = sinustract_end_8;
    }
    
    public int getSinustract_end_9() {
        return sinustract_end_9;
    }
    
    public void setSinustract_end_9(int sinustract_end_9) {
        this.sinustract_end_9 = sinustract_end_9;
    }
    
    public int getSinustract_end_10() {
        return sinustract_end_10;
    }
    
    public void setSinustract_end_10(int sinustract_end_10) {
        this.sinustract_end_10 = sinustract_end_10;
    }
    
    // *********************************
    
    public int getLocation_start() {
        return undermining_start;
    }
    
    public void setLocation_start(int undermining_start) {
        this.undermining_start = undermining_start;
    }
    
    public int getLocation_end() {
        return undermining_end;
    }
    
    public void setLocation_end(int undermining_end) {
        this.undermining_end = undermining_end;
    }
    
    public String[] getWound_edge() {
        return wound_edge;
    }
    
    public void setWound_edge(String[] wound_edge) {
        this.wound_edge = wound_edge;
    }
    
    public String[] getPeriwound() {
        return periwound;
    }
    
    public void setPeriwound(String[] periwound) {
        this.periwound = periwound;
    }
    
    public int getStatus() {
        return status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    
    public String getWound_depth() {
        return wound_depth;
    }
    
    public void setWound_depth(String wound_depth) {
        this.wound_depth = wound_depth;
    }
    
    public int getExudate_amount() {
        return exudate_amount;
    }
    
    public void setExudate_amount(int exudate_amount) {
        this.exudate_amount = exudate_amount;
    }
    
    public String[] getColour() {
        return colour;
    }
    
    public void setColour(String[] colour) {
        this.colour = colour;
    }
    
    public Integer getExudate_odour() {
        return exudate_odour;
    }
    
    public void setExudate_odour(Integer exudate_odour) {
        this.exudate_odour = exudate_odour;
    }
    
    
    
    public int getNum_sinus_tracts() {
        return num_sinus_tracts;
    }
    
    public void setNum_sinus_tracts(int num_sinus_tracts) {
        this.num_sinus_tracts = num_sinus_tracts;
    }
    
    public String[] getExudate_type() {
        return exudate_type;
    }
    
    public void setExudate_type(String[] exudate_type) {
        this.exudate_type = exudate_type;
    }
    
    public int getHealrate_percent() {
        return healrate_percent;
    }
    
    public void setHealrate_percent(int healrate_percent) {
        this.healrate_percent = healrate_percent;
    }
    
    public String getHealrate_timestamp() {
        return healrate_timestamp;
    }
    
    public void setHealrate_timestamp(String healrate_timestamp) {
        this.healrate_timestamp = healrate_timestamp;
    }
    
    public String getWoundbase_percentage() {
        return woundbase_percentage;
    }
    
    public void setWoundbase_percentage(String woundbase_percentage) {
        this.woundbase_percentage = woundbase_percentage;
    }
    
    public int getDischarge_reason() {
        return discharge_reason;
    }
    
    public void setDischarge_reason(int discharge_reason) {
        this.discharge_reason = discharge_reason;
    }
    
    public List getWoundbasePercentages() throws ApplicationException {
       List v = new ArrayList();
       try {
           if (woundbase_percentage != null) {
               Vector wndbase_percentage = Serialize.unserialize(woundbase_percentage, ",");
               if (woundbase != null) {
                   for (int x = 0; x < woundbase.length; x++) {
                       AssessmentWoundBedVO w = new AssessmentWoundBedVO();
                       if(wndbase_percentage==null || wndbase_percentage.get(x).equals("")){
                           w.setPercentage(new Integer("0"));
                       }else{
                           w.setPercentage(new Integer(wndbase_percentage.get(x)+""));
                       }
                       w.setWound_bed(Integer.parseInt(woundbase[x]));
                       v.add(w);
                   }
               }
           }
       } catch (ArrayIndexOutOfBoundsException e) {
           e.printStackTrace();
           return new ArrayList();//this is not valid likely because of undermining because selected (no validation)
       }
       return v;
   }
    public String getImage_array() {
        return image_array;
    }
    public void setImage_array(String image_array) {
        this.image_array = image_array;
    }
    public FormFile getImage_1() {
        return image_1;
    }
    
    public void setImage_1(FormFile image_1) {
        this.image_1 = image_1;
    }
    
    public FormFile getImage_2() {
        return image_2;
    }
    
    public void setImage_2(FormFile image_2) {
        this.image_2 = image_2;
    }
    
    
    
    /**
     * @method getRecurrent
     * @field recurrent - int
     * @return Returns the recurrent.
     */
    public Integer getRecurrent() {
        return recurrent;
    }
    
    /**
     * @method setRecurrent
     * @field recurrent - int
     * @param recurrent
     *            The recurrent to set.
     */
    public void setRecurrent(Integer recurrent) {
        this.recurrent = recurrent;
    }
    
    public int getUnderminingdepth_1() {
        return underminingdepth_1;
    }
    
    public void setUnderminingdepth_1(int underminingdepth_1) {
        this.underminingdepth_1 = underminingdepth_1;
    }
    
    public int getUnderminingdepth_2() {
        return underminingdepth_2;
    }
    
    public void setUnderminingdepth_2(int underminingdepth_2) {
        this.underminingdepth_2 = underminingdepth_2;
    }
    
    public int getUnderminingdepth_3() {
        return underminingdepth_3;
    }
    
    public void setUnderminingdepth_3(int underminingdepth_3) {
        this.underminingdepth_3 = underminingdepth_3;
    }
    
    public int getUnderminingdepth_4() {
        return underminingdepth_4;
    }
    
    public void setUnderminingdepth_4(int underminingdepth_4) {
        this.underminingdepth_4 = underminingdepth_4;
    }
    
    public int getUnderminingdepth_5() {
        return underminingdepth_5;
    }
    
    public void setUnderminingdepth_5(int underminingdepth_5) {
        this.underminingdepth_5 = underminingdepth_5;
    }
    
    public int getUnderminingdepth_6() {
        return underminingdepth_6;
    }
    
    public void setUnderminingdepth_6(int underminingdepth_6) {
        this.underminingdepth_6 = underminingdepth_6;
    }
    
    public int getUnderminingdepth_7() {
        return underminingdepth_7;
    }
    
    public void setUnderminingdepth_7(int underminingdepth_7) {
        this.underminingdepth_7 = underminingdepth_7;
    }
    
    public int getUnderminingdepth_8() {
        return underminingdepth_8;
    }
    
    public void setUnderminingdepth_8(int underminingdepth_8) {
        this.underminingdepth_8 = underminingdepth_8;
    }
    
    public int getUnderminingdepth_9() {
        return underminingdepth_9;
    }
    
    public void setUnderminingdepth_9(int underminingdepth_9) {
        this.underminingdepth_9 = underminingdepth_9;
    }
    
    public int getUnderminingdepth_10() {
        return underminingdepth_10;
    }
    
    public void setUnderminingdepth_10(int underminingdepth_10) {
        this.underminingdepth_10 = underminingdepth_10;
    }
    
    public int getUnderminingdepth_mini_1() {
        return underminingdepth_mini_1;
    }
    
    public void setUnderminingdepth_mini_1(int underminingdepth_mini_1) {
        this.underminingdepth_mini_1 = underminingdepth_mini_1;
    }
    
    public int getUnderminingdepth_mini_2() {
        return underminingdepth_mini_2;
    }
    
    public void setUnderminingdepth_mini_2(int underminingdepth_mini_2) {
        this.underminingdepth_mini_2 = underminingdepth_mini_2;
    }
    
    public int getUnderminingdepth_mini_3() {
        return underminingdepth_mini_3;
    }
    
    public void setUnderminingdepth_mini_3(int underminingdepth_mini_3) {
        this.underminingdepth_mini_3 = underminingdepth_mini_3;
    }
    
    public int getUnderminingdepth_mini_4() {
        return underminingdepth_mini_4;
    }
    
    public void setUnderminingdepth_mini_4(int underminingdepth_mini_4) {
        this.underminingdepth_mini_4 = underminingdepth_mini_4;
    }
    
    public int getUnderminingdepth_mini_5() {
        return underminingdepth_mini_5;
    }
    
    public void setUnderminingdepth_mini_5(int underminingdepth_mini_5) {
        this.underminingdepth_mini_5 = underminingdepth_mini_5;
    }
    
    public int getUnderminingdepth_mini_6() {
        return underminingdepth_mini_6;
    }
    
    public void setUnderminingdepth_mini_6(int underminingdepth_mini_6) {
        this.underminingdepth_mini_6 = underminingdepth_mini_6;
    }
    
    public int getUnderminingdepth_mini_7() {
        return underminingdepth_mini_7;
    }
    
    public void setUnderminingdepth_mini_7(int underminingdepth_mini_7) {
        this.underminingdepth_mini_7 = underminingdepth_mini_7;
    }
    
    public int getUnderminingdepth_mini_8() {
        return underminingdepth_mini_8;
    }
    
    public void setUnderminingdepth_mini_8(int underminingdepth_mini_8) {
        this.underminingdepth_mini_8 = underminingdepth_mini_8;
    }
    
    public int getUnderminingdepth_mini_9() {
        return underminingdepth_mini_9;
    }
    
    public void setUnderminingdepth_mini_9(int underminingdepth_mini_9) {
        this.underminingdepth_mini_9 = underminingdepth_mini_9;
    }
    
    public int getUnderminingdepth_mini_10() {
        return underminingdepth_mini_10;
    }
    
    public void setUnderminingdepth_mini_10(int underminingdepth_mini_10) {
        this.underminingdepth_mini_10 = underminingdepth_mini_10;
    }
    
    public int getUndermining_start_1() {
        return undermining_start_1;
    }
    
    public void setUndermining_start_1(int undermining_start_1) {
        this.undermining_start_1 = undermining_start_1;
    }
    
    public int getUndermining_start_2() {
        return undermining_start_2;
    }
    
    public void setUndermining_start_2(int undermining_start_2) {
        this.undermining_start_2 = undermining_start_2;
    }
    
    public int getUndermining_start_3() {
        return undermining_start_3;
    }
    
    public void setUndermining_start_3(int undermining_start_3) {
        this.undermining_start_3 = undermining_start_3;
    }
    
    public int getUndermining_start_4() {
        return undermining_start_4;
    }
    
    public void setUndermining_start_4(int undermining_start_4) {
        this.undermining_start_4 = undermining_start_4;
    }
    
    public int getUndermining_start_5() {
        return undermining_start_5;
    }
    
    public void setUndermining_start_5(int undermining_start_5) {
        this.undermining_start_5 = undermining_start_5;
    }
    
    public int getUndermining_start_6() {
        return undermining_start_6;
    }
    
    public void setUndermining_start_6(int undermining_start_6) {
        this.undermining_start_6 = undermining_start_6;
    }
    
    public int getUndermining_start_7() {
        return undermining_start_7;
    }
    
    public void setUndermining_start_7(int undermining_start_7) {
        this.undermining_start_7 = undermining_start_7;
    }
    
    public int getUndermining_start_8() {
        return undermining_start_8;
    }
    
    public void setUndermining_start_8(int undermining_start_8) {
        this.undermining_start_8 = undermining_start_8;
    }
    
    public int getUndermining_start_9() {
        return undermining_start_9;
    }
    
    public void setUndermining_start_9(int undermining_start_9) {
        this.undermining_start_9 = undermining_start_9;
    }
    
    public int getUndermining_start_10() {
        return undermining_start_10;
    }
    
    public void setUndermining_start_10(int undermining_start_10) {
        this.undermining_start_10 = undermining_start_10;
    }
    
    public int getUndermining_end_1() {
        return undermining_end_1;
    }
    
    public void setUndermining_end_1(int undermining_end_1) {
        this.undermining_end_1 = undermining_end_1;
    }
    
    public int getUndermining_end_2() {
        return undermining_end_2;
    }
    
    public void setUndermining_end_2(int undermining_end_2) {
        this.undermining_end_2 = undermining_end_2;
    }
    
    public int getUndermining_end_3() {
        return undermining_end_3;
    }
    
    public void setUndermining_end_3(int undermining_end_3) {
        this.undermining_end_3 = undermining_end_3;
    }
    
    public int getUndermining_end_4() {
        return undermining_end_4;
    }
    
    public void setUndermining_end_4(int undermining_end_4) {
        this.undermining_end_4 = undermining_end_4;
    }
    
    public int getUndermining_end_5() {
        return undermining_end_5;
    }
    
    public void setUndermining_end_5(int undermining_end_5) {
        this.undermining_end_5 = undermining_end_5;
    }
    
    public int getUndermining_end_6() {
        return undermining_end_6;
    }
    
    public void setUndermining_end_6(int undermining_end_6) {
        this.undermining_end_6 = undermining_end_6;
    }
    
    public int getUndermining_end_7() {
        return undermining_end_7;
    }
    
    public void setUndermining_end_7(int undermining_end_7) {
        this.undermining_end_7 = undermining_end_7;
    }
    
    public int getUndermining_end_8() {
        return undermining_end_8;
    }
    
    public void setUndermining_end_8(int undermining_end_8) {
        this.undermining_end_8 = undermining_end_8;
    }
    
    public int getUndermining_end_9() {
        return undermining_end_9;
    }
    
    public void setUndermining_end_9(int undermining_end_9) {
        this.undermining_end_9 = undermining_end_9;
    }
    
    public int getUndermining_end_10() {
        return undermining_end_10;
    }
    
    public void setUndermining_end_10(int undermining_end_10) {
        this.undermining_end_10 = undermining_end_10;
    }
    
    public int getNum_under() {
        return num_under;
    }
    
    public void setNum_under(int num_undermining) {
        this.num_under = num_undermining;
    }
    
    public String getImage_array2() {
        return image_array2;
    }
    
    public void setImage_array2(String image_array2) {
        this.image_array2 = image_array2;
    }
    
    public String getDischarge_other() {
        return discharge_other;
    }
    
    public void setDischarge_other(String discharge_other) {
        this.discharge_other = discharge_other;
    }
    
    public int getWound_date_day() {
        return wound_date_day;
    }
    
    public void setWound_date_day(int wound_date_day) {
        this.wound_date_day = wound_date_day;
    }
    
    public int getWound_date_month() {
        return wound_date_month;
    }
    public void setWound_date_month(int wound_date_month) {
        this.wound_date_month = wound_date_month;
    }
    
    public int getWound_date_year() {
        return wound_date_year;
    }
    
    public void setWound_date_year(int wound_date_year) {
        this.wound_date_year = wound_date_year;
    }
    
    public AssessmentEachwoundVO getFormData(ProfessionalVO userVO)  throws ApplicationException{
        AssessmentEachwoundVO assessmentEachwoundVO = new AssessmentEachwoundVO();
        
        if( full_assessment == -1){ full_assessment = 1; }
        PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
        assessmentEachwoundVO.setActive(0);
        assessmentEachwoundVO.setDeleted(0);
        assessmentEachwoundVO.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
        assessmentEachwoundVO.setWound_id(new Integer((String) session.getAttribute("wound_id")));
        assessmentEachwoundVO.setPain(new Integer(getPain()));
        assessmentEachwoundVO.setLength((getLength()<0?null:getLength()));
        assessmentEachwoundVO.setWidth((getWidth()<0?null:getWidth()));
        assessmentEachwoundVO.setDepth((getDepth()<0?null:getDepth()));
        assessmentEachwoundVO.setSkin_temperature((getSkin_temperature()<0?null:getSkin_temperature()));
        assessmentEachwoundVO.setRecurrent(getRecurrent()==null?-1:new Integer(getRecurrent()));
        assessmentEachwoundVO.setWound_edge(Serialize.serialize(getWound_edge()));
        assessmentEachwoundVO.setPeriwound_skin(Serialize.serialize(getPeriwound()));
        assessmentEachwoundVO.setClosed_date(pdate.getDate(getClosed_month(),getClosed_day(),getClosed_year()));
        try{
            if(assessmentEachwoundVO.getClosed_date()==null && assessmentEachwoundVO.getStatus().equals(Integer.parseInt(Common.getConfig("woundClosed")))){
               assessmentEachwoundVO.setClosed_date(new Date());
            }
        }catch(NullPointerException e){
            
        }catch(NumberFormatException e){
            
        }
        assessmentEachwoundVO.setWound_date(pdate.getDate(getWound_date_month(), getWound_date_day(), getWound_date_year()));
        
        assessmentEachwoundVO.setFull_assessment(new Integer(getFull_assessment()));
        assessmentEachwoundVO.setExudate_amount(getExudate_amount());
        assessmentEachwoundVO.setWound_depth("" + getWound_depth());
        assessmentEachwoundVO.setExudate_colour(Serialize.serialize(getColour()));
        assessmentEachwoundVO.setExudate_odour((getExudate_odour()==null?new Integer("-1"):new Integer(getExudate_odour())));
        assessmentEachwoundVO.setExudate_type(Serialize.serialize(getExudate_type()));
        assessmentEachwoundVO.setPush_scale(push_scale);
        assessmentEachwoundVO.setTarget_discharge_date(pdate.getDate(getTarget_discharge_date_month(),getTarget_discharge_date_day(),getTarget_discharge_date_year()));
        //set for all alphas incase some do not goto treatment.
        assessmentEachwoundVO.setPain_comments(Common.convertCarriageReturns(pain_comments,false));
        assessmentEachwoundVO.setStatus(new Integer(getStatus() + ""));
        
        // ---------------
        assessmentEachwoundVO.setCreated_on(new Date());
        assessmentEachwoundVO.setDischarge_reason(new Integer(getDischarge_reason()));
        assessmentEachwoundVO.setDischarge_other(getDischarge_other());
        assessmentEachwoundVO.setFistulas(Common.convertCarriageReturns(getFistulas(),false));
        assessmentEachwoundVO.setNum_fistulas(getNum_fistulas());
        // NEW FIELDS
        //assessmentEachwoundVO.setAdjunctive(getAdjunctive());
        //assessmentEachwoundVO.setAdjunctive_other_text("");
        // assessmentEachwoundVO.setVac_start_date("");
        // assessmentEachwoundVO.setVac_end_date("");
        // assessmentEachwoundVO.setReading(new Integer("0"));
        // assessmentEachwoundVO.setReading_var("");
        
        return assessmentEachwoundVO;
        
    }
    
   public List getSinus() {
       List v = new ArrayList();
       try{
       for (int x = 0; x < getNum_sinus_tracts(); x++) {
           int cnt = x+1;
           AssessmentSinustractsVO u = new AssessmentSinustractsVO();
           Integer dcm = (Integer)Common.getFieldValue("sinusdepth_"+cnt,this);
           Integer dmm = (Integer)Common.getFieldValue("sinusdepth_mini_"+cnt,this);
           Integer dstart= (Integer)Common.getFieldValue("sinustract_start_"+cnt,this);
           
           if(dcm<0){
               dcm=null;
           }
           if(dmm<0){
               dmm=null;
           }
           u.setDepth_cm(dcm);
           u.setDepth_mm(dmm);
           u.setLocation_start(new Integer(dstart));
           v.add(u);
       }
       }catch(Exception e){
           e.printStackTrace();
           return new ArrayList();
       }
       return v;
   }
   public List getUndermining() {
       List v = new ArrayList();
       try {
           for (int x = 0; x < getNum_under(); x++) {
               int cnt = x + 1;
               AssessmentUnderminingVO u = new AssessmentUnderminingVO();
               Integer dcm = (Integer) Common.getFieldValue("underminingdepth_" + cnt, this);
               Integer dmm = (Integer) Common.getFieldValue("underminingdepth_mini_" + cnt, this);
               if(dcm<0){
                dcm=null;
               }
               if(dmm<0){
                    dmm=null;
               }
               Integer dstart = (Integer) Common.getFieldValue("undermining_start_" + cnt, this);
               Integer dend = (Integer) Common.getFieldValue("undermining_end_" + cnt, this);
               u.setDepth_cm(dcm);
               u.setDepth_mm(dmm);
               u.setLocation_start(new Integer(dstart));
               u.setLocation_end(new Integer(dend));
               v.add(u);
           }
       } catch (Exception e) {
       }
       return v;
   }
    
    /**
     * @method getVacend_day
     * @field vacend_day - int
     * @return Returns the vacend_day.
     */
    public int getVacend_day() {
        return vacend_day;
    }
    
    /**
     * @method setVacend_day
     * @field vacend_day - int
     * @param vacend_day The vacend_day to set.
     */
    public void setVacend_day(int vacend_day) {
        this.vacend_day = vacend_day;
    }
    
    /**
     * @method getVacend_month
     * @field vacend_month - int
     * @return Returns the vacend_month.
     */
    public int getVacend_month() {
        return vacend_month;
    }
    
    /**
     * @method setVacend_month
     * @field vacend_month - int
     * @param vacend_month The vacend_month to set.
     */
    public void setVacend_month(int vacend_month) {
        this.vacend_month = vacend_month;
    }
    
    /**
     * @method getVacend_year
     * @field vacend_year - int
     * @return Returns the vacend_year.
     */
    public int getVacend_year() {
        return vacend_year;
    }
    
    /**
     * @method setVacend_year
     * @field vacend_year - int
     * @param vacend_year The vacend_year to set.
     */
    public void setVacend_year(int vacend_year) {
        this.vacend_year = vacend_year;
    }
    
    /**
     * @method getVacstart_day
     * @field vacstart_day - int
     * @return Returns the vacstart_day.
     */
    public int getVacstart_day() {
        return vacstart_day;
    }
    
    /**
     * @method setVacstart_day
     * @field vacstart_day - int
     * @param vacstart_day The vacstart_day to set.
     */
    public void setVacstart_day(int vacstart_day) {
        this.vacstart_day = vacstart_day;
    }
    
    /**
     * @method getVacstart_month
     * @field vacstart_month - int
     * @return Returns the vacstart_month.
     */
    public int getVacstart_month() {
        return vacstart_month;
    }
    
    /**
     * @method setVacstart_month
     * @field vacstart_month - int
     * @param vacstart_month The vacstart_month to set.
     */
    public void setVacstart_month(int vacstart_month) {
        this.vacstart_month = vacstart_month;
    }
    
    /**
     * @method getVacstart_year
     * @field vacstart_year - int
     * @return Returns the vacstart_year.
     */
    public int getVacstart_year() {
        return vacstart_year;
    }
    
    /**
     * @method setVacstart_year
     * @field vacstart_year - int
     * @param vacstart_year The vacstart_year to set.
     */
    public void setVacstart_year(int vacstart_year) {
        this.vacstart_year = vacstart_year;
    }
    
    
    public String getReading_group() {
        return reading_group;
    }
    
    public void setReading_group(String reading_group) {
        this.reading_group = reading_group;
    }
    
    public int getPressure_reading() {
        return pressure_reading;
    }
    
    public void setPressure_reading(int pressure_reading) {
        this.pressure_reading = pressure_reading;
    }
    
    public String getVac() {
        return vac;
    }
    
    public void setVac(String vac) {
        this.vac = vac;
    }
    
    public String[] getAdjunctive() {
        return adjunctive;
    }
    
    public void setAdjunctive(String[] adjunctive) {
        this.adjunctive = adjunctive;
    }
    
    public String getAdjunctive_other() {
        return adjunctive_other;
    }
    
    public void setAdjunctive_other(String adjunctive_other) {
        this.adjunctive_other = adjunctive_other;
    }
    
    public String getLaser() {
        return laser;
    }
    
    public void setLaser(String laser) {
        this.laser = laser;
    }
    
    public String getPain_comments() {
        return pain_comments;
    }
    
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
    
    public String getfistulas_1() {
        return fistulas_1;
    }
    
    public void setfistulas_1(String fistulas_1) {
        this.fistulas_1 = fistulas_1;
    }
    
    public String getfistulas_2() {
        return fistulas_2;
    }
    
    public void setfistulas_2(String fistulas_2) {
        this.fistulas_2 = fistulas_2;
    }
    
    public String getfistulas_3() {
        return fistulas_3;
    }
    
    public void setfistulas_3(String fistulas_3) {
        this.fistulas_3 = fistulas_3;
    }
    
    public String getfistulas_4() {
        return fistulas_4;
    }
    
    public void setfistulas_4(String fistulas_4) {
        this.fistulas_4 = fistulas_4;
    }
    
    public String getfistulas_5() {
        return fistulas_5;
    }
    
    public void setfistulas_5(String fistulas_5) {
        this.fistulas_5 = fistulas_5;
    }
    
    public String getfistulas_6() {
        return fistulas_6;
    }
    
    public void setfistulas_6(String fistulas_6) {
        this.fistulas_6 = fistulas_6;
    }
    
    public String getfistulas_7() {
        return fistulas_7;
    }
    
    public void setfistulas_7(String fistulas_7) {
        this.fistulas_7 = fistulas_7;
    }
    
    public String getfistulas_8() {
        return fistulas_8;
    }
    
    public void setfistulas_8(String fistulas_8) {
        this.fistulas_8 = fistulas_8;
    }
    
    public String getfistulas_9() {
        return fistulas_9;
    }
    
    public void setfistulas_9(String fistulas_9) {
        this.fistulas_9 = fistulas_9;
    }
    
    public String getfistulas_10() {
        return fistulas_10;
    }
    
    public void setfistulas_10(String fistulas_10) {
        this.fistulas_10 = fistulas_10;
    }
    public String getFistulas() throws ApplicationException{
        Vector fist = new Vector();
        if(fistulas_1!=null && !fistulas_1.equals("")){
            fist.add(fistulas_1);
            
        }else if(fistulas_2!=null && !fistulas_2.equals("")){
            fist.add(fistulas_2);
            
        }else if(fistulas_3!=null && !fistulas_3.equals("")){
            fist.add(fistulas_3);
            
        }else if(fistulas_4!=null && !fistulas_4.equals("")){
            fist.add(fistulas_4);
            
        }else if(fistulas_5!=null && !fistulas_5.equals("")){
            fist.add(fistulas_5);
            
        }else if(fistulas_6!=null && !fistulas_6.equals("")){
            fist.add(fistulas_6);
            
        }else if(fistulas_7!=null && !fistulas_7.equals("")){
            fist.add(fistulas_7);
            
        }else if(fistulas_8!=null && !fistulas_8.equals("")){
            fist.add(fistulas_8);
            
        }else if(fistulas_9!=null && !fistulas_9.equals("")){
            fist.add(fistulas_9);
            
        }else if(fistulas_10!=null && !fistulas_10.equals("")){
            fist.add(fistulas_10);
            
        }
        
        return Serialize.serializeStraight(fist.toArray());
    }
    
    public int getNum_fistulas() {
        return num_fistulas;
    }
    
    public void setNum_fistulas(int num_fistulas) {
        this.num_fistulas = num_fistulas;
    }
    
    
    
    public int getCs_date_day() {
        return cs_date_day;
    }
    
    public void setCs_date_day(int cs_date_day) {
        this.cs_date_day = cs_date_day;
    }
    
    public int getCs_date_month() {
        return cs_date_month;
    }
    
    public void setCs_date_month(int cs_date_month) {
        this.cs_date_month = cs_date_month;
    }
    
    public int getCs_date_year() {
        return cs_date_year;
    }
    
    public void setCs_date_year(int cs_date_year) {
        this.cs_date_year = cs_date_year;
    }
    
    public int getCs_date_show() {
        return cs_date_show;
    }
    
    public void setCs_date_show(int cs_date_show) {
        this.cs_date_show = cs_date_show;
    }
    
    public int getPhone_visit() {
        return phone_visit;
    }
    
    public void setPhone_visit(int phone_visit) {
        this.phone_visit = phone_visit;
    }
    /**
     * @return the push_scale
     */
    public Integer getPush_scale() {
        return push_scale;
    }
    /**
     * @param push_scale the push_scale to set
     */
    public void setPush_scale(Integer push_scale) {
        this.push_scale = push_scale;
    }
    /**
     * @return the image_array3
     */
    public String getImage_array3() {
        return image_array3;
    }
    /**
     * @param image_array3 the image_array3 to set
     */
    public void setImage_array3(String image_array3) {
        this.image_array3 = image_array3;
    }
    /**
     * @return the target_discharge_date_day
     */
    public int getTarget_discharge_date_day() {
        return target_discharge_date_day;
    }
    /**
     * @param target_discharge_date_day the target_discharge_date_day to set
     */
    public void setTarget_discharge_date_day(int target_discharge_date_day) {
        this.target_discharge_date_day = target_discharge_date_day;
    }
    /**
     * @return the target_discharge_date_month
     */
    public int getTarget_discharge_date_month() {
        return target_discharge_date_month;
    }
    /**
     * @param target_discharge_date_month the target_discharge_date_month to set
     */
    public void setTarget_discharge_date_month(int target_discharge_date_month) {
        this.target_discharge_date_month = target_discharge_date_month;
    }
    /**
     * @return the target_discharge_date_year
     */
    public int getTarget_discharge_date_year() {
        return target_discharge_date_year;
    }
    /**
     * @param target_discharge_date_year the target_discharge_date_year to set
     */
    public void setTarget_discharge_date_year(int target_discharge_date_year) {
        this.target_discharge_date_year = target_discharge_date_year;
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the depth
     */
    public double getDepth() {
        return depth;
    }

    /**
     * @param depth the depth to set
     */
    public void setDepth(double depth) {
        this.depth = depth;
    }

    /**
     * @return the skin_temperature
     */
    public double getSkin_temperature() {
        return skin_temperature;
    }

    /**
     * @param skin_temperature the skin_temperature to set
     */
    public void setSkin_temperature(double skin_temperature) {
        this.skin_temperature = skin_temperature;
    }
   
}
