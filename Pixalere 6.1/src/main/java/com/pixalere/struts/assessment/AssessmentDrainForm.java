package com.pixalere.struts.assessment;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import java.util.Date;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.auth.bean.ProfessionalVO;
public class AssessmentDrainForm extends ActionForm{

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentEachwoundForm.class);
   
    
    private int status;
    private int discharge_reason;
    private Date created_on;

    private String image_array3;
    private int type_of_drain;
    private String type_of_drain_other;
    private int sutured;
    private int drainage_num;
    private int cs_date_day;
    private int cs_date_month;
    private int cs_date_year;
    private int cs_date_show;    
    //images
    private String image_array;    
    private String image_array2;
    private FormFile image_1;
    private FormFile image_2;
    private FormFile image_3;
    public FormFile getImage_3() {
        return image_3;
    }

    public void setImage_3(FormFile image_3) {
        this.image_3 = image_3;
    }
    private Integer drain_removed_intact;
    private int visit_freq;
    private String[] drain_site;
    private int drain_removed_date_day;
    private int drain_removed_date_month;
    private int drain_removed_date_year;
    private int tubes_changed_date_day;
    private int tubes_changed_date_month;
    private int tubes_changed_date_year;

    private Integer drainage_amount_mls1;
    private Integer drainage_amount_hrs1;
    private Integer drainage_amount_start1;
    private Integer drainage_amount_end1;
    private String[] drain_characteristics1;
    private String[] drain_odour1;

    private Integer drainage_amount_mls2;
    private Integer drainage_amount_hrs2;
    private Integer drainage_amount_start2;
    private Integer drainage_amount_end2;
    private String[] drain_characteristics2;
    private String[] drain_odour2;

    private Integer drainage_amount_mls3;
    private Integer drainage_amount_hrs3;
    private Integer drainage_amount_start3;
    private Integer drainage_amount_end3;
    private String[] drain_characteristics3;
    private String[] drain_odour3;

    private Integer drainage_amount_mls4;
    private Integer drainage_amount_hrs4;
    private Integer drainage_amount_start4;
    private Integer drainage_amount_end4;
    private String[] drain_characteristics4;
    private String[] drain_odour4;
     private Integer drainage_amount_mls5;
    private Integer drainage_amount_hrs5;
    private Integer drainage_amount_start5;
    private Integer drainage_amount_end5;
    private String[] drain_characteristics5;
    private String[] drain_odour5;
    private int additional_days_date5_month;
    private int additional_days_date4_month;
    private int additional_days_date3_month;
    private int additional_days_date2_month;
    private int additional_days_date1_month;
    private int additional_days_date5_day;
    private int additional_days_date4_day;
    private int additional_days_date3_day;
    private int additional_days_date2_day;
    private int additional_days_date1_day;
    private int additional_days_date5_year;
    private int additional_days_date4_year;
    private int additional_days_date3_year;
    private int additional_days_date2_year;
    private int additional_days_date1_year;
    private Integer pain;
    private String pain_comments;
    private int full_assessment;
    private HttpSession session = null;
    
    private String[] peri_drain_area;
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        setSession(request.getSession());
        
        ActionErrors errors = new ActionErrors();
        
        if (image_1!=null && image_1.getFileName().length()>0 && image_1.getFileSize() == 0) {
            errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
        }

       if (image_2!=null && image_2.getFileName().length()>0 && image_2.getFileSize() == 0) {
            errors.add("file2",new ActionError("pixalere.woundassessment.error.filemissing2"));
        }
       
        return errors;
        
    }
    
    public AssessmentDrainVO getFormData(ProfessionalVO userVO) throws ApplicationException{

    	AssessmentDrainVO assessmentDrainVO = new AssessmentDrainVO();
        if( full_assessment == -1){ full_assessment = 1; }
        PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
        assessmentDrainVO.setActive(0);
        assessmentDrainVO.setDeleted(0);
        assessmentDrainVO.setFull_assessment(getFull_assessment());
        assessmentDrainVO.setStatus(new Integer(getStatus() + ""));
        assessmentDrainVO.setDischarge_reason(new Integer(getDischarge_reason()));
    	assessmentDrainVO.setPatient_id(new Integer((String) getSession().getAttribute("patient_id")));
    	assessmentDrainVO.setWound_id(new Integer((String) getSession().getAttribute("wound_id")));
        assessmentDrainVO.setType_of_drain(new Integer(getType_of_drain() + ""));
        assessmentDrainVO.setType_of_drain_other(getType_of_drain_other());
        assessmentDrainVO.setSutured(getSutured());
        assessmentDrainVO.setPeri_drain_area(Serialize.serialize(getPeri_drain_area()));
        assessmentDrainVO.setCreated_on(new Date());
        assessmentDrainVO.setDrain_characteristics1(Serialize.serialize(getDrain_characteristics1()));
        assessmentDrainVO.setDrain_odour2(Serialize.serialize(getDrain_odour2()));
        assessmentDrainVO.setDrain_characteristics2(Serialize.serialize(getDrain_characteristics2()));
        assessmentDrainVO.setDrain_odour3(Serialize.serialize(getDrain_odour3()));
        assessmentDrainVO.setDrain_characteristics3(Serialize.serialize(getDrain_characteristics3()));
        assessmentDrainVO.setDrain_odour4(Serialize.serialize(getDrain_odour4()));
        assessmentDrainVO.setDrain_characteristics4(Serialize.serialize(getDrain_characteristics4()));
        assessmentDrainVO.setDrain_odour5(Serialize.serialize(getDrain_odour5()));
        assessmentDrainVO.setDrain_characteristics5(Serialize.serialize(getDrain_characteristics5()));
        assessmentDrainVO.setDrain_odour1(Serialize.serialize(getDrain_odour1()));
     
        assessmentDrainVO.setDrainage_amount_mls1(getDrainage_amount_mls1()>0?getDrainage_amount_mls1():null);
        assessmentDrainVO.setDrainage_amount_hrs1(getDrainage_amount_hrs1()>0?getDrainage_amount_hrs1():null);
        assessmentDrainVO.setDrainage_amount1_end(getDrainage_amount_end1());
        assessmentDrainVO.setDrainage_amount1_start(getDrainage_amount_start1());
        assessmentDrainVO.setAdditional_days_date1(null);//PDate.getEpochFromDate(getAdditional_days_date1_month(),getAdditional_days_date1_day(),getAdditional_days_date1_year()));
        
        assessmentDrainVO.setDrainage_amount_mls2(getDrainage_amount_mls2()!=null && getDrainage_amount_mls2()>0?getDrainage_amount_mls2():null);
        assessmentDrainVO.setDrainage_amount_hrs2(getDrainage_amount_hrs2()!=null && getDrainage_amount_hrs2()>0?getDrainage_amount_hrs2():null);
        assessmentDrainVO.setDrainage_amount2_end(getDrainage_amount_end2());
        assessmentDrainVO.setDrainage_amount2_start(getDrainage_amount_start2());
        assessmentDrainVO.setAdditional_days_date2(pdate.getDate(getAdditional_days_date2_month(),getAdditional_days_date2_day(),getAdditional_days_date2_year()));
        
        assessmentDrainVO.setDrainage_amount_mls3(getDrainage_amount_mls3()!=null && getDrainage_amount_mls3()>0?getDrainage_amount_mls3():null);
        assessmentDrainVO.setDrainage_amount_hrs3(getDrainage_amount_hrs3()!=null && getDrainage_amount_hrs3()>0?getDrainage_amount_hrs3():null);
        assessmentDrainVO.setDrainage_amount3_end(getDrainage_amount_end3());
        assessmentDrainVO.setDrainage_amount3_start(getDrainage_amount_start3());
        assessmentDrainVO.setAdditional_days_date3(pdate.getDate(getAdditional_days_date3_month(),getAdditional_days_date3_day(),getAdditional_days_date3_year()));
        
        
        assessmentDrainVO.setDrainage_amount_mls4(getDrainage_amount_mls4()!=null && getDrainage_amount_mls4()>0?getDrainage_amount_mls4():null);
        assessmentDrainVO.setDrainage_amount_hrs4(getDrainage_amount_hrs4()!=null && getDrainage_amount_hrs4()>0?getDrainage_amount_hrs4():null);
        assessmentDrainVO.setDrainage_amount4_end(getDrainage_amount_end4());
        assessmentDrainVO.setDrainage_amount4_start(getDrainage_amount_start4());
        assessmentDrainVO.setAdditional_days_date4(pdate.getDate(getAdditional_days_date4_month(),getAdditional_days_date4_day(),getAdditional_days_date4_year()));
        
        
        assessmentDrainVO.setDrainage_amount_mls5(getDrainage_amount_mls5()!=null && getDrainage_amount_mls5()>0?getDrainage_amount_mls5():null);
        assessmentDrainVO.setDrainage_amount_hrs5(getDrainage_amount_hrs5()!=null && getDrainage_amount_hrs5()>0?getDrainage_amount_hrs5():null);
        assessmentDrainVO.setDrainage_amount5_end(getDrainage_amount_end5());
        assessmentDrainVO.setDrainage_amount5_start(getDrainage_amount_start5());
        assessmentDrainVO.setAdditional_days_date5(pdate.getDate(getAdditional_days_date5_month(),getAdditional_days_date5_day(),getAdditional_days_date5_year()));
        
        assessmentDrainVO.setPain(getPain());
        assessmentDrainVO.setPain_comments(Common.convertCarriageReturns(getPain_comments(),false));
        assessmentDrainVO.setTubes_changed_date(pdate.getDate(getTubes_changed_date_month(),getTubes_changed_date_day(),getTubes_changed_date_year()));
        assessmentDrainVO.setDrainage_num(getDrainage_num());
        assessmentDrainVO.setDrain_site(Serialize.serialize(getDrain_site()));
        assessmentDrainVO.setDrain_removed_intact(getDrain_removed_intact()==null?-1:getDrain_removed_intact());
        assessmentDrainVO.setClosed_date(pdate.getDate(getDrain_removed_date_month(),getDrain_removed_date_day(),getDrain_removed_date_year()));
        
        try{
            if(assessmentDrainVO.getClosed_date()==null && assessmentDrainVO.getStatus().equals(Integer.parseInt(Common.getConfig("drainClosed")))){
               assessmentDrainVO.setClosed_date(new Date());
            }
        }catch(NullPointerException e){
            
        }catch(NumberFormatException e){
            
        }

        return assessmentDrainVO;
    }
    
   
    public int getVisit_freq() {
        return visit_freq;
    }
    
    public void setVisit_freq(int visit_freq) {
        this.visit_freq = visit_freq;
    }
    

    
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    
    
    public String getImage_array() {
        return image_array;
    }
    
    public void setImage_array(String image_array) {
        this.image_array = image_array;
    }
    
    public String getImage_array2() {
        return image_array2;
    }
    
    public void setImage_array2(String image_array2) {
        this.image_array2 = image_array2;
    }
    
    public FormFile getImage_1() {
        return image_1;
    }
    
    public void setImage_1(FormFile image_1) {
        this.image_1 = image_1;
    }

    public FormFile getImage_2() {
        return image_2;
    }
    
    public void setImage_2(FormFile image_2) {
        this.image_2 = image_2;
    }
    
    public int getStatus() {
        return status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }

    public int getType_of_drain() {
        return type_of_drain;
    }

    public void setType_of_drain(int type_of_drain) {
        this.type_of_drain = type_of_drain;
    }

    public int getSutured() {
        return sutured;
    }

    public void setSutured(int sutured) {
        this.sutured = sutured;
    }

 
   

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    

    public String[] getDrain_site() {
        return drain_site;
    }

    public void setDrain_site(String[] drain_site) {
        this.drain_site = drain_site;
    }
 
    public int getDrain_removed_date_day() {
        return drain_removed_date_day;
    }

    public void setDrain_removed_date_day(int drain_removed_date_day) {
        this.drain_removed_date_day = drain_removed_date_day;
    }

    public int getDrain_removed_date_month() {
        return drain_removed_date_month;
    }

    public void setDrain_removed_date_month(int drain_removed_date_month) {
        this.drain_removed_date_month = drain_removed_date_month;
    }

    public int getDrain_removed_date_year() {
        return drain_removed_date_year;
    }

    public void setDrain_removed_date_year(int drain_removed_date_year) {
        this.drain_removed_date_year = drain_removed_date_year;
    }
  
    public int getCs_date_day() {
        return cs_date_day;
    }

    public void setCs_date_day(int cs_date_day) {
        this.cs_date_day = cs_date_day;
    }

    public int getCs_date_month() {
        return cs_date_month;
    }

    public void setCs_date_month(int cs_date_month) {
        this.cs_date_month = cs_date_month;
    }

    public int getCs_date_year() {
        return cs_date_year;
    }

    public void setCs_date_year(int cs_date_year) {
        this.cs_date_year = cs_date_year;
    }

    public int getCs_date_show() {
        return cs_date_show;
    }

    public void setCs_date_show(int cs_date_show) {
        this.cs_date_show = cs_date_show;
    }



    public Integer getDrainage_amount_mls1() {
        return drainage_amount_mls1;
    }

    public void setDrainage_amount_mls1(Integer drainage_amount_mls1) {
        this.drainage_amount_mls1 = drainage_amount_mls1;
    }

    public Integer getDrainage_amount_hrs1() {
        return drainage_amount_hrs1;
    }

    public void setDrainage_amount_hrs1(Integer drainage_amount_hrs1) {
        this.drainage_amount_hrs1 = drainage_amount_hrs1;
    }

    public Integer getDrainage_amount_start1() {
        return drainage_amount_start1;
    }

    public void setDrainage_amount_start1(Integer drainage_amount_start1) {
        this.drainage_amount_start1 = drainage_amount_start1;
    }

    public Integer getDrainage_amount_end1() {
        return drainage_amount_end1;
    }

    public void setDrainage_amount_end1(Integer drainage_amount_end1) {
        this.drainage_amount_end1 = drainage_amount_end1;
    }

    public String[] getDrain_characteristics1() {
        return drain_characteristics1;
    }

    public void setDrain_characteristics1(String[] drain_characteristics1) {
        this.drain_characteristics1 = drain_characteristics1;
    }

    public String[] getDrain_odour1() {
        return drain_odour1;
    }

    public void setDrain_odour1(String[] odour1) {
        this.drain_odour1 = odour1;
    }

  

    public Integer getDrainage_amount_mls2() {
        return drainage_amount_mls2;
    }

    public void setDrainage_amount_mls2(Integer drainage_amount_mls2) {
        this.drainage_amount_mls2 = drainage_amount_mls2;
    }

    public Integer getDrainage_amount_hrs2() {
        return drainage_amount_hrs2;
    }

    public void setDrainage_amount_hrs2(Integer drainage_amount_hrs2) {
        this.drainage_amount_hrs2 = drainage_amount_hrs2;
    }

    public Integer getDrainage_amount_start2() {
        return drainage_amount_start2;
    }

    public void setDrainage_amount_start2(Integer drainage_amount_start2) {
        this.drainage_amount_start2 = drainage_amount_start2;
    }

    public Integer getDrainage_amount_end2() {
        return drainage_amount_end2;
    }

    public void setDrainage_amount_end2(Integer drainage_amount_end2) {
        this.drainage_amount_end2 = drainage_amount_end2;
    }

    public String[] getDrain_characteristics2() {
        return drain_characteristics2;
    }

    public void setDrain_characteristics2(String[] drain_characteristics2) {
        this.drain_characteristics2 = drain_characteristics2;
    }

    public String[] getDrain_odour2() {
        return drain_odour2;
    }

    public void setDrain_odour2(String[] odour2) {
        this.drain_odour2 = odour2;
    }

  

    public Integer getDrainage_amount_mls3() {
        return drainage_amount_mls3;
    }

    public void setDrainage_amount_mls3(Integer drainage_amount_mls3) {
        this.drainage_amount_mls3 = drainage_amount_mls3;
    }

    public Integer getDrainage_amount_hrs3() {
        return drainage_amount_hrs3;
    }

    public void setDrainage_amount_hrs3(Integer drainage_amount_hrs3) {
        this.drainage_amount_hrs3 = drainage_amount_hrs3;
    }

    public Integer getDrainage_amount_start3() {
        return drainage_amount_start3;
    }

    public void setDrainage_amount_start3(Integer drainage_amount_start3) {
        this.drainage_amount_start3 = drainage_amount_start3;
    }
public String[] getPeri_drain_area() {
	         return peri_drain_area;
	     }

	     public void setPeri_drain_area(String[] peri_drain_area) {
	         this.peri_drain_area = peri_drain_area;
	     }
    public Integer getDrainage_amount_end3() {
        return drainage_amount_end3;
    }

    public void setDrainage_amount_end3(Integer drainage_amount_end3) {
        this.drainage_amount_end3 = drainage_amount_end3;
    }

    public String[] getDrain_characteristics3() {
        return drain_characteristics3;
    }

    public void setDrain_characteristics3(String[] drain_characteristics3) {
        this.drain_characteristics3 = drain_characteristics3;
    }

    public String[] getDrain_odour3() {
        return drain_odour3;
    }

    public void setDrain_odour3(String[] drain_odour3) {
        this.drain_odour3 = drain_odour3;
    }

  

    public Integer getDrainage_amount_mls4() {
        return drainage_amount_mls4;
    }

    public void setDrainage_amount_mls4(Integer drainage_amount_mls4) {
        this.drainage_amount_mls4 = drainage_amount_mls4;
    }

    public Integer getDrainage_amount_hrs4() {
        return drainage_amount_hrs4;
    }

    public void setDrainage_amount_hrs4(Integer drainage_amount_hrs4) {
        this.drainage_amount_hrs4 = drainage_amount_hrs4;
    }

    public Integer getDrainage_amount_start4() {
        return drainage_amount_start4;
    }

    public void setDrainage_amount_start4(Integer drainage_amount_start4) {
        this.drainage_amount_start4 = drainage_amount_start4;
    }

    public Integer getDrainage_amount_end4() {
        return drainage_amount_end4;
    }

    public void setDrainage_amount_end4(Integer drainage_amount_end4) {
        this.drainage_amount_end4 = drainage_amount_end4;
    }

    public String[] getDrain_characteristics4() {
        return drain_characteristics4;
    }

    public void setDrain_characteristics4(String[] drain_characteristics4) {
        this.drain_characteristics4 = drain_characteristics4;
    }

    public String[] getDrain_odour4() {
        return drain_odour4;
    }

    public void setDrain_odour4(String[] drain_odour4) {
        this.drain_odour4 = drain_odour4;
    }

 

    public Integer getDrainage_amount_mls5() {
        return drainage_amount_mls5;
    }

    public void setDrainage_amount_mls5(Integer drainage_amount_mls5) {
        this.drainage_amount_mls5 = drainage_amount_mls5;
    }

    public Integer getDrainage_amount_hrs5() {
        return drainage_amount_hrs5;
    }

    public void setDrainage_amount_hrs5(Integer drainage_amount_hrs5) {
        this.drainage_amount_hrs5 = drainage_amount_hrs5;
    }

    public Integer getDrainage_amount_start5() {
        return drainage_amount_start5;
    }

    public void setDrainage_amount_start5(Integer drainage_amount_start5) {
        this.drainage_amount_start5 = drainage_amount_start5;
    }

    public Integer getDrainage_amount_end5() {
        return drainage_amount_end5;
    }

    public void setDrainage_amount_end5(Integer drainage_amount_end5) {
        this.drainage_amount_end5 = drainage_amount_end5;
    }

    public String[] getDrain_characteristics5() {
        return drain_characteristics5;
    }

    public void setDrain_characteristics5(String[] drain_characteristics5) {
        this.drain_characteristics5 = drain_characteristics5;
    }

    public String[] getDrain_odour5() {
        return drain_odour5;
    }

    public void setDrain_odour5(String[] drain_odour5) {
        this.drain_odour5 = drain_odour5;
    }


    public int getTubes_changed_date_day() {
        return tubes_changed_date_day;
    }

    public void setTubes_changed_date_day(int tubes_changed_date_day) {
        this.tubes_changed_date_day = tubes_changed_date_day;
    }

    public int getTubes_changed_date_month() {
        return tubes_changed_date_month;
    }

    public void setTubes_changed_date_month(int tubes_changed_date_month) {
        this.tubes_changed_date_month = tubes_changed_date_month;
    }

    public int getTubes_changed_date_year() {
        return tubes_changed_date_year;
    }

    public void setTubes_changed_date_year(int tubes_changed_date_year) {
        this.tubes_changed_date_year = tubes_changed_date_year;
    }


    public int getDischarge_reason() {
        return discharge_reason;
    }

    public void setDischarge_reason(int discharge_reason) {
        this.discharge_reason = discharge_reason;
    }

    public Integer getDrain_removed_intact() {
        return drain_removed_intact;
    }

    public void setDrain_removed_intact(Integer drain_removed_intact) {
        this.drain_removed_intact = drain_removed_intact;
    }

    public int getDrainage_num() {
        return drainage_num;
    }

    public void setDrainage_num(int drainage_num) {
        this.drainage_num = drainage_num;
    }

    public int getAdditional_days_date5_month() {
        return additional_days_date5_month;
    }

    public void setAdditional_days_date5_month(int additional_days_date5_month) {
        this.additional_days_date5_month = additional_days_date5_month;
    }

    public int getAdditional_days_date4_month() {
        return additional_days_date4_month;
    }

    public void setAdditional_days_date4_month(int additional_days_date4_month) {
        this.additional_days_date4_month = additional_days_date4_month;
    }

    public int getAdditional_days_date3_month() {
        return additional_days_date3_month;
    }

    public void setAdditional_days_date3_month(int additional_days_date3_month) {
        this.additional_days_date3_month = additional_days_date3_month;
    }

    public int getAdditional_days_date2_month() {
        return additional_days_date2_month;
    }

    public void setAdditional_days_date2_month(int additional_days_date2_month) {
        this.additional_days_date2_month = additional_days_date2_month;
    }

    public int getAdditional_days_date1_month() {
        return additional_days_date1_month;
    }

    public void setAdditional_days_date1_month(int additional_days_date1_month) {
        this.additional_days_date1_month = additional_days_date1_month;
    }

    public int getAdditional_days_date5_day() {
        return additional_days_date5_day;
    }

    public void setAdditional_days_date5_day(int additional_days_date5_day) {
        this.additional_days_date5_day = additional_days_date5_day;
    }

    public int getAdditional_days_date4_day() {
        return additional_days_date4_day;
    }

    public void setAdditional_days_date4_day(int additional_days_date4_day) {
        this.additional_days_date4_day = additional_days_date4_day;
    }

    public int getAdditional_days_date3_day() {
        return additional_days_date3_day;
    }

    public void setAdditional_days_date3_day(int additional_days_date3_day) {
        this.additional_days_date3_day = additional_days_date3_day;
    }

    public int getAdditional_days_date2_day() {
        return additional_days_date2_day;
    }

    public void setAdditional_days_date2_day(int additional_days_date2_day) {
        this.additional_days_date2_day = additional_days_date2_day;
    }

    public int getAdditional_days_date1_day() {
        return additional_days_date1_day;
    }

    public void setAdditional_days_date1_day(int additional_days_date1_day) {
        this.additional_days_date1_day = additional_days_date1_day;
    }

    public int getAdditional_days_date5_year() {
        return additional_days_date5_year;
    }

    public void setAdditional_days_date5_year(int additional_days_date5_year) {
        this.additional_days_date5_year = additional_days_date5_year;
    }

    public int getAdditional_days_date4_year() {
        return additional_days_date4_year;
    }

    public void setAdditional_days_date4_year(int additional_days_date4_year) {
        this.additional_days_date4_year = additional_days_date4_year;
    }

    public int getAdditional_days_date3_year() {
        return additional_days_date3_year;
    }

    public void setAdditional_days_date3_year(int additional_days_date3_year) {
        this.additional_days_date3_year = additional_days_date3_year;
    }

    public int getAdditional_days_date2_year() {
        return additional_days_date2_year;
    }

    public void setAdditional_days_date2_year(int additional_days_date2_year) {
        this.additional_days_date2_year = additional_days_date2_year;
    }

    public int getAdditional_days_date1_year() {
        return additional_days_date1_year;
    }

    public void setAdditional_days_date1_year(int additional_days_date1_year) {
        this.additional_days_date1_year = additional_days_date1_year;
    }

    public String getType_of_drain_other() {
        return type_of_drain_other;
    }

    public void setType_of_drain_other(String type_of_drain_other) {
        this.type_of_drain_other = type_of_drain_other;
    }

    public int getFull_assessment() {
        return full_assessment;
    }

    public void setFull_assessment(int full_assessment) {
        this.full_assessment = full_assessment;
    }
    
    public Integer getPain() {
        return pain;
    }


    public void setPain(Integer pain) {
        this.pain = pain;
    }
public String getPain_comments() {
        return pain_comments;
    }


    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }

    /**
     * @return the image_array3
     */
    public String getImage_array3() {
        return image_array3;
    }

    /**
     * @param image_array3 the image_array3 to set
     */
    public void setImage_array3(String image_array3) {
        this.image_array3 = image_array3;
    }
}