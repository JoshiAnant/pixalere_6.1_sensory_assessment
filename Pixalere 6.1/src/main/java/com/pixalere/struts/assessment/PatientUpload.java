/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.assessment;

import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.ImageManipulation;
import com.pixalere.utils.PDate;
import java.util.Date;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.Constants;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;

/**
 *
 * @author travis
 */
public class PatientUpload extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientUpload.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PatientUploadForm uploadform = (PatientUploadForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        boolean tokenValid = isTokenValid(request);
        if (tokenValid) {
            resetToken(request);

            PatientServiceImpl pservice = new PatientServiceImpl(language);
            AssessmentServiceImpl assessmentBD = new AssessmentServiceImpl(language);
            WoundAssessmentLocationServiceImpl assessWal = new WoundAssessmentLocationServiceImpl();
            WoundServiceImpl walManager = new WoundServiceImpl(language);
            WoundAssessmentServiceImpl waManager = new WoundAssessmentServiceImpl();
            AssessmentImagesServiceImpl imanager = new AssessmentImagesServiceImpl();
            AssessmentCommentsServiceImpl cservice = new AssessmentCommentsServiceImpl();

            ProfessionalVO user = (ProfessionalVO) session.getAttribute("userVO");
            if (user == null) {//session is invalid.. return to login
                request.setAttribute("session_invalid", "yes");
                return (mapping.findForward("system.logoff"));
            }
            Integer wound_profile_type_id = null;
            if (request.getParameter("wound_profile_type_id") == null || ((String) request.getParameter("wound_profile_type_id")).equals("")) {
                return (mapping.findForward("uploader.patientupload.success"));
            } else if (((String) request.getParameter("act")).equals("switchWound")) {
                int id = new Integer((String) request.getParameter("id"));
                //get Assessments by wound_profile_type.
                //set profile_id
            } else {
                try {
                    wound_profile_type_id = uploadform.getWound_profile_type_id();
                    WoundProfileTypeVO temp = new WoundProfileTypeVO();
                    temp.setId(wound_profile_type_id);
                    WoundProfileTypeVO wpt = walManager.getWoundProfileType(temp);
                    WoundVO wound = walManager.getWound(wpt.getWound_id());
                    PatientAccountVO patient = pservice.getPatient(wpt.getPatient_id());

                    //save Wound_assessment.
                    WoundAssessmentVO assess = new WoundAssessmentVO();
                    assess.setActive(1);
                    assess.setLastmodified_on(new Date());
                    assess.setHas_comments(1);
                    assess.setOffline_flag(0);
                    assess.setPatient_id(wpt.getPatient_id());
                    assess.setProfessional_id(user.getId());
                    assess.setCreated_on(new Date());

                    assess.setTreatment_location_id(patient.getTreatment_location_id());
                    assess.setUser_signature(new PDate().getProfessionalSignature(new Date(), user, locale));
                    assess.setVisit(0);
                    assess.setWound_id(wound.getId());
                    waManager.saveAssessment(assess);
                    WoundAssessmentVO assessment = waManager.getAssessment(assess);

                    //insert Referral.
                    ReferralsTrackingVO ref = new ReferralsTrackingVO();
                    ReferralsTrackingServiceImpl refservice = new ReferralsTrackingServiceImpl(language);
                    ref.setActive(1);
                    ref.setAssessment_id(assessment.getId());
                    ref.setCurrent(1);
                    ref.setEntry_type(0);
                    ref.setProfessional_id(user.getId());
                    ref.setCreated_on(new Date());
                    ref.setWound_id(assessment.getWound_id());
                    ref.setWound_profile_type_id(wpt.getId());
                    ref.setPatient_account_id(patient.getId());
                    refservice.saveReferralsTracking(ref);

                    ReferralsTrackingVO newreferral = refservice.getReferralsTracking(ref);


                    //save 2 images
                    ImageManipulation imageMan = new ImageManipulation(wpt.getPatient_id() + "", assess.getUser_signature(), Common.getPhotosPath(), assessment.getId() + "");
                    String imageName1 = "";
                    String imageName2 = "";
                    AssessmentImagesVO imgt = new AssessmentImagesVO();
                    imgt.setAssessment_id(new Integer(assessment.getId()));
                    //imgt.setAlpha_id(new Integer(alpha_id));
                    imgt.setWhichImage(Constants.IMAGE_1);
                    AssessmentImagesVO imageRec1 = imanager.getImage(imgt);
                    imgt.setWhichImage(Constants.IMAGE_2);
                    AssessmentImagesVO imageRec2 = imanager.getImage(imgt);

                    String currentAlpha = "";


                    long currenttime = PDate.getEpochTime();
                    FormFile imageForm1 = uploadform.getImage_1();
                    if (imageName1.equals("") && imageForm1 != null && !imageForm1.getFileName().equals("")) {

                        imageName1 = currenttime + "_" + currentAlpha + "-1.jpg";
                        String error = imageMan.processUploadedFile(imageForm1, currenttime + "_" + currentAlpha + "-1.jpg", 7);
                        if (!error.equals("")) {
                            ActionErrors errors = new ActionErrors();
                            request.setAttribute("exception", "y");

                            String exception = error;
                            request.setAttribute("exception_log", exception);
                            errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                            saveErrors(request, errors);
                            //return (mapping.findForward("pixalere.error"));
                        }
                    }

                    FormFile imageForm2 = uploadform.getImage_2();
                    if (imageName2.equals("") && imageForm2 != null && !imageForm2.getFileName().equals("")) {
                        imageName2 = currenttime + "_" + currentAlpha + "-2.jpg";
                        String error = imageMan.processUploadedFile(imageForm2, currenttime + "_" + currentAlpha + "-2.jpg", 7);
                        //test
                        if (!error.equals("")) {
                            ActionErrors errors = new ActionErrors();
                            request.setAttribute("exception", "y");

                            String exception = error;
                            request.setAttribute("exception_log", exception);
                            errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                            saveErrors(request, errors);
                            //return (mapping.findForward("pixalere.error"));
                        }
                    }
                    AssessmentImagesVO imagesVO1 = new AssessmentImagesVO();
                    imagesVO1.setPatient_id(wpt.getPatient_id());
                    imagesVO1.setWound_id(wpt.getWound_id());
                    imagesVO1.setAssessment_id(assessment.getId());
                    imagesVO1.setWound_profile_type_id(wpt.getId());

                    if (imageName1 != null && !imageName1.equals("")) {

                        imagesVO1.setImages(imageName1);
                        //imagesVO1.setAlpha_id(new Integer(alpha_id));
                        imagesVO1.setWhichImage(new Integer("1"));
                        // assessmentBD.removeImages((String)
                        // session.getAttribute("assessment_id"),(String)alphas.get(3));

                        AssessmentImagesVO i1 = new AssessmentImagesVO();
                        i1.setAssessment_id(assessment.getId());
                        //i1.setAlpha_id(new Integer(new Integer(alpha_id)));
                        i1.setWhichImage(1);
                        AssessmentImagesVO checkExists = imanager.getImage(i1);
                        if (checkExists == null || checkExists.getImages().equals("")) {
                            imanager.saveImage(imagesVO1);
                        }

                    }
                    AssessmentImagesVO imagesVO2 = new AssessmentImagesVO();
                    imagesVO2.setPatient_id(wpt.getPatient_id());
                    imagesVO2.setWound_id(wpt.getWound_id());
                    imagesVO2.setAssessment_id(assessment.getId());
                    imagesVO2.setWound_profile_type_id(wpt.getId());


                    if (imageName2 != null && !imageName2.equals("")) {

                        imagesVO2.setImages(imageName2);
                        //imagesVO2.setAlpha_id(new Integer(alpha_id));
                        imagesVO2.setWhichImage(new Integer("2"));
                        // assessmentBD.removeImages((String)
                        // session.getAttribute("assessment_id"),(String)alphas.get(3));

                        AssessmentImagesVO i1 = new AssessmentImagesVO();
                        i1.setAssessment_id(assessment.getId());
                        //i1.setAlpha_id(new Integer(new Integer(alpha_id)));
                        i1.setWhichImage(2);
                        AssessmentImagesVO checkExists = imanager.getImage(i1);
                        if (checkExists == null || checkExists.getImages().equals("")) {
                            imanager.saveImage(imagesVO2);
                        }

                    }
                    //save comments
                    AssessmentCommentsVO comment = new AssessmentCommentsVO();
                    comment.setAssessment_id(assessment.getId());
                    comment.setBody(uploadform.getComments());
                    comment.setComment_type(Constants.COMMENT_TYPE_NOTE);
                    comment.setPatient_id(assessment.getPatient_id());
                    comment.setPosition_id(user.getPosition_id());
                    if (newreferral != null) {
                        comment.setReferral_id(newreferral.getId());
                    }
                    comment.setProfessional_id(user.getId());
                    comment.setCreated_on(new Date());
                    comment.setUser_signature(new PDate().getProfessionalSignature(new Date(), user, locale));
                    comment.setWound_id(assessment.getWound_id());
                    comment.setWound_profile_type_id(wpt.getId());
                    cservice.saveComment(comment);

                } catch (Exception e) {
                    e.printStackTrace();
                    //throw new ApplicationException();
                }
            }
        }
        return (mapping.findForward(
                "uploader.patientupload.success"));

    }
}
