/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.assessment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.Constants;
/**
 *
 * @author travis
 */
public class PatientUploadForm  extends ActionForm {
    private FormFile image_1;
    private FormFile image_2;
    private Integer wound_profile_type_id;
    private String comments;
     private HttpSession session = null;


    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = (request.getSession());

        ActionErrors errors = new ActionErrors();
        if (getImage_1() != null && getImage_1().getFileName().length() > 0 && getImage_1().getFileSize() == 0) {
            errors.add("file1", new ActionError("pixalere.woundassessment.error.filemissing1"));
        }

        if (getImage_2() != null && getImage_2().getFileName().length() > 0 && getImage_2().getFileSize() == 0) {
            errors.add("file2", new ActionError("pixalere.woundassessment.error.filemissing2"));
        }

        return errors;

    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    /**
     * @return the image_1
     */
    public FormFile getImage_1() {
        return image_1;
    }

    /**
     * @param image_1 the image_1 to set
     */
    public void setImage_1(FormFile image_1) {
        this.image_1 = image_1;
    }

    /**
     * @return the image_2
     */
    public FormFile getImage_2() {
        return image_2;
    }

    /**
     * @param image_2 the image_2 to set
     */
    public void setImage_2(FormFile image_2) {
        this.image_2 = image_2;
    }

   
    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the wound_profile_type_id
     */
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }

    /**
     * @param wound_profile_type_id the wound_profile_type_id to set
     */
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
}
