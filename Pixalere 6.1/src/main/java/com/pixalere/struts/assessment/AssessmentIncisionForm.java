package com.pixalere.struts.assessment;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.utils.PDate;
import java.util.Date;
import com.pixalere.utils.Serialize;
import com.pixalere.auth.bean.ProfessionalVO;
public class AssessmentIncisionForm extends ActionForm {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentEachwoundForm.class);
    private int phone_visit;
    private int incision_closure_reason;
    private String[] postop_management;
    private int clinical_path;
    private int postop_days;
    private int clinical_path_date_month;
    private int clinical_path_date_year;
    private int clinical_path_date_day;
    private int cs_date_day;
    private String image_array3;
    private FormFile image_3;
    public FormFile getImage_3() {
        return image_3;
    }

    public void setImage_3(FormFile image_3) {
        this.image_3 = image_3;
    }
    private int cs_date_month;
    private int cs_date_year;
    private int cs_date_show;
    private int status;
    private int pain;
    private String pain_comments;
    private int incision_status;
    private String[] incision_exudate;
    private String[] peri_incisional;
    private String[] incision_closure_methods;
    private int incision_closure_status;
    private int exudate_amount;
    //images
    private String image_array;
    private String image_array2;
    private FormFile image_1;
    private FormFile image_2;
    private int full_assessment;
    //Closed Date
    private int closed_day;
    private int closed_month;
    private int closed_year;
    private int visit_freq;
    private int discharge_reason;
    private HttpSession session = null;
    

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        setSession(request.getSession());
        
        ActionErrors errors = new ActionErrors();

        if (image_1 != null && image_1.getFileName().length() > 0 && image_1.getFileSize() == 0) {
            errors.add("file1", new ActionError("pixalere.woundassessment.error.filemissing1"));
        }

        if (image_2 != null && image_2.getFileName().length() > 0 && image_2.getFileSize() == 0) {
            errors.add("file2", new ActionError("pixalere.woundassessment.error.filemissing2"));
        }

        return errors;

    }

    public AssessmentIncisionVO getFormData(ProfessionalVO userVO) throws ApplicationException {

        AssessmentIncisionVO assessmentIncisionVO = new AssessmentIncisionVO();
        if (full_assessment == -1) {
            full_assessment = 1;
        }
        PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
        assessmentIncisionVO.setActive(0);
        assessmentIncisionVO.setDeleted(0);
        assessmentIncisionVO.setFull_assessment(getFull_assessment());
        assessmentIncisionVO.setStatus(new Integer(getStatus() + ""));
        assessmentIncisionVO.setDischarge_reason(new Integer(getDischarge_reason()));
        assessmentIncisionVO.setClosed_date(pdate.getDate(getClosed_month(), getClosed_day(), getClosed_year()));
        try{
            if(assessmentIncisionVO.getClosed_date()==null && assessmentIncisionVO.getStatus().equals(Integer.parseInt(Common.getConfig("incisionClosed")))){
               assessmentIncisionVO.setClosed_date(new Date());
            }
        }catch(NullPointerException e){
            
        }catch(NumberFormatException e){
            
        }
        
        assessmentIncisionVO.setPatient_id(new Integer((String) getSession().getAttribute("patient_id")));
    	assessmentIncisionVO.setWound_id(new Integer((String) getSession().getAttribute("wound_id")));
        assessmentIncisionVO.setPostop_management(Serialize.serialize(getPostop_management()));
        assessmentIncisionVO.setIncision_exudate(Serialize.serialize(getIncision_exudate()));
        assessmentIncisionVO.setIncision_status(getIncision_status());
        assessmentIncisionVO.setPeri_incisional(Serialize.serialize(getPeri_incisional()));
        assessmentIncisionVO.setIncision_closure_methods(Serialize.serialize(getIncision_closure_methods()));
        //assessmentIncisionVO.setIncision_closure_status(new Integer(getIncision_closure_status()));
        assessmentIncisionVO.setExudate_amount(getExudate_amount());
        assessmentIncisionVO.setPain_comments(Common.convertCarriageReturns(pain_comments, false));
        assessmentIncisionVO.setClinical_path(new Integer(getClinical_path()));
        assessmentIncisionVO.setClinical_path_date(pdate.getDate(getClinical_path_date_month(), getClinical_path_date_day(), getClinical_path_date_year()));
        if(getPostop_days()>0){
        assessmentIncisionVO.setPostop_days(getPostop_days());
        }
        //dropdown
        assessmentIncisionVO.setCreated_on(new Date());
        assessmentIncisionVO.setPain(new Integer(getPain()));
        
        //set for all alphas incase some do not goto treatment.

        assessmentIncisionVO.setStatus(new Integer(getStatus() + ""));


        return assessmentIncisionVO;
    }

    public int getVisit_freq() {
        return visit_freq;
    }

    public void setVisit_freq(int visit_freq) {
        this.visit_freq = visit_freq;
    }

    public int getPain() {
        return pain;
    }

    public void setPain(int pain) {
        this.pain = pain;
    }

    public int getDischarge_reason() {
        return discharge_reason;
    }

    public void setDischarge_reason(int discharge_reason) {
        this.discharge_reason = discharge_reason;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    public String getImage_array() {
        return image_array;
    }

    public void setImage_array(String image_array) {
        this.image_array = image_array;
    }

    public String getImage_array2() {
        return image_array2;
    }

    public void setImage_array2(String image_array2) {
        this.image_array2 = image_array2;
    }

    public FormFile getImage_1() {
        return image_1;
    }

    public void setImage_1(FormFile image_1) {
        this.image_1 = image_1;
    }

    public FormFile getImage_2() {
        return image_2;
    }

    public void setImage_2(FormFile image_2) {
        this.image_2 = image_2;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIncision_status() {
        return incision_status;
    }

    public void setIncision_status(int incision_status) {
        this.incision_status = incision_status;
    }

    public String[] getIncision_exudate() {
        return incision_exudate;
    }

    public void setIncision_exudate(String[] incision_exudate) {
        this.incision_exudate = incision_exudate;
    }

    public String[] getPeri_incisional() {
        return peri_incisional;
    }

    public void setPeri_incisional(String[] peri_incisional) {
        this.peri_incisional = peri_incisional;
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    

    public int getClosed_day() {
        return closed_day;
    }

    public void setClosed_day(int closed_day) {
        this.closed_day = closed_day;
    }

    public int getClosed_month() {
        return closed_month;
    }

    public void setClosed_month(int closed_month) {
        this.closed_month = closed_month;
    }

    public int getClosed_year() {
        return closed_year;
    }

    public void setClosed_year(int closed_year) {
        this.closed_year = closed_year;
    }

    public String[] getIncision_closure_methods() {
        return incision_closure_methods;
    }

    public void setIncision_closure_methods(String[] incision_closure_methods) {
        this.incision_closure_methods = incision_closure_methods;
    }

    public int getIncision_closure_status() {
        return incision_closure_status;
    }

    public void setIncision_closure_status(int incision_closure_status) {
        this.incision_closure_status = incision_closure_status;
    }

    public int getExudate_amount() {
        return exudate_amount;
    }

    public void setExudate_amount(int exudate_amount) {
        this.exudate_amount = exudate_amount;
    }

    public String getPain_comments() {
        return pain_comments;
    }

    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }

    public int getCs_date_day() {
        return cs_date_day;
    }

    public void setCs_date_day(int cs_date_day) {
        this.cs_date_day = cs_date_day;
    }

    public int getCs_date_month() {
        return cs_date_month;
    }

    public void setCs_date_month(int cs_date_month) {
        this.cs_date_month = cs_date_month;
    }

    public int getCs_date_year() {
        return cs_date_year;
    }

    public void setCs_date_year(int cs_date_year) {
        this.cs_date_year = cs_date_year;
    }

    public int getCs_date_show() {
        return cs_date_show;
    }

    public void setCs_date_show(int cs_date_show) {
        this.cs_date_show = cs_date_show;
    }

    public int getClinical_path() {
        return clinical_path;
    }

    public void setClinical_path(int clinical_path) {
        this.clinical_path = clinical_path;
    }

    public int getClinical_path_date_month() {
        return clinical_path_date_month;
    }

    public void setClinical_path_date_month(int clinical_path_date_month) {
        this.clinical_path_date_month = clinical_path_date_month;
    }

    public int getClinical_path_date_year() {
        return clinical_path_date_year;
    }

    public void setClinical_path_date_year(int clinical_path_date_year) {
        this.clinical_path_date_year = clinical_path_date_year;
    }

    public int getClinical_path_date_day() {
        return clinical_path_date_day;
    }

    public void setClinical_path_date_day(int clinical_path_date_day) {
        this.clinical_path_date_day = clinical_path_date_day;
    }

    public int getPhone_visit() {
        return phone_visit;
    }

    public void setPhone_visit(int phone_visit) {
        this.phone_visit = phone_visit;
    }

    public int getIncision_closure_reason() {
        return incision_closure_reason;
    }

    public void setIncision_closure_reason(int incision_closure_reason) {
        this.incision_closure_reason = incision_closure_reason;
    }

    public String[] getPostop_management() {
        return postop_management;
    }

    public void setPostop_management(String[] postop_management) {
        this.postop_management = postop_management;
    }

    public int getPostop_days() {
        return postop_days;
    }

    public void setPostop_days(int postop_days) {
        this.postop_days = postop_days;
    }

    public int getFull_assessment() {
        return full_assessment;
    }

    public void setFull_assessment(int full_assessment) {
        this.full_assessment = full_assessment;
    }

    /**
     * @return the image_array3
     */
    public String getImage_array3() {
        return image_array3;
    }

    /**
     * @param image_array3 the image_array3 to set
     */
    public void setImage_array3(String image_array3) {
        this.image_array3 = image_array3;
    }


 
}
