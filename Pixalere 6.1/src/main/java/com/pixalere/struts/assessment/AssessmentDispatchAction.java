package com.pixalere.struts.assessment;

import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
/**
 * A {@link org.apache.struts.action.Action} action for the Assessment landing page
 *
 * @since 3.0
 * @author travis morris
 */
public class AssessmentDispatchAction extends Action {
    private static final Logger log = Logger.getLogger(AssessmentDispatchAction.class);
    private AssessmentServiceImpl assessmentManager = null;

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        ActionForward forward = new ActionForward();
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);

        try {

            String default_alpha = null;
            String wound_id = (String) session.getAttribute("wound_id");

            request.setAttribute("page", "assess");

            //shouldnt be here if we havent gone to wound profile first.
            if (wound_id == null || wound_id.length() == 0) {
                ActionErrors errors = new ActionErrors();
                errors.add("nowound_id", new ActionError("pixalere.assessment.form.error.nowound_id"));
                saveErrors(request, errors);
                request.setAttribute("warning_serverside", "1");
                return (mapping.findForward("uploader.assessment.failure"));
            }
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            request.setAttribute("offline", (Common.isOffline() == true ? "1" : "0"));

            String alpha_id = (String) session.getAttribute("alpha_id");
            
            WoundServiceImpl woundBD = new WoundServiceImpl(language);

            //get bluemodel image
            if (wound_id != null && wound_id.length() > 0) {
                WoundProfileVO woundVO2 = new WoundProfileVO();
                woundVO2.setWound_id(new Integer(wound_id));
                woundVO2.setActive(new Integer(0));
                woundVO2.setProfessional_id(userVO.getId());
                WoundProfileVO woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                if (woundVO == null) {
                    woundVO2 = new WoundProfileVO();
                    woundVO2.setWound_id(new Integer(wound_id));
                    woundVO2.setCurrent_flag(new Integer(1));
                    woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                }
                if(woundVO!=null){
                    request.setAttribute("wound_profile", woundVO.getWound());
                }else{
                    ActionErrors errors = new ActionErrors();
                    errors.add("nowound_id", new ActionError("pixalere.assessment.form.error.nowound_id"));
                    saveErrors(request, errors);
                    request.setAttribute("warning_serverside", "1");
                    return (mapping.findForward("uploader.assessment.failure"));
                }
            }
            // this will get all the current alphas to populate the alpha
            // dropdown in the assessment form
            // it calls show hide similar to wound profile but the excess coded
            // is ripped out and all it does
            // is populate the alphas in the dropdown on the assessment form
            Vector<String> alphas = null;
            String alphaid = "";
 
            assessmentManager = new AssessmentServiceImpl(language);
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentLocationVO wlvo = new WoundAssessmentLocationVO();
            WoundAssessmentLocationVO newVO = null;
            //if setup screen is disabled, we must get the first default alpha, and redirect.
           
                WoundAssessmentLocationVO activeLoco = new WoundAssessmentLocationVO();
                activeLoco.setWound_id(new Integer(wound_id));
                activeLoco.setDischarge(0);
                activeLoco.setActive(1);


                //activeLoco.
                WoundAssessmentLocationVO activeAlpha = walManager.getAlpha(activeLoco, false, Constants.HIDE_INCISIONS, Constants.WOUND_PROFILE_TYPE);
                //get default alpha in order of alpha_types.
                if (activeAlpha == null) {
                    activeAlpha = walManager.getAlpha(activeLoco, false, Constants.HIDE_INCISIONS, Constants.INCISIONTAG_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = walManager.getAlpha(activeLoco, false, Constants.HIDE_INCISIONS, Constants.TUBESANDDRAINS_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = walManager.getAlpha(activeLoco, false, Constants.HIDE_INCISIONS, Constants.SKIN_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = walManager.getAlpha(activeLoco, false, Constants.HIDE_INCISIONS, Constants.OSTOMY_PROFILE_TYPE);
                }
                if (activeAlpha == null) {
                    activeAlpha = walManager.getAlpha(activeLoco, false, Constants.HIDE_INCISIONS, Constants.BURN_PROFILE_TYPE);
                }
                WoundAssessmentLocationVO inactiveLoco = new WoundAssessmentLocationVO();
                inactiveLoco.setWound_id(new Integer(wound_id));
                inactiveLoco.setActive(0);
                inactiveLoco.setProfessional_id(userVO.getId());


                WoundAssessmentLocationVO inactiveAlpha = walManager.getAlpha(inactiveLoco, false, Constants.HIDE_INCISIONS, Constants.WOUND_PROFILE_TYPE);
                if (inactiveAlpha == null) {
                    inactiveAlpha = walManager.getAlpha(inactiveLoco, false, Constants.HIDE_INCISIONS, Constants.INCISIONTAG_PROFILE_TYPE);
                }
                if (inactiveAlpha == null) {
                    inactiveAlpha = walManager.getAlpha(inactiveLoco, false, Constants.HIDE_INCISIONS, Constants.TUBESANDDRAINS_PROFILE_TYPE);
                }
                if (inactiveAlpha == null) {
                    inactiveAlpha = walManager.getAlpha(inactiveLoco, false, Constants.HIDE_INCISIONS, Constants.SKIN_PROFILE_TYPE);
                }
                if (inactiveAlpha == null) {
                    inactiveAlpha = walManager.getAlpha(inactiveLoco, false, Constants.HIDE_INCISIONS, Constants.OSTOMY_PROFILE_TYPE);
                }
                if (inactiveAlpha == null) {
                    inactiveAlpha = walManager.getAlpha(inactiveLoco, false, Constants.HIDE_INCISIONS, Constants.BURN_PROFILE_TYPE);
                }

                if (activeAlpha == null && inactiveAlpha != null) {
                    activeAlpha = inactiveAlpha;
                } else if (inactiveAlpha != null && activeAlpha.getId().intValue() > inactiveAlpha.getId().intValue()) {
                    activeAlpha = inactiveAlpha;
                }
                if (alpha_id == null && activeAlpha != null) {
                    alpha_id = activeAlpha.getId() + "";
                }

         
            if (alpha_id == null) {
                 WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
                WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
                tloco.setWound_id(new Integer((String) wound_id));
                tloco.setDischarge(0); // Only open alphas
                Vector<WoundAssessmentLocationVO> allalphas = walservice.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, true);//4th parameter (extra sort) is ignored
                Hashtable alphaMap = new Hashtable();
                for (WoundAssessmentLocationVO v : allalphas) {
                    alphaMap.put(v.getAlpha(), v);
                }
                request.setAttribute("allalphas", alphaMap);
                if(allalphas!=null && allalphas.size()>0){
                    return (mapping.findForward("uploader.assessment.noalpha"));
                }else{
                    return (mapping.findForward("uploader.assessment.failure"));
                }
            } else {
                session.setAttribute("alpha_id", alpha_id);
                wlvo.setId(new Integer(alpha_id));
                wlvo.setWound_id(new Integer(wound_id));
                newVO = walManager.getAlpha(wlvo, true, Constants.HIDE_INCISIONS, null);

                // set a page parameter to which alpha is selected to set the alpha dropdown.
                if (newVO != null) {

                    request.setAttribute("selected_alpha", newVO.getAlpha());
                } else {
                    session.removeAttribute("alpha_id");
                }
                //Doesn't the individual setups ahandle this>'
                //request.setAttribute("unfinishedAlphas", getAllAlphas(new Integer((String) wound_id).intValue()));

            }

            //used in the setup a
            if (newVO != null) {
 
                request.setAttribute("anchor", request.getAttribute("anchor"));
                //used in setup action
                if ((newVO.getWound_profile_type().getAlpha_type() != null) && (newVO.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE))) {
                    return mapping.findForward("uploader.assessment.ostomy");
                } else if ((newVO.getWound_profile_type().getAlpha_type() != null) && (newVO.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE))) {
                    return mapping.findForward("uploader.assessment.drain");
                } else if ((newVO.getWound_profile_type().getAlpha_type() != null) && (newVO.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE))) {
                    return mapping.findForward("uploader.assessment.skin");
                } else if ((newVO.getWound_profile_type().getAlpha_type() != null) && (newVO.getWound_profile_type().getAlpha_type().equals(Constants.INCISIONTAG_PROFILE_TYPE))) {
                    return mapping.findForward("uploader.assessment.incision");
                } else if ((newVO.getWound_profile_type().getAlpha_type() != null) && (newVO.getWound_profile_type().getAlpha_type().equals(Constants.BURN_PROFILE_TYPE))) {
                    return mapping.findForward("uploader.assessment.burn");
                } else {
                    return mapping.findForward("uploader.assessment.wound");
                }
            } else {
                return mapping.findForward("uploader.assessment.noalpha");
            }


        } catch (ApplicationException e) {
            log.error("An application exception has been raised in AssessmentDispatchAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

    }
}
