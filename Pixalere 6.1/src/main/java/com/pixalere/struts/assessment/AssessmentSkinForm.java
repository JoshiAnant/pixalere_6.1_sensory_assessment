package com.pixalere.struts.assessment;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.utils.*;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Constants;
import java.util.Date;
/**
 * @author
 */
public class AssessmentSkinForm extends ActionForm {
    private int phone_visit;
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentSkinForm.class);
    private int full_assessment;
    


    private int wound_date_day;
    private int wound_date_month;
    private int wound_date_year;

    
    private String discharge_other;
    
    private int status;
    private int closed_day;
    private int closed_month;
    private int closed_year;
    
    private String healrate_timestamp;
    
    private int healrate_percent;
    
    private int discharge_reason;
    
    private String image_array;
    private String image_array2;
    private String image_array3;
    private FormFile image_1;
    private FormFile image_2;
    private FormFile image_3;
    
    
    private int pain;
    
    private double length;
    
    private double width;
    
    private double skin_temperature;
    private double depth;
    
    private String site;
    private int recurrent;
    private String pain_comments;
    
    
    
    HttpSession session = null;
  
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        session = request.getSession();
        ActionErrors errors = new ActionErrors();
        if (image_1!=null && image_1.getFileName().length()>0 && image_1.getFileSize() == 0) {
          errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
        }
        if (image_2!=null && image_2.getFileName().length()>0 && image_2.getFileSize() == 0) {
            errors.add("file2",new ActionError("pixalere.woundassessment.error.filemissing2"));
        }
        if (image_3!=null && image_3.getFileName().length()>0 && image_3.getFileSize() == 0) {
            errors.add("file3",new ActionError("pixalere.woundassessment.error.filemissing3"));
        }
        return errors;
        
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    
    /**
     * Returns the password.
     *
     * @return String
     */
    public int getPain() {
        return pain;
    }
    
    public void setPain(int pain) {
        this.pain = pain;
    }
    
    public void setClosed_day(int closed_day){
        this.closed_day=closed_day;
    }
    public int getClosed_day(){
        return closed_day;
    }
    public void setClosed_month(int closed_month){
        this.closed_month=closed_month;
    }
    public int getClosed_month(){
        return closed_month;
    }
    public void setClosed_year(int closed_year){
        this.closed_year=closed_year;
    }
    public int getClosed_year(){
        return closed_year;
    }
    /**
     * @method getFull_assessment
     * @field full_assessment - int
     * @return Returns the full_assessment.
     */
    public int getFull_assessment() {
        return full_assessment;
    }
    
    /**
     * @method setFull_assessment
     * @field full_assessment - int
     * @param full_assessment The full_assessment to set.
     */
    public void setFull_assessment(int full_assessment) {
        this.full_assessment = full_assessment;
    }
    
    public double getLength() {
        return length;
    }
    
    public void setLength(double length) {
        this.length = length;
    }
    
    public double getWidth() {
        return width;
    }
    
    public void setWidth(double width) {
        this.width = width;
    }
    
    public double getDepth() {
        return depth;
    }
    
    public void setDepth(double depth) {
        this.depth = depth;
    }
    
    
    
    public int getStatus() {
        return status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    
    
    
    public int getHealrate_percent() {
        return healrate_percent;
    }
    
    public void setHealrate_percent(int healrate_percent) {
        this.healrate_percent = healrate_percent;
    }
    
    public String getHealrate_timestamp() {
        return healrate_timestamp;
    }
    
    public void setHealrate_timestamp(String healrate_timestamp) {
        this.healrate_timestamp = healrate_timestamp;
    }
    
   
    
    public int getDischarge_reason() {
        return discharge_reason;
    }
    
    public void setDischarge_reason(int discharge_reason) {
        this.discharge_reason = discharge_reason;
    }
   
    public String getImage_array() {
        return image_array;
    }
    public void setImage_array(String image_array) {
        this.image_array = image_array;
    }
    public FormFile getImage_1() {
        return image_1;
    }
    
    public void setImage_1(FormFile image_1) {
        this.image_1 = image_1;
    }
    
    public FormFile getImage_2() {
        return image_2;
    }
    
    public void setImage_2(FormFile image_2) {
        this.image_2 = image_2;
    }
     public FormFile getImage_3() {
        return image_3;
    }
    
    public void setImage_3(FormFile image_3) {
        this.image_3 = image_3;
    }
    
    
    
    /**
     * @method getRecurrent
     * @field recurrent - int
     * @return Returns the recurrent.
     */
    public Integer getRecurrent() {
        return recurrent;
    }
    
    /**
     * @method setRecurrent
     * @field recurrent - int
     * @param recurrent
     *            The recurrent to set.
     */
    public void setRecurrent(Integer recurrent) {
        this.recurrent = recurrent;
    }
    
    public String getImage_array2() {
        return image_array2;
    }
    
    public void setImage_array2(String image_array2) {
        this.image_array2 = image_array2;
    }
        public String getImage_array3() {
        return image_array3;
    }
    
    public void setImage_array3(String image_array3) {
        this.image_array3 = image_array3;
    }
    
    public String getDischarge_other() {
        return discharge_other;
    }
    
    public void setDischarge_other(String discharge_other) {
        this.discharge_other = discharge_other;
    }
    
    
    public AssessmentSkinVO getFormData(ProfessionalVO userVO)  throws ApplicationException{
        AssessmentSkinVO assessmentEachwoundVO = new AssessmentSkinVO();
        if( full_assessment == -1){ full_assessment = 1; }
        PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
        assessmentEachwoundVO.setActive(0);
        assessmentEachwoundVO.setDeleted(0);
        assessmentEachwoundVO.setPatient_id(new Integer((String) session.getAttribute("patient_id")));
        assessmentEachwoundVO.setWound_id(new Integer((String) session.getAttribute("wound_id")));
        assessmentEachwoundVO.setPain(new Integer(getPain()));
        assessmentEachwoundVO.setLength(getLength());
        assessmentEachwoundVO.setSkin_temperature((getSkin_temperature()<0?null:getSkin_temperature()));
        assessmentEachwoundVO.setWidth(getWidth());
        assessmentEachwoundVO.setDepth(getDepth());
        assessmentEachwoundVO.setSite(getSite());
        assessmentEachwoundVO.setRecurrent(getRecurrent()==null?-1:new Integer(getRecurrent()));
        assessmentEachwoundVO.setClosed_date(pdate.getDate(getClosed_month(),getClosed_day(),getClosed_year()));
        try{
            if(assessmentEachwoundVO.getClosed_date()==null && assessmentEachwoundVO.getStatus().equals(Integer.parseInt(Common.getConfig("skinClosed")))){
               assessmentEachwoundVO.setClosed_date(new Date());
            }
        }catch(NullPointerException e){
            
        }catch(NumberFormatException e){
            
        }
        assessmentEachwoundVO.setWound_date(pdate.getDate(getWound_date_month(), getWound_date_day(), getWound_date_year()));
        assessmentEachwoundVO.setFull_assessment(new Integer(getFull_assessment()));
        
        
        //set for all alphas incase some do not goto treatment.
        assessmentEachwoundVO.setPain_comments(Common.convertCarriageReturns(pain_comments,false));

        assessmentEachwoundVO.setStatus(new Integer(getStatus() + ""));
        
        // ---------------
        assessmentEachwoundVO.setCreated_on(new Date());
        assessmentEachwoundVO.setDischarge_reason(new Integer(getDischarge_reason()));
        assessmentEachwoundVO.setDischarge_other(getDischarge_other());
          
        return assessmentEachwoundVO;
        
    }
    
    
    public String getPain_comments() {
        return pain_comments;
    }
    
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
   
    
    
    public int getPhone_visit() {
        return phone_visit;
    }
    
    public void setPhone_visit(int phone_visit) {
        this.phone_visit = phone_visit;
    }

    /**
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * @param site the site to set
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * @return the skin_temperature
     */
    public double getSkin_temperature() {
        return skin_temperature;
    }

    /**
     * @param skin_temperature the skin_temperature to set
     */
    public void setSkin_temperature(double skin_temperature) {
        this.skin_temperature = skin_temperature;
    }

    /**
     * @return the wound_date_day
     */
    public int getWound_date_day() {
        return wound_date_day;
    }

    /**
     * @param wound_date_day the wound_date_day to set
     */
    public void setWound_date_day(int wound_date_day) {
        this.wound_date_day = wound_date_day;
    }

    /**
     * @return the wound_date_month
     */
    public int getWound_date_month() {
        return wound_date_month;
    }

    /**
     * @param wound_date_month the wound_date_month to set
     */
    public void setWound_date_month(int wound_date_month) {
        this.wound_date_month = wound_date_month;
    }

    /**
     * @return the wound_date_year
     */
    public int getWound_date_year() {
        return wound_date_year;
    }

    /**
     * @param wound_date_year the wound_date_year to set
     */
    public void setWound_date_year(int wound_date_year) {
        this.wound_date_year = wound_date_year;
    }
   
}
