package com.pixalere.struts.assessment;

import java.util.Date;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import org.apache.log4j.Logger;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.ImageManipulation;
import com.pixalere.utils.PDate;

public class AssessmentBurn extends Action {
    private static final Logger logs = Logger.getLogger(AssessmentBurn.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);

        WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
        WoundAssessmentServiceImpl waManager = new WoundAssessmentServiceImpl();

        AssessmentBurnForm assessmentBurnForm = (AssessmentBurnForm) form;

        if (isTokenValid(request)) {
            resetToken(request);
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            try {
                String locale = Common.getLanguageLocale(language);
                
                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                String user_signature = pdate.getProfessionalSignature(new Date(), userVO, locale);

                AssessmentServiceImpl assessmentBD = new AssessmentServiceImpl(language);
                AssessmentImagesServiceImpl imanager = new AssessmentImagesServiceImpl();
                WoundAssessmentLocationServiceImpl walBD = new WoundAssessmentLocationServiceImpl();
                WoundAssessmentServiceImpl waBD = new WoundAssessmentServiceImpl();
                PatientServiceImpl pamanager = new PatientServiceImpl(language);
                String wound_id = (String) session.getAttribute("wound_id");
                String patient_id = (String) session.getAttribute("patient_id");
                String alpha_id = null;
                if (session.getAttribute("alpha_id") == null || ((String) session.getAttribute("alpha_id")).equals("")) {
                    return (mapping.findForward("uploader.assessment.noalpha"));
                } else {
                    alpha_id = (String) session.getAttribute("alpha_id");
                    if (request.getParameter("alpha_detail_id") != null && !((String) request.getParameter("alpha_detail_id")).equals("")) {//the forwarding alpha.. if it exists
                        //alpha_id = wound_location_details.id.. need to refactor and fix.
                        WoundLocationDetailsVO v = new WoundLocationDetailsVO();
                        v.setId(new Integer((String) request.getParameter("alpha_detail_id")));
                        WoundLocationDetailsVO detail = walBD.getAlphaDetail(v);
                        if (detail != null) {
                            session.setAttribute("alpha_id", detail.getAlpha_id() + "");
                        }
                    } else if (request.getParameter("alpha_id") != null && !((String) request.getParameter("alpha_id")).equals("")) {
                        session.setAttribute("alpha_id", (String) request.getParameter("alpha_id"));
                    }
                }

                String assessment_id = null;
                WoundAssessmentVO atmp = new WoundAssessmentVO();
                atmp.setWound_id(new Integer(wound_id));
                atmp.setActive(0);
                atmp.setProfessional_id(userVO.getId());
                WoundAssessmentVO tmpAssess = waBD.getAssessment(atmp);
                if (tmpAssess != null) {
                    assessment_id = tmpAssess.getId() + "";
                }
                if (((String) request.getParameter("cancel") == null) || !((String) request.getParameter("cancel")).equals("yes")) {
                    //Ensure assessment_id session variable exists
                    if ((assessment_id != null) && !(assessment_id.equals(""))) {
                        // do nothing is already present
                    } else {
                        //get assessment_id from wound_assessment table
                        WoundAssessmentVO assess = new WoundAssessmentVO();
                        assess.setPatient_id(new Integer(patient_id));
                        assess.setWound_id(new Integer(wound_id));
                        assess.setProfessional_id(new Integer(userVO.getId() + ""));
                        assess.setCreated_on(new Date());
                        assess.setActive(new Integer(0));
                        assess.setUser_signature(user_signature);
                        PatientAccountVO patientAcc = (PatientAccountVO) session.getAttribute("patientAccount");
                        if (patientAcc != null && patientAcc.getTreatment_location_id() != null) {
                            assess.setTreatment_location_id(patientAcc.getTreatment_location_id());
                        }
                        assess.setOffline_flag(Common.isOffline() == true ? 1 : 0);
                        waBD.saveAssessment(assess);
                        atmp = new WoundAssessmentVO();
                        atmp.setWound_id(new Integer(wound_id));
                        atmp.setActive(0);
                        atmp.setProfessional_id(userVO.getId());
                        tmpAssess = waBD.getAssessment(atmp);
                        if (tmpAssess != null) {
                            assessment_id = tmpAssess.getId() + "";
                        }

                    }

                    if (assessment_id != null) {
                        Common common = new Common();
                        String path_absolute = Common.getPhotosPath();

                        try {

                            ImageManipulation imageMan = new ImageManipulation(patient_id, user_signature, path_absolute, assessment_id);

                            AssessmentImagesVO imgt = new AssessmentImagesVO();
                            imgt.setAssessment_id(new Integer(assessment_id));
                            imgt.setAlpha_id(new Integer(alpha_id));
                            imgt.setWhichImage(Constants.IMAGE_1);
                            AssessmentImagesVO imageRec1 = imanager.getImage(imgt);
                            imgt.setWhichImage(Constants.IMAGE_2);
                            AssessmentImagesVO imageRec2 = imanager.getImage(imgt);

                            WoundAssessmentLocationVO crit = new WoundAssessmentLocationVO();

                            crit.setId(new Integer(alpha_id));

                            WoundAssessmentLocationVO currAlpha = walManager.getAlpha(crit, true, false, null);
                            String currentAlpha = "";
                            if (currAlpha != null) {
                                currentAlpha = currAlpha.getAlpha();
                            }

                            //remove images
                            if (imageRec1 != null && imageRec1.getImages() != null) {
                                //request.setAttribute("image_array", (Serialize.arrayIze(imageRec1.getImages()).size()>0)?Serialize.arrayIze(imageRec1.getImages()).get(0):"");
                                if (request.getParameter("delete") != null) {
                                    if (((String) request.getParameter("delete")).equals("image")) {
                                        imageRec1.setImages("");
                                        //request.setAttribute("image_array", "");
                                        imanager.saveImage(imageRec1);
                                    }
                                }
                            }

                            if (imageRec2 != null && imageRec2.getImages() != null) {
                                //Why are we doing this?request.setAttribute("image_array2", (Serialize.arrayIze(imageRec2.getImages()).size()>0)?Serialize.arrayIze(imageRec2.getImages()).get(0):"");
                                if (request.getParameter("delete") != null) {
                                    if (((String) request.getParameter("delete")).equals("imageExtra")) {
                                        imageRec2.setImages("");
                                        //request.setAttribute("image_array2", "");
                                        imanager.saveImage(imageRec2);
                                    }
                                }
                            }
                            imgt.setWhichImage(Constants.IMAGE_3);
                            AssessmentImagesVO imageRec3 = imanager.getImage(imgt);
                            if (imageRec3 != null && imageRec3.getImages() != null) {
                                //request.setAttribute("image_array2", (Serialize.arrayIze(imageRec2.getImages()).size()>0)?Serialize.arrayIze(imageRec2.getImages()).get(0):"");
                                if (request.getParameter("delete") != null) {
                                    if (((String) request.getParameter("delete")).equals("imageExtra2")) {
                                        imageRec3.setImages("");
                                        //request.setAttribute("image_array2", "");
                                        imanager.saveImage(imageRec3);
                                    }
                                }
                            }
                            String imageName3 = "";
                            if (assessmentBurnForm.getImage_array3() != null) {
                                imageName3 = assessmentBurnForm.getImage_array3();
                            }
                            String imageName1 = "";
                            String imageName2 = "";
                            if (assessmentBurnForm.getImage_array() != null) {
                                imageName1 = assessmentBurnForm.getImage_array();

                            }
                            if (assessmentBurnForm.getImage_array2() != null) {
                                imageName2 = assessmentBurnForm.getImage_array2();
                            }

                            long currenttime = pdate.getEpochTime();
                            FormFile imageForm1 = assessmentBurnForm.getImage_1();
                            if (imageName1.equals("") && imageForm1 != null && !imageForm1.getFileName().equals("")) {
                                imageName1 = currenttime + "_" + currentAlpha + "-1.jpg";
                                String error = imageMan.processUploadedFile(imageForm1, currenttime + "_" + currentAlpha + "-1.jpg", 7);
                                if (!error.equals("")) {
                                    ActionErrors errors = new ActionErrors();
                                    request.setAttribute("exception", "y");

                                    String exception = error;
                                    request.setAttribute("exception_log", exception);
                                    errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                                    saveErrors(request, errors);
                                    //return (mapping.findForward("pixalere.error"));
                                }
                            }

                            FormFile imageForm2 = assessmentBurnForm.getImage_2();
                            if (imageName2.equals("") && imageForm2 != null && !imageForm2.getFileName().equals("")) {
                                imageName2 = currenttime + "_" + currentAlpha + "-1.jpg";
                                String error = imageMan.processUploadedFile(imageForm2, currenttime + "_" + currentAlpha + "-2.jpg", 7);
                                if (!error.equals("")) {
                                    ActionErrors errors = new ActionErrors();
                                    request.setAttribute("exception", "y");

                                    String exception = error;
                                    request.setAttribute("exception_log", exception);
                                    errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                                    saveErrors(request, errors);
                                    //return (mapping.findForward("pixalere.error"));
                                }
                            } 
                            FormFile imageForm3 = assessmentBurnForm.getImage_3();
                            if (imageName3.equals("") && imageForm3 != null && !imageForm3.getFileName().equals("")) {
                                imageName3 = currenttime + "_" + currentAlpha + "-3.jpg";
                                String error = imageMan.processUploadedFile(imageForm3, currenttime + "_" + currentAlpha + "-2.jpg", 7);
                                if (!error.equals("")) {
                                    ActionErrors errors = new ActionErrors();
                                    request.setAttribute("exception", "y");

                                    String exception = error;
                                    request.setAttribute("exception_log", exception);
                                    errors.add("exception", new ActionError("pixalere.assesment.error.saving_images"));
                                    saveErrors(request, errors);
                                    //return (mapping.findForward("pixalere.error"));
                                }
                            } 
                            // sets alpha from tokenized stirng above
                            // Setting global fields if they exist previously.
                            AssessmentBurnVO assessmentBurnVO = assessmentBurnForm.getFormData(userVO);
                            AssessmentBurnVO initBurn = new AssessmentBurnVO();
                            initBurn.setAssessment_id(new Integer(assessment_id));
                            initBurn.setAlpha_id(new Integer(alpha_id));
                            AssessmentBurnVO vo = (AssessmentBurnVO) assessmentBD.getAssessment(initBurn);
                            assessmentBurnVO.setAssessment_id(new Integer(assessment_id));
                            assessmentBurnVO.setAlpha_id(new Integer(alpha_id));
                            if (vo != null) {
                                assessmentBurnVO.setId(vo.getId());
                                // set products back if there are any

                                if (assessmentBurnVO.getStatus() == null || assessmentBurnVO.getStatus().equals(new Integer(0))) {
                                    //get default Active... if status is disabled or empty.
                                    if (Common.getConfig("BurnActive") != null && !Common.getConfig("burnActive").equals("")) {
                                        assessmentBurnVO.setStatus(new Integer(Common.getConfig("burnActive")));
                                    }
                                }
                                //TODO what need to be set here?
                            } else {
                                // if there are none initialize to ""

                                if (assessmentBurnVO.getStatus() == null || assessmentBurnVO.getStatus().equals(new Integer(0))) {

                                    //get default Active... if status is disabled or empty.
                                    if (Common.getConfig("BurnActive") != null && !Common.getConfig("burnActive").equals("")) {
                                        assessmentBurnVO.setStatus(new Integer(Common.getConfig("burnActive")));
                                    }
                                }
                                //TODO what need to be set here?

                            }
                            assessmentBurnVO.setWound_profile_type_id(currAlpha.getWound_profile_type_id());
                            assessmentBD.saveAssessment(assessmentBurnVO);

                            //inserting images now
                            AssessmentImagesVO imagesVO1 = new AssessmentImagesVO();
                            imagesVO1.setPatient_id(new Integer(patient_id));
                            imagesVO1.setWound_id(new Integer(wound_id));
                            imagesVO1.setAssessment_id(new Integer(assessment_id));
                            imagesVO1.setWound_profile_type_id(currAlpha.getWound_profile_type_id());
                            if (imageRec1 != null) {
                                imagesVO1 = imageRec1;
                            }

                            if (imageName1 != null && !imageName1.equals("") && assessmentBurnForm.getImage_array().equals("")
                                    && (request.getParameter("delete") != null && !((String) request.getParameter("delete")).equals("image"))) {
                                imagesVO1.setImages(imageName1);
                                imagesVO1.setAlpha_id(new Integer(alpha_id));
                                imagesVO1.setWhichImage(new Integer("1"));
                                AssessmentImagesVO i1 = new AssessmentImagesVO();
                                i1.setAssessment_id(new Integer(assessment_id));
                                i1.setAlpha_id(new Integer(alpha_id));
                                i1.setWhichImage(1);
                                AssessmentImagesVO checkExists = imanager.getImage(i1);
                                if (checkExists == null || checkExists.getImages().equals("")) {
                                    imanager.saveImage(imagesVO1);
                                }

                            }

                            AssessmentImagesVO imagesVO2 = new AssessmentImagesVO();
                            imagesVO2.setPatient_id(new Integer(patient_id));
                            imagesVO2.setWound_id(new Integer(wound_id));
                            imagesVO2.setAssessment_id(new Integer(assessment_id));
                            imagesVO2.setWound_profile_type_id(currAlpha.getWound_profile_type_id());
                            if (imageRec2 != null) {
                                imagesVO2 = imageRec2;
                            }

                            if (imageName2 != null && !imageName2.equals("") && (request.getParameter("delete") != null
                                    && !((String) request.getParameter("delete")).equals("imageExtra"))) {
                                imagesVO2.setImages(imageName2);
                                imagesVO2.setAlpha_id(new Integer(alpha_id));
                                imagesVO2.setWhichImage(new Integer("2"));

                                AssessmentImagesVO i2 = new AssessmentImagesVO();
                                i2.setAssessment_id(new Integer(assessment_id));
                                i2.setAlpha_id(new Integer(alpha_id));
                                i2.setWhichImage(2);
                                AssessmentImagesVO checkExists = imanager.getImage(i2);
                                if (checkExists == null || checkExists.getImages().equals("")) {
                                    imanager.saveImage(imagesVO2);
                                }

                            }
                            
                            AssessmentImagesVO imagesVO3 = new AssessmentImagesVO();
                            imagesVO3.setPatient_id(new Integer(patient_id));
                            imagesVO3.setWound_id(new Integer(wound_id));
                            imagesVO3.setAssessment_id(new Integer(assessment_id));
                            imagesVO3.setWound_profile_type_id(currAlpha.getWound_profile_type_id());
                            if (imageRec3 != null) {
                                imagesVO3 = imageRec3;
                            }

                            if (imageName3 != null && !imageName3.equals("") && (request.getParameter("delete") != null &&
                                    !((String) request.getParameter("delete")).equals("imageExtra2"))) {
                                imagesVO3.setImages(imageName3);
                                imagesVO3.setAlpha_id(new Integer(alpha_id));
                                imagesVO3.setWhichImage(new Integer("3"));
                                AssessmentImagesVO i2 = new AssessmentImagesVO();
                                i2.setAssessment_id(new Integer(assessment_id));
                                i2.setAlpha_id(new Integer(alpha_id));
                                i2.setWhichImage(3);
                                AssessmentImagesVO checkExists = imanager.getImage(i2);
                                if ((checkExists == null || checkExists.getImages().equals(""))) {
                                    imanager.saveImage(imagesVO3);
                                }
                            }
                            AssessmentBurnVO assessment = new AssessmentBurnVO();
                            assessment.setAssessment_id(new Integer(assessment_id));
                            assessment.setAlpha_id(new Integer(alpha_id));
                            assessment.setPatient_id(new Integer(patient_id));
                            assessment.setWound_id(new Integer(wound_id));


                            logs.info("##$$##$$##-----END ASSESSMENTEACHWOUND.JAVA-----##$$##$$##");
                            /** ******************************************************************* */
                        } catch (ApplicationException e) {
                            logs.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
                            ActionErrors errors = new ActionErrors();
                            request.setAttribute("exception", "y");
                            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                            errors.add("exception", new ActionError("pixalere.common.error"));
                            saveErrors(request, errors);
                        }
                    }
                } else {

                    AssessmentBurnVO vo = new AssessmentBurnVO();

                    if (assessment_id != null) {
                        vo.setAssessment_id(new Integer(assessment_id));
                        vo.setActive(null);
                        vo.setAlpha_id(new Integer(alpha_id));

                        if (vo.getAssessment_id() != null && vo.getAlpha_id() != null) {
                            assessmentBD.removeAssessment(vo);
                            imanager.removeImages(vo.getAssessment_id() + "", vo.getAlpha_id() + "");

                        }
                    }


                }
            } catch (ApplicationException e) {
                logs.error("An application exception has been raised in AssessmentEachwound.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
            }
        }
        ActionForward forward = new ActionForward();

        String pageParam = request.getParameter("page");

        if (pageParam == null) {
            forward = (mapping.findForward("uploader.go.assess"));
        }

        if (pageParam != null && pageParam.equals("patient")) {
            forward = (mapping.findForward("uploader.go.patient"));
        } else if (pageParam != null && pageParam.equals("woundprofiles")) {
            forward = (mapping.findForward("uploader.go.profile"));
        } else if (pageParam != null && pageParam.equals("treatment")) {
            forward = (mapping.findForward("uploader.go.treatment"));
        } else if (pageParam != null && pageParam.equals("summary")) {
            forward = (mapping.findForward("uploader.go.summary"));
        } else if (pageParam != null && pageParam.equals("reporting")) {
            forward = (mapping.findForward("uploader.go.reporting"));
        } else if (pageParam != null && pageParam.equals("viewer")) {
            forward = (mapping.findForward("uploader.go.viewer"));
        } else if (pageParam != null && pageParam.equals("admin")) {
            forward = (mapping.findForward("uploader.go.admin"));
        } else {
            forward = (mapping.findForward("uploader.go.assess"));
        }

        return forward;
    }
}
