package com.pixalere.struts.assessment;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.TreeMap;
import com.pixalere.utils.PDate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.bean.SummaryVO;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Common;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import java.util.ArrayList;
import java.util.Collection;

public class AssessmentBurnSetupAction extends Action {

    private static final Logger log = Logger.getLogger(AssessmentBurnSetupAction.class);

    public Hashtable getAreaValues(String strInput, String[] arrAreas) {
        Hashtable hash = new Hashtable();
        if (strInput != null && strInput.trim().length() > 0) {
            for (int intAreas = 0; intAreas < arrAreas.length; intAreas++) {
                String strValues = "";
                int intPos = strInput.indexOf(arrAreas[intAreas]);
                if (intPos > -1) {
                    strValues = strInput.substring(intPos);
                    intPos = strValues.indexOf(":");
                    if (intPos > -1) {
                        strValues = strValues.substring(intPos + 1);
                    }
                    intPos = strValues.indexOf(";");
                    if (intPos > -1) {
                        strValues = strValues.substring(0, intPos);
                    }
                    hash.put(arrAreas[intAreas], strValues.split(","));
                }
            }
        }
        return hash;
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }

            System.out.println("----- Setup Burns ");

            //Generate Token - prevent multiple posts
            saveToken(request);
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentServiceImpl waManager = new WoundAssessmentServiceImpl();
            AssessmentImagesServiceImpl imanager = new AssessmentImagesServiceImpl();
            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            comp.setFlowchart(com.pixalere.utils.Constants.BURN_ASSESSMENT);
            java.util.Collection components = gui.getAllComponents(comp);
            java.util.Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);
            ReportBuilder reportService = new ReportBuilder(language);
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            String patient_id = (String) session.getAttribute("patient_id");
            PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
            PatientProfileVO tmpProfile = pmanager.getTemporaryPatientProfile(Integer.parseInt(patient_id), userVO.getId());
            if (tmpProfile == null) {
                PatientProfileVO pp = new PatientProfileVO();
                pp.setPatient_id(new Integer(patient_id));
                pp.setCurrent_flag(new Integer(1));
                pp.setActive(new Integer(1));
                pp.setDeleted(new Integer(0));
                tmpProfile = pmanager.getPatientProfile(pp);
            }
            Collection resultsPP = new ArrayList();
            List<FieldValues> comparedppdata = null;
            if (tmpProfile != null) {
                resultsPP.add(tmpProfile);
                RowData[] ppRecord = pmanager.getAllPatientProfilesForFlowchart(resultsPP, userVO, false, false);
                comparedppdata = Common.compareGlobal(ppRecord);//Getting flowchart data.
            }
            request.setAttribute("patient_profile", reportService.organizeSummary(comparedppdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.patientprofile", locale)));

            AssessmentBurnVO vo = new AssessmentBurnVO();

            String wound_id = (String) session.getAttribute("wound_id");
            String alpha_id = (String) session.getAttribute("alpha_id");
            String assessment_id = null;
            WoundAssessmentVO atmp = new WoundAssessmentVO();
            atmp.setWound_id(new Integer(wound_id));
            atmp.setActive(0);
            atmp.setProfessional_id(userVO.getId());
            WoundAssessmentVO tmpAssess = waManager.getAssessment(atmp);
            if (tmpAssess != null) {
                assessment_id = tmpAssess.getId() + "";
            }
            // User selected Populate previous
            if (request.getParameter("pop") != null && ((String) request.getParameter("pop")).equals("1")) {
                //Burn has no dimension data, it should not be a full assessemnt ==>>> ????? This is copieed from Ostomy...
                AssessmentBurnVO inc = new AssessmentBurnVO();
                inc.setAlpha_id(new Integer(alpha_id));
                inc.setWound_id(new Integer(wound_id));
                inc.setFull_assessment(new Integer((String) request.getParameter("full")));
                vo = (AssessmentBurnVO) manager.retrieveLastActiveAssessment(inc, Constants.BURN_PROFILE_TYPE, -1);
                if (vo != null && request.getParameter("full") != null) {
                    vo.setFull_assessment(new Integer((String) request.getParameter("full")));
                }
            } else {

                if (assessment_id != null) {
                    AssessmentBurnVO Burn = new AssessmentBurnVO();
                    Burn.setAlpha_id(new Integer(alpha_id));
                    Burn.setAssessment_id(new Integer(assessment_id));
                    vo = (AssessmentBurnVO) manager.getAssessment(Burn);
                }

            }

            if (vo != null) {
                // set assessment object to form
                request.setAttribute("assessment", vo);
                request.setAttribute("date_of_onset", vo.getAgeofWoundDate());
                request.setAttribute("unserializedBurn_wound", Serialize.arrayIze(vo.getBurn_wound()));
                request.setAttribute("unserializedExudate_colour", Serialize.arrayIze((vo.getExudate_colour())));
                request.setAttribute("unserializedExudate_amount", Serialize.arrayIze((vo.getExudate_amount())));
                request.setAttribute("unserializedGrafts", Serialize.arrayIze((vo.getGrafts())));
                request.setAttribute("unserializedDonors", Serialize.arrayIze((vo.getDonors())));

                for(Object s : Serialize.arrayIze((vo.getDonors()))){
                    System.out.println(s);
                }
                request.setAttribute("pain_comments", Common.convertCarriageReturns(vo.getPain_comments(), true));
            }

            if (assessment_id != null) {
                AssessmentImagesVO imgt = new AssessmentImagesVO();
                imgt.setAssessment_id(new Integer(assessment_id));
                imgt.setAlpha_id(new Integer(alpha_id));
                imgt.setWhichImage(Constants.IMAGE_1);
                AssessmentImagesVO image = imanager.getImage(imgt);
                imgt.setWhichImage(Constants.IMAGE_2);
                AssessmentImagesVO extraImage = imanager.getImage(imgt);

                if (image != null && image.getImages() != null) {
                    request.setAttribute("image_array", image.getImages());
                }

                if (extraImage != null && extraImage.getImages() != null) {
                    request.setAttribute("image_array2", extraImage.getImages());
                }
            }

            request.setAttribute("isRequired", "1");

            log.info("##$$##$$##-----GRABBING THE USER SIGNATURE-----##$$##$$##");
            AssessmentBurnVO assess = new AssessmentBurnVO();
            assess.setAlpha_id(new Integer(alpha_id));
            assess.setWound_id(new Integer(wound_id));

            AssessmentBurnVO voUpdate = (AssessmentBurnVO) manager.retrieveLastActiveAssessment(assess, Constants.BURN_PROFILE_TYPE, -1);

            if (voUpdate != null) {

                log.info("##$$##$$##-----THERE HAS BEEN A PREVIOUS ASSESSMENT WITH A SIGNATURE-----##$$##$$##");

                WoundAssessmentVO assessVO = voUpdate.getWoundAssessment();

                if (assessVO != null) {

                    request.setAttribute("user_signature", assessVO.getUser_signature());
                    request.setAttribute("profName", (Common.getProfessionalsName((String) request.getAttribute("user_signature"), locale)));
                } else {
                    request.setAttribute("user_signature", Common.getLocalizedString("pixalere.na", locale));
                }
            } else {
                log.info("##$$##$$##-----NO PREVIOUS ASSESSMENT, SIGNATURE IS N/A-----##$$##$$##");

                request.setAttribute("user_signature", Common.getLocalizedString("pixalere.na", locale));
            }

            // Populate Lists
            log.info("##$$##$$##-----INITIALIZE ALL PAGE CONTROLS-----##$$##$$##");

            ListServiceImpl listBD = new ListServiceImpl(language);

            request.setAttribute("wound_status", listBD.getLists(LookupVO.BURN_STATUS));
            request.setAttribute("discharge_reason", listBD.getLists(LookupVO.BURN_DISCHARGE_REASON));
            request.setAttribute("burn_status", listBD.getLists(LookupVO.BURN_STATUS));
            request.setAttribute("burn_wound", listBD.getLists(LookupVO.BURN_WOUND));
            request.setAttribute("exudate_colour", listBD.getLists(LookupVO.BURN_EXUDATE_COLOUR));
            request.setAttribute("exudate_amount", listBD.getLists(LookupVO.BURN_EXUDATE_AMOUNT));
            request.setAttribute("grafts", listBD.getLists(LookupVO.BURN_GRAFTS));
            request.setAttribute("donors", listBD.getLists(LookupVO.BURN_DONORS));
            request.setAttribute("burn_closure_reason", listBD.getLists(LookupVO.BURN_DISCHARGE_REASON));

            request.setAttribute("pain", listBD.getLists(LookupVO.PAIN));
            log.info("##$$##$$##-----END ASSESSMENTBurnSETUPACTION-----##$$##$$##");

            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
            tloco.setWound_id(new Integer((String) wound_id));
            tloco.setDischarge(0); // Only open alphas
            Vector<WoundAssessmentLocationVO> allalphas = walservice.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, true);//4th parameter (extra sort) is ignored
            TreeMap alphaMap = new TreeMap();
            for (WoundAssessmentLocationVO v : allalphas) {
                alphaMap.put(v.getAlpha(), v);
            }
            request.setAttribute("allalphas", alphaMap);
            if (assessment_id != null) {

                //list of unfinished alphas ("A", "B", "C",...)
                AssessmentBurnVO drainer = new AssessmentBurnVO();
                drainer.setWound_id(new Integer(wound_id));
                drainer.setAssessment_id(new Integer(assessment_id));
                List<String> unfinishedAlphas = walManager.computeUncompletedAlphas(drainer, userVO.getId());
                StringBuilder unfinishedAlphasString = new StringBuilder();

                if (unfinishedAlphas != null) {
                    for (String unfinishedAlpha : unfinishedAlphas) {
                        unfinishedAlphasString.append('\"');
                        unfinishedAlphasString.append(unfinishedAlpha);
                        unfinishedAlphasString.append('\"');
                        unfinishedAlphasString.append(',');
                    }
                }

                if (unfinishedAlphas != null && unfinishedAlphas.size() > 0) {
                    request.setAttribute("unfinishedAlphas", unfinishedAlphasString.substring(0, unfinishedAlphasString.length() - 1));
                } else {
                    request.setAttribute("unfinishedAlphas", "");
                }
            } else {
                //a null assessment id means that no assessments have been completed
                Vector<WoundAssessmentLocationVO> allAlphas = walManager.retrieveLocationsForWound(userVO.getId(), Integer.parseInt(wound_id), false, false);

                StringBuilder unfinishedAlphasString = new StringBuilder();

                for (WoundAssessmentLocationVO loc : allAlphas) {
                    unfinishedAlphasString.append('\"');
                    unfinishedAlphasString.append(loc.getAlpha());
                    unfinishedAlphasString.append('\"');
                    unfinishedAlphasString.append(',');

                    if (allAlphas.size() > 0) {
                        //cut off the trailing comma
                        request.setAttribute("unfinishedAlphas", unfinishedAlphasString.substring(0, unfinishedAlphasString.length() - 1));
                    } else {
                        request.setAttribute("unfinishedAlphas", "");
                    }
                }
            }

            //get bluemodel image
            if (wound_id != null && wound_id.length() > 0) {
                WoundServiceImpl woundBD = new WoundServiceImpl(language);
                WoundProfileVO woundVO2 = new WoundProfileVO();
                woundVO2.setWound_id(new Integer(wound_id));
                woundVO2.setActive(new Integer(0));
                woundVO2.setProfessional_id(userVO.getId());
                WoundProfileVO woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                if (woundVO == null) {
                    woundVO2 = new WoundProfileVO();
                    woundVO2.setWound_id(new Integer(wound_id));
                    woundVO2.setCurrent_flag(new Integer(1));
                    woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                }
                request.setAttribute("wound_profile", woundVO.getWound());
                Collection resultsWP = new ArrayList();
                List<FieldValues> comparedwpdata = null;
                if (woundVO != null) {
                    resultsWP.add(woundVO);
                    RowData[] wpRecord = woundBD.getAllWoundProfilesForFlowchart(resultsWP, userVO, false,false);
                    comparedwpdata = Common.compareGlobal(wpRecord);//Getting flowchart data.
                }
                request.setAttribute("wound_profile_readonly",reportService.organizeSummary(comparedwpdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.woundprofile", locale)));
            
            }
            if (assessment_id != null) {
                request.setAttribute("assessment_id", assessment_id);
            } else {
                //create one
                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                WoundAssessmentVO assess2 = new WoundAssessmentVO();
                assess2.setPatient_id(new Integer(patient_id));
                assess2.setWound_id(new Integer(wound_id));
                assess2.setProfessional_id(new Integer(userVO.getId() + ""));
                assess2.setCreated_on(new Date());
                assess2.setActive(new Integer(0));
                assess2.setOffline_flag(Common.isOffline() == true ? 1 : 0);

                String user_signature = pdate.getProfessionalSignature(new Date(), userVO, locale);
                assess2.setUser_signature(user_signature);
                PatientAccountVO patientAcc = (PatientAccountVO) session.getAttribute("patientAccount");
                assess2.setTreatment_location_id(patientAcc.getTreatment_location_id());

                waManager.saveAssessment(assess2);
                WoundAssessmentVO wat = new WoundAssessmentVO();
                wat.setWound_id(new Integer(wound_id));
                wat.setProfessional_id(userVO.getId());
                WoundAssessmentVO assessmentObject = waManager.getAssessment(wat);
                if (assessmentObject != null) {
                    request.setAttribute("assessment_id", assessmentObject.getId());
                }
            }

        } catch (ApplicationException e) {

            log.error("An application exception has been raised in AssessmentEachwoundSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        return (mapping.findForward("uploader.assessment.success"));
    }

}
