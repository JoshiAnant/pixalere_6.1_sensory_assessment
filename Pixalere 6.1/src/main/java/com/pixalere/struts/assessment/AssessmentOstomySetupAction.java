package com.pixalere.struts.assessment;

import java.util.List;
import java.util.Date;
import java.util.Vector;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.PDate;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.bean.SummaryVO;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Common;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import java.util.ArrayList;

public class AssessmentOstomySetupAction extends Action {
    private static final Logger log = Logger.getLogger(AssessmentOstomySetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
            request.setAttribute("page", "assess");
            //Generate Token - prevent multiple posts
            saveToken(request);
            AssessmentServiceImpl manager = new AssessmentServiceImpl(language);
            AssessmentImagesServiceImpl imanager = new AssessmentImagesServiceImpl();
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentServiceImpl waManager = new WoundAssessmentServiceImpl();
            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            comp.setFlowchart(com.pixalere.utils.Constants.OSTOMY_ASSESSMENT);
            java.util.Collection components = gui.getAllComponents(comp);
            java.util.Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");
            String alpha_id = (String) session.getAttribute("alpha_id");

            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            AssessmentOstomyVO vo = new AssessmentOstomyVO();
            ReportBuilder reportService = new ReportBuilder(language);
PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
            PatientProfileVO tmpProfile = pmanager.getTemporaryPatientProfile(Integer.parseInt(patient_id), userVO.getId());
            if(tmpProfile==null){
                PatientProfileVO pp = new PatientProfileVO();
                pp.setPatient_id(new Integer(patient_id));
                pp.setCurrent_flag(new Integer(1));
                pp.setActive(new Integer(1));
                pp.setDeleted(new Integer(0));
                tmpProfile = pmanager.getPatientProfile(pp);
            }
                Collection resultsPP = new ArrayList();
                List<FieldValues> comparedppdata = null;
                if (tmpProfile != null) {
                    resultsPP.add(tmpProfile);
                    RowData[] ppRecord = pmanager.getAllPatientProfilesForFlowchart(resultsPP, userVO, false,false);
                    comparedppdata = Common.compareGlobal(ppRecord);//Getting flowchart data.
                }
                request.setAttribute("patient_profile",reportService.organizeSummary(comparedppdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.patientprofile", locale)));
            
            String assessment_id = null;
            WoundAssessmentVO atmp = new WoundAssessmentVO();
            atmp.setWound_id(new Integer(wound_id));
            atmp.setActive(0);
            atmp.setProfessional_id(userVO.getId());
            WoundAssessmentVO tmpAssess = waManager.getAssessment(atmp);
            if (tmpAssess != null) {
                assessment_id = tmpAssess.getId() + "";
            }

            // User selected Populate previous
            if (request.getParameter("pop") != null) {
                //ostomy has no dimension data, it should not be a full assessemnt
                AssessmentOstomyVO assess = new AssessmentOstomyVO();
                assess.setAlpha_id(new Integer(alpha_id));
                assess.setWound_id(new Integer(wound_id));

                AssessmentOstomyVO fullVO = null;
                if (request.getParameter("full") != null && ((String) request.getParameter("full")).equals("1")) {
                    fullVO = (AssessmentOstomyVO) manager.retrieveLastActiveAssessment(assess, Constants.OSTOMY_PROFILE_TYPE, 1);
                    
                }
                    vo = (AssessmentOstomyVO) manager.retrieveLastActiveAssessment(assess, Constants.OSTOMY_PROFILE_TYPE, -1);
                    vo.setFull_assessment(0);
                //if (vo != null && request.getParameter("full") != null) {
                    //vo.setFull_assessment(new Integer((String) request.getParameter("full")));
                //}
                if (vo!=null && request.getParameter("full") != null && ((String) request.getParameter("full")).equals("0") && vo != null) {
                    vo.setFull_assessment(new Integer((String) request.getParameter("full")));
                    vo.setCircumference_mm(null);
                    vo.setLength_mm(null);
                    vo.setWidth_mm(null);

                }else if(request.getParameter("full") != null && ((String) request.getParameter("full")).equals("1") && vo!=null && fullVO!=null){
                    vo.setFull_assessment(new Integer((String) request.getParameter("full")));
                   vo.setCircumference_mm(fullVO.getCircumference_mm());
                    vo.setLength_mm(fullVO.getLength_mm());
                    vo.setWidth_mm(fullVO.getWidth_mm());
                }
            } else {

                try {
                    AssessmentOstomyVO ostomy = new AssessmentOstomyVO();
                    if (assessment_id != null) {
                        ostomy.setAlpha_id(new Integer(alpha_id));
                        ostomy.setAssessment_id(new Integer(assessment_id));
                        vo = (AssessmentOstomyVO) manager.getAssessment(ostomy);
                    }
                } catch (ApplicationException e) {
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }

            }

            if (vo != null) {
                
                
                // set assessment object to form
                request.setAttribute("assessment", vo);
                request.setAttribute("nutritional_status_other", Common.convertCarriageReturns(vo.getNutritional_status_other(), true));

                request.setAttribute("training_comments", Common.convertCarriageReturns(vo.getTraining_comments(), true));
                request.setAttribute("selfcare_comments", Common.convertCarriageReturns(vo.getSelf_care_progress_comments(), true));
                request.setAttribute("pouch_comments", Common.convertCarriageReturns(vo.getFlange_pouch_comments(), true));
                request.setAttribute("drainage_comments", Common.convertCarriageReturns(vo.getDrainage_comments(), true));
                request.setAttribute("margin_comments", Common.convertCarriageReturns(vo.getMucocutaneous_margin_comments(), true));
                request.setAttribute("mucocutaneous_margin_array", vo.getSeperations());
                request.setAttribute("unserializedDrainage", Serialize.arrayIze(vo.getDrainage()));
                request.setAttribute("unserializedProfile", Serialize.arrayIze(vo.getProfile()));
                request.setAttribute("unserializedUrineColour", Serialize.arrayIze(vo.getUrine_colour()));
                request.setAttribute("unserializedSelfCareProgress", Serialize.arrayIze(vo.getSelf_care_progress()));
                request.setAttribute("unserializedUrineType", Serialize.arrayIze(vo.getUrine_type()));
                request.setAttribute("unserializedPouching", Serialize.arrayIze(vo.getPouching()));
                request.setAttribute("unserializedAppearance", Serialize.arrayIze(vo.getAppearance()));
                request.setAttribute("unserializedContour", Serialize.arrayIze(vo.getContour()));
                request.setAttribute("unserializedDrainage", Serialize.arrayIze(vo.getDrainage()));
                request.setAttribute("unserializedMucocutaneousMargin", Serialize.arrayIze(vo.getMucocutaneous_margin()));
                request.setAttribute("unserializedPerifistulaSkin", Serialize.arrayIze(vo.getPerifistula_skin()));
                request.setAttribute("unserializedStoolColour", Serialize.arrayIze(vo.getStool_colour()));
                request.setAttribute("unserializedStoolConsistency", Serialize.arrayIze(vo.getStool_consistency()));
                request.setAttribute("unserializedPeriostomySkin", Serialize.arrayIze(vo.getPeriostomy_skin()));

            }

            if (assessment_id != null) {
                AssessmentImagesVO imgt = new AssessmentImagesVO();
                imgt.setAssessment_id(new Integer(assessment_id));
                imgt.setAlpha_id(new Integer(alpha_id));
                imgt.setWhichImage(Constants.IMAGE_1);
                AssessmentImagesVO image = imanager.getImage(imgt);
                imgt.setWhichImage(Constants.IMAGE_2);
                AssessmentImagesVO extraImage = imanager.getImage(imgt);
                imgt.setWhichImage(Constants.IMAGE_3);
                AssessmentImagesVO extraImage2 = imanager.getImage(imgt);

                if (image != null && image.getImages() != null) {
                    request.setAttribute("image_array", image.getImages());
                }

                if (extraImage != null && extraImage.getImages() != null) {
                    request.setAttribute("image_array2", extraImage.getImages());
                }
                if (extraImage2 != null && extraImage2.getImages() != null) {
                    request.setAttribute("image_array3", extraImage2.getImages());
                }
            }
            request.setAttribute("isRequired", "1");

            log.info("##$$##$$##-----GRABBING THE USER SIGNATURE-----##$$##$$##");
            AssessmentOstomyVO assess = new AssessmentOstomyVO();
            assess.setAlpha_id(new Integer(alpha_id));
            assess.setWound_id(new Integer(wound_id));
            assess.setActive(1);
            AssessmentOstomyVO voUpdate = (AssessmentOstomyVO) manager.getAssessment(assess);

            if (voUpdate != null) {
                AssessmentCommentsServiceImpl cservice = new AssessmentCommentsServiceImpl();
                //Find outstanding recommendations.
                ReferralsTrackingServiceImpl rservice = new ReferralsTrackingServiceImpl(language);

                if (((String) request.getParameter("action")) != null && ((String) request.getParameter("action")).equals("acknowledge")) {
                    ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                    rtmp.setWound_profile_type_id(voUpdate.getWound_profile_type_id());
                    rtmp.setEntry_type(Constants.RECOMMENDATION);
                    rtmp.setCurrent(Constants.ACTIVE);
                    Collection<ReferralsTrackingVO> referrals = rservice.getAllReferralsByCriteria(rtmp);
                    for (ReferralsTrackingVO referral : referrals) {
                        if (referral != null) {
                            referral.setCurrent(new Integer(0));
                            rservice.saveReferralsTracking(referral);

                            log.info("PixalereSearch.perform: Inserting new referral (ackknowledgement)");
                            referral.setCurrent(new Integer("1"));
                            referral.setEntry_type(Constants.ACKNOWLEDGE_RECOMMENDATION); // "Acknowledge Recommendation"
                            referral.setCreated_on(new Date());
                            referral.setProfessional_id(userVO.getId());
                            referral.setId(null);
                            rservice.saveReferralsTracking(referral);



                        }
                    }
                }
                ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                rtmp.setWound_profile_type_id(voUpdate.getWound_profile_type_id());
                rtmp.setEntry_type(Constants.RECOMMENDATION);
                rtmp.setCurrent(Constants.ACTIVE);
                Collection<ReferralsTrackingVO> referrals = rservice.getAllReferralsByCriteria(rtmp);
                Vector comments = new Vector();
                for (ReferralsTrackingVO referral : referrals) {
                    //get recommendations/comments
                    AssessmentCommentsVO ctmp = new AssessmentCommentsVO();
                    if(!referral.getAssessment_id().equals(-1)){
                    ctmp.setAssessment_id(referral.getAssessment_id());
                    }else{
                        ctmp.setReferral_id(referral.getId());
                    }
                    Collection<AssessmentCommentsVO> comments2 = cservice.getAllComments(ctmp);
                    for (AssessmentCommentsVO comment : comments2) {
                        comments.add(comment);
                    }
                }
                request.setAttribute("comments", comments);
                log.info("##$$##$$##-----THERE HAS BEEN A PREVIOUS ASSESSMENT WITH A SIGNATURE-----##$$##$$##");

                WoundAssessmentVO assessVO = voUpdate.getWoundAssessment();

                if (assessVO != null) {
                    AssessmentOstomyVO ostTMP = new AssessmentOstomyVO();
                    ostTMP.setAssessment_id(assessVO.getId());
                    ostTMP.setAlpha_id(new Integer(alpha_id));

                    if (voUpdate != null) {
                        request.setAttribute("ostomyStatus", voUpdate.getStatus());
                    }
                    request.setAttribute("user_signature", assessVO.getUser_signature());
                    request.setAttribute("profName", (Common.getProfessionalsName((String) request.getAttribute("user_signature"),locale)));
                    request.setAttribute("lastassess_date",assessVO.getVisited_on());
                } else {
                    request.setAttribute("user_signature", Common.getLocalizedString("pixalere.na",locale));
                }
            } else {
                log.info("##$$##$$##-----NO PREVIOUS ASSESSMENT, SIGNATURE IS N/A-----##$$##$$##");

                request.setAttribute("user_signature", Common.getLocalizedString("pixalere.na",locale));
            }

            // Populate Lists
            log.info("##$$##$$##-----INITIALIZE ALL PAGE CONTROLS-----##$$##$$##");
            ListServiceImpl listBD = new ListServiceImpl(language);

            request.setAttribute("periostomy_skin", listBD.getLists(LookupVO.PERIOSTOMY_SKIN));
            request.setAttribute(
                    "self_care_progress", listBD.getLists(LookupVO.SELF_CARE_PROGRESS));
            request.setAttribute(
                    "devices", listBD.getLists(LookupVO.DEVICES));
            request.setAttribute(
                    "profile", listBD.getLists(LookupVO.PROFILE));
            request.setAttribute(
                    "nursing_visit_frequency", listBD.getLists(LookupVO.NURSING_VISIT_FREQUENCY));
            request.setAttribute(
                    "priority", listBD.getLists(LookupVO.PRIORITY));
            request.setAttribute(
                    "wound_status", listBD.getLists(LookupVO.OSTOMY_STATUS));
            request.setAttribute(
                    "discharge_reason", listBD.getLists(LookupVO.OSTOMY_DISCHARGE_REASON));
            request.setAttribute(
                    "urine_colour", listBD.getLists(LookupVO.URINE_COLOUR));
            request.setAttribute(
                    "urine_type", listBD.getLists(LookupVO.URINE_TYPE));
            request.setAttribute(
                    "urine_quantity", listBD.getLists(LookupVO.URINE_QUANTITY));
            request.setAttribute(
                    "stool_colour", listBD.getLists(LookupVO.STOOL_COLOUR));
            request.setAttribute(
                    "stool_consistency", listBD.getLists(LookupVO.STOOL_CONSISTENCY));
            request.setAttribute(
                    "stool_quantity", listBD.getLists(LookupVO.STOOL_QUANTITY));
            request.setAttribute(
                    "flange_pouch", listBD.getLists(LookupVO.FLANGE_POUCH));
            request.setAttribute(
                    "shape", listBD.getLists(LookupVO.STOMA_SHAPE));
            request.setAttribute(
                    "appearance", listBD.getLists(LookupVO.SKIN_APPEARANCE));
            request.setAttribute(
                    "contour", listBD.getLists(LookupVO.SKIN_CONTOUR));
            request.setAttribute(
                    "pouching", listBD.getLists(LookupVO.POUCHING));
            request.setAttribute(
                    "drainage", listBD.getLists(LookupVO.OSTOMY_DRAINAGE));
            request.setAttribute(
                    "construction", listBD.getLists(LookupVO.CONSTRUCTION));
            request.setAttribute(
                    "mucocutaneous_margin", listBD.getLists(LookupVO.MUCOCUTANEOUS_MARGIN));
            request.setAttribute(
                    "nutritional_status", listBD.getLists(LookupVO.NUTRITIONAL_STATUS));

            request.setAttribute(
                    "perifistula_skin", listBD.getLists(LookupVO.PERI_FISTULA_SKIN));

            log.info(
                    "##$$##$$##-----END ASSESSMENTOSTOMYSETUPACTION-----##$$##$$##");

            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
            tloco.setWound_id(
                    new Integer((String) wound_id));
            tloco.setDischarge(
                    0); // Only open alphas
            Vector<WoundAssessmentLocationVO> allalphas = walservice.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, true);//4th parameter (extra sort) is ignored
            //TreeMap alphaMap = new TreeMap();
            //for (WoundAssessmentLocationVO v : allalphas) {
            //    alphaMap.put(v.getAlpha(), v);
            //}
            request.setAttribute( "allalphas", allalphas);









            if (assessment_id != null) {

                //list of unfinished alphas ("A", "B", "C",...)
                AssessmentOstomyVO t = new AssessmentOstomyVO();
                t.setWound_id(new Integer(wound_id));
                t.setAssessment_id(new Integer(assessment_id));
                List<String> unfinishedAlphas = walManager.computeUncompletedAlphas(t, userVO.getId());
                StringBuilder unfinishedAlphasString = new StringBuilder();
                List<WoundAssessmentLocationVO> completedAssess = new java.util.ArrayList();
                for(WoundAssessmentLocationVO a : allalphas){completedAssess.add(a);}
                //get completed assessements for checkmark
                for(int i = completedAssess.size()-1;i>=0;i--){
                    WoundAssessmentLocationVO v = completedAssess.get(i);
                    for (String unfinishedAlpha : unfinishedAlphas) {
                        if(v.getAlpha().equals(unfinishedAlpha)){
                            completedAssess.remove(i);//remove alpha from completed list, as its unfinished
                        }
                    }
                }
                request.setAttribute("completed_assess",completedAssess);
                if (unfinishedAlphas != null) {
                    for (String unfinishedAlpha : unfinishedAlphas) {
                        unfinishedAlphasString.append('\"');
                        unfinishedAlphasString.append(unfinishedAlpha);
                        unfinishedAlphasString.append('\"');
                        unfinishedAlphasString.append(',');
                    }
                }

                if (unfinishedAlphas != null && unfinishedAlphas.size() > 0) {
                    request.setAttribute("unfinishedAlphas", unfinishedAlphasString.substring(0, unfinishedAlphasString.length() - 1));
                } else {
                    request.setAttribute("unfinishedAlphas", "");
                }

            } else {
                //a null assessment id means that no assessments have been completed
                Vector<WoundAssessmentLocationVO> allAlphas = walManager.retrieveLocationsForWound(userVO.getId(), Integer.parseInt(wound_id), false, false);

                StringBuilder unfinishedAlphasString = new StringBuilder();

                for (WoundAssessmentLocationVO loc : allAlphas) {
                    unfinishedAlphasString.append('\"');
                    unfinishedAlphasString.append(loc.getAlpha());
                    unfinishedAlphasString.append('\"');
                    unfinishedAlphasString.append(',');

                    if (allAlphas.size() > 0) {
                        //cut off the trailing comma
                        request.setAttribute("unfinishedAlphas", unfinishedAlphasString.substring(0, unfinishedAlphasString.length() - 1));
                    } else {
                        request.setAttribute("unfinishedAlphas", "");
                    }
                }
            }
            //get bluemodel image
            if (wound_id
                    != null && wound_id.length() > 0) {
                WoundServiceImpl woundBD = new WoundServiceImpl(language);
                WoundProfileVO woundVO2 = new WoundProfileVO();
                woundVO2.setWound_id(new Integer(wound_id));
                woundVO2.setActive(new Integer(0));
                woundVO2.setProfessional_id(userVO.getId());
                WoundProfileVO woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                if (woundVO == null) {
                    woundVO2 = new WoundProfileVO();
                    woundVO2.setWound_id(new Integer(wound_id));
                    woundVO2.setCurrent_flag(new Integer(1));
                    woundVO = (WoundProfileVO) woundBD.getWoundProfile(woundVO2);
                }
                request.setAttribute("wound_profile", woundVO.getWound());
                Collection resultsWP = new ArrayList();
                List<FieldValues> comparedwpdata = null;
                if (woundVO != null) {
                    resultsWP.add(woundVO);
                    RowData[] wpRecord = woundBD.getAllWoundProfilesForFlowchart(resultsWP, userVO, false,false);
                    comparedwpdata = Common.compareGlobal(wpRecord);//Getting flowchart data.
                }
                request.setAttribute("wound_profile_readonly",reportService.organizeSummary(comparedwpdata, new ArrayList<SummaryVO>(), Common.getLocalizedString("pixalere.alert.woundprofile", locale)));
            

                Date today = new Date();
                if (woundVO != null && woundVO.getDate_surgery() != null && !woundVO.getDate_surgery().equals("")) {
                    Date surgery_date = woundVO.getDate_surgery();
                    long days = PDate.getDaysBetweenDates(today, surgery_date);
                    if(days>0){
                        request.setAttribute("postop_days", days);
                    }
                }
            }
            WoundAssessmentLocationVO a = new WoundAssessmentLocationVO();

            a.setId(new Integer(alpha_id));
            WoundAssessmentLocationVO alphaLoco = walManager.getAlpha(a);



            if (alphaLoco.getAlpha().indexOf("ostu") != -1) {
                request.setAttribute("elimination", "urine");
            } else if (alphaLoco.getAlpha().indexOf("ostf") != -1) {
                request.setAttribute("elimination", "stool");
            }
            if (assessment_id != null) {
                request.setAttribute("assessment_id", assessment_id);
            } else {
                //create one
                WoundAssessmentVO assess2 = new WoundAssessmentVO();
                assess2.setPatient_id(new Integer(patient_id));
                assess2.setWound_id(new Integer(wound_id));
                assess2.setProfessional_id(new Integer(userVO.getId() + ""));
                assess2.setCreated_on(new Date());
                assess2.setActive(new Integer(0));
                assess2.setOffline_flag(Common.isOffline() == true ? 1 : 0);

                String user_signature = pdate.getProfessionalSignature(new Date(), userVO,locale);
                assess2.setUser_signature(user_signature);
                PatientAccountVO patientAcc = (PatientAccountVO) session.getAttribute("patientAccount");
                assess2.setTreatment_location_id(patientAcc.getTreatment_location_id());

                waManager.saveAssessment(assess2);
                WoundAssessmentVO wat = new WoundAssessmentVO();
                wat.setWound_id(new Integer(wound_id));
                wat.setProfessional_id(userVO.getId());
                WoundAssessmentVO assessmentObject = waManager.getAssessment(wat);
                if (assessmentObject != null) {
                    request.setAttribute("assessment_id", assessmentObject.getId());
                }
            }
        } catch (ApplicationException e) {

            log.error("An application exception has been raised in AssessmentEachwoundSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(
                    request, errors);


            return (mapping.findForward("pixalere.error"));


        }
        String anchor = (String) request.getAttribute("anchor");
        request.setAttribute("anchor", anchor);


        if (anchor != null && anchor.equals("anchor_margin")) {

            return (mapping.findForward("uploader.assessment.margin"));


        } else {

            return (mapping.findForward("uploader.assessment.success"));

        }
    }
}
