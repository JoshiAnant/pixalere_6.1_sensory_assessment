/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.assessment;

import com.pixalere.wound.bean.WoundProfileTypeVO;
import java.util.Collection;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.assessment.service.*;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.utils.Common;
import java.util.Hashtable;
import java.util.Vector;

/**
 *
 * @author travis
 */
public class PatientUploadSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientUploadSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        log.info("=-=-=-=-=-=-=-=-=-=-=-- PatientUploadSetupAction - Starting =-=-=-=-=-=-=-=-=-=-=-- ");
        HttpSession session = request.getSession();
        ProfessionalVO user = (ProfessionalVO) session.getAttribute("userVO");

        Integer language = Common.getLanguageIdFromSession(session);

        if (user == null) {//session is invalid.. return to login
            request.setAttribute("session_invalid", "yes");
            return (mapping.findForward("system.logoff"));
        }
        try {
            saveToken(request);
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            WoundAssessmentServiceImpl manager = new WoundAssessmentServiceImpl();
            WoundServiceImpl wservice = new WoundServiceImpl(language);

            PatientServiceImpl patientService = new PatientServiceImpl(language);
            AssessmentImagesServiceImpl iservice = new AssessmentImagesServiceImpl();
            AssignPatientsServiceImpl assignService = new AssignPatientsServiceImpl();
            AssignPatientsVO assignTMP = new AssignPatientsVO();
            AssessmentCommentsServiceImpl cmanager = new AssessmentCommentsServiceImpl();
            assignTMP.setProfessionalId(user.getId());
            
            AssignPatientsVO asspatient = assignService.getAssignedPatient(assignTMP);
            if (asspatient != null) {
                PatientAccountVO patient = patientService.getPatient(asspatient.getPatientId());
                session.setAttribute("patientAccount", patient);
                Vector woundProfilesDrop = wservice.getWoundProfilesDropdown(asspatient.getPatientId(), false, user.getId().intValue());
                
                 request.setAttribute("types",woundProfilesDrop);
                //Get Alphas/Profile types.
                WoundProfileTypeVO t = new WoundProfileTypeVO();
                t.setPatient_id(patient.getPatient_id());
                //t.setActive(1);
                t.setClosed(0);
                Collection<WoundProfileTypeVO> wounds = wservice.getWoundProfileTypes(t, -1);
                
                Hashtable assessh = new Hashtable();
                Hashtable hash_images = new Hashtable();
                for (WoundProfileTypeVO alpha : wounds) {
                    WoundAssessmentVO wat = new WoundAssessmentVO();
                    wat.setWound_id(alpha.getWound_id());
                    wat.setProfessional_id(user.getId());
                    wat.setActive(1);
                    Collection<WoundAssessmentVO> assess = manager.getAllAssessments(wat);

                    AbstractAssessmentVO vo = new AbstractAssessmentVO();
                    vo.setPatient_id(alpha.getPatient_id());
                    vo.setWound_profile_type_id(alpha.getId());
                    Vector assessments = cmanager.getAllComments(assess, vo, (String) session.getAttribute("professional_id"), "", "",Common.getLanguageLocale(language));
                    //request.setAttribute("comments", assessments);
                    for(WoundAssessmentVO w : assess){
                        AssessmentImagesVO img = new AssessmentImagesVO();
                        img.setWound_profile_type_id(alpha.getId());
                        img.setAssessment_id(w.getId());
                        Collection<AssessmentImagesVO> images = iservice.getAllImages(img);
                        
                        if(images!=null && images.size()>0){
                            hash_images.put("img"+w.getId(),images);
                        }
                        assessh.put(alpha.getId(),assessments);
                    }
                }
                request.setAttribute("images",hash_images);
                request.setAttribute("comments",assessh);
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientUploadSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        return (mapping.findForward("assess.patientupload.success"));

    }
}
