package com.pixalere.struts.assessment;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.Constants;
public class AssessmentBurnForm extends ActionForm {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentEachwoundForm.class);
    private int phone_visit;
    private int burn_closure_reason;

    private String burn_areas;

    private int wound_day;
    private int wound_month;
    private int wound_year;

    private String image_array3;
    private FormFile image_3;
    public FormFile getImage_3() {
        return image_3;
    }

    public void setImage_3(FormFile image_3) {
        this.image_3 = image_3;
    }
    private Integer pain;
    private String pain_comments;
    private String[] burn_wound;
    private String[] exudate_colour;
    private String[] exudate_amount;
    private String[] grafts;
    private String[] donors;
    //images
    private String image_array;
    private String image_array2;
    
    private FormFile image_1;
    private FormFile image_2;
    private int full_assessment;
    //Closed Date
    private int burn_status;
    private int closed_day;
    private int closed_month;
    private int closed_year;
    private int discharge_reason;

    private int status;


    private HttpSession session = null;
    

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        setSession(request.getSession());
        
        ActionErrors errors = new ActionErrors();

        if (image_1 != null && image_1.getFileName().length() > 0 && image_1.getFileSize() == 0) {
            errors.add("file1", new ActionError("pixalere.woundassessment.error.filemissing1"));
        }

        if (image_2 != null && image_2.getFileName().length() > 0 && image_2.getFileSize() == 0) {
            errors.add("file2", new ActionError("pixalere.woundassessment.error.filemissing2"));
        }
        if (image_3 != null && image_3.getFileName().length() > 0 && image_3.getFileSize() == 0) {
            errors.add("file3", new ActionError("pixalere.woundassessment.error.filemissing2"));
        }
        return errors;

    }

    public AssessmentBurnVO getFormData(ProfessionalVO userVO) throws ApplicationException {
        AssessmentBurnVO assessmentBurnVO = new AssessmentBurnVO();
        if (full_assessment == -1) {
            full_assessment = 1;
        }
        PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
        assessmentBurnVO.setActive(0);
        assessmentBurnVO.setDeleted(0);
        assessmentBurnVO.setFull_assessment(getFull_assessment());
        assessmentBurnVO.setStatus(new Integer(getStatus() + ""));
        assessmentBurnVO.setDischarge_reason(new Integer(getDischarge_reason()));
        assessmentBurnVO.setClosed_date(pdate.getDate(getClosed_month(), getClosed_day(), getClosed_year()));
        try{
            if(assessmentBurnVO.getClosed_date()==null && assessmentBurnVO.getStatus().equals(Integer.parseInt(Common.getConfig("burnClosed")))){
               assessmentBurnVO.setClosed_date(new Date());
            }
        }catch(NullPointerException e){
            
        }catch(NumberFormatException e){
            
        }
        assessmentBurnVO.setPatient_id(new Integer((String) getSession().getAttribute("patient_id")));
    	assessmentBurnVO.setWound_id(new Integer((String) getSession().getAttribute("wound_id")));
        assessmentBurnVO.setBurn_status(getBurn_status());
        assessmentBurnVO.setWound_date(pdate.getDate(getWound_month(), getWound_day(), getWound_year()));

        assessmentBurnVO.setPain(new Integer(getPain()));
        assessmentBurnVO.setPain_comments(Common.convertCarriageReturns(pain_comments, false));
        
        assessmentBurnVO.setBurn_wound(Serialize.serialize(getBurn_wound()));
        assessmentBurnVO.setExudate_colour(Serialize.serialize(getExudate_colour()));
        assessmentBurnVO.setExudate_amount(Serialize.serialize(getExudate_amount()));
        assessmentBurnVO.setGrafts(Serialize.serialize(getGrafts()));
        assessmentBurnVO.setDonors(Serialize.serialize(getDonors()));
        
        assessmentBurnVO.setCreated_on(new Date());
        
        //set for all alphas incase some do not goto treatment.

        assessmentBurnVO.setStatus(new Integer(getStatus() + ""));


        return assessmentBurnVO;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    

    public int getFull_assessment() {
        return full_assessment;
    }

    public void setFull_assessment(int full_assessment) {
        this.full_assessment = full_assessment;
    }
    public int getPhone_visit() {
        return phone_visit;
    }

    public void setPhone_visit(int phone_visit) {
        this.phone_visit = phone_visit;
    }


    public int getWound_day() {
        return wound_day;
    }

    public void setWound_day(int wound_day) {
        this.wound_day = wound_day;
    }

    public int getWound_month() {
        return wound_month;
    }

    public void setWound_month(int wound_month) {
        this.wound_month = wound_month;
    }

    public int getWound_year() {
        return wound_year;
    }

    public void setWound_year(int wound_year) {
        this.wound_year = wound_year;
    }

    public String getBurn_areas() {
        return burn_areas;
    }

    public void setBurn_areas(String burn_areas) {
        this.burn_areas = burn_areas;
    }

    public int getPain() {
        return pain;
    }

    public void setPain(int pain) {
        this.pain = pain;
    }

    public String getPain_comments() {
        return pain_comments;
    }

    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }

    public String[] getBurn_wound() {
        return this.burn_wound;
    }

    public void setBurn_wound(String[] burn_wound) {
        this.burn_wound = burn_wound;
    }

    public String[] getExudate_colour() {
        return this.exudate_colour;
    }

    public void setExudate_colour(String[] exudate_colour) {
        this.exudate_colour = exudate_colour;
    }

    public String[] getExudate_amount() {
        return this.exudate_amount;
    }

    public void setExudate_amount(String[] exudate_amount) {
        this.exudate_amount = exudate_amount;
    }

    public String[] getGrafts() {
        return this.grafts;
    }

    public void setGrafts(String[] grafts) {
        this.grafts = grafts;
    }

    public String[] getDonors() {
        return this.donors;
    }

    public void setDonors(String[] donors) {
        this.donors = donors;
    }

    public int getDischarge_reason() {
        return discharge_reason;
    }

    public void setDischarge_reason(int discharge_reason) {
        this.discharge_reason = discharge_reason;
    }

    public String getImage_array() {
        return image_array;
    }

    public void setImage_array(String image_array) {
        this.image_array = image_array;
    }

    public String getImage_array2() {
        return image_array2;
    }

    public void setImage_array2(String image_array2) {
        this.image_array2 = image_array2;
    }

    public FormFile getImage_1() {
        return image_1;
    }

    public void setImage_1(FormFile image_1) {
        this.image_1 = image_1;
    }

    public FormFile getImage_2() {
        return image_2;
    }

    public void setImage_2(FormFile image_2) {
        this.image_2 = image_2;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getBurn_status() {
        return burn_status;
    }

    public void setBurn_status(int burn_status) {
        this.burn_status = burn_status;
    }

    public int getClosed_day() {
        return closed_day;
    }

    public void setClosed_day(int closed_day) {
        this.closed_day = closed_day;
    }

    public int getClosed_month() {
        return closed_month;
    }

    public void setClosed_month(int closed_month) {
        this.closed_month = closed_month;
    }

    public int getClosed_year() {
        return closed_year;
    }

    public void setClosed_year(int closed_year) {
        this.closed_year = closed_year;
    }

    public int getBurn_closure_reason() {
        return burn_closure_reason;
    }

    public void setBurn_closure_reason(int burn_closure_reason) {
        this.burn_closure_reason = burn_closure_reason;
    }

    private String patient_id;
    private String wound_id;
    /**
     * @return the patient_id
     */
    public String getPatient_id() {
        return patient_id;
    }

    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    /**
     * @return the wound_id
     */
    public String getWound_id() {
        return wound_id;
    }

    /**
     * @param wound_id the wound_id to set
     */
    public void setWound_id(String wound_id) {
        this.wound_id = wound_id;
    }

    /**
     * @return the image_array3
     */
    public String getImage_array3() {
        return image_array3;
    }

    /**
     * @param image_array3 the image_array3 to set
     */
    public void setImage_array3(String image_array3) {
        this.image_array3 = image_array3;
    }

 
}
