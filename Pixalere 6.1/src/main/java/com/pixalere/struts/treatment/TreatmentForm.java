package com.pixalere.struts.treatment;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

public class TreatmentForm extends ActionForm {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TreatmentForm.class);
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    private Integer[] npwt_alphas1;
    private Integer[] npwt_alphas2;
    private Integer bath;
    private Integer nail_care;
    private String[] current_products;
    private String[] csresult;
    private String[] antibiotics;
    private String[] wound_ccac_questions;
    private int as_needed;
    private int cs_show;
    private int vendor_name1;
    private int machine_acquirement1;
    private int vendor_name2;
    private int machine_acquirement2;
    private int treatmentmodalities_show;
    private String skin_nursing_care_plan;
    private String skin_treatment_comments;
    private String wound_nursing_care_plan;
    private String wound_treatment_comments;
    private String ostomy_nursing_care_plan;
    private String ostomy_treatment_comments;
    private String incision_nursing_care_plan;
    private String incision_treatment_comments;
    private String drain_nursing_care_plan;
    private String drain_treatment_comments;
    private String burn_nursing_care_plan;
    private String burn_treatment_comments;
    private String[] dressing_change_frequency;
    private String[] laser;
    private String[] massage;
    private String[] hbot;
    private String[] ultrasound;
    private String[] estim;
    private int laser_show;
    private int estim_show;
    private int ultrasound_show;
    private int hbot_show;
    private int massage_show;
    private int visit_freq;
    private int vac_show;
    private int vacstart2_day;
    private int vacstart2_month;
    private int vacstart2_year;
    private int vacend2_day;
    private int vacend2_month;
    private int vacend2_year;
    private int initiated_by2;
    private int reason_for_ending2;
    private int goal_of_therapy2;
    private int np_connector2;
    private String kci_num2;
    private String serial_num2;
    private int pressure_reading2;
    private int reading2;
    private int therapy_setting1;
    private int therapy_setting2;
    
    private int vacstart1_day;
    private int vacstart1_month;
    private int vacstart1_year;
    private int vacend1_day;
    private int vacend1_month;
    private int vacend1_year;
    private int initiated_by1;
    private int reason_for_ending1;
    private int goal_of_therapy1;
    private int np_connector1;
    private String kci_num1;
    private String serial_num1;
    private int pressure_reading1;
    private int reading1;
    private int reading_group1;
    private Integer[] adjunctive;
    private String adjunctive_other;
    private int cs_done;
    private int pop_treatment;
    private int review_done;
    public void setCurrent_products(String[] current_products){
        this.current_products = current_products;
    }
    
    public String[] getCurrent_products() {
        return current_products;
    }

    public int getVac_show() {
        return vac_show;
    }
    
    public void setVac_show(int vac_show) {
        this.vac_show = vac_show;
    }
    
    public Integer[] getAdjunctive() {
        return adjunctive;
    }
    
    public void setAdjunctive(Integer[] adjunctive) {
        this.adjunctive = adjunctive;
    }
    /**
     * @return the as_needed
     */
    public Integer getAs_needed() {
        return as_needed;
    }

    /**
     * @param as_needed the as_needed to set
     */
    public void setAs_needed(Integer as_needed) {
        this.as_needed = as_needed;
    }

    /**
     * @return the wound_ccac_questions
     */
    public String[] getWound_ccac_questions() {
        return wound_ccac_questions;
    }

    /**
     * @param wound_ccac_questions the wound_ccac_questions to set
     */
    public void setWound_ccac_questions(String[] wound_ccac_questions) {
        this.wound_ccac_questions = wound_ccac_questions;
    }
    public String getAdjunctive_other() {
        return adjunctive_other;
    }
    
    public void setAdjunctive_other(String adjunctive_other) {
        this.adjunctive_other = adjunctive_other;
    }
    
    public String[] getDressing_change_frequency() {
        return dressing_change_frequency;
    }
    
    public void setDressing_change_frequency(String[] dressing_change_frequency) {
        this.dressing_change_frequency = dressing_change_frequency;
    }
    
    public int getVisit_freq() {
        return visit_freq;
    }
    
    public void setVisit_freq(int visit_freq) {
        this.visit_freq = visit_freq;
    }
    
    public String[] getLaser() {
        return laser;
    }
    
    public void setLaser(String[] laser) {
        this.laser = laser;
    }
    
    public String[] getMassage() {
        return massage;
    }
    
    public void setMassage(String[] massage) {
        this.massage = massage;
    }
    
    public String[] getHbot() {
        return hbot;
    }
    
    public void setHbot(String[] hbot) {
        this.hbot = hbot;
    }
    
    public String[] getUltrasound() {
        return ultrasound;
    }
    
    public void setUltrasound(String[] ultrasound) {
        this.ultrasound = ultrasound;
    }
    
    public String[] getEstim() {
        return estim;
    }
    
    public void setEstim(String[] estim) {
        this.estim = estim;
    }
    
    public int getLaser_show() {
        return laser_show;
    }
    
    public void setLaser_show(int laser_show) {
        this.laser_show = laser_show;
    }
    
    public int getEstim_show() {
        return estim_show;
    }
    
    public void setEstim_show(int estim_show) {
        this.estim_show = estim_show;
    }
    
    public int getUltrasound_show() {
        return ultrasound_show;
    }
    
    public void setUltrasound_show(int ultrasound_show) {
        this.ultrasound_show = ultrasound_show;
    }
    
    public int getHbot_show() {
        return hbot_show;
    }
    
    public void setHbot_show(int hbot_show) {
        this.hbot_show = hbot_show;
    }
    
    public int getMassage_show() {
        return massage_show;
    }
    
    public void setMassage_show(int massage_show) {
        this.massage_show = massage_show;
    }
    
    public String[] getCsresult() {
        return csresult;
    }
    
    public void setCsresult(String[] csresult) {
        this.csresult = csresult;
    }
    
    public int getCs_show() {
        return cs_show;
    }
    
    public void setCs_show(int cs_show) {
        this.cs_show = cs_show;
    }
    
    public int getTreatmentmodalities_show() {
        return treatmentmodalities_show;
    }
    
    public void setTreatmentmodalities_show(int treatmentmodalities_show) {
        this.treatmentmodalities_show = treatmentmodalities_show;
    }
    
    public String[] getAntibiotics() {
        return antibiotics;
    }
    
    public void setAntibiotics(String[] antibiotics) {
        this.antibiotics = antibiotics;
    }
    
    public String getWound_nursing_care_plan() {
        return wound_nursing_care_plan;
    }
    
    public void setWound_nursing_care_plan(String wound_nursing_care_plan) {
        this.wound_nursing_care_plan = wound_nursing_care_plan;
    }
    
    public String getWound_treatment_comments() {
        return wound_treatment_comments;
    }
    
    public void setWound_treatment_comments(String wound_treatment_comments) {
        this.wound_treatment_comments = wound_treatment_comments;
    }
    
    public String getOstomy_nursing_care_plan() {
        return ostomy_nursing_care_plan;
    }
    
    public void setOstomy_nursing_care_plan(String ostomy_nursing_care_plan) {
        this.ostomy_nursing_care_plan = ostomy_nursing_care_plan;
    }
    
    public String getOstomy_treatment_comments() {
        return ostomy_treatment_comments;
    }
    
    public void setOstomy_treatment_comments(String ostomy_treatment_comments) {
        this.ostomy_treatment_comments = ostomy_treatment_comments;
    }
    
    public String getIncision_nursing_care_plan() {
        return incision_nursing_care_plan;
    }
    
    public void setIncision_nursing_care_plan(String incision_nursing_care_plan) {
        this.incision_nursing_care_plan = incision_nursing_care_plan;
    }
    
    public String getIncision_treatment_comments() {
        return incision_treatment_comments;
    }
    
    public void setIncision_treatment_comments(String incision_treatment_comments) {
        this.incision_treatment_comments = incision_treatment_comments;
    }
    
    public String getDrain_nursing_care_plan() {
        return drain_nursing_care_plan;
    }
    
    public void setDrain_nursing_care_plan(String drain_nursing_care_plan) {
        this.drain_nursing_care_plan = drain_nursing_care_plan;
    }
    
    public String getDrain_treatment_comments() {
        return drain_treatment_comments;
    }
    
    public void setDrain_treatment_comments(String drain_treatment_comments) {
        this.drain_treatment_comments = drain_treatment_comments;
    }

    public String getBurn_nursing_care_plan() {
        return burn_nursing_care_plan;
    }

    public void setBurn_nursing_care_plan(String burn_nursing_care_plan) {
        this.burn_nursing_care_plan = burn_nursing_care_plan;
    }

    public String getBurn_treatment_comments() {
        return burn_treatment_comments;
    }

    public void setBurn_treatment_comments(String burn_treatment_comments) {
        this.burn_treatment_comments = burn_treatment_comments;
    }

    public int getPop_treatment() {
        return pop_treatment;
    }

    public void setPop_treatment(int pop_treatment) {
        this.pop_treatment = pop_treatment;
    }
   
 

    /**
     * @return the review_done
     */
    public int getReview_done() {
        return review_done;
    }

    /**
     * @param review_done the review_done to set
     */
    public void setReview_done(int review_done) {
        this.review_done = review_done;
    }

    /**
     * @return the skin_nursing_care_plan
     */
  
    public String getSkin_nursing_care_plan() {
        return skin_nursing_care_plan;

    }

    /**

 
     * @param skin_nursing_care_plan the skin_nursing_care_plan to set

     */


    public void setSkin_nursing_care_plan(String skin_nursing_care_plan) {
        this.skin_nursing_care_plan = skin_nursing_care_plan;

    }


    /**
     * @return the skin_treatment_comments
     */
    public String getSkin_treatment_comments() {
        return skin_treatment_comments;
    }

    /**
     * @param skin_treatment_comments the skin_treatment_comments to set
     */
    public void setSkin_treatment_comments(String skin_treatment_comments) {
        this.skin_treatment_comments = skin_treatment_comments;
    }

   

    /**
     * @return the vacstart1_day
     */
    public int getVacstart1_day() {
        return vacstart1_day;
    }

    /**
     * @param vacstart1_day the vacstart1_day to set
     */
    public void setVacstart1_day(int vacstart1_day) {
        this.vacstart1_day = vacstart1_day;
    }

    /**
     * @return the vacstart1_month
     */
    public int getVacstart1_month() {
        return vacstart1_month;
    }

    /**
     * @param vacstart1_month the vacstart1_month to set
     */
    public void setVacstart1_month(int vacstart1_month) {
        this.vacstart1_month = vacstart1_month;
    }

    /**
     * @return the vacstart1_year
     */
    public int getVacstart1_year() {
        return vacstart1_year;
    }

    /**
     * @param vacstart1_year the vacstart1_year to set
     */
    public void setVacstart1_year(int vacstart1_year) {
        this.vacstart1_year = vacstart1_year;
    }

    /**
     * @return the vacend1_day
     */
    public int getVacend1_day() {
        return vacend1_day;
    }

    /**
     * @param vacend1_day the vacend1_day to set
     */
    public void setVacend1_day(int vacend1_day) {
        this.vacend1_day = vacend1_day;
    }

    /**
     * @return the vacend1_month
     */
    public int getVacend1_month() {
        return vacend1_month;
    }

    /**
     * @param vacend1_month the vacend1_month to set
     */
    public void setVacend1_month(int vacend1_month) {
        this.vacend1_month = vacend1_month;
    }

    /**
     * @return the vacend1_year
     */
    public int getVacend1_year() {
        return vacend1_year;
    }

    /**
     * @param vacend1_year the vacend1_year to set
     */
    public void setVacend1_year(int vacend1_year) {
        this.vacend1_year = vacend1_year;
    }

   

    /**
     * @return the pressure_reading1
     */
    public int getPressure_reading1() {
        return pressure_reading1;
    }

    /**
     * @param pressure_reading1 the pressure_reading1 to set
     */
    public void setPressure_reading1(int pressure_reading1) {
        this.pressure_reading1 = pressure_reading1;
    }

    /**
     * @return the reading1
     */
    public int getReading1() {
        return reading1;
    }

    /**
     * @param reading1 the reading1 to set
     */
    public void setReading1(int reading1) {
        this.reading1 = reading1;
    }

    /**
     * @return the reading_group1
     */
    public int getReading_group1() {
        return reading_group1;
    }

    /**
     * @param reading_group1 the reading_group1 to set
     */
    public void setReading_group1(int reading_group1) {
        this.reading_group1 = reading_group1;
    }

    /**
     * @return the vacstart2_day
     */
    public int getVacstart2_day() {
        return vacstart2_day;
    }

    /**
     * @param vacstart2_day the vacstart2_day to set
     */
    public void setVacstart2_day(int vacstart2_day) {
        this.vacstart2_day = vacstart2_day;
    }

    /**
     * @return the vacstart2_month
     */
    public int getVacstart2_month() {
        return vacstart2_month;
    }

    /**
     * @param vacstart2_month the vacstart2_month to set
     */
    public void setVacstart2_month(int vacstart2_month) {
        this.vacstart2_month = vacstart2_month;
    }

    /**
     * @return the vacstart2_year
     */
    public int getVacstart2_year() {
        return vacstart2_year;
    }

    /**
     * @param vacstart2_year the vacstart2_year to set
     */
    public void setVacstart2_year(int vacstart2_year) {
        this.vacstart2_year = vacstart2_year;
    }

    /**
     * @return the vacend2_day
     */
    public int getVacend2_day() {
        return vacend2_day;
    }

    /**
     * @param vacend2_day the vacend2_day to set
     */
    public void setVacend2_day(int vacend2_day) {
        this.vacend2_day = vacend2_day;
    }

    /**
     * @return the vacend2_month
     */
    public int getVacend2_month() {
        return vacend2_month;
    }

    /**
     * @param vacend2_month the vacend2_month to set
     */
    public void setVacend2_month(int vacend2_month) {
        this.vacend2_month = vacend2_month;
    }

    /**
     * @return the vacend2_year
     */
    public int getVacend2_year() {
        return vacend2_year;
    }

    /**
     * @param vacend2_year the vacend2_year to set
     */
    public void setVacend2_year(int vacend2_year) {
        this.vacend2_year = vacend2_year;
    }

    /**
     * @return the initiated_by2
     */
    public int getInitiated_by2() {
        return initiated_by2;
    }

    /**
     * @param initiated_by2 the initiated_by2 to set
     */
    public void setInitiated_by2(int initiated_by2) {
        this.initiated_by2 = initiated_by2;
    }

    /**
     * @return the reason_for_ending2
     */
    public int getReason_for_ending2() {
        return reason_for_ending2;
    }

    /**
     * @param reason_for_ending2 the reason_for_ending2 to set
     */
    public void setReason_for_ending2(int reason_for_ending2) {
        this.reason_for_ending2 = reason_for_ending2;
    }

    /**
     * @return the goal_of_therapy2
     */
    public int getGoal_of_therapy2() {
        return goal_of_therapy2;
    }

    /**
     * @param goal_of_therapy2 the goal_of_therapy2 to set
     */
    public void setGoal_of_therapy2(int goal_of_therapy2) {
        this.goal_of_therapy2 = goal_of_therapy2;
    }

    /**
     * @return the np_connector2
     */
    public int getNp_connector2() {
        return np_connector2;
    }

    /**
     * @param np_connector2 the np_connector2 to set
     */
    public void setNp_connector2(int np_connector2) {
        this.np_connector2 = np_connector2;
    }

    /**
     * @return the kci_num2
     */
    public String getKci_num2() {
        return kci_num2;
    }

    /**
     * @param kci_num2 the kci_num2 to set
     */
    public void setKci_num2(String kci_num2) {
        this.kci_num2 = kci_num2;
    }

    /**
     * @return the serial_num2
     */
    public String getSerial_num2() {
        return serial_num2;
    }

    /**
     * @param serial_num2 the serial_num2 to set
     */
    public void setSerial_num2(String serial_num2) {
        this.serial_num2 = serial_num2;
    }

    /**
     * @return the pressure_reading2
     */
    public int getPressure_reading2() {
        return pressure_reading2;
    }

    /**
     * @param pressure_reading2 the pressure_reading2 to set
     */
    public void setPressure_reading2(int pressure_reading2) {
        this.pressure_reading2 = pressure_reading2;
    }

    /**
     * @return the reading2
     */
    public int getReading2() {
        return reading2;
    }

    /**
     * @param reading2 the reading2 to set
     */
    public void setReading2(int reading2) {
        this.reading2 = reading2;
    }

 
    /**
     * @return the initiated_by1
     */
    public int getInitiated_by1() {
        return initiated_by1;
    }

    /**
     * @param initiated_by1 the initiated_by1 to set
     */
    public void setInitiated_by1(int initiated_by1) {
        this.initiated_by1 = initiated_by1;
    }

    /**
     * @return the reason_for_ending1
     */
    public int getReason_for_ending1() {
        return reason_for_ending1;
    }

    /**
     * @param reason_for_ending1 the reason_for_ending1 to set
     */
    public void setReason_for_ending1(int reason_for_ending1) {
        this.reason_for_ending1 = reason_for_ending1;
    }

    /**
     * @return the goal_of_therapy1
     */
    public int getGoal_of_therapy1() {
        return goal_of_therapy1;
    }

    /**
     * @param goal_of_therapy1 the goal_of_therapy1 to set
     */
    public void setGoal_of_therapy1(int goal_of_therapy1) {
        this.goal_of_therapy1 = goal_of_therapy1;
    }

    /**
     * @return the np_connector1
     */
    public int getNp_connector1() {
        return np_connector1;
    }

    /**
     * @param np_connector1 the np_connector1 to set
     */
    public void setNp_connector1(int np_connector1) {
        this.np_connector1 = np_connector1;
    }

    /**
     * @return the kci_num1
     */
    public String getKci_num1() {
        return kci_num1;
    }

    /**
     * @param kci_num1 the kci_num1 to set
     */
    public void setKci_num1(String kci_num1) {
        this.kci_num1 = kci_num1;
    }

    /**
     * @return the serial_num1
     */
    public String getSerial_num1() {
        return serial_num1;
    }

    /**
     * @param serial_num1 the serial_num1 to set
     */
    public void setSerial_num1(String serial_num1) {
        this.serial_num1 = serial_num1;
    }

    /**
     * @return the npwt_alphas1
     */
    public Integer[] getNpwt_alphas1() {
        return npwt_alphas1;
    }

    /**
     * @param npwt_alphas1 the npwt_alphas1 to set
     */
    public void setNpwt_alphas1(Integer[] npwt_alphas1) {
        this.npwt_alphas1 = npwt_alphas1;
    }

    /**
     * @return the npwt_alphas2
     */
    public Integer[] getNpwt_alphas2() {
        return npwt_alphas2;
    }

    /**
     * @param npwt_alphas2 the npwt_alphas2 to set
     */
    public void setNpwt_alphas2(Integer[] npwt_alphas2) {
        this.npwt_alphas2 = npwt_alphas2;
    }

    /**
     * @return the therapy_setting1
     */
    public int getTherapy_setting1() {
        return therapy_setting1;
    }

    /**
     * @param therapy_setting1 the therapy_setting1 to set
     */
    public void setTherapy_setting1(int therapy_setting1) {
        this.therapy_setting1 = therapy_setting1;
    }

    /**
     * @return the therapy_setting2
     */
    public int getTherapy_setting2() {
        return therapy_setting2;
    }

    /**
     * @param therapy_setting2 the therapy_setting2 to set
     */
    public void setTherapy_setting2(int therapy_setting2) {
        this.therapy_setting2 = therapy_setting2;
    }

    /**
     * @return the cs_done
     */
    public int getCs_done() {
        return cs_done;
    }

    /**
     * @param cs_done the cs_done to set
     */
    public void setCs_done(int cs_done) {
        this.cs_done = cs_done;
    }
    /**
     * @return the vendor_name
     */
    public Integer getVendor_name2() {
        return vendor_name2;
    }

    /**
     * @param vendor_name the vendor_name to set
     */
    public void setVendor_name2(Integer vendor_name2) {
        this.vendor_name2 = vendor_name2;
    }

    /**
     * @return the machine_acquirement
     */
    public Integer getMachine_acquirement2() {
        return machine_acquirement2;
    }

    /**
     * @param machine_acquirement the machine_acquirement to set
     */
    public void setMachine_acquirement2(Integer machine_acquirement2) {
        this.machine_acquirement2 = machine_acquirement2;
    }
    /**
     * @return the vendor_name
     */
    public Integer getVendor_name1() {
        return vendor_name1;
    }

    /**
     * @param vendor_name the vendor_name to set
     */
    public void setVendor_name1(Integer vendor_name1) {
        this.vendor_name1 = vendor_name1;
    }

    /**
     * @return the machine_acquirement
     */
    public Integer getMachine_acquirement1() {
        return machine_acquirement1;
    }

    /**
     * @param machine_acquirement the machine_acquirement to set
     */
    public void setMachine_acquirement1(Integer machine_acquirement1) {
        this.machine_acquirement1 = machine_acquirement1;
    }

    /**
     * @return the bath
     */
    public Integer getBath() {
        return bath;
    }

    /**
     * @param bath the bath to set
     */
    public void setBath(Integer bath) {
        this.bath = bath;
    }

    /**
     * @return the nail_care
     */
    public Integer getNail_care() {
        return nail_care;
    }

    /**
     * @param nail_care the nail_care to set
     */
    public void setNail_care(Integer nail_care) {
        this.nail_care = nail_care;
    }
}
