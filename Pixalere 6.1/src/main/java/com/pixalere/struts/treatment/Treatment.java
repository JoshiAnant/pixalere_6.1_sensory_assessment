package com.pixalere.struts.treatment;
import com.pixalere.common.bean.TriggerVO;
import com.pixalere.common.bean.TriggerProductsVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentNPWTServiceImpl;
import com.pixalere.assessment.bean.*;
import java.util.Date;
import java.util.Collection;
import java.text.SimpleDateFormat;
import com.pixalere.utils.*;
import java.util.Vector;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.StringTokenizer;
import java.util.List;
import java.util.LinkedList;
import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.common.service.TriggerServiceImpl;
public class Treatment extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Treatment.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        TreatmentForm treatmentForm = (TreatmentForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        AssessmentNPWTServiceImpl assessmentNPWTBD = new AssessmentNPWTServiceImpl();
        AssessmentServiceImpl assessmentBD = new AssessmentServiceImpl(language);
        NursingCarePlanServiceImpl ncpBD = new NursingCarePlanServiceImpl();
        WoundAssessmentServiceImpl waassessmentBD = new WoundAssessmentServiceImpl();
        TreatmentCommentsServiceImpl tBD = new TreatmentCommentsServiceImpl();
        WoundAssessmentLocationVO woundLocation = new WoundAssessmentLocationVO();
        LogAuditServiceImpl lservice = new LogAuditServiceImpl();
         TriggerServiceImpl tservice = new TriggerServiceImpl();
        if (session.getAttribute("patient_id") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        String patient_id = (String) session.getAttribute("patient_id");
        String wound_id = (String) session.getAttribute("wound_id");
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
        boolean tokenValid = isTokenValid(request);
        if (tokenValid) {
            resetToken(request);
            session.removeAttribute("treatment_completion_error");
            try {
                if (treatmentForm.getWound_nursing_care_plan() != null && treatmentForm.getWound_nursing_care_plan().equals("") && Common.getConfig("allowFullAssesmentDue").equals("1")) {
                    treatmentForm.setWound_nursing_care_plan("Full Assessment Due:");
                }


                if (treatmentForm.getOstomy_nursing_care_plan() != null && treatmentForm.getOstomy_nursing_care_plan().equals("") && Common.getConfig("allowFullAssesmentDue").equals("1")) {
                    treatmentForm.setOstomy_nursing_care_plan("Full Assessment Due:");
                }
                
                String assessment_id = null;
                WoundAssessmentVO atmp = new WoundAssessmentVO();
                atmp.setWound_id(new Integer(wound_id));
                atmp.setActive(0);
                atmp.setProfessional_id(userVO.getId());
                WoundAssessmentVO tmpAssess = waassessmentBD.getAssessment(atmp);
                if (tmpAssess != null) {
                    assessment_id = tmpAssess.getId() + "";
                }
                
                if (request.getParameter("assessment_select") != "" && assessment_id != null) {// && request.getParameter("page") == "products"){
                    String[] products = treatmentForm.getCurrent_products();
                    // update assessment_eachwound to clear any products that existed in session.
                    // products are set to nothing.
                    Collection<AbstractAssessmentVO> woundAssessments = assessmentBD.getAllAssessments(new Integer(assessment_id).intValue());
                    for (AbstractAssessmentVO updateAssessmentVO : woundAssessments) {

                        AssessmentProductVO productVO = new AssessmentProductVO();
                        //grab last ncp
                        NursingCarePlanVO oldNCP = new NursingCarePlanVO();
                        oldNCP.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                        oldNCP.setActive(new Integer(1));
                        oldNCP.setWound_id(new Integer(wound_id));
                        oldNCP = ncpBD.getNursingCarePlan(oldNCP);
                        Vector dcf = Common.parseSelectByAlpha(updateAssessmentVO.getAlphaLocation().getAlpha(), treatmentForm.getDressing_change_frequency(),locale);
                        int dcf_access = 0;
                        if (dcf != null && dcf.size() != 0) {


                            dcf_access = Integer.parseInt(dcf.get(0) + "");
                        }


                        DressingChangeFrequencyVO f = new DressingChangeFrequencyVO();
                        f.setActive(0);
                        f.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                        f.setAssessment_id(new Integer(assessment_id));
                        f.setPatient_id(new Integer(patient_id));
                        f.setWound_id(new Integer(wound_id));
                        f.setProfessional_id(userVO.getId());
                        f.setDcf(new Integer(dcf_access));
                        f.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        f.setCreated_on(new Date());
                        f.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                        DressingChangeFrequencyVO fvo = new DressingChangeFrequencyVO();
                        fvo.setAssessment_id(new Integer(assessment_id));
                        fvo.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                        fvo.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        fvo = ncpBD.getDressingChangeFrequency(fvo);
                        if (fvo != null) {
                            f.setId(fvo.getId());
                        }
                        if (dcf_access != 0) {
                            ncpBD.saveDressingChangeFrequency(f);
                        }
                        if (updateAssessmentVO instanceof AssessmentEachwoundVO) {
                            productVO.setWound_assessment_id(updateAssessmentVO.getId());

                            NursingCarePlanVO ncpvo = new NursingCarePlanVO();
                            ncpvo.setAssessment_id(new Integer(assessment_id));
                            ncpvo.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            ncpvo = ncpBD.getNursingCarePlan(ncpvo);
                            NursingCarePlanVO newncp = new NursingCarePlanVO();
                            newncp.setActive(0);
                            newncp.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newncp.setAssessment_id(new Integer(assessment_id));
                            newncp.setPatient_id(new Integer(patient_id));
                            newncp.setWound_id(new Integer(wound_id));
                            newncp.setProfessional_id(userVO.getId());
                            newncp.setVisit_frequency(new Integer(treatmentForm.getVisit_freq()));
                            newncp.setAs_needed(new Integer(treatmentForm.getAs_needed()));
                            newncp.setCreated_on(new Date());
                            newncp.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newncp.setBody(Common.convertCarriageReturns(treatmentForm.getWound_nursing_care_plan(), false));
                            if (ncpvo != null) {
                                newncp.setId(ncpvo.getId());
                            }
                            if (newncp != null && newncp.getBody() != null) {
                                if ((treatmentForm.getWound_nursing_care_plan() != null && !(treatmentForm.getWound_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP == null) || (treatmentForm.getWound_nursing_care_plan() != null && !(treatmentForm.getWound_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP != null && oldNCP.getBody() != null && newncp.getBody() != null && !oldNCP.getBody().equals(newncp.getBody()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                } else if ((treatmentForm.getVisit_freq() != 0 && oldNCP == null) || (treatmentForm.getVisit_freq() != 0 && oldNCP != null && treatmentForm.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                } else if ((treatmentForm.getAs_needed() >= 0 && oldNCP == null) || (treatmentForm.getAs_needed() >=0 && oldNCP != null && treatmentForm.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }
                            }
                            TreatmentCommentVO getcurrTreat = new TreatmentCommentVO();
                            getcurrTreat.setAssessment_id(new Integer(assessment_id));
                            getcurrTreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            getcurrTreat = tBD.getTreatmentComment(getcurrTreat);
                            TreatmentCommentVO newtreat = new TreatmentCommentVO();
                            newtreat.setActive(0);
                            newtreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newtreat.setAssessment_id(new Integer(assessment_id));
                            newtreat.setPatient_id(new Integer(patient_id));
                            newtreat.setWound_id(new Integer(wound_id));
                            newtreat.setProfessional_id(userVO.getId());
                            newtreat.setCreated_on(new Date());
                            newtreat.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getWound_treatment_comments(), false)));
                            if (getcurrTreat != null) {
                                newtreat.setId(getcurrTreat.getId());
                            }
                            if ((treatmentForm.getWound_treatment_comments() != null )) {
                                newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getWound_treatment_comments(), false)));

                                tBD.saveTreatmentComment(newtreat);
                            }
                        } else if (updateAssessmentVO instanceof AssessmentOstomyVO) {
                            productVO.setOstomy_assessment_id(updateAssessmentVO.getId());

                            String body = Common.convertCarriageReturns(treatmentForm.getOstomy_nursing_care_plan(), false);
                            NursingCarePlanVO ncpvo = new NursingCarePlanVO();
                            ncpvo.setAssessment_id(new Integer(assessment_id));
                            ncpvo.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            ncpvo = ncpBD.getNursingCarePlan(ncpvo);
                            NursingCarePlanVO newncp = new NursingCarePlanVO();
                            newncp.setActive(0);
                            newncp.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newncp.setAssessment_id(new Integer(assessment_id));
                            newncp.setPatient_id(new Integer(patient_id));
                            newncp.setWound_id(new Integer(wound_id));
                            newncp.setProfessional_id(userVO.getId());
                            newncp.setCreated_on(new Date());
                            newncp.setVisit_frequency(new Integer(treatmentForm.getVisit_freq()));
                            newncp.setAs_needed(new Integer(treatmentForm.getAs_needed()));
                            newncp.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newncp.setBody(body);
                            if (ncpvo != null) {
                                newncp.setId(ncpvo.getId());
                            }
                            if (newncp != null && newncp.getBody() != null) {
                                if ((treatmentForm.getOstomy_nursing_care_plan() != null && !(treatmentForm.getOstomy_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP == null) || (treatmentForm.getOstomy_nursing_care_plan() != null && !(treatmentForm.getOstomy_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP != null && oldNCP.getBody() != null && newncp.getBody() != null && !oldNCP.getBody().equals(newncp.getBody()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                } else if ((treatmentForm.getVisit_freq() != 0 && oldNCP == null) || (treatmentForm.getVisit_freq() != 0 && oldNCP != null && treatmentForm.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }else if ((treatmentForm.getAs_needed() >= 0 && oldNCP == null) || (treatmentForm.getAs_needed() >= 0 && oldNCP != null && treatmentForm.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }
                            }
                            TreatmentCommentVO getcurrTreat = new TreatmentCommentVO();
                            getcurrTreat.setAssessment_id(new Integer(assessment_id));
                            getcurrTreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            getcurrTreat = tBD.getTreatmentComment(getcurrTreat);
                            TreatmentCommentVO newtreat = new TreatmentCommentVO();
                            newtreat.setActive(0);
                            newtreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newtreat.setAssessment_id(new Integer(assessment_id));
                            newtreat.setPatient_id(new Integer(patient_id));
                            newtreat.setWound_id(new Integer(wound_id));
                            newtreat.setProfessional_id(userVO.getId());
                            newtreat.setCreated_on(new Date());
                            newtreat.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getOstomy_treatment_comments(), false)));
                            if (getcurrTreat != null) {
                                newtreat.setId(getcurrTreat.getId());
                            }
                            if (treatmentForm.getOstomy_treatment_comments() != null) {
                                newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getOstomy_treatment_comments(), false)));
                                tBD.saveTreatmentComment(newtreat);
                            }
                        } else if (updateAssessmentVO instanceof AssessmentSkinVO) {
                            productVO.setSkin_assessment_id(updateAssessmentVO.getId());

                            String body = Common.convertCarriageReturns(treatmentForm.getSkin_nursing_care_plan(), false);
                            NursingCarePlanVO ncpvo = new NursingCarePlanVO();
                            ncpvo.setAssessment_id(new Integer(assessment_id));
                            ncpvo.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            ncpvo = ncpBD.getNursingCarePlan(ncpvo);
                            NursingCarePlanVO newncp = new NursingCarePlanVO();
                            newncp.setActive(0);
                            newncp.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newncp.setAssessment_id(new Integer(assessment_id));
                            newncp.setPatient_id(new Integer(patient_id));
                            newncp.setWound_id(new Integer(wound_id));
                            newncp.setProfessional_id(userVO.getId());
                            newncp.setCreated_on(new Date());
                            newncp.setVisit_frequency(new Integer(treatmentForm.getVisit_freq()));
                            newncp.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newncp.setBody(body);
                            if (ncpvo != null) {
                                newncp.setId(ncpvo.getId());
                            }
                            if (newncp != null && newncp.getBody() != null) {
                                if ((treatmentForm.getSkin_nursing_care_plan() != null && !(treatmentForm.getSkin_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP == null) || (treatmentForm.getSkin_nursing_care_plan() != null && !(treatmentForm.getSkin_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP != null && oldNCP.getBody() != null && newncp.getBody() != null && !oldNCP.getBody().equals(newncp.getBody()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                } else if ((treatmentForm.getVisit_freq() != 0 && oldNCP == null) || (treatmentForm.getVisit_freq() != 0 && oldNCP != null && treatmentForm.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }else if ((treatmentForm.getAs_needed() >= 0 && oldNCP == null) || (treatmentForm.getAs_needed() >= 0 && oldNCP != null && treatmentForm.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }
                            }
                            TreatmentCommentVO getcurrTreat = new TreatmentCommentVO();
                            getcurrTreat.setAssessment_id(new Integer(assessment_id));
                            getcurrTreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            getcurrTreat = tBD.getTreatmentComment(getcurrTreat);
                            TreatmentCommentVO newtreat = new TreatmentCommentVO();
                            newtreat.setActive(0);
                            newtreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newtreat.setAssessment_id(new Integer(assessment_id));
                            newtreat.setPatient_id(new Integer(patient_id));
                            newtreat.setWound_id(new Integer(wound_id));
                            newtreat.setProfessional_id(userVO.getId());
                            newtreat.setCreated_on(new Date());
                            newtreat.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getSkin_treatment_comments(), false)));
                            if (getcurrTreat != null) {
                                newtreat.setId(getcurrTreat.getId());
                            }
                            if (treatmentForm.getSkin_treatment_comments() != null) {
                                newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getSkin_treatment_comments(), false)));
                                tBD.saveTreatmentComment(newtreat);
                            }
                        } else if (updateAssessmentVO instanceof AssessmentIncisionVO) {
                            productVO.setIncision_assessment_id(updateAssessmentVO.getId());

                            NursingCarePlanVO ncpvo = new NursingCarePlanVO();
                            ncpvo.setAssessment_id(new Integer(assessment_id));
                            ncpvo.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            ncpvo = ncpBD.getNursingCarePlan(ncpvo);
                            NursingCarePlanVO newncp = new NursingCarePlanVO();
                            newncp.setActive(0);
                            newncp.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newncp.setAssessment_id(new Integer(assessment_id));
                            newncp.setPatient_id(new Integer(patient_id));
                            newncp.setWound_id(new Integer(wound_id));
                            newncp.setProfessional_id(userVO.getId());
                            newncp.setVisit_frequency(new Integer(treatmentForm.getVisit_freq()));
                            newncp.setAs_needed(new Integer(treatmentForm.getAs_needed()));
                            newncp.setCreated_on(new Date());
                            newncp.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newncp.setBody(Common.convertCarriageReturns(treatmentForm.getIncision_nursing_care_plan(), false));
                            if (ncpvo != null) {
                                newncp.setId(ncpvo.getId());
                            }
                            if (newncp != null && newncp.getBody() != null) {
                                if ((treatmentForm.getIncision_nursing_care_plan() != null && !(treatmentForm.getIncision_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP == null) || (treatmentForm.getIncision_nursing_care_plan() != null && !(treatmentForm.getIncision_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP != null && oldNCP.getBody() != null && newncp.getBody() != null && !oldNCP.getBody().equals(newncp.getBody()))) {
                                    ncpBD.saveNursingCarePlan(newncp);

                                } else if ((treatmentForm.getVisit_freq() != 0 && oldNCP == null) || (treatmentForm.getVisit_freq() != 0 && oldNCP != null && treatmentForm.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }else if ((treatmentForm.getAs_needed() >= 0 && oldNCP == null) || (treatmentForm.getAs_needed() >= 0 && oldNCP != null && treatmentForm.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }
                            }
                            TreatmentCommentVO getcurrTreat = new TreatmentCommentVO();
                            getcurrTreat.setAssessment_id(new Integer(assessment_id));
                            getcurrTreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            getcurrTreat = tBD.getTreatmentComment(getcurrTreat);
                            TreatmentCommentVO newtreat = new TreatmentCommentVO();
                            newtreat.setActive(0);
                            newtreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newtreat.setAssessment_id(new Integer(assessment_id));
                            newtreat.setPatient_id(new Integer(patient_id));
                            newtreat.setWound_id(new Integer(wound_id));
                            newtreat.setProfessional_id(userVO.getId());
                            newtreat.setCreated_on(new Date());
                            newtreat.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getIncision_treatment_comments(), false)));
                            if (getcurrTreat != null) {
                                newtreat.setId(getcurrTreat.getId());
                            }
                            if (treatmentForm.getIncision_treatment_comments() != null) {
                                newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getIncision_treatment_comments(), false)));
                                tBD.saveTreatmentComment(newtreat);
                            }

                        } else if (updateAssessmentVO instanceof AssessmentDrainVO) {
                            productVO.setDrain_assessment_id(updateAssessmentVO.getId());


                            NursingCarePlanVO ncpvo = new NursingCarePlanVO();
                            ncpvo.setAssessment_id(new Integer(assessment_id));
                            ncpvo.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            ncpvo = ncpBD.getNursingCarePlan(ncpvo);
                            NursingCarePlanVO newncp = new NursingCarePlanVO();
                            newncp.setActive(0);
                            newncp.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newncp.setAssessment_id(new Integer(assessment_id));
                            newncp.setPatient_id(new Integer(patient_id));
                            newncp.setWound_id(new Integer(wound_id));
                            newncp.setProfessional_id(userVO.getId());
                            newncp.setVisit_frequency(new Integer(treatmentForm.getVisit_freq()));
                            newncp.setAs_needed(new Integer(treatmentForm.getAs_needed()));
                            newncp.setCreated_on(new Date());
                            newncp.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newncp.setBody(Common.convertCarriageReturns(treatmentForm.getDrain_nursing_care_plan(), false));
                            if (ncpvo != null) {
                                newncp.setId(ncpvo.getId());
                            }
                            if (newncp != null && newncp.getBody() != null) {
                                if ((treatmentForm.getDrain_nursing_care_plan() != null && !(treatmentForm.getDrain_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP == null) || (treatmentForm.getDrain_nursing_care_plan() != null && !(treatmentForm.getDrain_nursing_care_plan().replaceAll("<br>","")).equals("") && oldNCP != null && oldNCP.getBody() != null && newncp.getBody() != null && !oldNCP.getBody().equals(newncp.getBody()))) {
                                    ncpBD.saveNursingCarePlan(newncp);

                                } else if ((treatmentForm.getVisit_freq() != 0 && oldNCP == null) || (treatmentForm.getVisit_freq() != 0 && oldNCP != null && treatmentForm.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }else if ((treatmentForm.getAs_needed() >= 0 && oldNCP == null) || (treatmentForm.getAs_needed() >= 0 && oldNCP != null && treatmentForm.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }
                            }
                            TreatmentCommentVO getcurrTreat = new TreatmentCommentVO();
                            getcurrTreat.setAssessment_id(new Integer(assessment_id));
                            getcurrTreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            getcurrTreat = tBD.getTreatmentComment(getcurrTreat);
                            TreatmentCommentVO newtreat = new TreatmentCommentVO();
                            newtreat.setActive(0);
                            newtreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newtreat.setAssessment_id(new Integer(assessment_id));
                            newtreat.setPatient_id(new Integer(patient_id));
                            newtreat.setWound_id(new Integer(wound_id));
                            newtreat.setProfessional_id(userVO.getId());
                            newtreat.setCreated_on(new Date());
                            newtreat.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getDrain_treatment_comments(), false)));
                            if (getcurrTreat != null) {
                                newtreat.setId(getcurrTreat.getId());
                            }
                            if (treatmentForm.getDrain_treatment_comments() != null) {
                                newtreat.setBody(Common.stripHTML(Common.convertCarriageReturns(treatmentForm.getDrain_treatment_comments(), false)));
                                tBD.saveTreatmentComment(newtreat);
                            }
                        } else if (updateAssessmentVO instanceof AssessmentBurnVO) {
                            productVO.setBurn_assessment_id(updateAssessmentVO.getId());

                            NursingCarePlanVO ncpvo = new NursingCarePlanVO();
                            ncpvo.setAssessment_id(new Integer(assessment_id));
                            ncpvo.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            ncpvo = ncpBD.getNursingCarePlan(ncpvo);
                            NursingCarePlanVO newncp = new NursingCarePlanVO();
                            newncp.setActive(0);
                            newncp.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newncp.setAssessment_id(new Integer(assessment_id));
                            newncp.setPatient_id(new Integer(patient_id));
                            newncp.setWound_id(new Integer(wound_id));
                            newncp.setVisit_frequency(new Integer(treatmentForm.getVisit_freq()));
                            newncp.setAs_needed(new Integer(treatmentForm.getAs_needed()));
                            newncp.setProfessional_id(userVO.getId());
                            newncp.setCreated_on(new Date());
                            newncp.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newncp.setBody(Common.convertCarriageReturns(treatmentForm.getBurn_nursing_care_plan(), false));
                            if (ncpvo != null) {
                                newncp.setId(ncpvo.getId());
                            }
                            if (newncp != null && newncp.getBody() != null) {
                                if ((treatmentForm.getBurn_nursing_care_plan() != null && !treatmentForm.getBurn_nursing_care_plan().equals("") && oldNCP == null) || (treatmentForm.getBurn_nursing_care_plan() != null && !treatmentForm.getBurn_nursing_care_plan().equals("") && oldNCP != null && oldNCP.getBody() != null && newncp.getBody() != null && !oldNCP.getBody().equals(newncp.getBody()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                } else if ((treatmentForm.getVisit_freq() != 0 && oldNCP == null) || (treatmentForm.getVisit_freq() != 0 && oldNCP != null && treatmentForm.getVisit_freq() != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }else if ((treatmentForm.getAs_needed() >= 0 && oldNCP == null) || (treatmentForm.getAs_needed() >= 0 && oldNCP != null && treatmentForm.getAs_needed() != (oldNCP.getAs_needed() == null ? 0 : oldNCP.getAs_needed().intValue()))) {
                                    ncpBD.saveNursingCarePlan(newncp);
                                }
                            }
                            TreatmentCommentVO getcurrTreat = new TreatmentCommentVO();
                            getcurrTreat.setAssessment_id(new Integer(assessment_id));
                            getcurrTreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            getcurrTreat = tBD.getTreatmentComment(getcurrTreat);
                            TreatmentCommentVO newtreat = new TreatmentCommentVO();
                            newtreat.setActive(0);
                            newtreat.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                            newtreat.setAssessment_id(new Integer(assessment_id));
                            newtreat.setPatient_id(new Integer(patient_id));
                            newtreat.setWound_id(new Integer(wound_id));
                            newtreat.setProfessional_id(userVO.getId());
                            newtreat.setCreated_on(new Date());
                            newtreat.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO,locale));
                            newtreat.setBody(Common.convertCarriageReturns(treatmentForm.getWound_nursing_care_plan(), false));
                            if (getcurrTreat != null) {
                                newtreat.setId(getcurrTreat.getId());
                            }
                            if (treatmentForm.getBurn_treatment_comments() != null && !treatmentForm.getBurn_treatment_comments().equals("")) {
                                newtreat.setBody(Common.convertCarriageReturns(treatmentForm.getBurn_treatment_comments(), false));
                                tBD.saveTreatmentComment(newtreat);
                            }
                        }

                        assessmentBD.removeProducts(productVO);
                        //Gather products
                        List list = new LinkedList();

                        int cnt = 0;

                        if (products != null) {

                            for (String product : products) {
                                    try{
                                StringTokenizer st = new StringTokenizer(product, "|");
                                if (st.hasMoreTokens()) {
                                    String quantity = st.nextToken();
                                    String alpha = st.nextToken();
                                    String prod_id = st.nextToken();
                                    if (alpha.equals(updateAssessmentVO.getAlpha_id() + "")) {
                                        AssessmentProductVO prod = new AssessmentProductVO();
                                        prod.setPatient_id(updateAssessmentVO.getPatient_id());
                                        prod.setWound_id(updateAssessmentVO.getWound_id());
                                        prod.setAssessment_id(updateAssessmentVO.getAssessment_id());
                                        if (updateAssessmentVO instanceof AssessmentEachwoundVO) {
                                            prod.setWound_assessment_id(updateAssessmentVO.getId());

                                        } else if (updateAssessmentVO instanceof AssessmentOstomyVO) {
                                            prod.setOstomy_assessment_id(updateAssessmentVO.getId());
                                        } else if (updateAssessmentVO instanceof AssessmentIncisionVO) {
                                            prod.setIncision_assessment_id(updateAssessmentVO.getId());
                                        } else if (updateAssessmentVO instanceof AssessmentDrainVO) {
                                            prod.setDrain_assessment_id(updateAssessmentVO.getId());
                                        } else if (updateAssessmentVO instanceof AssessmentBurnVO) {
                                            prod.setBurn_assessment_id(updateAssessmentVO.getId());
                                        }else if (updateAssessmentVO instanceof AssessmentSkinVO) {
                                            prod.setSkin_assessment_id(updateAssessmentVO.getId());
                                        }

                                        prod.setAlpha_id(updateAssessmentVO.getAlpha_id());
                                        prod.setProduct_id(prod_id.indexOf("Other;") == -1 ? new Integer(prod_id) : new Integer(-1));
                                        if(!Common.isNumeric(quantity)){
                                            quantity="0";
                                        }
                                        prod.setQuantity(quantity);
                                        prod.setOther(prod_id.indexOf("Other;") != -1 ? prod_id : "");
                                        prod.setActive(new Integer(0));

                                        assessmentBD.saveProduct(prod);
                                        if (Common.getConfig("useTriggers").equals("1") && Common.isOffline() == false) {
                                            try{
                                                boolean triggered = false;
                                                String triggers = (String)request.getParameter("triggers");
                                                if(triggers!=null && !triggers.equals("")){
                                                    String prod_serial = "";
                                                    String[] serial = triggers.split(":");
                                                    for(String id : serial){
                                                       TriggerVO trigger = tservice.getTrigger(new Integer(id));
                                                       Collection<TriggerProductsVO> prods = trigger.getProduct_ids();
                                                       for(TriggerProductsVO trigprod : prods){
                                                           if(trigprod.getProduct_id().equals(prod.getProduct_id())){
                                                               prod_serial=prod_serial+"["+trigprod.getProduct_id()+"] alpha_id:{"+prod.getAlpha_id()+"} assessment_id:{"+prod.getAssessment_id()+"}";
                                                               triggered=true;
                                                           }
                                                       }
                                                       if(prod_serial!=null && !prod_serial.equals("")){
                                                        LogAuditVO vo = new LogAuditVO(userVO.getId(), new Integer(patient_id), "trigger.product.used", "triggers",trigger.getTrigger_id(),prod_serial);
                                                        lservice.saveLogAudit(vo);
                                                       }
                                                       if(triggered == false){
                                                            LogAuditVO vo = new LogAuditVO(userVO.getId(), new Integer(patient_id), "trigger.product.not_used", "triggers",trigger.getTrigger_id(),"["+prod.getProduct_id()+"] alpha_id:{"+prod.getAlpha_id()+"} assessment_id:{"+prod.getAssessment_id()+"}");
                                                            lservice.saveLogAudit(vo);
                                                        }
                                                    }
                                                    
                                                }
                                                
                                            }catch(Exception e){}
                                            //Collection<LogAuditVO> logs = lservice.getLogs(log, PDate., null);
                                        }
                                    }
                                }
                                    }catch(java.util.NoSuchElementException e){
                                        log.error("StringTokenizer: NoSuchElement: "+e.getMessage());
                                    }
                            }
                        }


                        assessmentBD.saveAssessment(updateAssessmentVO);
                        //Save Negative Pressure Wound therapy.
                        AssessmentNPWTVO nt = new AssessmentNPWTVO();
                        nt.setAssessment_id(updateAssessmentVO.getAssessment_id());
                        nt.setSecondary(0);
                        //find out if we need to update or insert.
                        AssessmentNPWTVO np = assessmentNPWTBD.getNegativePressure(nt);
                        if (np != null) {
                            //purge previously saved.
                            assessmentNPWTBD.removeNegativePressure(np);
                        }
                        if(treatmentForm.getNpwt_alphas1()!=null){
                        np = new AssessmentNPWTVO();
                        np.setAssessment_id(updateAssessmentVO.getAssessment_id());
                        np.setGoal_of_therapy(treatmentForm.getGoal_of_therapy1());
                        np.setVendor_name(treatmentForm.getVendor_name1());
                        np.setMachine_acquirement(treatmentForm.getMachine_acquirement1());
                        np.setSecondary(0);
                        np.setInitiated_by(treatmentForm.getInitiated_by1());
                        np.setKci_num(treatmentForm.getKci_num1());
                        np.setSerial_num(treatmentForm.getSerial_num1());
                        np.setPatient_id(updateAssessmentVO.getPatient_id());
                        np.setNp_connector(treatmentForm.getNp_connector1());
                        np.setPressure_reading(treatmentForm.getPressure_reading1());
                        np.setProfessional_id(userVO.getId());
                        np.setReason_for_ending(treatmentForm.getReason_for_ending1());
                        np.setTherapy_setting(treatmentForm.getTherapy_setting1());
                        
                        np.setVac_end_date(PDate.getDate(treatmentForm.getVacend1_month(), treatmentForm.getVacend1_day(), treatmentForm.getVacend1_year()));
                        np.setVac_start_date(PDate.getDate(treatmentForm.getVacstart1_month(), treatmentForm.getVacstart1_day(), treatmentForm.getVacstart1_year()));
                        np.setWound_id(updateAssessmentVO.getWound_id());
                        
                        assessmentNPWTBD.saveNegativePressure(np);
                        AssessmentNPWTVO new_np = assessmentNPWTBD.getNegativePressure(np);
                        if(new_np!=null && treatmentForm.getNpwt_alphas1()!=null){
                            //save alphas.
                            for(Integer id : treatmentForm.getNpwt_alphas1()){
                                AssessmentNPWTAlphaVO a = new AssessmentNPWTAlphaVO();
                                a.setAlpha_id(id);
                                a.setAssessment_npwt_id(new_np.getId());
                                assessmentNPWTBD.saveNegativePressureAlpha(a);
                            }
                        }
                        }
                        //Save Secondary NPWT
                        //Save Negative Pressure Wound therapy.
                        AssessmentNPWTVO nt2 = new AssessmentNPWTVO();
                        nt2.setAssessment_id(updateAssessmentVO.getAssessment_id());
                        nt2.setSecondary(1);
                        //find out if we need to update or insert.
                        AssessmentNPWTVO np2 = assessmentNPWTBD.getNegativePressure(nt2);
                        if (np2 != null) {
                            //purge previously saved.
                            assessmentNPWTBD.removeNegativePressure(np2);
                        }
                        if(treatmentForm.getNpwt_alphas2()!=null){
                        np2 = new AssessmentNPWTVO();
                        np2.setAssessment_id(updateAssessmentVO.getAssessment_id());
                        np2.setGoal_of_therapy(treatmentForm.getGoal_of_therapy2());
                        np2.setSecondary(0);
                        np2.setInitiated_by(treatmentForm.getInitiated_by2());
                        np.setVendor_name(treatmentForm.getVendor_name2());
                        np.setMachine_acquirement(treatmentForm.getMachine_acquirement2());
                        np2.setKci_num(treatmentForm.getKci_num2());
                        np2.setSerial_num(treatmentForm.getSerial_num2());
                        np2.setPatient_id(updateAssessmentVO.getPatient_id());
                        np2.setNp_connector(treatmentForm.getNp_connector2());
                        np2.setPressure_reading(treatmentForm.getPressure_reading2());
                        np2.setProfessional_id(userVO.getId());
                        np2.setReason_for_ending(treatmentForm.getReason_for_ending2());
                        np2.setTherapy_setting(treatmentForm.getTherapy_setting2());
                        np2.setVac_end_date(PDate.getDate(treatmentForm.getVacend2_month(), treatmentForm.getVacend2_day(), treatmentForm.getVacend2_year()));
                        np2.setVac_start_date(PDate.getDate(treatmentForm.getVacstart2_month(), treatmentForm.getVacstart2_day(), treatmentForm.getVacstart2_year()));
                        np2.setWound_id(updateAssessmentVO.getWound_id());
                        assessmentNPWTBD.saveNegativePressure(np2);
                        AssessmentNPWTVO new_np2 = assessmentNPWTBD.getNegativePressure(np2);
                        if(new_np2!=null && treatmentForm.getNpwt_alphas2()!=null){
                            //save alphas.
                            for(Integer id : treatmentForm.getNpwt_alphas2()){
                                AssessmentNPWTAlphaVO a = new AssessmentNPWTAlphaVO();
                                a.setAlpha_id(id);
                                a.setAssessment_npwt_id(new_np2.getId());
                                assessmentNPWTBD.saveNegativePressureAlpha(a);
                            }
                        }
                        }
                        AssessmentTreatmentVO t = new AssessmentTreatmentVO();
                        t.setAssessment_id(updateAssessmentVO.getAssessment_id());
                        t.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        //find out if we need to update or insert.
                        AssessmentTreatmentVO treatment = tBD.getAssessmentTreatment(t);

                        if (treatment == null) {
                            treatment = new AssessmentTreatmentVO();
                        }
                        

                        //treatment.setAntibiotics(Common.stripCarriageReturns(Serialize.serialize(treatmentForm.getAntibiotics())));
                        treatment.setAssessment_id(updateAssessmentVO.getAssessment_id());
                        treatment.setWound_id(updateAssessmentVO.getWound_id());
                        treatment.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        treatment.setWound_profile_type_id(updateAssessmentVO.getWound_profile_type_id());
                        treatment.setBath(treatmentForm.getBath());
                        treatment.setNail_care(treatmentForm.getNail_care());
                        treatment.setReview_done(treatmentForm.getReview_done());
                        treatment.setCs_done(treatmentForm.getCs_done());
                        treatment.setBandages_in(null);
                        treatment.setBandages_out(null);
                        treatment.setPatient_id(updateAssessmentVO.getPatient_id());
                        treatment.setProfessional_id(userVO.getId());
                        if (request.getParameter("bandages_out"+updateAssessmentVO.getAlpha_id()) != null && ((String)request.getParameter("bandages_out"+updateAssessmentVO.getAlpha_id())).length() != 0) {
                            String out = (String)request.getParameter("bandages_out"+updateAssessmentVO.getAlpha_id());
                            treatment.setBandages_out(new Integer(out));
                        }
                        if (request.getParameter("bandages_in"+updateAssessmentVO.getAlpha_id()) != null && ((String)request.getParameter("bandages_in"+updateAssessmentVO.getAlpha_id())).length() != 0) {
                            String in = (String)request.getParameter("bandages_in"+updateAssessmentVO.getAlpha_id());
                            treatment.setBandages_in(new Integer(in));
                        }
                        //treatment.setArray_elements(treatment_array);
                        //save to each individual alpha now because later this section will be per alpha.
                        tBD.saveAssessmentTreatment(treatment);
                        
                        AssessmentTreatmentVO tt = new AssessmentTreatmentVO();
                        tt.setAssessment_id(updateAssessmentVO.getAssessment_id());
                        tt.setAlpha_id(updateAssessmentVO.getAlpha_id());
                        //find out if we need to update or insert.
                        AssessmentTreatmentVO returnedTreat = tBD.getAssessmentTreatment(tt);
                        if(returnedTreat!=null){
                            try {
                                AssessmentTreatmentArraysVO deleteCS = new AssessmentTreatmentArraysVO();
                                deleteCS.setAssessment_treatment_id(returnedTreat.getId());
                                deleteCS.setAlpha_id(updateAssessmentVO.getAlpha_id());
                                tBD.removeTreatmentArray(deleteCS);
                                if (treatmentForm.getAdjunctive() != null) {
                                    for (Integer item : treatmentForm.getAdjunctive()) {
                                        String other = "";
                                        
                                        if(request.getParameter("other"+item)!=null){
                                            other = (String)request.getParameter("other"+item);

                                        }
                                        AssessmentTreatmentArraysVO it = new AssessmentTreatmentArraysVO(returnedTreat.getId(), LookupVO.ADJUNCTIVE, item, updateAssessmentVO.getAlpha_id(),other);
                                        tBD.saveTreatmentArray(it);
                                    }
                                }
                                
                                //System.out.println("CSRESULT: "+treatmentForm.getCsresult().toString());
                                Vector<String> v = Common.parseSelectByAlpha(updateAssessmentVO.getAlphaLocation().getAlpha(), treatmentForm.getCsresult(),locale);
                                for (String item : v) {
                                    
                                    int id = new Integer(item.trim());
                                    
                                    AssessmentTreatmentArraysVO it = new AssessmentTreatmentArraysVO(returnedTreat.getId(), LookupVO.CNSRESULT, id, updateAssessmentVO.getAlpha_id(),"");
                                    tBD.saveTreatmentArray(it);
                                }
                                Vector<String> v2 = Common.parseSelectByAlpha(updateAssessmentVO.getAlphaLocation().getAlpha(), treatmentForm.getWound_ccac_questions(),locale);
                                for (String item : v2) {
                                    int id = new Integer(item.trim());
                                    AssessmentTreatmentArraysVO it = new AssessmentTreatmentArraysVO(returnedTreat.getId(), LookupVO.CCAC_QUESTIONS, id, updateAssessmentVO.getAlpha_id(), "");
                                    tBD.saveTreatmentArray(it);
                                }
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                        String[] antibiotics = treatmentForm.getAntibiotics();

                        if (antibiotics != null && returnedTreat!=null) {
                            AssessmentAntibioticVO tmpAnti = new AssessmentAntibioticVO();
                            tmpAnti.setAssessment_treatment_id(returnedTreat.getId());
                            //tmpAnti.setAlpha_id(updateAssessmentVO.getAlpha_id());
                            assessmentBD.removeAntibiotic(tmpAnti);
                            

                            for (String antibiotic : antibiotics) {
                                try {
                                    int index = antibiotic.indexOf(" : ");
                                    String name = antibiotic.substring(0, index);
                                    String tmp = antibiotic.substring(index + 3, antibiotic.length());

                                    int index3 = tmp.indexOf(" to ");
                                    String date1 = tmp.substring(0, index3);
                                    String date2 = "";
                                    try {

                                        date2 = tmp.substring(index3 + 4, tmp.length());
                                    } catch (StringIndexOutOfBoundsException e) {
                                    }
                                    SimpleDateFormat sf = new SimpleDateFormat("d/M/yyyy");
                                    Date start_date = sf.parse(date1);
                                    Date end_date = null;
                                    if (date2 != null && !date2.equals("") && !date2.equals("//")) {
                                        end_date = sf.parse(date2);
                                    }
                                    if (start_date != null && !name.equals("") && !name.equals("")) {
                                        AssessmentAntibioticVO a = new AssessmentAntibioticVO();
                                        a.setAssessment_treatment_id(returnedTreat.getId());
                                        a.setStart_date(start_date);
                                        if (end_date != null) {//may not have end_date
                                            a.setEnd_date(end_date);
                                        }
                                        a.setAntibiotic(name);
                                        assessmentBD.saveAntibiotic(a);
                                    }
                                } catch (Exception e) {
                                    log.error("Unparseable Date: (IGNORE)"+e.getMessage());
                                }
                            }
                        }
                        log.info("##$$##$$##-----ASSESSMENT TO UPDATE ASSESSMENT ID : " + updateAssessmentVO.getAssessment_id() + "-----##$$##$$##");
                    }


                }
                request.setAttribute("pop_treatment", treatmentForm.getPop_treatment() + "");
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in Treatment.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        }

        if (request.getParameter("page") == null && userVO != null) {
            ActionErrors errors = new ActionErrors();
            errors.add("errors", new ActionError("pixalere.common.page.error"));
            saveErrors(request, errors);
            return (mapping.findForward("uploader.treatment.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("uploader.go.profile"));
        }
    }
    public boolean compareNCP(String body,int visit_freq, NursingCarePlanVO oldNCP, NursingCarePlanVO newNCP) {

        if ((body != null && !(body.replaceAll("<br>", "")).equals("") && oldNCP == null) || (body != null && !(body.replaceAll("<br>", "")).equals("") && oldNCP != null && oldNCP.getBody() != null && newNCP.getBody() != null && !oldNCP.getBody().equals(newNCP.getBody()))) {
            return true;
        } else if ((visit_freq != 0 && oldNCP == null) || (visit_freq != 0 && oldNCP != null && visit_freq != (oldNCP.getVisit_frequency() == null ? 0 : oldNCP.getVisit_frequency().intValue()))) {
            return true;
        } 
        return false;
    }
}
