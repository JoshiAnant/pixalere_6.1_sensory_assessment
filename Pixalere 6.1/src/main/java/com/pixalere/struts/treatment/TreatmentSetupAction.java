package com.pixalere.struts.treatment;

import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.guibeans.trigger.ProductTrigger;
import com.pixalere.common.service.TriggerServiceImpl;
import com.pixalere.assessment.bean.*;
import java.util.TreeMap;
import com.pixalere.common.bean.LibraryVO;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.bean.ComponentsVO;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.common.dao.ProductsDAO;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentNPWTServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.utils.*;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.text.SimpleDateFormat;
import java.util.*;

public class TreatmentSetupAction extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TreatmentSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
            if (session.getAttribute("patient_id") == null) {
                return (mapping.findForward("uploader.null.patient"));
            }
            String patient_id = (String) session.getAttribute("patient_id");
            String wound_id = (String) session.getAttribute("wound_id");

            if (wound_id == null || wound_id.length() == 0) {
                ActionErrors errors = new ActionErrors();
                errors.add("nowound_id", new ActionError("pixalere.assessment.form.error.nowound_id"));
                saveErrors(request, errors);
                return (mapping.findForward("uploader.go.woundprofiles"));
            }
            //Generate Token - prevent multiple posts
            saveToken(request);
            //get components records for this table
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            comp.setFlowchart(com.pixalere.utils.Constants.WOUND_ASSESSMENT);
            Collection components = gui.getAllComponents(comp);
            java.util.Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);

            com.pixalere.common.bean.ComponentsVO comp2 = new com.pixalere.common.bean.ComponentsVO();
            comp2.setFlowchart(com.pixalere.utils.Constants.NURSING_CARE_PLAN);
            Collection components2 = gui.getAllComponents(comp2);
            java.util.Hashtable hashized2 = Common.hashComponents(components2);
            request.setAttribute("componentsTreatment", hashized2);

            ProductsVO productsVO = new ProductsVO();
            ProductsServiceImpl productsBD = new ProductsServiceImpl();
            ProductsDAO productsDAO = new ProductsDAO();

            ProductCategoryVO productCategoryVO = new ProductCategoryVO();
            ProductCategoryServiceImpl productCategoryDAO = new ProductCategoryServiceImpl();
            WoundAssessmentServiceImpl waDAO = new WoundAssessmentServiceImpl();
            productsVO.setActive(new Integer(1));
            PatientServiceImpl pBD = new PatientServiceImpl(language);
            AssessmentServiceImpl assessmentBD = new AssessmentServiceImpl(language);
            PatientProfileServiceImpl profileService = new PatientProfileServiceImpl(language);
            AssessmentNPWTServiceImpl npwtBD = new AssessmentNPWTServiceImpl();
            TreatmentCommentsServiceImpl tBD = new TreatmentCommentsServiceImpl();
            NursingCarePlanServiceImpl nBD = new NursingCarePlanServiceImpl();
            WoundAssessmentLocationServiceImpl walManager = new WoundAssessmentLocationServiceImpl();
            TriggerServiceImpl tservice = new TriggerServiceImpl();
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            request.setAttribute("page", "treatment");

            Vector currentProducts = new Vector();
            Vector currentProductsId = new Vector();
            String assessment_id = null;
            WoundAssessmentVO atmp = new WoundAssessmentVO();
            atmp.setWound_id(new Integer(wound_id));
            atmp.setActive(0);
            atmp.setProfessional_id(userVO.getId());
            WoundAssessmentVO tmpAssess = waDAO.getAssessment(atmp);
            if (tmpAssess != null) {
                assessment_id = tmpAssess.getId() + "";
            } else {
                return (mapping.findForward("uploader.go.assess"));
            }

            Collection<AbstractAssessmentVO> lastAssessmentCol = assessmentBD.getAllAssessments(Integer.parseInt(assessment_id));

            //Display wound profile on page
            com.pixalere.wound.service.WoundServiceImpl woundBD = new com.pixalere.wound.service.WoundServiceImpl(language);
            WoundProfileVO woundVO = null;
            if (wound_id != null && (wound_id).length() > 0) {
                WoundProfileVO criteria = new WoundProfileVO();
                criteria.setWound_id(new Integer(wound_id));
                criteria.setActive(new Integer(0));
                criteria.setProfessional_id(userVO.getId());
                woundVO = (WoundProfileVO) woundBD.getWoundProfile(criteria);
                if (woundVO == null) {
                    criteria.setActive(1);
                    criteria.setProfessional_id(null);
                    criteria.setDeleted(new Integer(0));
                    //woundVO = (WoundProfileVO) woundBD.getWoundProfile(criteria);
                }
                if (woundVO != null) {
                    request.setAttribute("wound_profile", woundVO.getWound());

                }
                WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
                WoundAssessmentLocationVO tloco = new WoundAssessmentLocationVO();
                tloco.setWound_id(new Integer((String) wound_id));
                tloco.setDischarge(0); // Only open alphas
                tloco.setDeleted(0);
                Vector<WoundAssessmentLocationVO> allalphas = walservice.retrieveLocationsForWound(userVO.getId(), new Integer(wound_id), false, true);//4th parameter (extra sort) is ignored
                TreeMap alphaMap = new TreeMap();
                for (WoundAssessmentLocationVO v : allalphas) {
                    alphaMap.put(v.getAlpha(), v);
                }
                request.setAttribute("allalphas", alphaMap);
            }
            //Get NPWT - Used also for Triggers (if enabled)
            AssessmentNPWTVO currentNptw = null; // Current valid nptw entry
            
            AssessmentNPWTVO npwttmp1 = new AssessmentNPWTVO();
            npwttmp1.setAssessment_id(new Integer(assessment_id));
            npwttmp1.setSecondary(0);
            AssessmentNPWTVO npwt1 = npwtBD.getNegativePressure(npwttmp1);
            if (npwt1 != null) {
                currentNptw = npwt1;
                request.setAttribute("npwt1", npwt1);
            } else {
                AssessmentNPWTVO npwttmp12 = new AssessmentNPWTVO();
                npwttmp12.setWound_id(new Integer(wound_id));
                npwttmp12.setSecondary(0);

                npwt1 = npwtBD.getNegativePressure(npwttmp12);
                if (npwt1 != null && npwt1.getReason_for_ending() != null && npwt1.getReason_for_ending() > 0) {
                    npwt1 = null;//If last npwt is closed, don't show it.
                } else {
                    currentNptw = npwt1;
                    request.setAttribute("npwt1", npwt1);
                }

            }
            AssessmentNPWTVO npwttmp2 = new AssessmentNPWTVO();
            npwttmp2.setAssessment_id(new Integer(assessment_id));
            npwttmp2.setSecondary(1);
            AssessmentNPWTVO npwt2 = npwtBD.getNegativePressure(npwttmp2);
            if (npwt2 != null) {
                request.setAttribute("npwt2", npwt2);
            } else {
                AssessmentNPWTVO npwttmp12 = new AssessmentNPWTVO();
                npwttmp12.setWound_id(new Integer(wound_id));
                npwttmp12.setSecondary(1);
                AssessmentNPWTVO npwt12 = npwtBD.getNegativePressure(npwttmp12);
                if (npwt1 == null && npwt12 != null) {
                    npwt12.setSecondary(0);
                    //if first npwt was closed, or not existent, make this #1
                    currentNptw = npwt12;
                    request.setAttribute("npwt1", npwt12);
                } else if (npwt12 != null) {
                    request.setAttribute("npwt2", npwt12);
                }
            }
            Vector<WoundAssessmentLocationVO> current_alphas = new Vector<WoundAssessmentLocationVO>();
            Vector<WoundAssessmentLocationVO> current_alphas_wound = new Vector<WoundAssessmentLocationVO>();
            Vector justAlphas = new Vector();//getting alphas for csresults component
            Vector<WoundAssessmentLocationVO> justAlphas_obj = new Vector<WoundAssessmentLocationVO>();//@todo refactor tehese 2 vectors into 1 later
            boolean woundAlphaExists = false;
            boolean ostomyAlphaExists = false;
            boolean drainAlphaExists = false;
            boolean skinAlphaExists = false;
            boolean incisionAlphaExists = false;
            boolean burnAlphaExists = false;
    
            PatientProfileVO profile = null;
            // Get profile for trigger evaluation
            if (Common.getConfig("useTriggers").equals("1") && Common.isOffline() == false) {
                profile = profileService.getTemporaryPatientProfile(Integer.parseInt(patient_id), userVO.getId());
                if(profile==null){
                    PatientProfileVO pp = new PatientProfileVO();
                    pp.setPatient_id(new Integer(patient_id));
                    pp.setCurrent_flag(new Integer(1));
                    pp.setActive(new Integer(1));
                    pp.setDeleted(new Integer(0));
                    profile = profileService.getPatientProfile(pp);
                }
            }
                
            
            String alpha = "A";
            String wound_name = "";
            for (AbstractAssessmentVO assess : lastAssessmentCol) {
                WoundAssessmentLocationVO v = assess.getAlphaLocation();
                if (v != null) {
                    String alpha_type = v.getWound_profile_type().getAlpha_type();
                    if (alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {
                        woundAlphaExists = true;
                        alpha = "A";
                    } else if (alpha_type.equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                        incisionAlphaExists = true;
                        alpha = "I";
                    } else if (alpha_type.equals(Constants.OSTOMY_PROFILE_TYPE)) {
                        ostomyAlphaExists = true;
                        alpha = "O";
                    } else if (alpha_type.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                        drainAlphaExists = true;
                        alpha = "D";
                    } else if (alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {
                        skinAlphaExists = true;
                        alpha = "S";
                    } else if (alpha_type.equals(Constants.BURN_PROFILE_TYPE)) {
                        burnAlphaExists = true;
                        alpha = "B";
                    }
                    // Name used later for trigger recommendations (if enabled)
                    wound_name = Common.getAlphaText(v.getAlpha(), locale);

                    // Check triggers for this assessment
                    if (Common.getConfig("useTriggers").equals("1") && Common.isOffline() == false) {
                        tservice.processProductTriggers(profile, woundVO, assess, alpha,currentNptw);
                    }

                    if (!Common.getConfig("woundActive").equals("-1") && alpha_type.equals(Constants.WOUND_PROFILE_TYPE) && (assess.getStatus().equals(new Integer(Common.getConfig("woundActive"))) || Common.getConfig("hideClosedAlphasOnTreatment").equals("0"))) {//if care_type and has unhealed wound
                        String display_alpha = v.getAlpha();
                        display_alpha = Common.getAlphaText(display_alpha, locale);
                        justAlphas.add(display_alpha);
                        WoundAssessmentLocationVO loc = assess.getAlphaLocation();
                        //if (Common.getConfig("hideClosedAlphasOnTreatment").equals("0")) {
                            current_alphas.add(assess.getAlphaLocation());
                            current_alphas_wound.add(assess.getAlphaLocation());
                        //}
                        justAlphas_obj.add(loc);
                    } else if (!Common.getConfig("incisionActive").equals("-1") && v.getWound_profile_type().getAlpha_type().equals(Constants.INCISIONTAG_PROFILE_TYPE) && (assess.getStatus().equals(new Integer(Common.getConfig("incisionActive"))) || Common.getConfig("hideClosedAlphasOnTreatment").equals("0"))) {

                        String display_alpha = v.getAlpha();
                        display_alpha = Common.getAlphaText(display_alpha, locale);
                        justAlphas.add(display_alpha);
                        WoundAssessmentLocationVO loc = assess.getAlphaLocation();
                        //if (Common.getConfig("hideClosedAlphasOnTreatment").equals("0")) {
                            current_alphas.add(assess.getAlphaLocation());
                       // }
                        justAlphas_obj.add(loc);
                    } else if (!Common.getConfig("drainActive").equals("-1") && v.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE) && (assess.getStatus().equals(new Integer(Common.getConfig("drainActive"))) || Common.getConfig("hideClosedAlphasOnTreatment").equals("0"))) {

                        String display_alpha = v.getAlpha();
                        display_alpha = Common.getAlphaText(display_alpha, locale);
                        justAlphas.add(display_alpha);
                        WoundAssessmentLocationVO loc = assess.getAlphaLocation();
                        //if (Common.getConfig("hideClosedAlphasOnTreatment").equals("0")) {
                            current_alphas.add(assess.getAlphaLocation());
                        //}
                        justAlphas_obj.add(loc);
                    } else if (!Common.getConfig("skinActive").equals("-1") && v.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE) && (assess.getStatus().equals(new Integer(Common.getConfig("skinActive"))) || Common.getConfig("hideClosedAlphasOnTreatment").equals("0"))) {

                        String display_alpha = v.getAlpha();
                        display_alpha = Common.getAlphaText(display_alpha, locale);
                        justAlphas.add(display_alpha);
                        WoundAssessmentLocationVO loc = assess.getAlphaLocation();
                        //if (Common.getConfig("hideClosedAlphasOnTreatment").equals("0")) {
                            current_alphas.add(assess.getAlphaLocation());
                        //}
                        justAlphas_obj.add(loc);
                    } else if ((!Common.getConfig("ostomyActive").equals("-1") && v.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE) && (assess.getStatus().equals(new Integer(Common.getConfig("ostomyActive"))) || assess.getStatus().equals(new Integer(Common.getConfig("ostomyHold")))) || Common.getConfig("hideClosedAlphasOnTreatment").equals("0"))) {
                        String display_alpha = v.getAlpha();
                        display_alpha = Common.getAlphaText(display_alpha, locale);
                        justAlphas.add(display_alpha);
                        WoundAssessmentLocationVO loc = assess.getAlphaLocation();
                        // (Common.getConfig("hideClosedAlphasOnTreatment").equals("0")) {
                            current_alphas.add(assess.getAlphaLocation());
                        //}
                        justAlphas_obj.add(loc);
                    } else if ((!Common.getConfig("burnActive").equals("-1") && v.getWound_profile_type().getAlpha_type().equals(Constants.BURN_PROFILE_TYPE) && (assess.getStatus().equals(new Integer(Common.getConfig("burnActive"))) || Common.getConfig("hideClosedAlphasOnTreatment").equals("0")))) {
                        String display_alpha = v.getAlpha();
                        display_alpha = Common.getAlphaText(display_alpha, locale);
                        justAlphas.add(display_alpha);
                        WoundAssessmentLocationVO loc = assess.getAlphaLocation();
                       // if (Common.getConfig("hideClosedAlphasOnTreatment").equals("0")) {
                            current_alphas.add(assess.getAlphaLocation());
                        //}
                        justAlphas_obj.add(loc);
                    } else {
                        // String display_alpha = v.getAlpha();
                        // display_alpha = Common.getAlphaText(display_alpha);
                        // justAlphas.add(display_alpha);
                        // justAlphas_obj.add(assess.getAlphaLocation());
                        // current_alphas.add(assess.getAlphaLocation());
                    }

                    //}
                } else {
                    //can't find alpha.
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", "Unable to find Alpha: " + assess.getAlpha_id() + "for Assessment: " + assess.getAssessment_id() + "\n\n");
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
            }
           

            
            request.setAttribute("alpha_list", current_alphas);
            request.setAttribute("alpha_list_wound", current_alphas_wound);

            request.setAttribute("woundAlphaExists", woundAlphaExists);
            request.setAttribute("ostomyAlphaExists", ostomyAlphaExists);
            request.setAttribute("incisionAlphaExists", incisionAlphaExists);
            request.setAttribute("drainAlphaExists", drainAlphaExists);
            request.setAttribute("skinAlphaExists", skinAlphaExists);
            request.setAttribute("burnAlphaExists", burnAlphaExists);
            request.setAttribute("just_alphas", justAlphas);
            request.setAttribute("just_alphas_obj", justAlphas_obj);
            PatientAccountVO patient = pBD.getPatient(Integer.parseInt(patient_id));
            LookupVO treatment = patient.getTreatmentLocation();

            request.setAttribute("product_list", productsBD.getAllProducts(userVO));//productsBD.findAllByCriteria(null, new Integer(0)));

            productCategoryVO.setActive(new Integer(1));
            request.setAttribute("category_list", productCategoryDAO.getAllProductCategoriesByCriteria(productCategoryVO));//findAllByCriteria(productCategoryVO));

            // TREATMENT MODALITIES
            ListServiceImpl listBD = new ListServiceImpl(language);
            request.setAttribute("dressing_change_frequency", listBD.getLists(LookupVO.DRESSING_CHANGE_FREQUENCY));
            request.setAttribute("nursing_visit_frequency", listBD.getLists(LookupVO.NURSING_VISIT_FREQUENCY));
            request.setAttribute("csresults_list", listBD.getLists(LookupVO.CNSRESULT));
            request.setAttribute("ccac_questions", listBD.getLists(LookupVO.CCAC_QUESTIONS));
            request.setAttribute("clinical_barriers", listBD.getLists(LookupVO.CLINICAL_BARRIERS));
            request.setAttribute("ivtherapy_barriers", listBD.getLists(LookupVO.IVTHERAPY_BARRIERS));
            request.setAttribute("adjunctive", listBD.getLists(LookupVO.ADJUNCTIVE));
            request.setAttribute("pressureReading", listBD.getLists(LookupVO.PRESSURE_READING));
            request.setAttribute("readingVar", listBD.getLists(LookupVO.READING_VAR));
            request.setAttribute("initiated_by", listBD.getLists(LookupVO.INITIATED_BY));
            request.setAttribute("goal_of_therapy", listBD.getLists(LookupVO.GOAL_OF_THERAPY));
            request.setAttribute("vendor_name", listBD.getLists(LookupVO.VENDOR_NAME));
            request.setAttribute("machine_acquirement", listBD.getLists(LookupVO.MACHINE_ACQUIREMENT));
            request.setAttribute("reason_for_ending", listBD.getLists(LookupVO.REASON_FOR_ENDING));
            request.setAttribute("np_connector", listBD.getLists(LookupVO.NP_CONNECTOR));
            request.setAttribute("bath_list", listBD.getLists(LookupVO.BATH));
            WoundAssessmentVO assessmentVO = null;

            WoundAssessmentVO currentWoundAssessment = null;
            if (assessment_id != null) {
                WoundAssessmentVO wat = new WoundAssessmentVO();
                wat.setId(new Integer(assessment_id));
                currentWoundAssessment = (WoundAssessmentVO) waDAO.getAssessment(wat);
            } else {
                if (wound_id != null && (wound_id).length() > 0) {
                    WoundAssessmentVO wat = new WoundAssessmentVO();
                    wat.setWound_id(new Integer(wound_id));

                    assessmentVO = (WoundAssessmentVO) waDAO.getAssessment(wat);
                }
            }
            if (currentWoundAssessment != null) {
                request.setAttribute("woundAssessment", currentWoundAssessment);

            } else if (assessmentVO != null) {
                request.setAttribute("woundAssessment", assessmentVO);

            }
            if (assessmentVO != null) {

                request.setAttribute("signature", assessmentVO.getUser_signature());
            }
            String latestAssessment = "0";
            if (assessmentVO != null) {
                latestAssessment = assessmentVO.getId() + "";
            }

            log.info("##$$##$$##-----POPULATE PREVIOUS PRODUCTS WAS SELECTED-----##$$##$$##");
            Integer eachVOT = null;

            Vector bandages = new Vector();

            // No repopulate, but get values for current assessments incase user filled out this form before
            Vector allCSResults = new Vector();
            Vector allCSValueResults = new Vector();
            Vector allDCFResults = new Vector();
            Vector allCCACQuestionsValueResults = new Vector();
            Vector allCCACQuestionsResults = new Vector();
            Vector allDCFValueResults = new Vector();

            boolean usingTemp = true;
            
            Hashtable required_hash = new Hashtable();
            //alpha array
            for (WoundAssessmentLocationVO currentAlpha : justAlphas_obj) {
                String alpha_type = currentAlpha.getWound_profile_type().getAlpha_type();
                AssessmentProductVO p = new AssessmentProductVO();
                p.setAlpha_id(currentAlpha.getId());
                p.setAssessment_id(new Integer(assessment_id));
                //get the appropriate alpha for the assesment to be passed to getProductsUnserialized.
                Collection<AssessmentProductVO> products = assessmentBD.getAllProducts(p);
                boolean reviewRequired = false;
                if ((products == null || products.size() == 0)) {
                    //Can't find any products as part of temp record.   Get last assessment products

                    AbstractAssessmentVO lastFullAssess = null;
                    AbstractAssessmentVO currentAssess = null;
                    AbstractAssessmentVO lastAssess = null;
                    if (alpha_type != null && alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {

                        AssessmentEachwoundVO a = new AssessmentEachwoundVO();
                        a.setAlpha_id(currentAlpha.getId());
                        lastFullAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), 1);
                        a = new AssessmentEachwoundVO();
                        a.setAlpha_id(currentAlpha.getId());
                        lastAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), -1);

                        a.setFull_assessment(null);
                        a.setAssessment_id(new Integer(assessment_id));
                        currentAssess = assessmentBD.getAssessment(a);
                        //check if bandages_out has been set to required.
                        ComponentsVO cp = new ComponentsVO();

                        cp.setField_name("bandages_in");

                        ComponentsVO comp_bandages = gui.getComponent(cp);

                        AssessmentEachwoundVO currRealAssess = (AssessmentEachwoundVO) currentAssess;
                        AssessmentEachwoundVO lastFullAssessObj = null;
                        if (lastFullAssess instanceof AssessmentEachwoundVO) {
                            lastFullAssessObj = (AssessmentEachwoundVO) lastFullAssess;
                        }

                        try {
                            boolean current = false;
                            boolean last = false;
                            if (currRealAssess != null && currRealAssess.getFull_assessment().equals(1) && (isDepthGreaterThanOne(currRealAssess) || isUnderminingDepthGreaterThanOne(currRealAssess.getUnderminings()) || isSinusDepthGreaterThanOne(currRealAssess.getSinustracts()))) {
                                current = true;
                            }
                            if ((current == false && currRealAssess != null && currRealAssess.getFull_assessment().equals(0)) && lastFullAssessObj != null && lastFullAssessObj.getFull_assessment().equals(1) && (isDepthGreaterThanOne(lastFullAssessObj) || isUnderminingDepthGreaterThanOne(lastFullAssessObj.getUnderminings()) || isSinusDepthGreaterThanOne(lastFullAssessObj.getSinustracts()))) {
                                last = true;
                            }
                            if (comp_bandages != null && comp_bandages.getRequired() == 1 && (current == true || last == true)) {
                                required_hash.put(currentAssess.getAlpha_id(), "bandage_required");
                            }
                        } catch (NullPointerException exe) {
                            System.out.println("bandages :" + comp_bandages.getRequired() + " " + currRealAssess + " " + lastFullAssessObj);
                            exe.printStackTrace();
                        }

                    } else if (alpha_type != null && alpha_type.equals(Constants.OSTOMY_PROFILE_TYPE)) {
                        AssessmentOstomyVO a = new AssessmentOstomyVO();
                        a.setAlpha_id(currentAlpha.getId());
                        lastAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), -1);
                        //get current assess
                        a.setAssessment_id(new Integer(assessment_id));
                        currentAssess = assessmentBD.getAssessment(a);

                    } else if (alpha_type != null && alpha_type.equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                        AssessmentIncisionVO a = new AssessmentIncisionVO();
                        a.setAlpha_id(currentAlpha.getId());
                        lastAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), -1);
                        //get current assess
                        a.setAssessment_id(new Integer(assessment_id));
                        currentAssess = assessmentBD.getAssessment(a);

                    } else if (alpha_type != null && alpha_type.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                        AssessmentDrainVO a = new AssessmentDrainVO();
                        a.setAlpha_id(currentAlpha.getId());
                        lastAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), -1);
                        //get current assess
                        a.setAssessment_id(new Integer(assessment_id));
                        currentAssess = assessmentBD.getAssessment(a);

                    } else if (alpha_type != null && alpha_type.equals(Constants.BURN_PROFILE_TYPE)) {

                        AssessmentBurnVO a = new AssessmentBurnVO();
                        a.setAlpha_id(currentAlpha.getId());
                        lastAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), -1);
                        //get current assess
                        a.setAssessment_id(new Integer(assessment_id));
                        currentAssess = assessmentBD.getAssessment(a);

                    } else if (alpha_type != null && alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {

                        AssessmentSkinVO a = new AssessmentSkinVO();
                        a.setAlpha_id(currentAlpha.getId());
                        lastAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), -1);
                        //get current assess
                        a.setAssessment_id(new Integer(assessment_id));
                        currentAssess = assessmentBD.getAssessment(a);
                    }

                    if (lastAssess != null) {

                        if (Common.getConfig("hideClosedAlphasOnTreatment").equals("0") || currentAssess != null && currentAssess.getStatus() != null && !((currentAssess instanceof AssessmentIncisionVO && Integer.toString(currentAssess.getStatus()).equals(Common.getConfig("incisionClosed"))) || (currentAssess instanceof AssessmentOstomyVO && Integer.toString(currentAssess.getStatus()).equals(Common.getConfig("ostomyClosed"))) || (currentAssess instanceof AssessmentEachwoundVO && Integer.toString(currentAssess.getStatus()).equals(Common.getConfig("woundClosed"))) || (currentAssess instanceof AssessmentDrainVO && Integer.toString(currentAssess.getStatus()).equals(Common.getConfig("drainClosed"))) || (currentAssess instanceof AssessmentBurnVO && Integer.toString(currentAssess.getStatus()).equals(Common.getConfig("burnClosed"))))) {
                            if (!Common.getConfig("disablePopProducts").equals("1")) {//if disable products is turned on, don't show
                                p = new AssessmentProductVO();
                                p.setAlpha_id(currentAlpha.getId());
                                p.setAssessment_id(lastAssess.getAssessment_id());
                                if (currentAssess != null && (currentAssess.getFull_assessment() == null || (currentAssess.getFull_assessment() != 2 && currentAssess.getFull_assessment() != 3))) {
                                    //get the appropriate alpha for the assesment to be passed to getProductsUnserialized.
                                    products = assessmentBD.getAllProducts(p);
                                    reviewRequired = true;
                                }
                            }
                        }
                    }
                } else {

                    //if we saved products already, then product changed, no need to thrown/check for warning.
                    request.setAttribute("product_changed", "true");
                    if (currentAlpha.getWound_profile_type().getAlpha_type().equals(Constants.WOUND_PROFILE_TYPE)) {
                        AssessmentEachwoundVO a = new AssessmentEachwoundVO();
                        a.setAlpha_id(currentAlpha.getId());
                        a.setAssessment_id(new Integer(assessment_id));
                        AbstractAssessmentVO ass = assessmentBD.getAssessment(a);
                        a = new AssessmentEachwoundVO();
                        a.setAlpha_id(currentAlpha.getId());
                        AbstractAssessmentVO lastAssess = assessmentBD.retrieveLastActiveAssessment(a, currentAlpha.getWound_profile_type().getAlpha_type(), 1);

                        AssessmentEachwoundVO lastAssessObj = (AssessmentEachwoundVO) lastAssess;
                        //check if bandages_out has been set to required.
                        ComponentsVO cp = new ComponentsVO();
                        cp.setField_name("bandages_in");
                        ComponentsVO comp_bandages = gui.getComponent(cp);
                        AssessmentEachwoundVO currAssess = (AssessmentEachwoundVO) ass;
                        boolean current = false;
                        boolean last = false;
                        if (currAssess != null && currAssess.getFull_assessment().equals(1) && (isDepthGreaterThanOne(currAssess) || isUnderminingDepthGreaterThanOne(currAssess.getUnderminings()) || isSinusDepthGreaterThanOne(currAssess.getSinustracts()))) {
                            current = true;
                        }
                        if ((current == false && currAssess != null && !currAssess.getFull_assessment().equals(1)) && lastAssessObj != null && lastAssessObj.getFull_assessment().equals(1) && (isDepthGreaterThanOne(lastAssessObj) || isUnderminingDepthGreaterThanOne(lastAssessObj.getUnderminings()) || isSinusDepthGreaterThanOne(lastAssessObj.getSinustracts()))) {
                            last = true;
                        }
                        if (comp_bandages != null && comp_bandages.getRequired() == 1 && (current == true || last == true)) {
                            required_hash.put(currAssess.getAlpha_id(), "bandage_required");
                        }
                        if (ass != null && (ass.getFull_assessment() == null || (ass.getFull_assessment() != 2 && ass.getFull_assessment() != 3))) {
                            reviewRequired = true;
                        }
                    }
                }
                request.setAttribute("reviewRequired", reviewRequired);
                for (AssessmentProductVO product : products) {

                    String display_alpha = currentAlpha.getAlpha();

                    display_alpha = Common.getAlphaText(display_alpha, locale);

                    ProductsVO item = product.getProduct();

                    if (product.getProduct_id().equals(new Integer("-1"))) {

                        currentProducts.addElement(product.getQuantity() + ":" + display_alpha + ":" + product.getOther());

                        currentProductsId.addElement(product.getQuantity() + "|" + product.getAlpha_id() + "|" + product.getOther());

                    } else if(item!=null){

                        currentProducts.addElement(product.getQuantity() + ":" + display_alpha + ":" + item.getTitle());

                        currentProductsId.addElement(product.getQuantity() + "|" + product.getAlpha_id() + "|" + product.getProduct_id());

                    }

                }

                //}
                //}
                request.setAttribute("historyWithIds", currentProductsId);

                request.setAttribute("history", currentProducts);
                NursingCarePlanVO ncp = new NursingCarePlanVO();
                ncp.setWound_id(new Integer(wound_id));
                ncp.setWound_profile_type_id(currentAlpha.getWound_profile_type_id());
                ncp.setDeleted(0);
                NursingCarePlanVO nncp = nBD.getNursingCarePlan(ncp);

                TreatmentCommentVO tc = new TreatmentCommentVO();
                tc.setWound_id(new Integer(wound_id));
                tc.setWound_profile_type_id(currentAlpha.getWound_profile_type_id());
                tc.setDeleted(0);
                if (Common.getConfig("repopulateTreatmentComments").equals("0")) {
                    tc.setAssessment_id(Integer.parseInt(assessment_id));
                }
                TreatmentCommentVO ntc = tBD.getTreatmentComment(tc);
                String prepend = "";

                if (currentAlpha.getWound_profile_type().getAlpha_type().equals(Constants.WOUND_PROFILE_TYPE)) {
                    prepend = "wound_";
                } else if (currentAlpha.getWound_profile_type().getAlpha_type().equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                    prepend = "incision_";
                } else if (currentAlpha.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                    prepend = "drain_";
                } else if (currentAlpha.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE)) {
                    prepend = "skin_";
                } else if (currentAlpha.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE)) {
                    prepend = "ostomy_";
                } else if (currentAlpha.getWound_profile_type().getAlpha_type().equals(Constants.BURN_PROFILE_TYPE)) {
                    prepend = "burn_";
                }
                DressingChangeFrequencyVO dcf = new DressingChangeFrequencyVO();
                dcf.setAlpha_id(currentAlpha.getId());

                //get last dcf for alpha
                DressingChangeFrequencyVO newDCF = nBD.getDressingChangeFrequency(dcf);
                if (newDCF != null) {
                    LookupVO l = listBD.getListItem(newDCF.getDcf());

                    allDCFResults.add(Common.getAlphaText(newDCF.getAlphaLocation().getAlpha(), locale) + ": " + l.getName(language));
                    allDCFValueResults.add(Common.getAlphaText(newDCF.getAlphaLocation().getAlpha(), locale) + ": " + newDCF.getDcf());

                }

                if (nncp != null) {
                    if (nncp.getVisit_frequency() != null && nncp.getVisit_frequency() != 0) {
                        request.setAttribute("visit_frequency", nncp.getVisit_frequency());
                    }
                    if (nncp.getAs_needed() != null && nncp.getAs_needed() > 0) {
                        request.setAttribute("as_needed", nncp.getAs_needed());
                    }
                    request.setAttribute(prepend + "nursing_care_plan", Common.convertCarriageReturns(nncp.getBody(), true));
                    request.setAttribute(prepend + "ncp_questions", nncp);
                }
                if (ntc != null) {

                    if (Common.getConfig("repopulateTreatmentComments").equals("1") || usingTemp == true) {
                        request.setAttribute(prepend + "treatment_comments", Common.convertCarriageReturns(ntc.getBody(), true));
                    } else {
                        request.setAttribute(prepend + "treatment_comments", "");
                    }

                }

                request.setAttribute("unserializedDCFresults", allDCFResults);
                request.setAttribute("unserializedDCFValueresults", allDCFValueResults);
            }
            request.setAttribute("bandages_required", required_hash);
            //end of alpha array.

            AssessmentTreatmentVO t = new AssessmentTreatmentVO();

            t.setAssessment_id(new Integer(assessment_id));

            Collection<AssessmentTreatmentVO> currentTreatments = tBD.getAllAssessmentTreatment(t);
            if (currentTreatments != null && currentTreatments.size() > 0) {
                request.setAttribute("current_treatments", currentTreatments);
            }

            //We need to retrieve the last assessments treatments
            Collection items = new ArrayList();
            for (WoundAssessmentLocationVO alphaWound : justAlphas_obj) {
                AssessmentEachwoundVO at = new AssessmentEachwoundVO();
                at.setAlpha_id(alphaWound.getId());
                at.setActive(1);
                AssessmentEachwoundVO lastAssess = (AssessmentEachwoundVO) assessmentBD.getAssessment(at, false);
                if (lastAssess != null) {
                    AssessmentTreatmentVO a1 = new AssessmentTreatmentVO();
                    a1.setAlpha_id(new Integer(alphaWound.getId()));
                    a1.setAssessment_id(lastAssess.getAssessment_id());
                    AssessmentTreatmentVO a = tBD.getAssessmentTreatment(a1);

                    if (a != null) {
                        //clear review_done
                        a.setReview_done(null);
                        items.add(a);
                    }
                }

            }
            if (items != null && items.size() > 0) {
                request.setAttribute("last_treatments", items);
            }
            if (currentTreatments == null || currentTreatments.size() == 0) {
                currentTreatments = items;
            }
            AssessmentTreatmentVO ttmp = new AssessmentTreatmentVO();
            ttmp.setWound_id(new Integer(wound_id));

            AssessmentTreatmentVO lastTreatment = tBD.getAssessmentTreatment(ttmp);

            List<String> antis = new ArrayList();
            if (currentTreatments != null && currentTreatments.size() > 0) {
                WoundAssessmentVO wat = new WoundAssessmentVO();
                wat.setWound_id(new Integer(wound_id));

                for (AssessmentTreatmentVO currentAssessment : currentTreatments) {

                    if (lastTreatment == null || currentAssessment.getId() > lastTreatment.getId()) {
                        lastTreatment = currentAssessment;
                    }

                    //get each record, and compile into 1 array.
                    //tarray.setResource_id(LookupVO.CNSRESULT);
                    Collection<ArrayValueObject> array = currentAssessment.getAssessment_treatment_arrays();
                    //System.out.println("Array Size: " + array.size());
                    request.setAttribute("unserializeAdjunctive", Common.filterArrayObjects(array.toArray(new AssessmentTreatmentArraysVO[array.size()]), LookupVO.ADJUNCTIVE));
                    for (ArrayValueObject each : array) {
                        if (each.getResource_id().equals(LookupVO.CNSRESULT)) {
                            allCSValueResults.add(Common.getAlphaText(currentAssessment.getAlphaLocation().getAlpha(), locale) + ": " + each.getLookup_id());
                            allCSResults.add(Common.getAlphaText(currentAssessment.getAlphaLocation().getAlpha(), locale) + ": " + each.getLookup().getName(language));
                        }
                    }
                    for (ArrayValueObject each : array) {
                        if (each.getResource_id().equals(LookupVO.CCAC_QUESTIONS)) {
                            allCCACQuestionsValueResults.add(Common.getAlphaText(currentAssessment.getAlphaLocation().getAlpha(), locale) + ": " + each.getLookup_id());
                            allCCACQuestionsResults.add(Common.getAlphaText(currentAssessment.getAlphaLocation().getAlpha(), locale) + ": " + each.getLookup().getName(language));
                        }
                    }

                    request.setAttribute("unserializedCsresults", allCSResults);
                    request.setAttribute("unserializedCsValueresults", allCSValueResults);
                    request.setAttribute("unserializedCCACValueresults", allCCACQuestionsValueResults);
                    request.setAttribute("unserializedCCACresults", allCCACQuestionsResults);
                    if (request.getParameter("category_select") != null && request.getParameter("category_select") != "") {
                    }
                }

            }
            if (lastTreatment != null) {
                Collection<AssessmentAntibioticVO> antibiotics = lastTreatment.getAntibiotics();
                SimpleDateFormat sf = new SimpleDateFormat("d/M/yyyy");
                for (AssessmentAntibioticVO a : antibiotics) {
                    antis.add(a.getPrintable(locale));
                }
                //if this treatment isn't from current session, clear out review_done
                if (!lastTreatment.getAssessment_id().equals(new Integer(assessment_id))) {
                    lastTreatment.setReview_done(null);
                }
                request.setAttribute("treatment", lastTreatment);
            }
            request.setAttribute("unserializeAntibiotics", antis);
            
            if (Common.getConfig("useTriggers").equals("1") && Common.isOffline() == false) {
                // Verify CCAC and CS_results for Triggers
                if (!allCCACQuestionsValueResults.isEmpty() || !allCSValueResults.isEmpty()){
                    tservice.processTreatmentTriggers(allCSValueResults, allCCACQuestionsValueResults, alpha);
                }
                
                List<ProductTrigger> product_recommendations = tservice.getRecommendations(wound_name);
                String serializedTriggers = "";
                if(product_recommendations!=null && product_recommendations.size()>0){
                    //log that a trigger was thrown.
                    LogAuditServiceImpl bd = new LogAuditServiceImpl();
                    Map<Integer, Boolean> trigs = tservice.getAppliedTriggers();
                   Iterator it = trigs.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry)it.next();
                        try{
                            //if trigger is applied, add to log, and front-end
                            if(trigs.get(pair.getKey())){
                                LogAuditVO vo = new LogAuditVO(userVO.getId(), new Integer(patient_id), Constants.TRIGGER_GEN, "triggers",new  Integer(pair.getKey()+""));
                                bd.saveLogAudit(vo);
                                serializedTriggers = serializedTriggers +pair.getKey()+":";
                            }
                        }catch(Exception e){}
                    }
                }
                request.setAttribute("triggers",serializedTriggers);
                request.setAttribute("prod_recommendations", product_recommendations);
            }

        } catch (ApplicationException e) {
            e.printStackTrace();
            log.error("An application exception has been raised in TreatmentSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(
                    request, errors);

            return (mapping.findForward("pixalere.error"));

        } catch (Exception e) {
            e.printStackTrace();

        }
        log.info("**** TreatmentSetupAction.perform: End of Treatment");

        return (mapping.findForward("uploader.treatment.success"));

    }

    private boolean isSinusDepthGreaterThanOne(Collection<AssessmentSinustractsVO> sinus) {
        try {
            if (sinus == null) {
                return false;
            } else {
                for (AssessmentSinustractsVO s : sinus) {
                    if (s == null) {
                        return false;
                    } else if ((s.getDepth_cm() == 1 && s.getDepth_mm() != null && s.getDepth_mm() >= 1) || s.getDepth_cm() > 1) {
                        return true;
                    }
                }
            }
        } catch (NullPointerException e) {
        }
        return false;
    }

    private boolean isDepthGreaterThanOne(AssessmentEachwoundVO s) {
        try {
            if (s == null) {
                return false;
            } else if ((s.getDepth() > 1)) {
                return true;
            }
        } catch (NullPointerException e) {
        }
        return false;
    }

    private boolean isUnderminingDepthGreaterThanOne(Collection<AssessmentUnderminingVO> under) {
        try {
            if (under == null) {
                return false;
            } else {
                for (AssessmentUnderminingVO s : under) {
                    if (s == null) {
                        return false;
                    } else if ((s.getDepth_cm() == 1 && s.getDepth_mm() != null && s.getDepth_mm() >= 1) || s.getDepth_cm() > 1) {
                        return true;
                    }
                }
            }
        } catch (NullPointerException e) {
        }
        return false;
    }
}
