/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.auth.bean.ProfessionalVO;
public class ImportProductsForm extends ActionForm{
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImportProductsForm.class);
    private FormFile products;
    private Integer import_type;
    private String category_name;
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        
        ActionErrors errors = new ActionErrors();
        if (products!=null && products.getFileName().length()>0 && products.getFileSize() == 0) {
            errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
        }

        return errors;
    }
    public void setProducts(FormFile products){
        this.products=products;
    }
    public FormFile getProducts(){
        return products;
    }
    /**
     * @return the import_type
     */
    public Integer getImport_type() {
        return import_type;
    }
    /**
     * @param import_type the import_type to set
     */
    public void setImport_type(Integer import_type) {
        this.import_type = import_type;
    }
    /**
     * @return the category_name
     */
    public String getCategory_name() {
        return category_name;
    }
    /**
     * @param category_name the category_name to set
     */
    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
