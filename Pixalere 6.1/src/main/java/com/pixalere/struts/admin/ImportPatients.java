/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import java.sql.Timestamp;
import java.util.Date;
import com.pixalere.utils.excel.ExcelReader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.Collection;
import org.apache.struts.upload.FormFile;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.PDate;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.xml.ProductHandler;
import com.pixalere.utils.xml.XMLReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.pixalere.patient.bean.PatientAccountVO;
import java.util.Vector;
import com.pixalere.common.ApplicationException;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.util.Hashtable;

/**
 * Import Patients save class. This class validates, and imports the XML
 *
 * @since 6.1
 * @author travis
 */
public class ImportPatients extends Action {

    static private Logger logs = Logger.getLogger(ImportPatients.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ImportPatientsForm productsForm = (ImportPatientsForm) form;
        FormFile patients = productsForm.getPatients();
        System.out.println("file" + patients);
        if (request.getParameter("action") != null && ((String) request.getParameter("action")).equals("import_patients")) {
            PatientServiceImpl pservice = new PatientServiceImpl();

            if (patients != null) {

                try {
                    String validation = Common.getConfig("integration_validation");

                    ExcelReader r = new ExcelReader();
                    Vector<PatientAccountVO> prodVect = r.readPatients(patients.getInputStream());
                    System.out.println("patients: " + prodVect.size());
                    for (PatientAccountVO p : prodVect) {
                        //check if patient exists.
                        Hashtable result = pservice.getPatient(p.getPhn(), p.getPhn2(), validation);
                        PatientAccountVO foundPatient = (PatientAccountVO) result.get("object");
                        if (foundPatient == null) {
                            if (p.getPhn() != null) {
                                try {
                                    boolean valid = pservice.validatePHN(p.getPhn());
                                    if (p.getPhn() != null && valid == true) {
                                        p.setOutofprovince(0);
                                    } else {
                                        String error = "Invalid PHN: ";
                                        String message = error + " " + p.getPhn() + " ID: " + p.getPhn2() + "\n\n";
                                        System.out.println(message);
                                        p.setOutofprovince(1);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    p.setOutofprovince(1);
                                }

                            }
                            //save new patient
                            p.setLastName_search(Common.stripName(p.getLastName()));
                            p.setAccount_status(1);
                            p.setCurrent_flag(1);
                            p.setCreated_by(6);
                            p.setUser_signature(new PDate().getDateStringWithHour(new Date(), "en") + " by Excel Import");
                            int ti = Constants.NO_TREATMENT_LOCATION;//legacy need to remove
                            try {
                                ti = Integer.parseInt(Common.getConfig("noTreatmentListID"));
                            } catch (NumberFormatException e) {
                            }
                            p.setTreatment_location_id(ti);//We don't know where they belong, so they go in no Treatment Location to force a move

                            pservice.updatePatient(p);//creates patient without patient_id
                        } else {
                            //update old one
                            // we found a patient who already exists.. so update it.
                            if (p.getPhn() != null) {
                                try {
                                    boolean valid = pservice.validatePHN(p.getPhn());
                                    if (p.getPhn() != null && valid == true) {
                                        p.setOutofprovince(0);
                                    } else {
                                        String error = "Invalid PHN: ";
                                        String message = error + " " + p.getPhn() + " ID: " + p.getPhn2() + "\n\n";
                                        System.out.println(message);
                                        p.setOutofprovince(1);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    p.setOutofprovince(1);
                                }

                            }
                            if (p.getDischarge_date() != null) {
                                p.setAccount_status(0);
                                if (foundPatient.getDischarge_date() == null) {
                                    p.setAction("DISCHARGED");
                                }
                            } else if (p.getDischarge_date() == null) {
                                p.setCreated_on(new Timestamp(new Date().getTime()));
                                p.setAccount_status(1);
                                if (foundPatient.getDischarge_date() != null) {
                                    p.setAction("ADMITTED");
                                }
                            } else {
                                p.setAccount_status(foundPatient.getAccount_status());
                                p.setAction(foundPatient.getAction());
                            }
                            p.setPatient_id(foundPatient.getPatient_id());

                            p.setLastName_search(Common.stripName(p.getLastName()));

                            p.setCurrent_flag(1);
                            p.setCreated_by(6);
                            p.setUser_signature(new PDate().getDateStringWithHour(new Date(), "en") + " by Excel Import");
                            if (foundPatient.getPatient_id() == null) {
                                p.setId(foundPatient.getId());
                                pservice.updatePatient(p);
                            } else {
                                pservice.savePatient(p);
                            }
                        }
                        
                    }
                } catch (FileNotFoundException e) {
                    logs.error("Unable to find Product file " + e.getMessage());
                } catch (IOException e) {
                    logs.error("Unable to read Product file " + e.getMessage());
                } catch (ApplicationException e) {
                    logs.error("Unable to save Product file " + e.getMessage());
                }

            }
        } else {
        }
        return mapping.findForward("admin.importpatients.success");
    }
}
