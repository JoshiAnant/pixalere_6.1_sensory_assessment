/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Common;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.Constants;
import java.util.Date;
/**
 * A PatientActivate class for activating patients.
 * 
 * @since 5.0
 * @author travis
 */
public class PatientActivate extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientActivate.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
            
        Integer language = Common.getLanguageIdFromSession(session);

        
        try {    
            if (session.getAttribute("patient_id") == null && session.getAttribute("admin_patient_id") == null ) {
             
                ActionErrors errors = new ActionErrors();
                errors.add("exception", new ActionError("pixalere.patient_id.null"));
                saveErrors(request, errors);
                return (mapping.findForward("admin.patient"));
            }
            String locale = Common.getLanguageLocale(language);
            
            int patient_id=-1;
            if(session.getAttribute("patient_id")!=null){
                patient_id=Integer.parseInt((String)session.getAttribute("patient_id"));
            }else if(session.getAttribute("admin_patient_id")!=null){
                patient_id=Integer.parseInt((String)session.getAttribute("admin_patient_id"));
                
            }
            PDate pdate = new PDate();
            PatientServiceImpl managerBD = new PatientServiceImpl(language);
            PatientAccountVO accountVO = new PatientAccountVO();
            accountVO.setPatient_id(patient_id);
            accountVO.setCurrent_flag(1);
            PatientAccountVO p = managerBD.getPatient(accountVO);
         
            if(p!=null){
                ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
                if(request.getParameter("treatmentLocation")!=null && !request.getParameter("treatmentLocation").equals("")){
                    p.setTreatment_location_id(Integer.parseInt((String)request.getParameter("treatmentLocation")));
                }
                p.setAccount_status(1);
                p.setAction(Constants.ADMIT);
                p.setDischarge_date(null);
                p.setDischarge_reason(null);
                p.setCreated_by(userVO.getId());
                p.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                managerBD.savePatient(p);
                if(session.getAttribute("admin_patient_id")!=null){
                    session.setAttribute("patient_id",session.getAttribute("admin_patient_id"));
                    session.removeAttribute("admin_patient_id");
                }
            }
            PatientAccountVO patient = managerBD.getPatient(patient_id);
            session.setAttribute("patientAccount",patient);
        } catch (Exception e) {
            System.out.println("error in pa:" + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("admin.patient.success"));
    }
}
