/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.LibraryVO;
import org.apache.struts.upload.FormFile;
/**
 *
 * @author travis
 */
public class LicenseForm  extends ActionForm{
    private String license;
    public LicenseForm(){}
    public String getLicense(){
        return license;
    }
    public void setLicense(String license){
        this.license=license;
    }
}
