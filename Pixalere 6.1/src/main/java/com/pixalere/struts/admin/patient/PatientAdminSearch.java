package com.pixalere.struts.admin.patient;

import com.pixalere.guibeans.PatientList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Vector;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 * A {@link org.apache.struts.action.Action} action which search's for patients
 *
 * @since 5.0
 * @author travis morris
 */
public class PatientAdminSearch extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientAdminSearch.class);
    /**
     * Overriding execute.
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PatientAdminSearchForm patientAdminForm = (PatientAdminSearchForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        try {

            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            PatientServiceImpl patientDAO = new PatientServiceImpl(language);
            String locale = Common.getLanguageLocale(language);

            Vector<PatientList> patients = new Vector();
            PatientAccountVO accVO = new PatientAccountVO();
            request.setAttribute("action", request.getParameter("action"));
            //should we include inactive patients?
            boolean includeInactive = true;
            if (patientAdminForm.getTreatmentregion() == -1) {
                patientAdminForm.setTreatmentlocationid(-1);
            }
            //combine phn values
            String phn = "";
            if (!patientAdminForm.getPhn1_1().equals("") ) {
                phn = patientAdminForm.getPhn1_1() + (patientAdminForm.getPhn1_2()!=null?patientAdminForm.getPhn1_2():"" )+ (patientAdminForm.getPhn1_3()!=null?patientAdminForm.getPhn1_3():"" );
            }
            System.out.println("phn"+phn);
            //Get patients by search criteria
            if (patientAdminForm.getId() > 0 || (patientAdminForm.getLastNameSearch()!=null && !Common.stripName(patientAdminForm.getLastNameSearch()).equals(""))
                    || (phn!=null && !phn.equals("")) || (patientAdminForm.getTreatmentlocationid() != 0)
                    || (patientAdminForm.getOtherSearch()!=null && !patientAdminForm.getOtherSearch().equals("")) || (patientAdminForm.getIdSearch() != 0)
                    || (patientAdminForm.getParisSearch()!=null && !patientAdminForm.getParisSearch().equals("")) || (patientAdminForm.getMrnSearch()!=null && !patientAdminForm.getMrnSearch().equals(""))) {
                Common.invalidatePatientSession(session);
                System.out.println("Lastname: "+Common.stripName(patientAdminForm.getLastNameSearch()));
                patients = patientDAO.findAllBySearch(
                        patientAdminForm.getId(),
                        ((patientAdminForm.getLastNameSearch()!=null && !patientAdminForm.getLastNameSearch().equals("")) ? Common.stripName(patientAdminForm.getLastNameSearch()) : ""),
                        userVO,
                        ((phn!=null && !phn.equals("")) ? accVO.getEncrypted(Common.stripPHN(phn), "pass4phn") : ""),
                        patientAdminForm.getTreatmentlocationid(),
                        ((patientAdminForm.getOtherSearch()!=null && !patientAdminForm.getOtherSearch().equals("")) ? accVO.getEncrypted(patientAdminForm.getOtherSearch(), "pass4phn") : ""),
                        ((patientAdminForm.getParisSearch()!=null && !patientAdminForm.getParisSearch().equals("")) ? accVO.getEncrypted(patientAdminForm.getParisSearch(), "pass4phn") : ""),
                        ((patientAdminForm.getMrnSearch()!=null && !patientAdminForm.getMrnSearch().equals("")) ? accVO.getEncrypted(patientAdminForm.getMrnSearch(), "pass4phn") : ""),
                        includeInactive, language);
                try {
                    if ((patientAdminForm.getLastNameSearch() != null && !patientAdminForm.getLastNameSearch().equals("")) && (patientAdminForm.getLastNameSearch().toLowerCase()).indexOf(userVO.getLastname().toLowerCase()) != -1) {
                        com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
                        logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(userVO.getId(), -1, com.pixalere.utils.Constants.SEARCH_PATIENT, "patient_accounts"));
                        if (Common.getConfig("logAuditEmails") != null && !Common.getConfig("logAuditEmails").equals("")) {
                            try {
                                String emails = Common.getConfig("logAuditEmails");
                                com.pixalere.mail.SendMailUsingAuthentication sm = new com.pixalere.mail.SendMailUsingAuthentication();
                                sm.postMail(emails, "Pixalere Nofication: Same Last Name Search", userVO.getFullName() + " (User ID: " + userVO.getId() + ") searched for a lastname that is equal to or similar to their own: " + patientAdminForm.getLastNameSearch(), "");
                            } catch (Exception e) {
                                log.error("Error Sending Notification Email: ", e);
                            }
                        }
                    }
                } catch (Exception e2) {
                    log.error("Error Sending Notification Email: ", e2);
                }
                if (patients.size() == 0 && !Common.stripPHN(phn).equals("")) {
                    // Couldn't find patient while searching by PHN, now try with stripped PHN (incase there were spaces in it)
                    String strippedSearchPhn = "";
                    String strippedSearchOther = "";
                    if (!phn.equals("")) {
                        strippedSearchPhn = phn;
                    }
                    if (!patientAdminForm.getOtherSearch().equals("")) {
                        strippedSearchOther = patientAdminForm.getOtherSearch();
                    }
                    //patients = patientDAO.findAllBySearch(0, "", userVO, accVO.getEncrypted(Common.stripPHN(patientAdminForm.getPhnSearch()),"pass4phn"), patientAdminForm.getTreatmentlocationid(),includeInactive);
                    patients = patientDAO.findAllBySearch(patientAdminForm.getIdSearch(), "", userVO, accVO.getEncrypted(Common.stripPHN(strippedSearchPhn), "pass4phn"), patientAdminForm.getTreatmentlocationid(), accVO.getEncrypted(Common.stripPHN(strippedSearchOther), "pass4phn"), patientAdminForm.getParisSearch(), patientAdminForm.getMrnSearch(), includeInactive, language);
                }
            }
            //Search by patient ID
            /*if (session.getAttribute("patient_id") != null) {
             patients = patientDAO.findAllBySearch(new Integer((String) session.getAttribute("patient_id")).intValue(), "",
             userVO, "", patientAdminForm.getTreatmentlocationid(), "", "", "", 0, includeInactive);
             }*/
            
            if ((patients == null || patients.size() == 0) && !Common.getLocalizedString("pixalere.offline", "en").equals("1") ) {
                //redirect to create patient.
                if (Common.getConfig("parisInterface").equals("1")) {
                    request.setAttribute("parisMessage", Common.getLocalizedString("pixalere.admin.patientaccounts.form.checkParis", locale));
                    request.setAttribute("tab_select", "searchDiv");
                    // TODO: Do we need to validate patient consent here?
                    return (mapping.findForward("admin.patientsearch.success"));
                } else if(userVO.getAccess_create_patient()!=null && userVO.getAccess_create_patient().equals(new Integer(1))) {
                    request.setAttribute("tab_select","createDiv");
                    request.setAttribute("action","add");
                    return (mapping.findForward("create.patient.success"));
                }else{
                    //throw error:  NO access to create patient.
                    request.setAttribute("error",Common.getLocalizedString("pixalere.admin.patientaccounts.form.patient_not_found_no_create",locale));
                 
                }
            }
            //check if user is searching for patients with same/similiar last name
            if (patients != null) {
                for (PatientList patient : patients) {
                    try {
                        if (patient != null && (patient.getAccount().getLastName().toLowerCase()).indexOf(userVO.getLastname().toLowerCase()) != -1) {
                            com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
                            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(userVO.getId(), patient.getAccount().getPatient_id(), com.pixalere.utils.Constants.SEARCH_PATIENT, "patient_accounts", patient.getAccount()));
                        }
                    } catch (Exception e) {
                    }
                }
                request.setAttribute("patientList", patients);
            }
            PatientAccountVO patientVORelevant = new PatientAccountVO();
            patientVORelevant.setId(0);
            request.setAttribute("patientVORelevant", patientVORelevant);
//			Populate treatment list
            LookupVO vo = new LookupVO();
            ListServiceImpl bd = new ListServiceImpl(language);
            Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
            Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
            request.setAttribute("inactivate_reason", bd.getLists(vo.INACTIVATE_REASON));
            request.setAttribute("treatmentCats", treatmentCatsVO);
            request.setAttribute("treatmentList", treatmentVO);
            Vector treatmentVOList = bd.findAllTreatmentsWithCats(userVO);
            request.setAttribute("treatmentList2", treatmentVOList);
            request.setAttribute("gender", bd.getLists(vo.GENDER));
            request.setAttribute("finishedOperation", false);
            request.setAttribute("professionalAccount", userVO);
        } catch (Exception e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        log.debug("Leaving PatientAdminSearch ****************");
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            request.setAttribute("tab_select", "searchDiv");
            return (mapping.findForward("admin.patientsearch.success"));
        }
    }
}
