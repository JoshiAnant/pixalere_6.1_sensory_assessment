package com.pixalere.struts.admin;

import com.pixalere.auth.dao.UserDAO;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Vector;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.Common;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.upload.FormFile;
/**
 * @author
 *
 */
public class ProfessionalAdminForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfessionalAdminForm.class);
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        
        UserDAO dao = new UserDAO();
        try {
            if(request.getParameter("action")!=null && ((String) request.getParameter("action")).equals("import")){
                //ActionErrors errors = new ActionErrors();
                if (excel!=null && excel.getFileName().length()>0 && excel.getFileSize() == 0) {
                    errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
                }

                return errors;
            }else if (request.getParameter("action")!=null && !((String) request.getParameter("action")).equals("unassign")) {

                if (Common.getConfig("useUserNames").equals("1")) {
                    ProfessionalVO userVO2 = new ProfessionalVO();
                    userVO2.setUser_name(getUser_name());

                    // userVO.setAccount_status(new Integer(1));  // Search for inactive accounts too
                    if (user_name.trim().equals("")) {
                        errors.add("username2", new ActionError("pixalere.admin.professionalaccounts.form.usernamerequired"));
                    } else {
                        boolean blnIsNumeric = true;
                        try {
                            Integer.parseInt(user_name.trim());
                        } catch (NumberFormatException nfe) {
                            blnIsNumeric = false;
                        }
                        if (blnIsNumeric == true) {
                            errors.add("username2", new ActionError("pixalere.admin.professionalaccounts.form.usernameinvalid"));
                            request.setAttribute("errorAdd", "1");
                        }
                    }
                    /*if(user_name.trim().equals("")){
                     errors.add("username2", new ActionError("pixalere.admin.professionalaccounts.form.usernamerequired"));
                     } else {*/
                    ProfessionalVO tmp = (ProfessionalVO) dao.findByCriteria(userVO2);
                    if (tmp != null && request.getParameter("action").equals("edit") && !tmp.getId().equals(new Integer(getId()))) {
                        errors.add("username", new ActionError("pixalere.admin.professionalaccounts.form.usernameexists"));
                        request.setAttribute("errorAdd", "1");
                    } else if (tmp != null && request.getParameter("action").equals("add")) {
                        errors.add("username", new ActionError("pixalere.admin.professionalaccounts.form.usernameexists"));
                        request.setAttribute("errorAdd", "1");
                    }
                    //}
                }
            }else if(((String) request.getParameter("action")).equals("import")){
                //ActionErrors errors = new ActionErrors();
                if (excel!=null && excel.getFileName().length()>0 && excel.getFileSize() == 0) {
                    errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
                }

                return errors;
            }
            if (errors.size() > 0) {
                ProfessionalVO userVO = new ProfessionalVO();
                userVO.setTraining_flag((((getTraining_flag() == null) ? (new Integer(0)) : new Integer(1))));
                userVO.setPosition_id(getPosition());
                userVO.setUser_name(getUser_name().toLowerCase());
                userVO.setFirstname(getFirst_name());
                userVO.setLastname(getLast_name());
                userVO.setPager(getPager());
                userVO.setEmail(getEmail());
                userVO.setEmployeeid(getEmployeeid());
                userVO.setTimezone(timezone);
                userVO.setIndividuals(getAccess_admin_patients() + "");
               userVO.setAccess_ccac_reporting(getAccess_ccac_reporting()==1?new Integer(1):new Integer(0));
                userVO.setAccess_viewer((((getAccess_viewer() == null) ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_admin(((getAccess_admin() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_allpatients(((getAccess_allpatients() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_assigning(((getAccess_assigning() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_professionals(((getAccess_admin_professionals() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_patients(((getAccess_admin_patients() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_regions(((getAccess_admin_regions() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_referrals(((getAccess_admin_referrals() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_listdata(((getAccess_admin_listdata() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_audit(((getAccess_admin_audit() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_create_patient(getAccess_create_patient() == null ? new Integer(0) : new Integer(1));
                userVO.setNew_user(new Integer(1));
                userVO.setAccess_uploader(((getAccess_uploader() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_reporting(((getAccess_reporting() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_superadmin(((getAccess_superadmin() == null || getAccess_superadmin().equals("0") ? (new Integer(0)) : new Integer(1))));
                userVO.setLocked(0);
                userVO.setInvalid_password_count(0);
                request.setAttribute("action", request.getParameter("action"));
                request.setAttribute("professionalAccount", userVO);
                request.setAttribute("professionalId", request.getParameter("professionalId"));
                request.setAttribute("position", position);
                LookupVO vo = new LookupVO();
                com.pixalere.admin.service.ResourcesServiceImpl resourcesBD = new com.pixalere.admin.service.ResourcesServiceImpl();
                com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
                ProfessionalVO tuser = null;
                Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, tuser, true);
                Vector treatmentCatsVO = (Vector) resourcesBD.getAllResources(vo.TREATMENT_LOCATION);
                request.setAttribute("treatmentCats", treatmentCatsVO);
                request.setAttribute("treatmentList", treatmentVO);
                request.setAttribute("professionalAccountAdd",userVO);
            }
        } catch (com.pixalere.common.DataAccessException ex) {
            log.error("Exception has been caught: " + ex.getMessage());
        } catch (ApplicationException ex) {
            log.error("Exception has been caught: " + ex.getMessage());
        }
        return errors;
    }
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    public String getPager() {
        return pager;
    }
    public void setPager(String pager) {
        this.pager = pager;
    }
    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }
    public String getAccess_uploader() {
        return access_uploader;
    }
    public void setAccess_uploader(String access_uploader) {
        this.access_uploader = access_uploader;
    }
    public String getAccess_viewer() {
        return access_viewer;
    }
    public void setAccess_viewer(String access_viewer) {
        this.access_viewer = access_viewer;
    }
    public String getAccess_admin_patients() {
        return access_admin_patients;
    }
    public void setAccess_admin_patients(String access_admin_patients) {
        this.access_admin_patients = access_admin_patients;
    }
    public String getAccess_admin() {
        return access_admin;
    }
    public void setAccess_admin(String access_admin) {
        this.access_admin = access_admin;
    }
    public String getAccess_allpatients() {
        return access_allpatients;
    }
    public void setAccess_allpatients(String access_allpatients) {
        this.access_allpatients = access_allpatients;
    }
    public String getAccess_admin_regions() {
        return access_admin_regions;
    }
    public void setAccess_admin_regions(String access_admin_regions) {
        this.access_admin_regions = access_admin_regions;
    }
    public String getAccess_reporting() {
        return access_reporting;
    }
    public void setAccess_reporting(String access_reporting) {
        this.access_reporting = access_reporting;
    }
    public String getAccess_admin_referrals() {
        return access_admin_referrals;
    }
    public void setAccess_admin_referrals(String access_admin_referrals) {
        this.access_admin_referrals = access_admin_referrals;
    }
    public String getAccess_admin_listdata() {
        return access_admin_listdata;
    }
    public void setAccess_admin_listdata(String access_admin_listdata) {
        this.access_admin_listdata = access_admin_listdata;
    }
    public String getAccess_assigning() {
        return access_assigning;
    }
    public void setAccess_assigning(String access_assigning) {
        this.access_assigning = access_assigning;
    }
    public String getAccess_admin_audit() {
        return access_admin_audit;
    }
    public void setAccess_admin_audit(String access_admin_audit) {
        this.access_admin_audit = access_admin_audit;
    }
    public String getAccess_products() {
        return access_products;
    }
    public void setAccess_products(String access_products) {
        this.access_products = access_products;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getAccess_admin_professionals() {
        return access_admin_professionals;
    }
    public void setAccess_admin_professionals(String access_admin_professionals) {
        this.access_admin_professionals = access_admin_professionals;
    }
    public String getLast_name() {
        return last_name;
    }
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
    public String getFirst_name() {
        return first_name;
    }
    public Integer[] getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(Integer[] treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    public String getPassword() {
        return password;
    }
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public void setAccount_status(int account_status) {
        this.account_status = account_status;
    }
    public int getAccount_status() {
        return account_status;
    }
    private FormFile excel;
    private String training_flag;
    private String employeeid;
    private int id;
    private int account_status;
    private String password;
    private Integer[] treatmentLocation;
    private String first_name;
    private String access_superadmin;
    private String last_name;
    private Integer patient_id;
    private Integer patient_id_edit;
    private String email;
    private int access_ccac_reporting;
    private String timezone;
    private String pager;
    private int position;
    private String access_uploader;
    private String access_viewer;
    private String access_admin_patients;
    private String access_admin;
    private String access_allpatients;
    private String access_admin_regions;
    private String access_reporting;
    private Integer access_create_patient;
    private String access_admin_referrals;
    private String access_admin_listdata;
    private String access_assigning;
    private String access_admin_audit;
    private String access_products;
    private String access_admin_professionals;
    private String user_name;
    private Integer usepatient_timezone;
    private Integer question_one;
    private Integer question_two;
    private String answer_one;
    private String answer_two;
    private Integer allow_remote_access;
    private Integer reset_password_method;
    /**
     * @method getUser_name
     * @field user_name - String
     * @return Returns the user_name.
     */
    public String getUser_name() {
        return user_name;
    }
public int getAccess_ccac_reporting() {
        return access_ccac_reporting;
    }
 
    public void setAccess_ccac_reporting(int access_ccac_reporting) {
        this.access_ccac_reporting = access_ccac_reporting;
    }
    /**
     * @method setUser_name
     * @field user_name - String
     * @param user_name The user_name to set.
     */
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
    public String getAccess_superadmin() {
        return access_superadmin;
    }
    public void setAccess_superadmin(String access_superadmin) {
        this.access_superadmin = access_superadmin;
    }
    public String getTraining_flag() {
        return training_flag;
    }
    public void setTraining_flag(String training_flag) {
        this.training_flag = training_flag;
    }
    /**
     * @return the timezone
     */
    public String getTimezone() {
        return timezone;
    }
    /**
     * @param timezone the timezone to set
     */
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
    public Integer getAccess_create_patient() {
        return access_create_patient;
    }
    public void setAccess_create_patient(Integer access_create_patient) {
        this.access_create_patient = access_create_patient;
    }

    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }
    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    /**
     * @return the patient_id_edit
     */
    public Integer getPatient_id_edit() {
        return patient_id_edit;
    }
    /**
     * @param patient_id_edit the patient_id_edit to set
     */
    public void setPatient_id_edit(Integer patient_id_edit) {
        this.patient_id_edit = patient_id_edit;
    }

    /**
     * @return the question_one
     */
    public Integer getQuestion_one() {
        return question_one;
    }

    /**
     * @param question_one the question_one to set
     */
    public void setQuestion_one(Integer question_one) {
        this.question_one = question_one;
    }

    /**
     * @return the question_two
     */
    public Integer getQuestion_two() {
        return question_two;
    }

    /**
     * @param question_two the question_two to set
     */
    public void setQuestion_two(Integer question_two) {
        this.question_two = question_two;
    }

    /**
     * @return the answer_one
     */
    public String getAnswer_one() {
        return answer_one;
    }

    /**
     * @param answer_one the answer_one to set
     */
    public void setAnswer_one(String answer_one) {
        this.answer_one = answer_one;
    }

    /**
     * @return the answer_two
     */
    public String getAnswer_two() {
        return answer_two;
    }

    /**
     * @param answer_two the answer_two to set
     */
    public void setAnswer_two(String answer_two) {
        this.answer_two = answer_two;
    }

    /**
     * @return the reset_password_method
     */
    public Integer getReset_password_method() {
        return reset_password_method;
    }

    /**
     * @param reset_password_method the reset_password_method to set
     */
    public void setReset_password_method(Integer reset_password_method) {
        this.reset_password_method = reset_password_method;
    }

    /**
     * @return the employeeid
     */
    public String getEmployeeid() {
        return employeeid;
    }

    /**
     * @param employeeid the employeeid to set
     */
    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    /**
     * @return the excel
     */
    public FormFile getExcel() {
        return excel;
    }

    /**
     * @param excel the excel to set
     */
    public void setExcel(FormFile excel) {
        this.excel = excel;
    }

    /**
     * @return the allow_remote_access
     */
    public Integer getAllow_remote_access() {
        return allow_remote_access;
    }

    /**
     * @param allow_remote_access the allow_remote_access to set
     */
    public void setAllow_remote_access(Integer allow_remote_access) {
        this.allow_remote_access = allow_remote_access;
    }
}
