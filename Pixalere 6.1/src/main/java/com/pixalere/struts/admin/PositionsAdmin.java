package com.pixalere.struts.admin;
import com.pixalere.auth.bean.PositionVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.TableUpdatesVO;
import java.util.Collection;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.PDate;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
public class PositionsAdmin extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsAdmin.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
     
        HttpSession session = request.getSession();
        
        ProfessionalVO userVO = (ProfessionalVO)session.getAttribute("userVO");
        if (request.getParameter("action") != null && request.getParameter("action").equals("add")) {// Insert
            // or
            log.info("************************ Starting PositionsAdmin- Add ***************************");
            PositionsAdminForm positionsAdminForm = (PositionsAdminForm) form;
            try {
                
                ProfessionalServiceImpl prodBD = new ProfessionalServiceImpl();
                
                String choice = request.getParameter("choice");
                
                PositionVO vo = positionsAdminForm.getFormData(choice,1);
                if (choice.equals("")) {
                    vo.setId(null);
                } else {
                    vo.setId(new Integer(choice));
                }
                prodBD.savePosition(vo);
                ConfigurationServiceImpl cmanager=new ConfigurationServiceImpl();
                
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setListdata(new Integer(new PDate().getEpochTime()+""));
                cmanager.saveTableUpdates(t);
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in PositionsAdmin.perform(): " + e.toString());
               ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                
            }
            
            log.info("************************ Starting PositionsAdmin - Done ***************************");
            
        } else if (request.getParameter("action") != null && request.getParameter("action").equals("edit")) {// Insert
            // or
            PositionsAdminForm positionsAdminForm = (PositionsAdminForm) form;
            try {
                
                ProfessionalServiceImpl prodBD = new ProfessionalServiceImpl();
                
                String choice = request.getParameter("choice");
                if (choice==null || choice.equals("")) {
                    choice = "0";
                }
                PositionVO vo = positionsAdminForm.getFormData(choice,1);
                prodBD.savePosition(vo);
                ConfigurationServiceImpl cmanager=new ConfigurationServiceImpl();
                
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setListdata(new Integer(new PDate().getEpochTime()+""));
                cmanager.saveTableUpdates(t);
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in PositionsAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                
            }
            log.info("************************ Starting PositionsAdmin - Done ***************************");
            
        }
        try {
            //Populate Category dropdown
            ProfessionalServiceImpl prodBD = new ProfessionalServiceImpl();
            request.setAttribute("positions", (Collection) prodBD.getPositions(new PositionVO()));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PositionsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.positions.success"));
        }
    }
    
}
