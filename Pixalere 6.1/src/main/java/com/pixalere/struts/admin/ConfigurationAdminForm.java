package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.upload.FormFile;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
public class ConfigurationAdminForm extends ActionForm {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConfigurationAdminForm.class);
    
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {
        HttpSession session=request.getSession();
        ActionErrors errors= new ActionErrors();
        return errors;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        //nothing
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    public FormFile getLogo(){
        return logo;
    }
    public void setLogo(FormFile logo){
        this.logo = logo;
    }
    private FormFile logo;
    private int id;
    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }
    private String value;
}
