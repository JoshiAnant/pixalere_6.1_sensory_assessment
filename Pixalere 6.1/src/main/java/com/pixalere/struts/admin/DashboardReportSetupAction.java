/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import com.pixalere.utils.FiscalQuarter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.ServletContext;
import com.pixalere.utils.Common;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.reporting.bean.*;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.reporting.service.DashboardReportService;
import java.util.List;
import java.util.Collection;
import java.util.Vector;

/**
 * Setup the Dashboard Reports GUI
 *
 * @since 5.0
 * @author travis morris
 */
public class DashboardReportSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DashboardReportSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        ListServiceImpl bd = new ListServiceImpl(language);
        log.info("Initializing DashboardReportSetupAction");
        try {
            Vector treatmentWithCatsVO = (Vector) bd.findAllTreatmentsWithCats(userVO);
            request.setAttribute("treatmentListWithCats", treatmentWithCatsVO);
            //load etiologies
            Collection<LookupVO> lookups = bd.getLists(LookupVO.ETIOLOGY);
            request.setAttribute("etiologies", lookups);

            ReportingServiceImpl service = new ReportingServiceImpl();

            if (request.getParameter("id") != null) {
                try {

                    int id = Integer.parseInt((String) request.getParameter("id"));
                    DashboardReportVO r = new DashboardReportVO();
                    r.setId(id);
                    service.removeDashboardReport(r);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            
            //temp placement for testing reports.
            ServletContext ctx = request.getSession().getServletContext();
            
            // OLD generate PDF.
            // ReportBuilder rservice = new ReportBuilder(Constants.DASHBOARD_REPORT, ctx);
            // rservice.setLanguage(language);
            
            // New Dashboard PDF
            DashboardReportService dashboardService = new DashboardReportService(ctx, language);
            
            //String user_stats = rservice.getUserStats();
            //While we're at it, lets check if its the first of the month, and send off the user statistics.
            //SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
            //mail.postMail("travis@pixalere.com", Common.getConfig("customer_name") + " User Stats for: " + PDate.getDate(new Date()), user_stats, "");

            //Collection<ConfigYearlyQuartersVO> quarters = service.getLastEightQuarters(new ConfigYearlyQuartersVO());//get last 9 quarters;
            Collection<DashboardReportVO> reports = service.getDashboardReports(new DashboardReportVO());
            System.out.println("Dashboard check:" + reports);
            request.setAttribute("dashboard_report", reports);
            if (reports != null) {
                for (DashboardReportVO report : reports) {
                    System.out.println("Calling Dashboard");
                    List<FiscalQuarter> quarters = Common.getQuarters(8,null);
                    if (quarters != null) {
                        // Old service
                        // rservice.generateDashboard(report, quarters);//generate PDF.
                        // New service, not extended
                        dashboardService.generateDashboardMail(report, quarters);
                    } else {
                        //Return error.
                    }
                }
            }
            //Generate a global report.. 1 for each treatment area.   Store
            //in database for future archival... broke down by etiology.. 1 page per etiology per treatment area.. TOC.
            //}

        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (Exception e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("admin.dashboardreport.success"));
    }
}
