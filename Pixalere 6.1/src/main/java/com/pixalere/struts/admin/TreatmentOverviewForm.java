package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;

/**
 * @author
 *
 */
public class TreatmentOverviewForm extends ActionForm {
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TreatmentOverviewForm.class);

	public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {
	  ActionErrors errors = new ActionErrors();

	  return errors;
	}
	public void reset(ActionMapping mapping,
						 HttpServletRequest request) {
	  
	}
	private String[] treatmentLocation;
        private int patient_status;
        private int caretype_status;
        private String[] caretypes;
        private int wound_goal;
        private int skin_goal;
        private int incision_goal;
        private int tubedrain_goal;
        private int ostomy_goal;
        private int treatment_modality;
    /**
     * @return the treatmentLocation
     */
    public String[] getTreatmentLocation() {
        return treatmentLocation;
    }
    /**
     * @param treatmentLocation the treatmentLocation to set
     */
    public void setTreatmentLocation(String[] treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    /**
     * @return the patient_status
     */
    public int getPatient_status() {
        return patient_status;
    }
    /**
     * @param patient_status the patient_status to set
     */
    public void setPatient_status(int patient_status) {
        this.patient_status = patient_status;
    }
    /**
     * @return the caretype_status
     */
    public int getCaretype_status() {
        return caretype_status;
    }
    /**
     * @param caretype_status the caretype_status to set
     */
    public void setCaretype_status(int caretype_status) {
        this.caretype_status = caretype_status;
    }
    /**
     * @return the caretypes
     */
    public String[] getCaretypes() {
        return caretypes;
    }
    /**
     * @param caretypes the caretypes to set
     */
    public void setCaretypes(String[] caretypes) {
        this.caretypes = caretypes;
    }
    /**
     * @return the wound_goal
     */
    public int getWound_goal() {
        return wound_goal;
    }
    /**
     * @param wound_goal the wound_goal to set
     */
    public void setWound_goal(int wound_goal) {
        this.wound_goal = wound_goal;
    }
    /**
     * @return the incision_goal
     */
    public int getIncision_goal() {
        return incision_goal;
    }
    /**
     * @param incision_goal the incision_goal to set
     */
    public void setIncision_goal(int incision_goal) {
        this.incision_goal = incision_goal;
    }
    /**
     * @return the tubedrain_goal
     */
    public int getTubedrain_goal() {
        return tubedrain_goal;
    }
    /**
     * @param tubedrain_goal the tubedrain_goal to set
     */
    public void setTubedrain_goal(int tubedrain_goal) {
        this.tubedrain_goal = tubedrain_goal;
    }
    /**
     * @return the ostomy_goal
     */
    public int getOstomy_goal() {
        return ostomy_goal;
    }
    /**
     * @param ostomy_goal the ostomy_goal to set
     */
    public void setOstomy_goal(int ostomy_goal) {
        this.ostomy_goal = ostomy_goal;
    }
    /**
     * @return the treatment_modality
     */
    public int getTreatment_modality() {
        return treatment_modality;
    }
    /**
     * @param treatment_modality the treatment_modality to set
     */
    public void setTreatment_modality(int treatment_modality) {
        this.treatment_modality = treatment_modality;
    }

    /**
     * @return the skin_goal
     */
    public int getSkin_goal() {
        return skin_goal;
    }

    /**
     * @param skin_goal the skin_goal to set
     */
    public void setSkin_goal(int skin_goal) {
        this.skin_goal = skin_goal;
    }
}
