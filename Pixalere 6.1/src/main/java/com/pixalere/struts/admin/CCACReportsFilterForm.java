/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.upload.FormFile;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 * This action form binds the form fields from the POST to the java bean.
 * @since 5.1
 * @version 5.1
 * 
 * @author travis
 */
public class CCACReportsFilterForm extends ActionForm{
    private int start_date_month;
    private int start_date_year;
    private int start_date_day;
    private int end_date_month;
    private int end_date_year;
    private int end_date_day;
    private int[] treatmentLocation;
    private int already_retrieved;
    /**
   * validate form data, and return to setupaction.
   * @param mapping
   * @param request
   * @return errors if error as thrown
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    HttpSession session=request.getSession();
       
    
    return errors;
  }
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    
  }
    /**
     * @return the start_date_month
     */
    public int getStart_date_month() {
        return start_date_month;
    }
    /**
     * @param start_date_month the start_date_month to set
     */
    public void setStart_date_month(int start_date_month) {
        this.start_date_month = start_date_month;
    }
    /**
     * @return the start_date_year
     */
    public int getStart_date_year() {
        return start_date_year;
    }
    /**
     * @param start_date_year the start_date_year to set
     */
    public void setStart_date_year(int start_date_year) {
        this.start_date_year = start_date_year;
    }
    /**
     * @return the start_date_day
     */
    public int getStart_date_day() {
        return start_date_day;
    }
    /**
     * @param start_date_day the start_date_day to set
     */
    public void setStart_date_day(int start_date_day) {
        this.start_date_day = start_date_day;
    }
    /**
     * @return the end_date_month
     */
    public int getEnd_date_month() {
        return end_date_month;
    }
    /**
     * @param end_date_month the end_date_month to set
     */
    public void setEnd_date_month(int end_date_month) {
        this.end_date_month = end_date_month;
    }
    /**
     * @return the end_date_year
     */
    public int getEnd_date_year() {
        return end_date_year;
    }
    /**
     * @param end_date_year the end_date_year to set
     */
    public void setEnd_date_year(int end_date_year) {
        this.end_date_year = end_date_year;
    }
    /**
     * @return the end_date_day
     */
    public int getEnd_date_day() {
        return end_date_day;
    }
    /**
     * @param end_date_day the end_date_day to set
     */
    public void setEnd_date_day(int end_date_day) {
        this.end_date_day = end_date_day;
    }
  
    /**
     * @return the already_retrieved
     */
    public int getAlready_retrieved() {
        return already_retrieved;
    }
    /**
     * @param already_retrieved the already_retrieved to set
     */
    public void setAlready_retrieved(int already_retrieved) {
        this.already_retrieved = already_retrieved;
    }
    /**
     * @return the treatmentLocation
     */
    public int[] getTreatmentLocation() {
        return treatmentLocation;
    }
    /**
     * @param treatmentLocation the treatmentLocation to set
     */
    public void setTreatmentLocation(int[] treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    
}