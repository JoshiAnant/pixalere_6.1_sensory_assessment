package com.pixalere.struts.admin;
import com.pixalere.common.bean.ProductCategoryVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import com.pixalere.common.*;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;

/**
 * @author 
 *
 */
public class ProductCategoryAdminForm extends ActionForm {
	
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsAdminForm.class);
public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {
	  ActionErrors errors = new ActionErrors();
	
	  
	  return errors;
	}
	
	public void reset(ActionMapping mapping,
						 HttpServletRequest request) {
	  setId(0);
	  setCategory("");
	  
	 	
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategory() {
		return category;
	}
	public ProductCategoryVO getFormData(String choice,int active){
		ProductCategoryVO vo = new ProductCategoryVO();
		if(!choice.equals("")){
			vo.setId(new Integer(choice));
		}
		vo.setTitle(getCategory());
		vo.setActive(new Integer(active));
		return vo;
	}
	private int id;
	private String category;
}
	  
