/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import java.util.List;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Enumeration;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.Collection;
import org.apache.struts.upload.FormFile;
import com.pixalere.common.ApplicationException;
import com.pixalere.mail.SendMailUsingAuthentication;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.PDate;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.Vector;
import com.pixalere.guibeans.Property;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.reporting.ReportingManager;
import com.pixalere.reporting.bean.DashboardByEtiologyVO;
import com.pixalere.reporting.bean.DashboardByLocationVO;
import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.ReportQueueEtiologyVO;
import com.pixalere.reporting.bean.ReportQueueTreatmentsVO;
import com.pixalere.reporting.bean.ReportQueueVO;
import com.pixalere.reporting.bean.ReportQueueWoundTypeVO;
import com.pixalere.reporting.service.DashboardReportService;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.utils.FiscalQuarter;
import java.io.File;
import com.pixalere.utils.TailUtil;
import java.util.ArrayList;
import java.util.Date;

/**
 * A maintenance class to display system properties, backup abilities, and
 * reading error logs.
 *
 * @since 5.0.1
 * @author tmorris
 */
public class Maintenance extends Action {

    static private Logger logs = Logger.getLogger(Maintenance.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        
        //Getting System Properties
        Properties systemProperties = System.getProperties();
        Enumeration enuProp = systemProperties.propertyNames();
        Vector<Property> properties = new Vector<Property>();
        String tomcat_path = "";
        while (enuProp.hasMoreElements()) {
            String propertyName = (String) enuProp.nextElement();
            String propertyValue = systemProperties.getProperty(propertyName);
            if (propertyName.equals("catalina.base")) {
                tomcat_path = propertyValue;
            }
            properties.add(new Property(propertyName, propertyValue));
        }
        request.setAttribute("properties", properties);
        //Getting the Threads
        Map<String, String> server_prop = getSysProps();
        request.setAttribute("threads", server_prop);
        request.setAttribute("thread_list", getAllThreadInfos());
        //get Log listing
        File log_check = new File(tomcat_path + "/logs");
        Vector<String> log_files = new Vector<String>();
        for (File file : log_check.listFiles()) {
            if (file.getName().indexOf("pixalere.err") != -1 || file.getName().equals("catalina.out") || file.getName().equals("stdout.log")) {
                log_files.add(file.getAbsolutePath());
                System.out.println("File: " + file.getPath());
            }
        }
        request.setAttribute("logs", log_files);
        if ((String) request.getParameter("log_file") != null && !((String) request.getParameter("log_file")).equals("")) {
            //get Last X lines of log file.
            String path = "";
            int count = 0;
            try {
                count = Integer.parseInt(((String) request.getParameter("count")));
                path = (String) request.getParameter("log_file");
            } catch (ClassCastException e) {
                logs.error("Log count or path are not valid objects" + e.getMessage());
            }
            request.setAttribute("log_name", path);
            request.setAttribute("log_dump", new TailUtil().tail(new File(path), count));
        }
       
        if (request.getParameter("action") != null && ((String) request.getParameter("action")).equals("empty_report_queue")) {
            try {
                ReportingServiceImpl rservice = new ReportingServiceImpl();
                
                // Old dashboard service
                // ReportBuilder rbuilder = new ReportBuilder(session.getServletContext());
                // New Dashboard service
                DashboardReportService dashboardService = new DashboardReportService(session.getServletContext(), language);
                
                Collection<DashboardReportVO> c = rservice.getDashboardReports(new DashboardReportVO());
                Collection<ReportQueueVO> queue = rservice.getReportQueues(new ReportQueueVO());
                Date start_date = null;
                Date end_date = null;
                String groupby = null;
                String breakdown = null;

                String full_assessment = null;
                String goal_days = null;
                String goals = null;
                //String print_patient_details = parameterForm.getPrintPatientDetails();
                //String apply_date_range = parameterForm.getPrintPatientDetails2();
                String includeTIP = null;

                String patient_id = null;
                for (ReportQueueVO q : queue) {
                    String report_type = q.getReport_type();

                    start_date = q.getStart_date();
                    end_date = q.getEnd_date();
                    ReportQueueTreatmentsVO ttmp = new ReportQueueTreatmentsVO();ttmp.setReport_id(q.getId());
                    Collection<ReportQueueTreatmentsVO> loc = rservice.getReportQueueTreatments(ttmp);
                    ReportQueueEtiologyVO etmp = new ReportQueueEtiologyVO();etmp.setReport_id(q.getId());
                    Collection<ReportQueueEtiologyVO> et = rservice.getReportQueueEtiologies(etmp);
                    ReportQueueWoundTypeVO wtmp = new ReportQueueWoundTypeVO();wtmp.setReport_id(q.getId());
                    Collection<ReportQueueWoundTypeVO> types = rservice.getReportQueueTypes(wtmp);
                    if (q.getReport_type().equals("report_dashboard")) {
                        DashboardReportVO rr = new DashboardReportVO();
                        rr.setEmail(q.getEmail());
                        
                        Collection<DashboardByEtiologyVO> etiologies = new ArrayList();
                        if (et != null) {
                            for (ReportQueueEtiologyVO e : et) {
                                DashboardByEtiologyVO etiology = new DashboardByEtiologyVO();
                                etiology.setDashboard_report_id(-1);
                                etiology.setEtiology_id(e.getLookup_id());
                                etiologies.add(etiology);
                            }
                        }
                        rr.setEtiologies(etiologies);
                        Collection<DashboardByLocationVO> treatments = new ArrayList();
                        if (loc != null) {
                            for (ReportQueueTreatmentsVO e : loc) {
                                DashboardByLocationVO l = new DashboardByLocationVO();
                                l.setDashboard_report_id(-1);
                                l.setTreatment_location_id(e.getTreatment_location_id());
                                treatments.add(l);
                            }
                        }
                        rr.setTreatmentLocations(treatments);

                        List<FiscalQuarter> quarters = Common.getQuarters(7, start_date);
                        if (types != null) {
                            for (ReportQueueWoundTypeVO t : types) {
                                rr.setWound_type(t.getWound_type());

                                // Old generate PDF.
                                // rbuilder.generateDashboard(rr, quarters);
                                // New service, not extended
                                dashboardService.generateDashboardMail(rr, quarters);
                            }
                        }
                        rservice.removeReportQueue(q);

                    } else {
                        String[] treatmentLocationArr = null;
                        if (loc != null) {
                            treatmentLocationArr = new String[loc.size()];
                        }
                        String[] etiologyArr = null;
                        if (et != null) {
                            etiologyArr = new String[et.size()];
                        }
                        String[] caretypesArr = null;
                        if (types != null) {
                            caretypesArr = new String[types.size()];
                        }
                        
                        int count = 0;
                        if (et != null) {
                            for (ReportQueueEtiologyVO e : et) {
                                etiologyArr[count] = e.getLookup_id() + "";
                                count++;
                            }
                        }
                        count = 0;
                        ProfessionalVO prof = new ProfessionalVO();
                        prof.setId(7);
                        Collection<UserAccountRegionsVO> regions = new ArrayList();
                        if (loc != null) {
                            for (ReportQueueTreatmentsVO e : loc) {
                                treatmentLocationArr[count] = e.getTreatment_location_id() + "";
                                regions.add(new UserAccountRegionsVO(-1, 7, e.getTreatment_location_id()));
                                count++;
                            }
                        }
                        prof.setRegions(regions);
                        count = 0;
                        if (types != null) {
                            for (ReportQueueWoundTypeVO e : types) {
                                caretypesArr[count] = e.getWound_type() + "";
                                count++;
                            }
                        }


                        ReportingManager manager = new ReportingManager(
                                prof, 
                                q.getStart_date(), 
                                q.getEnd_date(), 
                                "treatmentLocation", 
                                breakdown, 
                                report_type, 
                                full_assessment, 
                                goal_days, 
                                null, 
                                treatmentLocationArr, 
                                includeTIP, 
                                etiologyArr, 
                                goals, 
                                caretypesArr, 
                                patient_id, 
                                session.getServletContext(), 
                                language);
                        
                        // Use current language of the user
                        String locale = Common.getLanguageLocale(language);
                        
                        byte[] bytes = manager.callReport(null, null);
                        System.out.println("Building report.");
                        SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
                        String[] emails = {q.getEmail()};
                        if (bytes != null) {
                            mail.postMail(emails, 
                                    Common.getLocalizedString("pixalere.reporting.form." + q.getReport_type(), locale) + " " + PDate.getMonthYear(PDate.getEpochTime() + ""), 
                                    Common.getLocalizedString("pixalere.reporting.form." + q.getReport_type(), locale) + " email", 
                                    bytes, "reportserver");//String[] recipients, String subject, String message,byte[] byteArray, String attachment_name
                        }
                        rservice.removeReportQueue(q);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapping.findForward("admin.maintenance.success");
    }

    /**
     * Helper method; stringfies the ThreadInfos and returns them as a string
     * array
     */
    public String[] getThreads() {
        ThreadInfo[] threadInfos = getAllThreadInfos();
        int arraySize = threadInfos.length;
        String[] threads = new String[arraySize];
        for (int i = 0; i < arraySize; i++) {
            threads[i] = threadInfos[i].toString().replace("at ", "<br />&nbsp; &nbsp; &nbsp; at ");
        }
        return threads;
    } // end of getThreadArray method

    /**
     * Generates an array of thread infos. Adopted from
     * http://nadeausoftware.com/articles/2008/04/java_tip_how_list_and_find_threads_and_thread_groups
     *
     */
    private ThreadInfo[] getAllThreadInfos() {
        final ThreadMXBean thbean = ManagementFactory.getThreadMXBean();
        final long[] ids = thbean.getAllThreadIds();
        ThreadInfo[] infos;
        if (!thbean.isObjectMonitorUsageSupported()
                || !thbean.isSynchronizerUsageSupported()) {
            infos = thbean.getThreadInfo(ids);
        } else {
            infos = thbean.getThreadInfo(ids, true, true);
        }
        final ThreadInfo[] notNulls = new ThreadInfo[infos.length];
        int nNotNulls = 0;
        for (ThreadInfo info : infos) {
            if (info != null) {
                notNulls[nNotNulls++] = info;
            }
        }
        if (nNotNulls == infos.length) {
            return infos;
        }
        return java.util.Arrays.copyOf(notNulls, nNotNulls);
    }// end of getAllThreadInfos method

    /**
     * Utility method generates various application specific data
     *
     */
    public Map<String, String> getSysProps() {
        final RuntimeMXBean rmxbean = ManagementFactory.getRuntimeMXBean();
        ThreadMXBean tb = ManagementFactory.getThreadMXBean();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        Map<String, String> sysProps = new HashMap<String, String>();
        sysProps.put("System Startup Time ", sdf.format(rmxbean.getStartTime()));
        sysProps.put("Thread Count - Current ", (tb.getThreadCount() + ""));
        sysProps.put("Thread Count - Peak ", (tb.getPeakThreadCount() + ""));
        return sysProps;
    }
}
