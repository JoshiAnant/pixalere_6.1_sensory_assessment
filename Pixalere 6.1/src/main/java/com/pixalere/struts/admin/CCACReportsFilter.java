/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.reporting.bean.CCACReportVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.*;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.reporting.service.ReportingServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;
/**
 * Getting CCAC Reports from ccac_reporting table.   This class will allow
 * a user with proper rights to retrieve reports previously generated, and retur
 * the results to the GUI.
 * 
 * @version 5.1
 * @since 5.1
 * @author travis
 */
public class CCACReportsFilter extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CCACReportsFilter.class);
    /**
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        CCACReportsFilterForm reportForm = (CCACReportsFilterForm) form;
        try{
            HttpSession session = request.getSession();
            ProfessionalVO user = (ProfessionalVO)session.getAttribute("userVO");
           
            ReportingServiceImpl rservice = new ReportingServiceImpl();
            Date start_date = PDate.getDate(reportForm.getStart_date_month(), reportForm.getStart_date_day(), reportForm.getStart_date_year());
            Date end_date = PDate.getDate(reportForm.getEnd_date_month(), reportForm.getEnd_date_day(), reportForm.getEnd_date_year());
            Collection<CCACReportVO> reports = rservice.getCCACReports(1,reportForm.getAlready_retrieved(),start_date,end_date,reportForm.getTreatmentLocation());
            request.setAttribute("reports",reports);
            
            ListServiceImpl bd = new ListServiceImpl();
            
            Vector treatmentVOList = bd.findAllTreatmentsWithCats(user);
            request.setAttribute("treatmentList2", treatmentVOList);
        } catch(ApplicationException e){
            log.error("An application exception has been raised in AssignPatientsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null &&request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.ccacreport.success"));
        }
    }
    
}