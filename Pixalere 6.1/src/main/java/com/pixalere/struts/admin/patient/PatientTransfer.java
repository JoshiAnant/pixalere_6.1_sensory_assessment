package com.pixalere.struts.admin.patient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import java.util.Date;
import com.pixalere.utils.PDate;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import java.util.Collection;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 * A {@link org.apache.struts.action.Action} action which transfers patients
 *
 * @since 5.0
 * @author travis morris
 */
public class PatientTransfer extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientTransfer.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        boolean tokenValid = isTokenValid(request);
        try {
            if (session.getAttribute("patient_id") == null && session.getAttribute("admin_patient_id") == null) {
                ActionErrors errors = new ActionErrors();
                errors.add("exception", new ActionError("pixalere.patient_id.null"));
                saveErrors(request, errors);
                return (mapping.findForward("admin.patient"));
            }
            if (tokenValid) {
                resetToken(request);
                PatientProfileServiceImpl profilemanagerBD = new PatientProfileServiceImpl(language);
                ReferralsTrackingServiceImpl rservice = new ReferralsTrackingServiceImpl(language);
                WoundServiceImpl wservice = new WoundServiceImpl(language);
                PatientServiceImpl managerBD = new PatientServiceImpl(language);
                int patient_id = -1;
                if (session.getAttribute("admin_patient_id") != null) {
                    patient_id = Integer.parseInt((String) (session.getAttribute("admin_patient_id") + ""));
                } else {
                    patient_id = Integer.parseInt((String) (session.getAttribute("patient_id") + ""));
                }
                PatientAccountVO v = managerBD.getPatient(patient_id);
                //trhow error if treatment location was not set.
                if (request.getParameter("treatmentLocation") == null) {
                    ActionErrors errors = new ActionErrors();
                    errors.add("exception", new ActionError("pixalere.treatment_location_id.null"));
                    saveErrors(request, errors);
                    return (mapping.findForward("admin.patient"));
                }
                com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
                PDate pdate = new PDate(userVO.getTimezone());
                // added for search by professional
                v.setTreatment_location_id(new Integer((String) request.getParameter("treatmentLocation")));
                v.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale));
                v.setCreated_by(((ProfessionalVO) session.getAttribute("userVO")).getId());
                v.setAction("");
                managerBD.savePatient(v);
                PatientAccountVO p = managerBD.getPatient(patient_id);
                ListServiceImpl bd = new ListServiceImpl(language);

                //update referrals with new location so proper clinician has access.
                ReferralsTrackingServiceImpl refManager = new ReferralsTrackingServiceImpl(language);
                ReferralsTrackingVO r = new ReferralsTrackingVO();
                //r.setEntry_type(Constants.REFERRAL);
                r.setCurrent(1);
                r.setPatient_id(v.getPatient_id());
                if (p != null) {
                    Collection<ReferralsTrackingVO> refs = refManager.getAllReferralsByCriteria(r);

                    for (ReferralsTrackingVO ref : refs) {
                        ref.setPatient_account_id(p.getId());
                        refManager.saveReferralsTracking(ref);
                    }
                }
                int error = managerBD.validatePatientId(userVO.getId(), patient_id);
                if (error == 0) {
                    session.setAttribute("patient_id", patient_id + "");
                }
                session.setAttribute("patient_id", patient_id + "");
                session.removeAttribute("admin_patient_id");
                session.setAttribute("patientAccount", p);
            }
        } catch (Exception e) {
            System.out.println("error in pa:" + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("admin.patient.success"));
    }
}
