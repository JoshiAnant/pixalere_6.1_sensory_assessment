package com.pixalere.struts.admin;

import com.pixalere.patient.bean.PatientAccountVO;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author 
 */
public class PatientConsentForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientConsentForm.class);

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        return super.validate(mapping, request); 
    }
    
    public PatientAccountVO getFormData(PatientAccountVO pat){
        // TODO: Fill in the fields
        // TODO: Save img file to disk, recover path (here?)
        return pat;
    }
    
    private String criteria1;
    private String criteria2;
    private String sharing;
    private String signatureURI;
    private String patient_consent_blurb;
    private String patient_consent_name;
    private Integer consent_services;
    private Integer consent_information;
    private Integer consent_verbal;
    // TODO: Field for signature image
    // TODO: Field for signature path (here?)
    
    
    public String getCriteria1() {
        return criteria1;
    }

    public void setCriteria1(String criteria1) {
        this.criteria1 = criteria1;
    }

    public String getCriteria2() {
        return criteria2;
    }

    public void setCriteria2(String criteria2) {
        this.criteria2 = criteria2;
    }

    public String getSharing() {
        return sharing;
    }

    public void setSharing(String sharing) {
        this.sharing = sharing;
    }

    public String getPatient_consent_blurb() {
        return patient_consent_blurb;
    }

    public void setPatient_consent_blurb(String patient_consent_blurb) {
        this.patient_consent_blurb = patient_consent_blurb;
    }

    public Integer getConsent_services() {
        return consent_services;
    }

    public void setConsent_services(Integer consent_services) {
        this.consent_services = consent_services;
    }

    public Integer getConsent_information() {
        return consent_information;
    }

    public void setConsent_information(Integer consent_information) {
        this.consent_information = consent_information;
    }

    public Integer getConsent_verbal() {
        return consent_verbal;
    }

    public void setConsent_verbal(Integer consent_verbal) {
        this.consent_verbal = consent_verbal;
    }

    public String getSignatureURI() {
        return signatureURI;
    }

    public void setSignatureURI(String signatureURI) {
        this.signatureURI = signatureURI;
    }

    public String getPatient_consent_name() {
        return patient_consent_name;
    }

    public void setPatient_consent_name(String patient_consent_name) {
        this.patient_consent_name = patient_consent_name;
    }
    
}
