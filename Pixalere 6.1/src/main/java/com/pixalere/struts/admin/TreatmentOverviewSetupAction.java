/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.admin.bean.AssignPatientsVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.Constants;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.utils.Common;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;

public class TreatmentOverviewSetupAction extends Action{

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        ProfessionalServiceImpl profService = new ProfessionalServiceImpl();
        ListServiceImpl bd = new ListServiceImpl(language);
        try{
            request.setAttribute("woundGoals",bd.getLists(LookupVO.WOUND_GOALS));
            request.setAttribute("drainGoals",bd.getLists(LookupVO.TUBEDRAIN_GOALS));
            request.setAttribute("ostomyGoals",bd.getLists(LookupVO.OSTOMY_GOALS));
            request.setAttribute("skinGoals",bd.getLists(LookupVO.SKIN_GOALS));
            request.setAttribute("incisionGoals",bd.getLists(LookupVO.INCISION_GOALS));
            AssignPatientsServiceImpl aService = new AssignPatientsServiceImpl();
            if(session.getAttribute("patient_id")!=null){
                if(userVO.getAccess_assigning().equals(new Integer(1))){
                    int patient_id = Integer.parseInt((String)session.getAttribute("patient_id"));
                    ProfessionalVO pt = new ProfessionalVO();
                    pt.setAccount_status(1);
                    request.setAttribute("professionals",profService.findAllBySearch(userVO.getId(),"","",-1,"", null,""));
                    AssignPatientsVO atmp = new AssignPatientsVO();
                    atmp.setPatientId(patient_id);
                    request.setAttribute("professionalsa",aService.getAllAssignedPatients(atmp));
                }
                request.setAttribute("inactivate_reason", bd.getLists(LookupVO.INACTIVATE_REASON));
            }
          
            Vector<LookupVO> treatmentCatsVO = (Vector) bd.getLists(LookupVO.TREATMENT_CATEGORY, userVO, false);
            Vector<LookupVO> treatmentVO = (Vector) bd.getLists(LookupVO.TREATMENT_LOCATION, userVO, false);
            if (userVO.getTraining_flag() == 1) {
                List<LookupVO> treatmentsWithCats = new ArrayList();
                List<LookupVO> treatments = new ArrayList();
                //remove all treatment locations but training if this user is a training user.
                for (LookupVO l : treatmentCatsVO) {
                   
                    if (l.getCategoryId() == Constants.TRAINING_TREATMENT_CATEGORY_ID) {
                        treatmentsWithCats.add(l);
                    } else {
                    }
                }
                for (LookupVO l : treatmentVO) {
                    if (l.getId() == Constants.TRAINING_TREATMENT_ID) {
                        treatments.add(l);
                    }
                }
                request.setAttribute("treatmentCats", treatmentsWithCats);
                request.setAttribute("treatmentList", treatments);
            } else {
                request.setAttribute("treatmentCats", treatmentCatsVO);
                request.setAttribute("treatmentList", treatmentVO);
            }
            Vector treatmentVOList = null;
            if(userVO.getTraining_flag() == null || userVO.getTraining_flag() != 1){
               treatmentVOList =  (Vector) bd.findAllTreatmentsWithCats(userVO);
            }else{
                treatmentVOList =  (Vector) bd.findAllTrainingTreatments(userVO);
            }
            request.setAttribute("treatmentList2", treatmentVOList);
        } catch (ApplicationException e) {
                
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        return mapping.findForward("admin.treatmentoverview.success");
    }
}
