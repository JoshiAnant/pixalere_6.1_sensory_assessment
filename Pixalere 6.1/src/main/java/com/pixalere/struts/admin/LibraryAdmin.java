package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.bean.LibraryVO;
import com.pixalere.common.bean.LibraryCategoryVO;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.utils.Serialize;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Vector;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Common;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.service.LibraryServiceImpl;
import com.pixalere.common.service.ConfigurationServiceImpl;
import java.io.*;
public class LibraryAdmin extends Action {
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        LibraryAdminForm infoAdminForm = (LibraryAdminForm) form;
        HttpSession session = request.getSession();
        
        LibraryServiceImpl manager = new LibraryServiceImpl();
        ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
        try {
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            PDate pdate = new PDate();
            LibraryAdminForm libform = (LibraryAdminForm) form;
            if (libform.getList_action() != null && libform.getList_action().equals("save")) {
                
                String time = pdate.getEpochTime() + "";
                LibraryVO p = new LibraryVO();
                if (libform.getId() != null) {
                    p.setId(libform.getId());
                }
                p.setName(libform.getName());
                if(libform.getMedia_type().equals("pdf")){
                    InputStream in = libform.getFile().getInputStream();
                    p.setPdf(time + ".pdf");
                    Common.writeToFile(Common.getDownloadsPath() + "pdfs/" + time + ".pdf", in, true);
                }else if(libform.getMedia_type().equals("url") || libform.getMedia_type().equals("vid")){
                    p.setUrl(libform.getUrl());
                }
                p.setMedia_type(libform.getMedia_type());
                p.setCategory_id(libform.getCategory());
                p.setOrderby(libform.getOrderby());
                manager.saveLibrary(p);
                File pathoff = new File(Common.getDownloadsPath()+"pdfs/" );
                if (!pathoff.exists()) {
                    try {
                        pathoff.mkdir();
                    } catch (Exception e) {
                        throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);
                    }
                }
                
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setPdf(new Integer(pdate.getEpochTime() + ""));
                cmanager.saveTableUpdates(t);
            } else if (libform.getList_action() != null && libform.getList_action().equals("delete")) {
                LibraryVO p = new LibraryVO();
                if (libform.getId() != null) {
                    p.setId(libform.getId());
                }
                manager.removeLibrary(p);
            /*TableUpdatesVO t = cmanager.getTableUpdates();
            t.setPdf(new Integer(PDate.getEpochTime()+""));
            cmanager.saveTableUpdates(t);*/
            } else if(libform.getList_action() != null && libform.getList_action().equals("editCategory")){
                LibraryCategoryVO c = new LibraryCategoryVO();
                c.setId(libform.getId());
                LibraryCategoryVO category = manager.getCategory(c);
                request.setAttribute("category",category);
                
            }
            else if(libform.getList_action() != null && libform.getList_action().equals("deleteCategory")){
                LibraryCategoryVO c = new LibraryCategoryVO();
                c.setId(libform.getCategory_id());
                
                java.util.Collection<LibraryVO> libs = manager.getAllLibraries(libform.getCategory_id());
                if(libs == null || libs.size() == 0){
                    manager.removeLibraryCategory(c);
                }
            }
            else if(libform.getList_action() != null && libform.getList_action().equals("saveCategory")){
            LibraryCategoryVO c = new LibraryCategoryVO();
            if(libform.getId()!=null){
                c.setId(libform.getCategory_id());
            }
                c.setName(libform.getName());
                
                c.setHide(libform.getHide());
                c.setOrder_by(libform.getOrderby());
                manager.saveLibraryCategory(c);
            }
            
        } catch (IOException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.library.success"));
        }
    }
}
