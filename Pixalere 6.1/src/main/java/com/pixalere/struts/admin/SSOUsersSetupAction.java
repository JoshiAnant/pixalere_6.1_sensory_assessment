package com.pixalere.struts.admin;

import com.pixalere.auth.bean.PositionVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.sso.bean.SSOPositionVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.service.OpenSAMLService;
import com.pixalere.sso.service.SSOProviderService;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Management UI for SSO Configuration Admin.
 *
 * @author Jose
 * @since 6.0
 */
public class SSOUsersSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOUsersSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        
        // To check session and know if ProviderId has been set
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        
        if (request.getParameter("providerId") == null && session.getAttribute("providerId") == null) {
            // No provider go back to Providers list
            return (mapping.findForward("admin.ssousers.failure"));
        }
        
        SSOProviderService service = new SSOProviderService();
        String s_p_id = request.getParameter("providerId") != null
                ? request.getParameter("providerId")
                : (String) session.getAttribute("providerId");
        session.setAttribute("providerId", null);

        int provider_id = Integer.parseInt(s_p_id);
        SSOProviderVO provider = service.getProviderById(provider_id);
        
        if (provider == null){
            // No provider go back to Providers list
            return (mapping.findForward("admin.ssousers.failure"));
        }
        
        try {
            // Set timezone list
            ListServiceImpl bd = new ListServiceImpl(language);
            request.setAttribute("timezone", bd.getLists(LookupVO.TIMEZONE));

            // Set Positions - Values list
            ProfessionalServiceImpl manager = new ProfessionalServiceImpl();
            Collection<PositionVO> pix_positions = manager.getPositions(new PositionVO(Constants.ACTIVE));
            
            Collection<SSOPositionVO> ssoPositions = provider.getSsoPositions();
            if (ssoPositions.isEmpty()){
                // No positions yet, create placeholders on the current and active Pix positions
                Collection<SSOPositionVO> ssopos = new ArrayList<SSOPositionVO>();
                for (PositionVO positionVO : pix_positions) {
                    SSOPositionVO p = new SSOPositionVO();
                    p.setPositionId(positionVO.getId());
                    p.setAttributeValue("");
                    p.setSsoProviderid(provider.getId());
                    p.setTitle(positionVO.getTitle());
                    ssoPositions.add(p);
                }
                
            } else {
                // Sync the positions: First remove from SSO, inactive or not defined
                // (already deleted) Pix positions. Then add Pix positions
                // that are not yet declared in the SSO positions, if any.
                
                // Remove
                Map<Integer,PositionVO> mapPix = new HashMap<Integer, PositionVO>();
                for (PositionVO positionVO : pix_positions) {
                    mapPix.put(positionVO.getId(), positionVO);
                }
                for (Iterator<SSOPositionVO> it = ssoPositions.iterator(); it.hasNext();) {
                    SSOPositionVO sSOPositionVO = it.next();
                    // If not in Active current Positions, remove from collection
                    if (!mapPix.containsKey(sSOPositionVO.getPositionId())){
                        it.remove();
                    }
                }
                // Add
                Map<Integer,SSOPositionVO> mapSSO = new HashMap<Integer,SSOPositionVO>();
                for (SSOPositionVO sSOPositionVO : ssoPositions) {
                    mapSSO.put(sSOPositionVO.getPositionId(), sSOPositionVO);
                }
                for (PositionVO positionVO : pix_positions) {
                    // Pix pos not in SSO, add it
                    if (!mapSSO.containsKey(positionVO.getId())){
                        SSOPositionVO p = new SSOPositionVO();
                        p.setPositionId(positionVO.getId());
                        p.setAttributeValue("");
                        p.setSsoProviderid(provider.getId());
                        p.setTitle(positionVO.getTitle());
                        ssoPositions.add(p);
                    }
                }
            }
            request.setAttribute("positions", ssoPositions);
            
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        String baseURL = OpenSAMLService.getBaseUrl(request);
        String urlConsumer = OpenSAMLService.getAssertionConsumerUrl(baseURL);
        request.setAttribute("urlConsumer", urlConsumer);
        String xmlMetadata = OpenSAMLService.generateMetadataXML(provider, urlConsumer);
        request.setAttribute("xmlMetadata", xmlMetadata);
        //String urlLogout = OpenSAMLService.getLogoutUrl(baseURL);
        //request.setAttribute("urlLogout", urlLogout);
        request.setAttribute("provider", provider);
        
        return (mapping.findForward("admin.ssousers.success"));
    }
    
}
