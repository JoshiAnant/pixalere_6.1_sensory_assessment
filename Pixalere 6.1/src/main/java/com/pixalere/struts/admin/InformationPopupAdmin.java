package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.InformationPopupVO;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.utils.Serialize;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Vector;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Common;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.TableUpdatesVO;
public class InformationPopupAdmin extends Action {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InformationPopupAdmin.class);
    
    public ActionForward execute(ActionMapping mapping,
            
            ActionForm form,
            
            HttpServletRequest request,
            
            HttpServletResponse response) {
        InformationPopupAdminForm infoAdminForm = (InformationPopupAdminForm) form;
        HttpSession session = request.getSession();
        
        GUIServiceImpl manager=new GUIServiceImpl();

        try {
            ProfessionalVO userVO = (ProfessionalVO)session.getAttribute("userVO");
            PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
                int component_id=new Integer(request.getParameter("component_id")).intValue();
                ComponentsVO component=manager.getComponent(new Integer(component_id));
                InformationPopupVO info = infoAdminForm.getFormData();
                
                
                if(info.getDescription().equals("")){
                    component.setInfo_popup_id(0);//clear info popup
                    manager.removeInfoPopup(infoAdminForm.getId());
                }else{
                    manager.saveInformationPopup(info);
                    info.setDescription(null);
                    InformationPopupVO temp = manager.getInformationPopup(info);
                    if(temp==null){
                        info.setId(null);
                        info.setDescription(null);
                        temp = manager.getInformationPopup(info);
                    }
                    if(temp!=null){
                        component.setInfo_popup_id(temp.getId());
                    }
                }
                manager.saveComponent(component);
                
                ConfigurationServiceImpl cmanager=new ConfigurationServiceImpl();
                
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setInformation_popup(new Integer(new PDate().getEpochTime()+""));
                cmanager.saveTableUpdates(t);
                
            
        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.info.success"));
        }
    }
    
}
