/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.pixalere.struts.admin;

import com.pixalere.admin.bean.VendorVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author travis
 */
public class VendorAdmin extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(VendorAdmin.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
     
        HttpSession session = request.getSession();
         ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
        ProfessionalVO userVO = (ProfessionalVO)session.getAttribute("userVO");
        
                System.out.println("1"+request.getParameter("action"));
        if (request.getParameter("action") != null && request.getParameter("action").equals("add")) {// Insert
            // or
            VendorForm positionsAdminForm = (VendorForm) form;
            try {
                
               
                System.out.println("1");
                String choice = request.getParameter("choice");
                
                VendorVO vo = positionsAdminForm.getFormData();
                if (choice.equals("")) {
                    vo.setVendor_id(null);
                } else {
                    vo.setVendor_id(new Integer(choice));
                }
                File pathoff = new File(Common.getDownloadsPath()+"productimgs/" );
                if (!pathoff.exists()) {
                    try {
                        pathoff.mkdir();
                    } catch (Exception e) {
                        throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);
                    }
                }
                
                System.out.println("2");
                if(positionsAdminForm.getLogo()!=null){
                    String time = PDate.getEpochTime()+"";
                    InputStream in = positionsAdminForm.getLogo().getInputStream();
                    vo.setLogo(time+"_"+positionsAdminForm.getLogo().getFileName());
                    Common.writeToFile(Common.getDownloadsPath() + "productimgs/" + time+"_"+positionsAdminForm.getLogo().getFileName(), in, true);
                }else{
                    vo.setLogo("");
                }
                
                System.out.println("3");
                cmanager.saveVendor(vo);
                
                System.out.println("4");
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setVendors(new Integer(new PDate().getEpochTime()+""));
                cmanager.saveTableUpdates(t);
                
                System.out.println("5");
            } catch (ApplicationException e) {
                e.printStackTrace();
                log.error("An application exception has been raised in VendorAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }catch (FileNotFoundException e) {
                e.printStackTrace();
                log.error("An application exception has been raised in VendorAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }catch (IOException e) {
                e.printStackTrace();
                log.error("An application exception has been raised in VendorAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
            
            log.info("************************ Starting PositionsAdmin - Done ***************************");
            
        } else if (request.getParameter("action") != null && request.getParameter("action").equals("edit")) {// Insert
            // or
            VendorForm positionsAdminForm = (VendorForm) form;
            try {
                
                ProfessionalServiceImpl prodBD = new ProfessionalServiceImpl();
                
                String choice = request.getParameter("choice");
                if (choice==null || choice.equals("")) {
                    choice = "0";
                }
                VendorVO vo = positionsAdminForm.getFormData();
                if (choice.equals("")) {
                    vo.setVendor_id(null);
                } else {
                    vo.setVendor_id(new Integer(choice));
                }
                File pathoff = new File(Common.getDownloadsPath()+"productimgs/" );
                if (!pathoff.exists()) {
                    try {
                        pathoff.mkdir();
                    } catch (Exception e) {
                        throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);
                    }
                }
                if(positionsAdminForm.getLogo()!=null){
                    String time = PDate.getEpochTime()+"";
                    InputStream in = positionsAdminForm.getLogo().getInputStream();
                    vo.setLogo(time+"_"+positionsAdminForm.getLogo().getFileName());
                    Common.writeToFile(Common.getDownloadsPath() + "productimgs/" + time+"_"+positionsAdminForm.getLogo().getFileName(), in, true);
                }else{
                    vo.setLogo("");
                }
                cmanager.saveVendor(vo);
                
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setListdata(new Integer(new PDate().getEpochTime()+""));
                cmanager.saveTableUpdates(t);
            }  catch (ApplicationException e) {
                log.error("An application exception has been raised in VendorAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }catch (FileNotFoundException e) {
                log.error("An application exception has been raised in VendorAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }catch (IOException e) {
                log.error("An application exception has been raised in VendorAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
            log.info("************************ Starting PositionsAdmin - Done ***************************");
            
        }
        try {
            //Populate Category dropdown
            ProfessionalServiceImpl prodBD = new ProfessionalServiceImpl();
            request.setAttribute("vendors", (Collection) cmanager.getAllVendors());
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PositionsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.vendor.success"));
        }
    
    
}

    
}
