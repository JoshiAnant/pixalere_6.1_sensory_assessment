package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.upload.FormFile;
/**
 * @author
 */
public class ProductsAdminForm extends ActionForm {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsAdminForm.class);
    public ActionErrors validate(ActionMapping mapping,
                                 HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
    public void reset(ActionMapping mapping,
                      HttpServletRequest request) {
        setId(0);
        setProduct("");
        setCost(0.0);
        //setGroupby("");
        setDescription("");
        setUrl("");
        setCompany("");
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public void setProduct(String product) {
        this.product = product;
    }
    public String getProduct() {
        return product;
    }
    public void setCost(double cost) {
        this.cost = cost;
    }
    public double getCost() {
        return cost;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    public String getQuantity() {
        return quantity;
    }
    public void setProduct_category(int product_category) {
        this.product_category = product_category;
    }
    public int getProduct_category() {
        return product_category;
    }
    private int[] treatmentLocation;
    private int id;
    private String product;
    private int all_treatment_location;
    private double cost;
    private String type;
    private String quantity;
    private int product_category;
    private String url;
    private String video_url;
    private String company;
    private String description;
    private Integer library_id;
    private FormFile image;
    
    private Integer display_image;
    private Integer display_description;
    private String recommendation_type = null;
    private FormFile pdf;
    
    public FormFile getImage(){
        return image;
    }
    public void setImage(FormFile image){
        this.image=image;
    }
    /**
     * @return the treatmentLocation
     */
    public int[] getTreatmentLocation() {
        return treatmentLocation;
    }
    /**
     * @param treatmentLocation the treatmentLocation to set
     */
    public void setTreatmentLocation(int[] treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    /**
     * @return the all_treatment_locations
     */
    public int getAll_treatment_location() {
        return all_treatment_location;
    }
    /**
     * @param all_treatment_location the all_treatment_locations to set
     */
    public void setAll_treatment_location(int all_treatment_location) {
        this.all_treatment_location = all_treatment_location;
    }
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }
    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }
    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the library_id
     */
    public Integer getLibrary_id() {
        return library_id;
    }
    /**
     * @param library_id the library_id to set
     */
    public void setLibrary_id(Integer library_id) {
        this.library_id = library_id;
    }

    public Integer getDisplay_image() {
        return display_image;
    }

    public void setDisplay_image(Integer display_image) {
        this.display_image = display_image;
    }

    public Integer getDisplay_description() {
        return display_description;
    }

    public void setDisplay_description(Integer display_description) {
        this.display_description = display_description;
    }

    public String getRecommendation_type() {
        return recommendation_type;
    }

    public void setRecommendation_type(String recommendation_type) {
        this.recommendation_type = recommendation_type;
    }

    public FormFile getPdf() {
        return pdf;
    }

    public void setPdf(FormFile pdf) {
        this.pdf = pdf;
    }

    /**
     * @return the video_url
     */
    public String getVideo_url() {
        return video_url;
    }

    /**
     * @param video_url the video_url to set
     */
    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }
}
	  
