/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.admin;

import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author 
 */
public class PatientConsentSetupAction extends Action {
    private static final Logger log = Logger.getLogger(PatientConsentSetupAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("page", "admin");
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        ListServiceImpl bd = new ListServiceImpl(language);
        
        String patient_id = (String) session.getAttribute("patient_id");
        if (patient_id == null) {
            return (mapping.findForward("admin.patient.consent_error"));
        }
        
        try {
            PatientServiceImpl patientService = new PatientServiceImpl(language);
            PatientAccountVO patient = patientService.getPatient(new Integer((String) patient_id));

            if (patient == null) {
                return (mapping.findForward("admin.patient.consent_error"));
            }

            request.setAttribute("patient_id", patient_id);
            request.setAttribute("patient_name", patient.getFirstName() + " " + patient.getLastName());
            
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy", Common.getLocaleFromLocaleString(locale));
            String currentDate = df.format(new Date());
            request.setAttribute("currentDate", currentDate);

            // Text values - Replacing break lines with <p> tags
            String consentIntro = "<p>" + Common.getConfig("consentIntro").replace("\n", "</p><p>") + "</p>";
            String consentSharingHeader = "<p>" + Common.getConfig("consentSharingHeader").replace("\n", "</p><p>") + "</p>";
            String consentDetails = "<p>" + Common.getConfig("consentDetails").replace("\n", "</p><p>") + "</p>";
            String consentDisclaimers = "<p>" + Common.getConfig("consentDisclaimers").replace("\n", "</p><p>") + "</p>";
            String consentAcceptance = "<p>" + Common.getConfig("consentAcceptance").replace("\n", "</p><p>") + "</p>";

            request.setAttribute("consentIntro", consentIntro);
            request.setAttribute("consentSharingHeader", consentSharingHeader);
            request.setAttribute("consentDetails", consentDetails);
            request.setAttribute("consentDisclaimers", consentDisclaimers);
            request.setAttribute("consentAcceptance", consentAcceptance);
            request.setAttribute("consentCriteria1", Common.getConfig("consentCriteria1"));
            request.setAttribute("consentCriteria2", Common.getConfig("consentCriteria2"));
            request.setAttribute("consentServicesChk", Common.getConfig("consentServicesChk"));
            request.setAttribute("consentInformationChk", Common.getConfig("consentInformationChk"));

            Collection<LookupVO> results = bd.getLists(LookupVO.VERBAL_PATIENT_CONSENT);
            request.setAttribute("consent_verbal_options", results);
        } catch (Exception e) {
            System.out.println("Error setup consent:" + e.toString());
            return (mapping.findForward("admin.patient.consent_error"));
        }
            
        
        return mapping.findForward("admin.patient.consent");
    }
    
    
    
}
