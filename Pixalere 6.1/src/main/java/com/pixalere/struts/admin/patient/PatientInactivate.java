package com.pixalere.struts.admin.patient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import com.pixalere.wound.bean.WoundVO;
import java.util.Vector;
import com.pixalere.utils.PDate;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import java.util.Collection;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.Constants;
import java.util.Date;

public class PatientInactivate extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientInactivate.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
            
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
            
        try {
            if (session.getAttribute("patient_id") == null) {
                ActionErrors errors = new ActionErrors();
                errors.add("exception", new ActionError("pixalere.patient_id.null"));
                saveErrors(request, errors);
                return (mapping.findForward("admin.patient.search"));
            }
            
            PatientServiceImpl managerBD = new PatientServiceImpl(language);
            ReferralsTrackingServiceImpl rservice = new ReferralsTrackingServiceImpl(language);
            WoundServiceImpl wservice = new WoundServiceImpl(language);
            PatientAccountVO p = managerBD.getPatient(new Integer(session.getAttribute("patient_id") + ""));
            if (p != null) {
                PDate pdate = new PDate();
                ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
                String discharge_date_day=(String)request.getParameter("backdated_day");
                String discharge_date_month=(String)request.getParameter("backdated_month");
                String discharge_date_year=(String)request.getParameter("backdated_year");
                p.setAccount_status(0);
                p.setIsAdmitted(0);
                p.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
                p.setCreated_by(userVO.getId());
                if(discharge_date_day!=null && !discharge_date_day.equals("") && discharge_date_month!=null && !discharge_date_month.equals("") && discharge_date_year!=null && !discharge_date_year.equals("")){
                    p.setDischarge_date(PDate.getDate(Integer.parseInt(discharge_date_month),Integer.parseInt(discharge_date_day),Integer.parseInt(discharge_date_year)));
                }
               
                try {
                    p.setDischarge_reason(new Integer((String) request.getParameter("inactivate_reason")));
                } catch (ClassCastException e) {
                }
                p.setAction(Constants.DISCHARGE);
                managerBD.savePatient(p);
                if (Common.getConfig("purgeReferrals").equals("1")) {
                    //remove outstanding referrals
                    try {
                        Collection<WoundVO> wounds = wservice.getWounds(p.getPatient_id());
                        Vector recommendations = new Vector();
                        for (WoundVO wound : wounds) {
                            ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                            rtmp.setWound_id(wound.getId());
                            rtmp.setEntry_type(Constants.REFERRAL);
                            rtmp.setCurrent(Constants.ACTIVE);
                            Collection<ReferralsTrackingVO> referrals = rservice.getAllReferralsByCriteria(rtmp);
                            for (ReferralsTrackingVO referral : referrals) {
                                rservice.removeReferral(referral);
                            }
                            rtmp.setEntry_type(Constants.RECOMMENDATION);
                            rtmp.setCurrent(Constants.ACTIVE);
                            referrals = rservice.getAllReferralsByCriteria(rtmp);
                            for (ReferralsTrackingVO referral : referrals) {
                                rservice.removeReferral(referral);
                            }
                        }
                    } catch (ApplicationException e) {
                        log.error(e.getMessage());
                    }
                }
                session.setAttribute("viewerOnly", "inactive");
            }
            PatientAccountVO patient = managerBD.getPatient(new Integer(session.getAttribute("patient_id") + ""));
            session.setAttribute("patientAccount", patient);
        } catch (Exception e) {
            System.out.println("error in pa:" + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("admin.patient.success"));
    }
}
