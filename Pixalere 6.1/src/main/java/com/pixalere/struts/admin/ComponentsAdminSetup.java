/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.service.GUIServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Vector;
/**
 * Setup the components GUI
 *
 * @since 5.0
 * @author travis
 */
public class ComponentsAdminSetup extends Action  {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ComponentsAdminSetup.class);
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        try{
            HttpSession session = request.getSession();
            //ComponentsAdminForm f = (ComponentsAdminForm)form;
            request.setAttribute("page","component");
            GUIServiceImpl gservice = new GUIServiceImpl();
            //if professional was selected, get all the assignments to that professional.
            if(request.getParameter("component_id")!=null && !request.getParameter("component_id").equals("")){
                ComponentsVO component = gservice.getComponent(new Integer((String)request.getParameter("component_id")));
                request.setAttribute("component",component);
            }
            //load dropdowns.
            Collection<ComponentsVO> pages = gservice.getAllComponents();
            request.setAttribute("pages",pages);
        } catch(Exception e){
            log.error("An application exception has been raised in ComponentsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")){
            return (mapping.findForward("uploader.go.profile"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("viewer")){
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else{
            return (mapping.findForward("admin.components.success"));
        }
    }
}
