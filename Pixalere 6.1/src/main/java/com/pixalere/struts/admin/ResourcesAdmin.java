package com.pixalere.struts.admin;

import java.util.List;
import java.util.ArrayList;
import com.pixalere.common.bean.LanguageVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.admin.service.ResourcesServiceImpl;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.bean.InformationPopupVO;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Collection;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.utils.PDate;
import java.util.Vector;
import com.pixalere.utils.Common;
import java.util.Iterator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ResourcesAdmin extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ResourcesAdmin.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        ResourcesAdminForm resourcesAdminForm = (ResourcesAdminForm) form;
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);

        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
        try {
            // Populate resource dropdown
            if (request.getParameter("resourceMenu") != null) {
                session.setAttribute("resourceMenu", request.getParameter("resourceMenu"));
            }
            // populate resoruce_id hidden field
            if (request.getParameter("resource_id") != null) {
                session.setAttribute("resource_id", request.getParameter("resource_id"));
            }
            PDate pdate = new PDate();
            // if adding, grab form data and add list item

            if (request.getParameter("action") != null && request.getParameter("action").equals("add")) {
                LookupVO resourcesVO = new LookupVO();
                //resourcesVO.setName(resourcesAdminForm.getName());
                resourcesVO.setModified_date(new java.util.Date());
                resourcesVO.setId(null);
                resourcesVO.setActive(new Integer(1));
                resourcesVO.setLocked(0);
                resourcesVO.setResourceId(new Integer(request.getParameter("resource_id")));
                resourcesVO.setCategoryId(new Integer(resourcesAdminForm.getCategoryId()));
                resourcesVO.setOther(new Integer(resourcesAdminForm.getIsOther()));
                resourcesVO.setTitle(new Integer(resourcesAdminForm.getTitle()));
                resourcesVO.setSoloBox(new Integer(resourcesAdminForm.getSoloBox()));
                resourcesVO.setReport_value(resourcesAdminForm.getReport_value());
                resourcesVO.setUpdate_access(resourcesAdminForm.getItem_filter() + "");
                resourcesVO.setOrderby(new Integer(resourcesAdminForm.getOrderby()));
                resourcesVO.setIcd9(resourcesAdminForm.getIcd9());
                resourcesVO.setIcd10(resourcesAdminForm.getIcd10());
                resourcesVO.setCpt(resourcesAdminForm.getCpt());
                if (resourcesAdminForm.getClinic() == 1) {
                    resourcesVO.setClinic(1);
                } else if (resourcesAdminForm.getClinic() == 0) {
                    resourcesVO.setClinic(0);
                }
                GUIServiceImpl manager = new GUIServiceImpl();
                ListServiceImpl managerBD = new ListServiceImpl(language);
                Collection<LanguageVO> languages = managerBD.getLanguages();
                List<LookupLocalizationVO> names = new ArrayList();
                ComponentsVO compTMP = new ComponentsVO();
                compTMP.setResource_id(new Integer(request.getParameter("resource_id")));
                ComponentsVO component = manager.getComponent(compTMP);
                boolean hasDefinition = false;
                for (LanguageVO lang : languages) {
                    //if text exists save a new localized string.
                    if (request.getParameter(lang.getName()) != null && !((String) request.getParameter(lang.getName())).equals("")) {
                        LookupLocalizationVO n = new LookupLocalizationVO();
                        //n.setLookup_id(new_id.getId());
                        n.setName((String) request.getParameter(lang.getName()));
                        n.setLanguage_id(lang.getId());
                        n.setResource_id(resourcesVO.getResourceId());
                        n.setDefinition((String) request.getParameter("definition" + lang.getName()));
                        if (request.getParameter("definition" + lang.getName()) != null && !(((String) request.getParameter("definition" + lang.getName())).equals(""))) {
                            hasDefinition = true;
                        }

                        names.add(n);
                    }
                }
                if (hasDefinition && component != null) {
                    InformationPopupVO infoTMP = new InformationPopupVO();
                    infoTMP.setComponent_id(component.getId());
                    InformationPopupVO info = manager.getInformationPopup(infoTMP);

                    if (info == null) {
                        info.setId(null);
                        info.setDescription(null);
                        info.setComponent_id(component.getId());
                        manager.saveInformationPopup(info);
                        info = manager.getInformationPopup(info);
                        
                    }
                    if (info != null) {
                        component.setInfo_popup_id(info.getId());
                    }

                    manager.saveComponent(component);
                } else if (component != null) {
                    component.setInfo_popup_id(0);//clear info popup
                    manager.saveComponent(component);
                }
                resourcesVO.setNames(names);
                managerBD.saveListItem(resourcesVO);
                //Get the new ListID:
                LookupVO new_id = managerBD.getListItem(resourcesVO);

                if (new_id != null) {
                    for (LookupLocalizationVO n : names) {

                        n.setLookup_id(new_id.getId());
                        managerBD.saveLocalization(n);

                    }
                }

                ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                TableUpdatesVO tu = cmanager.getTableUpdates();
                tu.setResources(new Integer(pdate.getEpochTime() + ""));
                cmanager.saveTableUpdates(tu);
                tu.setListdata(new Integer(pdate.getEpochTime() + ""));
                cmanager.saveTableUpdates(tu);
                tu.setListdata(new Integer(PDate.getEpochTime() + ""));
                cmanager.saveTableUpdates(tu);
                if (Integer.parseInt((String) request.getParameter("resource_id")) == resourcesVO.TREATMENT_LOCATION_INT) {
                    ListsDAO dao = new ListsDAO();
                    resourcesVO.setId(null);
                    LookupVO tmpVO = (LookupVO) dao.findByCriteria(resourcesVO);
                    ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
                    ListServiceImpl bd = new ListServiceImpl(language);
                    if (resourcesAdminForm.getItem_filter() == 0 || resourcesAdminForm.getItem_filter() == 1) {
                        Vector<ProfessionalVO> users = pservice.findAllBySearch(0, "", "", 0, tmpVO.getId() + "", null, "");//int id, String first_name, String last_name, String position, String treatment_id
                        for (ProfessionalVO user : users) {
                            // 0 - No update access
                            // 1 - Update access
                            boolean update_access = false;
                            //check if this user has any other treatment locations with update access before switching.
                            Vector<LookupVO> treatmentVO2 = (Vector<LookupVO>) bd.getLists(LookupVO.TREATMENT_LOCATION, user, false);
                            for (LookupVO item : treatmentVO2) {
                                if (item.getUpdate_access().equals("1")) {
                                    update_access = true;
                                }
                            }
                            if (update_access == false) {
                                user.setAccess_create_patient(0);
                                pservice.saveProfessional(user, userVO.getId());
                            } else {
                                user.setAccess_create_patient(1);
                                pservice.saveProfessional(user, userVO.getId());
                            }
                        }
                    }
                    ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
                    UserAccountRegionsVO r = new UserAccountRegionsVO();
                    r.setUser_account_id(userVO.getId());
                    r.setTreatment_location_id(tmpVO.getId());
                    userBD.saveTreatmentLocation(r);
                }

                com.pixalere.admin.service.ResourcesServiceImpl bd = new com.pixalere.admin.service.ResourcesServiceImpl();
                ResourcesVO vo = (ResourcesVO) bd.getResource(new Integer(request.getParameter("resource_id")));
                request.setAttribute("resource", vo);
                request.setAttribute("resource_id", request.getParameter("resource_id"));
                // if editing, grab form data, and edit list item
            } else if (request.getParameter("action") != null && request.getParameter("action").equals("edit")) {
                LookupVO resourcesVO = new LookupVO();

                resourcesVO.setLocked(0);
                resourcesVO.setId(new Integer(request.getParameter("id")));
                resourcesVO.setActive(new Integer(1));
                resourcesVO.setResourceId(new Integer(request.getParameter("resource_id")));
                resourcesVO.setCategoryId(new Integer(resourcesAdminForm.getCategoryId()));
                resourcesVO.setTitle(new Integer(resourcesAdminForm.getTitle()));
                resourcesVO.setModified_date(new java.util.Date());
                resourcesVO.setSoloBox(new Integer(resourcesAdminForm.getSoloBox()));
                resourcesVO.setOther(new Integer(resourcesAdminForm.getIsOther()));
                resourcesVO.setOrderby(new Integer(resourcesAdminForm.getOrderby()));
                resourcesVO.setUpdate_access(resourcesAdminForm.getItem_filter() + "");
                resourcesVO.setReport_value(resourcesAdminForm.getReport_value() + "");
                resourcesVO.setIcd9(resourcesAdminForm.getIcd9());
                resourcesVO.setIcd10(resourcesAdminForm.getIcd10());
                resourcesVO.setCpt(resourcesAdminForm.getCpt());
                if (resourcesAdminForm.getClinic() == 1) {
                    resourcesVO.setClinic(1);
                } else if (resourcesAdminForm.getClinic() == 0) {
                    resourcesVO.setClinic(0);
                }
                ListServiceImpl managerBD = new ListServiceImpl(language);

                Collection<LanguageVO> languages = managerBD.getLanguages();
                List<LookupLocalizationVO> names = new ArrayList();
                for (LanguageVO lang : languages) {
                    //if text exists save a new localized string.
                    if (request.getParameter(lang.getName()) != null && !((String) request.getParameter(lang.getName())).equals("")) {
                        LookupLocalizationVO n = new LookupLocalizationVO();
                        //n.setLookup_id(new_id.getId());
                        n.setName((String) request.getParameter(lang.getName()));
                        n.setLanguage_id(lang.getId());
                        n.setResource_id(resourcesVO.getResourceId());
                        names.add(n);
                    }
                }
                resourcesVO.setNames(names);
                managerBD.saveListItem(resourcesVO);
                //Get the new ListID:
                LookupVO new_id = managerBD.getListItem(resourcesVO);

                if (new_id != null) {
                    for (LookupLocalizationVO n : names) {

                        n.setLookup_id(new_id.getId());
                        managerBD.saveLocalization(n);

                    }
                }
                session.removeAttribute("listitem");

                ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();

                if (resourcesAdminForm.getItem_filter() == 0 || resourcesAdminForm.getItem_filter() == 1) {
                    Vector<ProfessionalVO> users = pservice.findAllBySearch(0, "", "", 0, new Integer(request.getParameter("id")) + "", null, "");//int id, String first_name, String last_name, String position, String treatment_id
                    for (ProfessionalVO user : users) {
                        // 0 - No update access
                        // 1 - Update access
                        boolean update_access = false;
                        //check if this user has any other treatment locations with update access before switching.
                        Vector<LookupVO> treatmentVO2 = (Vector<LookupVO>) managerBD.getLists(LookupVO.TREATMENT_LOCATION, user, false);
                        for (LookupVO item : treatmentVO2) {
                            if (item.getUpdate_access() != null && item.getUpdate_access().equals("1")) {
                                update_access = true;
                            }
                        }
                        if (update_access == false) {
                            user.setAccess_create_patient(0);
                            pservice.saveProfessional(user, userVO.getId());
                        } else {
                            user.setAccess_create_patient(1);
                            pservice.saveProfessional(user, userVO.getId());
                        }
                    }
                }
                ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setResources(new Integer(pdate.getEpochTime() + ""));
                cmanager.saveTableUpdates(t);
                t.setListdata(new Integer(pdate.getEpochTime() + ""));
                cmanager.saveTableUpdates(t);

                com.pixalere.admin.service.ResourcesServiceImpl bd = new com.pixalere.admin.service.ResourcesServiceImpl();
                ResourcesVO vo = (ResourcesVO) bd.getResource(new Integer(request.getParameter("resource_id")));
                request.setAttribute("resource", vo);
            } else if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("edit")) {
                try {
                    String item_id = request.getParameter("choice");
                    ListServiceImpl manager = new ListServiceImpl(language);
                    LookupVO lookupVO = (LookupVO) manager.getListItem(Integer.parseInt(item_id));
                    // request.setAttribute("resource", LookupVO);
                    session.setAttribute("listitem", lookupVO); // Quick fix because the request info gets lost

                    Collection<LanguageVO> languages = manager.getLanguages();
                    // On Manage Resources we only show main languages, no country localizations
                    // so we need to remove country locales from the list
                    for (Iterator<LanguageVO> it = languages.iterator(); it.hasNext();) {
                        LanguageVO languageVO = it.next();
                        if (languageVO.getName().indexOf("_") > 0) {
                            it.remove();
                        }
                    }

                    request.setAttribute("action", "edit");
                    request.setAttribute("resource_id", request.getParameter("resource_id"));
                    request.setAttribute("languages", languages);
                    ResourcesServiceImpl bd = new ResourcesServiceImpl();
                    if (request.getParameter("resource_id") == null) {
                        return mapping.findForward("admin.resources.success");
                    }
                    ResourcesVO vo = (ResourcesVO) bd.getResource(Integer.parseInt(request.getParameter("resource_id")));
                    if (vo != null) {
                        request.setAttribute("resource_section", vo.getName());
                    }
                    return (mapping.findForward("admin.resources.success"));
                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in resourcesAdminSetupAction.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
            } else if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("delete")) {
                try {
                    String item_id = request.getParameter("choice");
                    ListServiceImpl manager = new ListServiceImpl(language);
                    LookupVO lookupVO = new LookupVO();
                    lookupVO.setId(new Integer(item_id));
                    lookupVO = manager.getListItem(new Integer(item_id).intValue());
                    lookupVO.setActive(new Integer(0));
                    manager.saveListItem(lookupVO);
                    ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                    TableUpdatesVO t = cmanager.getTableUpdates();
                    t.setResources(new Integer(pdate.getEpochTime() + ""));
                    cmanager.saveTableUpdates(t);
                    t.setListdata(new Integer(pdate.getEpochTime() + ""));
                    cmanager.saveTableUpdates(t);
                    lookupVO = null;
                    request.setAttribute("resources", lookupVO);
                    request.setAttribute("action", "add");
                    request.setAttribute("resource_id", request.getParameter("resource_id"));
                    session.setAttribute("resource_id", request.getParameter("resource_id"));
                    ResourcesServiceImpl bd = new ResourcesServiceImpl();
                    // 22/07/06 SV: Disable because vo.getName() is causing null pointer exception
                    // ResourcesVO vo = (ResourcesVO) bd.retrieveItem(Integer.parseInt(request.getParameter("resource_id")));
                    // request.setAttribute("resource_section", vo.getName());
                } catch (ApplicationException e) {
                    log.error("An application exception has been raised in resources.perform(): " + e.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception", "y");
                    request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                    errors.add("exception", new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);
                    return (mapping.findForward("pixalere.error"));
                }
            }
        } catch (com.pixalere.common.DataAccessException e) {
            log.error("An application exception has been raised in ResourcesAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ResourcesAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.resources.success"));
        }
    }
}
