package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.ConfigurationVO;
import com.pixalere.utils.Serialize;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Vector;
import java.util.Hashtable;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.io.*;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.TableUpdatesVO;
public class ConfigurationAdmin extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConfigurationAdmin.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        ConfigurationAdminForm configurationAdminForm = (ConfigurationAdminForm) form;
        HttpSession session = request.getSession();
        ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
        if (request.getParameter("optionid") != null) {
            try {
                ConfigurationVO c = manager.getConfiguration(new Integer((String) request.getParameter("optionid")));
                if (c != null) {
                    request.setAttribute("configuration", c);
                }
            } catch (Exception e) {
            }
        } else if (configurationAdminForm.getId() > 0) {
            try {
                
                ConfigurationVO configuration = manager.getConfiguration(configurationAdminForm.getId());
                
                // Default language update? 
                if (configuration.getConfig_setting().equals(Common.DEFAULT_LANGUAGE_CONF) &&
                        !configuration.getCurrent_value().equals(configurationAdminForm.getValue())){
                    // Update information popup table
                    Common.translateInformationPopup(configurationAdminForm.getValue());
                }
                
                configuration.setReadonly(0);
                configuration.setCurrent_value(configurationAdminForm.getValue() != null ? configurationAdminForm.getValue() : "");
                if (configuration != null && 
                        configuration.getComponent_type().equals("CHKB") && 
                        (configurationAdminForm.getValue() == null || configurationAdminForm.getValue().equals(""))) {
                    configuration.setCurrent_value("0");
                }
                manager.saveConfiguration(configuration);
                TableUpdatesVO t = manager.getTableUpdates();
                t.setConfiguration(new Integer(new PDate().getEpochTime() + ""));
                manager.saveTableUpdates(t);
                
            } catch (ApplicationException e) {
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
            // Reload config
            Hashtable config = new Hashtable();
            try {
                config = manager.getConfiguration();
                Common.CONFIG = config;
            } catch (ApplicationException e) {
                log.error("DataAccessException in retrieveConfiguration: " + e.toString());
                e.printStackTrace();
            }
            session.setAttribute("config", config);
        } else if (configurationAdminForm.getLogo() != null) {
            try {
                InputStream in = configurationAdminForm.getLogo().getInputStream();
                File pathoff = new File(Common.getPhotosPath());
                if (!pathoff.exists()) {
                    try {
                        pathoff.mkdir();
                    } catch (Exception e) {
                      //  throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);
                    }
                }
            
            Common.writeToFile(Common.getPhotosPath() + "/logo.gif", in, true);
            } catch (FileNotFoundException ee) {
            }catch (IOException ee) {
            }
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.configuration.success"));
        }
    }
}
