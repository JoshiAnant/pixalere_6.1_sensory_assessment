/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.PDate;
/**
 *
 * @author travis
 */
public class PatientUnassignForm  extends ActionForm {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientUnassignForm.class);
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
    private int unassign_patient_id;
    /**
     * @return the unassign_patient_id
     */
    public int getUnassign_patient_id() {
        return unassign_patient_id;
    }
    /**
     * @param unassign_patient_id the unassign_patient_id to set
     */
    public void setUnassign_patient_id(int unassign_patient_id) {
        this.unassign_patient_id = unassign_patient_id;
    }
}
