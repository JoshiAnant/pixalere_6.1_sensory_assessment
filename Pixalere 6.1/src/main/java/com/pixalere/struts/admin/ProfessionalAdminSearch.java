/*
 * Copyright (c) 2006 Your Corporation. All Rights Reserved.
 */
package com.pixalere.struts.admin;

import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Vector;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.utils.*;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.auth.dao.UserDAO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.admin.service.ResourcesServiceImpl;
import java.util.Collection;

public class ProfessionalAdminSearch extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfessionalAdminSearch.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        ProfessionalAdminSearchForm proAdminForm = (ProfessionalAdminSearchForm) form;
        HttpSession session = request.getSession();
            
        Integer language = Common.getLanguageIdFromSession(session);

        try {
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            ProfessionalServiceImpl bd = new ProfessionalServiceImpl();
            Vector users = new Vector();
            ProfessionalVO accVO = new ProfessionalVO();
            accVO = (ProfessionalVO)session.getAttribute("userVO");
            //if(session.getAttribute("admin_pro_id")!=null){
            //    users=bd.findAllBySearch(new Integer((String)session.getAttribute("admin_pro_id")).intValue(),"","",-1,"");
            if (session.getAttribute("admin_pro_id") != null) {
                users = bd.findAllBySearch(new Integer((String) session.getAttribute("admin_pro_id")).intValue(), "", "", -1, "",null,"");
                session.removeAttribute("admin_pro_id");
                //if(session.getAttribute("admin_pro_id")!=null){
                //    users=bd.findAllBySearch(new Integer((String)session.getAttribute("admin_pro_id")).intValue(),"","",-1,"");
            } else if (!proAdminForm.getId().equals("") || !proAdminForm.getFirstNameSearch().equals("") || !proAdminForm.getLastNameSearch().equals("") || proAdminForm.getPosition() > 0 || !proAdminForm.getTreatmentLocation().equals("")) {
                int intProfessionalId = 0;
                if (!proAdminForm.getId().equals("")) {
                    //search by id
                    try {
                        intProfessionalId = Integer.parseInt(proAdminForm.getId());
                        UserDAO searchUserDAO = new UserDAO();
                        ProfessionalVO searchReturnVO = null;
                        ProfessionalVO searchuserVO = new ProfessionalVO();
                        searchuserVO.setId(new Integer(intProfessionalId));
                        searchReturnVO = (ProfessionalVO) searchUserDAO.findByCriteria(searchuserVO);
                        if (searchReturnVO != null) {
                            users.add(searchReturnVO);
                        }
                    } catch (NumberFormatException e) {
                        if (Common.getConfig("useUserNames").equals("1")) {
                            // Try to retrieve professional_id by username given
                            UserDAO searchUserDAO = new UserDAO();
                            ProfessionalVO searchReturnVO = null;
                            ProfessionalVO searchuserVO = new ProfessionalVO();
                            searchuserVO.setUser_name(proAdminForm.getId());
                            searchReturnVO = (ProfessionalVO) searchUserDAO.findByCriteria(searchuserVO);
                            if (searchReturnVO != null) {
                                users.add(searchReturnVO);
                            }
                        }
                    }
                } else {
                    users = bd.findAllBySearch(intProfessionalId, proAdminForm.getFirstNameSearch(), proAdminForm.getLastNameSearch(), proAdminForm.getPosition(), proAdminForm.getTreatmentLocation(),userVO.getRegions(),proAdminForm.getEmployeeIdSearch());
                }
                // Sort on "From", "To"
                for (int intSort1 = 0; intSort1 < users.size(); intSort1++) {
                    for (int intSort2 = 0; intSort2 < users.size() - intSort1 - 1; intSort2++) {
                        ProfessionalVO sortRec1 = (ProfessionalVO) users.get(intSort2);
                        ProfessionalVO sortRec2 = (ProfessionalVO) users.get(intSort2 + 1);
                        if ((sortRec1.getLastname().toUpperCase()
                                + sortRec1.getFirstname().toUpperCase()).compareTo(sortRec2.getLastname().toUpperCase()
                                + sortRec2.getFirstname().toUpperCase()) > 0) {
                            users.setElementAt(sortRec2, intSort2);
                            users.setElementAt(sortRec1, intSort2 + 1);
                        }
                    }
                }
            }
            //session.removeAttribute("admin_pro_id");
            request.setAttribute("professionalList", users);
            LookupVO vo = new LookupVO();
            ResourcesServiceImpl resourcesBD = new ResourcesServiceImpl();
            com.pixalere.common.service.ListServiceImpl lbd = new com.pixalere.common.service.ListServiceImpl(language);
            AssignPatientsServiceImpl assignService = new AssignPatientsServiceImpl();
            AssignPatientsVO assignTMP = new AssignPatientsVO();
            assignTMP.setProfessionalId(accVO.getId());
            Collection<AssignPatientsVO> assigned = assignService.getAllAssignedPatients(assignTMP);
            request.setAttribute("assigned",assigned);
            Vector treatmentWithCatsVO = (Vector) lbd.findAllTreatmentsWithCats(userVO);
            Vector treatmentCatsVO = (Vector) lbd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
            Vector treatmentVO = (Vector) lbd.getLists(vo.TREATMENT_LOCATION, userVO, false);
            request.setAttribute("treatmentListWithCats", treatmentWithCatsVO);
            request.setAttribute("treatmentCats", treatmentCatsVO);
            request.setAttribute("treatmentList", treatmentVO);
            request.setAttribute("positions", bd.getPositions(new PositionVO(Constants.ACTIVE)));
            request.setAttribute("timezone", lbd.getLists(vo.TIMEZONE));
        } catch (com.pixalere.common.DataAccessException e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        log.debug("Leaving PatientAdminSearch ****************");
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.professionalsearch.success"));
        }
    }
}
