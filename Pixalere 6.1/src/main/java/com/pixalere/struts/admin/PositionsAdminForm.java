package com.pixalere.struts.admin;
import com.pixalere.common.bean.ProductCategoryVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.common.*;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;

/**
 * @author 
 *
 */
public class PositionsAdminForm extends ActionForm {
	
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsAdminForm.class);
public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {
	  ActionErrors errors = new ActionErrors();
	
	  
	  return errors;
	}
	
	public void reset(ActionMapping mapping,
						 HttpServletRequest request) {
	  setId(0);
	  setTitle("");
          setReferral_popup(0);
          setRecommendation_popup(0);
	  setColour("");
          setEnforce_images(0);
	 	
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public PositionVO getFormData(String choice,int active){
		PositionVO vo = new PositionVO();
		if(!choice.equals("")){
			vo.setId(new Integer(choice));
		}
		vo.setTitle(getTitle());
		vo.setActive(new Integer(active));
                vo.setReferral_popup(getReferral_popup());
                vo.setRecommendation_popup(getRecommendation_popup());
                vo.setColour(getColour().trim().substring(0,7));
                vo.setOrderby(orderby);
                if(enforce_images<1){enforce_images=0;}
                vo.setEnforce_images(enforce_images);
		return vo;
	}
        private int orderby;
	private int id;
	private String title;
        private int referral_popup;
        private int recommendation_popup;
        private String colour;
        private int enforce_images;
        /**
   * @return the orderby
   */
  public Integer getOrderby() {
      return orderby;
  }
  /**
   * @param orderby the orderby to set
   */
  public void setOrderby(Integer orderby) {
      this.orderby = orderby;
  }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the referral_popup
     */
    public int getReferral_popup() {
        return referral_popup;
    }
    /**
     * @param referral_popup the referral_popup to set
     */
    public void setReferral_popup(int referral_popup) {
        this.referral_popup = referral_popup;
    }
    /**
     * @return the recommendation_popup
     */
    public int getRecommendation_popup() {
        return recommendation_popup;
    }
    /**
     * @param recommendation_popup the recommendation_popup to set
     */
    public void setRecommendation_popup(int recommendation_popup) {
        this.recommendation_popup = recommendation_popup;
    }
    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }
    /**
     * @param colour the colour to set
     */
    public void setColour(String colour) {
        this.colour = colour;
    }

    /**
     * @return the enforce_images
     */
    public int getEnforce_images() {
        return enforce_images;
    }

    /**
     * @param enforce_images the enforce_images to set
     */
    public void setEnforce_images(int enforce_images) {
        this.enforce_images = enforce_images;
    }
}
	  
