package com.pixalere.struts.admin;

import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.Constants;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.dao.WoundAssessmentDAO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Hashtable;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.PDate;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PatientHome extends Action {
    private static final Logger log = Logger.getLogger(PatientHome.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("page", "admin");
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        try {
            LookupVO vo = new LookupVO();
            PatientServiceImpl pservice = new PatientServiceImpl(language);
            ListServiceImpl bd = new ListServiceImpl(language);
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            ReferralsTrackingServiceImpl rservice = new ReferralsTrackingServiceImpl(language);
            ProfessionalServiceImpl profService = new ProfessionalServiceImpl();
            AssignPatientsServiceImpl aService = new AssignPatientsServiceImpl();
            WoundServiceImpl wservice = new WoundServiceImpl(language);
            WoundAssessmentServiceImpl aservice = new WoundAssessmentServiceImpl();
            //Generate Token - prevent multiple posts
            saveToken(request);
            if (session.getAttribute("patient_id") != null || session.getAttribute("admin_patient_id") != null) {
                Date now = new Date();
                PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
                request.setAttribute("backdated_year", pdate.getYear(now));
                request.setAttribute("backdated_day", pdate.getDay(now));
                request.setAttribute("backdated_month", pdate.getMonthNum(now));
                WoundAssessmentDAO was = new WoundAssessmentDAO();
                PatientAccountVO patient = (PatientAccountVO) session.getAttribute("patientAccount");
                if (patient != null) {
                    Collection<WoundVO> wounds = wservice.getWounds(patient.getPatient_id());
                          
                    if (Common.getConfig("purgeReferrals").equals("1")) {
                        //check if outstanding Recommendations exist.
                        //retrieve wounds of Patient.
                        try {
                            
                            Vector recommendations = new Vector();
                            for (WoundVO wound : wounds) {
                                ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                                rtmp.setWound_id(wound.getId());
                                rtmp.setEntry_type(Constants.RECOMMENDATION);
                                rtmp.setCurrent(Constants.ACTIVE);
                                ReferralsTrackingVO referral = rservice.getReferralsTracking(rtmp);
                                if (referral != null) {
                                    recommendations.add(wound.getWound_location_detailed());
                                }
                                if(referral == null){
                                    rtmp = new ReferralsTrackingVO();
                                    rtmp.setWound_id(wound.getId());
                                    rtmp.setEntry_type(Constants.REFERRAL);
                                    rtmp.setCurrent(Constants.ACTIVE);
                                    referral = rservice.getReferralsTracking(rtmp);
                                    if (referral != null) {
                                        recommendations.add(wound.getWound_location_detailed());
                                    }
                                }
                            }
                            if (recommendations != null && recommendations.size() > 0) {
                                request.setAttribute("recommendations", recommendations);
                            }
                        } catch (ApplicationException e) {
                            log.error(e.getMessage());
                        }
                    }
                    int error = pservice.validatePatientId(userVO.getId(), patient.getPatient_id());
                    
                   
                    request.setAttribute("access_patient", error);
                    String[] nums = Common.getConfig("phnFormat").split("-");
                    Vector phns = pservice.getPHN(patient, nums);
                    if (patient.getOutofprovince().equals(new Integer(1)) && phns.size() == 1) {
                        request.setAttribute("Editphn11", phns.get(0));
                    }else  {
                        request.setAttribute("Editphn1_1", phns);
                    } 
                }
            } else {
                
               //if we're redirecting to Add?   Capture Add event..
               if (session.getAttribute("patientAccount") != null) {
                    PatientAccountVO patient = (PatientAccountVO) session.getAttribute("patientAccount");
                    
                    String[] nums = Common.getConfig("phnFormat").split("-");
                    Vector phns = pservice.getPHN(patient, nums);
                    if (patient.getOutofprovince().equals(new Integer(1)) && phns.size() == 1) {
                        request.setAttribute("Addphn11", phns.get(0));
                    }else  {
                        request.setAttribute("Addphn1_1", phns);
                    } 
                    request.setAttribute("patientAccountAdd", (PatientAccountVO) session.getAttribute("patientAccount"));
                }
            }
            Collection assess = null;
            try {
                if (session.getAttribute("patient_id") != null) {
                    WoundAssessmentDAO was = new WoundAssessmentDAO();
                    WoundAssessmentVO wass = new WoundAssessmentVO();
                    wass.setPatient_id(new Integer(session.getAttribute("patient_id") + ""));
                    wass.setActive(Integer.valueOf(1));
                    assess = was.findAllByCriteriaByRange(wass, "", "");
                    if (assess != null) {
                        List filterVisits = aservice.getVisitCount(assess, null,null, language);
                        if (filterVisits != null) {
                            request.setAttribute("visit", filterVisits);
                        }
                    }
                }
            } catch (Exception e) {
            }
            if (userVO.getAccess_assigning().equals(new Integer(1))) {
                    
                    ProfessionalVO pt = new ProfessionalVO();
                    pt.setAccount_status(Integer.valueOf(1));
                    //request.setAttribute("professionals", profService.findAllBySearch(userVO.getId().intValue(), "", "", -1, ""));
                    AssignPatientsVO atmp = new AssignPatientsVO();
                    atmp.setProfessionalId(Integer.valueOf(userVO.getId()));
                    request.setAttribute("unassigned_patients", aService.getAllAssignedPatients(atmp));
                
            }
            request.setAttribute("inactivate_reason", bd.getLists(169));
            
            request.setAttribute("funding_source", bd.getLists(LookupVO.FUNDING_SOURCE));

            Vector treatmentCatsVO = bd.getLists(LookupVO.TREATMENT_CATEGORY, userVO, false);
            Vector treatmentAllCatsVO = bd.getLists(LookupVO.TREATMENT_CATEGORY, null, false);
            Vector treatmentVO = bd.getLists(LookupVO.TREATMENT_LOCATION, null, false);
           
            request.setAttribute("treatmentCats", treatmentCatsVO);
            request.setAttribute("treatmentAllCats", treatmentAllCatsVO);
            request.setAttribute("treatmentList", treatmentVO);
            Vector treatmentVOList = null;
            //Vector treatmentAllList = (Vector)bd.findAllTreatmentsWithCats(null);
            if(userVO.getTraining_flag() == null || userVO.getTraining_flag() != 1){
               treatmentVOList =  (Vector) bd.findAllTreatmentsWithCats(userVO);
            }else{
                treatmentVOList =  (Vector) bd.findAllTrainingTreatments(userVO);
                //treatmentAllList = treatmentVOList;
            }
            
            
            request.setAttribute("transferTreatmentList",treatmentVOList);
            request.setAttribute("treatmentList2", treatmentVOList);
            request.setAttribute("professionalAccount", userVO);
            
            request.setAttribute("woundGoals", bd.getLists(86));
            request.setAttribute("drainGoals", bd.getLists(168));
            request.setAttribute("ostomyGoals", bd.getLists(87));
            request.setAttribute("timezone", bd.getLists(LookupVO.TIMEZONE));
            ListsDAO listDAO = new ListsDAO();
            GUIServiceImpl gui = new GUIServiceImpl();
            ComponentsVO comp = new ComponentsVO();
            comp.setFlowchart(Integer.valueOf(1));
            Collection components = gui.getAllComponents(comp);
            Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);
            vo.setResourceId(new Integer(7));
            request.setAttribute("gender", listDAO.findAllByCriteria(vo, true));
            vo.setResourceId(new Integer(4));
            request.setAttribute("patientResidence", listDAO.findAllByCriteria(vo, true));
        } catch (Exception e) {
            log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return mapping.findForward("pixalere.error");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("patient"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.patient");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("woundprofiles"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.profile");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("assess"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.assess");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("viewer"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.viewer");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("treatment"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.treatment");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("summary"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.summary");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("reporting"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.reporting");
        }
        if ((request.getParameter(
                "page") != null) && (request.getParameter("page").equals("admin"))) {
            session.removeAttribute("admin_patient_id");
            return mapping.findForward("uploader.go.admin");
        }
        return mapping.findForward("admin.patient.success");
    }
}