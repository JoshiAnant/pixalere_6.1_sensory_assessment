/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.utils.Common;
import java.util.Vector;
import com.pixalere.utils.PDate;
import java.util.Date;
import java.util.Collection;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 * A {@link org.apache.struts.action.Action} action which creates patients
 *
 * @since 5.0
 * @author travis morris
 */
public class PatientCreate extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientCreate.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PatientCreateForm patientCreateForm = (PatientCreateForm) form;
        HttpSession session = request.getSession();

        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
            boolean tokenValid = isTokenValid(request);
            if (tokenValid) {
                resetToken(request);
                com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
                PDate pdate = new PDate(userVO.getTimezone());
                PatientServiceImpl managerBD = new PatientServiceImpl(language);
                PatientAccountVO accountVO = patientCreateForm.getFormData(null, request);
                accountVO.setAccount_status(new Integer(1));
                // added for search by professional
                accountVO.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale));
                accountVO.setCreated_on(new Date());
                accountVO.setCreated_by(((ProfessionalVO) session.getAttribute("userVO")).getId());
                accountVO.setIsAdmitted(1);
                accountVO.setAction("");
                accountVO.setVisits(new Integer("0"));

                Integer patient_id = null;
                if (accountVO.getPatient_id() == null && (request.getParameter("patient_account_id") != null && !((String) request.getParameter("patient_account_id")).equals(""))) {
                //meditech patients don't have assigned id's until they are moved.
                    //check if patient with same PHN already exists.
                    PatientAccountVO pa = new PatientAccountVO();
                    pa.setId(patientCreateForm.getPatient_account_id());

                    PatientAccountVO v = managerBD.getPatient(pa);

                    ActionErrors errors = new ActionErrors();
                    PatientAccountVO ptTMP = new PatientAccountVO();
                    if (v.getPhn2() != null && !v.getPhn2().equals("")) {
                        ptTMP.setPhn2(v.getPhn2());
                    } else {
                        ptTMP.setPhn(v.getPhn());
                    }
                    ptTMP.setCurrent_flag(1);
                    Collection<PatientAccountVO> patients = managerBD.getAllPatients(ptTMP);
                    //System.out.println(patientCreateForm.getPatient_account_id()+"Size:"+patients.size()+" "+v.getPhn());
                    if (patients != null && patients.size() > 1) {//would be 1 record if its only current.. greater means one already exists.
                        boolean already = false;
                        for (PatientAccountVO acc : patients) {
                            //System.out.println("error2: patient: "+v.getId()+" "+acc.getId()+" "+acc.getPatient_id());
                            if (!acc.getId().equals(v.getId()) && acc.getPatient_id() != null) {
                                int error = managerBD.validatePatientId(userVO.getId().intValue(), acc.getPatient_id().intValue());
                                String treatment_location = "";
                                if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                                    treatment_location = Common.getLocalizedString("pixalere.TIP", locale);
                                } else {
                                    LookupVO listVO = acc.getTreatmentLocation();
                                    treatment_location = listVO.getName(language);
                                }
                                Object[] patientinfo = {"Not Assigned" + "", treatment_location, "PHN's: " + acc.getPhn() + " " + v.getPhn()};
                                errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_meditech", patientinfo));
                                already = true;
                            } else {
                                String treatment_location = "";
                                if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                                    treatment_location = Common.getLocalizedString("pixalere.TIP", locale);
                                } else {
                                    LookupVO listVO = acc.getTreatmentLocation();
                                    treatment_location = listVO.getName(language);
                                }
                                Object[] patientinfo = {"Not Assigned" + "", treatment_location, "PHN's: " + acc.getPhn() + " " + v.getPhn()};
                                errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form.mrnexists_meditech", patientinfo));
                                already = true;
                            }
                        }
                        //found a patient with the same PHN which isnt the same one we just tried to transfer
                        if (errors != null && errors.size() > 0) {
                        //request.setAttribute("action", "transfer");
                            //return (mapping.findForward("admin.patientcreate.success"));
                        }
                    } else {
                        PatientAccountVO old_patient_record = managerBD.getPatient(pa);
                        old_patient_record.setCurrent_flag(0);
                        managerBD.updatePatient(old_patient_record);//clear old out record
                    }
                    patient_id = managerBD.addPatient(accountVO);
                    accountVO.setPatient_id(patient_id);
                    Common.invalidatePatientSession(session);
                    session.setAttribute("patient_id", patient_id + "");
                    patientCreateForm.setPatientId(patient_id);
                } else {
                    //create patient, and get new patient_id
                    patient_id = managerBD.addPatient(accountVO);
                    Common.invalidatePatientSession(session);
                    session.setAttribute("patient_id", patient_id + "");
                }
                //@todo we redirect to PatientHoe, should be able to delete everythingi below?
                // Populate treatment list
                LookupVO vo = new LookupVO();
                ListServiceImpl bd = new ListServiceImpl(language);
                Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
                Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
                request.setAttribute("treatmentCats", treatmentCatsVO);
                request.setAttribute("treatmentList", treatmentVO);
                LookupVO vo2 = new LookupVO();
                ListsDAO listDAO = new ListsDAO();
                vo2.setResourceId(new Integer(vo2.PATIENT_RESIDENCE));
                //Populate all the tabs.
                request.setAttribute("patientResidence", listDAO.findAllByCriteria(vo2));
                ProfessionalServiceImpl profService = new ProfessionalServiceImpl();
                AssignPatientsServiceImpl aService = new AssignPatientsServiceImpl();
                if (session.getAttribute("patient_id") != null) {
                    if (userVO.getAccess_assigning().equals(new Integer(1))) {
                        ProfessionalVO pt = new ProfessionalVO();
                        pt.setAccount_status(1);
                        request.setAttribute("professionals", profService.findAllBySearch(userVO.getId(), "", "", -1, "", null,""));
                        AssignPatientsVO atmp = new AssignPatientsVO();
                        atmp.setPatientId(patient_id);
                        request.setAttribute("professionalsa", aService.getAllAssignedPatients(atmp));
                    }
                    request.setAttribute("inactivate_reason", bd.getLists(LookupVO.INACTIVATE_REASON));
                }
                vo2.setResourceId(new Integer(vo.GENDER));
                request.setAttribute("gender", listDAO.findAllByCriteria(vo2));
                String patient_id_get = session.getAttribute("patient_id") + "";
                PatientAccountVO patientVOGet = (PatientAccountVO) managerBD.getPatient(Integer.parseInt(patient_id_get));
                // START CHECK ACCESS PERMISSIONS
                boolean access = false;
                if (patientVOGet.getTreatment_location_id() == -1 || profService.validate(userVO.getRegions(), patientVOGet.getTreatment_location_id()) == true) {
                    access = true;
                }
                AssignPatientsServiceImpl apManagerBD = new AssignPatientsServiceImpl();
                AssignPatientsVO apvo = new AssignPatientsVO();
                apvo.setPatientId(patientVOGet.getPatient_id());
                apvo.setProfessionalId(userVO.getId());
                AssignPatientsVO findAPVO = apManagerBD.getAssignedPatient(apvo);
                if (findAPVO != null) {
                    access = true;
                }
                session.setAttribute("patientAccount", patientVOGet);
                request.setAttribute("patientAccess", access);
            }
        } catch (Exception e) {
            System.out.println("error in pa:" + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        return (mapping.findForward("admin.patientcreate.success"));

    }
}
