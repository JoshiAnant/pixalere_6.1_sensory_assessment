package com.pixalere.struts.admin;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.utils.Constants;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.service.LicenseServiceImpl;
import com.pixalere.utils.Common;

public class ProfessionalAdminSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfessionalAdminSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        ProfessionalVO u = (ProfessionalVO) session.getAttribute("userVO");
        request.setAttribute("page", "admin");
        String pro_id = request.getParameter("choice");
        request.setAttribute("action", "add");
        request.setAttribute("professionalId", "0");
        ProfessionalServiceImpl manager = new ProfessionalServiceImpl();
        if (u == null) {//session is invalid.. return to login
            request.setAttribute("session_invalid", "yes");
            return (mapping.findForward("system.logoff"));
        }
        LicenseServiceImpl lservice = new LicenseServiceImpl();
        String num_users = lservice.getNumUsers();
        int count = manager.getCount();
        if(num_users!=null && !num_users.equals("")){
            try{
                int license_number = Integer.parseInt(num_users);
                if(count >= (license_number+1)){//add 1 so #7 admin user is not affected.
                    request.setAttribute("disable_create_user","true");
                    request.setAttribute("license_number",license_number);
                }else if((count+5) >= (license_number+1)){
                    request.setAttribute("warning_create_user","true");
                    request.setAttribute("license_number",((license_number+1) - count));
                }
            }catch(NumberFormatException e){
                request.setAttribute("disable_create_user","true");
                request.setAttribute("license_number",0);
            }
        }
        if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("unlock")) {
            try {
                ProfessionalVO usr = manager.getProfessional(Integer.parseInt((String) request.getParameter("choice")));
                if (usr != null) {
                    usr.setLocked(0);
                    usr.setInvalid_password_count(0);
                    manager.saveProfessional(usr,u.getId());
                    ActionErrors errors = new ActionErrors();
                    errors.add("unlocked", new ActionError("pixalere.common.unlocked"));
                    saveErrors(request, errors);
                }
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in ProfessionalAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        } else if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("edit")) {
            try {
                com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
                ProfessionalVO usr = manager.getProfessional(Integer.parseInt((String) request.getParameter("choice")));
                request.setAttribute("professionalAccount", usr);
                request.setAttribute("action", "edit");
                request.setAttribute("professionalId", pro_id);
                //Vector vecRegions=Serialize.arrayIze(usr.getRegions());
                String record = "0";
                List<LookupVO> professionalRegions = new ArrayList<LookupVO>();
                int[] reg = LookupVO.TREATMENT_LOCATION;
                for (int i = 0; i < reg.length; i++) {
                    //faster to get all with 1 connection and filter then than to make 20 connections
                    //getting 1 at a time.  Most with access to prof accounts have
                    //80+ treatment locations meaning 80+ db connectiosn in a matter of seconds.
                    Collection<LookupVO> treats = bd.getLists(reg[i]);
                    for (LookupVO t : treats) {
                        //System.out.println("T: "+t.getName()+" "+manager.validate(usr.getRegions(), t.getId()));
                        if (manager.validate(usr.getRegions(), t.getId()) == true) {
                            
                                    t.setResourceId(new Integer(0));
                                    t.setTitle(new Integer(0));
                                    professionalRegions.add(t);
                            
                        }
                    }
                    //shouldn't need to do this, but just in case its still there.
                    if(usr.getTraining_flag().equals("0")){
                            for(LookupVO t : professionalRegions){
                                if(t.getId() == Constants.TRAINING_TREATMENT_ID){
                                    professionalRegions.remove(t);
                                    break;
                                }
                            }
                        
                        }
                }
                ///////////////Collections.sort(professionalRegions,new LookupVO());
                //consolidating the two databases into 1.
                LookupVO trainingTL = bd.getListItem(Constants.TRAINING_TREATMENT_ID);
                request.setAttribute("trainingTreatment", trainingTL);//setting the training_treatment_location
                request.setAttribute("professionalRegions", professionalRegions);
                LookupVO vo2 = bd.getListItem(new Integer(record).intValue());
                if (vo2 != null) {
                    request.setAttribute("resourceId", vo2.getResourceId());
                }
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in ProfessionalAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        } else if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("delete")) {
            try {
                ProfessionalVO userVO = new ProfessionalVO();
                userVO.setId(new Integer(pro_id));
                userVO.setDeleted_on(new Date());
                userVO.setLastmodified_on(new Date());
                manager.removeProfessional(userVO,u.getId());
                userVO = null;
                request.setAttribute("professionalAccount", userVO);
                request.setAttribute("action", "add");
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in ProfessionalAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        } else if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("enable")) {
            try {
                ProfessionalVO userVO = new ProfessionalVO();
                userVO.setId(new Integer(pro_id));
                ProfessionalVO activate = manager.getProfessional(new Integer(pro_id));
                activate.setDeleted_on(null);
                activate.setLastmodified_on(new Date());
                activate.setAccount_status(1);
                manager.saveProfessional(activate,u.getId());
                request.setAttribute("professionalAccount", activate);
                request.setAttribute("action", "add");
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in ProfessionalAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        }
        try {
            LookupVO vo = new LookupVO();
            com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

            Vector treatmentVO2 = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
            Vector treatmentWithCatsVO = (Vector) bd.findAllTreatmentsWithCats(userVO);
            Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
            Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
            request.setAttribute("timezone", bd.getLists(vo.TIMEZONE));
            request.setAttribute("treatmentListWithCats", treatmentWithCatsVO);
            request.setAttribute("treatmentCats", treatmentCatsVO);
            request.setAttribute("treatmentList", treatmentVO);
            request.setAttribute("treatmentList2", treatmentVO2);
            
            request.setAttribute("positions", manager.getPositions(new PositionVO(Constants.ACTIVE)));
            } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        try {

            if (request.getParameter("admin_pro_id") != null) {
                ProfessionalVO user = new ProfessionalVO();
                user.setId(new Integer((String) request.getParameter("admin_pro_id")));
                Collection prof = (Collection) manager.getProfessionals(user);
                request.setAttribute("professionalList", prof);
            }
        } catch (com.pixalere.common.ApplicationException e) {
            log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.professional.success"));
        }
    }
}
