/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionErrors;
import com.pixalere.auth.bean.ProfessionalVO;
public class ImportPatientsForm extends ActionForm{
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImportPatientsForm.class);
    private FormFile patients;
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        
        ActionErrors errors = new ActionErrors();
        if (getPatients()!=null && getPatients().getFileName().length()>0 && getPatients().getFileSize() == 0) {
            errors.add("file1",new ActionError("pixalere.woundassessment.error.filemissing1"));
        }

        return errors;
    }

    /**
     * @return the patients
     */
    public FormFile getPatients() {
        return patients;
    }

    /**
     * @param patients the patients to set
     */
    public void setPatients(FormFile patients) {
        this.patients = patients;
    }
}
