package com.pixalere.struts.admin;

import com.pixalere.common.ApplicationException;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.bean.SSORoleVO;
import com.pixalere.sso.service.OpenSAMLService;
import com.pixalere.sso.service.SSOProviderService;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Management UI for SSO Configuration Admin.
 *
 * @author Jose
 * @since 6.0
 */
public class SSORolesSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOLocationsSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        // To check session and know if ProviderId has been set
        HttpSession session = request.getSession();
        //Integer language = Common.getLanguageIdFromSession(session);
        
        if (request.getParameter("providerId") == null && session.getAttribute("providerId") == null) {
            // No provider go back to Providers list
            return (mapping.findForward("admin.ssoroles.failure"));
        }
        
        SSOProviderService service = new SSOProviderService();
        String s_p_id = request.getParameter("providerId") != null
                ? request.getParameter("providerId")
                : (String) session.getAttribute("providerId");
        session.setAttribute("providerId", null);

        int provider_id = Integer.parseInt(s_p_id);
        SSOProviderVO provider = service.getProviderById(provider_id);
        
        if (provider == null){
            // No provider go back to Providers list
            return (mapping.findForward("admin.ssoroles.failure"));
        }
        
        // Info tab
        String baseURL = OpenSAMLService.getBaseUrl(request);
        String urlConsumer = OpenSAMLService.getAssertionConsumerUrl(baseURL);
        request.setAttribute("urlConsumer", urlConsumer);
        String xmlMetadata = OpenSAMLService.generateMetadataXML(provider, urlConsumer);
        request.setAttribute("xmlMetadata", xmlMetadata);
        //String urlLogout = OpenSAMLService.getLogoutUrl(baseURL);
        //request.setAttribute("urlLogout", urlLogout);
        
        // Add or Edit form
        if (request.getParameter("action") != null && !request.getParameter("action").equals("updateConfig")) {
            SSORoleVO roleVO = new SSORoleVO();
            //ListServiceImpl listService = new ListServiceImpl(language);
            try {
                if (request.getParameter("action").equals("edit") && request.getParameter("roleId") != null){
                    // Get roleVO
                    String s_loc_id = request.getParameter("roleId");
                    
                    // Role id
                    int role_id = Integer.parseInt(s_loc_id);
                    roleVO = service.getSSORoleById(role_id);
                }
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in SSORolesSetupAction.execute(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
            
            request.setAttribute("role", roleVO);
            request.setAttribute("provider", provider);
            return (mapping.findForward("admin.ssoroles.edit"));
        }
        
        // Update attribute name
        if (request.getParameter("action") != null && request.getParameter("action").equals("updateConfig")) {
            String attrName = request.getParameter("attributeRoles");
            provider.setAttributeRoles((attrName == null? attrName : attrName.trim()));
            try {
                service.saveProvider(provider);
                // reload
                provider = service.getProviderById(provider_id);
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in SSOLocationsSetupAction.execute(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        }
        
        // General Table
        Collection<SSORoleVO> ssoRoles = provider.getSsoRoles();
        
        request.setAttribute("roles", ssoRoles);
        request.setAttribute("provider", provider);
        return (mapping.findForward("admin.ssoroles.success"));
    }
    
}
