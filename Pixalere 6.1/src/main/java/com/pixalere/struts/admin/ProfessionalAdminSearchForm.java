/*
 * Copyright (c) 2006 Your Corporation. All Rights Reserved.
 */
package com.pixalere.struts.admin;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;



/**
 * @author 
 *
 */
public class ProfessionalAdminSearchForm extends ActionForm {
	
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientAdminForm.class);


	public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {
	  ActionErrors errors = new ActionErrors();
	  
	  
	  return errors;

	}
	
	public void reset(ActionMapping mapping,
						 HttpServletRequest request) {

	  setFirstNameSearch("");
	  setLastNameSearch("");
	  setId("");
      setPosition(-1);
      setTreatmentLocation("");
      setEmployeeIdSearch("");

	}
	
	private String id;
	private String firstNameSearch;
	private String lastNameSearch;
    private int position;
    private String treatmentLocation;
	private String employeeIdSearch;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstNameSearch() {
		return firstNameSearch;
	}
	public void setFirstNameSearch(String firstNameSearch) {
		this.firstNameSearch = firstNameSearch;
	}
	public String getLastNameSearch() {
		return lastNameSearch;
	}
	public void setLastNameSearch(String lastNameSearch) {
		this.lastNameSearch = lastNameSearch;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}

    public String getTreatmentLocation() {
        return treatmentLocation;
    }

    public void setTreatmentLocation(String treatmentLocation) {
        this. treatmentLocation = treatmentLocation;
    }

    /**
     * @return the employeeIdSearch
     */
    public String getEmployeeIdSearch() {
        return employeeIdSearch;
    }

    /**
     * @param employeeIdSearch the employeeIdSearch to set
     */
    public void setEmployeeIdSearch(String employeeIdSearch) {
        this.employeeIdSearch = employeeIdSearch;
    }

}
