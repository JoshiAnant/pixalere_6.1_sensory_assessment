/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.pixalere.struts.admin;

import com.pixalere.admin.bean.VendorVO;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author travis
 */
public class VendorForm extends ActionForm {
	
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsAdminForm.class);
public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {
	  ActionErrors errors = new ActionErrors();
	
	  
	  return errors;
	}
	
	public void reset(ActionMapping mapping,
						 HttpServletRequest request) {
	  setId(0);
	  setTitle("");
          
	 	
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public VendorVO getFormData(){
		VendorVO vo = new VendorVO();
		
                vo.setBg_colour(bg_colour);
                vo.setHeader_colour(header_colour);
                vo.setName(title);
                
		return vo;
	}
	private int id;
	private String title;
        private String header_colour;
        private String bg_colour;
        private FormFile logo;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the header_colour
     */
    public String getHeader_colour() {
        return header_colour;
    }

    /**
     * @param header_colour the header_colour to set
     */
    public void setHeader_colour(String header_colour) {
        this.header_colour = header_colour;
    }

    /**
     * @return the bg_colour
     */
    public String getBg_colour() {
        return bg_colour;
    }

    /**
     * @param bg_colour the bg_colour to set
     */
    public void setBg_colour(String bg_colour) {
        this.bg_colour = bg_colour;
    }

    /**
     * @return the logo
     */
    public FormFile getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(FormFile logo) {
        this.logo = logo;
    }

    
}
