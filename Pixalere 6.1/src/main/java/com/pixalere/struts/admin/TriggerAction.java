/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.TriggerProductsVO;
import com.pixalere.common.bean.TriggerVO;
import com.pixalere.common.service.TriggerServiceImpl;
import java.util.Collection;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.utils.Common;
import java.util.ArrayList;
import java.util.Iterator;
/**
 *
 * @author travis
 */
public class TriggerAction  extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        TriggerForm triggerForm = (TriggerForm)form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
      
        TriggerServiceImpl tservice = new TriggerServiceImpl();
        ListServiceImpl lservice = new ListServiceImpl(language);
        GUIServiceImpl gservice = new GUIServiceImpl();
        try {
            String action = triggerForm.getAction();

            if(action!=null) {
                if(action.equals("create") || action.equals("edit")){
                    TriggerVO t = new TriggerVO();

                    t.setName(triggerForm.getName());
                    t.setDescription(triggerForm.getDescription());
                    t.setTrigger_type(triggerForm.getTrigger_type());
                    t.setWound_type(triggerForm.getWound_type());
                    t.setActive(triggerForm.getActive()==null?0:1);
                    
                    if (action.equals("edit")) {
                        t.setTrigger_id(triggerForm.getTrigger_id());
                    }
                    
                    tservice.saveTrigger(t);
                    // Reloading to make sure it was saved
                    t = tservice.getTrigger(t);

                    if(t!=null){
                        // Remove old products 
                        Iterator<TriggerProductsVO> it = t.getProduct_ids().iterator();
                        while (it.hasNext()){
                            TriggerProductsVO oldProductId = it.next();
                            tservice.deleteTriggerProducts(oldProductId);
                            it.remove();
                        }
                        // Save products selected, if any
                        if(triggerForm.getProduct_id()!=null){
                            for(Integer id : triggerForm.getProduct_id()){
                                TriggerProductsVO prod = new TriggerProductsVO();
                                prod.setTrigger_id(t.getTrigger_id());
                                prod.setProduct_id(id);
                                tservice.saveTriggerProducts(prod);
                            }
                        }
                    }
                } else if(action.equals("wizard")){
                    // Sub elements - Selectables for current component
                    ComponentsVO component = gservice.getComponent(new Integer(triggerForm.getComponent_id()));
                    Collection<LookupVO> lists = null;

                    if (component.getResource_id() > 0){
                        lists = lservice.getLists(component.getResource_id());
                    } else if (component.getComponent_type().equals("YN")) {
                        // we need to build a Yes/No selector
                        lists = new ArrayList<LookupVO>();

                        LookupVO voYes = new LookupVO();
                        int yesId = 2;
                        voYes.setId(yesId);
                        Collection<LookupLocalizationVO> textsYes = new ArrayList<LookupLocalizationVO>();
                        LookupLocalizationVO locYes = new LookupLocalizationVO();
                        locYes.setLanguage_id(language);
                        locYes.setName(Common.getLocalizedString("pixalere.yes", locale));
                        textsYes.add(locYes);
                        voYes.setNames(textsYes);
                        lists.add(voYes);

                        LookupVO voNo = new LookupVO();
                        int noId = (component.getField_name().equals("review_done"))? 0 : 1;
                        voNo.setId(noId);
                        Collection<LookupLocalizationVO> textsNo = new ArrayList<LookupLocalizationVO>();
                        LookupLocalizationVO locNo = new LookupLocalizationVO();
                        locNo.setLanguage_id(language);
                        locNo.setName(Common.getLocalizedString("pixalere.no", locale));
                        textsNo.add(locNo);
                        voNo.setNames(textsNo);
                        lists.add(voNo);
                    } else if (component.getComponent_type().equals("AT")){
                        // we need to build an AssessmentType selector
                        lists = new ArrayList<LookupVO>();

                        LookupVO voAssType1 = new LookupVO();
                        voAssType1.setId(1);
                        Collection<LookupLocalizationVO> textsNo1 = new ArrayList<LookupLocalizationVO>();
                        LookupLocalizationVO locNo1 = new LookupLocalizationVO();
                        locNo1.setLanguage_id(language);
                        locNo1.setName(Common.getLocalizedString("pixalere.woundassessment.form.full_assessment", locale));
                        textsNo1.add(locNo1);
                        voAssType1.setNames(textsNo1);
                        lists.add(voAssType1);

                        LookupVO voAssType0 = new LookupVO();
                        voAssType0.setId(0);
                        Collection<LookupLocalizationVO> textsNo0 = new ArrayList<LookupLocalizationVO>();
                        LookupLocalizationVO locNo0 = new LookupLocalizationVO();
                        locNo0.setLanguage_id(language);
                        locNo0.setName(Common.getLocalizedString("pixalere.woundassessment.form.partial_assessment", locale));
                        textsNo0.add(locNo0);
                        voAssType0.setNames(textsNo0);
                        lists.add(voAssType0);

                        LookupVO voAssType2 = new LookupVO();
                        voAssType2.setId(2);
                        Collection<LookupLocalizationVO> textsNo2 = new ArrayList<LookupLocalizationVO>();
                        LookupLocalizationVO locNo2 = new LookupLocalizationVO();
                        locNo2.setLanguage_id(language);
                        locNo2.setName(Common.getLocalizedString("pixalere.not_assessed_today", locale));
                        textsNo2.add(locNo2);
                        voAssType2.setNames(textsNo2);
                        lists.add(voAssType2);

                        LookupVO voAssType3 = new LookupVO();
                        voAssType3.setId(3);
                        Collection<LookupLocalizationVO> textsNo3 = new ArrayList<LookupLocalizationVO>();
                        LookupLocalizationVO locNo3 = new LookupLocalizationVO();
                        locNo3.setLanguage_id(language);
                        locNo3.setName(Common.getLocalizedString("pixalere.woundassessment.form.phone_visit", locale));
                        textsNo3.add(locNo3);
                        voAssType3.setNames(textsNo3);
                        lists.add(voAssType3);
                    } else if (component.getField_name().equals("co_morbidities")){
                        lists = new ArrayList<LookupVO>();
                        lists.addAll(lservice.getLists(LookupVO.COMORBIDITIES, null, false));
                    }

                    request.setAttribute("lists", lists);
                    return (mapping.findForward("admin.trigger.continue"));
                }
            }
        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("admin.trigger.success"));
    }
}