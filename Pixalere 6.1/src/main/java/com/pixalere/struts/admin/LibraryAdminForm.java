package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.LibraryVO;
import org.apache.struts.upload.FormFile;
/**
 *
 * @author travismorris
 */
public class LibraryAdminForm  extends ActionForm{
    private Integer id;
    private String name;
    private FormFile file;
    private Integer category_id;
    private Integer hide;
    private String list_action;
    private Integer orderby;
    private String media_type;
    private String url;
     public void setCategory(int category){
        this.category=category;
    }
    public int getCategory(){
        return category;
    }
    private int category;
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {
        HttpSession session=request.getSession();
        ActionErrors errors= new ActionErrors();
        if(getHide() == null){
            hide=0;
        }
        return errors;
    }
    public String getList_action(){
        return list_action;
    }
    public void setList_action(String list_action){
        this.list_action=list_action;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public Integer getId(){
        return id;
    }
    public void setId(Integer id){
        this.id=id;
    }
    public FormFile getFile(){
        return file;
    }
    public void setFile(FormFile file){
        this.file=file;
    }
    public Integer getOrderby(){
        return orderby;
    }
    public void setOrderby(Integer orderby){
        this.orderby=orderby;
    }
    /**
     * @return the category_id
     */
    public Integer getCategory_id() {
        return category_id;
    }
    /**
     * @param category_id the category_id to set
     */
    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }
    /**
     * @return the hide
     */
    public Integer getHide() {
        return hide;
    }
    /**
     * @param hide the hide to set
     */
    public void setHide(Integer hide) {
        this.hide = hide;
    }
    /**
     * @return the media_type
     */
    public String getMedia_type() {
        return media_type;
    }
    /**
     * @param media_type the media_type to set
     */
    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }
    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
