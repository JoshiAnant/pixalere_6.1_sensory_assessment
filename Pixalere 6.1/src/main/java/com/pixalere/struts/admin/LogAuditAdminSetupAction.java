package com.pixalere.struts.admin;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import com.pixalere.utils.PDate;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Vector;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
public class LogAuditAdminSetupAction extends Action {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LogAuditAdminSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        
        request.setAttribute("page","admin");
        
        try{
            ProfessionalServiceImpl userDAO=new ProfessionalServiceImpl();
            ProfessionalVO userVO=new ProfessionalVO();
            userVO = (ProfessionalVO)session.getAttribute("userVO");
            LogAuditServiceImpl lservice = new LogAuditServiceImpl();
         
            java.util.Date start_date = PDate.getDateFromString((String)request.getParameter("start_date"),false);
            java.util.Date end_date = PDate.getDateFromString((String)request.getParameter("end_date"),false);
            String table_name = (String)request.getParameter("table_filter");
            
            request.setAttribute("tables",lservice.getTableNames());
            if(request.getParameter("professional_id")!=null && !request.getParameter("professional_id").equals("")){
                request.setAttribute("professional_id",request.getParameter("professional_id"));
                try{
                    int id = new Integer((String)request.getParameter("professional_id"));
                    LogAuditVO ltmp = new LogAuditVO();
                    ltmp.setProfessionalId(id);
                    request.setAttribute("logs",lservice.getLogs(ltmp,start_date,end_date,table_name));
                }catch(NumberFormatException e){
                }
            }else if(request.getParameter("patient_id")!=null && !request.getParameter("patient_id").equals("")){
                
                PatientServiceImpl pservice = new PatientServiceImpl(com.pixalere.utils.Constants.ENGLISH);
                try {
                    PatientAccountVO p = pservice.getPatient(new Integer((String)request.getParameter("patient_id")));
                    session.setAttribute("patient_id", request.getParameter("patient_id"));
                    if (p != null) {
                        session.setAttribute("patientAccount", p);
                    }
                } catch (NumberFormatException e) {
                } catch (Exception e) {
                    
                }
                System.out.println("patient_id: "+request.getParameter("patient_id"));
                try{
                    int id = new Integer((String)request.getParameter("patient_id"));
                    LogAuditVO ltmp = new LogAuditVO();
                    ltmp.setPatientId(id);
                    request.setAttribute("logs",lservice.getLogs(ltmp,start_date,end_date,table_name));
                }catch(NumberFormatException e){
                }
            }else {
            }
            
            
        } catch(Exception e){
            log.error("An application exception has been raised in LogAuditAdminSetupAction.perform(): " + e.toString());
           ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        }       else if(request.getParameter("page")!=null &&request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else{
            return (mapping.findForward("admin.logaudit.success"));}
    }
}
