/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 * The action form, which maps the GUI form fields to the java bean.
 *
 * @since 5.0
 * @author travis morris
 */
public class DashboardReportForm  extends ActionForm {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DashboardReportForm.class);
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {
        HttpSession session=request.getSession();
        ActionErrors errors= new ActionErrors();
        return errors;
    }
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    private int id;
    private String email;
    private int[] treatmentLocation;
    private String wound_type;
    private int etiology_all;
    private int[] etiologies;
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * @return the treatmentLocation
     */
    public int[] getTreatmentLocation() {
        return treatmentLocation;
    }
    /**
     * @param treatmentLocation the treatmentLocation to set
     */
    public void setTreatmentLocation(int[] treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    /**
     * @return the wound_type
     */
    public String getWound_type() {
        return wound_type;
    }
    /**
     * @param wound_type the wound_type to set
     */
    public void setWound_type(String wound_type) {
        this.wound_type = wound_type;
    }
    /**
     * @return the etiology
     */
    public int getEtiology_all() {
        return etiology_all;
    }
    /**
     * @param etiology the etiology to set
     */
    public void setEtiology_all(int etiology_all) {
        this.etiology_all = etiology_all;
    }
    /**
     * @return the etiologies
     */
    public int[] getEtiologies() {
        return etiologies;
    }
    /**
     * @param etiologies the etiologies to set
     */
    public void setEtiologies(int[] etiologies) {
        this.etiologies = etiologies;
    }

}

