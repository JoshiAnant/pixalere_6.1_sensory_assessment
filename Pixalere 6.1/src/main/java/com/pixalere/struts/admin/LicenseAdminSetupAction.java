/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import com.pixalere.common.service.LicenseServiceImpl;
import com.pixalere.common.bean.LicenseVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Common;
/**
 *
 * @author travis
 */
public class LicenseAdminSetupAction  extends Action{
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
       LicenseServiceImpl manager=new LicenseServiceImpl();
        try{
            LicenseVO license = manager.getLicense();
            if(license!=null){
                request.setAttribute("license",license.getLicense());
                com.verhas.licensor.License licenseService = new com.verhas.licensor.License();
                licenseService.setLicenseEncoded(license.getLicense());
                request.setAttribute("view_license",licenseService.getLicenseString());
            }
        }catch (ApplicationException e){
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }catch (Exception e){
            
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }	else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.license.success"));
        }
    }

}
