/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.admin.bean.AssignPatientsVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import java.util.Date;
import com.pixalere.utils.Common;
import com.pixalere.auth.bean.ProfessionalVO;

/**
 * A {@link org.apache.struts.action.Action} action which sets up the patient
 * assign page.
 *
 * @since 5.0
 * @author travis morris
 */
public class PatientAssignAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientAssignAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PatientAssignForm patientAdminForm = (PatientAssignForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {

            PatientServiceImpl paservice = new PatientServiceImpl();
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

            if (userVO != null && userVO.getId() > 0 && (session.getAttribute("admin_patient_id")!=null)) {
                Integer patient_id = Integer.parseInt((String)session.getAttribute("admin_patient_id"));
                
                PatientAccountVO patient = paservice.getPatient(patient_id);
                if(patient!=null){
                    session.setAttribute("patientAccount",patient);
                }
                AssignPatientsVO assign = new AssignPatientsVO();
                AssignPatientsServiceImpl pservice = new AssignPatientsServiceImpl();
                assign.setPatientId(new Integer((String)session.getAttribute("admin_patient_id")));
                assign.setProfessionalId(userVO.getId());
                AssignPatientsVO check = pservice.getAssignedPatient(assign);
                
                if (check == null) {
                    assign.setCreated_on(new Date());
                    assign.setAssignedBy(userVO.getId());
                    pservice.saveAssignedPatient(assign);
                    request.setAttribute("saved_assign", "yes");
                    int error = paservice.validatePatientId(userVO.getId(), patient_id);
                    if (error == 0) {
                        session.setAttribute("patient_id", patient_id+"");
                        session.removeAttribute("admin_patient_id");
                    }
                }
            } else {
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", Common.getLocalizedString("pixalere.admin.form.assign_patients_error", locale));
                errors.add("exception", new ActionError("pixalere.admin.form.assign_patients_error"));
                saveErrors(request, errors);
            }

        } catch (Exception e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        log.debug("Leaving PatientAdminSearch ****************");
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            request.setAttribute("tab_select", "searchDiv");
            return (mapping.findForward("admin.patient.success"));
        }
    }
}
