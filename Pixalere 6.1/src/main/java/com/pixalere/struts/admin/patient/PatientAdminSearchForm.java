package com.pixalere.struts.admin.patient;

import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.patient.service.PatientServiceImpl;
import java.util.Iterator;
import com.pixalere.utils.*;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.patient.bean.PatientAccountVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;
import java.util.Collection;


/**
 * A {@link org.apache.struts.action.ActionForm} actionform for validating search criteria
 *
 * @since 5.0
 * @author travis morris
 */
public class PatientAdminSearchForm extends ActionForm {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientAdminSearchForm.class);

    private int id;
    private int treatmentlocationid;
    private int treatmentregion;
    private int idSearch;
    private String lastNameSearch;
    private String phn1_1;
    private String phn1_2;
    private String phn1_3;
    private String otherSearch;
    private String parisSearch;
    private String mrnSearch;
    //no validation needed.
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        HttpSession session=request.getSession();

        return errors;
    }

    public void reset(ActionMapping mapping,
            HttpServletRequest request) {
        setLastNameSearch("");
        setId(0);
        
        setTreatmentlocationid(0);
        setOtherSearch("");
        setPhn1_1("");
        setPhn1_2("");
        setPhn1_3("");
        setMrnSearch("");
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLastNameSearch() {
        return lastNameSearch;
    }
    public void setLastNameSearch(String lastNameSearch) {
        this.lastNameSearch = lastNameSearch;
    }


    

    public int getTreatmentlocationid() {
        return treatmentlocationid;
    }

    public void setTreatmentlocationid(int treatmentlocationid) {
        this.treatmentlocationid = treatmentlocationid;
    }
    public int getTreatmentregion() {
        return treatmentregion;
    }

    public void setTreatmentregion(int treatmentregion) {
        this.treatmentregion = treatmentregion;
    }

    public int getIdSearch() {
        return idSearch;
    }
    public void setIdSearch(int idSearch) {
        this.idSearch = idSearch;
    }

    public String getOtherSearch() {
        return otherSearch;
    }
    public void setOtherSearch(String otherSearch) {
        this.otherSearch = otherSearch;
    }

    public String getParisSearch() {
        return parisSearch;
    }
    public void setParisSearch(String parisSearch) {
        this.parisSearch = parisSearch;
    }

    public String getMrnSearch() {
        return mrnSearch;
    }
    public void setMrnSearch(String mrnSearch) {
        this.mrnSearch = mrnSearch;
    }

    /**
     * @return the phn1_1
     */
    public String getPhn1_1() {
        return phn1_1;
    }

    /**
     * @param phn1_1 the phn1_1 to set
     */
    public void setPhn1_1(String phn1_1) {
        this.phn1_1 = phn1_1;
    }

    /**
     * @return the phn1_2
     */
    public String getPhn1_2() {
        return phn1_2;
    }

    /**
     * @param phn1_2 the phn1_2 to set
     */
    public void setPhn1_2(String phn1_2) {
        this.phn1_2 = phn1_2;
    }

    /**
     * @return the phn1_3
     */
    public String getPhn1_3() {
        return phn1_3;
    }

    /**
     * @param phn1_3 the phn1_3 to set
     */
    public void setPhn1_3(String phn1_3) {
        this.phn1_3 = phn1_3;
    }
}
