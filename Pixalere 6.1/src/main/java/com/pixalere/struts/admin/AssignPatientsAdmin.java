package com.pixalere.struts.admin;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.utils.*;
import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/**
 * Saving Assign patients
 *
 * @version 5.0
 * @since 3.0
 * @author travis morris
 */
public class AssignPatientsAdmin extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssignPatientsAdmin.class);
    /**
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        AssignPatientsAdminForm assignPatientsAdminForm = (AssignPatientsAdminForm) form;
        try{
            HttpSession session = request.getSession();
            //if professional button was clicked, setup the page to assign the patient
            if(request.getParameter("professionalSubmit")!=null){
                session.setAttribute("assignProfessionalId",request.getParameter("professional"));
                request.setAttribute("selected",(String)request.getParameter("professional"));
                session.setAttribute("professionalSubmit",request.getParameter("professionalSubmit"));
            }
            //if the assigning patient button is clicked, save the assignment.
            if(request.getParameter("patientSubmit")!=null ){
                session.setAttribute("professionalSubmit","");
                AssignPatientsVO assignPatientsVO=new AssignPatientsVO();
                ProfessionalVO p = (ProfessionalVO)session.getAttribute("userVO");
                PDate pdate = new PDate();
                assignPatientsVO.setProfessionalId(new Integer((String)session.getAttribute("assignProfessionalId")));
                assignPatientsVO.setPatientId(new Integer(request.getParameter("patients")));
                assignPatientsVO.setAssignedBy( p.getId() );
                assignPatientsVO.setCreated_on(new Date());
                assignPatientsVO.setAccountType("patient");
                AssignPatientsServiceImpl managerBD=new AssignPatientsServiceImpl();
                managerBD.saveAssignedPatient(assignPatientsVO);
                //session.setAttribute("assignProfessionalId",request.getParameter("professional"));
                request.setAttribute("selected",session.getAttribute("assignProfessionalId"));
                session.setAttribute("assignProfessionalId",session.getAttribute("assignProfessionalId"));
                //session.setAttribute("professionalSubmit",request.getParameter("professionalSubmit"));
            }
            if(request.getParameter("action") != null && request.getParameter("action").equals("delete")){
                AssignPatientsServiceImpl managerBD = new AssignPatientsServiceImpl();
                AssignPatientsVO vo=new AssignPatientsVO();
                vo.setId(new Integer(request.getParameter("choice")));
                managerBD.removeAssignedPatient(vo);
                session.setAttribute("professionalSubmit","");
                //session.setAttribute("assignProfessionalId",request.getParameter("professional"));
                request.setAttribute("selected",session.getAttribute("assignProfessionalId"));
                session.setAttribute("assignProfessionalId",session.getAttribute("assignProfessionalId"));
            }
        } catch(ApplicationException e){
            log.error("An application exception has been raised in AssignPatientsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null &&request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.assignpatients.success"));
        }
    }
    
}
