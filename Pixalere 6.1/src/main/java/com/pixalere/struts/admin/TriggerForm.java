/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
/**
 *
 * @author travis
 */
public class TriggerForm extends ActionForm {
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TriggerForm.class);

	public ActionErrors validate(ActionMapping mapping,
								 HttpServletRequest request) {
	  ActionErrors errors = new ActionErrors();

	  return errors;
	}
	public void reset(ActionMapping mapping,
						 HttpServletRequest request) {
	  
	}
        private Integer active;
        private Integer trigger_id;
        private String name;
        private String description;
        private String wound_type;
        private String trigger_type;
        private String lookup_id;
        private Integer trigger_criteria_id;
        private String comparator;
        private String criteria_type;
        private String criteria;
        private String action;
        private String value;
        private Integer component_id;
        private Integer[] product_id;
        private Integer[] library_id;

    /**
     * @return the trigger_id
     */
    public Integer getTrigger_id() {
        return trigger_id;
    }

    /**
     * @param trigger_id the trigger_id to set
     */
    public void setTrigger_id(Integer trigger_id) {
        this.trigger_id = trigger_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the wound_type
     */
    public String getWound_type() {
        return wound_type;
    }

    /**
     * @param wound_type the wound_type to set
     */
    public void setWound_type(String wound_type) {
        this.wound_type = wound_type;
    }

    /**
     * @return the trigger_type
     */
    public String getTrigger_type() {
        return trigger_type;
    }

    /**
     * @param trigger_type the trigger_type to set
     */
    public void setTrigger_type(String trigger_type) {
        this.trigger_type = trigger_type;
    }

    /**
     * @return the trigger_criteria_id
     */
    public Integer getTrigger_criteria_id() {
        return trigger_criteria_id;
    }

    /**
     * @param trigger_criteria_id the trigger_criteria_id to set
     */
    public void setTrigger_criteria_id(Integer trigger_criteria_id) {
        this.trigger_criteria_id = trigger_criteria_id;
    }

    

    /**
     * @return the criteria_type
     */
    public String getCriteria_type() {
        return criteria_type;
    }

    /**
     * @param criteria_type the criteria_type to set
     */
    public void setCriteria_type(String criteria_type) {
        this.criteria_type = criteria_type;
    }

    /**
     * @return the criteria
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the comparator
     */
    public String getComparator() {
        return comparator;
    }

    /**
     * @param comparator the comparator to set
     */
    public void setComparator(String comparator) {
        this.comparator = comparator;
    }

    /**
     * @return the lookup_id
     */
    public String getLookup_id() {
        return lookup_id;
    }

    /**
     * @param lookup_id the lookup_id to set
     */
    public void setLookup_id(String lookup_id) {
        this.lookup_id = lookup_id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the component_id
     */
    public Integer getComponent_id() {
        return component_id;
    }

    /**
     * @param component_id the component_id to set
     */
    public void setComponent_id(Integer component_id) {
        this.component_id = component_id;
    }

    /**
     * @return the product_id
     */
    public Integer[] getProduct_id() {
        return product_id;
    }

    /**
     * @param product_id the product_id to set
     */
    public void setProduct_id(Integer[] product_id) {
        this.product_id = product_id;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return the library_id
     */
    public Integer[] getLibrary_id() {
        return library_id;
    }

    /**
     * @param library_id the library_id to set
     */
    public void setLibrary_id(Integer[] library_id) {
        this.library_id = library_id;
    }
}
