package com.pixalere.struts.admin;

import com.pixalere.common.ApplicationException;
import com.pixalere.sso.bean.SSOLocationVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.bean.SSORoleVO;
import com.pixalere.sso.service.OpenSAMLService;
import com.pixalere.sso.service.SSOProviderService;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Management UI for SSO Configuration Admin.
 *
 * @author Jose
 * @since 6.0
 */
public class SSOAdminSetupAction extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOAdminSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        
        // To check session and know if ProviderId has been set
        HttpSession session = request.getSession();
        
        SSOProviderService service = new SSOProviderService();
        
        if (request.getParameter("action") != null && request.getParameter("action").equals("add")) {
            // Saving new Provider stub
            SSOProviderVO newProvider = new SSOProviderVO();
            String identityId = request.getParameter("idpIdentityId");
            String ssoType = request.getParameter("ssoType");
            String description = request.getParameter("description");

            newProvider.setIdpIdentityId(identityId == null? identityId: identityId.trim());
            newProvider.setSsoType(ssoType);
            newProvider.setDescription(description);
            
            // Initialize to false all non-null values
            newProvider.setActive(0);
            newProvider.setIsAuthRequestSigned(0);
            newProvider.setIsAuthRequestEncrypted(0);
            newProvider.setIsAuthnResponseSigned(0);
            newProvider.setIsAuthResponseEncrypted(0);
            newProvider.setIsLogoutEnabled(0);
            newProvider.setIsLogoutRequestSigned(0);
            newProvider.setUseAttributeTraining(0);
            newProvider.setUseAttributeReporting(0);
            newProvider.setSelfConfRoles(0);
            newProvider.setSelfConfLocations(0);
            newProvider.setSelfConfPositions(0);
            newProvider.setOnlyMainField(0);
            newProvider.setAllowSSOUpdate(0);
            
            try {
                service.saveProvider(newProvider);
            } catch (ApplicationException ex) {
                log.error("An application exception has been raised in SSOAdminSetupAction (addding SSOProvider): " + ex.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception","y");
                request.setAttribute("exception_log",Common.buildException(ex));
                errors.add("exception",new ActionError("pixalere.common.error"));
                saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
            }
        } 

        if (request.getParameter("providerId") == null && session.getAttribute("providerId") == null) {
            // If IdP not defined display the existing SSOProviders List
            List<SSOProviderVO> providers = service.getAllProviders();
            SSOProviderVO provider = new SSOProviderVO();
            
            request.setAttribute("providers", providers);
            request.setAttribute("provider", provider);
            return (mapping.findForward("admin.ssolist.success"));

        } else {
            // Main/default Provider config page
            String s_p_id = request.getParameter("providerId") != null
                    ? request.getParameter("providerId")
                    : (String) session.getAttribute("providerId");
            session.setAttribute("providerId", null);

            int provider_id = Integer.parseInt(s_p_id);
            SSOProviderVO provider = service.getProviderById(provider_id);
            
            if (request.getParameter("action") != null && request.getParameter("action").equals("delete")) {
                try {
                    // Clean up all settings
                    service.removePositions(provider_id);
                    
                    Collection<SSOLocationVO> ssoLocations = provider.getSsoLocations();
                    Collection<SSORoleVO> ssoRoles = provider.getSsoRoles();
                    
                    for (Iterator<SSORoleVO> it = ssoRoles.iterator(); it.hasNext();) {
                        SSORoleVO roleVO = it.next();
                        service.removeRole(roleVO);
                        it.remove();
                    }
                    
                    for (Iterator<SSOLocationVO> it = ssoLocations.iterator(); it.hasNext();) {
                        SSOLocationVO locVO = it.next();
                        service.removeLocation(locVO);
                        it.remove();
                    }
                    
                    // Delete this provider
                    service.removeProvider(provider);
                } catch (ApplicationException ex) {
                    log.error("An application exception has been raised in SSOAdminSetupAction (deleting SSOProvider): " + ex.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception","y");
                    request.setAttribute("exception_log",Common.buildException(ex));
                    errors.add("exception",new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                }
                
                // Go back to SSOProviders List
                List<SSOProviderVO> providers = service.getAllProviders();
                provider = new SSOProviderVO();

                request.setAttribute("providers", providers);
                request.setAttribute("provider", provider);
                return (mapping.findForward("admin.ssolist.success"));
            } else {
                // Display specified Provider for update/edition
                if (session.getAttribute("confErrors") != null) {
                    List<String> errors = (List<String>) session.getAttribute("confErrors");
                    session.removeAttribute("confErrors");
                    request.setAttribute("confErrors", errors);
                }
                
                request.setAttribute("provider", provider);
                String baseURL = OpenSAMLService.getBaseUrl(request);
                String urlConsumer = OpenSAMLService.getAssertionConsumerUrl(baseURL);
                request.setAttribute("urlConsumer", urlConsumer);
                String xmlMetadata = OpenSAMLService.generateMetadataXML(provider, urlConsumer);
                request.setAttribute("xmlMetadata", xmlMetadata);
                //String urlLogout = OpenSAMLService.getLogoutUrl(baseURL);
                //request.setAttribute("urlLogout", urlLogout);
                
                return (mapping.findForward("admin.ssoconfig.success"));
            }
            
        }
    }
    
}