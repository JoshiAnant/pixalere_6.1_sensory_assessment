package com.pixalere.struts.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.bean.ProductsByFacilityVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.utils.Serialize;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.TableUpdatesVO;
import java.util.Collection;
import com.pixalere.common.ApplicationException;

import com.pixalere.utils.Common;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.bean.TriggerProductsVO;
import com.pixalere.common.bean.TriggerVO;
import com.pixalere.common.service.TriggerServiceImpl;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.PDate;
import java.io.File;
import java.io.InputStream;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

public class ProductsAdmin extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsAdmin.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

        String action = request.getParameter("action");
        String choice = request.getParameter("choice");

        try {
            ProductsAdminForm productsAdminForm = (ProductsAdminForm) form;

            // Insert or Edit Product data
            if (action != null && action.equals("add")) {

                ProductsServiceImpl prodBD = new ProductsServiceImpl();
                ProductsVO newProduct = new ProductsVO();
                ProductsVO oldProduct = null;

                if (!choice.equals("")) {
                    oldProduct = (ProductsVO) prodBD.getProduct(Integer.parseInt(choice));
                    if (oldProduct != null && oldProduct.getCost() != newProduct.getCost()) {
                        oldProduct.setActive(new Integer(0));
                        prodBD.saveProduct(oldProduct);
                        ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                        TableUpdatesVO t = cmanager.getTableUpdates();
                        t.setProducts(new Integer(new PDate().getEpochTime() + ""));
                        cmanager.saveTableUpdates(t);
                        if (oldProduct != null && oldProduct.getBaseId() != null) {
                            newProduct.setBaseId(oldProduct.getBaseId());
                        } else {
                            newProduct.setBaseId(Integer.parseInt(choice));
                        }
                        newProduct.setUid(oldProduct.getUid());
                    } else {
                        newProduct.setId(oldProduct.getId());
                        ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                        TableUpdatesVO t = cmanager.getTableUpdates();
                        t.setProducts(new Integer(new PDate().getEpochTime() + ""));
                        cmanager.saveTableUpdates(t);
                    }
                }
                newProduct.setAll_treatment_locations(productsAdminForm.getAll_treatment_location());
                newProduct.setTitle((productsAdminForm.getProduct().trim()).replace('\'', '`'));
                newProduct.setCost(productsAdminForm.getCost());
                newProduct.setCategory_id(new Integer(productsAdminForm.getProduct_category()));
                newProduct.setType(productsAdminForm.getType());
                newProduct.setQuantity(productsAdminForm.getQuantity());
                newProduct.setDescription(productsAdminForm.getDescription());
                newProduct.setCompany(productsAdminForm.getCompany());
                newProduct.setUrl(productsAdminForm.getUrl());
                // Set updated data
                newProduct.setDescription(productsAdminForm.getDescription());
                newProduct.setVideo_url(productsAdminForm.getVideo_url());
                newProduct.setDisplay_image(productsAdminForm.getDisplay_image());
                newProduct.setDisplay_description(productsAdminForm.getDisplay_description());
                newProduct.setRecommendation_type(productsAdminForm.getRecommendation_type());

                // If changed, update image else maintain old data
                if (productsAdminForm.getImage() != null && !productsAdminForm.getImage().getFileName().isEmpty()) {
                    File pathoff = new File(Common.getDownloadsPath() + "productimgs/");
                    if (!pathoff.exists()) {
                        try {
                            pathoff.mkdir();
                        } catch (Exception e) {
                            throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);
                        }
                    }
                    String imageName = PDate.getEpochTime() + "_" + productsAdminForm.getImage().getFileName();
                    InputStream in = productsAdminForm.getImage().getInputStream();
                    newProduct.setImage(imageName);
                    Common.writeToFile(Common.getDownloadsPath() + "productimgs/" + imageName, in, true);
                } else if (oldProduct != null) {
                    newProduct.setImage(oldProduct.getImage());
                }

                // If changed, update pdf else maintain old data
                if (productsAdminForm.getPdf() != null && !productsAdminForm.getPdf().getFileName().isEmpty()) {
                    File pathoff = new File(Common.getDownloadsPath() + "productpdfs/");
                    if (!pathoff.exists()) {
                        try {
                            pathoff.mkdir();
                        } catch (Exception e) {
                            throw new ApplicationException("Error in PdfManipulation.createFolders: " + e.toString(), e);
                        }
                    }
                    String pdfName = PDate.getEpochTime() + "_" + productsAdminForm.getPdf().getFileName();
                    InputStream in = productsAdminForm.getPdf().getInputStream();
                    newProduct.setPdf(pdfName);
                    Common.writeToFile(Common.getDownloadsPath() + "productpdfs/" + pdfName, in, true);
                } else if (oldProduct != null) {
                    newProduct.setPdf(oldProduct.getPdf());
                }

                newProduct.setActive(new Integer(1));
                newProduct.setLibrary_id(productsAdminForm.getLibrary_id());
                prodBD.saveProduct(newProduct);

                ProductsVO updatedProduct = prodBD.getProduct(newProduct);
                request.setAttribute("saved_product", updatedProduct);
                //Delete old treatment locations..
                if (updatedProduct != null && updatedProduct.getFacilities() != null) {
                    for (ProductsByFacilityVO facility : updatedProduct.getFacilities()) {
                        prodBD.removeProductsByFacility(facility);
                    }
                }
                if (productsAdminForm.getAll_treatment_location() != 1
                        && updatedProduct != null
                        && productsAdminForm.getTreatmentLocation() != null
                        && productsAdminForm.getTreatmentLocation().length > 0) {
                    for (int id : productsAdminForm.getTreatmentLocation()) {
                        ProductsByFacilityVO f = new ProductsByFacilityVO();
                        f.setProduct_id(updatedProduct.getId());
                        f.setTreatment_location_id(id);
                        prodBD.saveProductsByFacility(f);
                    }
                }
                ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setProducts(new Integer(new PDate().getEpochTime() + ""));
                cmanager.saveTableUpdates(t);

            } else if (action != null && action.equals("trigger_product") && !choice.equals("")) {

                // Update product recommendation (triggers)
                ProductsServiceImpl prodBD = new ProductsServiceImpl();
                ProductsVO oldProduct = (ProductsVO) prodBD.getProduct(Integer.parseInt(choice));
                ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                // Record change
                /*ProductsVO newProduct = new ProductsVO();
                 newProduct.setActive(oldProduct.getActive());
                 System.out.println("Old active: " + oldProduct.getActive());
                 System.out.println("New active: " + newProduct.getActive());
                 oldProduct.setActive(new Integer(0));
                 prodBD.saveProduct(oldProduct);

                
                 TableUpdatesVO t = cmanager.getTableUpdates();
                 t.setProducts(new Integer(new PDate().getEpochTime() + ""));
                 cmanager.saveTableUpdates(t);
                 if (oldProduct.getBaseId() != null) {
                 newProduct.setBaseId(oldProduct.getBaseId());
                 } else {
                 newProduct.setBaseId(Integer.parseInt(choice));
                 }*/

                // Recover previous data
                /*newProduct.setAll_treatment_locations(oldProduct.getAll_treatment_locations());
                 newProduct.setTitle(oldProduct.getTitle());
                 newProduct.setCost(oldProduct.getCost());
                 newProduct.setCategory_id(oldProduct.getCategory_id());
                 newProduct.setType(oldProduct.getType());
                 newProduct.setQuantity(oldProduct.getQuantity());
                 newProduct.setCompany(oldProduct.getCompany());
                 newProduct.setLibrary_id(oldProduct.getLibrary_id());*/
                // Set updated data
                oldProduct.setDescription(productsAdminForm.getDescription());
                oldProduct.setUrl(productsAdminForm.getUrl());
                oldProduct.setVideo_url(productsAdminForm.getVideo_url());
                oldProduct.setDisplay_image(productsAdminForm.getDisplay_image());
                oldProduct.setDisplay_description(productsAdminForm.getDisplay_description());
                oldProduct.setRecommendation_type(productsAdminForm.getRecommendation_type());

                // If changed, update image else maintain old data
                if (productsAdminForm.getImage() != null && !productsAdminForm.getImage().getFileName().isEmpty()) {
                    File pathoff = new File(Common.getDownloadsPath() + "productimgs/");
                    if (!pathoff.exists()) {
                        try {
                            pathoff.mkdir();
                        } catch (Exception e) {
                            throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);
                        }
                    }
                    String imageName = PDate.getEpochTime() + "_" + productsAdminForm.getImage().getFileName();
                    InputStream in = productsAdminForm.getImage().getInputStream();
                    oldProduct.setImage(imageName);
                    Common.writeToFile(Common.getDownloadsPath() + "productimgs/" + imageName, in, true);
                } else {
                    oldProduct.setImage(oldProduct.getImage());
                }

                // If changed, update pdf else maintain old data
                if (productsAdminForm.getPdf() != null && !productsAdminForm.getPdf().getFileName().isEmpty()) {
                    File pathoff = new File(Common.getDownloadsPath() + "productpdfs/");
                    if (!pathoff.exists()) {
                        try {
                            pathoff.mkdir();
                        } catch (Exception e) {
                            throw new ApplicationException("Error in PdfManipulation.createFolders: " + e.toString(), e);
                        }
                    }
                    String pdfName = PDate.getEpochTime() + "_" + productsAdminForm.getPdf().getFileName();
                    InputStream in = productsAdminForm.getPdf().getInputStream();
                    oldProduct.setPdf(pdfName);
                    Common.writeToFile(Common.getDownloadsPath() + "productpdfs/" + pdfName, in, true);
                } else {
                    oldProduct.setPdf(oldProduct.getPdf());
                }

                prodBD.saveProduct(oldProduct);
                ProductsVO updatedProduct = prodBD.getProduct(oldProduct);

                request.setAttribute("saved_product", updatedProduct);
                //Delete old treatment locations..
                if (updatedProduct != null && updatedProduct.getFacilities() != null) {
                    for (ProductsByFacilityVO facility : updatedProduct.getFacilities()) {
                        prodBD.removeProductsByFacility(facility);
                    }
                }
                // If applies, copy previous locations
                /*if (updatedProduct != null
                 && updatedProduct.getAll_treatment_locations() != 1
                 && oldProduct.getFacilities() != null
                 && oldProduct.getFacilities().size() > 0) {

                 for (ProductsByFacilityVO f : oldProduct.getFacilities()) {
                 ProductsByFacilityVO f2 = new ProductsByFacilityVO();
                 f2.setProduct_id(updatedProduct.getId());
                 f2.setTreatment_location_id(f.getTreatment_location_id());
                 prodBD.saveProductsByFacility(f2);
                 }
                 }*/
                cmanager = new ConfigurationServiceImpl();
                TableUpdatesVO t = cmanager.getTableUpdates();
                t.setProducts(new Integer(new PDate().getEpochTime() + ""));
                cmanager.saveTableUpdates(t);

                // Update ALL relevant Triggers to point to the newly updated product
                if (updatedProduct != null) {
                    TriggerServiceImpl tservice = new TriggerServiceImpl();
                    int old_product_id = oldProduct.getId();
                    int new_product_id = updatedProduct.getId();

                    tservice.updateAllProductTriggers(old_product_id, new_product_id);
                }

                String trigger_id = request.getParameter("trigger_id");

                // Redirect values upon saving (Remove? they don't do anything here)
                request.setAttribute("trigger_id", trigger_id);
                request.setAttribute("action", "products");
            }

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ProductsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (FileNotFoundException e) {
            log.error("An application exception has been raised in ProductsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (IOException e) {
            log.error("An application exception has been raised in ProductsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }

        if (action == null || !action.equals("trigger_product")) {
            try {
                ProductCategoryServiceImpl bd = new ProductCategoryServiceImpl();
                ProductsServiceImpl prodBD = new ProductsServiceImpl();
                ProductCategoryVO vo = new ProductCategoryVO();
                vo.setActive(1);

                request.setAttribute("productCategories", (Collection) bd.getAllProductCategoriesByCriteria(vo));
                request.setAttribute("productsList", (Collection) prodBD.getAllProducts(userVO));

            } catch (ApplicationException e) {
                log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        }

        if (request.getParameter("page") != null) {
            if (request.getParameter("page").equals("patient")) {
                return (mapping.findForward("uploader.go.patient"));
            } else if (request.getParameter("page").equals("woundprofiles")) {
                return (mapping.findForward("uploader.go.profile"));
            } else if (request.getParameter("page").equals("treatment")) {
                return (mapping.findForward("uploader.go.treatment"));
            } else if (request.getParameter("page").equals("assess")) {
                return (mapping.findForward("uploader.go.assess"));
            } else if (request.getParameter("page").equals("viewer")) {
                return (mapping.findForward("uploader.go.viewer"));
            } else if (request.getParameter("page").equals("summary")) {
                return (mapping.findForward("uploader.go.summary"));
            } else if (request.getParameter("page").equals("reporting")) {
                return (mapping.findForward("uploader.go.reporting"));
            } else if (request.getParameter("page").equals("admin")) {
                return (mapping.findForward("uploader.go.admin"));
            }
        }

        if (action != null && action.equals("trigger_product")) {
            return (mapping.findForward("admin.productstriggers.success"));
        } else {
            return (mapping.findForward("admin.products.success"));
        }

    }
}
