package com.pixalere.struts.admin;

import com.pixalere.auth.bean.ProfessionalVO;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ListServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.utils.Common;
import java.util.Vector;

/**
 * Landing class in Admin to redirect all the menu options.
 *
 * @since 3.2
 * @author travis morris
 * @see Action
 */
public class AdminSetupAction extends Action {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AdminSetupAction.class);

    /**
     * TODO: Look into passing patientAcocuntVO throw submit (edit) *
     */
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();

        if (session.getAttribute("userVO") == null) {
            return (mapping.findForward("uploader.null.patient"));
        }
        if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("patient_accounts")) {
            return (mapping.findForward("admin.patient.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("professional_accounts")) {
            return (mapping.findForward("admin.professional.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("import_products")) {
            return (mapping.findForward("admin.importproducts.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("resources")) {
            return (mapping.findForward("admin.resources.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("products")) {
            return (mapping.findForward("admin.products.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("productcats")) {
            return (mapping.findForward("admin.productcats.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("library")) {
            return (mapping.findForward("admin.library.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("productsinfo")) {
            return (mapping.findForward("admin.productsinfo.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("assign_patients")) {
            return (mapping.findForward("admin.assignpatients.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("components")) {
            return (mapping.findForward("admin.components.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("quarter")) {
            return (mapping.findForward("admin.quarter.success"));
        }
        if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("triggers")) {
            System.out.println("admin.trigger.success");
            return (mapping.findForward("admin.trigger.success"));
        } else if (request.getParameter("pixActionMenu") != null && ((String) request.getParameter("pixActionMenu")).equals("ccacreports")) {
            return (mapping.findForward("admin.ccacreports.success"));

       } else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("dashboard")) {
        return (mapping.findForward("admin.dashboard.success"));
        }else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("vendorsetup")) {
        return (mapping.findForward("admin.vendor.success"));
        } else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("storage")) {
        return (mapping.findForward("admin.storage.success"));
        } else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("info")) {
        return (mapping.findForward("admin.info.success"));
        } else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("positions")) {
        return (mapping.findForward("admin.positions.success"));
        }else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("configuration")) {
        return (mapping.findForward("admin.configuration.success"));
        }else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("license")) {
        return (mapping.findForward("admin.license.success"));
        } else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("user_list")) {
        return (mapping.findForward("admin.userlist.success"));
        }else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("maintenance")) {
        return (mapping.findForward("admin.maintenance.success"));
        }else if (request.getParameter("pixActionMenu") != null && ( (String) request.getParameter("pixActionMenu")).equals("sso_config")) {
        return (mapping.findForward("admin.ssolist.success"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
        return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")){
        return (mapping.findForward("uploader.go.profile"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("viewer")){
        return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("treatment")){
        return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
        return (mapping.findForward("uploader.go.reporting"));
        } else {
        String list_action = request.getParameter("list_action");
        String choice = request.getParameter("choice");
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            session.removeAttribute("admin_patient_id");

            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));

        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            session.removeAttribute("admin_patient_id");
            return (mapping.findForward("uploader.go.admin"));
        } 
        if (list_action != null) {

                //patient has been selected set the session variable.
                /*if (choice != null && !list_action.equals("interface_create")) {
                 Common.invalidatePatientSession(session);
                    

                    session.setAttribute("patient_account_id", choice + "");
                    PatientServiceImpl pservice = new PatientServiceImpl();
                    try {
                        PatientAccountVO p = pservice.getPatient(new Integer(choice));
                        if (p != null) {
                            session.setAttribute("patientAccount", p);
                        }
                    } catch (NumberFormatException e) {
                    } catch (Exception e) {
                        log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception", "y");
                        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                        errors.add("exception", new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);
                        return (mapping.findForward("pixalere.error"));
                    }
                }*/
            
                if (list_action.equals("wound") && request.getParameter("wound_id")!=null) {
                    Common.invalidateWoundSession(session);
                    session.setAttribute("patient_id", choice + "");
                    PatientServiceImpl pservice = new PatientServiceImpl();
                    int wound_id=-1;
                    try{ 
                        wound_id=Integer.parseInt((String)request.getParameter("wound_id"));
                        if(wound_id>0){
                            session.setAttribute("wound_id",(String)request.getParameter("wound_id"));
                        }
                    }catch(NumberFormatException e){log.error("Error: ",e);}
                    try {
                        PatientAccountVO p = pservice.getPatient(new Integer(choice));
                        if (p != null) {
                            session.setAttribute("patientAccount", p);
                        }
                    } catch (NumberFormatException e) {
                    } catch (Exception e) {
                        log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception", "y");
                        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                        errors.add("exception", new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);
                        return (mapping.findForward("pixalere.error"));
                    }
                    return (mapping.findForward("forward.assessment.success"));
                    
                }else if (list_action.equals("admfnc")) {
                    Common.invalidatePatientSession(session);

                    session.setAttribute("patient_id", choice + "");

                    PatientServiceImpl pservice = new PatientServiceImpl();
                    ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
                    try{
                    int error = pservice.validatePatientId(userVO.getId(), Integer.parseInt(choice));
                
                    if(error == 0 || error == 4){
                        session.setAttribute("patient_id",choice+"");
                    }else{
                        
                        session.setAttribute("admin_patient_id",choice + "");
                    }
                    }catch(ApplicationException ex){
                        log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + ex.toString());
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception", "y");
                        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(ex));
                        errors.add("exception", new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);
                        return (mapping.findForward("pixalere.error"));
                    }catch(NumberFormatException ex){
                        ex.printStackTrace();
                    }
                    try {
                        PatientAccountVO p = pservice.getPatient(new Integer(choice));
                        if (p != null) {
                            session.setAttribute("patientAccount", p);
                        }
                    } catch (NumberFormatException e) {
                    } catch (Exception e) {
                        log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception", "y");
                        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                        errors.add("exception", new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);
                        return (mapping.findForward("pixalere.error"));
                    }
                    return (mapping.findForward("admin.patient"));
                } else if (list_action.equals("view")) {
                    Common.invalidatePatientSession(session);
                    session.setAttribute("patient_id", choice + "");
                    PatientServiceImpl pservice = new PatientServiceImpl();
                    try {
                        PatientAccountVO p = pservice.getPatient(new Integer(choice));
                        if (p != null) {
                            session.setAttribute("patientAccount", p);
                            if(p.getAccount_status().equals(new Integer(0))){
                                session.setAttribute("viewerOnly", "inactive");
                            }
                        }
                    } catch (NumberFormatException e) {
                    } catch (Exception e) {
                        log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception", "y");
                        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                        errors.add("exception", new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);
                        return (mapping.findForward("pixalere.error"));
                    }
                    return (mapping.findForward("uploader.go.viewer"));
                } else if (list_action.equals("interface_create")) {

                    PatientServiceImpl managerBD = new PatientServiceImpl();
                    try {
                        PatientAccountVO v = managerBD.getPatientByID(new Integer((String) request.getParameter("choice")));
                        if (v != null && v.getPatient_id() == null) {
                            if (v != null) {

                                Common.invalidatePatientSession(session);
                                session.setAttribute("patientAccountAdd", v);
                                String[] nums = Common.getConfig("phnFormat").split("-");
                                Vector phns = managerBD.getPHN(v, nums);
                                if (v.getOutofprovince().equals(new Integer(1)) && phns.size() == 1) {
                                    session.setAttribute("Addphn11", phns.get(0));
                                } else {
                                    session.setAttribute("Addphn1_1", phns);
                                }
                            }
                            return (mapping.findForward("admin.patient.create"));

                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return (mapping.findForward("admin.patient.create"));
                    }
                } else if (list_action.equals("transfer")) {
                     return (mapping.findForward("admin.patient.transfer"));
                } else if (list_action.equals("assign")) {
                   return (mapping.findForward("admin.patient.assign"));
                }
            }
            return (mapping.findForward("admin.patient.success"));
        }
    }
}
