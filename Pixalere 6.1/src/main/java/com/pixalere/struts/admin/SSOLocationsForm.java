package com.pixalere.struts.admin;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 */
public class SSOLocationsForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOLocationsForm.class);
    
    private int providerId;
    private int locationId;
    private Integer[] treatmentLocation;
    private String attributeValue;
    
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        treatmentLocation = new Integer[0];
        super.reset(mapping, request); 
    }

    public Integer[] getTreatmentLocation() {
        return treatmentLocation;
    }

    public void setTreatmentLocation(Integer[] treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }
    
    public String getTreatmentsIds(){
        String ids = "";
        for (int i = 0; i < treatmentLocation.length; i++) {
            Integer loc_id = treatmentLocation[i];
            ids += loc_id;
            if (i + 1 < treatmentLocation.length){
                ids += ",";
            }
        }
        return ids;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }
    
    /*public Integer getTreatmentLocation(int index) {
        return treatmentLocation[index];
    }
    
    public void setTreatmentLocation(int index, Integer positionId){
        // Allocate the array
	if (treatmentLocation == null) {
            treatmentLocation = new Integer[index + 1];
	}

	// Grow the array
	if (index >= treatmentLocation.length) {
            Integer[] newIntArray = new Integer[index + 1];
            System.arraycopy(treatmentLocation, 0, newIntArray , 0, treatmentLocation.length);
            treatmentLocation = newIntArray ;
	}
        
        // Set the indexed property
	treatmentLocation[index] = positionId;
    }*/
}
