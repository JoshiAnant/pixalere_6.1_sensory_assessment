package com.pixalere.struts.admin;

import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMessage;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Vector;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 * @author
 *
 */
public class PatientAdminForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientAdminForm.class);
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        HttpSession session = request.getSession();
        ActionErrors errors = new ActionErrors();
        ProfessionalVO userVO=(ProfessionalVO)session.getAttribute("userVO");
            
        Integer language = Common.getLanguageIdFromSession(session);
        
        ListServiceImpl listBD=new ListServiceImpl(language);
        if(!action.equals("transfer")){
            firstName = firstName.trim();
            lastName = lastName.trim();
            if (getOutofprovince().equals("0")) {
                try {
                    PatientAccountVO tmpVO = new PatientAccountVO();
                    tmpVO.setPhn(getPhn1() + getPhn2() + getPhn3());
                    PatientDAO pmBD = new PatientDAO();
                    Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO,true);
                    boolean already = false;
                    PatientServiceImpl manager=new PatientServiceImpl(language);
                    for(PatientAccountVO acc : results){
                        int error=manager.validatePatientId(userVO.getId().intValue(),acc.getPatient_id().intValue());
                        String treatment_location = "";
                        if(acc.getTreatment_location_id().equals(new Integer(-1))){
                            treatment_location = Common.getLocalizedString("pixalere.TIP","en");
                        }else{
	                        LookupVO lookupVO=acc.getTreatmentLocation();
                            treatment_location = lookupVO.getName(language);
                        }
                        Object[] patientinfo={acc.getPatient_id()+"",treatment_location};
                        //if user does not have permission to see user, throw error.
                        if(patientId != (acc.getPatient_id().intValue()) && already == false && (error==3 || error==2)){
                            errors.add("access",new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_notassigned",patientinfo));
                            already = true;
                        } else if(patientId != (acc.getPatient_id().intValue()) && already == false && (error == 0 || error==4 || error==5)){
                            errors.add("PHN exists", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_assigned",patientinfo));
                            already = true;
                            //Populate edit form with patient who already had that phn #
                            request.setAttribute("patientAccountEdit", acc);
                            //request.setAttribute("unserializeTreat", patientVO.getUnserializedString( database));
                            ListServiceImpl bd = new ListServiceImpl(language);
                            LookupVO treat=(LookupVO)bd.getListItem(new Integer(acc.getTreatment_location_id()).intValue());
                            request.setAttribute("treatmentEdit",treat);
                            try {
                                String[] nums = Common.getConfig("phnFormat").split("-");
                                Vector phns = manager.getPHN(acc,nums);
                                if (manager.getPHN(acc,nums).size() > 1) {
                                    for(int x = 0; x< phns.size();x++){
                                        request.setAttribute("Editphn1_"+(x+1),phns.get(x));
                                    }
                                }else if (phns.size() == 1) {
                                    request.setAttribute("Editphn11", phns.get(0));
                                }
                            }catch(Exception ex){
                                //do nothing becuase the phn is corrupt and needs to be re filed.
                                request.setAttribute("phn_error", "Please re-enter PHN");
                            }
                            request.setAttribute("action", "edit");
                            request.setAttribute("patientId", acc.getPatient_id());
                            session.removeAttribute("admin_patient_id");
                        }
                    }
                } catch (Exception ex) {
                    errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
                }
            }
        }
        if (errors.size() == 0) {
			// CHECK FOR DUPLICATE MRN
			if (getPhn2()!=null && !getPhn2().equals(""))
			{
				try {
					PatientAccountVO tmpVO = new PatientAccountVO();
					tmpVO.setPhn2(getPhn2());
					PatientDAO pmBD = new PatientDAO();
					Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO,true);
					boolean already = false;
					for(PatientAccountVO acc : results){
						if (already == false)
						{
							if (patientId != (acc.getPatient_id().intValue()))
							{
								String treatment_location = "";
								if(acc.getTreatment_location_id().equals(new Integer(-1))){
									treatment_location = Common.getLocalizedString("pixalere.TIP","en");
								}else{
									LookupVO lookupVO=acc.getTreatmentLocation();
									treatment_location = lookupVO.getName(language);
								}
								Object[] patientinfo={acc.getPatient_id()+"",treatment_location};
								errors.add("access",new ActionMessage("pixalere.admin.patientaccounts.form.mrnexists",patientinfo));
							}
						}
						already = true;
					}

				} catch (Exception ex) {
					errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
				}
			}
		}
        if (errors.size() == 0) {
			// CHECK FOR DUPLICATE PARIS
			if (!getPhn3().equals(""))
			{
				try {
					PatientAccountVO tmpVO = new PatientAccountVO();
					tmpVO.setPhn3(getPhn3());
					PatientDAO pmBD = new PatientDAO();
					Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO,true);
					boolean already = false;
					for(PatientAccountVO acc : results){
						if (already == false)
						{
							if (patientId != (acc.getPatient_id().intValue()))
							{
								String treatment_location = "";
								if(acc.getTreatment_location_id().equals(new Integer(-1))){
									treatment_location = Common.getLocalizedString("pixalere.TIP","en");
								}else{
									LookupVO lookupVO=acc.getTreatmentLocation();
									treatment_location = lookupVO.getName(language);
								}
								Object[] patientinfo={acc.getPatient_id()+"",treatment_location};
								errors.add("access",new ActionMessage("pixalere.admin.patientaccounts.form.parisexists",patientinfo));
							}
						}
						already = true;
					}
				} catch (Exception ex) {
					errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
				}
			}
		}
        if (errors.size() > 0) {
            request.setAttribute("warning_serverside","1");
            PatientAccountVO accountVO = new PatientAccountVO();
            accountVO.setFirstName(getFirstName());
            accountVO.setLastName(getLastName());
            accountVO.setTreatment_location_id(Integer.parseInt(getTreatmentLocation()+""));
            try{
                ListServiceImpl listbd=new ListServiceImpl(language);
                LookupVO lookupVO=listbd.getListItem(new Integer(getTreatmentLocation()).intValue());
                accountVO.setTreatmentLocation(lookupVO);
            }catch(Exception ex){}
            accountVO.setPhn(getPhn1() + getPhn2()
            + getPhn3());
            accountVO.setOutofprovince(Integer.parseInt(getOutofprovince()));
            if (getOutofprovince().equals("0"))
            {
            	accountVO.setPhn(getPhn1() + getPhn2() + getPhn3());
			} else if (getOutofprovince().equals("1")) {
				accountVO.setPhn(getPhn11());
			} else {
				accountVO.setPhn("");
			}

            accountVO.setPatient_id(new Integer(patientId));
            accountVO.setPhn3(getPhn3());
            accountVO.setPhn2(getPhn2());
            try{
			//accountVO.setDob(PDate.getXTimestamp(getDob()));
		}catch(Exception e){}
			accountVO.setGender(getGender());
			accountVO.setPatient_residence(getPatient_residence());

            request.setAttribute("patientAccountAdd", accountVO);
            if (getOutofprovince().equals("0")) {
                request.setAttribute("phn1_1", phn1);
                request.setAttribute("phn1_2", phn2);
                request.setAttribute("phn1_3", phn3);
            } else {
                request.setAttribute("phn11", phn11);
            }
            request.setAttribute("action", request.getParameter("action"));
            request.setAttribute("patientId", patientId + "");
            request.setAttribute("errorAdd","1");
            try{
                LookupVO vo = new LookupVO();
                com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
                ProfessionalVO tuser=(ProfessionalVO)session.getAttribute("userVO");
	            Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY,userVO,false);
    	        Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION,userVO,false);
	            request.setAttribute("treatmentCats", treatmentCatsVO);
    	        request.setAttribute("treatmentList", treatmentVO);
            }catch(Exception ex){
                log.error("Exception handled: "+ex.getMessage());
            }
        }
        return errors;
    }
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    public String getPhn11() {
        return phn11;
    }
    public void setPhn11(String phn11) {
        this.phn11 = phn11;
    }
    private String phn11;
    public String getPhn() {
        return phn;
    }
    public String getPhn1() {
        return phn1;
    }
    public String getPhn2() {
        return phn2;
    }
    public String getPhn3() {
        return phn3;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getTreatmentRegion() {
        return treatmentRegion;
    }
    public void setTreatmentRegion(String treatmentRegion) {
        this.treatmentRegion = treatmentRegion;
    }
    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    public void setPhn(String phn) {
        this.phn = phn;
    }
    public void setPhn1(String phn1) {
        this.phn1 = phn1;
    }
    public void setPhn2(String phn2) {
        this.phn2 = phn2;
    }
    public void setPhn3(String phn3) {
        this.phn3 = phn3;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public Integer getAccountStatus() {
        return accountStatus;
    }
    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }
    public int getPatientId() {
        return patientId;
    }
    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }
    public String getOutofprovince() {
        return outofprovince;
    }
    public void setOutofprovince(String outofprovince) {
        this.outofprovince = outofprovince;
    }
    public String getDischarge() {
        return discharge;
    }
    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
   
    public int getPatient_residence() {
        return patient_residence;
    }
    public void setPatient_residence(int patient_residence) {
        this.patient_residence=patient_residence;
    }
    public int getGender() {
        return gender;
    }
    public void setGender(int gender) {
        this.gender=gender;
    }
    public int getDob_month() {
        return dob_month;
    }
    public void setDob_month(int dob_month) {
        this.dob_month=dob_month;
    }
    public int getDob_year() {
        return dob_year;
    }
    public void setDob_year(int dob_year) {
        this.dob_year=dob_year;
    }
    public int getDob_day() {
        return dob_day;
    }
    public void setDob_day(int dob_day) {
        this.dob_day=dob_day;
    }

    public PatientAccountVO getFormData(PatientAccountVO pat){
        PatientAccountVO accountVO = new PatientAccountVO();
        if(action.equals("transfer")){
            accountVO=pat;
            if(getTreatmentRegion().equals("-1")) {
                accountVO.setTreatment_location_id(-1);
            } else {
                accountVO.setTreatment_location_id(new Integer(getTreatmentLocation()).intValue());
            }
        }else{
            accountVO.setFirstName(getFirstName().replaceAll("\"",""));
            accountVO.setLastName(getLastName().replaceAll("\"",""));
            accountVO.setLastName_search(Common.stripName(getLastName()));
    
            if(getTreatmentRegion().indexOf("-1")>-1) {
                accountVO.setTreatment_location_id(-1);
            } else {
                accountVO.setTreatment_location_id(new Integer(getTreatmentLocation()).intValue());
            }
            accountVO.setAccount_status(getAccountStatus());
            accountVO.setCurrent_flag(new Integer(1));
            String phn="";
            if(getOutofprovince().equals("0")){
                phn=getPhn1() + getPhn2() + getPhn3();
            } else if(getOutofprovince().equals("1")){
                phn=getPhn11();
            } else {
				phn="";
			}
            accountVO.setPhn(phn);
            accountVO.setOutofprovince(Integer.parseInt(getOutofprovince()));
            accountVO.setGender(gender);
            try{
               // accountVO.setDob(PDate.getXTimestamp(getDob()));
            }catch(Exception e){}
            accountVO.setPatient_residence(patient_residence);
            accountVO.setPhn3(paris);
            accountVO.setPhn2(mrn);

        }
        return accountVO;
    }
    private String treatmentRegion;
    private String treatmentLocation;
    private String firstName;
    private String lastName;
    private Integer accountStatus;
    private int patientId;
    private String phn;
    private String phn1;
    private String phn2;
    private String phn3;
    private String outofprovince;
    private String discharge;
    private String action;
    private String timezone;
    private String paris;
    private String mrn;
    private Integer gender;
    private Integer dob_month;
    private Integer dob_year;
    private Integer dob_day;
    private Integer patient_residence;

    /**
     * @return the timezone
     */
    public String getTimezone() {
        return timezone;
    }
    /**
     * @param timezone the timezone to set
     */
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
