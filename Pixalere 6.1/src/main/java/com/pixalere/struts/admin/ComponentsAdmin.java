/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.service.GUIServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.utils.*;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.common.service.ConfigurationServiceImpl;
import java.util.Collection;
import java.util.Vector;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/**
 * Updating Components patients
 *
 * @version 5.0
 * @since 5.0
 * @author travis morris
 */
public class ComponentsAdmin extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ComponentsAdmin.class);
    /**
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        ComponentsAdminForm cform = (ComponentsAdminForm) form;
        try {
            HttpSession session = request.getSession();
            //if professional button was clicked, setup the page to assign the patient
            //if professional was selected, get all the assignments to that professional.
            //load dropdowns.
            GUIServiceImpl gservice = new GUIServiceImpl();
            Collection<ComponentsVO> pages = gservice.getAllComponents();
            request.setAttribute("pages",pages);
            if(request.getParameter("component_id")!=null && !request.getParameter("component_id").equals("")){
                ComponentsVO component = gservice.getComponent(new Integer((String)request.getParameter("component_id")));
                request.setAttribute("component",component);
            }else if(cform.getId()!=null){
                ComponentsVO component = gservice.getComponent(cform.getId());
                if(component!=null){
                    component.setRequired(cform.getRequired()==null?0:cform.getRequired());
                    component.setAllow_edit(cform.getAllow_edit()==null?0:cform.getAllow_edit());
                    component.setHide_gui(cform.getAllow_gui()==null?0:cform.getAllow_gui());
                    component.setHide_flowchart(cform.getAllow_flowchart()==null?0:cform.getAllow_flowchart());
                    component.setValidation(cform.getValidation());
                    component.setNum_columns(cform.getNum_columns());
                    gservice.saveComponent(component);
                    ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
                    TableUpdatesVO t = manager.getTableUpdates();
                    t.setComponents(new Integer(new PDate().getEpochTime() + ""));
                    manager.saveTableUpdates(t);
                }
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ComponentsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.components.success"));
        }
    }
}
