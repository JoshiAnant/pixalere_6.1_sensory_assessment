package com.pixalere.struts.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.bean.ProductsByFacilityVO;
import com.pixalere.common.service.LibraryServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.utils.Common;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProductsAdminSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsAdminSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("page", "admin");
        HttpSession session = request.getSession();
        ProfessionalVO userVO = (ProfessionalVO)session.getAttribute("userVO");

        
        Integer language = Common.getLanguageIdFromSession(session);
        
        if(request.getParameter("category_id")!=null ){
            try{
                
                int cate_id = new Integer(request.getParameter("category_id")+"");
                ProductCategoryServiceImpl bd = new ProductCategoryServiceImpl();
                ProductsServiceImpl prodBD = new ProductsServiceImpl();
                request.setAttribute("cate_id",cate_id);
                request.setAttribute("productsList", (Collection) prodBD.getAllProducts(request.getParameter("category_id")+"",userVO));
            }catch (ApplicationException e){}
        
        }else if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("edit")) {
            try {
                ProductsServiceImpl bd = new ProductsServiceImpl();
                ListServiceImpl lservice = new ListServiceImpl();
                ProductsVO vo = (ProductsVO) bd.getProduct(Integer.parseInt(request.getParameter("choice")));
                request.setAttribute("choice", vo.getId() + "");
                request.setAttribute("id", vo.getId() + "");
                request.setAttribute("product", vo.getTitle());
                request.setAttribute("cost", vo.getCost());
                request.setAttribute("url",vo.getUrl());
                request.setAttribute("video_url",vo.getVideo_url());
                request.setAttribute("pdf",vo.getPdf());
                request.setAttribute("image",vo.getImage());
                request.setAttribute("company",vo.getCompany());
                request.setAttribute("description",vo.getDescription());
                request.setAttribute("recommendation_type",vo.getRecommendation_type());
                request.setAttribute("display_image",vo.getDisplay_image());
                request.setAttribute("library_id",vo.getLibrary_id());
                log.info("$$##$$##$$------PRODUCT CATEGORY == "+vo.getCategory()+" ------$$##$$##$$");
                request.setAttribute("categoryProduct", vo.getCategory_id() + "");
                request.setAttribute("type", vo.getType());
                request.setAttribute("quantity", vo.getQuantity());
                request.setAttribute("all_treatment_location",vo.getAll_treatment_locations());
                request.setAttribute("action", "edit");
                List<LookupVO> regions = new ArrayList();
                for(ProductsByFacilityVO p : vo.getFacilities()){
                    LookupVO l = lservice.getListItem(p.getTreatment_location_id());
                    regions.add(l);
                }
                request.setAttribute("regions",regions);
                //request.setAttribute("product",vo);//@todo remove requests above nd use this.
                
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                
            }
        } else if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("delete")) {
            try {
                ProductsServiceImpl manager = new ProductsServiceImpl();
                ProductsVO accountVO = (ProductsVO) manager.getProduct(Integer.parseInt(request.getParameter("choice")));
                manager.removeProduct(accountVO);
                
            } catch (Exception e) {
                log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                return mapping.findForward("admin.patient.failure");
                
            }
        }
        
        try {
            ProductCategoryServiceImpl bd = new ProductCategoryServiceImpl();
            ProductsServiceImpl prodBD = new ProductsServiceImpl();
            ProductCategoryVO vo = new ProductCategoryVO();vo.setActive(1);
            request.setAttribute("productCategories", (Collection) bd.getAllProductCategoriesByCriteria(vo));
            request.setAttribute("action", "add");
            LibraryServiceImpl libservice = new LibraryServiceImpl();
            request.setAttribute("libraries",libservice.getAllLibraries(5));
            ListServiceImpl lservice=new ListServiceImpl(language);
            LookupVO lookupVO = new LookupVO();
            ListsDAO listDAO = new ListsDAO();
            Collection<ProductsVO> treatmentWithCatsVO =  lservice.findAllTreatmentsWithCats(userVO);
            
            request.setAttribute("treatmentListWithCats", treatmentWithCatsVO);
            
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Exception has occurred in ProductsAdminSetupAction.perform(): " + e.toString());
            return (mapping.findForward("admin.products.failure"));
        }
        log.info("DONE IN PRODUCTS (****************************************");
        
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.products.success"));
        }
    }
    
}
