package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.InformationPopupVO;
/**
 * @author
 *
 */
public class InformationPopupAdminForm extends ActionForm {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InformationPopupAdminForm.class);
    
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {
        HttpSession session=request.getSession();
        ActionErrors errors= new ActionErrors();
        return errors;
        
    }
    
    public void reset(ActionMapping mapping,
            
    HttpServletRequest request) {
        setId(0);
        setTitle("");
        setDescription("");
        setComponent_id(0);
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    public InformationPopupVO getFormData(){
        InformationPopupVO pop = new InformationPopupVO();
        if(id!=0 && id!=-1){pop.setId(id);}
        pop.setComponent_id(getComponent_id());
        pop.setTitle(title);
        pop.setDescription(description);
        return pop;
    }
    private int id;
    private String description;
    private String title;
    private int component_id;
    public int getComponent_id() {
        return component_id;
    }
    public void setComponent_id(int component_id) {
        this.component_id = component_id;
    }
}

