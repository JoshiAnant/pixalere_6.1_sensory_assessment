package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Vector;
import com.pixalere.admin.dao.AssignPatientsDAO;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.auth.dao.UserDAO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.patient.bean.PatientAccountVO;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 * Setup the assign patients GUI.
 *
 * @since 3.0
 * @author travis
 */
public class AssignPatientsAdminSetupAction extends Action {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ResourcesAdminSetupAction.class);
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        try{
            HttpSession session = request.getSession();
        
            request.setAttribute("page","admin");
            //if professional was selected, get all the assignments to that professional.
            if(session.getAttribute("professionalSubmit")!=null){
                request.setAttribute("professionalSelected","true");
                AssignPatientsVO vo=new AssignPatientsVO();
                vo.setProfessionalId(new Integer((String)session.getAttribute("assignProfessionalId")));
                AssignPatientsDAO dao=new AssignPatientsDAO();
                Vector results=(Vector)dao.findAllByCriteria(vo);
                
                request.setAttribute("accountList",results);
            }
            
            request.setAttribute("professionalSubmit",null);
            //request.setAttribute("professionalSelected","true");
            PatientDAO patientDAO=new PatientDAO();
            PatientAccountVO patientVO=new PatientAccountVO();
            patientVO.setAccount_status(new Integer(1));
            Collection patientList=(Collection)patientDAO.findAllByCriteria(patientVO,true);
            request.setAttribute("patientList",patientList);
            UserDAO userDAO=new UserDAO();
            ProfessionalVO userVO=new ProfessionalVO();
            userVO.setAccount_status(new Integer(1));
            Collection professionalList=(Collection)userDAO.findAllByCriteria(userVO);
            session.setAttribute("professionalList",professionalList);
        } catch(Exception e){
            log.error("An application exception has been raised in AssignPatientsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")){
            return (mapping.findForward("uploader.go.profile"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("viewer")){
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else{
            return (mapping.findForward("admin.assignpatients.success"));
        }
    }
}
