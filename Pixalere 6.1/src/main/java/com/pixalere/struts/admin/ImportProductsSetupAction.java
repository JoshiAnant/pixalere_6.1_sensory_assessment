/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.common.service.ProductsServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
public class ImportProductsSetupAction
        extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImportProductsSetupAction.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
            
            return mapping.findForward("admin.importproducts.success");
    }
}
