/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;

import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMessage;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Vector;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 * @author
 *
 */
public class PatientEditForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientEditForm.class);

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        ListServiceImpl listBD = new ListServiceImpl(language);
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");

        //if(!action.equals("transfer")){
        int patientId = 0;
        if (session.getAttribute("patient_id") != null) {
            patientId = new Integer(session.getAttribute("patient_id") + "");
        }
        if (firstName != null) {
            firstName = firstName.trim();
        }
        if (lastName != null) {
            lastName = lastName.trim();
        }
        if (middle_name != null) {
            middle_name = middle_name.trim();
        }
        if (getOutofprovince() != null && getOutofprovince().equals("0")) {
            try {
                PatientAccountVO tmpVO = new PatientAccountVO();
                String p = "";
                for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                    p = p + request.getParameter("editphn1_" + (x + 1));
                }
                tmpVO.setPhn(p);
                PatientDAO pmBD = new PatientDAO();
                Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO, true);
                boolean already = false;
                PatientServiceImpl manager = new PatientServiceImpl(language);
                for (PatientAccountVO acc : results) {
                    int error = 0;
                    if (acc.getPatient_id() != null) {
                        error = manager.validatePatientId(userVO.getId().intValue(), acc.getPatient_id().intValue());
                    }
                    String treatment_location = "";
                    if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                        treatment_location = Common.getLocalizedString("pixalere.TIP", "en");
                    } else {
                        LookupVO lookupVO = acc.getTreatmentLocation();
                        treatment_location = lookupVO.getName(language);
                    }
                    Object[] patientinfo = {(acc.getPatient_id() == null ? "" : acc.getPatient_id()), treatment_location, Common.getConfig("phn2_name")};
                    //if user does not have permission to see user, throw error.
                    if (patientId != ((acc.getPatient_id() != null ? acc.getPatient_id().intValue() : 0)) && already == false && (error == 3 || error == 2)) {
                        if (acc.getPatient_id() == null) {
                            errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_emrpatients", patientinfo));
                        } else {
                            errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_notassigned", patientinfo));
                        }
                        already = true;
                    } else if (patientId != ((acc.getPatient_id() != null ? acc.getPatient_id().intValue() : 0)) && already == false && (error == 0 || error == 4 || error == 5)) {
                        if (acc.getPatient_id() == null) {
                            errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_emrpatients", patientinfo));
                        } else {
                            errors.add("PHN exists", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_assigned", patientinfo));
                        }
                        already = true;
                        //Populate edit form with patient who already had that phn #
                        request.setAttribute("patientAccountEdit", acc);
                        //request.setAttribute("unserializeTreat", patientVO.getUnserializedString( database));
                        ListServiceImpl bd = new ListServiceImpl(language);
                        LookupVO treat = (LookupVO) bd.getListItem(new Integer(acc.getTreatment_location_id()).intValue());
                        request.setAttribute("treatmentEdit", treat);
                        try {
                            String[] nums = Common.getConfig("phnFormat").split("-");
                            Vector phns = manager.getPHN(acc, nums);
                            if (manager.getPHN(acc, nums).size() > 1) {
                                request.setAttribute("Editphn1_1", phns);

                            } else if (phns.size() == 1) {
                                request.setAttribute("Editphn11", phns.get(0));
                            }
                        } catch (Exception ex) {
                            //do nothing becuase the phn is corrupt and needs to be re filed.
                            request.setAttribute("phn_error", "Please re-enter PHN");
                        }
                        request.setAttribute("action", "edit");
                        request.setAttribute("patientId", acc.getPatient_id());
                        //session.removeAttribute("admin_patient_id");
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
            }
        }
        //}
        if (errors.size() == 0) {
            // CHECK FOR DUPLICATE MRN
            if (getEditphn2() != null && !getEditphn2().equals("")) {
                try {
                    PatientAccountVO tmpVO = new PatientAccountVO();
                    tmpVO.setPhn2(getEditphn2());
                    PatientDAO pmBD = new PatientDAO();
                    Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO, true);
                    boolean already = false;
                    for (PatientAccountVO acc : results) {
                        if (already == false) {
                           
                            if (patientId != (acc.getPatient_id().intValue())) {
                                String treatment_location = "";
                                if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                                    treatment_location = Common.getLocalizedString("pixalere.TIP", "en");
                                } else {
                                    LookupVO lookupVO = acc.getTreatmentLocation();
                                    treatment_location = lookupVO.getName(language);
                                }
                                Object[] patientinfo = {acc.getPatient_id() + "", treatment_location, Common.getConfig("phn2_name")};
                                errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form._exists", patientinfo));
                            }
                        }
                        already = true;
                    }

                } catch (Exception ex) {
                    errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
                }
            }
        }
        if (errors.size() == 0) {

            // CHECK FOR DUPLICATE PARIS
            if (editphn3 != null && !getEditphn3().equals("")) {
                try {
                    PatientAccountVO tmpVO = new PatientAccountVO();
                    tmpVO.setPhn3(getEditphn3());
                    PatientDAO pmBD = new PatientDAO();
                    Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO, true);
                    boolean already = false;
                    for (PatientAccountVO acc : results) {
                        if (already == false) {
                            
                            if (patientId != (acc.getPatient_id().intValue())) {
                                String treatment_location = "";
                                if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                                    treatment_location = Common.getLocalizedString("pixalere.TIP", "en");
                                } else {
                                    LookupVO lookupVO = acc.getTreatmentLocation();
                                    treatment_location = lookupVO.getName(language);
                                }
                                Object[] patientinfo = {acc.getPatient_id() + "", treatment_location, Common.getConfig("phn3_name")};
                                errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form._exists", patientinfo));
                            }
                        }
                        already = true;
                    }
                } catch (Exception ex) {
                    errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
                }
            }
        }
        if (errors.size() > 0) {
            request.setAttribute("warning_serverside", "1");
            PatientAccountVO accountVO = new PatientAccountVO();
            accountVO.setFirstName(getFirstName());
            accountVO.setLastName(getLastName());
            accountVO.setMiddleName(getMiddle_name());
            accountVO.setTreatment_location_id(Integer.parseInt(getTreatmentLocation() + ""));
            accountVO.setAllergies(getAllergies());

            accountVO.setFunding_source(getFunding_source());
            try {
                ListServiceImpl listbd = new ListServiceImpl(language);
                LookupVO lookupVO = listbd.getListItem(new Integer(getTreatmentLocation()).intValue());
                accountVO.setTreatmentLocation(lookupVO);
            } catch (Exception ex) {
            }

            accountVO.setOutofprovince(Integer.parseInt(getOutofprovince()));
            String p = "";
            for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                p = p + request.getParameter("editphn1_" + (x + 1));
            }
            //System.out.println("PHN:"+p);
            if (p == null || p.equals("nullnullnull")) {
                accountVO.setPhn("");
            } else {
                accountVO.setPhn(p);
            }
            accountVO.setPatient_id(new Integer(patientId));
            accountVO.setPhn3(getEditphn3());
            accountVO.setPhn2(getEditphn2());
            try {
                accountVO.setDob(getEditdob_year() + "-" + getEditdob_month() + "-" + getEditdob_day() + "T00:00:00");
            } catch (Exception e) {
            }
            accountVO.setGender(getGender());
            //accountVO.setPatient_residence(getPatient_residence());

            request.setAttribute("patientAccountEdit", accountVO);
            if (getOutofprovince().equals("0")) {
                p = "";
                for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                    p = p + request.getParameter("editphn1_" + (x + 1));
                }
                if (p == null || p.equals("nullnullnull")) {
                    accountVO.setPhn("");
                } else {
                    accountVO.setPhn(p);
                }
            } else {
                request.setAttribute("phn11", editphn11);
            }
            request.setAttribute("action", request.getParameter("action"));

            request.setAttribute("errorAdd", "1");
            try {
                LookupVO vo = new LookupVO();
                com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
                ProfessionalVO tuser = (ProfessionalVO) session.getAttribute("userVO");
                Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
                Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
                request.setAttribute("treatmentCats", treatmentCatsVO);
                request.setAttribute("treatmentList", treatmentVO);
            } catch (Exception ex) {
                log.error("Exception handled: " + ex.getMessage());
            }
        }
        return errors;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    public String getEditphn11() {
        return editphn11;
    }

    public void setEditphn11(String editphn11) {
        this.editphn11 = editphn11;
    }
    private String editphn11;
    /*public String getEditphn() {
     return editphn;
     }*/

    public String getEditphn1_1() {
        return editphn1_1;
    }

    public String getEditphn1_2() {
        return editphn1_2;
    }

    public String getEditphn1_3() {
        return editphn1_3;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTreatmentRegion() {
        return treatmentRegion;
    }

    public void setTreatmentRegion(String treatmentRegion) {
        this.treatmentRegion = treatmentRegion;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }

    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    /*public void setEditphn(String editphn) {
     this.editphn = editphn;
     }*/

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getOutofprovince() {
        return outofprovince;
    }

    public void setOutofprovince(String outofprovince) {
        this.outofprovince = outofprovince;
    }

    public String getDischarge() {
        return discharge;
    }

    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getEditphn3() {
        return editphn3;
    }

    public void setEditphn3(String phn3) {
        this.editphn3 = phn3;
    }

    public String getEditphn2() {
        return editphn2;
    }

    public void setEditphn2(String phn2) {
        this.editphn2 = phn2;
    }

    public int getPatient_residence() {
        return patient_residence;
    }

    public void setPatient_residence(int patient_residence) {
        this.patient_residence = patient_residence;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public PatientAccountVO getFormData(PatientAccountVO pat, HttpServletRequest request) {
        PatientAccountVO accountVO = new PatientAccountVO();
        accountVO.setFirstName(getFirstName().replaceAll("\"", ""));
        accountVO.setLastName(getLastName().replaceAll("\"", ""));
        accountVO.setMiddleName(getMiddle_name().replaceAll("\"", ""));
        accountVO.setAllergies(getAllergies().replaceAll("\"", ""));
        accountVO.setVersion_code(getVersion_code());

        accountVO.setFunding_source(getFunding_source());
   
        accountVO.setLastName_search(Common.stripName(getLastName()));
        if (getTreatmentRegion().indexOf("-1") > -1) {
            accountVO.setTreatment_location_id(-1);
        } else {
            accountVO.setTreatment_location_id(new Integer(getTreatmentLocation()).intValue());
        }
        accountVO.setAccount_status(1);
        accountVO.setCurrent_flag(new Integer(1));
        String phn = "";
        if (getOutofprovince() != null && getOutofprovince().equals("0")) {

            for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                phn = phn + request.getParameter("editphn1_" + (x + 1));
            }

        } else if (getOutofprovince() != null && getOutofprovince().equals("1")) {
            phn = getEditphn11();
        } else {
            phn = "";
        }
        if (phn == null || phn.equals("nullnullnull")) {
            accountVO.setPhn("");
        } else {
            accountVO.setPhn(phn);
        }
        if(getOutofprovince()!=null){
            accountVO.setOutofprovince(Integer.parseInt(getOutofprovince()));
        }else{
            accountVO.setOutofprovince(2);
        }
        accountVO.setGender(gender);
        try {
            accountVO.setDob(editdob_year + "-" + editdob_month + "-" + editdob_day + "T00:00:00");
        } catch (Exception e) {
        }
        accountVO.setPatient_residence(patient_residence);
        accountVO.setPhn3(editphn3);
        accountVO.setPhn2(editphn2);

        return accountVO;
    }
    private String treatmentRegion;
    private String treatmentLocation;
    private String firstName;
    private String lastName;
    private Integer accountStatus;
    private Integer editdob_day;
    private Integer editdob_month;
    private Integer editdob_year;
    //private String editphn;
    private String editphn1_1;
    private String editphn1_2;
    private String editphn1_3;
    private String outofprovince;
    private String discharge;
    private String action;
    private String editphn3;
    private String editphn2;
    private Integer gender;
    private Integer patient_residence;
    private String allergies;
    private String version_code;
    private String middle_name;
    private Integer funding_source;

    /**
     * @return the editdob_day
     */
    public Integer getEditdob_day() {
        return editdob_day;
    }

    /**
     * @param editdob_day the editdob_day to set
     */
    public void setEditdob_day(Integer editdob_day) {
        this.editdob_day = editdob_day;
    }

    /**
     * @return the editdob_month
     */
    public Integer getEditdob_month() {
        return editdob_month;
    }

    /**
     * @param editdob_month the editdob_month to set
     */
    public void setEditdob_month(Integer editdob_month) {
        this.editdob_month = editdob_month;
    }

    /**
     * @return the editdob_year
     */
    public Integer getEditdob_year() {
        return editdob_year;
    }

    /**
     * @param editdob_year the editdob_year to set
     */
    public void setEditdob_year(Integer editdob_year) {
        this.editdob_year = editdob_year;
    }

    /**
     * @return the allergies
     */
    public String getAllergies() {
        return allergies;
    }

    /**
     * @param allergies the allergies to set
     */
    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    /**
     * @return the version_code
     */
    public String getVersion_code() {
        return version_code;
    }

    /**
     * @param version_code the version_code to set
     */
    public void setVersion_code(String version_code) {
        this.version_code = version_code;
    }

    /**
     * @return the middle_name
     */
    public String getMiddle_name() {
        return middle_name;
    }

    /**
     * @param middle_name the middle_name to set
     */
    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    /**
     * @return the funding_source
     */
    public Integer getFunding_source() {
        return funding_source;
    }

    /**
     * @param funding_source the funding_source to set
     */
    public void setFunding_source(Integer funding_source) {
        this.funding_source = funding_source;
    }
}
