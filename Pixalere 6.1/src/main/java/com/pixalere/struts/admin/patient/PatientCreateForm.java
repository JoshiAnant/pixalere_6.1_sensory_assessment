/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;

import com.pixalere.auth.bean.ProfessionalVO;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMessage;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Vector;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 * @author
 *
 */
public class PatientCreateForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientCreateForm.class);
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        HttpSession session = request.getSession();
        ActionErrors errors = new ActionErrors();
        ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            Integer language = 1;
        if(session.getAttribute("language")!=null){try{language = (Integer)session.getAttribute("language");}catch(NullPointerException e){}}
        //Integer language = Common.getLanguageIdFromSession(session);
        
        ListServiceImpl listBD = new ListServiceImpl(language);
        firstName = firstName.trim();
        lastName = lastName.trim();
        middle_name=middle_name.trim();
        if (getOutofprovince() == null || getOutofprovince().equals("0")) {
            try {
                PatientAccountVO tmpVO = new PatientAccountVO();
                String p = "";
                for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                    p = p + request.getParameter("phn1_" + (x + 1));
                }
                if (p == null || p.equals("nullnullnull")) {
                    tmpVO.setPhn("");
                } else {
                    tmpVO.setPhn(p);
                }
                PatientDAO pmBD = new PatientDAO();
                Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO, true);
                boolean already = false;
                PatientServiceImpl manager = new PatientServiceImpl(language);
                for (PatientAccountVO acc : results) {
                    int error=0;
                    if(acc.getPatient_id()!=null){
                        error = manager.validatePatientId(userVO.getId().intValue(), acc.getPatient_id().intValue());
                    }
                    String treatment_location = "";
                    if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                        treatment_location = Common.getLocalizedString("pixalere.TIP","en");
                    } else {
                        LookupVO lookupVO = acc.getTreatmentLocation();
                        treatment_location = lookupVO.getName(language);
                    }
                    Object[] patientinfo = {(acc.getPatient_id()==null?"":acc.getPatient_id()), treatment_location,Common.getConfig("phn2_name")};
                    //if user does not have permission to see user, throw error.
                    //System.out.println("error: "+error+" patient: "+patient_account_id+" "+acc.getId()+" ");
                    if (!tmpVO.getPhn().equals("") && patient_account_id != (acc.getId()==null?0:acc.getId())  && already == false && (error == 3 || error == 2)) {
                       
                        if(acc.getPatient_id()==null){
                                errors.add("access",new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_emrpatients",patientinfo));
                            }else{
                                errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_notassigned", patientinfo));
                            }already = true;
                    } else if (!tmpVO.getPhn().equals("") && patient_account_id != (acc.getId()==null?0:acc.getId()) && already == false && (error == 0 || error == 4 || error == 5)) {
                     
                        if(acc.getPatient_id()==null){
                                errors.add("access",new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_emrpatients",patientinfo));
                            }else{
                                errors.add("PHN exists", new ActionMessage("pixalere.admin.patientaccounts.form.phnexists_assigned", patientinfo));
                            }already = true;
                        //Populate edit form with patient who already had that phn #
                        request.setAttribute("patientAccountAdd", acc);
                        //request.setAttribute("unserializeTreat", patientVO.getUnserializedString( database));
                        ListServiceImpl bd = new ListServiceImpl(language);
                        LookupVO treat = (LookupVO) bd.getListItem(new Integer(acc.getTreatment_location_id()).intValue());
                        request.setAttribute("treatmentAdd", treat);
                        try {
                            String[] nums = Common.getConfig("phnFormat").split("-");
                       
                            Vector phns = manager.getPHN(acc, nums);
                            if (phns.size() > 1) {
                                //for(int x = 0; x< phns.size();x++){
                                request.setAttribute("Addphn1_1", phns);
                                //}
                            } else if (phns.size() == 1) {
                                request.setAttribute("Addphn11", phns.get(0));
                            }
                        } catch (Exception ex) {
                            //do nothing becuase the phn is corrupt and needs to be re filed.
                            request.setAttribute("phn_error", "Please re-enter PHN");
                        }
                        //session.setAttribute("patient_id", acc.getPatient_id()+"");
                        //session.setAttribute("patientAccount",acc);
                        //return (mapping.findForward("admin.patient.edit"));
                    }
                }
            } catch (Exception ex) {
                errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
            }
        }
        if (errors.size() == 0) {
            // CHECK FOR DUPLICATE MRN
            if (getPhn2() != null && !getPhn2().equals("")) {
                try {
                    PatientAccountVO tmpVO = new PatientAccountVO();
                    tmpVO.setPhn2(getPhn2());
                    PatientDAO pmBD = new PatientDAO();
                    Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO, true);
                    boolean already = false;
                    for (PatientAccountVO acc : results) {
                        if (already == false) {
                          
                            if (patient_account_id != (acc.getId()==null?0:acc.getId())) {
                                String treatment_location = "";
                                if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                                    treatment_location = Common.getLocalizedString("pixalere.TIP","en");
                                } else {
                                    LookupVO lookupVO = acc.getTreatmentLocation();
                                    treatment_location = lookupVO.getName(language);
                                }
                                Object[] patientinfo = {acc.getPatient_id() + "", treatment_location, Common.getConfig("phn2_name")};
                                errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form._exists", patientinfo));
                            }
                        }
                        already = true;
                    }

                } catch (Exception ex) {
                    errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
                }
            }
        }
        if (errors.size() == 0) {
            // CHECK FOR DUPLICATE PARIS
            if (!getPhn3().equals("")) {
                try {
                    PatientAccountVO tmpVO = new PatientAccountVO();
                    tmpVO.setPhn3(getPhn3());
                    PatientDAO pmBD = new PatientDAO();
                    Collection<PatientAccountVO> results = pmBD.findAllByCriteria(tmpVO, true);
                    boolean already = false;
                    for (PatientAccountVO acc : results) {
                        if (already == false) {
                          
                             if (patient_account_id != (acc.getId()==null?0:acc.getId())) {
                                String treatment_location = "";
                                if (acc.getTreatment_location_id().equals(new Integer(-1))) {
                                    treatment_location = Common.getLocalizedString("pixalere.TIP","en");
                                } else {
                                    LookupVO lookupVO = acc.getTreatmentLocation();
                                    treatment_location = lookupVO.getName(language);
                                }
                                Object[] patientinfo = {acc.getPatient_id() + "", treatment_location, Common.getConfig("phn3_name")};
                                errors.add("access", new ActionMessage("pixalere.admin.patientaccounts.form._exists", patientinfo));
                            }
                        }
                        already = true;
                    }
                } catch (Exception ex) {
                    errors.add("PixalereError", new ActionError("pixalere.common.error", com.pixalere.utils.Common.buildException(ex)));
                }
            }
        }
        if (errors.size() > 0) {
            request.setAttribute("warning_serverside", "1");
            PatientAccountVO accountVO = new PatientAccountVO();
            accountVO.setFirstName(getFirstName());
            accountVO.setLastName(getLastName());
            accountVO.setMiddleName(getMiddle_name());
            accountVO.setTreatment_location_id(Integer.parseInt(getTreatmentLocation() + ""));
            
            accountVO.setFunding_source(getFunding_source());
            try {
                ListServiceImpl listbd = new ListServiceImpl(language);
                LookupVO lookupVO = listbd.getListItem(new Integer(getTreatmentLocation()).intValue());
                accountVO.setTreatmentLocation(lookupVO);
            } catch (Exception ex) {
            }
            String p = "";
            for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                p = p + request.getParameter("phn1_" + (x + 1));
            }
            if (p == null || p.equals("nullnullnull")) {
                accountVO.setPhn("");
            } else {
                accountVO.setPhn(p);
            }
            if (getOutofprovince() != null) {
                accountVO.setOutofprovince(Integer.parseInt(getOutofprovince()));
            }
            if (getOutofprovince() != null && getOutofprovince().equals("0")) {
                if (p == null || p.equals("nullnullnull")) {
                    accountVO.setPhn("");
                } else {
                    accountVO.setPhn(p);
                }
            } else if (getOutofprovince() != null && getOutofprovince().equals("2")) {
                if (getPhn11() == null || getPhn11().equals("nullnullnull")) {
                    accountVO.setPhn("");
                } else {
                    accountVO.setPhn(getPhn11());
                }
            } else {
                accountVO.setPhn("");
            }
            accountVO.setAllergies(allergies);
            accountVO.setPatient_id(new Integer(patientId));
            accountVO.setPhn3(getPhn3());
            accountVO.setPhn2(getPhn2());
            try {
                accountVO.setDob(getDob_year() + "-" + getDob_month() + "-" + getDob_day() + "T00:00:00");
            } catch (Exception e) {
            }
            accountVO.setGender(getGender());
            accountVO.setPatient_residence(getPatient_residence());

            request.setAttribute("patientAccountAdd", accountVO);
            if (getOutofprovince().equals("0")) {
                Vector phns = new Vector();
                for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                    phns.add((String) request.getParameter("phn1_" + (x + 1)));
                }
                request.setAttribute("Addphn1_1", phns);
            } else {
                request.setAttribute("phn11", getPhn11());
            }
            request.setAttribute("action", request.getParameter("action"));
            request.setAttribute("patientId", patientId + "");
            request.setAttribute("errorAdd", "1");
            try {
                LookupVO vo = new LookupVO();
                com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
                ProfessionalVO tuser = (ProfessionalVO) session.getAttribute("userVO");
                Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
                Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
                request.setAttribute("treatmentCats", treatmentCatsVO);
                request.setAttribute("treatmentList", treatmentVO);
            } catch (Exception ex) {
                log.error("Exception handled: " + ex.getMessage());
            }
        }
        return errors;
    }
    private String treatmentRegion;
    private String treatmentLocation;
    private String firstName;
    private String lastName;
    private Integer accountStatus;
    private int patientId;
    private String phn11;
    private String phn2;
    private String phn3;
    private String outofprovince;
    private String discharge;
    private String action;
    private Integer gender;
    private Integer dob_day;
    private Integer dob_month;
    private Integer dob_year;
    private String allergies;
    private Integer patient_residence;
    private String version_code;
    
    private Integer funding_source;
    private Integer patient_account_id;
    private String middle_name;
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    public PatientAccountVO getFormData(PatientAccountVO pat, HttpServletRequest request) {
        PatientAccountVO accountVO = new PatientAccountVO();
        try {
            accountVO.setFirstName(getFirstName().replaceAll("\"", ""));
            accountVO.setLastName(getLastName().replaceAll("\"", ""));
            accountVO.setMiddleName(getMiddle_name().replaceAll("\"", ""));
            accountVO.setAllergies(getAllergies().replaceAll("\"", ""));
            accountVO.setLastName_search(Common.stripName(getLastName()));
            accountVO.setFunding_source(getFunding_source());
            accountVO.setVersion_code(getVersion_code());
            if (getTreatmentRegion().indexOf("-1") > -1) {
                accountVO.setTreatment_location_id(-1);
            } else {
                accountVO.setTreatment_location_id(new Integer(getTreatmentLocation()).intValue());
            }
            accountVO.setAccount_status(getAccountStatus());
            accountVO.setCurrent_flag(new Integer(1));
            String phn = "";
            String p = "";
            for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                p = p + request.getParameter("phn1_" + (x + 1));
            }
            if (getOutofprovince().equals("0")) {
                phn = p;
            } else if (getOutofprovince().equals("1")) {
                phn = getPhn11();
            } else {
                phn = "";
            }
            if(phn == null || phn.equals("nullnullnull")){
                accountVO.setPhn("");
            }else{
                accountVO.setPhn(phn);
            }
            accountVO.setOutofprovince(Integer.parseInt(getOutofprovince()));
            accountVO.setGender(gender);
            try {
                accountVO.setDob(getDob_year() + "-" + getDob_month() + "-" + getDob_day() + "T00:00:00");
            } catch (Exception e) {
                e.printStackTrace();
            }
            accountVO.setPatient_residence(patient_residence);
            accountVO.setPhn3(phn3);
            accountVO.setPhn2(phn2);
        } catch (Exception ef) {
            ef.printStackTrace();
        }
        return accountVO;
    }
/**
     * @return the patient_account_id
     */
    public Integer getPatient_account_id() {
        return patient_account_id;
    }
    /**
     * @param patient_account_id the patient_account_id to set
     */
    public void setPatient_account_id(Integer patient_account_id) {
        this.patient_account_id = patient_account_id;
    }
    /**
     * @return the treatmentRegion
     */
    public String getTreatmentRegion() {
        return treatmentRegion;
    }
    /**
     * @param treatmentRegion the treatmentRegion to set
     */
    public void setTreatmentRegion(String treatmentRegion) {
        this.treatmentRegion = treatmentRegion;
    }
    /**
     * @return the treatmentLocation
     */
    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    /**
     * @param treatmentLocation the treatmentLocation to set
     */
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * @return the accountStatus
     */
    public Integer getAccountStatus() {
        return accountStatus;
    }
    /**
     * @param accountStatus the accountStatus to set
     */
    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }
    /**
     * @return the patientId
     */
    public int getPatientId() {
        return patientId;
    }
    /**
     * @param patientId the patientId to set
     */
    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }
    /**
     * @return the phn2
     */
    public String getPhn2() {
        return phn2;
    }
    /**
     * @param phn2 the phn2 to set
     */
    public void setPhn2(String phn2) {
        this.phn2 = phn2;
    }
    /**
     * @return the phn3
     */
    public String getPhn3() {
        return phn3;
    }
    /**
     * @param phn3 the phn3 to set
     */
    public void setPhn3(String phn3) {
        this.phn3 = phn3;
    }
    /**
     * @return the outofprovince
     */
    public String getOutofprovince() {
        return outofprovince;
    }
    /**
     * @param outofprovince the outofprovince to set
     */
    public void setOutofprovince(String outofprovince) {
        this.outofprovince = outofprovince;
    }
    /**
     * @return the discharge
     */
    public String getDischarge() {
        return discharge;
    }
    /**
     * @param discharge the discharge to set
     */
    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return the gender
     */
    public Integer getGender() {
        return gender;
    }
    /**
     * @param gender the gender to set
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }
    /**
     * @return the patient_residence
     */
    public Integer getPatient_residence() {
        return patient_residence;
    }
    /**
     * @param patient_residence the patient_residence to set
     */
    public void setPatient_residence(Integer patient_residence) {
        this.patient_residence = patient_residence;
    }
    /**
     * @return the phn11
     */
    public String getPhn11() {
        return phn11;
    }
    /**
     * @param phn11 the phn11 to set
     */
    public void setPhn11(String phn11) {
        this.phn11 = phn11;
    }
    /**
     * @return the dob_day
     */
    public Integer getDob_day() {
        return dob_day;
    }
    /**
     * @param dob_day the dob_day to set
     */
    public void setDob_day(Integer dob_day) {
        this.dob_day = dob_day;
    }
    /**
     * @return the dob_month
     */
    public Integer getDob_month() {
        return dob_month;
    }
    /**
     * @param dob_month the dob_month to set
     */
    public void setDob_month(Integer dob_month) {
        this.dob_month = dob_month;
    }
    /**
     * @return the dob_year
     */
    public Integer getDob_year() {
        return dob_year;
    }
    /**
     * @param dob_year the dob_year to set
     */
    public void setDob_year(Integer dob_year) {
        this.dob_year = dob_year;
    }
    /**
     * @return the allergies
     */
    public String getAllergies() {
        return allergies;
    }
    /**
     * @param allergies the allergies to set
     */
    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }
    /**
     * @return the version_code
     */
    public String getVersion_code() {
        return version_code;
    }
    /**
     * @param version_code the version_code to set
     */
    public void setVersion_code(String version_code) {
        this.version_code = version_code;
    }
    /**
     * @return the middle_name
     */
    public String getMiddle_name() {
        return middle_name;
    }
    /**
     * @param middle_name the middle_name to set
     */
    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }
  
    /**
     * @return the funding_source
     */
    public Integer getFunding_source() {
        return funding_source;
    }
    /**
     * @param funding_source the funding_source to set
     */
    public void setFunding_source(Integer funding_source) {
        this.funding_source = funding_source;
    }
}