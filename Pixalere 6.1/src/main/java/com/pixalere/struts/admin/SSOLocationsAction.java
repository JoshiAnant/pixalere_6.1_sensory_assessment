package com.pixalere.struts.admin;

import com.pixalere.common.ApplicationException;
import com.pixalere.sso.bean.SSOLocationVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.service.SSOProviderService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 */
public class SSOLocationsAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOLocationsSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        
        HttpSession session = request.getSession();
        //Integer language = Common.getLanguageIdFromSession(session);
        SSOProviderService service = new SSOProviderService();
        
        SSOLocationsForm lform = (SSOLocationsForm) form;
        
        try {
            // Get provider being edited
            SSOProviderVO provider = service.getProviderById(lform.getProviderId());
            if (provider != null) {
                SSOLocationVO locationVO = new SSOLocationVO();
                // Delete
                if (request.getParameter("action") != null && request.getParameter("action").equals("delete") 
                        && request.getParameter("locationId") != null && !request.getParameter("locationId").isEmpty()){
                    locationVO = service.getSSOLocationById(lform.getLocationId());
                    service.removeLocation(locationVO);
                } else {
                    // Add or update
                    if (request.getParameter("locationId") != null && !request.getParameter("locationId").isEmpty()) {
                        // Updating an existing mapping
                        locationVO = service.getSSOLocationById(lform.getLocationId());
                    } else {
                        locationVO.setSsoProviderid(provider.getId());
                    }
                    locationVO.setAttributeValue(lform.getAttributeValue());
                    locationVO.setPixLocationsids(lform.getTreatmentsIds());

                    service.saveLocation(locationVO);
                }
                
                // Store in session current Provider (Redirecting to SSOUsersSetup)
                session.setAttribute("providerId", String.valueOf(provider.getId()));
            }  else {
                System.out.println("ERROR: SSOLocationsAction - SSOProvider to update not found id: " + lform.getProviderId());
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in SSOLocationsSetupAction.execute(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        return (mapping.findForward("admin.sso.success"));
    }
    
}
