package com.pixalere.struts.admin;

import com.pixalere.common.ApplicationException;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.bean.SSORoleVO;
import com.pixalere.sso.service.SSOProviderService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 */
public class SSORolesAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSORolesSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        
        HttpSession session = request.getSession();
        //Integer language = Common.getLanguageIdFromSession(session);
        SSOProviderService service = new SSOProviderService();
        
        SSORolesForm rform = (SSORolesForm) form;
        
        try {
            // Get provider being edited
            SSOProviderVO provider = service.getProviderById(rform.getProviderId());
            if (provider != null) {
                SSORoleVO roleVO = new SSORoleVO();
                // Delete
                if (request.getParameter("action") != null && request.getParameter("action").equals("delete") 
                        && request.getParameter("roleId") != null && !request.getParameter("roleId").isEmpty()){
                    roleVO = service.getSSORoleById(rform.getRoleId());
                    service.removeRole(roleVO);
                } else {
                    if (request.getParameter("roleId") != null && !request.getParameter("roleId").isEmpty()) {
                        // Updating an existing mapping
                        roleVO = service.getSSORoleById(rform.getRoleId());
                    } else {
                        roleVO.setSsoProviderid(provider.getId());
                    }
                    roleVO = rform.getUpdatedRole(roleVO);
                    service.saveRole(roleVO);
                }
                
                // Store in session current Provider (Redirecting to SSOUsersSetup)
                session.setAttribute("providerId", String.valueOf(provider.getId()));
            }  else {
                System.out.println("ERROR: SSORolesAction - SSOProvider to update not found id: " + rform.getProviderId());
            }
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in SSORolesSetupAction.execute(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        return (mapping.findForward("admin.sso.success"));
    }
}
    