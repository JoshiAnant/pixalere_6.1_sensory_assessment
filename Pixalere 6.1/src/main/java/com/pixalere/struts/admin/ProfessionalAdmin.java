package com.pixalere.struts.admin;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.LookupVO;
import java.util.Collection;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.admin.service.ResourcesServiceImpl;
import com.pixalere.auth.dao.UserDAO;
import com.pixalere.utils.*;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.auth.bean.PasswordHistoryVO;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.utils.excel.ExcelReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class ProfessionalAdmin extends Action {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfessionalAdmin.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("position", request.getParameter("position"));
        request.setAttribute("action", "add");
        HttpSession session = request.getSession();
        ProfessionalServiceImpl managerBD = new ProfessionalServiceImpl();
        ProfessionalAdminForm professionalAdminForm = (ProfessionalAdminForm) form;
        
        Integer language = Common.getLanguageIdFromSession(session);
        ListServiceImpl lservice = new ListServiceImpl(language);
        try {
            String locale = Common.getLanguageLocale(language);

            if (((String) request.getParameter("action")).equals("unassign")) {
                try {
                    int prof_id = Integer.parseInt(request.getParameter("id"));
                    int patient_id = Integer.parseInt(request.getParameter("patients"));

                    AssignPatientsVO p = new AssignPatientsVO();
                    p.setProfessionalId(prof_id);
                    p.setPatientId(patient_id);
                    AssignPatientsServiceImpl aservice = new AssignPatientsServiceImpl();
                    AssignPatientsVO check = aservice.getAssignedPatient(p);
                    if (check != null) {
                        aservice.removeAssignedPatient(check);
                        request.setAttribute("result", "1");
                        request.setAttribute("error", Common.getLocalizedString("pixalere.admin.professional.error.patient_unassigned", locale));
                    } else {
                        request.setAttribute("result", "0");
                        request.setAttribute("error", Common.getLocalizedString("pixalere.admin.professional.error.failed_unassign_nopatient", locale));
                    }
                } catch (Exception e) {
                    request.setAttribute("result", "0");
                    request.setAttribute("error", e.getMessage());
                }
                return (mapping.findForward("admin.professionalunassign.success"));
            } else if (request.getParameter("action").equals("import")) {
                List<String> result = new ArrayList();
                ExcelReader r = new ExcelReader();
                FormFile excel = professionalAdminForm.getExcel();
                try {
                    Vector<ProfessionalVO> prodVect = r.readProfessional(excel.getInputStream());
                    java.util.Collection<PositionVO> positions = managerBD.getPositions(new PositionVO());
                    for (ProfessionalVO prof : prodVect) {
                        String treatment = prof.getAnswer_two();prof.setAnswer_two(null);
                        String position = prof.getAnswer_one();prof.setAnswer_one(null);
                        //check if prof exists by email / username / employeeid
                        int position_id = -1;
                        int treatment_id = -1;
                        for(PositionVO l : positions){
                            if(l!=null && l.getTitle().equals(position)){
                                position_id=l.getId();
                            }
                        }
                        if(position_id<=0){
                            //save new position
                            PositionVO pos = new PositionVO();
                            pos.setActive(1);
                            pos.setColour("#FCFCFC");
                            pos.setOrderby(1);
                            pos.setRecommendation_popup(0);
                            pos.setReferral_popup(0);
                            pos.setTitle(position);
                            managerBD.savePosition(pos);
                            PositionVO p = managerBD.getPosition(pos);
                            if(p!=null){
                                position_id = p.getId();
                            }
                        }
                        prof.setPosition_id(position_id);
                        //check if treatment location exists
                        Collection<LookupVO> list = lservice.getLists(LookupVO.TREATMENT_LOCATION_INT);
                        for(LookupVO l : list){
                            Collection<LookupLocalizationVO> ll = l.getNames();
                            for(LookupLocalizationVO ll2 : ll){
                                if(ll2.getLanguage_id()==1 && ll2.getName().equals(treatment)){
                                    treatment_id = ll2.getLookup_id();
                                }
                            }
                        }
                        
                        if(prof.getUser_name()== null || prof.getUser_name().equals("")){
                            result.add(prof.getFullName()+" "+Common.getLocalizedString("import_profs.no_username", locale));
                        }else{
                            //make sure they don't exist first.
                            ProfessionalVO username = new ProfessionalVO();
                            username.setUser_name(prof.getUser_name());
                            ProfessionalVO usernames = managerBD.getProfessional(username);
                            ProfessionalVO employeeid = new ProfessionalVO();
                            ProfessionalVO employees = null;
                            if(employeeid!=null && !employeeid.equals("")){
                                employeeid.setEmployeeid(prof.getEmployeeid());
                                employees = managerBD.getProfessional(employeeid);
                            }
                            if(usernames == null && employees == null){
                                managerBD.saveProfessional(prof, 6);
                                ProfessionalVO p = managerBD.getProfessional(prof);
                                if(treatment_id>0 && p!=null){
                                    UserAccountRegionsVO region = new UserAccountRegionsVO();
                                    region.setUser_account_id(p.getId());
                                    region.setTreatment_location_id(treatment_id);
                                    managerBD.saveTreatmentLocation(region);
                                }
                                result.add(prof.getFullName()+"(#"+p.getId()+") "+Common.getLocalizedString("import_profs.imported", locale));
                            }else{
                                result.add(prof.getFullName()+" "+Common.getLocalizedString("import_profs.username_exists", locale));
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    log.error("Unable to find Product file " + e.getMessage());
                } catch (IOException e) {
                    log.error("Unable to read Product file " + e.getMessage());
                } catch (ApplicationException e) {
                    log.error("Unable to save Product file " + e.getMessage());
                }
                request.setAttribute("professionalListImport",result);
                return (mapping.findForward("admin.professional.import"));
            } else {
                ProfessionalVO auth = (ProfessionalVO) session.getAttribute("userVO");
                if (auth == null) {//session is invalid.. return to login
                    request.setAttribute("session_invalid", "yes");
                    return (mapping.findForward("system.logoff"));
                }
                ProfessionalVO userVO = new ProfessionalVO();
                if ((request.getParameter("action").equals("add"))) {
                    userVO.setCreated_on(new Date());
                    userVO.setLastmodified_on(new Date());
                    log.info("ProfessionalAdmin.perform: Adding professional");
                    userVO.setPassword((MD5.hash(professionalAdminForm.getPassword())).toLowerCase());
                    userVO.setNew_user(new Integer(1));
                } else if (professionalAdminForm.getPassword().equals("")) {
                    log.info("ProfessionalAdmin.perform: Editing professional... ignoring password.");
                    userVO.setId(new Integer(professionalAdminForm.getId()));
                    userVO.setNew_user(new Integer(0));
                    userVO.setLastmodified_on(new Date());

                    ProfessionalVO tmpuserVO = (ProfessionalVO) managerBD.getProfessional(professionalAdminForm.getId());
                    if (tmpuserVO.getNew_user().equals(new Integer(1))) {
                        userVO.setNew_user(new Integer(1));
                    }
                    userVO.setPassword(tmpuserVO.getPassword());
                    userVO.setAnswer_one(tmpuserVO.getAnswer_one());
                    userVO.setAnswer_two(tmpuserVO.getAnswer_two());
                    userVO.setQuestion_two(tmpuserVO.getQuestion_two());
                    userVO.setQuestion_one(tmpuserVO.getQuestion_one());
                    userVO.setReset_password_method(tmpuserVO.getReset_password_method());
                } else {
                    log.info("ProfessionalAdmin.perform: Editing professional");
                    userVO.setPassword((MD5.hash(professionalAdminForm.getPassword())).toLowerCase());
                    userVO.setId(new Integer(professionalAdminForm.getId()));
                    userVO.setNew_user(new Integer(1));
                    log.info("ProfessionalAdmin.perform: Editing professional... inserting password into history");
                    PasswordHistoryVO historyVO = new PasswordHistoryVO();
                    historyVO.setProfessional_id(new Integer(professionalAdminForm.getId()));
                    historyVO.setCreated_on(new Date());
                    historyVO.setPassword((MD5.hash(professionalAdminForm.getPassword())).toLowerCase());
                    managerBD.insertPasswordForHistory(historyVO);
                }
                userVO.setAccount_status(new Integer(1));
                userVO.setPosition_id(professionalAdminForm.getPosition());
                userVO.setFirstname(professionalAdminForm.getFirst_name());
                userVO.setFirstname_search(Common.stripName(professionalAdminForm.getFirst_name()));
                userVO.setLastname(professionalAdminForm.getLast_name());
                userVO.setEmployeeid(professionalAdminForm.getEmployeeid());
                userVO.setLastname_search(Common.stripName(professionalAdminForm.getLast_name()));
                userVO.setPager(professionalAdminForm.getPager());
                if (professionalAdminForm.getPager() != null && professionalAdminForm.getPager().length() >= 10 && auth.getReset_password_method() == null) {
                    userVO.setReset_password_method(1);
                }
                userVO.setEmail(professionalAdminForm.getEmail());
                userVO.setIndividuals(professionalAdminForm.getAccess_admin_patients() + "");
                userVO.setTraining_flag((((professionalAdminForm.getTraining_flag() == null) ? (new Integer(0)) : new Integer(1))));
                userVO.setTimezone(professionalAdminForm.getTimezone());
                userVO.setAccess_viewer((((professionalAdminForm.getAccess_viewer() == null) ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_admin(((professionalAdminForm.getAccess_admin() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_allpatients(((professionalAdminForm.getAccess_allpatients() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_assigning(((professionalAdminForm.getAccess_assigning() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_professionals(((professionalAdminForm.getAccess_admin_professionals() == null ? (new Integer(0))
                        : new Integer(1))));
                userVO.setAccess_create_patient(((professionalAdminForm.getAccess_create_patient() == null ? (new Integer(0))
                        : new Integer(1))));

                userVO.setAccess_patients(((professionalAdminForm.getAccess_admin_patients() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_regions(((professionalAdminForm.getAccess_admin_regions() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_referrals(((professionalAdminForm.getAccess_admin_referrals() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_listdata(((professionalAdminForm.getAccess_admin_listdata() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_audit(((professionalAdminForm.getAccess_admin_audit() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_superadmin(((professionalAdminForm.getAccess_superadmin() != null && professionalAdminForm.getAccess_superadmin().equals("1") ? (new Integer(1)) : new Integer(0))));
                userVO.setAccess_ccac_reporting(((professionalAdminForm.getAccess_ccac_reporting() == 1 ? (new Integer(1)) : new Integer(0))));
                userVO.setAllow_remote_access(((professionalAdminForm.getAllow_remote_access() != null && professionalAdminForm.getAllow_remote_access() == 1 ? (new Integer(1)) : new Integer(0))));
                userVO.setUser_name(professionalAdminForm.getUser_name());
                com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl();
                if (professionalAdminForm.getTreatmentLocation() != null) {
                    for (int treatment_location_id : professionalAdminForm.getTreatmentLocation()) {
                        if (treatment_location_id > 0) {
                            try {
                                LookupVO lookup = bd.getListItem(treatment_location_id);
                                if (lookup.getUpdate_access().equals("1")) {
                                    userVO.setAccess_create_patient(1);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }
                }
                userVO.setAccess_uploader(((professionalAdminForm.getAccess_uploader() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setAccess_reporting(((professionalAdminForm.getAccess_reporting() == null ? (new Integer(0)) : new Integer(1))));
                userVO.setLocked(0);
                userVO.setInvalid_password_count(0);
                managerBD.saveProfessional(userVO, auth.getId());
                UserDAO userDAO = new UserDAO();
                userVO.setId(null);
                ProfessionalVO u = (ProfessionalVO) userDAO.findByCriteria(userVO);
                if (u != null && u.getId() != null) {
                    //remove old access rights first.
                    managerBD.removeTreatmentLocation(u.getId());
                    if ((request.getParameter("action").equals("add"))) {
                        if (professionalAdminForm.getTraining_flag() != null && professionalAdminForm.getTraining_flag().equals("1")) {
                            UserAccountRegionsVO t = new UserAccountRegionsVO();
                            t.setTreatment_location_id(Constants.TRAINING_TREATMENT_ID);
                            t.setUser_account_id(u.getId());
                            managerBD.saveTreatmentLocation(t);

                        }
                        if (professionalAdminForm.getTreatmentLocation() != null) {
                            for (Integer t : professionalAdminForm.getTreatmentLocation()) {
                                UserAccountRegionsVO tmp = new UserAccountRegionsVO();
                                tmp.setTreatment_location_id(t);
                                tmp.setUser_account_id(u.getId());
                                managerBD.saveTreatmentLocation(tmp);
                            }
                        }
                    } else if (request.getParameter("action").equals("edit")) {
                        if (professionalAdminForm.getTraining_flag() != null && professionalAdminForm.getTraining_flag().equals("1")) {
                            UserAccountRegionsVO t = new UserAccountRegionsVO();
                            t.setTreatment_location_id(Constants.TRAINING_TREATMENT_ID);
                            t.setUser_account_id(u.getId());
                            managerBD.saveTreatmentLocation(t);
                        }
                        if (professionalAdminForm.getTreatmentLocation() != null) {
                            for (Integer t : professionalAdminForm.getTreatmentLocation()) {
                                if ( professionalAdminForm.getTraining_flag() == null && t == Constants.TRAINING_TREATMENT_ID) {
                                    //ignore location
                                }else{
                                    UserAccountRegionsVO tmp = new UserAccountRegionsVO();
                                    tmp.setTreatment_location_id(t);
                                    tmp.setUser_account_id(u.getId());
                                    managerBD.saveTreatmentLocation(tmp);
                                }
                            }
                        }
                    }

                    //@todo why are we doing this ttwice?
                    ProfessionalVO prof = (ProfessionalVO) userDAO.findByCriteria(userVO);
                    if (prof != null && prof.getId().equals(auth.getId())) {
                        session.setAttribute(Constants.PROFESSIONAL, prof);
                    }
                    session.setAttribute("admin_pro_id", u.getId() + "");
                    //if Professional is a Patient
                    //System.out.println("Assign a Patient" + u.getPosition().getTitle());
                    if (u.getPosition().getTitle().equals("Patient") && (professionalAdminForm.getPatient_id() != null && professionalAdminForm.getPatient_id() > 0) || (professionalAdminForm.getPatient_id_edit() != null && professionalAdminForm.getPatient_id_edit() > 0)) {
                        int patient_id = 0;
                        if (request.getParameter("action").equals("edit")) {
                            patient_id = professionalAdminForm.getPatient_id_edit();
                        } else if (request.getParameter("action").equals("add")) {
                            patient_id = professionalAdminForm.getPatient_id();
                        }
                        if (patient_id > 0) {
                            int user_id = u.getId();
                            AssignPatientsVO tmp = new AssignPatientsVO();
                            AssignPatientsServiceImpl aservice = new AssignPatientsServiceImpl();
                            tmp.setPatientId(patient_id);
                            tmp.setProfessionalId(user_id);
                            AssignPatientsVO check = aservice.getAssignedPatient(tmp);
                            if (check != null) {
                                check.setPatientId(patient_id);
                                check.setAssignedBy(auth.getId());
                                check.setAccountType("Patient");
                                check.setCreated_on(new Date());
                                aservice.saveAssignedPatient(check);
                            } else {
                                check = new AssignPatientsVO();
                                check.setPatientId(patient_id);
                                check.setProfessionalId(user_id);
                                check.setAssignedBy(auth.getId());
                                check.setAccountType("Patient");
                                check.setCreated_on(new Date());
                                aservice.saveAssignedPatient(check);
                            }
                        }
                    }
                }
            }
        } catch (com.pixalere.common.DataAccessException e) {
            log.error("An application exception has been raised in LogAuditAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in ProfessionalAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        try {
            LookupVO vo = new LookupVO();
            ResourcesServiceImpl resourcesBD = new ResourcesServiceImpl();
            // com.pixalere.professional.ProfessionalVO
            // userVO =(com.pixalere.professional.ProfessionalVO)session.getAttribute("userVO");
            com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
            com.pixalere.auth.bean.ProfessionalVO userVO = null;
            Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, true);
            Vector treatmentCatsVO = (Vector) resourcesBD.getAllResources(vo.TREATMENT_LOCATION);

            request.setAttribute("positions", managerBD.getPositions(new PositionVO(Constants.ACTIVE)));
            request.setAttribute("treatmentCats", treatmentCatsVO);
            request.setAttribute("treatmentList", treatmentVO);
            Vector treatmentVO2 = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
            request.setAttribute("treatmentList2", treatmentVO2);
            Vector treatmentTrainingWithCatsVO = (Vector) bd.findAllTrainingTreatments(userVO);
            request.setAttribute("treatmentTraining", treatmentTrainingWithCatsVO);
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.professional.success"));
        }
    }
}
