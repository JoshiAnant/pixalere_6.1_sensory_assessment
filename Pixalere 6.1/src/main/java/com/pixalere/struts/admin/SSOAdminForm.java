package com.pixalere.struts.admin;

import com.pixalere.sso.bean.SSOProviderVO;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author Jose
 * @since 6.0
 */
public class SSOAdminForm extends ActionForm {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOAdminForm.class);
    
    private int idProvider;
    private String active;
    private String idpIdentityId;
    private String pixIdentityId;
    private String ssoType;
    private String saml2BindingType;
    private String description;
    private String saml2SSOUrl;
    private String logoutRequestUrl;
    private String isLogoutEnabled;
    private String saml2ArtifactUrl;
    private String isAuthRequestSigned;
    private String isAuthnResponseSigned;
    private String isAuthRequestEncrypted;
    private String isAuthResponseEncrypted;
    private String isLogoutRequestSigned;
    private String certFilepath;
    private String certAlgorithm;
    
    public SSOProviderVO getUpdatedProvider(SSOProviderVO vo){
        // Checkboxes values
        int i_active = (active == null)? 0: 1;
        int i_isLogoutEnabled = (isLogoutEnabled == null)? 0: 1;
        int i_isAuthRequestSigned = (isAuthRequestSigned == null)? 0: 1;
        int i_isAuthnResponseSigned = (isAuthnResponseSigned == null)? 0: 1;
        int i_isAuthRequestEncrypted = (isAuthRequestEncrypted == null)? 0: 1;
        int i_isAuthResponseEncrypted = (isAuthResponseEncrypted == null)? 0: 1;
        int i_isLogoutRequestSigned = (isLogoutRequestSigned == null)? 0: 1;
        
        vo.setActive(i_active);
        vo.setIsLogoutEnabled(i_isLogoutEnabled);
        vo.setIsAuthRequestSigned(i_isAuthRequestSigned);
        vo.setIsAuthnResponseSigned(i_isAuthnResponseSigned);
        vo.setIsAuthRequestEncrypted(i_isAuthRequestEncrypted);
        vo.setIsAuthResponseEncrypted(i_isAuthResponseEncrypted);
        vo.setIsLogoutRequestSigned(i_isLogoutRequestSigned);
        
        // String values
        vo.setSsoType((ssoType == null? ssoType: ssoType.trim()));
        vo.setDescription((description == null? description: description.trim()));
        vo.setPixIdentityId((pixIdentityId == null? pixIdentityId: pixIdentityId.trim()));
        vo.setIdpIdentityId((idpIdentityId == null? idpIdentityId: idpIdentityId.trim()));
        vo.setSaml2SSOUrl((saml2SSOUrl == null? saml2SSOUrl: saml2SSOUrl.trim()));
        vo.setSaml2ArtifactUrl((saml2ArtifactUrl == null? saml2ArtifactUrl: saml2ArtifactUrl.trim()));
        vo.setSaml2BindingType((saml2BindingType == null? saml2BindingType: saml2BindingType.trim()));
        vo.setLogoutRequestUrl((logoutRequestUrl == null? logoutRequestUrl: logoutRequestUrl.trim()));
        vo.setCertAlgorithm((certAlgorithm == null? certAlgorithm: certAlgorithm.trim()));
        vo.setCertFilepath((certFilepath == null? certFilepath: certFilepath.trim()));

        
        return vo;
    }

    public int getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(int idProvider) {
        this.idProvider = idProvider;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getIdpIdentityId() {
        return idpIdentityId;
    }

    public void setIdpIdentityId(String idpIdentityId) {
        this.idpIdentityId = idpIdentityId;
    }

    public String getPixIdentityId() {
        return pixIdentityId;
    }

    public void setPixIdentityId(String pixIdentityId) {
        this.pixIdentityId = pixIdentityId;
    }

    public String getSsoType() {
        return ssoType;
    }

    public void setSsoType(String ssoType) {
        this.ssoType = ssoType;
    }

    public String getSaml2BindingType() {
        return saml2BindingType;
    }

    public void setSaml2BindingType(String saml2BindingType) {
        this.saml2BindingType = saml2BindingType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSaml2SSOUrl() {
        return saml2SSOUrl;
    }

    public void setSaml2SSOUrl(String saml2SSOUrl) {
        this.saml2SSOUrl = saml2SSOUrl;
    }

    public String getLogoutRequestUrl() {
        return logoutRequestUrl;
    }

    public void setLogoutRequestUrl(String logoutRequestUrl) {
        this.logoutRequestUrl = logoutRequestUrl;
    }

    public String getIsLogoutEnabled() {
        return isLogoutEnabled;
    }

    public void setIsLogoutEnabled(String isLogoutEnabled) {
        this.isLogoutEnabled = isLogoutEnabled;
    }

    public String getSaml2ArtifactUrl() {
        return saml2ArtifactUrl;
    }

    public void setSaml2ArtifactUrl(String saml2ArtifactUrl) {
        this.saml2ArtifactUrl = saml2ArtifactUrl;
    }

    public String getIsAuthRequestSigned() {
        return isAuthRequestSigned;
    }

    public void setIsAuthRequestSigned(String isAuthRequestSigned) {
        this.isAuthRequestSigned = isAuthRequestSigned;
    }

    public String getIsAuthnResponseSigned() {
        return isAuthnResponseSigned;
    }

    public void setIsAuthnResponseSigned(String isAuthnResponseSigned) {
        this.isAuthnResponseSigned = isAuthnResponseSigned;
    }

    public String getIsAuthRequestEncrypted() {
        return isAuthRequestEncrypted;
    }

    public void setIsAuthRequestEncrypted(String isAuthRequestEncrypted) {
        this.isAuthRequestEncrypted = isAuthRequestEncrypted;
    }

    public String getIsAuthResponseEncrypted() {
        return isAuthResponseEncrypted;
    }

    public void setIsAuthResponseEncrypted(String isAuthResponseEncrypted) {
        this.isAuthResponseEncrypted = isAuthResponseEncrypted;
    }

    public String getIsLogoutRequestSigned() {
        return isLogoutRequestSigned;
    }

    public void setIsLogoutRequestSigned(String isLogoutRequestSigned) {
        this.isLogoutRequestSigned = isLogoutRequestSigned;
    }

    public String getCertFilepath() {
        return certFilepath;
    }

    public void setCertFilepath(String certFilepath) {
        this.certFilepath = certFilepath;
    }

    public String getCertAlgorithm() {
        return certAlgorithm;
    }

    public void setCertAlgorithm(String certAlgorithm) {
        this.certAlgorithm = certAlgorithm;
    }
    
   
}
