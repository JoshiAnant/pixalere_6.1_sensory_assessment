package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.admin.dao.AssignPatientsDAO;
import com.pixalere.admin.bean.AssignPatientsVO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 * Struts validation class
 *
 * @author travis morris
 * @since 3.0
 */
public class AssignPatientsAdminForm extends ActionForm {
  static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssignPatientsAdminForm.class);
  private int id;
  private String name;
  /**
   * validate form data, and return to setupaction.
   * @param mapping
   * @param request
   * @return errors if error as thrown
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    HttpSession session=request.getSession();
    //if assignment button was clicked, validate input.
    if(request.getParameter("patientSubmit")!=null ){
        try{
            AssignPatientsDAO assign=new AssignPatientsDAO();
            AssignPatientsVO tmp=new AssignPatientsVO();
            tmp.setPatientId(new Integer((String)request.getParameter("patients")));
            tmp.setProfessionalId(new Integer((String)session.getAttribute("assignProfessionalId")));
            AssignPatientsVO vo=(AssignPatientsVO)assign.findByCriteria(tmp);
            //if assignment already exists, no sense duplicating,throw an error alerting
            //the user of the existing assignment.
            if(vo!=null){
                session.setAttribute("professionalSubmit","");
            	request.setAttribute("selected",session.getAttribute("assignProfessionalId"));
            	session.setAttribute("assignProfessionalId",session.getAttribute("assignProfessionalId"));
                errors.add("assignpatients",new ActionError("pixalere.admin.assignpatients.exists.error"));
            }
        }catch(Exception e){
           //saveErrors(request, errors);
        }
    }
    return errors;
  }
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    setId(0);
    setName("");
  }
  public void setId(int id) {
    this.id = id;
  }
  public int getId() {
    return id;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getName() {
    return name;
  }
}

