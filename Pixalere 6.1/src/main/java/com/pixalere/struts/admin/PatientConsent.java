/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.admin;

import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.struts.reporting.ParameterAction;
import com.pixalere.struts.reporting.ParameterForm;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author User
 */
public class PatientConsent extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParameterAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        PatientConsentForm consentForm = (PatientConsentForm) form;
        Integer language = Common.getLanguageIdFromSession(session);
        String patient_id = (String) session.getAttribute("patient_id");
        
        if (patient_id == null) {
            return (mapping.findForward("admin.patient.consent_error"));
        }
        
        try {
            PatientServiceImpl patientService = new PatientServiceImpl(language);
            PatientAccountVO patient = patientService.getPatient(new Integer((String) patient_id));

            if (patient == null) {
                return (mapping.findForward("admin.patient.consent_error"));
            }

            patient.setConsent_information(consentForm.getConsent_information());
            patient.setConsent_services(consentForm.getConsent_services());
            patient.setConsent_verbal(consentForm.getConsent_verbal());
            patient.setPatient_consent_name(consentForm.getPatient_consent_name());

            // Save image to disk
            String signature = patientService.recordSignatureAsImage(patient, consentForm.getSignatureURI());
            patient.setPatient_consent_sig(signature);
            
            // Make sure signature/verbal are exclusive (null if other not null)
            if (signature != null){
                patient.setConsent_verbal(null);
            }

            // Generate HTML blurb from data
            String consentIntro = 
                    "<div class=\"subsection\" id=\"intro\">" +
                    "<p>" + Common.getConfig("consentIntro").replace("\n", "</p><p>") + "</p>" +
                    "</div>";
            String criteria1 = 
                    "<div class=\"criteria\">\n" +
                    "       <p>"+ Common.getConfig("consentCriteria1") + "</p>" +
                    "       <div class=\"criteria_content\">" + 
                    "          " + consentForm.getCriteria1().replace("\n", "</br>") +
                    "       </div>" +
                    "</div>";
            String criteria2 = 
                    "<div class=\"criteria\">\n" +
                    "       <p>"+ Common.getConfig("consentCriteria2") + "</p>" +
                    "       <div class=\"criteria_content\">" + 
                    "          " + consentForm.getCriteria2().replace("\n", "</br>") +
                    "       </div>" +
                    "</div>";
            String criterias = 
                    "<div class=\"subsection\" id=\"criterias\">\n" +
                    criteria1 + criteria2 +
                    "</div>";
            String consentSharingHeader = 
                    "<div class=\"subsection\" id=\"sharing\">" +
                    "   <p>" + Common.getConfig("consentSharingHeader").replace("\n", "</p><p>") + "</p>" +
                    "   <div class=\"sharing_content\">" + 
                    consentForm.getSharing().replace("\n", "</br>") +
                    "   </div>" +
                    "</div>";
            String consentDetails = 
                    "<div class=\"subsection\" id=\"details\">" +
                    "<p>" + Common.getConfig("consentDetails").replace("\n", "</p><p>") + "</p>" +
                    "</div>";
            String acceptServices = 
                    "<div class=\"subsection\" id=\"checkbox_services\">\n" +
            "            <dl>\n" +
            "                <dd>\n" +
            "                    <input type=\"checkbox\" name=\"consent_services\" id=\"consent_services_chk\" value=\"1\" checked=\"checked\" disabled=\"disabled\" \n" +
            "                        /> &nbsp; <label for=\"consent_services_chk\">" + Common.getConfig("consentServicesChk") + "</label>\n" +
            "                </dd>\n" +
            "            </dl>\n" +
            "        </div>";
            String consentDisclaimers = 
                    "<div class=\"subsection\" id=\"disclaimers\">" +
                    "<p>" + Common.getConfig("consentDisclaimers").replace("\n", "</p><p>") + "</p>" +
                    "</div>";
            String acceptInformation = 
                    "<div class=\"subsection\" id=\"checkbox_information\">\n" +
            "            <dl>\n" +
            "                <dd>\n" +
            "                    <input type=\"checkbox\" name=\"consent_information\" id=\"consent_information_chk\" value=\"1\" checked=\"checked\" disabled=\"disabled\" \n" +
            "                        /> &nbsp; <label for=\"consent_information_chk\">" + Common.getConfig("consentInformationChk") + "</label>\n" +
            "                </dd>\n" +
            "            </dl>\n" +
            "        </div>";
            String consentAcceptance = 
                    "<div class=\"subsection\" id=\"acceptance\">" +
                    "<p>" + Common.getConfig("consentAcceptance").replace("\n", "</p><p>") + "</p>" + 
                    "</div>";
            
            // Contract content
            // * Note: It doesn't contain the name/date/signature sections,
            // that should be added on recovery when consulting/revoking.
            String patientBlurb = 
                    consentIntro +
                    criterias +
                    consentSharingHeader +
                    consentDetails +
                    acceptServices +
                    consentDisclaimers +
                    acceptInformation +
                    consentAcceptance;
            patient.setPatient_consent_blurb(patientBlurb);
            // Save Timestamp
            patient.setConsent_date(new Date());

            // Update patient (Is correct to call this method? - see comment on meditech)
            patientService.updatePatient(patient);
        } catch (Exception e) {
            System.out.println("Error saving consent:" + e.toString());
            return (mapping.findForward("admin.patient.consent_error"));
        }
        
        return (mapping.findForward("admin.patient.consent_saved"));
    }
    
}
