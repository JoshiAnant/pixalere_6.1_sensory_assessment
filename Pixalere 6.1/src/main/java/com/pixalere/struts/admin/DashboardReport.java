package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.ServletContext;
import com.pixalere.utils.Common;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.reporting.bean.*;
/**
 * Save the Dashboard Report by User from GUI
 *
 * @since 5.0
 * @author travis morris
 */
public class DashboardReport extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DashboardReport.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
        try {
            DashboardReportForm reportform = (DashboardReportForm) form;
            ReportingServiceImpl rservice = new ReportingServiceImpl();
            DashboardReportVO dash = new DashboardReportVO();
            dash.setEmail(reportform.getEmail());
            if (reportform.getEtiology_all() == 1) {
                dash.setEtiology_all(1);
            } else {
                dash.setEtiology_all(0);
            }
            dash.setWound_type(reportform.getWound_type());
            rservice.saveDashboardReport(dash);
            DashboardReportVO report = rservice.getDashboardReport(dash);
            if (report != null) {
                if (reportform.getTreatmentLocation() != null) {
                    for (int treat : reportform.getTreatmentLocation()) {
                        DashboardByLocationVO d = new DashboardByLocationVO();
                        d.setDashboard_report_id(report.getId());
                        d.setTreatment_location_id(treat);
                        rservice.saveDashboardByLocation(d);
                    }
                }
                if (reportform.getEtiologies() != null) {
                    for (int et : reportform.getEtiologies()) {
                        DashboardByEtiologyVO d = new DashboardByEtiologyVO();
                        d.setDashboard_report_id(report.getId());
                        d.setEtiology_id(et);
                        rservice.saveDashboardByEtiology(d);
                    }
                }
            }
        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward(
                "admin.dashboardreport.success"));
    }
}
