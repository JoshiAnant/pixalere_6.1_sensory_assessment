/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.utils.PDate;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
/**
 *
 * @author travis
 */
public class PatientUnassignAction extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientUnassignAction.class);
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PatientUnassignForm patientAdminForm = (PatientUnassignForm) form;
        HttpSession session = request.getSession();
        try {
            String patient_id = "";
            if(session.getAttribute("patient_id")!=null){
                patient_id=(String)session.getAttribute("patient_id");
            }
            
            
            ProfessionalVO userVO = (ProfessionalVO)session.getAttribute("userVO");
            if(patientAdminForm.getUnassign_patient_id()>0 && session.getAttribute("userVO")!=null){
                AssignPatientsVO assign=new AssignPatientsVO();
                AssignPatientsServiceImpl pservice = new AssignPatientsServiceImpl();
                assign.setPatientId(patientAdminForm.getUnassign_patient_id());
                assign.setProfessionalId(userVO.getId());
                pservice.removeAssignedPatient(assign);
                if(!patient_id.equals("") && patientAdminForm.getUnassign_patient_id() == Integer.parseInt(patient_id)){
                    session.removeAttribute("patient_id");
                    session.removeAttribute("patientAccount");
                }
                   
            }else if(patientAdminForm.getUnassign_patient_id()==-411 && session.getAttribute("userVO")!=null){
                AssignPatientsVO assign=new AssignPatientsVO();
                AssignPatientsServiceImpl pservice = new AssignPatientsServiceImpl();
                assign.setProfessionalId(userVO.getId());
                pservice.removeAssignedPatient(assign);
            }

        } catch (Exception e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        log.debug("Leaving PatientAdminSearch ****************");
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            request.setAttribute("tab_select","searchDiv");
            return (mapping.findForward("admin.patient.success"));
        }
    }

}
