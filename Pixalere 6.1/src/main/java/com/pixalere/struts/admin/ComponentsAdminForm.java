/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
/**
 *
 * @author travis
 */
public class ComponentsAdminForm  extends ActionForm{
    private Integer component_id;
    private Integer id;
    private Integer order_by;
    private Integer required;
    private Integer ignore_info_popup;
    private String validation;
    private Integer num_columns;
    
    private Integer allow_gui;
    private Integer allow_flowchart;
    private Integer allow_edit;
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ComponentsAdminForm.class);
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {
        HttpSession session=request.getSession();
        ActionErrors errors= new ActionErrors();
        return errors;
    }
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    }
    /**
     * @return the component_id
     */
    public Integer getComponent_id() {
        return component_id;
    }
    /**
     * @param component_id the component_id to set
     */
    public void setComponent_id(Integer component_id) {
        this.component_id = component_id;
    }
    /**
     * @return the order_by
     */
    public Integer getOrder_by() {
        return order_by;
    }
    /**
     * @param order_by the order_by to set
     */
    public void setOrder_by(Integer order_by) {
        this.order_by = order_by;
    }
    /**
     * @return the required
     */
    public Integer getRequired() {
        return required;
    }
    /**
     * @param required the required to set
     */
    public void setRequired(Integer required) {
        this.required = required;
    }
    /**
     * @return the ignore_info_popup
     */
    public Integer getIgnore_info_popup() {
        return ignore_info_popup;
    }
    /**
     * @param ignore_info_popup the ignore_info_popup to set
     */
    public void setIgnore_info_popup(Integer ignore_info_popup) {
        this.ignore_info_popup = ignore_info_popup;
    }
    /**
     * @return the validation
     */
    public String getValidation() {
        return validation;
    }
    /**
     * @param validation the validation to set
     */
    public void setValidation(String validation) {
        this.validation = validation;
    }
    /**
     * @return the allow_edit
     */
    public Integer getAllow_edit() {
        return allow_edit;
    }
    /**
     * @param allow_edit the allow_edit to set
     */
    public void setAllow_edit(Integer allow_edit) {
        this.allow_edit = allow_edit;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the num_columns
     */
    public Integer getNum_columns() {
        return num_columns;
    }
    /**
     * @param num_columns the num_columns to set
     */
    public void setNum_columns(Integer num_columns) {
        this.num_columns = num_columns;
    }

    /**
     * @return the allow_gui
     */
    public Integer getAllow_gui() {
        return allow_gui;
    }

    /**
     * @param allow_gui the allow_gui to set
     */
    public void setAllow_gui(Integer allow_gui) {
        this.allow_gui = allow_gui;
    }

    /**
     * @return the allow_flowchart
     */
    public Integer getAllow_flowchart() {
        return allow_flowchart;
    }

    /**
     * @param allow_flowchart the allow_flowchart to set
     */
    public void setAllow_flowchart(Integer allow_flowchart) {
        this.allow_flowchart = allow_flowchart;
    }
}
