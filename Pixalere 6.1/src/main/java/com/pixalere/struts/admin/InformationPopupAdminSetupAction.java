package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.InformationPopupVO;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Common;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
public class InformationPopupAdminSetupAction
        extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InformationPopupAdminSetupAction.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        request.setAttribute("page", "admin");
        request.setAttribute("action", "add");
        HttpSession session = request.getSession();
        
        GUIServiceImpl manager=new GUIServiceImpl();
        ListServiceImpl iservice = new ListServiceImpl();
        if(request.getParameter("resource_id") == null){
            request.setAttribute("resource_id",session.getAttribute("resource_id"));
        }
        
        try{
            Collection components = manager.getAllComponents();
            request.setAttribute("components",components);
            if(request.getParameter("selectComponent")!=null && request.getParameter("selectComponent").equals("yes") && request.getParameter("component_id")!=null && !((String)request.getParameter("component_id")).equals("")){
                request.setAttribute("selected",request.getParameter("component_id"));
                
                ComponentsVO info = manager.getComponent(new Integer(request.getParameter("component_id")).intValue());
                request.setAttribute("info",info);
                if(info!=null){
                    Collection<LookupVO> list = iservice.getLists(info.getResource_id());
                    request.setAttribute("list",list);
                }
            }
        }catch (ApplicationException e){
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        }else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        }  else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }	else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.info.success"));
        }
    }
    
}
