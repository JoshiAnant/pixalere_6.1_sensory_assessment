/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.service.TriggerServiceImpl;
import com.pixalere.common.bean.TriggerVO;
import com.pixalere.common.bean.TriggerCriteriaVO;
import java.util.Collection;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.TriggerProductsVO;
import com.pixalere.utils.Common;
import java.util.ArrayList;
import java.util.Iterator;
/**
 *
 * @author travis
 */
public class TriggerSetupAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        
        TriggerServiceImpl tservice = new TriggerServiceImpl();
        ProductsServiceImpl pservice = new ProductsServiceImpl();
        
        try {
            String action = request.getParameter("action");
            request.setAttribute("action", action);
            
            // List of Products are loaded only for trigger create or trigger edit or delete
            if(request.getParameter("trigger_id")==null 
                    || ((String)request.getParameter("trigger_id")).equals("")
                    || (action.equals("edit"))
                    || (action.equals("trigger_delete"))
                    ) {
                ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
                
                Collection<ProductsVO> products = pservice.getAllProducts(userVO);
                request.setAttribute("products",products);
            }
            
            
            if(request.getParameter("trigger_id")==null 
                    || ((String)request.getParameter("trigger_id")).equals("") 
                    || (action.equals("trigger_delete"))
                    ) {
                if (action != null && action.equals("trigger_delete")) {
                    TriggerVO trigger = tservice.getTrigger(new Integer(request.getParameter("trigger_id")));
                    // Delete products relationships
                    Iterator<TriggerProductsVO> itProd = trigger.getProduct_ids().iterator();
                    while (itProd.hasNext()){
                        TriggerProductsVO oldProductId = itProd.next();
                        tservice.deleteTriggerProducts(oldProductId);
                        itProd.remove();
                    }
                    // Delete conditions
                    Iterator<TriggerCriteriaVO> itCond = trigger.getOptions().iterator();
                    while (itCond.hasNext()){
                        TriggerCriteriaVO condition = itCond.next();
                        tservice.deleteTriggerCriteria(condition);
                        itCond.remove();
                    }
                    // Delete trigger
                    tservice.deleteTrigger(trigger);
                    request.setAttribute("action", "");
                }
                
                // Load all triggers for trigger create
                Collection<TriggerVO> triggers = tservice.getAllTriggers();
                request.setAttribute("triggers", triggers);
            } else {
                TriggerVO trigger = tservice.getTrigger(new Integer(request.getParameter("trigger_id")));
                request.setAttribute("trigger_id",request.getParameter("trigger_id"));
                request.setAttribute("trigger",trigger);
                
                if (action.equals("products")) {
                    Collection<ProductsVO> recommendedList = new ArrayList<ProductsVO>(trigger.getProduct_ids().size());

                    for(TriggerProductsVO product_id : trigger.getProduct_ids()){
                        ProductsVO product = pservice.getProduct(product_id.getProduct_id());
                        recommendedList.add(product);
                    }
                    request.setAttribute("recommendedList",recommendedList);
                    
                } else if (action.equals("edit_product")){
                    request.setAttribute("trigger_name",trigger.getName());
                    
                    ProductsVO product = (ProductsVO) pservice.getProduct(Integer.parseInt(request.getParameter("product_id")));
                    request.setAttribute("choice", product.getId() + "");
                    request.setAttribute("product", product.getTitle());
                    request.setAttribute("action", "edit_product");
                    
                    request.setAttribute("recommendation_type", product.getRecommendation_type());
                    request.setAttribute("url",product.getUrl());
                    request.setAttribute("video_url",product.getVideo_url());
                    request.setAttribute("pdf",product.getPdf());
                    request.setAttribute("display_image",product.getDisplay_image());
                    request.setAttribute("image",product.getImage());
                    request.setAttribute("display_description",product.getDisplay_description());
                    request.setAttribute("description",product.getDescription());
                    
                }
            }
                

        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if ((request.getParameter("page") != null)) {
            if (request.getParameter("page").equals("patient")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.patient");
            }
            if (request.getParameter("page").equals("woundprofiles")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.profile");
            }
            if (request.getParameter("page").equals("assess")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.assess");
            }
            if (request.getParameter("page").equals("viewer")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.viewer");
            }
            if (request.getParameter("page").equals("treatment")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.treatment");
            }
            if (request.getParameter("page").equals("summary")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.summary");
            }
            if (request.getParameter("page").equals("reporting")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.reporting");
            }
            if (request.getParameter("page").equals("admin")) {
                session.removeAttribute("admin_patient_id");
                return mapping.findForward("uploader.go.admin");
            }
        }
        return (mapping.findForward("admin.trigger.success"));
    }
}
