package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import com.pixalere.admin.service.ResourcesServiceImpl;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.service.LicenseServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.common.bean.LanguageVO;
import java.util.ArrayList;
import java.util.Iterator;
public class ResourcesAdminSetupAction
        extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ResourcesAdminSetupAction.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        request.setAttribute("page", "admin");
        if (request.getAttribute("action") == null) {
            request.setAttribute("action", "add");
        }
        HttpSession session = request.getSession();
        ListServiceImpl listservice = new ListServiceImpl();
        try {
            Collection<LanguageVO> languages = listservice.getLanguages();
            // On Manage Resources we only show main languages, no country localizations
            // so we need to remove country locales from the list
            for (Iterator<LanguageVO> it = languages.iterator(); it.hasNext();) {
                LanguageVO languageVO = it.next();
                if (languageVO.getName().indexOf("_") > 0){
                    it.remove();
                }
            }
            request.setAttribute("languages", languages);
        } catch (ApplicationException e) {
            Collection<LanguageVO> languages = new ArrayList();
            LanguageVO l = new LanguageVO();
            l.setId(1);
            l.setName("en");
            languages.add(l);
            request.setAttribute("languages", languages);
        }
        if (request.getParameter("resource_id") != null) {
            request.setAttribute("resource_id", session.getAttribute("resource_id"));
        }
        log.info("##$$##$$##-----RESOURCE ID after if " + request.getAttribute("resource_id") + "-----##$$##$$##");
        if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("1"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("1"))) {
            setupList(new Integer(1), "Funding Source", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("2"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("2"))) {
            setupList(new Integer(2), "Referral Source", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("3"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("3"))) {
            setupList(new Integer(3), "Treatment Location", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
            LicenseServiceImpl lservice = new LicenseServiceImpl();
            String num_facilities = lservice.getNumFacilities();
            request.setAttribute("num_facilities", num_facilities);

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("4"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("4"))) {
            setupList(new Integer(4), "Patient Residence", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("5"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("5"))) {
            setupList(new Integer(5), "Co-morbidities", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("6"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("6"))) {
            setupList(new Integer(6), "Intefering Medications", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("7"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("7"))) {
            setupList(new Integer(7), "Gender", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("8"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("8"))) {
            setupList(new Integer(8), "Etiology", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("9"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("9"))) {
            setupList(new Integer(9), "Program Stay", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("10"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("10"))) {
            setupList(new Integer(10), "Program Stay Variance", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("11"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("11"))) {
            setupList(new Integer(11), "Patient Care", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("12"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("12"))) {
            setupList(new Integer(12), "Pressure Ulcer Stage", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("13"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("13"))) {
            setupList(new Integer(13), "Albumin", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("14"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("14"))) {
            setupList(new Integer(14), "Pre Albumin", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("15"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("15"))) {
            setupList(new Integer(15), "Infections", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("16"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("16"))) {
            setupList(new Integer(16), "Investigations", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("17"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("17"))) {
            setupList(new Integer(17), "VAC", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("18"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("18"))) {
            setupList(new Integer(18), "Limb Color", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("19"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("19"))) {
            setupList(new Integer(19), "Limb Temperature", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("20"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("20"))) {
            setupList(new Integer(20), "Limb Edema", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("21"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("21"))) {
            setupList(new Integer(21), "Limb Edema Severity", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("22"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("22"))) {
            setupList(new Integer(22), "Doppler", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("23"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("23"))) {
            setupList(new Integer(23), "Palpation", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("24"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("24"))) {
            setupList(new Integer(24), "Exudate Type", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("25"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("25"))) {
            setupList(new Integer(25), "Exudate Amount", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("26"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("26"))) {
            setupList(new Integer(26), "Odour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("28"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("28"))) {
            setupList(new Integer(28), "Dressing Change Frequency", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("29"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("29"))) {
            setupList(new Integer(29), "Nursing Visit Frequency", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("30"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("30"))) {
            setupList(new Integer(30), "Priority", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("31"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("31"))) {
            setupList(new Integer(31), "Wound Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("32"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("32"))) {
            setupList(new Integer(32), "Discharge Reason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("33"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("33"))) {
            setupList(new Integer(33), "Wound Bed", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("34"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("34"))) {
            setupList(new Integer(34), "Wound Edge", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("35"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("35"))) {
            setupList(new Integer(35), "Wound Bed Percent", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("36"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("36"))) {
            setupList(new Integer(36), "Colour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("37"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("37"))) {
            setupList(new Integer(37), "Periwound Skin", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("38"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("38"))) {
            setupList(new Integer(38), "C&S Result", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("39"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("39"))) {
            setupList(new Integer(39), "C&S Route", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("40"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("40"))) {
            setupList(new Integer(40), "Pressure Reading", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("41"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("41"))) {
            setupList(new Integer(41), "Endocrine/Metabolic/Nutritional", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("42"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("42"))) {
            setupList(new Integer(42), "Heart/Circulation", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("43"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("43"))) {
            setupList(new Integer(43), "Muscoloskeletal", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("44"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("44"))) {
            setupList(new Integer(44), "Nuerological", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("45"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("45"))) {
            setupList(new Integer(45), "Psychiatric/Mood", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("46"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("46"))) {
            setupList(new Integer(46), "Pulmonary", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("47"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("47"))) {
            setupList(new Integer(47), "Sensory", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("48"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("48"))) {
            setupList(new Integer(48), "Infections", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("49"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("49"))) {
            setupList(new Integer(49), "Other", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("50"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("50"))) {
            setupList(new Integer(50), "Treatment Category", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("51"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("51"))) {
            setupList(new Integer(51), "Treatment Location", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
            setupCategories(request);

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("54"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("54"))) {
            setupList(new Integer(54), "Blood Sugar Meal", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("55"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("55"))) {
            setupList(new Integer(55), "Visit", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("56"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("56"))) {
            setupList(new Integer(56), "Position", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("58"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("58"))) {
            setupList(new Integer(58), "Interfering Medications", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("59"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("59"))) {
            setupList(new Integer(59), "Pouching System", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("60"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("60"))) {
            setupList(new Integer(60), "Close Proximity of Stoma", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("61"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("61"))) {
            setupList(new Integer(61), "Stool - Colour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("62"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("62"))) {
            setupList(new Integer(62), "Type of Ostomy", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("63"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("63"))) {
            setupList(new Integer(63), "Application changed by", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("64"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("64"))) {
            setupList(new Integer(64), "Psycho-social Concerns", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("65"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("65"))) {
            setupList(new Integer(65), "Nutritional Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("66"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("66"))) {
            setupList(new Integer(66), "Urine - Colour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("67"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("67"))) {
            setupList(new Integer(67), "Skin - Texture", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("68"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("68"))) {
            setupList(new Integer(68), "Stoma - Colour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("69"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("69"))) {
            setupList(new Integer(69), "Rectal Discharge", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("70"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("70"))) {
            setupList(new Integer(70), "Urine - Type", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("71"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("71"))) {
            setupList(new Integer(71), "Urine - Quantity", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("72"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("72"))) {
            setupList(new Integer(72), "Stool - Consistency", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("73"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("73"))) {
            setupList(new Integer(73), "Stool - Quantity", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("74"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("74"))) {
            setupList(new Integer(74), "Skin - Appearance", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("75"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("75"))) {
            setupList(new Integer(75), "Skin - Contour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("76"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("76"))) {
            setupList(new Integer(76), "Stoma - Moisture", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("77"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("77"))) {
            setupList(new Integer(77), "Stoma - Height", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("78"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("78"))) {
            setupList(new Integer(78), "Stoma - Shape", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("79"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("79"))) {
            setupList(new Integer(79), "Teaching and Learning Checklist", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("80"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("80"))) {
            setupList(new Integer(80), "Permanent Ostomy Profile", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("81"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("81"))) {
            setupList(new Integer(81), "Device Present", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("82"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("82"))) {
            setupList(new Integer(82), "Training Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("83"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("83"))) {
            setupList(new Integer(83), "Irrigiation", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("84"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("84"))) {
            setupList(new Integer(84), "Stoma Construction", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("85"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("85"))) {
            setupList(new Integer(85), "Ostomy Etiology", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("86"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("86"))) {
            setupList(new Integer(86), "Goal of Wound", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("87"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("87"))) {
            setupList(new Integer(87), "Goal of Ostomy", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("167"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("167"))) {
            setupList(new Integer(167), "Goal of Incision", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("168"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("168"))) {
            setupList(new Integer(168), "Goal of Tube/Drain", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("88"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("88"))) {
            setupList(new Integer(88), "Ostomy Measurement", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("89"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("89"))) {
            setupList(new Integer(89), "Patient Limitations", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("90"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("90"))) {
            setupList(new Integer(90), "Incision: Incision Exudate", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("91"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("91"))) {
            setupList(new Integer(91), "Incision: S&S Infections", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("92"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("92"))) {
            setupList(new Integer(92), "Incision: Incision Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("93"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("93"))) {
            setupList(new Integer(93), "Drain: Type of Drain", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("94"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("94"))) {
            setupList(new Integer(94), "Drain: Peri Drain Skin", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("95"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("95"))) {
            setupList(new Integer(95), "Drain: Drainage", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("96"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("96"))) {
            setupList(new Integer(96), "Drain: Drainage Odour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("97"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("97"))) {
            setupList(new Integer(97), "Pain", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("98"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("98"))) {
            setupList(new Integer(98), "Reading Variable", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("99"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("99"))) {
            setupList(new Integer(99), "Incision Closure Type", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("100"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("100"))) {
            setupList(new Integer(100), "Incision Closure Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("101"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("101"))) {
            setupList(new Integer(101), "Incision Exudate Amount", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("102"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("102"))) {
            setupList(new Integer(102), "Drain Site", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("103"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("103"))) {
            setupList(new Integer(103), "Laser", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("104"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("104"))) {
            setupList(new Integer(104), "Braden Sensory", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("105"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("105"))) {
            setupList(new Integer(105), "Braden Moisture", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("106"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("106"))) {
            setupList(new Integer(106), "Braden Activity", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("107"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("107"))) {
            setupList(new Integer(107), "Braden Nutrition", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("108"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("108"))) {
            setupList(new Integer(108), "Braden Friction", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("109"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("109"))) {
            setupList(new Integer(109), "Braden Mobility", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("110"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("110"))) {
            setupList(new Integer(110), "Missing Limbs", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("111"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("111"))) {
            setupList(new Integer(111), "Pain Assessment", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("112"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("112"))) {
            setupList(new Integer(112), "Skin Assessment", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("113"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("113"))) {
            setupList(new Integer(113), "Sensory", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("114"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("114"))) {
            setupList(new Integer(114), "Proprioception", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("115"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("115"))) {
            setupList(new Integer(115), "Deformities", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("116"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("116"))) {
            setupList(new Integer(116), "Skin", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("117"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("117"))) {
            setupList(new Integer(117), "Toes", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("118"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("118"))) {
            setupList(new Integer(118), "Weight Bearing Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("119"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("119"))) {
            setupList(new Integer(119), "Balance", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("120"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("120"))) {
            setupList(new Integer(120), "Calf Muscle Function", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("121"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("121"))) {
            setupList(new Integer(121), "Mobility Aids", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("122"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("122"))) {
            setupList(new Integer(122), "Muscle Tone", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("123"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("123"))) {
            setupList(new Integer(123), "Arches", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("124"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("124"))) {
            setupList(new Integer(124), "Foot Active", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("125"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("125"))) {
            setupList(new Integer(125), "Foot Passive", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("126"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("126"))) {
            setupList(new Integer(126), "Sensation", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("127"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("127"))) {
            setupList(new Integer(127), "Lab Done", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("128"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("128"))) {
            setupList(new Integer(128), "Transcutaneous Pressures", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("129"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("129"))) {
            setupList(new Integer(129), "Transcutaneous Pressures", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("130"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("130"))) {
            setupList(new Integer(130), "Transcutaneous Pressures", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("131"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("131"))) {
            setupList(new Integer(131), "Transcutaneous Pressures", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("132"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("132"))) {
            setupList(new Integer(132), "Transcutaneous Pressures", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("134"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("134"))) {
            setupList(new Integer(134), "PostOp Etiology", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("135"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("135"))) {
            setupList(new Integer(135), "Drains Etiology", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("136"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("136"))) {
            setupList(new Integer(136), "Construction", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("137"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("137"))) {
            setupList(new Integer(137), "Ostomy Drainage", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("138"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("138"))) {
            setupList(new Integer(138), "Ostomy: Mucocutaneous Margin", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("139"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("139"))) {
            setupList(new Integer(139), "Ostomy: Peri-fistula Skin", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("140"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("140"))) {
            setupList(new Integer(140), "Tubes&Drains: Exudate", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("141"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("141"))) {
            setupList(new Integer(141), "Tubes&Drains: Exudate Amount", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("143"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("143"))) {
            setupList(new Integer(143), "Tubes&Drains: Drainage Amount", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("144"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("144"))) {
            setupList(new Integer(144), "Tubes&Drains: Drain Removed Reason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("146"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("146"))) {
            setupList(new Integer(146), "Wound Profile: Teaching Goals", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("147"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("147"))) {
            setupList(new Integer(147), "Ostomy Assessment: Nutritional Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("148"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("148"))) {
            setupList(new Integer(148), "Wound Profile: Type of Ostomy", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("149"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("149"))) {
            setupList(new Integer(149), "Ostomy Assessment: Discharge Reason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("150"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("150"))) {
            setupList(new Integer(150), "Incision Assessment: Discharge Reason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("151"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("151"))) {
            setupList(new Integer(151), "Incision Assessment: Incision Closure Reason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("152"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("152"))) {
            setupList(new Integer(152), "Incision Assessment: PostOp Management", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("156"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("156"))) {
            setupList(new Integer(156), "Burn Assessment: Wound Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("157"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("157"))) {
            setupList(new Integer(157), "Burn Assessment: Burn Wound", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("158"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("158"))) {
            setupList(new Integer(158), "Burn Assessment: Exudate Colour", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("159"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("159"))) {
            setupList(new Integer(159), "Burn Assessment: Exudate Amount", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("160"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("160"))) {
            setupList(new Integer(160), "Burn Assessment: Grafts", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("161"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("161"))) {
            setupList(new Integer(161), "Burn Assessment: Donors", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("162"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("162"))) {
            setupList(new Integer(162), "Burn Assessment: Discharge Reason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("173"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("173"))) {
            setupList(new Integer(173), "Burn Etiology", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("164"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("164"))) {
            setupList(new Integer(164), "Professionals", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("165"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("165"))) {
            setupList(new Integer(165), "Interfering Medications", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("169"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("169"))) {
            setupList(new Integer(169), "Inactivate Reason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("170"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("170"))) {
            setupList(new Integer(170), "TimeZone  (tz formats: http://en.wikipedia.org/wiki/List_of_tz_database_time_zones)", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");

        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("175"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("175"))) {
            setupList(new Integer(175), "Wound Acquired", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }  else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("180"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("180"))) {
            setupList(new Integer(180), "Skin Etiology", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        } else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("181"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("181"))) {
            setupList(new Integer(181), "Skin Status", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("182"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("182"))) {
            setupList(new Integer(182), "Skin  Discharge REason", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("183"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("183"))) {
            setupList(new Integer(183), "Skin Goals", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("190"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("190"))) {
            setupList(new Integer(190), "Range of Motion", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("189"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("189"))) {
            setupList(new Integer(189), "Upper Limb Edema Location", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("188"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("188"))) {
            setupList(new Integer(188), "Missing Upper Limbs", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("187"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("187"))) {
            setupList(new Integer(187), "Goal of Therapy", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("186"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("186"))) {
            setupList(new Integer(186), "Reason for Ending", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("185"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("185"))) {
            setupList(new Integer(185), "Intiated By", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("184"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("184"))) {
            setupList(new Integer(184), "Negative Pressure Connector", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("193"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("193"))) {
            setupList(new Integer(193), "IV Therapy Barriers", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("192"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("192"))) {
            setupList(new Integer(192), "Clinical Barriers", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("200"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("200"))) {
            setupList(new Integer(200), "Advanced Limb Shape", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("195"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("195"))) {
            setupList(new Integer(195), "External Referral", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("196"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("196"))) {
            setupList(new Integer(196), "Arranged", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("199"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("199"))) {
            setupList(new Integer(199), "Advanced Limb Temperature", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("198"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("198"))) {
            setupList(new Integer(198), "Advanced Pain Assessment", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("201"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("201"))) {
            setupList(new Integer(201), "Vendor Name", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("202"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("202"))) {
            setupList(new Integer(202), "Machine Acquirement", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }else if ((request.getParameter("resourceMenu") != null && request.getParameter("resourceMenu").equals("166"))
                || (session.getAttribute("resource_id") != null && session.getAttribute("resource_id").equals("166"))) {
            setupList(new Integer(166), "Devices", request);
            session.removeAttribute("resource_id");
            session.removeAttribute("resourceMenu");
        }

        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.resources.success"));
        }
    }
    public void setupCategories(HttpServletRequest request) {
        try {
            ListsDAO resourcesDAO = new ListsDAO();
            LookupVO resourcesVO = new LookupVO();
            resourcesVO.setResourceId(resourcesVO.TREATMENT_CATEGORY_INT);
            Collection items = (Collection) resourcesDAO.findAllByCriteria(resourcesVO);
            request.setAttribute("resourceCategories", items);
        } catch (Exception e) {
            log.error("An application exception has been raised in ResourcesAdminSetupAction.perform(): " + e.toString());
        }
    }
    public void setupList(Integer resource_id, String resource_name, HttpServletRequest request) {
        if (request.getParameter("resource_id") == null && request.getParameter("resourceMenu") != null) {
            request.setAttribute("resource_id", request.getParameter("resourceMenu"));
            ResourcesServiceImpl rservice = new ResourcesServiceImpl();
            try {
                ResourcesVO resource = rservice.getResource(Integer.parseInt((String) request.getParameter("resourceMenu")));
                request.setAttribute("resource", resource);
            } catch (ApplicationException e) {
            }
        }
        //else {
        //request.setAttribute("resource_id", request.getParameter("resourceMenu"));
        //}*/
        try {
            request.setAttribute("resource_section", resource_name);
            ListsDAO resourcesDAO = new ListsDAO();
            LookupVO resourcesVO = new LookupVO();
            resourcesVO.setResourceId(resource_id);
            Collection items = (Collection) resourcesDAO.findAllByCriteria(resourcesVO, false);
            request.setAttribute("resourceList", items);
        } catch (Exception e) {
            log.error("An application exception has been raised in ProfessionalAdminSetupAction.perform(): " + e.toString());
        }
    }
}
