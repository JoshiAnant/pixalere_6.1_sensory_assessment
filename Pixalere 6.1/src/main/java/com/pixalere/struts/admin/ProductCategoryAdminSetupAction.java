package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.*;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
public class ProductCategoryAdminSetupAction extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductCategoryAdminSetupAction.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm     form,
            HttpServletRequest request,
            HttpServletResponse response){
        request.setAttribute("page","admin");
        HttpSession session = request.getSession();
        ProfessionalVO userVO = (ProfessionalVO)session.getAttribute("userVO");
        if(request.getParameter("list_action")!=null&&request.getParameter("list_action").equals("edit")){
            try{
                ProductCategoryServiceImpl bd=new ProductCategoryServiceImpl();
                ProductCategoryVO vo =(ProductCategoryVO)bd.getProductCategory(Integer.parseInt(request.getParameter("choice")));
                request.setAttribute("choice",vo.getId()+"");
                request.setAttribute("category",vo.getTitle());
                request.setAttribute("action","edit");
            } catch(ApplicationException e){
                log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                
            }
        } else if(request.getParameter("list_action")!=null&&request.getParameter("list_action").equals("delete")){
            try{
                ProductCategoryServiceImpl manager=new ProductCategoryServiceImpl();
                ProductCategoryVO accountVO=(ProductCategoryVO)manager.getProductCategory(Integer.parseInt(request.getParameter("choice")));
                manager.removeProductCategory(accountVO);
            } catch(ApplicationException e){
                log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                
            }
        } else{
            request.setAttribute("action","add");
        }
        try{
            ProductCategoryServiceImpl catBD=new ProductCategoryServiceImpl();
            ProductCategoryVO vo = new ProductCategoryVO();vo.setActive(1);
            Collection colCats=(Collection)catBD.getAllProductCategoriesByCriteria(vo);
            Vector vecCatsWithItemsCount=new Vector();
            Iterator iter1 = colCats.iterator();
            while (iter1.hasNext()) {
                ProductCategoryVO prodCat = (ProductCategoryVO) iter1.next();
		        ProductsServiceImpl productsBD = new ProductsServiceImpl();
                Collection allProdsInCat = (Collection)productsBD.getAllProducts(Integer.toString(prodCat.getId()),userVO);
    	        if(allProdsInCat==null)
    	        	{
    	        	prodCat.setItemsCount(new Integer(0));  
    	       		}
    	        else
    	        	{
    	        	prodCat.setItemsCount(allProdsInCat.size());  
    	       		}
                vecCatsWithItemsCount.add(prodCat);
                }
            request.setAttribute("productCategories",vecCatsWithItemsCount);
            
        }catch(ApplicationException e){
            log.error("Exception has occurred in ProductsCategoryAdminSetupAction.perform(): "+ e.toString());
    	    System.out.println("Error ProductsCategoryAdminSetupAction: "+e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")){
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("viewer")){
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else{
            return (mapping.findForward("admin.productcats.success"));
        }
    }
    
}
