/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.common.service.TriggerServiceImpl;
import com.pixalere.common.bean.TriggerVO;
import com.pixalere.common.bean.TriggerCriteriaVO;
import java.util.Collection;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.guibeans.TriggerCondition;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.util.ArrayList;
import java.util.Iterator;
/**
 *
 * @author travis
 */
public class TriggerConditionsAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        TriggerForm triggerForm = (TriggerForm)form;
        
        TriggerServiceImpl tservice = new TriggerServiceImpl();
        GUIServiceImpl gservice = new GUIServiceImpl();
        
        try {
            String action = request.getParameter("action");
            
            TriggerVO trigger = tservice.getTrigger(new Integer(request.getParameter("trigger_id")));
            request.setAttribute("trigger_id", request.getParameter("trigger_id"));
            
            if (action != null){
                if(action.equals("save_condition")){
                    TriggerCriteriaVO t = new TriggerCriteriaVO();
                    t.setTrigger_id(triggerForm.getTrigger_id());
                    t.setCriteria_type(triggerForm.getCriteria_type());
                    t.setMatch_type(triggerForm.getComparator());
                    t.setComponent_id(triggerForm.getComponent_id());

                    if(triggerForm.getCriteria_type().equals("lookup")){
                        t.setCriteria(triggerForm.getLookup_id());
                    } else {
                        // Validate type of value depending on component type

                        t.setCriteria(triggerForm.getValue().trim());
                    }
                    t.setDescription(triggerForm.getDescription());
                    tservice.saveTriggerOption(t);

                } else if(action.equals("del_condition")){ 
                    TriggerCriteriaVO condition = new TriggerCriteriaVO();
                    condition.setTrigger_criteria_id(triggerForm.getTrigger_criteria_id());
                    tservice.deleteTriggerCriteria(condition);

                } else if (action.equals("update_operator")){
                    String operator = request.getParameter("logic_operator");
                    trigger.setLogic_operator(operator);
                    tservice.saveTrigger(trigger);
                }
            }
            request.setAttribute("trigger",trigger);
                
            ListServiceImpl lservice = new ListServiceImpl();

            Collection<TriggerCriteriaVO> criterias =  tservice.getAllOptions(new Integer(request.getParameter("trigger_id")));
            Collection<TriggerCondition> conditions = new ArrayList<TriggerCondition>();

            for (TriggerCriteriaVO triggerCriteriaVO : criterias) {
                TriggerCondition condition = new TriggerCondition();
                // Copy values
                condition.setTrigger_criteria_id(triggerCriteriaVO.getTrigger_criteria_id());
                condition.setTrigger_id(triggerCriteriaVO.getTrigger_id());
                condition.setCriteria_type(triggerCriteriaVO.getCriteria_type());
                condition.setComponent_id(triggerCriteriaVO.getComponent_id());
                condition.setCriteria(triggerCriteriaVO.getCriteria());
                condition.setMatch_type(triggerCriteriaVO.getMatch_type());

                ComponentsVO componentsVO = gservice.getComponent(triggerCriteriaVO.getComponent_id());

                // Component title (does not applies to healrate)
                if (!triggerCriteriaVO.getCriteria_type().equals("healrate")){
                    String title = Common.getLocalizedString(componentsVO.getLabel_key(), locale);
                    condition.setComponent_title(title);
                }

                // Criteria title (applies only to lookup)
                if (triggerCriteriaVO.getCriteria_type().equals("lookup")){
                    if (componentsVO.getField_name().equals("co_morbidities") || (componentsVO.getResource_id() != null && componentsVO.getResource_id() > 0)) {
                        int lookup_id = Integer.parseInt(triggerCriteriaVO.getCriteria());
                        LookupVO arr = lservice.getListItem(lookup_id);
                        String criteria_title = arr.getName(language);
                        condition.setCriteria_title(criteria_title);
                    } else {
                        if (componentsVO.getComponent_type().equals("YN")){
                            String criteria_title = (triggerCriteriaVO.getCriteria().equals("2"))
                                    ? Common.getLocalizedString("pixalere.yes", locale)
                                    : Common.getLocalizedString("pixalere.no", locale);
                            condition.setCriteria_title(criteria_title);
                        }
                        if (componentsVO.getComponent_type().equals("AT")){
                            String criteria_title = "";
                            if (triggerCriteriaVO.getCriteria().equals("1"))
                                criteria_title = Common.getLocalizedString("pixalere.woundassessment.form.full_assessment", locale);
                            if (triggerCriteriaVO.getCriteria().equals("0"))
                                criteria_title = Common.getLocalizedString("pixalere.woundassessment.form.partial_assessment", locale);
                            if (triggerCriteriaVO.getCriteria().equals("2"))
                                criteria_title = Common.getLocalizedString("pixalere.not_assessed_today", locale);
                            if (triggerCriteriaVO.getCriteria().equals("3"))
                                criteria_title = Common.getLocalizedString("pixalere.woundassessment.form.phone_visit", locale);
                            condition.setCriteria_title(criteria_title);
                        }
                    }

                }

                conditions.add(condition);
            }

            request.setAttribute("options",conditions);
            ComponentsVO comptmp = new ComponentsVO();
            
            // Patient Profile Components
            ComponentsVO comorbidities = gservice.getComponent("co_morbidities", Constants.PATIENT_PROFILE);
            request.setAttribute("comorbiditiesComp",comorbidities);
            // FIXME: (At the DB) this field name loads interfering factors NOT interfering meds
            ComponentsVO factorsinter = gservice.getComponent("interfering_meds", Constants.PATIENT_PROFILE);
            request.setAttribute("factorsinterComp",factorsinter);
            
            // Wound Profile components: Goals and Etiology
            if(trigger!=null && trigger.getWound_type().equals("S") ){
                ComponentsVO et = gservice.getComponent("etiology", Constants.SKIN_PROFILE);
                request.setAttribute("etiologyComp",et);
                ComponentsVO g = gservice.getComponent("goals", Constants.SKIN_PROFILE);
                request.setAttribute("goalsComp",g);
            } else {
                // FIXME: Add profile entries to components table for: Ostomy, T&D, Burns, Incisions
                // Right now we are using only wound profile (and skin above)
                ComponentsVO et = gservice.getComponent("etiology", Constants.WOUND_PROFILE);
                request.setAttribute("etiologyComp",et);
                ComponentsVO g = gservice.getComponent("goals", Constants.WOUND_PROFILE);
                request.setAttribute("goalsComp",g);
            }

            // Assessment components
            if(trigger!=null) {
                if(trigger.getWound_type().equals("A")){
                    comptmp.setFlowchart(Constants.WOUND_ASSESSMENT);
                }else if((trigger.getWound_type().equals("U") ||  trigger.getWound_type().equals("F"))){
                    comptmp.setFlowchart(Constants.OSTOMY_ASSESSMENT);
                }else if(trigger.getWound_type().equals("S") ){
                    comptmp.setFlowchart(Constants.SKIN_ASSESSMENT);
                }else if(trigger.getWound_type().equals("I") ){
                    comptmp.setFlowchart(Constants.INCISION_ASSESSMENT);
                }else if(trigger.getWound_type().equals("D") ){
                    comptmp.setFlowchart(Constants.DRAIN_ASSESSMENT);
                }
                Collection<ComponentsVO> components = gservice.getAllComponents(comptmp);
                // Filter out components that don't apply / pending to implement
                for (Iterator<ComponentsVO> it = components.iterator(); it.hasNext();) {
                    ComponentsVO componentsVO = it.next();
                    if (componentsVO.getField_type().equals("DATE")){
                        it.remove();
                    } else if (componentsVO.getComponent_type().equals("TXTA")         // Open text
                                || componentsVO.getComponent_type().equals("PDF")   // n/a
                                || componentsVO.getComponent_type().equals("PROD")  // Open text
                                || componentsVO.getComponent_type().equals("COMM")  // Open text
                                || componentsVO.getComponent_type().equals("MMAS")  // Multiple too open
                                || componentsVO.getComponent_type().equals("MTXT")  // Multiple too open
                                || componentsVO.getComponent_type().equals("IMG")   // Image name dont help
                                || componentsVO.getComponent_type().equals("ADM")   // n/a
                                || componentsVO.getComponent_type().equals("REF")   // where to get the options?
                                ){ 
                            it.remove();
                    } else if (componentsVO.getField_name().equals("review_done")   // All these: Data not available at Treatment Icon (when triggers apply)
                                || componentsVO.getField_name().equals("cs_done")
                                || componentsVO.getComponent_type().equals("MDDA")
                                || componentsVO.getField_name().equals("bandages_out")
                                || componentsVO.getField_name().equals("bandages_in")
                            ) {
                        it.remove();
                    }
                }

                request.setAttribute("components", components);
            }

        } catch (ApplicationException e) {
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        return (mapping.findForward("admin.trigger.success"));
    }
}
