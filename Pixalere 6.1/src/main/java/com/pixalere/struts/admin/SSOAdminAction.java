package com.pixalere.struts.admin;

import com.pixalere.common.ApplicationException;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.service.OpenSAMLService;
import com.pixalere.sso.service.SSOProviderService;
import com.pixalere.utils.Common;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Save main SSO Identity settings action.
 * 
 * @author Jose
 * @since 6.0
 */
public class SSOAdminAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOAdminAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        try{
            HttpSession session = request.getSession();
            Integer language = Common.getLanguageIdFromSession(session);
            String locale = Common.getLanguageLocale(language);
            SSOProviderService service = new SSOProviderService();
            SSOAdminForm idpform = (SSOAdminForm) form;
            
            // Get provider to be updated
            SSOProviderVO updatedProvider = service.getProviderById(idpform.getIdProvider());
            
            if (updatedProvider != null) {
                
                updatedProvider = idpform.getUpdatedProvider(updatedProvider);
                // Check configuration
                List<String> errors = OpenSAMLService.validateSSOConfig(updatedProvider, locale);
                
                // Are we enabling this provider? If so, check if valid
                if (updatedProvider.getActive() == 1 && errors.isEmpty()){
                    // Enabling this one, disable all the other providers!
                    List<SSOProviderVO> all = service.getAllProviders();
                    for (SSOProviderVO ssopvo : all) {
                        if (!ssopvo.getId().equals(updatedProvider.getId()) && ssopvo.getActive() == 1){
                            ssopvo.setActive(0);
                            service.saveProvider(ssopvo);
                        }
                    }
                    
                } else {   
                    // Disable, not valid configuration
                    updatedProvider.setActive(0);
                }
                
                service.saveProvider(updatedProvider);
                
                // Store in session current Provider (Redirecting to SSOAdminSetup)
                session.setAttribute("providerId", String.valueOf(updatedProvider.getId()));
                
                // Store in session missing/errors in conf
                session.setAttribute("confErrors", errors);
            } else {
                System.out.println("ERROR: SSOAdminAction - SSOProvider to update not found id: " + idpform.getIdProvider());
            }
        } catch(ApplicationException e){
            log.error("An application exception has been raised in SSOAdminAction.execute(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        // Return to SSOAdminSetup - Edit IDP config page / Main page if not found
        return (mapping.findForward("admin.sso.success"));
    }
}
