/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import java.util.Vector;
import com.pixalere.utils.excel.ExcelReader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.Collection;
import org.apache.struts.upload.FormFile;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.PDate;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.xml.ProductHandler;
import com.pixalere.utils.xml.XMLReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.common.service.ProductsServiceImpl;
import java.util.Vector;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.common.service.TriggerServiceImpl;
import com.pixalere.common.bean.TriggerProductsVO;
/**
 * Import Products save class.  This class validates, and imports the XML
 *
 * @since 5.0
 * @author travis
 */
public class ImportProducts extends Action {
    static private Logger logs = Logger.getLogger(ImportProducts.class);
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ImportProductsForm productsForm = (ImportProductsForm) form;
        FormFile products = productsForm.getProducts();
        ProductsServiceImpl pservice = new ProductsServiceImpl();
        ProductCategoryServiceImpl pcservice = new ProductCategoryServiceImpl();
        TriggerServiceImpl tservice = new TriggerServiceImpl();
        if (products != null) {
            if (productsForm.getImport_type() == 0) {
                XMLReader xmlReader = new XMLReader();
                try {
                    ProductHandler handler = new ProductHandler();
                    xmlReader.readXML(products.getInputStream(), handler);
                    Vector<ProductsVO> productsVector = handler.getProducts();
                    for (ProductsVO product : productsVector) {
                        ProductsVO pCrit = new ProductsVO();
                        pCrit.setUid(product.getUid());
                        pCrit.setActive(1);
                        try {
                            ProductsVO old_product = pservice.getProduct(pCrit);
                            if (old_product == null) {
                                //check if product category exists.
                                ProductCategoryVO pc = new ProductCategoryVO();
                                pc.setActive(1);
                                String cat = productsForm.getCategory_name();
                                if (cat != null && !cat.equals("")) {
                                    pc.setTitle(cat);
                                } else {
                                    pc.setTitle(product.getCategory().getTitle());
                                }
                                ProductCategoryVO result = pcservice.getProductCategory(pc);
                                if (result != null) {
                                    product.setCategory_id(result.getId());
                                } else {
                                    //no category found, so we must create it.
                                    ProductCategoryVO category = new ProductCategoryVO();
                                    category.setActive(1);
                                    category.setTitle(product.getCategory().getTitle());
                                    pcservice.saveProductCategory(category);
                                    ProductCategoryVO new_cat = pcservice.getProductCategory(category);
                                    product.setCategory_id(new_cat.getId());
                                }
                                product.setAll_treatment_locations(1);
                                pservice.saveProduct(product);
                            } else {
                                old_product.setActive(new Integer(0));
                                
                                
                                pservice.saveProduct(old_product);
                                ConfigurationServiceImpl cmanager = new ConfigurationServiceImpl();
                                TableUpdatesVO t = cmanager.getTableUpdates();
                                t.setProducts(new Integer(new PDate().getEpochTime() + ""));
                                cmanager.saveTableUpdates(t);
                                if (old_product.getBaseId() != null) {
                                    product.setBaseId(old_product.getBaseId());
                                } else {
                                    product.setBaseId(old_product.getId());
                                }
                                //check if dressing category exists.
                                ProductCategoryVO pc = new ProductCategoryVO();
                                pc.setActive(1);
                                pc.setTitle(product.getCategory().getTitle());
                                ProductCategoryVO result = pcservice.getProductCategory(pc);
                                if (result != null) {
                                    product.setCategory_id(result.getId());
                                } else {
                                    //no category found, so we must create it.
                                    ProductCategoryVO category = new ProductCategoryVO();
                                    category.setActive(1);
                                    category.setTitle(product.getCategory().getTitle());
                                    pcservice.saveProductCategory(category);
                                    ProductCategoryVO new_cat = pcservice.getProductCategory(category);
                                    product.setCategory_id(new_cat.getId());
                                }
                                product.setAll_treatment_locations(1);
                                
                                pservice.saveProduct(product);
                                ProductsVO newTrig = pservice.getProduct(product);
                                //Check if any triggers exist that use this product
                                if(newTrig !=null){
                                    Collection<TriggerProductsVO> trigs = tservice.getTriggerProducts(old_product.getId());
                                    for(TriggerProductsVO tprod : trigs){
                                        tprod.setProduct_id(newTrig.getId());
                                        tservice.saveTriggerProducts(tprod);
                                    }
                                }
                            }
                        } catch (ApplicationException e) {
                            handler.setError_count(handler.getError_count() + 1);
                            handler.setError_log(handler.getError_log() + "\n\n" + e.getMessage());
                            logs.error("Error thrown saving product: " + e.getMessage());
                        }
                    }
                    request.setAttribute("import_count", handler.getImport_count());
                    request.setAttribute("error_count", handler.getError_count());
                    request.setAttribute("error_log", handler.getError_log());
                } catch (FileNotFoundException e) {
                    logs.error("Unable to find Product file " + e.getMessage());
                } catch (IOException e) {
                    logs.error("Unable to read Product file " + e.getMessage());
                }
            } else if (productsForm.getImport_type() == 1) {
                try {
                    ExcelReader r = new ExcelReader();
                    Vector<ProductsVO> prodVect = r.readProducts(products.getInputStream());
                    
                    for (ProductsVO product : prodVect) {
                        //check if product category exists.
                        ProductCategoryVO pc = new ProductCategoryVO();
                        pc.setActive(1);
                        String cat = productsForm.getCategory_name();
                        if (product.getCategory() == null || product.getCategory().getTitle() == null || product.getCategory().getTitle().equals("")) {
                                pc.setTitle(cat);
                            } else {
                                pc.setTitle(product.getCategory().getTitle());
                            }
                        
                        ProductCategoryVO result = pcservice.getProductCategory(pc);
                        if (result != null) {
                            product.setCategory_id(result.getId());
                        } else {
                            //no category found, so we must create it.
                            ProductCategoryVO category = new ProductCategoryVO();
                            category.setActive(1);
                            category.setTitle(pc.getTitle());
                            if (!category.getTitle().equals("")) {
                                pcservice.saveProductCategory(category);
                            }
                            ProductCategoryVO new_cat = pcservice.getProductCategory(category);
                            if(new_cat!=null){
                                product.setCategory_id(new_cat.getId());
                            }
                        }
                        product.setAll_treatment_locations(1);
                        if (product.getUid() != null && !product.getUid().equals("")) {
                            ProductsVO old_prod = new ProductsVO();
                            old_prod.setUid(product.getUid());
                            old_prod.setActive(1);
                            ProductsVO old = pservice.getProduct(old_prod);
                            if (old != null) {
                                old.setActive(0);
                                pservice.saveProduct(old);
                                if(old.getBaseId()!=null){
                                    product.setBaseId(old.getBaseId());
                                }else{
                                    product.setBaseId(old.getId());
                                }
                            }
                            product.setActive(1);

                        }
                        pservice.saveProduct(product);
                    }
                } catch (FileNotFoundException e) {
                    logs.error("Unable to find Product file " + e.getMessage());
                } catch (IOException e) {
                    logs.error("Unable to read Product file " + e.getMessage());
                } catch (ApplicationException e) {
                    logs.error("Unable to save Product file " + e.getMessage());
                }
            }
        } else {
        }
        return mapping.findForward("admin.importproducts.success");
    }
}
