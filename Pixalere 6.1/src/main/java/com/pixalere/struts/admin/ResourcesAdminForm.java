package com.pixalere.struts.admin;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.utils.Common;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.dao.ListsDAO;
import java.util.Collection;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.admin.bean.ResourcesVO;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;
/**
 * @author
 *
 */
public class ResourcesAdminForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ResourcesAdminForm.class);
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        HttpSession session = request.getSession();
        ActionErrors errors = new ActionErrors();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        PatientServiceImpl patientbd = new PatientServiceImpl(language);
        String action = (String) request.getParameter("list_action");
        String item_id = request.getParameter("choice");
        ListsDAO bd = new ListsDAO();
        ListServiceImpl manager = new ListServiceImpl(language);
        
        try {
            if (request.getParameter("list_action") != null && request.getParameter("list_action").equals("delete")) {
                LookupVO lookupVO = new LookupVO();
                lookupVO.setId(new Integer(item_id));
                lookupVO = manager.getListItem(new Integer(item_id).intValue());
                boolean isTreatment = false;
                boolean isTreatmentCat = false;
                if (lookupVO.getResource().getResourceId().intValue() == LookupVO.TREATMENT_LOCATION[0]) {
                        isTreatment = true;
                }
                else if (lookupVO.getResource().getResourceId().intValue() == LookupVO.TREATMENT_CATEGORY[0]) {
                        isTreatmentCat = true;
                }
                
                if (isTreatment == true) {
                    PatientAccountVO p = new PatientAccountVO();
                    p.setTreatment_location_id(new Integer(item_id));
                    p.setCurrent_flag(1);
                    PatientAccountVO p1 = patientbd.getPatient(p);
                    if (p1 != null) {
                        Object[] patientinfo={lookupVO.getName(language)};
                        errors.add("delete", new ActionMessage("pixalere.admin.resources.error.patientexists", patientinfo));
                    }
                }else  if(isTreatmentCat == true){
                    //find out if active treatments exist.
                    LookupVO f = new LookupVO();
                    f.setCategoryId(lookupVO.getId());
                    LookupVO check = manager.getListItem(f);
                    if(check!=null){
                        
                        
                        Object[] treatmentinfo ={lookupVO.getName(language),Common.getConfig("treatmentLocationName")};
                        errors.add("delete", new ActionMessage("pixalere.admin.resources.error.treatmentexists", treatmentinfo));
                    }
                }
            }
            /*if(request.getParameter("action") != null && (((String)request.getParameter("action")).equals("edit") || ((String)request.getParameter("action")).equals("add"))){
                Collection<LookupVO> items = manager.getLists(new Integer(request.getParameter("resource_id")));
                boolean found = false;
                for(LookupVO item : items){
                    for(item.getN)
                    if (item != null && !item.getId().equals(new Integer((String) request.getParameter("id"))) && found == true) {
                        errors.add("Duplicate: ", new ActionError("pixalere.admin.resources.error"));
                        setupList(new Integer((String) request.getParameter("resource_id")), request);
                    }
                }
            }*/
        } catch (Exception ex) {
            ex.printStackTrace();
            errors.add("exception", new ActionError("pixalere.admin.resources.error"));
        }
        return errors;
    }
    public void setupList(Integer resource_id, HttpServletRequest request) {
        if (request.getParameter("resource_id") == null && request.getParameter("resourceMenu") != null) {
            request.setAttribute("resource_id", request.getParameter("resourceMenu"));
        }
        //else {
        //request.setAttribute("resource_id", request.getParameter("resourceMenu"));
        //}*/
        try {
            com.pixalere.admin.service.ResourcesServiceImpl bd = new com.pixalere.admin.service.ResourcesServiceImpl();
            ResourcesVO vo = (ResourcesVO) bd.getResource(resource_id.intValue());
            request.setAttribute("resource_section", vo.getName());
            com.pixalere.common.dao.ListsDAO resourcesDAO = new com.pixalere.common.dao.ListsDAO();
            com.pixalere.common.bean.LookupVO resourcesVO = new com.pixalere.common.bean.LookupVO();
            resourcesVO.setResourceId(resource_id);
            Collection items = (Collection) resourcesDAO.findAllByCriteria(resourcesVO);
            request.setAttribute("resourceList", items);
        } catch (Exception e) {
            log.error("An application exception has been raised in ProfessionalAdminSetupAction.perform(): " + e.toString());
        }
    }
    public void reset(ActionMapping mapping,
            HttpServletRequest request) {
        setId(0);
        setName("");
        setValue("");
        setReport_value("");
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setIsOther(int isOther) {
        this.isOther = isOther;
    }
    public int getIsOther() {
        return isOther;
    }
    public int getCategoryId(){
        return categoryId;
    }
    public void setCategoryId(int categoryId){
        this.categoryId=categoryId;
    }
    public int getOrderby() {
        return orderby;
    }
    public void setOrderby(int orderby) {
        this.orderby = orderby;
    }
    public int getTitle() {
        return title;
    }
    public void setTitle(int title) {
        this.title = title;
    }
   
    public int getSoloBox() {
        return solobox;
    }
    public void setSoloBox(int solobox) {
        this.solobox = solobox;
    }
    public void setReport_value(String report_value) {
        this.report_value = report_value;
    }
    public String getReport_value() {
        return report_value;
    }
    private String definition;
    private int clinic;
    private int item_filter;
    private int id;
    private String name;
    private String value;
    private int isOther;
    private int title;
    private int solobox;
    private String cpt;
   private String icd9;
   private String icd10;
    private int categoryId;
    private int orderby;
    private String report_value;
    private String ip_address;
    public int getItem_filter() {
        return item_filter;
    }
    public void setItem_filter(int item_filter) {
        this.item_filter = item_filter;
    }
        /**
     * @return the cpt
     */
    public String getCpt() {
        return cpt;
    }
    /**
     * @param cpt the cpt to set
     */
    public void setCpt(String cpt) {
        this.cpt = cpt;
    }
    /**
     * @return the icd9
     */
    public String getIcd9() {
        return icd9;
    }
    /**
     * @param icd9 the icd9 to set
     */
    public void setIcd9(String icd9) {
        this.icd9 = icd9;
    }
    /**
     * @return the icd10
     */
    public String getIcd10() {
        return icd10;
    }
    /**
     * @param icd10 the icd10 to set
     */
    public void setIcd10(String icd10) {
        this.icd10 = icd10;
    }

    /**
     * @return the clinic
     */
    public int getClinic() {
        return clinic;
    }
    /**
     * @param clinic the clinic to set
     */
    public void setClinic(int clinic) {
        this.clinic = clinic;
    }

    /**
     * @return the ip_address
     */
    public String getIp_address() {
        return ip_address;
    }

    /**
     * @param ip_address the ip_address to set
     */
    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    /**
     * @return the definition
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * @param definition the definition to set
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    
}

