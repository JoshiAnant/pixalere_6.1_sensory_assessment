package com.pixalere.struts.admin;

import com.pixalere.sso.bean.SSOPositionVO;
import com.pixalere.sso.bean.SSOProviderVO;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 * @since 6.0
 */
public class SSOUsersForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOAdminForm.class);
    
    private int idProvider;
    private String timezone;
    private String attributePixauth;
    private String attributeValPixauth;
    private String attributeUsername;
    private String attributeEmail;
    private String attributeFirstname;
    private String attributeLastname;
    private String attributePosition;
    private String useAttributeTraining;
    private String useAttributeReporting;
    private String attributeNameTraining;
    private String attributeNameReporting;
    private String attributeValueTraining;
    private String attributeValueReporting;
    private String mainField;
    
    private String selfConfRoles;
    private String selfConfLocations;
    private String selfConfPositions;
    
    private String onlyMainField;
    private String allowSSOUpdate;
    
    private Integer[] positionIds;
    private String[] positionsValues;
    private String[] positionsTitles;

    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        positionIds = new Integer[0];
        positionsValues = new String[0];
        positionsTitles = new String[0];
        super.reset(mapping, request); 
    }
    
    /**
     * Convenience method to build and return the SSOPositions from 
     * the indexed properties.
     * 
     * @param providerId
     * @return 
     */
    public List<SSOPositionVO> getPositions(int providerId){
        List<SSOPositionVO> list = new ArrayList<SSOPositionVO>();
        
        for (int i = 0; i < positionIds.length; i++) {
            SSOPositionVO pos = new SSOPositionVO();
            pos.setPositionId(positionIds[i]);
            pos.setTitle(positionsTitles[i]);
            pos.setAttributeValue(positionsValues[i]);
            pos.setSsoProviderid(providerId);
            list.add(pos);
        }
        
        return list;
    }
    
    /**
     * Updates only the relevant fields of the SSOProvider for
     * the Users Configurations Admin page.
     * 
     * @param vo
     * @return 
     */
    public SSOProviderVO getUpdatedProvider(SSOProviderVO vo){
        int i_useAttributeTraining = (useAttributeTraining == null)? 0: useAttributeTraining.equals("0")? 0: 1;
        int i_useAttributeReporting = (useAttributeReporting == null)? 0: useAttributeReporting.equals("0")? 0: 1;
        int i_selfConfRoles = (selfConfRoles == null)? 0 : selfConfRoles.equals("0")? 0: 1;
        int i_selfConfLocations = (selfConfLocations == null)? 0 : selfConfLocations.equals("0")? 0: 1;
        int i_selfConfPositions = (selfConfPositions == null)? 0 : selfConfPositions.equals("0")? 0: 1;
        int i_onlyMainField = (onlyMainField == null)? 0: 1;
        int i_allowSSOUpdate = (allowSSOUpdate == null)? 0: 1;
        
        vo.setUseAttributeTraining(i_useAttributeTraining);
        vo.setUseAttributeReporting(i_useAttributeReporting);
        vo.setSelfConfRoles(i_selfConfRoles);
        vo.setSelfConfLocations(i_selfConfLocations);
        vo.setSelfConfPositions(i_selfConfPositions);
        vo.setOnlyMainField(i_onlyMainField);
        vo.setAllowSSOUpdate(i_allowSSOUpdate);

        vo.setTimezone(timezone);
        vo.setAttributePixauth((attributePixauth == null? attributePixauth : attributePixauth.trim()));
        vo.setAttributeValPixauth((attributeValPixauth == null? attributeValPixauth : attributeValPixauth.trim()));
        vo.setAttributeUsername((attributeUsername == null? attributeUsername : attributeUsername.trim()));
        vo.setAttributeEmail((attributeEmail == null? attributeEmail : attributeEmail.trim()));
        vo.setAttributeFirstname((attributeFirstname == null? attributeFirstname : attributeFirstname.trim()));
        vo.setAttributeLastname((attributeLastname == null? attributeLastname : attributeLastname.trim()));
        vo.setAttributePosition((attributePosition == null? attributePosition : attributePosition.trim()));
        
        vo.setAttributeNameTraining((attributeNameTraining == null? attributeNameTraining : attributeNameTraining.trim()));
        vo.setAttributeNameReporting((attributeNameReporting == null? attributeNameReporting : attributeNameReporting.trim()));
        vo.setAttributeValueTraining((attributeValueTraining == null? attributeValueTraining : attributeValueTraining.trim()));
        vo.setAttributeValueReporting((attributeValueReporting == null? attributeValueReporting : attributeValueReporting.trim()));
        vo.setMainField(mainField);
        
        return vo;
    }

    public int getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(int idProvider) {
        this.idProvider = idProvider;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getAttributePixauth() {
        return attributePixauth;
    }

    public void setAttributePixauth(String attributePixauth) {
        this.attributePixauth = attributePixauth;
    }

    public String getAttributeValPixauth() {
        return attributeValPixauth;
    }

    public void setAttributeValPixauth(String attributeValPixauth) {
        this.attributeValPixauth = attributeValPixauth;
    }

    public String getAttributeUsername() {
        return attributeUsername;
    }

    public void setAttributeUsername(String attributeUsername) {
        this.attributeUsername = attributeUsername;
    }

    public String getAttributeEmail() {
        return attributeEmail;
    }

    public void setAttributeEmail(String attributeEmail) {
        this.attributeEmail = attributeEmail;
    }

    public String getAttributeFirstname() {
        return attributeFirstname;
    }

    public void setAttributeFirstname(String attributeFirstname) {
        this.attributeFirstname = attributeFirstname;
    }

    public String getAttributeLastname() {
        return attributeLastname;
    }

    public void setAttributeLastname(String attributeLastname) {
        this.attributeLastname = attributeLastname;
    }

    public String getAttributePosition() {
        return attributePosition;
    }

    public void setAttributePosition(String attributePosition) {
        this.attributePosition = attributePosition;
    }

    public String getUseAttributeTraining() {
        return useAttributeTraining;
    }

    public void setUseAttributeTraining(String useAttributeTraining) {
        this.useAttributeTraining = useAttributeTraining;
    }

    public String getUseAttributeReporting() {
        return useAttributeReporting;
    }

    public void setUseAttributeReporting(String useAttributeReporting) {
        this.useAttributeReporting = useAttributeReporting;
    }

    public String getAttributeNameTraining() {
        return attributeNameTraining;
    }

    public void setAttributeNameTraining(String attributeNameTraining) {
        this.attributeNameTraining = attributeNameTraining;
    }

    public String getAttributeNameReporting() {
        return attributeNameReporting;
    }

    public void setAttributeNameReporting(String attributeNameReporting) {
        this.attributeNameReporting = attributeNameReporting;
    }

    public String getAttributeValueTraining() {
        return attributeValueTraining;
    }

    public void setAttributeValueTraining(String attributeValueTraining) {
        this.attributeValueTraining = attributeValueTraining;
    }

    public String getAttributeValueReporting() {
        return attributeValueReporting;
    }

    public void setAttributeValueReporting(String attributeValueReporting) {
        this.attributeValueReporting = attributeValueReporting;
    }

    public Integer[] getPositionIds() {
        return positionIds;
    }

    public void setPositionIds(Integer[] positionIds) {
        this.positionIds = positionIds;
    }
    
    public Integer getPositionIds(int index) {
        return positionIds[index];
    }
    
    public void setPositionIds(int index, Integer positionId){
        // Allocate the array
	if (positionIds == null) {
            positionIds = new Integer[index + 1];
	}

	// Grow the array
	if (index >= positionIds.length) {
            Integer[] newIntArray = new Integer[index + 1];
            System.arraycopy(positionIds, 0, newIntArray , 0, positionIds.length);
            positionIds = newIntArray ;
	}
        
        // Set the indexed property
	positionIds[index] = positionId;
    }

    public String[] getPositionsValues() {
        return positionsValues;
    }
    
    public String getPositionsValues(int index) {
        return positionsValues[index];
    }

    public void setPositionsValues(String[] positionsValues) {
        this.positionsValues = positionsValues;
    }
    
    public void setPositionsValues(int index, String positionsValue){
        positionsValues = addToArrayString(positionsValues, index, positionsValue);
    }

    public String[] getPositionsTitles() {
        return positionsTitles;
    }
    
    public String getPositionsTitles(int index) {
        return positionsTitles[index];
    }

    public void setPositionsTitles(String[] positionsTitles) {
        this.positionsTitles = positionsTitles;
    }

    public void setPositionsTitles(int index, String positionsTitle){
        positionsTitles = addToArrayString(positionsTitles, index, positionsTitle);
    }

    public String getMainField() {
        return mainField;
    }

    public void setMainField(String mainField) {
        this.mainField = mainField;
    }

    public String getSelfConfRoles() {
        return selfConfRoles;
    }

    public void setSelfConfRoles(String selfConfRoles) {
        this.selfConfRoles = selfConfRoles;
    }

    public String getSelfConfLocations() {
        return selfConfLocations;
    }

    public void setSelfConfLocations(String selfConfLocations) {
        this.selfConfLocations = selfConfLocations;
    }

    public String getSelfConfPositions() {
        return selfConfPositions;
    }

    public void setSelfConfPositions(String selfConfPositions) {
        this.selfConfPositions = selfConfPositions;
    }

    public String getOnlyMainField() {
        return onlyMainField;
    }

    public void setOnlyMainField(String onlyMainField) {
        this.onlyMainField = onlyMainField;
    }

    public String getAllowSSOUpdate() {
        return allowSSOUpdate;
    }

    public void setAllowSSOUpdate(String allowSSOUpdate) {
        this.allowSSOUpdate = allowSSOUpdate;
    }
    
    private String[] addToArrayString(String[] arrayString, int index, String stringValue){
        // Allocate the array
        if (arrayString == null) {
            arrayString = new String[index + 1];
        }

        // Grow the array
        if (index >= arrayString.length) {
            String[] newStringArray = new String[index + 1];
            System.arraycopy(arrayString, 0, newStringArray , 0, arrayString.length);
            arrayString = newStringArray ;
        }

        // Set the indexed property
        arrayString[index] = stringValue;
        return arrayString;
    }
}
