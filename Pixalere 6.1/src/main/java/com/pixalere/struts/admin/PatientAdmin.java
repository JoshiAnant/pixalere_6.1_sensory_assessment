package com.pixalere.struts.admin;

import com.pixalere.utils.Common;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.admin.bean.AssignPatientsVO;
import java.util.Date;
import java.util.Vector;
import com.pixalere.utils.PDate;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import java.util.Collection;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import com.pixalere.utils.Constants;

public class PatientAdmin extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientAdmin.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
            
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        PatientAdminForm patientAdminForm = (PatientAdminForm) form;
        request.setAttribute("isPost", "0");
        try {

            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            //accountVO.setAccount_status(patientAdminForm.getAccountStatus());
            request.setAttribute("action", request.getParameter("action"));
            PatientServiceImpl managerBD = new PatientServiceImpl(language);
            PatientProfileServiceImpl profilemanagerBD = new PatientProfileServiceImpl(language);
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            PatientProfileVO patientProfileVO = new PatientProfileVO();
            patientProfileVO.setPatient_id(patientAdminForm.getPatientId());
            request.setAttribute("patientId", patientAdminForm.getPatientId());
            patientProfileVO.setCurrent_flag(new Integer(1));
            PatientProfileVO prf = profilemanagerBD.getPatientProfile(patientProfileVO);
            if (request.getParameter("action").equals("transfer")) {
                PatientAccountVO v = managerBD.getPatient(patientAdminForm.getPatientId());
                int patient_account_id_old = v.getId();
                PatientAccountVO accountVO = patientAdminForm.getFormData(v);
                // added for search by professional
                accountVO.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale));
                accountVO.setCreated_by(((ProfessionalVO) session.getAttribute("userVO")).getId());
                //accountVO.setTime_stamp(pdate.getEpochTime()+"");
                // Update the patient profile
                if (prf != null) {
                    prf.setCurrent_flag(new Integer(0));
                    profilemanagerBD.savePatientProfile(prf);
                    prf.setUser_signature(accountVO.getUser_signature());
                    prf.setCreated_on(new Date());
                    prf.setLastmodified_on(new Date());
                    prf.setProfessional_id(((ProfessionalVO) session.getAttribute("userVO")).getId());
                    prf.setTreatment_location_id(accountVO.getTreatment_location_id());
                    prf.setCurrent_flag(new Integer(1));
                    prf.setId(null);
                    profilemanagerBD.savePatientProfile(prf);
                }

                managerBD.savePatient(accountVO);
                accountVO.setId(null);
                PatientAccountVO p = managerBD.getPatient(accountVO);
                if (p != null && p.getPatient_id() != null) {
                    request.setAttribute("action", "transfer");
                    
                    ReferralsTrackingServiceImpl rservice = new ReferralsTrackingServiceImpl(language);
                    if (!Common.getConfig("purgeReferrals").equals("1") && v != null) {
                        //update referrals with new location so proper clinician has access.
                        ReferralsTrackingVO r = new ReferralsTrackingVO();
                        //r.setEntry_type(Constants.REFERRAL);
                        r.setCurrent(1);
                        r.setPatient_id(v.getPatient_id());
                        if (p != null) {
                            Collection<ReferralsTrackingVO> refs = rservice.getAllReferralsByCriteria(r);
                            for (ReferralsTrackingVO ref : refs) {
                                ref.setPatient_account_id(p.getId());
                                rservice.saveReferralsTracking(ref);
                            }
                        }
                    } else if (v != null) {
                        //remove outstanding referrals
                        try {
                            //Collection<WoundVO> wounds = wservice.getWounds(p.getPatient_id());
                            Vector recommendations = new Vector();
                            //for (WoundVO wound : wounds) {
                            ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                            rtmp.setPatient_id(v.getPatient_id());
                            rtmp.setEntry_type(Constants.REFERRAL);
                            rtmp.setCurrent(Constants.ACTIVE);
                            Collection<ReferralsTrackingVO> referrals = rservice.getAllReferralsByCriteria(rtmp);
                            for (ReferralsTrackingVO referral : referrals) {
                                rservice.removeReferral(referral);
                            }
                            rtmp.setEntry_type(Constants.RECOMMENDATION);
                            rtmp.setCurrent(Constants.ACTIVE);
                            referrals = rservice.getAllReferralsByCriteria(rtmp);
                            for (ReferralsTrackingVO referral : referrals) {
                                rservice.removeReferral(referral);
                            }
                            //}
                        } catch (ApplicationException e) {
                            log.error(e.getMessage());
                        }
                    }
                } else {
                    request.setAttribute("action", "add");
                }
                request.setAttribute("patientAccountEdit", p);
                //request.setAttribute("unserializeTreat", patientVO.getUnserializedString( database));
                ListServiceImpl bd = new ListServiceImpl(language);
                /* PIXALERE 4.1, NEED TO INVESTIGATE IMPACT
                PatientAccountVO p = managerBD.getPatient(accountVO);
                //update referrals with new location so proper clinician has access.
                ReferralsTrackingServiceImpl refManager = new ReferralsTrackingServiceImpl(language);
                ReferralsTrackingVO r = new ReferralsTrackingVO();
                r.setEntry_type(Constants.REFERRAL);
                r.setCurrent(1);
                r.setPatient_account_id(patient_account_id_old);
                if(p!=null){
                Collection<ReferralsTrackingVO> refs =  refManager.getAllReferralsByCriteria(r);
                for(ReferralsTrackingVO ref : refs){
                ref.setPatient_account_id(p.getId());
                refManager.saveReferralsTracking(ref);
                }
                }
                //update referrals with new location so proper nurse has access.
                r = new ReferralsTrackingVO();
                r.setEntry_type(Constants.RECOMMENDATION);
                r.setCurrent(1);
                r.setPatient_account_id(patient_account_id_old);
                if(p!=null){
                Collection<ReferralsTrackingVO> refs =  refManager.getAllReferralsByCriteria(r);
                for(ReferralsTrackingVO ref : refs){
                ref.setPatient_account_id(p.getId());
                refManager.saveReferralsTracking(ref);
                }
                }
                
                request.setAttribute("patientAccountEdit", p);
                //request.setAttribute("unserializeTreat", patientVO.getUnserializedString( database));
                ListServiceImpl bd = new ListServiceImpl(language);
                if (session.getAttribute("admin_patient_id") != null) {
                Vector patients = managerBD.findAllBySearch(Integer.parseInt((String) session.getAttribute("admin_patient_id")), "",(ProfessionalVO) session.getAttribute("userVO"),"",0,false);
                request.setAttribute("patientList", patients);
                }
                 */
                if (session.getAttribute("admin_patient_id") != null) {
                    Vector patients = managerBD.findAllBySearch(Integer.parseInt((String) session.getAttribute("admin_patient_id")), "", (ProfessionalVO) session.getAttribute("userVO"), "", 0, "", "", "", false, language);
                    request.setAttribute("patientList", patients);
                } else {
                    request.setAttribute("patientList", null);
                }

                request.setAttribute("action", "edit");
                accountVO.setPatient_id(new Integer(patientAdminForm.getPatientId()));
                // added for search by professional
                accountVO.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale));
                accountVO.setCreated_by(((ProfessionalVO) session.getAttribute("userVO")).getId());

                LookupVO lookupVO = (LookupVO) bd.getListItem(new Integer(accountVO.getTreatment_location_id()).intValue());
                request.setAttribute("treatmentEdit", lookupVO);
                session.setAttribute("admin_patient_id", patientAdminForm.getPatientId() + "");
                request.setAttribute("isPost", "1");
            } else if (request.getParameter("action").equals("edit")) {
                //System.out.println("in patient edit 1");
            } else {
                PatientAccountVO accountVO = patientAdminForm.getFormData(null);
                request.setAttribute("action", "add");
                request.setAttribute("isPost", "1");
                accountVO.setAccount_status(new Integer(1));
                // added for search by professional
                accountVO.setUser_signature(pdate.getProfessionalSignature(new Date(), (ProfessionalVO) session.getAttribute("userVO"), locale));
                accountVO.setCreated_by(((ProfessionalVO) session.getAttribute("userVO")).getId());
                accountVO.setVisits(new Integer("0"));
                int admin_patient_id = managerBD.addPatient(accountVO);
                session.setAttribute("admin_patient_id", admin_patient_id + "");
                if (session.getAttribute("admin_patient_id") != null) {
                    Vector patients = managerBD.findAllBySearch(Integer.parseInt((String) session.getAttribute("admin_patient_id")), "", (ProfessionalVO) session.getAttribute("userVO"), "", 0, "", "", "", false, language);
                    request.setAttribute("patientList", patients);
                } else {
                    request.setAttribute("patientList", null);
                }

            }
            // Populate treatment list
            LookupVO vo = new LookupVO();
            ListServiceImpl bd = new ListServiceImpl(language);

            Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
            Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
            request.setAttribute("treatmentCats", treatmentCatsVO);
            request.setAttribute("treatmentList", treatmentVO);
            LookupVO vo2 = new LookupVO();
            ListsDAO listDAO = new ListsDAO();
            vo2.setResourceId(new Integer(vo2.PATIENT_RESIDENCE));
            request.setAttribute("patientResidence", listDAO.findAllByCriteria(vo2));
            vo2.setResourceId(new Integer(vo.GENDER));
            request.setAttribute("gender", listDAO.findAllByCriteria(vo2));
            String patient_id_get = session.getAttribute("admin_patient_id") + "";
            PatientAccountVO patientVOGet = (PatientAccountVO) managerBD.getPatient(Integer.parseInt(patient_id_get));
            // START CHECK ACCESS PERMISSIONS
            boolean access = false;
            if (patientVOGet.getTreatment_location_id() == -1 || (pservice.validate(userVO.getRegions(), patientVOGet.getTreatment_location_id()) == true)) {
                access = true;
            }
            AssignPatientsServiceImpl apManagerBD = new AssignPatientsServiceImpl();
            AssignPatientsVO apvo = new AssignPatientsVO();
            apvo.setPatientId(patientVOGet.getPatient_id());
            apvo.setProfessionalId(userVO.getId());
            AssignPatientsVO findAPVO = apManagerBD.getAssignedPatient(apvo);
            if (findAPVO != null) {
                access = true;
            }
            //System.out.println("apvo id:" + findAPVO.getId());
            // END CHECK ACCESS PERMISSIONS
         
            request.setAttribute("patientVORelevant", patientVOGet);
            request.setAttribute("patientAccountEdit", patientVOGet);
            request.setAttribute("patientAccess", access);
          


        } catch (Exception e) {
            System.out.println("error in pa:" + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("summary")) {
            return (mapping.findForward("uploader.go.summary"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("reporting")) {
            return (mapping.findForward("uploader.go.reporting"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("admin")) {
            return (mapping.findForward("uploader.go.admin"));
        } else {
            if (request.getAttribute("action").equals("transfer")) {
                return (mapping.findForward("admin.patient.success"));
            } else if (request.getAttribute("action").equals("edit")) {
                return (mapping.findForward("admin.patient.success"));
            } else if (request.getAttribute("action").equals("add")) {
                return (mapping.findForward("admin.patient.success"));
            } else {
                return (mapping.findForward("admin.patient.success"));
            }
        }
    }
}
