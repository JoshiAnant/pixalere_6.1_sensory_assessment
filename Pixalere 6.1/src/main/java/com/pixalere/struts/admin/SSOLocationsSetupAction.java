package com.pixalere.struts.admin;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.sso.bean.SSOLocationVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.service.OpenSAMLService;
import com.pixalere.sso.service.SSOProviderService;
import com.pixalere.utils.Common;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Management UI for SSO Configuration Admin.
 *
 * @author Jose
 * @since 6.0
 */
public class SSOLocationsSetupAction  extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOLocationsSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        // To check session and know if ProviderId has been set
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        
        if (request.getParameter("providerId") == null && session.getAttribute("providerId") == null) {
            // No provider go back to Providers list
            return (mapping.findForward("admin.ssolocations.failure"));
        }
        
        SSOProviderService service = new SSOProviderService();
        String s_p_id = request.getParameter("providerId") != null
                ? request.getParameter("providerId")
                : (String) session.getAttribute("providerId");
        session.setAttribute("providerId", null);

        int provider_id = Integer.parseInt(s_p_id);
        SSOProviderVO provider = service.getProviderById(provider_id);
        
        if (provider == null){
            // No provider go back to Providers list
            return (mapping.findForward("admin.ssolocations.failure"));
        }
        
        // Info tab
        String baseURL = OpenSAMLService.getBaseUrl(request);
        String urlConsumer = OpenSAMLService.getAssertionConsumerUrl(baseURL);
        request.setAttribute("urlConsumer", urlConsumer);
        String xmlMetadata = OpenSAMLService.generateMetadataXML(provider, urlConsumer);
        request.setAttribute("xmlMetadata", xmlMetadata);
        //String urlLogout = OpenSAMLService.getLogoutUrl(baseURL);
        //request.setAttribute("urlLogout", urlLogout);
        
        // Add or Edit form
        if (request.getParameter("action") != null && !request.getParameter("action").equals("updateConfig")) {
            SSOLocationVO locationVO = new SSOLocationVO();
            ListServiceImpl listService = new ListServiceImpl(language);
            try {
                if (request.getParameter("action").equals("edit") && request.getParameter("locationId") != null){
                    // Get locationVO
                    String s_loc_id = request.getParameter("locationId");
                    
                    // Location id
                    int location_id = Integer.parseInt(s_loc_id);
                    locationVO = service.getSSOLocationById(location_id);
                    
                    // SSO Location locations
                    List<Integer> locationIdList = new ArrayList<Integer>();
                    String ids = locationVO.getPixLocationsids();
                    if (ids != null && !ids.isEmpty()){
                        String[] arrayIds = ids.split(",");
                        for (String sLoc : arrayIds) {
                            int idLoc = Integer.parseInt(sLoc);
                            locationIdList.add(idLoc);
                        }
                    }
                    
                    // General selector filter
                    List<LookupVO> professionalRegions = new ArrayList<LookupVO>();
                    int[] reg = LookupVO.TREATMENT_LOCATION;
                    for (int i = 0; i < reg.length; i++) {
                        // Faster to get all and then filter 
                        Collection<LookupVO> allLocations = listService.getLists(reg[i]);
                        for (LookupVO generalLocation : allLocations) {
                            if (locationIdList.contains(generalLocation.getId())){
                                generalLocation.setResourceId(new Integer(0));
                                generalLocation.setTitle(new Integer(0));
                                professionalRegions.add(generalLocation);
                            }
                        }
                    }
                    
                    request.setAttribute("professionalRegions", professionalRegions);
                }
                
                // Locations selector
                ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
                Vector treatmentWithCatsVO = (Vector) listService.findAllTreatmentsWithCats(userVO);
                request.setAttribute("treatmentListWithCats", treatmentWithCatsVO);
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in SSOLocationsSetupAction.execute(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
            
            
            
            request.setAttribute("location", locationVO);
            request.setAttribute("provider", provider);
            return (mapping.findForward("admin.ssolocations.edit"));
        }
        
        // Update attribute name
        if (request.getParameter("action") != null && request.getParameter("action").equals("updateConfig")) {
            String attrName = request.getParameter("attributeLocations");
            provider.setAttributeLocations((attrName == null? attrName : attrName.trim()));
            try {
                service.saveProvider(provider);
                // reload
                provider = service.getProviderById(provider_id);
            } catch (ApplicationException e) {
                log.error("An application exception has been raised in SSOLocationsSetupAction.execute(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception", "y");
                request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                errors.add("exception", new ActionError("pixalere.common.error"));
                saveErrors(request, errors);
                return (mapping.findForward("pixalere.error"));
            }
        }
        
        // General Table
        try {
            
            // Get all locations names entries for the current language
            ListServiceImpl bd = new ListServiceImpl(language);
            Map<Integer, LookupVO> locationsMap = new HashMap<Integer, LookupVO>();
            int[] reg = LookupVO.TREATMENT_LOCATION;
            for (int i = 0; i < reg.length; i++) {
                // Faster to get all and then filter 
                Collection<LookupVO> locationsLookups = bd.getLists(reg[i]);
                for (LookupVO locationLookup : locationsLookups) {
                    locationsMap.put(locationLookup.getId(), locationLookup);
                }
            }
            // Set the localized name for each location_id in each defined SSO location 
            Collection<SSOLocationVO> ssoLocations = provider.getSsoLocations();
            // Set up location names for each attribute value set
            for (SSOLocationVO locationVO : ssoLocations) {
                List<String> locationTitles = new ArrayList<String>();
                String ids = locationVO.getPixLocationsids();
                if (ids != null && !ids.isEmpty()){
                    String[] arrayIds = ids.split(",");
                    for (String sLoc : arrayIds) {
                        int idLoc = Integer.parseInt(sLoc);
                        LookupVO vo = locationsMap.get(idLoc);
                        if (vo != null){
                            locationTitles.add(vo.getName(language));
                        }
                    }
                }
                String titles = "";
                for (Iterator<String> it = locationTitles.iterator(); it.hasNext();) {
                    String string_t = it.next();
                    titles += string_t;
                    if (it.hasNext()){
                        titles += ", ";
                    }
                }
                locationVO.setTitles(titles);
            }
            
            request.setAttribute("locations", ssoLocations);
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in SSOLocationsSetupAction.execute(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        request.setAttribute("provider", provider);
        
        return (mapping.findForward("admin.ssolocations.success"));
    }
    
}
