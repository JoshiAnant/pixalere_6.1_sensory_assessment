/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.*;
import com.pixalere.reporting.bean.CCACReportVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import java.util.Vector;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/**
 * Getting CCAC Reports from ccac_reporting table.   This class will allow
 * a user with proper rights to retrieve reports previously generated, and build
 * the report to the CCAC Standard, and return the pdf stream.  By default this class 
 * will retrieve all new ccac reports.
 * 
 * @version 5.1
 * @since 5.1
 * @author travis
 */
public class CCACReportsSetupAction  extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CCACReports.class);
    /**
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        try{
            HttpSession session = request.getSession();
            ReportingServiceImpl rservice = new ReportingServiceImpl();
            ProfessionalVO user = (ProfessionalVO)session.getAttribute("userVO");
            //get all new ccac reports within your treatment locations.
            int[] treatments = new int[user.getRegions().size()];
            int cnt = 0;
            for(UserAccountRegionsVO region : user.getRegions()){
                treatments[cnt] = region.getTreatment_location_id();
                cnt++;
            }
            Collection<CCACReportVO> reports = rservice.getCCACReports(1, 0, null, null, treatments);
            request.setAttribute("reports",reports);
            ListServiceImpl bd = new ListServiceImpl();
            
            Vector treatmentVOList = bd.findAllTreatmentsWithCats(user);
            request.setAttribute("treatmentList2", treatmentVOList);
        } catch(ApplicationException e){
            log.error("An application exception has been raised in AssignPatientsAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null &&request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if (request.getParameter("page")!=null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.ccacreports.success"));
        }
    }
    
}