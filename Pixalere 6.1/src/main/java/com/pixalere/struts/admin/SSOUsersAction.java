package com.pixalere.struts.admin;

import com.pixalere.common.ApplicationException;
import com.pixalere.sso.bean.SSOPositionVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.service.SSOProviderService;
import com.pixalere.utils.Common;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Save Users SSO Identity settings action.
 * 
 * @author Jose
 * @since 6.0
 */
public class SSOUsersAction  extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOAdminAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
        try{
            HttpSession session = request.getSession();
            SSOProviderService service = new SSOProviderService();
            SSOUsersForm idpform = (SSOUsersForm) form;
            
            // Get provider to be updated
            SSOProviderVO updatedProvider = service.getProviderById(idpform.getIdProvider());
            
            if (updatedProvider != null) {
                updatedProvider = idpform.getUpdatedProvider(updatedProvider);
                // Update provider data
                service.saveProvider(updatedProvider);
                
                // Update positions array values
                List<SSOPositionVO> positions = idpform.getPositions(updatedProvider.getId());
                service.removePositions(updatedProvider.getId());
                for (SSOPositionVO newPosition : positions) {
                    service.savePosition(newPosition);
                }
                
                // Store in session current Provider (Redirecting to SSOUsersSetup)
                session.setAttribute("providerId", String.valueOf(updatedProvider.getId()));
            } else {
                System.out.println("ERROR: SSOUsersAction - SSOProvider to update not found id: " + idpform.getIdProvider());
            }
            
        } catch(ApplicationException e){
            log.error("An application exception has been raised in SSOUsersAction.execute: " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        // Return to Users config page
        return (mapping.findForward("admin.sso.success"));
    }
}
