package com.pixalere.struts.admin;

import java.util.List;
import com.pixalere.assessment.dao.WoundAssessmentDAO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.utils.Common;
import java.util.Vector;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Constants;
import java.util.Collection;
import java.util.ArrayList;

public class TreatmentOverview extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TreatmentOverview.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        TreatmentOverviewForm tForm = (TreatmentOverviewForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        try {
            request.setAttribute("tab_select", "overviewDiv");
            String locale = Common.getLanguageLocale(language);
            
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            //gather results.
            PatientServiceImpl pservice = new PatientServiceImpl(language);
            Vector results = pservice.findAllOverviewBySearch(tForm.getPatient_status(), tForm.getCaretype_status(), tForm.getCaretypes(), tForm.getSkin_goal(),tForm.getWound_goal(), tForm.getOstomy_goal(), tForm.getTubedrain_goal(), tForm.getIncision_goal(), tForm.getTreatmentLocation(), locale);
            /*int patient_status, int care_type_status,
             String care_type_all, String care_type_wound, String care_type_incision, String care_type_tube, String care_type_ostomy, String care_type_burn,
             int wound_type, int incision_type, int tube_type, int ostomy_type, int burn_type,
             String[] treatmentLocations*/

            request.setAttribute("patients", results);
            //Repopulate all the other tabs.

            ProfessionalServiceImpl profService = new ProfessionalServiceImpl();
            AssignPatientsServiceImpl aService = new AssignPatientsServiceImpl();
            WoundAssessmentServiceImpl wservice = new WoundAssessmentServiceImpl();
            ListServiceImpl bd = new ListServiceImpl(language);
            
            if (session.getAttribute("patient_id") != null) {
                WoundAssessmentDAO was = new WoundAssessmentDAO();
                Collection<WoundAssessmentVO> assess = null; //get all assessments by user.
                try {
                    WoundAssessmentVO wass = new WoundAssessmentVO();
                    wass.setPatient_id(new Integer(session.getAttribute("patient_id") + ""));
                    wass.setActive(1);
                    assess = was.findAllByCriteriaByRange(wass, "", "");
                } catch (Exception e) {
                }
                List filterVisits = wservice.getVisitCount(assess, null, null, language);
                request.setAttribute("visit", filterVisits);
                if (userVO.getAccess_assigning().equals(new Integer(1))) {
                    int patient_id = Integer.parseInt(session.getAttribute("patient_id") + "");
                    ProfessionalVO pt = new ProfessionalVO();
                    pt.setAccount_status(1);
                    request.setAttribute("professionals", profService.findAllBySearch(userVO.getId(), "", "", -1, "", null,""));
                    AssignPatientsVO atmp = new AssignPatientsVO();
                    atmp.setPatientId(patient_id);
                    request.setAttribute("professionalsa", aService.getAllAssignedPatients(atmp));
                }
                request.setAttribute("inactivate_reason", bd.getLists(LookupVO.INACTIVATE_REASON));
            }
            Vector<LookupVO> treatmentCatsVO = (Vector) bd.getLists(LookupVO.TREATMENT_CATEGORY, userVO, false);
            Vector<LookupVO> treatmentVO = (Vector) bd.getLists(LookupVO.TREATMENT_LOCATION, userVO, false);

            if (userVO.getTraining_flag() == 1) {
                List<LookupVO> treatmentsWithCats = new ArrayList();
                List<LookupVO> treatments = new ArrayList();
                //remove all treatment locations but training if this user is a training user.
                for (LookupVO l : treatmentCatsVO) {
                    if (l.getCategoryId() == Constants.TRAINING_TREATMENT_CATEGORY_ID) {
                        treatmentsWithCats.add(l);
                    } else {
                    }
                }
                for (LookupVO l : treatmentVO) {
                    if (l.getId() == Constants.TRAINING_TREATMENT_ID) {
                        treatments.add(l);
                    }
                }
                request.setAttribute("treatmentCats", treatmentsWithCats);
                request.setAttribute("treatmentList", treatments);
            } else {
                request.setAttribute("treatmentCats", treatmentCatsVO);
                request.setAttribute("treatmentList", treatmentVO);
            }
            Vector treatmentVOList = null;
            if (userVO.getTraining_flag() == null || userVO.getTraining_flag() != 1) {
                treatmentVOList = (Vector) bd.findAllTreatmentsWithCats(userVO);
            } else {
                treatmentVOList = (Vector) bd.findAllTrainingTreatments(userVO);
            }
            request.setAttribute("treatmentList2", treatmentVOList);
            request.setAttribute("professionalAccount", userVO);
            request.setAttribute("patientAccess", false);
            request.setAttribute("woundGoals", bd.getLists(LookupVO.WOUND_GOALS));
            request.setAttribute("drainGoals", bd.getLists(LookupVO.TUBEDRAIN_GOALS));
            request.setAttribute("ostomyGoals", bd.getLists(LookupVO.OSTOMY_GOALS));
            request.setAttribute("skinGoals", bd.getLists(LookupVO.SKIN_GOALS));
            //get components records for this table
            ListsDAO listDAO = new ListsDAO();
            com.pixalere.common.service.GUIServiceImpl gui = new com.pixalere.common.service.GUIServiceImpl();
            com.pixalere.common.bean.ComponentsVO comp = new com.pixalere.common.bean.ComponentsVO();
            comp.setFlowchart(com.pixalere.utils.Constants.PATIENT_PROFILE);
            java.util.Collection components = gui.getAllComponents(comp);
            java.util.Hashtable hashized = Common.hashComponents(components);
            request.setAttribute("components", hashized);
            LookupVO vo = new LookupVO();
            vo.setResourceId(new Integer(vo.GENDER));
            request.setAttribute("gender", listDAO.findAllByCriteria(vo));
            vo.setResourceId(new Integer(LookupVO.PATIENT_RESIDENCE));
            request.setAttribute("patientResidence", listDAO.findAllByCriteria(vo));

        } catch (Exception e) {
            log.error("An application exception has been raised in PatientAdmin.perform(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        log.debug("Leaving PatientAdminSearch ****************");
        return (mapping.findForward("admin.treatmentoverview.success"));
    }
}
