/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin;

import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import javax.servlet.ServletContext;
//import com.pixalere.common.service.PdfServiceImpl;
//import com.pixalere.common.bean.Pdf;

import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;

/**
 *
 * @author travdes
 */
public class VersionCheckSetupAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();

        request.setAttribute("status", Common.migrationStatus());

        if (!Common.isDBPatched()) {
            return (mapping.findForward("version.success"));
        } else {
            return (mapping.findForward("go.login"));
        }

    }
}