package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import com.pixalere.utils.Constants;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
public class PositionsAdminSetupAction extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PositionsAdminSetupAction.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm     form,
            HttpServletRequest request,
            HttpServletResponse response){
        request.setAttribute("page","admin");
        HttpSession session = request.getSession();
        
        if(request.getParameter("list_action")!=null&&request.getParameter("list_action").equals("edit")){
            try{
                ProfessionalServiceImpl bd=new ProfessionalServiceImpl();
                PositionVO pos = new PositionVO();pos.setId(Integer.parseInt(request.getParameter("choice")));
                PositionVO vo =(PositionVO)bd.getPosition(pos);
                request.setAttribute("position",vo);
                request.setAttribute("choice",vo.getId()+"");
                request.setAttribute("action","edit");
            } catch(ApplicationException e){
                log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
                request.setAttribute("exception","y");
                request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
                errors.add("exception",new ActionError("pixalere.common.error"));
                saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                
            }
        } else if(request.getParameter("list_action")!=null&&request.getParameter("list_action").equals("delete")){
            try{
                ProfessionalServiceImpl manager=new ProfessionalServiceImpl();
                PositionVO pos = new PositionVO();pos.setId(Integer.parseInt(request.getParameter("choice")));
                PositionVO vo =(PositionVO)manager.getPosition(pos);
                vo.setActive(0);
                manager.removePosition(vo);
            } catch(ApplicationException e){
                log.error("An application exception has been raised in PatientAdminSetupAction.perform(): " + e.toString());
                ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                
            }
        } else{
            request.setAttribute("action","add");
        }
        try{
            ProfessionalServiceImpl catBD=new ProfessionalServiceImpl();
            
            Collection<PositionVO> positions=(Collection)catBD.getPositions(new PositionVO(Constants.ACTIVE));
            
            request.setAttribute("positions",positions);
            
        }catch(ApplicationException e){
            log.error("Exception has occurred in ProductsCategoryAdminSetupAction.perform(): "+ e.toString());
    	    System.out.println("Error ProductsCategoryAdminSetupAction: "+e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",com.pixalere.utils.Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
        
        if(request.getParameter("page")!=null && request.getParameter("page").equals("patient")){
            return (mapping.findForward("uploader.go.patient"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("woundprofiles")){
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("assess")) {
            return (mapping.findForward("uploader.go.assess"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("viewer")){
            return (mapping.findForward("uploader.go.viewer"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }				else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else{
            return (mapping.findForward("admin.positions.success"));
        }
    }
    
}
