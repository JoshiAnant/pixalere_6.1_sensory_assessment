/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.admin.patient;

import com.pixalere.utils.Common;
import com.pixalere.assessment.bean.NursingFixesVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import java.util.Date;
import java.util.Vector;
import com.pixalere.utils.PDate;
import java.util.Collection;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 * A {@link org.apache.struts.action.Action} action which updates patients
 *
 * @since 5.0
 * @author travis morris
 */
public class PatientEdit extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientEdit.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PatientEditForm patientAdminForm = (PatientEditForm) form;
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        try {
            boolean tokenValid = isTokenValid(request);
            if (session.getAttribute("patient_id") == null) {
                ActionErrors errors = new ActionErrors();
                errors.add("exception", new ActionError("pixalere.patient_id.null"));
                saveErrors(request, errors);
                return (mapping.findForward("admin.patient"));
            }
            PatientServiceImpl managerBD = new PatientServiceImpl(language);
            if (tokenValid) {
                resetToken(request);
            PatientAccountVO accountVO = patientAdminForm.getFormData(null, request);
            accountVO.setAction("");
            accountVO.setPatient_id(new Integer(session.getAttribute("patient_id") + ""));
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
            PDate pdate = new PDate(userVO.getTimezone());
            System.out.println("in patient edit 3");
            // added for search by professional
            accountVO.setUser_signature(pdate.getProfessionalSignature(new Date(), userVO, locale));
            accountVO.setCreated_by(userVO.getId());
            //Check if gender changed, if it did, we'll need to change blueman body parts to the new gender.
            PatientAccountVO oldAccount = (PatientAccountVO) session.getAttribute("patientAccount");
            managerBD.savePatient(accountVO);

            PatientAccountVO p = managerBD.getPatient(new Integer(session.getAttribute("patient_id") + ""));
            request.setAttribute("patientAccountEdit", p);

            session.setAttribute("patientAccount", p);
            if (oldAccount != null) {
                //System.out.println("Gender: "+p);oldAccount.getGender()+" "+accountVO.getGender());
                if (!oldAccount.getGender().equals(accountVO.getGender())) {

                    //gender changed.. get wp's
                    WoundServiceImpl wmanager = new WoundServiceImpl(language);
                    Collection<WoundVO> wounds = wmanager.getWounds(accountVO.getPatient_id());
                    boolean foundWoundWithGender = false;
                    int wound_id = 0;
                    for (WoundVO wound : wounds) {
                        String wound_image_name = wound.getImage_name();
                        String gender = wound_image_name.substring(0, 1);
                        String location = wound.getWound_location_detailed();
                        wound_image_name = wound_image_name.substring(1, wound_image_name.length());//with gender removed.
                        AssessmentServiceImpl aservice = new AssessmentServiceImpl();
                        if (gender.equals("m")) {
                            gender = "f";
                            wound_image_name = gender + wound_image_name;
                            wound.setImage_name(wound_image_name);
                            wmanager.saveWound(wound);
                            foundWoundWithGender = true;
                            NursingFixesVO fixes = new NursingFixesVO();
                            fixes.setPatient_id(oldAccount.getPatient_id());
                            fixes.setWound_id(wound_id);
                            fixes.setProfessional_id(userVO.getId());
                            fixes.setRow_id(wound_id);
                            fixes.setDelete_reason(Common.getLocalizedString("pixalere.woundprofile.fixes.wound_location", locale) + ": " + Common.getLocalizedString("pixalere.woundprofile.fixes.genderchange", locale));
                            fixes.setCreated_on(new Date());
                            fixes.setInitials(pdate.getProfessionalSignature(new Date(), userVO, locale));
                            fixes.setField("98");
                            fixes.setError(location);
                            aservice.saveNursingFixes(fixes);
                        } else if (gender.equals("f")) {
                            gender = "m";
                            wound_image_name = gender + wound_image_name;
                            wound.setImage_name(wound_image_name);
                            wmanager.saveWound(wound);
                            foundWoundWithGender = true;
                            NursingFixesVO fixes = new NursingFixesVO();
                            fixes.setPatient_id(oldAccount.getPatient_id());
                            fixes.setWound_id(wound_id);
                            fixes.setProfessional_id(userVO.getId());
                            fixes.setRow_id(wound_id);
                            fixes.setDelete_reason(Common.getLocalizedString("pixalere.woundprofile.fixes.wound_location", locale) + " :" + Common.getLocalizedString("pixalere.woundprofile.fixes.genderchange", locale));
                            fixes.setCreated_on(new Date());
                            fixes.setInitials(pdate.getProfessionalSignature(new Date(), userVO, locale));
                            fixes.setField("98");
                            fixes.setError(location);
                            aservice.saveNursingFixes(fixes);
                        }
                    }
                    if (foundWoundWithGender == true) {


                        ActionErrors errors = new ActionErrors();
                        errors.add("exception", new ActionError("gender.change.wp"));
                        saveErrors(request, errors);
                        return (mapping.findForward("admin.patient.success"));
                    }
                }
            }

            // Populate treatment list
            LookupVO vo = new LookupVO();
            ListServiceImpl bd = new ListServiceImpl(language);
            ProfessionalServiceImpl profService = new ProfessionalServiceImpl();
            AssignPatientsServiceImpl aService = new AssignPatientsServiceImpl();
            if (session.getAttribute("patient_id") != null) {
                if (userVO.getAccess_assigning().equals(new Integer(1))) {
                    int patient_id = Integer.parseInt((String) session.getAttribute("patient_id"));
                    ProfessionalVO pt = new ProfessionalVO();
                    pt.setAccount_status(1);
                    request.setAttribute("professionals", profService.findAllBySearch(userVO.getId(), "", "", -1, "",null,""));
                    AssignPatientsVO atmp = new AssignPatientsVO();
                    atmp.setPatientId(patient_id);
                    request.setAttribute("professionalsa", aService.getAllAssignedPatients(atmp));
                }
                request.setAttribute("inactivate_reason", bd.getLists(LookupVO.INACTIVATE_REASON));
            }
            Vector treatmentCatsVO = (Vector) bd.getLists(vo.TREATMENT_CATEGORY, userVO, false);
            Vector treatmentVO = (Vector) bd.getLists(vo.TREATMENT_LOCATION, userVO, false);
            request.setAttribute("treatmentCats", treatmentCatsVO);
            request.setAttribute("treatmentList", treatmentVO);
            LookupVO vo2 = new LookupVO();
            ListsDAO listDAO = new ListsDAO();
            //vo2.setResourceId(new Integer(vo2.PATIENT_RESIDENCE));
            //request.setAttribute("patientResidence", listDAO.findAllByCriteria(vo2));
            vo2.setResourceId(new Integer(vo.GENDER));
            request.setAttribute("gender", listDAO.findAllByCriteria(vo2));
    }
        } catch (Exception e) {
            System.out.println("error in pa:" + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        return (mapping.findForward("admin.patient.success"));

    }
}
