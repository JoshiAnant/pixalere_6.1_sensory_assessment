/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.admin;

import com.pixalere.sso.bean.SSORoleVO;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author Jose
 */
public class SSORolesForm  extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOLocationsForm.class);
    
    private int providerId;
    private int roleId;
    
    private String name;
    private String attributeValue;
    private String description;
    private String active;
    private String accessAllpatients;
    private String accessPatients;
    private String accessUploader;
    private String accessViewer;
    private String accessAdmin;
    private String accessCreatePatient;
    private String accessAssigning;
    private String accessListdata;
    private String accessProfessionals;
    private String accessCcacReporting;
    private String accessReporting;
    private String accessAudit;
    private String accessSuperadmin;
    private String trainingFlag;
    
    public SSORoleVO getUpdatedRole(SSORoleVO vo){
        vo.setName(name);
        vo.setDescription(description);
        vo.setAttributeValue((attributeValue == null? attributeValue: attributeValue.trim()));
        
        int i_active = (active == null)? 0: 1;
        int i_accessAllpatients = (accessAllpatients == null)? 0: 1;
        int i_accessPatients = (accessPatients == null)? 0: 1;
        int i_accessUploader = (accessUploader == null)? 0: 1;
        int i_accessViewer = (accessViewer == null)? 0: 1;
        int i_accessAdmin = (accessAdmin == null)? 0: 1;
        int i_accessCreatePatient = (accessCreatePatient == null)? 0: 1;
        int i_accessAssigning = (accessAssigning == null)? 0: 1;
        int i_accessListdata = (accessListdata == null)? 0: 1;
        int i_accessProfessionals = (accessProfessionals == null)? 0: 1;
        int i_accessCcacReporting = (accessCcacReporting == null)? 0: 1;
        int i_accessReporting = (accessReporting == null)? 0: 1;
        int i_accessAudit = (accessAudit == null)? 0: 1;
        int i_accessSuperadmin = (accessSuperadmin == null)? 0: 1;
        int i_trainingFlag = (trainingFlag == null)? 0: 1;
        
        vo.setActive(i_active);
        vo.setAccessAllpatients(i_accessAllpatients);
        vo.setAccessPatients(i_accessPatients);
        vo.setAccessUploader(i_accessUploader);
        vo.setAccessViewer(i_accessViewer);
        vo.setAccessAdmin(i_accessAdmin);
        vo.setAccessCreatePatient(i_accessCreatePatient);
        vo.setAccessAssigning(i_accessAssigning);
        vo.setAccessListdata(i_accessListdata);
        vo.setAccessProfessionals(i_accessProfessionals);
        vo.setAccessCcacReporting(i_accessCcacReporting);
        vo.setAccessReporting(i_accessReporting);
        vo.setAccessAudit(i_accessAudit);
        vo.setAccessSuperadmin(i_accessSuperadmin);
        vo.setTrainingFlag(i_trainingFlag);
        return vo;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getAccessAllpatients() {
        return accessAllpatients;
    }

    public void setAccessAllpatients(String accessAllpatients) {
        this.accessAllpatients = accessAllpatients;
    }

    public String getAccessPatients() {
        return accessPatients;
    }

    public void setAccessPatients(String accessPatients) {
        this.accessPatients = accessPatients;
    }

    public String getAccessUploader() {
        return accessUploader;
    }

    public void setAccessUploader(String accessUploader) {
        this.accessUploader = accessUploader;
    }

    public String getAccessViewer() {
        return accessViewer;
    }

    public void setAccessViewer(String accessViewer) {
        this.accessViewer = accessViewer;
    }

    public String getAccessAdmin() {
        return accessAdmin;
    }

    public void setAccessAdmin(String accessAdmin) {
        this.accessAdmin = accessAdmin;
    }

    public String getAccessCreatePatient() {
        return accessCreatePatient;
    }

    public void setAccessCreatePatient(String accessCreatePatient) {
        this.accessCreatePatient = accessCreatePatient;
    }

    public String getAccessAssigning() {
        return accessAssigning;
    }

    public void setAccessAssigning(String accessAssigning) {
        this.accessAssigning = accessAssigning;
    }

    public String getAccessListdata() {
        return accessListdata;
    }

    public void setAccessListdata(String accessListdata) {
        this.accessListdata = accessListdata;
    }

    public String getAccessProfessionals() {
        return accessProfessionals;
    }

    public void setAccessProfessionals(String accessProfessionals) {
        this.accessProfessionals = accessProfessionals;
    }

    public String getAccessCcacReporting() {
        return accessCcacReporting;
    }

    public void setAccessCcacReporting(String accessCcacReporting) {
        this.accessCcacReporting = accessCcacReporting;
    }

    public String getAccessReporting() {
        return accessReporting;
    }

    public void setAccessReporting(String accessReporting) {
        this.accessReporting = accessReporting;
    }

    public String getAccessAudit() {
        return accessAudit;
    }

    public void setAccessAudit(String accessAudit) {
        this.accessAudit = accessAudit;
    }

    public String getAccessSuperadmin() {
        return accessSuperadmin;
    }

    public void setAccessSuperadmin(String accessSuperadmin) {
        this.accessSuperadmin = accessSuperadmin;
    }

    public String getTrainingFlag() {
        return trainingFlag;
    }

    public void setTrainingFlag(String trainingFlag) {
        this.trainingFlag = trainingFlag;
    }

}
