package com.pixalere.struts.admin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.ConfigurationVO;
import com.pixalere.common.ConfigurationListVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ApplicationException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.utils.Common;
public class ConfigurationAdminSetupAction
        extends Action {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConfigurationAdminSetupAction.class);
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        request.setAttribute("page", "admin");
        request.setAttribute("action", "edit");
        HttpSession session = request.getSession();
        
        ConfigurationServiceImpl manager=new ConfigurationServiceImpl();
        try{
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            Collection<ConfigurationVO> configCollection = manager.retrieveAllForAdmin(userVO.getId());
            request.setAttribute("configurations",configCollection);
        }catch (ApplicationException e){
          e.printStackTrace();
            ActionErrors errors = new ActionErrors();
            errors.add("PixalereError", new ActionError("pixalere.common.error", Common.buildException(e)));
            saveErrors(request, errors);
            request.setAttribute("exception","y");
            return (mapping.findForward("pixalere.error"));
        }
        if (request.getParameter("page") != null && request.getParameter("page").equals("patient")) {
            return (mapping.findForward("uploader.go.patient"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("woundprofiles")) {
            return (mapping.findForward("uploader.go.profile"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("viewer")) {
            return (mapping.findForward("uploader.go.viewer"));
        } else if (request.getParameter("page") != null && request.getParameter("page").equals("treatment")) {
            return (mapping.findForward("uploader.go.treatment"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.go.summary"));
        } else if(request.getParameter("page")!=null && request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.go.reporting"));
        }	else if(request.getParameter("page")!=null && request.getParameter("page").equals("admin")){
            return (mapping.findForward("uploader.go.admin"));
        } else {
            return (mapping.findForward("admin.configuration.success"));
        }
    }
    
}
