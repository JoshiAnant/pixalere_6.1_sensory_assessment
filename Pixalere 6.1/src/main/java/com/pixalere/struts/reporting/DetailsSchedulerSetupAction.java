/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.reporting;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.reporting.bean.ScheduleConfigVO;
import com.pixalere.reporting.bean.ScheduleLocationVO;
import com.pixalere.reporting.bean.ReportScheduleVO;
import com.pixalere.reporting.service.ReportSchedulerService;
import com.pixalere.utils.Common;
import com.pixalere.utils.FiscalQuarter;
import com.pixalere.utils.PDate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 * @since 6.0
 */
public class DetailsSchedulerSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DetailsSchedulerSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        ReportSchedulerService service = new ReportSchedulerService();
        ListServiceImpl listService = new ListServiceImpl(language);
        
        String schedule_id_s = request.getParameter("scheduleId") != null && !request.getParameter("scheduleId").isEmpty()
                ? request.getParameter("scheduleId")
                : (String) session.getAttribute("scheduleId");
        session.setAttribute("scheduleId", null);
        
        
        String config_id_s = request.getParameter("configId") != null && !request.getParameter("configId").isEmpty()
                ? request.getParameter("configId")
                : (String) session.getAttribute("configId") != null
                ? (String) session.getAttribute("configId")
                : "0";
        int config_id = Integer.parseInt(config_id_s);
        session.setAttribute("configId", null);
        
        if (schedule_id_s == null || schedule_id_s.isEmpty() ){
            return (mapping.findForward("uploader.rscheduler.failure"));
        }
        
        if (request.getParameter("action") != null && request.getParameter("action").equals("edit") && config_id == 0){
            return (mapping.findForward("uploader.rscheduler.failure"));
        }
        
        try {
            int schedule_id = Integer.parseInt(schedule_id_s);
            ReportScheduleVO schedule = service.getScheduleById(schedule_id);
            
            String execution_on = "";
            String start = "";
            String end = "";
            
            String start1 = "";
            String start2 = "";
            String start3 = "";
            String start4 = "";
            String start5 = "";
            String end1 = "";
            String end2 = "";
            String end3 = "";
            String end4 = "";
            String end5 = "";
            
            ScheduleConfigVO config;
            if (config_id > 0) {
                config = service.getScheduleConfigById(config_id);
                
                String formatExecute = "dd/MM/yyyy hh:mm a";
                execution_on = dateToString(config.getExecution_on(), formatExecute);
                
                String formatDates = "dd/MM/yyyy";
                
                // For custom dashboard case
                if (schedule.getReport_type().equals("report_dashboard_4quarters")){
                    List<FiscalQuarter> ranges = PDate.getUnserializedCustomDatesDashboard(config.getExtra_options());

                    for (FiscalQuarter fiscalQuarter : ranges) {
                        if (fiscalQuarter.getOrder() == 1) {
                            start1 = dateToString(fiscalQuarter.getStartDate(), formatDates);
                            end1 = dateToString(fiscalQuarter.getEndDate(), formatDates);
                        }
                        if (fiscalQuarter.getOrder() == 2) {
                            start2 = dateToString(fiscalQuarter.getStartDate(), formatDates);
                            end2 = dateToString(fiscalQuarter.getEndDate(), formatDates);
                        }
                        if (fiscalQuarter.getOrder() == 3) {
                            start3 = dateToString(fiscalQuarter.getStartDate(), formatDates);
                            end3 = dateToString(fiscalQuarter.getEndDate(), formatDates);
                        }
                        if (fiscalQuarter.getOrder() == 4) {
                            start4 = dateToString(fiscalQuarter.getStartDate(), formatDates);
                            end4 = dateToString(fiscalQuarter.getEndDate(), formatDates);
                        }
                        if (fiscalQuarter.getOrder() == 5) {
                            start5 = dateToString(fiscalQuarter.getStartDate(), formatDates);
                            end5 = dateToString(fiscalQuarter.getEndDate(), formatDates);
                        }
                    }
                } else {
                    start = dateToString(config.getStart_date(), formatDates);
                    end = dateToString(config.getEnd_date(), formatDates);
                }
                
            } else {
                config = new ScheduleConfigVO();
                config.setProcessed(0);
            }
            String title = Common.getLocalizedString("pixalere.reporting.form." + schedule.getReport_type(), locale);
            if (schedule.getCustom_title() == null || schedule.getCustom_title().trim().isEmpty()){
                schedule.setCustom_title(title);
            } else {
                title += " - " + schedule.getCustom_title();
                schedule.setCustom_title(title);
            }
            
            // Build locations names display text
            Map<Integer, LookupVO> allLocationsMap = new HashMap<Integer, LookupVO>();
            // Get all locations beforehand
            int[] reg = LookupVO.TREATMENT_LOCATION;
            List<LookupVO> locations = new ArrayList<LookupVO>();
            for (int i = 0; i < reg.length; i++) {
                // Better get all first
                locations.addAll(listService.getLists(reg[i]));
            }
            for (LookupVO lookupVO : locations) {
                allLocationsMap.put(lookupVO.getId(), lookupVO);
            }
            
            List<String> locationTitles = new ArrayList<String>();
            Collection<ScheduleLocationVO> locsCol = schedule.getLocations();
            if (locsCol != null) {
                for (ScheduleLocationVO locVO : locsCol) {
                    if (allLocationsMap.containsKey(locVO.getTreatment_location_id())){
                        LookupVO vo = allLocationsMap.get(locVO.getTreatment_location_id());
                        locationTitles.add(vo.getName(language));
                    }
                }
            }
            String locationsNames = "";
            for (Iterator<String> it = locationTitles.iterator(); it.hasNext();) {
                String string_t = it.next();
                locationsNames += string_t;
                if (it.hasNext()){
                    locationsNames += ", ";
                }
            }
            schedule.setLocationsDisplay(locationsNames);
            
            request.setAttribute("schedule", schedule);
            request.setAttribute("scheduleConfig", config);
            
            request.setAttribute("execution_on", execution_on);
            
            // For custom dashboard case
            request.setAttribute("start1", start1);
            request.setAttribute("start2", start2);
            request.setAttribute("start3", start3);
            request.setAttribute("start4", start4);
            request.setAttribute("start5", start5);
            request.setAttribute("end1", end1);
            request.setAttribute("end2", end2);
            request.setAttribute("end3", end3);
            request.setAttribute("end4", end4);
            request.setAttribute("end5", end5);
            // For other reports
            request.setAttribute("start", start);
            request.setAttribute("end", end);
            
            

        } catch (ApplicationException e) {
            log.error("An application exception has been raised in SSOLocationsSetupAction.execute(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        return (mapping.findForward("uploader.rscheduler.schedule"));
    }
    
    private String dateToString(Date date, String format){
        SimpleDateFormat sf = new SimpleDateFormat(format);
        
        try {
            if (date != null) {
                return sf.format(date);
            }
        } catch (Exception e) {
            return "";
        }
        return "";
    }
}
