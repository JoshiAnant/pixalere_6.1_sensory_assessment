package com.pixalere.struts.reporting;

import com.pixalere.utils.FiscalQuarter;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.utils.PDate;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import java.util.Date;
import java.util.List;

public class ParameterForm extends ActionForm {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportingHomeForm.class);
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        //nothing
    }
    
    public String getFullAssessment() {
        return fullAssessment;
    }
    
    public void setFullAssessment(String fullAssessment) {
        this.fullAssessment = fullAssessment;
    }
    
    public String getPrintPatientDetails() {
        return printPatientDetails;
    }
    
    public void setPrintPatientDetails(String printPatientDetails) {
        this.printPatientDetails = printPatientDetails;
    }
    
    public String getPrintPatientDetails2() {
        return printPatientDetails2;
    }
    
    public void setPrintPatientDetails2(String printPatientDetails2) {
        this.printPatientDetails2 = printPatientDetails2;
    }
    
    public String getStart_date_day() {
        return start_date_day;
    }
    
    public void setStart_date_day(String start_date_day) {
        this.start_date_day = start_date_day;
    }
    
    public String getEnd_date_day() {
        return end_date_day;
    }
    
    public void setEnd_date_day(String end_date_day) {
        this.end_date_day = end_date_day;
    }
    
    public String getStart_date_month() {
        return start_date_month;
    }
    
    public void setStart_date_month(String start_date_month) {
        this.start_date_month = start_date_month;
    }
    
    public String getStart_date_year() {
        return start_date_year;
    }
    
    public void setStart_date_year(String start_date_year) {
        this.start_date_year = start_date_year;
    }
    
    public String getEnd_date_month() {
        return end_date_month;
    }
    
    public void setEnd_date_month(String end_date_month) {
        this.end_date_month = end_date_month;
    }
    
    public String getEnd_date_year() {
        return end_date_year;
    }
    
    public void setEnd_date_year(String end_date_year) {
        this.end_date_year = end_date_year;
    }

    public String getStart_1_date_day() {
        return start_1_date_day;
    }

    public void setStart_1_date_day(String start_1_date_day) {
        this.start_1_date_day = start_1_date_day;
    }

    public String getStart_1_date_month() {
        return start_1_date_month;
    }

    public void setStart_1_date_month(String start_1_date_month) {
        this.start_1_date_month = start_1_date_month;
    }

    public String getStart_1_date_year() {
        return start_1_date_year;
    }

    public void setStart_1_date_year(String start_1_date_year) {
        this.start_1_date_year = start_1_date_year;
    }

    public String getEnd_1_date_day() {
        return end_1_date_day;
    }

    public void setEnd_1_date_day(String end_1_date_day) {
        this.end_1_date_day = end_1_date_day;
    }

    public String getEnd_1_date_month() {
        return end_1_date_month;
    }

    public void setEnd_1_date_month(String end_1_date_month) {
        this.end_1_date_month = end_1_date_month;
    }

    public String getEnd_1_date_year() {
        return end_1_date_year;
    }

    public void setEnd_1_date_year(String end_1_date_year) {
        this.end_1_date_year = end_1_date_year;
    }

    public String getStart_2_date_day() {
        return start_2_date_day;
    }

    public void setStart_2_date_day(String start_2_date_day) {
        this.start_2_date_day = start_2_date_day;
    }

    public String getStart_2_date_month() {
        return start_2_date_month;
    }

    public void setStart_2_date_month(String start_2_date_month) {
        this.start_2_date_month = start_2_date_month;
    }

    public String getStart_2_date_year() {
        return start_2_date_year;
    }

    public void setStart_2_date_year(String start_2_date_year) {
        this.start_2_date_year = start_2_date_year;
    }

    public String getEnd_2_date_day() {
        return end_2_date_day;
    }

    public void setEnd_2_date_day(String end_2_date_day) {
        this.end_2_date_day = end_2_date_day;
    }

    public String getEnd_2_date_month() {
        return end_2_date_month;
    }

    public void setEnd_2_date_month(String end_2_date_month) {
        this.end_2_date_month = end_2_date_month;
    }

    public String getEnd_2_date_year() {
        return end_2_date_year;
    }

    public void setEnd_2_date_year(String end_2_date_year) {
        this.end_2_date_year = end_2_date_year;
    }

    public String getStart_3_date_day() {
        return start_3_date_day;
    }

    public void setStart_3_date_day(String start_3_date_day) {
        this.start_3_date_day = start_3_date_day;
    }

    public String getStart_3_date_month() {
        return start_3_date_month;
    }

    public void setStart_3_date_month(String start_3_date_month) {
        this.start_3_date_month = start_3_date_month;
    }

    public String getStart_3_date_year() {
        return start_3_date_year;
    }

    public void setStart_3_date_year(String start_3_date_year) {
        this.start_3_date_year = start_3_date_year;
    }

    public String getEnd_3_date_day() {
        return end_3_date_day;
    }

    public void setEnd_3_date_day(String end_3_date_day) {
        this.end_3_date_day = end_3_date_day;
    }

    public String getEnd_3_date_month() {
        return end_3_date_month;
    }

    public void setEnd_3_date_month(String end_3_date_month) {
        this.end_3_date_month = end_3_date_month;
    }

    public String getEnd_3_date_year() {
        return end_3_date_year;
    }

    public void setEnd_3_date_year(String end_3_date_year) {
        this.end_3_date_year = end_3_date_year;
    }

    public String getStart_4_date_day() {
        return start_4_date_day;
    }

    public void setStart_4_date_day(String start_4_date_day) {
        this.start_4_date_day = start_4_date_day;
    }

    public String getStart_4_date_month() {
        return start_4_date_month;
    }

    public void setStart_4_date_month(String start_4_date_month) {
        this.start_4_date_month = start_4_date_month;
    }

    public String getStart_4_date_year() {
        return start_4_date_year;
    }

    public void setStart_4_date_year(String start_4_date_year) {
        this.start_4_date_year = start_4_date_year;
    }

    public String getEnd_4_date_day() {
        return end_4_date_day;
    }

    public void setEnd_4_date_day(String end_4_date_day) {
        this.end_4_date_day = end_4_date_day;
    }

    public String getEnd_4_date_month() {
        return end_4_date_month;
    }

    public void setEnd_4_date_month(String end_4_date_month) {
        this.end_4_date_month = end_4_date_month;
    }

    public String getEnd_4_date_year() {
        return end_4_date_year;
    }

    public void setEnd_4_date_year(String end_4_date_year) {
        this.end_4_date_year = end_4_date_year;
    }

    public String getStart_5_date_day() {
        return start_5_date_day;
    }

    public void setStart_5_date_day(String start_5_date_day) {
        this.start_5_date_day = start_5_date_day;
    }

    public String getStart_5_date_month() {
        return start_5_date_month;
    }

    public void setStart_5_date_month(String start_5_date_month) {
        this.start_5_date_month = start_5_date_month;
    }

    public String getStart_5_date_year() {
        return start_5_date_year;
    }

    public void setStart_5_date_year(String start_5_date_year) {
        this.start_5_date_year = start_5_date_year;
    }

    public String getEnd_5_date_day() {
        return end_5_date_day;
    }

    public void setEnd_5_date_day(String end_5_date_day) {
        this.end_5_date_day = end_5_date_day;
    }

    public String getEnd_5_date_month() {
        return end_5_date_month;
    }

    public void setEnd_5_date_month(String end_5_date_month) {
        this.end_5_date_month = end_5_date_month;
    }

    public String getEnd_5_date_year() {
        return end_5_date_year;
    }

    public void setEnd_5_date_year(String end_5_date_year) {
        this.end_5_date_year = end_5_date_year;
    }

    public String getGoalDays() {
        return goalDays;
    }
    
    public void setGoalDays(String goalDays) {
        this.goalDays = goalDays;
    }

    public String getReportType() {
        return reportType;
    }
    
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
    
    public String getGroupBy() {
        return groupBy;
    }
    
    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }
    
    public String getBreakdown() {
        return breakdown;
    }
    
    public void setBreakdown(String breakdown) {
        this.breakdown = breakdown;
    }
    
    public Date getStartDate(){
        PDate d=new PDate();
        try{
            if (getPrintPatientDetails2()==null) {
                return null;
            } else {
                return d.getDate(Integer.parseInt(start_date_month),Integer.parseInt(start_date_day),Integer.parseInt(start_date_year));
            }
        }catch(NumberFormatException ex){
            return null;
        }
    }
    public Date getEndDate(){
       
        try{
            if (getPrintPatientDetails2()==null) {
                return null;
            } else {
                // Add 86399 to include the full day of the enddate, and substract 3661... just because it would go over 12:00 am otherwise....
                return PDate.getDate(Integer.parseInt(end_date_month),Integer.parseInt(end_date_day),Integer.parseInt(end_date_year));
            }
        }catch(NumberFormatException ex){
            return null;
        }
    }

    public List<FiscalQuarter> getDashboardRanges() {
        List<FiscalQuarter> ranges = new ArrayList<FiscalQuarter>();
        
        FiscalQuarter qs1 = new FiscalQuarter();
        qs1.setQuarter(1);
        qs1.setOrder(1);
    	qs1.setIsBlank(true);
        try {

            Date start1 = PDate.getDate(Integer.parseInt(start_1_date_month), Integer.parseInt(start_1_date_day), Integer.parseInt(start_1_date_year));
            Date end1 = PDate.getDate(Integer.parseInt(end_1_date_month), Integer.parseInt(end_1_date_day), Integer.parseInt(end_1_date_year));

            if (start1.before(end1)) {
            	qs1.setStartDate(start1);
            	qs1.setEndDate(end1);
            	qs1.setIsBlank(false);
            }
        } catch (NumberFormatException ex) {
        }
        ranges.add(qs1);

        
        FiscalQuarter qs2 = new FiscalQuarter();
        qs2.setQuarter(2);
        qs2.setOrder(2);
    	qs2.setIsBlank(true);
        try {

            Date start2 = PDate.getDate(Integer.parseInt(start_2_date_month), Integer.parseInt(start_2_date_day), Integer.parseInt(start_2_date_year));
            Date end2 = PDate.getDate(Integer.parseInt(end_2_date_month), Integer.parseInt(end_2_date_day), Integer.parseInt(end_2_date_year));

            if (start2.before(end2)) {
            	qs2.setStartDate(start2);
            	qs2.setEndDate(end2);
            	qs2.setIsBlank(false);
            }
        } catch (NumberFormatException ex) {
        }
        ranges.add(qs2);

        
        FiscalQuarter qs3 = new FiscalQuarter();
        qs3.setQuarter(3);
        qs3.setOrder(3);
    	qs3.setIsBlank(true);
        try {

            Date start3 = PDate.getDate(Integer.parseInt(start_3_date_month), Integer.parseInt(start_3_date_day), Integer.parseInt(start_3_date_year));
            Date end3 = PDate.getDate(Integer.parseInt(end_3_date_month), Integer.parseInt(end_3_date_day), Integer.parseInt(end_3_date_year));

            if (start3.before(end3)) {
            	qs3.setStartDate(start3);
            	qs3.setEndDate(end3);
            	qs3.setIsBlank(false);
            }
        } catch (NumberFormatException ex) {
        }
        ranges.add(qs3);

        
        FiscalQuarter qs4 = new FiscalQuarter();
        qs4.setQuarter(4);
        qs4.setOrder(4);
    	qs4.setIsBlank(true);
        try {

            Date start4 = PDate.getDate(Integer.parseInt(start_4_date_month), Integer.parseInt(start_4_date_day), Integer.parseInt(start_4_date_year));
            Date end4 = PDate.getDate(Integer.parseInt(end_4_date_month), Integer.parseInt(end_4_date_day), Integer.parseInt(end_4_date_year));

            if (start4.before(end4)) {
            	qs4.setStartDate(start4);
            	qs4.setEndDate(end4);
            	qs4.setIsBlank(false);
            }
        } catch (NumberFormatException ex) {
        }
        ranges.add(qs4);

        
        FiscalQuarter qs5 = new FiscalQuarter();
        qs5.setQuarter(5);
        qs5.setOrder(5);
    	qs5.setIsBlank(true);
        try {

            Date start5 = PDate.getDate(Integer.parseInt(start_5_date_month), Integer.parseInt(start_5_date_day), Integer.parseInt(start_5_date_year));
            Date end5 = PDate.getDate(Integer.parseInt(end_5_date_month), Integer.parseInt(end_5_date_day), Integer.parseInt(end_5_date_year));

            if (start5.before(end5)) {
            	qs5.setStartDate(start5);
            	qs5.setEndDate(end5);
            	qs5.setIsBlank(false);
            }
        } catch (NumberFormatException ex) {
        }
        ranges.add(qs5);

        return ranges;
    }

    private String doc_type;
    private Integer consecutive_days;
    private String closed_wounds;
    private String fullAssessment;
    private String printPatientDetails;
    private String printPatientDetails2;
    private String applyDateRange;
    private String start_date_day;
    private String end_date_day;
    private String start_date_month;
    private String start_date_year;
    private String end_date_month;
    private String end_date_year;
    private String goalDays;
    private String goals;
    private String reportType;
    private String groupBy;
    private String breakdown;
    private String patient_id;
    private String[] treatmentLocations;
    private String includeTIP;
    private String[] etiology;
    private String[] caretypes;
    private String care_type;
    private String email;
    private String sortBy;
    private String filterStatus;
    private Integer inactive;
    private Integer display_no_records;
    private Integer display_locations;
    private String custom_title;

    // Dashboard data ranges
    private String start_1_date_day;
    private String start_1_date_month;
    private String start_1_date_year;
    private String end_1_date_day;
    private String end_1_date_month;
    private String end_1_date_year;

    private String start_2_date_day;
    private String start_2_date_month;
    private String start_2_date_year;
    private String end_2_date_day;
    private String end_2_date_month;
    private String end_2_date_year;

    private String start_3_date_day;
    private String start_3_date_month;
    private String start_3_date_year;
    private String end_3_date_day;
    private String end_3_date_month;
    private String end_3_date_year;

    private String start_4_date_day;
    private String start_4_date_month;
    private String start_4_date_year;
    private String end_4_date_day;
    private String end_4_date_month;
    private String end_4_date_year;

    private String start_5_date_day;
    private String start_5_date_month;
    private String start_5_date_year;
    private String end_5_date_day;
    private String end_5_date_month;
    private String end_5_date_year;

    public String[] getTreatmentLocations() {
        return treatmentLocations;
    }
    
    public void setTreatmentLocations(String[] treatmentLocations) {
        this.treatmentLocations = treatmentLocations;
    }
    
    public String getIncludeTIP() {
        return includeTIP;
    }
    
    public void setIncludeTIP(String includeTIP) {
        this.includeTIP = includeTIP;
    }
    
    public String[] getEtiology() {
        return etiology;
    }
    
    public void setEtiology(String[] etiology) {
        this.etiology = etiology;
    }
    
    public String getGoals() {
        return goals;
    }
    
    public void setGoals(String goals) {
        this.goals = goals;
    }
    
    public String[] getCaretypes() {
        return caretypes;
    }
    
    public void setCaretypes(String[] caretypes) {
        this.caretypes = caretypes;
    }
    
    public String getPatient_id(){
        return patient_id;
    }
    public void setPatient_id(String patient_id){
        this.patient_id=patient_id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getDisplay_locations() {
        return display_locations;
    }

    public void setDisplay_locations(Integer display_locations) {
        this.display_locations = display_locations;
    }

    public String getCustom_title() {
        return custom_title;
    }

    public void setCustom_title(String custom_title) {
        this.custom_title = custom_title;
    }

    /**
     * @return the care_type
     */
    public String getCare_type() {
        return care_type;
    }

    /**
     * @param care_type the care_type to set
     */
    public void setCare_type(String care_type) {
        this.care_type = care_type;
    }

    /**
     * @return the closed_wounds
     */
    public String getClosed_wounds() {
        return closed_wounds;
    }

    /**
     * @param closed_wounds the closed_wounds to set
     */
    public void setClosed_wounds(String closed_wounds) {
        this.closed_wounds = closed_wounds;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getFilterStatus() {
        return filterStatus;
    }

    public void setFilterStatus(String filterStatus) {
        this.filterStatus = filterStatus;
    }
/**
     * @return the consecutive_days
     */
    public Integer getConsecutive_days() {
        return consecutive_days;
    }

    /**
     * @param consecutive_days the consecutive_days to set
     */
    public void setConsecutive_days(Integer consecutive_days) {
        this.consecutive_days = consecutive_days;
    }
    /**
     * @return the inactive
     */
    public Integer getInactive() {
        return inactive;
    }

    /**
     * @param inactive the inactive to set
     */
    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public Integer getDisplay_no_records() {
        return display_no_records;
    }

    public void setDisplay_no_records(Integer display_no_records) {
        this.display_no_records = display_no_records;
    }
    /**
     * @return the doc_type
     */
    public String getDoc_type() {
        return doc_type;
    }

    /**
     * @param doc_type the doc_type to set
     */
    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }
    private String data_method;

    /**
     * @return the data_method
     */
    public String getData_method() {
        return data_method;
    }

    /**
     * @param data_method the data_method to set
     */
    public void setData_method(String data_method) {
        this.data_method = data_method;
    }
}
