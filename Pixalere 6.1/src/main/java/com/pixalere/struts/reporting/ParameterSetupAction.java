package com.pixalere.struts.reporting;

import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.utils.Common;
import java.util.*;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.reporting.bean.SimpleItemTO;
import com.pixalere.utils.Constants;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//initializes the reporting home page.
public class ParameterSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParameterSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        LookupVO vo = new LookupVO();
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);

        try {
            com.pixalere.common.service.ListServiceImpl bd = new com.pixalere.common.service.ListServiceImpl(language);
            com.pixalere.auth.bean.ProfessionalVO userVO = (com.pixalere.auth.bean.ProfessionalVO) session.getAttribute("userVO");
            GUIServiceImpl gbd = new GUIServiceImpl();
            ComponentsVO c = new ComponentsVO();
            c.setFlowchart(Constants.PATIENT_PROFILE);
            Collection<ComponentsVO> components = gbd.getAllComponents(c);
            for (ComponentsVO cc : components) {
                if (cc.getField_name().equals("wound_acquired") && cc.getHide_gui() == 1) {
                    request.setAttribute("hide_acquired", "true");
                }
            }

            if (request.getParameter("queue") != null && ((String) request.getParameter("queue")).equals("true")) {

                return (mapping.findForward("uploader.reportqueue.success"));
            } else {
                Vector treatmentVO = (Vector) bd.findAllTreatmentsWithCats(userVO);
                request.setAttribute("treatmentList", treatmentVO);

                ListsDAO listBD = new ListsDAO();
                Vector<LookupVO> etiology = new Vector<LookupVO>();

                if (Common.getConfig("allowWoundCare").equals("1")) {
                    vo.setResourceId(new Integer(vo.WOUND_GOALS));
                    Collection colGoals = listBD.findAllByCriteria(vo);
                    request.setAttribute("goalsWoundcare", colGoals);
                    vo.setResourceId(new Integer(vo.ETIOLOGY));
                    ArrayList<LookupVO> list = new ArrayList(listBD.findAllByCriteria(vo));
                    Collections.sort(list,new LookupVO(language));
                    for (LookupVO l : list) {
                        etiology.add(l);
                    }
                }

                if (Common.getConfig("allowStoma").equals("1")) {
                    vo.setResourceId(new Integer(vo.OSTOMY_ETIOLOGY));
                    ArrayList<LookupVO> list = new ArrayList(listBD.findAllByCriteria(vo));
                    Collections.sort(list,new LookupVO(language));
                    for (LookupVO l : list) {
                        etiology.add(l);
                    }
                    if (Common.getConfig("includeOstomyInOnLastAssessmentReport").equals("1")) {
                        vo.setResourceId(new Integer(vo.OSTOMY_GOALS));
                        Collection colGoals = listBD.findAllByCriteria(vo);
                        request.setAttribute("goalsOstomy", colGoals);
                    }
                }

                if (Common.getConfig("allowIncision").equals("1")) {
                    vo.setResourceId(new Integer(vo.POSTOP_ETIOLOGY));
                   ArrayList<LookupVO> list = new ArrayList(listBD.findAllByCriteria(vo));
                    Collections.sort(list,new LookupVO(language));
                    for (LookupVO l : list) {
                        etiology.add(l);
                    }
                    vo.setResourceId(new Integer(vo.INCISION_GOALS));
                    Collection colGoals = listBD.findAllByCriteria(vo);
                    request.setAttribute("goalsIncision", colGoals);
                }

                if (Common.getConfig("allowDrain").equals("1")) {
                    vo.setResourceId(new Integer(vo.DRAINS_ETIOLOGY));
                    ArrayList<LookupVO> list = new ArrayList(listBD.findAllByCriteria(vo));
                    Collections.sort(list,new LookupVO(language));
                    for (LookupVO l : list) {
                        etiology.add(l);
                    }
                    vo.setResourceId(new Integer(vo.TUBEDRAIN_GOALS));
                    Collection colGoals = listBD.findAllByCriteria(vo);
                    request.setAttribute("goalsDrain", colGoals);
                }
                if (Common.getConfig("allowSkin").equals("1")) {
                    vo.setResourceId(new Integer(vo.SKIN_ETIOLOGY));
                    ArrayList<LookupVO> list = new ArrayList(listBD.findAllByCriteria(vo));
                    Collections.sort(list,new LookupVO(language));
                    for (LookupVO l : list) {
                        etiology.add(l);
                    }
                    vo.setResourceId(new Integer(vo.SKIN_GOALS));
                    Collection colGoals = listBD.findAllByCriteria(vo);
                    request.setAttribute("goalsSkin", colGoals);
                }
                request.setAttribute("etiologyList", etiology);

                request.setAttribute("report_type", request.getParameter("report"));

                Calendar cal = Calendar.getInstance();
                int intYearPrevMonth = cal.get(Calendar.YEAR);
                int intMonthPrevMonth = cal.get(Calendar.MONTH);  // Months are 0 - 11, so in range 1 - 12 this will be previous month
                if (intMonthPrevMonth == 0) {
                    intMonthPrevMonth = 12;
                    intYearPrevMonth = intYearPrevMonth - 1;
                }
                int intLastDayPrevMonth = 31;
                if (intMonthPrevMonth == 4 || intMonthPrevMonth == 6 || intMonthPrevMonth == 9 || intMonthPrevMonth == 11) {
                    intLastDayPrevMonth = 30;
                }
                if (intMonthPrevMonth == 2) {
                    if (intYearPrevMonth % 4 == 0) {
                        intLastDayPrevMonth = 29;
                    } else {
                        intLastDayPrevMonth = 28;
                    }
                }
                
                // Month/Year selectors for Facilities
                List<SimpleItemTO> months = new ArrayList<SimpleItemTO>();
                List<SimpleItemTO> years = new ArrayList<SimpleItemTO>();

                Locale loc = Common.getLocaleFromLocaleString(locale);
                DateFormat df = new SimpleDateFormat("MMMMM", loc);
                for (int i = 0; i < 12; i++) {
                    cal.set(Calendar.MONTH, i);
                    String sMonth = df.format(cal.getTime());
                    SimpleItemTO item = new SimpleItemTO(i, sMonth);
                    months.add(item);
                }
                for (int i = cal.get(Calendar.YEAR)-10; i <= cal.get(Calendar.YEAR); i++) {
                    String sYear = String.valueOf(i);
                    SimpleItemTO item = new SimpleItemTO(i, sYear);
                    years.add(item);
                }
                
                
                request.setAttribute("prev_month_year", intYearPrevMonth);
                request.setAttribute("prev_month_month", intMonthPrevMonth);
                request.setAttribute("prev_month_last_day", intLastDayPrevMonth);
                
                request.setAttribute("months_list", months);
                request.setAttribute("years_list", years);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("failed to retrieve treatments" + e.getMessage());
        }

        return (mapping.findForward("uploader.reportparameter.success"));

    }
}
