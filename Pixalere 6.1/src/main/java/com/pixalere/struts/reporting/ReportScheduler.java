package com.pixalere.struts.reporting;

import com.pixalere.common.ApplicationException;
import com.pixalere.reporting.bean.ScheduleConfigVO;
import com.pixalere.reporting.bean.ScheduleLocationVO;
import com.pixalere.reporting.bean.ReportScheduleVO;
import com.pixalere.reporting.service.ReportSchedulerService;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 * @since 6.0
 */
public class ReportScheduler extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportScheduler.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        ReportSchedulerService service = new ReportSchedulerService();
        // Delete Scheduler
        if (request.getParameter("action") != null && request.getParameter("action").equals("delete")){
            String s_s_id = request.getParameter("scheduleId") != null
                    ? request.getParameter("scheduleId")
                    : "0";

            int schedule_id = Integer.parseInt(s_s_id);
            if (schedule_id > 0){
                // We can try to delete
                ReportScheduleVO vo = service.getScheduleById(schedule_id);
                if (vo != null){
                    // Remove associated objects
                    Collection<ScheduleLocationVO> locations = vo.getLocations();
                    Collection<ScheduleConfigVO> configs = vo.getConfigs();
                    
                    try {
                        for (Iterator<ScheduleConfigVO> it = configs.iterator(); it.hasNext();) {
                            ScheduleConfigVO configVO = it.next();
                            service.removeConfig(configVO);
                            it.remove();
                        }
                        
                        for (Iterator<ScheduleLocationVO> it = locations.iterator(); it.hasNext();) {
                            ScheduleLocationVO locationVO = it.next();
                            service.removeLocation(locationVO);
                            it.remove();
                        }
                        
                        // Delete the schedule
                        service.removeSchedule(vo);
                    } catch (ApplicationException ex) {
                        log.error("An application exception has been raised in ReportScheduler (deleting ReportScheduleVO): " + ex.toString());
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception","y");
                        request.setAttribute("exception_log",Common.buildException(ex));
                        errors.add("exception",new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                    }
                        
                }
            }
        
            return (mapping.findForward("uploader.rscheduler.success"));
        }
        
        // Save / Update Scheduler
        SchedulerForm schedulerForm = (SchedulerForm) form;
        ReportScheduleVO schedule;
        
        String s_s_id = request.getParameter("scheduleId") != null && !request.getParameter("scheduleId").isEmpty()
            ? request.getParameter("scheduleId")
            : "0";
        int schedule_id = Integer.parseInt(s_s_id);
        if (schedule_id > 0){
            schedule = service.getScheduleById(schedule_id);
        } else {
            schedule = new ReportScheduleVO();
            schedule.setCreated_on(new Date());
        }
        
        // Get the new data
        schedule = schedulerForm.getUpdatedSchedule(schedule);
        schedule.setUser_language(language);
        
        try {
            // Save the scheduler
            service.saveSchedule(schedule);
            
            // If new, reload to get id
            if (schedule_id == 0){
                // reload from db
                ReportScheduleVO sample = new ReportScheduleVO();
                sample.setCreated_on(schedule.getCreated_on());
                sample.setCustom_title(schedule.getCustom_title());
                schedule = service.getScheduleByCriteria(sample);
                schedule_id = schedule.getId();
            } else {
                // Not new - remove old locations
                Collection<ScheduleLocationVO> locations = schedule.getLocations();
                try {
                    for (Iterator<ScheduleLocationVO> it = locations.iterator(); it.hasNext();) {
                        ScheduleLocationVO locationVO = it.next();
                        service.removeLocation(locationVO);
                        it.remove();
                    }
                } catch (ApplicationException ex) {
                    log.error("An application exception has been raised in ReportScheduler (deleting ScheduleLocationVO while updating): " + ex.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception","y");
                    request.setAttribute("exception_log",Common.buildException(ex));
                    errors.add("exception",new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                }
            }
            
            // TODO: ADD MORE REPORT TYPES HERE (ETIOLOGIES)
            
            // Save new locations
            Integer[] locsIds = schedulerForm.getTreatmentLocation();
            for (Integer locId : locsIds) {
                ScheduleLocationVO locVO = new ScheduleLocationVO(locId, schedule_id);
                service.saveLocation(locVO);
            }
            
            // Store id in session (Redirecting to ReportSchedulerSetup)
            session.setAttribute("scheduleId", String.valueOf(schedule.getId()));
            
        } catch(ApplicationException e){
            log.error("An application exception has been raised in ReportScheduler.execute(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(e));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
        
        
        
        return (mapping.findForward("uploader.rscheduler.success"));
    }
    
}
