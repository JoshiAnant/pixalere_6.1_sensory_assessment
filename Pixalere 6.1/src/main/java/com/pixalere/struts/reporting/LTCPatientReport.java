/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.reporting;
import com.pixalere.utils.Common;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/**
 *
 * @author travis
 */
public class LTCPatientReport  extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LTCPatientReport.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        
        Integer language = Common.getLanguageIdFromSession(session);

        try {
            // FIXME: Why leave this here?
        } catch (Exception e) {
            e.printStackTrace();
            log.error("failed to retrieve treatments" + e.getMessage());
        }

        return (mapping.findForward("reports.ltcpatient.success"));

    }

    
}
