/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.reporting;

import com.pixalere.common.ApplicationException;
import com.pixalere.reporting.bean.ReportScheduleVO;
import com.pixalere.reporting.bean.ScheduleConfigVO;
import com.pixalere.reporting.service.ReportSchedulerService;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 * @since 6.0
 */
public class DetailsScheduler extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DetailsScheduler.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        String s_config_id = request.getParameter("configId") != null && !request.getParameter("configId").isEmpty()
                ? request.getParameter("configId")
                : "0";
        int config_id = Integer.parseInt(s_config_id);

        String s_s_id = request.getParameter("scheduleId") != null && !request.getParameter("scheduleId").isEmpty()
                ? request.getParameter("scheduleId")
                : "0";
        int schedule_id = Integer.parseInt(s_s_id);
        
        ReportSchedulerService service = new ReportSchedulerService();
        // Delete Config
        if (request.getParameter("action") != null && request.getParameter("action").equals("delete")){
            if (config_id > 0 && schedule_id > 0){
                try {
                    // We can try to delete
                    ScheduleConfigVO vo = service.getScheduleConfigById(config_id);
                    if (vo != null && vo.getReport_schedule_id() == schedule_id){
                        // Delete the schedule config
                        service.removeConfig(vo);
                    }
                } catch (ApplicationException ex) {
                    log.error("An application exception has been raised in DetailsScheduler (deleting ScheduleConfigVO): " + ex.toString());
                    ActionErrors errors = new ActionErrors();
                    request.setAttribute("exception","y");
                    request.setAttribute("exception_log",Common.buildException(ex));
                    errors.add("exception",new ActionError("pixalere.common.error"));
                    saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
                }
                
            }
        
            // Store in session current Schedule (Redirecting to )
            session.setAttribute("scheduleId", String.valueOf(schedule_id));

            return (mapping.findForward("uploader.rscheduler.delete"));
        }
        
        DetailsSchedulerForm details = (DetailsSchedulerForm) form;
        
        if (schedule_id == 0){
            return (mapping.findForward("uploader.rscheduler.failure"));
        }
        
        try {
            ScheduleConfigVO config;
            if (config_id > 0){
                config = service.getScheduleConfigById(config_id);
            } else {
                config = new ScheduleConfigVO();
                config.setReport_schedule_id(schedule_id);
                config.setProcessed(0);
                config.setCreated_on(new Date());
            }
            
            if (config.getReport_schedule_id() != schedule_id){
                return (mapping.findForward("uploader.rscheduler.failure"));
            }
            
            // Add new data / Update
            // Regex for validating timestamp -> execution on
            String regex_pattern_timestamp = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20|21)\\d\\d) (0?[1-9]|[01][0-2]):([0-5][0-9]) ([AP]M)";
            Pattern pattern_timestamp = Pattern.compile(regex_pattern_timestamp);

            boolean validDate = true;
            String s_execution_on = details.getExecution_on();
            if (s_execution_on != null && !s_execution_on.isEmpty()){
                Matcher matcher = pattern_timestamp.matcher(s_execution_on);
                if (matcher.matches()) {
                    String formatExecuteon = "dd/MM/yyyy hh:mm a";
                    SimpleDateFormat sdf = new SimpleDateFormat(formatExecuteon);
                    try {
                        Date date = sdf.parse(s_execution_on);
                        config.setExecution_on(date);
                    } catch (ParseException ex) {
                        validDate = false;
                    }
                }
            }

            if (validDate){
                // Transform relevant dates according to report type
                ReportScheduleVO schedule = service.getScheduleById(schedule_id);
                
                if (schedule.getReport_type().equals("report_dashboard_4quarters")){
                    String ranges = PDate.getSerializedCustomDatesDashboard(details);
                    config.setExtra_options(ranges);
                } else {
                    config.setStart_date(stringToDate(details.getStart(), "dd/MM/yyyy"));
                    config.setEnd_date(stringToDate(details.getEnd(), "dd/MM/yyyy"));
                }
                
                service.saveConfig(config);
                
                // Are we saving a new config? Then reload
                if (config_id == 0){
                    ScheduleConfigVO sample = new ScheduleConfigVO();
                    sample.setCreated_on(config.getCreated_on());
                    sample.setExecution_on(config.getExecution_on());
                    config = service.getScheduleConfigByCriteria(sample);
                    config_id = config.getId();
                }
                
                session.setAttribute("configId", String.valueOf(config.getId()));
                session.setAttribute("scheduleId", String.valueOf(schedule.getId()));
                
                return (mapping.findForward("uploader.rscheduler.success"));
            } else {
                // Not a valid execution date, go back to Schedule setup
                return (mapping.findForward("uploader.rscheduler.failure"));
            }
            
        } catch (ApplicationException ex) {
            log.error("An application exception has been raised in ReportScheduler (deleting ScheduleLocationVO while updating): " + ex.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception","y");
            request.setAttribute("exception_log",Common.buildException(ex));
            errors.add("exception",new ActionError("pixalere.common.error"));
            saveErrors(request, errors);return (mapping.findForward("pixalere.error"));
        }
    }
    
    private Date stringToDate(String date, String format){
        SimpleDateFormat sf = new SimpleDateFormat(format);
        
        try {
            if (date != null) {
                return sf.parse(date);
            }
        } catch (ParseException e) {
            return null;
        }
        return null;
    }
    
}
