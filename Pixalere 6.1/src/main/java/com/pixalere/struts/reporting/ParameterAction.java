package com.pixalere.struts.reporting;

import com.pixalere.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.reporting.ReportingManager;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.bean.*;
import com.pixalere.common.ApplicationException;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.reporting.service.DashboardReportService;
import com.pixalere.reporting.service.ExportDataSNService;
import com.pixalere.utils.Common;
import com.pixalere.utils.FiscalQuarter;
import com.pixalere.utils.PDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class ParameterAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParameterAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ParameterForm parameterForm = (ParameterForm) form;
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        try {
            if(session.getAttribute("userVO")==null){
                request.setAttribute("session_invalid", "yes");
                return (mapping.findForward("system.logoff"));
            }
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            ReportingServiceImpl rservice = new ReportingServiceImpl();
            Date start_date = parameterForm.getStartDate();
            Date end_date = parameterForm.getEndDate();
            String report_type = parameterForm.getReportType();
            
            if ( (!report_type.equals("report_dashboard") && !report_type.equals("report_dashboard_4quarters")) && 
                    (end_date==null || start_date.after(end_date))) {
                start_date = null; // Because we're checking on start_date
            }
            Integer inactive= parameterForm.getInactive();
            String closed_wounds = parameterForm.getClosed_wounds();
            String groupby = parameterForm.getGroupBy();
            String breakdown = parameterForm.getBreakdown();
            String full_assessment = parameterForm.getFullAssessment();
            String goal_days = parameterForm.getGoalDays();
            String goals = parameterForm.getGoals();
            String print_patient_details = parameterForm.getPrintPatientDetails();
            String apply_date_range = parameterForm.getPrintPatientDetails2();
            String[] treatment_location = parameterForm.getTreatmentLocations();
            String includeTIP = parameterForm.getIncludeTIP();
            String[] etiology = parameterForm.getEtiology();
            String[] caretypes = parameterForm.getCaretypes();
            String patient_id = parameterForm.getPatient_id();

            if (print_patient_details == null) {
                print_patient_details = "0";
            }
            if (includeTIP == null) {
                includeTIP = "0";
            }
            if (treatment_location == null) {
                treatment_location = new String[1];
                treatment_location[0] = "";
            }

            // Flag email/window for Dashboards
            // Dashboard
            boolean dashboard_by_email = false;
            // Dashboard 4 quarters
            boolean dashboard4_by_email = true;

//System.out.println("ParameterAction.java: Call ReportingManager");
//System.out.println("ParameterAction.java: start_date="+start_date);
//System.out.println("ParameterAction.java: end_date="+end_date);
//System.out.println("ParameterAction.java: goal_days="+goal_days);
//System.out.println("ParameterAction.java: groupby="+groupby);
//System.out.println("ParameterAction.java: breakdown="+breakdown);
//System.out.println("ParameterAction.java: report_type="+report_type);
//System.out.println("ParameterAction.java: full_assessment="+full_assessment);
//System.out.println("ParameterAction.java: print_patient_details="+print_patient_details);
//System.out.println("ParameterAction.java: apply_date_range="+apply_date_range);
//System.out.println("ParameterAction.java: treatment_location="+treatment_location);
//System.out.println("ParameterAction.java: includeTIP="+includeTIP);
//System.out.println("ParameterAction.java: patient_id="+patient_id);
//System.out.println("ParameterAction.java: goals="+goals);
//System.out.println("ParameterAction.java: caretypes="+caretypes);
            try {
                //ReportingManager manager = new ReportingManager(userVO, start_date, end_date, groupby, breakdown, report_type, full_assessment, goal_days, print_patient_details, treatment_location, includeTIP, etiology, goals, caretypes, patient_id,true);
                if (
                        report_type.equals("report_patient_outcomes") || 
                        (report_type.equals("report_dashboard") && dashboard_by_email) ||
                        report_type.equals("report_wound_count") || 
                        report_type.equals("report_overview_active_patients") || 
                        report_type.equals("report_referrals_and_recommendations") || 
                        report_type.equals("report_healtime_and_cost_per_etiology") ||
                        (report_type.equals("report_dashboard_4quarters") && (dashboard4_by_email))) {

                    ReportQueueVO queue = new ReportQueueVO();
                    queue.setReport_type(report_type);
                    queue.setEnd_date(end_date);
                    queue.setStart_date(start_date);
                    queue.setCreated_on(new Date());
                    queue.setEmail(parameterForm.getEmail());
                    queue.setDisplay_locations(parameterForm.getDisplay_locations());
                    queue.setCustom_title(parameterForm.getCustom_title());
                    
                    // Saving the 5 custom intervals in the ExtraFilter field
                    String datesColumns = PDate.getSerializedCustomDatesDashboard(parameterForm);
                    queue.setExtra_filter(datesColumns);
                    
                    queue.setActive(1);
                    queue.setUser_language(language);
                    rservice.saveReportQueue(queue);
                    
                    ReportQueueVO q = rservice.getReportQueue(queue);
                    if (q != null) {
                         if(report_type.equals("report_overview_active_patients")){
                            List<UserAccountRegionsVO> treats = new ArrayList();
                            for(UserAccountRegionsVO l : userVO.getRegions()){
                                if(!l.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)){
                                    treats.add(l);
                                }
                            }
                            treatment_location = new String[treats.size()];
                            int x = 0;
                          
                            for(UserAccountRegionsVO l : treats){

                                    treatment_location[x] = l.getTreatment_location_id()+"";
                                    x++;
                                
                            }
                        }
                        if (treatment_location != null) {
                            for (String t : treatment_location) {
                                int id = Integer.parseInt(t);
                                ReportQueueTreatmentsVO mp = new ReportQueueTreatmentsVO();
                                mp.setReport_id(q.getId());
                                mp.setTreatment_location_id(id);
                                rservice.saveReportQueueTreatment(mp);
                            }
                        }
                        if (etiology != null) {
                            for (String t : etiology) {
                                int id = Integer.parseInt(t);
                                ReportQueueEtiologyVO mp = new ReportQueueEtiologyVO();
                                mp.setReport_id(q.getId());
                                mp.setLookup_id(id);

                                rservice.saveReportQueueEtiology(mp);

                            }
                        }
                        if (caretypes != null) {
                            for (String t : caretypes) {
                                String id = t;
                                ReportQueueWoundTypeVO mp = new ReportQueueWoundTypeVO();
                                mp.setReport_id(q.getId());
                                mp.setWound_type(t);
                                rservice.saveReportQueueType(mp);

                            }
                        }
                    }
                    
                    return (mapping.findForward("uploader.go.reportingqueue"));

                } else if (report_type.equals("report_wound_count_with_dressings") ) {
                    
                    ReportBuilder rbuilder = new ReportBuilder(userVO, -1, request, response, language);
                    rbuilder.generateWoundCountsWithDressings(parameterForm.getDoc_type(),treatment_location,start_date,end_date,parameterForm.getConsecutive_days());
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed

                }else if (report_type.equals("wound_list") ) {
                    String sMonth = parameterForm.getStart_date_month().trim();
                    String sYear = parameterForm.getStart_date_year().trim();
                    int month = Integer.valueOf(sMonth);
                    int year = Integer.valueOf(sYear);
                    
                    ReportBuilder rbuilder = new ReportBuilder(userVO, -1, request, response, language);
                    rbuilder.generateWoundListReport(treatment_location, year, month);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                }  else if (report_type.equals("report_wound_acquired") ) {
                    ReportBuilder rbuilder = new ReportBuilder(userVO,Constants.WOUND_ACQUIRED_REPORT,request,response, language);
                    rbuilder.generateWoundAcquiredReport(start_date,end_date,treatment_location,etiology);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                } else if (report_type.equals("report_facility_comparison")) {
                    String sMonth = parameterForm.getStart_date_month().trim();
                    String sYear = parameterForm.getStart_date_year().trim();
                    int month = Integer.valueOf(sMonth);
                    int year = Integer.valueOf(sYear);
                    
                    ReportBuilder rbuilder = new ReportBuilder(userVO, -1, request, response, language);
                    rbuilder.generateFacilitiesComparisonReport(treatment_location, etiology, year, month,groupby);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                } else if (report_type.equals("wound_tracking_details_monthly") ) {
                    String sMonth = parameterForm.getStart_date_month().trim();
                    String sYear = parameterForm.getStart_date_year().trim();
                    int month = Integer.valueOf(sMonth);
                    int year = Integer.valueOf(sYear);
                    
                    String filterStatus = parameterForm.getFilterStatus();
                    Collection<UserAccountRegionsVO> regions = new ArrayList();
                    if (treatment_location != null) {
                        for (String location : treatment_location) {
                            int location_id = Integer.parseInt(location);
                            regions.add(new UserAccountRegionsVO(-1, 7, location_id));
                        }
                    }
                    ReportBuilder rbuilder = new ReportBuilder(userVO, -1, request, response, language);
                    rbuilder.generateWoundTrackingDetailsReport(regions, filterStatus, null, year, month);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                } else if (report_type.equals("report_wound_tracking") ) {
                    String sortBy = parameterForm.getSortBy();
                    String filterStatus = parameterForm.getFilterStatus();
                    Collection<UserAccountRegionsVO> regions = new ArrayList();
                    if (treatment_location != null) {
                        for (String location : treatment_location) {
                            int location_id = Integer.parseInt(location);
                            regions.add(new UserAccountRegionsVO(-1, 7, location_id));
                        }
                    }
                    ReportBuilder rbuilder = new ReportBuilder(userVO, -1, request, response, language);
                    rbuilder.generateWoundTrackingReport(regions, sortBy, filterStatus, null);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                }else if (report_type.equals("report_usage_stats") ) {
                    ReportBuilder rbuilder = new ReportBuilder(userVO,-1,request,response, language);
                    List<Integer> locations = new ArrayList<Integer>();
                    if (treatment_location != null) {
                        for (String location : treatment_location) {
                            int location_id = Integer.parseInt(location);
                            locations.add(location_id);
                        }
                    }
                    Integer display_no_records = parameterForm.getDisplay_no_records();
                    rbuilder.generateUsageStatsReport(start_date,end_date,inactive, locations, display_no_records);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                } else if (report_type.equals("referrals") ) {
                    ReportBuilder rbuilder = new ReportBuilder(userVO, -1, request, response, language);
                    List<Integer> locations = new ArrayList<Integer>();
                    if (treatment_location != null) {
                        for (String location : treatment_location) {
                            int location_id = Integer.parseInt(location);
                            locations.add(location_id);
                        }
                    }
                    rbuilder.generateReferralsReport(locations);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                } else if (report_type.equals("report_sn_extract")) {
                    if (userVO.getAccess_superadmin() == 1){
                        List<Integer> locations = new ArrayList<Integer>();
                        if (treatment_location != null) {
                            for (String location : treatment_location) {
                                int location_id = Integer.parseInt(location);
                                locations.add(location_id);
                            }
                        }
                        
                        ExportDataSNService dataService = new ExportDataSNService(language, request, response);
                        dataService.exportData(start_date, end_date, locations);
                    } else {
                        log.error("Unauthorized user tried to generate: " + report_type);
                        mapping.findForward("uploader.patient.failure");
                    }
                } else if (report_type.equals("report_dashboard") || report_type.equals("report_dashboard_4quarters")) {
                    // Create DashboardReportVO
                    // FIXME? This should be removed, and use directly the string arrays
                    // but maybe doing all that refactoring is not worth doing, given
                    // the planned reporting suit :/
                    Collection<DashboardByLocationVO> treatments = new ArrayList();
                    if (treatment_location != null) {
                        for (String t : treatment_location) {
                            int id_location = Integer.parseInt(t);
                            DashboardByLocationVO l = new DashboardByLocationVO();
                            l.setDashboard_report_id(-1);
                            l.setTreatment_location_id(id_location);
                            treatments.add(l);
                        }
                    }
                    Collection<DashboardByEtiologyVO> etiologies = new ArrayList();
                    if (etiology != null) {
                        for (String t : etiology) {
                            int id_etiology = Integer.parseInt(t);
                            DashboardByEtiologyVO etVo = new DashboardByEtiologyVO();
                            etVo.setDashboard_report_id(-1);
                            etVo.setEtiology_id(id_etiology);
                            etiologies.add(etVo);
                        }
                    }
                    
                    DashboardReportVO dashboardReportVO = new DashboardReportVO();
                    dashboardReportVO.setTreatmentLocations(treatments);
                    dashboardReportVO.setEtiologies(etiologies);
                    
                    // FIXME: Verify closed_wounds is the correct param
                    // Possible correct: care_type
                    if(parameterForm.getClosed_wounds()!=null){
                        dashboardReportVO.setWound_type(parameterForm.getClosed_wounds());
                    } else {
                        dashboardReportVO.setWound_type("All");
                    }
                    DashboardReportService dashboardService = new DashboardReportService(request, response, language);
                    
                    List<FiscalQuarter> quarters;
                    
                    // Old dashboard - 8 quarters - 2 years - landscape
                    if (report_type.equals("report_dashboard")) {
                        quarters = Common.getQuarters(7, start_date);
                        dashboardService.generateDashboardLive(dashboardReportVO, quarters);
                        
                    } else {
                        // Extended dashboard - 5 custom periods - portrait
                        quarters = parameterForm.getDashboardRanges();
                        
                        boolean displayLocations = parameterForm.getDisplay_locations() == null
                                ? false
                                : parameterForm.getDisplay_locations() == 1;
                        String customTitle = parameterForm.getCustom_title();
                        
                        dashboardService.generateExtendedDashboardLive(dashboardReportVO, quarters, displayLocations, customTitle);
                    }
                    
                }
                    /*else if (report_type.equals("report_patient_file") ) {
                    ReportBuilder rbuilder = new ReportBuilder(userVO,Constants.PATIENT_FILE_REPORT,request,response);
                    rbuilder.generatePatientFileReport(start_date,end_date,patient_id,"");
                } */else {
                    //legacy reporting.
                    ReportingManager manager = new ReportingManager(userVO, start_date, end_date, groupby, breakdown, report_type, full_assessment, goal_days, print_patient_details, treatment_location, includeTIP, etiology, goals, caretypes, patient_id, session.getServletContext(), language);
                    manager.callReport(request, response);
                    return null;//don't reddirect otherwise we get the error:
                    //Cannot call sendRedirect() after the response has been committed
                }
                    
            } catch (ApplicationException xe) {
            }
        } catch (Exception e) {
            log.error("An application exception has been raised in ParameterAction.perform(): " + e.toString());
            e.printStackTrace();
            mapping.findForward("uploader.patient.failure");

        }

        return (mapping.findForward("uploader.go.reporting"));

    }
}
