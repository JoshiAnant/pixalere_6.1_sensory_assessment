package com.pixalere.struts.reporting;

import com.pixalere.reporting.bean.ReportScheduleVO;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 */
public class SchedulerForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SchedulerForm.class);
    
    private String report_type;
    private String active;
    private String custom_title;
    private String use_title;
    private Integer[] treatmentLocation;
    private String display_locations;
    private String recipients_emails;
    private String enable_reminder;
    private String reminder_limit;
    private String reminder_emails;
    
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        treatmentLocation = new Integer[0];
        super.reset(mapping, request); 
    }
    
    public Integer[] getTreatmentLocation() {
        return treatmentLocation;
    }

    public void setTreatmentLocation(Integer[] treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    
    public ReportScheduleVO getUpdatedSchedule(ReportScheduleVO vo){
        // Checkboxes values
        int i_active =(active == null)? 0: 1;
        int i_use_title =(use_title == null)? 0: 1;
        int i_display_locations =(display_locations == null)? 0: 1;
        int i_enable_reminder =(enable_reminder == null)? 0: 1;

        vo.setActive(i_active);
        vo.setUse_title(i_use_title);
        vo.setDisplay_locations(i_display_locations);
        vo.setEnable_reminder(i_enable_reminder);
        
        // String values
        vo.setReport_type(report_type == null? report_type: report_type.trim());
	vo.setCustom_title(custom_title == null? custom_title: custom_title.trim());
	vo.setRecipients_emails(recipients_emails == null? recipients_emails: recipients_emails.trim());
	vo.setReminder_emails(reminder_emails == null? reminder_emails: reminder_emails.trim());
        
        // Integer value
        int i_reminder_limit = Integer.valueOf(reminder_limit);
        vo.setReminder_limit(i_reminder_limit);

        return vo;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCustom_title() {
        return custom_title;
    }

    public void setCustom_title(String custom_title) {
        this.custom_title = custom_title;
    }

    public String getUse_title() {
        return use_title;
    }

    public void setUse_title(String use_title) {
        this.use_title = use_title;
    }

    public String getDisplay_locations() {
        return display_locations;
    }

    public void setDisplay_locations(String display_locations) {
        this.display_locations = display_locations;
    }

    public String getRecipients_emails() {
        return recipients_emails;
    }

    public void setRecipients_emails(String recipients_emails) {
        this.recipients_emails = recipients_emails;
    }

    public String getEnable_reminder() {
        return enable_reminder;
    }

    public void setEnable_reminder(String enable_reminder) {
        this.enable_reminder = enable_reminder;
    }

    public String getReminder_limit() {
        return reminder_limit;
    }

    public void setReminder_limit(String reminder_limit) {
        this.reminder_limit = reminder_limit;
    }

    public String getReminder_emails() {
        return reminder_emails;
    }

    public void setReminder_emails(String reminder_emails) {
        this.reminder_emails = reminder_emails;
    }
    
    
}
