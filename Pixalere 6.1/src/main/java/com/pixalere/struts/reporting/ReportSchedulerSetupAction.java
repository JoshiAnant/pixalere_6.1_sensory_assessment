package com.pixalere.struts.reporting;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.reporting.bean.ScheduleConfigVO;
import com.pixalere.reporting.bean.ScheduleLocationVO;
import com.pixalere.reporting.bean.ReportScheduleVO;
import com.pixalere.reporting.service.ReportSchedulerService;
import com.pixalere.utils.Common;
import com.pixalere.utils.FiscalQuarter;
import com.pixalere.utils.PDate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 * @since 6.0
 */
public class ReportSchedulerSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportSchedulerSetupAction.class);
    
    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        ReportSchedulerService service = new ReportSchedulerService();
        ListServiceImpl listService = new ListServiceImpl(language);
        
        try {
            // List
            if (request.getParameter("scheduleId") == null && 
                    session.getAttribute("scheduleId") == null && 
                    request.getParameter("action") == null) {
                 // We are not editing nor adding, display the list
                List<ReportScheduleVO> schedules = service.getAllScheduled();

                Map<Integer, LookupVO> allLocationsMap = new HashMap<Integer, LookupVO>();
                // Get all locations beforehand
                if (schedules != null && !schedules.isEmpty()){
                    int[] reg = LookupVO.TREATMENT_LOCATION;
                    List<LookupVO> locations = new ArrayList<LookupVO>();
                    for (int i = 0; i < reg.length; i++) {
                        // Better get all first
                        locations.addAll(listService.getLists(reg[i]));
                    }
                    for (LookupVO lookupVO : locations) {
                        allLocationsMap.put(lookupVO.getId(), lookupVO);
                    }
                }


                for (ReportScheduleVO schedule : schedules) {
                    String title = Common.getLocalizedString("pixalere.reporting.form." + schedule.getReport_type(), locale);
                    if (schedule.getCustom_title() == null || schedule.getCustom_title().trim().isEmpty()){
                        schedule.setCustom_title(title);
                    } else {
                        title += " - " + schedule.getCustom_title();
                        schedule.setCustom_title(title);
                    }

                    // Build locations names display text
                    List<String> locationTitles = new ArrayList<String>();
                    Collection<ScheduleLocationVO> locsCol = schedule.getLocations();
                    if (locsCol != null) {
                        for (ScheduleLocationVO locVO : locsCol) {
                            if (allLocationsMap.containsKey(locVO.getTreatment_location_id())){
                                LookupVO vo = allLocationsMap.get(locVO.getTreatment_location_id());
                                locationTitles.add(vo.getName(language));
                            }
                        }
                    }
                    String locationsNames = "";
                    for (Iterator<String> it = locationTitles.iterator(); it.hasNext();) {
                        String string_t = it.next();
                        locationsNames += string_t;
                        if (it.hasNext()){
                            locationsNames += ", ";
                        }
                    }
                    
                    locationsNames = locationsNames.isEmpty()
                            ? Common.getLocalizedString("pixalere.reporting.scheduler.nolocations", locale)
                            : locationsNames;
                    schedule.setLocationsDisplay(locationsNames);

                    // Next reports for this setup, Sorted by execution date
                    List<ScheduleConfigVO> confs = new ArrayList<ScheduleConfigVO>();
                    confs.addAll(schedule.getConfigs());
                    Collections.sort(confs, new Comparator<ScheduleConfigVO>() {

                        @Override
                        public int compare(ScheduleConfigVO o1, ScheduleConfigVO o2) {
                            if (o1.getExecution_on() == null || o2.getExecution_on() == null) return 0;
                            return o1.getExecution_on().compareTo(o2.getExecution_on());
                        }
                    });
                    // Cut off already executed OR 
                    // those which execution data already passed (Corner case of 5 min old execution)
                    for (Iterator<ScheduleConfigVO> it = confs.iterator(); it.hasNext();) {
                        ScheduleConfigVO configVO = it.next();
                        if (configVO.getProcessed().equals(1) || configVO.getExecution_on().before(new Date())){
                            it.remove();
                        }
                    }

                    // Date of next programmed report
                    Date next = confs.isEmpty()? null : confs.get(0).getExecution_on();
                    String nextDisplay;
                    if (next == null) {
                        nextDisplay = Common.getLocalizedString("pixalere.reporting.scheduler.nonextconfig", locale);
                    } else {
                        String tmp = PDate.getFormattedDate(next, "yyyy-MM-dd HH:mm");
                        nextDisplay = (tmp.isEmpty())
                                ? Common.getLocalizedString("pixalere.reporting.scheduler.nonextconfig", locale)
                                : tmp;
                    }
                    schedule.setNextDateDisplay(nextDisplay);

                    // Number of programmed dates left
                    schedule.setRemainingDisplay(confs.size() + "");

                }
                request.setAttribute("schedules", schedules);
                
                
                return (mapping.findForward("uploader.rscheduler.list"));
            }
            
            ProfessionalVO userVO = (ProfessionalVO) session.getAttribute("userVO");
            
            // Adding or Editing a Schedule    
            ReportScheduleVO schedule = new ReportScheduleVO();
            String s_schedule_id = request.getParameter("scheduleId") != null
                    ? request.getParameter("scheduleId")
                    : session.getAttribute("scheduleId") != null
                    ? (String) session.getAttribute("scheduleId") 
                    : null;
            session.setAttribute("scheduleId", null);
            int schedule_id = s_schedule_id != null && !s_schedule_id.isEmpty()
                    ? Integer.parseInt(s_schedule_id)
                    : 0;

            if (schedule_id > 0){

                schedule = service.getScheduleById(schedule_id);

                // Schedule locations
                List<Integer> locationIdList = new ArrayList<Integer>();
                Collection<ScheduleLocationVO> locs = schedule.getLocations();
                for (ScheduleLocationVO locVO : locs) {
                    locationIdList.add(locVO.getTreatment_location_id());
                }    

                // General location selector
                List<LookupVO> professionalRegions = new ArrayList<LookupVO>();
                int[] reg = LookupVO.TREATMENT_LOCATION;
                for (int i = 0; i < reg.length; i++) {
                    // Faster to get all and then filter 
                    Collection<LookupVO> allLocations = listService.getLists(reg[i]);
                    for (LookupVO generalLocation : allLocations) {
                        if (locationIdList.contains(generalLocation.getId())){
                            generalLocation.setResourceId(new Integer(0));
                            generalLocation.setTitle(new Integer(0));
                            professionalRegions.add(generalLocation);
                        }
                    }
                }

                request.setAttribute("professionalRegions", professionalRegions);

                // Convert Configurations to Display Format
                int maxRanges = 0;
                Collection<ScheduleConfigVO> confs = schedule.getConfigs();
                String headerTable = "<th>" 
                            + Common.getLocalizedString("pixalere.reporting.schedule_label.table_date", locale)
                            + "</th>";
                
                // TODO: ADD MORE REPORT TYPES HERE
                
                if (schedule.getReport_type().equals("report_dashboard_4quarters")){
                    String formatColumns = "dd/MMM/yyyy";
                    String formatExecution = "yyyy/MMM/dd hh:mm a";
                    
                    for (ScheduleConfigVO confVO : confs) {
                        if (confVO.getExtra_options() != null && !confVO.getExtra_options().isEmpty()){
                            List<FiscalQuarter> quarters = PDate.getUnserializedCustomDatesDashboard(confVO.getExtra_options());
                            // Convert the quarters to a readable string
                            String display = "";
                            DateFormat df = new SimpleDateFormat(formatExecution);
                            String execution = df.format(confVO.getExecution_on());
                            display += "<td><b>" + execution + "</b></td>";
                            df = new SimpleDateFormat(formatColumns);
                            int nranges = 0;
                            for (FiscalQuarter fquarter : quarters) {
                                if (!fquarter.isIsBlank()){
                                    String range = df.format(fquarter.getStartDate()) + "-" + df.format(fquarter.getEndDate());
                                    display += "<td>" + range + "</td>";
                                } else {
                                    display += "<td> </td>";
                                }
                                nranges++;
                            }
                            maxRanges = (nranges > maxRanges)? nranges : maxRanges;
                            confVO.setDisplayText(display);
                        }
                    }
                    // Header rows
                    for (int i = 0; i < maxRanges; i++) {
                        int idx = i+1;
                        headerTable += "<th>" 
                            + Common.getLocalizedString("pixalere.reporting.schedule_dash.range" + idx, locale)
                            + "</th>";
                    }
                }
                    
                request.setAttribute("headerTable", headerTable);
            } else {
                // Init some default values new schedule
                schedule.setActive(1);
                schedule.setEnable_reminder(1);
                schedule.setReminder_limit(0);
                if (userVO.getEmail() != null && !userVO.getEmail().isEmpty()){
                    schedule.setReminder_emails(userVO.getEmail());
                }
            }
            request.setAttribute("schedule", schedule);

            // Locations selector
            Vector treatmentWithCatsVO = (Vector) listService.findAllTreatmentsWithCats(userVO);
            request.setAttribute("treatmentListWithCats", treatmentWithCatsVO);
            
            
            return (mapping.findForward("uploader.rscheduler.schedule"));
        } catch (ApplicationException e) {
            log.error("An application exception has been raised in SSOLocationsSetupAction.execute(): " + e.toString());
            ActionErrors errors = new ActionErrors();
            request.setAttribute("exception", "y");
            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
            errors.add("exception", new ActionError("pixalere.common.error"));
            saveErrors(request, errors);
            return (mapping.findForward("pixalere.error"));
        }
    }
    
}
