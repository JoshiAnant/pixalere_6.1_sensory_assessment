package com.pixalere.struts.reporting;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportingHome extends Action {

	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportingHome.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
          log.info("##$$##$$##-----PAGE = "+request.getParameter("page")+"-----##$$##$$##");
          
		if(request.getParameter("page").equals("patient")){
			return (mapping.findForward("uploader.reportinghome.patient"));
		}
		else if(request.getParameter("page").equals("woundprofiles")){
            return (mapping.findForward("uploader.reportinghome.profile"));
		}
        else if(request.getParameter("page").equals("reporting")){
            return (mapping.findForward("uploader.reportinghome.reporting"));
        }
		else if(request.getParameter("page").equals("assess")){
            return (mapping.findForward("uploader.reportinghome.assess"));
		}
        else if(request.getParameter("page").equals("treatment")){
            return (mapping.findForward("uploader.reportinghome.treatment"));
        }
        else if(request.getParameter("page").equals("viewer")){
            return (mapping.findForward("uploader.reportinghome.viewer"));
        }
        else if(request.getParameter("page").equals("summary")){
            return (mapping.findForward("uploader.reportinghome.summary"));
        }
        else if(request.getParameter("page").equals("admin")){
            log.info("############$$$$$$$$$$$$################# Page: "+request.getParameter("page"));
            return (mapping.findForward("uploader.reportinghome.admin"));
        }
        else if(request.getParameter("page").equals("metrics_report")){
            return (mapping.findForward("uploader.reporting.metrics"));
            }
		else{
                  return (mapping.findForward("uploader.reportinghome.reporting"));
		}
	}
}

