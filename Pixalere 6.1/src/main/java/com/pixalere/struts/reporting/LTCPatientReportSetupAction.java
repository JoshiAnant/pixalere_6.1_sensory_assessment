/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.struts.reporting;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.nursing.LTCPatientList;
import java.util.Collection;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.Common;
/**
 * The Long-term care report, this class performs the load of the patient list.
 * This class was meant to display a list of patients, with the patients current
 * ailments and the ability to drill down further.
 *
 * @since 6.0
 * @version 1
 * @author travis
 */
public class LTCPatientReportSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LTCPatientReportSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ProfessionalVO userVO = null;

        Integer language = Common.getLanguageIdFromSession(session);

        try {
                ReportBuilder rbuilder = new ReportBuilder(session.getServletContext());
                Date endDate= new Date();
                Date startDate = new Date(2012,1,1);
                Collection<LTCPatientList> patients = rbuilder.getLTCPatientReport(userVO, startDate, endDate);
                
                request.setAttribute("patients",patients);
        } catch (ApplicationException e) {
            e.printStackTrace();
            log.error("failed to retrieve treatments" + e.getMessage());
        }

        return (mapping.findForward("reports.ltcpatient.success"));

    }
}
