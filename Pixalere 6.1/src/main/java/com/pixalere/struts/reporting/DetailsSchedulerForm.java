package com.pixalere.struts.reporting;

import org.apache.struts.action.ActionForm;

/**
 *
 * @author Jose
 */
public class DetailsSchedulerForm extends ActionForm {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DetailsSchedulerForm.class);
    
    private String execution_on;
    // Dashboard custom form
    private String start1;
    private String end1;
    private String start2;
    private String end2;
    private String start3;
    private String end3;
    private String start4;
    private String end4;
    private String start5;
    private String end5;
    // General interval
    private String start;
    private String end;

    public String getExecution_on() {
        return execution_on;
    }

    public void setExecution_on(String execution_on) {
        this.execution_on = execution_on;
    }

    public String getStart1() {
        return start1;
    }

    public void setStart1(String start1) {
        this.start1 = start1;
    }

    public String getEnd1() {
        return end1;
    }

    public void setEnd1(String end1) {
        this.end1 = end1;
    }

    public String getStart2() {
        return start2;
    }

    public void setStart2(String start2) {
        this.start2 = start2;
    }

    public String getEnd2() {
        return end2;
    }

    public void setEnd2(String end2) {
        this.end2 = end2;
    }

    public String getStart3() {
        return start3;
    }

    public void setStart3(String start3) {
        this.start3 = start3;
    }

    public String getEnd3() {
        return end3;
    }

    public void setEnd3(String end3) {
        this.end3 = end3;
    }

    public String getStart4() {
        return start4;
    }

    public void setStart4(String start4) {
        this.start4 = start4;
    }

    public String getEnd4() {
        return end4;
    }

    public void setEnd4(String end4) {
        this.end4 = end4;
    }

    public String getStart5() {
        return start5;
    }

    public void setStart5(String start5) {
        this.start5 = start5;
    }

    public String getEnd5() {
        return end5;
    }

    public void setEnd5(String end5) {
        this.end5 = end5;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
    
    
}
