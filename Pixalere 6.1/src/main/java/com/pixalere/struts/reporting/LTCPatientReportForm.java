/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.reporting;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import com.pixalere.utils.PDate;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import java.util.Date;
public class LTCPatientReportForm extends ActionForm {
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LTCPatientReportForm.class);
    private Integer start_date_day;
    private Integer start_date_month;
    private Integer start_date_year;
    private Integer end_date_day;
    private Integer end_date_month;
    private Integer end_date_year;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        //nothing
    }

    /**
     * @return the start_date_day
     */
    public Integer getStart_date_day() {
        return start_date_day;
    }

    /**
     * @param start_date_day the start_date_day to set
     */
    public void setStart_date_day(Integer start_date_day) {
        this.start_date_day = start_date_day;
    }

    /**
     * @return the start_date_month
     */
    public Integer getStart_date_month() {
        return start_date_month;
    }

    /**
     * @param start_date_month the start_date_month to set
     */
    public void setStart_date_month(Integer start_date_month) {
        this.start_date_month = start_date_month;
    }

    /**
     * @return the start_date_year
     */
    public Integer getStart_date_year() {
        return start_date_year;
    }

    /**
     * @param start_date_year the start_date_year to set
     */
    public void setStart_date_year(Integer start_date_year) {
        this.start_date_year = start_date_year;
    }

    /**
     * @return the end_date_day
     */
    public Integer getEnd_date_day() {
        return end_date_day;
    }

    /**
     * @param end_date_day the end_date_day to set
     */
    public void setEnd_date_day(Integer end_date_day) {
        this.end_date_day = end_date_day;
    }

    /**
     * @return the end_date_month
     */
    public Integer getEnd_date_month() {
        return end_date_month;
    }

    /**
     * @param end_date_month the end_date_month to set
     */
    public void setEnd_date_month(Integer end_date_month) {
        this.end_date_month = end_date_month;
    }

    /**
     * @return the end_date_year
     */
    public Integer getEnd_date_year() {
        return end_date_year;
    }

    /**
     * @param end_date_year the end_date_year to set
     */
    public void setEnd_date_year(Integer end_date_year) {
        this.end_date_year = end_date_year;
    }
    
}
