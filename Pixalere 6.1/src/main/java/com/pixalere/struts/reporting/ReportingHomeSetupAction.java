package com.pixalere.struts.reporting;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.ApplicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.HttpSession;
public class ReportingHomeSetupAction extends Action {
//initializes the reporting home page.
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportingHomeSetupAction.class);
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
	    request.setAttribute("page", "reporting");
        HttpSession session = request.getSession();
        GUIServiceImpl cservice = new GUIServiceImpl();
        ComponentsVO comp = new ComponentsVO();
        comp.setField_name("wound_acquired");
        try{
        ComponentsVO component = cservice.getComponent(comp);
        if(component == null || component.getHide_gui() != 0){
            request.setAttribute("hide_wound_acquired","1");
        }
        }catch(ApplicationException e){
            log.error(e.toString());
        }
        return (mapping.findForward("uploader.reportinghome.success"));
	}
}
