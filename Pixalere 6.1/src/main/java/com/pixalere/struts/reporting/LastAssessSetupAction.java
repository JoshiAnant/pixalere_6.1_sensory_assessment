/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.struts.reporting;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.manager.LastAssessments;
import com.pixalere.utils.Common;

public class LastAssessSetupAction extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LastAssessSetupAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        PatientServiceImpl pservice = new PatientServiceImpl();
        ProfessionalVO prof = (ProfessionalVO)session.getAttribute("userVO");
        
        Integer language = Common.getLanguageIdFromSession(session);
        
        if(prof!=null){
            ReportBuilder rb = new ReportBuilder(language);
            List<LastAssessments> report =  rb.getLastAssessmentReport(prof,1000);
            request.setAttribute("report",report);
        }
        return mapping.findForward("uploader.lastassess.success");
    }
}
