/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.guibeans;
/**
 * This bean is used to store generic content for dropdown population within the view.
 * @author travis
 */
public class DropdownBean extends Object{
    private String value;
    private String text;
    public DropdownBean(String value,String text){
        this.value=value;
        this.text=text;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
    /**
     * @return the text
     */
    public String getText() {
        return text;
    }
    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
    
}