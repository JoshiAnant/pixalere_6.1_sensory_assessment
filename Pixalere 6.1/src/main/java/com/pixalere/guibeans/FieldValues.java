/*
 * FieldValues.java
 *
 * Created on June 13, 2007, 11:12 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.guibeans;
import java.util.Vector;
import java.util.Hashtable;
/**
 *
 * @author travismorris
 */
public class FieldValues {
    private String title;
    private String db_value;
    private String value;
    private int allow_edit;
    private String field_name;
    private int component_id;
    //only used for summary
    private int value_changed;
    private String old_value;
   
    public int getClosed() {
        return isClosed;
    }
    public void setClosed(int closed) {
        isClosed = closed;
    }
    private int isClosed;
    
    /** Creates a new instance of FieldValues */
    public FieldValues() {
        
    }
    
    public void setValue(String value){
        this.value=value;
    }
    public String getValue(){
        return value;
    }
    public void setTitle(String title){
        this.title=title;
    }
    public String getTitle(){
        return title;
    }
    public int getAllow_edit() {
        return allow_edit;
    }
    public void setAllow_edit(int allow_edit) {
        this.allow_edit = allow_edit;
    }
    public String getField_name() {
        return field_name;
    }
    public void setField_name(String field_name) {
        this.field_name = field_name;
    }
    public int getComponent_id() {
        return component_id;
    }
    public void setComponent_id(int component_id) {
        this.component_id = component_id;
    }
    public int getValue_changed() {
        return value_changed;
    }
    public void setValue_changed(int value_changed) {
        this.value_changed = value_changed;
    }
    public String getOld_value() {
        return old_value;
    }
    public void setOld_value(String old_value) {
        this.old_value = old_value;
    }
    public String getDb_value() {
        return db_value;
    }
    public void setDb_value(String db_value) {
        this.db_value = db_value;
    }
    
  
}
