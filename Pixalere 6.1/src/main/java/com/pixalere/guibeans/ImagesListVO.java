package com.pixalere.guibeans;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
public class ImagesListVO extends ValueObject implements Serializable {
    /** nullable persistent field */
    private String image;
    private String image_info;
    private String image_details;
	
	/** default constructor */
    public ImagesListVO() {
    }
    public String getImage() {
        return this.image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getImage_info() {
        return this.image_info;
    }
    public void setImage_info(String image_info) {
        this.image_info = image_info;
    }
    public String getImage_details() {
        return this.image_details;
    }
    public void setImage_details(String image_details) {
        this.image_details = image_details;
    }
}
