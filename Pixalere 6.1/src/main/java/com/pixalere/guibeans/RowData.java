/*
 * RowData.java
 *
 * Created on June 15, 2007, 1:44 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.guibeans;
import java.util.List;
/**
 *
 * @author travismorris
 */
public class RowData {
    private String header;
    private int alpha_id;
    private int wound_profile_type_id;
    private int id;
    private List<FieldValues> fields;
    private String user_signature;
    private int deleted;
    private String delete_signature;
    private String delete_reason;
    private String alphaName;
    public int isDeleted(){
        return getDeleted();
    }
    /**
     * @return the deleted
     */
    public int getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
    /**
     * Creates a new instance of RowData
     */
    public RowData() {
    }
    public int getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(int alpha_id) {
        this.alpha_id = alpha_id;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public List<FieldValues> getFields() {
        return fields;
    }
    public void setFields(List<FieldValues> fields) {
        this.fields = fields;
    }
    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public String getHeader() {
        return header;
    }
    public void setHeader(String header) {
        this.header = header;
    }
    /**
     * @return the delete_signature
     */
    public String getDelete_signature() {
        return delete_signature;
    }
    /**
     * @param delete_signature the delete_signature to set
     */
    public void setDelete_signature(String delete_signature) {
        this.delete_signature = delete_signature;
    }
    /**
     * @return the delete_reason
     */
    public String getDelete_reason() {
        return delete_reason;
    }
    /**
     * @param delete_reason the delete_reason to set
     */
    public void setDelete_reason(String delete_reason) {
        this.delete_reason = delete_reason;
    }

    /**
     * @return the wound_profile_type_id
     */
    public int getWound_profile_type_id() {
        return wound_profile_type_id;
    }

    /**
     * @param wound_profile_type_id the wound_profile_type_id to set
     */
    public void setWound_profile_type_id(int wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }

    /**
     * @return the alphaName
     */
    public String getAlphaName() {
        return alphaName;
    }

    /**
     * @param alphaName the alphaName to set
     */
    public void setAlphaName(String alphaName) {
        this.alphaName = alphaName;
    }
    
}
