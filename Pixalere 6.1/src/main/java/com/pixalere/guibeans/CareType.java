/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.guibeans;
/**
 *
 * @author travis
 */
public class CareType {
    private String profile;
    private String caretype_status;
    private String caretype;
    private String heal;
    public CareType(String profile, String caretype_status, String caretype, String heal){
        this.caretype=caretype;
        this.caretype_status=caretype_status;
        this.profile=profile;
        this.heal=heal;
    }
    /**
     * @return the profile
     */
    public String getProfile() {
        return profile;
    }
    /**
     * @param profile the profile to set
     */
    public void setProfile(String profile) {
        this.profile = profile;
    }
    /**
     * @return the caretype_status
     */
    public String getCaretype_status() {
        return caretype_status;
    }
    /**
     * @param caretype_status the caretype_status to set
     */
    public void setCaretype_status(String caretype_status) {
        this.caretype_status = caretype_status;
    }
    /**
     * @return the caretype
     */
    public String getCaretype() {
        return caretype;
    }
    /**
     * @param caretype the caretype to set
     */
    public void setCaretype(String caretype) {
        this.caretype = caretype;
    }
    /**
     * @return the heal
     */
    public String getHeal() {
        return heal;
    }
    /**
     * @param heal the heal to set
     */
    public void setHeal(String heal) {
        this.heal = heal;
    }
    
}
