package com.pixalere.guibeans;
import java.io.Serializable;
/** 
 *           wound_profiles
 *          @author Travis Morris (with help from Hibernate)
 *        
*/
public class WoundProfileItem  implements Serializable {
    /** identifier field */
    private int id;
    private String name;
    private String image_name;
	private String status;
        private int deleted;
	private boolean isNew=false;
    
    /** default constructor */
    public WoundProfileItem() {
    }
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
	public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getImage_name() {
        return this.image_name;
    }
    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }
	public boolean getIsNew(){
		return isNew;
	}
	public void setIsNew(boolean isNew){
		this.isNew=isNew;
	}
    /**
     * @return the deleted
     */
    public int getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
