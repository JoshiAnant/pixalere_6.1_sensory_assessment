/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.guibeans;
import com.pixalere.common.bean.ComponentsVO;
import java.util.List;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.guibeans.DropdownBean;
import java.util.Collection;
/**
 * A java bean for velocityDispatcher macro.  This bean will house the component
 * record use for displaying information popups, and settng field names.  The data 
 * object will contain the relevant nformation needed to set the velocity macro 
 * (html element).
 * 
 * @version 6.1
 * @since 6.1
 * @author travis
 */
public class VelocityField {
    private ComponentsVO component;
    private List<ArrayValueObject> items;
    private List<LookupVO> items_lookup;
    private Collection<LookupVO> list;
    private Collection<LookupVO> list2;
    private List<DropdownBean> items_string;
    private Integer id;
    private String text;
    private String title;
    
    public VelocityField(){}

    /**
     * @return the component
     */
    public ComponentsVO getComponent() {
        return component;
    }

    /**
     * @param component the component to set
     */
    public void setComponent(ComponentsVO component) {
        this.component = component;
    }

    /**
     * @return the items
     */
    public List<ArrayValueObject> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<ArrayValueObject> items) {
        this.items = items;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the items_lookup
     */
    public List<LookupVO> getItems_lookup() {
        return items_lookup;
    }

    /**
     * @param items_lookup the items_lookup to set
     */
    public void setItems_lookup(List<LookupVO> items_lookup) {
        this.items_lookup = items_lookup;
    }

    /**
     * @return the items_string
     */
    public List<DropdownBean> getItems_string() {
        return items_string;
    }

    /**
     * @param items_string the items_string to set
     */
    public void setItems_string(List<DropdownBean> items_string) {
        this.items_string = items_string;
    }

    /**
     * @return the list
     */
    public Collection<LookupVO> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(Collection<LookupVO> list) {
        this.list = list;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the list2
     */
    public Collection<LookupVO> getList2() {
        return list2;
    }

    /**
     * @param list2 the list2 to set
     */
    public void setList2(Collection<LookupVO> list2) {
        this.list2 = list2;
    }
    
}
