package com.pixalere.guibeans;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Vector;

/**
 *           patient_profiles
 *          @author Travis Morris (with help from Hibernate)
 *
 */
public class PatientSearchVO extends ValueObject implements Serializable {
    
    /** nullable persistent field */
    private int patient_id;
    private int dob_day=0;
    private int dob_year=0;
    private String dob_month;
    private String status;
    private Vector wounds;
    private String treatment_location;
    
    /** default constructor */
    public PatientSearchVO() {
    }
    public String getStatus() {
        return this.status;
    }
    
    /**
     * @method getTreatment_location
     * @field treatment_location - String
     * @return Returns the treatment_location.
     */
    public String getTreatment_location() {
        return treatment_location;
    }
    /**
     * @method setTreatment_location
     * @field treatment_location - String
     * @param treatment_location The treatment_location to set.
     */
    public void setTreatment_location(String treatment_location) {
        this.treatment_location = treatment_location;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Vector getWounds() {
        return this.wounds;
    }
    
    public void setWounds(Vector wounds) {
        this.wounds = wounds;
    }
    
    public int getDob_day() {
        return this.dob_day;
    }
    
    public void setDob_day(int dob_day) {
        this.dob_day = dob_day;
    }
    public int getDob_year() {
        return this.dob_year;
    }
    
    public void setDob_year(int dob_year) {
        this.dob_year = dob_year;
    }
    public String getDob_month() {
        return this.dob_month;
    }
    
    public void setDob_month(String dob_month) {
        this.dob_month = dob_month;
    }
    public int getPatient_id() {
        return this.patient_id;
    }
    
    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }
}