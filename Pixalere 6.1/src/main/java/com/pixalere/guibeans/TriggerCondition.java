/*
 * 
 */

package com.pixalere.guibeans;

/**
 * GUI bean for TriggerCondition that adds a couple fields
 * respective to the corresponding DB bean for display purposes.
 * 
 * @author Jose
 */
public class TriggerCondition {
    private Integer trigger_criteria_id;
    private Integer trigger_id;
    
    private String match_type;
    private String criteria_type;
    
    private Integer component_id;
    private String component_title;
    private String criteria;
    private String criteria_title;
    
    private String description;

    public Integer getTrigger_criteria_id() {
        return trigger_criteria_id;
    }

    public void setTrigger_criteria_id(Integer trigger_criteria_id) {
        this.trigger_criteria_id = trigger_criteria_id;
    }

    public Integer getTrigger_id() {
        return trigger_id;
    }

    public void setTrigger_id(Integer trigger_id) {
        this.trigger_id = trigger_id;
    }

    public String getMatch_type() {
        return match_type;
    }

    public void setMatch_type(String match_type) {
        this.match_type = match_type;
    }

    public String getCriteria_type() {
        return criteria_type;
    }

    public void setCriteria_type(String criteria_type) {
        this.criteria_type = criteria_type;
    }

    public Integer getComponent_id() {
        return component_id;
    }

    public void setComponent_id(Integer component_id) {
        this.component_id = component_id;
    }

    public String getComponent_title() {
        return component_title;
    }

    public void setComponent_title(String component_title) {
        this.component_title = component_title;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getCriteria_title() {
        return criteria_title;
    }

    public void setCriteria_title(String critera_title) {
        this.criteria_title = critera_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
