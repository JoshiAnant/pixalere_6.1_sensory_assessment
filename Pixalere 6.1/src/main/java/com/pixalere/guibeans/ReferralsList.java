package com.pixalere.guibeans;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import java.util.Comparator;
/**
 *          assessment_comments
 *          @author Travis Morris (with help from Hibernate)
 *
 */
public class ReferralsList extends ValueObject implements Comparator, Serializable {
    
    
    /** nullable persistent field */
    private Integer assessment_id;
    private Integer wound_profile_type_id;
    private Integer wound_id;
    private Integer patient_id;
    private String patient_name;
    private String top_priority;
    private String timestamp;
    private String location;
    private String wound_location_extended;
    
    
    public String getWound_location_extended() {
        return wound_location_extended;
    }
    
    public void setWound_location_extended(String wound_location_extended) {
        this.wound_location_extended = wound_location_extended;
    }
    public ReferralsList(){}
    /** default constructor */
    public ReferralsList(Integer wound_id,Integer wound_profile_type_id,Integer patient_id, String patient_name, String top_priority, String timestamp,  Integer assessment_id, String location,String wound_location_extended) {
        this.wound_id=wound_id;
        this.setWound_profile_type_id(wound_profile_type_id);
        this.assessment_id=assessment_id;
        this.patient_id=patient_id;
        this.patient_name=patient_name;
        this.top_priority=top_priority;
        this.timestamp=timestamp;
        this.location=location;
        this.wound_location_extended=wound_location_extended;
    }
    
    public Integer getAssessment_id(){
        return this.assessment_id;
    }
    public Integer getWound_id(){
        return this.wound_id;
    }
    public Integer getPatient_id(){
        return this.patient_id;
    }
    public String getPatient_name() {
        return this.patient_name;
    }
    public String getTop_priority() {
        return this.top_priority;
    }
    
    public String getTimestamp(){
        return timestamp;
    }
    public String getLocation(){
        return location;
    }
    
    public int compare(Object o1, Object o2) {
        ReferralsList ref1 = (ReferralsList) o1;
        ReferralsList ref2 = (ReferralsList) o2;
        
        if(!ref1.getLocation().equals(ref2.getLocation())){
            return ref1.getLocation().compareTo(ref2.getLocation());
        } else{
            return ref1.getTimestamp().compareTo(ref2.getTimestamp());
        }
        
        
    }
    
    public boolean equals(Object o)	{
        return true;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
}
