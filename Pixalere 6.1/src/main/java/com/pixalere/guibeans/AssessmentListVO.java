
package com.pixalere.guibeans;

import java.util.Vector;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * assessment_comments
 *
 * @author Travis Morris (with help from Hibernate)
 * 
*/
public class AssessmentListVO extends ValueObject implements Comparator, Serializable {

    private Vector comments = new Vector();
    /**
     * nullable persistent field
     */
    private Integer assessment_id;
    private Date timestamp;
    private String user_signature;

    /**
     * default constructor
     */
    public AssessmentListVO() {
    }

    public Integer getAssessment_id() {
        return this.assessment_id;
    }

    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Vector getComments() {
        return comments;
    }

    public void setComments(Vector comments) {
        this.comments = comments;
    }

    public String getUser_signature() {
        return user_signature;
    }

    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }

    public int compare(Object o1, Object o2) {
        AssessmentListVO ref1 = (AssessmentListVO) o1;
        AssessmentListVO ref2 = (AssessmentListVO) o2;
        if (!ref1.getTimestamp().equals(ref2.getTimestamp())) {
            if (ref2.getTimestamp().before(ref1.getTimestamp())) {
                return -1;
            } else {
                return 1;
            }
        } else {
            return ref2.getAssessment_id().compareTo(ref1.getAssessment_id());
        }

    }

    public boolean equals(Object o) {
        return true;
    }

    public String formatedDate() {

        if (timestamp != null) {
            try {
                DateFormat df = new SimpleDateFormat("HHmm'h' dd/MMM/yyyy");

                String value = df.format(timestamp);
                return value;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
