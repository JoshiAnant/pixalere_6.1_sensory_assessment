/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.guibeans.trigger;

import com.pixalere.common.bean.ProductsVO;
import java.util.Collection;
import com.pixalere.common.service.ProductsServiceImpl;
/**
 *
 * @author travis
 */
public class ProductTrigger {
    public ProductTrigger(){}
    private Collection<ProductsVO> products;
    private String wound_name;
    private Integer trigger_id;
    private String summary;
    public String getSummary(){return summary;}
    public String getWound_name(){return wound_name;}
    public Collection<ProductsVO> getProducts(){return products;}
    public void setSummary(String summary){this.summary=summary;}
    public void setWound_name(String wound_name){this.wound_name=wound_name;}
    public void setProducts(Collection<ProductsVO> products){this.products=products;}

    /**
     * @return the trigger_id
     */
    public Integer getTrigger_id() {
        return trigger_id;
    }

    /**
     * @param trigger_id the trigger_id to set
     */
    public void setTrigger_id(Integer trigger_id) {
        this.trigger_id = trigger_id;
    }
}
