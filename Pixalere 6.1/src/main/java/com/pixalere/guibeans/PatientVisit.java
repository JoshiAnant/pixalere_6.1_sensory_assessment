/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.guibeans;
import java.util.Date;
/**
 *
 * @author travdes
 */
public class PatientVisit {
    private String treatment_location;
    private int count;
    private String reason;
    private String start;
    private String end;
    private String wound;
    public PatientVisit(String treatment_location, int count,  String reason, String start, String end,String wound){
        this.count = count;
        this.treatment_location=treatment_location;
        this.reason=reason;
        this.start=start;
        this.end=end;
        this.wound=wound;
    }
    /**
     * @return the treatment_location
     */
    public String getTreatment_location() {
        return treatment_location;
    }
    /**
     * @param treatment_location the treatment_location to set
     */
    public void setTreatment_location(String treatment_location) {
        this.treatment_location = treatment_location;
    }
    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }
    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }
    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }
    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
    /**
     * @return the start
     */
    public String getStart() {
        return start;
    }
    /**
     * @param start the start to set
     */
    public void setStart(String start) {
        this.start = start;
    }
    /**
     * @return the end
     */
    public String getEnd() {
        return end;
    }
    /**
     * @param end the end to set
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * @return the wound
     */
    public String getWound() {
        return wound;
    }

    /**
     * @param wound the wound to set
     */
    public void setWound(String wound) {
        this.wound = wound;
    }
}
