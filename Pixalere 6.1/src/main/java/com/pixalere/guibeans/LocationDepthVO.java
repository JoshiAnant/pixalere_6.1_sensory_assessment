package com.pixalere.guibeans;
//  LocationDepthVO.java
//  project_v3Common
//
//  Created by Travis Morris on Wed Feb 16 2005.
//  Copyright (c) 2005 __MyCompanyName__. All rights reserved.
//
import java.io.Serializable;
import com.pixalere.common.ValueObject;
public class LocationDepthVO extends ValueObject implements Serializable {
	private Integer depth_cm;
	private Integer depth_mm;
	private Integer location_start;
	private Integer location_end;
	public LocationDepthVO(){}
	
	public void setDepth_cm(Integer depth_cm){
		this.depth_cm=depth_cm;
	}
	public Integer getDepth_cm(){
		return depth_cm;
	}
	public void setDepth_mm(Integer depth_mm){
		this.depth_mm=depth_mm;
	}
	public Integer getDepth_mm(){
		return depth_mm;
	}
	public void setLocation_start(Integer location_start){
		this.location_start=location_start;
	}
	public Integer getLocation_start(){
		return location_start;
	}
	public void setLocation_end(Integer location_end){
		this.location_end=location_end;
	}
	public Integer getLocation_end(){
		return location_end;
	}
}
