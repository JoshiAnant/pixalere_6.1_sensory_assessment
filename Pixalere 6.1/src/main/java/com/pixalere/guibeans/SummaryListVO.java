package com.pixalere.guibeans;
//  SinusTractVO.java
//  project_v3Common
//
//  Created by Travis Morris on Wed Feb 16 2005.
//  Copyright (c) 2005 __MyCompanyName__. All rights reserved.
//
public class SummaryListVO {
	private String color;
	private String value;
	private String oldvalue;

	public SummaryListVO(){}
	public SummaryListVO(String color, String value, String oldvalue){
		this.color=color;
		this.value=value;
		this.oldvalue=oldvalue;
	}
	public void setColor(String color){
		this.color=color;
	}
	public String getColor(){
		return color;
	}
	public void setValue(String value){
		this.value=value;
	}
	public String getValue(){
		return value;
	}
	public void setOldvalue(String oldvalue){
		this.oldvalue=oldvalue;
	}
	public String getOldvalue(){
		return oldvalue;
	}
}
