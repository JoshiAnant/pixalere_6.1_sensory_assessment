package com.pixalere.guibeans;
import com.pixalere.common.bean.ReferralsTrackingVO;
import java.io.Serializable;
import java.util.Date;
public class Comments implements Comparable, Serializable {
    public Comments() {
    }
    public Comments(String comments, String user_signature, String comment_type, Integer assessment_id, String woundTimestamp, ReferralsTrackingVO referral, String professional_name,  Date timestamp, Integer professional_id,String colour, Integer id, Integer deleted, String delete_reason,String delete_signature) {
        this.comments = comments;
        this.professional_name = professional_name;
        this.professional_id = professional_id;
        this.timestamp = timestamp;
        this.comment_type = comment_type;
        this.assessment_id = assessment_id;
        this.woundTimestamp = woundTimestamp;
        this.complete = complete;
        this.user_signature = user_signature;
        this.referral = referral;
        this.id = id;
        this.deleted = deleted;
        this.delete_reason = delete_reason;
        this.delete_signature=delete_signature;
        this.colour=colour;
    }
   
    public String getComments() {
        return comments;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getDelete_signature(){
        return delete_signature;
    }
    public void setDelete_signature(String delete_signature){
        this.delete_signature=delete_signature;
    }
    public Date getTimestamp() {
        return timestamp;
    }
    public String getWoundTimestamp() {
        return woundTimestamp;
    }
    public String getComment_type() {
        return comment_type;
    }
    public Integer getAssessment_id() {
        return assessment_id;
    }
    public Integer getComplete() {
        return complete;
    }
    public ReferralsTrackingVO getReferral() {
        return referral;
    }
    public void setReferral(ReferralsTrackingVO referral) {
        this.referral = referral;
    }
    public int compareTo(Object o1) {
        Comments ref1 = (Comments) o1;
        //Comments ref2 = (Comments) o2;
        if (this.getTimestamp().before(ref1.getTimestamp())) {
            return -1;
        } else if (this.getTimestamp().after(ref1.getTimestamp())){
            return 1;
        }else if(this.getTimestamp().equals(ref1.getTimestamp())){
            int comp = this.getComment_type().compareTo(ref1.getComment_type()); 
            if(comp != 0){
                return comp;
            }
        }
        return 0;

    }
    public String getProfessional_name() {
        return professional_name;
    }
    public Integer getDeleted() {
        return deleted;
    }
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public void setProfessional_name(String professional_name) {
        this.professional_name = professional_name;
    }
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public String getUser_signature() {
        return user_signature;
    }
    public boolean equals(Object o) {
        return true;
    }
    public String getDelete_reason() {
        return delete_reason;
    }
    private String delete_reason;
    private Integer id;
    private ReferralsTrackingVO referral;
    private Integer professional_id;
    private String woundTimestamp;
    private String comments;
    private boolean isFromSpecialist;
    private String delete_signature;
    private String user_signature;
    private Date timestamp;
    private String comment_type;
    private Integer assessment_id;
    private Integer complete;
    private Integer deleted;
    private String professional_name;
    private String colour;
    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }
    /**
     * @param colour the colour to set
     */
    public void setColour(String colour) {
        this.colour = colour;
    }
}