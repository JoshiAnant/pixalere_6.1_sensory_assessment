/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.guibeans;
import java.util.List;
import com.pixalere.patient.bean.PatientAccountVO;
/**
 *
 * @author travismorris
 */
public class PatientList {
    private PatientAccountVO account;
    private boolean access_permission;
    private boolean active;
    //private ArrayList<CareType> caretypes;
    private List<WoundProfileItem> wounds;
    private String delete_reason;
    private String treatment_location;
    public PatientList(PatientAccountVO account, boolean access_permission, boolean active, String treatment_location) {
        this.access_permission = access_permission;
        this.account = account;
        this.active = active;
        this.treatment_location = treatment_location;
    }
    public PatientAccountVO getAccount() {
        return account;
    }
    public void setAccount(PatientAccountVO account) {
        this.account = account;
    }
    public boolean isAccess_permission() {
        return access_permission;
    }
    public void setAccess_permission(boolean access_permission) {
        this.access_permission = access_permission;
    }
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }
    /**
     * @return the delete_reason
     */
    public String getDelete_reason() {
        return delete_reason;
    }
    /**
     * @param delete_reason the delete_reason to set
     */
    public void setDelete_reason(String delete_reason) {
        this.delete_reason = delete_reason;
    }
    /**
     * @return the caretypes
     *
    public ArrayList<CareType> getCaretypes() {
        return caretypes;
    }
  
    public void setCaretypes(ArrayList<CareType> caretypes) {
        this.caretypes = caretypes;
    }
    
     * @return the treatment_location
     */
    public String getTreatment_location() {
        return treatment_location;
    }
    /**
     * @param treatment_location the treatment_location to set
     */
    public void setTreatment_location(String treatment_location) {
        this.treatment_location = treatment_location;
    }

    /**
     * @return the wounds
     */
    public List<WoundProfileItem> getWounds() {
        return wounds;
    }

    /**
     * @param wounds the wounds to set
     */
    public void setWounds(List<WoundProfileItem> wounds) {
        this.wounds = wounds;
    }

  

    }
