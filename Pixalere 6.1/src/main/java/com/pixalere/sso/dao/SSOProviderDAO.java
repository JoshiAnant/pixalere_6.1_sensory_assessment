package com.pixalere.sso.dao;

import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import java.util.Collection;

import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.ValueObject;
import com.pixalere.sso.bean.SSOLocationVO;
import com.pixalere.sso.bean.SSOPositionVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.bean.SSORoleVO;

/**
 *  This class is responsible for handling all CRUD logic associated with the
 *  sso_providers table.
 *
 * @since 6.0
 * @author Jose
 *
 */
public class SSOProviderDAO implements DataAccessObject {

    // Create Log4j category instance for logging
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOProviderDAO.class);
    

    public SSOProviderDAO() {
        
    }

    /**
     * Deletes sso_providers records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 6.0
     */
    @Override
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.delete()***********************");
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            SSOProviderVO ssoProviderVO = (SSOProviderVO) deleteRecord;

            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(ssoProviderVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.delete(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in SSOProviderVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in SSOProviderVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving SSOProviderDAO.delete()***********************");
    }
    
    public void deletePosition(SSOPositionVO deleteRecord) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.deletePosition()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(deleteRecord);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.deletePosition(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.deletePosition()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.deletePosition(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.deletePosition()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving SSOProviderDAO.deletePosition()***********************");
    }

    public void deleteLocation(SSOLocationVO deleteRecord) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.deleteLocation()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(deleteRecord);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.deleteLocation(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.deleteLocation()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.deleteLocation(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.deleteLocation()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving SSOProviderDAO.deleteLocation()***********************");
    }
    
    public void deleteRole(SSORoleVO deleteRecord) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.deleteRole()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(deleteRecord);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.deleteRole(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.deleteRole()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.deleteRole(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.deleteRole()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving SSOProviderDAO.deleteRole()***********************");
    }
    
    /**
     * Finds a single sso_providers record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the sso_providers table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 6.0
     */
    @Override
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        SSOProviderVO ssoProviderVO = new SSOProviderVO();

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ssoProviderVO.setId(new Integer(primaryKey));

            Query query = new QueryByCriteria(ssoProviderVO);
            log.debug("SSOProviderDAO.findByPK(): " + query.toString() + " :::: Value=" + primaryKey);

            ssoProviderVO = (SSOProviderVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in SSOProviderDAO.findByPK()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Leaving SSOProviderDAO.findByPK()***********************");
        return ssoProviderVO;
    }

    /**
     * Finds a all sso_providers records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ssoProviderVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 6.0
     */
    public Collection findAllByCriteria(SSOProviderVO ssoProviderVO) throws DataAccessException {
        //log.info("***********************Entering sso_providersDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();

            Query query = new QueryByCriteria(ssoProviderVO);
            results = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in SSOProviderDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Leaving SSOProviderDAO.findByCriteria()***********************");
        return results;
    }
    
    public Collection findAllPositionsByCriteria(SSOPositionVO vo) throws DataAccessException {
        //log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(vo);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in SSOProviderDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving SSOProviderDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds an sso_providers record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ssoProviderVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @return 
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 6.0
     */
    public ValueObject findByCriteria(SSOProviderVO ssoProviderVO) throws DataAccessException {
        //log.info("***********************Entering sso_providersDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        SSOProviderVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new SSOProviderVO();

            Query query = new QueryByCriteria(ssoProviderVO);
            returnVO = (SSOProviderVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in SSOProviderDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Leaving SSOProviderDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Updates a single sso_providers record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the sso_providers table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 6.0
     */
    @Override
    public void update(ValueObject updateRecord) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.update()***********************");
        SSOProviderVO ssoProviderVO = null;
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ssoProviderVO = (SSOProviderVO) updateRecord;

            broker.beginTransaction();
            broker.store(ssoProviderVO);
            broker.commitTransaction();
            
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in SSOProviderDAO.update()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.update(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in SSOProviderDAO.update()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Entering SSOProviderDAO.update()***********************");
    }
    
    public void updatePosition(SSOPositionVO vo) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.update()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(vo);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.updatePosition(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in SSOProviderDAO.updatePosition()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.updatePosition(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in SSOProviderDAO.updatePosition()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Entering SSOProviderDAO.updatePosition()***********************");
    }
    
    public void updateLocation(SSOLocationVO vo) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.update()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(vo);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.updateLocation(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in SSOProviderDAO.updateLocation()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.updateLocation(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in SSOProviderDAO.updateLocation()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Entering SSOProviderDAO.updateLocation()***********************");
    }
    
    public void updateRole(SSORoleVO vo) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.update()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(vo);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.updateRole(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in SSOProviderDAO.updateRole()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in SSOProviderDAO.updateRole(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in SSOProviderDAO.updateRole()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Entering SSOProviderDAO.updateRole()***********************");
    }
    
    public ValueObject findSSOLocationByCriteria(SSOLocationVO locationVO) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.findSSOLocationByCriteria()***********************");
        PersistenceBroker broker = null;
        SSOLocationVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new SSOLocationVO();
            Query query = new QueryByCriteria(locationVO);
            returnVO = (SSOLocationVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.findSSOLocationByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in SSOProviderDAO.findSSOLocationByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving SSOProviderDAO.findSSOLocationByCriteria()***********************");
        return returnVO;
    }
    
    public ValueObject findSSORoleByCriteria(SSORoleVO roleVO) throws DataAccessException {
        //log.info("***********************Entering SSOProviderDAO.findSSORoleByCriteria()***********************");
        PersistenceBroker broker = null;
        SSORoleVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new SSORoleVO();
            Query query = new QueryByCriteria(roleVO);
            returnVO = (SSORoleVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in SSOProviderDAO.findSSORoleByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in SSOProviderDAO.findSSORoleByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving SSOProviderDAO.findSSORoleByCriteria()***********************");
        return returnVO;
    }
}
