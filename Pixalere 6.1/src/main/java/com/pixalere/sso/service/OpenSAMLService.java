/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.sso.service;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.auth.dao.UserDAO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.sso.bean.SSOAttribute;
import com.pixalere.sso.bean.SSOLocationVO;
import com.pixalere.sso.bean.SSOPositionVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.bean.SSORoleVO;
import com.pixalere.sso.dao.SSOProviderDAO;
import com.pixalere.sso.endpoint.SSOConsumerArtifact;
import com.pixalere.sso.endpoint.SSOConsumerPost;
import com.pixalere.sso.endpoint.SSOLogout;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.utils.MD5;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.joda.time.DateTime;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.binding.decoding.HTTPPostDecoder;
import org.opensaml.saml2.core.*;
import org.opensaml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.NameIDFormat;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.saml2.metadata.SingleLogoutService;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.util.XMLHelper;
import org.opensaml.xml.validation.ValidationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Services and processing related to SSO using opensaml.
 * 
 * @author Jose
 */
public class OpenSAMLService {
       // Create Log4j category instance for logging
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(OpenSAMLService.class);
       
    /**
     * Http parameter that contains the post data (assertion) encoded.
     */
    public static String POST_RELAY_STATE_PARAM = "RelayState";
    
    
    public static SSOProviderVO getCurrentSSOConfig() {
        SSOProviderVO currentSsoConfig = null;
        
        try {
            SSOProviderVO criteriaVO = new SSOProviderVO();
            SSOProviderDAO dao = new SSOProviderDAO();
            criteriaVO.setActive(1);
            Collection<SSOProviderVO> dash = (Collection<SSOProviderVO>) dao.findAllByCriteria(criteriaVO);
            if (dash != null && dash.size() > 0){
                currentSsoConfig = (new ArrayList<SSOProviderVO>(dash)).get(0);
                //System.out.println("*Loading SSO config: \n--- " + currentSsoConfig.getId() + "\n--- " + currentSsoConfig.getIdpIdentityId());
            }

        } catch (DataAccessException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        
        return currentSsoConfig;
    }
    
    /**
     * Validates that the received object has been configured completely,
     * that is, that it has all the minimum required values set, the 
     * minimum values required depend on the type of the SSO, full or partial.
     * 
     * In case of any error or lack of data, it will return a String List
     * containing the errors description.
     * 
     * Full SSO means that the users are completely managed by the remote 
     * Identity Provider, and new users will be added whenever a non-existen user
     * with all the appropriated attributes is redirected to Pixalere.
     * 
     * Partial SSO means to have the users are created in Pixalere, but delegating
     * only the the authentication to the Identity Provider.
     * 
     * @param ssopvo
     * @param locale
     * @return 
     */
    public static List<String> validateSSOConfig(SSOProviderVO ssopvo, String locale){
        List<String> errors = new ArrayList<String>();
        
        if (ssopvo.getPixIdentityId() == null || ssopvo.getPixIdentityId().isEmpty()){
            errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.pixalere_id", locale));
        }
        if (ssopvo.getIdpIdentityId() == null || ssopvo.getIdpIdentityId().isEmpty()){
            errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.provider_id", locale));
        }
        if (ssopvo.getSaml2SSOUrl() == null || ssopvo.getSaml2SSOUrl().isEmpty() || !ssopvo.getSaml2SSOUrl().startsWith("http")){
            errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.ssor_url", locale));
        }
        if (
            (ssopvo.getAttributePixauth() == null || ssopvo.getAttributePixauth().isEmpty()) || 
            (ssopvo.getAttributeValPixauth() == null || ssopvo.getAttributeValPixauth().isEmpty())
                ){
            errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.ssor_url", locale));
        }
        
        if (ssopvo.getOnlyMainField() == 0 || ssopvo.getMainField().equals("username")) {
            if (ssopvo.getAttributeUsername() == null || ssopvo.getAttributeUsername().isEmpty()){
                errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_USERNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, "", locale));
            } else {
                if ((ssopvo.getAttributeUsername().indexOf(" ") != -1) || (ssopvo.getAttributeUsername().indexOf(",") != -1)){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_USERNAME, SSOConstants.ERROR_SPACE_COMMA, SSOConstants.TYPE_NAME, ssopvo.getAttributeUsername(), locale));
                }
            }
        }
        if (ssopvo.getOnlyMainField() == 0 || ssopvo.getMainField().equals("email")) {
            if (ssopvo.getAttributeEmail() == null || ssopvo.getAttributeEmail().isEmpty()){
                errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_EMAIL, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, "", locale));
            } else {
                if ((ssopvo.getAttributeEmail().indexOf(" ") != -1) || (ssopvo.getAttributeEmail().indexOf(",") != -1)){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_EMAIL, SSOConstants.ERROR_SPACE_COMMA, SSOConstants.TYPE_NAME, ssopvo.getAttributeEmail(), locale));
                }
            }
        }
        
        // Validate if only_main is false
        if (ssopvo.getOnlyMainField() == null || ssopvo.getOnlyMainField() == 0) {

            if (ssopvo.getAttributeFirstname() == null || ssopvo.getAttributeFirstname().isEmpty()){
                errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_FIRSTNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, "", locale));
            } else {
                if ((ssopvo.getAttributeFirstname().indexOf(" ") != -1) || (ssopvo.getAttributeFirstname().indexOf(",") != -1)){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_FIRSTNAME, SSOConstants.ERROR_SPACE_COMMA, SSOConstants.TYPE_NAME, ssopvo.getAttributeFirstname(), locale));
                }
            }
            if (ssopvo.getAttributeLastname() == null || ssopvo.getAttributeLastname().isEmpty()){
                errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LASTNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, "", locale));
            } else {
                if ((ssopvo.getAttributeLastname().indexOf(" ") != -1) || (ssopvo.getAttributeLastname().indexOf(",") != -1)){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LASTNAME, SSOConstants.ERROR_SPACE_COMMA, SSOConstants.TYPE_NAME, ssopvo.getAttributeLastname(), locale));
                }
            }
            if (ssopvo.getAttributePosition() == null || ssopvo.getAttributePosition().isEmpty()){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_POSITION, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, "", locale));
            } else {
                if ((ssopvo.getAttributePosition().indexOf(" ") != -1) || (ssopvo.getAttributePosition().indexOf(",") != -1)){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_POSITION, SSOConstants.ERROR_SPACE_COMMA, SSOConstants.TYPE_NAME, ssopvo.getAttributePosition(), locale));
                }
            }
            if (ssopvo.getAttributeLocations() == null || ssopvo.getAttributeLocations().isEmpty()){
                errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LOCATION, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, "", locale));
            } else {
                if ((ssopvo.getAttributeLocations().indexOf(" ") != -1) || (ssopvo.getAttributeLocations().indexOf(",") != -1)){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LOCATION, SSOConstants.ERROR_SPACE_COMMA, SSOConstants.TYPE_NAME, ssopvo.getAttributeLocations(), locale));
                }
            }
            if (ssopvo.getAttributeRoles() == null || ssopvo.getAttributeRoles().isEmpty()){
                errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_ROLE, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, "", locale));
            } else {
                if ((ssopvo.getAttributeRoles().indexOf(" ") != -1) || (ssopvo.getAttributeRoles().indexOf(",") != -1)){
                    errors.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_ROLE, SSOConstants.ERROR_SPACE_COMMA, SSOConstants.TYPE_NAME, ssopvo.getAttributeRoles(), locale));
                }
            }

            if (ssopvo.getUseAttributeTraining() == 1){
                if (
                    (ssopvo.getAttributeNameTraining() == null || ssopvo.getAttributeNameTraining().isEmpty() ) ||
                    (ssopvo.getAttributeValueTraining() == null || ssopvo.getAttributeValueTraining().isEmpty() )
                    ) {
                    errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.training_attributes", locale));
                }
            }
            if (ssopvo.getUseAttributeReporting() == 1){
                if (
                    (ssopvo.getAttributeNameReporting() == null || ssopvo.getAttributeNameReporting().isEmpty() ) ||
                    (ssopvo.getAttributeValueReporting() == null || ssopvo.getAttributeValueReporting().isEmpty() )
                    ){
                    errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.reporting_attributes", locale));
                }
            }

            // Security Settings
            if ((ssopvo.getIsAuthRequestSigned() == 1) ||
                (ssopvo.getIsAuthRequestEncrypted() == 1) ||
                (ssopvo.getIsAuthnResponseSigned() == 1) ||
                (ssopvo.getIsAuthResponseEncrypted() == 1) ) {
                if (ssopvo.getCertFilepath() == null || ssopvo.getCertFilepath().isEmpty()) {
                    errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.certificate", locale));
                }
            }

            // TODO: Validate if Binding is Artifact

            // TODO: Validate if Logout is enabled

            if (ssopvo.getSelfConfRoles() == null){
                System.out.println("validateSSOConfig -- ssopvo.getSelfConfRoles IS NULL ******");
            }

            // Validate NO manual setting is active if we are in full SSO (all automated)
            if (Common.getConfig("fullSSOUserCreation") != null && Common.getConfig("fullSSOUserCreation").equals("1")){
                if (ssopvo.getSelfConfRoles() == 1){
                    errors.add(Common.getLocalizedString("sso.admin.validation.full_manual_roles", locale));
                }
                if (ssopvo.getSelfConfLocations() == 1){
                    errors.add(Common.getLocalizedString("sso.admin.validation.full_manual_locations", locale));
                }
                if (ssopvo.getSelfConfPositions() == 1){
                    errors.add(Common.getLocalizedString("sso.admin.validation.full_manual_positions", locale));
                }
            }

            Collection<SSOLocationVO> ssoLocations = ssopvo.getSsoLocations();
            Collection<SSORoleVO> ssoRoles = ssopvo.getSsoRoles();
            Collection<SSOPositionVO> ssoPositions = ssopvo.getSsoPositions();

            // Full SSO and Manual roles disabled - Check if we have roles
            if (
                    (Common.getConfig("fullSSOUserCreation") != null && Common.getConfig("fullSSOUserCreation").equals("1")) &&
                    ssopvo.getSelfConfRoles() == 0
                    ){
                if (ssoRoles == null || ssoRoles.isEmpty()){
                    errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.roles", locale));
                } else {
                    // Check that all have values defined and at least one is enabled
                    int count_active = 0;
                    int empty = 0;
                    for (SSORoleVO roleVO : ssoRoles) {
                        if (roleVO.getAttributeValue() == null || roleVO.getAttributeValue().isEmpty()){
                            empty++;
                        }
                        if (roleVO.getActive() == 1){
                            count_active++;
                        }
                    }
                    if (empty > 0){
                        errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.roles_some", locale));
                    }
                    if (count_active == 0){
                        errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.roles_active", locale));
                    }
                }
            }

            // Full SSO and Manual locations disabled - Check if we have locations
            if (
                    (Common.getConfig("fullSSOUserCreation") != null && Common.getConfig("fullSSOUserCreation").equals("1")) &&
                    ssopvo.getSelfConfLocations() == 0
                    ){
                if (ssoLocations == null || ssoLocations.isEmpty()){
                    errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.locations", locale));
                } else {
                    // Check that all have values defined
                    int empty = 0;
                    for (SSOLocationVO locationVO : ssoLocations) {
                        if (locationVO.getPixLocationsids() == null || locationVO.getPixLocationsids().isEmpty()){
                            empty++;
                        }
                    }
                    if (empty > 0){
                        errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.locations_some", locale));
                    }
                }
            }

            // Full SSO and Manual positions disabled - Check if we have positions
            if (
                    (Common.getConfig("fullSSOUserCreation") != null && Common.getConfig("fullSSOUserCreation").equals("1")) &&
                    ssopvo.getSelfConfPositions() == 0
                    ){
                if (ssoPositions == null || ssoPositions.isEmpty()){
                    errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.positions", locale));
                } else {
                    for (SSOPositionVO posVO : ssoPositions) {
                        if (posVO.getAttributeValue() == null || posVO.getAttributeValue().isEmpty()){
                            errors.add(Common.getLocalizedString("sso.admin.validation.error_miss.positions", locale));
                            break;
                        }
                    }
                }
            }
            
        } // END if (ssopvo.getOnlyMainField() == 0) 
        
        if ((Common.getConfig("createUsersSSO") != null && Common.getConfig("createUsersSSO").equals("1")) && 
                (ssopvo.getAllowSSOUpdate() == null || ssopvo.getAllowSSOUpdate() == 0)
                ){
            // Creation requires allowing Update!
            errors.add(Common.getLocalizedString("sso.admin.validation.creation_allow_update", locale));
        }
        
        if ((Common.getConfig("fullSSOUserCreation") != null && Common.getConfig("fullSSOUserCreation").equals("1")) && 
                (Common.getConfig("createUsersSSO") == null || Common.getConfig("createUsersSSO").equals("0"))
                ){
            // Full creation requires SSO creation!
            errors.add(Common.getLocalizedString("sso.admin.validation.full_and_creation", locale));
        }
        return errors;
    }
    
    /**
     * Evaluates whether the received SSO attributes are enough to login
     * users according to the current SSO settings, returns a list of
     * the attributes that are missing from the evaluated map of attributes.
     * 
     * @param ssoAttributes
     * @param ssopvo
     * @param locale
     * @return 
     */
    public static List<String> findMissingAttributesSSOAttributes(Map<String, SSOAttribute> ssoAttributes, SSOProviderVO ssopvo, String locale){
        List<String> missingList = new ArrayList<String>();
        
        // User name 
        if (!ssoAttributes.containsKey(ssopvo.getAttributeUsername())){
            missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_USERNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeUsername(), locale));
        } else {
            String value = ssoAttributes.get(ssopvo.getAttributeUsername()).getValue();
            if (value == null || value.isEmpty()){
                missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_USERNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
            }
        }

        // Email 
        if (!ssoAttributes.containsKey(ssopvo.getAttributeEmail())){
            missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_EMAIL, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeEmail(), locale));
        } else {
            String value = ssoAttributes.get(ssopvo.getAttributeEmail()).getValue();
            if (value == null || value.isEmpty()){
                missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_EMAIL, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
            }
        }
        
        
        // Validate when only_main is false
        if (Common.getConfig("createUsersSSO") != null && Common.getConfig("createUsersSSO").equals("1")){
            // First name
            if (!ssoAttributes.containsKey(ssopvo.getAttributeFirstname())){
                missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_FIRSTNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeFirstname(), locale));
            } else {
                String value = ssoAttributes.get(ssopvo.getAttributeFirstname()).getValue();
                if (value == null || value.isEmpty()){
                    missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_FIRSTNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                }
            }
            // Last name
            if (!ssoAttributes.containsKey(ssopvo.getAttributeLastname())){
                missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LASTNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeLastname(), locale));
            } else {
                String value = ssoAttributes.get(ssopvo.getAttributeLastname()).getValue();
                if (value == null || value.isEmpty()){
                    missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LASTNAME, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                }
            }
            
            if (ssopvo.getOnlyMainField() == null || ssopvo.getOnlyMainField() == 0) {
                // Attribute Training
                if (ssopvo.getUseAttributeTraining() != null && ssopvo.getUseAttributeTraining() == 1){
                    if (!ssoAttributes.containsKey(ssopvo.getAttributeNameTraining())){
                        missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_TRAINING, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeNameTraining(), locale));
                    } else {
                        String value = ssoAttributes.get(ssopvo.getAttributeNameTraining()).getValue();
                        if (value == null || value.isEmpty()){
                            missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_TRAINING, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                        }
                    }
                }
                // Attribute Reporting
                if (ssopvo.getUseAttributeReporting()!= null && ssopvo.getUseAttributeReporting()== 1){
                    if (!ssoAttributes.containsKey(ssopvo.getAttributeNameReporting())){
                        missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_REPORTING, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeNameReporting(), locale));
                    } else {
                        String value = ssoAttributes.get(ssopvo.getAttributeNameReporting()).getValue();
                        if (value == null || value.isEmpty()){
                            missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_REPORTING, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                        }
                    }
                }
            }
                
        }
        
        // Validate when manual positions is false
        if (ssopvo.getSelfConfPositions() == null || ssopvo.getSelfConfPositions() == 0) {
            if (!ssoAttributes.containsKey(ssopvo.getAttributePosition())){
                missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_POSITION, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributePosition(), locale));
            } else {
                if (ssoAttributes.get(ssopvo.getAttributePosition()).isSingle()){
                    String value = ssoAttributes.get(ssopvo.getAttributePosition()).getValue();
                    if (value == null || value.isEmpty()){
                        missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_POSITION, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                    }
                } else {
                    missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_POSITION, SSOConstants.ERROR_NOT_SINGLE, SSOConstants.TYPE_VALUE, "", locale));
                }
            }
        }
        
        // Validate when manual locations is false
        if (ssopvo.getSelfConfLocations() == null || ssopvo.getSelfConfLocations() == 0) {
            if (!ssoAttributes.containsKey(ssopvo.getAttributeLocations())){
                missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LOCATION, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeLocations(), locale));
            } else {
                if (ssoAttributes.get(ssopvo.getAttributeLocations()).isSingle()){
                    String value = ssoAttributes.get(ssopvo.getAttributeLocations()).getValue();
                    if (value == null || value.isEmpty()){
                        missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LOCATION, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                    }
                } else {
                    int not_empty = 0;
                    for (String value : ssoAttributes.get(ssopvo.getAttributeLocations()).getValues()) {
                        if (value != null && !value.isEmpty()){
                            not_empty++;
                        }
                    }
                    if (not_empty == 0){
                        missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_LOCATION, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                    }
                }
                
            }
        }
        
        // Validate when manual roles is false
        if (ssopvo.getSelfConfRoles()== null || ssopvo.getSelfConfRoles() == 0) {
            if (!ssoAttributes.containsKey(ssopvo.getAttributeRoles())){
                missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_ROLE, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_NAME, ssopvo.getAttributeRoles(), locale));
            } else {
                if (ssoAttributes.get(ssopvo.getAttributeRoles()).isSingle()){
                    String value = ssoAttributes.get(ssopvo.getAttributeRoles()).getValue();
                    if (value == null || value.isEmpty()){
                        missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_ROLE, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                    }
                } else {
                    int not_empty = 0;
                    for (String value : ssoAttributes.get(ssopvo.getAttributeRoles()).getValues()) {
                        if (value != null && !value.isEmpty()){
                            not_empty++;
                        }
                    }
                    if (not_empty == 0){
                        missingList.add(SSOConstants.getErrorMsg(SSOConstants.FIELD_ROLE, SSOConstants.ERROR_MISSING, SSOConstants.TYPE_VALUE, "", locale));
                    }
                }
                
            }
        }
        return missingList;
    }
    
    /**
     * Evaluates wheter a ProfessionalVO has all the required settings
     * configured and so is able to work on Pixalere, returns a list
     * containing the sections that need to be setup before the userVO
     * can be given access to the system.
     * 
     * @param userVO
     * @param ssopvo
     * @return 
     */
    public static List<String> findMissingSettingsProfessionalVOSetup(ProfessionalVO userVO, SSOProviderVO ssopvo){
        List<String> missingList = new ArrayList<String>();

        // No first or no lastname
        if (userVO.getFirstname() == null || userVO.getFirstname().isEmpty()){
            missingList.add(SSOConstants.MISSING_FNAME);
        }
        if (userVO.getLastname() == null || userVO.getLastname().isEmpty()){
            missingList.add(SSOConstants.MISSING_LNAME);
        }
        // No username
        if (userVO.getUser_name() == null || userVO.getUser_name().isEmpty()){
            missingList.add(SSOConstants.MISSING_UNAME);
        }
        // Email is required only if is the main field
        if (ssopvo.getMainField().equals("email")){
            if (userVO.getEmail() == null || userVO.getEmail().isEmpty()){
                missingList.add(SSOConstants.MISSING_EMAIL);
            }
        }
        // No position
        if (userVO.getPosition_id() == null || userVO.getPosition_id() == 0){
            missingList.add(SSOConstants.MISSING_POS);
        }
        // No locations
        if (userVO.getRegions().isEmpty()){
            missingList.add(SSOConstants.MISSING_LOC);
        }
        // No timezone
        if (userVO.getTimezone() == null || userVO.getTimezone().isEmpty()){
            missingList.add(SSOConstants.MISSING_TIMEZONE);
        }
        // No permissions
        if ( (userVO.getAccess_viewer() == null || userVO.getAccess_viewer() == 0) &&
            (userVO.getAccess_admin() == null || userVO.getAccess_admin() == 0) &&
            (userVO.getAccess_allpatients() == null || userVO.getAccess_allpatients() == 0) &&
            (userVO.getAccess_assigning() == null || userVO.getAccess_assigning() == 0) &&
            (userVO.getAccess_patients() == null || userVO.getAccess_patients() == 0) &&
            (userVO.getAccess_listdata() == null || userVO.getAccess_listdata() == 0) &&
            (userVO.getAccess_superadmin() == null || userVO.getAccess_superadmin() == 0) &&
            (userVO.getAccess_audit() == null || userVO.getAccess_audit() == 0) &&
            (userVO.getAccess_professionals() == null || userVO.getAccess_professionals() == 0) &&
            (userVO.getAccess_uploader() == null || userVO.getAccess_uploader() == 0) &&
            (userVO.getAccess_reporting() == null || userVO.getAccess_reporting() == 0) &&
            (userVO.getAccess_ccac_reporting() == null || userVO.getAccess_ccac_reporting() == 0) ){

            missingList.add(SSOConstants.MISSING_PER);
        }

        return missingList;
    }
    /**
     * Returns the full URL of the SAML Assertion consumer endpoint, used
     * for creating Authn request and for building XML binding descriptions.
     * 
     * @param baseUrl
     * @return 
     */
    public static String getAssertionConsumerUrl(String baseUrl){
        return baseUrl + SSOConsumerPost.CONSUMER_URI + ".do";
    }
    
    /**
     * Returns the full URL of the SAML Assertion consumer endpoint, used
     * for creating Authn request and for building XML binding descriptions.
     * 
     * @param baseUrl
     * @return 
     */
    public static String getArtifactConsumerUrl(String baseUrl){
        return baseUrl + SSOConsumerArtifact.CONSUMER_URI + ".do";
    }
    
    /**
     * Returns the full URL of the Logout Redirect endpoint, used
     * for creating Authn request and for building XML binding descriptions.
     * 
     * @param baseUrl
     * @return 
     */
    public static String getLogoutUrl(String baseUrl){
        return baseUrl + SSOLogout.LOGOUT_URI + ".do";
    }
    
    /**
     * Convenience method to get the base URL used for the request,
     * used for generating the correct URLs for the SSO endpoints.
     * 
     * @param request
     * @return 
     */
    public static String getBaseUrl(HttpServletRequest request){
        String url = request.getRequestURL().toString();
        String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";
        return baseURL;
    }
    
    /**
     * Converts an Assertion to a Map of SSOAttribute objects, for easier
     * processing.
     * 
     * @param assertion
     * @return 
     */
    public static Map<String, SSOAttribute> getAttributesFromAssertion(Assertion assertion){
        Map<String, SSOAttribute> map = new HashMap<String, SSOAttribute>();
        for (Attribute attribute : assertion.getAttributeStatements().get(0).getAttributes()) {
            //System.out.println("Getting attribute: " + attribute.getName());
            
            SSOAttribute ssoa = new SSOAttribute();
            ssoa.setName(attribute.getName());
            
            List<XMLObject> values = attribute.getAttributeValues();
            if (values.size() > 1) {
                List<String> svalues = new ArrayList<String>();
                for (XMLObject xmlObject : values) {
                    svalues.add(((XSString) xmlObject).getValue());
                }
                ssoa.setValues(svalues);
                ssoa.setSingle(false);
            } else if (values.size() == 1) {
                ssoa.setValue(((XSString) values.get(0)).getValue());
                ssoa.setSingle(true);
            }
            
            // Ignore empty attributes
            if (values.size() >= 1){
                map.put(attribute.getName(), ssoa);
            }
        }
        return map;
    }
    
    /**
     * Convenience method to find out if a specified attribute-value
     * combination exists in a map of sso attributes.
     * 
     * @param attributeName
     * @param attributeValue
     * @param attributesMap
     * @return 
     */
    public static boolean ssoAttributeValueExists(String attributeName, String attributeValue, Map<String, SSOAttribute> attributesMap){
        boolean result = false;
        if (attributesMap.containsKey(attributeName)){
            SSOAttribute ssoa = attributesMap.get(attributeName);
            if (ssoa.isSingle()){
                result = attributeValue.equals(ssoa.getValue());
            } else {
                result = ssoa.getValues().contains(attributeValue);
            }
        }
        return result;
    }
    
    public static String generateMetadataXML(SSOProviderVO ssopvo, String urlConsumer){
        String xml = "";
        
        EntityDescriptor entityDescriptor = OpenSAMLUtils.buildSAMLObject(EntityDescriptor.class);
        entityDescriptor.setEntityID(ssopvo.getPixIdentityId());
        
        // SSO Descriptor
        SPSSODescriptor spSSODescriptor = OpenSAMLUtils.buildSAMLObject(SPSSODescriptor.class);
        spSSODescriptor.setWantAssertionsSigned(ssopvo.getIsAuthResponseEncrypted() == 1); 
        spSSODescriptor.setAuthnRequestsSigned(ssopvo.getIsAuthnResponseSigned() == 1);
        
        // Request transient pseudonym
        NameIDFormat nameIDFormat = OpenSAMLUtils.buildSAMLObject(NameIDFormat.class);
        nameIDFormat.setFormat(NameIDType.PERSISTENT);
        spSSODescriptor.getNameIDFormats().add(nameIDFormat);
        
        // AssertionConsumerService
        AssertionConsumerService assertionConsumerService = OpenSAMLUtils.buildSAMLObject(AssertionConsumerService.class);
        assertionConsumerService.setIndex(0);
        assertionConsumerService.setBinding(SAMLConstants.SAML2_POST_BINDING_URI);
        assertionConsumerService.setLocation(urlConsumer);
        spSSODescriptor.getAssertionConsumerServices().add(assertionConsumerService);
        
        // Note: Logout service if is added later, same for Artifact 
        //SingleLogoutService singleLogoutService = OpenSAMLUtils.buildSAMLObject(SingleLogoutService.class);
        //singleLogoutService.setLocation(urlLogout);
        
        // Set protocol, add sso descriptor to entity
        spSSODescriptor.addSupportedProtocol(SAMLConstants.SAML20P_NS);
        entityDescriptor.getRoleDescriptors().add(spSSODescriptor);
 
        // Convert to XML
        DocumentBuilder builder;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Marshaller out = Configuration.getMarshallerFactory().getMarshaller(entityDescriptor);
            out.marshall(entityDescriptor, document);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StreamResult streamResult = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(document);
            transformer.transform(source, streamResult);
            xml = streamResult.getWriter().toString();
            
        } catch (ParserConfigurationException e) {
            log.error(e.getMessage(), e);
        } catch (MarshallingException e) {
            log.error(e.getMessage(), e);
        } catch (TransformerException e) {
            log.error(e.getMessage(), e);
        }
        
        return xml;
    }
    
    /**
     * Validates the SSO attributes in the map for completion/correction
     * in order to meet the minimum required to create a valid Pixalere 
     * ProfessionalUserVO according to the current SSOProviderVO
     * object; if any parameter or condition is missing adds a 
     * String error describing the nature of the error to the return List.
     * 
     * Checks for valid roles, valid locations tags, and required fields (username,
     * email, first name and last name.
     * 
     * For a valid set of attributes the return value is an empty List.
     * 
     * @param map the list of SSO attributes to validate
     * @return a List with String errors msgs if any is found.
     */
    public List<String> validateSsoUserAttributes(Map<String, SSOAttribute> map){
        List<String> errorMsgs = new ArrayList<String>();
        
        // Checking required fields
        SSOAttribute attrEmail = map.get(ssoConfig.getAttributeEmail());
        if (attrEmail == null){
            errorMsgs.add("");
        } else {
            if (!attrEmail.isSingle()) {
                errorMsgs.add("");
            } else {
                if (attrEmail.getValue() == null || attrEmail.getValue().isEmpty()) {
                    errorMsgs.add("");
                }
            }
        }
        
        
        return errorMsgs;
    }
    
    // State variables after receiving Post Assertion
    private String inResponseTo;
    private String responseXML;
    private String relayState;
    private String userId;
    private boolean validSignature;
    
    // Validator to be used
    private SignatureValidator signatureValidator;
    
    // SSO Configuration - Set on construction - Unchangeable for this object
    private final SSOProviderVO ssoConfig;

    public SSOProviderVO getSsoConfig() {
        return ssoConfig;
    }

    /**
     * Empty constructor
     */
    public OpenSAMLService() {
        this.ssoConfig = OpenSAMLService.getCurrentSSOConfig();
    }
    
    /**
     * 
     * @param ssoAttributes
     * @return 
     * @throws com.pixalere.common.ApplicationException 
     */
    public ProfessionalVO createProfessionalVO(Map<String, SSOAttribute> ssoAttributes) throws ApplicationException {
        ProfessionalVO user = new ProfessionalVO();
        user.setCreated_on(new Date());
        user.setLastmodified_on(new Date());
        
        user.setPassword((MD5.hash(SSOConstants.USER_SSO_PASS)).toLowerCase());
        // SSO, not new, we don't need to reset password or anything
        user.setNew_user(new Integer(0));
        user.setAnswer_one(SSOConstants.USER_SSO_ANSWER);
        user.setAnswer_two(SSOConstants.USER_SSO_ANSWER);
        
        user.setAccount_status(new Integer(1));
        user.setLocked(0);
        user.setInvalid_password_count(0);
        
        // Set email
        String username = ssoAttributes.get(ssoConfig.getAttributeUsername()).getValue();
        user.setUser_name(username);
        // Set username
        String email = ssoAttributes.get(ssoConfig.getAttributeEmail()).getValue();
        user.setEmail(email);
        // Set first and last name
        String firstname = ssoAttributes.get(ssoConfig.getAttributeFirstname()).getValue();
        user.setFirstname(firstname);
        String lastname = ssoAttributes.get(ssoConfig.getAttributeLastname()).getValue();
        user.setLastname(lastname);
        user.setFirstname_search(Common.stripName(user.getFirstname()));
        
        // Set timezone
        user.setTimezone(ssoConfig.getTimezone());
        
        // Save user
        ProfessionalServiceImpl manager = new ProfessionalServiceImpl();
        manager.saveProfessional(user, SSOConstants.ADMIN_ID_SSO);
        
        if (Common.getConfig("enableSSODebug") != null && Common.getConfig("enableSSODebug").equals("1")){
            System.out.println("OpenSAMLService.createProfessionalVO() New SSO User - username: " + user.getUser_name() + " - email: " + user.getEmail());
        }
        return user;
    }
    
    /**
     * Updates all the sections that are allowed to be updated per current
     * Provider configuration and saves all the changes to the BD, it cannot
     * create new Professional users, set/change email nor usernames.
     * 
     * @param user
     * @param ssoAttributes 
     * @return  
     * @throws com.pixalere.common.ApplicationException 
     */
    public ProfessionalVO updateProfessionalVOWithSSOAttributes(ProfessionalVO user, Map<String, SSOAttribute> ssoAttributes) throws ApplicationException {
        if (this.ssoConfig.getAllowSSOUpdate() != null && this.ssoConfig.getAllowSSOUpdate() == 1){
            List<String> changes = new ArrayList<String>();
            // First and last names
            SSOAttribute fnameAttr = ssoAttributes.get(ssoConfig.getAttributeFirstname());
            if (fnameAttr != null && fnameAttr.isSingle() && !fnameAttr.getValue().isEmpty()){
                user.setFirstname(fnameAttr.getValue());
                changes.add("firstname");
            }
            SSOAttribute lnameAttr = ssoAttributes.get(ssoConfig.getAttributeLastname());
            if (lnameAttr != null && lnameAttr.isSingle() && !lnameAttr.getValue().isEmpty()){
                user.setLastname(lnameAttr.getValue());
                changes.add("lastname");
            }
            
            // Email can be updated if it's not the main field
            if (!ssoConfig.getMainField().equals("email")) {
                SSOAttribute emailAttr = ssoAttributes.get(ssoConfig.getAttributeEmail());
                if (emailAttr != null && emailAttr.isSingle() && !emailAttr.getValue().isEmpty()){
                    user.setEmail(emailAttr.getValue());
                    changes.add("email");
                }
            }
            // Positions 
            if (ssoConfig.getSelfConfPositions() == null || ssoConfig.getSelfConfPositions() == 0) {
                // Position
                SSOAttribute positionAttr = ssoAttributes.get(ssoConfig.getAttributePosition());
                if (positionAttr != null && positionAttr.isSingle() && !positionAttr.getValue().isEmpty()){
                    int pos_id = 0;
                    for (SSOPositionVO posVO : ssoConfig.getSsoPositions()) {
                        if (posVO.getAttributeValue().equals(positionAttr.getValue())){
                            pos_id = posVO.getPositionId();
                        }
                    }
                    if (pos_id != 0){
                        user.setPosition_id(pos_id);
                        changes.add("position_id");
                    }
                }
            }
            //  Roles 
            if (ssoConfig.getSelfConfRoles()== null || ssoConfig.getSelfConfRoles() == 0) {
                // Roles to User permissions
                SSOAttribute rolesAttr = ssoAttributes.get(ssoConfig.getAttributeRoles());
                if (rolesAttr != null){
                    List<String> receivedRoles = new ArrayList<String>();
                    if (rolesAttr.isSingle()){
                        receivedRoles.add(rolesAttr.getValue());
                    } else {
                        receivedRoles.addAll(rolesAttr.getValues());
                    }
                    // Verify that there is at least one applicable role
                    boolean canApply = false;
                    for (SSORoleVO roleVO : ssoConfig.getSsoRoles()) {
                        if (receivedRoles.contains(roleVO.getAttributeValue())){
                            canApply = true;
                            break;
                        }
                    }
                    if (!receivedRoles.isEmpty() && canApply){
                        // Clear all current user permissions
                        user.setAccess_allpatients(0);
                        user.setAccess_patients(0);
                        user.setAccess_uploader(0);
                        user.setAccess_viewer(0);
                        user.setAccess_admin(0);
                        user.setAccess_create_patient(0);
                        user.setAccess_assigning(0);
                        user.setAccess_listdata(0);
                        user.setAccess_professionals(0);
                        user.setAccess_ccac_reporting(0);
                        user.setAccess_reporting(0);
                        user.setAccess_audit(0);
                        user.setAccess_superadmin(0);
                        user.setTraining_flag(0);
                        for (SSORoleVO roleVO : ssoConfig.getSsoRoles()) {
                            // Saved role Matches one received
                            if (receivedRoles.contains(roleVO.getAttributeValue())){
                                // Additive roles, only change if the Role is authorizing, otherwise keep current value
                                int i_access_allpatients = roleVO.getAccessAllpatients() == 1? roleVO.getAccessAllpatients(): user.getAccess_allpatients();
                                int i_access_patients = roleVO.getAccessPatients() == 1? roleVO.getAccessPatients(): user.getAccess_patients();
                                int i_access_uploader = roleVO.getAccessUploader() == 1? roleVO.getAccessUploader(): user.getAccess_uploader();
                                int i_access_viewer = roleVO.getAccessViewer() == 1? roleVO.getAccessViewer(): user.getAccess_viewer();
                                int i_access_admin = roleVO.getAccessAdmin() == 1? roleVO.getAccessAdmin(): user.getAccess_admin();
                                int i_access_create_patient = roleVO.getAccessCreatePatient() == 1? roleVO.getAccessCreatePatient(): user.getAccess_create_patient();
                                int i_access_assigning = roleVO.getAccessAssigning() == 1? roleVO.getAccessAssigning(): user.getAccess_assigning();
                                int i_access_listdata = roleVO.getAccessListdata() == 1? roleVO.getAccessListdata(): user.getAccess_listdata();
                                int i_access_professionals = roleVO.getAccessProfessionals() == 1? roleVO.getAccessProfessionals(): user.getAccess_professionals();
                                int i_access_ccac_reporting = roleVO.getAccessCcacReporting() == 1? roleVO.getAccessCcacReporting(): user.getAccess_ccac_reporting();
                                int i_access_reporting = roleVO.getAccessReporting() == 1? roleVO.getAccessReporting(): user.getAccess_reporting();
                                int i_access_audit = roleVO.getAccessAudit() == 1? roleVO.getAccessAudit(): user.getAccess_audit();
                                int i_access_superadmin = roleVO.getAccessSuperadmin() == 1? roleVO.getAccessSuperadmin(): user.getAccess_superadmin();
                                int i_training_flag = roleVO.getTrainingFlag() == 1? roleVO.getTrainingFlag(): user.getTraining_flag();
                                // Set values
                                user.setAccess_allpatients(i_access_allpatients);
                                user.setAccess_patients(i_access_patients);
                                user.setAccess_uploader(i_access_uploader);
                                user.setAccess_viewer(i_access_viewer);
                                user.setAccess_admin(i_access_admin);
                                user.setAccess_create_patient(i_access_create_patient);
                                user.setAccess_assigning(i_access_assigning);
                                // Correction: SSO Access List Data is Manage Resources --> Access Regions in Prof User
                                user.setAccess_regions(i_access_listdata);
                                user.setAccess_professionals(i_access_professionals);
                                user.setAccess_ccac_reporting(i_access_ccac_reporting);
                                user.setAccess_reporting(i_access_reporting);
                                user.setAccess_audit(i_access_audit);
                                user.setAccess_superadmin(i_access_superadmin);
                                user.setTraining_flag(i_training_flag);
                                changes.add("role_applied [" + roleVO.getAttributeValue() + "]");
                            }
                        }
                    }
                }
            }
            
            // -- Needs to be after possible roles update
            if (ssoConfig.getOnlyMainField() == null || ssoConfig.getOnlyMainField() == 0) {
                // Reporting flag as attrib
                if (ssoConfig.getUseAttributeReporting() != null && ssoConfig.getUseAttributeReporting() == 1){
                    SSOAttribute reportingAttr = ssoAttributes.get(ssoConfig.getAttributeNameReporting());
                    if (reportingAttr != null && reportingAttr.isSingle() && !reportingAttr.getValue().isEmpty()){
                        if (reportingAttr.getValue().equals(ssoConfig.getAttributeValueReporting())){
                            user.setAccess_reporting(1);
                            changes.add("access_reporting");
                        }
                    } else if (reportingAttr != null && !reportingAttr.isSingle() && !reportingAttr.getValues().isEmpty()) {
                        for (String value : reportingAttr.getValues()) {
                            if (value.equals(ssoConfig.getAttributeValueReporting())){
                                user.setAccess_reporting(1);
                                changes.add("access_reporting");
                            }
                        }
                    }
                }
                // Training flag as attrib
                if (ssoConfig.getUseAttributeTraining() != null && ssoConfig.getUseAttributeTraining() == 1){
                    SSOAttribute trainingAttr = ssoAttributes.get(ssoConfig.getAttributeNameTraining());
                    if (trainingAttr != null && trainingAttr.isSingle() && !trainingAttr.getValue().isEmpty()){
                        if (trainingAttr.getValue().equals(ssoConfig.getAttributeValueTraining())){
                            user.setTraining_flag(1);
                            changes.add("training_flag");
                        }
                    } else if (trainingAttr != null && !trainingAttr.isSingle() && !trainingAttr.getValues().isEmpty()) {
                        for (String value : trainingAttr.getValues()) {
                            if (value.equals(ssoConfig.getAttributeValueTraining())){
                                user.setTraining_flag(1);
                                changes.add("training_flag");
                            }
                        }
                    }
                }
            }
                        
            // Save user
            ProfessionalServiceImpl manager = new ProfessionalServiceImpl();
            user.setLastmodified_on(new Date());
            user.setLocked(0);
            user.setInvalid_password_count(0);
            // Password can be empty? Set a hard coded one just in case
            if (user.getPassword() == null || user.getPassword().isEmpty()){
                user.setPassword((MD5.hash(SSOConstants.USER_SSO_PASS)).toLowerCase());
                // SSO, not new, we don't need to reset password or anything
                user.setNew_user(new Integer(0));
                user.setAnswer_one(SSOConstants.USER_SSO_ANSWER);
                user.setAnswer_two(SSOConstants.USER_SSO_ANSWER);
                changes.add("sso_password");
            }
            // Automated saving, Admin id = SSO
            manager.saveProfessional(user, SSOConstants.ADMIN_ID_SSO);
            
           // Update Locations
            if (ssoConfig.getSelfConfLocations() == null || ssoConfig.getSelfConfLocations() == 0) {
                // Locations
                SSOAttribute locationsAttr = ssoAttributes.get(ssoConfig.getAttributeLocations());
                if (locationsAttr != null){
                    List<String> receivedLocations = new ArrayList<String>();
                    if (locationsAttr.isSingle()){
                        receivedLocations.add(locationsAttr.getValue());
                    } else {
                        receivedLocations.addAll(locationsAttr.getValues());
                    }
                    // Verify that there is at least one applicable location
                    boolean canApply = false;
                    for (SSOLocationVO locationVO : ssoConfig.getSsoLocations()) {
                        if (receivedLocations.contains(locationVO.getAttributeValue()) && !locationVO.getPixLocationsids().isEmpty()){
                            canApply = true;
                            break;
                        }
                    }
                    // Clear only if there is at least one applicable SSO location, otherwise don't alter the profile
                    if (canApply){
                        // Clear all previous locations
                        manager.removeTreatmentLocation(user.getId());
                    }
                
                    Set<Integer> locationIdSet = new HashSet<Integer>();
                    for (SSOLocationVO locationVO : ssoConfig.getSsoLocations()) {
                        if (receivedLocations.contains(locationVO.getAttributeValue()) && !locationVO.getPixLocationsids().isEmpty()){
                            // Get ids
                            String ids = locationVO.getPixLocationsids();
                            if (ids != null && !ids.isEmpty()){
                                String[] arrayIds = ids.split(",");
                                for (String sLoc : arrayIds) {
                                    int idLoc = Integer.parseInt(sLoc);
                                    locationIdSet.add(idLoc);
                                }
                            }
                        }
                    }
                    // Create all the found locations
                    for (Integer idLoc : locationIdSet) {
                        if (idLoc != null){
                            UserAccountRegionsVO tmp = new UserAccountRegionsVO();
                            tmp.setTreatment_location_id(idLoc);
                            tmp.setUser_account_id(user.getId());
                            manager.saveTreatmentLocation(tmp);
                        }
                    }
                    changes.add(locationIdSet.size() + " new locations");
                }
                
                // Reload user
                user = manager.getProfessionalByUsernameOrId(user.getUser_name());
                
                // If user is in training, add the location
                if (user.getTraining_flag() != null && user.getTraining_flag() == 1){
                    // Check first if the User has the training location already
                    boolean alreadyTraining = false;
                    for (UserAccountRegionsVO locVO : user.getRegions()) {
                        if (locVO.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)){
                            alreadyTraining = true;
                            break;
                        }
                    }
                    // If not found, add it
                    if (!alreadyTraining){
                        UserAccountRegionsVO trainingLoc = new UserAccountRegionsVO();
                        trainingLoc.setTreatment_location_id(Constants.TRAINING_TREATMENT_ID);
                        trainingLoc.setUser_account_id(user.getId());
                        manager.saveTreatmentLocation(trainingLoc);
                        changes.add("training_location");
                    }
                }
            }
            // Debug/test - Display changes done
            String all_changes = "[";
            for (Iterator<String> it = changes.iterator(); it.hasNext();) {
                String change = it.next();
                all_changes += change;
                if (it.hasNext()){
                    all_changes += ", ";
                } else {
                    all_changes += "]";
                }
            }
            if (Common.getConfig("enableSSODebug") != null && Common.getConfig("enableSSODebug").equals("1")){
                System.out.println("OpenSAMLService.updateProfessionalVOWithSSOAttributes() SSO User updated, fields changed:\n" + all_changes);
            }
            // Reload with all changes
            return manager.getProfessionalByUsernameOrId(user.getUser_name());
        }
        return user;
    }
    
    public AuthnRequest buildAuthnRequest(String baseUrl) {
        AuthnRequest authnRequest = OpenSAMLUtils.buildSAMLObject(AuthnRequest.class);
        
        authnRequest.setIssueInstant(new DateTime());
        authnRequest.setVersion(org.opensaml.common.SAMLVersion.VERSION_20);
        
        // Set Identity Provider params        
        authnRequest.setDestination(ssoConfig.getSaml2SSOUrl());
        authnRequest.setNameIDPolicy(buildNameIdPolicy());
        authnRequest.setRequestedAuthnContext(buildRequestedAuthnContext());
        
        // Set by the type of SAML binding to be used (POST vs ARTIFACT)
        if (ssoConfig.getSaml2BindingType().equals(OpenSAMLUtils.BINDING_TYPE_ARTIFACT)) {
            // TODO: SOAP Protocol
            // Binding use SOAP to resolve the artifact - requires Pix with SSL
            authnRequest.setProtocolBinding(SAMLConstants.SAML2_ARTIFACT_BINDING_URI);
            // Artifact Consumer URL
            authnRequest.setAssertionConsumerServiceURL(getArtifactConsumerUrl(baseUrl));
        } else {
            // Binding that works decoding Post data to get the user Assertion
            authnRequest.setProtocolBinding(SAMLConstants.SAML2_POST_BINDING_URI);
            // POST Consumer URL
            authnRequest.setAssertionConsumerServiceURL(getAssertionConsumerUrl(baseUrl));
        }
        
        // Set Service Provider (Pix) params
        authnRequest.setID(OpenSAMLUtils.generateSecureRandomId());
        authnRequest.setIssuer(buildIssuer(ssoConfig));
        

        return authnRequest;
    }
    
    
    public Assertion getAssertionfromPost(HttpServletRequest request){
        HTTPPostDecoder decode = new HTTPPostDecoder( new BasicParserPool());
        BasicSAMLMessageContext context = new BasicSAMLMessageContext();
        HttpServletRequestAdapter adapter = new HttpServletRequestAdapter(request);
        context.setInboundMessageTransport(adapter);
        
        // Decode the POST data
        try {
            decode.decode(context);
        } catch (MessageDecodingException ex) {
            Logger.getLogger(OpenSAMLService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(OpenSAMLService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        relayState = adapter.getParameterValue(POST_RELAY_STATE_PARAM); // decode.getRelayState();
        
        // Only decode the relay state if there is one
        if ((relayState != null) && (relayState.equalsIgnoreCase("") == false)) {
            relayState = new String(Base64.decode(relayState));
        }

        // Get the Response object
        // Response rsp = OpenSAMLUtils.buildSAMLObject(Response.class);
        Response rsp = (Response) context.getInboundMessage();
        
        // If null no need to continue
        if (rsp == null) return null;
        
        // Log XML object for debug
        //OpenSAMLUtils.logSAMLObject(rsp);
        
        // Get the id of the originating request
        // -> Useful if later we start tracking SSO requests,
        // -> this would match the one generated in this class method buildAuthnRequest() 
        inResponseTo = rsp.getInResponseTo();
        
        
        // Look in the SAML Response to pull out the Subject information
        Assertion assertion = null;
        // Get the list of assertions
        List<Assertion> assertionsList = rsp.getAssertions();
        
        // Make sure at least one is present
        if (assertionsList.size() > 0) {
            // Get the first one 
            assertion = assertionsList.get(0);
            
            // If we have to validate the signature, create the validator
            this.validSignature = false;
            if (ssoConfig.getIsAuthnResponseSigned() == 1){
                buildSignatureValidator();
            }

            // Validate the signature of the assertion
            if (signatureValidator != null){
                Signature signatureToValidate;
                signatureToValidate = assertion.getSignature();
                try {
                    // Try to validate.
                    signatureValidator.validate(signatureToValidate);
                    //System.out.println("******** VALID SIGNATURE *******");
                    this.validSignature = true;
                } catch (ValidationException ex) {
                    System.out.println("ERROR: OpenSAMLService.getAssertionfromPost - Validation failed!!\n" + ex.getMessage());
                    this.validSignature = false;
                }
            } else {
                if (ssoConfig.getIsAuthnResponseSigned() == 1){
                    System.out.println("OpenSAMLService.getAssertionfromPost: Not validation of Signatare enabled or available. Skipping.");
                }
            }
            
            // Get the User identifier
            Subject subject = assertion.getSubject();
            NameID nameId = subject.getNameID();
            userId = nameId.getValue();
        }

        // Get the response in XML format
        Marshaller marshaller = Configuration.getMarshallerFactory().getMarshaller(rsp);
        try {
            Element authDOM = marshaller.marshall(rsp);
            StringWriter rspWrt = new StringWriter();

            XMLHelper.writeNode(authDOM, rspWrt);
            responseXML = rspWrt.toString();
        } catch (MarshallingException ex) {
            Logger.getLogger(OpenSAMLService.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
        return assertion;
    }
    
    // Reads the corresponding public certificate file of the current
    // Identity Provider and creates a signature validator
    private void buildSignatureValidator() {
        signatureValidator = null;
        // Ensure we have a valid config
        if (ssoConfig == null) return;
        
        try {
            BasicX509Credential publicCredential = new BasicX509Credential();
            File publicKeyFile = new File(ssoConfig.getCertFilepath());

            if (publicKeyFile.exists()) {
                CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
                InputStream fileStream = new FileInputStream(publicKeyFile);
                X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(fileStream);
                fileStream.close();

                X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(certificate.getPublicKey().getEncoded());
                KeyFactory keyFactory = KeyFactory.getInstance(ssoConfig.getCertAlgorithm());
                PublicKey key = keyFactory.generatePublic(publicKeySpec);

                //Validate Public Key against Signature
                if (key != null) {
                    publicCredential.setPublicKey(key);
                    signatureValidator = new SignatureValidator(publicCredential);
                }
            }
        } catch (InvalidKeySpecException e) {
            System.out.println(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println("SSO ERROR: Signature validation failed. Not found: " + ssoConfig.getCertFilepath());
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (CertificateException e) {
            System.out.println(e.getMessage());
        }
    }
    
    private RequestedAuthnContext buildRequestedAuthnContext() {
        RequestedAuthnContext requestedAuthnContext = OpenSAMLUtils.buildSAMLObject(RequestedAuthnContext.class);
        // Suggestion to IdP: Use a method that is at minimum as secure as defined in the AuthnContextClassRef below
        requestedAuthnContext.setComparison(AuthnContextComparisonTypeEnumeration.MINIMUM);

        // Type of authentication Suggested to the IdP
        AuthnContextClassRef passwordAuthnContextClassRef = OpenSAMLUtils.buildSAMLObject(AuthnContextClassRef.class);
        passwordAuthnContextClassRef.setAuthnContextClassRef(AuthnContext.PASSWORD_AUTHN_CTX);

        requestedAuthnContext.getAuthnContextClassRefs().add(passwordAuthnContextClassRef);

        return requestedAuthnContext;

    }
    
    
    private Issuer buildIssuer(SSOProviderVO config) {
        Issuer issuer = OpenSAMLUtils.buildSAMLObject(Issuer.class);
        issuer.setValue(config.getPixIdentityId());

        return issuer;
    }
    
    private NameIDPolicy buildNameIdPolicy() {
        NameIDPolicy nameIDPolicy = OpenSAMLUtils.buildSAMLObject(NameIDPolicy.class);
        
        // Allow creation of users on login
        nameIDPolicy.setAllowCreate(true);

        // Request persistent identifier for users
        nameIDPolicy.setFormat(NameIDType.PERSISTENT);

        return nameIDPolicy;
    }

    public String getInResponseTo() {
        return inResponseTo;
    }

    public String getResponseXML() {
        return responseXML;
    }

    public String getRelayState() {
        return relayState;
    }

    public String getUserId() {
        return userId;
    }

    public boolean isValidSignature() {
        return validSignature;
    }
    
}
