package com.pixalere.sso.service;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.sso.bean.SSOLocationVO;
import com.pixalere.sso.bean.SSOPositionVO;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.bean.SSORoleVO;
import com.pixalere.sso.dao.SSOProviderDAO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose
 */
public class SSOProviderService {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSOProviderService.class);
    
    private SSOProviderDAO dao;

    public SSOProviderService() {
        dao = new SSOProviderDAO();
    }
    
    public List<SSOProviderVO> getAllProviders(){
        List<SSOProviderVO> list = new ArrayList<SSOProviderVO>();
        try {
            SSOProviderVO criteriaVO = new SSOProviderVO();
            Collection<SSOProviderVO> dash = (Collection<SSOProviderVO>) dao.findAllByCriteria(criteriaVO);
            list.addAll(dash);
        } catch (DataAccessException ex) {
            Logger.getLogger(SSOProviderService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public SSOProviderVO getProviderById(Integer id){
        SSOProviderVO prov = null;
        try {
            prov = (SSOProviderVO) dao.findByPK(id);
        } catch (DataAccessException ex) {
            Logger.getLogger(SSOProviderService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }
    
    public SSOLocationVO getSSOLocationById(Integer id) throws ApplicationException{
        SSOLocationVO loc = new SSOLocationVO();
        loc.setId(id);
        try {
            loc = (SSOLocationVO) dao.findSSOLocationByCriteria(loc);
        } catch (DataAccessException ex) {
            log.error("Error in dao.getSSOLocationById(): " + ex.toString());
            throw new ApplicationException("Error in SSOProviderService.getSSOLocationById(): " + ex.toString(), ex);
        }
        
        return loc;
    }
    
    public SSORoleVO getSSORoleById(Integer id) throws ApplicationException{
        SSORoleVO role = new SSORoleVO();
        role.setId(id);
        try {
            role = (SSORoleVO) dao.findSSORoleByCriteria(role);
        } catch (DataAccessException ex) {
            log.error("Error in dao.getSSORoleById(): " + ex.toString());
            throw new ApplicationException("Error in SSOProviderService.getSSORoleById(): " + ex.toString(), ex);
        }
        
        return role;
    }

    public void saveProvider(SSOProviderVO provider) throws ApplicationException {
        try {
            dao.update(provider);
        } catch (DataAccessException ex) {
            Logger.getLogger(SSOProviderService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("SSOProviderService threw an error: " + ex.getMessage());
        }
    }
    
    public void savePosition(SSOPositionVO vo) throws ApplicationException {
        try {
            dao.updatePosition(vo);
        } catch (DataAccessException e) {
            log.error("Error in dao.savePosition(): " + e.toString());
            throw new ApplicationException("Error in SSOProviderService.savePosition(): " + e.toString(), e);
        }
    }
    
    public void saveLocation(SSOLocationVO vo) throws ApplicationException {
        try {
            dao.updateLocation(vo);
        } catch (DataAccessException e) {
            log.error("Error in dao.saveLocation(): " + e.toString());
            throw new ApplicationException("Error in SSOProviderService.saveLocation(): " + e.toString(), e);
        }
    }
    
    public void saveRole(SSORoleVO vo) throws ApplicationException {
        try {
            dao.updateRole(vo);
        } catch (DataAccessException e) {
            log.error("Error in dao.saveRole(): " + e.toString());
            throw new ApplicationException("Error in SSOProviderService.saveRole(): " + e.toString(), e);
        }
    }
    
    public void removeProvider(SSOProviderVO provider)throws ApplicationException {
        try {
            dao.delete(provider);
        } catch (DataAccessException ex) {
            Logger.getLogger(SSOProviderService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("SSOProviderService threw an error: " + ex.getMessage());
        }
    }
    
    public void removeLocation(SSOLocationVO location)throws ApplicationException {
        try {
            dao.deleteLocation(location);
        } catch (DataAccessException ex) {
            Logger.getLogger(SSOProviderService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("SSOLocationService threw an error: " + ex.getMessage());
        }
    }
    
    public void removeRole(SSORoleVO role)throws ApplicationException {
        try {
            dao.deleteRole(role);
        } catch (DataAccessException ex) {
            Logger.getLogger(SSOProviderService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("SSORoleService threw an error: " + ex.getMessage());
        }
    }
    
    public void removePositions(int providerId) throws ApplicationException {
        try {
            if (providerId > 0){
                SSOPositionVO sample = new SSOPositionVO();
                sample.setSsoProviderid(providerId);
                Collection<SSOPositionVO> collection = dao.findAllPositionsByCriteria(sample);
                for (SSOPositionVO vo : collection) {
                    dao.deletePosition(vo);
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
}
