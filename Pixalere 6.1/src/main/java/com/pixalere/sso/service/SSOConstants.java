package com.pixalere.sso.service;

import com.pixalere.utils.Common;

/**
 * Shared constants, messages and resources keys used by SSO components.
 * 
 * @author Jose
 * @since 6.0
 */
public class SSOConstants {
    // Constant used to set as password for the SSO created users
    public static String USER_SSO_PASS = "sso_access23!pix";
    public static String USER_SSO_ANSWER = "sso_answer";
    // Used to indicate that the user was created/updated via SSO
    public static int ADMIN_ID_SSO = -3;
    
    // Missing Sections
    public static String MISSING_UNAME = "sso.userfield_error.uname";
    public static String MISSING_EMAIL = "sso.userfield_error.email";
    public static String MISSING_FNAME = "sso.userfield_error.lname";
    public static String MISSING_LNAME = "sso.userfield_error.fname";
    public static String MISSING_POS = "sso.userfield_error.pos";
    public static String MISSING_LOC = "sso.userfield_error.loc";
    public static String MISSING_PER = "sso.userfield_error.per";
    public static String MISSING_TIMEZONE = "sso.userfield_error.timezone";
    // SSO fields type
    public static String TYPE_NAME = "name";
    public static String TYPE_VALUE = "value";
    // SSO fields
    public static String FIELD_USERNAME = "username";
	public static String FIELD_EMAIL = "email";
	public static String FIELD_FIRSTNAME = "firstname";
	public static String FIELD_LASTNAME = "lastname";
	public static String FIELD_ROLE = "role";
	public static String FIELD_LOCATION = "location";
        public static String FIELD_POSITION = "position";
        public static String FIELD_TRAINING = "training";
        public static String FIELD_REPORTING = "reporting";
    // SSO error reasons
    public static String ERROR_MISSING = "missing";
	public static String ERROR_NOT_SINGLE = "notsingle";
	public static String ERROR_NOT_VALID = "notvalid";
        public static String ERROR_SPACE_COMMA = "space_comma";
	public static String ERROR_NOT_UNIQUE = "notunique";
    
    public static String getErrorMsg(String field, String reason, String type, String fieldName, String locale){
        if (type.equals(TYPE_NAME)){
            return Common.getLocalizedString("sso.errors." + field, locale) 
                + (fieldName.isEmpty()? "" : "[" + fieldName + "] ")  
                + Common.getLocalizedString("sso.errors." + reason, locale);
        } else {
            return Common.getLocalizedString("sso.errors.attr." + field, locale) 
                + (fieldName.isEmpty()? "" : "[" + fieldName + "] ")  
                + Common.getLocalizedString("sso.errors." + reason, locale);
        }
    }
}
