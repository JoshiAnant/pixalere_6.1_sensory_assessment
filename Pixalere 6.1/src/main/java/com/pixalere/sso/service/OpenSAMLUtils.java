package com.pixalere.sso.service;

import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.log4j.Logger;
import org.opensaml.common.impl.SecureRandomIdentifierGenerator;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallingException;
import org.w3c.dom.Document;

/**
 * Several utilities for OpenSAML SSO processing.
 *
 * @author Jose
 * @since 6.0
 */
public class OpenSAMLUtils {
    private static final Logger logger = Logger.getLogger(OpenSAMLUtils.class);
    private static SecureRandomIdentifierGenerator secureRandomIdGenerator;
    
    // CONSTANTS
    public static final String BINDING_TYPE_POST = "POST";
    public static final String BINDING_TYPE_ARTIFACT = "ARTIFACT";
    
    static {
        try {
            secureRandomIdGenerator = new SecureRandomIdentifierGenerator();
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage(), e);
        }
    }
    
    /**
     * Generates an unique random ID used for identifying SP requests.
     * 
     * @return 
     */
    public static String generateSecureRandomId() {
        return secureRandomIdGenerator.generateIdentifier();
    }
    
    /**
     * Helper method to build openSAML objects.
     * 
     * @param <T>
     * @param clazz
     * @return 
     */
    public static <T> T buildSAMLObject(final Class<T> clazz) {
        T object = null;
        try {
            XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
            QName defaultElementName = (QName)clazz.getDeclaredField("DEFAULT_ELEMENT_NAME").get(null);
            object = (T)builderFactory.getBuilder(defaultElementName).buildObject(defaultElementName);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Could not create SAML object");
        } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException("Could not create SAML object");
        }

        return object;
    }
    
    /**
     * Logging opensaml objects for debugging purposes.
     * 
     * @param object 
     */
    public static void logSAMLObject(final XMLObject object) {
        try {
            DocumentBuilder builder;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);

            builder = factory.newDocumentBuilder();

            Document document = builder.newDocument();
            Marshaller out = Configuration.getMarshallerFactory().getMarshaller(object);
            out.marshall(object, document);
            // Legible format
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            // Tranform
            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(document);
            transformer.transform(source, result);
            String xmlString = result.getWriter().toString();

            System.out.println(xmlString);
        } catch (ParserConfigurationException e) {
            logger.error(e.getMessage(), e);
        } catch (MarshallingException e) {
            logger.error(e.getMessage(), e);
        } catch (TransformerException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
