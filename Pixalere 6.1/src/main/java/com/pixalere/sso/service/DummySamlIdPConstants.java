package com.pixalere.sso.service;

import com.pixalere.sso.bean.SSOProviderVO;

/**
 * Test variables used for local development, we set them here for fastest
 * configuration using the corresponding parameters with a test Identity
 * Provider with SAML support (like simpleSAMLphp). If this works, the 
 * full setup using the Admin interface of our Pixalere local installation
 * should be made to confirm it works too.
 * 
 * @author Jose
 * @since 6.0
 */
public class DummySamlIdPConstants {
    
    public static SSOProviderVO getDummyProvider(){
        SSOProviderVO idpConfig = new SSOProviderVO();
        
        idpConfig.setIdpIdentityId(IDP_ENTITY_ID);
        idpConfig.setSaml2SSOUrl(SSO_SERVICE);
        idpConfig.setSaml2BindingType(BINDING_TYPE_POST);
        idpConfig.setIsAuthRequestSigned(SIGN_AUTH_REQUESTS);
        idpConfig.setIsAuthnResponseSigned(AUTH_RESPONSES_SIGNED);
        idpConfig.setIsAuthResponseEncrypted(AUTH_RESPONSES_ENCRYPTED);
        
        idpConfig.setCertAlgorithm(IDP_CERT_ALG);
        idpConfig.setCertFilepath(IDP_CERT_PATH);
        
        idpConfig.setPixIdentityId(SP_IDENTITY_ID);
        idpConfig.setSsoType(SSO_TYPE);
        idpConfig.setDescription(IDP_DESCRIPTION);
        
        return idpConfig;
    }
    
    // Set the corresponding data according to a test Identity Provider
//    public static final String IDP_ENTITY_ID = "TestIDP";
//    public static final String SSO_SERVICE = "http://localhost:8080/webprofile-ref-project/idp/singleSignOnService";
//    public static final String ARTIFACT_RESOLUTION_SERVICE = "http://localhost:8080/webprofile-ref-project/idp/artifactResolutionService";
    
    // Local identity
    public static final String SP_IDENTITY_ID = "Pixalere";
    public static final String SSO_TYPE = "saml2";
    public static final String IDP_DESCRIPTION = "Locally setup Identity Provider "
            + "for development using simpleSAMLphp. Read the corresponding "
            + "Pixalere documentation to install according to these parameters.";
    
    // Note: Our local Pixalere installation should be registered in the test IdP!
    public static final String BINDING_TYPE_POST = "POST";
    public static final String BINDING_TYPE_ARTIFACT = "ARTIFACT";
    
    // We need self-signed SSL cert&key installed in our local Pix if true
    public static final Integer SIGN_AUTH_REQUESTS = 0;
    
    // We need the public SSL cert file of the IdP installed in our local Pix if true
    public static final Integer AUTH_RESPONSES_SIGNED = 1;
    public static final Integer AUTH_RESPONSES_ENCRYPTED = 0;
    
    // Local simpleSAMLphp as IdP (non-SSL)
    public static final String IDP_ENTITY_ID = "https://mysamlidp.com/idp/saml2/idp/metadata.php";
    public static final String SSO_SERVICE = "https://mysamlidp.com/idp/saml2/idp/SSOService.php";
    public static final String ARTIFACT_RESOLUTION_SERVICE = "";
    public static final String IDP_CERT_PATH = "C:\\pixalere\\ssocerts\\mysamlidp.com.crt";
    public static final String IDP_CERT_ALG = "RSA";
    
    // Local simpleSAMLphp as IdP (SSL)
}
