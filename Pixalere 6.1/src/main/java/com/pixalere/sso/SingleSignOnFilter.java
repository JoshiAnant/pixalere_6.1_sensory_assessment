/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.sso;

import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.endpoint.SSOConsumerArtifact;
import com.pixalere.sso.endpoint.SSOConsumerPost;
import com.pixalere.sso.endpoint.SSOLogout;
import com.pixalere.sso.service.OpenSAMLService;
import com.pixalere.sso.service.OpenSAMLUtils;
import com.pixalere.utils.Common;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.binding.encoding.HTTPRedirectDeflateEncoder;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.saml2.metadata.SingleSignOnService;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;

/**
 * Intercepts all requests and if SSO has been enabled and the user
 * is not logged in, starts the SSO process for authentication. The init
 * method validates the current configuration and libraries availables, as well
 * as initializing all the required objects for processing requests.
 * 
 * @since 6.0
 * @author Jose
 */
public class SingleSignOnFilter implements Filter {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.sso.SingleSignOnFilter.class);
    
    // Urls and types that always need to be excluded from SSO
    private static final String[] FIXED_EXCLUDED_URLS_TYPES = {
        // Common file types
        ".css",
        ".CSS",
        ".gif",
        ".GIF",
        ".jpg",
        ".JPG",
        ".ico",
        ".wsdl",
        "/login.do",
        // Web Services
        "/services",
        "/services/",
        "/AssessmentCommentsService",
        "/AssessmentImagesService",
        "/AssessmentNPWTService",
        "/AssessmentService",
        "/AssignPatientsService",
        "/ConfigurationService",
        "/GUIService",
        // IntegrationService exposed already as PixalereService?
        "/IntegrationService",
        "/PixalereService",
        "/LibraryService",
        "/ListService",
        "/LogAuditService",
        "/NursingCarePlanService",
        "/PatientProfileService",
        "/PatientService",
        // Is PCCService really exposed?
        "/PCCService",
        "/ProductCategoryService",
        "/ProductsService",
        "/ProfessionalService",
        "/ReferralsTrackingService",
        "/ResourcesService",
        "/TreatmentCommentsService",
        "/WoundAssessmentLocationService",
        "/WoundAssessmentService",
        "/WoundService",
        // SSO end points
        SSOConsumerPost.CONSUMER_URI + ".do",
        SSOConsumerArtifact.CONSUMER_URI + ".do",
        SSOLogout.LOGOUT_URI + ".do"
    };
    // Exclude queries containing:
    private static final String[] FIXED_EXCLUDED_QUERY = {"wsdl", "stylesheet"};
    private boolean debugEnabled;
    

    @Override
    public void destroy() {
        
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();
        debugEnabled = Common.getConfig("enableSSODebug") != null && Common.getConfig("enableSSODebug").equals("1");
        
        if (
                // NOTE: The order is intended to evaluate the most resource
                // intensive conditions last
                
                // Only if not Logged in already
                session.getAttribute("userVO") == null
                // Exclude fixed file types and fixed urls like SSO endpoints
                // (css, /login.do, /ssoLogout.do, etc)
                && !excludedFixedUrl(httpServletRequest.getRequestURI())
                // Exclude Web Service wsdl query (query string wsdl)
                && !excludedFixedQueries(httpServletRequest.getQueryString())
                // Check configuration to start SSO
                && Common.getConfig("enableSSO") != null 
                && Common.getConfig("enableSSO").equals("1")
                // Exclude custom file types and urls
                && !excludedRequest(httpServletRequest.getRequestURI())
                ){
            
            // Validate bypass tokens
            if (isTokenBypassSSO(request) || isSessionBypassSSO(httpServletRequest, session)){
                // Allowed to continue - Bypassing SSO
                if (debugEnabled) System.out.println("SingleSignOnFilter: Bypassing SSO !!!");
                
                // If this request goes to index, then store the bypassing in the session
                if (httpServletRequest.getServletPath().endsWith("/index.html")){
                    session.setAttribute("tokenSSO", httpServletRequest.getQueryString());
                }
                
                // BYPASS TO URL
                if (debugEnabled) System.out.println("Bypassing getRequestURI(): [" + httpServletRequest.getRequestURI() + "]");
                chain.doFilter(request, response);
            } else {
                // Start SSO process                
                HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                
                // TODO: Other SSO protocols?
                
                // ** SAML SSO SECTION **
                // IMPORTANT: True for local testing (using Dummy hardcoded
                // SAML data), use the empty constructor to use SSO configuration in the DB
                //OpenSAMLService openSAMLService = new OpenSAMLService(true);
                OpenSAMLService openSAMLService = new OpenSAMLService();
                
                // Run SSO only if we have a valid SSO configuration enabled
                SSOProviderVO ssopvo = openSAMLService.getSsoConfig();
                // Errors in configuration? Display?
                if (ssopvo != null && ssopvo.getActive().equals(1)){
                    if (debugEnabled) System.out.println("SingleSignOnFilter: *********  SSO STARTED ****** ");
                    if (debugEnabled) System.out.println("getRequestURI(): [" + httpServletRequest.getRequestURI() + "]");
                    
                    // Create the Authentication request
                    String baseURL = OpenSAMLService.getBaseUrl(httpServletRequest);
                    AuthnRequest authnRequest = openSAMLService.buildAuthnRequest(baseURL);

                    // Redirect to the Identity Provider
                    redirectToIdentityProvider(httpServletResponse, authnRequest, openSAMLService);
                } else {
                    // SSO cannot be applied, missing an enabled SSO Configuration
                    if (debugEnabled) System.out.println("SSO configuration missing or no provider enabled! Ignoring SSO.");
                    chain.doFilter(request, response);
                }
            }
            
        } else {
            // SSO does not apply, continue to the requested page
            chain.doFilter(request, response);
        }
        
    }
    
    
    private void redirectToIdentityProvider(HttpServletResponse httpServletResponse, AuthnRequest authnRequest, OpenSAMLService openSAMLService) {
        HttpServletResponseAdapter responseAdapter = new HttpServletResponseAdapter(httpServletResponse, true);
        SSOProviderVO ssoConfig = openSAMLService.getSsoConfig();
        
        BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject> context = new BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject>();
        
        String idpUrl = ssoConfig.getSaml2SSOUrl();
        context.setPeerEntityEndpoint(getIPDEndpoint(idpUrl));
        
        context.setOutboundSAMLMessage(authnRequest);
        context.setOutboundMessageTransport(responseAdapter);
        
        // Are we SSL signing? Do the IdP requires it?
        // Then we need to set the credentials
        if (ssoConfig.getIsAuthRequestSigned() == 1){
            
            // TODO: Generate and Set the Pixalere credential
            // context.setOutboundSAMLMessageSigningCredential(openSAMLService.getCredential());
        }
        

        HTTPRedirectDeflateEncoder encoder = new HTTPRedirectDeflateEncoder();
        //System.out.println("AuthnRequest: ");
        //OpenSAMLUtils.logSAMLObject(authnRequest);

        if (debugEnabled) System.out.println("Redirecting to IDP");
        try {
            encoder.encode(context);
        } catch (MessageEncodingException e) {
            System.out.println("Error redirecting POST: \n" + e.getMessage());
            throw new RuntimeException(e);
        }
    }
    
    private Endpoint getIPDEndpoint(String ssoUrl) {
        SingleSignOnService endpoint = OpenSAMLUtils.buildSAMLObject(SingleSignOnService.class);
        // For the first request we will use Redirect always
        endpoint.setBinding(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
        endpoint.setLocation(ssoUrl);

        return endpoint;
    }
    
    /**
     * Validates for fixed Query strings that should be excluded from SSO.
     * @param queryString
     * @return 
     */
    private boolean excludedFixedQueries(String queryString){
        boolean exclude = false;
        if (queryString != null) {
            for (String query : FIXED_EXCLUDED_QUERY) {
                if (!query.isEmpty() && queryString.contains(query)){
                    exclude = true;
                    //if (debugEnabled) System.out.println("SingleSignOnFilter.excludedFixedQueries() Excluding fixed query string: " + queryString);
                    break;
                }
            }
        }
        return exclude;
    }
    
    /**
     * Validates for fixed URLs and types that should be excluded.
     * @param uriRequest
     * @return 
     */
    private boolean excludedFixedUrl(String uriRequest){
        boolean exclude = false;
        for (String type : FIXED_EXCLUDED_URLS_TYPES) {
            if (!type.isEmpty() && uriRequest.endsWith(type)){
                exclude = true;
                //if (debugEnabled) System.out.println("SingleSignOnFilter.excludedFixedUrl() Excluding fixed URL: " + uriRequest);
                break;
            }
        }
        return exclude;
    }
    
    /**
     * Validates for the custom URLs and types that should be excluded.
     * @param uriRequest
     * @return 
     */
    private boolean excludedRequest(String uriRequest){
        //String uri = request.getRequestURI();
        boolean exclude = false;
        String excludedTypesSSO = Common.getConfig("excludedTypesSSO");
        if (excludedTypesSSO != null){
            String[] excludedTypes = excludedTypesSSO.split(";");
            for (String type : excludedTypes) {
                if (!type.isEmpty() && uriRequest.endsWith(type)){
                    exclude = true;
                    break;
                }
            }
        }
        
        return exclude;
    }
    
    private boolean isSessionBypassSSO(HttpServletRequest req, HttpSession session){
        boolean bypass = false;
        
        // If this is LoginSetup.do and we were redirected from index.html check (session flag)
        if (req.getServletPath().endsWith("LoginSetup.do") 
                && session.getAttribute("tokenSSO") != null){
            
            String sessionTokenSSO = (String) session.getAttribute("tokenSSO");
            
            // Validate for super user token
            String superToken = Common.getConfig("bypassSSOsuper");
            if (superToken != null) {
                // If the super admin token is used, it overrides any config
                bypass = superToken.equals(sessionTokenSSO);
                if (debugEnabled) System.out.println("SESSION - Bypassing by super token: " + bypass);
            }

            // If we are in partial SSO, validate for user bypass token
            if (Common.getConfig("fullSSO") == null || 
                    (Common.getConfig("fullSSO") != null && !Common.getConfig("fullSSO").equals("1"))
                    ) {

                String passToken = Common.getConfig("bypassSSOtoken");
                if (!bypass && passToken != null) {
                    bypass = passToken.equals(sessionTokenSSO);
                    if (debugEnabled) System.out.println("SESSION - Bypassing by user normal token: " + bypass);
                }
            }
            
            // Remove flag from session
            session.setAttribute("tokenSSO", null);
        }
        
        return bypass;
    }
    
    private boolean isTokenBypassSSO(ServletRequest req){
        boolean bypass = false;
        
        // Validate for super user token
        String superToken = Common.getConfig("bypassSSOsuper");
        if (superToken != null) {
            // If the super admin token is used, it overrides any config
            bypass = validateToken(superToken, req);
            if (debugEnabled) System.out.println("Bypassing by super token: " + bypass);
        }
        
        // If we are in partial SSO, validate for user bypass token
        if (Common.getConfig("fullSSO") == null || 
                (Common.getConfig("fullSSO") != null && !Common.getConfig("fullSSO").equals("1"))
                ) {
            
            String passToken = Common.getConfig("bypassSSOtoken");
            if (!bypass && passToken != null) {
                bypass = validateToken(passToken, req);
                if (debugEnabled) System.out.println("Bypassing by user normal token: " + bypass);
            }
        }
        
        return bypass;
    }
    
    private boolean validateToken(String configTokens, ServletRequest req){
        HttpServletRequest request = (HttpServletRequest) req;
        String tokencode[] = configTokens.split("=");
        if (tokencode.length == 2) {
            String token = tokencode[0];
            String code = tokencode[1];
            return request.getParameter(token) == null
                    ? false 
                    : request.getParameter(token).equals(code);
        } 
        if (debugEnabled) System.out.println("SingleSignOnFilter.validateToken() Wrong format for bypass token: " + configTokens + ". Should be in the format <parameter>=<value>");
        return false;
    }

    @Override
    public void init(FilterConfig fc) throws ServletException {
        //System.out.println("SingleSignOnFilter init");
        
        // Validation Code 
        boolean validJCE = Configuration.validateJCEProviders();
        //System.out.println("SingleSignOnFilter validJCE = " + validJCE);
        Configuration.validateNonSunJAXP();
        /*
        System.out.println("\n** SingleSignOnFilter Security Providers **");
        for (Provider jceProvider : Security.getProviders()) {
            System.out.println(jceProvider.getInfo());
        }
        System.out.println("** ************************************ **\n");
        */
        try {
            //System.out.println("OpenSAML Bootstrapping");
            DefaultBootstrap.bootstrap();
        } catch (org.opensaml.xml.ConfigurationException e) {
            System.out.println("OpenSAML Bootstrapping failed");
            e.printStackTrace();
            throw new RuntimeException("OpenSAML Bootstrapping failed");
        }
    }
    
}
