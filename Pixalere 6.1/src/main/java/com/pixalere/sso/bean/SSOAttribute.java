package com.pixalere.sso.bean;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Jose
 * @since 6.0
 */
public class SSOAttribute {
    private String name;
    private String value;
    private boolean single;
    private boolean warning;
    private List<String> values;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSingle() {
        return single;
    }

    public void setSingle(boolean single) {
        this.single = single;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public boolean isWarning() {
        return warning;
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    @Override
    public String toString() {
        String val = "";
        if (values != null){
            for (Iterator<String> it = values.iterator(); it.hasNext();) {
                String string = it.next();
                val += string;
                if (it.hasNext()){
                    val += ", ";
                }
            }
        }
        return "SSOAttribute [ " +  name + ": " + (single? value: val) + " ]";
    }
    
    
}
