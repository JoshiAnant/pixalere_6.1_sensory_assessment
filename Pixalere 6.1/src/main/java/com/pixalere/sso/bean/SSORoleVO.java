package com.pixalere.sso.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 * This is the sso_roles (DB table) bean 
 * Each entry corresponds to an instance of {@link com.pixalere.sso.bean.SSOProviderVO },
 * attributeValue is the value sent by the IdentityProvider, set 
 * in the field named SSOProviderVO.attributeRoles, while the list of 
 * flags corresponds  to the Pixalere user permissions that should enabled
 * for the user which has the corresponding role name, stored in the field name.
 * .
 * 
 * @author Jose
 * @since 6.0
 */
public class SSORoleVO extends ValueObject implements Serializable {
    
    private Integer ssoProviderid;
    private String name;
    private String description;
    private Integer active;
    private String attributeValue;
    private Integer accessAllpatients;
    private Integer accessPatients;
    private Integer accessUploader;
    private Integer accessViewer;
    private Integer accessAdmin;
    private Integer accessCreatePatient;
    private Integer accessAssigning;
    private Integer accessListdata;
    private Integer accessProfessionals;
    private Integer accessCcacReporting;
    private Integer accessReporting;
    private Integer accessAudit;
    private Integer accessSuperadmin;
    private Integer trainingFlag;

    public SSORoleVO() {
    }

    public Integer getSsoProviderid() {
        return ssoProviderid;
    }

    public void setSsoProviderid(Integer ssoProviderid) {
        this.ssoProviderid = ssoProviderid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public Integer getAccessAllpatients() {
        return accessAllpatients;
    }

    public void setAccessAllpatients(Integer accessAllpatients) {
        this.accessAllpatients = accessAllpatients;
    }

    public Integer getAccessPatients() {
        return accessPatients;
    }

    public void setAccessPatients(Integer accessPatients) {
        this.accessPatients = accessPatients;
    }

    public Integer getAccessUploader() {
        return accessUploader;
    }

    public void setAccessUploader(Integer accessUploader) {
        this.accessUploader = accessUploader;
    }

    public Integer getAccessViewer() {
        return accessViewer;
    }

    public void setAccessViewer(Integer accessViewer) {
        this.accessViewer = accessViewer;
    }

    public Integer getAccessAdmin() {
        return accessAdmin;
    }

    public void setAccessAdmin(Integer accessAdmin) {
        this.accessAdmin = accessAdmin;
    }

    public Integer getAccessCreatePatient() {
        return accessCreatePatient;
    }

    public void setAccessCreatePatient(Integer accessCreatePatient) {
        this.accessCreatePatient = accessCreatePatient;
    }

    public Integer getAccessAssigning() {
        return accessAssigning;
    }

    public void setAccessAssigning(Integer accessAssigning) {
        this.accessAssigning = accessAssigning;
    }

    public Integer getAccessListdata() {
        return accessListdata;
    }

    public void setAccessListdata(Integer accessListdata) {
        this.accessListdata = accessListdata;
    }

    public Integer getAccessProfessionals() {
        return accessProfessionals;
    }

    public void setAccessProfessionals(Integer accessProfessionals) {
        this.accessProfessionals = accessProfessionals;
    }

    public Integer getAccessCcacReporting() {
        return accessCcacReporting;
    }

    public void setAccessCcacReporting(Integer accessCcacReporting) {
        this.accessCcacReporting = accessCcacReporting;
    }

    public Integer getAccessReporting() {
        return accessReporting;
    }

    public void setAccessReporting(Integer accessReporting) {
        this.accessReporting = accessReporting;
    }

    public Integer getAccessAudit() {
        return accessAudit;
    }

    public void setAccessAudit(Integer accessAudit) {
        this.accessAudit = accessAudit;
    }

    public Integer getAccessSuperadmin() {
        return accessSuperadmin;
    }

    public void setAccessSuperadmin(Integer accessSuperadmin) {
        this.accessSuperadmin = accessSuperadmin;
    }

    public Integer getTrainingFlag() {
        return trainingFlag;
    }

    public void setTrainingFlag(Integer trainingFlag) {
        this.trainingFlag = trainingFlag;
    }

}
