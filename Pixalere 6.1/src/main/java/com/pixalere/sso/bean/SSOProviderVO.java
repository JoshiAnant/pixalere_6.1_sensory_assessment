package com.pixalere.sso.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Collection;

/**
 * This is the sso_providers (DB table) bean
 * Refer to {@link com.pixalere.sso.service.OpenSAMLService } for the calls
 * which utilize this bean.
 * 
 * @author Jose
 * @since 6.0
 */
public class SSOProviderVO extends ValueObject implements Serializable {
    // Enable/disable
    private Integer active;
    private String ssoType;
    private String description;
    
    // This Providers and Pix's identities (Constant String Identifiers)
    private String pixIdentityId;
    private String idpIdentityId;
    
    // SSO requests parameters 
    // SAML2: POST Binding
    private String saml2SSOUrl;
    // TODO: (If required, although unlikely) SAML2: Artifact Binding/SOAP parameters
    private String saml2ArtifactUrl;
    private Integer isAuthRequestSigned;
    private Integer isAuthRequestEncrypted;
    private Integer isAuthnResponseSigned;
    private Integer isAuthResponseEncrypted;
    private String saml2BindingType;

    // Single Logout parameters
    private Integer isLogoutEnabled;
    private Integer isLogoutRequestSigned;
    private String logoutRequestUrl;
    
    // IdP Certificates parameters
    private String certFilepath;
    private String certAlgorithm; // Can be "RSA" or "DSA"

    // User mapping data
    // Timezone assigned for all users in this IDP
    private String timezone;
    // Select which field is the SSO linker field
    private String mainField;  // can be username OR email

    // If enabled, all the other SSO user fields are ignored
    // the user is able to log in with his unique id
    // but User creation, configuration and management 
    // is done completely in Pixalere
    private Integer onlyMainField;

    // If enabled, when SSO attributes other than the
    // mainField are available, Pixalere will try to
    // update the user configuration with the new data
    private Integer allowSSOUpdate;

    // Field that contains the data that defines if the user can access Pix
    private String attributePixauth;
    // Value that must be in the field named by <attribute_name_pixauth>
    private String attributeValPixauth;
    // Username of the user, must be unique.
    private String attributeUsername;
    // E-mail of the user, must be unique.
    private String attributeEmail;
    // First name.
    private String attributeFirstname;
    // Last name.
    private String attributeLastname;
    // Professional position
    private String attributePosition;

    // Field that contains an array of location names. Mapped to PIX locations using the table sso_locations.
    private String attributeLocations;
    // Field that contains an array of roles. Mapped to PIX access flags using the table sso_roles.
    private String attributeRoles;
    
    // Special attributes (training and reporting)
    // If they are false, training/reporting are managed using dedicated roles
    // flag for using a dedicated attribute for enabling/disabling training
    private Integer useAttributeTraining;
    // flag for using a dedicated attribute for enabling/disabling reporting
    private Integer useAttributeReporting;
    // names of the sso attributes that contains the enabling value for training or reporting
    private String attributeNameTraining;
    private String attributeNameReporting;
    // values that the sso attributes should contain for enabling training or reporting
    private String attributeValueTraining;
    private String attributeValueReporting;

    // Flags to indicate which sections should be setup manually if were in Partial SSO
    private Integer selfConfRoles;
    private Integer selfConfLocations;
    private Integer selfConfPositions;
    
    // Collection of attribute to Pix locations mappings
    private Collection<SSOLocationVO> ssoLocations;
    // Collection of roles to permissions mappings
    private Collection<SSORoleVO> ssoRoles;
    // Collection of values to Pix positions
    private Collection<SSOPositionVO> ssoPositions;
    
    public SSOProviderVO() {
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getSsoType() {
        return ssoType;
    }

    public void setSsoType(String ssoType) {
        this.ssoType = ssoType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getPixIdentityId() {
        return pixIdentityId;
    }

    public void setPixIdentityId(String pixIdentityId) {
        this.pixIdentityId = pixIdentityId;
    }

    public String getIdpIdentityId() {
        return idpIdentityId;
    }

    public void setIdpIdentityId(String idpIdentityId) {
        this.idpIdentityId = idpIdentityId;
    }

    public String getSaml2SSOUrl() {
        return saml2SSOUrl;
    }

    public void setSaml2SSOUrl(String saml2SSOUrl) {
        this.saml2SSOUrl = saml2SSOUrl;
    }
    
    public String getSaml2ArtifactUrl() {
        return saml2ArtifactUrl;
    }

    public void setSaml2ArtifactUrl(String saml2ArtifactUrl) {
        this.saml2ArtifactUrl = saml2ArtifactUrl;
    }

    public Integer getIsAuthRequestSigned() {
        return isAuthRequestSigned;
    }

    public void setIsAuthRequestSigned(Integer isAuthRequestSigned) {
        this.isAuthRequestSigned = isAuthRequestSigned;
    }

    public Integer getIsAuthRequestEncrypted() {
        return isAuthRequestEncrypted;
    }

    public void setIsAuthRequestEncrypted(Integer isAuthRequestEncrypted) {
        this.isAuthRequestEncrypted = isAuthRequestEncrypted;
    }

    public Integer getIsAuthnResponseSigned() {
        return isAuthnResponseSigned;
    }

    public void setIsAuthnResponseSigned(Integer isAuthnResponseSigned) {
        this.isAuthnResponseSigned = isAuthnResponseSigned;
    }

    public Integer getIsAuthResponseEncrypted() {
        return isAuthResponseEncrypted;
    }

    public void setIsAuthResponseEncrypted(Integer isAuthResponseEncrypted) {
        this.isAuthResponseEncrypted = isAuthResponseEncrypted;
    }

    public String getSaml2BindingType() {
        return saml2BindingType;
    }

    public void setSaml2BindingType(String saml2BindingType) {
        this.saml2BindingType = saml2BindingType;
    }

    public Integer getIsLogoutEnabled() {
        return isLogoutEnabled;
    }

    public void setIsLogoutEnabled(Integer isLogoutEnabled) {
        this.isLogoutEnabled = isLogoutEnabled;
    }

    public Integer getIsLogoutRequestSigned() {
        return isLogoutRequestSigned;
    }

    public void setIsLogoutRequestSigned(Integer isLogoutRequestSigned) {
        this.isLogoutRequestSigned = isLogoutRequestSigned;
    }

    public String getLogoutRequestUrl() {
        return logoutRequestUrl;
    }

    public void setLogoutRequestUrl(String logoutRequestUrl) {
        this.logoutRequestUrl = logoutRequestUrl;
    }

    public String getCertAlgorithm() {
        return certAlgorithm;
    }

    public void setCertAlgorithm(String certAlgorithm) {
        this.certAlgorithm = certAlgorithm;
    }

    public String getCertFilepath() {
        return certFilepath;
    }

    public void setCertFilepath(String certFilepath) {
        this.certFilepath = certFilepath;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getMainField() {
        return mainField;
    }

    public void setMainField(String mainField) {
        this.mainField = mainField;
    }

    public String getAttributePixauth() {
        return attributePixauth;
    }

    public void setAttributePixauth(String attributePixauth) {
        this.attributePixauth = attributePixauth;
    }

    public String getAttributeValPixauth() {
        return attributeValPixauth;
    }

    public void setAttributeValPixauth(String attributeValPixauth) {
        this.attributeValPixauth = attributeValPixauth;
    }

    public String getAttributeUsername() {
        return attributeUsername;
    }

    public void setAttributeUsername(String attributeUsername) {
        this.attributeUsername = attributeUsername;
    }

    public String getAttributeEmail() {
        return attributeEmail;
    }

    public void setAttributeEmail(String attributeEmail) {
        this.attributeEmail = attributeEmail;
    }

    public String getAttributeFirstname() {
        return attributeFirstname;
    }

    public void setAttributeFirstname(String attributeFirstname) {
        this.attributeFirstname = attributeFirstname;
    }

    public String getAttributeLastname() {
        return attributeLastname;
    }

    public void setAttributeLastname(String attributeLastname) {
        this.attributeLastname = attributeLastname;
    }

    
    public String getAttributePosition() {
        return attributePosition;
    }

    public void setAttributePosition(String attributePosition) {
        this.attributePosition = attributePosition;
    }

    public String getAttributeLocations() {
        return attributeLocations;
    }

    public void setAttributeLocations(String attributeLocations) {
        this.attributeLocations = attributeLocations;
    }

    public String getAttributeRoles() {
        return attributeRoles;
    }

    public void setAttributeRoles(String attributeRoles) {
        this.attributeRoles = attributeRoles;
    }

    public Collection<SSOLocationVO> getSsoLocations() {
        return ssoLocations;
    }

    public void setSsoLocations(Collection<SSOLocationVO> ssoLocations) {
        this.ssoLocations = ssoLocations;
    }

    public Collection<SSORoleVO> getSsoRoles() {
        return ssoRoles;
    }

    public void setSsoRoles(Collection<SSORoleVO> ssoRoles) {
        this.ssoRoles = ssoRoles;
    }

    public Collection<SSOPositionVO> getSsoPositions() {
        return ssoPositions;
    }

    public void setSsoPositions(Collection<SSOPositionVO> ssoPositions) {
        this.ssoPositions = ssoPositions;
    }

    public Integer getUseAttributeTraining() {
        return useAttributeTraining;
    }

    public void setUseAttributeTraining(Integer useAttributeTraining) {
        this.useAttributeTraining = useAttributeTraining;
    }

    public Integer getUseAttributeReporting() {
        return useAttributeReporting;
    }

    public void setUseAttributeReporting(Integer useAttributeReporting) {
        this.useAttributeReporting = useAttributeReporting;
    }

    public String getAttributeNameTraining() {
        return attributeNameTraining;
    }

    public void setAttributeNameTraining(String attributeNameTraining) {
        this.attributeNameTraining = attributeNameTraining;
    }

    public String getAttributeNameReporting() {
        return attributeNameReporting;
    }

    public void setAttributeNameReporting(String attributeNameReporting) {
        this.attributeNameReporting = attributeNameReporting;
    }

    public String getAttributeValueTraining() {
        return attributeValueTraining;
    }

    public void setAttributeValueTraining(String attributeValueTraining) {
        this.attributeValueTraining = attributeValueTraining;
    }

    public String getAttributeValueReporting() {
        return attributeValueReporting;
    }

    public void setAttributeValueReporting(String attributeValueReporting) {
        this.attributeValueReporting = attributeValueReporting;
    }

    public Integer getSelfConfRoles() {
        return selfConfRoles;
    }

    public void setSelfConfRoles(Integer selfConfRoles) {
        this.selfConfRoles = selfConfRoles;
    }

    public Integer getSelfConfLocations() {
        return selfConfLocations;
    }

    public void setSelfConfLocations(Integer selfConfLocations) {
        this.selfConfLocations = selfConfLocations;
    }

    public Integer getSelfConfPositions() {
        return selfConfPositions;
    }

    public void setSelfConfPositions(Integer selfConfPositions) {
        this.selfConfPositions = selfConfPositions;
    }

    public Integer getOnlyMainField() {
        return onlyMainField;
    }

    public void setOnlyMainField(Integer onlyMainField) {
        this.onlyMainField = onlyMainField;
    }

    public Integer getAllowSSOUpdate() {
        return allowSSOUpdate;
    }

    public void setAllowSSOUpdate(Integer allowSSOUpdate) {
        this.allowSSOUpdate = allowSSOUpdate;
    }
    
}
