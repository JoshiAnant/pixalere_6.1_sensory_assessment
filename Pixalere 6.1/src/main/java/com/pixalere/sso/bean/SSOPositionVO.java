package com.pixalere.sso.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 * This is the sso_positions (DB table) bean 
 * Each entry corresponds to an instance of {@link com.pixalere.sso.bean.SSOProviderVO },
 * attributeValue is the value sent by the IdentityProvider, contained 
 * in the field named SSOProviderVO.ssoPositions, each entry corresponds
 * to one Pixalere defined position like nurse, doctor, etc., used
 * to set the direction of Referrals and Recommendations during assessments;
 * the attribute_value indicates that the user with that value is to be
 * associated with the related position (positionId), related to the table 
 * user_positions {@link com.pixalere.auth.bean.PositionVO }.
 * .
 * 
 * @author Jose
 * @since 6.0
 */
public class SSOPositionVO extends ValueObject implements Serializable {
    private Integer ssoProviderid;
    private Integer positionId;
    private String attributeValue;
    private String title;
    
    public SSOPositionVO() {
    }

    public Integer getSsoProviderid() {
        return ssoProviderid;
    }

    public void setSsoProviderid(Integer ssoProviderid) {
        this.ssoProviderid = ssoProviderid;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
}
