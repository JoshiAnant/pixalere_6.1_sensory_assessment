package com.pixalere.sso.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.List;

/**
 * This is the sso_locations (DB table) bean 
 * Each entry corresponds to an instance of {@link com.pixalere.sso.bean.SSOProviderVO },
 * attributeValue is the value sent by the IdentityProvider, contained 
 * in the field named SSOProviderVO.attributeLocations, while the array of 
 * ids in this class stored in pixLocationsids corresponds to one or more 
 * Pixalere defined locations that should be given access to the user which 
 * has the corresponding attributeValue.
 * .
 * 
 * @author Jose
 * @since 6.0
 */
public class SSOLocationVO extends ValueObject implements Serializable {
    private Integer ssoProviderid;
    private String attributeValue;
    private String pixLocationsids;

    // Transient field, display list of location names
    private String titles;
    
    public SSOLocationVO() {
    }

    public Integer getSsoProviderid() {
        return ssoProviderid;
    }

    public void setSsoProviderid(Integer ssoProviderid) {
        this.ssoProviderid = ssoProviderid;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getPixLocationsids() {
        return pixLocationsids;
    }

    public void setPixLocationsids(String pixLocationsids) {
        this.pixLocationsids = pixLocationsids;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }
    
}
