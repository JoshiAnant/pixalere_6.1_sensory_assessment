package com.pixalere.sso.endpoint;

import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.sso.bean.SSOAttribute;
import com.pixalere.sso.bean.SSOProviderVO;
import com.pixalere.sso.service.OpenSAMLService;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSString;

/**
 *
 * @author Jose
 */
public class SSOConsumerPost extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.sso.endpoint.SSOConsumerPost.class);
    
    /**
     * Consumer POST URI, it must match the declared action path 
     * for this controller at the struts config file 
     * /WEB-INF/struts-config.xml.
     */
    public static String CONSUMER_URI = "SSOConsumerPost";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        // If Logged already, go to home page
        if (session.getAttribute("userVO") != null) {
            return (mapping.findForward("login.success"));
        }
        
        OpenSAMLService openSAMLService = new OpenSAMLService();
        // Get current active configuration
        SSOProviderVO ssoConfig = openSAMLService.getSsoConfig();
        // If SSO not enabled OR not enabled Provider --> return to login
        if (Common.getConfig("enableSSO") != null && Common.getConfig("enableSSO").equals("1")){
            if (ssoConfig == null || ssoConfig.getActive().equals(0)){
                return (mapping.findForward("go.login"));
            }
        } else {
            return (mapping.findForward("go.login"));
        }
        
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        
        // Extract & Validate Assertion 
        Assertion assertion = openSAMLService.getAssertionfromPost(request);
        
        // If not assertion was found, go to error page
        if (assertion == null){
            // SET display error MSG - No data for login found
            request.setAttribute("errorCause", Common.getLocalizedString("sso.loginerror.no_data", locale));
            setConfigInRequest(request);
            return (mapping.findForward("sso.failure"));
        }
        
        // If the signature validation failed, go to error page
        if (ssoConfig.getIsAuthnResponseSigned() == 1 && !openSAMLService.isValidSignature()) {
            // SET display error MSG - The signature is not valid
            request.setAttribute("errorCause", Common.getLocalizedString("sso.loginerror.no_valid_signature", locale));
            setConfigInRequest(request);
            return (mapping.findForward("sso.failure"));
        }
        
        // Convert the assertion to fields
        Map<String, SSOAttribute> ssoAttributes = OpenSAMLService.getAttributesFromAssertion(assertion);
        
        boolean debugEnabled = Common.getConfig("enableSSODebug") != null && Common.getConfig("enableSSODebug").equals("1");
        if (debugEnabled) {
            for (SSOAttribute attr : ssoAttributes.values()) {
                System.out.println(attr);
            }
        }
        
        // Can access Pix?
        boolean hasPixSetting = OpenSAMLService.ssoAttributeValueExists(ssoConfig.getAttributePixauth(), ssoConfig.getAttributeValPixauth(), ssoAttributes);
        
        if (!hasPixSetting) {
            // SET display error MSG - User without access to Pixalere
            request.setAttribute("errorCause", Common.getLocalizedString("sso.loginerror.no_access_pix", locale) 
                    + " [" + ssoConfig.getAttributePixauth() + ":" + ssoConfig.getAttributeValPixauth() + "]");
            request.setAttribute("ssoFields", ssoAttributes);
            setConfigInRequest(request);
            return (mapping.findForward("sso.failure"));
        }
               
        String ssoUniqueAttribute;
        if (ssoConfig.getMainField().equals("username")) {
            ssoUniqueAttribute = ssoConfig.getAttributeUsername();
        } else {
            ssoUniqueAttribute = ssoConfig.getAttributeEmail();
        }
        
        // Find the corresponding Pix user of the Assertion
        SSOAttribute pixUniqueField = ssoAttributes.get(ssoUniqueAttribute);
        
        if (pixUniqueField == null){
            // SET display error MSG - User id missing
            request.setAttribute("errorCause", Common.getLocalizedString("sso.loginerror.missing_pix_id", locale) + "[" + ssoUniqueAttribute + "]");
            request.setAttribute("ssoFields", ssoAttributes);
            setConfigInRequest(request);
            return (mapping.findForward("sso.failure"));
        }
        
        String userId = pixUniqueField.getValue();
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        ProfessionalVO user;
        if (ssoConfig.getMainField().equals("username")) {
            // By user name
            user = userBD.getProfessionalByUsernameOrId(userId);
        } else {
            // By email
            ProfessionalVO userVO = new ProfessionalVO();
            userVO.setEmail(userId);
            userVO.setAccount_status(1);
            user = userBD.getProfessional(userVO);
        }
        
        // Pix user found
        if (user != null){
            // Validate if complete
            List<String> missList = OpenSAMLService.findMissingSettingsProfessionalVOSetup(user, ssoConfig);
            
            // Ignore all except main ID
            if (ssoConfig.getOnlyMainField() == 1) {
                // User is fully configured?
                if (missList.isEmpty()){
                    // Case #1 Simplest SSO, just find, verify and login
                    // LOGIN
                    if (debugEnabled) System.out.println("*** SSO Case #1 Login");
                    startSession(user, session, language, locale);
                    logSSOLogin(request, user, locale);
                    return (mapping.findForward("login.success"));
                } else {
                    // Case #2 Simple SSO - The User is not properly setup (rare case)
                    // Incomplete conf - send to partial warning page
                    List<String> partialSections = new ArrayList<String>();
                    for (String missitem : missList) {
                        partialSections.add(Common.getLocalizedString(missitem, locale));
                    }
                    if (debugEnabled) System.out.println("*** SSO Case #2 Partial");
                    request.setAttribute("pix_user_id", userId);
                    request.setAttribute("partialSections", partialSections);
                    setConfigInRequest(request);
                    return (mapping.findForward("sso.partial"));
                }
            } else {
                // We need to take into account other settings
                // User is fully configured?
                if (missList.isEmpty()){
                    // It is, can we override some settings?
                    if (ssoConfig.getAllowSSOUpdate() != null && ssoConfig.getAllowSSOUpdate() == 1){
                        // Case #3 User OK plus update with SSO settings
                        try {
                            user = openSAMLService.updateProfessionalVOWithSSOAttributes(user, ssoAttributes);
                        } catch (ApplicationException e) {
                            log.error("An application exception has been raised in SSOConsumerPost [Case #3]: " + e.toString());
                            ActionErrors errors = new ActionErrors();
                            request.setAttribute("exception", "y");
                            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                            errors.add("exception", new ActionError("pixalere.common.error"));
                            saveErrors(request, errors);
                            return (mapping.findForward("pixalere.error"));
                        }
                        // Login
                        if (debugEnabled) System.out.println("*** SSO Case #3 Login and update");
                        startSession(user, session, language, locale);
                        logSSOLogin(request, user, locale);
                        return (mapping.findForward("login.success"));
                        
                    } else {
                        // Case #4 SSO with params - User is OK, but SSO update is not allowed
                        // LOGIN
                        if (debugEnabled) System.out.println("*** SSO Case #4 Login - noUpdate");
                        startSession(user, session, language, locale);
                        logSSOLogin(request, user, locale);
                        return (mapping.findForward("login.success"));
                    }
                } else {
                    // User is NOT fully configurated, can we try to complete it using SSO?
                    if (ssoConfig.getAllowSSOUpdate() != null && ssoConfig.getAllowSSOUpdate() == 1){
                        // Update using SSO
                        // Case #5 and 6 Update VO with SSO settings
                        if (debugEnabled) System.out.println("*** SSO Case #5, Case #6 - update ProfessionalVO - id:" + user.getId());
                        try {
                            user = openSAMLService.updateProfessionalVOWithSSOAttributes(user, ssoAttributes);
                        } catch (ApplicationException e) {
                            log.error("An application exception has been raised in SSOConsumerPost [Case #5 and 6]: " + e.toString());
                            ActionErrors errors = new ActionErrors();
                            request.setAttribute("exception", "y");
                            request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                            errors.add("exception", new ActionError("pixalere.common.error"));
                            saveErrors(request, errors);
                            return (mapping.findForward("pixalere.error"));
                        }
                        // Reload
                        user = userBD.getProfessionalByUsernameOrId(user.getId().toString());
                        // Is complete now?
                        missList = OpenSAMLService.findMissingSettingsProfessionalVOSetup(user, ssoConfig);
                        if (missList.isEmpty()){
                            // Case #5 Completed user
                            // LOGIN
                            if (debugEnabled) System.out.println("*** SSO Case #5 Login");
                            startSession(user, session, language, locale);
                            logSSOLogin(request, user, locale);
                            return (mapping.findForward("login.success"));
                        } else {
                            // Case #6 - The User is still not properly setup (rare case)
                            // Incomplete conf - send to partial warning page
                            if (debugEnabled) System.out.println("*** SSO Case #6 Partial");
                            List<String> partialSections = new ArrayList<String>();
                            for (String missitem : missList) {
                                partialSections.add(Common.getLocalizedString(missitem, locale));
                            }
                            
                            request.setAttribute("pix_user_id", userId);
                            request.setAttribute("partialSections", partialSections);
                            setConfigInRequest(request);
                            return (mapping.findForward("sso.partial"));
                        }
                    } else {
                        // We cannot update, send to Error page
                        // Case #7 - The User is not properly setup and we cannot try to complete it
                        // Possibly an user created partially by SSO with still pending configuration
                        if (debugEnabled) System.out.println("*** SSO Case #7 Partial");
                        List<String> partialSections = new ArrayList<String>();
                        for (String missitem : missList) {
                            partialSections.add(Common.getLocalizedString(missitem, locale));
                        }
                        request.setAttribute("pix_user_id", userId);
                        request.setAttribute("partialSections", partialSections);
                        setConfigInRequest(request);
                        return (mapping.findForward("sso.partial"));
                        
                    } // End: If not fully can we complete
                } // End: Is fully config   
            } // End: only main settings
        } else {
            // User doesn't exist
            // Can we create a new one?
            if (Common.getConfig("createUsersSSO") != null && Common.getConfig("createUsersSSO").equals("1")){
                
                List<String> errorDetails = OpenSAMLService.findMissingAttributesSSOAttributes(ssoAttributes, ssoConfig, locale);
                if (errorDetails.isEmpty()){
                    // Create User
                    ProfessionalVO newUser = openSAMLService.createProfessionalVO(ssoAttributes);
                    // Reload User (we need id)
                    ProfessionalVO userSaved = userBD.getProfessionalByUsernameOrId(newUser.getUser_name());
                    if (debugEnabled) System.out.println("*** SSO Case #8, Case #9 - CREATE NEW USER - New ProfessionalVO created id:" + userSaved.getId());
                    
                    // Update User
                    // Case #8, Case #9 New User VO with SSO settings
                    try {
                        userSaved = openSAMLService.updateProfessionalVOWithSSOAttributes(userSaved, ssoAttributes);
                    } catch (ApplicationException e) {
                        log.error("An application exception has been raised in SSOConsumerPost update Professional [Case #8 or #9]: " + e.toString());
                        ActionErrors errors = new ActionErrors();
                        request.setAttribute("exception", "y");
                        request.setAttribute("exception_log", com.pixalere.utils.Common.buildException(e));
                        errors.add("exception", new ActionError("pixalere.common.error"));
                        saveErrors(request, errors);
                        return (mapping.findForward("pixalere.error"));
                    }
                    
                    // The user has all the settings for login?
                    List<String> missList = OpenSAMLService.findMissingSettingsProfessionalVOSetup(userSaved, ssoConfig);
                    if (missList.isEmpty()){
                        // Case #8 Completed new user
                        // LOGIN
                        if (debugEnabled) System.out.println("*** SSO Case #8 Login!");
                        startSession(userSaved, session, language, locale);
                        logSSOLogin(request, userSaved, locale);
                        return (mapping.findForward("login.success"));
                    } else {
                        // Case #9 - The new User is still not properly setup
                        // Incomplete conf - send to partial warning page
                        if (debugEnabled) System.out.println("*** SSO Case #9");
                        List<String> partialSections = new ArrayList<String>();
                        for (String missitem : missList) {
                            partialSections.add(Common.getLocalizedString(missitem, locale));
                        }
                        request.setAttribute("pix_user_id", userId);
                        request.setAttribute("partialSections", partialSections);
                        setConfigInRequest(request);
                        return (mapping.findForward("sso.partial"));
                    }
                    
                } else {
                    // Case #10 - Not enough attributes to create a user with the current configuration
                    // Set cause
                    if (debugEnabled) System.out.println("*** SSO Case #10");
                    request.setAttribute("errorCause", Common.getLocalizedString("sso.loginerror.missing_attributes", locale));
                    request.setAttribute("errorDetails", errorDetails);
                    request.setAttribute("ssoFields", ssoAttributes);
                    setConfigInRequest(request);
                    return (mapping.findForward("sso.failure"));
                }
            } else {
                // Case #11 - User (id) unknown, cannot create new users
                if (debugEnabled) System.out.println("*** SSO Case #11");
                request.setAttribute("errorCause", Common.getLocalizedString("sso.loginerror.user_unknown", locale) + "[" + userId + "]");
                request.setAttribute("ssoFields", ssoAttributes);
                setConfigInRequest(request);
                return (mapping.findForward("sso.failure"));
                        
            }
        }      
    }
    /**
     * We are outside session, in order to display properly the error
     * and partial pages, we need to provide the config object.
     * 
     * @param request 
     */
    private void setConfigInRequest(HttpServletRequest request){
        // Session is no longer present - Set config in the request
        Hashtable config = new Hashtable();
        ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
        try {
            config = manager.getConfiguration();
        } catch (ApplicationException e) {
            log.error("DataAccessException in retrieveConfiguration: " + e.toString());
            System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());

            e.printStackTrace();
        }
        request.setAttribute("config", config);
    }
    
    private void startSession(ProfessionalVO user, HttpSession session, Integer language, String locale){
        session.setAttribute("userVO", user);
        session.setAttribute(Common.SESSION_LANGUAGE, language);
        java.util.Locale localee = new java.util.Locale(locale);
        session.setAttribute(org.apache.struts.Globals.LOCALE_KEY, localee);
        // Set config data
        ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
        Hashtable config = new Hashtable();
        try {
            config = manager.getConfiguration();
            Common.CONFIG = config;
        } catch (ApplicationException e) {
            log.error("DataAccessException in retrieveConfiguration SSOConsumerPost.startSession: " + e.toString());
            System.out.println("DataAccessException in retrieveConfiguration SSOConsumerPost.startSession: " + e.toString());

            e.printStackTrace();
        }
        session.setAttribute("config", config);

    }
    
    private void logSSOLogin(HttpServletRequest request, ProfessionalVO user, String locale){
        PDate pdate = new PDate(user != null ? user.getTimezone() : Constants.TIMEZONE);

        LogAuditServiceImpl bd = new LogAuditServiceImpl();

        LogAuditVO vo = new LogAuditVO(user.getId(), new Integer(0), Constants.LOGGED_IN, request.getRemoteAddr());
        vo.setDescription(request.getHeader("User-Agent"));
        try {
            bd.saveLogAudit(vo);
        } catch (ApplicationException e) {
            System.out.println("ApplicationException - Saving log for by SSO user id: " + user.getId());
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String strHour = Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
        while (strHour.length() < 2) {
            strHour = '0' + strHour;
        }
        String strMinute = Integer.toString(calendar.get(Calendar.MINUTE));
        while (strMinute.length() < 2) {
            strMinute = '0' + strMinute;
        }
        String strSeconds = Integer.toString(calendar.get(Calendar.SECOND));
        while (strSeconds.length() < 2) {
            strSeconds = '0' + strSeconds;
        }
        try {
            // Equivalent log line as in the normal login:
            // DON'T REMOVE THE FOLLOWING LINE PLEASE, WE NEED IT IN THE LOG
            System.out.println(pdate.getDateString(PDate.getDate(calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.YEAR)), locale) + " " + strHour + ":" + strMinute + ":" + strSeconds + " -- Logon to Live by user " + user.getId() + " -- SSO");
        } catch (ApplicationException e) {
            System.out.println("ApplicationException - Printing log in by SSO user id: " + user.getId());
            e.printStackTrace();
        }
    }
    
    /* PRIVATE LOG DEBUG METHODS */
    private void logAuthenticationMethod(Assertion assertion) {
        System.out.println("Authentication method: " + assertion.getAuthnStatements().get(0)
                .getAuthnContext().getAuthnContextClassRef().getAuthnContextClassRef());
    }

    private void logAuthenticationInstant(Assertion assertion) {
        System.out.println("Authentication instant: " + assertion.getAuthnStatements().get(0).getAuthnInstant());
    }

    private void logAssertionAttributes(Assertion assertion) {
        for (Attribute attribute : assertion.getAttributeStatements().get(0).getAttributes()) {
            System.out.println("Attribute name: " + attribute.getName());
            for (XMLObject attributeValue : attribute.getAttributeValues()) {
                System.out.println("Attribute value: " + ((XSString) attributeValue).getValue());
            }
        }
    }
}
