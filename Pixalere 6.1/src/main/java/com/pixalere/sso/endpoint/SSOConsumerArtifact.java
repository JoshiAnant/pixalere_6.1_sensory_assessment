package com.pixalere.sso.endpoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 */
public class SSOConsumerArtifact extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.sso.endpoint.SSOConsumerArtifact.class);
    
    /**
     * Consumer Artifact URI, it must match the declared action path 
     * for this controller at the struts config file 
     * /WEB-INF/struts-config.xml.
     */
    public static String CONSUMER_URI = "SSOConsumerArtifact";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //<forward name="login.success" path="/HomeSetup.do" />
        //<forward name="sso.failure" path="/vm/sso/ssofailure.vm"/>
        
        // TODO: SAML2 Artifact Binding Implementation - Outline:
        
        // Receive POST from IdP containing Artifact
        
        // Extract Artifact
        
        // Resolve artifact - Synchronous SOAP call to IdP
        
        // Retrieve Assertion
        
        // From here, same process than SSOConsumerPost implementation
        
        // Extract & Validate Assertion 
            // Not valid: failure
        
        
        // Find the corresponding user of the Assertion
        
        
        // No user found - Can we create a new one?
        
            // Yes Map assertions properties to user properties
            // Try to Create new user
        
        
        // Do we have an user ?
            // Yes: Authenticate (Populate session)
        
            // TMP return: Redirect to login page
            return (mapping.findForward("go.login"));
            
            // No: Error authenticating / Can't create Users --> failure
    }
    
}
