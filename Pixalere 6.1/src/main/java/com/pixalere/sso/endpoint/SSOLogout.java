package com.pixalere.sso.endpoint;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Jose
 */
public class SSOLogout extends Action {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(com.pixalere.sso.endpoint.SSOLogout.class);
    
    /**
     * SSO Logout URI, it must match the declared action path 
     * for this controller at the struts config file 
     * /WEB-INF/struts-config.xml.
     */
    public static String LOGOUT_URI = "SSOLogout";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        // TODO: If Universal Logout enabled AND we're NOT coming from an IdP redirect
            //Yes: Do Redirect to Idp to logout there too
        
        // Clean up session 
        HttpSession session = request.getSession();
        session.removeAttribute("referral");
        session.removeAttribute("config");
        session.removeAttribute("userVO");
        Common.invalidatePatientSession(session);
        session.invalidate();
        
        if(request.getAttribute("session_invalid")!=null){
            request.setAttribute("session_invalid",request.getAttribute("session_invalid"));
        }
        
        // Session is no longer present - Set config in the request
        Hashtable config = new Hashtable();
        ConfigurationServiceImpl manager = new ConfigurationServiceImpl();
        try {
            config = manager.getConfiguration();
        } catch (ApplicationException e) {
            log.error("DataAccessException in retrieveConfiguration: " + e.toString());
            System.out.println("DataAccessException in retrieveConfiguration: " + e.toString());

            e.printStackTrace();
        }
        request.setAttribute("config", config);
        
        // Display SSO logout 
        return (mapping.findForward("ssologout.success"));
    }
    
}
