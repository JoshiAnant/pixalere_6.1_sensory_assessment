package com.pixalere.assessment.service;
import com.pixalere.common.ApplicationException;
import java.util.Collection;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface AssessmentCommentsService {
    public AssessmentCommentsVO getComment(@WebParam(name="criteria") AssessmentCommentsVO criteria) throws ApplicationException;
    public void saveComment(@WebParam(name="updateRecord") AssessmentCommentsVO updateRecord) throws ApplicationException;
    public Collection<AssessmentCommentsVO> getAllComments(@WebParam(name="assessmentComment") AssessmentCommentsVO assessmentComments) throws ApplicationException;
}