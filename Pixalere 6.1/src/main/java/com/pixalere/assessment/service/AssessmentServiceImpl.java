package com.pixalere.assessment.service;

import com.pixalere.guibeans.RowData;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.dao.*;
import com.pixalere.common.*;
import com.pixalere.common.bean.AutoReferralVO;
import java.text.DecimalFormat;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Date;
import com.pixalere.utils.*;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import java.util.Iterator;
import java.util.Collection;
import java.util.Hashtable;
import java.util.ArrayList;
import com.pixalere.integration.bean.EventVO;
import com.pixalere.integration.dao.IntegrationDAO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.service.WoundServiceImpl;
import javax.jws.WebService;

/**
 * This is the assessment service implementation Refer to {@link com.pixalere.assessment.service.AssessmentService
 * } to see if there are any web services available.
 * <p/>
 * <img src="AssessmentServiceImpl.png"/>
 *
 * @view AssessmentServiceImpl
 * @match class com.pixalere.assessment.
 *
 * @opt hide
 * @match class
 * com.pixalere.assessment\.(dao.AssessmentDAO|bean.AssessmentIncisionVO|bean.AssessmentDrainVO|bean.AssessmentOstomyVO|bean.AssessmentEachwoundVO|service.AssessmentService)
 * @opt !hide
 */
@WebService(endpointInterface = "com.pixalere.assessment.service.AssessmentService", serviceName = "AssessmentService")
public class AssessmentServiceImpl implements AssessmentService {
    // Create Log4j category instance for logging

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentServiceImpl.class);
    AssessmentDAO dao = null;
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    private Integer language = 1;
    private String locale;

    public AssessmentServiceImpl(Integer language) {
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
        dao = new AssessmentDAO();
    }

    public AssessmentServiceImpl() {
    }

    /**
     * Updates the assessment record to the assessment_eachwound or
     * assessment_ostomy table.
     */
    public void saveAssessment(AbstractAssessmentVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.update(vo);
            if (vo instanceof AssessmentEachwoundVO) {
                AssessmentEachwoundVO v = (AssessmentEachwoundVO) vo;
                AssessmentEachwoundVO new_id = (AssessmentEachwoundVO) getAssessment(v);
                if (new_id != null && new_id.getWoundAssessment() != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new_id.getWoundAssessment().getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_wound", new_id));
                }
            } else if (vo instanceof AssessmentOstomyVO) {
                AssessmentOstomyVO v = (AssessmentOstomyVO) vo;
                AssessmentOstomyVO new_id = (AssessmentOstomyVO) getAssessment(v);
                if (new_id != null && new_id.getWoundAssessment() != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new_id.getWoundAssessment().getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_ostomy", new_id));
                }
            } else if (vo instanceof AssessmentIncisionVO) {
                AssessmentIncisionVO v = (AssessmentIncisionVO) vo;
                AssessmentIncisionVO new_id = (AssessmentIncisionVO) getAssessment(v);
                if (new_id != null && new_id.getWoundAssessment() != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new_id.getWoundAssessment().getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_incision", new_id));
                }
            } else if (vo instanceof AssessmentDrainVO) {
                AssessmentDrainVO v = (AssessmentDrainVO) vo;
                AssessmentDrainVO new_id = (AssessmentDrainVO) getAssessment(v);
                if (new_id != null && new_id.getWoundAssessment() != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new_id.getWoundAssessment().getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_drain", new_id));
                }
            } else if (vo instanceof AssessmentBurnVO) {
                AssessmentBurnVO v = (AssessmentBurnVO) vo;
                AssessmentBurnVO new_id = (AssessmentBurnVO) getAssessment(v);
                if (new_id != null && new_id.getWoundAssessment() != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new_id.getWoundAssessment().getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_drain", new_id));
                }
            } else if (vo instanceof AssessmentSkinVO) {
                AssessmentSkinVO v = (AssessmentSkinVO) vo;
                AssessmentSkinVO new_id = (AssessmentSkinVO) getAssessment(v);
                if (new_id != null && new_id.getWoundAssessment() != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(new_id.getWoundAssessment().getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_skin", new_id));
                }
            }

            WoundAssessmentServiceImpl aservice = new WoundAssessmentServiceImpl();
            aservice.checkAutoReferralsForOffline(vo.getAssessment_id(), vo.getWound_profile_type_id(), language);

        } catch (DataAccessException e) {
            throw new ApplicationException("saveAssessment has thrown an error:" + e.getMessage());
        }
    }

    public void saveNursingFixes(NursingFixesVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.insertNursingFixes(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public Collection<AssessmentProductVO> getAllProducts(AssessmentProductVO assess) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    /**
     * Returns true/false if the treatment step has been accessed, and product
     * or comment has been entered
     * <p/>
     *
     * @param assessment_id the id of the wound_assessment record we are
     * checking the for.
     * @return if the treatment page has been completed
     */
    public boolean verifyTreatmentCompletion(int assessment_id) throws ApplicationException {
        boolean ifDataExists = false;//if data exists, continue to summary pge.
        try {
            Collection<AbstractAssessmentVO> assessments = getAllAssessments(assessment_id);
            TreatmentCommentsServiceImpl tService = new TreatmentCommentsServiceImpl();
            if (assessments.size() == 0) {// If no assessments are done, then no treatment needed.
                ifDataExists = true;
            }
            int num_closed = 0;
            for (AbstractAssessmentVO eachAssessment : assessments) {
                //If it doesn't find any products completed or a treatment plan,then they need to be returned
                //to the treatment page to complete them.
                TreatmentCommentVO tc = new TreatmentCommentVO();
                tc.setAssessment_id(eachAssessment.getAssessment_id());
                tc.setWound_profile_type_id(eachAssessment.getWound_profile_type_id());
                tc = tService.getTreatmentComment(tc);
                AssessmentProductVO tp = new AssessmentProductVO();
                tp.setAssessment_id(eachAssessment.getAssessment_id());
                tp.setAlpha_id(eachAssessment.getAlpha_id());
                Collection<AssessmentProductVO> products = getAllProducts(tp);
                if ((products != null && products.size() != 0) || (tc != null && tc.getBody() != null && !tc.getBody().equals(""))) {
                    ifDataExists = true;
                }
                if (eachAssessment.getStatus() != null && (eachAssessment.getStatus().equals(new Integer(Common.getConfig("woundClosed"))) || eachAssessment.getStatus().equals(new Integer(Common.getConfig("incisionClosed"))) || eachAssessment.getStatus().equals(new Integer(Common.getConfig("drainClosed"))) || eachAssessment.getStatus().equals(new Integer(Common.getConfig("ostomyClosed"))) || eachAssessment.getStatus().equals(new Integer(Common.getConfig("ostomyHold"))))) {
                    num_closed++;
                }
            }
            if (num_closed == assessments.size()) {
                ifDataExists = true;
            }
        } catch (ApplicationException e) {
            log.error("Exception occurred in verifyTreatmentPageCompletion" + e.getMessage());
            throw new ApplicationException("Exception occurred in verifyTreatmentPageCompletion" + e.getMessage(), e);
        }
        return ifDataExists;
    }

    /**
     * Returns all AssessmentEachwoundVO objects that can be shown on the
     * screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param primaryKey the assessment_id of the assessment_eachwound record
     * @return the assessment_eachwound records
     * @see AssessmentEachwoundVO
     */
    public Collection getAllAssessments(int assessment_id) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            AssessmentEachwoundVO wnd = new AssessmentEachwoundVO();
            AssessmentOstomyVO ost = new AssessmentOstomyVO();
            AssessmentIncisionVO ins = new AssessmentIncisionVO();
            AssessmentSkinVO skn = new AssessmentSkinVO();
            AssessmentDrainVO drn = new AssessmentDrainVO();
            AssessmentBurnVO brn = new AssessmentBurnVO();
            ins.setAssessment_id(assessment_id);
            wnd.setAssessment_id(assessment_id);
            skn.setAssessment_id(assessment_id);
            ost.setAssessment_id(assessment_id);
            drn.setAssessment_id(assessment_id);
            brn.setAssessment_id(assessment_id);
            Collection assessments1 = woundAssessmentDAO.findAllByCriteria(wnd, "alpha_id", false, 0);
            Collection assessments2 = woundAssessmentDAO.findAllByCriteria(ost, "alpha_id", false, 0);
            Collection assessments3 = woundAssessmentDAO.findAllByCriteria(ins, "alpha_id", false, 0);
            Collection assessments4 = woundAssessmentDAO.findAllByCriteria(drn, "alpha_id", false, 0);
            Collection assessments5 = woundAssessmentDAO.findAllByCriteria(brn, "alpha_id", false, 0);
            Collection assessments6 = woundAssessmentDAO.findAllByCriteria(skn, "alpha_id", false, 0);
            if (assessments1 != null && assessments1.size() > 0) {
                assessments1.addAll(assessments2);
                assessments1.addAll(assessments3);
                assessments1.addAll(assessments4);
                assessments1.addAll(assessments5);
                assessments1.addAll(assessments6);
                return assessments1;
            } else if (assessments2 != null && assessments2.size() > 0) {
                assessments2.addAll(assessments3);
                assessments2.addAll(assessments4);
                assessments2.addAll(assessments5);
                assessments2.addAll(assessments6);
                return assessments2;
            } else if (assessments3 != null && assessments3.size() > 0) {
                assessments3.addAll(assessments4);
                assessments3.addAll(assessments5);
                assessments3.addAll(assessments6);
                return assessments3;
            } else if (assessments4 != null && assessments4.size() > 0) {
                assessments4.addAll(assessments5);
                assessments4.addAll(assessments6);
                return assessments4;
            } else if (assessments5 != null && assessments5.size() > 0) {
                assessments5.addAll(assessments6);
                return assessments5;
            } else {
                return assessments6;
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    /**
     * Finds the top priority of individual assessment_eachwounds by
     * wound_assessment.id.
     *
     * @param assessment_id the id from wound_assessment.
     * @return int the top priority from assessment_eachwound
     * @since 3.0
     */
    public int getTopPriority(int assessment_id) {
        int top_priority = 300000;
        try {
            Collection col = getAllAssessments(assessment_id);
            Iterator iter = col.iterator();
            while (iter.hasNext()) {
                AbstractAssessmentVO tmpVO = (AbstractAssessmentVO) iter.next();
                if (tmpVO.getPriority().intValue() < top_priority) {
                    top_priority = tmpVO.getPriority().intValue();
                }
            }
        } catch (Exception ex) {
        }
        if (top_priority == 300000) {
            top_priority = 0;
        }
        return top_priority;
    }

    public void mergeAssessments(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        AssessmentEachwoundVO i = new AssessmentEachwoundVO();
        com.pixalere.integration.dao.IntegrationDAO integrationDAO = new com.pixalere.integration.dao.IntegrationDAO();
        i.setPatient_id(patient_from);
        Collection<AssessmentEachwoundVO> many = getAllAssessments(i, -1, false);
        for (AssessmentEachwoundVO a : many) {
            a.setPatient_id(patient_to);
            saveAssessment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_eachwound");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
        AssessmentDrainVO i2 = new AssessmentDrainVO();
        i2.setPatient_id(patient_from);
        Collection<AssessmentDrainVO> many2 = getAllAssessments(i2, -1, false);
        for (AssessmentDrainVO a : many2) {
            a.setPatient_id(patient_to);
            saveAssessment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_drain");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
        AssessmentIncisionVO i3 = new AssessmentIncisionVO();
        i3.setPatient_id(patient_from);
        Collection<AssessmentIncisionVO> many3 = getAllAssessments(i3, -1, false);
        for (AssessmentIncisionVO a : many3) {
            a.setPatient_id(patient_to);
            saveAssessment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_incision");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
        AssessmentOstomyVO i4 = new AssessmentOstomyVO();
        i4.setPatient_id(patient_from);
        Collection<AssessmentOstomyVO> many4 = getAllAssessments(i4, -1, false);
        for (AssessmentOstomyVO a : many4) {
            a.setPatient_id(patient_to);
            saveAssessment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_ostomy");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
        AssessmentBurnVO i5 = new AssessmentBurnVO();
        i5.setPatient_id(patient_from);
        Collection<AssessmentBurnVO> many5 = getAllAssessments(i5, -1, false);
        for (AssessmentBurnVO a : many5) {
            a.setPatient_id(patient_to);
            saveAssessment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_burn");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    /**
     * Returns all AssessmentEachwoundVO objects that can be shown on the
     * screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param vo the assessment_eachwound object for criteria
     * @param limit the # of records you want returned.
     * @return the assessment_eachwound records
     * @see AssessmentEachwoundVO
     */
    public Collection getAllAssessments(AbstractAssessmentVO assess,String sort_column,int limit, boolean blnDescending) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess, sort_column, blnDescending, limit);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    /**
     * Returns all AssessmentEachwoundVO objects that can be shown on the
     * screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param vo the assessment_eachwound object for criteria
     * @param limit the # of records you want returned.
     * @return the assessment_eachwound records
     * @see AssessmentEachwoundVO
     */
    public Collection getAllAssessments(AbstractAssessmentVO assess, int limit, boolean blnDescending) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess, "visited_on", blnDescending, limit);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    /**
     * Returns all Assessment objects that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param vo the assessment_eachwound object for criteria
     * @return the assessment_eachwound records
     * @see AssessmentEachwoundVO
     */
    public Collection<AssessmentEachwoundVO> getWoundAssessmentsForOffline(AbstractAssessmentVO assess) throws ApplicationException {
        ArrayList<AssessmentEachwoundVO> results = new ArrayList<AssessmentEachwoundVO>();
        AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
        AssessmentEachwoundVO assessFull = (AssessmentEachwoundVO) retrieveLastActiveAssessment(assess, Constants.WOUND_PROFILE_TYPE, 1);
        //get partial assessments.
        AssessmentEachwoundVO assessPart = (AssessmentEachwoundVO) retrieveLastActiveAssessment(assess, Constants.WOUND_PROFILE_TYPE, 0);
        if (assessFull != null) {
            results.add(assessFull);
        }
        if (assessPart != null) {
            if (assessFull == null || (assessPart.getWoundAssessment().getCreated_on() != null && assessFull.getWoundAssessment().getCreated_on() != null && assessPart.getWoundAssessment().getCreated_on().after(assessFull.getWoundAssessment().getCreated_on()))) {
                results.add(assessPart);
            }
        }
        return results;
    }

    public Collection<AssessmentDrainVO> getDrainAssessmentsForOffline(AbstractAssessmentVO assess) throws ApplicationException {
        ArrayList<AssessmentDrainVO> results = new ArrayList<AssessmentDrainVO>();
        AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
        AssessmentDrainVO assessFull = (AssessmentDrainVO) retrieveLastActiveAssessment(assess, Constants.TUBESANDDRAINS_PROFILE_TYPE, 1);
        //get partial assessments.
        AssessmentDrainVO assessPart = (AssessmentDrainVO) retrieveLastActiveAssessment(assess, Constants.TUBESANDDRAINS_PROFILE_TYPE, 0);
        if (assessFull != null) {
            results.add(assessFull);
        }
        if (assessPart != null) {
            if (assessFull == null || (assessPart.getWoundAssessment().getCreated_on() != null && assessFull.getWoundAssessment().getCreated_on() != null && assessPart.getWoundAssessment().getCreated_on().after(assessFull.getWoundAssessment().getCreated_on()))) {
                results.add(assessPart);
            }
        }
        return results;
    }

    public Collection<AssessmentIncisionVO> getIncisionAssessmentsForOffline(AbstractAssessmentVO assess) throws ApplicationException {
        ArrayList<AssessmentIncisionVO> results = new ArrayList<AssessmentIncisionVO>();
        AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
        AssessmentIncisionVO assessFull = (AssessmentIncisionVO) retrieveLastActiveAssessment(assess, Constants.INCISIONTAG_PROFILE_TYPE, 1);
        //get partial assessments.
        AssessmentIncisionVO assessPart = (AssessmentIncisionVO) retrieveLastActiveAssessment(assess, Constants.INCISIONTAG_PROFILE_TYPE, 0);
        if (assessFull != null) {
            results.add(assessFull);
        }
        if (assessPart != null) {
            if (assessFull == null || (assessPart.getWoundAssessment().getCreated_on() != null && assessFull.getWoundAssessment().getCreated_on() != null && assessPart.getWoundAssessment().getCreated_on().after(assessFull.getWoundAssessment().getCreated_on()))) {
                results.add(assessPart);
            }
        }
        return results;
    }

    public Collection<AssessmentOstomyVO> getOstomyAssessmentsForOffline(AbstractAssessmentVO assess) throws ApplicationException {
        ArrayList<AssessmentOstomyVO> results = new ArrayList<AssessmentOstomyVO>();
        AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
        AssessmentOstomyVO assessFull = (AssessmentOstomyVO) retrieveLastActiveAssessment(assess, Constants.OSTOMY_PROFILE_TYPE, 1);
        //get partial assessments.
        AssessmentOstomyVO assessPart = (AssessmentOstomyVO) retrieveLastActiveAssessment(assess, Constants.OSTOMY_PROFILE_TYPE, 0);
        if (assessFull != null) {
            results.add(assessFull);
        }
        if (assessPart != null) {
            if (assessFull == null || (assessPart.getWoundAssessment().getCreated_on() != null && assessFull.getWoundAssessment().getCreated_on() != null && assessPart.getWoundAssessment().getCreated_on().after(assessFull.getWoundAssessment().getCreated_on()))) {
                results.add(assessPart);
            }
        }
        return results;
    }

    public Collection<AssessmentBurnVO> getBurnAssessmentsForOffline(AbstractAssessmentVO assess) throws ApplicationException {
        ArrayList<AssessmentBurnVO> results = new ArrayList<AssessmentBurnVO>();
        AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
        AssessmentBurnVO assessFull = (AssessmentBurnVO) retrieveLastActiveAssessment(assess, Constants.BURN_PROFILE_TYPE, 1);
        //get partial assessments.
        AssessmentBurnVO assessPart = (AssessmentBurnVO) retrieveLastActiveAssessment(assess, Constants.BURN_PROFILE_TYPE, 0);
        if (assessFull != null) {
            results.add(assessFull);
        }
        if (assessPart != null) {
            if (assessFull == null || (assessPart.getWoundAssessment().getCreated_on() != null && assessFull.getWoundAssessment().getCreated_on() != null && assessPart.getWoundAssessment().getCreated_on().after(assessFull.getWoundAssessment().getCreated_on()))) {
                results.add(assessPart);
            }
        }
        return results;
    }

    public Collection<AssessmentSkinVO> getSkinAssessmentsForOffline(AbstractAssessmentVO assess) throws ApplicationException {
        ArrayList<AssessmentSkinVO> results = new ArrayList<AssessmentSkinVO>();
        AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
        AssessmentSkinVO assessFull = (AssessmentSkinVO) retrieveLastActiveAssessment(assess, Constants.SKIN_PROFILE_TYPE, 1);
        //get partial assessments.
        AssessmentSkinVO assessPart = (AssessmentSkinVO) retrieveLastActiveAssessment(assess, Constants.SKIN_PROFILE_TYPE, 0);
        if (assessFull != null) {
            results.add(assessFull);
        }
        if (assessPart != null) {
            if (assessFull == null || (assessPart.getWoundAssessment().getCreated_on() != null && assessFull.getWoundAssessment().getCreated_on() != null && assessPart.getWoundAssessment().getCreated_on().after(assessFull.getWoundAssessment().getCreated_on()))) {
                results.add(assessPart);
            }
        }
        return results;
    }
    /*
     * Returns all Signaturs and Assessment_id's by alpha
     *
     * @return Vector returns a vector of object array's which contain signatures
     * @param vo the assessment-eachwound object for retrieval criteria
     * @param limit the # of records we want returned, 0 means all
     *
     * @since 3.2
     */

    public Vector getAllAssessmentSignaturesByAlpha(AbstractAssessmentVO assess, int limit) throws ApplicationException {
        Vector t = new Vector();
        try {
            AssessmentDAO dao = new AssessmentDAO();
            Collection<AbstractAssessmentVO> results = getAllAssessments(assess, 0, Constants.DESC_ORDER);
            int count = 0;
            for (AbstractAssessmentVO aTMPVO : results) {
                WoundAssessmentVO assessVO = aTMPVO.getWoundAssessment();
                //limit is used to ignore first X records (specifically for  flowcharts)
                if (limit < count && assessVO != null && assessVO.getActive().intValue() == 1) {
                    Object[] o = {assessVO.getUser_signature(), aTMPVO.getId() + ""};
                    t.add(o);
                }
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return t;
    }

    public Collection getNursingFixes(String id, String field) throws ApplicationException {
        Collection results = null;
        try {
            AssessmentDAO dao = new AssessmentDAO();
            results = dao.findNursingFixes(id, field);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return results;
    }
    /*
     * getAllOstomyAssessments gets all records for ostomy wounds.  The data is orgnaized
     * to be presented within the GUi flowcharts.  The reason it was done this way is to cause
     * less changes when it comes down to customization time for the clients.  This may change
     * in the next release when this will be less ofa  concern.
     *
     * @return Vector the flowchart columns
     * @param
     * @param limit # of records required
     * @param id the assessment record to be shown in the history column
     * @since 3.2
     */

    public RowData[] getAllOstomyAssessmentsForFlowchart(Collection<AssessmentOstomyVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] assessments = null;
        try {
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            AssessmentNPWTServiceImpl nService = new AssessmentNPWTServiceImpl();
            GUIServiceImpl gui = new GUIServiceImpl();
            AssessmentDAO dao = new AssessmentDAO();
            WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
            ComponentsVO comTMP = new ComponentsVO();
            TreatmentCommentsServiceImpl tService = new TreatmentCommentsServiceImpl();
            
            assessments = new RowData[results.size()];
            comTMP.setFlowchart(Constants.OSTOMY_ASSESSMENT);
            Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
            PDate pdate = new PDate(currentProfessional != null ? currentProfessional.getTimezone() : Constants.TIMEZONE);
            int cnt = 2;
            boolean[] allow_edit_closed = new boolean[results.size()];
            for (ComponentsVO component : components) {
                if (component.getHide_flowchart() == 0) {
                    String title = Common.getLocalizedString(component.getLabel_key(), locale);
                    int extend_count = 0;
                    int count = 0;
                    for (AssessmentOstomyVO wound : results) {
                        WoundAssessmentLocationVO alpha = wound.getAlphaLocation();
                        //Common.alphaStore.put(alpha.getAlpha(),alpha);
                        ProfessionalVO userVO = null;
                        WoundAssessmentVO wnd = wound.getWoundAssessment();
                        userVO = wnd.getProfessional();
                        int col1_closed = 0;
                        int col2_closed = 0;
                        int col3_closed = 0;
                        if (cnt == 2) {
                            List<FieldValues> fields = new ArrayList();
                            FieldValues f = new FieldValues();
                            f.setTitle("id");
                            String valuet2 = wound.getId() + "";
                            f.setValue(valuet2);
                            fields.add(f);
                            FieldValues f2 = new FieldValues();
                            f2.setTitle(Common.getLocalizedString("pixalere.ostomyassessment.form.pageheader", locale));
                            String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchorw\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + wnd.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (wound.getId() != null ? Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", "backdate"), blnEmbedCode, locale) : "");
                            f2.setValue(valuet22);
                            fields.add(f2);
                            assessments[count] = new RowData();
                            assessments[count].setAlpha_id(wound.getAlpha_id().intValue());
                            assessments[count].setWound_profile_type_id(wound.getWound_profile_type_id());
                            assessments[count].setUser_signature(wnd.getUser_signature());
                            assessments[count].setId(wound.getId() != null ? wound.getId().intValue() : 0);
                            assessments[count].setFields(fields);
                            assessments[count].setAlphaName(wound.getAlphaLocation().getAlphaName());
                            assessments[count].setHeader("pixalere.woundassessment.form.pageheader");
                            assessments[count].setDeleted(wound.getDeleted() == null ? 0 : wound.getDeleted());
                            assessments[count].setDelete_signature(wound.getDelete_signature() == null ? "" : wound.getDelete_signature() + "");
                            assessments[count].setDelete_reason(wound.getDeleted_reason());
                        }
                        List<FieldValues> fields = assessments[count].getFields();
                        //Get all columns for each component
                        if (component.getField_name().equals("status")) {
                            FieldValues field = new FieldValues();
                            Hashtable v = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                            Vector va = (Vector) v.get("value");
                            if (va.size() > 0) {
                                if ((wound.getStatus() + "").equals(Common.getConfig("ostomyClosed"))) {
                                    if (count == 0) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 1) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 2) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    }
                                }
                                String value = (String) va.get(0);
                                value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                Date now = new Date();
                                int timeframe = 0;
                                try {
                                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                                    if (timeframe_config == -1) {
                                        timeframe = 3;
                                    }
                                } catch (Exception e) {
                                }
                                now = PDate.subtractDays(now, timeframe);
                                //System.out.println("CHECK: "+currentProfessional+" "+now+" "+wnd);
                                try {
                                    if (currentProfessional != null && (currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (wnd.getCreated_on().after(now)) || timeframe == 0)) {
                                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                                    } else {
                                        field.setAllow_edit(0);
                                    }
                                } catch (NullPointerException e) {
                                    field.setAllow_edit(0);
                                }
                                field.setField_name(component.getField_name());
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            try {
                                if (t != null) {
                                    Collection<ArrayValueObject> arrays = t.getAssessment_treatment_arrays();
                                    List<ArrayValueObject> is = Common.filterArrayObjects(arrays.toArray(new AssessmentTreatmentArraysVO[arrays.size()]), component.getResource_id());
                                    value = Serialize.serialize(is, ", ", language);
                                }
                            } catch (ClassCastException e) {
                            }
                            field.setDb_value(value);
                            if (value != null && !value.equals("")) {
                                value = value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            }
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentNPWTVO t = nService.getNegativePressure(wound.getAssessment_id(), wound.getAlpha_id());
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                            FieldValues field = new FieldValues();
                            TreatmentCommentVO t = new TreatmentCommentVO();
                            t.setAssessment_id(wound.getAssessment_id());
                            t.setWound_profile_type_id(wound.getWound_profile_type_id());
                            t = tService.getTreatmentComment(t);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            String db_value = "";
                            if (t != null) {
                                Collection<AssessmentAntibioticVO> antibiotics = t.getAntibiotics();
                                List<String> antis = new ArrayList();
                                SimpleDateFormat sf = new SimpleDateFormat(Constants.DATE_FORMAT);
                                for (AssessmentAntibioticVO a : antibiotics) {
                                    String start_date = "";
                                    String end_date = "";
                                    try {
                                       if(a.getStart_date()!=null){
                                            start_date = sf.format(a.getStart_date());
                                        }
                                        if(a.getEnd_date()!=null){
                                            end_date = sf.format(a.getEnd_date());
                                        }
                                    } catch (NullPointerException e) {
                                        //date is empty,
                                    }
                                    String antibiotic = a.getAntibiotic() + " : " + start_date + " to " + end_date;
                                    antis.add(antibiotic);
                                }
                                if (antis != null && antis.size() > 0) {
                                    db_value = Serialize.serialize(antis, ", ");
                                    value = db_value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                }
                            }
                            field.setDb_value(db_value);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setOstomy_assessment_id(wound.getId());
                            Collection<AssessmentProductVO> products = getAllProducts(p);
                            String value = Common.printVector(Common.printProducts(products), blnEmbedCode) + Common.showNursingFixes(dao.findNursingFixes(wound != null && wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            FieldValues field2 = new FieldValues();
                            field2.setValue(value);
                            field2.setTitle(title);
                            field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                            field2.setField_name(component.getField_name());
                            field2.setComponent_id(component.getId().intValue());
                            fields.add(field2);
                        } else if (component.getField_name().equals("mucocutaneous_margin_location") && component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {
                            FieldValues field = new FieldValues();
                            String value = Common.parseMultipleSeperations(wound.getSeperations(), false, locale);
                            field.setDb_value(value);
                            value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            field.setValue(value);
                            field.setTitle(title + " Location");
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("mucocutaneous_margin_depth") && component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {
                            FieldValues field2 = new FieldValues();
                            String value = Common.parseMultipleSeperations(wound.getSeperations(), true, locale);
                            field2.setDb_value(value);
                            value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            field2.setValue(value);
                            field2.setTitle(title + " Depth");
                            field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                            field2.setField_name(component.getField_name());
                            field2.setComponent_id(component.getId().intValue());
                            fields.add(field2);
                        } else {
                            if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wnd, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wnd.getId() != null ? wnd.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            } else {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0 && fieldn.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            }
                        }
                        assessments[count].setFields(fields);
                        count++;
                    }
                    cnt++;
                    cnt = cnt + extend_count;//in case I have to split out a row into 2
                }
            }
        } catch (DataAccessException e) {
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            e.printStackTrace();
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return assessments;
    }

    public RowData[] getAllIncisionAssessmentsForFlowchart(Collection<AssessmentIncisionVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] assessments = null;
        try {
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            AssessmentNPWTServiceImpl nService = new AssessmentNPWTServiceImpl();
            GUIServiceImpl gui = new GUIServiceImpl();
            AssessmentDAO dao = new AssessmentDAO();
            WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
            ComponentsVO comTMP = new ComponentsVO();
            TreatmentCommentsServiceImpl tService = new TreatmentCommentsServiceImpl();
            assessments = new RowData[results.size()];
            
            comTMP.setFlowchart(Constants.INCISION_ASSESSMENT);
            Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
            PDate pdate = new PDate(currentProfessional != null ? currentProfessional.getTimezone() : Constants.TIMEZONE);
            int cnt = 2;
            for (ComponentsVO component : components) {
                if (component.getHide_flowchart() == 0) {
                    String title = Common.getLocalizedString(component.getLabel_key(), locale);
                    int extend_count = 0;
                    int count = 0;
                    for (AssessmentIncisionVO wound : results) {
                        WoundAssessmentLocationVO alpha = wound.getAlphaLocation();
                        //Common.alphaStore.put(alpha.getAlpha(),alpha);
                        ProfessionalVO userVO = null;
                        WoundAssessmentVO wnd = wound.getWoundAssessment();
                        userVO = wnd.getProfessional();
                        int col1_closed = 0;
                        int col2_closed = 0;
                        int col3_closed = 0;
                        if (cnt == 2) {
                            List<FieldValues> fields = new ArrayList();
                            FieldValues f = new FieldValues();
                            f.setTitle("id");
                            String valuet2 = wound.getId() + "";
                            f.setValue(valuet2);
                            fields.add(f);
                            FieldValues f2 = new FieldValues();
                            f2.setTitle(Common.getLocalizedString("pixalere.incisionassessment.form.pageheader", locale));
                            String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchorw\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + wnd.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (wound.getId() != null ? Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", "backdate"), blnEmbedCode, locale) : "");
                            f2.setValue(valuet22);
                            fields.add(f2);
                            assessments[count] = new RowData();
                            assessments[count].setAlpha_id(wound.getAlpha_id().intValue());
                            assessments[count].setWound_profile_type_id(wound.getWound_profile_type_id());
                            assessments[count].setUser_signature(wnd.getUser_signature());
                            assessments[count].setId(wound.getId() != null ? wound.getId().intValue() : 0);
                            assessments[count].setAlphaName(wound.getAlphaLocation().getAlphaName());
                            assessments[count].setFields(fields);
                            assessments[count].setHeader("pixalere.incisionassessment.form.pageheader");
                            assessments[count].setDeleted(wound.getDeleted() == null ? 0 : wound.getDeleted());
                            assessments[count].setDelete_signature(wound.getDelete_signature() == null ? "" : wound.getDelete_signature() + "");
                            assessments[count].setDelete_reason(wound.getDeleted_reason());
                        }
                        List<FieldValues> fields = assessments[count].getFields();
                        //Get all columns for each component
                        if (component.getField_name().equals("status")) {
                            FieldValues field = new FieldValues();
                            Hashtable v = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                            Vector va = (Vector) v.get("value");
                            if (va.size() > 0) {
                                if ((wound.getStatus() + "").equals(Common.getConfig("incisionClosed"))) {
                                    if (count == 0) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 1) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 2) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    }
                                }
                                String value = (String) va.get(0);
                                value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                Date now = new Date();
                                int timeframe = 0;
                                try {
                                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                                    if (timeframe_config == -1) {
                                        timeframe = 3;
                                    }
                                } catch (Exception e) {
                                }
                                now = PDate.subtractDays(now, timeframe);
                                try {
                                    if (((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (wnd.getCreated_on().after(now)) || timeframe == 0)) {
                                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("incisionClosed")), Common.isOffline() ? 1 : 0));
                                    } else {
                                        field.setAllow_edit(0);
                                    }
                                } catch (NullPointerException e) {
                                    field.setAllow_edit(0);
                                }
                                field.setField_name(component.getField_name());
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            try {
                                if (t != null) {
                                    Collection<ArrayValueObject> arrays = t.getAssessment_treatment_arrays();
                                    List<ArrayValueObject> is = Common.filterArrayObjects(arrays.toArray(new AssessmentTreatmentArraysVO[arrays.size()]), component.getResource_id());
                                    value = Serialize.serialize(is, ", ", language);
                                }
                            } catch (ClassCastException e) {
                            }
                            field.setDb_value(value);
                            if (value != null && !value.equals("")) {
                                value = value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            }
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setIncision_assessment_id(wound.getId());
                            Collection<AssessmentProductVO> products = getAllProducts(p);
                            String value = Common.printVector(Common.printProducts(products), blnEmbedCode) + Common.showNursingFixes(dao.findNursingFixes(wound != null && wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            FieldValues field2 = new FieldValues();
                            field2.setValue(value);
                            field2.setTitle(title);
                            field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                            field2.setField_name(component.getField_name());
                            field2.setComponent_id(component.getId().intValue());
                            fields.add(field2);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentNPWTVO t = nService.getNegativePressure(wound.getAssessment_id(), wound.getAlpha_id());
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            String db_value = "";
                            if (t != null) {
                                Collection<AssessmentAntibioticVO> antibiotics = t.getAntibiotics();
                                List<String> antis = new ArrayList();
                                SimpleDateFormat sf = new SimpleDateFormat(Constants.DATE_FORMAT);
                                for (AssessmentAntibioticVO a : antibiotics) {
                                    String start_date = "";
                                    String end_date = "";
                                    try {
                                        if(a.getStart_date()!=null){
                                            start_date = sf.format(a.getStart_date());
                                        }
                                        if(a.getEnd_date()!=null){
                                            end_date = sf.format(a.getEnd_date());
                                        }
                                    } catch (NullPointerException e) {
                                        //date is empty,
                                    }
                                    String antibiotic = a.getAntibiotic() + " : " + start_date + " to " + end_date;
                                    antis.add(antibiotic);
                                }
                                if (antis != null && antis.size() > 0) {
                                    db_value = Serialize.serialize(antis, ", ");
                                    value = db_value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                }
                            }
                            field.setDb_value(db_value);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                            FieldValues field = new FieldValues();
                            TreatmentCommentVO t = new TreatmentCommentVO();
                            t.setAssessment_id(wound.getAssessment_id());
                            t.setWound_profile_type_id(wound.getWound_profile_type_id());
                            t = tService.getTreatmentComment(t);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else {
                            if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wnd, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wnd.getId() != null ? wnd.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("incisionClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            } else {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("incisionClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            }
                        }
                        assessments[count].setFields(fields);
                        count++;
                    }
                    cnt++;
                    cnt = cnt + extend_count;//in case I have to split out a row into 2
                }
            }
        } catch (DataAccessException e) {
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return assessments;
    }

    public RowData[] getAllDrainAssessmentsForFlowchart(Collection<AssessmentDrainVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] assessments = null;
        try {
            TreatmentCommentsServiceImpl tService = new TreatmentCommentsServiceImpl();
            AssessmentNPWTServiceImpl nService = new AssessmentNPWTServiceImpl();
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            GUIServiceImpl gui = new GUIServiceImpl();
            AssessmentDAO dao = new AssessmentDAO();
            WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
            ComponentsVO comTMP = new ComponentsVO();
            
            AssessmentDrainVO idobject = new AssessmentDrainVO();
            assessments = new RowData[results.size()];
            comTMP.setFlowchart(Constants.DRAIN_ASSESSMENT);
            Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
            int cnt = 2;
            boolean[] allow_edit_closed = new boolean[results.size()];
            for (ComponentsVO component : components) {
                if (component.getHide_flowchart() == 0) {
                    String title = Common.getLocalizedString(component.getLabel_key(), locale);
                    int extend_count = 0;
                    int count = 0;
                    for (AssessmentDrainVO wound : results) {
                        //Common.alphaStore.put(alpha.getAlpha(),alpha);
                        ProfessionalVO userVO = null;
                        WoundAssessmentVO wnd = wound.getWoundAssessment();
                        userVO = wnd.getProfessional();
                        PDate pdate = new PDate();
                        int col1_closed = 0;
                        int col2_closed = 0;
                        int col3_closed = 0;
                        if (cnt == 2) {
                            List<FieldValues> fields = new ArrayList();
                            FieldValues f = new FieldValues();
                            f.setTitle("id");
                            String valuet2 = wound.getId() + "";
                            f.setValue(valuet2);
                            fields.add(f);
                            FieldValues f2 = new FieldValues();
                            f2.setTitle(Common.getLocalizedString("pixalere.drainassessment.form.pageheader", locale));
                            String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchorw\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + wnd.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (wound.getId() != null ? Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", "backdate"), blnEmbedCode, locale) : "");
                            f2.setValue(valuet22);
                            fields.add(f2);
                            assessments[count] = new RowData();
                            assessments[count].setAlpha_id(wound.getAlpha_id().intValue());
                            assessments[count].setWound_profile_type_id(wound.getWound_profile_type_id());
                            assessments[count].setUser_signature(wnd.getUser_signature());
                            assessments[count].setId(wound.getId() != null ? wound.getId().intValue() : 0);
                            assessments[count].setFields(fields);
                            assessments[count].setHeader("pixalere.drainassessment.form.pageheader");
                            assessments[count].setAlphaName(wound.getAlphaLocation().getAlphaName());
                            assessments[count].setDeleted(wound.getDeleted() == null ? 0 : wound.getDeleted());
                            assessments[count].setDelete_signature(wound.getDelete_signature() == null ? "" : wound.getDelete_signature() + "");
                            assessments[count].setDelete_reason(wound.getDeleted_reason());
                        }
                        List<FieldValues> fields = assessments[count].getFields();
                        //Get all columns for each component
                        if (component.getField_name().equals("status")) {
                            FieldValues field = new FieldValues();
                            Hashtable v = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                            Vector va = (Vector) v.get("value");
                            if (va.size() > 0) {
                                if ((wound.getStatus() + "").equals(Common.getConfig("drainClosed"))) {
                                    if (count == 0) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 1) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 2) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    }
                                }
                                String value = (String) va.get(0);
                                value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                Date now = new Date();
                                int timeframe = 0;
                                try {
                                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                                    if (timeframe_config == -1) {
                                        timeframe = 3;
                                    }
                                } catch (Exception e) {
                                }
                                now = PDate.subtractDays(now, timeframe);
                                try {
                                    if (((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (wnd.getCreated_on().after(now)) || timeframe == 0)) {
                                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("drainClosed")), Common.isOffline() ? 1 : 0));
                                    } else {
                                        field.setAllow_edit(0);
                                    }
                                } catch (NullPointerException e) {
                                    field.setAllow_edit(0);
                                }
                                field.setField_name(component.getField_name());
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setDrain_assessment_id(wound.getId());
                            Collection<AssessmentProductVO> products = getAllProducts(p);
                            String value = Common.printVector(Common.printProducts(products), blnEmbedCode) + Common.showNursingFixes(dao.findNursingFixes(wound != null && wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            ;
                            FieldValues field2 = new FieldValues();
                            field2.setValue(value);
                            field2.setTitle(title);
                            field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                            field2.setField_name(component.getField_name());
                            field2.setComponent_id(component.getId().intValue());
                            fields.add(field2);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentNPWTVO t = nService.getNegativePressure(wound.getAssessment_id(), wound.getAlpha_id());
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                            FieldValues field = new FieldValues();
                            TreatmentCommentVO t = new TreatmentCommentVO();
                            t.setAssessment_id(wound.getAssessment_id());
                            t.setWound_profile_type_id(wound.getWound_profile_type_id());
                            t = tService.getTreatmentComment(t);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            try {
                                if (t != null) {
                                    Collection<ArrayValueObject> arrays = t.getAssessment_treatment_arrays();
                                    List<ArrayValueObject> is = Common.filterArrayObjects(arrays.toArray(new AssessmentTreatmentArraysVO[arrays.size()]), component.getResource_id());
                                    value = Serialize.serialize(is, ", ", language);
                                }
                            } catch (ClassCastException e) {
                            }
                            field.setDb_value(value);
                            if (value != null && !value.equals("")) {
                                value = value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            }
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            String db_value = "";
                            List<String> antis = new ArrayList();
                            if (t != null) {
                                Collection<AssessmentAntibioticVO> antibiotics = t.getAntibiotics();
                                SimpleDateFormat sf = new SimpleDateFormat(Constants.DATE_FORMAT);
                                for (AssessmentAntibioticVO a : antibiotics) {
                                    String start_date = "";
                                    String end_date = "";
                                    try {
                                       if(a.getStart_date()!=null){
                                            start_date = sf.format(a.getStart_date());
                                        }
                                        if(a.getEnd_date()!=null){
                                            end_date = sf.format(a.getEnd_date());
                                        }
                                    } catch (NullPointerException e) {
                                        //date is empty,
                                    }
                                    String antibiotic = a.getAntibiotic() + " : " + start_date + " to " + end_date;
                                    antis.add(antibiotic);
                                }
                                if (antis != null && antis.size() > 0) {
                                    db_value = Serialize.serialize(antis, ", ");
                                    value = db_value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                }
                            }
                            field.setDb_value(db_value);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else {
                            if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wnd, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wnd.getId() != null ? wnd.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("drainClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            } else {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = null;
                                Vector vvalue = new Vector();
                                Vector fieldn = new Vector();
                                if (component.getTable_id() != null && component.getTable_id().equals(Constants.TUBESDRAINS_SECTION)) {
                                    int n = 0;
                                    try {
                                        Integer num_str = (Integer) Common.getFieldValue("drainage_num", wound);
                                        n = num_str.intValue();
                                    } catch (NumberFormatException numexp) {
                                        n = 0;
                                    } catch (ClassCastException numexp) {
                                        n = 0;
                                    }
                                    String s = "";
                                    String f = "";
                                    String fld_name = component.getField_name();
                                    if (n == 0) {
                                        n = 1;
                                    }
                                    for (int z = 1; z <= n; z++) {
                                        component.setField_name(fld_name + "" + z);
                                        vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                                        Vector v = (Vector) vhash.get("value");
                                        s = s + "" + v.get(0) + ", ";
                                        Vector fv = (Vector) vhash.get("field");
                                        f = fv.get(0) + "";
                                    }
                                    component.setField_name(fld_name);
                                    if (s.length() > 1) {
                                        s = s.substring(0, s.length() - 2);
                                    }
                                    vvalue.add(s);
                                    fieldn.add(f);
                                } else {
                                    vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                                    vvalue = (Vector) vhash.get("value");
                                    fieldn = (Vector) vhash.get("field");
                                }
                                if (vvalue.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("drainClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            }
                        }
                        assessments[count].setFields(fields);
                        count++;
                    }
                    cnt++;
                    cnt = cnt + extend_count;//in case I have to split out a row into 2
                }
            }
        } catch (DataAccessException e) {
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return assessments;
    }

    public RowData[] getAllSkinAssessmentsForFlowchart(Collection<AssessmentSkinVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] assessments = null;
        try {
            TreatmentCommentsServiceImpl tService = new TreatmentCommentsServiceImpl();
            AssessmentNPWTServiceImpl nService = new AssessmentNPWTServiceImpl();
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            GUIServiceImpl gui = new GUIServiceImpl();
            AssessmentDAO dao = new AssessmentDAO();
            WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
            ComponentsVO comTMP = new ComponentsVO();
            
            AssessmentSkinVO idobject = new AssessmentSkinVO();
            assessments = new RowData[results.size()];
            comTMP.setFlowchart(Constants.SKIN_ASSESSMENT);
            Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
            int cnt = 2;
            boolean[] allow_edit_closed = new boolean[results.size()];
            for (ComponentsVO component : components) {
                String title = Common.getLocalizedString(component.getLabel_key(), locale);
                int extend_count = 0;
                int count = 0;
                for (AssessmentSkinVO wound : results) {
                    WoundAssessmentLocationVO alpha = wound.getAlphaLocation();
                    //Common.alphaStore.put(alpha.getAlpha(),alpha);
                    ProfessionalVO userVO = null;
                    WoundAssessmentVO wnd = wound.getWoundAssessment();
                    userVO = wnd.getProfessional();
                    PDate pdate = new PDate();
                    int col1_closed = 0;
                    int col2_closed = 0;
                    int col3_closed = 0;
                    if (cnt == 2) {
                        List<FieldValues> fields = new ArrayList();
                        FieldValues f = new FieldValues();
                        f.setTitle("id");
                        String valuet2 = wound.getId() + "";
                        f.setValue(valuet2);
                        fields.add(f);
                        FieldValues f2 = new FieldValues();
                        f2.setTitle(Common.getLocalizedString("pixalere.skinassessment.form.pageheader", locale));
                        String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchorw\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + wnd.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (wound.getId() != null ? Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", "backdate"), blnEmbedCode, locale) : "");
                        f2.setValue(valuet22);
                        fields.add(f2);
                        assessments[count] = new RowData();
                        assessments[count].setAlpha_id(wound.getAlpha_id().intValue());
                            assessments[count].setWound_profile_type_id(wound.getWound_profile_type_id());
                        assessments[count].setUser_signature(wnd.getUser_signature());
                        assessments[count].setId(wound.getId() != null ? wound.getId().intValue() : 0);
                        assessments[count].setFields(fields);
                        assessments[count].setHeader("pixalere.skinassessment.form.pageheader");
                            assessments[count].setAlphaName(wound.getAlphaLocation().getAlphaName());
                        assessments[count].setDeleted(wound.getDeleted() == null ? 0 : wound.getDeleted());
                        assessments[count].setDelete_signature(wound.getDelete_signature() == null ? "" : wound.getDelete_signature() + "");
                        assessments[count].setDelete_reason(wound.getDeleted_reason());
                    }
                    List<FieldValues> fields = assessments[count].getFields();
                    //Get all columns for each component
                    if (component.getField_name().equals("status")) {
                        FieldValues field = new FieldValues();
                        Hashtable v = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                        Vector va = (Vector) v.get("value");
                        if (va.size() > 0) {
                            if ((wound.getStatus() + "").equals(Common.getConfig("skinClosed"))) {
                                if (count == 0) {
                                    field.setClosed(1);
                                    col1_closed = 1;
                                } else if (count == 1) {
                                    field.setClosed(1);
                                    col1_closed = 1;
                                } else if (count == 2) {
                                    field.setClosed(1);
                                    col1_closed = 1;
                                }
                            }
                            String value = (String) va.get(0);
                            value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            field.setValue(value);
                            field.setTitle(title);
                            Date now = new Date();
                            int timeframe = 0;
                            try {
                                int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                                if (timeframe_config == -1) {
                                    timeframe = 3;
                                }
                            } catch (Exception e) {
                            }
                            now = PDate.subtractDays(now, timeframe);
                            if (((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (wnd.getCreated_on().after(now)) || timeframe == 0)) {
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("skinClosed")), Common.isOffline() ? 1 : 0));
                            } else {
                                field.setAllow_edit(0);
                            }
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        }
                    } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                        AssessmentProductVO p = new AssessmentProductVO();
                        p.setSkin_assessment_id(wound.getId());
                        Collection<AssessmentProductVO> products = getAllProducts(p);
                        String value = Common.printVector(Common.printProducts(products), blnEmbedCode) + Common.showNursingFixes(dao.findNursingFixes(wound != null && wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);;
                        FieldValues field2 = new FieldValues();
                        field2.setValue(value);
                        field2.setTitle(title);
                        field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                        field2.setField_name(component.getField_name());
                        field2.setComponent_id(component.getId().intValue());
                        fields.add(field2);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                        FieldValues field = new FieldValues();

                        AssessmentNPWTVO t = nService.getNegativePressure(wound.getAssessment_id(), wound.getAlpha_id());

                        Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                        Vector vvalue = (Vector) vhash.get("value");
                        Vector fieldn = (Vector) vhash.get("field");
                        if (vvalue.size() > 0) {
                            String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name((String) fieldn.get(0));
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        }
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                        FieldValues field = new FieldValues();
                        TreatmentCommentVO t = new TreatmentCommentVO();
                        t.setAssessment_id(wound.getAssessment_id());
                        t.setWound_profile_type_id(wound.getWound_profile_type_id());
                        t = tService.getTreatmentComment(t);
                        Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                        Vector vvalue = (Vector) vhash.get("value");
                        Vector fieldn = (Vector) vhash.get("field");
                        if (vvalue.size() > 0) {
                            String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name((String) fieldn.get(0));
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        }
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION)) {
                        FieldValues field = new FieldValues();
                        AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                        treat.setAssessment_id(wound.getAssessment_id());
                        treat.setAlpha_id(wound.getAlpha_id());
                        AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                        String value = "";
                        try {
                            if (t != null) {
                                Collection<ArrayValueObject> arrays = t.getAssessment_treatment_arrays();
                                List<ArrayValueObject> is = Common.filterArrayObjects(arrays.toArray(new AssessmentTreatmentArraysVO[arrays.size()]), component.getResource_id());
                                value = Serialize.serialize(is, ", ", language);
                            }
                        } catch (ClassCastException e) {
                        }
                        field.setDb_value(value);
                        if (value != null && !value.equals("")) {
                            value = value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                        }
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {
                        FieldValues field = new FieldValues();
                        AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                        treat.setAssessment_id(wound.getAssessment_id());
                        treat.setAlpha_id(wound.getAlpha_id());
                        AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                        String value = "";
                        String db_value = "";
                        List<String> antis = new ArrayList();
                        if (t != null) {
                            Collection<AssessmentAntibioticVO> antibiotics = t.getAntibiotics();
                            SimpleDateFormat sf = new SimpleDateFormat(Constants.DATE_FORMAT);
                            for (AssessmentAntibioticVO a : antibiotics) {
                                String start_date = "";
                                String end_date = "";
                                try {
                                    if(a.getStart_date()!=null){
                                            start_date = sf.format(a.getStart_date());
                                        }
                                        if(a.getEnd_date()!=null){
                                            end_date = sf.format(a.getEnd_date());
                                        }
                                } catch (NullPointerException e) {
                                    //date is empty,
                                }
                                String antibiotic = a.getAntibiotic() + " : " + start_date + " to " + end_date;
                                antis.add(antibiotic);
                            }
                            if (antis != null && antis.size() > 0) {
                                db_value = Serialize.serialize(antis, ", ");
                                value = db_value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            }
                        }
                        field.setDb_value(db_value);
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT)) {
                        FieldValues field = new FieldValues();
                        AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                        treat.setAssessment_id(wound.getAssessment_id());
                        treat.setAlpha_id(wound.getAlpha_id());
                        AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                        Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                        Vector vvalue = (Vector) vhash.get("value");
                        Vector fieldn = (Vector) vhash.get("field");
                        if (vvalue.size() > 0) {
                            String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name((String) fieldn.get(0));
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        }
                    } else {
                        if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                            FieldValues field = new FieldValues();
                            Hashtable vhash = Common.getPrintableValue(wnd, currentProfessional, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wnd.getId() != null ? wnd.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("drainClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else {
                            FieldValues field = new FieldValues();
                            Hashtable vhash = null;
                            Vector vvalue = new Vector();
                            Vector fieldn = new Vector();
                            vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                            vvalue = (Vector) vhash.get("value");
                            fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("skinClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        }
                    }
                    assessments[count].setFields(fields);
                    count++;
                }
                cnt++;
                cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        } catch (DataAccessException e) {
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return assessments;
    }

    public RowData[] getAllBurnAssessmentsForFlowchart(Collection<AssessmentBurnVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] assessments = null;
        try {
            TreatmentCommentsServiceImpl tService = new TreatmentCommentsServiceImpl();
            AssessmentNPWTServiceImpl nService = new AssessmentNPWTServiceImpl();
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            GUIServiceImpl gui = new GUIServiceImpl();
            AssessmentDAO dao = new AssessmentDAO();
            WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
            ComponentsVO comTMP = new ComponentsVO();
            
            AssessmentBurnVO idobject = new AssessmentBurnVO();
            assessments = new RowData[results.size()];
            comTMP.setFlowchart(Constants.BURN_ASSESSMENT);
            Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
            int cnt = 2;
            boolean[] allow_edit_closed = new boolean[results.size()];
            for (ComponentsVO component : components) {
                if (component.getHide_flowchart() == 0) {
                    String title = Common.getLocalizedString(component.getLabel_key(), locale);
                    int extend_count = 0;
                    int count = 0;
                    for (AssessmentBurnVO wound : results) {
                        WoundAssessmentLocationVO alpha = wound.getAlphaLocation();
                        //Common.alphaStore.put(alpha.getAlpha(),alpha);
                        ProfessionalVO userVO = null;
                        WoundAssessmentVO wnd = wound.getWoundAssessment();
                        userVO = wnd.getProfessional();
                        PDate pdate = new PDate();
                        int col1_closed = 0;
                        int col2_closed = 0;
                        int col3_closed = 0;
                        if (cnt == 2) {
                            List<FieldValues> fields = new ArrayList();
                            FieldValues f = new FieldValues();
                            f.setTitle("id");
                            String valuet2 = wound.getId() + "";
                            f.setValue(valuet2);
                            fields.add(f);
                            FieldValues f2 = new FieldValues();
                            f2.setTitle(Common.getLocalizedString("pixalere.burnassessment.form.pageheader", locale));
                            String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchorw\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + wnd.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (wound.getId() != null ? Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", "backdate"), blnEmbedCode, locale) : "");
                            f2.setValue(valuet22);
                            fields.add(f2);
                            assessments[count] = new RowData();
                            assessments[count].setAlpha_id(wound.getAlpha_id().intValue());
                            assessments[count].setWound_profile_type_id(wound.getWound_profile_type_id());
                            assessments[count].setUser_signature(wnd.getUser_signature());
                            assessments[count].setId(wound.getId() != null ? wound.getId().intValue() : 0);
                            assessments[count].setFields(fields);
                            assessments[count].setHeader("pixalere.burnassessment.form.pageheader");
                            assessments[count].setDeleted(wound.getDeleted() == null ? 0 : wound.getDeleted());
                            assessments[count].setAlphaName(wound.getAlphaLocation().getAlphaName());
                            assessments[count].setDelete_signature(wound.getDelete_signature() == null ? "" : wound.getDelete_signature() + "");
                            assessments[count].setDelete_reason(wound.getDeleted_reason());
                        }
                        List<FieldValues> fields = assessments[count].getFields();
                        //Get all columns for each component
                        if (component.getField_name().equals("status")) {
                            FieldValues field = new FieldValues();
                            Hashtable v = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                            Vector va = (Vector) v.get("value");
                            if (va.size() > 0) {
                                if ((wound.getStatus() + "").equals(Common.getConfig("burnClosed"))) {
                                    if (count == 0) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 1) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 2) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    }
                                }
                                String value = (String) va.get(0);
                                value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                Date now = new Date();
                                int timeframe = 0;
                                try {
                                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                                    if (timeframe_config == -1) {
                                        timeframe = 3;
                                    }
                                } catch (Exception e) {
                                }
                                now = PDate.subtractDays(now, timeframe);
                                if (((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (wnd.getCreated_on().after(now)) || timeframe == 0)) {
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("burnClosed")), Common.isOffline() ? 1 : 0));
                                } else {
                                    field.setAllow_edit(0);
                                }
                                field.setField_name(component.getField_name());
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentNPWTVO t = nService.getNegativePressure(wound.getAssessment_id(), wound.getAlpha_id());
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                            FieldValues field = new FieldValues();
                            TreatmentCommentVO t = new TreatmentCommentVO();
                            t.setAssessment_id(wound.getAssessment_id());
                            t.setWound_profile_type_id(wound.getWound_profile_type_id());
                            t = tService.getTreatmentComment(t);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes((t != null && t.getId() != null ? t.getId().intValue() + "" : "0"), component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("burnClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            String db_value = "";
                            if (t != null) {
                                Collection<AssessmentAntibioticVO> antibiotics = t.getAntibiotics();
                                List<String> antis = new ArrayList();
                                SimpleDateFormat sf = new SimpleDateFormat(Constants.DATE_FORMAT);
                                for (AssessmentAntibioticVO a : antibiotics) {
                                    String start_date = "";
                                    String end_date = "";
                                    try {
                                        if(a.getStart_date()!=null){
                                            start_date = sf.format(a.getStart_date());
                                        }
                                        if(a.getEnd_date()!=null){
                                            end_date = sf.format(a.getEnd_date());
                                        }
                                    } catch (NullPointerException e) {
                                        //date is empty,
                                    }
                                    String antibiotic = a.getAntibiotic() + " : " + start_date + " to " + end_date;
                                    antis.add(antibiotic);
                                }
                                if (antis != null && antis.size() > 0) {
                                    db_value = Serialize.serialize(antis, ", ");
                                    value = db_value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                }
                            }
                            field.setDb_value(db_value);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            String value = "";
                            try {
                                if (t != null) {
                                    Collection<ArrayValueObject> arrays = t.getAssessment_treatment_arrays();
                                    List<ArrayValueObject> is = Common.filterArrayObjects(arrays.toArray(new AssessmentTreatmentArraysVO[arrays.size()]), component.getResource_id());
                                    value = Serialize.serialize(is, ", ", language);
                                }
                            } catch (ClassCastException e) {
                            }
                            field.setDb_value(value);
                            if (value != null && !value.equals("")) {
                                value = value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            }
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else {
                            if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wnd, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wnd.getId() != null ? wnd.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("burnClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                                AssessmentProductVO p = new AssessmentProductVO();
                                p.setBurn_assessment_id(wound.getId());
                                Collection<AssessmentProductVO> products = getAllProducts(p);
                                String value = Common.printVector(Common.printProducts(products), blnEmbedCode) + Common.showNursingFixes(dao.findNursingFixes(wound != null && wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                ;
                                FieldValues field2 = new FieldValues();
                                field2.setValue(value);
                                field2.setTitle(title);
                                field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                                field2.setField_name(component.getField_name());
                                field2.setComponent_id(component.getId().intValue());
                                fields.add(field2);
                            } else {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = null;
                                Vector vvalue = new Vector();
                                Vector fieldn = new Vector();
                                //if (component.getTable_id() != null && component.getTable_id().equals(Constants.BURN_SECTION)) {
                                if (component.getTable_id() != null) {
                                    int n = 0;
                                    try {
                                        Integer num_str = (Integer) Common.getFieldValue("drainage_num", wound);
                                        n = num_str.intValue();
                                    } catch (NumberFormatException numexp) {
                                        n = 0;
                                    } catch (ClassCastException numexp) {
                                        n = 0;
                                    }
                                    String s = "";
                                    String f = "";
                                    String fld_name = component.getField_name();
                                    for (int z = 1; z <= n; z++) {
                                        component.setField_name(fld_name + "" + z);
                                        vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                                        Vector v = (Vector) vhash.get("value");
                                        s = s + "" + v.get(0) + ", ";
                                        Vector fv = (Vector) vhash.get("field");
                                        f = fv.get(0) + "";
                                    }
                                    component.setField_name(fld_name);
                                    if (s.length() > 1) {
                                        s = s.substring(0, s.length() - 2);
                                    }
                                    vvalue.add(s);
                                    fieldn.add(f);
                                } else {
                                    vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                                    vvalue = (Vector) vhash.get("value");
                                    fieldn = (Vector) vhash.get("field");
                                }
                                if (vvalue.size() > 0) {
                                    String value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("burnClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            }
                        }
                        assessments[count].setFields(fields);
                        count++;
                    }
                    cnt++;
                    cnt = cnt + extend_count;//in case I have to split out a row into 2
                }
            }
        } catch (DataAccessException e) {
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return assessments;
    }
    /*
     * The following  method gets all assessment_eachwound records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param vo the AssessmentEachwoundVO object to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */

    public RowData[] getAllAssessmentsForFlowchart(Collection<AssessmentEachwoundVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] assessments = null;
        int NUM_TITLES = 2;
        try {
            TreatmentCommentsServiceImpl tService = new TreatmentCommentsServiceImpl();
            AssessmentNPWTServiceImpl nService = new AssessmentNPWTServiceImpl();
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            GUIServiceImpl gui = new GUIServiceImpl();
            AssessmentDAO dao = new AssessmentDAO();
            WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
            ComponentsVO comTMP = new ComponentsVO();
            
            assessments = new RowData[results.size()];
            comTMP.setFlowchart(Constants.WOUND_ASSESSMENT);
            Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
            int cnt = 2;//start at 2, as titles are 0,1
            boolean[] allow_edit_closed = new boolean[results.size()];
            for (ComponentsVO component : components) {
                if (component.getHide_flowchart() == 0) {
                    String title = Common.getLocalizedString(component.getLabel_key(), locale);
                    int count = 0;
                    for (AssessmentEachwoundVO wound : results) {
                        //WoundAssessmentLocationVO alpha = wound.getAlphaLocation();
                        //Common.alphaStore.put(alpha.getAlpha(),alpha);
                        ProfessionalVO userVO = null;
                        WoundAssessmentVO wnd = wound.getWoundAssessment();
                        userVO = wnd.getProfessional();
                        PDate pdate = new PDate();
                        int col1_closed = 0;
                        int col2_closed = 0;
                        int col3_closed = 0;
                        if (cnt == 2) {
                            List<FieldValues> fields = new ArrayList();
                            FieldValues f = new FieldValues();
                            f.setTitle("id");
                            String valuet2 = wound.getId() + "";
                            f.setValue(valuet2);
                            fields.add(f);
                            FieldValues f2 = new FieldValues();
                            f2.setTitle(Common.getLocalizedString("pixalere.woundassessment.form.pageheader", locale));
                            String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchorw\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + wnd.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (wound.getId() != null ? Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", "backdate"), blnEmbedCode, locale) : "");
                            f2.setValue(valuet22);
                            fields.add(f2);
                            assessments[count] = new RowData();
                            assessments[count].setWound_profile_type_id(wound.getWound_profile_type_id());
                            assessments[count].setAlpha_id(wound.getAlpha_id().intValue());
                            assessments[count].setUser_signature(wnd.getUser_signature());
                            assessments[count].setId(wound.getId() != null ? wound.getId().intValue() : 0);
                            assessments[count].setAlphaName(wound.getAlphaLocation().getAlphaName());
                            assessments[count].setFields(fields);
                            assessments[count].setHeader("pixalere.woundassessment.form.pageheader");
                            assessments[count].setDeleted(wound.getDeleted() == null ? 0 : wound.getDeleted());
                            assessments[count].setDelete_signature(wound.getDelete_signature() == null ? "" : wound.getDelete_signature() + "");
                            assessments[count].setDelete_reason(wound.getDeleted_reason());
                        }
                        List<FieldValues> fields = assessments[count].getFields();
                        //Get all columns for each component
                        String value = "";
                        if (component.getField_name().equals("undermining_location") && component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {
                            FieldValues field = new FieldValues();
                            value = Common.parseMultipleUndermining(wound.getUnderminings(), false, locale);
                            value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            field.setValue(value);
                            field.setTitle(title + " Location");
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getComponent_type().equals(Constants.PRODUCTS_COMPONENT_TYPE)) {
                            AssessmentProductVO p = new AssessmentProductVO();
                            p.setWound_assessment_id(wound.getId());
                            Collection<AssessmentProductVO> products = getAllProducts(p);

                            //System.out.println(wound.getId()+" "+wound.getAlpha_id()+" "+wound.getAssessment_id()+" Flowchart: "+products.size());

                            value = Common.printVector(Common.printProducts(products), blnEmbedCode) + Common.showNursingFixes(dao.findNursingFixes(wound != null && wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            FieldValues field2 = new FieldValues();
                            field2.setValue(value);
                            field2.setTitle(title);
                            field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("ostomyClosed")), Common.isOffline() ? 1 : 0));
                            field2.setField_name(component.getField_name());
                            field2.setComponent_id(component.getId().intValue());
                            fields.add(field2);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_ARRAY_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            value = "";
                            try {
                                if (t != null) {
                                    Collection<ArrayValueObject> arrays = t.getAssessment_treatment_arrays();
                                    List<ArrayValueObject> is = Common.filterArrayObjects(arrays.toArray(new AssessmentTreatmentArraysVO[arrays.size()]), component.getResource_id());
                                    value = Serialize.serialize(is, ", ", language);
                                }
                            } catch (ClassCastException e) {
                            }
                            field.setDb_value(value);
                            if (value != null && !value.equals("")) {
                                value = value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                            }
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT_TREATMENT)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.NPWT_SECTION)) {
                            FieldValues field = new FieldValues();
                            AssessmentNPWTVO t = nService.getNegativePressure(wound.getAssessment_id(), wound.getAlpha_id());
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_DATE_COMPONENT_TYPE)) {
                            FieldValues field = new FieldValues();
                            AssessmentTreatmentVO treat = new AssessmentTreatmentVO();
                            treat.setAssessment_id(wound.getAssessment_id());
                            treat.setAlpha_id(wound.getAlpha_id());
                            AssessmentTreatmentVO t = tService.getAssessmentTreatment(treat);

                            String db_value = "";
                            if (t != null) {
                                Collection<AssessmentAntibioticVO> antibiotics = t.getAntibiotics();
                                value = "";
                                List<String> antis = new ArrayList();
                                SimpleDateFormat sf = new SimpleDateFormat(Constants.DATE_FORMAT);

                                for (AssessmentAntibioticVO a : antibiotics) {
                                    String start_date = "";
                                    String end_date = "";
                                    try {
                                        if(a.getStart_date()!=null){
                                            start_date = sf.format(a.getStart_date());
                                        }
                                        if(a.getEnd_date()!=null){
                                            end_date = sf.format(a.getEnd_date());
                                        }
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }
                                    String antibiotic = a.getAntibiotic() + " : " + start_date + " to " + end_date;
                                    //System.out.println("Antibiotics: "+antibiotics);
                                    antis.add(antibiotic);
                                }
                                if (antis != null && antis.size() > 0) {
                                    db_value = Serialize.serialize(antis, ", ");
                                }
                                value = db_value + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                
                            }
                            field.setDb_value(db_value);
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("status")) {  //dont think this is needed anymore
                            FieldValues field = new FieldValues();
                            Hashtable v = Common.getPrintableValue(wound, userVO, component, blnEmbedCode, count, language);
                            Vector va = (Vector) v.get("value");
                            if (va.size() > 0) {
                                value = (String) va.get(0);
                                if ((wound.getStatus() + "").equals(Common.getConfig("woundClosed"))) {
                                    if (count == 0) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 1) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    } else if (count == 2) {
                                        field.setClosed(1);
                                        col1_closed = 1;
                                    }
                                }
                                value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                Date now = new Date();
                                int timeframe = 0;
                                try {
                                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                                    if (timeframe_config == -1) {
                                        timeframe = 3;
                                    }
                                } catch (Exception e) {
                                }
                                now = PDate.subtractDays(now, timeframe);
                                if (((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (wnd.getCreated_on().after(now)) || timeframe == 0)) {
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                } else {
                                    field.setAllow_edit(0);
                                }
                                field.setField_name(component.getField_name());
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.TREATMENT_COMMENTS_SECTION)) {
                            FieldValues field = new FieldValues();
                            TreatmentCommentVO t = new TreatmentCommentVO();
                            t.setAssessment_id(wound.getAssessment_id());
                            t.setWound_profile_type_id(wound.getWound_profile_type_id());
                            t = tService.getTreatmentComment(t);
                            Hashtable vhash = Common.getPrintableValue(t, userVO, component, blnEmbedCode, count, language);
                            Vector vvalue = (Vector) vhash.get("value");
                            Vector fieldn = (Vector) vhash.get("field");
                            if (vvalue.size() > 0) {
                                value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(t != null && t.getId() != null ? t.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field.setValue(value);
                                field.setTitle(title);
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name((String) fieldn.get(0));
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            }
                        } else {
                            if (component.getTable_id() != null && component.getTable_id().equals(Constants.ASSESSMENT)) {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wnd, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0) {
                                    value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wnd.getId() != null ? wnd.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            } else if (component.getField_name().equals("sinus_depth") && component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {
                                FieldValues field2 = new FieldValues();
                                value = Common.parseMultipleTracts(wound.getSinustracts(), true);
                                value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field2.setValue(value);
                                field2.setTitle(title + " Depth (cm)");
                                field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field2.setField_name(component.getField_name());
                                field2.setComponent_id(component.getId().intValue());
                                fields.add(field2);
                            } else if (component.getField_name().equals("undermining_depth") && component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {
                                FieldValues field2 = new FieldValues();
                                value = Common.parseMultipleUndermining(wound.getUnderminings(), true, locale);
                                value = value + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                field2.setValue(value);
                                field2.setTitle(title + " Depth (cm)");
                                field2.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field2.setField_name(component.getField_name());
                                field2.setComponent_id(component.getId().intValue());
                                fields.add(field2);
                            } else if (component.getField_name().equals("sinus_location") && component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {
                                FieldValues field = new FieldValues();
                                value = Common.parseMultipleTracts(wound.getSinustracts(), false);
                                value = value + (component.getAllow_edit().equals(new Integer("1")) ? Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale) : "");
                                field.setValue(value);
                                field.setTitle(title + " Location");
                                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                field.setField_name(component.getField_name());
                                field.setComponent_id(component.getId().intValue());
                                fields.add(field);
                            } else {
                                FieldValues field = new FieldValues();
                                Hashtable vhash = Common.getPrintableValue(wound, currentProfessional, component, blnEmbedCode, count, language);
                                Vector vvalue = (Vector) vhash.get("value");
                                Vector fieldn = (Vector) vhash.get("field");
                                if (vvalue.size() > 0) {
                                    value = vvalue.get(0) + Common.showNursingFixes(dao.findNursingFixes(wound.getId() != null ? wound.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                                    field.setValue(value);
                                    if (cnt == 0) {
                                        field.setClosed(col1_closed);
                                    } else if (cnt == 1) {
                                        field.setClosed(col1_closed);
                                    } else if (cnt == 2) {
                                        field.setClosed(col1_closed);
                                    }
                                    field.setTitle(title);
                                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, component.getEnabled_close(), Integer.toString(wound.getStatus()).equals(Common.getConfig("woundClosed")), Common.isOffline() ? 1 : 0));
                                    field.setField_name((String) fieldn.get(0));
                                    field.setComponent_id(component.getId().intValue());
                                    fields.add(field);
                                }
                            }
                        }
                        assessments[count].setFields(fields);
                        count++;
                    }
                    cnt++;
                }
            }
        } catch (DataAccessException e) {
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return assessments;
    }
    /*
     * Get the last Active assessment regardless of type.
     * @param assess the criteria to find the object
     * @param alpha_type the alpha type to retrieve (ie ostomy etc)
     * @param intFull last assessment with measurements from last full or partial -1 assessment, 0 last partial, 1 last full.
     * @return Assessment object
     * 
     */

    public AbstractAssessmentVO retrieveLastActiveAssessment(AbstractAssessmentVO assess, String alpha_type, Integer intFull) throws ApplicationException {
        AssessmentEachwoundVO woundVO = null;
        AssessmentOstomyVO ostomyVO = null;
        AssessmentIncisionVO incisionVO = null;
        AssessmentDrainVO drainVO = null;
        AssessmentBurnVO burnVO = null;
        AssessmentSkinVO skinVO = null;
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            //partial.setFull_assessment(new Integer(0));
            if (alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {
                AssessmentEachwoundVO last = new AssessmentEachwoundVO();
                last.setAlpha_id(new Integer(assess.getAlpha_id()));
                if (assess.getWound_id() != null) {
                    last.setWound_id(new Integer(assess.getWound_id()));
                }
                if (intFull != -1) {
                    last.setFull_assessment(intFull);
                }
                AssessmentEachwoundVO resultVO = (AssessmentEachwoundVO) woundAssessmentDAO.findLastActiveAssessment(last, false);
                if (intFull == -1 && resultVO != null && resultVO.getFull_assessment() == 0) // If last assessment is partial
                {
                    AssessmentEachwoundVO full = new AssessmentEachwoundVO();
                    full.setAlpha_id(new Integer(assess.getAlpha_id()));
                    if (assess.getWound_id() != null) {
                        full.setWound_id(new Integer(assess.getWound_id()));
                    }
                    AssessmentEachwoundVO fullVO = (AssessmentEachwoundVO) woundAssessmentDAO.findLastActiveAssessment(full, false);
                    if (fullVO != null) {
                        resultVO.setLength(fullVO.getLength());
                        resultVO.setWidth(fullVO.getWidth());
                        resultVO.setDepth(fullVO.getDepth());

                        resultVO.setUnderminings(fullVO.getUnderminings());
                        resultVO.setSinustracts(fullVO.getSinustracts());
                    }
                }
                if(resultVO!=null){
                    resultVO.setPain_comments("");
                }
                return resultVO;
            } else if (alpha_type.equals(Constants.OSTOMY_PROFILE_TYPE)) {
                AssessmentOstomyVO crit = new AssessmentOstomyVO();
                crit.setAlpha_id(new Integer(assess.getAlpha_id()));
                if (  assess!=null && assess.getWound_id() != null) {
                    crit.setWound_id(new Integer(assess.getWound_id()));
                }
                if (intFull != -1) {
                    crit.setFull_assessment(intFull);
                }
                ostomyVO = (AssessmentOstomyVO) woundAssessmentDAO.findLastActiveAssessment(crit, false);
                //until we ahve a difference between full/partial always get last noe regardless
                if (ostomyVO != null) {
                    ostomyVO.setFull_assessment(assess.getFull_assessment());
                }
                return ostomyVO;
            } else if (alpha_type.equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                AssessmentIncisionVO crit = new AssessmentIncisionVO();
                crit.setAlpha_id(new Integer(assess.getAlpha_id()));
                if (assess.getWound_id() != null) {
                    crit.setWound_id(new Integer(assess.getWound_id()));
                }
                incisionVO = (AssessmentIncisionVO) woundAssessmentDAO.findLastActiveAssessment(crit, false);
                //until we ahve a difference between full/partial always get last noe regardless
                if (incisionVO != null && assess!=null) {
                    incisionVO.setFull_assessment(assess.getFull_assessment());
                }
                if(incisionVO!=null){
                    incisionVO.setPain_comments("");
                }
                return incisionVO;
            } else if (alpha_type.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                AssessmentDrainVO crit = new AssessmentDrainVO();
                crit.setAlpha_id(new Integer(assess.getAlpha_id()));
                if (assess.getWound_id() != null) {
                    crit.setWound_id(new Integer(assess.getWound_id()));
                }
                drainVO = (AssessmentDrainVO) woundAssessmentDAO.findLastActiveAssessment(crit, false);
                //until we ahve a difference between full/partial always get last noe regardless
                if (drainVO != null && assess!=null) {
                    drainVO.setFull_assessment(assess.getFull_assessment());
                }
                if(drainVO!=null){
                    drainVO.setPain_comments("");
                }
                return drainVO;
            } else if (alpha_type.equals(Constants.BURN_PROFILE_TYPE)) {
                AssessmentBurnVO crit = new AssessmentBurnVO();
                crit.setAlpha_id(new Integer(assess.getAlpha_id()));
                if (assess.getWound_id() != null) {
                    crit.setWound_id(new Integer(assess.getWound_id()));
                }
                burnVO = (AssessmentBurnVO) woundAssessmentDAO.findLastActiveAssessment(crit, false);
                //until we ahve a difference between full/partial always get last noe regardless
                if (burnVO != null && assess!=null) {
                    burnVO.setFull_assessment(assess.getFull_assessment());
                }
                if(burnVO!=null){
                    burnVO.setPain_comments("");
                }
                return burnVO;
            } else if (alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {
                AssessmentSkinVO crit = new AssessmentSkinVO();
                crit.setAlpha_id(new Integer(assess.getAlpha_id()));
                if (assess.getWound_id() != null) {
                    crit.setWound_id(new Integer(assess.getWound_id()));
                }
                skinVO = (AssessmentSkinVO) woundAssessmentDAO.findLastActiveAssessment(crit, false);
                //until we ahve a difference between full/partial always get last noe regardless
                if (skinVO != null && assess!=null) {
                    skinVO.setFull_assessment(assess.getFull_assessment());
                }
                if(skinVO!=null){
                    skinVO.setPain_comments("");
                }
                return skinVO;
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentEachwoundManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
        return null;
    }

    /**
     * Retrieve ostomy assessment data for the given
     * <code>assessment</code>
     *
     * @param assessment to search for
     * @return AssessmentOstomyVO
     */
    public AbstractAssessmentVO getAssessment(AbstractAssessmentVO assessment) throws ApplicationException {
        AssessmentDAO assessmentDAO = new AssessmentDAO();
        try {
            return (AbstractAssessmentVO) assessmentDAO.findByCriteria(assessment);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.getAssessment(): " + e.toString(), e);
        }
    }

    /**
     * Retrieve ostomy assessment data for the given
     * <code>assessment</code>
     *
     * @param assessment to search for
     * @return AssessmentOstomyVO
     */
    public AbstractAssessmentVO getAssessment(AbstractAssessmentVO assessment, boolean showDeleted) throws ApplicationException {
        AssessmentDAO assessmentDAO = new AssessmentDAO();
        try {
            return (AbstractAssessmentVO) assessmentDAO.findByCriteria(assessment, showDeleted);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.getAssessment(): " + e.toString(), e);
        }
    }

    /**
     * Retrieve ostomy assessment data for the given
     * <code>assessment</code>
     *
     * @param assessment to search for
     * @return AssessmentOstomyVO
     */
    public AbstractAssessmentVO getAssessmentWithinRange(AbstractAssessmentVO assessment, Date start_date, Date end_date) throws ApplicationException {
        AssessmentDAO assessmentDAO = new AssessmentDAO();
        try {
            return (AbstractAssessmentVO) assessmentDAO.findByCriteriaWithinRange(assessment, start_date, end_date);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.getAssessment(): " + e.toString(), e);
        }
    }

    /**
     * Retrieve ostomy assessment data for the given
     * <code>assessment</code>
     *
     * @param assessment to search for
     * @return AssessmentOstomyVO
     * @deprecated
     * @todo remove
     */
    public Collection getAssessmentsWithinRange(AbstractAssessmentVO assessment, Date start_date, Date end_date) throws ApplicationException {
        AssessmentDAO assessmentDAO = new AssessmentDAO();
        try {
            return assessmentDAO.findAllByCriteriaWithinRange(assessment, start_date, end_date);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.getAssessment(): " + e.toString(), e);
        }
    }

    /**
     * Returns an AssessmentOstomyVO object that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param primaryKey the assessment_id of the assessment_eachwound record
     * @return the assessment_eachwound record
     * @see AssessmentOstomyVO
     */
    public AbstractAssessmentVO retrieveAssessment(int assessment_id, int alpha_id, String alpha_type) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            if (alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {
                AssessmentEachwoundVO a = new AssessmentEachwoundVO();
                if (assessment_id != -1) {
                    a.setAssessment_id(assessment_id);
                }
                if (alpha_id != -1) {
                    a.setAlpha_id(alpha_id);
                }
                return (AbstractAssessmentVO) assessmentDAO.findByPK(a);
            } else if (alpha_type.equals(Constants.OSTOMY_PROFILE_TYPE)) {
                AssessmentOstomyVO a = new AssessmentOstomyVO();
                if (assessment_id != -1) {
                    a.setAssessment_id(assessment_id);
                }
                if (alpha_id != -1) {
                    a.setAlpha_id(alpha_id);
                }
                return (AbstractAssessmentVO) assessmentDAO.findByPK(a);
            } else if (alpha_type.equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                AssessmentIncisionVO a = new AssessmentIncisionVO();
                if (assessment_id != -1) {
                    a.setAssessment_id(assessment_id);
                }
                if (alpha_id != -1) {
                    a.setAlpha_id(alpha_id);
                }
                return (AbstractAssessmentVO) assessmentDAO.findByPK(a);
            } else if (alpha_type.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                AssessmentDrainVO a = new AssessmentDrainVO();
                if (assessment_id != -1) {
                    a.setAssessment_id(assessment_id);
                }
                if (alpha_id != -1) {
                    a.setAlpha_id(alpha_id);
                }
                return (AbstractAssessmentVO) assessmentDAO.findByPK(a);
            } else if (alpha_type.equals(Constants.BURN_PROFILE_TYPE)) {
                AssessmentBurnVO a = new AssessmentBurnVO();
                if (assessment_id != -1) {
                    a.setAssessment_id(assessment_id);
                }
                if (alpha_id != -1) {
                    a.setAlpha_id(alpha_id);
                }
                return (AbstractAssessmentVO) assessmentDAO.findByPK(a);
            } else if (alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {
                AssessmentSkinVO a = new AssessmentSkinVO();
                if (assessment_id != -1) {
                    a.setAssessment_id(assessment_id);
                }
                if (alpha_id != -1) {
                    a.setAlpha_id(alpha_id);
                }
                return (AbstractAssessmentVO) assessmentDAO.findByPK(a);
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAssessmentEachwound(): " + e.toString(), e);
        }
        return new AbstractAssessmentVO();
    }

    public void mergeProducts(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        AssessmentDAO assessmentDAO = new AssessmentDAO();
        IntegrationDAO integrationDAO = new IntegrationDAO();
        try {
            AssessmentProductVO i = new AssessmentProductVO();
            i.setPatient_id(patient_from);
            Collection<AssessmentProductVO> many = assessmentDAO.findAllProductsByCriteria(i);
            for (AssessmentProductVO a : many) {
                a.setPatient_id(patient_to);
                saveProduct(a);
                event.setMerge(1);
                event.setPatient_to(patient_to);
                event.setPatient_from(patient_from);
                event.setTable_name("assessment_product");
                event.setRow_id(a.getId());
                try {
                    integrationDAO.insert(event);// saving event, so we can rollback if need be.
                } catch (DataAccessException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAssessmentEachwound(): " + e.toString(), e);
        }
    }

    public Collection findAllPastAssessmentsByCriteria(AbstractAssessmentVO t) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            AssessmentEachwoundVO each = new AssessmentEachwoundVO();
            each.setWound_id(t.getWound_id());
            each.setAssessment_id(t.getAssessment_id());
            each.setAlpha_id(t.getAlpha_id());
            each.setWound_profile_type_id(t.getWound_profile_type_id());
            each.setDeleted(t.getDeleted());
            AssessmentOstomyVO ostomy = new AssessmentOstomyVO();
            ostomy.setWound_id(t.getWound_id());
            ostomy.setAssessment_id(t.getAssessment_id());
            ostomy.setAlpha_id(t.getAlpha_id());
            ostomy.setWound_profile_type_id(t.getWound_profile_type_id());
            ostomy.setDeleted(t.getDeleted());
            AssessmentIncisionVO incision = new AssessmentIncisionVO();
            incision.setWound_id(t.getWound_id());
            incision.setAssessment_id(t.getAssessment_id());
            incision.setAlpha_id(t.getAlpha_id());
            incision.setWound_profile_type_id(t.getWound_profile_type_id());
            incision.setDeleted(t.getDeleted());
            AssessmentDrainVO drain = new AssessmentDrainVO();
            drain.setWound_id(t.getWound_id());
            drain.setAssessment_id(t.getAssessment_id());
            drain.setAlpha_id(t.getAlpha_id());
            drain.setWound_profile_type_id(t.getWound_profile_type_id());
            drain.setDeleted(t.getDeleted());
            AssessmentBurnVO burn = new AssessmentBurnVO();
            burn.setWound_id(t.getWound_id());
            burn.setAssessment_id(t.getAssessment_id());
            burn.setAlpha_id(t.getAlpha_id());
            burn.setWound_profile_type_id(t.getWound_profile_type_id());
            burn.setDeleted(t.getDeleted());

            AssessmentSkinVO skin = new AssessmentSkinVO();
            skin.setWound_id(t.getWound_id());
            skin.setAssessment_id(t.getAssessment_id());
            skin.setAlpha_id(t.getAlpha_id());
            skin.setWound_profile_type_id(t.getWound_profile_type_id());
            skin.setDeleted(t.getDeleted());

            Collection wc = assessmentDAO.findAllPastAssessmentsByWoundIdDescending(each);
            Collection oc = assessmentDAO.findAllPastAssessmentsByWoundIdDescending(ostomy);
            Collection in = assessmentDAO.findAllPastAssessmentsByWoundIdDescending(incision);
            Collection dr = assessmentDAO.findAllPastAssessmentsByWoundIdDescending(drain);
            Collection bu = assessmentDAO.findAllPastAssessmentsByWoundIdDescending(burn);
            Collection sk = assessmentDAO.findAllPastAssessmentsByWoundIdDescending(skin);
            wc.addAll(oc);
            wc.addAll(in);
            wc.addAll(dr);
            wc.addAll(bu);
            wc.addAll(sk);
            return wc;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAssessmentEachwound(): " + e.toString(), e);
        }
    }

    public void saveProduct(AssessmentProductVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.updateProduct(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveProduct has thrown an error:" + e.getMessage());
        }
    }

    public void dropTMPAssessmentByAlpha(int patient_id, int alpha_id) {
        AssessmentDAO assessDAO = new AssessmentDAO();
        AbstractAssessmentVO assess = new AbstractAssessmentVO();
        assess.setPatient_id(patient_id);
        assess.setAlpha_id(alpha_id);
        assess.setActive(new Integer(0));
        AssessmentEachwoundVO assessA = new AssessmentEachwoundVO();
        assessA.setPatient_id(patient_id);
        assessA.setAlpha_id(alpha_id);
        assessA.setActive(new Integer(0));
        removeAssessment(assessA);
        AssessmentIncisionVO assessI = new AssessmentIncisionVO();
        assessI.setPatient_id(patient_id);
        assessI.setAlpha_id(alpha_id);
        assessI.setActive(new Integer(0));
        removeAssessment(assessI);
        AssessmentDrainVO assessD = new AssessmentDrainVO();
        assessD.setPatient_id(patient_id);
        assessD.setAlpha_id(alpha_id);
        assessD.setActive(new Integer(0));
        removeAssessment(assessD);
        AssessmentOstomyVO assessO = new AssessmentOstomyVO();
        assessO.setPatient_id(patient_id);
        assessO.setAlpha_id(alpha_id);
        assessO.setActive(new Integer(0));
        removeAssessment(assessO);
        AssessmentBurnVO assessB = new AssessmentBurnVO();
        assessB.setPatient_id(patient_id);
        assessB.setAlpha_id(alpha_id);
        assessB.setActive(new Integer(0));
        removeAssessment(assessB);
        AssessmentSkinVO assessS = new AssessmentSkinVO();
        assessS.setPatient_id(patient_id);
        assessS.setAlpha_id(alpha_id);
        assessS.setActive(new Integer(0));
        removeAssessment(assessS);
    }

    public void removeAssessment(AbstractAssessmentVO a) {
        AssessmentDAO dao = new AssessmentDAO();
        try {
            Collection<AbstractAssessmentVO> all_for_delete = null;
            if (a instanceof AssessmentEachwoundVO) {
                all_for_delete = getAllAssessments((AssessmentEachwoundVO) a, -1, false);
            } else if (a instanceof AssessmentDrainVO) {
                all_for_delete = getAllAssessments((AssessmentDrainVO) a, -1, false);
            } else if (a instanceof AssessmentIncisionVO) {
                all_for_delete = getAllAssessments((AssessmentIncisionVO) a, -1, false);
            } else if (a instanceof AssessmentOstomyVO) {
                all_for_delete = getAllAssessments((AssessmentOstomyVO) a, -1, false);
            } else if (a instanceof AssessmentBurnVO) {
                all_for_delete = getAllAssessments((AssessmentBurnVO) a, -1, false);
            } else if (a instanceof AssessmentSkinVO) {
                all_for_delete = getAllAssessments((AssessmentSkinVO) a, -1, false);
            }
            dao.delete(a);
            for (AbstractAssessmentVO obj : all_for_delete) {
                if (obj instanceof AssessmentEachwoundVO) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(obj.getWoundAssessment().getProfessional_id(), obj.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "assessment_eachwound", obj));
                } else if (obj instanceof AssessmentDrainVO) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(obj.getWoundAssessment().getProfessional_id(), obj.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "assessment_drain", obj));
                } else if (obj instanceof AssessmentIncisionVO) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(obj.getWoundAssessment().getProfessional_id(), obj.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "assessment_incision", obj));
                } else if (obj instanceof AssessmentOstomyVO) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(obj.getWoundAssessment().getProfessional_id(), obj.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "assessment_ostomy", obj));
                } else if (obj instanceof AssessmentBurnVO) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(obj.getWoundAssessment().getProfessional_id(), obj.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "assessment_burn", obj));
                } else if (obj instanceof AssessmentSkinVO) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(obj.getWoundAssessment().getProfessional_id(), obj.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "assessment_skin", obj));
                }
            }
        } catch (ApplicationException e) {
        }
    }

    public void removeProducts(AssessmentProductVO prods) {
        AssessmentDAO dao = new AssessmentDAO();
        dao.deleteProducts(prods);
    }

    public void moveAssessment(int alpha_id_to, Integer wound_type, String moved_reason, int assessment_id, ProfessionalVO userVO) throws ApplicationException {
        try {
            AssessmentServiceImpl assess = new AssessmentServiceImpl(Constants.ENGLISH);
            AssessmentImagesServiceImpl imagesService = new AssessmentImagesServiceImpl();
            WoundAssessmentLocationServiceImpl walmanager = new WoundAssessmentLocationServiceImpl();
            if (wound_type.equals(Constants.WOUND_ASSESSMENT)) {
                AssessmentEachwoundVO a = new AssessmentEachwoundVO();
                a.setId(assessment_id);
                AbstractAssessmentVO assessment = assess.getAssessment(a);
                if (assessment != null) {
                    int alpha_id = assessment.getAlpha_id();
                    assessment.setDeleted(1);
                    assessment.setDeleted_reason(moved_reason);
                    assess.saveAssessment(assessment);//saving previous assessment which is now deleted.
                    assessment.setDeleted(0);
                    assessment.setDeleted_reason("");
                    assessment.setAlpha_id(alpha_id_to);
                    assessment.setId(null);//save a new assessment.. 
                    assess.saveAssessment(assessment);
                    AssessmentImagesVO im = new AssessmentImagesVO();
                    im.setAlpha_id(alpha_id);
                    im.setAssessment_id(assessment.getAssessment_id());
                    Collection<AssessmentImagesVO> images = imagesService.getAllImages(im);
                    for (AssessmentImagesVO img : images) {
                        AssessmentImagesVO remImg = img;
                        imagesService.removeImage(remImg);
                        img.setAlpha_id(alpha_id_to);
                        img.setId(null);
                        imagesService.saveImage(img);
                    }
                    if (Integer.toString(assessment.getStatus()).equals(Common.getConfig("woundClosed"))) {
                        walmanager.reopenAlpha(alpha_id);
                        //close new alpha
                        WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                        locateVO.setId(alpha_id_to);
                        WoundAssessmentLocationVO resultVO = walmanager.getAlpha(locateVO, true, false, null);
                        if (resultVO != null && resultVO.getDischarge().equals(0)) {
                            resultVO.setDischarge(new Integer("1"));
                            resultVO.setClose_timestamp(new Date());
                            walmanager.saveAlpha(resultVO);
                        }
                    }
                }
            } else if (wound_type.equals(Constants.OSTOMY_ASSESSMENT)) {
                AssessmentOstomyVO a = new AssessmentOstomyVO();
                a.setId(assessment_id);
                AbstractAssessmentVO assessment = assess.getAssessment(a);
                if (assessment != null) {
                    int alpha_id = assessment.getAlpha_id();
                    assessment.setDeleted(1);
                    assessment.setDeleted_reason(moved_reason);
                    assess.saveAssessment(assessment);//saving previous assessment which is now deleted.
                    assessment.setDeleted(0);
                    assessment.setDeleted_reason("");
                    assessment.setAlpha_id(alpha_id_to);
                    assessment.setId(null);//save a new assessment..
                    assess.saveAssessment(assessment);
                    AssessmentImagesVO im = new AssessmentImagesVO();
                    im.setAlpha_id(alpha_id);
                    im.setAssessment_id(assessment.getAssessment_id());
                    Collection<AssessmentImagesVO> images = imagesService.getAllImages(im);
                    for (AssessmentImagesVO img : images) {
                        AssessmentImagesVO remImg = img;
                        imagesService.removeImage(remImg);
                        img.setAlpha_id(alpha_id_to);
                        img.setId(null);
                        imagesService.saveImage(img);
                    }
                    if (Integer.toString(assessment.getStatus()).equals(Common.getConfig("ostomyClosed"))) {
                        walmanager.reopenAlpha(alpha_id);
                        //close new alpha
                        WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                        locateVO.setId(alpha_id_to);
                        WoundAssessmentLocationVO resultVO = walmanager.getAlpha(locateVO, true, false, null);
                        if (resultVO != null && resultVO.getDischarge().equals(0)) {
                            resultVO.setDischarge(new Integer("1"));
                            resultVO.setClose_timestamp(new Date());
                            walmanager.saveAlpha(resultVO);
                        }
                    }
                }
            } else if (wound_type.equals(Constants.SKIN_ASSESSMENT)) {
                AssessmentSkinVO a = new AssessmentSkinVO();
                a.setId(assessment_id);
                AbstractAssessmentVO assessment = assess.getAssessment(a);
                if (assessment != null) {
                    int alpha_id = assessment.getAlpha_id();
                    assessment.setDeleted(1);
                    assessment.setDeleted_reason(moved_reason);
                    assess.saveAssessment(assessment);//saving previous assessment which is now deleted.
                    assessment.setDeleted(0);
                    assessment.setDeleted_reason("");
                    assessment.setAlpha_id(alpha_id_to);
                    assessment.setId(null);//save a new assessment..
                    assess.saveAssessment(assessment);
                    AssessmentImagesVO im = new AssessmentImagesVO();
                    im.setAlpha_id(alpha_id);
                    im.setAssessment_id(assessment.getAssessment_id());
                    Collection<AssessmentImagesVO> images = imagesService.getAllImages(im);
                    for (AssessmentImagesVO img : images) {
                        AssessmentImagesVO remImg = img;
                        imagesService.removeImage(remImg);
                        img.setAlpha_id(alpha_id_to);
                        img.setId(null);
                        imagesService.saveImage(img);
                    }
                    if (Integer.toString(assessment.getStatus()).equals(Common.getConfig("skinClosed"))) {
                        walmanager.reopenAlpha(alpha_id);
                        //close new alpha
                        WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                        locateVO.setId(alpha_id_to);
                        WoundAssessmentLocationVO resultVO = walmanager.getAlpha(locateVO, true, false, null);
                        if (resultVO != null && resultVO.getDischarge().equals(0)) {
                            resultVO.setDischarge(new Integer("1"));
                            resultVO.setClose_timestamp(new Date());
                            walmanager.saveAlpha(resultVO);
                        }
                    }
                }
            } else if (wound_type.equals(Constants.DRAIN_ASSESSMENT)) {
                AssessmentDrainVO a = new AssessmentDrainVO();
                a.setId(assessment_id);
                AbstractAssessmentVO assessment = assess.getAssessment(a);
                if (assessment != null) {
                    int alpha_id = assessment.getAlpha_id();
                    assessment.setDeleted(1);
                    assessment.setDeleted_reason(moved_reason);
                    assess.saveAssessment(assessment);//saving previous assessment which is now deleted.
                    assessment.setDeleted(0);
                    assessment.setDeleted_reason("");
                    assessment.setAlpha_id(alpha_id_to);
                    assessment.setId(null);//save a new assessment..
                    assess.saveAssessment(assessment);
                    AssessmentImagesVO im = new AssessmentImagesVO();
                    im.setAlpha_id(alpha_id);
                    im.setAssessment_id(assessment.getAssessment_id());
                    Collection<AssessmentImagesVO> images = imagesService.getAllImages(im);
                    for (AssessmentImagesVO img : images) {
                        AssessmentImagesVO remImg = img;
                        imagesService.removeImage(remImg);
                        img.setAlpha_id(alpha_id_to);
                        img.setId(null);
                        imagesService.saveImage(img);
                    }
                    if (Integer.toString(assessment.getStatus()).equals(Common.getConfig("drainClosed"))) {
                        walmanager.reopenAlpha(alpha_id);
                        //close new alpha
                        WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                        locateVO.setId(alpha_id_to);
                        WoundAssessmentLocationVO resultVO = walmanager.getAlpha(locateVO, true, false, null);
                        if (resultVO != null && resultVO.getDischarge().equals(0)) {
                            resultVO.setDischarge(new Integer("1"));
                            resultVO.setClose_timestamp(new Date());
                            walmanager.saveAlpha(resultVO);
                        }
                    }
                }
            } else if (wound_type.equals(Constants.INCISION_ASSESSMENT)) {
                AssessmentIncisionVO a = new AssessmentIncisionVO();
                a.setId(assessment_id);
                AbstractAssessmentVO assessment = assess.getAssessment(a);
                if (assessment != null) {
                    int alpha_id = assessment.getAlpha_id();
                    assessment.setDeleted(1);
                    assessment.setDeleted_reason(moved_reason);
                    assess.saveAssessment(assessment);//saving previous assessment which is now deleted.
                    assessment.setDeleted(0);
                    assessment.setDeleted_reason("");
                    assessment.setAlpha_id(alpha_id_to);
                    assessment.setId(null);//save a new assessment.. 
                    assess.saveAssessment(assessment);
                    AssessmentImagesVO im = new AssessmentImagesVO();
                    im.setAlpha_id(alpha_id);
                    im.setAssessment_id(assessment.getAssessment_id());
                    Collection<AssessmentImagesVO> images = imagesService.getAllImages(im);
                    for (AssessmentImagesVO img : images) {
                        AssessmentImagesVO remImg = img;
                        imagesService.removeImage(remImg);
                        img.setAlpha_id(alpha_id_to);
                        img.setId(null);
                        imagesService.saveImage(img);
                    }
                    if (Integer.toString(assessment.getStatus()).equals(Common.getConfig("incisionClosed"))) {
                        walmanager.reopenAlpha(alpha_id);
                        //close new alpha
                        WoundAssessmentLocationVO locateVO = new WoundAssessmentLocationVO();
                        locateVO.setId(alpha_id_to);
                        WoundAssessmentLocationVO resultVO = walmanager.getAlpha(locateVO, true, false, null);
                        if (resultVO != null && resultVO.getDischarge().equals(1)) {
                            resultVO.setDischarge(new Integer("0"));
                            resultVO.setClose_timestamp(new Date());
                            walmanager.saveAlpha(resultVO);
                        }
                    }
                }
            }

            //
        } catch (ApplicationException e) {
            log.error("Error in UserBD.addPatient(): " + e.toString(), e);
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
    }

    public Collection getAllAntibiotics(AssessmentAntibioticVO assess) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    public void saveAntibiotic(AssessmentAntibioticVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.saveAntibiotic(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public void removeAntibiotic(AssessmentAntibioticVO p) {
        AssessmentDAO dao = new AssessmentDAO();
        dao.deleteAntibiotic(p);
    }

    public void removeUndermining(AssessmentUnderminingVO p) {
        AssessmentDAO dao = new AssessmentDAO();
        dao.deleteUndermining(p);
    }

    public void removeWoundBed(AssessmentWoundBedVO p) {
        AssessmentDAO dao = new AssessmentDAO();
        dao.deleteWoundBed(p);
    }

    public void removeSinustract(AssessmentSinustractsVO p) {
        AssessmentDAO dao = new AssessmentDAO();
        dao.deleteSinustract(p);
    }

    public void saveSinustract(AssessmentSinustractsVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.saveSinustract(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public void saveUndermining(AssessmentUnderminingVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.saveUndermining(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public void saveWoundBed(AssessmentWoundBedVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.saveWoundBed(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public Collection getAllWoundBeds(AssessmentWoundBedVO assess) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    public Collection getAllSinustracts(AssessmentSinustractsVO assess) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    public Collection getAllUndermining(AssessmentUnderminingVO assess) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    public Collection getAllSeperations(AssessmentMucocSeperationsVO assess) throws ApplicationException {
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }

    public void saveSeperation(AssessmentMucocSeperationsVO vo) throws ApplicationException {
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            assessmentDAO.saveSeperation(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }

    public void removeSeperation(AssessmentMucocSeperationsVO p) {
        AssessmentDAO dao = new AssessmentDAO();
        dao.deleteSeperation(p);
    }

    /**
     * Returns the auto heal rate reason if triggered.
     * <p/>
     * This record will return an empty string if nothing is triggered.
     *
     * @param lngTimestampNow the timestamp which is either current, or when
     * assessment was saved (in offline's case)
     * @param assessment_id (if from online = -1)
     * @return the referral reason
     */
    public Double getHealRate(int alpha_id, int assessment_id, Integer language) throws ApplicationException {

        //default to English, method doesn't require translation
        AssessmentServiceImpl eachBD = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
        WoundServiceImpl woundBD = new WoundServiceImpl(language);
        ListServiceImpl listmanager = new ListServiceImpl(language);
        
        PDate pdate = new PDate();
        Double heal_rate = null;
        try {

            // Find ID of the unsaved wound assessment
            WoundAssessmentVO wavTMP = new WoundAssessmentVO();
            if (assessment_id > 0) {
                wavTMP = new WoundAssessmentVO();
                wavTMP.setId(assessment_id);

                int intHealDays = new Integer((String) Common.getConfig("autoReferralsHealDays")).intValue();
                Date lngTimestampHealCheck = PDate.subtractDays(new Date(), intHealDays);
                WoundAssessmentVO woundAssessmentVO = wamanager.getAssessment(wavTMP);
                //Find alpha I                 D's for the unsaved wound assessments
                if (woundAssessmentVO != null) {
                    Collection<AbstractAssessmentVO> assessments = eachBD.getAllAssessments(woundAssessmentVO.getId());
                    //Loop for all unsaved wound care alphas
                    for (AbstractAssessmentVO unsavedassessment : assessments) {


                        //Alpha Wound types allowed.
                        if (unsavedassessment.getAlphaLocation().getWound_profile_type().getAlpha_type().equals("A")) {
                            AssessmentEachwoundVO assess = new AssessmentEachwoundVO();
                            assess.setAlpha_id(unsavedassessment.getAlpha_id());
                            assess.setFull_assessment(1);
                            double intCurrentHealRate = -1;
                            double intOldHealRate = -1;
                            double intLength = 0;
                            double intWidth = 0;
                            double intDepth = 0;
                            double intLength_old = 0;
                            double intWidth_old = 0;
                            double intDepth_old = 0;
                            String strOldHealRateDate = "";
                            AbstractAssessmentVO results = eachBD.getAssessmentWithinRange(assess, null, lngTimestampHealCheck);
                            AssessmentEachwoundVO checkVO = (AssessmentEachwoundVO) results;
                            if (checkVO != null) {

                                strOldHealRateDate = pdate.getDateString(checkVO.getCreated_on(),locale);
                                if ((checkVO.getWidth() > -1)) {

                                // Get dimensions, multiply centimeters by 10 to get it into an integer
                                intLength_old = (checkVO.getLength());
                                intWidth_old = (checkVO.getWidth());
                                 //intDepth_old = (checkVO.getDepth_cm().equals(new Integer(-1)) ? new Integer(0) : checkVO.getDepth_cm()) * 10 + (checkVO.getDepth_mm().equals(new Integer(-1)) ? new Integer(0) : checkVO.getDepth_mm());
                                    if (intLength_old == 0) {
                                        intLength_old = 1;
                                    }
                                    if (intWidth_old == 0) {
                                        intWidth_old = 1;
                                    }
                                    if (intDepth_old != 0) {
                                        intOldHealRate = intLength_old * intWidth_old;// * intDepth_old;
                                    } else {
                                        intOldHealRate = intLength_old * intWidth_old;// * 1;
                                    }
                                }
                                AssessmentEachwoundVO unsaved = (AssessmentEachwoundVO) unsavedassessment;
                               if ((unsaved.getWidth() > -1) && unsaved.getLength() > -1) {
                                // Get dimensions, multiply centimeters by 10 to get it into an integer
                                intLength = unsaved.getLength();
                                intWidth = unsaved.getWidth();
                              //intDepth = (unsaved.getDepth_cm().equals(new Integer(-1)) ? new Integer(0) : unsaved.getDepth_cm()) * 10 + (unsaved.getDepth_mm().equals(new Integer(-1)) ? new Integer(0) : unsaved.getDepth_mm());
                                    if (intLength == 0) {
                                        intLength = 1;
                                    }
                                    if (intWidth == 0) {
                                        intWidth = 1;
                                    }
                                    if (intDepth != 0) {
                                        intCurrentHealRate = intLength * intWidth;// * intDepth;
                                    } else {
                                        intCurrentHealRate = intLength * intWidth;// * 1;
                                    }
                                }
                            }

                            if (intOldHealRate != -1 && intCurrentHealRate != -1) {
                                double divisible = (intCurrentHealRate / intOldHealRate);
                                double changed = (intOldHealRate - intCurrentHealRate) / intCurrentHealRate;
                                //((y2 - y1) / y1)*100 = your percentage change.
                                double intHealRatePerc = changed;
                                DecimalFormat f = new DecimalFormat("##.00");
                                heal_rate = Double.parseDouble(f.format(intHealRatePerc));


                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return heal_rate;
    }
}
