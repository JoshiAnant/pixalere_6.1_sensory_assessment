package com.pixalere.assessment.service;
import com.pixalere.assessment.bean.AssessmentTreatmentArraysVO;
import com.pixalere.assessment.bean.AssessmentTreatmentVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface TreatmentCommentsService {
    public TreatmentCommentVO getTreatmentComment(@WebParam(name="crit") TreatmentCommentVO crit) throws ApplicationException;
    public void saveTreatmentComment(@WebParam(name="updateRecord") TreatmentCommentVO updateRecord) throws ApplicationException;
    public Collection<TreatmentCommentVO> getAllTreatmentComments(@WebParam(name="records") TreatmentCommentVO records, @WebParam(name="limit")int limit) throws ApplicationException;
    public AssessmentTreatmentVO getAssessmentTreatment(@WebParam(name="crit") AssessmentTreatmentVO crit) throws ApplicationException;
    public void saveAssessmentTreatment(@WebParam(name="updateRecord") AssessmentTreatmentVO updateRecord) throws ApplicationException;
    public void saveTreatmentArray(@WebParam(name="records") AssessmentTreatmentArraysVO array)throws ApplicationException;
    
}