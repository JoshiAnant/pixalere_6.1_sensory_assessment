package com.pixalere.assessment.service;
/**
 *     This is the assessment images service implementation
 *     Refer to {@link com.pixalere.assessment.service.AssessmentImagesService } to see if there are
 *     any web services available.
 *
 *     <img src="AssessmentImagesServiceImpl.png"/>
 *
 *
 *     @view  AssessmentImagsServiceImpl
 *
 *     @match class com.pixalere.assessment.*
 *     @opt hide
 *     @match class com.pixalere.assessment\.(dao.AssessmentImagesDAO|bean.AssessmentImagesVO|service.AssessmentImagesService)
 *     @opt !hide
 *
*/
import com.pixalere.guibeans.AssessmentListVO;
import com.pixalere.guibeans.ImagesListVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.dao.*;
import com.pixalere.common.*;
import com.pixalere.utils.PDate;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.Common;
import java.util.Vector;
import java.util.Collection;
import com.pixalere.utils.Constants;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;
import java.io.File;
import java.io.IOException;
import com.pixalere.integration.bean.EventVO;
@WebService(endpointInterface = "com.pixalere.assessment.service.AssessmentImagesService", serviceName = "AssessmentImagesService")
public class AssessmentImagesServiceImpl implements AssessmentImagesService {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentImagesServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    AssessmentDAO dao = null;
    public AssessmentImagesServiceImpl() {
        
        dao = new AssessmentDAO();
    }
  
    public Collection getAllImages(AssessmentImagesVO images) throws ApplicationException {
        Collection imagesVO = null;
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            imagesVO = assessmentDAO.findAllImagesByCriteria(images);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
        return imagesVO;
    }
    public void mergeImages(int patient_from,int patient_to,EventVO event) throws ApplicationException {
        AssessmentImagesVO i = new AssessmentImagesVO();
        com.pixalere.integration.dao.IntegrationDAO integrationDAO = new  com.pixalere.integration.dao.IntegrationDAO();
        i.setPatient_id(patient_from);
        Collection<AssessmentImagesVO> images = getAllImages(i);
        for(AssessmentImagesVO img : images){
            img.setPatient_id(patient_to);
            saveImage(img);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_images");
            event.setRow_id(img.getId());
            try{
                integrationDAO.insert(event);
            }catch(DataAccessException ex){
                ex.printStackTrace();
            }
        }
    }
    public Vector<AssessmentImagesVO> getAllImageFiles(AssessmentImagesVO images) throws ApplicationException {
        Collection<AssessmentImagesVO> imagesVO = null;
        Vector files = new Vector();
        try {
            AssessmentDAO assessmentDAO = new AssessmentDAO();
            imagesVO = assessmentDAO.findAllImagesByCriteria(images);
            for (AssessmentImagesVO i : imagesVO) {
                //Vector filenames = Serialize.arrayIze(i.getImages());
                //String filename = "";
                /*if (filenames.size() > 0) {
                    filename = (String) filenames.get(0);
                }*/
                if (!i.getImages().equals("")) {
                    File f = new File(Common.getPhotosPath() + "/" + i.getPatient_id() + "/" + i.getAssessment_id() + "/" + i.getImages());
                    if(f.exists() == true){
                        DataSource ds = new FileDataSource(new File(Common.getPhotosPath() + "/" + i.getPatient_id() + "/" + i.getAssessment_id() + "/" + i.getImages()));
                        i.setImagefile(new DataHandler(ds));
                        files.add(i);
                    }
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
        return files;
    }
    public AssessmentImagesVO getImage(int id) throws ApplicationException {
        AssessmentImagesVO woundVO = null;
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            AssessmentImagesVO img = new AssessmentImagesVO();
            img.setId(id);
            woundVO = (AssessmentImagesVO) woundAssessmentDAO.findImageByCriteria(img);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
        return woundVO;
    }
    public AssessmentImagesVO getImage(AssessmentImagesVO img) throws ApplicationException {
        AssessmentImagesVO woundVO = null;
        try {
            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
            woundVO = (AssessmentImagesVO) woundAssessmentDAO.findImageByCriteria(img);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
        return woundVO;
    }
    public Vector getImageList(String assessment_id, Integer wound_profile_type_id,int language) throws ApplicationException {
        Vector asssess_images = new Vector();
        WoundAssessmentLocationServiceImpl wal = new WoundAssessmentLocationServiceImpl();
        try {
            String locale = Common.getLanguageLocale(language);
            
            WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
            AssessmentImagesVO imgt = new AssessmentImagesVO();
            imgt.setAssessment_id(new Integer(assessment_id));
            imgt.setWound_profile_type_id(wound_profile_type_id);
            Collection<AssessmentImagesVO> imgs = getAllImages(imgt);// get all images for this assessment
            
            Vector imageVect = new Vector();
            for (AssessmentImagesVO images : imgs) {
                if (images != null && !images.getImages().equals("") && !images.getImages().equals("a:0:{}")) {
                    //Vector<String> imageVectTmp = Serialize.arrayIze(images.getImages());
                    //for (String img : imageVectTmp) {
                        String img = images.getImages();
                        ImagesListVO list = new ImagesListVO();
                        String strAlphaDetails = "";
                        if (images.getId() != null) {
                            if (images.getId() == 0) {
                                strAlphaDetails = Common.getLocalizedString("pixalere.viewer.form.misc_image",locale);
                            } else {
                                WoundAssessmentLocationVO alpha_details_tmp = new WoundAssessmentLocationVO();
                                alpha_details_tmp.setId(images.getAlpha_id());
                                WoundAssessmentLocationVO alpha_details = wal.getAlpha(alpha_details_tmp, true, false, null);
                                if (alpha_details!=null && alpha_details.getWound_profile_type().getAlpha_type().equals("T")) {
                                    String n = alpha_details.getAlpha().substring(3, 4);
                                    strAlphaDetails = Common.getLocalizedString("pixalere.viewer.form.assessment_incisiontag",locale) + " #" + n;
                                }
                                if (alpha_details!=null && alpha_details.getWound_profile_type().getAlpha_type().equals("S")) {
                                    String n = alpha_details.getAlpha().substring(1, 1);
                                    strAlphaDetails = Common.getLocalizedString("pixalere.viewer.form.assessment_skin",locale) + " #" + n;
                                }
                                if (alpha_details!=null && alpha_details.getWound_profile_type().getAlpha_type().equals("D")) {
                                    strAlphaDetails = Common.getLocalizedString("pixalere.viewer.form.assessment_drain",locale) + " #" + alpha_details.getAlpha();
                                }
                                if (alpha_details!=null && alpha_details.getWound_profile_type().getAlpha_type().equals("A")) {
                                    strAlphaDetails = Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare",locale) + " " + alpha_details.getAlpha();
                                }
                                if (alpha_details!=null && alpha_details.getWound_profile_type().getAlpha_type().equals("O") && alpha_details.getAlpha().indexOf("ostu")!=-1) {
                                    strAlphaDetails = Common.getLocalizedString("pixalere.viewer.form.assessment_urinalstoma",locale);
                                }
                                if (alpha_details!=null && alpha_details.getWound_profile_type().getAlpha_type().equals("O") && alpha_details.getAlpha().indexOf("ostf")!=-1) {
                                    strAlphaDetails = Common.getLocalizedString("pixalere.viewer.form.assessment_fecalstoma",locale);
                                }
                            }
                        }
                        list.setImage(img);
                        list.setImage_info(strAlphaDetails); // For mouse-over on thumbnails
                        list.setImage_details(strAlphaDetails); // Showing under image, can contain more details if necessary
                        asssess_images.add(list);
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAllAssessmentsByWoundId():" + e.toString(), e);
        }
        return asssess_images;
    }
    /**
     * Updates the Assessment images record to the assessment_images table.
     *
     * @param images the assessment images record
     * @see AssessmentImagesVO
     */
    public void saveImage(AssessmentImagesVO images) throws ApplicationException {
        AssessmentDAO imagesDAO = new AssessmentDAO();
        try {
            boolean imagefailed = false;
            if (images.getImagefile() != null) {
                //Vector<String> imgVect = Serialize.arrayIze(images.getImages());
                //if (imgVect.size() > 0) {
                    
                    //String f = imgVect.get(0);
                    DataHandler dh = images.getImagefile();
                    String fname= Common.getPhotosPath() + "/" + images.getPatient_id() + "/" + images.getAssessment_id() + "/"+images.getImages();
                    DataSource ds = dh.getDataSource();
                    if (fname != null && !fname.equals("")) {
                        
                        try {
                            Common.writeToFile(fname, ds.getInputStream(), true);
                        } catch (IOException ioe) {
                            ioe.printStackTrace(System.err);
                        }
                    }
                //}
            }
            if (imagefailed == false) {
                imagesDAO.insertImages(images);
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            //throw new ApplicationException("Error in AssessmentManager.checkAssessmentExistForProfId: " + e.getMessage(), e);
        } catch(Exception e){//in case there's an error uploading from offline.. don't want it thrown back tho.
        }
    }
    /**
     * Returns assessment ID and timestamp for all assessments that has one or more images
     */
    public Vector retrieveAssessmentDates(String wound_profile_type_id, String wound_id) throws ApplicationException {
        Vector assessments = new Vector();
        try {
            WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
            WoundAssessmentVO woundAssessmentVO = new WoundAssessmentVO();
            woundAssessmentVO.setWound_id(new Integer(wound_id));// retrieve all wound assessments by wound
            AssessmentImagesServiceImpl iService = new AssessmentImagesServiceImpl();
            Collection<WoundAssessmentVO> assessmentsCollection = woundAssessmentDAO.findAllByCriteria(woundAssessmentVO);
            Integer intLastID = null;
            for (WoundAssessmentVO assessVO : assessmentsCollection) {
                AssessmentImagesVO v = new AssessmentImagesVO();
                v.setWound_profile_type_id(new Integer(wound_profile_type_id));
                v.setAssessment_id(assessVO.getId());
                Collection<AssessmentImagesVO> imgs = iService.getAllImages(v);// get all images for this assessment
                ProfessionalVO userVO = assessVO.getProfessional();
                PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
                for (AssessmentImagesVO images : imgs) {
                    if (intLastID != assessVO.getId() && images != null && !images.getImages().equals("") && !images.getImages().equals("a:0:{}")) {
                        intLastID = assessVO.getId();
                        AssessmentListVO list = new AssessmentListVO();
                        list.setAssessment_id(assessVO.getId());
                        list.setTimestamp(assessVO.getCreated_on());
                        assessments.add(list);
                    }
                }
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAssessmentDates():" + e.toString(), e);
        }
        return assessments;
    }
    public void removeImages(String assessment_id, String alpha_id) throws ApplicationException {
        AssessmentDAO imagesDAO = new AssessmentDAO();
        try {
            AssessmentImagesVO imgt = new AssessmentImagesVO();
            imgt.setAssessment_id(new Integer(assessment_id));
            imgt.setAlpha_id(new Integer(alpha_id));
            imgt.setWhichImage(Constants.IMAGE_1);
            AssessmentImagesVO imagesVO = (AssessmentImagesVO) imagesDAO.findImageByCriteria(imgt);
            if (imagesVO != null) {
                imagesDAO.deleteImages(imagesVO);
            }
            imgt.setWhichImage(Constants.IMAGE_2);
            AssessmentImagesVO imagesVO2 = (AssessmentImagesVO) imagesDAO.findImageByCriteria(imgt);
            if (imagesVO2 != null) {
                imagesDAO.deleteImages(imagesVO2);
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("Error in AssessmentManager.removeImages: " + e.getMessage(), e);
        }
    }
    /**
     * Remove data from the given <code>object</code>
     * @param criteria to search for
     */
    public void removeImage(AssessmentImagesVO object) {
        AssessmentDAO assessmentDAO = new AssessmentDAO();
        assessmentDAO.deleteImages(object);
    }
}