package com.pixalere.assessment.service;
import com.pixalere.common.ApplicationException;
import java.util.Collection;
import com.pixalere.assessment.bean.*;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface AssessmentService {
    public AbstractAssessmentVO getAssessment(@WebParam(name = "assessment") AbstractAssessmentVO assessment) throws ApplicationException;
    public AbstractAssessmentVO retrieveAssessment(@WebParam(name = "assessment") int assessment_id, int alpha_id, String alpha_type) throws ApplicationException;
    public void saveAssessment(@WebParam(name = "updateRecord") AbstractAssessmentVO updateRecord) throws ApplicationException;
    public Collection<AssessmentEachwoundVO> getWoundAssessmentsForOffline(@WebParam(name = "assessment") AbstractAssessmentVO assessment) throws ApplicationException;
    public Collection<AssessmentSkinVO> getSkinAssessmentsForOffline(@WebParam(name = "assessment") AbstractAssessmentVO assessment) throws ApplicationException;
    public Collection<AssessmentBurnVO> getBurnAssessmentsForOffline(@WebParam(name = "assessment") AbstractAssessmentVO assessment) throws ApplicationException;
    public Collection<AssessmentDrainVO> getDrainAssessmentsForOffline(@WebParam(name = "assessment") AbstractAssessmentVO assessment) throws ApplicationException;
    public Collection<AssessmentIncisionVO> getIncisionAssessmentsForOffline(@WebParam(name = "assessment") AbstractAssessmentVO assessment) throws ApplicationException;
    public Collection<AssessmentOstomyVO> getOstomyAssessmentsForOffline(@WebParam(name = "assessment") AbstractAssessmentVO assessment) throws ApplicationException;
    public void saveProduct(@WebParam(name = "updateRecord") AssessmentProductVO updateRecord) throws ApplicationException;
    public void saveAntibiotic(@WebParam(name = "updateRecord") AssessmentAntibioticVO updateRecord) throws ApplicationException;
    public Collection<AssessmentProductVO> getAllProducts(@WebParam(name = "assessment") AssessmentProductVO assessment) throws ApplicationException;
    public void saveSeperation(@WebParam(name = "updateRecord") AssessmentMucocSeperationsVO updateRecord) throws ApplicationException;
    public void saveUndermining(@WebParam(name = "updateRecord") AssessmentUnderminingVO updateRecord) throws ApplicationException;
    public void saveSinustract(@WebParam(name = "updateRecord") AssessmentSinustractsVO updateRecord) throws ApplicationException;
    public void saveWoundBed(@WebParam(name = "updateRecord") AssessmentWoundBedVO updateRecord) throws ApplicationException;
    
}