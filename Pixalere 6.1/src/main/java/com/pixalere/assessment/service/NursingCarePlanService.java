package com.pixalere.assessment.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface NursingCarePlanService {
    public NursingCarePlanVO getNursingCarePlan(@WebParam(name="crit") NursingCarePlanVO crit) throws ApplicationException;
    public void saveNursingCarePlan(@WebParam(name="updateRecord") NursingCarePlanVO updateRecord) throws ApplicationException;
    public Collection<NursingCarePlanVO> getAllNursingCarePlans(@WebParam(name="records") NursingCarePlanVO records, @WebParam(name="limit") int limit) throws ApplicationException;
    public DressingChangeFrequencyVO getDressingChangeFrequency(@WebParam(name="crit") DressingChangeFrequencyVO crit) throws ApplicationException;
    public void saveDressingChangeFrequency(@WebParam(name="updateRecord") DressingChangeFrequencyVO updateRecord) throws ApplicationException;
    public Collection<DressingChangeFrequencyVO> getAllDressingChangeFrequencies(@WebParam(name="records") DressingChangeFrequencyVO records, @WebParam(name="limit") int limit) throws ApplicationException;
}