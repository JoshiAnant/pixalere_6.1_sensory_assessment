package com.pixalere.assessment.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface WoundAssessmentService {
    public WoundAssessmentVO getAssessment(@WebParam(name="crit") WoundAssessmentVO crit) throws ApplicationException;
    public void saveAssessment(@WebParam(name="updateRecord") WoundAssessmentVO updateRecord) throws ApplicationException;
    public Collection<WoundAssessmentVO> getAllAssessments(@WebParam(name="records") WoundAssessmentVO records) throws ApplicationException;
    
    public void checkAutoReferralsForOffline(@WebParam(name="assessment_id") int assessment_id,@WebParam(name="wound_profile_type_id") int wound_profile_type_id,@WebParam(name="language") int language) throws ApplicationException;
}