package com.pixalere.assessment.service;
import com.pixalere.assessment.bean.AssessmentTreatmentArraysVO;
import com.pixalere.assessment.bean.AssessmentTreatmentVO;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.dao.*;
import com.pixalere.common.*;
import java.util.Vector;
import java.util.Collection;
import javax.jws.WebService;
import com.pixalere.integration.bean.EventVO;
/**
 *     This is the treatment comments service implementation
 *     Refer to {@link com.pixalere.assessment.service.TreatmentCommentsService } to see if there are
 *     any web services available.
 *
 *     <img src="TreatmentCommentsServiceImpl.png"/>
 *
 *
 *     @view  TreatmentCommentsServiceImpl
 *
 *     @match class com.pixalere.assessment.*
 *     @opt hide
 *     @match class com.pixalere.assessment\.(dao.TreatmentCommentsDAO|bean.TreatmentCommentsVO|service.TreatmentCommentsService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.assessment.service.TreatmentCommentsService", serviceName = "TreatmentCommentsService")
public class TreatmentCommentsServiceImpl implements TreatmentCommentsService {//implements TreatmentCommentsService{
    // Create Log4j category instance for logging
   
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TreatmentCommentsServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    TreatmentCommentDAO dao = null;
    public TreatmentCommentsServiceImpl() {
        dao = new TreatmentCommentDAO();
    }
   
    public void saveTreatmentComment(TreatmentCommentVO vo) {
        TreatmentCommentDAO assessmentDAO = new TreatmentCommentDAO();
        assessmentDAO.update(vo);
        try {
            if (vo != null) {
                vo.setBody(null);
                TreatmentCommentVO new_id = (TreatmentCommentVO) getTreatmentComment(vo);
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "treatment_comments", new_id));
                }
            }
        } catch (ApplicationException e) {
        }
    }
    public void saveAssessmentTreatment(AssessmentTreatmentVO vo) {
        TreatmentCommentDAO assessmentDAO = new TreatmentCommentDAO();
        assessmentDAO.updateAssessmentTreatment(vo);
        try {
            if (vo != null) {
                AssessmentTreatmentVO new_id = (AssessmentTreatmentVO) getAssessmentTreatment(vo);
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_treatment", new_id));
                }
            }
        } catch (ApplicationException e) {
        }
    }
    /**
     * Returns all NursingCarePlanVO objects that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  assess  the nursing-care_plan object for criteria
     * @param limit the # of records you want returned.
     * @return      the assessment_eachwound records
     * @see         TreatmentCommentVO
     */
    public Collection getAllTreatmentComments(TreatmentCommentVO assess, int limit) throws ApplicationException {
        try {
            TreatmentCommentDAO woundAssessmentDAO = new TreatmentCommentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    public Collection getAllAssessmentTreatment(AssessmentTreatmentVO assess) throws ApplicationException {
        try {
            TreatmentCommentDAO woundAssessmentDAO = new TreatmentCommentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    public void mergeTreatmentComments(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        com.pixalere.integration.dao.IntegrationDAO integrationDAO = new com.pixalere.integration.dao.IntegrationDAO();
        TreatmentCommentVO i = new TreatmentCommentVO();
        i.setPatient_id(patient_from);
        Collection<TreatmentCommentVO> many = getAllTreatmentComments(i, -1);
        for (TreatmentCommentVO a : many) {
            a.setPatient_id(patient_to);
            saveTreatmentComment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("treatment_comments");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    /**
     * Remove data from the given <code>object</code>
     * @param object to search for
     */
    public void removeTreatmentComment(TreatmentCommentVO object) {
        TreatmentCommentDAO assessmentDAO = new TreatmentCommentDAO();
        try {
            TreatmentCommentVO new_id = (TreatmentCommentVO) getTreatmentComment(object);
            assessmentDAO.delete(object);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(object.getProfessional_id(), object.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "treatment_comments", new_id));
            }
        } catch (ApplicationException e) {
        }
    }
    /**
     * Remove data from the given <code>object</code>
     * @param object to search for
     */
    public void removeAssessmentTreatment(AssessmentTreatmentVO object) {
        TreatmentCommentDAO assessmentDAO = new TreatmentCommentDAO();
        assessmentDAO.deleteAssessmentTreatment(object);
        try {
            if (object != null) {
                AssessmentTreatmentVO new_id = (AssessmentTreatmentVO) getAssessmentTreatment(object);
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(object.getProfessional_id(), object.getPatient_id(), com.pixalere.utils.Constants.DELETE_RECORD, "assessment_treatment", new_id));
                }
            }
        } catch (ApplicationException e) {
        }
    }
    /**
     * Retrieve ncp data for the given <code>assessment</code>
     * @param assessment to search for
     * @return TreatmentCommentVO
     */
    public TreatmentCommentVO getTreatmentComment(TreatmentCommentVO assessment) {
        TreatmentCommentDAO assessmentDAO = new TreatmentCommentDAO();
        try {
            return (TreatmentCommentVO) assessmentDAO.findByCriteria(assessment);
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.getNursingCarePlan(): " + e.getMessage());
            return null;
        }
    }
    /**
     * Retrieve ncp data for the given <code>assessment</code>
     * @param assessment to search for
     * @return TreatmentCommentVO
     */
    public AssessmentTreatmentVO getAssessmentTreatment(AssessmentTreatmentVO assessment) {
        TreatmentCommentDAO assessmentDAO = new TreatmentCommentDAO();
        try {
            return (AssessmentTreatmentVO) assessmentDAO.findByCriteria(assessment);
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.getNursingCarePlan(): " + e.getMessage());
            return null;
        }
    }
    public Collection getAllTreatmentArray(AssessmentTreatmentArraysVO assess) throws ApplicationException {
        try {
            TreatmentCommentDAO woundAssessmentDAO = new TreatmentCommentDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    public void saveTreatmentArray(AssessmentTreatmentArraysVO vo) throws ApplicationException {
        try {
            TreatmentCommentDAO assessmentDAO = new TreatmentCommentDAO();
            assessmentDAO.saveTreatmentArrays(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }
    public void removeTreatmentArray(AssessmentTreatmentArraysVO p) {
        TreatmentCommentDAO dao = new TreatmentCommentDAO();
        dao.deleteTreatmentArrays(p);
    }
}
