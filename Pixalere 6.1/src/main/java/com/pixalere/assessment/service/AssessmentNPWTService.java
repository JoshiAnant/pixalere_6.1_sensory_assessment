/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.assessment.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.assessment.bean.AssessmentNPWTVO;
import com.pixalere.assessment.bean.AssessmentNPWTAlphaVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface AssessmentNPWTService {
    public AssessmentNPWTVO getNegativePressure(@WebParam(name="crit") AssessmentNPWTVO crit) throws ApplicationException;
    public void saveNegativePressure(@WebParam(name="updateRecord") AssessmentNPWTVO updateRecord) throws ApplicationException;
    public Collection<AssessmentNPWTVO> getAllNegativePressure(@WebParam(name="records") AssessmentNPWTVO records, @WebParam(name="limit") int limit) throws ApplicationException;
    public AssessmentNPWTAlphaVO getNegativePressureAlpha(@WebParam(name="crit") AssessmentNPWTAlphaVO crit) throws ApplicationException;
    public void saveNegativePressureAlpha(@WebParam(name="updateRecord") AssessmentNPWTAlphaVO updateRecord) throws ApplicationException;
    public Collection<AssessmentNPWTAlphaVO> getAllNegativePressureAlphas(@WebParam(name="records") AssessmentNPWTAlphaVO records, @WebParam(name="limit") int limit) throws ApplicationException;
}
