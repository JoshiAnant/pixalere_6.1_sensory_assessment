package com.pixalere.assessment.service;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import com.pixalere.assessment.dao.*;
import com.pixalere.common.*;
import java.util.Vector;
import java.util.Collection;
import javax.jws.WebService;
import com.pixalere.integration.dao.IntegrationDAO;
import com.pixalere.integration.bean.EventVO;
@WebService(endpointInterface = "com.pixalere.assessment.service.NursingCarePlanService", serviceName = "NursingCarePlanService")
/**
 *     This is the nursing care plan service implementation
 *     Refer to {@link com.pixalere.assessment.service.NursingCarePlanService } to see if there are
 *     any web services available.
 *
 *     <img src="NursingCarePlanServiceImpl.png"/>
 *
 *
 *     @view  NursingCarePlanServiceImpl
 *
 *     @match class com.pixalere.assessment.*
 *     @opt hide
 *     @match class com.pixalere.assessment\.(dao.NursingCarePlanDAO|bean.NursingCarePlanVO|bean.DressingChangeFrequencyVO|service.NursingCarePlanService)
 *     @opt !hide
 *
 */
public class NursingCarePlanServiceImpl implements NursingCarePlanService {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NursingCarePlanServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    NursingCarePlanDAO dao = null;
    public NursingCarePlanServiceImpl() {
        dao = new NursingCarePlanDAO();
    }
    /**
     * Updates the ncp record to the nursing_care_plan table.
     *
     */
    public void saveNursingCarePlan(NursingCarePlanVO vo) {
        NursingCarePlanDAO assessmentDAO = new NursingCarePlanDAO();
        assessmentDAO.update(vo);
        if (vo != null) {
            vo.setBody(null);
            NursingCarePlanVO new_id = (NursingCarePlanVO) getNursingCarePlan(vo);
            try {
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "nursing_care_plan", new_id));
                }
            } catch (ApplicationException e) {
            }
        }
    }
    public void saveDressingChangeFrequency(DressingChangeFrequencyVO vo) {
        NursingCarePlanDAO assessmentDAO = new NursingCarePlanDAO();
        assessmentDAO.updateDcf(vo);
        if(vo!=null){
        try {
            DressingChangeFrequencyVO new_id = (DressingChangeFrequencyVO) getDressingChangeFrequency(vo);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "dressing_change_frequency", new_id));
            }
        } catch (ApplicationException e) {
        }
        }
    }
    public void mergeNursingCarePlans(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        NursingCarePlanVO i = new NursingCarePlanVO();
        com.pixalere.integration.dao.IntegrationDAO integrationDAO = new com.pixalere.integration.dao.IntegrationDAO();
        i.setPatient_id(patient_from);
        Collection<NursingCarePlanVO> many = getAllNursingCarePlans(i, -1);
        for (NursingCarePlanVO a : many) {
            a.setPatient_id(patient_to);
            saveNursingCarePlan(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("nursing_care_plan");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void mergeDressingChangeFrequencies(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        DressingChangeFrequencyVO i = new DressingChangeFrequencyVO();
        i.setPatient_id(patient_from);
        Collection<DressingChangeFrequencyVO> many = getAllDressingChangeFrequencies(i, -1);
        for (DressingChangeFrequencyVO a : many) {
            a.setPatient_id(patient_to);
            saveDressingChangeFrequency(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("dressing_change_frequency");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    /**
     * Returns all NursingCarePlanVO objects that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  vo  the nursing-care_plan object for criteria
     * @param limit the # of records you want returned.
     * @return      the assessment_eachwound records
     * @see         NursingCarePlanVO
     */
    public Collection getAllNursingCarePlans(NursingCarePlanVO assess, int limit) throws ApplicationException {
        try {
            NursingCarePlanDAO woundAssessmentDAO = new NursingCarePlanDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve ncps data for the given <code>assessment</code>
     * @param assessment to search for
     * @return NursingCarePlans
     */
    public Collection getNursingCarePlansWithinRange(NursingCarePlanVO assessment, int start_date, int end_date) throws ApplicationException {
        NursingCarePlanDAO assessmentDAO = new NursingCarePlanDAO();
        try {
            return assessmentDAO.findAllNCPByCriteriaWithinRange(assessment, start_date, end_date);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.getAssessment(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve dcf assessment data for the given <code>assessment</code>
     * @param assessment to search for
     * @return DressingChangeFrequencyVO
     */
    public Collection getDressingChangeFrequenciesWithinRange(DressingChangeFrequencyVO assessment, java.util.Date start_date, java.util.Date end_date) throws ApplicationException {
        NursingCarePlanDAO assessmentDAO = new NursingCarePlanDAO();
        try {
            return assessmentDAO.findAllDressingsByCriteriaWithinRange(assessment, start_date, end_date);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.getAssessment(): " + e.toString(), e);
        }
    }
    /**
     * Returns all NursingCarePlanVO objects that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  vo  the nursing-care_plan object for criteria
     * @param limit the # of records you want returned.
     * @return      the assessment_eachwound records
     * @see         AssessmentEachwoundVO
     */
    public Collection getAllDressingChangeFrequencies(DressingChangeFrequencyVO assess, int limit) throws ApplicationException {
        try {
            NursingCarePlanDAO woundAssessmentDAO = new NursingCarePlanDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve ncp data for the given <code>assessment</code>
     * @param assessment to search for
     * @return NursingCarePlanVO
     */
    public NursingCarePlanVO getNursingCarePlan(NursingCarePlanVO assessment) {
        NursingCarePlanDAO assessmentDAO = new NursingCarePlanDAO();
        try {
            return (NursingCarePlanVO) assessmentDAO.findByCriteria(assessment);
        } catch (DataAccessException e) {
            e.printStackTrace();
            log.error("AssessmentManagerBD.getNursingCarePlan(): " + e.getMessage());
            return null;
        }
    }
    /**
     * Retrieve ncp data for the given <code>assessment</code>
     * @param assessment to search for
     * @return NursingCarePlanVO
     */
    public DressingChangeFrequencyVO getDressingChangeFrequency(DressingChangeFrequencyVO assessment) {
        NursingCarePlanDAO assessmentDAO = new NursingCarePlanDAO();
        try {
            return (DressingChangeFrequencyVO) assessmentDAO.findByCriteria(assessment);
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.getNursingCarePlan(): " + e.getMessage());
            return null;
        }
    }
    /**
     * Remove data from the given <code>object</code>
     * @param criteria to search for
     */
    public void removeNursingCarePlan(NursingCarePlanVO object) {
        NursingCarePlanDAO assessmentDAO = new NursingCarePlanDAO();
        try {
            NursingCarePlanVO new_id = (NursingCarePlanVO) getNursingCarePlan(object);
            assessmentDAO.delete(object);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(object.getProfessional_id(), object.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "nursing_care_plan", new_id));
            }
        } catch (ApplicationException e) {
        }
    }
}
