/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.assessment.service;
import com.pixalere.assessment.bean.AssessmentNPWTVO;
import com.pixalere.assessment.bean.AssessmentNPWTAlphaVO;
import com.pixalere.assessment.dao.AssessmentNPWTDAO;
import com.pixalere.assessment.dao.NursingCarePlanDAO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import java.util.Collection;
import javax.jws.WebService;
@WebService(endpointInterface = "com.pixalere.assessment.service.AssessmentNPWTService", serviceName = "AssessmentNPWTService")
/**
 *     This is the Negative Pressure Wound Therapy service implementation
 *     Refer to {@link com.pixalere.assessment.service.AssessmentNPWTService } to see if there are
 *     any web services available.
 *
 *     <img src="AssessmentNPWTServiceImpl.png"/>
 *
 *
 *     @view  AssessmentNPWTServiceImpl
 *
 *     @match class com.pixalere.assessment.*
 *     @opt hide
 *     @match class com.pixalere.assessment\.(dao.AssessmentNPWTDAO|bean.AssessmentNPWTVO|bean.AssessmentNPWTAlphaVO)
 *     @opt !hide
 *
 */
public class AssessmentNPWTServiceImpl implements AssessmentNPWTService{
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentNPWTServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    AssessmentNPWTDAO dao = null;
    public AssessmentNPWTServiceImpl() {
        dao = new AssessmentNPWTDAO();
    }
    /**
     * Updates the negative pressure wound therapy record to the assessment_npwt table.
     *
     */
    public void saveNegativePressure(AssessmentNPWTVO vo) {
        AssessmentNPWTDAO assessmentDAO = new AssessmentNPWTDAO();
        assessmentDAO.update(vo);
        if (vo != null) {
            AssessmentNPWTVO new_id = (AssessmentNPWTVO) getNegativePressure(vo);
            try {
                if (new_id != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_npwt", new_id));
                }
            } catch (ApplicationException e) {
            }
        }
    }
     /**
     * Returns all AssessmentNPWTVO objects that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  vo  the negative pressure object for criteria
     * @param limit the # of records you want returned.
     * @return      the assessment_npwt records
     * @see         AssessmentNPWTVO
     */
    public Collection getAllNegativePressure(AssessmentNPWTVO assess, int limit) throws ApplicationException {
        try {
            AssessmentNPWTDAO woundAssessmentDAO = new AssessmentNPWTDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve negative pressure data for the given <code>assessment</code>
     * @param assessment to search for
     * @return AssessmentNPWTVO
     */
    public AssessmentNPWTVO getNegativePressure(AssessmentNPWTVO assessment) {
        AssessmentNPWTDAO assessmentDAO = new AssessmentNPWTDAO();
        try {
            return (AssessmentNPWTVO) assessmentDAO.findByCriteria(assessment);
        } catch (DataAccessException e) {
            e.printStackTrace();
            log.error("manager.getNegativePressure(): " + e.getMessage());
            return null;
        }
    }
     /* Retrieve negative pressure data for the given <code>assessment_id</code> and <code> alpha_id</code>
     * @param assessment_id to search for
     * @param alpha_id (joins assessment_npwt_alpha)
     * @return AssessmentNPWTVO
     */
    public AssessmentNPWTVO getNegativePressure(int assessment_id,int alpha_id) {
        AssessmentNPWTDAO assessmentDAO = new AssessmentNPWTDAO();
        try {
            return (AssessmentNPWTVO) assessmentDAO.findByCriteria(assessment_id,alpha_id);
        } catch (DataAccessException e) {
            e.printStackTrace();
            log.error("manager.getNegativePressure(): " + e.getMessage());
            return null;
        }
    }
     /**
     * Remove data from the given <code>object</code>
     * @param criteria to search for
     */
    public void removeNegativePressure(AssessmentNPWTVO object) {
        AssessmentNPWTDAO assessmentDAO = new AssessmentNPWTDAO();
        try {
            AssessmentNPWTVO new_id = (AssessmentNPWTVO) getNegativePressure(object);
            assessmentDAO.delete(object);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(object.getProfessional_id(), object.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "assessment_npwt", new_id));
            }
        } catch (ApplicationException e) {
        }
    }
    /**
     * Updates the negative pressure wound therapy record to the assessment_npwt_alpha table.
     *
     */
    public void saveNegativePressureAlpha(AssessmentNPWTAlphaVO vo) {
        AssessmentNPWTDAO assessmentDAO = new AssessmentNPWTDAO();
        assessmentDAO.updateNPWTAlpha(vo);
        
    }
     /**
     * Returns all AssessmentNPWTVO objects that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  vo  the negative pressure object for criteria
     * @param limit the # of records you want returned.
     * @return      the assessment_npwt records
     * @see         AssessmentNPWTVO
     */
    public Collection getAllNegativePressureAlphas(AssessmentNPWTAlphaVO assess, int limit) throws ApplicationException {
        try {
            AssessmentNPWTDAO woundAssessmentDAO = new AssessmentNPWTDAO();
            return woundAssessmentDAO.findAllNPWTAlphaByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in manager.getAllNegativePressureAlphas(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve negative pressure data for the given <code>assessment</code>
     * @param assessment to search for
     * @return NursingCarePlanVO
     */
    public AssessmentNPWTAlphaVO getNegativePressureAlpha(AssessmentNPWTAlphaVO assessment) {
        AssessmentNPWTDAO assessmentDAO = new AssessmentNPWTDAO();
        try {
            return (AssessmentNPWTAlphaVO) assessmentDAO.findNPWTAlphaByCriteria(assessment);
        } catch (DataAccessException e) {
            e.printStackTrace();
            log.error("manager.getNegativePressure(): " + e.getMessage());
            return null;
        }
    }
     /**
     * Remove data from the given <code>object</code>
     * @param criteria to search for
     */
    public void removeNegativePressureAlpha(AssessmentNPWTAlphaVO object) {
        AssessmentNPWTDAO assessmentDAO = new AssessmentNPWTDAO();
        
            AssessmentNPWTAlphaVO new_id = (AssessmentNPWTAlphaVO) getNegativePressureAlpha(object);
            assessmentDAO.delete(object);
           
    }
}
