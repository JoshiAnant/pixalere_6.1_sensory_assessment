package com.pixalere.assessment.service;
import com.pixalere.guibeans.Comments;
import com.pixalere.guibeans.AssessmentListVO;
import java.util.Date;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.dao.*;
import com.pixalere.common.*;
import com.pixalere.common.dao.*;
import java.util.Vector;
import com.pixalere.utils.*;
import java.util.Collection;
import java.util.Collections;
import javax.jws.WebService;
import com.pixalere.integration.bean.EventVO;
/**
 *     This is the assessment comments service implementation
 *     Refer to {@link com.pixalere.assessment.service.AssessmentCommentsService } to see if there are
 *     any web services available.
 *
 *     <img src="AssessmentCommentsServiceImpl.png"/>
 *
 *
 *     @view  AssessmentServicesServiceImpl
 *
 *     @match class com.pixalere.assessment.*
 *     @opt hide
 *     @match class com.pixalere.assessment\.(dao.AssessmentCommentsDAO|bean.AssessmentCommentsVO|service.AssessmentsCommentsService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.assessment.service.AssessmentCommentsService", serviceName = "AssessmentCommentsService")
public class AssessmentCommentsServiceImpl implements AssessmentCommentsService {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AssessmentCommentsServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    AssessmentCommentsDAO dao = null;
    public AssessmentCommentsServiceImpl() {
        
        dao = new AssessmentCommentsDAO();
    }
   
    
/**
     * Returns an AssessmentCommentsVO object that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  assessment_id  the id of the wound_assessment record
     * @return      the AssessmentCommentsVo record
     * @see         AssessmentCommentsVO
     */
    
    public AssessmentCommentsVO getComment(AssessmentCommentsVO c) throws ApplicationException {
        AssessmentCommentsVO woundVO = null;
        try {
            AssessmentCommentsDAO woundAssessmentDAO = new AssessmentCommentsDAO();
            woundVO = (AssessmentCommentsVO) woundAssessmentDAO.findByCriteria(c);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
        return woundVO;
    }
/**
   * Returns a collection of AssessmentComments objects to be shown in a report
   * <p>
   * This method always returns immediately, whether or not the record exists.   
   * The object will return NULL if the record does not exist.  
   * 
   * @param start_date the start date to use to filter comments
   * @param end_date the end date to use to filter comments
   * @param wound_profile_type_id the wound profile type we want the comments for
   * @return AssessmentComments records.
   * @see AssessmentCommentsVO
   */
  public Collection getAllComments(String start_date, String end_date, int wound_profile_type_id) throws ApplicationException{
      Collection<AssessmentCommentsVO> comments = null;
      try{
          AssessmentCommentsDAO cdao = new AssessmentCommentsDAO();
          comments = cdao.findAllByCriteriaWithinRange(start_date, end_date, wound_profile_type_id);
          
      }catch (DataAccessException e) {
          e.printStackTrace();
          throw new ApplicationException("DataAccessException Error in AssessmentCommentsServiceImpl.getAllComments(): " + e.toString(), e);
      }
      return comments;
  }
    /**
     * Returns AssessmentComments objects  that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @return      the AssessmentcommentsVO records
     * @see         AssessmentCommentsVO
     */
    public Vector getAllComments(Collection<WoundAssessmentVO> assess, AbstractAssessmentVO eachwoundVO, String professional_id, String start_date, String end_date,String locale)
            throws ApplicationException {
            WoundAssessmentServiceImpl waManager=new WoundAssessmentServiceImpl();
        Vector assessments = new Vector();
        try {
            WoundAssessmentVO wavo = new WoundAssessmentVO();
            AssessmentCommentsDAO cdao = new AssessmentCommentsDAO();
            AssessmentCommentsVO commentsVO = new AssessmentCommentsVO();
            ReferralsTrackingVO tvo = new ReferralsTrackingVO();
            ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
            wavo.setActive(new Integer(1));
            wavo.setPatient_id(eachwoundVO.getPatient_id());
            wavo.setWound_id(eachwoundVO.getWound_id());
            Vector<WoundAssessmentVO> a = waManager.filterAssessments(assess, start_date, end_date);
            for (WoundAssessmentVO assessVO : a) {
                AssessmentListVO al = new AssessmentListVO();
                ProfessionalVO userVO = userBD.getProfessional(assessVO.getProfessional_id().intValue());
                al.setAssessment_id(assessVO.getId());
                al.setTimestamp(assessVO.getCreated_on());
                al.setUser_signature(assessVO.getUser_signature());
                
                Vector comments = new Vector();
                commentsVO.setAssessment_id(assessVO.getId());
                commentsVO.setWound_profile_type_id(eachwoundVO.getWound_profile_type_id());
                
                Collection<AssessmentCommentsVO> commentss = cdao.findAllByCriteria(commentsVO);
                for (AssessmentCommentsVO comment : commentss) {
                    
                    PositionVO p = new PositionVO();
                    p.setId(comment.getPosition_id());
                    PositionVO position = userBD.getPosition(p);
                    
                    PDate pdate = new PDate(userVO!=null?userVO.getTimezone():Constants.TIMEZONE);
                    String woundStamp = pdate.getMonth(assessVO.getCreated_on(),locale) + "/" + pdate.getDay(assessVO.getCreated_on()) + "/" + pdate.getYear(assessVO.getCreated_on()) + " " + pdate.getHour(assessVO.getCreated_on()) + ":" + pdate.getMin(assessVO.getCreated_on());
                    comments.add(new Comments(comment.getBody(), comment.getUser_signature(), comment.getComment_type(), comment.getAssessment_id(), woundStamp, comment.getReferral(), (comment.getProfessional()!=null?comment.getProfessional().getFirstname() + " " + comment.getProfessional().getLastname():""), comment.getCreated_on(),comment.getProfessional_id(),position.getColour(),comment.getId(),comment.getDeleted(),comment.getDelete_reason(),comment.getDelete_signature()));// insert
                }
                
                //check if referral exists
                Collections.sort(comments);
                //IF NO COMMENTS OR REFERRALS EXIST, DON'T ADD.'
                if (commentss.size() == 0  ) {
                    //String woundStamp = pd.getMonth(assessVO.getCreated_on()) + "/" + pd.getDay(assessVO.getCreated_on()) + "/" + pd.getYear(assessVO.getCreated_on()) + " " + pd.getHour(assessVO.getCreated_on()) + ":" + pd.getMin(assessVO.getCreated_on());
                    //comments.add(new Comments("", "", "", assessVO.getId(), woundStamp, null, "", null,0,"",0,0,"",""));
                    //al.setComments(comments);
                    //assessments.add(al);
                } else if (commentss.size() > 0 ) {
                    al.setComments(comments);
                    assessments.add(al);
                }
            }
            //get all Messages
            AssessmentCommentsVO message = new AssessmentCommentsVO();
            message.setAssessment_id(-1);
            message.setWound_profile_type_id(eachwoundVO.getWound_profile_type_id());

            Collection<AssessmentCommentsVO> messages = cdao.findAllByCriteria(message);//,time_start,time_end);
            for (AssessmentCommentsVO msg : messages) {
                
                

                Vector v = new Vector();
                v.add(new Comments(msg.getBody(), msg.getUser_signature(), msg.getComment_type(), -1, "", msg.getReferral(), (msg.getProfessional()!=null?msg.getProfessional().getFirstname() + " " + msg.getProfessional().getLastname():""), msg.getCreated_on(),msg.getProfessional_id(),(msg.getPosition()!=null?msg.getPosition().getColour():"#FFF"),msg.getId(),msg.getDeleted(),msg.getDelete_reason(),msg.getDelete_signature()));
                AssessmentListVO al = new AssessmentListVO();
                
                al.setAssessment_id(-1);
                al.setTimestamp(msg.getCreated_on());
                al.setUser_signature(msg.getUser_signature());
                al.setComments(v);
                assessments.add(al);
            }
            
            
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentEachwoundMAangerBD.getAllComments(): " + e.toString(), e);
        }
        Collections.sort(assessments,new AssessmentListVO());
        return assessments;
    }
    /**
     * Get All Assessment Commments by Critieria
     * 
     * @param AssessmentCommentsVO object within query criteria
     * @return Collection of objects
     * @throws com.pixalere.common.ApplicationException
     */
     public Collection<AssessmentCommentsVO> getAllComments(AssessmentCommentsVO crit) throws ApplicationException {
         try {
            AssessmentCommentsDAO dao = new AssessmentCommentsDAO();
            Collection<AssessmentCommentsVO> c = dao.findAllByCriteria(crit);
            return c;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
     }
    public void mergeComments(int patient_from,int patient_to,EventVO event) throws ApplicationException {
        AssessmentCommentsVO i = new AssessmentCommentsVO();
        com.pixalere.integration.dao.IntegrationDAO integrationDAO = new  com.pixalere.integration.dao.IntegrationDAO();
        i.setPatient_id(patient_from);
        Collection<AssessmentCommentsVO> many = getAllComments(i);
        for(AssessmentCommentsVO a : many){
            a.setPatient_id(patient_to);
            saveComment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_comments");
            event.setRow_id(a.getId());
            try{
                integrationDAO.insert(event);
            }catch(DataAccessException ex){
                ex.printStackTrace();
            }
        }
    }
     public int getCommentsCount(WoundAssessmentVO v) throws ApplicationException {
        int count = 0;
        try {
            AssessmentCommentsDAO dao = new AssessmentCommentsDAO();
            count = dao.getCommentsCount(v);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in ProfessionalServiceImpl.validateUserId(): " + e.toString(), e);
        }
        return count;
    }
     /**
     * Updates the assessment comments record to the assessment_comments table.
     *
     * @param comments the assessment comments record
     * @see AssessmentCommentsVO
     */
    public void saveComment(AssessmentCommentsVO comments) {
        AssessmentCommentsDAO commentsDAO = new AssessmentCommentsDAO();
        commentsDAO.update(comments);
        try{
            comments.setBody(null);
        AssessmentCommentsVO newComm = getComment(comments);
        if(newComm == null){
            //wasn'tt saved
            System.out.println("Comment wasn't saved: "+comments.getAssessment_id()+" "+comments.getWound_profile_type_id());
        }
        logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(comments.getProfessional_id(), comments.getPatient_id(),(comments.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "assessment_comments", newComm));
        }catch(Exception e){}
    }
    /**
     * Remove data from the given <code>object</code>
     * @param criteria to search for
     */
    public void removeComment(AssessmentCommentsVO object)  {
        AssessmentCommentsDAO assessmentDAO = new AssessmentCommentsDAO();
        assessmentDAO.delete(object);
        
    }
}