package com.pixalere.assessment.service;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.assessment.dao.*;
import com.pixalere.common.*;
import java.util.Vector;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import javax.jws.WebService;
import com.pixalere.integration.bean.EventVO;
/**
 *     This is the wound assessment location service implementation
 *     Refer to {@link com.pixalere.assessment.service.WoundAssessmentLocationService } to see if there are
 *     any web services available.
 *
 *     <img src="WoundAssessmentLocationServiceImpl.png"/>
 *
 *
 *     @view  WoundAssessmentLocationServiceImpl
 *
 *     @match class com.pixalere.assessment.*
 *     @opt hide
 *     @match class com.pixalere.assessment\.(dao.WoundAssessmentLocationDAO|bean.WoundAssessmentLocationVO|bean.WoundLocationDetailsVO|service.WoundAssessmentLocationService)
 *     @opt !hide
 *
 */
@WebService(endpointInterface = "com.pixalere.assessment.service.WoundAssessmentLocationService", serviceName = "WoundAssessmentLocationService")
public class WoundAssessmentLocationServiceImpl implements WoundAssessmentLocationService {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundAssessmentLocationServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    WoundAssessmentLocationDAO dao = null;
    public WoundAssessmentLocationServiceImpl() {
        dao = new WoundAssessmentLocationDAO();
    }
    public boolean isWoundHealed(int wound_id, int professional_id) throws ApplicationException {
        WoundAssessmentLocationDAO wDAO = new WoundAssessmentLocationDAO();
        try {
            boolean result = wDAO.isWoundProfileHealed(wound_id, professional_id);
            return result;
        } catch (DataAccessException e) {
            throw new ApplicationException("Exception thrown getting healedBool" + e.getMessage());
        }
    }
    /**
     * Drops inactive/temporary alphas
     *
     * @param patient_id the patient_id of the alpha record
     * @param professional_id the professional of the alpha record
     * @see WoundAssessmentLocationVO
     */
    public void removeAlpha(WoundAssessmentLocationVO vo, Date purge_days) {
        try {
            Vector<WoundAssessmentLocationVO> alphas = getAllAlphas(vo);
            WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
            
            
            locationDAO.deleteTMP(vo, purge_days);
            for (WoundAssessmentLocationVO new_id : alphas) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "wound_assessment_location", new_id));
            }
        } catch (ApplicationException e) {
        }
    }
    /**
     * Retrieves aWoundAssessmentLocation records for the given <code>alpha_id</code>
     * @param alpha_id to search for
     * @return WoundAssessmentLocationVO instance
     */
    public WoundAssessmentLocationVO getAlpha(WoundAssessmentLocationVO tmp, boolean order, boolean hideIncisions, String wound_profile_type) {
        WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
        try {
            return (WoundAssessmentLocationVO) locationDAO.findByCriteria(tmp, order, hideIncisions, wound_profile_type);
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.retrieveLocationsForWounds(): " + e.getMessage());
            return null;
        }
    }
    public WoundAssessmentLocationVO getAlpha(WoundAssessmentLocationVO tmp) {
        WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
        try {
            return (WoundAssessmentLocationVO) locationDAO.findByCriteria(tmp);
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.retrieveLocationsForWounds(): " + e.getMessage());
            return null;
        }
    }
    public Collection getAllAlphaDetails(WoundLocationDetailsVO tmp) {
        WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
        try {
            return locationDAO.findAllDetailsByCriteria(tmp);
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.retrieveLocationsForWounds(): " + e.getMessage());
            return null;
        }
    }
    public WoundLocationDetailsVO getAlphaDetail(WoundLocationDetailsVO tmp) {
        WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
        try {
            return (WoundLocationDetailsVO) locationDAO.findDetailedByCriteria(tmp);
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.retrieveLocationsForWounds(): " + e.getMessage());
            return null;
        }
    }
   
    public void mergeAlphas(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        com.pixalere.integration.dao.IntegrationDAO integrationDAO = new com.pixalere.integration.dao.IntegrationDAO();
        WoundAssessmentLocationVO i = new WoundAssessmentLocationVO();
        i.setPatient_id(patient_from);
        Collection<WoundAssessmentLocationVO> many = getAllAlphas(i);
        for (WoundAssessmentLocationVO a : many) {
            a.setPatient_id(patient_to);
            saveAlpha(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("wound_assessment_location");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public Vector<WoundAssessmentLocationVO> getAllAlphas(WoundAssessmentLocationVO tmp) throws ApplicationException {
        try {
            WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
            int prof = 0;
            if (tmp.getProfessional_id() != null) {
                prof = tmp.getProfessional_id().intValue();
            }
            Vector<WoundAssessmentLocationVO> c = dao.findAllByCriteria(tmp, prof, true, "");//4th parameter (extra sort) is ignored
            return c;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAssessmentEachwound(): " + e.toString(), e);
        }
    }
    public Vector<WoundAssessmentLocationVO> getAllAlphas(WoundAssessmentLocationVO tmp, boolean order) throws ApplicationException {
        try {
            WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
            int prof = 0;
            if (tmp.getProfessional_id() != null) {
                prof = tmp.getProfessional_id().intValue();
            }
            Vector<WoundAssessmentLocationVO> c = dao.findAllByCriteria(tmp, prof, order, "");//4th parameter (extra sort) is ignored
            return c;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAssessmentEachwound(): " + e.toString(), e);
        }
    }
    public void removeAlpha(int alpha_id, int professional_id) throws ApplicationException {
        try {
            WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
            
            WoundAssessmentLocationVO a = new WoundAssessmentLocationVO();
            a.setId(new Integer(alpha_id));
            a.setProfessional_id(new Integer(professional_id));
            WoundAssessmentLocationVO new_id = getAlpha(a);
            dao.delete(a);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(a.getProfessional_id(), a.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "wound_assessment_location", new_id));
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAssessmentEachwound(): " + e.toString(), e);
        }
    }
    /**
     * Computes a list of all alphas which don't have assessments for the given assessment id.
     *
     * @param wound_id id of the current wound
     * @param assessment_id id of the current assessment
     * @return a list of strings representing all unfinished alphas. Each list element will be the single
     * letter representing that alpha ("A", "B", "E", "F",...)
     * @throws ApplicationException Thrown if a problem occurs
     */
    public List<String> computeUncompletedAlphas(AbstractAssessmentVO assess, int professional_id) throws ApplicationException {
        ArrayList<String> unfinishedAlphas = null;
        //default to English, method doesn't require translation
        AssessmentServiceImpl manager = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        //get all inactive wound_assessment_location records for this wound_id
        Vector<WoundAssessmentLocationVO> locations = retrieveLocationsForWound(professional_id, assess.getWound_id().intValue(), false, false);
        if (locations != null && locations.size() != 0) {
            Collection<AbstractAssessmentVO> assessments = manager.getAllAssessments(assess.getAssessment_id().intValue());
            unfinishedAlphas = new ArrayList<String>(locations.size());
            //for each wound_assessment_location record, make sure there's a corresponding eachwound record.
            for (WoundAssessmentLocationVO locVO : locations) {
                if (locVO.getAlpha().indexOf("incs") == -1) {
                    int alpha_id = locVO.getId();
                    boolean found = false;
                    for (AbstractAssessmentVO vo : assessments) {
                        if (vo.getAlpha_id() == alpha_id) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        unfinishedAlphas.add(locVO.getAlpha());
                    }
                }
            }
        }
        return unfinishedAlphas;
    }
    /**
     * Retrieves all WoundAssessmentLocation records for the given <code>wound_id</code> and <code>active</code>
     * value.
     * @param wound_id wound_id to search for
     * @param active if active is true, only get active records, if its false, get all records
     * @param healed if healed is false, get only active records (non healed), its true, get all.
     * @return Vector of WoundAssessmentLocationVO instances
     */
    public Vector<WoundAssessmentLocationVO> retrieveLocationsForWound(int professional_id, int wound_id, boolean active, boolean healed) {
        WoundAssessmentLocationDAO locationDAO = new WoundAssessmentLocationDAO();
        WoundAssessmentLocationVO vo = new WoundAssessmentLocationVO();
        vo.setWound_id(wound_id);
        if (active == true) {
            vo.setActive(1);
        }
        if (healed == false) {
            vo.setDischarge(0);
        }
        try {
            Vector s = locationDAO.findAllByCriteria(vo, professional_id, false, "");//4th parameter (extra sort) is ignored
            return s;
        } catch (DataAccessException e) {
            log.error("AssessmentManagerBD.retrieveLocationsForWounds(): " + e.getMessage());
            return new Vector();
        }
    }
    public void saveAlphaDetail(WoundLocationDetailsVO location) {
        try {
            WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
            dao.insertDetailed(location);
        } catch (DataAccessException e) {
            // throw new ApplicationException("DataAccessException Error in
            // AssessmentManagerBD.retrieveLastWoundAssessment(): " +
            // e.toString(),e);
        }
    }
    public void saveAlpha(WoundAssessmentLocationVO location) {
        try {
            WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
            dao.insert(location);
            WoundAssessmentLocationVO new_id = getAlpha(location);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(location.getProfessional_id(), location.getPatient_id(), (location.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wound_assessment_location", new_id));
            }
        } catch (DataAccessException e) {
            // throw new ApplicationException("DataAccessException Error in
            // AssessmentManagerBD.retrieveLastWoundAssessment(): " +
            // e.toString(),e);
        }catch (ApplicationException e) {
            // throw new ApplicationException("DataAccessException Error in
            // AssessmentManagerBD.retrieveLastWoundAssessment(): " +
            // e.toString(),e);
        }
    }
    public void updateAlpha(WoundAssessmentLocationVO location) {
        try {
            WoundAssessmentLocationDAO dao = new WoundAssessmentLocationDAO();
            dao.update(location);
            WoundAssessmentLocationVO new_id = getAlpha(location);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(location.getProfessional_id(), location.getPatient_id(), (location.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wound_assessment_location", new_id));
            }
        } catch (DataAccessException e) {
            // throw new ApplicationException("DataAccessException Error in
            // AssessmentManagerBD.retrieveLastWoundAssessment(): " +
            // e.toString(),e);
        }catch (ApplicationException e) {
            // throw new ApplicationException("DataAccessException Error in
            // AssessmentManagerBD.retrieveLastWoundAssessment(): " +
            // e.toString(),e);
        }
    }
    public void reopenAlpha(int intAlpha) {
        {
            WoundAssessmentLocationVO searchAlpha = new WoundAssessmentLocationVO();
            searchAlpha.setId(new Integer(intAlpha));
            WoundAssessmentLocationVO alpha = (WoundAssessmentLocationVO) getAlpha(searchAlpha);
            if (alpha.getDischarge() == 1) {
                alpha.setClose_timestamp(null);
                alpha.setDischarge(0);
                updateAlpha(alpha);
                //default to English, method doesn't require translation
                WoundServiceImpl wbd = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
                wbd.reopenWoundType(alpha.getWound_profile_type().getId());
            }
        }
    }
}
