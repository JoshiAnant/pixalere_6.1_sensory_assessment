package com.pixalere.assessment.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface WoundAssessmentLocationService {
    public WoundAssessmentLocationVO getAlpha(@WebParam(name="crit") WoundAssessmentLocationVO crit) throws ApplicationException;
    public void saveAlpha(@WebParam(name="updateRecord") WoundAssessmentLocationVO updateRecord) throws ApplicationException;
    public Collection<WoundAssessmentLocationVO> getAllAlphas(@WebParam(name="records") WoundAssessmentLocationVO records) throws ApplicationException;
    public void saveAlphaDetail(@WebParam(name="updateRecord") WoundLocationDetailsVO updateRecord) throws ApplicationException;
    
}