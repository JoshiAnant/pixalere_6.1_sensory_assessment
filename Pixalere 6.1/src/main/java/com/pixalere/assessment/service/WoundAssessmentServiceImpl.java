package com.pixalere.assessment.service;
/**
 *     This is the wound assessment service implementation
 *     Refer to {@link com.pixalere.assessment.service.WoundAssessmentService } to see if there are
 *     any web services available.
 *
 *     <img src="WoundAssessmentServiceImpl.png"/>
 *
 *
 *     @view  WoundAssessmentServiceImpl
 *
 *     @match class com.pixalere.assessment.*
 *     @opt hide
 *     @match class com.pixalere.assessment\.(dao.WoundAssessmentDAO|bean.WoundAssessmentVO|service.WoundAssessmentService)
 *     @opt !hide
 *
 */
import com.pixalere.guibeans.PatientVisit;
import java.util.Date;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.dao.*;
import com.pixalere.common.*;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.bean.*;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.utils.Constants;
import java.util.Vector;
import java.util.Collection;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.integration.bean.EventVO;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class WoundAssessmentServiceImpl implements WoundAssessmentService {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WoundAssessmentServiceImpl.class);
    
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    WoundAssessmentDAO dao = null;
    
    public WoundAssessmentServiceImpl() {
        dao = new WoundAssessmentDAO();
    }
    /**
     * Checks if the wound_assessment record exists
     *
     * @param profile         the wound_assessment criteria
     * @param professional_id the professional criteria
     * @see WoundAssessmentVO
     */
    public boolean checkAssessmentExistForProfId(int assessment_id, int professional_id) throws ApplicationException {
        boolean exists = false;
        try {
            WoundAssessmentDAO wDAO = new WoundAssessmentDAO();
            WoundAssessmentVO condition = new WoundAssessmentVO();
            condition.setId(assessment_id);
            condition.setActive(1);
            WoundAssessmentVO wProfile = (WoundAssessmentVO) wDAO.findByCriteria(condition);
            if (wProfile != null && wProfile.getProfessional_id().intValue() == professional_id) {
                exists = true;
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("Error in AssessmentManager.checkAssessmentExistForProfId: " + e.getMessage(), e);
        }
        return exists;
    }
/**
   * Returns a next WoundAssessmentVO object that can be shown on the screen.
   * <p/>
   * This method always returns immediately, whether or not the
   * record exists.  The Object will return NULL if the record does not
   * exist.
   *
   * @param woundAssessmentId the id of the wound_assessment record
   * @return the wound_assessment record
   * @see WoundAssessmentVO
   */
  public WoundAssessmentVO getNextAssessment(int id) throws ApplicationException {
      try {
          WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
          return (WoundAssessmentVO) woundAssessmentDAO.findNextByCriteria(id);
      } catch (DataAccessException e) {
          throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
      }
  }
/**
   * Returns a prev WoundAssessmentVO object that can be shown on the screen.
   * <p/>
   * This method always returns immediately, whether or not the
   * record exists.  The Object will return NULL if the record does not
   * exist.
   *
   * @param woundAssessmentId the id of the wound_assessment record
   * @return the wound_assessment record
   * @see WoundAssessmentVO
   */
  public WoundAssessmentVO getPrevAssessment(int id) throws ApplicationException {
      try {
          WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
          return (WoundAssessmentVO) woundAssessmentDAO.findPrevByCriteria(id);
      } catch (DataAccessException e) {
          throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
      }
  }
    /**
     * Updates the wound_assessment to the wound_assessment table.
     *
     * @param woundAssessmentVO the wound_assessment record
     * @see WoundAssessmentVO
     */
    public void saveAssessment(WoundAssessmentVO woundAssessmentVO) {
        WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
        woundAssessmentDAO.update(woundAssessmentVO);
        try {
            WoundAssessmentVO new_id = getAssessment(woundAssessmentVO);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(woundAssessmentVO.getProfessional_id(), woundAssessmentVO.getPatient_id(), (woundAssessmentVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "wound_assessment", new_id));
            }
        } catch (ApplicationException e) {
            // throw new ApplicationException("DataAccessException Error in
            // AssessmentManagerBD.retrieveLastWoundAssessment(): " +
            // e.toString(),e);
        }
    }
    public void removeAssessmentOffline(int patient_id, int patient_account_id) throws ApplicationException {
        /*AssessmentServiceImpl ass = new AssessmentServiceImpl(database);
        AssessmentImagesServiceImpl images = new AssessmentImagesServiceImpl(database);
        AssessmentCommentsServiceImpl comments = new AssessmentCommentsServiceImpl(database);
        AssessmentRecommendationsServiceImpl recommendations = new AssessmentRecommendationsServiceImpl(database);*/
        WoundAssessmentDAO dao = new WoundAssessmentDAO();
        /*NursingCarePlanServiceImpl ncp = new NursingCarePlanServiceImpl(database);
        TreatmentCommentsServiceImpl tc = new TreatmentCommentsServiceImpl(database);*/
        //Collection<WoundAssessmentVO> a = dao.findAllByCriteria(assessment);
        //for (WoundAssessmentVO assess : a) {
        dao.deleteOffline(patient_id, patient_account_id);
        //}
    }
    public void removeAssessment(WoundAssessmentVO assessment, Date purge_days) throws ApplicationException {
        //default to English, method doesn't require translation
        AssessmentServiceImpl ass = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        AssessmentImagesServiceImpl images = new AssessmentImagesServiceImpl();
        AssessmentCommentsServiceImpl comments = new AssessmentCommentsServiceImpl();
        WoundAssessmentDAO dao = new WoundAssessmentDAO();
        NursingCarePlanServiceImpl ncp = new NursingCarePlanServiceImpl();
        TreatmentCommentsServiceImpl tc = new TreatmentCommentsServiceImpl();
        try {
            Collection<WoundAssessmentVO> a = dao.findAllByCriteria(assessment);
            for (WoundAssessmentVO assess : a) {
                dao.delete(assess, purge_days);
                Date last_update = assess.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (purge_days == null || last_update.before(purge_days)) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(assess.getProfessional_id(), assess.getPatient_id(), com.pixalere.utils.Constants.DELETE_RECORD, "wound_assessment", assess));
                }
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("Error in AssessmentManager.checkAssessmentExistForProfId: " + e.getMessage(), e);
        } catch (ApplicationException e) {
            // throw new ApplicationException("DataAccessException Error in
            // AssessmentManagerBD.retrieveLastWoundAssessment(): " +
            // e.toString(),e);
        }
    }
    /**
     * Returns an WoundAssessmentVO object that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param woundAssessmentId the id of the wound_assessment record
     * @return the wound_assessment record
     * @see WoundAssessmentVO
     */
    public WoundAssessmentVO getAssessment(WoundAssessmentVO vo) throws ApplicationException {
        try {
            WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
            return (WoundAssessmentVO) woundAssessmentDAO.findByCriteria(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    public Vector<WoundAssessmentVO> filterAssessments(Collection<WoundAssessmentVO> assess, String start_id, String end_id)
            throws ApplicationException {
        Vector assessments = new Vector();
        try {
            for (WoundAssessmentVO a : assess) {
                if ((start_id  == null || start_id.equals("")) || (end_id  == null || end_id.equals(""))) {
                    assessments.add(a);
                } else if (a.getId() >= (Integer.parseInt(start_id)) && a.getId()<= (Integer.parseInt(end_id))) {
                    assessments.add(a);
                }
            }
            return assessments;
        } catch (NumberFormatException e) {
            throw new ApplicationException("NumberFormatException Error in AssessmentManagerBD.filterwoundassessmentsByDaterange" + e.getMessage(), e);
        }
    }
    /**
     * Returns all WoundAssessmentVO objects that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param vo the wound_assessment object for criteria
     * @return the wound_assessment records
     * @see WoundAssessmentVO
     */
    public Collection getAllAssessments(WoundAssessmentVO condition) throws ApplicationException {
        try {
            WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
            return woundAssessmentDAO.findAllByCriteria(condition);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAllWoundAssessments(): " + e.toString(), e);
        }
    }
    public void mergeAssessments(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        com.pixalere.integration.dao.IntegrationDAO integrationDAO = new com.pixalere.integration.dao.IntegrationDAO();
        WoundAssessmentVO i = new WoundAssessmentVO();
        i.setPatient_id(patient_from);
        Collection<WoundAssessmentVO> many = getAllAssessments(i);
        for (WoundAssessmentVO a : many) {
            a.setPatient_id(patient_to);
            saveAssessment(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("wound_assessment");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    /**
     * Returns all WoundAssessmentVO objects that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param vo the wound_assessment object for criteria
     * @return the wound_assessment records
     * @see WoundAssessmentVO
     */
    public Collection getAllAssessments(WoundAssessmentVO condition, String start_id, String end_id) throws ApplicationException {
        try {
            WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
            return woundAssessmentDAO.findAllByCriteriaByRange(condition, start_id, end_id);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAllWoundAssessments(): " + e.toString(), e);
        }
    }
    /**
     * Returns all WoundAssessmentVO objects that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param vo the wound_assessment object for criteria
     * @return the wound_assessment records
     * @see WoundAssessmentVO
     */
    public Collection getAllAssessmentsByDateRange(WoundAssessmentVO condition, Date start_date, Date end_date) throws ApplicationException {
        try {
            WoundAssessmentDAO woundAssessmentDAO = new WoundAssessmentDAO();
            return woundAssessmentDAO.findAllByCriteriaByDateRange(condition, start_date, end_date);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveAllWoundAssessments(): " + e.toString(), e);
        }
    }
    
    /**
     * Returns the auto heal rate reason if triggered.
     * <p/>
     * This record will return an empty string if nothing is triggered.
     *
     * @param lngTimestampNow the timestamp which is either current, or when assessment was saved (in offline's case)
     * @param assessment_id (if from online = -1)
     * @return the referral reason
     */
    public String checkHealRateAutoReferral(Date lngTimestampNow, Collection<GoalsVO> woundGoals, String wound_id, ProfessionalVO userVO, int assessment_id,Integer language) throws ApplicationException {
        String strAutoReferral = "0";
        String strAutoReferralReason = "";
        //default to English, method doesn't require translation
        AssessmentServiceImpl eachBD = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        ReferralsTrackingServiceImpl referrals = new ReferralsTrackingServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
        WoundServiceImpl woundBD = new WoundServiceImpl(language);
        ListServiceImpl listmanager = new ListServiceImpl(language);
        
        String locale = Common.getLanguageLocale(language);
        
        int intHealDays = new Integer((String) Common.getConfig("autoReferralsHealDays")).intValue();
        PDate pdate = new PDate();
        Date lngTimestampHealCheck = PDate.subtractDays(new Date(),intHealDays);
        Date refTimestamp = referrals.getLatestReferralTimestamp(new Integer(wound_id));
        //If no ref is found, use start date of wound profile.. count 21 days forward.
        if (refTimestamp == null) {
            WoundVO wpstart = woundBD.getWound(new Integer(wound_id));
            if (wpstart != null) {
                refTimestamp = wpstart.getStart_date();
            }
        }
        int intPrevDays = new Integer((String) Common.getConfig("autoReferralsPrevRefDays")).intValue();
        Date backDated = PDate.subtractDays(lngTimestampNow ,(intPrevDays));
        try{
        if (refTimestamp.before(backDated)) {
            // Find ID of the unsaved wound assessment
            WoundAssessmentVO wavTMP = new WoundAssessmentVO();
            wavTMP.setWound_id(new Integer(wound_id + ""));
            wavTMP.setActive(0);
            wavTMP.setProfessional_id(userVO.getId());
            if (assessment_id != -1) {
                wavTMP = new WoundAssessmentVO();
                wavTMP.setId(assessment_id);
            }
            WoundAssessmentVO woundAssessmentVO = wamanager.getAssessment(wavTMP);
            //Find alpha I                 D's for the unsaved wound assessments
            if (woundAssessmentVO != null) {
                Collection<AbstractAssessmentVO> assessments = eachBD.getAllAssessments(woundAssessmentVO.getId());
                //Loop for all unsaved wound care alphas
                for (AbstractAssessmentVO unsavedassessment : assessments) {
                    
                    LookupVO g = listmanager.getListItem(new Integer((String) Common.getConfig("woundGoalHealable")).intValue());
                    //Ensure wound is healable..
                    boolean healed = false;
                    for (GoalsVO goal : woundGoals) {
                        if (unsavedassessment.getAlpha_id().equals(goal.getAlpha_id()) && g.getId().equals(goal.getLookup_id())) {
                            
                            healed = true;
                        }
                    }
                    //Alpha Wound types allowed.
                    if (unsavedassessment.getAlphaLocation().getWound_profile_type().getAlpha_type().equals("A") && healed == true && unsavedassessment.getStatus().equals(new Integer((String) Common.getConfig("woundActive")).intValue())) {
                        AssessmentEachwoundVO assess = new AssessmentEachwoundVO();
                        assess.setAlpha_id(unsavedassessment.getAlpha_id());
                        assess.setWound_id(new Integer(wound_id));
                        assess.setFull_assessment(1);
                        double intCurrentHealRate = -1;
                        double intOldHealRate = -1;
                        double intLength = 0;
                        double intWidth = 0;
                        double intDepth = 0;
                        double intLength_old = 0;
                        double intWidth_old = 0;
                        double intDepth_old = 0;
                        String strOldHealRateDate = "";
                        AbstractAssessmentVO results = eachBD.getAssessmentWithinRange(assess, null, lngTimestampHealCheck);
                        AssessmentEachwoundVO checkVO = (AssessmentEachwoundVO) results;
                        if (checkVO != null) {
                            strOldHealRateDate = pdate.getDateString(checkVO.getCreated_on(),locale);
                            if ((checkVO.getWidth() > -1) && checkVO.getLength() > -1) {

                                // Get dimensions, multiply centimeters by 10 to get it into an integer
                                intLength_old = checkVO.getLength();
                                intWidth_old =  checkVO.getWidth();
                                //intDepth_old = (checkVO.getDepth_cm().equals(new Integer(-1)) ? new Integer(0) : checkVO.getDepth_cm()) * 10 + (checkVO.getDepth_mm().equals(new Integer(-1)) ? new Integer(0) : checkVO.getDepth_mm());
                                if (intLength_old == 0) {
                                    intLength_old = 1;
                                }
                                if (intWidth_old == 0) {
                                    intWidth_old = 1;
                                }
                                if (intDepth_old != 0) {
                                    intOldHealRate = intLength_old * intWidth_old;// * intDepth_old;
                                } else {
                                    intOldHealRate = intLength_old * intWidth_old;// * 1;
                                }
                            }
                            AssessmentEachwoundVO unsaved = (AssessmentEachwoundVO) unsavedassessment;
                            if (unsaved.getWidth() > -1 && unsaved.getLength() > -1) {

                                // Get dimensions, multiply centimeters by 10 to get it into an integer
                                intLength = (unsaved.getLength());
                                intWidth = (unsaved.getWidth());
                                //intDepth = (unsaved.getDepth_cm().equals(new Integer(-1)) ? new Integer(0) : unsaved.getDepth_cm()) * 10 + (unsaved.getDepth_mm().equals(new Integer(-1)) ? new Integer(0) : unsaved.getDepth_mm());
                                //@todo, we should use depth (Volume) unless 0 or less, then do area.
                                if (intLength == 0) {
                                    intLength = 1;
                                }
                                if (intWidth == 0) {
                                    intWidth = 1;
                                }
                                if (intDepth != 0) {
                                    intCurrentHealRate = intLength * intWidth;// * intDepth;
                                } else {
                                    intCurrentHealRate = intLength * intWidth;// * 1;
                                }
                            }
                        }
                        if (Common.getConfig("makeAutoReferralsHealrate").equals("1") && intOldHealRate != -1 && intCurrentHealRate != -1) {
                            double divisible = (intCurrentHealRate / intOldHealRate) * 100;
                            double changed = (intOldHealRate - intCurrentHealRate) / intCurrentHealRate;
                            //((y2 - y1) / y1)*100 = your percentage change.
                            double intHealRatePerc = changed*100;
                            if (intHealRatePerc < Integer.parseInt(Common.getConfig("autoReferralsHealrate"))) {
                                String refTMP = Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare",locale) + " " + unsavedassessment.getAlphaLocation().getAlpha() + "" + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_healrate_since",locale) + " " + Common.Round(intHealRatePerc, 2) + "% " + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_healrate_is",locale) + " " + strOldHealRateDate;
                                strAutoReferral = "1";
                                strAutoReferralReason = strAutoReferralReason + "; " + refTMP;
                                //Add Referral (inactive one).
                            ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                            referralVO.setProfessional_id(userVO.getId());
                            referralVO.setAssessment_id(unsavedassessment.getAssessment_id());
                            referralVO.setWound_id(new Integer((String) wound_id));
                            referralVO.setCurrent(new Integer("0"));
                            referralVO.setEntry_type(0); // "Referral"
                            referralVO.setActive(0);
                            referralVO.setWound_profile_type_id(unsavedassessment.getWound_profile_type_id());
                            //check if an already existing inactive is in the database.
                            ReferralsTrackingVO exists = referrals.getReferralsTracking(referralVO);
                            if(exists !=null){
                                referralVO.setId(exists.getId());
                            }
                            ReferralsTrackingVO referral_id = null;
                            referralVO.setCreated_on(new Date());
                            referralVO.setEntry_type(0);
                            //referralVO.setTop_priority(summaryForm.getPriority_wound());
                            //referralVO.setAuto_referral(summaryForm.getAuto_referral_type());
                            referrals.saveReferralsTracking(referralVO, unsavedassessment.getPatient_id());
                            referral_id = referrals.getReferralsTracking(referralVO);
                            //Add auto referrals records by alpha_id
                            AutoReferralVO autoRef = new AutoReferralVO();
                            autoRef.setReferral_id(referral_id.getId());
                            autoRef.setAuto_referral_type(Constants.AUTO_REFERRAL_HEALRATE);
                            autoRef.setAlpha_id(unsavedassessment.getAlpha_id());
                            //check if there's already one there.
                            AutoReferralVO auto_exists = referrals.getAutoReferral(autoRef);
                            if(auto_exists == null){
                                referrals.saveAutoReferral(autoRef);
                            }
                            }
                        }
                    }
                }
            }
        }
        }catch(Exception e){
           // e.printStackTrace();
        }
        return strAutoReferralReason;
    }
    /**
     * Returns the dressing change frequency auto referral  reason if triggered.
     * <p/>
     * This record will return an empty string if nothing is triggered.
     *
     * @param tmpWP  the temporary (inactive) wound profile
     * @param liveWP the previously committed wound profile.
     * @param assessment_id if in online (summary) set to -1
     * @return the referral reason
     */
    public String checkDcfAutoReferral(WoundProfileVO tmpWP, WoundProfileVO liveWP, String wound_id, Collection<GoalsVO> woundGoals, ProfessionalVO userVO, int assessment_id,int language) throws Exception {
        String strAutoReferralReason = "";
        NursingCarePlanServiceImpl ncpmanager = new NursingCarePlanServiceImpl();
        ReferralsTrackingServiceImpl referrals = new ReferralsTrackingServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        //default to English, method doesn't require translation
        AssessmentServiceImpl eachBD = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundServiceImpl woundBD = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
        ListServiceImpl listmanager = new ListServiceImpl(language);
        
        String locale = Common.getLanguageLocale(language);
        
        int intDressingsDays = new Integer((String) Common.getConfig("autoReferralsDressingsDays")).intValue();
        PDate pdate = new PDate();
        Date lngTimestampDressingsCheck = PDate.subtractDays(new Date() , (intDressingsDays));
        // Find ID of the unsaved wound assessment
        WoundAssessmentVO wavTMP = new WoundAssessmentVO();
        wavTMP.setWound_id(new Integer(wound_id + ""));
        wavTMP.setActive(0);
        wavTMP.setProfessional_id(userVO.getId());
        if (assessment_id != -1) {
            wavTMP = new WoundAssessmentVO();
            wavTMP.setId(assessment_id);
        }
        WoundAssessmentVO woundAssessmentVO = wamanager.getAssessment(wavTMP);
        Date refTimestamp = referrals.getLatestReferralTimestamp(new Integer(wound_id));
        //If no ref is found, use start date of wound profile.. count 21 days forward.
        if (refTimestamp == null) {
            WoundVO wpstart = woundBD.getWound(new Integer(wound_id));
            if (wpstart != null) {
                refTimestamp = wpstart.getStart_date() ;
            }
        }
        LookupVO g = listmanager.getListItem(new Integer((String) Common.getConfig("woundGoalHealable")).intValue());
        //if wound profile is older then 21 days
        if (woundAssessmentVO != null && (refTimestamp.before(lngTimestampDressingsCheck))) {
            Collection<AbstractAssessmentVO> assessments = eachBD.getAllAssessments(woundAssessmentVO.getId());
            //Loop for all unsaved wound care alphas
            for (AbstractAssessmentVO unsavedassessment : assessments) {
                //Ensure wound is healable..
                boolean healed = false;
                for (GoalsVO goal : woundGoals) {
                    if (unsavedassessment.getAlpha_id().equals(goal.getAlpha_id()) && g.getId().equals(goal.getLookup_id())) {
                        healed = true;
                    }
                }
                //Alpha Wound types allowed.
                if (unsavedassessment.getFull_assessment().equals(new Integer(1)) && unsavedassessment.getAlphaLocation().getWound_profile_type().getAlpha_type().equals("A") && healed == true && unsavedassessment.getStatus().equals(new Integer((String) Common.getConfig("woundActive")).intValue())) {
                    DressingChangeFrequencyVO assess = new DressingChangeFrequencyVO();
                    assess.setAlpha_id(unsavedassessment.getAlpha_id());
                    assess.setWound_id(new Integer(wound_id));
                    float fltCurrentDressingChangesFrequency = -1;
                    float fltOldDressingChangesFrequency = -1;
                    // Retrieve all historical assessments for this alpha plus the unsaved one (which will be the first)
                    Collection<DressingChangeFrequencyVO> results = ncpmanager.getDressingChangeFrequenciesWithinRange(assess, null, lngTimestampDressingsCheck);
                    for (DressingChangeFrequencyVO dcf : results) {
                        //if all assessments meet critieria throw referral.
                        if (dcf != null) {
                            if (dcf.getDcf() != null) {
                                LookupVO dressingVO = dcf.getDressingChangeFrequency();
                                if (dressingVO != null) {
                                    fltOldDressingChangesFrequency = Float.valueOf(dressingVO.getReport_value());
                                    //why are we getting all of them if we're only using the last one?
                                }
                            }
                        }
                    }
                    DressingChangeFrequencyVO dcftmp = new DressingChangeFrequencyVO();
                    dcftmp.setAlpha_id(unsavedassessment.getAlpha_id());
                    dcftmp.setAssessment_id(unsavedassessment.getAssessment_id());
                    DressingChangeFrequencyVO dcf2 = ncpmanager.getDressingChangeFrequency(dcftmp);
                    if (dcf2 != null) {
                        if (dcf2.getDcf() != null) {
                            try {
                                LookupVO dressingVO = listmanager.getListItem(dcf2.getDcf());
                                if (dressingVO != null) {
                                    fltCurrentDressingChangesFrequency = Float.valueOf(dressingVO.getReport_value());
                                }
                            } catch (ApplicationException e) {
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (Common.getConfig("makeAutoReferralsDressing").equals("1") && fltOldDressingChangesFrequency != -1 && fltCurrentDressingChangesFrequency != -1) {
                        try{
                        float fltCheckDressingChangesFrequency = Float.valueOf(Common.getConfig("autoReferralsDressingsPerWeek"));
                        if (fltOldDressingChangesFrequency > fltCheckDressingChangesFrequency && fltCurrentDressingChangesFrequency > fltCheckDressingChangesFrequency) {
                            strAutoReferralReason = strAutoReferralReason + "; " + Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare",locale) + " " + unsavedassessment.getAlphaLocation().getAlpha() + "" + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_dressing_changes",locale) + " " + Common.getConfig("autoReferralsDressingsPerWeek") + " " + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_dressing_changes_more_than",locale) + " " + Common.getConfig("autoReferralsDressingsDays") + " " + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_dressing_changes_days",locale);
                            //Add Referral (inactive one).
                            ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                            referralVO.setProfessional_id(userVO.getId());
                            referralVO.setAssessment_id(unsavedassessment.getAssessment_id());
                            referralVO.setWound_id(new Integer((String) wound_id));
                            referralVO.setCurrent(new Integer("0"));
                            referralVO.setEntry_type(0); // "Referral"
                            referralVO.setActive(0);
                            referralVO.setWound_profile_type_id(unsavedassessment.getWound_profile_type_id());
                            //check if an already existing inactive is in the database.
                            ReferralsTrackingVO exists = referrals.getReferralsTracking(referralVO);
                            if(exists !=null){
                                referralVO.setId(exists.getId());
                            }
                            ReferralsTrackingVO referral_id = null;
                            referralVO.setCreated_on(new Date());
                            referralVO.setEntry_type(0);
                            //referralVO.setTop_priority(summaryForm.getPriority_wound());
                            //referralVO.setAuto_referral(summaryForm.getAuto_referral_type());
                            referrals.saveReferralsTracking(referralVO, unsavedassessment.getPatient_id());
                            referral_id = referrals.getReferralsTracking(referralVO);
                            //Add auto referrals records by alpha_id
                            AutoReferralVO autoRef = new AutoReferralVO();
                            autoRef.setReferral_id(referral_id.getId());
                            autoRef.setAuto_referral_type(Constants.AUTO_REFERRAL_DCF);
                            autoRef.setAlpha_id(unsavedassessment.getAlpha_id());
                            //check if there's already one there.
                            AutoReferralVO auto_exists = referrals.getAutoReferral(autoRef);
                            if(auto_exists == null){
                                referrals.saveAutoReferral(autoRef);
                            }
                        }
                        }catch(Exception e){log.error("Exception Generating DCF Referral: "+e.getMessage());}
                    }
                }
            }
        }
        return strAutoReferralReason;
    }
    /**
     * Returns the antimicrobial auto referral  reason if triggered.
     * <p/>
     * This record will return an empty string if nothing is triggered.
     *
     * @param tmpWP  the temporary (inactive) wound profile
     * @param liveWP the previously committed wound profile.
     * @param assessment_id if in online (summary) set to -1
     * @return the referral reason
     */
    public String checkAntimicrobialAutoReferral(WoundProfileVO tmpWP, WoundProfileVO liveWP, Collection<GoalsVO> woundGoals, String wound_id, ProfessionalVO userVO, int assessment_id,int language) throws Exception {
        String strAutoReferralReason = "";
                //default to English, method doesn't require translation
        AssessmentServiceImpl eachBD = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
        ReferralsTrackingServiceImpl referrals = new ReferralsTrackingServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundServiceImpl woundBD = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        ListServiceImpl listmanager = new ListServiceImpl(language);
        
        String locale = Common.getLanguageLocale(language);
        
        int intAntiDays = new Integer((String) Common.getConfig("autoReferralsAntimicrobialDays")).intValue();
        PDate pdate = new PDate();
        Date lngTimestampAntiCheck = PDate.subtractDays(new Date(), (intAntiDays));
        String anti_products = Common.getConfig("referralsAntimicrobialProducts");
        String[] antimicrobial_products = Serialize.unserializeByDelimiter(anti_products, ",");
        // Find ID of the unsaved wound assessment
        Date refTimestamp = referrals.getLatestReferralTimestamp(new Integer(wound_id));
        //If no ref is found, use start date of wound profile.. count 21 days forward.
        if (refTimestamp == null) {
            WoundVO wpstart = woundBD.getWound(new Integer(wound_id));
            if (wpstart != null) {
                refTimestamp = wpstart.getStart_date() ;
            }
        }
        WoundAssessmentVO wavTMP = new WoundAssessmentVO();
        wavTMP.setWound_id(new Integer(wound_id + ""));
        wavTMP.setActive(0);
        wavTMP.setProfessional_id(userVO.getId());
        if (assessment_id != -1) {
            wavTMP = new WoundAssessmentVO();
            wavTMP.setId(assessment_id);
        }
        WoundAssessmentVO woundAssessmentVO = wamanager.getAssessment(wavTMP);
        //Find alpha I                 D's for the unsaved wound assessments
        LookupVO g = listmanager.getListItem(new Integer((String) Common.getConfig("woundGoalHealable")).intValue());
        //if wound profile is older then 21 days
        if (woundAssessmentVO != null && (refTimestamp.before(lngTimestampAntiCheck))) {
            Collection<AbstractAssessmentVO> assessments = eachBD.getAllAssessments(woundAssessmentVO.getId());
            //Loop for all unsaved wound care alphas
            for (AbstractAssessmentVO unsavedassessment : assessments) {
                AssessmentProductVO ptmp = new AssessmentProductVO();
                ptmp.setAssessment_id(unsavedassessment.getAssessment_id());
                ptmp.setAlpha_id(unsavedassessment.getAlpha_id());
                Collection<AssessmentProductVO> unsaved_products = eachBD.getAllProducts(ptmp);
                boolean found = false;
                for (String anti : antimicrobial_products) {
                    anti = anti.trim();
                    for (AssessmentProductVO product : unsaved_products) {
                        if (anti.equals(product.getProduct_id() + "")) {
                            found = true;
                        }
                    }
                }
                //Ensure wound is healable..
                boolean healed = false;
                for (GoalsVO goal : woundGoals) {
                    if (unsavedassessment.getAlpha_id().equals(goal.getAlpha_id()) && g.getId().equals(goal.getLookup_id())) {
                        healed = true;
                    }
                }
                //Alpha Wound types allowed.
                //found == true : no sense looking if there's no antimicrobials in the current assessment
                if (found == true && unsavedassessment.getAlphaLocation().getWound_profile_type().getAlpha_type().equals("A") && healed == true && unsavedassessment.getStatus().equals(new Integer((String) Common.getConfig("woundActive")).intValue())) {
                    AssessmentEachwoundVO assess = new AssessmentEachwoundVO();
                    assess.setAlpha_id(unsavedassessment.getAlpha_id());
                    assess.setWound_id(new Integer(wound_id));
                    assess.setFull_assessment(1);
                    boolean antimicrobialReferral = true;
                    String antiProductsList = "";
                    // Retrieve all historical assessments for this alpha plus the unsaved one (which will be the first)
                    AbstractAssessmentVO result = eachBD.getAssessmentWithinRange(assess, null, lngTimestampAntiCheck );//if there are no prior assessments then don't throw referral
                    if (result == null) {
                        antimicrobialReferral = false;
                    }
                    boolean antimicrobialInAssess = false;
                    AssessmentEachwoundVO checkVO = (AssessmentEachwoundVO) result;
                    if (checkVO != null) {
                        AssessmentProductVO tp2 = new AssessmentProductVO();
                        tp2.setAssessment_id(checkVO.getAssessment_id());
                        tp2.setAlpha_id(checkVO.getAlpha_id());
                        Collection<AssessmentProductVO> old_products = eachBD.getAllProducts(tp2);
                        for (String anti : antimicrobial_products) {
                            anti = anti.trim();
                            for (AssessmentProductVO product : old_products) {
                                if (anti.equals(product.getProduct_id() + "")) {
                                    //if there'a  microbial product
                                    antimicrobialInAssess = true;
                                    //antiProductsList = antiProductsList + "" + product.getProduct().getTitle() + ",";
                                }
                            }
                        }
                    }
                    //there were no microbial products in this assessment, so we must break, and not throw referral.
                    if (antimicrobialInAssess == false) {
                        antimicrobialReferral = false;
                        //break;
                    } else {
                        antimicrobialReferral = true;
                    }
                    if (Common.getConfig("makeAutoReferralAntimicrobial").equals("1") && antimicrobialReferral == true) {
                        strAutoReferralReason = strAutoReferralReason + "; " + Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare",locale) + " " + unsavedassessment.getAlphaLocation().getAlpha() + "" + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_antimicrobial",locale) + " " + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_antimicrobial_days",locale) + " " + Common.getConfig("autoReferralsHealDays") + " " + Common.getLocalizedString("pixalere.summary.form.autoreferral_comment_antimicrobial_period",locale);
                        //Add Referral (inactive one).
                            ReferralsTrackingVO referralVO = new ReferralsTrackingVO();
                            referralVO.setProfessional_id(userVO.getId());
                            referralVO.setAssessment_id(unsavedassessment.getAssessment_id());
                            referralVO.setWound_id(new Integer((String) wound_id));
                            referralVO.setCurrent(new Integer("0"));
                            referralVO.setEntry_type(0); // "Referral"
                            referralVO.setActive(0);
                            referralVO.setWound_profile_type_id(unsavedassessment.getWound_profile_type_id());
                            //check if an already existing inactive is in the database.
                            ReferralsTrackingVO exists = referrals.getReferralsTracking(referralVO);
                            if(exists !=null){
                                referralVO.setId(exists.getId());
                            }
                            ReferralsTrackingVO referral_id = null;
                            referralVO.setCreated_on(new Date());
                            referralVO.setEntry_type(0);
                            //referralVO.setTop_priority(summaryForm.getPriority_wound());
                            //referralVO.setAuto_referral(summaryForm.getAuto_referral_type());
                            referrals.saveReferralsTracking(referralVO, unsavedassessment.getPatient_id());
                            referral_id = referrals.getReferralsTracking(referralVO);
                            //Add auto referrals records by alpha_id
                            AutoReferralVO autoRef = new AutoReferralVO();
                            autoRef.setReferral_id(referral_id.getId());
                            autoRef.setAuto_referral_type(Constants.AUTO_REFERRAL_ANTIMICROBIAL);
                            autoRef.setAlpha_id(unsavedassessment.getAlpha_id());
                            
                            //check if there's already one there.
                            AutoReferralVO auto_exists = referrals.getAutoReferral(autoRef);
                            if(auto_exists == null){
                                referrals.saveAutoReferral(autoRef);
                            }
                    }
                }
            }
        }
        return strAutoReferralReason;
    }
    /**
     * Calls all the auto referral methods, and checks if something should be triggered.
     * If it does, then it inserts a new referral record if one doesnt already exist, and a comment.
     *
     * @param assessment_id
     * @param wound_profile_type_id
     */
    public void checkAutoReferralsForOffline(int assessment_id, int wound_profile_type_id,int language) {
        if (assessment_id != -1) {
                    //default to English, method doesn't require translation
            WoundServiceImpl wpService = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            WoundAssessmentServiceImpl wamanager = new WoundAssessmentServiceImpl();
            AssessmentCommentsServiceImpl cmanager = new AssessmentCommentsServiceImpl();
            String autoReferralReason = "";
            int autoReferral = 0;
            int autoReferralPriority = 0;
            try {
                WoundAssessmentVO wa = new WoundAssessmentVO();
                wa.setId(assessment_id);
                WoundAssessmentVO currentAssess = wamanager.getAssessment(wa);
                PDate pdate = new PDate(currentAssess.getProfessional() != null ? currentAssess.getProfessional().getTimezone() : Constants.TIMEZONE);
                //already committed, so no need to pass tmpWP
                WoundProfileVO wp = new WoundProfileVO();
                wp.setWound_id(currentAssess.getWound_id());
                wp.setCurrent_flag(1);
                wp.setActive(1);
                WoundProfileVO currentWP = wpService.getWoundProfile(wp);
                if (currentWP != null) {
                    Date lngTimestamp = currentAssess.getCreated_on();
                    String reason = checkHealRateAutoReferral(lngTimestamp, currentWP.getGoals(), currentAssess.getWound_id() + "", currentAssess.getProfessional(), assessment_id,language);
                    if (!reason.equals("")) {
                        autoReferral = 1;
                        autoReferralPriority = 6;
                        autoReferralReason = autoReferralReason + "; " + reason;
                    }
                    reason = "";
                    reason = checkDcfAutoReferral(null, currentWP, currentAssess.getWound_id() + "", currentWP.getGoals(), currentAssess.getProfessional(), assessment_id,language);
                    if (!reason.equals("")) {
                        autoReferral = 1;
                        autoReferralPriority = 7;
                        if (autoReferralPriority == 6) {
                            autoReferralPriority = 8;
                        }
                        autoReferralReason = autoReferralReason + "; " + reason;
                    }
                    reason = "";
                    reason = checkAntimicrobialAutoReferral(null, currentWP, currentWP.getGoals(), currentAssess.getWound_id() + "", currentAssess.getProfessional(), assessment_id,language);
                    if (!reason.equals("")) {
                        autoReferral = 1;
                        autoReferralPriority = 9;
                        autoReferralReason = autoReferralReason + "; " + reason;
                    }
                    if (autoReferral == 1) {
                        PatientServiceImpl pservice = new PatientServiceImpl(com.pixalere.utils.Constants.ENGLISH);
                        PatientAccountVO p = new PatientAccountVO();
                        p.setPatient_id(currentAssess.getPatient_id());
                        p.setCurrent_flag(1);
                        PatientAccountVO patient = pservice.getPatient(p);
                        ReferralsTrackingServiceImpl referrals = new ReferralsTrackingServiceImpl(com.pixalere.utils.Constants.ENGLISH);
                        /*ReferralsTrackingVO r = new ReferralsTrackingVO();
                        r.setCurrent(1);
                       
                        r.setEntry_type(0);
                        r.setProfessional_id(currentAssess.getProfessional().getId());
                        r.setCreated_on(currentAssess.getCreated_on());
                        //check if one was already done
                        ReferralsTrackingVO chref = new ReferralsTrackingVO();
                        chref.setAssessment_id(assessment_id);
                        chref.setWound_profile_type_id(wound_profile_type_id);
                        chref.setEntry_type(0);
                        ReferralsTrackingVO liveRef = referrals.getReferralsTracking(chref);
                        r.setTop_priority(new Integer(Common.getConfig("autoReferralPriority")));
                        if (liveRef != null) {
                            r.setId(liveRef.getId());//if it already exists, update current one.
                        }
                        r.setPatient_account_id(patient.getId());
                        r.setWound_id(currentAssess.getWound_id());
                        r.setWound_profile_type_id(wound_profile_type_id);
                        r.setAssessment_id(currentAssess.getId());
                        referrals.saveReferralsTracking(r);*/
                        //check if comment was already entered, if so, append comment, otherwise insert new.
                        AssessmentCommentsVO c = new AssessmentCommentsVO();
                        c.setAssessment_id(assessment_id);
                        c.setProfessional_id(currentAssess.getProfessional().getId());
                        AssessmentCommentsVO comment = cmanager.getComment(c);
                        if (comment != null) {
                            comment.setBody(comment.getBody() + " " + autoReferralReason);
                        } else {
                            comment = new AssessmentCommentsVO();
                            comment.setProfessional_id(currentAssess.getProfessional().getId());
                            comment.setAssessment_id(assessment_id);
                            comment.setPatient_id(currentAssess.getPatient_id());
                            comment.setWound_id(currentAssess.getWound_id());
                            comment.setWound_profile_type_id(wound_profile_type_id);
                            comment.setBody(autoReferralReason);
                            comment.setCreated_on(new Date());
                            comment.setUser_signature(pdate.getProfessionalSignature(new Date(), currentAssess.getProfessional(),"en"));
                            cmanager.saveComment(comment);
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }
      /**
     * Get the visit count between date ranges.
     *
     * @since 3.2
     */
    public static List getVisitCount(Collection<WoundAssessmentVO> assess, Date start, Date end, Integer language) {
        List<PatientVisit> list = new ArrayList();
        int count = 0;
        boolean checkDone = false;
        int size = 0;
        Hashtable filterHash = new Hashtable();
        WoundServiceImpl wservice = new WoundServiceImpl(language);
        Date enc_start = null;
        if (assess != null) {
            for (WoundAssessmentVO assessment : assess) {
                PDate pdate = new PDate();
                Date time = null;
                int assessStart = 0;
                int assessEnd = 0;
                try {
                    time = assessment.getCreated_on();

                } catch (NumberFormatException e) {
                }
                size++;
                //System.out.println("Time: " + assessStart);
                if ((start == null || end == null) || (start.before(time) && end.after(time))) {
                    if (assessment.getVisit() != null && assessment.getVisit().equals(new Integer(1))) {
                        if (count == 0) {
                            //first assessment, grab start date
                            enc_start = assessment.getCreated_on();
                        }
                        count++;
                    }
                    checkDone = false;
                    //System.out.println("visit: " + assessment.getVisit());
                    if (assessment.getReset_visit() == 1) {
                        try{
                            WoundVO wound = wservice.getWound(assessment.getWound_id());
                            String location = "";
                            if(wound!=null){
                                location = wound.getWound_location_detailed();
                            }
                            PatientVisit visit = new PatientVisit(assessment.getTreatment_location_id() + "", count, assessment.getReset_reason(), new PDate().getDate(enc_start, true), pdate.getDate(assessment.getCreated_on(), true),location);
                            count = 0;
                            checkDone = true;//already added, so dont add twice.
                            list.add(visit);
                        }catch(ApplicationException e){}
                    }
                    //System.out.println(checkDone + "Size: " + assess.size() + " " + size);
                    if (assess.size() == size && checkDone == false) {//isnt reset yet.. so add now.
                        try{
                            WoundVO wound = wservice.getWound(assessment.getWound_id());
                            String location = "";
                            if(wound!=null){
                                location = wound.getWound_location_detailed();
                            }
                            PatientVisit visit = new PatientVisit(assessment.getTreatment_location_id() + "", count, assessment.getReset_reason(), new PDate().getDate(enc_start, false), pdate.getDate(assessment.getCreated_on(), false),location);
                            list.add(visit);
                        }catch(ApplicationException e){}
                    }
                }

            }
        }
        //get Correct Treatment Location title;
        List visits = new ArrayList();
        Hashtable ids = new Hashtable();
        ListServiceImpl lservice = new ListServiceImpl(language);
        for (PatientVisit vis : list) {
            int id = Integer.parseInt(vis.getTreatment_location());
            String treatment = "";
            if (ids.get(id + "") != null) {//No sense making db calls if we dont need to.
                treatment = (String) ids.get(id + "");
            } else {
                try {
                    LookupVO item = lservice.getListItem(id);
                    ids.put(id + "", item.getName(language));
                    treatment = item.getName(language);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
            //System.out.println("Filter: " + treatment + " " + vis.getCount() + " " + vis.getReason());
            PatientVisit vp = new PatientVisit(treatment, vis.getCount(), vis.getReason(), vis.getStart(), vis.getEnd(),vis.getWound());
            visits.add(vp);
        }

        return visits;
    }
}
