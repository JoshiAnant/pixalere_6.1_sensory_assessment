package com.pixalere.assessment.service;
import com.pixalere.common.ApplicationException;
import java.util.Vector;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface AssessmentImagesService {
    public AssessmentImagesVO getImage(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveImage(@WebParam(name="updateRecord") AssessmentImagesVO updateRecord) throws ApplicationException;
    public Vector<AssessmentImagesVO> getAllImageFiles(@WebParam(name="assessmentImages") AssessmentImagesVO assessmentImages) throws ApplicationException;
}