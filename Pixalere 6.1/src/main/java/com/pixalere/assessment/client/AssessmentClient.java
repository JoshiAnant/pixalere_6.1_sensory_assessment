package com.pixalere.assessment.client;
import com.pixalere.assessment.service.AssessmentService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class AssessmentClient extends AbstractClient{
    public AssessmentClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  AssessmentService createAssessmentClient(){
    	
    	factory.setServiceClass(AssessmentService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"AssessmentService");
        AssessmentService service = (AssessmentService) getClient();       
        return service;
    }
}