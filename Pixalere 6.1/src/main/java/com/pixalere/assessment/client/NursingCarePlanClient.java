package com.pixalere.assessment.client;
import com.pixalere.assessment.service.NursingCarePlanService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class NursingCarePlanClient extends AbstractClient{
    public NursingCarePlanClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  NursingCarePlanService createNursingCarePlanClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(NursingCarePlanService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"NursingCarePlanService");
        NursingCarePlanService service = (NursingCarePlanService) getClient();       
        return service;
    }
}