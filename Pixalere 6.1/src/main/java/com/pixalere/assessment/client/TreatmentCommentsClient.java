package com.pixalere.assessment.client;
import com.pixalere.assessment.service.TreatmentCommentsService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class TreatmentCommentsClient extends AbstractClient{
    public TreatmentCommentsClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  TreatmentCommentsService createTreatmentCommentsClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(TreatmentCommentsService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"TreatmentCommentsService");
        TreatmentCommentsService service = (TreatmentCommentsService) getClient();       
        return service;
    }
}