package com.pixalere.assessment.client;
import com.pixalere.assessment.service.WoundAssessmentLocationService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class WoundAssessmentLocationClient extends AbstractClient{
    public WoundAssessmentLocationClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  WoundAssessmentLocationService createWoundAssessmentLocationClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(WoundAssessmentLocationService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"WoundAssessmentLocationService");
        WoundAssessmentLocationService service = (WoundAssessmentLocationService) getClient();       
        return service;
    }
}