package com.pixalere.assessment.client;
import com.pixalere.assessment.service.AssessmentImagesService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class AssessmentImagesClient extends AbstractClient{
    public AssessmentImagesClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  AssessmentImagesService createAssessmentImagesClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(AssessmentImagesService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"AssessmentImagesService");
        AssessmentImagesService service = (AssessmentImagesService) getClient();       
        return service;
    }
}