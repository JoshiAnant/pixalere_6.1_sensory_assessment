package com.pixalere.assessment.client;
import com.pixalere.assessment.service.AssessmentNPWTService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class AssessmentNPWTClient extends AbstractClient{
    public AssessmentNPWTClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  AssessmentNPWTService createNPWTClient(){
    	
    	factory.setServiceClass(AssessmentNPWTService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"AssessmentNPWTService");
        AssessmentNPWTService service = (AssessmentNPWTService) getClient();       
        return service;
    }
}