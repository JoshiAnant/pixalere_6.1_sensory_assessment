package com.pixalere.assessment.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Feb 23, 2010
 * Time: 10:00:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class AssessmentUnderminingVO   extends ValueObject implements Serializable {
    private Integer id;
    private Integer alpha_id;
    private Integer assessment_wound_id;
    private Integer depth_cm;
    private Integer depth_mm;
    private Integer location_start;
    private Integer location_end;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    public Integer getAssessment_wound_id() {
        return assessment_wound_id;
    }
    public void setAssessment_wound_id(Integer assessment_wound_id) {
        this.assessment_wound_id = assessment_wound_id;
    }
    public Integer getDepth_cm() {
        return depth_cm;
    }
    public void setDepth_cm(Integer depth_cm) {
        this.depth_cm = depth_cm;
    }
    public Integer getDepth_mm() {
        return depth_mm;
    }
    public void setDepth_mm(Integer depth_mm) {
        this.depth_mm = depth_mm;
    }
    public Integer getLocation_start() {
        return location_start;
    }
    public void setLocation_start(Integer location_start) {
        this.location_start = location_start;
    }
    public Integer getLocation_end() {
        return location_end;
    }
    public void setLocation_end(Integer location_end) {
        this.location_end = location_end;
    }
}
