package com.pixalere.assessment.bean;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.Vector;
import com.pixalere.utils.Common;

/**
 *     This is the AssessmentEachwound  bean for the assessment_eachwound  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentVO
 *
 */
public class AssessmentEachwoundVO extends AbstractAssessmentVO {
    
    
    
    private static int PERIWOUND_SKIN = 1;
    
    private static int COLOUR = 2;
    private static int WOUND_EDGE=3;
    private static int ADJUNCTIVE = 4;
    private static int EXUDATE_TYPE=5;
    private String pain_comments;
    private Integer recurrent;
    private String laser;
    private Date wound_date;
    private Collection<AssessmentUnderminingVO> underminings;
    private Collection<AssessmentSinustractsVO> sinustracts;
    private Collection<AssessmentWoundBedVO> woundbed;
    private String fistulas;
    private Integer num_fistulas;
    /** nullable persistent field */
    private Double length;
    private Double width;
    private Double skin_temperature;
    private Integer push_scale;
    /** nullable persistent field */
    private Double depth;
    /** nullable persistent field */
    private String wound_edge;
    private Integer pressure_ulcer;
    private Integer pain;
    
    /** nullable persistent field */
    private String periwound_skin;
    
    /** nullable persistent field */
    private Integer exudate_amount;
    
    /** nullable persistent field */
    private String wound_depth;
    
    /** nullable persistent field */
    private String exudate_colour;
    
    /** nullable persistent field */
    private Integer exudate_odour;
    
    /** nullable persistent field */

    
    /** nullable persistent field */
    private String exudate_type;
    private Double heal_rate;
    
    private WoundAssessmentVO woundAssessment;
    
    public Integer getPain(){
        return pain;
    }
    public void setPain(Integer pain){
        this.pain=pain;
    }
    public Integer getPressure_ulcer(){
        return pressure_ulcer;
    }
    public void setPressure_ulcer(Integer pressure_ulcer){
        this.pressure_ulcer=pressure_ulcer;
    }
    /**
     * @method getWound_date
     * @field wound_date - Integer
     * @return Returns the wound_date.
     */
    public Date getWound_date() {
        return wound_date;
    }
    
    
   
    /**
     * @method setWound_date
     * @field wound_date - Integer
     * @param wound_date
     *            The wound_date to set.
     */
    public void setWound_date(Date wound_date) {
        this.wound_date = wound_date;
    }
    
    /**
     * @method getRecurrent
     * @field recurrent - Integer
     * @return Returns the recurrent.
     */
    public Integer getRecurrent() {
        return recurrent;
    }
    
    /**
     * @method setRecurrent
     * @field recurrent - Integer
     * @param recurrent
     *            The recurrent to set.
     */
    public void setRecurrent(Integer recurrent) {
        this.recurrent = recurrent;
    }
    
  
    
    
    /** default constructor */
    public AssessmentEachwoundVO() {
    }
    

    
    
    public String getWound_edge() {
        return this.wound_edge;
    }
    
    public void setWound_edge(String wound_edge) {
        this.wound_edge = wound_edge;
    }
    
    public String getPeriwound_skin() {
        return this.periwound_skin;
    }
    
    public void setPeriwound_skin(String periwound_skin) {
        this.periwound_skin = periwound_skin;
    }
    
    public Integer getExudate_amount() {
        return this.exudate_amount;
    }
    
    public void setExudate_amount(Integer exudate_amount) {
        this.exudate_amount = exudate_amount;
    }
    
    public String getWound_depth() {
        return this.wound_depth;
    }
    
    public void setWound_depth(String wound_depth) {
        this.wound_depth = wound_depth;
    }
    
    public String getExudate_colour() {
        return this.exudate_colour;
    }
    
    public void setExudate_colour(String exudate_colour) {
        this.exudate_colour = exudate_colour;
    }
    
    public Integer getExudate_odour() {
        return this.exudate_odour;
    }
    
    public void setExudate_odour(Integer exudate_odour) {
        this.exudate_odour = exudate_odour;
    }
    
    
   
    
    public String getExudate_type() {
        return this.exudate_type;
    }
    
    
    
    public void setExudate_type(String exudate_type) {
        this.exudate_type = exudate_type;
    }
    
    public boolean ifExists(String serial, String item) {
        
        if (serial == null || serial.indexOf(item.replaceAll(" ","_")) == -1) {
            return false;
        } else {
            return true;
        }
    }
    
    
    public String toString(Integer item) {
        if (item.intValue() == -1 || item == null) {
            return "0";
        }
        return item + "";
    }
   public Collection<AssessmentUnderminingVO> getUnderminings() {
       return underminings;
   }
   public void setUnderminings(Collection<AssessmentUnderminingVO> underminings) {
       this.underminings = underminings;
   }
   public Collection<AssessmentSinustractsVO> getSinustracts() {
       return sinustracts;
   }
   public void setSinustracts(Collection<AssessmentSinustractsVO> sinustracts) {
       this.sinustracts = sinustracts;
   }
   public Collection<AssessmentWoundBedVO> getWoundbed() {
       return woundbed;
   }
   public void setWoundbed(Collection<AssessmentWoundBedVO> woundbed) {
       this.woundbed = woundbed;
   }
    
    public Vector getAgeofWoundDate() {
        Vector dates = new Vector();
        try {
            
            if (getWound_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar
                    .setTime(getWound_date());
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        }catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            dates = new Vector();
        }
        return dates;
    }
    public Vector getClosedDate() {
        Vector dates = new Vector();
        try {
            
            if (getClosed_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getClosed_date());
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }
        catch (Exception e) {
            e.printStackTrace();
            dates = new Vector();
        }
        return dates;
    }
    
    
   
    public WoundAssessmentVO getWoundAssessment() {
        return woundAssessment;
    }
    
    public void setWoundAssessment(WoundAssessmentVO woundAssessment) {
        this.woundAssessment = woundAssessment;
    }
    public String getLaser() {
        return laser;
    }
    public void setLaser(String laser) {
        this.laser = laser;
    }
    public String getPain_comments() {
        return pain_comments;
    }
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
    public String getFistulas() {
        return fistulas;
    }
    public void setFistulas(String fistulas) {
        this.fistulas = fistulas;
    }
    public Integer getNum_fistulas() {
        return num_fistulas;
    }
    public void setNum_fistulas(Integer num_fistulas) {
        this.num_fistulas = num_fistulas;
    }
   /** Legacy.. only used to upgrade all clients to 4.1.2 **/
    public boolean ifWoundBaseExists(String serial,String item){
       if (serial == null || serial.indexOf(item) == -1) {
           return false;
       } else {
           return true;
       }
   }
       public int getPercentagesByItem(String percentages, String item) {
       String tmpString = "";
       if(percentages == null){
           percentages = "";
       }
       if(item == null){
           item = "";
       }
       if (percentages.indexOf("\"" + item.toLowerCase().replaceAll(" ", "_")
       + "\"") != -1
               || percentages.indexOf("\""
               + item.toLowerCase().replaceAll(" ", "_") + "\"") != -1) {
           int num = percentages.indexOf("\""
                   + item.toLowerCase().replaceAll(" ", "_") + "\"");
           String parsed = percentages.substring(num
                   + item.toLowerCase().replaceAll(" ", "_").length() + 8,
                   percentages.length());
           tmpString = parsed.substring(0, parsed.indexOf("\""));
       } else if (percentages.indexOf("\"woundbase_"
               + item.toLowerCase().replaceAll(" ", "_") + "\"") != -1
               || percentages.indexOf("\""
               + item.toLowerCase().replaceAll(" ", "_") + "\"") != -1) {
           int num = percentages.indexOf("\"woundbase_"
                   + item.toLowerCase().replaceAll(" ", "_") + "\"");
           String parsed = percentages.substring(num
                   + item.toLowerCase().replaceAll(" ", "_").length() + 18,
                   percentages.length());
           tmpString = parsed.substring(0, parsed.indexOf("\""));
       }
       if (tmpString.equals("")) {
           return 0;
       }
       return (Integer.parseInt(tmpString));
   }
    public String getWound_base() {
       return this.wound_base;
   }
   public void setWound_base(String wound_base) {
       this.wound_base = wound_base;
   }
   public String getWound_base_percentage() {
       return this.wound_base_percentage;
   }
   public void setWound_base_percentage(String wound_base_percentage) {
       this.wound_base_percentage = wound_base_percentage;
   }
 /** nullable persistent field */
   private String wound_base;
   /** nullable persistent field */
   private String wound_base_percentage;
  
    /**
     * @return the push_scale
     */
    public Integer getPush_scale() {
        return push_scale;
    }
    /**
     * @param push_scale the push_scale to set
     */
    public void setPush_scale(Integer push_scale) {
        this.push_scale = push_scale;
    }
    private Date target_discharge_date;
    public Vector getTargetDischargeDate() {
        Vector<String> dates = new Vector<String>();
        try {
             if (getTarget_discharge_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getTarget_discharge_date());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {ex.printStackTrace();
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {e.printStackTrace();
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    /**
     * @return the target_discharge_date
     */
    public Date getTarget_discharge_date() {
        return target_discharge_date;
    }
    /**
     * @param target_discharge_date the target_discharge_date to set
     */
    public void setTarget_discharge_date(Date target_discharge_date) {
        this.target_discharge_date = target_discharge_date;
    }

    /**
     * @return the heal_rate
     */
    public Double getHeal_rate() {
        return heal_rate;
    }

    /**
     * @param heal_rate the heal_rate to set
     */
    public void setHeal_rate(Double heal_rate) {
        this.heal_rate = heal_rate;
    }

    /**
     * @return the length
     */
    public Double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Double length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public Double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Double width) {
        this.width = width;
    }

    /**
     * @return the depth
     */
    public Double getDepth() {
        return depth;
    }

    /**
     * @param depth the depth to set
     */
    public void setDepth(Double depth) {
        this.depth = depth;
    }

    /**
     * @return the skin_temperature
     */
    public Double getSkin_temperature() {
        return skin_temperature;
    }

    /**
     * @param skin_temperature the skin_temperature to set
     */
    public void setSkin_temperature(Double skin_temperature) {
        this.skin_temperature = skin_temperature;
    }
}
