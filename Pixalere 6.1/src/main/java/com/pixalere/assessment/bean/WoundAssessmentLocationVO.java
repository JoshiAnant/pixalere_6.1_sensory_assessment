package com.pixalere.assessment.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.utils.Common;
import java.util.Date;
/**
 *     This is the WoundAssessmentLocation  bean for the wound_assessment_location  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundProfileTypeVO
 *     @has 1..1 Has 1..* com.pixalere.wound.bean.WoundLocationDetailsVO
 *
 */
public class WoundAssessmentLocationVO extends ValueObject implements Serializable {
    private WoundProfileTypeVO wound_profile_type;
    private WoundLocationDetailsVO[] alpha_details;
    /** identifier field */
    private Integer id;
    private Integer wound_profile_type_id;
    private Integer professional_id;
    /** nullable persistent field */
    private Integer patient_id;
    /** nullable persistent field */
    private Integer wound_id;
    private Integer x;
    private Integer y;
    /** nullable persistent field */
    private String alpha;
    /** nullable persistent field */
    private Integer active;
    /** nullable persistent field */
    private Integer discharge;
    /** nullable persistent field */
    private Integer reactivate;
    /** nullable persistent field */
    private Date open_timestamp;
    /** nullable persistent field */
    private Date close_timestamp;
    private Date lastmodified_on;
    private Date deleted_on;
    private Double current_heal_rate;
    /** default constructor */
    public WoundAssessmentLocationVO() {
    }
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    public Integer getDeleted() {
        return deleted;
    }
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public String getDeleted_reason() {
        return deleted_reason;
    }
    public void setDeleted_reason(String deleted_reason) {
        this.deleted_reason = deleted_reason;
    }
    public String getDelete_signature() {
        return delete_signature;
    }
    public void setDelete_signature(String delete_signature) {
        this.delete_signature = delete_signature;
    }
    public Integer getId() {
        return this.id;
    }
    /**
     * @method getProfessional_id
     * @field professional_id - Integer
     * @return Returns the professional_id.
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    /**
     * @method setProfessional_id
     * @field professional_id - Integer
     * @param professional_id The professional_id to set.
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public Integer getId(Integer id) {
        return this.getId();
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatient_id() {
        return this.patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getWound_id() {
        return this.wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public String getAlpha() {
        return this.alpha;
    }
    public void setAlpha(String alpha) {
        this.alpha = alpha;
    }
    public Integer getActive() {
        return this.active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public Integer getReactivate() {
        return this.reactivate;
    }
    public void setReactivate(Integer reactivate) {
        this.reactivate = reactivate;
    }
    public Integer getDischarge() {
        return this.discharge;
    }
    public void setDischarge(Integer discharge) {
        this.discharge = discharge;
    }
    public Date getOpen_timestamp() {
        return this.open_timestamp;
    }
    public void setOpen_timestamp(Date open_timestamp) {
        this.open_timestamp = open_timestamp;
    }
    public Date getClose_timestamp() {
        return this.close_timestamp;
    }
    public void setClose_timestamp(Date close_timestamp) {
        this.close_timestamp = close_timestamp;
    }
    public void setAlpha_details(WoundLocationDetailsVO[] alpha_details) {
        this.alpha_details = alpha_details;
    }
    public WoundLocationDetailsVO[] getAlpha_details() {
        return alpha_details;
    }
    /*public boolean isOstomy(){
    return ALPHA_TYPE_OSTOMY.equalsIgnoreCase(this.getAlpha_type());
    }
    public boolean isWound(){
    return ALPHA_TYPE_WOUND.equalsIgnoreCase(this.getAlpha_type());
    }
    public boolean isDrain(){
    return ALPHA_TYPE_DRAIN.equalsIgnoreCase(this.getAlpha_type());
    }
    public boolean isSurgery(){
    return ALPHA_TYPE_SURGERY.equalsIgnoreCase(this.getAlpha_type());
    }   */
    // FIXME: Temp while we find the locale/language at the page
    public String getAlphaName(){
        String locale = "en";
        return getAlphaName(locale);
    }
    public String getAlphaName(String locale) {
        String strAlphaName = "";
        try {
            if (alpha != null && getWound_profile_type() != null) {
                if (getWound_profile_type().getAlpha_type().indexOf("A") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare", locale) + " " + getAlpha();
                }
                if (getWound_profile_type().getAlpha_type().indexOf("D") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_drain", locale) + " " + getAlpha().substring(2);
                }
                if (getWound_profile_type().getAlpha_type().indexOf("T") != -1) {
                    strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_incisiontag", locale) + " " + getAlpha().substring(3);
                }
                if (getWound_profile_type().getAlpha_type().indexOf("O") != -1) {
                    if (getAlpha().indexOf("ostu") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_urinalstoma", locale) + " " + getAlpha().substring(4);
                    }
                    if (getAlpha().indexOf("ostf") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_fecalstoma", locale) + " " + getAlpha().substring(4);
                    }
                    
                }
                if (getWound_profile_type().getAlpha_type().indexOf("S") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_skin", locale) + " " + getAlpha().substring(1);
                }
                if (getWound_profile_type().getAlpha_type().indexOf("B") != -1) {
                        strAlphaName = Common.getLocalizedString("pixalere.viewer.form.assessment_burn", locale) ;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAlphaName;
    }
    public WoundProfileTypeVO getWound_profile_type() {
        return wound_profile_type;
    }
    public void setWound_profile_type(WoundProfileTypeVO wound_profile_type) {
        this.wound_profile_type = wound_profile_type;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
    
    /**
     * @return the x
     */
    public Integer getX() {
        return x;
    }
    /**
     * @param x the x to set
     */
    public void setX(Integer x) {
        this.x = x;
    }
    /**
     * @return the y
     */
    public Integer getY() {
        return y;
    }
    /**
     * @param y the y to set
     */
    public void setY(Integer y) {
        this.y = y;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }

    /**
     * @return the current_heal_rate
     */
    public Double getCurrent_heal_rate() {
        return current_heal_rate;
    }

    /**
     * @param current_heal_rate the current_heal_rate to set
     */
    public void setCurrent_heal_rate(Double current_heal_rate) {
        this.current_heal_rate = current_heal_rate;
    }
}
