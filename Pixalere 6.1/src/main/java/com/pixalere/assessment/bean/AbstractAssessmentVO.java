package com.pixalere.assessment.bean;

import java.util.Date;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.common.ValueObject;
import java.util.Hashtable;

/**
 *     This is the Abstract class for all the assessment  (DB table) beans
 *     Refer to {@link com.pixalere.assessment.service.AssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentVO
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentLocationVO
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundProfileTypeVO
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.AssessmentProductVO
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *     @has 1..1 Has 1..1 com.pixalere.patient.bean.PatientAccountVO
*/
public class AbstractAssessmentVO extends ValueObject{
    private WoundAssessmentVO woundAssessment;
    private WoundAssessmentLocationVO alphaLocation;
    private WoundProfileTypeVO wound_profile_type;

    
    private ProfessionalVO professional;
    private PatientAccountVO patient;
    /** identifier field */
    private Integer id;
    private Integer full_assessment;
    private Integer patient_id;
    private String discharge_other;
    private Date closed_date;
    private Integer wound_id;
    private Integer assessment_id;
    private Integer status;
    private Integer alpha_id;
    private Integer priority;
    private Integer discharge_reason;
    private Integer active;
    private Integer teaching_status;
    private Date lastmodified_on;
    private Date created_on;
    
   
    private Integer wound_profile_type_id;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
    private String moved_reason;
    private Integer prev_alpha_id;
     public Integer getDeleted() {
         return deleted;
     }

     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }

     public String getDeleted_reason() {
         return deleted_reason;
     }

     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }

     public String getDelete_signature() {
         return delete_signature;
     }

     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    
   
    
    public WoundAssessmentLocationVO getAlphaLocation() {
        return alphaLocation;
    }
    
    public void setAlphaLocation(WoundAssessmentLocationVO alphaLocation) {
        this.alphaLocation = alphaLocation;
    }
    
    public WoundAssessmentVO getWoundAssessment() {
        return woundAssessment;
    }
    
    public void setWoundAssessment(WoundAssessmentVO woundAssessment) {
        this.woundAssessment = woundAssessment;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    
    public PatientAccountVO getPatient() {
        return patient;
    }
    
    public void setPatient(PatientAccountVO patient) {
        this.patient = patient;
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getFull_assessment() {
        return full_assessment;
    }
    
    public void setFull_assessment(Integer full_assessment) {
        this.full_assessment = full_assessment;
    }
    
    public Integer getPatient_id() {
        return patient_id;
    }
    
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    
    public String getDischarge_other() {
        return discharge_other;
    }
    
    public void setDischarge_other(String discharge_other) {
        this.discharge_other = discharge_other;
    }
    
  
    public Date getClosed_date() {
        return closed_date;
    }
    
    public void setClosed_date(Date closed_date) {
        this.closed_date = closed_date;
    }
    
    public Integer getWound_id() {
        return wound_id;
    }
    
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    
    public Integer getAssessment_id() {
        return assessment_id;
    }
    
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    
    public Integer getStatus() {
        return status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public Integer getAlpha_id() {
        return alpha_id;
    }
    
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    
 
    
    public Integer getPriority() {
        return priority;
    }
    
    public void setPriority(Integer priority) {
        this.priority = priority;
    }
    
    public Integer getDischarge_reason() {
        return discharge_reason;
    }
    
    public void setDischarge_reason(Integer discharge_reason) {
        this.discharge_reason = discharge_reason;
    }
    
    public Integer getActive() {
        return active;
    }
    
    public void setActive(Integer active) {
        this.active = active;
    }
    
    public boolean ifExists(String serial, String item) {
        
        if (serial == null || serial.indexOf(item.replaceAll(" ","_")) == -1) {
            return false;
        } else {
            return true;
        }
    }
    
    public Hashtable printAssessment(){
        return null;
    }
    
    public Integer getTeaching_status() {
        return teaching_status;
    }
    
    public void setTeaching_status(Integer teaching_status) {
        this.teaching_status = teaching_status;
    }



    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date last_update) {
        this.created_on = last_update;
    }


    public WoundProfileTypeVO getWound_profile_type() {
        return wound_profile_type;
    }

    public void setWound_profile_type(WoundProfileTypeVO wound_profile_type) {
        this.wound_profile_type = wound_profile_type;
    }

    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }

    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }

    /**
     * @return the prev_alpha_id
     */
    public Integer getPrev_alpha_id() {
        return prev_alpha_id;
    }

    /**
     * @param prev_alpha_id the prev_alpha_id to set
     */
    public void setPrev_alpha_id(Integer prev_alpha_id) {
        this.prev_alpha_id = prev_alpha_id;
    }

    /**
     * @return the moved_reason
     */
    public String getMoved_reason() {
        return moved_reason;
    }

    /**
     * @param moved_reason the moved_reason to set
     */
    public void setMoved_reason(String moved_reason) {
        this.moved_reason = moved_reason;
    }
/**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }

    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
      private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }

    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }

    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }

    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
}
