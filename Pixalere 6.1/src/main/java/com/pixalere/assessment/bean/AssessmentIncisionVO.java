package com.pixalere.assessment.bean;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Vector;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.*;
/**
 *     This is the AssessmentIncision bean for the assessment_incision  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class AssessmentIncisionVO  extends AbstractAssessmentVO {
        private String postop_management;
        private Integer postop_days;
	static private Logger log = Logger.getLogger(AssessmentIncisionVO.class);
        private String pain_comments;
        private Integer pain;
        private Integer incision_status;
        private String incision_exudate;
        private String peri_incisional;
        private Integer clinical_path;
        private Date clinical_path_date;
        private String incision_closure_methods;
        private Integer exudate_amount;
        private Integer postop_day;
        
 	public AssessmentIncisionVO() {
	}
	
	
        public Vector getClinicalPathDate() {
        Vector<String> dates = new Vector<String>();
        try {
            
            if (clinical_path_date == null) {
                return new Vector();
            }
            
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(clinical_path_date );
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    
    public Vector getClosedDate() {
        Vector dates = new Vector();
        try {
            
            if (getClosed_date() == null ) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getClosed_date());
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }
        catch (Exception e) {
            e.printStackTrace();
            dates = new Vector();
        }
        return dates;
    }
    
    public Integer getPain(){
        return pain;
    }
    public void setPain(Integer pain){
        this.pain=pain;
    }
    
    public Integer getIncision_status() {
        return incision_status;
    }
    public void setIncision_status(Integer incision_status) {
        this.incision_status = incision_status;
    }
    public String getIncision_exudate() {
        return incision_exudate;
    }
    public void setIncision_exudate(String incision_exudate) {
        this.incision_exudate = incision_exudate;
    }
    public String getPeri_incisional() {
        return peri_incisional;
    }
    public void setPeri_incisional(String peri_incisional) {
        this.peri_incisional = peri_incisional;
    }
    public String getIncision_closure_methods() {
        return incision_closure_methods;
    }
    public void setIncision_closure_methods(String incision_closure_methods) {
        this.incision_closure_methods = incision_closure_methods;
    }
 
    public Integer getExudate_amount() {
        return exudate_amount;
    }
    public void setExudate_amount(Integer exudate_amount) {
        this.exudate_amount = exudate_amount;
    }
  
    public Integer getPostop_day() {
        return postop_day;
    }
    public void setPostop_day(Integer postop_day) {
        this.postop_day = postop_day;
    }
   
    public String getPain_comments() {
        return pain_comments;
    }
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
  
    public Integer getClinical_path() {
        return clinical_path;
    }
    public void setClinical_path(Integer clinical_path) {
        this.clinical_path = clinical_path;
    }
    public Date getClinical_path_date() {
        return clinical_path_date;
    }
    public void setClinical_path_date(Date clinical_path_date) {
        this.clinical_path_date = clinical_path_date;
    }
   
    public String getPostop_management() {
        return postop_management;
    }
    public void setPostop_management(String postop_management) {
        this.postop_management = postop_management;
    }
    public Integer getPostop_days() {
        return postop_days;
    }
    public void setPostop_days(Integer postop_days) {
        this.postop_days = postop_days;
    }
  
      
}
