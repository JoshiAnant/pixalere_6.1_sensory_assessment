package com.pixalere.assessment.bean;
import com.pixalere.common.ValueObject;
import com.pixalere.common.ArrayValueObject;
import java.io.Serializable;
import com.pixalere.utils.PDate;
import java.util.Date;
import com.pixalere.common.ApplicationException;
import java.util.Collection;
/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Mar 1, 2010
 * Time: 10:03:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class AssessmentTreatmentVO    extends ValueObject implements Serializable {
    private Integer wound_id;
    private Integer alpha_id;
    private Integer patient_id;
    private Integer professional_id;
    private Integer assessment_id;
    private Integer id;
    private Integer wound_profile_type_id;
    private String adjunctive_other;
    private Integer cs_done;
    private Integer bath;
    private Integer nail_care;
    private Date cs_date;
    private Integer review_done;
    private Integer bandages_in;
    private Integer bandages_out;
    private Collection<ArrayValueObject> assessment_treatment_arrays;
    private Collection<AssessmentAntibioticVO> antibiotics;
    private WoundAssessmentLocationVO alphaLocation;
    public Integer getReview_done(){
        return review_done;
    }
    public void setReview_done(Integer review_done){
        this.review_done=review_done;
    }
    
    public Integer getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    public Integer getAssessment_id() {
        return assessment_id;
    }
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getAdjunctive_other() {
        return adjunctive_other;
    }
    public void setAdjunctive_other(String adjunctive_other) {
        this.adjunctive_other = adjunctive_other;
    }
 
    public Date getCs_date() {
        return cs_date;
    }
    public void setCs_date(Date cs_date) {
        this.cs_date = cs_date;
    }
   
    public Integer getBandages_out() {
        return bandages_out;
    }
    public void setBandages_out(Integer bandages_out) {
        this.bandages_out = bandages_out;
    }
    public Integer getBandages_in() {
        return bandages_in;
    }
    public void setBandages_in(Integer bandages_in) {
        this.bandages_in = bandages_in;
    }
    public WoundAssessmentLocationVO getAlphaLocation() {
        return alphaLocation;
    }
    public void setAlphaLocation(WoundAssessmentLocationVO alphaLocation) {
        this.alphaLocation = alphaLocation;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
    public String getMonth(Date dates)  {
        String m = "";
        PDate p = new PDate();
        try{
        m = p.getMonthNum(dates);//PDate expects seconds not milliseconds
        }catch(ApplicationException e){
            //System.out.println("Error parsing date");
        }
        return m;
    }
    public String getYear(Date dates)  {
        String m = "";
        PDate p = new PDate();
        
        m = p.getYear(dates);//PDate expects seconds not milliseconds
        
            //System.out.println("Error parsing date");
        
        return m;
    }
    public String getDay(Date dates)  {
        String m = "";
        PDate p = new PDate();
        try{
        m = p.getDay(dates);//PDate expects seconds not milliseconds
        }catch(ApplicationException e){
            //System.out.println("Error parsing date");
        }
        return m;
    }
    /**
     * @return the assessment_treatment_arrays
     */
    public Collection<ArrayValueObject> getAssessment_treatment_arrays() {
        return assessment_treatment_arrays;
    }
    /**
     * @param assessment_treatment_arrays the assessment_treatment_arrays to set
     */
    public void setAssessment_treatment_arrays(Collection<ArrayValueObject> assessment_treatment_arrays) {
        this.assessment_treatment_arrays = assessment_treatment_arrays;
    }
    /**
     * @return the antibiotics
     */
    public Collection<AssessmentAntibioticVO> getAntibiotics() {
        return antibiotics;
    }
    /**
     * @param antibiotics the antibiotics to set
     */
    public void setAntibiotics(Collection<AssessmentAntibioticVO> antibiotics) {
        this.antibiotics = antibiotics;
    }
    /**
     * @return the wound_id
     */
    public Integer getWound_id() {
        return wound_id;
    }
    /**
     * @param wound_id the wound_id to set
     */
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }
    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    /**
     * @return the cs_done
     */
    public Integer getCs_done() {
        return cs_done;
    }
    /**
     * @param cs_done the cs_done to set
     */
    public void setCs_done(Integer cs_done) {
        this.cs_done = cs_done;
    }

    /**
     * @return the bath
     */
    public Integer getBath() {
        return bath;
    }

    /**
     * @param bath the bath to set
     */
    public void setBath(Integer bath) {
        this.bath = bath;
    }

    /**
     * @return the nail_care
     */
    public Integer getNail_care() {
        return nail_care;
    }

    /**
     * @param nail_care the nail_care to set
     */
    public void setNail_care(Integer nail_care) {
        this.nail_care = nail_care;
    }
}
