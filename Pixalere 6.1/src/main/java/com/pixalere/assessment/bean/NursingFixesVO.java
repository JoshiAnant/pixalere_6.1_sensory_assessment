/*
 * Copyright (c) 2006 Your Corporation. All Rights Reserved.
 */
package com.pixalere.assessment.bean;
import java.io.Serializable;
import java.util.Date;
/**
 *     This is the NursingFixesVO  bean for the nursing_fixes  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.WoundAssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class NursingFixesVO extends com.pixalere.common.ValueObject implements Serializable{
private Integer patient_id;
private Integer wound_id;
private Integer row_id;
private Integer professional_id;
private String initials;
private String field;
private String error;
    private String delete_reason;
    public Date getCreated_on() {
        return created_on;
    }
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    private Integer id;
 private Date created_on;
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
public Integer getWound_id() {
        return wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public String getInitials() {
        return initials;
    }
    public void setInitials(String initials) {
        this.initials = initials;
    }
    public String getField() {
        return field;
    }
    public void setField(String field) {
        this.field = field;
    }
    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getRow_id() {
        return row_id;
    }
    public void setRow_id(Integer row_id) {
        this.row_id = row_id;
    }
    public String getDelete_reason() {
        return delete_reason;
    }
    public void setDelete_reason(String delete_reason) {
        this.delete_reason = delete_reason;
    }
  
}