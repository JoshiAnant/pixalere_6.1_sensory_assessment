package com.pixalere.assessment.bean;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import java.util.TimeZone;
import java.util.Vector;

import com.pixalere.utils.Common;

/**
 *     This is the AssessmentSkinVO  bean for the assessment_skin  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentVO
 *
 */
public class AssessmentSkinVO extends AbstractAssessmentVO {
    
    private String pain_comments;
    private Integer recurrent;
    private String laser;
    private Double skin_temperature;
    private Date wound_date;
    
    /** nullable persistent field */
    private Double length;
    private Double width;
    
    /** nullable persistent field */
    private Double depth;
    private Integer pain;
    

    
    /** nullable persistent field */
    private String site;
    
    private WoundAssessmentVO woundAssessment;
    
    public Integer getPain(){
        return pain;
    }
    public void setPain(Integer pain){
        this.pain=pain;
    }
    
    /**
     * @method getWound_date
     * @field wound_date - Integer
     * @return Returns the wound_date.
     */
    public Date getWound_date() {
        return wound_date;
    }
    
    
   
    /**
     * @method setWound_date
     * @field wound_date - Integer
     * @param wound_date
     *            The wound_date to set.
     */
    public void setWound_date(Date wound_date) {
        this.wound_date = wound_date;
    }
    
    /**
     * @method getRecurrent
     * @field recurrent - Integer
     * @return Returns the recurrent.
     */
    public Integer getRecurrent() {
        return recurrent;
    }
    
    /**
     * @method setRecurrent
     * @field recurrent - Integer
     * @param recurrent
     *            The recurrent to set.
     */
    public void setRecurrent(Integer recurrent) {
        this.recurrent = recurrent;
    }
    
  
    
    
    /** default constructor */
    public AssessmentSkinVO() {
    }
    
    
   
    
    public String getSite() {
        return this.site;
    }
    
    
    
    public void setSite(String site) {
        this.site = site;
    }
    
    public boolean ifExists(String serial, String item) {
        
        if (serial == null || serial.indexOf(item.replaceAll(" ","_")) == -1) {
            return false;
        } else {
            return true;
        }
    }
    
    
    public String toString(Integer item) {
        if (item.intValue() == -1 || item == null) {
            return "0";
        }
        return item + "";
    }
    public Vector getAgeofWoundDate() {
        Vector dates = new Vector();
        try {
            
            if (getWound_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar
                    .setTime(getWound_date());
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        }catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            dates = new Vector();
        }
        return dates;
    }
    public Vector getClosedDate() {
        Vector dates = new Vector();
        try {
            
            if (getClosed_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            
            calendar.setTime(getClosed_date());
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }
        catch (Exception e) {
            e.printStackTrace();
            dates = new Vector();
        }
        return dates;
    }
    

    public WoundAssessmentVO getWoundAssessment() {
        return woundAssessment;
    }
    
    public void setWoundAssessment(WoundAssessmentVO woundAssessment) {
        this.woundAssessment = woundAssessment;
    }
   
    public String getPain_comments() {
        return pain_comments;
    }
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }

    /**
     * @return the length
     */
    public Double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Double length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public Double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Double width) {
        this.width = width;
    }

    /**
     * @return the depth
     */
    public Double getDepth() {
        return depth;
    }

    /**
     * @param depth the depth to set
     */
    public void setDepth(Double depth) {
        this.depth = depth;
    }

    /**
     * @return the skin_temperature
     */
    public Double getSkin_temperature() {
        return skin_temperature;
    }

    /**
     * @param skin_temperature the skin_temperature to set
     */
    public void setSkin_temperature(Double skin_temperature) {
        this.skin_temperature = skin_temperature;
    }
   
}
