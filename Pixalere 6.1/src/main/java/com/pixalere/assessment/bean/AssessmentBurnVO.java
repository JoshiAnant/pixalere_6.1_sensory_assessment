package com.pixalere.assessment.bean;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.Vector;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.utils.Constants;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.TripleDes;
import com.pixalere.utils.Constants;
/**
 *     This is the AssessmentBurn class for all the assessment  (DB table) beans
 *     Refer to {@link com.pixalere.assessment.service.AssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentVO
 *
 *
 */
public class AssessmentBurnVO extends AbstractAssessmentVO {
    
    private Integer burn_status;
    private Date wound_date;
    private Integer pain;
    private String pain_comments;
    private String burn_wound;
    private String exudate_colour;
    private String exudate_amount;
    private String grafts;
    private String donors;
    private WoundAssessmentVO woundAssessment;
    
    /**
     * @method getWound_date
     * @field wound_date - Integer
     * @return Returns the wound_date.
     */
    public Date getWound_date() {
        return wound_date;
    }
    
    public Integer getBurn_status() {
        return burn_status;
    }
    public void setBurn_status(Integer burn_status) {
        this.burn_status = burn_status;
    }
    public Integer getPain(){
        return pain;
    }
    public void setPain(Integer pain){
        this.pain=pain;
    }
    public String getPain_comments() {
        return pain_comments;
    }
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
    public void setWound_date(Date wound_date) {
        this.wound_date = wound_date;
    }
    
    /** default constructor */
    public AssessmentBurnVO() {
    }
    
    public String getBurn_wound() {
        return this.burn_wound;
    }
    
    public void setBurn_wound(String burn_wound) {
        this.burn_wound = burn_wound;
    }
    
    public String getExudate_colour() {
        return this.exudate_colour;
    }
    public void setExudate_colour(String exudate_colour) {
        this.exudate_colour = exudate_colour;
    }
    public String getExudate_amount() {
        return this.exudate_amount;
    }
    
    public void setExudate_amount(String exudate_amount) {
        this.exudate_amount = exudate_amount;
    }
    
    public String getGrafts() {
        return this.grafts;
    }
    
    public void setGrafts(String grafts) {
        this.grafts = grafts;
    }
    
    public String getDonors() {
        return this.donors;
    }
    
    public void setDonors(String donors) {
        this.donors = donors;
    }
    
    public Vector getAgeofWoundDate() {
        Vector dates = new Vector();
        try {
            
            if (getWound_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar
                    .setTime(getWound_date());
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        }catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            dates = new Vector();
        }
        return dates;
    }
    public Vector getClosedDate() {
        Vector dates = new Vector();
        try {
            
            if (getClosed_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getClosed_date());
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }
        catch (Exception e) {
            e.printStackTrace();
            dates = new Vector();
        }
        return dates;
    }
    
    
    public WoundAssessmentVO getWoundAssessment() {
        return woundAssessment;
    }
    
    public void setWoundAssessment(WoundAssessmentVO woundAssessment) {
        this.woundAssessment = woundAssessment;
    }
}
