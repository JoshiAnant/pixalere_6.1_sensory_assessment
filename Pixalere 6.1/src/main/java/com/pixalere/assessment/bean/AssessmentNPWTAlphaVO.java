/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.assessment.bean;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import com.pixalere.utils.PDate;
import java.io.Serializable;
import java.util.Date;
import java.util.Collection;
/**
 *
 * @author travis
 */
public class AssessmentNPWTAlphaVO extends ValueObject implements Serializable {
    private Integer id;
    private Integer alpha_id;
    private Integer assessment_npwt_id;
    public AssessmentNPWTAlphaVO() {
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the alpha_id
     */
    public Integer getAlpha_id() {
        return alpha_id;
    }
    /**
     * @param alpha_id the alpha_id to set
     */
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    /**
     * @return the assessment_npwt_id
     */
    public Integer getAssessment_npwt_id() {
        return assessment_npwt_id;
    }
    /**
     * @param assessment_npwt_id the assessment_npwt_id to set
     */
    public void setAssessment_npwt_id(Integer assessment_npwt_id) {
        this.assessment_npwt_id = assessment_npwt_id;
    }
}
