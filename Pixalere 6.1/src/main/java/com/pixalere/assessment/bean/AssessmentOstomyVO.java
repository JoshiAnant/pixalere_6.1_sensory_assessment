package com.pixalere.assessment.bean;
import java.util.Collection;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Vector;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import java.util.Hashtable;
import com.pixalere.guibeans.LocationDepthVO;
import org.apache.log4j.Logger;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.*;
/**
 *     This is the AssessmentOstomy bean for the assessment_ostomy  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentCommentsServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundProfileTypeVO
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *
 */
public class AssessmentOstomyVO  extends AbstractAssessmentVO {
    
    static private Logger log = Logger.getLogger(AssessmentOstomyVO.class);
    private String drainage_comments;
    private Integer postop_days;
    private Integer length_mm;
    private Integer width_mm;
    
    private Integer circumference_mm;
    private Integer nutritional_status;
    private Integer num_mucocutaneous_margin;
    private String mucocutaneous_margin;
    private String mucocutaneous_margin_comments;
    private String perifistula_skin;
    private Date date_surgery;
    private Integer clinical_path;
    private Date clinical_path_date;
 private Collection<AssessmentMucocSeperationsVO> seperations;
    private String self_care_progress;
    private String self_care_progress_comments;
    private Integer shape;
    private String pouching;
    private String urine_colour;
    private String urine_type;
    private Integer urine_quantity;
    private String stool_colour;
    private String stool_consistency;
    private Integer stool_quantity;
    private String nutritional_status_other;
    private Integer flange_pouch;
    private String flange_pouch_comments;
    private String periostomy_skin;
    private Integer construction;
    private String drainage;
    private String profile;
    private String ostomy_other;
    private Integer goal_of_ostomy;
    private String ostomy_etiology;
    private String ostomy_etiology_other;
    private Integer devices;
    private String appearance;
    private String contour;
    
    private String training_comments;
    
    public AssessmentOstomyVO() {
    }
    
    
    
    
    
    public String getOstomy_other() {
        return ostomy_other;
    }
    
    public void setOstomy_other(String ostomy_other) {
        this.ostomy_other = ostomy_other;
    }
    
    
    
    public Integer getShape() {
        return shape;
    }
    
    public void setShape(Integer shape) {
        this.shape = shape;
    }
    
    
    public String getUrine_colour() {
        return urine_colour;
    }
    
    public void setUrine_colour(String urine_colour) {
        this.urine_colour = urine_colour;
    }
    
    public String getUrine_type() {
        return urine_type;
    }
    
    public void setUrine_type(String urine_type) {
        this.urine_type = urine_type;
    }
    
    public Integer getUrine_quantity() {
        return urine_quantity;
    }
    
    public void setUrine_quantity(Integer urine_quantity) {
        this.urine_quantity = urine_quantity;
    }
    
  
    
    
    
    public String getStool_colour() {
        return stool_colour;
    }
    
    public void setStool_colour(String stool_colour) {
        this.stool_colour = stool_colour;
    }
    
    public String getStool_consistency() {
        return stool_consistency;
    }
    
    public void setStool_consistency(String stool_consistency) {
        this.stool_consistency = stool_consistency;
    }
    
    public Integer getStool_quantity() {
        return stool_quantity;
    }
    
    public void setStool_quantity(Integer stool_quantity) {
        this.stool_quantity = stool_quantity;
    }
    
    public String getStool_quantity_other() {
        return getNutritional_status_other();
    }
    
    public void setStool_quantity_other(String stool_quantity_other) {
        this.setNutritional_status_other(stool_quantity_other);
    }
    
    
    
    public String getTraining_comments() {
        return training_comments;
    }
    
    public void setTraining_comments(String training_comments) {
        this.training_comments = training_comments;
    }
    
    
    
    public Date getDate_surgery() {
        return date_surgery;
    }
    public void setDate_surgery(Date date_surgery) {
        this.date_surgery=date_surgery;
    }
    
    
    public Vector getClinicalPathDate() {
        Vector<String> dates = new Vector<String>();
        try {
            
            if (clinical_path_date == null) {
                return new Vector();
            }
            
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(clinical_path_date );
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getDateSurgery() {
        Vector<String> dates = new Vector<String>();
        try {
            
            if (date_surgery == null) {
                return new Vector();
            }
            
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date_surgery );
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getClosedDate() {
        Vector<String> dates = new Vector<String>();
        try {
            
            if (getClosed_date() == null) {
                return new Vector();
            }
            
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date(Long.parseLong(getClosed_date()+"") * (long) 1000));
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex){
            dates=new Vector();//empty date, return empty vector.
        }catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    
    
    public Integer getGoal_of_ostomy() {
        return goal_of_ostomy;
    }
    
    public void setGoal_of_ostomy(Integer goal_of_ostomy) {
        this.goal_of_ostomy = goal_of_ostomy;
    }
    
    public String getOstomy_etiology_other() {
        return ostomy_etiology_other;
    }
    public void setOstomy_etiology_other(String ostomy_etiology_other) {
        this.ostomy_etiology_other = ostomy_etiology_other;
    }
    
    
    public String getOstomy_etiology() {
        return ostomy_etiology;
    }
    public void setOstomy_etiology(String ostomy_etiology) {
        this.ostomy_etiology = ostomy_etiology;
    }
    
    
    
    public Integer getDevices() {
        return devices;
    }
    
    public void setDevices(Integer devices) {
        this.devices = devices;
    }
    
    
    
    public String getAppearance() {
        return appearance;
    }
    
    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }
    
    public String getContour() {
        return contour;
    }
    
    public void setContour(String contour) {
        this.contour = contour;
    }
    
    
    public Integer getClinical_path() {
        return clinical_path;
    }
    
    public void setClinical_path(Integer clinical_path) {
        this.clinical_path = clinical_path;
    }
    
    public Date getClinical_path_date() {
        return clinical_path_date;
    }
    
    public void setClinical_path_date(Date clinical_path_date) {
        this.clinical_path_date = clinical_path_date;
    }
    
    
    
    
    
    public String getSelf_care_progress() {
        return self_care_progress;
    }
    
    public void setSelf_care_progress(String self_care_progress) {
        this.self_care_progress = self_care_progress;
    }
    public Integer getFlange_pouch() {
        return flange_pouch;
    }
    
    public void setFlange_pouch(Integer flange_pouch) {
        this.flange_pouch = flange_pouch;
    }
    public String getFlange_pouch_comments() {
        return flange_pouch_comments;
    }
    
    public void setFlange_pouch_comments(String flange_pouch_comments) {
        this.flange_pouch_comments = flange_pouch_comments;
    }
    
    public String getProfile() {
        return profile;
    }
    
    public void setProfile(String profile) {
        this.profile = profile;
    }
    
    public String getPouching() {
        return pouching;
    }
    
    public void setPouching(String pouching) {
        this.pouching = pouching;
    }
    
    public String getPeriostomy_skin() {
        return periostomy_skin;
    }
    
    public void setPeriostomy_skin(String periostomy_skin) {
        this.periostomy_skin = periostomy_skin;
    }
    public Integer getConstruction() {
        return construction;
    }
    public void setConstruction(Integer construction) {
        this.construction = construction;
    }
    public String getDrainage() {
        return drainage;
    }
    public void setDrainage(String drainage) {
        this.drainage = drainage;
    }
    public String getSelf_care_progress_comments() {
        return self_care_progress_comments;
    }
    public void setSelf_care_progress_comments(String self_care_progress_comments) {
        this.self_care_progress_comments = self_care_progress_comments;
    }

    
    public String getMucocutaneous_margin() {
        return mucocutaneous_margin;
    }
    public void setMucocutaneous_margin(String mucocutaneous_margin) {
        this.mucocutaneous_margin = mucocutaneous_margin;
    }
    public String getMucocutaneous_margin_comments() {
        return mucocutaneous_margin_comments;
    }
    public void setMucocutaneous_margin_comments(String mucocutaneous_margin_comments) {
        this.mucocutaneous_margin_comments = mucocutaneous_margin_comments;
    }
    public String getPerifistula_skin() {
        return perifistula_skin;
    }
    public void setPerifistula_skin(String perifistula_skin) {
        this.perifistula_skin = perifistula_skin;
    }
    public Integer getNutritional_status() {
        return nutritional_status;
    }
    public void setNutritional_status(Integer nutritional_status) {
        this.nutritional_status = nutritional_status;
    }
    public Integer getCircumference_mm() {
        return circumference_mm;
    }
    public void setCircumference_mm(Integer circumference_mm) {
        this.circumference_mm = circumference_mm;
    }
    public String getDrainage_comments() {
        return drainage_comments;
    }
    public void setDrainage_comments(String drainage_comments) {
        this.drainage_comments = drainage_comments;
    }
    public Integer getLength_mm() {
        return length_mm;
    }
    public void setLength_mm(Integer length_mm) {
        this.length_mm = length_mm;
    }
    public Integer getWidth_mm() {
        return width_mm;
    }
    public void setWidth_mm(Integer width_mm) {
        this.width_mm = width_mm;
    }
    public Integer getPostop_days() {
        return postop_days;
    }
    public void setPostop_days(Integer postop_days) {
        this.postop_days = postop_days;
    }
   
    
  
    public String getNutritional_status_other() {
        return nutritional_status_other;
    }
    public void setNutritional_status_other(String nutritional_status_other) {
        this.nutritional_status_other = nutritional_status_other;
    }
    
   public Collection<AssessmentMucocSeperationsVO> getSeperations() {
       return seperations;
   }
   public void setSeperations(Collection<AssessmentMucocSeperationsVO> seperations) {
       this.seperations = seperations;
   }
}
