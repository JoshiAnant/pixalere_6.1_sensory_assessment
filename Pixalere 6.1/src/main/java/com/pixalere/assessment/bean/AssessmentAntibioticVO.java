package com.pixalere.assessment.bean;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.ValueObject;
import java.text.SimpleDateFormat;
import java.io.Serializable;
import java.util.Date;
import com.pixalere.utils.Common;
/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Mar 15, 2010
 * Time: 3:08:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class AssessmentAntibioticVO extends ValueObject implements Serializable {
    private String antibiotic;
    private Date start_date;
    private Date end_date;
    private Integer id;
    private Integer assessment_treatment_id;
    private WoundAssessmentLocationVO alpha;
    public AssessmentAntibioticVO() {
    }
    public AssessmentAntibioticVO(String antibiotic, Date start_date, Date end_date, Integer assessment_treatment_id) {
        this.assessment_treatment_id = assessment_treatment_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.antibiotic = antibiotic;
    }
    public String getAntibiotic() {
        return antibiotic;
    }
    public void setAntibiotic(String antibiotic) {
        this.antibiotic = antibiotic;
    }
    public Date getStart_date() {
        return start_date;
    }
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }
    public Date getEnd_date() {
        return end_date;
    }
    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getAssessment_treatment_id() {
        return assessment_treatment_id;
    }
    public void setAssessment_treatment_id(Integer assessment_treatment_id) {
        this.assessment_treatment_id = assessment_treatment_id;
    }
    public String getPrintable(String locale) {
        SimpleDateFormat sf = new SimpleDateFormat("d/M/yyyy");
        String startdate = "";
        String enddate = "";
        if (getStart_date() != null && !getStart_date().equals("")) {
            startdate = sf.format(getStart_date());
        }
        if (getEnd_date() != null && !getEnd_date().equals("")) {
            enddate = sf.format(getEnd_date());
        }
        String antibiotic =  getAntibiotic() + " : " + startdate + " to " + enddate;
        return antibiotic;
    }
}
