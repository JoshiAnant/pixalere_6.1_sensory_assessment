package com.pixalere.assessment.bean;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.LookupVO;
import java.io.Serializable;
/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Feb 23, 2010
 * Time: 10:06:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class AssessmentWoundBedVO   extends ValueObject implements Serializable {
    private Integer id;
    private Integer alpha_id;
    private Integer assessment_wound_id;
    private Integer wound_bed;
    private Integer percentage;
    private LookupVO lookup;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getAssessment_wound_id() {
        return assessment_wound_id;
    }
    public void setAssessment_wound_id(Integer assessment_wound_id) {
        this.assessment_wound_id = assessment_wound_id;
    }
    public Integer getWound_bed() {
        return wound_bed;
    }
    public void setWound_bed(Integer wound_bed) {
        this.wound_bed = wound_bed;
    }
    public Integer getPercentage() {
        return percentage;
    }
    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
    public Integer getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    /**
     * @return the lookup
     */
    public LookupVO getLookup() {
        return lookup;
    }
    /**
     * @param lookup the lookup to set
     */
    public void setLookup(LookupVO lookup) {
        this.lookup = lookup;
    }
}
