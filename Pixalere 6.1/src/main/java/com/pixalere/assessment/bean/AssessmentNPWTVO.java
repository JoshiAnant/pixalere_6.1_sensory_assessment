/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.assessment.bean;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import com.pixalere.utils.PDate;
import java.io.Serializable;
import java.util.Date;
import java.util.Collection;
/**
 *
 * @author travis
 */
public class AssessmentNPWTVO extends ValueObject implements Serializable {
    
            private Integer id;
            private Integer secondary;
            private Integer vendor_name;
            private Integer machine_acquirement;
            private Integer patient_id;
            private Integer professional_id;
            private Integer wound_id;
            private Integer assessment_id;
            private Integer goal_of_therapy;
            private Date vac_start_date;
            private Date vac_end_date;
            private Integer pressure_reading;
            private Integer therapy_setting;
            private Integer initiated_by;
            private String serial_num;
            private String kci_num;
            private Integer np_connector;
            private Collection<AssessmentNPWTAlphaVO> alphas;
            private Integer reason_for_ending;
            public AssessmentNPWTVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }
    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    /**
     * @return the wound_id
     */
    public Integer getWound_id() {
        return wound_id;
    }
    /**
     * @param wound_id the wound_id to set
     */
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    /**
     * @return the assessment_id
     */
    public Integer getAssessment_id() {
        return assessment_id;
    }
    /**
     * @param assessment_id the assessment_id to set
     */
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    /**
     * @return the vac_start_date
     */
    public Date getVac_start_date() {
        return vac_start_date;
    }
    /**
     * @param vac_start_date the vac_start_date to set
     */
    public void setVac_start_date(Date vac_start_date) {
        this.vac_start_date = vac_start_date;
    }
    /**
     * @return the vac_end_date
     */
    public Date getVac_end_date() {
        return vac_end_date;
    }
    /**
     * @param vac_end_date the vac_end_date to set
     */
    public void setVac_end_date(Date vac_end_date) {
        this.vac_end_date = vac_end_date;
    }
    /**
     * @return the pressure_reading
     */
    public Integer getPressure_reading() {
        return pressure_reading;
    }
    /**
     * @param pressure_reading the pressure_reading to set
     */
    public void setPressure_reading(Integer pressure_reading) {
        this.pressure_reading = pressure_reading;
    }
    /**
     * @return the therapy_setting
     */
    public Integer getTherapy_setting() {
        return therapy_setting;
    }
    /**
     * @param therapy_setting the therapy_setting to set
     */
    public void setTherapy_setting(Integer therapy_setting) {
        this.therapy_setting = therapy_setting;
    }
    /**
     * @return the initiated_by
     */
    public Integer getInitiated_by() {
        return initiated_by;
    }
    /**
     * @param initiated_by the initiated_by to set
     */
    public void setInitiated_by(Integer initiated_by) {
        this.initiated_by = initiated_by;
    }
    /**
     * @return the serial_num
     */
    public String getSerial_num() {
        return serial_num;
    }
    /**
     * @param serial_num the serial_num to set
     */
    public void setSerial_num(String serial_num) {
        this.serial_num = serial_num;
    }
    /**
     * @return the kci_num
     */
    public String getKci_num() {
        return kci_num;
    }
    /**
     * @param kci_num the kci_num to set
     */
    public void setKci_num(String kci_num) {
        this.kci_num = kci_num;
    }
    /**
     * @return the alphas
     */
    public Collection<AssessmentNPWTAlphaVO> getAlphas() {
        return alphas;
    }
    /**
     * @param alphas the alphas to set
     */
    public void setAlphas(Collection<AssessmentNPWTAlphaVO> alphas) {
        this.alphas = alphas;
    }
    /**
     * @return the reason_for_ending
     */
    public Integer getReason_for_ending() {
        return reason_for_ending;
    }
    /**
     * @param reason_for_ending the reason_for_ending to set
     */
    public void setReason_for_ending(Integer reason_for_ending) {
        this.reason_for_ending = reason_for_ending;
    }
    /**
     * @return the goal_of_therapy
     */
    public Integer getGoal_of_therapy() {
        return goal_of_therapy;
    }
    /**
     * @param goal_of_therapy the goal_of_therapy to set
     */
    public void setGoal_of_therapy(Integer goal_of_therapy) {
        this.goal_of_therapy = goal_of_therapy;
    }
    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    /**
     * @return the secondary
     */
    public Integer getSecondary() {
        return secondary;
    }
    /**
     * @param secondary the secondary to set
     */
    public void setSecondary(Integer secondary) {
        this.secondary = secondary;
    }
          public String getMonth(Date dates)  {
        String m = "";
        PDate p = new PDate();
        try{
        m = p.getMonthNum(dates);//PDate expects seconds not milliseconds
        }catch(ApplicationException e){
            //logs.error("Error Parsing Message"+e.getMessage());
        }
        return m;
    }
    public String getYear(Date dates)  {
        String m = "";
        PDate p = new PDate();
        
        m = p.getYear(dates);//PDate expects seconds not milliseconds
        
            //System.out.println("Error parsing date");
        
        return m;
    }
    public String getDay(Date dates)  {
        String m = "";
        PDate p = new PDate();
        try{
        m = p.getDay(dates);//PDate expects seconds not milliseconds
        }catch(ApplicationException e){
            //System.out.println("Error parsing date");
        }
        return m;
    }   
    /**
     * @return the np_connector
     */
    public Integer getNp_connector() {
        return np_connector;
    }
    /**
     * @param np_connector the np_connector to set
     */
    public void setNp_connector(Integer np_connector) {
        this.np_connector = np_connector;
    }

    /**
     * @return the vendor_name
     */
    public Integer getVendor_name() {
        return vendor_name;
    }

    /**
     * @param vendor_name the vendor_name to set
     */
    public void setVendor_name(Integer vendor_name) {
        this.vendor_name = vendor_name;
    }

    /**
     * @return the machine_acquirement
     */
    public Integer getMachine_acquirement() {
        return machine_acquirement;
    }

    /**
     * @param machine_acquirement the machine_acquirement to set
     */
    public void setMachine_acquirement(Integer machine_acquirement) {
        this.machine_acquirement = machine_acquirement;
    }
   
}
