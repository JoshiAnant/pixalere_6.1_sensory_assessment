/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.assessment.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Vector;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.PDate;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Collection;
import com.pixalere.utils.Common;
import java.util.Date;
/**
 * This is the DressingChangeFrequency bean for the dressing_change_frequency
 * (DB table) Refer to {@link com.pixalere.assessment.service.NursingCarePlanServiceImpl
 * } for the calls which utilize this bean.
 *
 * @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentLocationVO
 * @has 1..1 Has 1..1 com.pixalere.common.bean.LookupVO
 * @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *
 */
public class DressingChangeFrequencyVO extends ValueObject implements Serializable {
    private Integer id;
    private Integer dcf;
    private Integer patient_id;
    private Integer wound_id;
    private Date created_on;
    private Integer professional_id;
    private String user_signature;
    private Integer active;
    private Integer alpha_id;
    private ProfessionalVO professional;
    private Integer wound_profile_type_id;
    private Integer assessment_id;
    private WoundAssessmentLocationVO alphaLocation;
    private LookupVO dressingChangeFrequency;
    public DressingChangeFrequencyVO() {
    }
    public WoundAssessmentLocationVO getAlphaLocation() {
        return alphaLocation;
    }
    public void setAlphaLocation(WoundAssessmentLocationVO alphaLocation) {
        this.alphaLocation = alphaLocation;
    }
    public Integer getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getDcf() {
        return dcf;
    }
    public void setDcf(Integer dcf) {
        this.dcf = dcf;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getWound_id() {
        return wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public Integer getAssessment_id() {
        return assessment_id;
    }
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
    public LookupVO getDressingChangeFrequency() {
        return dressingChangeFrequency;
    }
    public void setDressingChangeFrequency(LookupVO dressingChangeFrequency) {
        this.dressingChangeFrequency = dressingChangeFrequency;
    }
    public String getAlpha(String locale) {
        if (alphaLocation != null) {
            String display_alpha = alphaLocation.getAlpha();
            if (display_alpha != null) {
                if (display_alpha.length() > 0) {
                    if (display_alpha.length() == 1) {
                        display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.woundcare", locale) + " " + display_alpha;
                    } else if (display_alpha.indexOf("ostu") != -1) {
                        display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.urinalstoma", locale);
                    } else if (display_alpha.indexOf("ostf") != -1) {
                        display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.fecalstoma", locale);
                    } else if (display_alpha.indexOf("td") != -1) {
                        display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.drain", locale) + " " + display_alpha.substring(2);
                    } else if (display_alpha.indexOf("s") == 0) {
                        display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.skin", locale) + " " + display_alpha.substring(1);
                    } else if (display_alpha.indexOf("tag") != -1) {
                        display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.incision_tag", locale) + " " + display_alpha.substring(3);
                    } else if (display_alpha.indexOf("burn") != -1) {
                        display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.burn", locale);
                    }
                }
            }
            return display_alpha;
        } else {
            return "";
        }
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
}