package com.pixalere.assessment.bean;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.ProductsVO;
/**
 *     This is the AssessmentProduct  bean for the assessment_product  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentVO
 *
 */
public class AssessmentProductVO extends ValueObject implements Serializable {
    private Integer patient_id;
    private Integer wound_id;
    private Integer assessment_id;
    private Integer wound_assessment_id;
    private Integer ostomy_assessment_id;
    private Integer incision_assessment_id;
    private Integer drain_assessment_id;
    private Integer burn_assessment_id;
    private Integer skin_assessment_id;
    private Integer product_id;
    private Integer alpha_id;
    private String quantity;
    private String other;
    private ProductsVO product;
    private Integer id;
    private Integer active;
    /** Creates a new instance of AssessmentProductVO */
    public AssessmentProductVO() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getWound_id() {
        return wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public Integer getAssessment_id() {
        return assessment_id;
    }
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    
    public Integer getProduct_id() {
        return product_id;
    }
    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }
    public Integer getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    public String getQuantity() {
        return quantity;
    }
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    public String getOther() {
        return other;
    }
    public void setOther(String other) {
        this.other = other;
    }
    public void setProduct(ProductsVO product){
        this.product=product;
    }
    public ProductsVO getProduct(){
        return product;
    }
    public Integer getWound_assessment_id() {
        return wound_assessment_id;
    }
    public void setWound_assessment_id(Integer wound_assessment_id) {
        this.wound_assessment_id = wound_assessment_id;
    }
    public Integer getOstomy_assessment_id() {
        return ostomy_assessment_id;
    }
    public void setOstomy_assessment_id(Integer ostomy_assessment_id) {
        this.ostomy_assessment_id = ostomy_assessment_id;
    }
    public Integer getIncision_assessment_id() {
        return incision_assessment_id;
    }
    public void setIncision_assessment_id(Integer incision_assessment_id) {
        this.incision_assessment_id = incision_assessment_id;
    }
    public Integer getDrain_assessment_id() {
        return drain_assessment_id;
    }
    public void setDrain_assessment_id(Integer drain_assessment_id) {
        this.drain_assessment_id = drain_assessment_id;
    }
    public Integer getBurn_assessment_id() {
        return burn_assessment_id;
    }
    public void setBurn_assessment_id(Integer burn_assessment_id) {
        this.burn_assessment_id = burn_assessment_id;
    }
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    /**
     * @return the skin_assessment_id
     */
    public Integer getSkin_assessment_id() {
        return skin_assessment_id;
    }
    /**
     * @param skin_assessment_id the skin_assessment_id to set
     */
    public void setSkin_assessment_id(Integer skin_assessment_id) {
        this.skin_assessment_id = skin_assessment_id;
    }
}
