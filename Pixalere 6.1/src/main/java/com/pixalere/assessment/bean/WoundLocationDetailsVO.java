package com.pixalere.assessment.bean;
/**
 *     This is the WoundLocationDetails  bean for the wound_location_details  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
import com.pixalere.common.ValueObject;
import java.io.Serializable;
public class WoundLocationDetailsVO extends ValueObject implements Serializable {
    private Integer id;
    private String image;
    private Integer alpha_id;
    private Integer deleted;
    private Integer x;
    private Integer y;
    public WoundLocationDetailsVO() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public Integer getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    public Integer getDeleted() {
        return this.deleted;
    }
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    /**
     * @return the x
     */
    public Integer getX() {
        return x;
    }
    /**
     * @param x the x to set
     */
    public void setX(Integer x) {
        this.x = x;
    }
    /**
     * @return the y
     */
    public Integer getY() {
        return y;
    }
    /**
     * @param y the y to set
     */
    public void setY(Integer y) {
        this.y = y;
    }
 
    
}