package com.pixalere.assessment.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Vector;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.PDate;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Collection;
import java.util.Date;
 /**
 *     This is the NursingCarePlanVO  bean for the nursing_care_plan  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.NursingCarePlanServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *
 */
public class NursingCarePlanVO extends ValueObject implements Serializable {
    private Integer id;
    private String body;
    private Integer patient_id;
    private Integer wound_id;
    private Date created_on;
    private Integer professional_id;
    private String user_signature;
    private Integer active;
    private ProfessionalVO professional;
    private Integer wound_profile_type_id;
    private Integer assessment_id;
    private Integer visit_frequency;
    private LookupVO visitFrequency;
     private Integer deleted;
    private String deleted_reason;
     private String delete_signature;
     private Integer as_needed;
     private Date deleted_on;
    public NursingCarePlanVO(){
    
        
    }
   public Integer getVisit_frequency() {
        return visit_frequency;
    }
    public void setVisit_frequency(Integer visit_frequency) {
        this.visit_frequency = visit_frequency;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getBody() {
        return body;
    }
    public void setBody(String body) {
        this.body = body;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getWound_id() {
        return wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public Integer getAssessment_id() {
        return assessment_id;
    }
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
    public LookupVO getVisitFrequency() {
        return visitFrequency;
    }
    public void setVisitFrequency(LookupVO visitFrequency) {
        this.visitFrequency = visitFrequency;
    }
     public Integer getDeleted() {
         return deleted;
     }
     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }
     public String getDeleted_reason() {
         return deleted_reason;
     }
     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }
     public String getDelete_signature() {
         return delete_signature;
     }
     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    /**
     * @return the as_needed
     */
    public Integer getAs_needed() {
        return as_needed;
    }
    /**
     * @param as_needed the as_needed to set
     */
    public void setAs_needed(Integer as_needed) {
        this.as_needed = as_needed;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }  private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
 }