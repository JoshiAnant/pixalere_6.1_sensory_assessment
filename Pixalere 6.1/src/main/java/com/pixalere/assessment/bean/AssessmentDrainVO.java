package com.pixalere.assessment.bean;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Vector;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import java.util.Hashtable;
import com.pixalere.utils.Constants;
import org.apache.log4j.Logger;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.*;
/**
 *     This is the AssessmentDrain  bean for the assessment_drain  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundProfileTypeVO
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *
 */
public class AssessmentDrainVO extends AbstractAssessmentVO {
    static private Logger log = Logger.getLogger(AssessmentDrainVO.class);
    private Integer type_of_drain;
    private String type_of_drain_other;
    private Integer sutured;
    private String drain_site;
    private Integer pain;
    private String pain_comments;
    private String peri_drain_area;
    private Integer drainage_amount_mls1;
    private Integer drainage_amount_hrs1;
    private Integer drainage_amount1_start;
    private Integer drainage_amount1_end;
    private String drain_characteristics1;
    private String drain_odour1;
    private Date additional_days_date1;
    private Integer drainage_amount_mls2;
    private Integer drainage_amount_hrs2;
    private Integer drainage_amount2_start;
    private Integer drainage_amount2_end;
    private String drain_characteristics2;
    private String drain_odour2;
    private Date additional_days_date2;
    private Integer drainage_amount_mls3;
    private Integer drainage_amount_hrs3;
    private Integer drainage_amount3_start;
    private Integer drainage_amount3_end;
    private String drain_characteristics3;
    private String drain_odour3;
    private Date additional_days_date3;
    private Integer drainage_amount_mls4;
    private Integer drainage_amount_hrs4;
    private Integer drainage_amount4_start;
    private Integer drainage_amount4_end;
    private String drain_characteristics4;
    private String drain_odour4;
    private Date additional_days_date4;
    private Integer drainage_amount_mls5;
    private Integer drainage_amount_hrs5;
    private Integer drainage_amount5_start;
    private Integer drainage_amount5_end;
    private String drain_characteristics5;
    private String drain_odour5;
    private Date additional_days_date5;
    private Integer drainage_num;
    private Integer discharge_reason;
    private Date tubes_changed_date;
    private Integer drain_removed_intact;
    public AssessmentDrainVO() {
    }
    
    public Vector getDrainRemovedDate() {
        Vector<String> dates = new Vector<String>();
        try {
            if (getClosed_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getClosed_date());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getAdditionalDaysDate1() {
        Vector<String> dates = new Vector<String>();
        try {
            if (getAdditional_days_date1() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getAdditional_days_date1());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getTubesChangedDate() {
        Vector<String> dates = new Vector<String>();
        try {
            if (getTubes_changed_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getTubes_changed_date());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getAdditionalDaysDate2() {
        Vector<String> dates = new Vector<String>();
        try {
            if (getAdditional_days_date2() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getAdditional_days_date2());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getAdditionalDaysDate3() {
        Vector<String> dates = new Vector<String>();
        try {
            if (getAdditional_days_date3() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getAdditional_days_date3());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getAdditionalDaysDate4() {
        Vector<String> dates = new Vector<String>();
        try {
            if (getAdditional_days_date4() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getAdditional_days_date4());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getAdditionalDaysDate5() {
        Vector<String> dates = new Vector<String>();
        try {
            if (getAdditional_days_date5() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getAdditional_days_date5());
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Integer getType_of_drain() {
        return type_of_drain;
    }
    public void setType_of_drain(Integer type_of_drain) {
        this.type_of_drain = type_of_drain;
    }
    public Integer getSutured() {
        return sutured;
    }
    public void setSutured(Integer sutured) {
        this.sutured = sutured;
    }
    public String getDrain_site() {
        return drain_site;
    }
    public void setDrain_site(String drain_site) {
        this.drain_site = drain_site;
    }
    public Integer getDrainage_amount_mls1() {
        return drainage_amount_mls1;
    }
    public void setDrainage_amount_mls1(Integer drainage_amount_mls1) {
        this.drainage_amount_mls1 = drainage_amount_mls1;
    }
    public Integer getDrainage_amount_hrs1() {
        return drainage_amount_hrs1;
    }
    public void setDrainage_amount_hrs1(Integer drainage_amount_hrs1) {
        this.drainage_amount_hrs1 = drainage_amount_hrs1;
    }
    public Integer getDrainage_amount1_start() {
        return drainage_amount1_start;
    }
    public void setDrainage_amount1_start(Integer drainage_amount_start1) {
        this.drainage_amount1_start = drainage_amount_start1;
    }
    public Integer getDrainage_amount1_end() {
        return drainage_amount1_end;
    }
    public void setDrainage_amount1_end(Integer drainage_amount_end1) {
        this.drainage_amount1_end = drainage_amount_end1;
    }
    public String getDrain_characteristics1() {
        return drain_characteristics1;
    }
    public void setDrain_characteristics1(String characteristics1) {
        this.drain_characteristics1 = characteristics1;
    }
    public String getDrain_odour1() {
        return drain_odour1;
    }
    public void setDrain_odour1(String odour1) {
        this.drain_odour1 = odour1;
    }
    public Integer getDrainage_amount_mls2() {
        return drainage_amount_mls2;
    }
    public void setDrainage_amount_mls2(Integer drainage_amount_mls2) {
        this.drainage_amount_mls2 = drainage_amount_mls2;
    }
    public Integer getDrainage_amount_hrs2() {
        return drainage_amount_hrs2;
    }
    public void setDrainage_amount_hrs2(Integer drainage_amount_hrs2) {
        this.drainage_amount_hrs2 = drainage_amount_hrs2;
    }
    public Integer getDrainage_amount2_start() {
        return drainage_amount2_start;
    }
    public void setDrainage_amount2_start(Integer drainage_amount_start2) {
        this.drainage_amount2_start = drainage_amount_start2;
    }
    public Integer getDrainage_amount2_end() {
        return drainage_amount2_end;
    }
    public void setDrainage_amount2_end(Integer drainage_amount_end2) {
        this.drainage_amount2_end = drainage_amount_end2;
    }
    public String getDrain_characteristics2() {
        return drain_characteristics2;
    }
    public void setDrain_characteristics2(String characteristics2) {
        this.drain_characteristics2 = characteristics2;
    }
    public String getDrain_odour2() {
        return drain_odour2;
    }
    public void setDrain_odour2(String odour2) {
        this.drain_odour2 = odour2;
    }
    public Integer getDrainage_amount_mls3() {
        return drainage_amount_mls3;
    }
    public void setDrainage_amount_mls3(Integer drainage_amount_mls3) {
        this.drainage_amount_mls3 = drainage_amount_mls3;
    }
    public Integer getDrainage_amount_hrs3() {
        return drainage_amount_hrs3;
    }
    public void setDrainage_amount_hrs3(Integer drainage_amount_hrs3) {
        this.drainage_amount_hrs3 = drainage_amount_hrs3;
    }
    public Integer getDrainage_amount3_start() {
        return drainage_amount3_start;
    }
    public void setDrainage_amount3_start(Integer drainage_amount_start3) {
        this.drainage_amount3_start = drainage_amount_start3;
    }
    public Integer getDrainage_amount3_end() {
        return drainage_amount3_end;
    }
    public void setDrainage_amount3_end(Integer drainage_amount_end3) {
        this.drainage_amount3_end = drainage_amount_end3;
    }
    public String getDrain_characteristics3() {
        return drain_characteristics3;
    }
    public void setDrain_characteristics3(String characteristics3) {
        this.drain_characteristics3 = characteristics3;
    }
    public String getDrain_odour3() {
        return drain_odour3;
    }
    public void setDrain_odour3(String odour3) {
        this.drain_odour3 = odour3;
    }
    public Integer getDrainage_amount_mls4() {
        return drainage_amount_mls4;
    }
    public void setDrainage_amount_mls4(Integer drainage_amount_mls4) {
        this.drainage_amount_mls4 = drainage_amount_mls4;
    }
    public Integer getDrainage_amount_hrs4() {
        return drainage_amount_hrs4;
    }
    public void setDrainage_amount_hrs4(Integer drainage_amount_hrs4) {
        this.drainage_amount_hrs4 = drainage_amount_hrs4;
    }
    public Integer getDrainage_amount4_start() {
        return drainage_amount4_start;
    }
    public void setDrainage_amount4_start(Integer drainage_amount_start4) {
        this.drainage_amount4_start = drainage_amount_start4;
    }
    public Integer getDrainage_amount4_end() {
        return drainage_amount4_end;
    }
    public void setDrainage_amount4_end(Integer drainage_amount_end4) {
        this.drainage_amount4_end = drainage_amount_end4;
    }
    public String getDrain_characteristics4() {
        return drain_characteristics4;
    }
    public void setDrain_characteristics4(String characteristics4) {
        this.drain_characteristics4 = characteristics4;
    }
    public String getDrain_odour4() {
        return drain_odour4;
    }
    public void setDrain_odour4(String odour4) {
        this.drain_odour4 = odour4;
    }
    public Integer getDrainage_amount_mls5() {
        return drainage_amount_mls5;
    }
    public void setDrainage_amount_mls5(Integer drainage_amount_mls5) {
        this.drainage_amount_mls5 = drainage_amount_mls5;
    }
    public Integer getDrainage_amount_hrs5() {
        return drainage_amount_hrs5;
    }
    public void setDrainage_amount_hrs5(Integer drainage_amount_hrs5) {
        this.drainage_amount_hrs5 = drainage_amount_hrs5;
    }
    public Integer getDrainage_amount5_start() {
        return drainage_amount5_start;
    }
    public void setDrainage_amount5_start(Integer drainage_amount_start5) {
        this.drainage_amount5_start = drainage_amount_start5;
    }
    public Integer getDrainage_amount5_end() {
        return drainage_amount5_end;
    }
    public void setDrainage_amount5_end(Integer drainage_amount_end5) {
        this.drainage_amount5_end = drainage_amount_end5;
    }
    public String getDrain_characteristics5() {
        return drain_characteristics5;
    }
    public void setDrain_characteristics5(String drain_characteristics5) {
        this.drain_characteristics5 = drain_characteristics5;
    }
    public String getDrain_odour5() {
        return drain_odour5;
    }
    public void setDrain_odour5(String drain_odour5) {
        this.drain_odour5 = drain_odour5;
    }
    public Date getAdditional_days_date5() {
        return additional_days_date5;
    }
    public void setAdditional_days_date5(Date additional_days_date5) {
        this.additional_days_date5 = additional_days_date5;
    }
    public Date getAdditional_days_date4() {
        return additional_days_date4;
    }
    public void setAdditional_days_date4(Date additional_days_date4) {
        this.additional_days_date4 = additional_days_date4;
    }
    public Date getAdditional_days_date3() {
        return additional_days_date3;
    }
    public void setAdditional_days_date3(Date additional_days_date3) {
        this.additional_days_date3 = additional_days_date3;
    }
    public Date getAdditional_days_date2() {
        return additional_days_date2;
    }
    public void setAdditional_days_date2(Date additional_days_date2) {
        this.additional_days_date2 = additional_days_date2;
    }
    public Date getAdditional_days_date1() {
        return additional_days_date1;
    }
    public void setAdditional_days_date1(Date additional_days_date1) {
        this.additional_days_date1 = additional_days_date1;
    }
    public Date getTubes_changed_date() {
        return tubes_changed_date;
    }
    public void setTubes_changed_date(Date tubes_changed_date) {
        this.tubes_changed_date = tubes_changed_date;
    }
    public Integer getDischarge_reason() {
        return discharge_reason;
    }
    public void setDischarge_reason(Integer discharge_reason) {
        this.discharge_reason = discharge_reason;
    }
    public Integer getDrain_removed_intact() {
        return drain_removed_intact;
    }
    public void setDrain_removed_intact(Integer drain_removed_intact) {
        this.drain_removed_intact = drain_removed_intact;
    }
    public Integer getDrainage_num() {
        return drainage_num;
    }
    public void setDrainage_num(Integer drainage_num) {
        this.drainage_num = drainage_num;
    }
    public String getType_of_drain_other() {
        return type_of_drain_other;
    }
    public void setType_of_drain_other(String type_of_drain_other) {
        this.type_of_drain_other = type_of_drain_other;
    }
    public String getPeri_drain_area() {
        return peri_drain_area;
    }
    public void setPeri_drain_area(String peri_drain_area) {
        this.peri_drain_area = peri_drain_area;
    }
    public Integer getPain() {
        return pain;
    }
    public void setPain(Integer pain) {
        this.pain = pain;
    }
    public String getPain_comments() {
        return pain_comments;
    }
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
}
