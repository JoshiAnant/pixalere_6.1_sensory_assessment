package com.pixalere.assessment.bean;
import com.pixalere.common.bean.ReferralsTrackingVO;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import java.util.Date;
import com.pixalere.auth.bean.PositionVO;
/**
 *     This is the AssessmentComments  bean for the assessment_comments  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentCommentsServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundProfileTypeVO
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *
 */
public class AssessmentCommentsVO extends ValueObject implements Serializable {
    private ProfessionalVO professional;
    private String delete_signature;
    private PositionVO position;
    private ReferralsTrackingVO referral;
    //private WoundAssessmentVO woundAssessment;
    /** identifier field */
    private Integer id;
    private WoundProfileTypeVO wound_profile_type;
    /** nullable persistent field */
    private Integer patient_id;
    /** nullable persistent field */
    private Integer wound_id;
    private Integer referral_id;
    /** nullable persistent field */
    private Integer assessment_id;
    /** nullable persistent field */
    private Integer professional_id;
    /** nullable persistent field */
    private Date created_on;
private Integer wound_profile_type_id;
    private Integer deleted;
    private String delete_reason;
    private Date deleted_on;
    private Integer position_id;
    private String comment_type;
    /** nullable persistent field */
    private String body;
    private String user_signature;
    private Integer ncp_changed;
    private Integer wcc_visit_required;
    private Date visited_on;
    /** full constructor */
    public AssessmentCommentsVO(Integer patient_id, Integer wound_id, Integer assessment_id, Integer professional_id, Date timestamp,  String body, Integer referral_id, Integer position_id,String comment_type) {
        this.setPatient_id(patient_id);
        this.setWound_id(wound_id);
        this.referral_id=referral_id;
        this.setAssessment_id(assessment_id);
        this.setProfessional_id(professional_id);
        this.setCreated_on(timestamp);
        this.setBody(body);
        this.position_id=position_id;
        this.comment_type=comment_type;
    }
    /** default constructor */
    public AssessmentCommentsVO() {
    }
    public ProfessionalVO getProfessional() {
        return this.professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public String getDelete_signature(){
        return delete_signature;
    }
    public void setDelete_signature(String delete_signature){
        this.delete_signature=delete_signature;
    }
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatient_id() {
        return this.patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public String getUser_signature() {
        return this.user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public Integer getWound_id() {
        return this.wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public Integer getAssessment_id() {
        return this.assessment_id;
    }
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    public Integer getProfessional_id() {
        return this.professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    
    public String getBody() {
        return this.body;
    }
    public void setBody(String body) {
        this.body = body;
    }
    public WoundProfileTypeVO getWound_profile_type() {
        return wound_profile_type;
    }
    public void setWound_profile_type(WoundProfileTypeVO wound_profile_type) {
        this.wound_profile_type = wound_profile_type;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
    public Integer getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    /**
     * @return the delete_reason
     */
    public String getDelete_reason() {
        return delete_reason;
    }
    /**
     * @param delete_reason the delete_reason to set
     */
    public void setDelete_reason(String delete_reason) {
        this.delete_reason = delete_reason;
    }
    /**
     * @return the referral
     */
    public ReferralsTrackingVO getReferral() {
        return referral;
    }
    /**
     * @param referral the referral to set
     */
    public void setReferral(ReferralsTrackingVO referral) {
        this.referral = referral;
    }
    /**
     * @return the referral_id
     */
    public Integer getReferral_id() {
        return referral_id;
    }
    /**
     * @param referral_id the referral_id to set
     */
    public void setReferral_id(Integer referral_id) {
        this.referral_id = referral_id;
    }
    /**
     * @return the position_id
     */
    public Integer getPosition_id() {
        return position_id;
    }
    /**
     * @param position_id the position_id to set
     */
    public void setPosition_id(Integer position_id) {
        this.position_id = position_id;
    }
    /**
     * @return the comment_type
     */
    public String getComment_type() {
        return comment_type;
    }
    /**
     * @param comment_type the comment_type to set
     */
    public void setComment_type(String comment_type) {
        this.comment_type = comment_type;
    }
    /**
     * @return the ncp_changed
     */
    public Integer getNcp_changed() {
        return ncp_changed;
    }
    /**
     * @param ncp_changed the ncp_changed to set
     */
    public void setNcp_changed(Integer ncp_changed) {
        this.ncp_changed = ncp_changed;
    }
    /**
     * @return the wcc_visit_required
     */
    public Integer getWcc_visit_required() {
        return wcc_visit_required;
    }
    /**
     * @param wcc_visit_required the wcc_visit_required to set
     */
    public void setWcc_visit_required(Integer wcc_visit_required) {
        this.wcc_visit_required = wcc_visit_required;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }

    /**
     * @return the position
     */
    public PositionVO getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(PositionVO position) {
        this.position = position;
    }
}
