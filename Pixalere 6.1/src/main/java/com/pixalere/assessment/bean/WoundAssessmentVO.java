package com.pixalere.assessment.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Date;
/**
 *     This is the WoundAssessment  bean for the wound_assessment  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.WoundAssessmentServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundProfileVO
 *     @has 1..1 Has 1..1 com.pixalere.patient.bean.PatientAccountVO
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *
 */
public class WoundAssessmentVO extends ValueObject implements Serializable {
    
    /** identifier field */
    private Integer id;
    private PatientAccountVO patientAccount;
    private WoundProfileVO woundProfile;
    
    private ProfessionalVO professional;
    //global assessment values
    private Integer visit;
    private Integer reset_visit;
    private String reset_reason;
   
    
	private String user_signature;
    /** nullable persistent field */
    private Integer patient_id;
    private Integer offline_flag;
    /** nullable persistent field */
    private Integer wound_id;
    private Integer treatment_location_id;
    /** nullable persistent field */
    private Integer professional_id;
    private Integer has_comments;
    /** nullable persistent field */
    private Date created_on;
    /** nullable persistent field */
    private Integer active;
    private Integer patient_account_id;
    
    private Date lastmodified_on;
    /** full constructor */
    public WoundAssessmentVO(Integer patient_id, Integer wound_id, Integer professional_id, Date timestamp, Integer active) {
        this.patient_id = patient_id;
        this.wound_id = wound_id;
        this.professional_id = professional_id;
        this.has_comments = has_comments;
        this.created_on = timestamp;
        this.active = active;
    }
       
    /** default constructor */
    public WoundAssessmentVO() {
    }
    public Integer getVisit(){
        return visit;
    }
    public void setVisit(Integer visit){
        this.visit=visit;
    }
    public Integer getReset_visit(){
        return reset_visit;
    }
    public void setReset_visit(Integer reset_visit){
        this.reset_visit=reset_visit;
    }
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatient_id() {
        return this.patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getWound_id() {
        return this.wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public Integer getTreatment_location_id() {
        return this.treatment_location_id;
    }
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }
    public Integer getProfessional_id() {
        return this.professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public Integer getHas_comments() {
        return this.has_comments;
    }
    public void setHas_comments(Integer has_comments) {
        this.has_comments = has_comments;
    }
    public String getReset_reason() {
        return this.reset_reason;
    }
    public void setReset_reason(String reset_reason) {
        this.reset_reason = reset_reason;
    }
    public Integer getActive() {
        return this.active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
	
    public String getUser_signature() {
        return this.user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public Integer getOffline_flag() {
        return offline_flag;
    }
    public void setOffline_flag(Integer offline_flag) {
        this.offline_flag = offline_flag;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
   
    
}
