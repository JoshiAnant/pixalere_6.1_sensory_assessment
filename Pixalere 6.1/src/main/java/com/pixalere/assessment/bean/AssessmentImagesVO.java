package com.pixalere.assessment.bean;
import javax.xml.bind.annotation.XmlMimeType;
import javax.activation.*;
import java.io.*;
import com.pixalere.common.ValueObject;       
/**
 *     This is the AssessmentImages bean for the assessment_images  (DB table)
 *     Refer to {@link com.pixalere.assessment.service.AssessmentImagesServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentVO
 *
 */
public class AssessmentImagesVO extends ValueObject implements Serializable {
    private WoundAssessmentVO woundAssessment;
    /** identifier field */
    private Integer id;
    private Integer alpha_id;
    /** nullable persistent field */
    private Integer patient_id;
    /** nullable persistent field */
    private Integer wound_id;
    private Integer whichImage;
    /** nullable persistent field */
    private Integer assessment_id;
    private Integer wound_profile_type_id;
    private String images;
    
    private DataHandler imagefile;
    
    @XmlMimeType("application/octet-stream")
    public DataHandler getImagefile(){
        return imagefile;
    }
    public void setImagefile(DataHandler imagefile){
        this.imagefile=imagefile;
    }
	/** default constructor */
    public AssessmentImagesVO() {
    }
	private Integer active;
    public Integer getAlpha_id() {
        return alpha_id;
    }
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    public Integer getActive(){
		return active;
		
	}
	public void setActive(Integer active){
		this.active=active;
	}
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getWhichImage() {
        return this.whichImage;
    }
    public void setWhichImage(Integer whichImage) {
        this.whichImage = whichImage;
    }
    public Integer getPatient_id() {
        return this.patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getWound_id() {
        return this.wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    public Integer getAssessment_id() {
        return this.assessment_id;
    }
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    public String getImages() {
        return this.images;
    }
    public void setImages(String images) {
        this.images = images;
    }
    
    private int counter=0;
    public WoundAssessmentVO getWoundAssessment() {
        return woundAssessment;
    }
    public void setWoundAssessment(WoundAssessmentVO woundAssessment) {
        this.woundAssessment = woundAssessment;
    }
    public int getCounter() {
        return counter;
    }
    public void setCounter(int counter) {
        this.counter = counter;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
}
