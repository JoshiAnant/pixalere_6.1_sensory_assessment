package com.pixalere.assessment.dao;
import com.pixalere.assessment.bean.AssessmentTreatmentArraysVO;
import com.pixalere.assessment.bean.AssessmentTreatmentVO;
import com.pixalere.common.*;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import java.util.Collection;
import java.util.Iterator;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  treatment_comment    table in the  .
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class TreatmentCommentDAO implements DataAccessObject {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(TreatmentCommentDAO.class);
    /**
     * Finds a all treatment_comment records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(com.pixalere.common.ValueObject)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(TreatmentCommentVO ProfessionalVO) throws
            DataAccessException {
        logs.info("***********************Entering TreatmentCommentDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        TreatmentCommentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new TreatmentCommentVO();
            QueryByCriteria query = new QueryByCriteria(ProfessionalVO);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in TreatmentCommentDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in TreatmentCommentDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info(
                "***********************Leaving TreatmentCommentDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all assessment_treatment records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findAllByCriteria(com.pixalere.common.ValueObject)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(AssessmentTreatmentVO assess) throws
            DataAccessException {
        logs.info("***********************Entering TreatmentCommentDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(assess);
            query.addOrderByDescending("id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in TreatmentCommentDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in TreatmentCommentDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info(
                "***********************Leaving TreatmentCommentDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an assessment_treatment record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(AssessmentTreatmentVO vo) throws DataAccessException {
        logs.info("***********************Entering TreatmentCommentDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AssessmentTreatmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentTreatmentVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("id");
            returnVO = (AssessmentTreatmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in TreatmentCommentDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in TreatmentCommentDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving TreatmentCommentDAO.findByCriteria()***********************");
        return returnVO;
    }
/**
     * Finds an assessment_treatment_arrays record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ArrayValueObject findByCriteria(AssessmentTreatmentArraysVO vo) throws DataAccessException {
        logs.info("***********************Entering TreatmentCommentDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AssessmentTreatmentArraysVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentTreatmentArraysVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("id");
            returnVO = (AssessmentTreatmentArraysVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in TreatmentCommentDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in TreatmentCommentDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving TreatmentCommentDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an treatment_comment record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(TreatmentCommentVO vo) throws DataAccessException {
        logs.info("***********************Entering TreatmentCommentDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        TreatmentCommentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new TreatmentCommentVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("created_on");
            returnVO = (TreatmentCommentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in TreatmentCommentDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in TreatmentCommentDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving TreatmentCommentDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a single treatment_comment record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the treatment_comment table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int id) throws DataAccessException {
        logs.info("********* Entering the TreatmentCommentDAO.findByPK****************");
        PersistenceBroker broker = null;
        TreatmentCommentVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new TreatmentCommentVO();
            assessmentVO.setId(new Integer(id));
            Query query = new QueryByCriteria(assessmentVO);
            assessmentVO = (TreatmentCommentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in TreatmentCommentDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in TreatmentCommentDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the TreatmentCommentDAO.findByPK");
        return assessmentVO;
    }
    /**
     * Finds a single treatment_comment record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @todo find out if this method is even used, i suspect not.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findLastTreatmentComment(String wound_id) throws DataAccessException {
        logs.info("********* Entering the TreatmentCommentDAO.getLastAssessment****************");
        PersistenceBroker broker = null;
        TreatmentCommentVO vo = null;
        Criteria crit = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit = new Criteria();
            crit.addEqualTo("active", new Integer(1));
            crit.addEqualTo("wound_id", new Integer(wound_id));
            QueryByCriteria query = QueryFactory.newQuery(TreatmentCommentVO.class, crit);
            query.addOrderByDescending("id");
            vo = (TreatmentCommentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in TreatmentCommentDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in TreatmentCommentDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the TreatmentCommentDAO.findByPK");
        return vo;
    }
    public void insert(ValueObject insert) {
        //Not needed.
    }
    /**
     * Updates a single treatment_comment record in the Pixalere .  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the treatment_comment table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void update(ValueObject update) {
        logs.info("************Entering TreatmentCommentDAO.update()************");
        PersistenceBroker broker = null;
        try {
            TreatmentCommentVO assessment = (TreatmentCommentVO) update;
            if(assessment!=null && assessment.getDeleted() == null){
                assessment.setDeleted(0);
            }
            
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in TreatmentCommentDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in TreatmentCommentDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
}finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with TreatmentCommentDAO.update()*************");
    }
    /**
     * Updates a single assessment_treatment record in the Pixalere .  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param update the object which represents a record in the assessment_treatment table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void updateAssessmentTreatment(ValueObject update) {
        logs.info("************Entering TreatmentCommentDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentTreatmentVO assessment = (AssessmentTreatmentVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
          
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in TreatmentCommentDAO.updateAssessmentTreatment(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in TreatmentCommentDAO.updateAssessmentTreatment(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
}finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with TreatmentCommentDAO.update()*************");
    }
    /**
     * Deletes treatment_comment records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject delete) {
        logs.info("************Entering TreatmentCommentDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            TreatmentCommentVO assessment = (TreatmentCommentVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            Collection<TreatmentCommentVO> result = findAllByCriteria(assessment);
            for (TreatmentCommentVO tmpResultVO : result) {
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
           }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in TreatmentCommentDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in TreatmentCommentDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
}catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
} finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with TreatmentCommentDAO.delete()*************");
    }
    /**
     * Deletes assessment_treatment records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param delete  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void deleteAssessmentTreatment(ValueObject delete) {
        logs.info("************Entering TreatmentCommentDAO.deleteAssessmentTreatment()************");
        PersistenceBroker broker = null;
        try {
            AssessmentTreatmentVO assessment = (AssessmentTreatmentVO) delete;
            Collection<AssessmentTreatmentVO> result = findAllByCriteria(assessment);
            broker = ConnectionLocator.getInstance().findBroker();
            for (AssessmentTreatmentVO tmpResultVO : result) {
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
               
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in TreatmentCommentDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in TreatmentCommentDAO.delete(): " + e.toString(), e);
        }catch (com.pixalere.common.DataAccessException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
}catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
} finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with TreatmentCommentDAO.delete()*************");
    }
    /**
     * Deletes a single AssessmentTreatmentArraysVO object into the assessment_treatment_arrays table.  The value object passed
     * in has to be of type AssessmentTreatmentArraysVO.
     *
     * @param delete fields in ValueObject represent fields in the assessment_treatment_arrays record.
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deleteTreatmentArrays(ArrayValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentTreatmentArraysVO s = (AssessmentTreatmentArraysVO) delete;
            if (s != null && (s.getAssessment_treatment_id() != null)) {
                Collection<AssessmentTreatmentArraysVO> result = findAllByCriteria(s);
                broker = ConnectionLocator.getInstance().findBroker();
                for (AssessmentTreatmentArraysVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                    
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
}catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
}finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     * Updates a single AssessmentTreatmentArraysVO object into the assessment_treatment_arrays table.  The value object passed
     * in has to be of type AssessmentTreatmentArraysVO.
     *
     * @param update fields in ValueObject represent fields in the assessment_treatment_arrays record.
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 4.1.2
     */
    public void saveTreatmentArrays(ArrayValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentTreatmentArraysVO assessment = (AssessmentTreatmentArraysVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
    }finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
    public Collection findAllByCriteria(AssessmentTreatmentArraysVO crit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
}
