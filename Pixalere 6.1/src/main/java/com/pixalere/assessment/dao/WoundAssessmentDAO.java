package com.pixalere.assessment.dao;
import com.pixalere.common.dao.ReferralsTrackingDAO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.*;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import java.util.*;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.dao.TreatmentCommentDAO;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  wound_assessment    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class WoundAssessmentDAO implements DataAccessObject {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(WoundAssessmentDAO.class);
    public WoundAssessmentDAO() {
    }
    /**
     * Finds a all wound_assessment records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param woundAssessmentVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(WoundAssessmentVO woundassessmentVO)
            throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //need to refactor this out later
            if (woundassessmentVO.getActive() == null || !woundassessmentVO.getActive().equals(new Integer(0))) {
                woundassessmentVO.setActive(new Integer(1));
            }
            QueryByCriteria query = new QueryByCriteria(woundassessmentVO);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "ServiceLocatorException thrown in WoundAssessmentDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundAssessmentDAO.findAllByCriteria()",
                    e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundAssessmentDAO.findAllByCriteria()***********************");
        return results;
    }
/**
   * Find a wound_assessment within a date range.  If not record is found, a nULL
   * is returned.
   * 
   * @param id the primary key of the wound_assessment table, if less then 1
   * this parameter will be ignored
   * @version 6.0
   * @since 6.0
   * 
   */
  public WoundAssessmentVO findPrevByCriteria(int id) throws DataAccessException {
      logs.info("***********************Entering WoundAssessmentDAO.findByCriteria()***********************");
      PersistenceBroker broker = null;
      WoundAssessmentVO returnVO = null;
      try {
          broker = ConnectionLocator.getInstance().findBroker();
          Criteria crit = new Criteria();
          QueryByCriteria query = null;
          crit.addEqualTo("active", new Integer(1));
          if (id > 0) {
              crit.addLessThan("id", id);
          }
          
          
          
          query = QueryFactory.newQuery(WoundAssessmentVO.class, crit);
          query.addOrderByDescending("created_on");
         
 
          returnVO = (WoundAssessmentVO) broker.getObjectByQuery(query);
      } catch (ConnectionLocatorException e) {
          logs.error("ServiceLocatorException thrown in WoundAssessmentDAO.findByCriteria(): " + e.toString(), e);
          throw new DataAccessException("ServiceLocatorException in WoundAssessmentDAO.findByCriteria()", e);
      } finally {
          if (broker != null) {
              broker.close();
          }
      }
      logs.info("***********************Leaving WoundAssessmentDAO.findByCriteria()***********************");
      return returnVO;
      
  }
  /**
   * Find a wound_assessment within a date range.  If not record is found, a nULL
   * is returned.
   * 
   * @param id the primary key of the wound_assessment table, if less then 1
   * this parameter will be ignored
   * @version 6.0
   * @since 6.0
   * 
   */
  public WoundAssessmentVO findNextByCriteria(int id) throws DataAccessException {
      logs.info("***********************Entering WoundAssessmentDAO.findByCriteria()***********************");
      PersistenceBroker broker = null;
      WoundAssessmentVO returnVO = null;
      try {
          broker = ConnectionLocator.getInstance().findBroker();
          Criteria crit = new Criteria();
          QueryByCriteria query = null;
          crit.addEqualTo("active", new Integer(1));
          if (id > 0) {
              crit.addGreaterThan("id", id);
          }
          
          
          
          query = QueryFactory.newQuery(WoundAssessmentVO.class, crit);
          query.addOrderByAscending("created_on");
         
          returnVO = (WoundAssessmentVO) broker.getObjectByQuery(query);
      } catch (ConnectionLocatorException e) {
          logs.error("ServiceLocatorException thrown in WoundAssessmentDAO.findByCriteria(): " + e.toString(), e);
          throw new DataAccessException("ServiceLocatorException in WoundAssessmentDAO.findByCriteria()", e);
      } finally {
          if (broker != null) {
              broker.close();
          }
      }
      logs.info("***********************Leaving WoundAssessmentDAO.findByCriteria()***********************");
      return returnVO;
      
  }
    /**
     * Finds a all wound_assessment records, as specified by the value object
     * passed in.  If no record is found a null value is returned. Used for Patient File Report so we only
     * want assessments which contain photos.
     *
     * @param woundAssessmentVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public List findAllByCriteriaForPatientFile(WoundAssessmentVO woundassessmentVO)
            throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        List results = new ArrayList();
        Hashtable<Integer, WoundAssessmentVO> consolidate = new Hashtable();
        WoundAssessmentLocationServiceImpl wal = new WoundAssessmentLocationServiceImpl();
        AssessmentImagesServiceImpl ais = new AssessmentImagesServiceImpl();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            WoundAssessmentLocationVO t = new WoundAssessmentLocationVO();
            t.setWound_id(woundassessmentVO.getWound_id());
            t.setActive(1);
            Vector<WoundAssessmentLocationVO> alphas = wal.getAllAlphas(t);
            for (WoundAssessmentLocationVO alpha : alphas) {
                AssessmentImagesVO tt = new AssessmentImagesVO();
                tt.setWound_id(woundassessmentVO.getWound_id());
                tt.setAlpha_id(alpha.getId());
                AssessmentImagesVO imTMP = ais.getImage(tt);//getting last assessment with a photo.
                WoundAssessmentVO ttt = new WoundAssessmentVO();
                ttt.setId(imTMP.getAssessment_id());
                QueryByCriteria query = new QueryByCriteria(ttt);
                WoundAssessmentVO results2 = (WoundAssessmentVO) broker.getObjectByQuery(query);
                if (results2 != null) {
                    consolidate.put(results2.getId(), results2);
                }
            }
            for (WoundAssessmentVO assess : consolidate.values()) {
                results.add(assess);
            }
            
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "ServiceLocatorException thrown in WoundAssessmentDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundAssessmentDAO.findAllByCriteria()",
                    e);
        } catch (ApplicationException e) {
            logs.error("DataAccessException thrown in WoundAssessmentDAO.findAllByCriteria(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundAssessmentDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all wound_assessment records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     *
     * @since 3.0
     */
    public Collection findAllByCriteriaByRange(WoundAssessmentVO userVO,
            String start_id, String end_id) throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("active", new Integer(1));
            crit.addEqualTo("patient_id", userVO.getPatient_id());
            if (userVO.getVisit() != null) {
                crit.addEqualTo("visit", 1);
            }
            if (userVO.getWound_id() != null) {
                crit.addEqualTo("wound_id", userVO.getWound_id());
            }
            if (!start_id.equals("")) {
                crit.addGreaterOrEqualThan("id", new Integer(start_id));
            }
            if (!end_id.equals("")) {
                crit.addLessOrEqualThan("id", new Integer(end_id));
            }
            query = QueryFactory.newQuery(WoundAssessmentVO.class, crit);
            if (userVO.getWound_id() != null) {
                query.addOrderByDescending("created_on");
            }
            //If start/end is not set, get last 30 records
            //set a limit.
            if (start_id.equals("")) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(30);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "ServiceLocatorException thrown in WoundAssessmentDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundAssessmentDAO.findAllByCriteria()",
                    e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundAssessmentDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all wound_assessment records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     *
     * @since 4.2
     */
    public Collection findAllByCriteriaByDateRange(WoundAssessmentVO userVO,
            Date start_date, Date end_date) throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("active", new Integer(1));
            crit.addEqualTo("patient_id", userVO.getPatient_id());
            if (userVO.getVisit() != null) {
                crit.addEqualTo("visit", 1);
            }
            if (userVO.getWound_id() != null) {
                crit.addEqualTo("wound_id", userVO.getWound_id());
            }
            if (!start_date.equals("")) {
                crit.addGreaterOrEqualThan("created_on", start_date);
            }
            if (!end_date.equals("")) {
                crit.addLessOrEqualThan("created_on", end_date);
            }
            query = QueryFactory.newQuery(WoundAssessmentVO.class, crit);
            if (userVO.getWound_id() != null) {
                query.addOrderByDescending("created_on");
            }
            //If start/end is not set, get last 30 records
            //set a limit.
            if (start_date.equals("")) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(30);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "ServiceLocatorException thrown in WoundAssessmentDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundAssessmentDAO.findAllByCriteria()",
                    e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundAssessmentDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an wound_assessment record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(WoundAssessmentVO assessmentVO)
            throws DataAccessException {
        logs.info("********* Entering the WoundAssessmentDAO.findByPK****************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("created_on");
            assessmentVO = (WoundAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "PersistanceBrokerException thrown in WoundAssessmentDAO.findByPK(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "Error in WoundAssessmentDAO.findByPK(): " + e.toString(),
                    e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the WoundAssessmentDAO.findByPK");
        return assessmentVO;
    }
    /**
     * Finds a single wound_assessment record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the wound_assessment table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int id) throws DataAccessException {
        logs.info("********* Entering the WoundAssessmentDAO.findByPK****************");
        PersistenceBroker broker = null;
        WoundAssessmentVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new WoundAssessmentVO();
            assessmentVO.setId(new Integer(id));
            Query query = new QueryByCriteria(assessmentVO);
            assessmentVO = (WoundAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "PersistanceBrokerException thrown in WoundAssessmentDAO.findByPK(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "Error in WoundAssessmentDAO.findByPK(): " + e.toString(),
                    e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the WoundAssessmentDAO.findByPK");
        return assessmentVO;
    }
    /**
     * Inserts a single WoundAssessmentVO object into the wound_assessment table.  The value object passed
     * in has to be of type WoundAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the wound_assessment record.
     * @see com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void insert(ValueObject insert) {
        logs.info("************Entering AssessmentEachwoundDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            WoundAssessmentVO assessment = (WoundAssessmentVO) insert;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.insert(): "
                    + e.toString(), e);
            e.printStackTrace();
            // throw new DataAccessException("Error in
            // WoundAssessmentDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "connectionLocatorException thrown in WoundAssessmentDAO.insert(): "
                    + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundAssessmentDAO.insert()*************");
    }
    /**
     * Inserts a single WoundAssessmentVO object into the wound_assessment table.  The value object passed
     * in has to be of type WoundAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the wound_assessment record.
     * @see com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void update(ValueObject update) {
        logs.info("************Entering WoundAssessmentDAO.update()************");
        PersistenceBroker broker = null;
        try {
            WoundAssessmentVO assessment = (WoundAssessmentVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
           
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.update(): "
                    + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundAssessmenmtDAO.update(): "
                    + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundAssessmentDAO.update()*************");
    }
    /**
     * Finds an wound_assessment record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param wound_id  the wound_profiles id.
     *
     * @todo refactor out, findByCriteria should handle this.
     * @since 3.0
     */
    public ValueObject findLastWoundAssessment(String wound_id)
            throws DataAccessException {
        PersistenceBroker broker = null;
        WoundAssessmentVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new WoundAssessmentVO();
            assessmentVO.setWound_id(new Integer(wound_id));
            assessmentVO.setActive(new Integer(1));
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("created_on");
            assessmentVO = (WoundAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "Error in AssessmentEachwoundDAO.findByPK(): "
                    + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return assessmentVO;
    }
    /**
     * Finds an wound_assessment record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param wound_id  the wound_profiles id.
     * @param professional_id the Professional logged in
     * @todo refactor out, findByCriteria should handle this.
     * @since 3.0
     */
    public ValueObject findLastInactiveWoundAssessment(String wound_id, int professional_id)
            throws DataAccessException {
        PersistenceBroker broker = null;
        WoundAssessmentVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new WoundAssessmentVO();
            assessmentVO.setWound_id(new Integer(wound_id));
            assessmentVO.setProfessional_id(new Integer(professional_id));
            assessmentVO.setActive(new Integer(0));
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("id");
            assessmentVO = (WoundAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "Error in AssessmentEachwoundDAO.findByPK(): "
                    + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return assessmentVO;
    }
    /**
     * Deletes wound_assessment records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject delete, Date purge_days) {
        logs.info("************Entering WoundAssessmentDAO.delete()************");
        AssessmentDAO ad = new AssessmentDAO();
        ReferralsTrackingDAO rt = new ReferralsTrackingDAO();
        NursingCarePlanDAO ncp = new NursingCarePlanDAO();
        TreatmentCommentDAO tcdao = new TreatmentCommentDAO();
        AssessmentCommentsDAO acdao = new AssessmentCommentsDAO();
        PersistenceBroker broker = null;
        try {
            WoundAssessmentVO assessment = (WoundAssessmentVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            Collection<WoundAssessmentVO> result = findAllIncludingDeletedByCriteria(assessment);
            for (WoundAssessmentVO tmpResultVO : result) {
                Date created_on = tmpResultVO.getCreated_on();
                
                if (purge_days == null || created_on.before(purge_days)) {
                    AssessmentTreatmentVO at = new AssessmentTreatmentVO();
                    at.setAssessment_id(tmpResultVO.getId());
                    tcdao.deleteAssessmentTreatment(at);
                    AssessmentProductVO p = new AssessmentProductVO();
                    p.setAssessment_id(tmpResultVO.getId());
                    ad.deleteProducts(p);
                    AssessmentImagesVO ii = new AssessmentImagesVO();
                    ii.setAssessment_id(tmpResultVO.getId());
                    ad.deleteImages(ii);
                    AssessmentDrainVO d = new AssessmentDrainVO();
                    d.setAssessment_id(tmpResultVO.getId());
                    ad.delete(d);
                    AssessmentIncisionVO i = new AssessmentIncisionVO();
                    i.setAssessment_id(tmpResultVO.getId());
                    ad.delete(i);
                    AssessmentEachwoundVO e = new AssessmentEachwoundVO();
                    e.setAssessment_id(tmpResultVO.getId());
                    ad.delete(e);
                    AssessmentOstomyVO o = new AssessmentOstomyVO();
                    o.setAssessment_id(tmpResultVO.getId());
                    ad.delete(o);
                    AssessmentBurnVO b = new AssessmentBurnVO();
                    b.setAssessment_id(tmpResultVO.getId());
                    ad.delete(b);
                    AssessmentCommentsVO bb = new AssessmentCommentsVO();
                    bb.setAssessment_id(tmpResultVO.getId());
                    acdao.delete(bb);
                    ReferralsTrackingVO rtdelete = new ReferralsTrackingVO();
                    rtdelete.setAssessment_id(tmpResultVO.getId());
                    rt.delete(rtdelete);
                    NursingCarePlanVO ncpdelete = new NursingCarePlanVO();
                    ncpdelete.setAssessment_id(tmpResultVO.getId());
                    ncp.delete(ncpdelete);
                    DressingChangeFrequencyVO dcfdelete = new DressingChangeFrequencyVO();
                    dcfdelete.setAssessment_id(tmpResultVO.getId());
                    //dcfdelete.setActive(0);
                    ncp.deleteDcf(dcfdelete);
                    TreatmentCommentVO tcdelete = new TreatmentCommentVO();
                    tcdelete.setAssessment_id(tmpResultVO.getId());
                    //tcdelete.setActive(0);
                    tcdao.delete(tcdelete);
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
               }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error(
                    "PersistanceBrokerException thrown in WoundAssessmentDAO.delete(): "
                    + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "connectionLocatorException thrown in WoundAssessmentDAO.delete(): "
                    + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundAssessmentDAO.delete()*************");
    }
/**
     * Deletes wound_assessment records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void deleteOffline(int patient_id, int patient_account_id) {
        logs.info("************Entering WoundAssessmentDAO.delete()************");
        AssessmentDAO ad = new AssessmentDAO();
        ReferralsTrackingDAO rt = new ReferralsTrackingDAO();
        AssessmentNPWTDAO an = new AssessmentNPWTDAO();
        NursingCarePlanDAO ncp = new NursingCarePlanDAO();
        TreatmentCommentDAO tcdao = new TreatmentCommentDAO();
        AssessmentCommentsDAO acdao =new AssessmentCommentsDAO();
        PersistenceBroker broker = null;
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            //Collection<WoundAssessmentVO> result=findAllIncludingDeletedByCriteria(assessment);
            //for(WoundAssessmentVO tmpResultVO : result){
                
                    AssessmentProductVO p = new AssessmentProductVO();
                    p.setPatient_id(patient_id);
                    ad.deleteProducts(p);
                    AssessmentImagesVO ii = new AssessmentImagesVO();
                    ii.setPatient_id(patient_id);
                    ad.deleteImages(ii);
                    AssessmentDrainVO d = new AssessmentDrainVO();
                    d.setPatient_id(patient_id);
                    ad.delete(d);
                    AssessmentIncisionVO i = new AssessmentIncisionVO();
                    i.setPatient_id(patient_id);
                    ad.delete(i);
                    AssessmentEachwoundVO e = new AssessmentEachwoundVO();
                    e.setPatient_id(patient_id);
                    ad.delete(e);
                    AssessmentOstomyVO o = new AssessmentOstomyVO();
                    o.setPatient_id(patient_id);
                    ad.delete(o);
                    AssessmentBurnVO b = new AssessmentBurnVO();
                    b.setPatient_id(patient_id);
                    ad.delete(b);
                    
                    AssessmentSkinVO s = new AssessmentSkinVO();
                    s.setPatient_id(patient_id);
                    ad.delete(s);
                    
                    AssessmentNPWTVO bt = new AssessmentNPWTVO();
                    bt.setPatient_id(patient_id);
                    an.delete(bt);
                    
                    ReferralsTrackingVO rtdelete = new ReferralsTrackingVO();
                    rtdelete.setPatient_account_id(patient_account_id);
                    rt.delete(rtdelete);
                    AssessmentCommentsVO acdelete = new AssessmentCommentsVO();
                    acdelete.setPatient_id(patient_id);
                    acdao.delete(acdelete);
                    NursingCarePlanVO ncpdelete = new NursingCarePlanVO();
                    ncpdelete.setPatient_id(patient_id);
                    ncp.delete(ncpdelete);
                    DressingChangeFrequencyVO dcfdelete =new DressingChangeFrequencyVO();
                    dcfdelete.setPatient_id(patient_id);
                    //dcfdelete.setActive(0);
                    ncp.deleteDcf(dcfdelete);
                    TreatmentCommentVO tcdelete = new TreatmentCommentVO();
                    tcdelete.setPatient_id(patient_id);
                    //tcdelete.setActive(0);
                    tcdao.delete(tcdelete);
                    WoundAssessmentVO tmpResultVO  = new WoundAssessmentVO();
                    tmpResultVO.setPatient_id(patient_id);
                    Collection<WoundAssessmentVO> tmpAssessment = findAllByCriteria(tmpResultVO);
                    for(WoundAssessmentVO w : tmpAssessment){
                        broker.beginTransaction();
                        broker.delete(w);
                        broker.commitTransaction();
                    }
                
            //}
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error(
                    "PersistanceBrokerException thrown in WoundAssessmentDAO.delete(): "
                    + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "connectionLocatorException thrown in WoundAssessmentDAO.delete(): "
                    + e.toString(), e);
        }catch (DataAccessException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.delete(): "+e.toString(),e);
        }finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs
                .info("*************Done with WoundAssessmentDAO.delete()*************");
    }
    public Collection findAllIncludingDeletedByCriteria(WoundAssessmentVO ProfessionalVO)
            throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(ProfessionalVO);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "ServiceLocatorException thrown in WoundAssessmentDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in WoundAssessmentDAO.findAllByCriteria()",
                    e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundAssessmentDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an wound_assessment record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id  the patient_id of the patient you are looking for
     * @param timestamp the timestamp of the assessment you are looking for. This is
     * good if you ar elooking for an assessment but don't know the id (maybe itw as just inserted).
     *
     * @todo refactor out, findByCriteria should handle this.
     * @since 3.0
     */
    public ValueObject retrieveWoundAssessmentByTimestamp(int patient_id, int wound_id,
            String timestamp) throws DataAccessException {
        PersistenceBroker broker = null;
        WoundAssessmentVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            int time = new Integer(timestamp).intValue();
            time = time - 7200;
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("patient_id", new Integer(patient_id));
            if (wound_id != 0) {
                crit.addEqualTo("wound_id", new Integer(wound_id));
            }
            crit.addGreaterOrEqualThan("created_on", new Integer(time));
            crit.addEqualTo("active", new Integer("1"));
            query = QueryFactory.newQuery(WoundAssessmentVO.class, crit);
            assessmentVO = (WoundAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "Error in AssessmentEachwoundDAO.findByPK(): "
                    + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return assessmentVO;
    }
    /**
     * Finds the # of assessments by treatment location.
     *
     * @param treatment_id  represents the treatment_location
     * @since 3.0
     */
    public int getAssessmentCount(int treatment_id)
            throws com.pixalere.common.DataAccessException {
        PersistenceBroker broker = null;
        int count = 0;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            com.pixalere.patient.dao.PatientDAO bd = new com.pixalere.patient.dao.PatientDAO();
            PatientAccountVO vo = new PatientAccountVO();
            vo.setTreatment_location_id(new Integer(treatment_id + ""));
            vo.setCurrent_flag(new Integer(1));
            Collection<PatientAccountVO> patients = bd.findAllByCriteria(vo, true);
            for (PatientAccountVO p : patients) {
                WoundAssessmentVO v = new WoundAssessmentVO();
                v.setPatient_id(p.getPatient_id());
                v.setActive(new Integer(1));
                QueryByCriteria query = new QueryByCriteria(v);
                count = count + broker.getCount(query);
            }
            return count;
        } catch (ConnectionLocatorException e) {
            logs.error(
                    "PersistanceBrokerException thrown in PatientDAO.findByPK(): "
                    + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): "
                    + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
    }
    /**
     * PlaceHolder
     *
     */
    public void delete(ValueObject delete) {
        PersistenceBroker broker = null;
        try {
            // NOT Used
        } catch (Exception ex) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");
    }
}
