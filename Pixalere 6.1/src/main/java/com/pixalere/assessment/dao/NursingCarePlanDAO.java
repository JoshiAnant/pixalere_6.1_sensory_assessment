package com.pixalere.assessment.dao;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import com.pixalere.common.ApplicationException;
import java.util.Collection;
import java.util.Iterator;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  nursing_care_plan    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class NursingCarePlanDAO implements DataAccessObject {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(NursingCarePlanDAO.class);
    
    public NursingCarePlanDAO(){
        
    }
    /**
     * Finds a all nursing_care_plan records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(NursingCarePlanVO ProfessionalVO) throws
            DataAccessException {
        logs.info("***********************Entering NursingCarePlanDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        NursingCarePlanVO returnVO = null;
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new NursingCarePlanVO();
            QueryByCriteria query = new QueryByCriteria(ProfessionalVO);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in NursingCarePlanDAO.findAllByCriteria(): " +
                    e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in NursingCarePlanDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) broker.close();
        }
        
        logs.info(
                "***********************Leaving NursingCarePlanDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds all assessment_eachwound record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param dcf  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllDressingsByCriteriaWithinRange(DressingChangeFrequencyVO dcf, java.util.Date start_date, java.util.Date end_date) throws DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("active", new Integer(1));
            if(dcf.getWound_id()!=null){
                crit.addEqualTo("wound_id", dcf.getWound_id());
            }if(dcf.getAlpha_id()!=null){
                crit.addEqualTo("alpha_id", dcf.getAlpha_id());
            }
            if (start_date!=null) {
                crit.addGreaterOrEqualThan("created_on", com.pixalere.utils.PDate.getDate(start_date,true));
            }
            if (end_date!=null) {
                crit.addLessOrEqualThan("created_on", com.pixalere.utils.PDate.getDate(end_date,true));
            }
            query = QueryFactory.newQuery(DressingChangeFrequencyVO.class, crit);
            query.addOrderByDescending("created_on");
            returnVO = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in AssessmentEachwoundDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds all nursing_care_plan record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param dcf  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 6.0
     */
    public Collection findAllNCPByCriteriaWithinRange(NursingCarePlanVO dcf, int start_date, int end_date) throws DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("active", new Integer(1));
            if(dcf.getWound_id()!=null){
                crit.addEqualTo("wound_id", dcf.getWound_id());
            }else if(dcf.getWound_id()!=null){
                crit.addEqualTo("wound_profile_type_id", dcf.getWound_profile_type_id());
            }
            if (start_date>0) {
                crit.addGreaterOrEqualThan("id", start_date);
            }
            if (end_date>0) {
                crit.addLessOrEqualThan("id", end_date);
            }
            query = QueryFactory.newQuery(NursingCarePlanVO.class, crit);
            query.addOrderByDescending("created_on");
            returnVO = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in AssessmentEachwoundDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a all dcf records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(DressingChangeFrequencyVO p) throws
            DataAccessException {
        logs.info("***********************Entering NursingCarePlanDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        DressingChangeFrequencyVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new DressingChangeFrequencyVO();
            QueryByCriteria query = new QueryByCriteria(p);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in NursingCarePlanDAO.findAllByCriteria(): " +
                    e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in NursingCarePlanDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) broker.close();
        }
        logs.info(
                "***********************Leaving NursingCarePlanDAO.findAllByCriteria()***********************");
        return results;
    }
    
    /**
     * Finds an nursing_care_plan record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(NursingCarePlanVO vo) throws DataAccessException{
        logs.info("***********************Entering NursingCarePlanDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        NursingCarePlanVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new NursingCarePlanVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("created_on");
            returnVO = (NursingCarePlanVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            logs.error("ServiceLocatorException thrown in NursingCarePlanDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in NursingCarePlanDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        logs.info("***********************Leaving NursingCarePlanDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an nursing_care_plan record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(DressingChangeFrequencyVO vo) throws DataAccessException{
        logs.info("***********************Entering NursingCarePlanDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        DressingChangeFrequencyVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new DressingChangeFrequencyVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("created_on");
            returnVO = (DressingChangeFrequencyVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            logs.error("ServiceLocatorException thrown in NursingCarePlanDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in NursingCarePlanDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        logs.info("***********************Leaving NursingCarePlanDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a single nursing_care_plan record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the nursing_care_plan table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int id) throws DataAccessException {
        logs.info("********* Entering the NursingCarePlanDAO.findByPK****************");
        PersistenceBroker broker = null;
        NursingCarePlanVO assessmentVO = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new NursingCarePlanVO();
            assessmentVO.setId(new Integer(id));
            Query query = new QueryByCriteria(assessmentVO);
            assessmentVO = (NursingCarePlanVO) broker.getObjectByQuery(query);
        }catch(ConnectionLocatorException e){
            logs.error("PersistanceBrokerException thrown in NursingCarePlanDAO.findByPK(): "+ e.toString(),e);
            throw new DataAccessException("Error in NursingCarePlanDAO.findByPK(): "+e.toString(),e);
        }finally {
            if(broker!=null)
                broker.close();
        }
        logs.info("********* Done the NursingCarePlanDAO.findByPK");
        return assessmentVO;
    }
    /**
     * Finds a single nursing_care_plan record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @todo find out if this method is even used, i suspect not.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findLastNursingCarePlan(String wound_id) throws DataAccessException {
        logs.info("********* Entering the NursingCarePlanDAO.getLastAssessment****************");
        PersistenceBroker broker = null;
        NursingCarePlanVO vo = null;
        Criteria crit=null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            crit=new Criteria();
            crit.addEqualTo("active",new Integer(1));
            crit.addEqualTo("wound_id",new Integer(wound_id));
            
            
            QueryByCriteria query = QueryFactory.newQuery(NursingCarePlanVO.class,crit);
            query.addOrderByDescending("id");
            vo = (NursingCarePlanVO) broker.getObjectByQuery(query);
        }catch(ConnectionLocatorException e){
            logs.error("PersistanceBrokerException thrown in NursingCarePlanDAO.findByPK(): "+ e.toString(),e);
            throw new DataAccessException("Error in NursingCarePlanDAO.findByPK(): "+e.toString(),e);
        }finally {
            if(broker!=null)
                broker.close();
        }
        logs.info("********* Done the NursingCarePlanDAO.findByPK");
        return vo;
    }
    
    public void insert(ValueObject insert){
        //Not needed.
        
    }
    /**
     * Updates a single nursing_care_plan record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the nursing_care_plan table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void update(ValueObject update){
        logs.info("************Entering NursingCarePlanDAO.update()************");
        PersistenceBroker broker=null;
        try{
            NursingCarePlanVO assessment =(NursingCarePlanVO) update;
            if(assessment!=null && assessment.getDeleted() == null){
                assessment.setDeleted(0);
            }
            
            broker=ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in NursingCarePlanDAO.update(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in NursingCarePlanDAO.update(): "+e.toString(),e);
            
        } catch (ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with NursingCarePlanDAO.update()*************");
        
        
    }
    public void updateDcf(ValueObject update){
        logs.info("************Entering NursingCarePlanDAO.update()************");
        PersistenceBroker broker=null;
        try{
            DressingChangeFrequencyVO assessment =(DressingChangeFrequencyVO) update;
            broker=ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in NursingCarePlanDAO.update(): "+e.toString(),e);
            e.printStackTrace();
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in NursingCarePlanDAO.update(): "+e.toString(),e);
        } catch (ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with NursingCarePlanDAO.update()*************");
    }
    /**
     * Deletes nursing_care_plan records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    
    public void delete(ValueObject delete){
        logs.info("************Entering NursingCarePlanDAO.delete()************");
        PersistenceBroker broker=null;
        try{
            NursingCarePlanVO assessment =(NursingCarePlanVO) delete;
            Collection<NursingCarePlanVO> result=findAllByCriteria(assessment);
            broker = ConnectionLocator.getInstance().findBroker();
            for(NursingCarePlanVO tmpResultVO : result){
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
            }
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in NursingCarePlanDAO.delete(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in NursingCarePlanDAO.delete(): "+e.toString(),e);
            
        }  catch (ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }catch (DataAccessException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with NursingCarePlanDAO.delete()*************");
    }
    public void deleteDcf(ValueObject delete){
        logs.info("************Entering NursingCarePlanDAO.delete()************");
        PersistenceBroker broker=null;
        try{
            DressingChangeFrequencyVO assessment =(DressingChangeFrequencyVO) delete;
            Collection<DressingChangeFrequencyVO> result=findAllByCriteria(assessment);
            broker = ConnectionLocator.getInstance().findBroker();
            for(DressingChangeFrequencyVO tmpResultVO : result){
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
            }
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in NursingCarePlanDAO.delete(): "+e.toString(),e);
            e.printStackTrace();
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in NursingCarePlanDAO.delete(): "+e.toString(),e);
        }  catch (ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }catch (DataAccessException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with NursingCarePlanDAO.delete()*************");
    }
}
