package com.pixalere.assessment.dao;
import com.pixalere.utils.Common;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import org.apache.ojb.broker.query.ReportQueryByCriteria;
import java.util.Vector;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.ojb.broker.query.QueryByCriteria;
import java.util.Collection;
import java.util.Iterator;
import java.util.Date;
/**
 * This class is responsible for handling all CRUD logic associated with the
 * assessment_eachwound,assessment_incision,assessment_ostomy,assessment_drain
 * tables.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class AssessmentDAO {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(AssessmentDAO.class);
    public AssessmentDAO() {
    }
    /**
     * Finds the assessment_sinustracts records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see
     * com.pixalere.common.DataAccessObject#findAllByCriteria(com.pixalere.common.ValueObject)
     * @return results all the sinustracts
     * @since 5.0
     */
    public Collection findAllByCriteria(AssessmentSinustractsVO crit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllImagesByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    public Collection findAllByCriteria(AssessmentProductVO crit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllImagesByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     *
     * Deletes a single AssessmentSinustractsVO object into the
     * assessment_sinustracts table. The value object passed
     *
     * in has to be of type AssessmentSinustractsVO.
     *
     *
     *
     * @param delete fields in ValueObject represent fields in the
     * assessment_sinustracts record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     *
     */
    public void deleteSinustract(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentSinustractsVO s = (AssessmentSinustractsVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getId() != null || s.getAssessment_wound_id() != null)) {
                Collection<AssessmentSinustractsVO> result = findAllByCriteria(s);
                for (AssessmentSinustractsVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     *
     * Updates a single AssessmentSinustractsVO object into the
     * assessment_sinustract table. The value object passed
     *
     * in has to be of type AssessmentSinustractsVO.
     *
     *
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_sinustract record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @todo refactor into its own AssessmentDAO
     *
     * @since 3.0
     *
     */
    public void saveSinustract(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentSinustractsVO assessment = (AssessmentSinustractsVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
    /**
     * Finds the assessment_undermining records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see
     * com.pixalere.common.DataAccessObject#findAllByCriteria(com.pixalere.common.ValueObject)
     * @return results all the underminings
     * @since 5.0
     */
    public Collection findAllByCriteria(AssessmentUnderminingVO crit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     *
     * Deletes a single AssessmentUnderminingVO object into the
     * assessment_undermining table. The value object passed
     *
     * in has to be of type AssessmentUnderminingVO.
     *
     *
     *
     * @param delete fields in ValueObject represent fields in the
     * assessment_undermining record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     *
     */
    public void deleteUndermining(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentUnderminingVO s = (AssessmentUnderminingVO) delete;
            if (s != null && (s.getId() != null || s.getAssessment_wound_id() != null)) {
                Collection<AssessmentUnderminingVO> result = findAllByCriteria(s);
                broker = ConnectionLocator.getInstance().findBroker();
                for (AssessmentUnderminingVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     *
     * Updates a single AssessmentUnderminingVO object into the
     * assessment_undermining table. The value object passed
     *
     * in has to be of type AssessmentUnderminingVO.
     *
     *
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_undermining record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @todo refactor into its own AssessmentDAO
     *
     * @since 3.0
     *
     */
    public void saveUndermining(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentUnderminingVO assessment = (AssessmentUnderminingVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
    /**
     * Finds the assessment_woundbed records, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see
     * com.pixalere.common.DataAccessObject#findAllByCriteria(com.pixalere.common.ValueObject)
     *
     * @since 5.0
     */
    public Collection findAllByCriteria(AssessmentWoundBedVO crit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     *
     * Deletes a single AssessmentWoundBedVO object into the assessment_woundbed
     * table. The value object passed
     *
     * in has to be of type AssessmentWoundBedVO.
     *
     *
     *
     * @param delete fields in ValueObject represent fields in the
     * assessment_woundbed record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     *
     */
    public void deleteWoundBed(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentWoundBedVO s = (AssessmentWoundBedVO) delete;
            if (s != null && (s.getId() != null || s.getAssessment_wound_id() != null)) {
                Collection<AssessmentWoundBedVO> result = findAllByCriteria(s);
                broker = ConnectionLocator.getInstance().findBroker();
                for (AssessmentWoundBedVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     *
     * Updates a single AssessmentUnderminingVO object into the
     * assessment_undermining table. The value object passed
     *
     * in has to be of type AssessmentUnderminingVO.
     *
     *
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_undermining record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @todo refactor into its own AssessmentDAO
     *
     * @since 3.0
     *
     */
    public void saveWoundBed(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentWoundBedVO assessment = (AssessmentWoundBedVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
    /**
     * Finds all assessment_eachwound records with the wound_id value passed in.
     * If no record is found a null value is returned.
     *
     * @param wound_id the wound_profile_id from wound_profiles.
     * @return Collection all AssessmentEachwoundVO records by specific wound.
     *
     * @since 3.0
     */
    public Collection findAllPastAssessments(AbstractAssessmentVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findAllPastAssessment****************");
        Collection results = null;
        PersistenceBroker broker = null;
        Criteria crit = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit = new Criteria();
            crit.addEqualTo("woundAssessment.status", new Integer(1));//get all active assessments
            crit.addEqualTo("deleted", new Integer(0));
            crit.addEqualTo("wound_id", assessmentVO.getWound_id());//by wound
            QueryByCriteria query = null;
            if (assessmentVO instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria(AssessmentEachwoundVO.class, crit);
            } else if (assessmentVO instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria(AssessmentOstomyVO.class, crit);
            } else if (assessmentVO instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria(AssessmentIncisionVO.class, crit);
            } else if (assessmentVO instanceof AssessmentDrainVO) {
                query = new QueryByCriteria(AssessmentDrainVO.class, crit);
            } else if (assessmentVO instanceof AssessmentSkinVO) {
                query = new QueryByCriteria(AssessmentSkinVO.class, crit);
            } else if (assessmentVO instanceof AssessmentBurnVO) {
                query = new QueryByCriteria(AssessmentBurnVO.class, crit);
            }
            query.addOrderByDescending("assessment_id");
            query.addOrderByAscending("alpha_id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findAllPastAssessments(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findAllPastAssessments(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findAllPastAssessments");
        return results;
    }
    /**
     * Finds all assessment_eachwound records with the wound_id value passed in,
     * in descending order. If no record is found a null value is returned.
     *
     * @param wound_id the wound_profile_id from wound_profiles.
     * @return Collection all AssessmentEachwoundVO records by specific wound.
     * @todo refactor this method out
     * @since 3.0
     */
    public Collection findAllPastAssessmentsByWoundIdDescending(AbstractAssessmentVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findAllPastAssessment****************");
        Collection results = null;
        //AbstractAssessmentVO assVO = new AssessmentEachwoundVO();
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //256 = Active    257 = Closed
            //assessmentVO.setActive(new Integer(com.pixalere.utils.Common.ACTIVE));
            assessmentVO.setActive(new Integer(1));
            if (assessmentVO != null && !Common.getConfig("showDeleted").equals("1")) {
                assessmentVO.setDeleted(0);
            }
            QueryByCriteria query = null;
            if (assessmentVO instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria((AssessmentEachwoundVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria((AssessmentOstomyVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria((AssessmentIncisionVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentDrainVO) {
                query = new QueryByCriteria((AssessmentDrainVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentSkinVO) {
                query = new QueryByCriteria((AssessmentSkinVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentBurnVO) {
                query = new QueryByCriteria((AssessmentBurnVO) assessmentVO);
            }
            query.addOrderByDescending("assessment_id");
            query.addOrderByAscending("alpha_id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findAllPastAssessmentsByWoundIdDescending(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findAllPastAssessments(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findAllPastAssessmentsByWoundIdDescending");
        return results;
    }
    /**
     * Finds a all assessment_eachwound records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(AbstractAssessmentVO assessmentVO, String sort, boolean desc, int limit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //returnVO = new AssessmentEachwoundVO();
            QueryByCriteria query = null;
            if (assessmentVO != null && !Common.getConfig("showDeleted").equals("1")) {
                assessmentVO.setDeleted(0);
            }
            if (assessmentVO instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria((AssessmentEachwoundVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria((AssessmentOstomyVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria((AssessmentIncisionVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentDrainVO) {
                query = new QueryByCriteria((AssessmentDrainVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentSkinVO) {
                query = new QueryByCriteria((AssessmentSkinVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentBurnVO) {
                query = new QueryByCriteria((AssessmentBurnVO) assessmentVO);
            }
            if (desc == false) {
                query.addOrderByAscending(sort);
            } else if (sort.equals("created_on")) {
                query.addOrderByDescending("woundAssessment.created_on");
                query.addOrderByDescending("id");
            } else if (sort.equals("visited_on")) {
                query.addOrderByDescending("woundAssessment.visited_on");
                query.addOrderByDescending("id");
            } else if (sort != null) {
                query.addOrderByDescending(sort);
            }
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //System.out.println("RESULTS"+results.size());
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all assessment_eachwound records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo Refactor this method out..
     * @since 3.0
     */
    public Collection findAllByCriteriaDescending(AbstractAssessmentVO assessmentVO) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllByCriteriaDescending()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = null;
            if (assessmentVO != null && !Common.getConfig("showDeleted").equals("1")) {
                assessmentVO.setDeleted(0);
            }
            if (assessmentVO instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria((AssessmentEachwoundVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria((AssessmentOstomyVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria((AssessmentIncisionVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentDrainVO) {
                query = new QueryByCriteria((AssessmentDrainVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentSkinVO) {
                query = new QueryByCriteria((AssessmentSkinVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentBurnVO) {
                query = new QueryByCriteria((AssessmentBurnVO) assessmentVO);
            }
            query.addOrderByDescending("woundAssessment.created_on");
            query.addOrderByDescending("id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteriaDescending(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteriaDescending()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info(
                "***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an assessment_eachwound record, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(AbstractAssessmentVO assessmentVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AbstractAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentEachwoundVO();
            QueryByCriteria query = null;
            if (assessmentVO != null && !Common.getConfig("showDeleted").equals("1")) {
                assessmentVO.setDeleted(0);
            }
            if (assessmentVO instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria((AssessmentEachwoundVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria((AssessmentOstomyVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria((AssessmentIncisionVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentDrainVO) {
                query = new QueryByCriteria((AssessmentDrainVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentSkinVO) {
                query = new QueryByCriteria((AssessmentSkinVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentBurnVO) {
                query = new QueryByCriteria((AssessmentBurnVO) assessmentVO);
            }
            query.addOrderByDescending("created_on");
            query.addOrderByDescending("id");
            returnVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in AssessmentEachwoundDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an assessment_eachwound record, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(AbstractAssessmentVO assessmentVO) throws DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AbstractAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentEachwoundVO();
            QueryByCriteria query = null;
            if (assessmentVO instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria((AssessmentEachwoundVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria((AssessmentOstomyVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria((AssessmentIncisionVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentDrainVO) {
                query = new QueryByCriteria((AssessmentDrainVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentBurnVO) {
                query = new QueryByCriteria((AssessmentBurnVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentSkinVO) {
                query = new QueryByCriteria((AssessmentSkinVO) assessmentVO);
            }
            query.addOrderByDescending("created_on");
            query.addOrderByDescending("id");
            returnVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in AssessmentEachwoundDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an assessment_eachwound record, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteriaWithinRange(AbstractAssessmentVO ass, Date start_date, Date end_date) throws DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AbstractAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("active", new Integer(1));
            crit.addEqualTo("deleted", new Integer(0));
            if (ass.getWound_id() != null) {
                crit.addEqualTo("wound_id", ass.getWound_id());
            }
            if (ass.getAlpha_id() != null) {
                crit.addEqualTo("alpha_id", ass.getAlpha_id());
            }
            if (ass.getFull_assessment() != null) {
                crit.addEqualTo("full_assessment", ass.getFull_assessment());
            }
            if (start_date != null) {
                crit.addGreaterOrEqualThan("created_on", start_date);
            }
            if (end_date != null) {
                crit.addLessOrEqualThan("created_on", end_date);
            }
            if (ass instanceof AssessmentEachwoundVO) {
                query = QueryFactory.newQuery(AssessmentEachwoundVO.class, crit);
            } else if (ass instanceof AssessmentIncisionVO) {
                query = QueryFactory.newQuery(AssessmentIncisionVO.class, crit);
            } else if (ass instanceof AssessmentBurnVO) {
                query = QueryFactory.newQuery(AssessmentBurnVO.class, crit);
            } else if (ass instanceof AssessmentDrainVO) {
                query = QueryFactory.newQuery(AssessmentDrainVO.class, crit);
            } else if (ass instanceof AssessmentSkinVO) {
                query = QueryFactory.newQuery(AssessmentSkinVO.class, crit);
            } else if (ass instanceof AssessmentOstomyVO) {
                query = QueryFactory.newQuery(AssessmentOstomyVO.class, crit);
            }
            query.addOrderByDescending("created_on");
            returnVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in AssessmentEachwoundDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds all assessment_eachwound record, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param valueobject fields in ValueObject which are not null will be used
     * as parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public Collection findAllByCriteriaWithinRange(AbstractAssessmentVO ass, Date start_date, Date end_date) throws DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("active", new Integer(1));
            crit.addEqualTo("deleted", new Integer(0));
            if (ass.getWound_id() != null) {
                crit.addEqualTo("wound_id", ass.getWound_id());
            }
            if (ass.getAlpha_id() != null) {
                crit.addEqualTo("alpha_id", ass.getAlpha_id());
            }
            if (ass.getFull_assessment() != null) {
                crit.addEqualTo("full_assessment", ass.getFull_assessment());
            }
            if (start_date != null) {
                crit.addGreaterOrEqualThan("created_on", start_date);
            }
            if (end_date != null) {
                crit.addLessOrEqualThan("created_on", end_date);
            }
            if (ass instanceof AssessmentEachwoundVO) {
                query = QueryFactory.newQuery(AssessmentEachwoundVO.class, crit);
            } else if (ass instanceof AssessmentIncisionVO) {
                query = QueryFactory.newQuery(AssessmentIncisionVO.class, crit);
            } else if (ass instanceof AssessmentBurnVO) {
                query = QueryFactory.newQuery(AssessmentBurnVO.class, crit);
            } else if (ass instanceof AssessmentDrainVO) {
                query = QueryFactory.newQuery(AssessmentDrainVO.class, crit);
            } else if (ass instanceof AssessmentSkinVO) {
                query = QueryFactory.newQuery(AssessmentSkinVO.class, crit);
            } else if (ass instanceof AssessmentOstomyVO) {
                query = QueryFactory.newQuery(AssessmentOstomyVO.class, crit);
            }
            query.addOrderByDescending("created_on");
            returnVO = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in AssessmentEachwoundDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a single assessment_eachwound record, as specified by the primary
     * key value passed in. If no record is found a null value is returned.
     *
     * @param assessment_id the primary key of the assessment_eachwound table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo make parameter asssessment_id to be primary key as its incorrect
     * @since 3.0
     */
    public ValueObject findByPK(AbstractAssessmentVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findByPK****************");
        PersistenceBroker broker = null;
        AbstractAssessmentVO assessVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = null;
            assessmentVO.setDeleted(0);
            if (assessmentVO instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria((AssessmentEachwoundVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria((AssessmentOstomyVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria((AssessmentIncisionVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentDrainVO) {
                query = new QueryByCriteria((AssessmentDrainVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentSkinVO) {
                query = new QueryByCriteria((AssessmentSkinVO) assessmentVO);
            } else if (assessmentVO instanceof AssessmentBurnVO) {
                query = new QueryByCriteria((AssessmentBurnVO) assessmentVO);
            }
            query.addOrderByDescending("id");
            assessVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findByPK");
        return assessVO;
    }
    /* Finds a single assessment_eachwound record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param assessment_id the assessment_id of the assessment_eachwound table.
     * @param alpha_id the alpha_id of the assessment_eachwound table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public ValueObject findLastAssessment(AbstractAssessmentVO vo) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.getLastAssessment****************");
        PersistenceBroker broker = null;
        AbstractAssessmentVO results = null;
        Criteria crit = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (vo != null) {
                vo.setDeleted(0);
            }
            results = (AbstractAssessmentVO) findByCriteria(vo);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return results;
    }
    /* Finds all assessment_eachwound records, as specified by the values
     * passed in.  If no record is found a null value is returned.
     *
     * @param assessment_id the assessment_id of the assessment_eachwound table.
     * @param alpha_id the alpha_id of the assessment_eachwound table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this out, to use findallByCriteria instead.
     * @since 3.0
     */
    public Collection findLastAssessments(AbstractAssessmentVO vo) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.getLastAssessment****************");
        PersistenceBroker broker = null;
        Collection results = null;
        Criteria crit = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            results = findAllByCriteria(vo, "id", true, 0);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return results;
    }
    /* Finds all nursing_fixes records, as specified by the values
     * passed in.  If no record is found a null value is returned.
     *
     * @param assessment_id the assessment_id of the assessment_eachwound table.
     * @param alpha_id the alpha_id of the assessment_eachwound table.
     * @param field the type of field which is being updated. For instance, if someone wants
     * a to find all nursing_fixes by field 'wound_bed'.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public Collection findNursingFixes(String id, String field) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.getLastAssessment****************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            com.pixalere.assessment.bean.NursingFixesVO vo = new com.pixalere.assessment.bean.NursingFixesVO();
            vo.setRow_id(new Integer(id));
            vo.setField(field);
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return results;
    }
    /* Finds all assessment_eachwound records, as specified by the values
     * passed in.  If no record is found a null value is returned.
     *
     * @param assessment_id the assessment_id of the assessment_eachwound table.
     * @param alpha_id the alpha_id of the assessment_eachwound table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor out to use findAllByCriteria instead
     * @since 3.0
     */
    public Collection findLastAssessmentsById(AbstractAssessmentVO vo) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.getLastAssessment****************");
        PersistenceBroker broker = null;
        Collection results = null;
        Criteria crit = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //vo.setAlpha_id(new Integer(alpha_id));
            results = findAllByCriteria(vo, "id", true, 0);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return results;
    }
    /* Finds all assessment_eachwound records, as specified by the values
     * passed in.  If no record is found a null value is returned.
     *
     * @param assessment_id the assessment_id of the assessment_eachwound table.
     * @param alpha_id the alpha_id of the assessment_eachwound table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this out to use findAllByCriteria instead.
     * @since 3.0
     */
    public Collection findLastAlphaAssessments(AbstractAssessmentVO vo) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findLastAssessmentsWithProducts****************");
        PersistenceBroker broker = null;
        Collection results = null;
        Criteria crit = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = null;
            if (vo != null && !Common.getConfig("showDeleted").equals("1")) {
                vo.setDeleted(0);
            }
            if (vo instanceof AssessmentEachwoundVO) {
                query = new QueryByCriteria((AssessmentEachwoundVO) vo);
            } else if (vo instanceof AssessmentOstomyVO) {
                query = new QueryByCriteria((AssessmentOstomyVO) vo);
            } else if (vo instanceof AssessmentIncisionVO) {
                query = new QueryByCriteria((AssessmentIncisionVO) vo);
            } else if (vo instanceof AssessmentDrainVO) {
                query = new QueryByCriteria((AssessmentDrainVO) vo);
            } else if (vo instanceof AssessmentSkinVO) {
                query = new QueryByCriteria((AssessmentSkinVO) vo);
            } else if (vo instanceof AssessmentBurnVO) {
                query = new QueryByCriteria((AssessmentBurnVO) vo);
            }
            //results=findAllByCriteria(vo);
            query.addOrderByDescending("id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return results;
    }
    /**
     * Finds an assessment_eachwound record, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this method out to use findByCriteria instead
     * @since 3.0
     */
    public ValueObject findLastActiveAssessment(AbstractAssessmentVO vo, boolean includeNotAssessed) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findLastActiveAssessment****************");
        PersistenceBroker broker = null;
        Criteria crit = null;
        AbstractAssessmentVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            /*vo.setActive(1);
             AbstractAssessmentVO ass=(AbstractAssessmentVO)findByCriteria(vo);
             return ass;*/
            crit = new Criteria();
            crit.addEqualTo("active", new Integer(1));//get all active assessments
            crit.addEqualTo("deleted", new Integer(0));
            if (vo.getWound_id() != null) {
                crit.addEqualTo("wound_id", vo.getWound_id());//by wound
            }
            if (vo.getAlpha_id() != null) {
                crit.addEqualTo("alpha_id", vo.getAlpha_id());
            }
            if (vo.getFull_assessment() != null) {
                crit.addEqualTo("full_assessment", vo.getFull_assessment());
            }
            if (!includeNotAssessed) {
                crit.addNotEqualTo("full_assessment", new Integer(2));//with products
                crit.addNotEqualTo("full_assessment", new Integer(3));
            }
            if (vo instanceof AssessmentEachwoundVO) {
                QueryByCriteria query = QueryFactory.newQuery(AssessmentEachwoundVO.class, crit);
                query.addOrderByDescending("visited_on");
                assessmentVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
            } else if (vo instanceof AssessmentIncisionVO) {
                QueryByCriteria query = QueryFactory.newQuery(AssessmentIncisionVO.class, crit);
                query.addOrderByDescending("visited_on");
                assessmentVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
            } else if (vo instanceof AssessmentOstomyVO) {
                QueryByCriteria query = QueryFactory.newQuery(AssessmentOstomyVO.class, crit);
                query.addOrderByDescending("visited_on");
                assessmentVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
            } else if (vo instanceof AssessmentDrainVO) {
                QueryByCriteria query = QueryFactory.newQuery(AssessmentDrainVO.class, crit);
                query.addOrderByDescending("visited_on");
                assessmentVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
            } else if (vo instanceof AssessmentSkinVO) {
                QueryByCriteria query = QueryFactory.newQuery(AssessmentSkinVO.class, crit);
                query.addOrderByDescending("visited_on");
                assessmentVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
            } else if (vo instanceof AssessmentBurnVO) {
                QueryByCriteria query = QueryFactory.newQuery(AssessmentBurnVO.class, crit);
                query.addOrderByDescending("visited_on");
                assessmentVO = (AbstractAssessmentVO) broker.getObjectByQuery(query);
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("Error in AssessmentDAO.findLastActiveAssessment: " + e.getMessage());
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
            //System.out.println("Error in AssessmentDAO.findLastActiveAssessment: " + e.getMessage());
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return assessmentVO;
    }
    /**
     * Inserts a single AssessmentEachwoundVO object into the
     * assessment_eachwound table. The value object passed in has to be of type
     * AssessmentEachwoundVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * assessment_eachwound record.
     * @see
     * com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void insert(ValueObject insert) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            AbstractAssessmentVO assessment = (AbstractAssessmentVO) insert;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            if (assessment != null && assessment.getDeleted() == null) {
                assessment.setDeleted(0);
            }
            if (assessment.getAssessment_id() != null) {
                if (insert instanceof AssessmentEachwoundVO) {
                    broker.store((AssessmentEachwoundVO) assessment);
                    broker.commitTransaction();
                } else if (insert instanceof AssessmentOstomyVO) {
                    broker.store((AssessmentOstomyVO) assessment);
                    broker.commitTransaction();
                } else if (insert instanceof AssessmentIncisionVO) {
                    broker.store((AssessmentIncisionVO) assessment);
                    broker.commitTransaction();
                } else if (insert instanceof AssessmentDrainVO) {
                    broker.store((AssessmentDrainVO) assessment);
                    broker.commitTransaction();
                } else if (insert instanceof AssessmentSkinVO) {
                    broker.store((AssessmentSkinVO) assessment);
                    broker.commitTransaction();
                } else if (insert instanceof AssessmentBurnVO) {
                    broker.store((AssessmentBurnVO) assessment);
                    broker.commitTransaction();
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.insert()*************");
    }
    /**
     * Updates a single AssessmentEachwoundVO object into the
     * assessment_eachwound table. The value object passed in has to be of type
     * AssessmentEachwoundVO.
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_eachwound record.
     * @see
     * com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void update(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AbstractAssessmentVO assessment = (AbstractAssessmentVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            if (assessment != null && assessment.getDeleted() == null) {
                assessment.setDeleted(0);
            }
            if (assessment.getAssessment_id() != null) {
                if (assessment instanceof AssessmentEachwoundVO) {
                    broker.store((AssessmentEachwoundVO) assessment);
                    broker.commitTransaction();
                } else if (assessment instanceof AssessmentOstomyVO) {
                    broker.store((AssessmentOstomyVO) assessment);
                    broker.commitTransaction();
                } else if (assessment instanceof AssessmentIncisionVO) {
                    broker.store((AssessmentIncisionVO) assessment);
                    broker.commitTransaction();
                } else if (assessment instanceof AssessmentDrainVO) {
                    broker.store((AssessmentDrainVO) assessment);
                    broker.commitTransaction();
                } else if (assessment instanceof AssessmentSkinVO) {
                    broker.store((AssessmentSkinVO) assessment);
                    broker.commitTransaction();
                } else if (assessment instanceof AssessmentBurnVO) {
                    broker.store((AssessmentBurnVO) assessment);
                    broker.commitTransaction();
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
    public void updateProduct(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentProductVO assessment = (AssessmentProductVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.updateProduct(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            e.printStackTrace();
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
    /**
     * Deletes assessment_eachwound records, as specified by the value object
     * passed in. Fields in Value Object which are not null will be used as
     * parameters in the SQL statement. Retrieves all records by criteria, then
     * deletes them.
     *
     * @param deleteRecord the value object which specifies which records should
     * be deleted
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AbstractAssessmentVO assess = null;
            NursingCarePlanDAO ndao = new NursingCarePlanDAO();
            if (delete instanceof AssessmentEachwoundVO) {
                assess = (AssessmentEachwoundVO) delete;
            } else if (delete instanceof AssessmentIncisionVO) {
                assess = (AssessmentIncisionVO) delete;
            } else if (delete instanceof AssessmentOstomyVO) {
                assess = (AssessmentOstomyVO) delete;
            } else if (delete instanceof AssessmentDrainVO) {
                assess = (AssessmentDrainVO) delete;
            } else if (delete instanceof AssessmentSkinVO) {
                assess = (AssessmentSkinVO) delete;
            } else if (delete instanceof AssessmentBurnVO) {
                assess = (AssessmentBurnVO) delete;
            }
            Collection<AbstractAssessmentVO> result = findAllByCriteria(assess, "id", true, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (AbstractAssessmentVO assessment : result) {
                broker.beginTransaction();
                if (assess instanceof AssessmentEachwoundVO) {
                    broker.delete((AssessmentEachwoundVO) assessment);
                    broker.commitTransaction();
                } else if (assess instanceof AssessmentOstomyVO) {
                    broker.delete((AssessmentOstomyVO) assessment);
                    broker.commitTransaction();
                } else if (assess instanceof AssessmentIncisionVO) {
                    broker.delete((AssessmentIncisionVO) assessment);
                    broker.commitTransaction();
                } else if (assess instanceof AssessmentDrainVO) {
                    broker.delete((AssessmentDrainVO) assessment);
                    broker.commitTransaction();
                } else if (assess instanceof AssessmentSkinVO) {
                    broker.delete((AssessmentSkinVO) assessment);
                    broker.commitTransaction();
                } else if (assess instanceof AssessmentBurnVO) {
                    broker.delete((AssessmentBurnVO) assessment);
                    broker.commitTransaction();
                }
                AssessmentProductVO p = new AssessmentProductVO();
                p.setPatient_id(assessment.getPatient_id());
                p.setAlpha_id(assessment.getAlpha_id());
                p.setAssessment_id(assessment.getAssessment_id());
                deleteProducts(p);
                AssessmentImagesVO img = new AssessmentImagesVO();
                img.setPatient_id(assessment.getPatient_id());
                img.setAlpha_id(assessment.getAlpha_id());
                img.setAssessment_id(assessment.getAssessment_id());
                deleteImages(img);
            }
        } catch (PersistenceBrokerException e) {
            if (broker != null) {
                broker.abortTransaction();
            }
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     * Updates a single AssessmentImagesVO object into the assessment_images
     * table. The value object passed in has to be of type AssessmentImagesVO.
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_images record.
     * @see
     * com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentImagesDAO
     * @since 3.0
     */
    public void insertImages(ValueObject insert) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.insertImages()************");
        PersistenceBroker broker = null;
        try {
            AssessmentImagesVO assessment = (AssessmentImagesVO) insert;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.insertImages(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.insertImages(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.insertImages()*************");
    }
    /**
     * Updates a single NursingFixesVO object into the nursing_fixes table. The
     * value object passed in has to be of type NursingFixesVO.
     *
     * @param update fields in ValueObject represent fields in the nursing_fixes
     * record.
     * @see
     * com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     * @todo refactor into its own NursingFixesDAO
     * @since 3.0
     */
    public void insertNursingFixes(ValueObject insert) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.insertImages()************");
        PersistenceBroker broker = null;
        try {
            com.pixalere.assessment.bean.NursingFixesVO vo = (com.pixalere.assessment.bean.NursingFixesVO) insert;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(vo);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.insertImages(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
            //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.insertImages(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.insertImages()*************");
    }
    /**
     * Updates a single AssessmentImagesVO object into the assessment_images
     * table. The value object passed in has to be of type AssessmentImagesVO.
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_images record.
     * @see
     * com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentImagesDAO
     * @since 3.0
     */
    public void updateImages(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.updateImages()************");
        PersistenceBroker broker = null;
        try {
            AssessmentImagesVO assessment = (AssessmentImagesVO) update;
            AssessmentImagesVO tmpAssess = (AssessmentImagesVO) findImageByCriteria(assessment);
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            if (tmpAssess == null) {
                broker.store(assessment);
                broker.commitTransaction();
            } else {
                tmpAssess.setImages(assessment.getImages());
                broker.store(tmpAssess);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.updateImages(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.updateImages(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.updateImages()*************");
    }
    /**
     * Deletes a single AssessmentImagesVO object into the assessment_images
     * table. The value object passed in has to be of type AssessmentImagesVO.
     *
     * @param delete fields in ValueObject represent fields in the
     * assessment_images record.
     * @see
     * com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentImagesDAO
     * @since 3.0
     */
    public void deleteImages(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentImagesVO images = (AssessmentImagesVO) delete;
            Collection<AssessmentImagesVO> result = findAllImagesByCriteria(images);
            broker = ConnectionLocator.getInstance().findBroker();
            for (AssessmentImagesVO imageResultVO : result) {
                broker.beginTransaction();
                broker.delete(imageResultVO);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     * Deletes a single AssessmentProductVO object into the assessment_product
     * table. The value object passed in has to be of type AssessmentProductVO.
     *
     * @param delete fields in ValueObject represent fields in the
     * assessment_product record.
     * @see
     * com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     * @since 3.0
     */
    public void deleteProducts(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentProductVO products = (AssessmentProductVO) delete;
            
            Collection<AssessmentProductVO> result = findAllProductsByCriteria(products);
            broker = ConnectionLocator.getInstance().findBroker();
            for (AssessmentProductVO prodResultVO : result) {
                broker.beginTransaction();
                broker.delete(prodResultVO);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    public Collection findAllImagesByCriteria(AssessmentImagesVO ProfessionalVO) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllImagesByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        AssessmentImagesVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentImagesVO();
            QueryByCriteria query = new QueryByCriteria(ProfessionalVO);
            query.addOrderByAscending("alpha_id");
            query.addOrderByAscending("which_image");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllImagesByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllImagesByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllImagesByCriteria()***********************");
        return results;
    }
    public Collection findAllProductsByCriteria(AssessmentProductVO ProfessionalVO) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllProductsByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        AssessmentProductVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentProductVO();
            QueryByCriteria query = new QueryByCriteria(ProfessionalVO);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllProductsByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllProductsByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllProductsByCriteria()***********************");
        return results;
    }
    /**
     * Finds all AssessmentImagesVO records, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param assessment_id fields in assessment_images by assessment_id
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this method out into its own AssessmentImagesDAO
     * @since 3.0
     */
    public Collection findAllImagesByAssessmentId(int assessment_id, Integer wound_profile_type_id) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findImagesByPK****************");
        PersistenceBroker broker = null;
        Collection results = null;
        AssessmentImagesVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new AssessmentImagesVO();
            assessmentVO.setAssessment_id(new Integer(assessment_id));
            if (wound_profile_type_id != null) {
                assessmentVO.setWound_profile_type_id(wound_profile_type_id);
            }
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByAscending("alpha_id");
            query.addOrderByAscending("which_image");
            results = broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findImagesByPK");
        return results;
    }
    /**
     * Finds an AssessmentImagesVO record, as specified by the values passed in.
     * If no record is found a null value is returned.
     *
     * @param assessment_id which wound_assessment
     * @param alpha_id which alpha assessment
     * @param image specifies which image you want (#1, or #2)
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this method out into its own AssessmentImagesDAO
     * @since 3.0
     */
    public ValueObject findImageByCriteria(AssessmentImagesVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findImagesByPK****************");
        PersistenceBroker broker = null;
        AssessmentImagesVO img = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("woundAssessment.created_on");
            img = (AssessmentImagesVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findImagesByPK");
        return img;
    }
    /**
     * Finds an AssessmentAntibioticVO record, as specified by the values passed
     * in. If no record is found a null value is returned.
     *
     *
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this method out into its own AssessmentDAO
     * @since 3.0
     */
    public ValueObject findByCriteria(AssessmentAntibioticVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findImagesByPK****************");
        PersistenceBroker broker = null;
        AssessmentAntibioticVO img = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("id");
            img = (AssessmentAntibioticVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findImagesByPK");
        return img;
    }
    /**
     * Finds an AssessmentAntibioticVO record, as specified by the values passed
     * in. If no record is found a null value is returned.
     *
     *
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this method out into its own AssessmentDAO
     * @since 3.0
     */
    public ValueObject findByCriteria(AssessmentMucocSeperationsVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentDAO.findByPK****************");
        PersistenceBroker broker = null;
        AssessmentMucocSeperationsVO img = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("id");
            img = (AssessmentMucocSeperationsVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentDAO.findByPK");
        return img;
    }
    /**
     * Finds an AssessmentProductVO record, as specified by the values passed
     * in. If no record is found a null value is returned.
     *
     *
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this method out into its own AssessmentDAO
     * @since 4.2
     */
    public ValueObject findProductByCriteria(AssessmentProductVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findImagesByPK****************");
        PersistenceBroker broker = null;
        AssessmentProductVO img = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("id");
            img = (AssessmentProductVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findImagesByPK");
        return img;
    }
    /**
     * Finds an NursingFixesVO record, as specified by the values passed in. If
     * no record is found a null value is returned.
     *
     * @param object
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 4.2
     */
    public ValueObject findNursingFixesByCriteria(NursingFixesVO assessmentVO) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findImagesByPK****************");
        PersistenceBroker broker = null;
        NursingFixesVO img = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(assessmentVO);
            query.addOrderByDescending("id");
            img = (NursingFixesVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findImagesByPK");
        return img;
    }
    /**
     * Finds an AssessmentImagesVO record, as specified by the values passed in.
     * If no record is found a null value is returned.
     *
     * @param id find a record by primary key
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @todo refactor this method out into its own AssessmentImagesDAO
     * @since 3.0
     */
    public ValueObject findImageByPK(String id) throws DataAccessException {
        logs.info("********* Entering the AssessmentEachwoundDAO.findImagesByPK****************");
        PersistenceBroker broker = null;
        AssessmentImagesVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new AssessmentImagesVO();
            assessmentVO.setId(new Integer(id));
            Query query = new QueryByCriteria(assessmentVO);
            assessmentVO = (AssessmentImagesVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in AssessmentEachwoundDAO.findImagesByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the AssessmentEachwoundDAO.findImagesByPK");
        return assessmentVO;
    }
    public Vector findAllAssessmentBySearch(String assessmentType) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //crit.addEqualTo("patient_id", new Integer(patient_id));
            //crit.addEqualTo("wound_id", new Integer(wound_id));
            //crit.addEqualTo("visit", 1);
            //crit.addEqualTo("current_flag", 1);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(AssessmentEachwoundVO.class, crit);
            if (assessmentType.equals("A")) {
                q = QueryFactory.newReportQuery(AssessmentEachwoundVO.class, crit);
            }
            if (assessmentType.equals("D")) {
                q = QueryFactory.newReportQuery(AssessmentDrainVO.class, crit);
            }
            if (assessmentType.equals("S")) {
                q = QueryFactory.newReportQuery(AssessmentSkinVO.class, crit);
            }
            if (assessmentType.equals("T")) {
                q = QueryFactory.newReportQuery(AssessmentIncisionVO.class, crit);
            }
            if (assessmentType.equals("O")) {
                q = QueryFactory.newReportQuery(AssessmentOstomyVO.class, crit);
            }
            if (assessmentType.equals("B")) {
                q = QueryFactory.newReportQuery(AssessmentBurnVO.class, crit);
            }
            q.setAttributes(new String[]{"id", "status", "patient_id", "wound_id", "assessment_id", "alpha_id", "wound_profile_type_id", "discharge_reason"});
            q.addOrderByDescending("patient_id");
            q.addOrderByDescending("wound_id");
            q.addOrderByDescending("id");
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            //System.out.println("Exception in ReportDAO: " + e);
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    public Collection findAllByCriteria(AssessmentAntibioticVO crit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Updates a single AssessmentAntibioticVO object into the
     * assessment_antibiotics table. The value object passed in has to be of
     * type AssessmentAntibioticVO.
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_antibioitcs record.
     * @see
     * com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 4.1.2
     */
    public void saveAntibiotic(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentAntibioticVO assessment = (AssessmentAntibioticVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
    /**
     * Deletes a single AssessmentAntibioticVO object into the
     * assessment_antibiotics table. The value object passed in has to be of
     * type AssessmentAntibioticVO.
     *
     * @param delete fields in ValueObject represent fields in the
     * assessment_treatment_arrays record.
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deleteAntibiotic(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentAntibioticVO s = (AssessmentAntibioticVO) delete;
            if (s != null && (s.getAssessment_treatment_id() != null)) {
                Collection<AssessmentAntibioticVO> result = findAllByCriteria(s);
                broker = ConnectionLocator.getInstance().findBroker();
                for (AssessmentAntibioticVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     * Mucocutaneous Marin *
     */
    public Collection findAllByCriteria(AssessmentMucocSeperationsVO crit) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentEachwoundDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentEachwoundDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentEachwoundDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving AssessmentEachwoundDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     *
     * Deletes a single AssessmentWoundBedVO object into the assessment_woundbed
     * table. The value object passed
     *
     * in has to be of type AssessmentWoundBedVO.
     *
     *
     *
     * @param delete fields in ValueObject represent fields in the
     * assessment_woundbed record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     *
     */
    public void deleteSeperation(ValueObject delete) {
        logs.info("************Entering AssessmentEachwoundDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            AssessmentMucocSeperationsVO s = (AssessmentMucocSeperationsVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getId() != null || s.getAssessment_ostomy_id() != null)) {
                Collection<AssessmentMucocSeperationsVO> result = findAllByCriteria(s);
                for (AssessmentMucocSeperationsVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.delete()*************");
    }
    /**
     *
     * Updates a single AssessmentUnderminingVO object into the
     * assessment_undermining table. The value object passed
     *
     * in has to be of type AssessmentUnderminingVO.
     *
     *
     *
     * @param update fields in ValueObject represent fields in the
     * assessment_undermining record.
     *
     * @see
     * com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @todo refactor into its own AssessmentDAO
     *
     * @since 3.0
     *
     */
    public void saveSeperation(ValueObject update) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.update()************");
        PersistenceBroker broker = null;
        try {
            AssessmentMucocSeperationsVO assessment = (AssessmentMucocSeperationsVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
            throw new DataAccessException("AssessmentDAO throwing SQLException: " + e.getMessage());
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in AssessmentEachwoundDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentEachwoundDAO.update()*************");
    }
}
