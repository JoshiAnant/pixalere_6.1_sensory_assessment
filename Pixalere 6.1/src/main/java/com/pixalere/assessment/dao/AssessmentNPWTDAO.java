/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.assessment.dao;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.log4j.Logger;
import com.pixalere.assessment.bean.AssessmentNPWTVO;
import com.pixalere.assessment.bean.AssessmentNPWTAlphaVO;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.ojb.broker.query.QueryByCriteria;
/**
 * The data access tier for accessing negative pressure wound therapy wound data
 * from the assessment_npwt and assessment_npwt_alpha tables.
 * 
 * @author travis
 * @since 5.1
 * @todo Need to implement this class as a singleton.
 */
public class AssessmentNPWTDAO {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(AssessmentNPWTDAO.class);
    
    public AssessmentNPWTDAO(){}
    /**
     * Finds a all assessment_npwt records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param obj fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 5.1
     * @since 5.1
     */
    public Collection findAllByCriteria(AssessmentNPWTVO obj) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentNPWTDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        AssessmentNPWTVO returnVO = null;
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentNPWTVO();
            QueryByCriteria query = new QueryByCriteria(obj);
            query.addOrderByDescending("id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentNPWTDAO.findAllByCriteria(): " +
                    e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentNPWTDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) broker.close();
        }
        
        logs.info(
                "***********************Leaving NursingCarePlanDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an assessment_npwt record, as specified by the assessment_id/alpha_id
     * passed in.  If no record is found a null value is returned.
     *
     * @param assessment_id  the assessment_id of the assessment_npwt record
     * @param alpha_id the alpha_id of the assessment_npwt_alpha record
     * @see com.pixalere.common.DataAccessObject#findByCriteria(java.lang.Object)
     *
     * @since 5.1
     * @version 5.1
     * @author travis
     */
    public ValueObject findByCriteria(int assessment_id,int alpha_id) throws DataAccessException{
        logs.info("***********************Entering AssessmentNPWTDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AssessmentNPWTVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            crit.addEqualTo("alphas.alpha_id", alpha_id+"");
            crit.addEqualTo("assessment_id",assessment_id+"");
            Query q = QueryFactory.newQuery(AssessmentNPWTVO.class, crit);
            returnVO = (AssessmentNPWTVO) broker.getObjectByQuery(q);
        } catch(ConnectionLocatorException e){
            logs.error("ServiceLocatorException thrown in AssessmentNPWTDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in AssessmentNPWTDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        logs.info("***********************Leaving AssessmentNPWTDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an assessment_npwt record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(java.lang.Object)
     *
     * @since 5.1
     * @version 5.1
     * @author travis
     */
    public ValueObject findByCriteria(AssessmentNPWTVO vo) throws DataAccessException{
        logs.info("***********************Entering AssessmentNPWTDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AssessmentNPWTVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentNPWTVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("id");
            returnVO = (AssessmentNPWTVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            logs.error("ServiceLocatorException thrown in AssessmentNPWTDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in AssessmentNPWTDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        logs.info("***********************Leaving AssessmentNPWTDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a single assessment_npwt record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the nursing_care_plan table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 5.1
     * @version 5.1
     * @author travis
     */
    public ValueObject findByPK(int id) throws DataAccessException {
        logs.info("********* Entering the AssessmentNPWTDAO.findByPK****************");
        PersistenceBroker broker = null;
        AssessmentNPWTVO assessmentVO = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new AssessmentNPWTVO();
            assessmentVO.setId(new Integer(id));
            Query query = new QueryByCriteria(assessmentVO);
            assessmentVO = (AssessmentNPWTVO) broker.getObjectByQuery(query);
        }catch(ConnectionLocatorException e){
            logs.error("PersistanceBrokerException thrown in AssessmentNPWTDAO.findByPK(): "+ e.toString(),e);
            throw new DataAccessException("Error in AssessmentNPWTDAO.findByPK(): "+e.toString(),e);
        }finally {
            if(broker!=null)
                broker.close();
        }
        logs.info("********* Done the AssessmentNPWTDAO.findByPK");
        return assessmentVO;
    }
    public void insert(ValueObject insert){
        //Not needed.
        
    }
    /**
     * Updates a single assessment_npwt record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the nursing_care_plan table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 5.1
     * @verson 5.1
     * @author 5.1
     */
    public void update(ValueObject update){
        logs.info("************Entering AssessmentNPWTDAO.update()************");
        PersistenceBroker broker=null;
        try{
            AssessmentNPWTVO assessment =(AssessmentNPWTVO) update;
            broker=ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentNPWTDAO.update(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in AssessmentNPWTDAO.update(): "+e.toString(),e);
            
        } catch (ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentNPWTDAO.update()*************");
        
        
    }
    /**
     * Deletes assessment_npwt records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 5.1
     * @version 5.1
     * @author travis
     */
    
    public void delete(ValueObject delete){
        logs.info("************Entering AssessmentNPWTDAO.delete()************");
        PersistenceBroker broker=null;
        try{
            AssessmentNPWTVO assessment =(AssessmentNPWTVO) delete;
            Collection<AssessmentNPWTVO> result=findAllByCriteria(assessment);
            broker = ConnectionLocator.getInstance().findBroker();
            for(AssessmentNPWTVO tmpResultVO : result){
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
            }
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentNPWTDAO.delete(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in AssessmentNPWTDAO.delete(): "+e.toString(),e);
            
        }  catch (ApplicationException e ){
            logs.error("DataAccessException thrown in AssessmentNPWTDAO.deleteTMP(): "+e.toString(),e);
        }catch (DataAccessException e ){
            logs.error("DataAccessException thrown in AssessmentNPWTDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentNPWTDAO.delete()*************");
    }
    /**
     * Finds a all assessment_npwt_alphas records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param obj fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 5.1
     * @since 5.1
     */
    public Collection findAllNPWTAlphaByCriteria(AssessmentNPWTAlphaVO obj) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentNPWTDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        AssessmentNPWTAlphaVO returnVO = null;
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentNPWTAlphaVO();
            QueryByCriteria query = new QueryByCriteria(obj);
            query.addOrderByDescending("id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentNPWTDAO.findAllByCriteria(): " +
                    e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentNPWTDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) broker.close();
        }
        
        logs.info(
                "***********************Leaving NursingCarePlanDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an assessment_npwt_alphas record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(java.lang.Object)
     *
     * @since 5.1
     * @version 5.1
     * @author travis
     */
    public ValueObject findNPWTAlphaByCriteria(AssessmentNPWTAlphaVO vo) throws DataAccessException{
        logs.info("***********************Entering AssessmentNPWTDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AssessmentNPWTAlphaVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentNPWTAlphaVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("id");
            returnVO = (AssessmentNPWTAlphaVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            logs.error("ServiceLocatorException thrown in AssessmentNPWTDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in AssessmentNPWTDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        logs.info("***********************Leaving AssessmentNPWTDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a single assessment_npwt_alphas record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the nursing_care_plan table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 5.1
     * @version 5.1
     * @author travis
     */
    public ValueObject findNPWTAlphaByPK(int id) throws DataAccessException {
        logs.info("********* Entering the AssessmentNPWTDAO.findByPK****************");
        PersistenceBroker broker = null;
        AssessmentNPWTAlphaVO assessmentVO = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new AssessmentNPWTAlphaVO();
            assessmentVO.setId(new Integer(id));
            Query query = new QueryByCriteria(assessmentVO);
            assessmentVO = (AssessmentNPWTAlphaVO) broker.getObjectByQuery(query);
        }catch(ConnectionLocatorException e){
            logs.error("PersistanceBrokerException thrown in AssessmentNPWTDAO.findByPK(): "+ e.toString(),e);
            throw new DataAccessException("Error in AssessmentNPWTDAO.findByPK(): "+e.toString(),e);
        }finally {
            if(broker!=null)
                broker.close();
        }
        logs.info("********* Done the AssessmentNPWTDAO.findByPK");
        return assessmentVO;
    }
 
    /**
     * Updates a single assessment_npwt_alphas record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the nursing_care_plan table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 5.1
     * @verson 5.1
     * @author 5.1
     */
    public void updateNPWTAlpha(ValueObject update){
        logs.info("************Entering AssessmentNPWTDAO.update()************");
        PersistenceBroker broker=null;
        try{
            AssessmentNPWTAlphaVO assessment =(AssessmentNPWTAlphaVO) update;
            broker=ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentNPWTDAO.update(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in AssessmentNPWTDAO.update(): "+e.toString(),e);
            
        } catch (ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentNPWTDAO.update()*************");
        
        
    }
    /**
     * Deletes assessment_npwt_alphas records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 5.1
     * @version 5.1
     * @author travis
     */
    
    public void deleteNPWTAlpha(ValueObject delete){
        logs.info("************Entering AssessmentNPWTDAO.delete()************");
        PersistenceBroker broker=null;
        try{
            AssessmentNPWTAlphaVO assessment =(AssessmentNPWTAlphaVO) delete;
            Collection<AssessmentNPWTAlphaVO> result=findAllNPWTAlphaByCriteria(assessment);
            broker = ConnectionLocator.getInstance().findBroker();
            for(AssessmentNPWTAlphaVO tmpResultVO : result){
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
            }
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentNPWTDAO.delete(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in AssessmentNPWTDAO.delete(): "+e.toString(),e);
            
        }  catch (ApplicationException e ){
            logs.error("DataAccessException thrown in AssessmentNPWTDAO.deleteTMP(): "+e.toString(),e);
        }catch (DataAccessException e ){
            logs.error("DataAccessException thrown in AssessmentNPWTDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentNPWTDAO.delete()*************");
    }
}
