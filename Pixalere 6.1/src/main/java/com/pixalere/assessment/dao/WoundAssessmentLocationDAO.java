package com.pixalere.assessment.dao;

import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.wound.bean.WoundAcquiredVO;
import java.util.Collection;
import java.util.Date;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import java.util.Vector;
import java.util.StringTokenizer;
import java.util.Collection;
import java.util.Iterator;
import java.util.Hashtable;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Constants;

/**
 * This class is responsible for handling all CRUD logic associated with the
 * wound_assessment_location table.
 *
 * @todo Need to implement this class as a singleton.
 */
public class WoundAssessmentLocationDAO implements DataAccessObject {

    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(WoundAssessmentDAO.class);

    public WoundAssessmentLocationDAO() {
    }

    public ValueObject findByCriteria(WoundAssessmentLocationVO woundVO) throws DataAccessException {
        logs.info("********* Entering the WoundAssessmentDAO.findByPK****************");
        PersistenceBroker broker = null;
        WoundAssessmentLocationVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Query query = new QueryByCriteria(woundVO);
            assessmentVO = (WoundAssessmentLocationVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in WoundAssessmentDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the WoundAssessmentLocationDAO.findByPK");
        return assessmentVO;
    }

    /**
     * Finds an wound_assessment_location record, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param vo fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @since 3.0
     */
    public ValueObject findByCriteria(WoundAssessmentLocationVO vo, boolean orderDesc, boolean hideIncisions, String wound_profile_type) throws DataAccessException {
        logs.info("##$$##$$##-----Entering WoundAssessmentLocationDAO.findByCriteria()-----##$$##$$##");
        PersistenceBroker broker = null;
        WoundAssessmentLocationVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundAssessmentLocationVO();
            Criteria crit = new Criteria();
            if (vo.getActive() != null) {
                crit.addEqualTo("active", vo.getActive());//get all active assessments
            }
            if (vo.getWound_id() != null) {
                crit.addEqualTo("wound_id", vo.getWound_id());//by wound
            }
            if (vo.getId() != null) {
                crit.addEqualTo("id", vo.getId());//by wound
            }
            if (vo.getProfessional_id() != null) {
                crit.addEqualTo("professional_id", vo.getProfessional_id());//by wound
            }
            if (vo.getWound_profile_type_id() != null) {
                crit.addEqualTo("wound_profile_type_id", vo.getWound_profile_type_id());//by wound
            }
            if (vo.getAlpha() != null) {
                crit.addEqualTo("alpha", vo.getAlpha());//by wound
            }
            if (vo.getDischarge() != null) {
                crit.addEqualTo("discharge", vo.getDischarge());//by wound
            }
            if (hideIncisions == true) {
                crit.addNotEqualTo("alpha", "incs");
            }
            if (wound_profile_type != null) {
                crit.addEqualTo("wound_profile_type.alpha_type", wound_profile_type);
            }
            QueryByCriteria query = new QueryByCriteria(WoundAssessmentLocationVO.class, crit);
            if (orderDesc == true) {
                query.addOrderByDescending("id"); // Get latest record, incase alpha has reopened
            } else {
                query.addOrderByAscending("id");
            }
            returnVO = (WoundAssessmentLocationVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundAssessmentLocationDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundAssessmentLocationDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("##$$##$$##-----Leaving WoundAssessmentLocationDAO.findByCriteria()-----##$$##$$##");
        return returnVO;
    }

    public ValueObject findDetailedByCriteria(WoundLocationDetailsVO vo) throws DataAccessException {
        logs.info("##$$##$$##-----Entering WoundAssessmentLocationDAO.findByCriteria()-----##$$##$$##");
        PersistenceBroker broker = null;
        WoundLocationDetailsVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundLocationDetailsVO();
            Query query = new QueryByCriteria(vo);
            returnVO = (WoundLocationDetailsVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundAssessmentLocationDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundAssessmentLocationDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("##$$##$$##-----Leaving WoundAssessmentLocationDAO.findByCriteria()-----##$$##$$##");
        return returnVO;
    }

    /**
     * Generates javascript array of alphas from wound_assessment_location.
     *
     * @param wound_id the wound_profile_id of the alphas you would like to
     * retrieve
     * @param alpha current serialized alpha
     * @param flag legacy, doesn't appear to be used anymore.
     * @param professional_id the professional currently logged in, (only
     * retrieve inactive alpha by this professional).
     * @todo this should not be in the CRUD!, need to move out into either
     * common or BD.
     * @since 3.0
     */
    public String findAllAlphasforSelection(String wound_id, String alpha, int flag, int professional_id) throws DataAccessException {
        String alphass = "";
        int showimage = 0;
        boolean alreadychecked = false; // This is so i know whether ive already
        // got the top alpha (for image browse)
        WoundAssessmentLocationVO vo = new WoundAssessmentLocationVO();
        vo.setWound_id(new Integer(wound_id));
        vo.setAlpha(alpha);
        vo.setActive(1);
        vo.setDischarge(0);
        Collection<WoundAssessmentLocationVO> locations = findAllByCriteria(vo, professional_id, false, "");//4th parameter (extra sort) is ignored
        StringTokenizer str2 = new StringTokenizer(alpha, ":");
        Vector<String> alphas = new Vector();
        for (String a : alphas) {
            alphas.add(a);
        }
        int count = 0;
        for (WoundAssessmentLocationVO locationVO : locations) {
            // Javascript function in VM: showWound(imgBox,intBox,strWound,strType,intDischarged,intActive,intAlphaID)
            WoundLocationDetailsVO[] coll = locationVO.getAlpha_details();
            for (WoundLocationDetailsVO detail : coll) {
                //alphass = alphass + "showWound(document.img" + detail.getBox() + "," + detail.getBox() + ",'" + detail.getImage() + "','" + locationVO.getWound_profile_type().getAlpha_type() + "'," + locationVO.getDischarge() + "," + locationVO.getActive() + "," + detail.getId() + ");";
            }
            alreadychecked = true;
            if (count == 0 && ((String) alphas.get(2)).equals(locationVO.getAlpha())) {
                showimage = 1;
            }
            count++;
        }
        vo.setActive(0);
        locations = findAllByCriteria(vo, professional_id, false, "");//4th parameter (extra sort) is ignored
        count = 0;
        for (WoundAssessmentLocationVO locationVO : locations) {
            if (locationVO.getProfessional_id() != null && locationVO.getProfessional_id().equals(new Integer(professional_id))) {
                WoundLocationDetailsVO[] coll = locationVO.getAlpha_details();
                for (WoundLocationDetailsVO detail : coll) {
                    //alphass = alphass + "showWound(document.img" + detail.getBox() + "," + detail.getBox() + ",'" + detail.getImage() + "','" + locationVO.getWound_profile_type().getAlpha_type() + "'," + locationVO.getDischarge() + "," + locationVO.getActive() + "," + detail.getId() + ");";
                }
                if (alreadychecked == false) {
                    if (count == 0 && ((String) alphas.get(2)).equals(locationVO.getAlpha())) {
                        showimage = 1;
                    }
                    count++;
                }
            }
        }
        return alphass;
    }

    /**
     * Finds a all wound_assessment_location records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param wound_id is the wound profile of the alpha you would like.
     * @todo this method should be passing in a valueobject to make it mroe
     * versatile.
     * @since 3.0
     */
    public Vector findAllByCriteria(WoundAssessmentLocationVO woundVO, int professional_id, boolean blnSortDescending, String sort) throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentLocationVO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Vector<WoundAssessmentLocationVO> results = new Vector<WoundAssessmentLocationVO>();
        Vector sorted_results = new Vector();//hack to get it in th right order, should probably do a comparator or set a sort orrder in the scehma
        WoundAssessmentLocationVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundAssessmentLocationVO();
            QueryByCriteria query = new QueryByCriteria(woundVO);
            if (blnSortDescending) {
                query.addOrderByDescending("id");
            } else {
                query.addOrderByAscending("alpha"); // SV 21NOV2007: For the order of alphas on the wp for selecting etiologies
            }
            if (!sort.equals("")) {
                query.addOrderByDescending(sort);
            }
            Collection res = (Collection) broker.getCollectionByQuery(query);
            Iterator iter = res.iterator();
            while (iter.hasNext()) {
                WoundAssessmentLocationVO vo = (WoundAssessmentLocationVO) iter.next();
                // if alpha is inactive it must be assigned to professional logged in
                if (professional_id == -1 || vo.getActive().equals(new Integer("1")) || (vo.getProfessional_id() != null && vo.getProfessional_id().equals(new Integer(professional_id)))) {
                    results.add(vo);
                }
            }
            //sort
            for (WoundAssessmentLocationVO a : results) {
                if (a.getAlpha().indexOf("ostu") == -1 && a.getAlpha().indexOf("ostf") == -1 && a.getAlpha().indexOf("s") != 0) {
                    sorted_results.add(a);
                }
            }
            for (WoundAssessmentLocationVO a : results) {
                if (a.getAlpha().indexOf("ostu") >= 0 || a.getAlpha().indexOf("ostf") >= 0) {
                    sorted_results.add(a);
                }
            }
            for (WoundAssessmentLocationVO a : results) {
                if (a.getAlpha().indexOf("s") == 0) {
                    sorted_results.add(a);
                }
            }

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return sorted_results;
    }

    /**
     * Finds a all wound_assessment_location records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param wound_id is the wound profile of the alpha you would like.
     * @todo this method should be passing in a valueobject to make it mroe
     * versatile.
     * @since 3.0
     */
    /// Only used for Conversion.
    public Vector findAllByCriteria(WoundAssessmentLocationVO woundVO, int professional_id, int start, int end, boolean blnSortDescending) throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentLocationVO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Vector results = new Vector();
        WoundAssessmentLocationVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundAssessmentLocationVO();
            QueryByCriteria query = new QueryByCriteria(woundVO);
            if (blnSortDescending) {
                query.addOrderByDescending("id");
            } else {
                query.addOrderByAscending("id"); // SV 21NOV2007: For the order of alphas on the wp for selecting etiologies
            }
            query.setStartAtIndex(start);
            query.setEndAtIndex(end);
            Collection res = (Collection) broker.getCollectionByQuery(query);
            Iterator iter = res.iterator();
            while (iter.hasNext()) {
                WoundAssessmentLocationVO vo = (WoundAssessmentLocationVO) iter.next();
                // if alpha is inactive it must be assigned to professional logged in
                if (professional_id == -1 || vo.getActive().equals(new Integer("1")) || (vo.getProfessional_id() != null && vo.getProfessional_id().equals(new Integer(professional_id)))) {
                    results.add(vo);
                }
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return results;
    }

    public Collection<WoundLocationDetailsVO> findAllDetailsByCriteria(WoundLocationDetailsVO woundVO) throws DataAccessException {
        logs.info("***********************Entering WoundAssessmentLocationVO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Vector results = new Vector();
        WoundLocationDetailsVO returnVO = null;
        Collection<WoundLocationDetailsVO> res = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new WoundLocationDetailsVO();
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            res = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return res;
    }

    /**
     * Finds a single wound_assessment_location record, as specified by the
     * primary key value passed in. If no record is found a null value is
     * returned.
     *
     * @param primaryKey the primary key of the wound_assessment_location table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public ValueObject findByPK(int id) throws DataAccessException {
        logs.info("********* Entering the WoundAssessmentDAO.findByPK****************");
        PersistenceBroker broker = null;
        WoundAssessmentLocationVO assessmentVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assessmentVO = new WoundAssessmentLocationVO();
            assessmentVO.setId(new Integer(id + ""));
            Query query = new QueryByCriteria(assessmentVO);
            assessmentVO = (WoundAssessmentLocationVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in WoundAssessmentDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the WoundAssessmentLocationDAO.findByPK");
        return assessmentVO;
    }

    /**
     * Finds all alphas, and verifies if the wound is healed
     *
     * @param wound-id the wound_profile you would like to check if its healed.
     * @todo this should be in BD.
     * @since 3.0
     */
    public boolean isWoundProfileHealed(int wound_id, int professional_id) throws DataAccessException {
        logs.info("##$$##$$##-----Entering the WoundAssessmentLocationDAO.isWoundProfileHealed-----##$$##$$##");
        PersistenceBroker broker = null;
        boolean healed = false;
        Collection<WoundAssessmentLocationVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            WoundAssessmentLocationVO assessmentVO2 = new WoundAssessmentLocationVO();
            assessmentVO2.setWound_id(new Integer(wound_id + ""));
            Query query2 = new QueryByCriteria(assessmentVO2);
            results = broker.getCollectionByQuery(query2);
            if (results.size() == 0) {
                healed = false;
            } else {
                Criteria crit = new Criteria();
                crit.addEqualTo("wound_id", new Integer(wound_id + ""));
                crit.addEqualTo("discharge", new Integer(0));
                crit.addNotEqualTo("alpha", "incs");
                QueryByCriteria query = new QueryByCriteria(WoundAssessmentLocationVO.class, crit);
                results = broker.getCollectionByQuery(query);
                if (results != null) {//if no active alphas exist, the wound is healed
                    logs.info("##$$##$$##-----NUMBER OF ACTIVE ALPHAS FOR WOUND PROFILE = " + results.size() + "-----##$$##$$##");
                    if (results.size() > 0) {
                        healed = true;
                        for (WoundAssessmentLocationVO alp : results) {
                            if (alp.getActive().equals(new Integer(1))) {
                                healed = false;
                            } else if (alp.getProfessional_id().equals(new Integer(professional_id))) {
                                healed = false;
                            }
                        }
                    } else {
                        logs.info("##$$##$$##-----WOUND PROFILE HEALED = TRUE-----##$$##$$##");
                        healed = true;
                    }
                }
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in WoundAssessmentDAO.isWoundProfileHealed(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("##$$##$$##-----WOUND PROFILE HEALED = " + healed + "-----##$$##$$##");
        logs.info("##$$##$$##-----Done the WoundAssessmentDAO.isWoundProfileHealed-----##$$##$$##");
        return healed;
    }

    /**
     * Inserts a single WoundAssessmentLocationVO object into the
     * wound_assessment_location table. The value object passed in has to be of
     * type WoundAssessmentLocationVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * wound_assessment_location record.
     * @since 3.0
     */
    public void insert(ValueObject insert) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            WoundAssessmentLocationVO assessment = (WoundAssessmentLocationVO) insert;
            if (assessment != null && assessment.getDeleted() == null) {
                assessment.setDeleted(0);
            }

            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
            // throw new DataAccessException("Error in
            // WoundAssessmentDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundAssessmentDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundAssessmentDAO.insert()*************");
    }

    public void insertDetailed(ValueObject insert) throws DataAccessException {
        logs.info("************Entering AssessmentEachwoundDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            WoundLocationDetailsVO assessment = (WoundLocationDetailsVO) insert;
            if (assessment != null && assessment.getDeleted() == null) {
                assessment.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
            // throw new DataAccessException("Error in
            // WoundAssessmentDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundAssessmentDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundAssessmentDAO.insert()*************");
    }

    /**
     * Inserts a single WoundAssessmentLocationVO object into the
     * wound_assessment_location table. The value object passed in has to be of
     * type WoundAssessmentLocationVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * wound_assessment_location record.
     * @since 3.0
     */
    public void update(ValueObject update) throws DataAccessException {
        logs.info("************Entering WoundAssessmentDAO.update()************");
        PersistenceBroker broker = null;
        try {
            WoundAssessmentLocationVO assessment = (WoundAssessmentLocationVO) update;
            if (assessment != null && assessment.getDeleted() == null) {
                assessment.setDeleted(0);
            }

            broker = ConnectionLocator.getInstance().findBroker();
            WoundAssessmentLocationVO alpha = new WoundAssessmentLocationVO();
            alpha.setPatient_id(assessment.getPatient_id());
            alpha.setWound_id(assessment.getWound_id());
            alpha.setAlpha(assessment.getAlpha());
            //alpha.setActive(new Integer(1));
            com.pixalere.assessment.bean.WoundAssessmentLocationVO duplicate = (com.pixalere.assessment.bean.WoundAssessmentLocationVO) findByCriteria((com.pixalere.assessment.bean.WoundAssessmentLocationVO) alpha, true, false, null);
            if (duplicate != null) {
                broker.beginTransaction();
                broker.store(assessment);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundAssessmenmtDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundAssessmentDAO.update()*************");
    }

    public void updateDetailed(ValueObject update) throws DataAccessException {
        logs.info("************Entering WoundAssessmentDAO.update()************");
        PersistenceBroker broker = null;
        try {
            WoundLocationDetailsVO assessment = (WoundLocationDetailsVO) update;
            if (assessment != null && assessment.getDeleted() == null) {
                assessment.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            WoundLocationDetailsVO alphat = new WoundLocationDetailsVO();
            alphat.setAlpha_id(assessment.getAlpha_id());
            //alpha.setBox(assessment.getBox());
            alphat.setImage(assessment.getImage());
            com.pixalere.assessment.bean.WoundLocationDetailsVO duplicate = (com.pixalere.assessment.bean.WoundLocationDetailsVO) findDetailedByCriteria((com.pixalere.assessment.bean.WoundLocationDetailsVO) alphat);
            if (duplicate != null) {
                broker.beginTransaction();
                broker.store(assessment);
                broker.commitTransaction();

            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundAssessmentDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundAssessmenmtDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundAssessmentDAO.update()*************");
    }

    /**
     * Deletes wound_assessment_location records, as specified by the value
     * object passed in. Fields in Value Object which are not null will be used
     * as parameters in the SQL statement. Retrieves all records by criteria,
     * then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should
     * be deleted
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 3.0
     */
    public void delete(ValueObject vo) throws DataAccessException {
        logs.info("************Entering WoundProfileDAO.delete()************");
        PersistenceBroker broker = null;
        WoundServiceImpl wservice = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        try {
            WoundAssessmentLocationVO alpha = (WoundAssessmentLocationVO) vo;
            int professional_id = -1;
            if (alpha.getProfessional_id() != null) {
                professional_id = alpha.getProfessional_id().intValue();
            }
            Vector alphas = findAllByCriteria(alpha, professional_id, false, "");//4th parameter (extra sort) is ignored
            Iterator iter = alphas.iterator();
            broker = ConnectionLocator.getInstance().findBroker();
            while (iter.hasNext()) {
                WoundAssessmentLocationVO currentAlpha = (WoundAssessmentLocationVO) iter.next();
                broker.beginTransaction();
                broker.delete(currentAlpha);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.delete()*************");
    }

    /**
     * Deletes all inactive wound_assessment_location records, as specified by
     * the value object passed in. Fields in Value Object which are not null
     * will be used as parameters in the SQL statement. Retrieves all records by
     * criteria, then deletes them.
     *
     * @param patient_id the patient alphas which you would like to delete
     * @param professional_id delete all inactive alphas by the currently logged
     * in professional
     * @since 3.0
     */
    public void deleteTMP(WoundAssessmentLocationVO tmpVO, Date purge_days) {
        logs.info("************Entering WoundProfileDAO.deleteTMP()************");
        //default to english, correct translation isn't need for deletes
        WoundServiceImpl wservice = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        PersistenceBroker broker = null;
        try {
            Collection result = findAllByCriteria(tmpVO, -1, false, "");//4th parameter (extra sort) is ignored
            Iterator iter = result.iterator();
            broker = ConnectionLocator.getInstance().findBroker();
            
            
            while (iter.hasNext()) {
                WoundAssessmentLocationVO tmpResultVO = (WoundAssessmentLocationVO) iter.next();
                if (tmpResultVO.getOpen_timestamp() != null) {
                    Date last_update = tmpResultVO.getOpen_timestamp();
                    //int days = Integer.parseInt(purge_days + "");
                    if (purge_days == null || last_update.before(purge_days)) {
                        //drop etiology/goals first
                        EtiologyVO et = new EtiologyVO();
                        GoalsVO g = new GoalsVO();
                        WoundAcquiredVO a = new WoundAcquiredVO();
                        et.setAlpha_id(tmpResultVO.getId());
                        g.setAlpha_id(tmpResultVO.getId());
                        a.setAlpha_id(tmpResultVO.getId());
                        Collection<EtiologyVO> ets = wservice.getAllEtiologies(et);
                        Collection<GoalsVO> gls = wservice.getAllGoals(g);
                        Collection<WoundAcquiredVO> acq = wservice.getAllAcquired(a);
                        for(EtiologyVO e : ets){
                            wservice.removeEtiology(e);
                        }
                        for(GoalsVO s : gls){
                            wservice.removeGoal(s);
                        }
                        for(WoundAcquiredVO s : acq){
                            wservice.removeAcquired(s);
                        }
                        broker.beginTransaction();
                        broker.delete(tmpResultVO);
                        broker.commitTransaction();

                    }
                }
            }
            WoundProfileDAO wpdao = new WoundProfileDAO();
            WoundProfileTypeVO wpt = new WoundProfileTypeVO();
            wpt.setPatient_id(tmpVO.getPatient_id());
            wpt.setWound_id(tmpVO.getWound_id());
            //check if all alphas are gone for a wound profile type.. then remove type.
            Collection<WoundProfileTypeVO> result2 = wpdao.findAllByCriteria(wpt, 0);
            for (WoundProfileTypeVO w : result2) {
                WoundAssessmentLocationVO alphaTMP = new WoundAssessmentLocationVO();
                alphaTMP.setWound_profile_type_id(w.getId());
                WoundAssessmentLocationVO alpha = (WoundAssessmentLocationVO) findByCriteria(alphaTMP);
                if (alpha == null && !w.getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE)) {
                    broker.beginTransaction();
                    broker.delete(w);
                    broker.commitTransaction();

                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.deleteTMP()*************");
    }

    /*
     * public void delete(ValueObject delete) { logs.info("************Entering
     * WoundAssessmentDAO.delete()************"); PersistenceBroker broker =
     * null; try { WoundAssessmentLocationVO assessment =
     * (WoundAssessmentLocationVO) delete; Collection result =
     * findAllByCriteria(assessment); Iterator iter = result.iterator(); broker =
     * ConnectionLocator.getInstance().findBroker(); while
     * (iter.hasNext()) { WoundAssessmentLocationVO tmpResultVO =
     * (WoundAssessmentLocationVO) iter.next(); broker.beginTransaction();
     * broker.delete(tmpResultVO); broker.commitTransaction(); } } catch
     * (PersistenceBrokerException e) { broker.abortTransaction();
     * logs.error("PersistanceBrokerException thrown in
     * WoundAssessmentDAO.delete(): " + e.toString(), e); e.printStackTrace();
     *  } catch (ConnectionLocatorException e) {
     * logs.error("connectionLocatorException thrown in
     * WoundAssessmentDAO.delete(): " + e.toString(), e);
     *  } catch (DataAccessException e) { logs.error("DataAccessException thrown
     * in WoundProfileDAO.deleteTMP(): " + e.toString(), e); } finally { if
     * (broker != null) { broker.close(); } } logs.info("*************Done with
     * WoundAssessmentDAO.delete()*************"); }
     */
}
