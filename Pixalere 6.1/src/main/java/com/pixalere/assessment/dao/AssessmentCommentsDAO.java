package com.pixalere.assessment.dao;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import java.util.Collection;
import java.util.Iterator;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  assessment_comments    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class AssessmentCommentsDAO implements DataAccessObject {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(AssessmentCommentsDAO.class);
    
    public AssessmentCommentsDAO(){
        
    }
    /**
     * Finds a all assessment_comments records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(AssessmentCommentsVO ProfessionalVO) throws
            DataAccessException {
        logs.info("***********************Entering AssessmentCommentsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        AssessmentCommentsVO returnVO = null;
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentCommentsVO();
            QueryByCriteria query = new QueryByCriteria(ProfessionalVO);
            query.addOrderByAscending("created_on");
            query.addOrderByAscending("comment_type");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentCommentsDAO.findAllByCriteria(): " +
                    e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentCommentsDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) broker.close();
        }
        
        logs.info(
                "***********************Leaving AssessmentCommentsDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
   * Finds all assessment_comments records, as specified by the date range
   * passed in
   * 
   * @param start_date
   * @param end_date
   * @param wound_profile_type_id
   * @version 6.0
   * @since 6.0
   */
  public Collection findAllByCriteriaWithinRange( String start_id, String end_id, int wound_profile_type_id) throws DataAccessException {
      logs.info("***********************Entering AssessmentCommentsDAO.findAllByCriteriaWithinRange()***********************");
      PersistenceBroker broker = null;
      Collection returnVO = null;
      try {
          broker = ConnectionLocator.getInstance().findBroker();
          Criteria crit = new Criteria();
          QueryByCriteria query = null;
          //crit.addIsNull("deleted");
          //System.out.println(wound_profile_type_id+" "+start_id+" "+end_id);
                
          if (wound_profile_type_id > 0) {
              crit.addEqualTo("wound_profile_type_id", wound_profile_type_id);
          
              if (start_id!=null && !start_id.equals("")) {
                  crit.addGreaterOrEqualThan("id", new Integer(start_id));
              }
              if ( end_id!=null && !end_id.equals("")) {
                  //System.out.println("End_date"+end_id);
                  crit.addLessOrEqualThan("id", new Integer(end_id));
              }
          }
          
          query = QueryFactory.newQuery(AssessmentCommentsVO.class, crit);
          
          query.addOrderByAscending("created_on");
            query.addOrderByAscending("comment_type");
          returnVO = (Collection) broker.getCollectionByQuery(query);
      } catch (ConnectionLocatorException e) {
          logs.error("ServiceLocatorException thrown in AssessmentCommentsDAO.findAllByCriteriaWithinRange(): " + e.toString(), e);
          throw new DataAccessException("ServiceLocatorException in AssessmentCommentsDAO.findAllByCriteriaWithinRange()", e);
      } finally {
          if (broker != null) {
              broker.close();
          }
      }
      logs.info("***********************Leaving AssessmentCommentsDAO.findAllByCriteriaWithinRange()***********************");
      return returnVO;
  }
    public int getCommentsCount(WoundAssessmentVO v) throws DataAccessException {
        logs.info("***********************Entering AssessmentCommentsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        int count=0; 
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            AssessmentCommentsVO c = new AssessmentCommentsVO();
            c.setAssessment_id(v.getId());
            QueryByCriteria query = new QueryByCriteria(c);
            count = broker.getCount(query);
            
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in AssessmentCommentsDAO.findAllByCriteria(): " +
                    e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in AssessmentCommentsDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) broker.close();
        }
        
        logs.info(
                "***********************Leaving AssessmentCommentsDAO.findAllByCriteria()***********************");
        return count;
    }
    /**
     * Finds an assessment_comments record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param vo  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(AssessmentCommentsVO vo) throws DataAccessException{
        logs.info("***********************Entering AssessmentCommentsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AssessmentCommentsVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssessmentCommentsVO();
            QueryByCriteria query = new QueryByCriteria(vo);
            query.addOrderByDescending("created_on");
            returnVO = (AssessmentCommentsVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            logs.error("ServiceLocatorException thrown in AssessmentCommentsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in AssessmentCommentsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        logs.info("***********************Leaving AssessmentCommentsDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a single assessment_comments record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the assessment_comments table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int assessment_id) throws DataAccessException {
        //not used
        return null;
    }
    
    public void insert(ValueObject insert){
        //Not needed.
        
    }
    /**
     * Updates a single assessment_comments record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the assessment_comments table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void update(ValueObject update){
        logs.info("************Entering AssessmentCommentsDAO.update()************");
        PersistenceBroker broker=null;
        try{
            AssessmentCommentsVO assessment =(AssessmentCommentsVO) update;
            if(assessment!=null && assessment.getDeleted() == null){
                assessment.setDeleted(0);
            }
            broker=ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentCommentsDAO.update(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in AssessmentCommentsDAO.update(): "+e.toString(),e);
            
        } catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentCommentsDAO.update()*************");
        
        
    }
    /**
     * Deletes assessment_comments records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    
    public void delete(ValueObject delete){
        logs.info("************Entering AssessmentCommentsDAO.delete()************");
        PersistenceBroker broker=null;
        try{
            AssessmentCommentsVO assessment =(AssessmentCommentsVO) delete;
            Collection<AssessmentCommentsVO> result=findAllByCriteria(assessment);
            broker = ConnectionLocator.getInstance().findBroker();
            for(AssessmentCommentsVO tmpResultVO : result){
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
            }
        } catch(PersistenceBrokerException e){
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in AssessmentCommentsDAO.delete(): "+e.toString(),e);
            e.printStackTrace();
            
        } catch(ConnectionLocatorException e){
            logs.error("connectionLocatorException thrown in AssessmentCommentsDAO.delete(): "+e.toString(),e);
            
        }  catch (DataAccessException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }catch (com.pixalere.common.ApplicationException e ){
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): "+e.toString(),e);
        }finally {
            if (broker!=null){
                broker.close();
            }
        }
        logs.info("*************Done with AssessmentCommentsDAO.delete()*************");
    }
}
