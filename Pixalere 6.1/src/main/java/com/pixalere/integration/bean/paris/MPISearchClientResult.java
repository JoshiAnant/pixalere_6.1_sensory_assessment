/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.integration.bean.paris;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import com.pixalere.integration.bean.paris.SearchClientResult;
/**
 *
 * @author travis
 */
@XmlRootElement(name="MPISearchClientResult")
public class MPISearchClientResult {
    
    
    private ArrayList<SearchClientResult> searchClientResults;
    private String searchResultType;
    
    public MPISearchClientResult(){}
    /**
     * @return the searchClientResults
     */
    @XmlElementWrapper(name = "SearchClientResults")
    // XmlElement sets the name of the entities
    @XmlElement(name = "SearchClientResult")
    public ArrayList<SearchClientResult> getSearchClientResults() {
        return searchClientResults;
    }
    /**
     * @param searchClientResults the searchClientResults to set
     */
    public void setSearchClientResults(ArrayList<SearchClientResult> searchClientResults) {
        this.searchClientResults = searchClientResults;
    }
    /**
     * @return the searchResultType
     */
    @XmlAttribute(name="SearchResultType")
    public String getSearchResultType() {
        return searchResultType;
    }
    /**
     * @param searchResultType the searchResultType to set
     */
    public void setSearchResultType(String searchResultType) {
        this.searchResultType = searchResultType;
    }
  
}
