/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.integration.bean.paris;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
/**
 *
 * @author travis
 */
@XmlRootElement(name = "Allergy")
public class Allergy {
    
    private String allergyType;
    
    private String allergyName;
    
    private String allergyReaction;
    public Allergy(){}
    /**
     * @return the allergyType
     */
    @XmlElement(name = "AllergyType")
    public String getAllergyType() {
        return allergyType;
    }
    /**
     * @param allergyType the allergyType to set
     */
    public void setAllergyType(String allergyType) {
        this.allergyType = allergyType;
    }
    /**
     * @return the allergyName
     */
    @XmlElement(name = "AllergyName")
    public String getAllergyName() {
        return allergyName;
    }
    /**
     * @param allergyName the allergyName to set
     */
    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }
    /**
     * @return the allergyReaction
     */
    @XmlElement(name = "AllergyReaction")
    public String getAllergyReaction() {
        return allergyReaction;
    }
    /**
     * @param allergyReaction the allergyReaction to set
     */
    public void setAllergyReaction(String allergyReaction) {
        this.allergyReaction = allergyReaction;
    }
}
