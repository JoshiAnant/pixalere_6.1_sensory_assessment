package com.pixalere.integration.bean.paris;
import com.pixalere.integration.bean.paris.Allergy;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
/**
 * Pixalere - Paris interface result. Pixalere sends paris_id,professional_id,empty string as an OCP through JDBC
 * which will end up being this XML document on return.
 *
 * @author travis morris - Feb 2012
 */
@XmlRootElement(name = "SearchClientResult")
public class SearchClientResult {
    
    private Integer id;
    private Integer clientId;//Paris ID
    private String firstName;
    private String lastName;
    private String secondName;
    private XMLGregorianCalendar birthDate;
    private String gender;
    private String phn;
    private ArrayList<Allergy> allergies;
    private String errorCode;
    private String errorMsg;
    private String source;
    /**
     * @return the clientId
     */
    public Integer getClientId() {
        return clientId;
    }
    @XmlElement(name = "ClientId", required = true)
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
    @XmlElement(name = "FirstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }
    @XmlElement(name = "LastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * @return the secondName
     */
    public String getSecondName() {
        return secondName;
    }
    @XmlElement(name = "SecondName")
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
    /**
     * @return the birthDate
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }
    @XmlElement(name = "BirthDate")
    @XmlSchemaType(name = "dateTime")
    public void setBirthDate(XMLGregorianCalendar birthDate) {
        this.birthDate = birthDate;
    }
    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }
    @XmlElement(name = "Gender")
    public void setGender(String gender) {
        this.gender = gender;
    }
    /**
     * @return the phn
     */
    public String getPhn() {
        return phn;
    }
    @XmlElement(name = "PHN")
    public void setPhn(String phn) {
        this.phn = phn;
    }
   
    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }
    @XmlElement(name = "ErrorCode")
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }
    @XmlElement(name = "ErrorMsg")
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    /**
     * @return the allergies
     */
    @XmlElementWrapper(name = "Allergies")
    @XmlElement(name="Allergy")
    public ArrayList<Allergy> getAllergies() {
        return allergies;
    }
    /**
     * @param allergies the allergies to set
     */
    public void setAllergies(ArrayList<Allergy> allergies) {
        this.allergies = allergies;
    }
    /**
     * @return the id
     */
    @XmlAttribute()
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the source
     */
    @XmlAttribute()
    public String getSource() {
        return source;
    }
    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }
   
}
