
package com.pixalere.integration.bean;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

public enum Gender {
    @XmlEnumValue("Male")
    MALE("Male"),
    @XmlEnumValue("Female")
    FEMALE("Female"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("Transgender")
    TRANSGENDER("Transgender"),
    @XmlEnumValue("Uncertain")
    UNCERTAIN("Uncertain"),
    @XmlEnumValue("M")
    M("M"),
    @XmlEnumValue("F")
    F("F"),
    @XmlEnumValue("T")
    T("T"),
    @XmlEnumValue("U")
    U("U");
    private final String value;
    Gender(String v) {
        value = v;
    }
    public String value() {
        return value;
    }
    public static Gender fromValue(String v) {
        for (Gender c:Gender.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
