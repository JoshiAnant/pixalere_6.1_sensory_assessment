package com.pixalere.integration.bean;
import com.pixalere.common.ValueObject;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;
/**
 *     This is the EventVO  bean which is associated with the events table.  This bean is quite a bit different then
 *     what the web service takes in so we separated the two into separate classes.
 *     Refer to {@link com.pixalere.integration.service.IntegrationServiceImpl } for the calls
 *     which utilize this bean.
 *
 *    @todo  rename Event class so it doesn't so closely resemble this one.
 *
 */
public class EventVO    extends ValueObject implements Serializable {
    private Integer id;
    private String messageId;
    private String sendingFacility;
    private Integer row_id;
    private String table_name;
    private Integer patient_from;
    private Integer patient_to;
    private Integer merge;
    private String messageDateTime;
    private String reason;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getMessageId() {
        return messageId;
    }
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
    public String getSendingFacility() {
        return sendingFacility;
    }
    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }
    public Integer getRow_id() {
        return row_id;
    }
    public void setRow_id(Integer row_id) {
        this.row_id = row_id;
    }
    public String getTable_name() {
        return table_name;
    }
    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }
    public Integer getPatient_from() {
        return patient_from;
    }
    public void setPatient_from(Integer patient_from) {
        this.patient_from = patient_from;
    }
    public Integer getPatient_to() {
        return patient_to;
    }
    public void setPatient_to(Integer patient_to) {
        this.patient_to = patient_to;
    }
    public Integer getMerge() {
        return merge;
    }
    public void setMerge(Integer merge) {
        this.merge = merge;
    }
    public String getMessageDateTime() {
        return messageDateTime;
    }
    public void setMessageDateTime(String messageDateTime) {
        this.messageDateTime = messageDateTime;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
}
