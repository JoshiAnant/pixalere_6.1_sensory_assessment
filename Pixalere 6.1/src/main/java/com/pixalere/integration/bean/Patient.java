package com.pixalere.integration.bean;
import java.util.Date;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.utils.TripleDes;
import com.pixalere.utils.Common;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
/**
 *     This is the Patient  bean is used to store the Patient demographic information from the web service.
 *     It is
 *     not direclty associated with any database table.
 *     Refer to {@link com.pixalere.integration.service.IntegrationServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.integration.bean.Gender
 *
 */
public class Patient  extends ValueObject implements Serializable {
    
    private Gender gender;
    private XMLGregorianCalendar dateOfBirth;
    private Date dischargeDate;
    private Date admitDate;
    private String firstName;
    private String allergies;
    private String middleName;
    private String lastName;
    private String accountNumber;
    private String phn;
    private String mrn;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String province;
    private String postalCode;

    public Gender getGender() {
        return gender;
    }
    @XmlElement(name = "gender", required = true)
    public void setGender(Gender gender) {
        this.gender = gender;
    }
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }
    @XmlElement(name = "DateOfBirth", required = true)
    @XmlSchemaType(name = "dateTime")
    public void setDateOfBirth(XMLGregorianCalendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    public String getFirstName() {
        return firstName;
    }
    @XmlElement(name = "FirstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    @XmlElement(name = "LastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    @XmlElement(name = "AccountNumber")
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getPhn() {
        return phn;
    }
    @XmlAttribute(name = "PHN")
    public void setPhn(String phn) {
        this.phn = phn;
    }
    public String getMrn() {
        return mrn;
    }
    @XmlAttribute(name = "MRN")
    public void setMrn(String mrn) {
        this.mrn = mrn;
    }
    public String getAddressLine1() {
        return addressLine1;
    }
    @XmlElement(name = "AddressLine1")
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }
    public String getAddressLine2() {
        return addressLine2;
    }
    @XmlElement(name = "AddressLine2")
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }
    public String getCity() {
        return city;
    }
    @XmlElement(name = "City")
    public void setCity(String city) {
        this.city = city;
    }
    public String getProvince() {
        return province;
    }
    @XmlElement(name = "Province")
    public void setProvince(String province) {
        this.province = province;
    }
    public String getPostalCode() {
        return postalCode;
    }
    @XmlElement(name = "PostalCode")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    /**
     * @return the dischargeDate
     */
    public Date getDischargeDate() {
        return dischargeDate;
    }
    /**
     * @param dischargeDate the dischargeDate to set
     */
    public void setDischargeDate(Date dischargeDate) {
        this.dischargeDate = dischargeDate;
    }
    /**
     * @return the admitDate
     */
    public Date getAdmitDate() {
        return admitDate;
    }
    /**
     * @param admitDate the admitDate to set
     */
    public void setAdmitDate(Date admitDate) {
        this.admitDate = admitDate;
    }
    /**
     * @return the allergies
     */
    public String getAllergies() {
        return allergies;
    }
    /**
     * @param allergies the allergies to set
     */
    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }
    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
