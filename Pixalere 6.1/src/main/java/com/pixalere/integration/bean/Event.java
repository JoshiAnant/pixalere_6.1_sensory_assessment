package com.pixalere.integration.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import javax.xml.datatype.XMLGregorianCalendar;
 /**
 *     This is the Events  bean is used to store the event information from the web service.  It is
 *     not direclty associated with any database table.
 *     Refer to {@link com.pixalere.integration.service.IntegrationServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 *
 */
public class Event   extends ValueObject implements Serializable {
    private String messageId;
    private String sendingFacility;
    private String reason;
    private XMLGregorianCalendar messageDateTime;
    public String getMessageId() {
        return messageId;
    }
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
    public String getSendingFacility() {
        return sendingFacility;
    }
    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }
    public XMLGregorianCalendar getMessageDateTime() {
        return messageDateTime;
    }
    public void setMessageDateTime(XMLGregorianCalendar messageDateTime) {
        this.messageDateTime = messageDateTime;
    }
    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }
    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
}
