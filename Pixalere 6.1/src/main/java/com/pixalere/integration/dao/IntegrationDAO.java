package com.pixalere.integration.dao;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.integration.bean.EventVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.utils.*;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Collection;
import com.pixalere.utils.Constants;
/**
 * Created by IntelliJ IDEA.
 * User: travis
 * Date: 8-Oct-2009
 * Time: 10:57:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntegrationDAO implements DataAccessObject {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(IntegrationDAO.class);
       
        public IntegrationDAO() {
           
        }
      
        public static void setDatabase(String db) {
            
        }
         /**
     * Inserts a single EventVO object into the patient_accounts table.  The value object passed
     * in has to be of type EventVO.
     *
     * @param insert fields in ValueObject represent fields in the events record.
     *
     * @since 4.1
     */
    public void insert(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            EventVO ev = (EventVO) insert;
            if(ev!=null){
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            ev.setId(null);
            broker.store(ev);
            broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.insert(): " + e.toString(), e);
            
        //throw new DataAccessException("Error in PatientDAO.insert(): "+e.toString(),e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
            e.printStackTrace();
        } catch (Exception e){e.printStackTrace();}finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
     /**
     * Finds a single <<table>> record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the <<table>> table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 4.0
     */
     public ValueObject findByPK(int rec_id) throws DataAccessException {
        PersistenceBroker broker = null;
        EventVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            EventVO ev = new EventVO();
            ev.setId(new Integer(rec_id));
            Query query = new QueryByCriteria(ev);
            resultVO = (EventVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }
     /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 3.2
     * @since 3.0
     */
    public void update(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            EventVO ev = (EventVO) update;
            if(ev!=null){
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(ev);
            broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }

     public void delete(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            EventVO ev = (EventVO) delete;
            Collection evs = findAllByCriteria(ev);
            Iterator iter = evs.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                EventVO currentev = (EventVO) iter.next();
                broker.beginTransaction();
                broker.delete(currentev);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
     public Collection findAllByCriteria( ValueObject vo) throws DataAccessException{
        PersistenceBroker broker = null;
        EventVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            EventVO productsVO=(EventVO)vo;
            QueryByCriteria query = new QueryByCriteria(productsVO);
            Collection<EventVO> col = (Collection) broker.getCollectionByQuery(query);
            return col;
        } catch(ConnectionLocatorException e){
            } catch(Exception e){
            } finally{
            if (broker!=null) broker.close();
        }
        return null;
    }
}
