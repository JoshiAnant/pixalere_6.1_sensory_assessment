package com.pixalere.integration.client;
import com.pixalere.integration.service.IntegrationService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 6.0
 * @see AbstractClient
 *
 */
public class IntegrationAuthClient  extends AbstractClient{
    public IntegrationAuthClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  IntegrationService createIntegrationClient(){//int user_id,String password) {
    	factory.setServiceClass(IntegrationService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"PixalereAuthService");
        IntegrationService service = (IntegrationService) getClient();
        return service;
    }
}