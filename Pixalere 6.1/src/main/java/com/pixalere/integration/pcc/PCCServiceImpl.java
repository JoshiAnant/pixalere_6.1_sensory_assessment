package com.pixalere.integration.pcc;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import com.pixalere.integration.bean.pcc.SubmitMessageResponse;
import com.pixalere.utils.Common;
import java.io.BufferedWriter;
import java.io.FileWriter;
import com.pixalere.integration.service.HL7ServiceImpl;
import java.io.IOException;
import org.apache.commons.codec.binary.Base64;

/**
 * Webservice tied specifically to the PCC interface, see their WSDL in docs.
 * Extends
 *
 * @see com.pixalere.integration.service.HL7ServiceImpl , this class only exists
 * as PCC requires a different namespace, as shown in package-info.
 *
 * @since 6.0
 * @version 6.0
 * @author travis
 */
public class PCCServiceImpl implements PCCService {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PCCServiceImpl.class);

    public PCCServiceImpl() {
    }

    public byte[] SubmitMessage(String username, String password, String data) {
        //public SubmitMessageResponse SubmitMessage(SubmitMessage msg){
        byte[] msh = null;
        //System.out.println("Data: "+data+" "+username+" "+password);
        System.out.println(Common.getConfig("integration")+"=="+("pcc_webservice")+" "+username+"=="+("pix")+" "+password+"=="+(Common.getConfig("webservice_password")));
        if (Common.getConfig("integration").equals("pcc_webservice")) {

            HL7ServiceImpl hservice = new HL7ServiceImpl();
            try {
                Base64 base = new Base64();
                byte[] decode = base.decodeBase64(data);
                String strbase = new String(decode, "UTF-8");
                //System.out.println(strbase);
                // Call HL7 Parser.

                Message ack = hservice.processHL7FlatFile(strbase);
                if (username.equals("pixalere") && password.equals(Common.getConfig("webservice_password"))) {
                    Parser parser = ack.getParser();
                    String message = parser.encode(ack);
                    //send back healthy MSH acknowledgement.
                    msh = message.getBytes();
                } else {

                    ack = ack.generateACK("AR", new HL7Exception("Error: Authenticaton failed.  Please verify the web service username and password"));
                    Parser parser = ack.getParser();
                    String message = parser.encode(ack);
                    msh = message.getBytes();
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Exception :" + e.getMessage());
                //send back error MSH acknowledgement.
                //String message = "MSH|^~\\&|PCC|123|PCC|321|20070518135133.671-0500||ACK|112A050DF2758_G|D|2.5\n"+
                //   "MSA|AE||"+e.getMessage()+"|1";
                //Base64 base = new Base64();
                //msh = base.encodeBase64(message.getBytes());
            }


            return msh;
        } else {
            try {
                HL7ServiceImpl hservice = new HL7ServiceImpl();
                Base64 base = new Base64();
                byte[] decode = base.decodeBase64(data);
                String strbase = new String(decode, "UTF-8");
System.out.println("hl7 "+strbase);
                Message ack = hservice.parseMessage(strbase);
                if (!username.equals("pixalere") || !password.equals(Common.getConfig("webservice_password"))) {
                    ack = ack.generateACK("AR", new HL7Exception("Error: Authentication failed.  Please verify the web service username and password"));
                }
                //authentication failed
                Parser parser = ack.getParser();
                String message = parser.encode(ack);
                return message.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (HL7Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
