
package com.pixalere.integration.pcc;
import com.pixalere.common.ApplicationException;
//import com.pixalere.integration.bean.pcc.SubmitMessageResponse;
import javax.jws.WebService;
import javax.jws.WebParam;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.soap.*;
import javax.jws.soap.SOAPBinding.*;
import javax.xml.ws.ResponseWrapper;
import javax.xml.ws.*;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 6.0
 *
 */
@WebService (targetNamespace = "http://www.pointclickcare.com/msg/")
//@SOAPBinding(parameterStyle = ParameterStyle.BARE)
public interface PCCService {
    @WebMethod(operationName="SubmitMessage")
    //@ResponseWrapper(targetNamespace="http://www.pointclickcare.com/msg/",
    // className="com.pixalere.integration.bean.pcc.SubmitMessageResponse")
    @WebResult(targetNamespace="http://www.pointclickcare.com/msg/",
     name="data")
    public byte[] SubmitMessage(@WebParam(name="username",targetNamespace="http://www.pointclickcare.com/msg/") String username, @WebParam(name="password",targetNamespace="http://www.pointclickcare.com/msg/") String password,@WebParam(name="data",targetNamespace="http://www.pointclickcare.com/msg/") String data) ;
}