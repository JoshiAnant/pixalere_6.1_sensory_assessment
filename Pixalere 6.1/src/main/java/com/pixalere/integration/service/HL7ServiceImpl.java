/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.integration.service;

import java.util.List;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import ca.uhn.hl7v2.parser.*;
import ca.uhn.hl7v2.model.AbstractMessage;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.PID;
import ca.uhn.hl7v2.model.v26.segment.AL1;
import ca.uhn.hl7v2.model.v26.segment.PV1;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.datatype.CX;
import ca.uhn.hl7v2.model.v26.datatype.DTM;
import ca.uhn.hl7v2.model.v26.datatype.ST;
import java.util.Date;
import ca.uhn.hl7v2.model.v26.datatype.CWE;
import com.pixalere.utils.Common;
import ca.uhn.hl7v2.model.v26.message.ORU_R01;
import ca.uhn.hl7v2.model.v26.message.ADT_A01;
import ca.uhn.hl7v2.model.v26.message.ADT_A02;
import ca.uhn.hl7v2.model.v26.message.ADT_A03;
import ca.uhn.hl7v2.model.v26.message.ADT_A05;
import ca.uhn.hl7v2.model.v26.message.ADT_A09;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.*;
import ca.uhn.hl7v2.model.v26.message.ADT_A12;
import ca.uhn.hl7v2.model.v26.segment.PID;
import ca.uhn.hl7v2.model.v26.segment.OBX;
import ca.uhn.hl7v2.model.v26.segment.IN1;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.group.ADT_A01_INSURANCE;
import ca.uhn.hl7v2.model.v26.group.ADT_A03_INSURANCE;
import ca.uhn.hl7v2.model.v26.group.ADT_A05_INSURANCE;
import com.pixalere.integration.bean.Event;
import com.pixalere.integration.bean.Gender;
import com.pixalere.integration.bean.Patient;
import com.pixalere.integration.bean.paris.MPISearchClientResult;
import com.pixalere.integration.bean.paris.SearchClientResult;
import com.pixalere.integration.bean.pcc.SubmitMessageResponse;
import com.pixalere.integration.service.IntegrationService;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.PDate;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author travis
 */
public class HL7ServiceImpl {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HL7ServiceImpl.class);

    public HL7ServiceImpl() {
    }

    public Message parseMessage(String content) throws HL7Exception,IOException {
        Message message = null;
        CanonicalModelClassFactory mcf = new CanonicalModelClassFactory("2.6");
        PipeParser pipeParser = new PipeParser(mcf);
        pipeParser.setValidationContext(new NoValidation());
        try {
            //parse the message string into a Message object
            if (content.indexOf("FHS") != -1) {
                BufferedReader b = new BufferedReader(new StringReader(content));
                String line = "";
                StringBuilder msgString = new StringBuilder();
                while ((line = b.readLine()) != null) {
                    if (line.indexOf("FHS") != -1 || line.indexOf("BHS") != -1) {
                        msgString = new StringBuilder();
                    } else if (line.indexOf("FTS") == -1 && line.indexOf("BTS") == -1) {
                        msgString.append(line + "\n\r");
                    }
                    if (line.indexOf("FTS") != -1) {
                        message = pipeParser.parse(msgString.toString());
                        //msh = extractMessage(message);
                    }
                }
            } else {
                //System.out.println("PRE "+content);
                message = pipeParser.parse(content);
                //System.out.println("POST "+message);
            }
            return message;
        } catch (HL7Exception e) {
            e.printStackTrace();
            try {
                return message.generateACK("AE", e);
            } catch (HL7Exception ee) {
                log.error("HL7Exception: " + ee.getMessage());
                System.out.println("HL7Exception: " + ee.getMessage());
                return null;
            } catch (IOException ee) {
                log.error("HL7Exception: " + ee.getMessage());
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            java.lang.Throwable cause=new java.lang.Throwable();
            HL7Exception hle=new HL7Exception("ERROR Parsing document"+e.getMessage(),cause);
            throw hle;
        }
    }

    public Message processHL7FlatFile(String content) throws HL7Exception,IOException {
        Message message = null;
        message = parseMessage(content);
        try {
            saveMessage(message);
        } catch (HL7Exception e) {
            e.printStackTrace();
            try {
                return message.generateACK("AE", e);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        try {
            return message.generateACK();
        } catch (HL7Exception e) {
            e.printStackTrace();
            try {
                return message.generateACK("AE", e);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void processXMLParisFlatFile(FileReader fileObj) throws Exception {
        JAXBContext cont = JAXBContext.newInstance(MPISearchClientResult.class);
        IntegrationServiceImpl iservice = new IntegrationServiceImpl();
        SchemaFactory sf = SchemaFactory.newInstance(
                javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
        //validtes xml file.
        //Schema schema = sf.newSchema(new File("mpi.xsd"));
        Unmarshaller um = cont.createUnmarshaller();
        MPISearchClientResult xml_file = (MPISearchClientResult) um.unmarshal(fileObj);
        log.info("Reading " + xml_file.getSearchClientResults().size() + " Patient records");
        for (SearchClientResult result : xml_file.getSearchClientResults()) {
            Patient patient = new Patient();
            // patient.setAccountNumber();
            if (result.getFirstName() != null) {
                patient.setFirstName(result.getFirstName());
            }
            if (result.getLastName() != null) {
                patient.setLastName(result.getLastName());
            }
            
            if (result.getPhn() != null) {
                patient.setPhn(result.getPhn());
            }
            patient.setMrn(result.getClientId() + "");
            if (result.getBirthDate() != null) {
                patient.setDateOfBirth(result.getBirthDate());
            }
            if (result.getGender() != null) {
                patient.setGender(Gender.fromValue(result.getGender()));
            }
            Event event = new Event();
            event.setSendingFacility("PARIS");
            event.setMessageDateTime(PDate.epochToGregorian(PDate.getEpochTime() + ""));
            event.setReason(result.getSource());
            iservice.savePatient(patient, event);
        }
    }

    private void saveMessage(Message message) throws HL7Exception {
        IntegrationServiceImpl iservice = new IntegrationServiceImpl();
        PID pid = null;
        MSH msh = null;
        IN1 ins = null;
        PV1 pv1 = null;
        List<AL1> all = null;
        
        if(message instanceof ORU_R01){
            ORU_R01 ac = (ORU_R01)message;
            for(int i=0 ; i<=ac.getPATIENT_RESULTReps(); i++) {
                for(int j=0 ;j<=ac.getPATIENT_RESULT(i).getORDER_OBSERVATIONReps(); j++) {
                    for(int k=0 ;k<=ac.getPATIENT_RESULT(i).getORDER_OBSERVATION(j).getOBSERVATIONReps();k++) {
                        OBX o = ac.getPATIENT_RESULT(i).getORDER_OBSERVATION(j).getOBSERVATION(k).getOBX();
                        //parse obx
                       
                    }
                }
             }
        }else if (message instanceof ADT_A01) {
            
            ADT_A01 ack = (ADT_A01) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            ADT_A01_INSURANCE tmp = ack.getINSURANCE();
            ins = tmp.getIN1();
            pv1 = ack.getPV1();
            all = ack.getAL1All();
        } else if (message instanceof ADT_A02) {
            ADT_A02 ack = (ADT_A02) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            pv1 = ack.getPV1();
        } else if (message instanceof ADT_A03) {
            ADT_A03 ack = (ADT_A03) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            ADT_A03_INSURANCE tmp = ack.getINSURANCE();
            ins = tmp.getIN1();
            pv1 = ack.getPV1();
            all = ack.getAL1All();
        } else if (message instanceof ADT_A05) {
            ADT_A05 ack = (ADT_A05) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            ADT_A05_INSURANCE tmp = ack.getINSURANCE();
            ins = tmp.getIN1();
            pv1 = ack.getPV1();
            all = ack.getAL1All();
        } else if (message instanceof ADT_A09) {
            ADT_A09 ack = (ADT_A09) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            pv1 = ack.getPV1();
        } else if (message instanceof ADT_A12) {
            ADT_A12 ack = (ADT_A12) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            pv1 = ack.getPV1();
        } else {
            throw new HL7Exception("Unsupported ADT :" + message.printStructure());
        }
        
        try {
            Patient patient = new Patient();
            // patient.setAccountNumber();
            if (pid.getPatientName() != null && pid.getPatientName().length >= 1) {
                patient.setFirstName(pid.getPatientName()[0].getGivenName().getValue());
            }
            if (pid.getPatientName() != null && pid.getPatientName().length >= 1) {
                patient.setLastName(pid.getPatientName()[0].getFamilyName().getSurname().getValue());
            }
            if (pid.getPatientName() != null && pid.getPatientName().length >= 1) {
                   patient.setMiddleName(pid.getPatientName()[0].getSecondAndFurtherGivenNamesOrInitialsThereof().getValue());
            }
            
            if (pv1.getDischargeDateTime() != null) {
                Date date = pv1.getDischargeDateTime().getValueAsDate();
                patient.setDischargeDate(date);
            }
            if (pv1.getAdmitDateTime() != null) {
                Date date = pv1.getAdmitDateTime().getValueAsDate();
                patient.setAdmitDate(date);
            }
            if (all != null) {
                String allergies = "";
                int cnt = 0;
                for (AL1 al1 : all) {
                    
                    if (al1 != null && al1.getAllergenCodeMnemonicDescription()!= null) {
                        CWE aller = al1.getAllergenCodeMnemonicDescription();
                        if (aller.getText() != null) {
                            ST str = aller.getText();
                            
                            if (str != null) {
                               if(cnt != 0){allergies=allergies+", ";}
                               allergies=allergies+str.getValue();
                               cnt++;
                            }
                        }
                    }
                }
                
                 patient.setAllergies(allergies);
            }
            CX[] ids = pid.getPatientIdentifierList();
            String mrn = null;
            String phn = null;
            if(ids!=null){
                for (CX cx : ids) {
                    //System.out.println(cx.getIdentifierCheckDigit() + "=======" + cx.getIDNumber() + "----" + cx.getIdentifierTypeCode().getValue());
                    if (cx.getIdentifierTypeCode().getValue() != null && cx.getIdentifierTypeCode().getValue().equals("PN")) {
                        mrn = cx.getIDNumber().getValue();
                    }
                    if (cx.getIdentifierTypeCode().getValue() != null && (cx.getIdentifierTypeCode().getValue().equals("PI") || cx.getIdentifierTypeCode().getValue().equals("MRN"))) {
                        mrn = cx.getIDNumber().getValue();
                    }
                    if (cx.getIdentifierTypeCode().getValue() != null && cx.getIdentifierTypeCode().getValue().equals("HCN")) {
                        phn = cx.getIDNumber().getValue();
                    }
                }

                if (ids.length == 1) {
                    //grab the first id and use it for an mrn
                }
            }
            
            /*if (pid.getPatientAccountNumber() != null && phn == null) {
                System.out.println("PHN: "+ pid.getPatientAccountNumber().getIDNumber());
                phn = pid.getPatientAccountNumber().getIDNumber().getValue().replaceAll(" ", "");
                
            }*/
            patient.setPhn(phn);
            patient.setMrn(mrn);
            if (pid.getDateTimeOfBirth() != null) {
                PDate pdate = new PDate();
                patient.setDateOfBirth(pdate.HL7DOBToGregorian(pid.getDateTimeOfBirth().getValue()));
            }
            if (pid.getAdministrativeSex() != null) {
                String gend = pid.getAdministrativeSex().getValue();
                if (gend != null) {
                    if (gend.equals("M")) {
                        patient.setGender(Gender.fromValue("Male"));
                    } else if (gend.equals("F")) {
                        patient.setGender(Gender.fromValue("Female"));
                    } else if (gend.equals("T")) {
                        patient.setGender(Gender.fromValue("Transgender"));
                    } else if (gend.equals("U")) {
                        patient.setGender(Gender.fromValue("Unknown"));
                    }
                }
            }
            Event event = new Event();
            event.setSendingFacility(msh.getSendingFacility().getNamespaceID().getValue());
            event.setMessageDateTime(PDate.epochToGregorian(PDate.getEpochTime() + ""));
            event.setReason(msh.getMessageType().getMessageCode().getValue());
            iservice.savePatient(patient, event);
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw new HL7Exception("ApplicationError:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new HL7Exception("Exception: " + e.getMessage());
        }
    }
}
