package com.pixalere.integration.service;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.bean.ReferralsTrackingVO;
import java.util.Collection;
import com.pixalere.integration.bean.HL7Response;
import com.pixalere.integration.bean.Patient;
import com.pixalere.integration.bean.Gender;
import com.pixalere.integration.bean.Event;
import com.pixalere.integration.bean.EventVO;
import com.pixalere.integration.service.IntegrationService;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.integration.dao.IntegrationDAO;
import com.pixalere.common.ApplicationException;
import com.pixalere.integration.client.IntegrationClient;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import java.io.FileWriter;
import java.io.BufferedWriter;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.codec.binary.Base64;
import com.pixalere.utils.PDate;
import java.util.Hashtable;
import java.util.Date;
import java.io.IOException;

/**
 * This is the integration (w pci/meditech etc) service implementation Refer to {@link com.pixalere.integration.service.IntegrationService
 * } to see if there are any web services available.
 * <p/>
 * <img src="IntegrationServiceImpl.png"/>
 *
 * @view IntegrationServiceImpl
 * @match class com.pixalere.integration.
 *
 * @opt hide
 * @match class
 * com.pixalere.integration\.(dao.IntegrationDAO|bean.IntegrationVO|service.IntegrationService)
 * @opt !hide
 */
public class IntegrationServiceImpl implements IntegrationService {

    public IntegrationServiceImpl() {
    }

    /**
     * Merge patients passed from EHR
     *
     * @param patient_old the old patient object (XML translation) - JAX-WS
     * @param patient_new the patient we're merging into (XML translation to
     * Object)
     * @param event
     * @throws ApplicationException
     */
    public void mergePatient(Patient patient_old, Patient patient_new, Event event) throws ApplicationException {
        if (Common.getConfig("integration").equals("xml_webservice")) {
            PatientServiceImpl patientService = new PatientServiceImpl(1);
            IntegrationDAO integrationDAO = new IntegrationDAO();
            //get patient from pix by phn/mrn dob etc.. then pass id's to patientserviceImpl.
            try {
                if (patient_old != null && patient_new != null) {
                    PatientAccountVO old = new PatientAccountVO();
                    old.setPhn(patient_old.getPhn());
                    old.setPhn2(patient_old.getMrn());
                    old.setCurrent_flag(1);
                    PatientAccountVO foundOld = patientService.getPatient(old);
                    if (foundOld != null) {
                        PatientAccountVO newP = new PatientAccountVO();
                        newP.setPhn(patient_new.getPhn());
                        newP.setPhn2(patient_new.getMrn());
                        newP.setCurrent_flag(1);
                        PatientAccountVO foundNew = patientService.getPatient(newP);
                        if (foundNew != null) {
                            EventVO evVO = new EventVO();
                            try {
                                evVO.setMessageDateTime(event.getMessageDateTime().toString());
                                evVO.setMessageId(event.getMessageId());
                                evVO.setSendingFacility(event.getSendingFacility());
                            } catch (NullPointerException exx) {
                                exx.printStackTrace();
                                throw new ApplicationException("ApplicationException: mergePatient - saveEvent " + exx.toString(), exx);
                            }
                            patientService.mergePatient(foundOld.getPatient_id(), foundNew.getPatient_id(), evVO);
                        }
                    }
                }
                //save event

            } catch (ApplicationException ex) {
                throw new ApplicationException("ApplicationException: mergePatient " + ex.toString(), ex);
            }
        } else {
            //Someone is trying to call this web service, but it's turned off. 
            //throw an error specifying such.
            throw new ApplicationException("Error: Webservice is turned off.  Please have a Pixalere administrator turn on web services.");
        }
    }

    /**
     * Save the patient being passed from the web service
     *
     * @param patient the patient object from the EMR/EHR from the web service
     * @param event the event object passed through the web service (where the
     * service was called from)
     *
     * @throws ApplicationException
     */
    public void savePatient(Patient patient, Event event) throws ApplicationException {
        System.out.println("Integration: " + Common.getConfig("integration"));
        if (Common.getConfig("integration").equals("xml_webservice") || Common.getConfig("integration").equals("hl7_webservice") || Common.getConfig("integration").equals("pcc_webservice") || Common.getConfig("integration").equals("flatfile")) {//flat file uses internal webservices (local)
            //check if patient exists by phn/mrn/dob etc.
            PatientServiceImpl patientService = new PatientServiceImpl(1);
            IntegrationDAO integrationDAO = new IntegrationDAO();
            try {
                String phn = patient.getPhn();
                String mrn = patient.getMrn();
                String validation = Common.getConfig("integration_validation");
                Hashtable result = patientService.getPatient(phn, mrn, validation);
                PatientAccountVO foundPatient = (PatientAccountVO) result.get("object");
                PDate pdate = new PDate();
                /*
                 If patients with same MRN is found, we update the patient in Pixalere.   Otherwise
                 we then check if a patient exists with the same PHN.   Reason being is that if the customer
                 turns this option on, we need to ensure that the same patient doesn't exist twice in the client system
                 with the same PHN, but different hospital or customer ID's.  But because each of these ID's can be different
                 but still be the same patient we can't just do an AND in the sql call.  ie: get all patients where MRN = and PHN=
                 because one might exist without the other.   But we also can't do an OR because we treat them both differently.
                 */
                //System.out.println("Checking"+ foundPatient+" "+mrn+" "+phn);
                if (foundPatient == null) {

                    if (!validation.equals("phn") || ((phn != null && !phn.equals("")) || (mrn != null && !mrn.equals("")))) {
                        foundPatient = new PatientAccountVO();
                        foundPatient.setAccount_status(1);
                        foundPatient.setCurrent_flag(1);
                        foundPatient.setPhn(patient.getPhn());

                        try {
                            boolean valid = patientService.validatePHN(patient.getPhn());
                            if (valid == true && patient.getPhn() != null) {
                                foundPatient.setOutofprovince(0);
                            } else {
                                String error = "Invalid PHN: ";
                                String message = error + " " + patient.getPhn() + " ID: " + patient.getMrn() + "\n\n";
                                log(message);
                                foundPatient.setOutofprovince(1);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            foundPatient.setOutofprovince(1);
                        }
                        foundPatient.setCreated_on(new Date());
                        if (patient.getDischargeDate() != null) {
                            foundPatient.setDischarge_date(patient.getDischargeDate());
                            foundPatient.setAccount_status(0);
                            foundPatient.setAction("DISCHARGE");
                        } else if (patient.getAdmitDate() != null) {
                            foundPatient.setCreated_on(patient.getAdmitDate());
                            foundPatient.setAccount_status(1);
                            foundPatient.setAction("ADMITTED");
                        } else {
                            foundPatient.setAccount_status(1);
                            foundPatient.setAction("ADMITTED");
                        }
                        foundPatient.setPhn2(patient.getMrn());
                        foundPatient.setCreated_by(6);
                        if(patient.getFirstName()!=null){
                            foundPatient.setFirstName(patient.getFirstName());
                        }
                        if(patient.getLastName()!=null){
                            foundPatient.setLastName(patient.getLastName());
                        }
                        if(patient.getMiddleName()!=null){
                            foundPatient.setMiddleName(patient.getMiddleName());
                        }
                        foundPatient.setAllergies(patient.getAllergies());
                        int gend = 0;
                        if (patient == null) {
                            System.out.println("Patient from Meditech is NULL!" + patient);
                        } else if (patient.getGender() == null) {
                            System.out.println("Patient from Meditech has a null Gender: " + patient.getPhn());
                        }
                        try {
                            if (patient.getGender() != null) {
                                if (patient.getGender().value().equals(Constants.MALE) || (patient.getGender().value()).equals(Constants.MALE_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("male"));
                                    foundPatient.setGender(gend);
                                } else if (patient.getGender().value().equals(Constants.FEMALE) || (patient.getGender().value()).equals(Constants.FEMALE_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("female"));
                                    foundPatient.setGender(gend);
                                } else if (patient.getGender().value().equals(Constants.UNKNOWN) || (patient.getGender().value()).equals(Constants.UNKNOWN_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("unknownGender"));
                                    foundPatient.setGender(gend);
                                } else if ((patient.getGender().value()).equals(Constants.TRANSGENDER) || (patient.getGender().value()).equals(Constants.TRANSGENDER_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("transGender"));
                                    foundPatient.setGender(gend);
                                }
                            }
                        } catch (Exception e) {
                        }
                        /*foundPatient.setAddressLine1(patient.getAddressLine1());
                         foundPatient.setAddressLine2(patient.getAddressLine2());
                         foundPatient.setAddressCity(patient.getCity());
                         foundPatient.setAddressProvince(patient.getProvince());
                         foundPatient.setAddressPostalCode(patient.getPostalCode()); */
                        if (patient.getDateOfBirth() != null) {
                            foundPatient.setDob(patient.getDateOfBirth().toString());
                        }
                        if(patient.getLastName()!=null){
                            foundPatient.setLastName_search(Common.stripName(patient.getLastName()));
                        }
                        foundPatient.setUser_signature(pdate.getDateStringWithHour(new Date(), "en") + " by " + event.getSendingFacility());
                        foundPatient.setVisits(0);
                        int ti = Constants.NO_TREATMENT_LOCATION;//legacy need to remove
                        try {
                            ti = Integer.parseInt(Common.getConfig("noTreatmentListID"));
                        } catch (NumberFormatException e) {
                        }
                        foundPatient.setTreatment_location_id(ti);//We don't know where they belong, so they go in no Treatment Location to force a move
                        patientService.updatePatient(foundPatient); //method only used by meditech to not add patient_id
                        EventVO evVO = new EventVO();
                        try {
                            evVO.setMessageDateTime(event.getMessageDateTime().toString());
                            evVO.setMessageId(event.getMessageId());
                            evVO.setSendingFacility(event.getSendingFacility());
                        } catch (NullPointerException exx) {
                            exx.printStackTrace();
                            throw new ApplicationException("ApplicationException: savePatient - saving Event " + exx.toString(), exx);
                        }

                        integrationDAO.insert(evVO);
                    } else {
                        String error = "No MRN or PHN # found.  Assumed Patient was new and saved to Pixalere.";
                        String message = "\n\n" + error + "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n\nEMR:\nMRN ID:" + patient.getMrn() + "\nFirst Name:" + patient.getFirstName() + "\nLast Name:" + patient.getLastName() + "\nPHN:" + patient.getPhn() + "\nDate of Birth:" + patient.getDateOfBirth().toString() + "\n\n\n";
                        log(message);
                    }
                } else {
                    /**
                     * This is where we need to do more data mapping. If the PHN
                     * matches, and atleast one other field (lastname, or date
                     * of birth) then we accept the record, otherwise, we send
                     * an email with the exception to the super administrator.
                     *
                     */
                    String criteria = (String) result.get("criteria");
                    //if we had to use a phn then check lastname/dob.
                    //System.out.println("DB:"+foundPatient.getDob()+" "+patient.getDateOfBirth().toString());
                    if ((criteria.equals("phn") && (patient.getLastName().toLowerCase().equals(foundPatient.getLastName().toLowerCase()) || (patient.getLastName().toLowerCase().equals(foundPatient.getFirstName().toLowerCase()) && patient.getFirstName().toLowerCase().equals(foundPatient.getLastName().toLowerCase())) || (PDate.gregorianToEpoch(patient.getDateOfBirth()) == foundPatient.getDateOfBirth().getTime()))) || criteria.equals("secondary_phn")) {

                        //No MRN exists, but we did find a PHN.. update the record.
                        PatientAccountVO updatePatient = new PatientAccountVO();
                        // we found a patient who already exists.. so update it.
                        if (patient.getPhn() != null) {
                            try {
                                boolean valid = patientService.validatePHN(patient.getPhn());
                                if (patient.getPhn() != null && valid == true) {
                                    updatePatient.setOutofprovince(0);
                                } else {
                                    String error = "Invalid PHN: ";
                                    String message = error + " " + patient.getPhn() + " ID: " + patient.getMrn() + "\n\n";
                                    log(message);
                                    updatePatient.setOutofprovince(1);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                updatePatient.setOutofprovince(1);
                            }
                            updatePatient.setPhn(patient.getPhn());
                        }
                        updatePatient.setPhn2(patient.getMrn());
                        updatePatient.setCreated_by(6);
                        if(patient.getFirstName()!=null){
                            updatePatient.setFirstName(patient.getFirstName());
                        }
                        if(patient.getLastName()!=null){
                            updatePatient.setLastName(patient.getLastName());
                        }
                        if(patient.getMiddleName()!=null){
                            updatePatient.setMiddleName(patient.getMiddleName());
                        }
                        if (patient.getAllergies() != null) {
                            updatePatient.setAllergies(patient.getAllergies());
                        } else {
                            updatePatient.setAllergies(foundPatient.getAllergies());
                        }
                        int gend = 0;
                        if (patient == null) {
                            System.out.println("Patient from Meditech is NULL!" + patient);
                        } else if (patient.getGender() == null) {
                            System.out.println("Patient from Meditech has a null Gender: " + patient.getPhn());
                        }
                        updatePatient.setCreated_on(new Date());
                        //System.out.println("customer : "+Common.getConfig("customer_name")+" : "+Common.getConfig("customer_name").equals("Interior Health Authority"));
                        if (!Common.getConfig("customer_name").equals("Interior Health Authority")) {
                            if (patient.getDischargeDate() != null) {
                                updatePatient.setDischarge_date(patient.getDischargeDate());
                                updatePatient.setAccount_status(0);
                                if (foundPatient.getDischarge_date() == null) {
                                    updatePatient.setAction("DISCHARGED");
                                }
                            } else if (patient.getAdmitDate() != null) {
                                if(patient.getAdmitDate().before(new Date())){
                                    updatePatient.setCreated_on(patient.getAdmitDate());
                                }
                                updatePatient.setAccount_status(1);
                                if (foundPatient.getDischarge_date() != null) {
                                    updatePatient.setAction("ADMITTED");
                                }
                            } else {
                                updatePatient.setAccount_status(foundPatient.getAccount_status());
                                updatePatient.setAction(foundPatient.getAction());
                            }
                        } else {
                           updatePatient.setAccount_status(foundPatient.getAccount_status());
                           updatePatient.setAction(foundPatient.getAction());
                        }
                        try {
                            if (patient.getGender() != null) {
                                if (patient.getGender().value().equals(Constants.MALE) || (patient.getGender().value()).equals(Constants.MALE_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("male"));
                                    updatePatient.setGender(gend);
                                } else if (patient.getGender().value().equals(Constants.FEMALE) || (patient.getGender().value()).equals(Constants.FEMALE_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("female"));
                                    updatePatient.setGender(gend);
                                } else if (patient.getGender().value().equals(Constants.UNKNOWN) || (patient.getGender().value()).equals(Constants.UNKNOWN_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("unknownGender"));
                                    updatePatient.setGender(gend);
                                } else if ((patient.getGender().value()).equals(Constants.TRANSGENDER) || (patient.getGender().value()).equals(Constants.TRANSGENDER_SHORT)) {
                                    gend = Integer.parseInt(Common.getConfig("transgender"));
                                    updatePatient.setGender(gend);
                                }
                            }
                        } catch (Exception e) {
                        }
                        /* foundPatient.setAddressLine1(patient.getAddressLine1());
                         foundPatient.setAddressLine2(patient.getAddressLine2());
                         foundPatient.setAddressCity(patient.getCity());
                         foundPatient.setAddressProvince(patient.getProvince());
                         foundPatient.setAddressPostalCode(patient.getPostalCode());   */
                        if (patient.getDateOfBirth() != null) {
                            updatePatient.setDob(patient.getDateOfBirth().toString());
                        }
                        try {
                            boolean valid = patientService.validatePHN(patient.getPhn());
                            if (valid == true) {
                                updatePatient.setOutofprovince(0);
                            } else {
                                updatePatient.setOutofprovince(1);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            updatePatient.setOutofprovince(1);
                        }
                        //updatePatient.setAccount_status(foundPatient.getAccount_status());
                        updatePatient.setPatient_id(foundPatient.getPatient_id());
                        updatePatient.setTreatment_location_id(foundPatient.getTreatment_location_id());
                        updatePatient.setCurrent_flag(1);
                        updatePatient.setFunding_source(foundPatient.getFunding_source());
                        updatePatient.setVisits(foundPatient.getVisits());
                        updatePatient.setPatient_residence(foundPatient.getPatient_residence());
                        if(updatePatient.getLastName()!=null){
                            updatePatient.setLastName_search(Common.stripName(patient.getLastName()));
                        }
                        updatePatient.setUser_signature(new PDate().getDateStringWithHour(new Date(), "en") + " by " + event.getSendingFacility());
                        if (diffPatient(foundPatient, updatePatient)) {
                            if (phn != null && !phn.trim().equals("") || mrn != null && !mrn.equals("")) {
                                if (foundPatient.getPatient_id() == null) {
                                    updatePatient.setId(foundPatient.getId());
                                    patientService.updatePatient(updatePatient);
                                } else {
                                    patientService.savePatient(updatePatient);

                                    PatientAccountVO p = patientService.getPatient(foundPatient.getPatient_id());

                                    //update referrals with new location so proper clinician has access.
                                    ReferralsTrackingServiceImpl refManager = new ReferralsTrackingServiceImpl(1);
                                    ReferralsTrackingVO r = new ReferralsTrackingVO();
                                    //r.setEntry_type(Constants.REFERRAL);
                                    r.setCurrent(1);
                                    r.setPatient_id(foundPatient.getPatient_id());
                                    if (p != null) {
                                        Collection<ReferralsTrackingVO> refs = refManager.getAllReferralsByCriteria(r);

                                        for (ReferralsTrackingVO ref : refs) {
                                            ref.setPatient_account_id(p.getId());
                                            refManager.saveReferralsTracking(ref);
                                        }
                                    }
                                }
                            } else {
                                System.out.println("Did not Update: Blank PHN");
                            }
                        }
                        //PatientAccountVO newPatient = patientService.getPatient(updatePatient);
                        EventVO evVO = new EventVO();
                        try {
                            evVO.setMessageDateTime(event.getMessageDateTime().toString());
                            evVO.setMessageId(event.getMessageId());
                            evVO.setSendingFacility(event.getSendingFacility());
                        } catch (NullPointerException exx) {
                            exx.printStackTrace();
                            throw new ApplicationException("ApplicationException: savePatient - saving Event " + exx.toString(), exx);
                        }
                        integrationDAO.insert(evVO);
                    } else {
                        String error = "PHN Found, but no match for Last Name or Date of Birth";
                        //Send email about potential duplicate patient.
                        String message = "\n\n" + error + "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n\nPARIS:\nPARIS ID:" + patient.getMrn() + "\nFirst Name:" + patient.getFirstName() + "\nLast Name:" + patient.getLastName() + "\nPHN:" + patient.getPhn() + "\nDate of Birth:" + patient.getDateOfBirth().toString() + "\n\n\nPixalere:\nFirst Name:" + foundPatient.getFirstName() + "\nLast Name:" + foundPatient.getLastName() + "\nPHN:" + foundPatient.getPhn() + "\nDate of Birth:" + foundPatient.getDob();
                        log(message);
                    }
                }
            } catch (ApplicationException ex) {
                ex.printStackTrace();
                System.out.println("P " + patient.getPhn() + " - app. exception: " + ex.getMessage());
                throw new ApplicationException("ApplicationException: savePatient " + ex.toString(), ex);
                //return to medi
            } catch (com.pixalere.common.DataAccessException ex) {
                ex.printStackTrace();
                System.out.println("P " + patient.getPhn() + " - app. exception: " + ex.getMessage());
                throw new ApplicationException("ApplicationException: savePatient " + ex.toString(), ex);
            } catch (Exception e) {
                //trying to find a weird phn blanke xception
                e.printStackTrace();
            }
        } else {
            //Someone is trying to call this web service, but it's turned off. 
            //throw an error specifying such.
            throw new ApplicationException("Error: Webservice is turned off.  Please have a Pixalere administrator turn on web services.");
        }
    }

    /**
     * The HL7 web service for pushing hl7 ADT requests to Pixalere.
     *
     * @param username
     * @param password
     * @param data
     * @return Acknowledgement.
     */
    public HL7Response submitMessage(String username, String password, String encoded_message) {
        if (Common.getConfig("integration").equals("hl7_webservice") && username.equals("pix") && password.equals(Common.getConfig("webservice_password"))) {
            byte[] msh = null;
            HL7ServiceImpl hservice = new HL7ServiceImpl();
            try {
                Base64 base = new Base64();
                byte[] decode = base.decodeBase64(encoded_message);
                String strbase = new String(decode, "UTF-8");
                System.out.println(strbase);
                // Call HL7 Parser.
                Message ack = hservice.processHL7FlatFile(strbase);
                Parser parser = ack.getParser();
                String message = parser.encode(ack);
                //send back healthy MSH acknowledgement.
                msh = base.encodeBase64(message.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
                log("Exception :" + e.getMessage());
                //ack.generateAck("Error:"+e.getMessage());
                //send back error MSH acknowledgement.
                //String message = "MSH|^~\\&|PCC|123|PCC|321|20070518135133.671-0500||ACK|112A050DF2758_G|D|2.5\n"+
                //   "MSA|AE||"+e.getMessage()+"|1";
                //Base64 base = new Base64();
                //msh = base.encodeBase64(message.getBytes());
            }
            return new HL7Response(msh);
        } else {
            //Someone is trying to call this web service, but it's turned off. 
            //throw an error specifying such.
            HL7ServiceImpl hservice = new HL7ServiceImpl();
            Base64 base = new Base64();
            
            try {
                Message ack = hservice.parseMessage(encoded_message);
                if (!Common.getConfig("integration").equals("hl7_webservice")) {
                    ack = ack.generateACK("AR", new HL7Exception("Error: Webservice is turned off.  Please have a Pixalere administrator turn on web services."));
                } else if (!username.equals("pix") || !password.equals(Common.getConfig("webservice_password"))) {
                    ack = ack.generateACK("AR", new HL7Exception("Error: Authenticaton failed.  Please verify the web service username and password"));
                }
                //ack.generateACK(message, null)
                //message = "Error: Webservice is turned off.  Please have a Pixalere administrator turn on web services.");
                Parser parser = ack.getParser();
                String message = parser.encode(ack);
                return new HL7Response(base.encodeBase64(message.getBytes()));
            } catch (IOException e) {
                return null;
            } catch (HL7Exception e) {
                return null;
            }
        }
    }

    private void log(String message) {
        try {
            // Create file
            FileWriter fstream = new FileWriter(Common.getConfig("dataPath") + "/pix_mapping.log", true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(message);
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    /**
     * We're storing millions of duplicate records, so we need to run a diff on
     * the record, and only update if there's a change.
     *
     * @param currentPatient the current patient in the database's java bean
     * @param newPatient from interface's java bean.
     * @return boolean true if there's a difference, false if they equal
     *
     */
    public boolean diffPatient(PatientAccountVO currentPatient, PatientAccountVO newPatient) {
        boolean changed = false;
        if (currentPatient == null) {
            changed = true;
        } else if (newPatient == null) {
            return changed;//we're returning false as the record coming from interface is null, no need to insert
        } else {
            if (compareObject(currentPatient.getDateOfBirth(), newPatient.getDateOfBirth())) {
                changed = true;
            } else if (compareObject(currentPatient.getFirstName(), newPatient.getFirstName())) {
                changed = true;
            } else if (compareObject(currentPatient.getLastName(), newPatient.getLastName())) {
                changed = true;
            } else if (compareObject(currentPatient.getGender(), newPatient.getGender())) {
                changed = true;
            } else if (compareObject(currentPatient.getPhn(), newPatient.getPhn())) {
                changed = true;
            } else if (compareObject(currentPatient.getPhn2(), newPatient.getPhn2())) {
                changed = true;
            } else if (compareObject(currentPatient.getAllergies(), newPatient.getAllergies())) {
                changed = true;
            } else if (compareObject(currentPatient.getAccount_status(), newPatient.getAccount_status())) {
                changed = true;
            } else if (compareObject(currentPatient.getDischarge_date(), newPatient.getDischarge_date())) {
                changed = true;
            } else if (compareObject(currentPatient.getAction(), newPatient.getAction())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Refactoring comparing into own method.
     */
    public boolean compareObject(Object obj1, Object obj2) {
        if (obj1 instanceof Date) {
            Date d1 = (Date) obj1;
            Date d2 = (Date) obj2;
            if (d1 == null && d2 != null) {
                return true;
            } else if (d1 != null && d2 == null) {
                return true;
            } else if (d1 == null && d2 == null) {
                return false;
            } else if (!d1.equals(d2)) {
                return true;
            }
        } else if (obj1 instanceof String) {
            String s1 = (String) obj1;
            String s2 = (String) obj2;
            if (s1 == null && s2 != null) {
                return true;
            } else if (s1 != null && s2 == null) {
                return true;
            } else if (s1 == null && s2 == null) {
                return false;
            } else if (!s1.equals(s2)) {
                return true;
            }
        } else if (obj1 instanceof Integer) {
            Integer s1 = (Integer) obj1;
            Integer s2 = (Integer) obj2;
            if (s1 == null && s2 != null) {
                return true;
            } else if (s1 != null && s2 == null) {
                return true;
            } else if (s1 == null && s2 == null) {
                return false;
            } else if (!s1.equals(s2)) {
                return true;
            }
        }
        return false;
    }
}
