package com.pixalere.integration.service;
import com.pixalere.integration.bean.HL7Response;
import com.pixalere.common.ApplicationException;
import com.pixalere.integration.bean.Patient;
import com.pixalere.integration.bean.Event;
import javax.jws.WebService;
import javax.jws.WebParam;
import ca.uhn.hl7v2.HL7Exception;
import java.io.IOException;
import javax.jws.WebResult;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService (targetNamespace = "http://service.integration.pixalere.com/")
public interface IntegrationService {
    public void savePatient(@WebParam(name="patient") Patient patient,@WebParam(name="event")  Event event) throws ApplicationException;//Meditech interface only
    public void mergePatient(@WebParam(name="patient_old") Patient patient_old, @WebParam(name="patient_new") Patient patient_new,@WebParam(name="event")  Event event) throws ApplicationException;
    @WebResult(targetNamespace="http://service.integration.pixalere.com/",
     name="encoded_message")
    public HL7Response submitMessage(@WebParam(name="username",targetNamespace="http://service.integration.pixalere.com/") String username, @WebParam(name="password",targetNamespace="http://service.integration.pixalere.com/") String password,@WebParam(name="encoded_message",targetNamespace="http://service.integration.pixalere.com/") String encoded_message) throws HL7Exception , IOException ;
}