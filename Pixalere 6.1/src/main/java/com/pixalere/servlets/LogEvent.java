/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.pixalere.servlets;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.admin.service.LogAuditServiceImpl;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.pixalere.common.ApplicationException;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import java.util.HashMap;
import javax.servlet.http.HttpSession;

/**
 * @version 6.1
 * @since 6.1
 * @author travis
 */
@WebServlet("/LogEvent")
public class LogEvent extends HttpServlet {

    public LogEvent() {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ProfessionalVO user = (ProfessionalVO)session.getAttribute("userVO");
        PatientAccountVO patient = (PatientAccountVO)session.getAttribute("patientAccount");
        try{
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        String table = (String)request.getParameter("table");
        String prod_id = (String)request.getParameter("prod_id");
        String trigger_id = (String)request.getParameter("trigger_id");
        if(user!=null && patient != null && table!=null && prod_id !=null){
        LogAuditServiceImpl lservice = new LogAuditServiceImpl();
        LogAuditVO log = new LogAuditVO();
        log.setAction("trigger.event");
        log.setCreated_on(new java.util.Date());
        log.setTable_name(table);
        log.setDescription(""+trigger_id);
        log.setRecord_id(new Integer(prod_id));
        log.setProfessionalId(user.getId());
        log.setPatientId(patient.getPatient_id());
        lservice.saveLogAudit(log);
        }
        }catch(ApplicationException e){
            //log.error(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
