/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.pixalere.servlets;

import com.pixalere.patient.bean.PatientAccountVO;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.pixalere.common.ApplicationException;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.utils.Common;
import java.util.HashMap;
import javax.servlet.http.HttpSession;

/**
 * @version 6.1
 * @since 6.1
 * @author travis
 */
@WebServlet("/PatientSheet")
public class PatientSheetCtl extends HttpServlet {

    public PatientSheetCtl() {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer language = Common.getLanguageIdFromSession(session);
        String locale = Common.getLanguageLocale(language);
        if (session.getAttribute("patient_id") != null) {
            Integer patient_id = Integer.parseInt((String) session.getAttribute("patient_id"));
            Collection<PatientAccountVO> patientList = null;
            PatientServiceImpl pservice = new PatientServiceImpl();
            Gson gson = new Gson();
            try {
                patientList = pservice.getAllPatients(patient_id);

            } catch (ApplicationException e) {
                e.printStackTrace();
            }
            List<List> rows = pservice.getPatientFlowsheet(patientList, locale);
            System.out.println("list" + rows.size());
            JsonElement element = gson.toJsonTree(rows, new TypeToken<List<List>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            response.setContentType("application/json");
            response.getWriter().print(jsonArray);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
