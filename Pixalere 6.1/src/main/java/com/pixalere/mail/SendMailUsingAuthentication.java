package com.pixalere.mail;

import java.io.ByteArrayOutputStream;
import com.pixalere.mail.ByteArrayDataSource;
import com.pixalere.utils.PDate;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import java.security.Security;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;
import com.pixalere.utils.Common;
/*
  To use this program, change values for the following three constants,
    SMTP_HOST_NAME -- Has your SMTP Host Name
    SMTP_AUTH_USER -- Has your SMTP Authentication UserName
    SMTP_AUTH_PWD  -- Has your SMTP Authentication Password
  Next change values for fields
  emailMsgTxt  -- Message Text for the Email
  emailSubjectTxt  -- Subject for email
  emailFromAddress -- Email Address whose name will appears as "from" address
  Next change value for "emailList".
  This String array has List of all Email Addresses to Email Email needs to be sent to.

  Next to run the program, execute it as follows,
  SendMailUsingAuthentication authProg = new SendMailUsingAuthentication();
*/
public class SendMailUsingAuthentication {
    private static final String SMTP_HOST_NAME = Common.getConfig("email_host");
    private static final String SMTP_AUTH_USER = Common.getConfig("email_user");
    private static final String SMTP_AUTH_PWD = Common.getConfig("email_password");
    private static final String subject = "Notification of new wound assessment entered into the Pixalere database";
    private String from = "support@pixalere.com";
    

    // Add List of Email address to who email needs to be sent to
    public SendMailUsingAuthentication() {
    }
    public void postMail(String recipients, String subject, String message, String attachment) throws MessagingException {
        boolean debug = true;
        String[] recip = recipients.split(",");
        //"The following patient, ID# " + patient + ", has had a recent wound assessment that requires your attention. Please follow this link: https://pixalere.com/vcha to view the assessment.";
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        //Set the host smtp address
        Properties props = new Properties();
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        if(Common.getConfig("from_email")!=null && !Common.getConfig("from_email").equals("") && !Common.getConfig("from_email").equals("-1")){
            from = Common.getConfig("from_email");
        }
        if(Common.getConfig("email_user")!=null && !Common.getConfig("email_user").equals("") && !Common.getConfig("email_user").equals("-1")){
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.user", SMTP_AUTH_USER);
            props.put("mail.smtp.password", SMTP_AUTH_PWD);
        }else{
            props.put("mail.smtp.auth", "false");
        }
        if(SMTP_HOST_NAME.equals("smtp.gmail.com")){
            
            props.put("mail.smtp.port", "465");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");
            System.out.println("Setting Props");
        }
        
       Session session = null;
       if(Common.getConfig("email_user")!=null && !Common.getConfig("email_user").equals("") && !Common.getConfig("email_user").equals("-1")){
           Authenticator auth = new SMTPAuthenticator();
           session = Session.getInstance(props, auth);
       }else{
           session = Session.getInstance(props,null);
       }
       session.setDebug(debug);
        // create a message
        MimeMessage msg = new MimeMessage(session);
        // set the from and to address
        InternetAddress addressFrom = new InternetAddress(from);
        msg.setFrom(addressFrom);
       for (String r : recip) {
            InternetAddress addressTo = new InternetAddress(r);
            msg.setRecipient(Message.RecipientType.TO, addressTo);

            // Setting the Subject and Content Type
            
            msg.setSubject(subject);
            msg.setText(message, "utf-8", "html");
            Transport.send(msg);
            //Email back nurse.
            
        }
    }
    
public void postMail(String[] recipients, String subject, String message,
           byte[] byteArray, String attachment_name) throws MessagingException {
       boolean debug = true;

       //Set the host smtp address
       Properties props = new Properties();
       props.put("mail.smtp.host", SMTP_HOST_NAME);
        if(Common.getConfig("email_user")!=null && !Common.getConfig("email_user").equals("") && !Common.getConfig("email_user").equals("-1")){
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.user", SMTP_AUTH_USER);
            props.put("mail.smtp.password", SMTP_AUTH_PWD);
        }else{
            props.put("mail.smtp.auth", "false");
        }
        if(Common.getConfig("from_email")!=null && !Common.getConfig("from_email").equals("") && !Common.getConfig("from_email").equals("-1")){
            from = Common.getConfig("from_email");
        }
        if(SMTP_HOST_NAME.equals("smtp.gmail.com")){
            props.put("mail.smtp.port", "465");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");
            System.out.println("Setting Props");
        }
       Session session = null;
       
       if(Common.getConfig("email_user")!=null && !Common.getConfig("email_user").equals("") && !Common.getConfig("email_user").equals("-1")){
           Authenticator auth = new SMTPAuthenticator();
           session = Session.getInstance(props, auth);
       }else{
           session = Session.getInstance(props,null);
       }
       

       session.setDebug(debug);

       // create a message
       MimeMessage msg = new MimeMessage(session);
       msg.addHeader("MIME-Version", "1.0");
       // set the from and to address
       InternetAddress[] address = new InternetAddress[recipients.length];
       for (int i = 0; i < recipients.length; i++) {
           System.out.println("recipients: "+recipients[i]);
           address[i] = new InternetAddress((String) recipients[i]);
       }
       InternetAddress addressFrom = new InternetAddress(from);
       msg.setFrom(addressFrom);
       msg.setRecipients(Message.RecipientType.TO, address);
       // Setting the Subject and Content Type
       msg.setSubject(subject);
       Date currentDate = new Date();
       msg.setSentDate(currentDate);

       msg.setText(message, "utf-8", "html");
       // Set the PDF report up as an attachment.
       DataSource[] adsAttachment = {new ByteArrayDataSource(attachment_name + "_" 
               + PDate.getDateAsStringWithFormat(currentDate, "HHmm'h'_dd-MMM-yyyy", "en") 
               + ".pdf", byteArray, "application/pdf")};

       // create and fill the first message part
       Vector vBodyParts = new Vector();
       MimeBodyPart bodyPart = new MimeBodyPart();

       // Add any file attachemnts
       for (int i = 0; i < adsAttachment.length; i++) {
           bodyPart = new MimeBodyPart();
           // attach the file to the message
           bodyPart.setDataHandler(new DataHandler(adsAttachment[i]));
           bodyPart.setDisposition(javax.mail.Part.ATTACHMENT);
           bodyPart.setFileName(adsAttachment[i].getName());
           vBodyParts.add(bodyPart);
       }


       // create the Multipart mime and attach the body parts
       Multipart mp = new MimeMultipart("related");
       for (int i = 0; i < vBodyParts.size(); i++) {
           mp.addBodyPart((MimeBodyPart) vBodyParts.elementAt(i));
       }

       // And finally add the Multipart to the message
       msg.setContent(mp);
       Transport.send(msg);


   }

   public void sendCustomMail(String recipients, String subject, String message) throws MessagingException {
       boolean debug = false;


       //Set the host smtp address
       Properties props = new Properties();
       props.put("mail.smtp.host", SMTP_HOST_NAME);
       props.put("mail.smtp.auth", "true");

       Authenticator auth = new SMTPAuthenticator();
       Session session = Session.getDefaultInstance(props, auth);

       session.setDebug(debug);

       // create a message
       Message msg = new MimeMessage(session);

       // set the from and to address
       InternetAddress addressFrom = new InternetAddress(from);
       msg.setFrom(addressFrom);

       InternetAddress addressTo = new InternetAddress(recipients);

       msg.setRecipient(Message.RecipientType.TO, addressTo);


       // Setting the Subject and Content Type
       msg.setSubject(subject);
       msg.setContent(message, "text/html");
       Transport.send(msg);
   }
    /**
     * SimpleAuthenticator is used to do simple authentication
     * when the SMTP server requires it.
     */
    private class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            String username = SMTP_AUTH_USER;
            String password = SMTP_AUTH_PWD;
            return new PasswordAuthentication(username, password);
        }
    }
}

