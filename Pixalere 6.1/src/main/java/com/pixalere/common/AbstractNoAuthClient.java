package com.pixalere.common;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.binding.soap.saaj.SAAJOutInterceptor;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import java.util.HashMap;
import java.util.Map;
import com.pixalere.utils.Constants;
import com.pixalere.admin.service.ResourcesService;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.auth.service.ClientPasswordCallback;

/**
 *
 * @author travismorris
 */
public class AbstractNoAuthClient {

    public JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

    public Object getClient() {
        //factory.getInInterceptors().add(new LoggingInInterceptor());
        //factory.getOutInterceptors().add(new LoggingOutInterceptor());
        //factory.setServiceClass(ResourcesService.class);
        //factory.setAddress("http://localhost:8080/vcha_junit/services/ResourcesService");
        Object service = (Object) factory.create();
        org.apache.cxf.endpoint.Client client = factory.getClientFactoryBean().create();
        Endpoint cxfEndpoint = client.getEndpoint();
        Map<String, Object> outProps = new HashMap<String, Object>();

        //WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        //cxfEndpoint.getOutInterceptors().add(wssOut);
        cxfEndpoint.getOutInterceptors().add(new SAAJOutInterceptor());
//The following code trusts all SSL certificates.
        HTTPConduit conduit = (HTTPConduit) client.getConduit();
        conduit.getClient().setReceiveTimeout(0);
        conduit.getClient().setConnectionTimeout(0);
        TLSClientParameters tcp = new TLSClientParameters();
        tcp.setDisableCNCheck(true);
        // Creating Trust Manager
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }};
        tcp.setTrustManagers(trustAllCerts);
        conduit.setTlsClientParameters(tcp);
        return service;
    }
}