package com.pixalere.common.client;
import com.pixalere.common.service.GUIService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class GUIClient extends AbstractClient{
    public GUIClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  GUIService createGUIClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(GUIService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"GUIService");
        GUIService service = (GUIService) getClient();       
        return service;
    }
}