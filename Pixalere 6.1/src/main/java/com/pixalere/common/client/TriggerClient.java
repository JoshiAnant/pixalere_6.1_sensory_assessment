/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.client;
import com.pixalere.common.service.TriggerService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 *
 * @author travis
 */
public class TriggerClient  extends AbstractClient{
    public TriggerClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  TriggerService createTriggerClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(TriggerService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"TriggerService");
        TriggerService service = (TriggerService) getClient();       
        return service;
    }
}
