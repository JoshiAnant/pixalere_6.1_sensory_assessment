package com.pixalere.common.client;
import com.pixalere.common.service.ConfigurationService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class ConfigurationClient extends AbstractClient{
    public ConfigurationClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  ConfigurationService createConfigurationClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(ConfigurationService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"ConfigurationService");
        ConfigurationService service = (ConfigurationService) getClient();       
        return service;
    }
}