package com.pixalere.common.client;
import com.pixalere.common.service.ProductCategoryService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class ProductCategoryClient extends AbstractClient{
    public ProductCategoryClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  ProductCategoryService createProductCategoryClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(ProductCategoryService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"ProductCategoryService");
        ProductCategoryService service = (ProductCategoryService) getClient();       
        return service;
    }
}