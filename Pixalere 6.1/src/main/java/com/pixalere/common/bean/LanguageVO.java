/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.common.ValueObject;
/**
 *     This is the language (DB table) bean
 *     Refer to {@link com.pixalere.common.service.ListServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 *     @has 1..* Has 1 com.pixalere.common.bean.LanguageVO
 */
public class LanguageVO  extends ValueObject{
    private Integer id;
    private String name;
    public LanguageVO (){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
