package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
/**
 *           wound_assessments
 *          @author Travis Morris (with help from Hibernate)
 *
*/
public class EmailVO extends ValueObject implements Serializable {
private String email;
private String name;
private String pager;
    private Integer id;
public EmailVO(){}
public EmailVO(String email, String pager, String name){
   
    this.email = email;
    this.pager=pager;
    this.name=name;
}
    public void setId(Integer id){
        this.id=id;
    }
    public Integer getId(){
        return id;
    }
public void setEmail(String email){
	this.email=email;
}
public String getEmail(){
	return email;
}
public void setPager(String pager){
	this.pager=pager;
}
public String getPager(){
	return pager;
}
public void setName(String name){
	this.name=name;
}
public String getName(){
	return name;
}
}