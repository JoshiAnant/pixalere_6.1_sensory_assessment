
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
/**
 *
 * @author travdes
 */
public class PatientVisitVO  extends ValueObject implements Serializable{
    private Integer id;
    private Integer count;
    private Integer patient_id;
    private Integer treatment_location_id;
    private LookupVO treatment_location;
    public PatientVisitVO(){}
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getCount() {
        return count;
    }
    public void setCount(Integer count) {
        this.count = count;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getTreatment_location_id() {
        return treatment_location_id;
    }
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }
    public LookupVO getTreatment_location() {
      return this.treatment_location;
    }
    public void setTreatment_location(LookupVO treatment_location) {
      this.treatment_location=treatment_location;
    }
}
