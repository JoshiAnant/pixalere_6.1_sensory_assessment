package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import java.util.Comparator;
 /**
 *     This is the ConfigurationVO  bean for the configuration  (DB table)
 *     Refer to {@link com.pixalere.common.service.ConfigurationServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class ConfigurationVO extends ValueObject implements Comparator, Serializable{
    private Integer id;
    private Integer category;
    private Integer readonly;
    private Integer orderby;
    private Integer selfadmin;
    private String config_setting;
    private String enable_component_rule;
    private String enable_options_rule;
    private String component_type;
    private String component_options;
    private String component_values;
    private String current_value;
    private String title;
    public ConfigurationVO()  {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getCategory() {
        return category;
    }
    public void setCategory(Integer category) {
        this.category = category;
    }
    public Integer getOrderby() {
        return orderby;
    }
    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }
    public Integer getSelfadmin() {
        return selfadmin;
    }
    public void setSelfadmin(Integer selfadmin) {
        this.selfadmin = selfadmin;
    }
    public String getConfig_setting() {
        return config_setting;
    }
    public void setConfig_setting(String config_setting) {
        this.config_setting = config_setting;
    }
    public String getEnable_component_rule() {
        return enable_component_rule;
    }
    public void setEnable_component_rule(String enable_component_rule) {
        this.enable_component_rule = enable_component_rule;
    }
    
    public String getEnable_options_rule() {
        return enable_options_rule;
    }
    public void setEnable_options_rule(String enable_options_rule) {
        this.enable_options_rule = enable_options_rule;
    }
    
    public String getComponent_type() {
        return component_type;
    }
    public void setComponent_type(String component_type) {
        this.component_type = component_type;
    }
    public String getComponent_options() {
        return component_options;
    }
    public void setComponent_options(String component_options) {
        this.component_options = component_options;
    }
    public String getComponent_values() {
        return component_values;
    }
    public void setComponent_values(String component_values) {
        this.component_values = component_values;
    }  
    
    public String getCurrent_value() {
        return current_value;
    }
    public void setCurrent_value(String current_value) {
        this.current_value = current_value;
    }
    /**
     * @return the readonly
     */
    public Integer getReadonly() {
        return readonly;
    }
    /**
     * @param readonly the readonly to set
     */
    public void setReadonly(Integer readonly) {
        this.readonly = readonly;
    }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
 public boolean equals(Object o)	{
        return true;
    }
    public int compare(Object o1, Object o2) {
        ConfigurationVO ref1 = (ConfigurationVO) o1;
        ConfigurationVO ref2 = (ConfigurationVO) o2;
        if(ref1.getCategory()!=null && ref2.getCategory()!=null && !ref1.getCategory().equals(ref2.getCategory())){
            return ref1.getCategory().compareTo(ref2.getCategory());
        } else{
            return ref1.getTitle().compareTo(ref2.getTitle());
        }
    }
   
}
