package com.pixalere.common.bean;
import com.pixalere.common.*;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import com.pixalere.auth.bean.PositionVO;
 /**
 *     This is the ReferralsTrackingVO  bean for the referrals_tracking  (DB table)
 *     Refer to {@link com.pixalere.common.service.ReferralsTrackingServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.patient.bean.PatientAccountVO
  *    @has 1..1 Has 1..1 com.pixalere.wound.bean.WoundVO
  *    @has 1..1 Has 1..1 com.pixalere.assessment.bean.WoundAssessmentVO
  *    @has 1..1 Has 1..1 com.pixalere.common.bean.LookupVO
 *
 */
public class ReferralsTrackingVO extends ValueObject implements Serializable {
    private Integer patient_account_id;
    private Integer patient_id;
    private PatientAccountVO patient;
    private Integer active;
    /** identifier field */
    private Integer id;
    private WoundVO wound;
    private WoundAssessmentVO wound_assessment;
    private LookupVO priorityList;
    /** nullable persistent field */
    private Integer professional_id;
    /** nullable persistent field */
    private Integer wound_id;
    //private Integer wound_profile_id;
    /** nullable persistent field */
    private Integer assessment_id;
    private Integer wound_profile_type_id;
    /** nullable persistent field */
    private Integer current;
    private Collection<AutoReferralVO> auto_referrals;
    /** nullable persistent field */
    private Date created_on;
    /** nullable persistent field */
    private Integer entry_type;
    private String type;
    private Integer assigned_to;
    /** nullable persistent field */
    private Integer top_priority;
    private String user_signature;
    //private Integer active;
    /** default constructor */
    public ReferralsTrackingVO() {
    }
        
    public void setWound(WoundVO wound){
        this.wound=wound;
    }
    public WoundVO getWound(){
        return wound;
    }
       
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getAssigned_to() {
        return this.assigned_to;
    }
    public void setAssigned_to(Integer assigned_to) {
        this.assigned_to = assigned_to;
    }
    public Integer getProfessional_id() {
        return this.professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public Integer getWound_id() {
        return this.wound_id;
    }
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    /*public Integer getWound_profile_id() {
        return this.wound_profile_id;
    }
    public void setWound_profile_id(Integer wound_profile_id) {
       
     this.wound_profile_id = wound_profile_id;
    }*/
    public Integer getAssessment_id() {
        return this.assessment_id;
    }
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    public Integer getEntry_type() {
        return this.entry_type;
    }
    public void setEntry_type(Integer entry_type) {
        this.entry_type = entry_type;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Integer getTop_priority() {
        return this.top_priority;
    }
    public void setTop_priority(Integer top_priority) {
        this.top_priority = top_priority;
    }
    public Integer getCurrent() {
        return this.current;
    }
    public void setCurrent(Integer current) {
        this.current = current;
    }
    
    public void setWoundAssessment(WoundAssessmentVO wound_assessment) {
        this.wound_assessment = wound_assessment;
    }
    public WoundAssessmentVO getWoundAssessment(){
        return wound_assessment;
    }
    public LookupVO getPriorityList() {
        return priorityList;
    }
    public void setPriorityList(LookupVO priorityList) {
        this.priorityList = priorityList;
    }
    public Integer getPatient_account_id() {
        return patient_account_id;
    }
    public void setPatient_account_id(Integer patient_account_id) {
        this.patient_account_id = patient_account_id;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public PatientAccountVO getPatient() {
        return patient;
    }
    public void setPatient(PatientAccountVO patient) {
        this.patient = patient;
    }
    public Integer getWound_profile_type_id() {
        return wound_profile_type_id;
    }
    public void setWound_profile_type_id(Integer wound_profile_type_id) {
        this.wound_profile_type_id = wound_profile_type_id;
    }
    /**
     * @return the auto_referrals
     */
    public Collection<AutoReferralVO> getAuto_referrals() {
        return auto_referrals;
    }
    /**
     * @param auto_referrals the auto_referrals to set
     */
    public void setAuto_referrals(Collection<AutoReferralVO> auto_referrals) {
        this.auto_referrals = auto_referrals;
    }
    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    /**
     * @return the user_signature
     */
    public String getUser_signature() {
        return user_signature;
    }

    /**
     * @param user_signature the user_signature to set
     */
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }





}