/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 *
 * @author travis
 */
public class TriggerLibraryVO  extends ValueObject implements Serializable {
    public TriggerLibraryVO(){}
    private Integer trigger_library_id;
    private Integer trigger_id;
    private Integer library_id;

    /**
     * @return the trigger_library_id
     */
    public Integer getTrigger_library_id() {
        return trigger_library_id;
    }

    /**
     * @param trigger_library_id the trigger_library_id to set
     */
    public void setTrigger_library_id(Integer trigger_library_id) {
        this.trigger_library_id = trigger_library_id;
    }

    /**
     * @return the trigger_id
     */
    public Integer getTrigger_id() {
        return trigger_id;
    }

    /**
     * @param trigger_id the trigger_id to set
     */
    public void setTrigger_id(Integer trigger_id) {
        this.trigger_id = trigger_id;
    }

    /**
     * @return the library_id
     */
    public Integer getLibrary_id() {
        return library_id;
    }

    /**
     * @param library_id the library_id to set
     */
    public void setLibrary_id(Integer library_id) {
        this.library_id = library_id;
    }
    
}
