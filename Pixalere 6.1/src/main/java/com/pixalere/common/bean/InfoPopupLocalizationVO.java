/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.admin.bean.ResourcesVO;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import java.util.Comparator;
import com.pixalere.utils.Common;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
/**
 *     This is the info_popup_l10n (DB table) bean
 *     Refer to {@link com.pixalere.common.service.ListServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 *     @has 1..* Has 1 com.pixalere.common.bean.InfoPopupLocalizationVO
*/
public class InfoPopupLocalizationVO  extends ValueObject{
    private Integer id;
    private String name;
    private Integer language_id;
    private Integer info_popup_id;
    private Date modified_date;
    public InfoPopupLocalizationVO (){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the language_id
     */
    public Integer getLanguage_id() {
        return language_id;
    }
    /**
     * @param language_id the language_id to set
     */
    public void setLanguage_id(Integer language_id) {
        this.language_id = language_id;
    }
 
    /**
     * @return the info_popup_id
     */
    public Integer getInfo_popup_id() {
        return info_popup_id;
    }
    /**
     * @param info_popup_id the info_popup_id to set
     */
    public void setInfo_popup_id(Integer info_popup_id) {
        this.info_popup_id = info_popup_id;
    }
    /**
     * @return the modified_date
     */
    public Date getModified_date() {
        return modified_date;
    }
    /**
     * @param modified_date the modified_date to set
     */
    public void setModified_date(Date modified_date) {
        this.modified_date = modified_date;
    }
}
