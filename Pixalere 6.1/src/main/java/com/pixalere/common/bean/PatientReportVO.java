package com.pixalere.common.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
 /**
 *     This is the PatientReportVO  bean for the patient_reports  (DB table)
 *     Refer to {@link com.pixalere.common.service.PatientReportService } for the calls
 *     which utilize this bean.
 *
 *     
 */
public class PatientReportVO extends ValueObject implements Serializable {
    /** nullable persistent field */
    private Integer patient_id;
    private Integer active;
    private Integer id;
    private String time_stamp;
    /** default constructor */
    public PatientReportVO() {
    }
    public Integer getPatient_id() {
        return this.patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public String getTime_stamp() {
        return this.time_stamp;
    }
    public void setTime_stamp(String timestamp) {
        this.time_stamp = timestamp;
    }
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
}
