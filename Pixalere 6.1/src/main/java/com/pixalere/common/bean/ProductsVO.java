/*
 * Created on Dec 13, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
import java.util.Collection;
/**
 *     This is the ProductsVO  bean for the products  (DB table)
 *     Refer to {@link com.pixalere.common.service.ProductsServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.common.bean.ProductCategoryVO
 *
 */
public class ProductsVO extends ValueObject implements Serializable {
    private String uid;
    private Integer id;
    private Integer base_id;
    private String video_url;
    private Integer category_id;
    private String title = null;
    private Double cost;
    private String description = null;
    private String company = null;
    private String url = null;
    private String image = null;
    private Integer active;
    private String type = null;
    private String quantity = null;
    private Integer library_id;
    private LibraryVO library;
    private Integer all_treatment_locations;
    private Integer display_image;
    private Integer display_description;
    private String pdf = null;
    private String recommendation_type = null;
    public static int PATIENT_CARE = 1;
    private ProductCategoryVO category = null;
    private Collection<ProductsByFacilityVO> facilities;
    public ProductsVO(Integer id, Integer base_id, Integer category_id, String title, Double cost, String description, String company, String url, String image, Integer active, String type, String quantity) {
        this.category_id = category_id;
        this.title = title;
        this.cost = cost;
        this.description = description;
        this.company = company;
        this.url = url;
        this.image = image;
        this.active = active;
        this.type = type;
        this.quantity = quantity;
        this.id = id;
        this.base_id = base_id;
    }
    public ProductsVO() {
    }
    public Integer getAll_treatment_locations() {
        return all_treatment_locations;
    }
    public void setAll_treatment_locations(Integer all_treatment_locations) {
        this.all_treatment_locations = all_treatment_locations;
    }
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getBaseId() {
        return this.base_id;
    }
    public void setBaseId(Integer base_id) {
        this.base_id = base_id;
    }
    public ProductCategoryVO getCategory() {
        return this.category;
    }
    public void setCategory(ProductCategoryVO category) {
        this.category = category;
    }
    public Integer getCategory_id() {
        return this.category_id;
    }
    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Double getCost() {
        return this.cost;
    }
    public void setCost(Double cost) {
        this.cost = cost;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getCompany() {
        return this.company;
    }
    public void setCompany(String company) {
        this.company = company;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getImage() {
        return this.image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public Integer getActive() {
        return this.active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getQuantity() {
        return this.quantity;
    }
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    /**
     * @return the treatment_location_id
     */
    public Collection<ProductsByFacilityVO> getFacilities() {
        return facilities;
    }
    /**
     * @param treatment_location_id the treatment_location_id to set
     */
    public void setFacilities(Collection<ProductsByFacilityVO> facilities) {
        this.facilities = facilities;
    }
    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }
    /**
     * @param uid the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }
    /**
     * @return the library_id
     */
    public Integer getLibrary_id() {
        return library_id;
    }
    /**
     * @param library_id the library_id to set
     */
    public void setLibrary_id(Integer library_id) {
        this.library_id = library_id;
    }
    /**
     * @return the library
     */
    public LibraryVO getLibrary() {
        return library;
    }
    /**
     * @param library the library to set
     */
    public void setLibrary(LibraryVO library) {
        this.library = library;
    }

    public Integer getDisplay_image() {
        return display_image;
    }

    public void setDisplay_image(Integer display_image) {
        this.display_image = display_image;
    }

    public Integer getDisplay_description() {
        return display_description;
    }

    public void setDisplay_description(Integer display_description) {
        this.display_description = display_description;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getRecommendation_type() {
        return recommendation_type;
    }

    public void setRecommendation_type(String recommendation_type) {
        this.recommendation_type = recommendation_type;
    }

    /**
     * @return the video_url
     */
    public String getVideo_url() {
        return video_url;
    }

    /**
     * @param video_url the video_url to set
     */
    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

}
