/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
/**
 *
 * @author travis
 */
public class LicenseVO  extends ValueObject implements Serializable {
private Integer id;
private String license;
    public LicenseVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the license
     */
    public String getLicense() {
        return license;
    }
    /**
     * @param license the license to set
     */
    public void setLicense(String license) {
        this.license = license;
    }
}
