package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
 /**
 *     This is the ComponentsVO  bean for the components  (DB table)
 *     Refer to {@link com.pixalere.common.service.GUIServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.common.bean.InformationPopupVO
 *
 */
public class ComponentsVO extends ValueObject implements Serializable {
    private Integer id;
    private Integer resource_id;
    private String field_name;
    private Integer flowchart;
    private String label_key;
    private Integer table_id;
    private Integer orderby;
    private Integer required;
    private Integer enabled_close;
    private Integer info_popup_id;
    private String field_type;
    private Integer num_columns;
    private String component_type;
    private Integer allow_edit;
    private String validation;
    private String validation_message;
    private Integer allow_required;
    private InformationPopupVO infopopup;
    private Integer ignore_info_popup;
    private Integer hide_flowchart;
    private Integer hide_gui;
    /** Creates a new instance of ComponentsVO */
    public ComponentsVO() {
        
    }
    public Integer getResource_id() {
        return resource_id;
    }
    public void setResource_id(Integer resource_id) {
        this.resource_id = resource_id;
    }
    public String getField_name() {
        return field_name;
    }
    public void setField_name(String field_name) {
        this.field_name = field_name;
    }
    public Integer getFlowchart() {
        return flowchart;
    }
    public void setFlowchart(Integer page) {
        this.flowchart = page;
    }
    public String getLabel_key() {
        return label_key;
    }
    public void setLabel_key(String label_key) {
        this.label_key = label_key;
    }
    public Integer getTable_id() {
        return table_id;
    }
    public void setTable_id(Integer section) {
        this.table_id = section;
    }
    public Integer getOrderby() {
        return orderby;
    }
    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }
  
    public Integer getRequired() {
        return required;
    }
    public void setRequired(Integer required) {
        this.required = required;
    }
    public Integer getEnabled_close() {
        return enabled_close;
    }
    public void setEnabled_close(Integer enabled_close) {
        this.enabled_close = enabled_close;
    }
 
    public Integer getInfo_popup_id() {
        return info_popup_id;
    }
    public void setInfo_popup_id(Integer info_popup_id) {
        this.info_popup_id = info_popup_id;
    }
    public String getField_type() {
        return field_type;
    }
    public void setField_type(String field_type) {
        this.field_type = field_type;
    }
    public String getComponent_type() {
        return component_type;
    }
    public void setComponent_type(String component_type) {
        this.component_type = component_type;
    }
    public Integer getAllow_edit() {
        return allow_edit;
    }
    public void setAllow_edit(Integer allow_edit) {
        this.allow_edit = allow_edit;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getValidation() {
        return validation;
    }
    public void setValidation(String validation) {
        this.validation = validation;
    }
    public InformationPopupVO getInfopopup() {
        return infopopup;
    }
    public void setInfopopup(InformationPopupVO infopopup) {
        this.infopopup = infopopup;
    }
    /**
     * @return the allow_required
     */
    public Integer getAllow_required() {
        return allow_required;
    }
    /**
     * @param allow_required the allow_required to set
     */
    public void setAllow_required(Integer allow_required) {
        this.allow_required = allow_required;
    }
    /**
     * @return the validation_message
     */
    public String getValidation_message() {
        return validation_message;
    }
    /**
     * @param validation_message the validation_message to set
     */
    public void setValidation_message(String validation_message) {
        this.validation_message = validation_message;
    }
    /**
     * @return the ignore_info_popup
     */
    public Integer getIgnore_info_popup() {
        return ignore_info_popup;
    }
    /**
     * @param ignore_info_popup the ignore_info_popup to set
     */
    public void setIgnore_info_popup(Integer ignore_info_popup) {
        this.ignore_info_popup = ignore_info_popup;
    }
    /**
     * @return the hide_flowchart
     */
    public Integer getHide_flowchart() {
        return hide_flowchart;
    }
    /**
     * @param hide_flowchart the hide_flowchart to set
     */
    public void setHide_flowchart(Integer hide_flowchart) {
        this.hide_flowchart = hide_flowchart;
    }
    /**
     * @return the hide_gui
     */
    public Integer getHide_gui() {
        return hide_gui;
    }
    /**
     * @param hide_gui the hide_gui to set
     */
    public void setHide_gui(Integer hide_gui) {
        this.hide_gui = hide_gui;
    }
    /**
     * @return the num_columns
     */
    public Integer getNum_columns() {
        return num_columns;
    }
    /**
     * @param num_columns the num_columns to set
     */
    public void setNum_columns(Integer num_columns) {
        this.num_columns = num_columns;
    }

   
}
