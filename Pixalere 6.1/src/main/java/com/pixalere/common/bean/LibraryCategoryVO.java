/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 *
 * @author travis
 */
public class LibraryCategoryVO   extends ValueObject implements Serializable {
    private Integer id;
    private String name;
    private Integer order_by;
    private Integer hide;
    public LibraryCategoryVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the order_by
     */
    public Integer getOrder_by() {
        return order_by;
    }
    /**
     * @param order_by the order_by to set
     */
    public void setOrder_by(Integer order_by) {
        this.order_by = order_by;
    }
    /**
     * @return the hide
     */
    public Integer getHide() {
        return hide;
    }
    /**
     * @param hide the hide to set
     */
    public void setHide(Integer hide) {
        this.hide = hide;
    }
    
}
