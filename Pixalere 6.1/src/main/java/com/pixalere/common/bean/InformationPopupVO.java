/*
 * InformationPopupVO.java
 *
 * Created on June 18, 2007, 7:08 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import com.pixalere.utils.Constants;
 /**
 *     This is the InformationPopupVO  bean for the information_popup  (DB table)
 *     Refer to {@link com.pixalere.common.service.GUIServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class InformationPopupVO extends ValueObject implements Serializable{
    private String title;
    private String description;
    private Integer id;
    private Integer component_id;
    //@todo byte array later for images
    /** Creates a new instance of InformationPopupVO */
    public InformationPopupVO()  {
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getComponent_id() {
        return component_id;
    }
    public void setComponent_id(Integer component_id) {
        this.component_id = component_id;
    }
    
  
    
}
