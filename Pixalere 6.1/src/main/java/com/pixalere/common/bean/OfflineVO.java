package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
 /**
 *     This is the OfflineVO  bean for the offline  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientProfileServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class OfflineVO extends ValueObject implements Serializable {
private Integer id;
private Integer patient_id;
private Integer wound_id;
private Integer assessment_id;
private Integer patientprofile_new;
private Integer woundprofile_new;
private Integer woundassessment_new;
private Integer diagnostic_changed;
private Integer wound_changed;
private Integer patientprofile_changed;
private Integer woundprofile_changed;
private Integer foot_changed;
private Integer sensory_changed;
public Integer getSensory_changed() {
	return sensory_changed;
}
public void setSensory_changed(Integer sensory_changed) {
	this.sensory_changed = sensory_changed;
}

private Integer limb_changed;
private Integer limb_adv_changed;
private Integer limb_upper_changed;
private Integer braden_changed;
private Integer exam_changed;
private String email;
private String email2;
private String wound_location;
private  Integer professional_id;
public OfflineVO(){}
public void setId(Integer id){
	this.id=id;
}
public Integer getId(){
	return id;
}
public void setPatient_id(Integer patient_id){
	this.patient_id=patient_id;
}
public Integer getPatient_id(){
	return patient_id;
}	
public void setEmail(String email){
	this.email=email;
}
public String getEmail(){
	return email;
}
public void setEmail2(String email2){
	this.email2=email2;
}
public String getEmail2(){
	return email2;
}
public void setWound_id(Integer wound_id){
	this.wound_id=wound_id;
}
public Integer getWound_id(){
	return wound_id;
}	
public void setPatientprofile_new(Integer patientprofile_new){
	this.patientprofile_new=patientprofile_new;
}
public Integer getPatientprofile_new(){
	return patientprofile_new;
}	
public void setWoundprofile_new(Integer woundprofile_new){
	this.woundprofile_new=woundprofile_new;
}
public Integer getWoundprofile_new(){
	return woundprofile_new;
}	
public void setWoundassessment_new(Integer woundassessment_new){
	this.woundassessment_new=woundassessment_new;
}
public Integer getWoundassessment_new(){
	return woundassessment_new;
}
public void setAssessment_id(Integer assessment_id){
	this.assessment_id=assessment_id;
}
public Integer getAssessment_id(){
	return assessment_id;
}	
public void setWound_location(String wound_location){
	this.wound_location=wound_location;
}
public String getWound_location(){
	return wound_location;
}
    public Integer getWound_changed() {
        return wound_changed;
    }
    public void setWound_changed(Integer wound_changed) {
        this.wound_changed = wound_changed;
    }
    public Integer getPatientprofile_changed() {
        return patientprofile_changed;
    }
    public void setPatientprofile_changed(Integer patientprofile_changed) {
        this.patientprofile_changed = patientprofile_changed;
    }
    public Integer getWoundprofile_changed() {
        return woundprofile_changed;
    }
    public void setWoundprofile_changed(Integer woundprofile_changed) {
        this.woundprofile_changed = woundprofile_changed;
    }
    public Integer getFoot_changed() {
        return foot_changed;
    }
    public void setFoot_changed(Integer foot_changed) {
        this.foot_changed = foot_changed;
    }
    public Integer getLimb_changed() {
        return limb_changed;
    }
    public void setLimb_changed(Integer limb_changed) {
        this.limb_changed = limb_changed;
    }
    public Integer getBraden_changed() {
        return braden_changed;
    }
    public void setBraden_changed(Integer braden_changed) {
        this.braden_changed = braden_changed;
    }
    /**
     * @return the exam_changed
     */
    public Integer getExam_changed() {
        return exam_changed;
    }
    /**
     * @param exam_changed the exam_changed to set
     */
    public void setExam_changed(Integer exam_changed) {
        this.exam_changed = exam_changed;
    }
    /**
     * @return the limb_adv_hanged
     */
    public Integer getLimb_adv_changed() {
        return limb_adv_changed;
    }
    /**
     * @param limb_adv_hanged the limb_adv_hanged to set
     */
    public void setLimb_adv_changed(Integer limb_adv_changed) {
        this.limb_adv_changed = limb_adv_changed;
    }
    /**
     * @return the limb_upper_changed
     */
    public Integer getLimb_upper_changed() {
        return limb_upper_changed;
    }
    /**
     * @param limb_upper_changed the limb_upper_changed to set
     */
    public void setLimb_upper_changed(Integer limb_upper_changed) {
        this.limb_upper_changed = limb_upper_changed;
    }
    /**
     * @return the diagnostic_changed
     */
    public Integer getDiagnostic_changed() {
        return diagnostic_changed;
    }
    /**
     * @param diagnostic_changed the diagnostic_changed to set
     */
    public void setDiagnostic_changed(Integer diagnostic_changed) {
        this.diagnostic_changed = diagnostic_changed;
    }

    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }

    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
 
}