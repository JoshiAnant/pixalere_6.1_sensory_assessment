package com.pixalere.common.bean;
import com.pixalere.utils.Common;
import com.pixalere.admin.bean.FacilityConfigVO;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.common.DataAccessException;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import com.pixalere.common.dao.ListsDAO;
import java.util.Collection;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * This is the listdata (DB table) bean Refer to {@link com.pixalere.common.service.ListServiceImpl
 * } for the calls which utilize this bean.
 *
 *
 * @has 1..* Has 1 com.pixalere.admin.bean.ResourcesVO
 */
public class LookupVO extends ValueObject  implements  Comparator,Serializable {

       
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LookupVO.class);
    /**
     * identifier field
     */
    private Integer id;
    private FacilityConfigVO facility_config;//@deprecated... remove 
    private static final String ENCRYPTION_KEY = "value4ky";
    private Integer locked;
    
    /*
    * CONSTANTS
    */
    public static final int VERBAL_PATIENT_CONSENT=205;
    public static final int CONF_PATIENT_CONSENT=120;
    public static final int VENDOR_NAME=201;
    public static final int MACHINE_ACQUIREMENT=202;
    public static final int PASSWORD_QUESTIONS=204;
    public static final int FUNDING_SOURCE = 1;
    public static final int REFERRAL_SOURCE = 2;
    public static final int PATIENT_RESIDENCE = 4;
    public static final int[] COMORBIDITIES = {41, 42, 43, 44, 45, 46, 47, 48, 49};
    public static final int EXTERNAL_REFERRAL = 195;
    public static final int ARRANGED = 196;
    //FIXME: this is really "interfering factors"
    public static final int INTERFERING_MEDS = 6;
    public static final int GENDER = 7;
    public static final int ETIOLOGY = 8;
    public static final int TEACHING_GOALS = 146;
    //public static final int PROGRAM_STAY= 9;
    //public static final int PROGRAM_STAY_VARIANCE= 10;
    public static final int PATIENT_CARE = 11;
    public static final int PRESSURE_ULCER_STAGE = 12;
    public static final int ALBUMIN = 13;
    public static final int PREALBUMIN = 14;
    public static final int INFECTIONS = 15;
    public static final int INVESTIGATIONS = 16;
    public static final int ADJUNCTIVE = 17;
    public static final int LIMB_COLOR = 18;
    public static final int LIMB_TEMPERATURE = 19;
    public static final int LIMB_EDEMA = 20;
    public static final int LIMB_EDEMA_SEVERITY = 21;
    public static final int DOPPLER = 22;
    public static final int PALPATION = 23;
    public static final int EXUDATE_TYPE = 24;
    public static final int EXUDATE_AMOUNT = 25;
    public static final int ODOUR = 26;
    public static final int DRESSING_CHANGE_FREQUENCY = 28;
    public static final int NURSING_VISIT_FREQUENCY = 29;
    public static final int PRIORITY = 30;
    public static final int WOUND_STATUS = 31;
    public static final int DRAIN_STATUS = 153;
    public static final int POSTOP_STATUS = 154;
    public static final int OSTOMY_STATUS = 155;
    public static final int BURN_STATUS = 156;
    public static final int WOUND_DISCHARGE_REASON = 32;
    public static final int OSTOMY_DISCHARGE_REASON = 149;
    public static final int INCISION_DISCHARGE_REASON = 150;
    public static final int DRAIN_DISCHARGE_REASON = 144;
    public static final int BURN_DISCHARGE_REASON = 162;
    public static final int WOUND_BED = 33;
    public static final int WOUND_EDGE = 34;
    public static final int WOUND_BED_PERCENT = 35;
    public static final int COLOUR = 36;
    public static final int PERIWOUND_SKIN = 37;
    public static final int CNSRESULT = 38;
    public static final int CNSROUTE = 39;
    public static final int PRESSURE_READING = 40;
    public static final int[] TREATMENT_CATEGORY = {50};
    public static final int TREATMENT_CATEGORY_INT = 50;
    public static final int[] TREATMENT_LOCATION = {51};
    public static final int TREATMENT_LOCATION_INT = 51;
    public static final int BLOOD_SUGAR_MEAL = 54;
    public static final int VISIT = 55;
    public static final int POSITION = 164;
    //FIXME: This is really "interfering meds"
    public static final int MED_COMMENTS = 165;
    public static final int INCISION_GOALS = 167;
    public static final int TUBEDRAIN_GOALS = 168;
    public static final int WOUND_ACQUIRED = 175;
    //Ostomy
    public static final int TYPE_OF_OSTOMY = 148;
    public static final int DEVICES = 166;
    public static final int STOOL_COLOUR = 61;
    public static final int PROFILE = 62;
    public static final int FLANGE_POUCH = 65;
    public static final int URINE_COLOUR = 66;
    public static final int POUCHING = 67;
    public static final int SELF_CARE_PROGRESS = 68;
    public static final int PERIOSTOMY_SKIN = 69;
    public static final int URINE_TYPE = 70;
    public static final int URINE_QUANTITY = 71;
    public static final int STOOL_CONSISTENCY = 72;
    public static final int STOOL_QUANTITY = 73;
    public static final int SKIN_APPEARANCE = 74;
    public static final int SKIN_CONTOUR = 75;
    public static final int STOMA_SHAPE = 78;
    public static final int OSTOMY_ETIOLOGY = 85;
    public static final int WOUND_GOALS = 86;
    public static final int OSTOMY_GOALS = 87;
    public static final int BURN_GOALS = 173;
    public static final int PATIENT_LIMITATIONS = 89;
    public static final int INCISION_EXUDATE = 90;
    public static final int PERI_INCISIONAL = 91;
    public static final int INCISION_STATUS = 92;
    public static final int NUTRITIONAL_STATUS = 147;
    public static final int SKIN_ETIOLOGY = 180;
    public static final int SKIN_STATUS = 181;
    public static final int SKIN_DISCHARGE_REASON = 182;
    public static final int SKIN_GOALS = 183;
    public static final int NP_CONNECTOR = 184;
    public static final int INITIATED_BY = 185;
    public static final int REASON_FOR_ENDING = 186;
    public static final int GOAL_OF_THERAPY = 187;
    public static final int PAIN = 97;
    public static final int READING_VAR = 98;
    public static final int INCISION_CLOSURE_METHODS = 99;
    public static final int INCISION_CLOSURE_STATUS = 100;
    public static final int INCISION_EXUDATE_AMOUNT = 101;
    public static final int DRAIN_SITE = 102;
    public static final int LASER = 103;
    public static final int BRADEN_SENSORY = 104;
    public static final int BRADEN_MOISTURE = 105;
    public static final int BRADEN_ACTIVITY = 106;
    public static final int BRADEN_MOBILITY = 107;
    public static final int BRADEN_NUTRITION = 108;
    public static final int BRADEN_FRICTION = 109;
    public static final int MISSING_LIMBS = 110;
    public static final int PAIN_ASSESSMENT = 111;
    public static final int SKIN_ADV_ASSESSMENT = 112;
    public static final int SENSATION = 126;
    public static final int SENSORY = 113;
    public static final int PROPRIOCEPTION = 114;
    public static final int DEFORMITIES = 115;
    public static final int SKIN_ASSESSMENT = 116;
    public static final int TOES = 117;
    public static final int WEIGHT_BEARING_STATUS = 118;
    public static final int BALANCE = 119;
    public static final int CALF_MUSCLE_PUMP_FUNCTION = 120;
    public static final int MOBILITY_AIDS = 121;
    public static final int MUSCLE_TONE = 122;
    public static final int ARCHES = 123;
    public static final int FOOT_ACTIVE = 124;
    public static final int FOOT_PASSIVE = 125;
    public static final int LAB_DONE = 127;
    public static final int TRANSCUTANEOUS_PRESSURES = 128;
    public static final int ESTIM = 129;
    public static final int MASSAGE = 130;
    public static final int ULTRASOUND = 131;
    public static final int HBOT = 132;
    public static final int FISTULA = 133;
    public static final int POSTOP_ETIOLOGY = 134;
    public static final int DRAINS_ETIOLOGY = 135;
    public static final int BURN_ETIOLOGY = 172;
    public static final int CONSTRUCTION = 136;
    public static final int OSTOMY_DRAINAGE = 137;
    public static final int MUCOCUTANEOUS_MARGIN = 138;
    public static final int PERI_FISTULA_SKIN = 139;
    public static final int INACTIVATE_REASON = 169;
    //Tubes & Drains
    public static final int DRAIN_EXUDATE = 140;
    public static final int DRAIN_EXUDATE_AMOUNT = 141;
    //public static final int DRAIN_DRAINAGE_AMOUNT_DESC=143;
    public static final int TYPE_OF_DRAIN = 93;
    public static final int PERI_DRAIN_AREA = 94;
    public static final int CHARACTERISTICS = 95;
    //public static final int DRAINAGE_ODOUR=96;
    //public static final int INCISION_CLOSURE_REASON=151;
    public static final int INCISION_POSTOP_MANAGEMENT = 152;
    public static final int CCAC_QUESTIONS = 191;
    public static final int CLINICAL_BARRIERS = 192;
    public static final int IVTHERAPY_BARRIERS = 193;
    public static final int PAIN_ADV_ASSESSMENT = 198;
    public static final int LIMB_ADV_TEMPERATURE = 199;
    //Burn
    public static final int BURN_WOUND = 157;
    public static final int BURN_EXUDATE_COLOUR = 158;
    public static final int BURN_EXUDATE_AMOUNT = 159;
    public static final int BURN_GRAFTS = 160;
    public static final int BURN_DONORS = 161;
    public static final int TIMEZONE = 170;
    public static final int MISSING_UPPER_LIMB = 188;
    public static final int UPPER_LIMB_EDEMA_LOCATION = 189;
    public static final int RANGE_OF_MOTION = 190;
    public static final int LIMB_SHAPE=200;
    public static final int BATH=250;
    
    /************************************************************/
    public static final int DIABETIC_FOOT_SKIN=201 ;
    public static final int SKIN_LEFTFOOT=251;
    public static final int SKIN_RIGHTFOOT=252;
    public static final int NAILS_LEFTFOOT=253;
    public static final int NAILS_RIGHTFOOT=254;
    public static final int DEFORMITY_LEFTFOOT=255;
    public static final int DEFORMITY_RIGHTFOOT=256;
    public static final int FOOTWEAR_LEFTFOOT=257;
    public static final int FOOTWEAR_RIGHTFOOT=258;
    public static final int TEMPCOLD_LEFTFOOT=259;
    public static final int TEMPCOLD_RIGHTFOOT=260;
    public static final int TEMPHOT_LEFTFOOT=261;
    public static final int TEMPHOT_RIGHTFOOT=262;
    public static final int RANGEOFMOTION_LEFTFOOT=263;
    public static final int RANGEOFMOTION_RIGHTFOOT=264;
    public static final int SENSATION_MONOFILAMENT_LEFTFOOT=265;
    public static final int SENSATION_MONOFILAMENT_RIGHTFOOT=266;
    public static final int SENSATION_QUESTION_LEFTFOOT=267;
    public static final int SENSATION_QUESTION_RIGHTFOOT=268;
    public static final int PEDAL_PULSE_LEFTFOOT=269;
    public static final int PEDAL_PULSE_RIGHTFOOT=270;
    public static final int DEPENDENT_RUBOR_LEFTFOOT=271;
    public static final int DEPENDENT_RUBOR_RIGHTFOOT=272;
    public static final int ERYTHEMA_LEFTFOOT=273;
    public static final int ERYTHEMA_RIGHTFOOT=274;
    
    public static final int SKIN_COMMENTS=275;
    public static final int NAILS_COMMENTS=276;
    public static final int DEFORMITY_COMMENTS=277;
    public static final int FOOTWEAR_COMMENTS=278;
    public static final int TEMP_COLD_COMMENTS=279;
    public static final int TEMP_HOT_COMMENTS=280;
    public static final int RANGEOFMOTION_COMMENTS=281;
    public static final int SENSATION_MONOFILAMENT_COMMENTS=282;
    public static final int SENSATION_QUESTION_COMMENTS=283;
    public static final int PEDAL_PULSE_COMMENTS=284;
    public static final int DEPENDENT_RUBOR_COMMENTS=285;
    public static final int  ERYTHEMA_COMMENTS=286;
    public static final int LEFTFOOT_SCORE=287;
    public static final int RIGHTFOOT_SCORE=288;
    public static final int LEFTFOOT_IMAGE_SCORE=289;
    public static final int RIGHTFOOT_IMAGE_SCORE=290;

    



    /************************************************************/

    /**
     * nullable persistent field
     */
    private Integer title;
    private Integer solobox;
    private String cpt;
    private String icd9;
    private String icd10;
    private Integer other;
    private Integer active;
    private Integer resourceId;
    private Integer categoryId;
    private Integer orderby;
    private ResourcesVO resource;
    private String update_access;
    private Integer clinic;
    private String report_value;
    private Date modified_date;
   
    /**
     * full constructor
     */
    public LookupVO(Integer resourceId, Integer active, Integer other, Integer orderby, Integer title, Integer solobox, String update_access) {
        this.other = other;
        this.active = active;

        this.resourceId = resourceId;
        this.categoryId = categoryId;
        this.orderby = orderby;
        this.title = title;
        this.solobox = solobox;
        this.update_access = update_access;
        this.report_value = report_value;
    }

    /**
     * default constructor
     */
    public LookupVO() {
    }
    public LookupVO(Integer language) {
        this.language=language;
    }
    private Collection<LookupLocalizationVO> names;

    /**
     * Get correct language
     *
     * @param language
     * @return language_value
     */
    public String getName(Integer language) {
        for (LookupLocalizationVO item : names) {
            if (language.equals(item.getLanguage_id())) {
                if (item.getName() != null && !item.getName().equals("")) {
                    return item.getName();
                }
            }
        }
        // Fallback for language country to language (i.e. es_ES --> es)
        String locale = Common.getLanguageLocale(language);
        if (locale.indexOf("_") > 0){
            String sublocale = locale.substring(0, locale.indexOf("_"));
            Integer sub_lang = Common.getLanguageIdFromLocale(sublocale);
            for (LookupLocalizationVO item : names) {
                if (sub_lang.equals(item.getLanguage_id())) {
                    if (item.getName() != null && !item.getName().equals("")) {
                        return item.getName();
                    }
                }
            }
        }
        //get English translation... since nothing was found.
        for (LookupLocalizationVO item : names) {
            if (item.getLanguage_id() == com.pixalere.utils.Constants.ENGLISH) {
                return item.getName();
            }
        }
        return "";
    }
    /**
     * Get correct language
     *
     * @param language
     * @return language_value
     */
    public String getDefinition(Integer language) {
        for (LookupLocalizationVO item : names) {
            if (language.equals(item.getLanguage_id())) {
                if (item.getDefinition() != null && !item.getDefinition().equals("")) {
                    return item.getDefinition();
                }
            }
        }
        // Fallback for language country to language (i.e. es_ES --> es)
        String locale = Common.getLanguageLocale(language);
        if (locale.indexOf("_") > 0){
            String sublocale = locale.substring(0, locale.indexOf("_"));
            Integer sub_lang = Common.getLanguageIdFromLocale(sublocale);
            for (LookupLocalizationVO item : names) {
                if (sub_lang.equals(item.getLanguage_id())) {
                    if (item.getDefinition() != null && !item.getDefinition().equals("")) {
                        return item.getDefinition();
                    }
                }
            }
        }
        //get English translation... since nothing was found.
        for (LookupLocalizationVO item : names) {
            if (item.getLanguage_id() == com.pixalere.utils.Constants.ENGLISH) {
                return item.getDefinition();
            }
        }
        return "";
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer listId) {
        this.id = listId;
    }

    public void setTitle(Integer title) {
        this.title = title;
    }

    public Integer getTitle() {
        return this.title;
    }

    public void setSoloBox(Integer solobox) {
        this.solobox = solobox;
    }

    public Integer getSoloBox() {
        return this.solobox;
    }

    public void setOther(Integer other) {
        this.other = other;
    }

    public Integer getOther() {
        return this.other;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

    public Integer getOrderby() {
        return orderby;
    }

    public Integer getResourceId() {
        return this.resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getActive() {
        return this.active;
    }

    public void setActive(Integer status) {
        this.active = status;
    }

    public void setReport_value(String report_value) {
        this.report_value = report_value;
    }

    public String getReport_value() {
        return this.report_value;
    }

    public void setResource(ResourcesVO resource) {
        this.resource = resource;
    }

    public ResourcesVO getResource() {
        return resource;
    }

    /**
     * @return the cpt
     */
    public String getCpt() {
        return cpt;
    }

    /**
     * @param cpt the cpt to set
     */
    public void setCpt(String cpt) {
        this.cpt = cpt;
    }

    /**
     * @return the icd9
     */
    public String getIcd9() {
        return icd9;
    }

    /**
     * @param icd9 the icd9 to set
     */
    public void setIcd9(String icd9) {
        this.icd9 = icd9;
    }

    /**
     * @return the icd10
     */
    public String getIcd10() {
        return icd10;
    }

    /**
     * @param icd10 the icd10 to set
     */
    public void setIcd10(String icd10) {
        this.icd10 = icd10;
    }

    /**
     * @return the locked
     */
    public Integer getLocked() {
        return locked;
    }

    /**
     * @param locked the locked to set
     */
    public void setLocked(Integer locked) {
        this.locked = locked;
    }

    public String getUpdate_access() {
        return update_access;
    }

    public void setUpdate_access(String update_access) {
        this.update_access = update_access;
    }

    /**
     * @return the facility_config
     */
    public FacilityConfigVO getFacility_config() {
        return facility_config;
    }

    /**
     * @param facility_config the facility_config to set
     */
    public void setFacility_config(FacilityConfigVO facility_config) {
        this.facility_config = facility_config;
    }

    /**
     * @return the clinic
     */
    public Integer getClinic() {
        return clinic;
    }

    /**
     * @param clinic the clinic to set
     */
    public void setClinic(Integer clinic) {
        this.clinic = clinic;
    }

    /**
     * @return the names
     */
    public Collection<LookupLocalizationVO> getNames() {
        return names;
    }

    /**
     * @param names the names to set
     */
    public void setNames(Collection<LookupLocalizationVO> names) {
        this.names = names;
    }

    public String getDate() {
        if (modified_date != null) {
            DateFormat d = new SimpleDateFormat("dd/MMMMM/yyyy hh:mm:ss");
            String value = d.format(modified_date);
            return value;
        }
        return "";
    }

    /**
     * @return the modified_date
     */
    public Date getModified_date() {
        return modified_date;
    }

    /**
     * @param modified_date the modified_date to set
     */
    public void setModified_date(Date modified_date) {
        this.modified_date = modified_date;
    }
    public boolean equals(Object o)	{
        return true;
    }
    private Integer language = 1;
    
    //override the orderby,sort by alphabet
    public int compare(Object o1, Object o2) {
        LookupVO ref1 = (LookupVO) o1;
        LookupVO ref2 = (LookupVO) o2;
        
        if(ref1.getName(language)!=null && ref2.getName(language)!=null && !ref1.getName(language).equals(ref2.getName(language))){
            return ref1.getName(language).compareTo(ref2.getName(language));
        } else{
            return ref1.getOrderby().compareTo(ref2.getOrderby());
        }
    }

}
