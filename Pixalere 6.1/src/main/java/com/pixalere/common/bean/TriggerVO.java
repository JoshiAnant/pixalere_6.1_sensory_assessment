/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Collection;
/**
 *
 * @author travis
 */
public class TriggerVO  extends ValueObject implements Serializable {
    public TriggerVO(){}
    private Integer active;
    private Integer trigger_id;
    private String name;
    private String description;
    private String wound_type;
    private String trigger_type;
    private String logic_operator;
    private Collection<TriggerProductsVO> product_ids;
    private Collection<TriggerCriteriaVO> options;
    /**
     * @return the trigger_id
     */
    public Integer getTrigger_id() {
        return trigger_id;
    }

    /**
     * @param trigger_id the trigger_id to set
     */
    public void setTrigger_id(Integer trigger_id) {
        this.trigger_id = trigger_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the wound_type
     */
    public String getWound_type() {
        return wound_type;
    }

    /**
     * @param wound_type the wound_type to set
     */
    public void setWound_type(String wound_type) {
        this.wound_type = wound_type;
    }

    /**
     * @return the trigger_type
     */
    public String getTrigger_type() {
        return trigger_type;
    }

    /**
     * @param trigger_type the trigger_type to set
     */
    public void setTrigger_type(String trigger_type) {
        this.trigger_type = trigger_type;
    }

    /**
     * @return the options
     */
    public Collection<TriggerCriteriaVO> getOptions() {
        return options;
    }

    /**
     * @param options the options to set
     */
    public void setOptions(Collection<TriggerCriteriaVO> options) {
        this.options = options;
    }

  

    /**
     * @return the product_ids
     */
    public Collection<TriggerProductsVO> getProduct_ids() {
        return product_ids;
    }

    /**
     * @param product_ids the product_ids to set
     */
    public void setProduct_ids(Collection<TriggerProductsVO> product_ids) {
        this.product_ids = product_ids;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    public String getLogic_operator() {
        return logic_operator;
    }

    public void setLogic_operator(String logic_operator) {
        this.logic_operator = logic_operator;
    }
}
