/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
 /**
 *     This is the QuickTipsVO  bean for the quick_tips  (DB table)
 *     Refer to {@link com.pixalere.common.service.GUIServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.common.bean.QuickTipsVO
 *
 */
public class QuickTipsVO extends ValueObject implements Serializable {
    private Integer tips_id;
    private String summary;
    private String body;
    private String type;
    private String page;
    public QuickTipsVO(){}

    /**
     * @return the tips_id
     */
    public Integer getTips_id() {
        return tips_id;
    }

    /**
     * @param tips_id the tips_id to set
     */
    public void setTips_id(Integer tips_id) {
        this.tips_id = tips_id;
    }

    /**
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * @param summary the summary to set
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page) {
        this.page = page;
    }
    
}
