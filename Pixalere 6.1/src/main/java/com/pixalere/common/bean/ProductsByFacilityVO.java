/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
import java.util.Vector;
import java.util.Collection;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.utils.Serialize;
/**
 *     This is the ProductsVO  bean for the products  (DB table)
 *     Refer to {@link com.pixalere.common.service.ProductsServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.common.bean.ProductCategoryVO
 *
 */
public class ProductsByFacilityVO extends ValueObject implements Serializable {
    private Integer id;
    private Integer product_id;
    private Integer treatment_location_id;
    public ProductsByFacilityVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the product_id
     */
    public Integer getProduct_id() {
        return product_id;
    }
    /**
     * @param product_id the product_id to set
     */
    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }
    /**
     * @return the treatment_location_id
     */
    public Integer getTreatment_location_id() {
        return treatment_location_id;
    }
    /**
     * @param treatment_location_id the treatment_location_id to set
     */
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }
}
