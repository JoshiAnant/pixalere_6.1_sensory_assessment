/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 *
 * @author travis
 */
public class TriggerProductsVO  extends ValueObject implements Serializable {
    public TriggerProductsVO(){}
    private Integer trigger_id;
    private Integer product_id;
    private Integer trigger_product_id;

    /**
     * @return the trigger_id
     */
    public Integer getTrigger_id() {
        return trigger_id;
    }

    /**
     * @param trigger_id the trigger_id to set
     */
    public void setTrigger_id(Integer trigger_id) {
        this.trigger_id = trigger_id;
    }

    /**
     * @return the product_id
     */
    public Integer getProduct_id() {
        return product_id;
    }

    /**
     * @param product_id the product_id to set
     */
    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    /**
     * @return the trigger_product_id
     */
    public Integer getTrigger_product_id() {
        return trigger_product_id;
    }

    /**
     * @param trigger_product_id the trigger_product_id to set
     */
    public void setTrigger_product_id(Integer trigger_product_id) {
        this.trigger_product_id = trigger_product_id;
    }
    
}
