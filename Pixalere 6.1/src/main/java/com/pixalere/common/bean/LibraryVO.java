
package com.pixalere.common.bean;
import com.pixalere.common.*;
import javax.xml.bind.annotation.XmlMimeType;
import javax.activation.*;
import java.io.*;
import java.io.Serializable;
 /**
 *     This is the LibraryVO  bean for the library  (DB table)
 *     Refer to {@link com.pixalere.common.service.LibraryServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class LibraryVO  extends ValueObject implements Serializable {
    private Integer id;
    private Integer orderby;
    private String name;
    private String pdf;
    private String url;
    private String media_type;
    private Integer category_id;
    private LibraryCategoryVO category;
    public void setCategory_id(Integer category_id){
        this.category_id=category_id;
    }
    public  Integer getCategory_id(){
        return category_id;
    }
    public void setId(Integer id){
        this.id=id;
    }
    public Integer getId(){
        return id;
    }
    public void setPdf(String pdf){
        this.pdf=pdf;
    }
    public String getPdf(){
        return pdf;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setOrderby(Integer orderby){
        this.orderby=orderby;
    }
    public Integer getOrderby(){
        return orderby;
    }
    private DataHandler pdfFile;
    @XmlMimeType("application/octet-stream")
    public DataHandler getPdfFile(){
        return pdfFile;
    }
    public void setPdfFile(DataHandler pdfFile){
        this.pdfFile=pdfFile;
    }
    /**
     * @return the category
     */
    public LibraryCategoryVO getCategory() {
        return category;
    }
    /**
     * @param category the category to set
     */
    public void setCategory(LibraryCategoryVO category) {
        this.category = category;
    }
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }
    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * @return the media_type
     */
    public String getMedia_type() {
        return media_type;
    }
    /**
     * @param media_type the media_type to set
     */
    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }
}
