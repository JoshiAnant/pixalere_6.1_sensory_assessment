/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 *
 * @author travis
 */
public class TriggerCriteriaVO  extends ValueObject implements Serializable {
    public TriggerCriteriaVO(){}
    private Integer trigger_criteria_id;
    private Integer trigger_id;
    private String match_type;
    private String criteria_type;
    private String criteria;
    private Integer component_id;
    private String description;
    /**
     * @return the trigger_criteria_id
     */
    public Integer getTrigger_criteria_id() {
        return trigger_criteria_id;
    }

    /**
     * @param trigger_criteria_id the trigger_criteria_id to set
     */
    public void setTrigger_criteria_id(Integer trigger_criteria_id) {
        this.trigger_criteria_id = trigger_criteria_id;
    }

    /**
     * @return the trigger_id
     */
    public Integer getTrigger_id() {
        return trigger_id;
    }

    /**
     * @param trigger_id the trigger_id to set
     */
    public void setTrigger_id(Integer trigger_id) {
        this.trigger_id = trigger_id;
    }

    /**
     * @return the match_type
     */
    public String getMatch_type() {
        return match_type;
    }

    /**
     * @param match_type the match_type to set
     */
    public void setMatch_type(String match_type) {
        this.match_type = match_type;
    }

    /**
     * @return the criteria_type
     */
    public String getCriteria_type() {
        return criteria_type;
    }

    /**
     * @param criteria_type the criteria_type to set
     */
    public void setCriteria_type(String criteria_type) {
        this.criteria_type = criteria_type;
    }

    /**
     * @return the criteria
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    /**
     * @return the component_id
     */
    public Integer getComponent_id() {
        return component_id;
    }

    /**
     * @param component_id the component_id to set
     */
    public void setComponent_id(Integer component_id) {
        this.component_id = component_id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
