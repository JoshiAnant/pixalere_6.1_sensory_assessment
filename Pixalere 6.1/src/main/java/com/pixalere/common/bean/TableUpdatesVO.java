/*
 * Created on Dec 13, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
 /**
 *     This is the TableUpdates  bean for the table_updates  (DB table)
 *     Refer to {@link com.pixalere.common.service.GUIServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class TableUpdatesVO   extends ValueObject implements Serializable {
	
	private Integer id;
	private Integer components;
        private Integer configuration;
        private Integer information_popup;
        private Integer listdata;
        private Integer location_images;
        private Integer products;
        private Integer products_categories;
        private Integer resources;
        private Integer blue_model_images;
        private Integer pdf;
        private Integer vendors;
	
	public TableUpdatesVO(){}
        public Integer getPdf(){
            return pdf;
        }
        public void setPdf(Integer pdf){
            this.pdf=pdf;
        }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getComponents() {
        return components;
    }
    public void setComponents(Integer components) {
        this.components = components;
    }
    public Integer getConfiguration() {
        return configuration;
    }
    public void setConfiguration(Integer configuration) {
        this.configuration = configuration;
    }
    public Integer getInformation_popup() {
        return information_popup;
    }
    public void setInformation_popup(Integer information_popup) {
        this.information_popup = information_popup;
    }
    public Integer getListdata() {
        return listdata;
    }
    public void setListdata(Integer listdata) {
        this.listdata = listdata;
    }
    public Integer getLocation_images() {
        return location_images;
    }
    public void setLocation_images(Integer location_images) {
        this.location_images = location_images;
    }
    public Integer getProducts() {
        return products;
    }
    public void setProducts(Integer products) {
        this.products = products;
    }
    public Integer getProducts_categories() {
        return products_categories;
    }
    public void setProducts_categories(Integer products_categories) {
        this.products_categories = products_categories;
    }
    public Integer getResources() {
        return resources;
    }
    public void setResources(Integer resources) {
        this.resources = resources;
    }
    public Integer getBlue_model_images() {
        return blue_model_images;
    }
    public void setBlue_model_images(Integer blue_model_images) {
        this.blue_model_images = blue_model_images;
    }

    /**
     * @return the vendors
     */
    public Integer getVendors() {
        return vendors;
    }

    /**
     * @param vendors the vendors to set
     */
    public void setVendors(Integer vendors) {
        this.vendors = vendors;
    }
	
    
	
}
