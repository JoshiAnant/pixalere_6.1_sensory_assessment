/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
 /**
 *     This is the AutoReferralsVO  bean for the auto_referrals  (DB table)
 *     Refer to {@link com.pixalere.common.service.ReferralsTrackingServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class AutoReferralVO extends ValueObject implements Serializable {
    private Integer id;
    private Integer initial_within_3weeks;
    private Integer referral_id;
    private String auto_referral_type;
    private Integer alpha_id;
    /** default constructor */
    public AutoReferralVO() {
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
   
    /**
     * @return the referral_id
     */
    public Integer getReferral_id() {
        return referral_id;
    }
    /**
     * @param referral_id the referral_id to set
     */
    public void setReferral_id(Integer referral_id) {
        this.referral_id = referral_id;
    }
    /**
     * @return the auto_referral_type
     */
    public String getAuto_referral_type() {
        return auto_referral_type;
    }
    /**
     * @param auto_referral_type the auto_referral_type to set
     */
    public void setAuto_referral_type(String auto_referral_type) {
        this.auto_referral_type = auto_referral_type;
    }
    /**
     * @return the alpha_id
     */
    public Integer getAlpha_id() {
        return alpha_id;
    }
    /**
     * @param alpha_id the alpha_id to set
     */
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    /**
     * @return the initial_within_3weeks
     */
    public Integer getInitial_within_3weeks() {
        return initial_within_3weeks;
    }
    /**
     * @param initial_within_3weeks the initial_within_3weeks to set
     */
    public void setInitial_within_3weeks(Integer initial_within_3weeks) {
        this.initial_within_3weeks = initial_within_3weeks;
    }
}