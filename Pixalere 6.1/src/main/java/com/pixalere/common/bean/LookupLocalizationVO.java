/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.bean;
import com.pixalere.admin.bean.ResourcesVO;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import java.util.Comparator;
import com.pixalere.utils.Common;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *     This is the listdata (DB table) bean
 *     Refer to {@link com.pixalere.common.service.ListServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 *     @has 1..* Has 1 com.pixalere.common.bean.LookupVO
*/
public class LookupLocalizationVO  extends ValueObject{
    private Integer id;
    private String name;
    private Integer language_id;
    private Integer lookup_id;
    private Integer resource_id;
    private String definition;
    public LookupLocalizationVO (){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the language_id
     */
    public Integer getLanguage_id() {
        return language_id;
    }
    /**
     * @param language_id the language_id to set
     */
    public void setLanguage_id(Integer language_id) {
        this.language_id = language_id;
    }
    /**
     * @return the lookup_id
     */
    public Integer getLookup_id() {
        return lookup_id;
    }
    /**
     * @param lookup_id the lookup_id to set
     */
    public void setLookup_id(Integer lookup_id) {
        this.lookup_id = lookup_id;
    }
    /**
     * @return the resource_id
     */
    public Integer getResource_id() {
        return resource_id;
    }
    /**
     * @param resource_id the resource_id to set
     */
    public void setResource_id(Integer resource_id) {
        this.resource_id = resource_id;
    }

    /**
     * @return the definition
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * @param definition the definition to set
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
