/*
 * Created on Dec 13, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pixalere.common.bean;
import com.pixalere.common.*;
import java.io.Serializable;
 /**
 *     This is the ProductCategoryVO  bean for the product_categories  (DB table)
 *     Refer to {@link com.pixalere.common.service.GUIServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class ProductCategoryVO   extends ValueObject implements Serializable {
	
	private Integer id;
	private Integer active;
	private String title=null;
	private Integer itemsCount=0;
	public ProductCategoryVO(Integer active,Integer itemsCount, String title){
		this.active=active;
		this.title=title;
                this.itemsCount=itemsCount;
		
    	}
	public ProductCategoryVO(){}
	
	public Integer getId() {
        	return this.id;
    	}
    	public void setId(Integer id) {
        	this.id = id;
    	}
    	public Integer getActive() {
        	return this.active;
    	}
    	public void setActive(Integer active) {
       		 this.active = active;
    	}
	public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Integer getItemsCount() {
        return itemsCount;
    }
    public void setItemsCount(Integer itemsCount) {
        this.itemsCount = itemsCount;
    }
	
}
