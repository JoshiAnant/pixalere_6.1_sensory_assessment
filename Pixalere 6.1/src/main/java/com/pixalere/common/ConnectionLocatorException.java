/*
 * Created on Dec 12, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pixalere.common;
/**
 * @author travdes
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConnectionLocatorException extends ApplicationException {
	Throwable exceptionCause = null;
	public ConnectionLocatorException (String msg){
		super(msg);
	}
	public ConnectionLocatorException(String msg, Throwable exception){
		super(msg,exception);
		exceptionCause=exception;
	}
	public void printStackTrace(){
		if(exceptionCause!=null){
			System.err.println("An exception has been caused by: "+exceptionCause.toString());
		}
	}
}
