/*
 * ConfigurationListVO.java
 *
 */
package com.pixalere.common;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import java.util.Comparator;
/**
 *
 * @author travismorris
 */
public class ConfigurationListVO extends ValueObject implements Serializable {
    private Integer id;
    private String component_disabled;
    private Integer readonly;
    private Integer selfadmin;
    private String config_setting;
    private String component_type;
    private String[] component_options;
    private String current_value;
    private Integer category;
    public ConfigurationListVO() {
    }
    /**
     * @return the readonly
     */
    public Integer getReadonly() {
        return readonly;
    }
    /**
     * @param readonly the readonly to set
     */
    public void setReadonly(Integer readonly) {
        this.readonly = readonly;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getComponent_disabled() {
        return component_disabled;
    }
    public void setComponent_disabled(String component_disabled) {
        this.component_disabled = component_disabled;
    }
    public Integer getSelfadmin() {
        return selfadmin;
    }
    public void setSelfadmin(Integer selfadmin) {
        this.selfadmin = selfadmin;
    }
    public String getConfig_setting() {
        return config_setting;
    }
    public void setConfig_setting(String config_setting) {
        this.config_setting = config_setting;
    }
    public String getComponent_type() {
        return component_type;
    }
    public void setComponent_type(String component_type) {
        this.component_type = component_type;
    }
    public String[] getComponent_options() {
        return component_options;
    }
    public void setComponent_options(String[] component_options) {
        this.component_options = component_options;
    }
    public String getCurrent_value() {
        return current_value;
    }
    public void setCurrent_value(String current_value) {
        this.current_value = current_value;
    }
    /**
     * @return the category
     */
    public Integer getCategory() {
        return category;
    }
    /**
     * @param category the category to set
     */
    public void setCategory(Integer category) {
        this.category = category;
    }
    private String title;
    private String description;
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
