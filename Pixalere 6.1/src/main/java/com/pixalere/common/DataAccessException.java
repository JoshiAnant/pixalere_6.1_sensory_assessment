package com.pixalere.common;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class DataAccessException extends Exception {
    Throwable exceptionCause = null;
    /** Creates a new instance of DataAccessException */
    public DataAccessException(String exceptionMsg) {
        super(exceptionMsg);
    }
    public DataAccessException(String exceptionMsg, Throwable exception){
       super(exceptionMsg);
       exceptionCause = exception;
    }
    public void printStackTrace(){
        if (exceptionCause!=null){
           System.err.println("An exception has been caused by: ");
           exceptionCause.printStackTrace();
        }
    }
}
