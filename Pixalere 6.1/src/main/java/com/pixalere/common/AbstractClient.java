/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.binding.soap.saaj.SAAJOutInterceptor;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import java.util.HashMap;
import java.util.Map;
import com.pixalere.utils.Constants;
import com.pixalere.admin.service.ResourcesService;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.auth.service.ClientPasswordCallback;
/**
 * Client proxy for JAX-WS.
 * 
 * @author travis morris
 * @since 4.2
 */
public class AbstractClient {
    public JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
    /**
     * Returns the client proxy we'll use for remote invocation.
     * @return factory JaxWS Proxy
     */
    public Object getClient() {
        //creating loggers for both request/responses.
        factory.getInInterceptors().add(new LoggingInInterceptor());
        factory.getOutInterceptors().add(new LoggingOutInterceptor());
        Object service = (Object) factory.create();
        //setup user/password for authenticating the services
        Map<String, Object> outProps = new HashMap<String, Object>();
        outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
        // Specify our username
        outProps.put(WSHandlerConstants.USER, "pix");//user_id+"");
        // Password type : plain text
        outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
        outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, ClientPasswordCallback.class.getName());
        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        //WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps);
        Client client = ClientProxy.getClient(service);
        client.getOutInterceptors().add(wssOut);
        //client.getInInterceptors().add(wssIn);
        client.getOutInterceptors().add(new SAAJOutInterceptor());
        //The following code trusts all SSL certificates.   Given that we don't
        //know if our client will use a trusted or self-signed certificate
        //we'll just accept them all.
        HTTPConduit conduit = (HTTPConduit) client.getConduit();
        //waiting indefinitely, will default to Tomcat Timeout.
        conduit.getClient().setReceiveTimeout(0);
        conduit.getClient().setConnectionTimeout(0);
        TLSClientParameters tcp = new TLSClientParameters();
        tcp.setDisableCNCheck(true);
        // Creating Trust Manager, trusts SSL certificats.
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }
        public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
        }
        public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
        }
    }};
        tcp.setTrustManagers(trustAllCerts);
        conduit.setTlsClientParameters(tcp);
        return service;
    }
}
