/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.dao;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.LicenseVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.bean.TableUpdatesVO;
import java.util.Collection;
import com.pixalere.utils.Constants;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.log4j.Logger;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  <<table>>    table.
 *
 *  @todo Need to implement this class as a singleton.
 *  @author travis morris
 *  @since 5.0
 */
public class LicenseDAO implements DataAccessObject{
    static private Logger log = Logger.getLogger(LicenseDAO.class);
    public LicenseDAO(){
    }
    /**
     * Deletes <<table>> records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 5.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.delete()***********************");
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            LicenseVO lib = (LicenseVO) deleteRecord;
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(lib);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ConfigurationVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.delete()***********************");
    }
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param license fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @deprecated
     * @since 5.0
     */
    public Collection findAllByCriteria(LicenseVO lib) throws DataAccessException{
        log.info("***********************Entering configurationDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(lib);
            query.addOrderByAscending("name");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param license  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 5.0
     */
    public LicenseVO findByCriteria(LicenseVO lib) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        LicenseVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LicenseVO();
            Query query = new QueryByCriteria(lib);
            returnVO = (LicenseVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findByCriteria()***********************");
        return returnVO;
    }
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.insert()***********************");
        LicenseVO lib = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            lib = (LicenseVO) insertRecord;
            broker.beginTransaction();
            broker.store(lib);
            broker.commitTransaction();
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ConfigurationDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.insert()***********************");
    }
public void update(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.insert()***********************");
        LicenseVO lib = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            lib = (LicenseVO) insertRecord;
            broker.beginTransaction();
            broker.store(lib);
            broker.commitTransaction();
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ConfigurationDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.insert()***********************");
    }
public LicenseVO findByID() throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        LicenseVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LicenseVO();
            LicenseVO lib = new LicenseVO();
            lib.setId(1);
            Query query = new QueryByCriteria(lib);
            returnVO = (LicenseVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findByCriteria()***********************");
        return returnVO;
    }
    public ValueObject findByPK(int dead){
        return null;
    }
    
}
