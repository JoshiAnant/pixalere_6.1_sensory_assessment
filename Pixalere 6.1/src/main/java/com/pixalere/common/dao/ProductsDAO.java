/*
 * Created on Dec 13, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pixalere.common.dao;

import com.pixalere.utils.Constants;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ProductsCategoriesLocalizationVO;
import com.pixalere.common.bean.ProductsLocalizationVO;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.service.ProductCategoryServiceImpl;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import java.util.Iterator;
import java.util.Vector;
import java.util.Collection;
import com.pixalere.common.bean.ProductsByFacilityVO;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.log4j.Logger;

/**
 * This class is responsible for handling all CRUD logic associated with the
 * products table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class ProductsDAO implements DataAccessObject {

    static private Logger log = Logger.getLogger(ProductsDAO.class);

    public ProductsDAO() {

    }

    /**
     * Deletes products records, as specified by the value object passed in.
     * Fields in Value Object which are not null will be used as parameters in
     * the SQL statement. Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should
     * be deleted
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ProductsDAO.delete()***********************");
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ProductsVO productsVO = (ProductsVO) deleteRecord;

            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(productsVO);
            broker.commitTransaction();

        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ProductsDAO.delete(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in ProductsVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ProductsVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.delete()***********************");
    }

    /**
     * Deletes products_by_facility records, as specified by the value object
     * passed in. Fields in Value Object which are not null will be used as
     * parameters in the SQL statement. Retrieves all records by criteria, then
     * deletes them.
     *
     * @param deleteRecord the value object which specifies which records should
     * be deleted
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void deleteProductsByFacility(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ProductsDAO.delete()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ProductsByFacilityVO productsVO = (ProductsByFacilityVO) deleteRecord;
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(productsVO);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ProductsDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ProductsVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ProductsVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.delete()***********************");
    }

    /**
     * Finds a single products record, as specified by the primary key value
     * passed in. If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the products table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        log.info("***********************Entering ProductsDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        ProductsVO productsVO = new ProductsVO();

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO.setId(new Integer(primaryKey));

            Query query = new QueryByCriteria(productsVO);
            log.debug("ProductsDAO.findByPK(): " + query.toString() + " :::: Value=" + primaryKey);

            productsVO = (ProductsVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByPK()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ProductsDAO.findByPK()***********************");
        return productsVO;
    }

    /**
     * Finds a all products records, as specified by the value object passed in,
     * as well as which patient_care it belongs to. If no record is found a null
     * value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     * @todo this should be refactored out, use default findAllByCriteria
     * @since 3.0
     * @deprecated
     */
    public Collection findAllByPatientCare(int patient_care) throws DataAccessException {
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ProductsVO> col = null;
        ProductsVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            Criteria crit = new Criteria();

            ListServiceImpl listbd = new ListServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            LookupVO lookupVO = listbd.getListItem(patient_care);

            crit.addEqualTo("active", new Integer(1));
            QueryByCriteria query = new QueryByCriteria(ProductsVO.class, crit);
            query.addOrderByAscending("title");

            col = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } catch (Exception e) {
            log.error("ProductsDAO.findByCriteria(): " + e.getMessage());
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return col;
    }

    /**
     * Finds a all products records, as specified by the value object passed in.
     * Only retrieve products associated with regions in this professional
     * accounts region list, or if product is falled with
     * all_treatment_locations. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @param prof logged in Professional, grab products based on facility.
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(ValueObject vo, ProfessionalVO prof) throws DataAccessException {
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ProductsVO> results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ProductsVO productsVO = (ProductsVO) vo;
            Criteria crit = new Criteria();
            if (productsVO.getActive() != null) {
                crit.addEqualTo("active", productsVO.getActive());
            } else {
                //crit.addEqualTo("active",1); //Offline needs to get everything.
            }
            
            if (productsVO.getCategory_id() != null) {
                crit.addEqualTo("category_id", productsVO.getCategory_id());
            }

            if (prof != null && prof.getRegions() != null) {
                Criteria or = new Criteria();
                for (UserAccountRegionsVO regions : prof.getRegions()) {
                    Criteria tmp = new Criteria();
                    tmp.addEqualTo("facilities.treatment_location_id", regions.getTreatment_location_id());
                    or.addOrCriteria(tmp);
                }
                Criteria all = new Criteria();
                //if no user, get all products

                Criteria tmp = new Criteria();
                tmp.addEqualTo("all_treatment_locations", 1);
                all.addOrCriteria(tmp);

                all.addOrCriteria(or);
                crit.addAndCriteria(all);
            }

            QueryByCriteria query = new QueryByCriteria(ProductsVO.class, crit, true);

            query.addOrderByAscending("title");
            //ProductCategoryServiceImpl prodCateBD=new ProductCategoryServiceImpl();

            results = (Collection) broker.getCollectionByQuery(query);
            /*for(ProductsVO prodsVO : col){
             //ProductCategoryVO prodCateVO = (ProductCategoryVO)prodCateBD.getProductCategory(prodsVO.getCategory_id().intValue());
             prodsVO.setCategory(prodCateVO);
             results.add(prodsVO);
             }*/

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("ProductsDAO.findByCriteria(): " + e.getMessage());
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }

    /**
<<<<<<< local
     * Finds a all products records with a URL attached.  If no record is found a null value is returned.
 
     * @since 6.0
     */
    public Collection findAllWithURL() throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ProductsVO> results=null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
     
            Criteria crit=new Criteria();
            crit.addEqualTo("active",1);
            crit.addNotEqualTo("url","");
            QueryByCriteria query = new QueryByCriteria(ProductsVO.class,crit);
            query.addOrderByAscending("title");

            results = (Collection) broker.getCollectionByQuery(query);
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()",e);
        } catch(Exception e){
            log.error("ProductsDAO.findByCriteria(): "+e.getMessage());
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all products+by_facility records, as specified by the value object
     * passed in. Only retrieve products associated with regions in this
=======
     * Finds a all products+by_facility records, as specified by the value
     * object passed in. Only retrieve products associated with regions in this
>>>>>>> other
     * professional accounts region list, or if product is falled with
     * all_treatment_locations. If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @param prof logged in Professional, grab products based on facility.
     *
     * @since 3.0
     */
    public Collection findAllByFacilityByCriteria(ValueObject vo) throws DataAccessException {
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ProductsByFacilityVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ProductsByFacilityVO productsVO = (ProductsByFacilityVO) vo;
            Criteria crit = new Criteria();
            crit.addEqualTo("product_id", productsVO.getProduct_id());
            /*Criteria or = new Criteria();
             for(UserAccountRegionsVO regions : prof.getRegions()){
             Criteria tmp = new Criteria();
             tmp.addEqualTo("products_by_facility.treatment_location_id",regions.getId());
             or.addOrCriteria(tmp);
             }
             crit.addAndCriteria(or);*/
            QueryByCriteria query = new QueryByCriteria(ProductsByFacilityVO.class, crit);

            //ProductCategoryServiceImpl prodCateBD=new ProductCategoryServiceImpl();
            results = (Collection) broker.getCollectionByQuery(query);
            /*for(ProductsVO prodsVO : col){
             //ProductCategoryVO prodCateVO = (ProductCategoryVO)prodCateBD.getProductCategory(prodsVO.getCategory_id().intValue());
             prodsVO.setCategory(prodCateVO);
             results.add(prodsVO);
             }*/
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } catch (Exception e) {
            log.error("ProductsDAO.findByCriteria(): " + e.getMessage());
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds an products record, as specified by the value object passed in. If
     * no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(ProductsVO productsVO) throws DataAccessException {
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ProductsVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ProductsVO();

            Query query = new QueryByCriteria(productsVO);
            returnVO = (ProductsVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Inserts a single productsVO object into the products table. The value
     * object passed in has to be of type productsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * products record.
     *
     * @since 3.0
     */
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        ProductsVO productsVO = null;
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO = (ProductsVO) insertRecord;

            broker.beginTransaction();
            broker.store(productsVO);
            broker.commitTransaction();

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ProductsDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();

            log.error("PersistenceBrokerException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ProductsDAO.insert()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ProductsDAO.insert()***********************");
    }

    /**
     * Inserts a single productsByFacilityVO object into the
     * products_by_facility table. The value object passed in has to be of type
     * productsByFacilityVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * products record.
     *
     * @since 5.0
     */
    public void insertProductsByFacility(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        ProductsByFacilityVO productsVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO = (ProductsByFacilityVO) insertRecord;
            broker.beginTransaction();
            broker.store(productsVO);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ProductsDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ProductsDAO.insert()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.insert()***********************");
    }

    /**
     * Inserts a single productsVO object into the products table. The value
     * object passed in has to be of type productsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * products record.
     *
     * @since 3.0
     */
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ProductsDAO.update()***********************");
        ProductsVO productsVO = null;
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO = (ProductsVO) updateRecord;

            broker.beginTransaction();
            broker.store(productsVO);
            broker.commitTransaction();

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ProductsDAO.update()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ProductsDAO.update(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in ProductsDAO.update()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Entering ProductsDAO.update()***********************");
    }

    /**
     * Finds an products_l10n record, as specified by the value object passed
     * in. If no record is found a null value is returned.
     *
     * @param products_l10n fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     *
     * @since 5.2
     */
    public ValueObject findProductLocalizationByCriteria(ProductsLocalizationVO productsVO) throws DataAccessException {
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ProductsLocalizationVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ProductsLocalizationVO();
            Query query = new QueryByCriteria(productsVO);
            returnVO = (ProductsLocalizationVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Inserts a single ProductsLocalizationVO object into the products_l10n
     * table. The value object passed in has to be of type productsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * products record.
     *
     * @since 5.2
     */
    public void insertProductsLocalization(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        ProductsLocalizationVO productsVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO = (ProductsLocalizationVO) insertRecord;
            broker.beginTransaction();
            broker.store(productsVO);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ProductsDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ProductsDAO.insert()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.insert()***********************");
    }

}
