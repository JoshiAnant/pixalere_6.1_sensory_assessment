/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.dao;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.TriggerVO;
import com.pixalere.common.bean.TriggerProductsVO;
import com.pixalere.common.bean.TriggerCriteriaVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import java.util.Collection;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.log4j.Logger;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  trigger and trigger_criteria    tables.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class TriggerDAO  implements DataAccessObject{
    static private Logger log = Logger.getLogger(TriggerDAO.class);
    
    public TriggerDAO(){
        
    }
    /**
     * Deletes trigger records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 6.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            TriggerVO trig = (TriggerVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(trig);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in TriggerDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in TriggerVO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in TriggerVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving TriggerDAO.delete()***********************");
    }
    
     /**
     * Finds a single trigger record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the trigger table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 6.0
     */
    public ValueObject findByPK(int  primaryKey) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        TriggerVO trigVO   = new TriggerVO();
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            trigVO.setTrigger_id(new Integer(primaryKey));
            
            Query query = new QueryByCriteria(trigVO);
            log.debug("TriggerDAO.findByPK(): "+query.toString()+" :::: Value="+primaryKey);
            
            trigVO = (TriggerVO)broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in TriggerDAO.findByPK()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.findByPK()***********************");
        return trigVO;
    }
    /**
     * Finds a all trigger records, as specified by the value object
     * passed in. Only retrieve triggers associated with these care types and trigger types.
     * If no record is found a null value is returned.
     *
     * @param care_type 
     * @return 
     * @throws com.pixalere.common.DataAccessException 
     *
     * @since 6.0
     */
    public Collection findAllByCriteria(String care_type) throws DataAccessException{
        log.info("***********************Entering triggerDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<TriggerVO> results=null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            
            Criteria crit=new Criteria();
            if(care_type!=null){
                crit.addEqualTo("wound_type",care_type);
            }
            crit.addEqualTo("active","1");
            /*Criteria or = new Criteria();
            for(UserAccountRegionsVO regions : prof.getRegions()){
                Criteria tmp = new Criteria();
                tmp.addEqualTo("products_by_facility.treatment_location_id",regions.getId());
                or.addOrCriteria(tmp);
            }
            crit.addAndCriteria(or);*/
            QueryByCriteria query = new QueryByCriteria(TriggerVO.class,crit);
            query.addOrderByAscending("name");
            //ProductCategoryServiceImpl prodCateBD=new ProductCategoryServiceImpl();
                
            results = (Collection) broker.getCollectionByQuery(query);
            /*for(ProductsVO prodsVO : col){
                //ProductCategoryVO prodCateVO = (ProductCategoryVO)prodCateBD.getProductCategory(prodsVO.getCategory_id().intValue());
                prodsVO.setCategory(prodCateVO);
                results.add(prodsVO);
            }*/
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.findAllByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in TriggerDAO.findAllByCriteria()",e);
        } catch(Exception e){
            log.error("TriggerDAO.findAllByCriteria(): "+e.getMessage());
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all trigger records, as specified by the value object
     * passed in. Only retrieve triggers associated with these care types and trigger types.
     * If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @param prof logged in Professional, grab products based on facility.
     *
     * @since 6.0
     */
    public Collection findAllByCriteria() throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<TriggerVO> results=null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            
            Criteria crit=new Criteria();
            
            QueryByCriteria query = new QueryByCriteria(TriggerVO.class,crit);
            query.addOrderByAscending("name");
                
            results = (Collection) broker.getCollectionByQuery(query);
    
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()",e);
        } catch(Exception e){
            log.error("ProductsDAO.findByCriteria(): "+e.getMessage());
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all trigger records, as specified by the value object
     * passed in. Only retrieve triggers associated with these care types and trigger types.
     * If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @param prof logged in Professional, grab products based on facility.
     *
     * @since 6.0
     */
    public Collection findAllByCriteria(int trigger_id) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<TriggerCriteriaVO> results=null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            
            Criteria crit=new Criteria();
            crit.addEqualTo("trigger_id",trigger_id);
            QueryByCriteria query = new QueryByCriteria(TriggerCriteriaVO.class,crit);
            query.addOrderByAscending("trigger_criteria_id");
                
            results = (Collection) broker.getCollectionByQuery(query);
    
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()",e);
        } catch(Exception e){
            log.error("ProductsDAO.findByCriteria(): "+e.getMessage());
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all trigger records, as specified by the value object
     * passed in. Only retrieve triggers associated with these products.
     * If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @param prof logged in Professional, grab products based on facility.
     *
     * @since 6.0
     */
    public Collection findAllProductTriggersByProduct(int product_id) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<TriggerProductsVO> results=null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            
            Criteria crit=new Criteria();
            crit.addEqualTo("product_id",product_id);
            QueryByCriteria query = new QueryByCriteria(TriggerProductsVO.class,crit);
            query.addOrderByAscending("trigger_id");
                
            results = (Collection) broker.getCollectionByQuery(query);
    
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()",e);
        } catch(Exception e){
            log.error("ProductsDAO.findByCriteria(): "+e.getMessage());
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds an trigger record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param trigger  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 6.0
     */
    public ValueObject findByCriteria(TriggerVO trig) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        TriggerVO returnVO   = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new TriggerVO();
            
            Query query = new QueryByCriteria(trig);
            returnVO = (TriggerVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in TriggerDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Inserts a single trigger object into the trigger table.  The value object passed
     * in has to be of type TriggerVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the trigger record.
     *
     * @since 6.0
     */
    public void update(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.insert()***********************");
        TriggerVO t = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            t = (TriggerVO) insertRecord;
            
            broker.beginTransaction();
            broker.store(t);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in TriggerDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in TriggerDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in TriggerDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.insert()***********************");
    }
    /**
     * Deletes trigger_criteria records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 6.0
     */
    public void deleteCriteria(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            TriggerCriteriaVO trig = (TriggerCriteriaVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(trig);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in TriggerDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in TriggerVO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in TriggerVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving TriggerDAO.delete()***********************");
    }
    
     /**
     * Finds a single trigger_criteria record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the trigger_criteria table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 6.0
     */
    public ValueObject findCriteriaByPK(int  primaryKey) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        TriggerCriteriaVO trigVO   = new TriggerCriteriaVO();
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            trigVO.setId(new Integer(primaryKey));
            
            Query query = new QueryByCriteria(trigVO);
            log.debug("TriggerDAO.findCriteriaByPK(): "+query.toString()+" :::: Value="+primaryKey);
            
            trigVO = (TriggerCriteriaVO)broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in TriggerDAO.findByPK()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.findByPK()***********************");
        return trigVO;
    }
    /**
     * Finds a all trigger_criteria records, as specified by the value object
     * passed in. Only retrieve trigger_criteria associated   
     * If no record is found a null value is returned.
     *
     * @param VO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @param prof logged in Professional, grab products based on facility.
     *
     * @since 6.0
     */
    public Collection findAllOptionsByCriteria(String match_type,String criteria_type) throws DataAccessException{
        log.info("***********************Entering TriggerDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<TriggerCriteriaVO> results=null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            
            Criteria crit=new Criteria();
            if(match_type!=null){
                crit.addEqualTo("match_type",match_type);
            }
            if(criteria_type!=null){
                crit.addEqualTo("criteria_type",criteria_type);
            }
            /*Criteria or = new Criteria();
            for(UserAccountRegionsVO regions : prof.getRegions()){
                Criteria tmp = new Criteria();
                tmp.addEqualTo("products_by_facility.treatment_location_id",regions.getId());
                or.addOrCriteria(tmp);
            }
            crit.addAndCriteria(or);*/
            QueryByCriteria query = new QueryByCriteria(TriggerCriteriaVO.class,crit);
            query.addOrderByAscending("trigger_criteria_id");
            //ProductCategoryServiceImpl prodCateBD=new ProductCategoryServiceImpl();
                
            results = (Collection) broker.getCollectionByQuery(query);
            /*for(ProductsVO prodsVO : col){
                //ProductCategoryVO prodCateVO = (ProductCategoryVO)prodCateBD.getProductCategory(prodsVO.getCategory_id().intValue());
                prodsVO.setCategory(prodCateVO);
                results.add(prodsVO);
            }*/
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in TriggerDAO.findByCriteria()",e);
        } catch(Exception e){
            log.error("TriggerDAO.findByCriteria(): "+e.getMessage());
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds an trigger_criteria record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param trigger_criteria  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 6.0
     */
    public ValueObject findOptionByCriteria(TriggerCriteriaVO trig) throws DataAccessException{
        log.info("***********************Entering TriggerDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        TriggerCriteriaVO returnVO   = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new TriggerCriteriaVO();
            
            Query query = new QueryByCriteria(trig);
            returnVO = (TriggerCriteriaVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in TriggerDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Inserts a single trigger_criteria object into the trigger table.  The value object passed
     * in has to be of type TriggerVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the trigger record.
     *
     * @since 6.0
     */
    public void insertOption(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.insertOption()***********************");
        TriggerCriteriaVO t = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            t = (TriggerCriteriaVO) insertRecord;
            
            broker.beginTransaction();
            broker.store(t);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.insertOption(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in TriggerDAO.insertOption()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in TriggerDAO.insertOption(): " + e.toString());
            throw new DataAccessException("Error in TriggerDAO.insertOption()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.insertOption()***********************");
    }
    /**
     * Inserts a single trigger_products object into the trigger table.  The value object passed
     * in has to be of type TriggerProductsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the trigger record.
     *
     * @since 6.0
     */
    public void insertProduct(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.insertProduct()***********************");
        TriggerProductsVO t = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            t = (TriggerProductsVO) insertRecord;
            
            broker.beginTransaction();
            broker.store(t);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.insertProduct(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in TriggerDAO.insertProduct()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in TriggerDAO.insertProduct(): " + e.toString());
            throw new DataAccessException("Error in TriggerDAO.insertProduct()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving TriggerDAO.insertProduct()***********************");
    }
    /**
     * Deletes a single trigger_products record, as specified by the value object
     * passed in.  The value object passed in has to be of type TriggerProductsVO.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 6.0
     */
    public void deleteProduct(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering TriggerDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            TriggerProductsVO trigProd = (TriggerProductsVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(trigProd);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in TriggerDAO.deleteProduct(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in TriggerVO.deleteProduct()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in TriggerDAO.deleteProduct(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in TriggerProductsVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving TriggerDAO.deleteProduct()***********************");
    }
}
