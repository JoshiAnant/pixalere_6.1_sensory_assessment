/*
 * Created on Dec 13, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pixalere.common.dao;
import com.pixalere.utils.Constants;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.ProductCategoryVO;
import com.pixalere.common.bean.ProductsCategoriesLocalizationVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import java.util.Collection;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.log4j.Logger;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  product_categories    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class ProductCategoryDAO implements DataAccessObject{
    static private Logger log = Logger.getLogger(ProductCategoryDAO.class);
    
    public ProductCategoryDAO(){
        
    }
    /**
     * Deletes product_categories records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ProductCategoryDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            ProductCategoryVO productsVO = (ProductCategoryVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(productsVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ProductCategoryDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ProductCategoryVO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductCategoryDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ProductCategoryVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ProductCategoryDAO.delete()***********************");
    }
    
    
    /**
     * Finds a single product_categories record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the product_categories table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int  primaryKey) throws DataAccessException {
        log.info("***********************Entering ProductCategoryDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        ProductCategoryVO productsVO   = new ProductCategoryVO();
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO.setId(new Integer(primaryKey));
            
            Query query = new QueryByCriteria(productsVO);
            log.debug("ProductCategoryDAO.findByPK(): "+query.toString()+" :::: Value="+primaryKey);
            
            productsVO = (ProductCategoryVO)broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductCategoryDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in ProductCategoryDAO.findByPK()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductCategoryDAO.findByPK()***********************");
        return productsVO;
    }
    
    /**
     * Finds a all product_categories records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(ProductCategoryVO productsVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(productsVO);
            query.addOrderByAscending("title");
            log.info("******************** QUERY: "+productsVO.getActive());
            results = (Collection) broker.getCollectionByQuery(query);
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductCategoryDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ProductCategoryDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductCategoryDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an product_categories record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(ProductCategoryVO productsVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ProductCategoryVO returnVO   = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ProductCategoryVO();
            
            Query query = new QueryByCriteria(productsVO);
            returnVO = (ProductCategoryVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductCategoryDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ProductCategoryDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductCategoryDAO.findByCriteria()***********************");
        return returnVO;
    }
    
    /**
     * Inserts a single ProductCategoryVO object into the product_categories table.  The value object passed
     * in has to be of type ProductCategoryVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the product_categories record.
     *
     * @since 3.0
     */
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        ProductCategoryVO productsVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO = (ProductCategoryVO) insertRecord;
            
            broker.beginTransaction();
            broker.store(productsVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductCategoryDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ProductCategoryDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in ProductCategoryDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ProductCategoryDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ProductCategoryDAO.insert()***********************");
    }
    
    /**
     * Inserts a single ProductCategoryVO object into the product_categories table.  The value object passed
     * in has to be of type ProductCategoryVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the product_categories record.
     *
     * @since 3.0
     */
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ProductCategoryDAO.update()***********************");
        ProductCategoryVO productsVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO = (ProductCategoryVO) updateRecord;
            
            broker.beginTransaction();
            broker.store(productsVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductCategoryDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ProductCategoryDAO.update()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ProductCategoryDAO.update(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ProductCategoryDAO.update()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Entering ProductCategoryDAO.update()***********************");
    }
    /**
     * Finds an products_categories_l10n record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param products_categories_l10n  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 5.2
     */
    public ValueObject findProductsCategoriesL10NByCriteria(ProductsCategoriesLocalizationVO productsVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ProductsCategoriesLocalizationVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ProductsCategoriesLocalizationVO();
            Query query = new QueryByCriteria(productsVO);
            returnVO = (ProductsCategoriesLocalizationVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Inserts a single ProductsCategoriesLocalizationVO object into the products_categories_l10n table.  The value object passed
     * in has to be of type productsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the products record.
     *
     * @since 5.2
     */
    public void insertProductsCategoriesLocalization(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        ProductsCategoriesLocalizationVO productsVO = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            productsVO = (ProductsCategoriesLocalizationVO) insertRecord;
            broker.beginTransaction();
            broker.store(productsVO);
            broker.commitTransaction();
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ProductsDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ProductsDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ProductsDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ProductsDAO.insert()***********************");
    }
}
