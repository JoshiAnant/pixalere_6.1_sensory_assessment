/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.dao;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.LibraryVO;
import com.pixalere.common.bean.LibraryCategoryVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.bean.TableUpdatesVO;
import java.util.Collection;
import com.pixalere.utils.Constants;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.log4j.Logger;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  <<table>>    table.
 * @author travis morris
 * @todo Need to implement this class as a singleton.
 *
 */
public class LibraryDAO implements DataAccessObject{
    static private Logger log = Logger.getLogger(LibraryDAO.class);
    
    public LibraryDAO(){
        
    }
    /**
     * Deletes <<table>> records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 4.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.delete()***********************");
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            LibraryVO lib = (LibraryVO) deleteRecord;
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(lib);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ConfigurationVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.delete()***********************");
    }
     /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param libary fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public Collection findAllByCriteria(LibraryVO lib) throws DataAccessException{
        log.info("***********************Entering configurationDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            
            QueryByCriteria query = new QueryByCriteria(lib);
            
            query.addOrderByAscending("orderby");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Deletes <<table>> records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 4.0
     */
    public void deleteCategory(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.delete()***********************");
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            LibraryCategoryVO lib = (LibraryCategoryVO) deleteRecord;
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(lib);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ConfigurationVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.delete()***********************");
    }
     /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param libary_category fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public Collection findAllCategoriesByCriteria(LibraryCategoryVO lib) throws DataAccessException{
        log.info("***********************Entering configurationDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            
            QueryByCriteria query = new QueryByCriteria(lib);
            
            query.addOrderByAscending("order_by");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param library  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public LibraryVO findByCriteria(LibraryVO lib) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        LibraryVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LibraryVO();
            Query query = new QueryByCriteria(lib);
            returnVO = (LibraryVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param library  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public LibraryCategoryVO findCategoryByCriteria(LibraryCategoryVO lib) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        LibraryCategoryVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LibraryCategoryVO();
            Query query = new QueryByCriteria(lib);
            returnVO = (LibraryCategoryVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 4.0
     * @since 4.0
     */
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.insert()***********************");
        LibraryVO lib = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            lib = (LibraryVO) insertRecord;
            broker.beginTransaction();
            broker.store(lib);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ConfigurationDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.insert()***********************");
    }
    /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 4.0
     * @since 4.0
     */
    public void insertCategory(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.insert()***********************");
        LibraryCategoryVO lib = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            lib = (LibraryCategoryVO) insertRecord;
            broker.beginTransaction();
            broker.store(lib);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ConfigurationDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.insert()***********************");
    }
    /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 4.0
     * @since 4.0
     */
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.update()***********************");
        LibraryVO lib = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            lib = (LibraryVO) updateRecord;
            broker.beginTransaction();
            broker.store(lib);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.update()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.update(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ConfigurationDAO.update()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Done ConfigurationDAO.update()***********************");
    }
    public ValueObject findByPK(int dead){
        return null;
    }
}