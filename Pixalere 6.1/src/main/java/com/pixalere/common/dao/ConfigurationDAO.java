package com.pixalere.common.dao;
        
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.ConfigurationVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.admin.bean.VendorVO;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.log4j.Logger;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  <<table>>    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class ConfigurationDAO implements DataAccessObject{
    static private Logger log = Logger.getLogger(ConfigurationDAO.class);
    
    public ConfigurationDAO(){
        
    }
    /**
     * Deletes <<table>> records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @author travis morris
     * @since 4.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            ConfigurationVO configurationVO = (ConfigurationVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(configurationVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ConfigurationVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.delete()***********************");
    }
    
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param configuration fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public List findAllByCriteria(ConfigurationVO configurationVO) throws DataAccessException{
        log.info("***********************Entering configurationDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        List<ConfigurationVO> results = new ArrayList();
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(configurationVO);
            query.addOrderByAscending("category");
            query.addOrderByAscending("config_setting");
            
            Collection<ConfigurationVO> resultst = (Collection<ConfigurationVO>) broker.getCollectionByQuery(query);
            for(ConfigurationVO config : resultst){
                try{
                config.setTitle(Common.getLocalizedString("pixalere.config."+config.getConfig_setting(),"en"));
                }catch(NullPointerException e){
                    //ignore missing config
                }
                if(config.getTitle()!=null && config.getCategory()!=null){
                    results.add(config);
                }else{
                    System.out.println("Missing Property: "+config.getConfig_setting()+" "+config.getCategory()+" "+config.getTitle());
                }
            }
            Collections.sort(results,new ConfigurationVO());
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        }catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ConfigurationDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param configuration  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0
     */
    public ConfigurationVO findByCriteria(ConfigurationVO configurationVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ConfigurationVO returnVO   = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ConfigurationVO();
            
            Query query = new QueryByCriteria(configurationVO);
            returnVO = (ConfigurationVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ConfigurationDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param table_updates  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.2
     */
    public ValueObject findTableUpdatesByCriteria(TableUpdatesVO table) throws DataAccessException{
        log.info("***********************Entering ComponentsDAO.findTableUpdatesByCriteria()***********************");
        PersistenceBroker broker = null;
        TableUpdatesVO returnVO   = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new TableUpdatesVO();
            
            Query query = new QueryByCriteria(table);
            returnVO = (TableUpdatesVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.findTableUpdatesByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ComponentsDAO.findTableUpdatesByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.findByCriteria()***********************");
        return returnVO;
    }
    /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 4.2
     * @since 4.2
     */
    public void updateTableUpdates(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        TableUpdatesVO table = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            table = (TableUpdatesVO) insertRecord;
            table.setId(new Integer(1));
            broker.beginTransaction();
            broker.store(table);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ComponentsDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in ComponentsDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ComponentsDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.insert()***********************");
    }
    /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 4.0
     * @since 4.0
     */
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.insert()***********************");
        ConfigurationVO configurationVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            configurationVO = (ConfigurationVO) insertRecord;
            
            broker.beginTransaction();
            broker.store(configurationVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ConfigurationDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ConfigurationDAO.insert()***********************");
    }
    /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 4.0
     * @since 4.0
     */
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.update()***********************");
        ConfigurationVO configurationVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            configurationVO = (ConfigurationVO) updateRecord;
            broker.beginTransaction();
            broker.store(configurationVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.update()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.update(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ConfigurationDAO.update()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Done ConfigurationDAO.update()***********************");
    }
    public void updateVendor(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.update()***********************");
        VendorVO configurationVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            configurationVO = (VendorVO) updateRecord;
            broker.beginTransaction();
            broker.store(configurationVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.update()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.update(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ConfigurationDAO.update()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Done ConfigurationDAO.update()***********************");
    }
    /**
     * Deletes <<table>> records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @author travis morris
     * @since 6.1
     */
    public void deleteVendor(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            VendorVO configurationVO = (VendorVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(configurationVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ConfigurationVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.delete()***********************");
    }
    
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param configuration fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 6.1
     */
    public Collection findAllByCriteria(VendorVO configurationVO) throws DataAccessException{
        log.info("***********************Entering configurationDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<VendorVO> results = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(configurationVO);
            query.addOrderByAscending("name");
            
            results = (Collection<VendorVO>) broker.getCollectionByQuery(query);
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        }catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ConfigurationDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param configuration  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 6.1
     */
    public VendorVO findByCriteria(VendorVO configurationVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        VendorVO returnVO   = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new VendorVO();
            
            Query query = new QueryByCriteria(configurationVO);
            returnVO = (VendorVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ConfigurationDAO.findByCriteria()***********************");
        return returnVO;
    }
    public ValueObject findByPK(int dead){
        return null;
    }
}
