package com.pixalere.common.dao;
        
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.bean.InformationPopupVO;
import com.pixalere.common.bean.TableUpdatesVO;
import java.util.Collection;
import com.pixalere.utils.Constants;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.log4j.Logger;
import com.pixalere.common.bean.QuickTipsVO;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  product_categories    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class ComponentsDAO implements DataAccessObject{
    static private Logger log = Logger.getLogger(ComponentsDAO.class);
    
    public ComponentsDAO(){
        
    }
     /**
     * Finds a all product_categories records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param pages the pages which we want components from
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(int[] pages) throws DataAccessException{
        log.info("***********************Entering productsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            //crit.addEqualTo("hide",0);
            Criteria or = new Criteria();
            for(int page : pages){
                Criteria c = new Criteria();
                c.addEqualTo("flowchart", page);
                or.addOrCriteria(c);
            }
            crit.addAndCriteria(or);
            QueryByCriteria query = QueryFactory.newQuery(ComponentsVO.class,crit);
            query.addOrderByAscending("flowchart");
            query.addOrderByAscending("orderby");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ComponentsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ComponentsDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Deletes product_categories records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ComponentsDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            ComponentsVO componentsVO = (ComponentsVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(componentsVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ComponentsDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ComponentsVO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ComponentsVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ComponentsDAO.delete()***********************");
    }
    
    
    /**
     * Finds a single product_categories record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the product_categories table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findInfoPopup(InformationPopupVO info) throws DataAccessException {
        log.info("***********************Entering ComponentsDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        InformationPopupVO t = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(info);
            query.addOrderByDescending("id");
            t = (InformationPopupVO)broker.getObjectByQuery(query);
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in ComponentsDAO.findByPK()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.findByPK()***********************");
        return t;
    }
    
    /**
     * Finds a all product_categories records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     * @version 6.1
     */
    public Collection findAllByCriteria(ComponentsVO componentsVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(componentsVO);
            query.addOrderByAscending("flowchart");
            query.addOrderByAscending("orderby");
            
            results = (Collection) broker.getCollectionByQuery(query);
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ComponentsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.findAllByCriteria()***********************");
        return results;
    }
     /**
     * Finds a all info_popups records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param InformationPopupVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.0.1
     */
    public Collection findAllByCriteria(InformationPopupVO componentsVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(componentsVO);
            
            results = (Collection) broker.getCollectionByQuery(query);
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ComponentsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all tips records, as specified by the page 
     * passed in.  If no record is found a null value is returned.
     *
     * @param page is the page which we're retrieve records for.
     *
     * @since 6.0
     */
    public Collection findAllByCriteria(String page) throws DataAccessException{
        log.info("***********************Entering productsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        
        
        try{
            QuickTipsVO t = new QuickTipsVO();
            if(page!=null){
                t.setPage(page);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(t);
            
            results = (Collection) broker.getCollectionByQuery(query);
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ComponentsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an product_categories record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(ComponentsVO componentsVO) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ComponentsVO returnVO   = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ComponentsVO();
            
            Query query = new QueryByCriteria(componentsVO);
            returnVO = (ComponentsVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ComponentsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.findByCriteria()***********************");
        return returnVO;
    }
    
    /**
     * Inserts a single ComponentsVO object into the product_categories table.  The value object passed
     * in has to be of type ComponentsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the product_categories record.
     *
     * @since 3.0
     */
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        ComponentsVO componentsVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            componentsVO = (ComponentsVO) insertRecord;
            
            broker.beginTransaction();
            broker.store(componentsVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ComponentsDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in ComponentsDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ComponentsDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.insert()***********************");
    }
     /**
     * Inserts a single QuickTipsVO object into the quick_tips table.  The value object passed
     * in has to be of type QuickTipsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the quick_tips record.
     *
     * @since 6.0
     */
    public void insertTips(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering ComponentsDAO.insertTips()***********************");
        QuickTipsVO componentsVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            componentsVO = (QuickTipsVO) insertRecord;
            
            broker.beginTransaction();
            broker.store(componentsVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.insertTips(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ComponentsDAO.insertTips()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in ComponentsDAO.insertTips(): " + e.toString());
            throw new DataAccessException("Error in ComponentsDAO.insertTips()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.insertTips()***********************");
    }
    /**
     * Inserts a single ComponentsVO object into the product_categories table.  The value object passed
     * in has to be of type ComponentsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the product_categories record.
     *
     * @since 3.0
     */
    public void insertInfoPopup(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        InformationPopupVO infoVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            infoVO = (InformationPopupVO) insertRecord;
            broker.beginTransaction();
            broker.store(infoVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ComponentsDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            
            log.error("PersistenceBrokerException thrown in ComponentsDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ComponentsDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Leaving ComponentsDAO.insert()***********************");
    }
    /**
     * Inserts a single ComponentsVO object into the product_categories table.  The value object passed
     * in has to be of type ComponentsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the product_categories record.
     *
     * @since 3.0
     */
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ComponentsDAO.update()***********************");
        ComponentsVO componentsVO = null;
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            componentsVO = (ComponentsVO) updateRecord;
            
            broker.beginTransaction();
            broker.store(componentsVO);
            broker.commitTransaction();
            
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ComponentsDAO.update()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ComponentsDAO.update(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ComponentsDAO.update()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        
        log.info("***********************Entering ComponentsDAO.update()***********************");
    }
    
    public ValueObject findByPK(int dead){
        return null;
    }
    public void deleteInfoPopup(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ComponentsDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            InformationPopupVO componentsVO = (InformationPopupVO) deleteRecord;
            
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(componentsVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ComponentsDAO.delete(): " + e.toString());
            e.printStackTrace();
            
            throw new DataAccessException("Error in ComponentsVO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ComponentsDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ComponentsVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ComponentsDAO.delete()***********************");
    }
}
