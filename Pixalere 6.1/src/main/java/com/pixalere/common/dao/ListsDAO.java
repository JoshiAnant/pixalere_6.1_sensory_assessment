package com.pixalere.common.dao;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import com.pixalere.utils.Constants;
import java.util.Collection;
import java.util.List;
import com.pixalere.utils.Common;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.LookupVO;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.QueryFactory;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.bean.LanguageVO;
/**
 *
 * This class is responsible for handling all CRUD logic associated with the
 *
 * listsdata table.
 *
 *
 *
 * @todo Need to implement this class as a singleton.
 *
 *
 *
 */
public class ListsDAO implements DataAccessObject {
    
    // Create Log4j category instance for logging
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger
            .getLogger(ListsDAO.class);
    
    
    public ListsDAO() {
        
    }
    /**
     * Finds a all listdata records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param resource_ids fields in ValueObject which are not null will be used as
     * @param onlyActive
     * parameters in the SQL statement.
     * @verson 4.2
     * @since 4.2
     */
    public Collection findAllByCriteria(List<Integer> resource_ids,boolean onlyActive)
    throws DataAccessException {
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LookupVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            if(onlyActive==true){
                crit.addEqualTo("active",1);
            }
            Criteria resourceCrit =new Criteria();
            for(Integer resource_id : resource_ids){
                Criteria t = new Criteria();
                t.addEqualTo("resource_id", resource_id);
                resourceCrit.addOrCriteria(t);
            }
            crit.addAndCriteria(resourceCrit);
            QueryByCriteria query = QueryFactory.newQuery(LookupVO.class,crit);
            query.addOrderByDescending("active");
            query.addOrderByAscending("orderby");
            results = broker.getCollectionByQuery(query);
        }
        catch (ConnectionLocatorException e) {
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
        }
        finally {
            if (broker != null)
                broker.close();
        }
        log
                .info("***********************Leaving ListsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Deletes list_data records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @version 3.2
     * @since 3.0
     */
    
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        
        log.info("***********************Entering ListsDAO.delete()***********************");
        
        PersistenceBroker broker = null;
        
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            
            LookupVO listsVO = (LookupVO) deleteRecord;
            
            // Begin the transaction.
            
            broker.beginTransaction();
            
            broker.delete(listsVO);
            
            broker.commitTransaction();
            
        }
        
        catch (PersistenceBrokerException e) {
            
            // if something went wrong: rollback
            
            broker.abortTransaction();
            
            log.error("PersistenceBrokerException thrown in ListsDAO.delete(): "
                    + e.toString());
            
            e.printStackTrace();
            
            throw new DataAccessException("Error in ListsVO.delete()", e);
            
        }
        
        catch (ConnectionLocatorException e) {
            
            log.error("ServiceLocatorException thrown in ListsDAO.delete(): "
                    + e.toString());
            
            throw new DataAccessException(
                    "ServiceLocator exception in ListsVO.delete()", e);
            
        }
        
        finally {
            
            if (broker != null)
                broker.close();
            
        }
        
        log.info("***********************Leaving ListsDAO.delete()***********************");
        
    }
    
    /**
     * Finds a single listdata record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the listdata table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @version 3.2
     * @since 3.0
     */
    
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        
        log.info("***********************Entering ListsDAO.findByPK()***********************");
        
        PersistenceBroker broker = null;
        
        LookupVO listsVO = new LookupVO();
        
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            listsVO.setId(new Integer(primaryKey));
            Query query = new QueryByCriteria(listsVO);
            
            log.debug("ListsDAO.findByPK(): " + query.toString() + " :::: Value=" + primaryKey);
            
            listsVO = (LookupVO) broker.getObjectByQuery(query);
            
        }
        
        catch (ConnectionLocatorException e) {
            
            log.error("ServiceLocatorException thrown in ListsDAO.findByPK(): "
                    + e.toString());
            
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByPK()", e);
            
        }
        
        finally {
            
            if (broker != null)
                broker.close();
            
        }
        
        log.info("***********************Leaving ListsDAO.findByPK()***********************");
        
        return listsVO;
        
    }
    
     /**
     * Finds a all listdata records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(LookupVO listsVO)
    throws DataAccessException {
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LookupVO> results = null;
        LookupVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LookupVO();
            listsVO.setActive(1);
            QueryByCriteria query = new QueryByCriteria(listsVO);
            query.addOrderByDescending("active");
            query.addOrderByAscending("orderby");
            results = broker.getCollectionByQuery(query);
            
        }
        catch (ConnectionLocatorException e) {
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
        }
        finally {
            if (broker != null)
                broker.close();
        }
        log
                .info("***********************Leaving ListsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all listdata records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(LookupVO listsVO, boolean onlyActive)
    throws DataAccessException {
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LookupVO> results = null;
        LookupVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LookupVO();
            if(onlyActive==true){
                listsVO.setActive(1);
            }
            QueryByCriteria query = new QueryByCriteria(listsVO);
            query.addOrderByDescending("active");
            query.addOrderByAscending("orderby");
            results = broker.getCollectionByQuery(query);
            
        }
        catch (ConnectionLocatorException e) {
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
        }
        finally {
            if (broker != null)
                broker.close();
        }
        log
                .info("***********************Leaving ListsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all language records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 5.2
     * @since 5.2
     */
    public Collection findLanguagesByCriteria(LanguageVO listsVO)
    throws DataAccessException {
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LanguageVO> results = null;
       
        try {
            broker = ConnectionLocator.getInstance().findBroker();
         
            QueryByCriteria query = new QueryByCriteria(listsVO);
           
            results = broker.getCollectionByQuery(query);
             }
        catch (ConnectionLocatorException e) {
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
        }
        finally {
            if (broker != null)
                broker.close();
        }
        log
                .info("***********************Leaving ListsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a language record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 5.2
     * @since 5.2
     */
    public LanguageVO findLanguageByCriteria(LanguageVO listsVO)
    throws DataAccessException {
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        LanguageVO results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(listsVO);
            results = (LanguageVO)broker.getObjectByQuery(query);
             }
        catch (ConnectionLocatorException e) {
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
        }
        finally {
            if (broker != null)
                broker.close();
        }
        log
                .info("***********************Leaving ListsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds an listsdata record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public ValueObject findByCriteria(LookupVO listsVO)
    throws DataAccessException {
        
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        
        PersistenceBroker broker = null;
        
        LookupVO returnVO = null;
        
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            
            returnVO = new LookupVO();
            
            listsVO.setActive(new Integer(1));
            Query query = new QueryByCriteria(listsVO);
            
            returnVO = (LookupVO) broker.getObjectByQuery(query);
            
            
        }
        
        catch (ConnectionLocatorException e) {
            
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
            
        }
        
        finally {
            
            if (broker != null)
                broker.close();
            
        }
        
        log.info("***********************Leaving ListsDAO.findByCriteria()***********************");
        
        return returnVO;
        
    }
    /**
     * Finds an lookup_l10n record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 6.0
     * @since 6.0
     */
    public ValueObject findByCriteria(LookupLocalizationVO listsVO)
    throws DataAccessException {
        
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        
        PersistenceBroker broker = null;
        
        LookupLocalizationVO returnVO = null;
        
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            
            returnVO = new LookupLocalizationVO();
            
            Query query = new QueryByCriteria(listsVO);
            
            returnVO = (LookupLocalizationVO) broker.getObjectByQuery(query);
            
            
        }
        
        catch (ConnectionLocatorException e) {
            
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
            
        }
        
        finally {
            
            if (broker != null)
                broker.close();
            
        }
        
        log.info("***********************Leaving ListsDAO.findByCriteria()***********************");
        
        return returnVO;
        
    }
    /**
     * Finds an lookup_l10n record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 6.0
     * @since 6.0
     */
    public Collection findAllByCriteria(LookupLocalizationVO listsVO)
    throws DataAccessException {
        
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        
        PersistenceBroker broker = null;
        
       Collection<LookupLocalizationVO> results = null;
        
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            Query query = new QueryByCriteria(listsVO);
            results = broker.getCollectionByQuery(query);
            
            
        }
        catch (ConnectionLocatorException e) {
            
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
            
        }
        
        finally {
            
            if (broker != null)
                broker.close();
            
        }
        
        log.info("***********************Leaving ListsDAO.findByCriteria()***********************");
        
        return results;
        
    }
    /**
     * Updates a single list_data record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the list_data table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 3.2
     * @since 3.0
     */
    
    public void update(ValueObject updateRecord) throws DataAccessException {
        
        log.info("***********************Entering ListsDAO.update()***********************");
        
        LookupVO listsVO = null;
        
        PersistenceBroker broker = null;
        
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            
            listsVO = (LookupVO) updateRecord;
            
            broker.beginTransaction();
            broker.store(listsVO);
            broker.commitTransaction();
            
        }
        
        catch (ConnectionLocatorException e) {
            
            log.error("ServiceLocatorException thrown in ListsDAO.update(): "
                    + e.toString());
            
            e.printStackTrace();
            throw new DataAccessException(
                    "ServiceLocator in Error in ListsDAO.update()", e);
            
        }
        
        catch (PersistenceBrokerException e) {
            
            // if something went wrong: rollback
            
            broker.abortTransaction();
            
            log.error("PersistenceBrokerException thrown in ListsDAO.update(): "
                    + e.toString());
            
            e.printStackTrace();
            
            throw new DataAccessException("Error in ListsDAO.update()", e);
            
        }
        
        finally {
            
            if (broker != null)
                broker.close();
            
        }
        
        log.info("***********************Entering ListsDAO.update()***********************");
        
    }
/**
     * Updates a single languages record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the list_data table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 3.2
     * @since 3.0
     */
    public void updateLanguage(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ListsDAO.updateLanguage()***********************");
        LanguageVO listsVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            listsVO = (LanguageVO) updateRecord;
            broker.beginTransaction();
            broker.store(listsVO);
            broker.commitTransaction();
        }
        catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ListsDAO.updateLanguage(): "
                    + e.toString());
            e.printStackTrace();
            throw new DataAccessException(
                    "ServiceLocator in Error in ListsDAO.updateLanguage()", e);
        }
        catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ListsDAO.updateLanguage(): "
                    + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ListsDAO.updateLanguage()", e);
        }
        finally {
            if (broker != null)
                broker.close();
        }
        log.info("***********************Entering ListsDAO.updateLanguage()***********************");
    }
    /**
     * Updates a single lookup_l10n record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the list_data table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 3.2
     * @since 3.0
     */
    public void updateLocalization(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ListsDAO.updateLocalization()***********************");
        LookupLocalizationVO listsVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            listsVO = (LookupLocalizationVO) updateRecord;
            broker.beginTransaction();
            broker.store(listsVO);
            broker.commitTransaction();
        }
        catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ListsDAO.updateLocalization(): "
                    + e.toString());
            e.printStackTrace();
            throw new DataAccessException(
                    "ServiceLocator in Error in ListsDAO.updateLocalization()", e);
        }
        catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ListsDAO.updateLocalization(): "
                    + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ListsDAO.updateLocalization()", e);
        }
        finally {
            if (broker != null)
                broker.close();
        }
        log.info("***********************Entering ListsDAO.updateLocalization()***********************");
    }
    /**
     * Finds a all listdata records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param listsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @todo find out if this method is legacy and no longer used, 
     * can be refactored into main one, and removed.
     * @verson 3.2
     * @since 3.0
     */
    public Collection<LookupVO> findAllIncludingInactiveByCriteria(LookupVO listsVO)
    throws DataAccessException {
        
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        
        PersistenceBroker broker = null;
        
        Collection<LookupVO> results = null;
        
        LookupVO returnVO = null;
        
        try {
            
            broker = ConnectionLocator.getInstance().findBroker();
            
            returnVO = new LookupVO();
            
            QueryByCriteria query = new QueryByCriteria(listsVO);
            query.addOrderByAscending("orderby");
            Collection<LookupVO> coll = broker.getCollectionByQuery(query);
            results = coll;
            
        }
        
        catch (ConnectionLocatorException e) {
            
            log.error("ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
            
        }
        
        finally {
            
            if (broker != null)
                broker.close();
            
        }
        
        log.info("***********************Leaving ListsDAO.findByCriteria()***********************");
        
        return results;
        
    }
}
