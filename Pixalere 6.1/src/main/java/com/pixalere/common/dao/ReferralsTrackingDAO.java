package com.pixalere.common.dao;
import com.pixalere.common.bean.AutoReferralVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import java.util.Hashtable;
import com.pixalere.common.*;
import com.pixalere.utils.PDate;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.utils.Constants;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.assessment.dao.WoundAssessmentDAO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.Criteria;
import java.util.Iterator;
import java.util.Collection;
import java.util.Vector;
import com.pixalere.utils.Serialize;
import com.pixalere.utils.Common;
import org.apache.ojb.odmg.PBCapsule;
import java.util.List;
import java.util.ArrayList;
import java.sql.*;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  referrals_tracking    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class ReferralsTrackingDAO implements DataAccessObject {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReferralsTrackingDAO.class);
    public ReferralsTrackingDAO() {
    }
    /**
     * Deletes referrals_tracking records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering AssignPatientsDAO.delete()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Collection result = findAllByCriteria(((ReferralsTrackingVO) deleteRecord));
            Iterator iter = result.iterator();
            while (iter.hasNext()) {
                ReferralsTrackingVO assignPatientsVO = (ReferralsTrackingVO) iter.next();
                //Begin the transaction.
                broker.beginTransaction();
                broker.delete(assignPatientsVO);
                broker.commitTransaction();
                
            }
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in AssignPatientsDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in AssignPatientsVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in AssignPatientsDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in AssignPatientsVO.delete()", e);
        } catch (com.pixalere.common.ApplicationException e) {
            log.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving AssignPatientsDAO.delete()***********************");
    }
    /**
     * Finds a all referrals_tracking records sorted for ET's popup.  If no record is found a null value is returned.
     *
     * @param sort sorts all records
     *
     * @since 3.0
     */
    public Collection sortAllByCriteria(Integer entry_type, ProfessionalVO user, boolean filterByPosition) throws DataAccessException {
        PersistenceBroker broker = null;
        Collection<ReferralsTrackingVO> results = null;
        ReferralsTrackingVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ReferralsTrackingVO();
            AssignPatientsServiceImpl aservice = new AssignPatientsServiceImpl();
            AssignPatientsVO apatient = new AssignPatientsVO();
            apatient.setProfessionalId(user.getId());
            Collection<AssignPatientsVO> assigns = aservice.getAllAssignedPatients(apatient);
            Collection<UserAccountRegionsVO> treatment_locations = user.getRegions();
            Criteria crit = new Criteria();
            //crit.addEqualTo("entry_type",entry_type);
            crit.addEqualTo("current", "1");
            crit.addEqualTo("active", "1");
            //crit.addEqualTo("patient.account_status", "1");
            
            Criteria entry1 = new Criteria();
            if (entry_type.intValue() == Constants.REFERRAL) {
                //Get all Referrals which are assigned to all, or a your position (logged in user)
                if(Common.getConfig("allowReferralByPositions")!=null && Common.getConfig("allowReferralByPositions").equals("1") && filterByPosition){
                    Criteria assignedCrit = new Criteria();
                    Criteria assignedCrit2 = new Criteria();
                    Criteria entryAssigned = new Criteria();
                    assignedCrit.addIsNull("assigned_to");
                    //eachAssigned.addEqualTo("assigned_to","-1");//ignore assigned to.
                    entryAssigned.addOrCriteria(assignedCrit);
                    assignedCrit2.addEqualTo("assigned_to",user.getPosition_id());//ignore assigned to.
                    entryAssigned.addOrCriteria(assignedCrit2);
                    crit.addAndCriteria(entryAssigned);
                }
                Criteria t = new Criteria();
                t.addEqualTo("entry_type", entry_type);
                entry1.addOrCriteria(t);
                Criteria t2 = new Criteria();
                t2.addEqualTo("entry_type", Constants.MESSAGE_FROM_NURSE);
                entry1.addOrCriteria(t2);
            } else if (entry_type.intValue() == Constants.RECOMMENDATION) {
                Criteria t = new Criteria();
                t.addEqualTo("entry_type", entry_type);
                entry1.addOrCriteria(t);
                Criteria t2 = new Criteria();
                t2.addEqualTo("entry_type", Constants.MESSAGE_FROM_CLINICIAN);
                entry1.addOrCriteria(t2);
            }
            crit.addAndCriteria(entry1);
            /*Criteria tmpCrit2=new  Criteria();
            Criteria t = new Criteria();
            t.addEqualToField("patient_id", Criteria.PARENT_QUERY_PREFIX + "patient_id"); 
            t.addEqualToField("current","1");
            t.addEqualToField("account_status","1");
            tmpCrit2.addAndCriteria(t);*/
            Criteria treatCrit = new Criteria();
            for (UserAccountRegionsVO treatmentLocation : treatment_locations) {
                Criteria tmpCrit = new Criteria();
                if(user.getTraining_flag() == 1){
                    
                    if(treatmentLocation.getTreatment_location_id() == Constants.TRAINING_TREATMENT_ID){
                        tmpCrit.addEqualTo("patient.treatment_location_id", treatmentLocation.getTreatment_location_id());
                        treatCrit.addOrCriteria(tmpCrit);
                    }
                }else{
                    if(!treatmentLocation.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)){
                        tmpCrit.addEqualTo("patient.treatment_location_id", treatmentLocation.getTreatment_location_id());
                        treatCrit.addOrCriteria(tmpCrit);
                    }
                }
            }
            if(Common.getConfig("applyPopupToAssigned")!=null && Common.getConfig("applyPopupToAssigned").equals("1")){

                //Criteria assignCrit = new Criteria();
                for(AssignPatientsVO p : assigns){
                    Criteria tmpACrit = new Criteria();
                    tmpACrit.addEqualTo("patient.patient_id",p.getPatientId());
                    treatCrit.addOrCriteria(tmpACrit);
                }
            }
           
            crit.addAndCriteria(treatCrit);
            
            //crit.addOrCriteria(tmpCrit2);
            QueryByCriteria query = QueryFactory.newQuery(ReferralsTrackingVO.class, crit);
            query.addOrderByDescending("patient.treatment_location_id");
            query.addOrderByDescending("patient.patient_id");
            query.setStartAtIndex(1);
            query.setEndAtIndex(250);
            
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } catch (ApplicationException e) {
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all referrals_tracking records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(ReferralsTrackingVO tmpVO) throws DataAccessException {
        PersistenceBroker broker = null;
        Collection results = null;
        ReferralsTrackingVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ReferralsTrackingVO();
            QueryByCriteria query = new QueryByCriteria(tmpVO);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }
/**
     * Finds a all auto_referrals records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param autoReferrals fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 5.1
     */
    public Collection findAllAutoReferralsByCriteria(AutoReferralVO tmpVO) throws DataAccessException {
        PersistenceBroker broker = null;
        Collection results = null;
        AutoReferralVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AutoReferralVO();
            QueryByCriteria query = new QueryByCriteria(tmpVO);
            query.addOrderByDescending("id");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ProductsDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ProductsDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ProductsDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds an referrals_tracking record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 4.1.1
     */
    public ValueObject findByCriteria(ReferralsTrackingVO referralsVO, Date timestamp) throws DataAccessException {
        log.info("***********************Entering ReferralsTrackingDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        ReferralsTrackingVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //QueryByCriteria query = new QueryByCriteria(referralsVO);
            //crit -- set order by time_stamp desc
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            crit.addEqualTo("current_flag", referralsVO.getCurrent());
            crit.addEqualTo("wound_profile_type_id", referralsVO.getWound_profile_type_id());
            if (referralsVO.getProfessional_id() != null) {
                crit.addEqualTo("professional_id", referralsVO.getProfessional_id());
            }
            crit.addEqualTo("entry_type", referralsVO.getEntry_type());
            if (referralsVO.getAssessment_id() != null) {
                crit.addEqualTo("assessment_id", referralsVO.getAssessment_id());
            }
            if (timestamp != null) {
                crit.addGreaterOrEqualThan("created_on", PDate.subtractDays(timestamp , 30));
            }
            query = QueryFactory.newQuery(ReferralsTrackingVO.class, crit);
            returnVO = (ReferralsTrackingVO) broker.getObjectByQuery(query);
            log.info("ReferralsTrackingDAO.findByCriteria(): return:" + returnVO);
        } catch (ConnectionLocatorException e) {
            e.printStackTrace();
            log.error("ServiceLocatorException thrown in ReferralsTrackingDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ReferralsTrackingDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ReferralsTrackingDAO.findAllByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds an referrals_tracking record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(ReferralsTrackingVO referralsVO) throws DataAccessException {
        log.info("***********************Entering ReferralsTrackingDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        ReferralsTrackingVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(referralsVO);
            
            query.addOrderByDescending("created_on");
            returnVO = (ReferralsTrackingVO) broker.getObjectByQuery(query);
            log.info("ReferralsTrackingDAO.findByCriteria(): return:" + returnVO);
        } catch (ConnectionLocatorException e) {
            e.printStackTrace();
            log.error("ServiceLocatorException thrown in ReferralsTrackingDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ReferralsTrackingDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ReferralsTrackingDAO.findAllByCriteria()***********************");
        return returnVO;
    }
/**
     * Finds an referrals_tracking record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 5.1
     */
    public ValueObject findAutoReferralByCriteria(AutoReferralVO referralsVO) throws DataAccessException {
        log.info("***********************Entering ReferralsTrackingDAO.findAutoReferralByCriteria()***********************");
        PersistenceBroker broker = null;
        AutoReferralVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(referralsVO);
            //crit -- set order by time_stamp desc
            returnVO = (AutoReferralVO) broker.getObjectByQuery(query);
            log.info("ReferralsTrackingDAO.findByCriteria(): return:" + returnVO);
        } catch (ConnectionLocatorException e) {
            e.printStackTrace();
            log.error("ServiceLocatorException thrown in ReferralsTrackingDAO.findAutoReferralByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ReferralsTrackingDAO.findAutoReferralByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ReferralsTrackingDAO.findAutoReferralByCriteria()***********************");
        return returnVO;
    }
    /**
     * Finds a single referrals_tracking record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the referrals_tracking table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        log.info("***********************Entering ReferralsTrackingDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        ReferralsTrackingVO assignPatientsVO = new ReferralsTrackingVO();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assignPatientsVO.setAssessment_id(new Integer(primaryKey));
            assignPatientsVO.setCurrent(new Integer(1));
            Query query = new QueryByCriteria(assignPatientsVO);
            log.debug("ReferralsTrackingDAO.findByPK(): " + query.toString() + " :::: Value=" + primaryKey);
            assignPatientsVO = (ReferralsTrackingVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReferralsTrackingDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in ReferralsTrackingDAO.findByPK()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ReferralsTrackingDAO.findByPK()***********************");
        return assignPatientsVO;
    }
    public void update(ValueObject obj) throws DataAccessException {
        log.info("***********************Entering ReferralsTrackingDAO.update()***********************");
        ReferralsTrackingVO assignPatientsVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            assignPatientsVO = (ReferralsTrackingVO) obj;
            broker.beginTransaction();
            broker.store(assignPatientsVO);
            broker.commitTransaction();
            
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReferralsTrackingDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ReferralsTrackingDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ReferralsTrackingDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ReferralsTrackingDAO.insert()", e);
        } catch (com.pixalere.common.ApplicationException e) {
            log.error("DataAccessException thrown in ReferralsTrackingDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            //
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving AssignPatientsDAO.update()***********************");
    }
    /**
     * Save the auto_referral
     * 
     * @since 5.1
     * @param obj
     * @throws DataAccessException
     */
    public void updateAutoReferral(ValueObject obj) throws DataAccessException {
        log.info("***********************Entering ReferralsTrackingDAO.updateAutoReferral()***********************");
        AutoReferralVO vo = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            vo = (AutoReferralVO) obj;
            broker.beginTransaction();
            broker.store(vo);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReferralsTrackingDAO.updateAutoReferral(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ReferralsTrackingDAO.updateAutoReferral()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ReferralsTrackingDAO.updateAutoReferral(): " + e.toString());
            throw new DataAccessException("Error in ReferralsTrackingDAO.updateAutoReferral()", e);
        } catch (com.pixalere.common.ApplicationException e) {
            log.error("DataAccessException thrown in ReferralsTrackingDAO.updateAutoReferral(): " + e.toString(), e);
        } catch (Exception ex) {
            //
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ReferralsTrackingDAO.update()***********************");
    }
    public void update(ValueObject updateRecord, int patient_id) throws DataAccessException {
        log.info("***********************Entering ReferralsTrackingDAO.update()***********************");
        ReferralsTrackingVO refVO = null;
        PersistenceBroker broker = null;
        PatientServiceImpl pd = new PatientServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            refVO = (ReferralsTrackingVO) updateRecord;
            
            PatientAccountVO patient = pd.getPatient(patient_id);
            refVO.setPatient_id(patient_id);
            refVO.setPatient_account_id(patient.getId());
            broker.beginTransaction();
            broker.store(refVO);
            broker.commitTransaction();
            
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReferralsTrackingDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ReferralsTrackingDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ReferralsTrackingDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ReferralsTrackingDAO.insert()", e);
        } catch (com.pixalere.common.ApplicationException e) {
            log.error("DataAccessException thrown in ReferralsTrackingDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            //
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ReferralsTrackingDAO.update()***********************");
    }
}
