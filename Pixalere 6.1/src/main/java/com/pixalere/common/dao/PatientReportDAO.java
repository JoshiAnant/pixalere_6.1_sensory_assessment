/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.dao;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.PatientReportVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.bean.TableUpdatesVO;
import java.util.Collection;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.log4j.Logger;
public class PatientReportDAO implements DataAccessObject{
    static private Logger log = Logger.getLogger(PatientReportDAO.class);
    
    public PatientReportDAO(){
        
    }
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.delete()***********************");
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            PatientReportVO lib = (PatientReportVO) deleteRecord;
            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(lib);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ConfigurationDAO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ConfigurationVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.delete()***********************");
    }
    public Collection<PatientReportVO> findAllByCriteria() throws DataAccessException{
        log.info("***********************Entering configurationDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PatientReportVO> results = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            PatientReportVO pr = new PatientReportVO();
            pr.setActive(1);
            QueryByCriteria query = new QueryByCriteria(pr);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findAllByCriteria()***********************");
        return results;
    }
    public PatientReportVO findByCriteria(PatientReportVO lib) throws DataAccessException{
        log.info("***********************Entering productsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        PatientReportVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new PatientReportVO();
            Query query = new QueryByCriteria(lib);
            returnVO = (PatientReportVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in ConfigurationDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.findByCriteria()***********************");
        return returnVO;
    }
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.insert()***********************");
        PatientReportVO lib = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            lib = (PatientReportVO) insertRecord;
            broker.beginTransaction();
            broker.store(lib);
            broker.commitTransaction();
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.insert()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in ConfigurationDAO.insert()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving ConfigurationDAO.insert()***********************");
    }
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ConfigurationDAO.update()***********************");
        PatientReportVO lib = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            lib = (PatientReportVO) updateRecord;
            broker.beginTransaction();
            broker.store(lib);
            broker.commitTransaction();
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in ConfigurationDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ConfigurationDAO.update()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ConfigurationDAO.update(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ConfigurationDAO.update()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Done ConfigurationDAO.update()***********************");
    }
    public ValueObject findByPK(int dead){
        return null;
    }
}