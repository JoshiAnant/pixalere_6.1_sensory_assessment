/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common;
import org.apache.log4j.Logger;
import java.io.*;
import org.flywaydb.core.Flyway;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import javax.sql.DataSource;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import org.flywaydb.core.api.MigrationInfo;
/**
 * A servlet which starts our database migration app, and checks if the database
 * needs to be upgraded.
 *
 * @since 5.1
 * @version 5.1
 * @author travis
 */
public class DbMigration extends HttpServlet {
    // Create Log4j category instance for logging
	static private Logger log = org.apache.log4j.Logger.getLogger(DbMigration.class);
    public void init() throws ServletException {
        String DATASOURCE_CONTEXT = "java:comp/env/jdbc/pixDS";
        System.out.println("initializing DbMIGRATION");
        Connection result = null;
        try {
            Context initialContext = new InitialContext();
            if (initialContext == null) {
                System.out.println("JNDI problem. Cannot get InitialContext.");
            }
            DataSource datasource = (DataSource) initialContext.lookup(DATASOURCE_CONTEXT);
            if (datasource != null) {
                result = datasource.getConnection();
                Flyway flyway = new Flyway();
                flyway.setDataSource(datasource);
                //flyway.setLocations("com.pixalere.migration");
                //flyway.setInitOnMigrate(true);
                flyway.isBaselineOnMigrate();
                
                //flyway.repair();
                flyway.setValidateOnMigrate(false);
		for (MigrationInfo i : flyway.info().all()) {
			log.info("migrate task: " + i.getVersion() + " : "
					+ i.getDescription() + " from file: " + i.getScript());
                        System.out.println("migrate task: " + i.getVersion() + " : "
					+ i.getDescription() + " from file: " + i.getScript());
		}
                flyway.migrate();
                
                System.out.println("We're Migrating!!!!");
            } else {
                System.out.println("Failed to lookup datasource.");
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
            System.out.println("Cannot get connection: " + ex);
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Cannot get connection: " + ex);
        }
        
    }
    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}
