/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common;

import com.pixalere.common.bean.ConfigurationVO;
import java.io.IOException;
import java.util.Date;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.*;
import com.pixalere.common.client.*;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.TimeZone;
import com.pixalere.common.service.ConfigurationServiceImpl;

/**
 *
 * @author travis
 */
public class TimeZoneFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        //setup remote services.
        try{
            ConfigurationServiceImpl cservice = new ConfigurationServiceImpl();
            ConfigurationVO configTimezone = new ConfigurationVO();
            configTimezone.setConfig_setting("timezone");
            ConfigurationVO currentTimezone = cservice.getConfiguration(configTimezone);
            
            if (currentTimezone != null) {
                TimeZone.setDefault(TimeZone.getTimeZone(currentTimezone.getCurrent_value()));
            }
            chain.doFilter(req, res);
        }catch(ApplicationException e){e.printStackTrace();}
    }

    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
        //add code to release any resource
    }
}
