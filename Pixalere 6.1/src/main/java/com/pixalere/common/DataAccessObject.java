package com.pixalere.common;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public interface DataAccessObject {
    public ValueObject findByPK(int primaryKey) throws DataAccessException;
    public void update(ValueObject updateRecord) throws DataAccessException;
    public void delete(ValueObject deleteRecord) throws DataAccessException;// refactor later..
    
}
