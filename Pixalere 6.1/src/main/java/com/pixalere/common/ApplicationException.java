package com.pixalere.common;
import org.apache.commons.lang.exception.NestableException;

public class ApplicationException extends NestableException {
    Throwable exceptionCause = null;
    
    /** Creates a new instance of ApplicationException */
    public ApplicationException(String msg) {
        super(msg);
    }
    
    public ApplicationException(String msg, Throwable exception){
      super(msg, exception);   
      exceptionCause = exception;
    }
    
    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
      if (exceptionCause!=null){
        System.err.println("An exception has been caused by: " + exceptionCause.toString());
        exceptionCause.printStackTrace();
      }
    }
}
