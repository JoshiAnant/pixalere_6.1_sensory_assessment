/*
 * Created on Dec 12, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.pixalere.common;
import org.apache.ojb.broker.PBFactoryException;
import org.apache.ojb.broker.PBKey;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerFactory;
/**
 * @author travdes
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConnectionLocator {
    private static ConnectionLocator locatorRef = null;
    static {
        locatorRef = new ConnectionLocator();
    }
    public ConnectionLocator() {
    }
    public static ConnectionLocator getInstance() {
        return locatorRef;
    }
    public PersistenceBroker findBroker() throws ConnectionLocatorException {
        PersistenceBroker broker = null;
        try {
            PBKey pbKey = new PBKey("live");
            broker = PersistenceBrokerFactory.createPersistenceBroker(pbKey);
            
        } catch (PBFactoryException e) {
            e.printStackTrace();
            throw new ConnectionLocatorException(
                    "PBFactoryException error occurred while parsing the repository.xml file in ServiceLocator constructor: -",
                    e);
        }
        return broker;
    }
}
