package com.pixalere.common;
import com.pixalere.common.bean.LookupVO;
import java.io.Serializable;
public abstract class ArrayValueObject extends Object implements Serializable {
    public ArrayValueObject() {
    }
    private LookupVO lookup;
    private Integer id;//oJb requires an id for object caching.. otherwise its not really needed.
    private Integer resource_id;
    private Integer lookup_id;
    private String other;
    public LookupVO getLookup() {
        return lookup;
    }
    public void setLookup(LookupVO lookup) {
        this.lookup = lookup;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getLookup_id() {
        return lookup_id;
    }
    public void setLookup_id(Integer lookup_id) {
        this.lookup_id = lookup_id;
    }
 /**
     * @return the resource_id
     */
    public Integer getResource_id() {
        return resource_id;
    }
    /**
     * @param resource_id the resource_id to set
     */
    public void setResource_id(Integer resource_id) {
        this.resource_id = resource_id;
    }
    /**
     * @return the other
     */
    public String getOther() {
        return other;
    }
    /**
     * @param other the other to set
     */
    public void setOther(String other) {
        this.other = other;
    }
}
