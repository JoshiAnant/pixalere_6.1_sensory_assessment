package com.pixalere.common.service;
import com.pixalere.utils.Constants;
import com.pixalere.common.*;
import com.pixalere.common.bean.ProductCategoryVO;
import java.util.Collection;
import com.pixalere.common.dao.*;
import javax.jws.WebService;
/**
 *     This is the product category service implementation
 *     Refer to {@link com.pixalere.common.service.ProductCategoryService } to see if there are
 *     any web services available.
 *
 *     <img src="ProductCategoryServiceImpl.png"/>
 *
 *
 *     @view  ProductCategoryServiceImpl
 *
 *     @match class com.pixalere.common.*
 *     @opt hide
 *     @match class com.pixalere.common\.(dao.ProductCategoryDAO|bean.ProductCategoryVO|service.ProductCategoryService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.common.service.ProductCategoryService", serviceName = "ProductCategoryService")
public class ProductCategoryServiceImpl implements ProductCategoryService{
	// Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductCategoryServiceImpl.class);
	
	
    public ProductCategoryServiceImpl(){
        
    }
    
	public void saveProductCategory(ProductCategoryVO lookupVO) throws ApplicationException{
		try{
		ProductCategoryDAO listDAO=new ProductCategoryDAO();
		listDAO.insert(lookupVO);
		}
		catch(DataAccessException e){
		  log.error("Error in ProductCategoryBD.addItem: " + e.toString());
		  throw new ApplicationException("Error in ProductCategoryBD.addItem: " + e.toString(),e);	
		}
	}
	
	public void removeProductCategory(ProductCategoryVO lookupVO) throws ApplicationException{
		try{
		ProductCategoryDAO listDAO=new ProductCategoryDAO();
		lookupVO.setActive(new Integer(0));
		listDAO.update(lookupVO);
		}
		catch(DataAccessException e){
		  log.error("Error in ProductCategoryBD.removeItem: " + e.toString());
		  throw new ApplicationException("Error in ProductCategoryBD.removeItem: " + e.toString(),e);	
		}
	}
	public ProductCategoryVO getProductCategory(int  primaryKey) throws ApplicationException{
    		try{
    		   ProductCategoryDAO listDAO = new ProductCategoryDAO();
    		   return (ProductCategoryVO) listDAO.findByPK(primaryKey);
    		}
    		catch (DataAccessException e){
    		  throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(),e);
    		}	
    	}
        public ProductCategoryVO getProductCategory(ProductCategoryVO cat) throws ApplicationException{
    		try{
    		   ProductCategoryDAO listDAO = new ProductCategoryDAO();
    		   return (ProductCategoryVO) listDAO.findByCriteria(cat);
    		}
    		catch (DataAccessException e){
    		  throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(),e);
    		}
    	}
	public Collection getAllProductCategoriesByCriteria(ProductCategoryVO vo) throws ApplicationException{
		try{
			ProductCategoryDAO productsDAO=new ProductCategoryDAO();
			return (Collection)productsDAO.findAllByCriteria(vo);
			
		}catch(DataAccessException e){
                    throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
		}
	}
        //@todo legacy method, need to remove in 6.1
        public Collection getAllProductCategories() throws ApplicationException{
		try{
			ProductCategoryDAO productsDAO=new ProductCategoryDAO();
                        ProductCategoryVO vo =new ProductCategoryVO();
			vo.setActive(new Integer(1));
			return (Collection)productsDAO.findAllByCriteria(vo);
			
		}catch(DataAccessException e){
                    throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
		}
	}
        
}
