package com.pixalere.common.service;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Constants;
import com.pixalere.common.*;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ProductCategoryVO;
import java.util.List;
import java.util.ArrayList;
import com.pixalere.common.dao.*;
import java.util.Collection;
import javax.jws.WebService;

import com.pixalere.common.bean.ProductsByFacilityVO;
 /**
 *     This is the product service implementation
 *     Refer to {@link com.pixalere.common.service.ProductsService } to see if there are
 *     any web services available.
 *
 *     <img src="ProductsServiceImpl.png"/>
 *
 *
 *     @view  ProductsServiceImpl
 *
 *     @match class com.pixalere.common.*
 *     @opt hide
 *     @match class com.pixalere.common\.(dao.ProductsDAO|bean.ProductsVO|service.ProductsService)
 *     @opt !hide
 *
*/

@WebService(endpointInterface = "com.pixalere.common.service.ProductsService", serviceName = "ProductsService")
public class ProductsServiceImpl implements ProductsService{
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductsServiceImpl.class);
    
    public ProductsServiceImpl(){
    }



    public Collection getAllProductsWithURL() throws ApplicationException{
        try{
            ProductsDAO productsDAO=new ProductsDAO();
            return (Collection)productsDAO.findAllWithURL();
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
  

   
    public Collection getAllProducts() throws ApplicationException{

        try{
            ProductsDAO productsDAO=new ProductsDAO();

            ProductsVO vo = new ProductsVO();

            return (Collection)productsDAO.findAllByCriteria(vo,null);

        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }

    }
    public Collection getAllProducts(ProfessionalVO prof) throws ApplicationException{
        try{
            ProductsDAO productsDAO=new ProductsDAO();
            ProductsVO vo=new ProductsVO();
            vo.setActive(new Integer(1));
            return (Collection)productsDAO.findAllByCriteria(vo,prof);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
        
    }
    public Collection getAllProductsWithCategories(ProfessionalVO prof) throws ApplicationException{
        List<ProductsVO> newProds = new ArrayList();
        try{
            ProductsDAO productsDAO=new ProductsDAO();
            ProductCategoryDAO catDAO = new ProductCategoryDAO();
            
            ProductCategoryVO voc = new ProductCategoryVO();
            voc.setActive(new Integer(1));
            Collection<ProductCategoryVO> cats = (Collection<ProductCategoryVO>)catDAO.findAllByCriteria(voc);
            
            for(ProductCategoryVO p : cats){
                ProductsVO t = new ProductsVO();
                t.setActive(p.getActive());
                t.setTitle(p.getTitle());
                t.setType("isTitle");
                newProds.add(t);
                //get prods for this category
                ProductsVO vo=new ProductsVO();
                vo.setActive(new Integer(1));
                vo.setCategory_id(p.getId());
                Collection<ProductsVO> products =  (Collection<ProductsVO>)productsDAO.findAllByCriteria(vo,prof);
                for(ProductsVO pp : products){
                    newProds.add(pp);
                }
                
            }
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
        return newProds;
    }
    public Collection getAllProducts(ProductsVO pvo , ProfessionalVO prof) throws ApplicationException{
        try{
            ProductsDAO productsDAO=new ProductsDAO();
            
            return (Collection)productsDAO.findAllByCriteria(pvo,prof);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
        
    }
    /** @deprecated 
     * 
     * @param vo
     * @return
     * @throws ApplicationException 
     */
    public Collection getAllProductsByCriteria(ProductsVO vo) throws ApplicationException{
        try{
            ProductsDAO productsDAO=new ProductsDAO();
            return (Collection)productsDAO.findAllByCriteria(vo,null);

        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }

    }
    public Collection getAllProducts(String category,ProfessionalVO prof) throws ApplicationException {
        try{
            ProductsDAO productsDAO=new ProductsDAO();
            ProductsVO vo=new ProductsVO();
            vo.setActive(new Integer(1));
            if(!category.equals("all")) {
                vo.setCategory_id(new Integer(category));
            }
            return (Collection)productsDAO.findAllByCriteria(vo,prof);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public void saveProduct(ProductsVO lookupVO) throws ApplicationException{
        try{
            ProductsDAO listDAO=new ProductsDAO();
            listDAO.insert(lookupVO);
        } catch(DataAccessException e){
            log.error("Error in ProductsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.addItem: " + e.toString(),e);
        }
    }
    public void saveProductsByFacility(ProductsByFacilityVO lookupVO) throws ApplicationException{
        try{
            ProductsDAO listDAO=new ProductsDAO();
            listDAO.insertProductsByFacility(lookupVO);
        } catch(DataAccessException e){
            log.error("Error in ProductsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.addItem: " + e.toString(),e);
        }
    }
    public void removeProduct(ProductsVO lookupVO) throws ApplicationException{
        try{
            ProductsDAO listDAO=new ProductsDAO();
            lookupVO.setActive(new Integer(0));
            listDAO.update(lookupVO);
        } catch(DataAccessException e){
            log.error("Error in ProductsBD.removeItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.removeItem: " + e.toString(),e);
        }
    }
    public void removeProductsByFacility(ProductsByFacilityVO lookupVO) throws ApplicationException{
        try{
            ProductsDAO listDAO=new ProductsDAO();
            listDAO.deleteProductsByFacility(lookupVO);
        } catch(DataAccessException e){
            log.error("Error in ProductsBD.removeItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.removeItem: " + e.toString(),e);
        }
    }
        public ProductsVO getProduct(ProductsVO prod) throws ApplicationException{
        try{
            ProductsDAO listDAO = new ProductsDAO();
            return (ProductsVO) listDAO.findByCriteria(prod);
        } catch (DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(),e);
        }
    }
    public ProductsVO getProduct(int primaryKey) throws ApplicationException{
        try{
            ProductsDAO listDAO = new ProductsDAO();
            return (ProductsVO) listDAO.findByPK(primaryKey);
        } catch (DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(),e);
        }
    }
}
