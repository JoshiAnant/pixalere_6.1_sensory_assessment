/*
 * Copyright (c) 2005 WebMed Technology Inc. All Rights Reserved.
 */
package com.pixalere.common.service;

import com.pixalere.common.bean.LanguageVO;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.utils.Constants;
import com.pixalere.admin.bean.ResourcesVO;
import java.util.ArrayList;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.admin.dao.*;
import com.pixalere.common.*;
import java.util.Vector;
import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import com.pixalere.auth.bean.ProfessionalVO;
import javax.jws.WebService;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.utils.Common;

/**
 *     This is the assign patients service implementation
 *     Refer to {@link com.pixalere.common.service.ListService } to see if there are
 *     any web services available.
 *     <img src="ListServiceImpl.png"/>
 *
 *     @view ListServiceImpl
 *
 *     @match class com.pixalere.common.*
 *     @opt hide
 *     @match class com.pixalere.common\.(dao.ListsDAO|bean.LookupVO|service.ListService)
 *     @opt !hide
 *
 */
@WebService(endpointInterface = "com.pixalere.common.service.ListService", serviceName = "ListService")
public class ListServiceImpl implements ListService {
    // Create Log4j category instance for logging

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ListServiceImpl.class);
    private Integer language=1;
    public ListServiceImpl(Integer language) {
        this.language=language;
    }
    public ListServiceImpl() {
    }
    /**
     * Inserts the latest listdata record. All components in the interface
     * allow an admin user to insert new resources to be displayed in the interface.
     *
     * @param LookupVO a resource object
     * @see LookupVO
     */
    public void saveListItem(LookupVO lookupVO) throws ApplicationException {
        try {
        	
            ListsDAO listDAO = new ListsDAO();

            if (lookupVO.getId() != null) {
                LookupVO t = new LookupVO();
                t.setId(lookupVO.getId());
                LookupVO old = (LookupVO) listDAO.findByCriteria(t);
                if (old != null) {
                    if(lookupVO.getResourceId().equals(new Integer(51))  || old.getName(language).equals(lookupVO.getName(language)) ){
                        listDAO.update(lookupVO);
                    } else {
                        old.setActive(0);
                        listDAO.update(old);//save old one (hide)
                        lookupVO.setId(null);//set to null to force insert.
                        listDAO.update(lookupVO);//
                    }
                }
                
            }else if(lookupVO.getId() == null){
                listDAO.update(lookupVO);//
            }
        } catch (DataAccessException e) {
            log.error("Error in ListBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ListBD.addItem: " + e.toString(), e);
        }
    }
/**
     * Inserts the latest lookup_l10n record. Manage Resources in the interface
     * allow an admin user to insert new resources to be displayed in the interface.
     *
     * @param LookupLocalizationVO a resource object
     * @see LookupLocalizationVO
     */
    public void saveLocalization(LookupLocalizationVO lookupVO) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();
            lookupVO.setId(null);//set to null to force insert.


            listDAO.updateLocalization(lookupVO);//
        } catch (DataAccessException e) {
            log.error("Error in ListBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ListBD.addItem: " + e.toString(), e);
        }
    }
    public void saveLanguage(LanguageVO lookupVO) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();
            lookupVO.setId(null);//set to null to force insert.


            listDAO.updateLanguage(lookupVO);//
        } catch (DataAccessException e) {
            log.error("Error in ListBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ListBD.addItem: " + e.toString(), e);
        }
    }
    public void saveListItemForOffline(LookupVO lookupVO) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();
            listDAO.update(lookupVO);//
        } catch (DataAccessException e) {
            log.error("Error in ListBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ListBD.addItem: " + e.toString(), e);
        }
    }

    /**
     * This method updates the LookupVO record in the database, and sets the
     * status = 0.
     *
     * @param LookupVO assigned LookupVO criteria
     * @see LookupVO
     */
    public void removeListItem(LookupVO lookupVO) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();
            //lookupVO.setActive(new Integer(0));
            LookupVO list = (LookupVO) listDAO.findByCriteria(lookupVO);
            if (list != null) {
                list.setActive(new Integer(0));
                listDAO.update(list);
            }
        } catch (DataAccessException e) {
            log.error("Error in ListBD.removeItem: " + e.toString());
            throw new ApplicationException("Error in ListBD.removeItem: " + e.toString(), e);
        }
    }
    public Vector findAllTrainingTreatments(ProfessionalVO professionalVO) throws ApplicationException {
        Vector results = new Vector();
        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        try {
            ListsDAO listdao = new ListsDAO();
            LookupVO critCatVO = new LookupVO();
            critCatVO.setResourceId(critCatVO.TREATMENT_CATEGORY_INT);
            
            critCatVO.setId(Constants.TRAINING_TREATMENT_CATEGORY_ID);//training category ID;
            
            Collection collCat = listdao.findAllByCriteria(critCatVO);
            Iterator iterCat = collCat.iterator();
            while (iterCat.hasNext()) {
                LookupVO catVO = (LookupVO) iterCat.next();
                boolean blnCatAdded = false;

                LookupVO critLocVO = new LookupVO();
                critLocVO.setCategoryId(catVO.getId());
                Collection collLoc = listdao.findAllByCriteria(critLocVO);
                Iterator iterLoc = collLoc.iterator();
                while (iterLoc.hasNext()) {
                    
                    LookupVO locVO = (LookupVO) iterLoc.next();
                    if (professionalVO == null || pservice.validate(professionalVO.getRegions(), locVO.getId()) == true || professionalVO.getAccess_superadmin().equals(new Integer(1))) {
                        if (!blnCatAdded) {
                            catVO.setTitle(new Integer(1));
                            results.add(catVO);//adding category to LookupVO Array
                            blnCatAdded = true;
                        }
                        results.add(locVO);
                    }
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.findAllTreatmentsWithCats: " + e.toString(), e);
        }
        return results;
    }
    public Vector findAllTreatmentsWithCats(ProfessionalVO professionalVO) throws ApplicationException {
        Vector results = new Vector();
        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        try {
            ListsDAO listdao = new ListsDAO();
            LookupVO critCatVO = new LookupVO();
            critCatVO.setResourceId(critCatVO.TREATMENT_CATEGORY_INT);
            Collection collCat = listdao.findAllByCriteria(critCatVO);
            Iterator iterCat = collCat.iterator();
            while (iterCat.hasNext()) {
                
                LookupVO catVO = (LookupVO) iterCat.next();
                if(!catVO.getId().equals(Constants.TRAINING_TREATMENT_CATEGORY_ID)){
                    boolean blnCatAdded = false;

                    LookupVO critLocVO = new LookupVO();
                    critLocVO.setCategoryId(catVO.getId());
                    Collection collLoc = listdao.findAllByCriteria(critLocVO);
                    Iterator iterLoc = collLoc.iterator();
                    while (iterLoc.hasNext()) {

                        LookupVO locVO = (LookupVO) iterLoc.next();
                        if (professionalVO == null || pservice.validate(professionalVO.getRegions(), locVO.getId()) == true || professionalVO.getAccess_superadmin().equals(new Integer(1))) {
                            if (!blnCatAdded) {
                                catVO.setTitle(new Integer(1));
                                results.add(catVO);//adding category to LookupVO Array
                                blnCatAdded = true;
                            }
                            results.add(locVO);
                        }
                    }
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.findAllTreatmentsWithCats: " + e.toString(), e);
        }
        return results;
    }

    /**
     * Returns a Vector of Lists with Resources
     * <p>
     * This method always returns immediately, whether or not the
     * vector is empty.  The method is meant to be called with multiple resource_id's. A
     * good exmaple of this is treatment_categories.  Only shows the treatments the Professional is currently
     * assigned to.  This Vector of lists returns a list of both Categories (if true) and Secondary lists.
     *
     * @param  array an array of resource_ids
     * @param ProfessionalVO the logged in User.
     * @param includeCategory if the Resource category should be included in the list.
     * @return      Vector a vector of LookupVO objects
     */
    public Vector getLists(int[] array, ProfessionalVO userVO, boolean includeCategory) throws ApplicationException {
        Vector<LookupVO> results = new Vector<LookupVO>();
        ResourcesDAO resBD = new ResourcesDAO();
        ListsDAO listdao = new ListsDAO();
        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        for (int x : array) {
            try {
                ResourcesVO tmpVO = new ResourcesVO();
                LookupVO fakeVO = new LookupVO();
                LookupVO vo = new LookupVO();
                if (includeCategory == true) {
                    tmpVO.setResourceId(new Integer(x));
                    ResourcesVO resVO = (ResourcesVO) resBD.findByCriteria(tmpVO);
                    Collection<LookupLocalizationVO> lColl = new ArrayList();
                    
                    if(resVO != null){
                        fakeVO.setId(resVO.getResourceId());
                        String resourceKey = resVO.getReport_value_label_key();
                        //System.out.println(resourceKey+" "+resVO.getReport_value_label_key());
                        // Get all the existent languages using the bundle key
                        lColl = Common.getLocalizationFromBundleKey(resourceKey);
                    }else{
                        LookupVO tm = new LookupVO();
                        tm.setCategoryId(new Integer(x));
                        LookupVO t = (LookupVO)listdao.findByCriteria(tm);
                        fakeVO.setId(t.getId());
                        
                        //FIXME: Which data should be here? Empty when is not found?
                        // No data in resources, take it from lookup
                        LookupLocalizationVO l = new LookupLocalizationVO();
                        // l.setName(resVO.getName());  // Commented out, resVO is null here
                        l.setLanguage_id(Common.IDLANG_EN); //English.. should add to constants or make configurable
                        lColl.add(l);
                    }
                    fakeVO.setResourceId(new Integer(0));//new Integer(array[x]));
                    fakeVO.setTitle(new Integer(1));
                    
                    fakeVO.setNames(lColl);
                    results.add(fakeVO);//adding category to LookupVO Array
                }
                vo.setResourceId(new Integer(x));//getting all lists by category

                Collection<LookupVO> c = listdao.findAllByCriteria(vo);

                for (LookupVO tmper : c) {
                    if (x != vo.TREATMENT_LOCATION_INT || userVO == null || (x == vo.TREATMENT_LOCATION_INT && pservice.validate(userVO.getRegions(), tmper.getId()) == true) || userVO.getAccess_superadmin().equals(new Integer(1))) {
                        results.add(tmper);
                    }
                }
            } catch (DataAccessException e) {
                log.error("Error in ListBD.findAllListItems: " + e.toString());
                throw new ApplicationException("DataAccessException Error in PatientManagerBD.findAllResourcesWithCateories(): " + e.toString(), e);
            }

        }
        return results;

    }

    /**
     * Returns a Collection of all of the Lists
     * <p>
     * This method always returns immediately, whether or not the
     * collection is empty.    and Secondary lists.
     *
     * @return      Collection  a collection of LookupVO objects
     */
    public Collection getAllLists() throws ApplicationException {
        try {
            ListsDAO dao = new ListsDAO();
            LookupVO lookupVO = new LookupVO();

            return (Collection) dao.findAllByCriteria(lookupVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
    /**
     * Returns a Collection of all of the Languages
     * <p>
     * This method always returns immediately, whether or not the
     * collection is empty.    and Secondary lists.
     *
     * @return      Collection  a collection of LanguagesVO objects
     */
    public Collection getLanguages() throws ApplicationException {
        try {
            ListsDAO dao = new ListsDAO();
            LanguageVO lookupVO = new LanguageVO();

            return (Collection) dao.findLanguagesByCriteria(lookupVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
    /**
     * Returns a Collection of all of the Languages
     * <p>
     * This method always returns immediately, whether or not the
     * collection is empty.    and Secondary lists.
     *
     * @return      Collection  a collection of LanguagesVO objects
     */
    public LanguageVO getLanguage(int id) throws ApplicationException {
        try {
            ListsDAO dao = new ListsDAO();
            LanguageVO lookupVO = new LanguageVO();
            lookupVO.setId(id);

            return dao.findLanguageByCriteria(lookupVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
    /**
     * Returns a Collection of all of the Lists
     * <p>
     * This method always returns immediately, whether or not the
     * collection is empty.    and Secondary lists.
     *
     * @return      Collection  a collection of LookupVO objects
     */
    public Collection<LookupVO> getAllListsForOffline() throws ApplicationException {
        try {
            ListsDAO dao = new ListsDAO();
            LookupVO lookupVO = new LookupVO();
            Collection<LookupVO> result = dao.findAllIncludingInactiveByCriteria(lookupVO);
            return result;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }

    public Collection getListsWithInactive(LookupVO listVO) throws ApplicationException {
        try {
            ListsDAO dao = new ListsDAO();
            return (Collection) dao.findAllIncludingInactiveByCriteria(listVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }

    /**
     * Returns a Collection of Lists
     * <p>
     * This method always returns immediately, whether or not the
     * collection is empty.    and Secondary lists.
     *
     * @param resource_id the Resource category
     * @return      Collection  a collection of LookupVO objects
     */
    public Collection getLists(int resource_id) throws ApplicationException {
        try {
        	
        	ListsDAO dao = new ListsDAO();
            LookupVO lookupVO = new LookupVO();
            lookupVO.setResourceId(new Integer(resource_id));
          
            return (Collection) dao.findAllByCriteria(lookupVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
        
    }

    /**
     * Returns a ListData object
     * <p>
     * This method always returns immediately, whether or not the
     * object is null. .
     *
     * @param  primaryKey the listdata primarykey
     * @return      LookupVO a listData object contain the primary key result.
     */
    public LookupVO getListItem(int primaryKey) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();
            LookupVO lookupVO = (LookupVO) listDAO.findByPK(primaryKey);
            return lookupVO;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveItem(): " + e.toString(), e);
        }
    }

    public LookupVO getListItem(String strValue) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();
            LookupVO vo = new LookupVO();
            //vo.setName(strValue);
            LookupLocalizationVO l = new LookupLocalizationVO();
            l.setName(strValue);
            l.setLanguage_id(Common.IDLANG_EN);//English.. should add to constants or make configurable
            LookupLocalizationVO loco = (LookupLocalizationVO)listDAO.findByCriteria(l);
            //Collection<LookupLocalizationVO> lColl = new ArrayList();
            //lColl.add(l);
            //vo.setNames(lColl);
            //vo.setActive(1);
            if(loco!=null){
                LookupVO lookup = new LookupVO();
                lookup.setId(loco.getLookup_id());
                return (LookupVO)listDAO.findByCriteria(lookup);
            }
            return null;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveItemByValue(): " + e.toString(), e);
        }
    }

    public LookupVO getListItem(LookupVO list) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();

            return (LookupVO) listDAO.findByCriteria(list);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveItemByValue(): " + e.toString(), e);
        }
    }
    public LookupLocalizationVO getLocalization(LookupLocalizationVO list) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();

            return (LookupLocalizationVO) listDAO.findByCriteria(list);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveItemByValue(): " + e.toString(), e);
        }
    }
    public Collection getLocalizations(LookupLocalizationVO list) throws ApplicationException {
        try {
            ListsDAO listDAO = new ListsDAO();

            return (Collection) listDAO.findAllByCriteria(list);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveItemByValue(): " + e.toString(), e);
        }
    }
    public List<LookupVO> filterList(Collection<LookupVO> list, Integer resource_id) throws ApplicationException {
        List<LookupVO> returnList = new ArrayList();
        try {
            for (LookupVO item : list) {
                if (item.getResourceId().equals(resource_id)) {
                    returnList.add(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnList;
    }
    /**
     * Returns a Collection of Lists
     * <p>
     * This method always returns immediately, whether or not the
     * collection is empty.    and Secondary lists.
     *
     * @param resource_ids the Resource categorys
     * @return      Collection  a collection of LookupVO objects
     */
    public Collection getLists(List<Integer> resource_ids,boolean isActive)throws ApplicationException{
        try{
            ListsDAO dao = new ListsDAO();


            return (Collection) dao.findAllByCriteria(resource_ids,isActive);
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(),e);
        }
    }
}
