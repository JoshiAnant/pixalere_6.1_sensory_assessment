/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.service;
import javax.servlet.ServletContext;
import com.verhas.licensor.License;
import com.pixalere.common.bean.LicenseVO;
import java.io.InputStream;
import com.pixalere.utils.Constants;
import com.pixalere.common.dao.LicenseDAO;
import com.pixalere.common.*;
/**
 *
 * @author travis
 */
public class LicenseServiceImpl {
    public LicenseServiceImpl() {
    }
    public void saveLicense(String license) {
        try {
            LicenseDAO dao = new LicenseDAO();
            LicenseVO lic = new LicenseVO();
            lic.setLicense(license);
            lic.setId(1);
            dao.insert(lic);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getNumUsers() {
        return getString("num_user");
    }
    public String getNumFacilities() {
        return getString("num_facilities");
    }
    public String getValidUntil() {
        return getString("valid_until");
    }
    public String getString(String key) {
        try {
            LicenseVO license = getLicense();
            License license2 = new License();
            license2.loadKeyRingFromResource("pubring.gpg", Constants.DIGEST);
            license2.setLicenseEncoded(license.getLicense());
            
            /*if (license2.isVerified()) {
            request.setAttribute("license_verify", license2.getLicenseString() );
            }*/
            return license2.getFeature(key);
        } catch (Exception e) {
            e.printStackTrace();
            //throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(), e);
            return "";
        }
        
        //String edition = lic.getFeature("edition");
        //String issue_date = lic.getFeature("valid-until");
        //String users = lic.getFeature("num-users");
    }
    public LicenseVO getLicense() throws ApplicationException {
        try {
            LicenseDAO dao = new LicenseDAO();
            LicenseVO license = (LicenseVO) dao.findByID();
            return license;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }
}
