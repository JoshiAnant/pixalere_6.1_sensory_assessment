/*
 * GUIManagerBD.java
 *
 * Created on June 13, 2007, 8:13 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.common.service;
import com.pixalere.utils.Constants;
import com.pixalere.common.*;
import com.pixalere.common.bean.InformationPopupVO;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.bean.QuickTipsVO;
import com.pixalere.common.dao.ComponentsDAO;
import java.util.Collection;
/**
 *
 * @author travismorris
 */
import javax.jws.WebService;
/**
 *     This is the gui/components service implementation
 *     Refer to {@link com.pixalere.common.service.GUIService } to see if there are
 *     any web services available.
 *
 *     <img src="GUIServiceImpl.png"/>
 *
 *
 *     @view  GUIServiceImpl
 *
 *     @match class com.pixalere.common.*
 *     @opt hide
 *     @match class com.pixalere.common\.(dao.ComponentsDAO|bean.InformationPopupVO|bean.ComponentsVO|service.GUIService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.common.service.GUIService", serviceName = "GUIService")
public class GUIServiceImpl implements GUIService{
    
static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(GUIServiceImpl.class);
    
    public GUIServiceImpl(){
        
    }
    
    public void saveComponent(ComponentsVO lookupVO) throws ApplicationException{
        try{
            ComponentsDAO listDAO=new ComponentsDAO();
            listDAO.insert(lookupVO);
        } catch(DataAccessException e){
            log.error("Error in ComponentsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ComponentsBD.addItem: " + e.toString(),e);
        }
    }
    public void saveTips(QuickTipsVO t) throws ApplicationException{
        try{
            ComponentsDAO listDAO=new ComponentsDAO();
            listDAO.insertTips(t);
        } catch(DataAccessException e){
            log.error("Error in ComponentsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ComponentsBD.addItem: " + e.toString(),e);
        }
    }
    public Collection getAllComponents() throws ApplicationException{
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            ComponentsVO vo=new ComponentsVO();
            
            return (Collection)componentsDAO.findAllByCriteria(vo);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
        
    }
    public Collection getAllInformationPopups() throws ApplicationException{
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            InformationPopupVO vo=new InformationPopupVO();
            
            return (Collection)componentsDAO.findAllByCriteria(vo);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
        
    }
    public ComponentsVO getComponent(int field) throws ApplicationException {
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            ComponentsVO c = new ComponentsVO();
            c.setId(new Integer(field));
            ComponentsVO v= (ComponentsVO)componentsDAO.findByCriteria(c);
            return v;
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    /*
     * Return the component with the field_name passed in.  Return NULL if the record
     * doesn't exist.
     * 
     * @param field the field_name of the component record
     * @return ComponentsVO
     */
    public ComponentsVO getComponent(String field_name, Integer flowchart) throws ApplicationException {
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            ComponentsVO c = new ComponentsVO();
            c.setField_name(field_name);
            c.setFlowchart(flowchart);
            ComponentsVO v= (ComponentsVO)componentsDAO.findByCriteria(c);
            return v;
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public ComponentsVO getComponent(ComponentsVO obj) throws ApplicationException {
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            
            return (ComponentsVO)componentsDAO.findByCriteria(obj);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public InformationPopupVO getInformationPopup(InformationPopupVO obj) throws ApplicationException {
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            
            return (InformationPopupVO)componentsDAO.findInfoPopup(obj);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public Collection getAllComponents(ComponentsVO obj) throws ApplicationException {
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            return (Collection)componentsDAO.findAllByCriteria(obj);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public Collection getAllTips(String page) throws ApplicationException {
        try{
            ComponentsDAO componentsDAO=new ComponentsDAO();
            
            return (Collection)componentsDAO.findAllByCriteria(page);
            
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public Collection getAllComponents(int[] pages) throws ApplicationException {
        try{
        	System.out.println("in getallcomponents int[] called");
            ComponentsDAO componentsDAO=new ComponentsDAO();
            return (Collection)componentsDAO.findAllByCriteria(pages);
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public void saveInformationPopup(InformationPopupVO lookupVO) throws ApplicationException{
      
        try{
            ComponentsDAO listDAO=new ComponentsDAO();
            listDAO.insertInfoPopup(lookupVO);
            
        } catch(DataAccessException e){
            log.error("Error in ProductsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.addItem: " + e.toString(),e);
        }
        
    }
    public void removeInfoPopup(int id) throws ApplicationException{
      
        try{
            ComponentsDAO listDAO=new ComponentsDAO();
            InformationPopupVO p = new InformationPopupVO();
            p.setId(id);
            listDAO.deleteInfoPopup(p);
            
        } catch(DataAccessException e){
            log.error("Error in ProductsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.addItem: " + e.toString(),e);
        }
        
    }
    
}