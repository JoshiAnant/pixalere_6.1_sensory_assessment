package com.pixalere.common.service;
import com.pixalere.common.*;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.common.dao.ConfigurationDAO;
import com.pixalere.common.bean.ConfigurationVO;
import com.pixalere.common.ConfigurationListVO;
import com.pixalere.admin.bean.VendorVO;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.List;
import java.util.Collections;
import java.util.Vector;
import java.util.Hashtable;
import javax.jws.WebService;
import java.util.TimeZone;
import com.pixalere.utils.Constants;
/**
 *     This is the configuration service implementation
 *     Refer to {@link com.pixalere.configuration.service.ConfigurationService } to see if there are
 *     any web services available.
 *
 *     <img src="ConfigurationServiceImpl.png"/>
 *
 *
 *     @view  ConfigurationServiceImpl
 *
 *     @match class com.pixalere.common.*
 *     @opt hide
 *     @match class com.pixalere.common\.(dao.ConfigurationDAO|bean.ConfigurationVO|service.ConfigurationService)
 *     @opt !hide
 *
 */
@WebService(endpointInterface = "com.pixalere.common.service.ConfigurationService", serviceName = "ConfigurationService")
public class ConfigurationServiceImpl implements ConfigurationService {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConfigurationServiceImpl.class);
    public ConfigurationServiceImpl() {
    }
    public List<ConfigurationVO> getAllConfigurations() throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            ConfigurationVO configurationVO = new ConfigurationVO();
            return (List<ConfigurationVO>) configurationDAO.findAllByCriteria(configurationVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }
    public List<VendorVO> getAllVendors() throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            VendorVO configurationVO = new VendorVO();
            return (List<VendorVO>) configurationDAO.findAllByCriteria(configurationVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }
    public boolean checkRule(String strRule) throws ApplicationException {
        boolean blnResult = true;
        try {
            if (strRule != null && strRule.trim().length() > 0) {
                boolean blnMustEqual = true;
                String strSetting = "";
                String strMatch = "";
                if (strRule.indexOf("==") != -1) {
                    strSetting = strRule.substring(0, strRule.indexOf("=="));
                    strMatch = strRule.substring(strRule.indexOf("==") + 2);
                } else if (strRule.indexOf("!=") != -1) {
                    strSetting = strRule.substring(0, strRule.indexOf("!="));
                    strMatch = strRule.substring(strRule.indexOf("==") + 2);
                    blnMustEqual = false;
                } else if (strRule.indexOf("=") != -1) {
                    strSetting = strRule.substring(0, strRule.indexOf("="));
                    strMatch = strRule.substring(strRule.indexOf("=") + 1);
                }
                // Remove invalid characters
                String strMatchClean = "";
                for (int i = 0; i < strMatch.length(); i++) {
                    if (strMatch.charAt(i) + "" != "'" && strMatch.charAt(i) + "" != "\"") {
                        strMatchClean = strMatchClean + strMatch.charAt(i);
                    }
                }
                ConfigurationDAO configurationDAO = new ConfigurationDAO();
                ConfigurationVO configurationTMPVO = new ConfigurationVO();
                configurationTMPVO.setConfig_setting(strSetting);
                configurationTMPVO = configurationDAO.findByCriteria(configurationTMPVO);
                if (blnMustEqual && !configurationTMPVO.getCurrent_value().equalsIgnoreCase(strMatchClean)
                        || !blnMustEqual && configurationTMPVO.getCurrent_value().equalsIgnoreCase(strMatchClean)) {
                    blnResult = false;
                }
            }
        } catch (Exception e) {
            //throw new ApplicationException("ConfigurationManagerBD threw an error: " + e.getMessage(), e);
        }
        return blnResult;
    }
    public Collection retrieveAllForAdmin(Integer intUserID) throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            ConfigurationVO configurationVO = new ConfigurationVO();
            if (intUserID != 7) {
                configurationVO.setSelfadmin(1);
            }
          
            Collection<ConfigurationVO> configCollection = configurationDAO.findAllByCriteria(configurationVO);
            Vector vecResults = new Vector();
            for (ConfigurationVO configVO : configCollection) {
                try {
                    ConfigurationListVO configurationListVO = new ConfigurationListVO();
                    String strDisabled = "";
                    if (configVO.getEnable_component_rule() != null && configVO.getEnable_component_rule() != "") {
                        String[] arrRules = configVO.getEnable_component_rule().split(",");
                        for (int i = 0; i < arrRules.length; i++) {
                            if (!checkRule(arrRules[i])) {
                                strDisabled = "disabled";
                            }
                        }
                    }
                    configurationListVO.setId(configVO.getId());
                    configurationListVO.setComponent_disabled(strDisabled);
                    configurationListVO.setSelfadmin(configVO.getSelfadmin());
                    configurationListVO.setConfig_setting(configVO.getConfig_setting());
                    configurationListVO.setComponent_type(configVO.getComponent_type());
                    configurationListVO.setReadonly(configVO.getReadonly());
                    configurationListVO.setCategory(configVO.getCategory());
                    String[] arrOptions = configVO.getComponent_options().split(",");
                    String[] arrValues = configVO.getComponent_values().split(",");
                    boolean blnRulesSet = false;
                    String[] arrRules = null;
                    if (configVO.getEnable_options_rule() != null && configVO.getEnable_options_rule().trim() != "") {
                        arrRules = configVO.getEnable_options_rule().split(",");
                        blnRulesSet = arrRules.length >= arrOptions.length;
                    }
                    for (int i = 0; i < arrOptions.length; i++) {
                        boolean blnEnabled = true;
                        if (blnRulesSet) {
                            blnEnabled = checkRule(arrRules[i]);
                        }
                        if (blnEnabled) {
                            String strOption = arrOptions[i];
                            String strValue = "";
                            String strSelected = "";
                            if (strOption.indexOf("pixalere.") != -1) {
                                strOption = Common.getLocalizedString(strOption,"en");
                            }
                            if (arrValues.length != arrOptions.length) {
                                strValue = strOption;
                            } else {
                                strValue = arrValues[i];
                            }
                            if (strValue.equalsIgnoreCase(configVO.getCurrent_value())) {
                                strSelected = " selected";
                            }
                            arrOptions[i] = "<option value='" + strValue + "'" + strSelected + ">" + strOption + "</option>";
                        } else {
                            arrOptions[i] = "";
                        }
                    }
                    configurationListVO.setComponent_options(arrOptions);
                    configurationListVO.setCurrent_value(configVO.getCurrent_value());
                    configurationListVO.setTitle(Common.getLocalizedString("pixalere.config.title." + configVO.getConfig_setting(),"en"));
                    configurationListVO.setDescription(Common.getLocalizedString("pixalere.config." + configVO.getConfig_setting(),"en"));
                    vecResults.add(configurationListVO);
                } catch (Exception e) {
                    throw new ApplicationException("UploadService threw an error: " + e.getMessage(), e);
                }
            }
            
            return (vecResults);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }
    public Hashtable getConfiguration() throws ApplicationException {
        Hashtable config = new Hashtable();
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            ConfigurationVO configurationVO = new ConfigurationVO();
            Collection<ConfigurationVO> configCollection = configurationDAO.findAllByCriteria(configurationVO);
            for (ConfigurationVO configVO : configCollection) {
                
                try {
                    if (configVO.getComponent_type() != "CAT") {
                        config.put(configVO.getConfig_setting(), configVO.getCurrent_value());
                    }
                } catch (Exception e) {
                    //log.error("retrieveConfiguration threw an error: " + e.getMessage());
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveConfiguration(): " + e.toString(), e);
        }
        return (config);
    }
    public ConfigurationVO getConfiguration(ConfigurationVO field) throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
           
            ConfigurationVO results = (ConfigurationVO) configurationDAO.findByCriteria(field);
            return results;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }
    public VendorVO getVendor(VendorVO field) throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
           
            VendorVO results = (VendorVO) configurationDAO.findByCriteria(field);
            return results;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }
    public ConfigurationVO getConfiguration(int field) throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            ConfigurationVO configurationVO = new ConfigurationVO();
            configurationVO.setId(new Integer(field));
            ConfigurationVO results = (ConfigurationVO) configurationDAO.findByCriteria(configurationVO);
            return results;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }
    public void saveConfiguration(ConfigurationVO configurationVO) throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            configurationDAO.insert(configurationVO);
        } catch (DataAccessException e) {
            log.error("Error in ConfigurationManagerBD.save: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(), e);
        }
    }
    public void saveVendor(VendorVO configurationVO) throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            configurationDAO.updateVendor(configurationVO);
        } catch (DataAccessException e) {
            log.error("Error in ConfigurationManagerBD.save: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(), e);
        }
    }
    public void saveTableUpdates(TableUpdatesVO table) throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            configurationDAO.updateTableUpdates(table);
        } catch (DataAccessException e) {
            log.error("Error in ConfigurationManagerBD.saveTableUpdate: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.saveTableUpdate: " + e.toString(), e);
        }
    }
    /**
     * Offline requires the server's timezone so we can ensure we're maintaining the same timezone on the client side.
     * @return TimeZone object (server's timezone)
     * @throws ApplicationException 
     */
    public String getServerTimeZone()throws ApplicationException{
        try{
            TimeZone timezone = TimeZone.getDefault();
            if(timezone!=null){
                return timezone.getID();
            }
            return null;
        }catch(Exception e){
            log.error("Error in ConfigurationManagerBD.getServerTimeZone: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.getServerTimeZone: " + e.toString(), e);
        }
    }
    public TableUpdatesVO getTableUpdates() throws ApplicationException {
        try {
            ConfigurationDAO configurationDAO = new ConfigurationDAO();
            TableUpdatesVO t = new TableUpdatesVO();
            t.setId(new Integer(1));
            TableUpdatesVO results = (TableUpdatesVO) configurationDAO.findTableUpdatesByCriteria(t);
            return results;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveTableUpdates(): " + e.toString(), e);
        } catch (Exception e){
            //ignore
            return null;
        }
    }
}
