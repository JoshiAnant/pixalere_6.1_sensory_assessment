/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.service;
import com.pixalere.common.*;
import com.pixalere.common.dao.LibraryDAO;
import com.pixalere.common.bean.LibraryVO;
import com.pixalere.common.bean.LibraryCategoryVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Vector;
import java.util.Hashtable;
import javax.jws.WebService;
import com.pixalere.utils.Constants;
import java.io.File;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
/**
 *     This is the library service implementation
 *     Refer to {@link com.pixalere.common.service.LibraryService } to see if there are
 *     any web services available.
 *
 *     <img src="LibraryServiceImpl.png"/>
 *
 *
 *     @view  LibraryServiceImpl
 *
 *     @match class com.pixalere.common.*
 *     @opt hide
 *     @match class com.pixalere.common\.(dao.LibraryDAO|bean.LibraryVO|service.LibraryService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.common.service.LibraryService", serviceName = "LibraryService")
public class LibraryServiceImpl implements LibraryService{
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LibraryServiceImpl.class);
    
    public LibraryServiceImpl(){
        
    }
    public Vector<LibraryVO> getAllPdfFiles() throws ApplicationException {
        Collection<LibraryVO> lib = null;
        Vector files = new Vector();
        try {
            LibraryDAO configurationDAO=new LibraryDAO();
            LibraryVO configurationVO=new LibraryVO();
            lib = (Collection)configurationDAO.findAllByCriteria(configurationVO);
            for (LibraryVO i : lib) {
                String filename = i.getPdf();

                if (!filename.equals("")) {
                    File f = new File(Common.getDownloadsPath() +"pdfs/" + filename);
                    if(f.exists() == true){
                        DataSource ds = new FileDataSource(new File(Common.getDownloadsPath() + "pdfs/" + filename));
                        i.setPdfFile(new DataHandler(ds));
                        files.add(i);
                    }
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
        return files;
    }
    public Collection getAllLibraries(int category) throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            LibraryVO configurationVO=new LibraryVO();
            if(category != 0 ){
                configurationVO.setCategory_id(category);
            }
            return (Collection)configurationDAO.findAllByCriteria(configurationVO);
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public Collection getAllCategories() throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            LibraryCategoryVO configurationVO=new LibraryCategoryVO();
            return (Collection)configurationDAO.findAllCategoriesByCriteria(configurationVO);
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public LibraryCategoryVO getCategory(LibraryCategoryVO cat) throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            
            return (LibraryCategoryVO)configurationDAO.findCategoryByCriteria(cat);
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public LibraryVO getLibrary(Integer id) throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            LibraryVO l = new LibraryVO();
            l.setId(id);
            return (LibraryVO)configurationDAO.findByCriteria(l);
        }catch(DataAccessException e){
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(),e);
        }
    }
    public void saveLibrary(LibraryVO configurationVO) throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            configurationDAO.insert(configurationVO);
        } catch(DataAccessException e){
            log.error("Error in ConfigurationManagerBD.save: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(),e);
        }
    }
    public void removeLibrary(LibraryVO configurationVO) throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            configurationDAO.delete(configurationVO);
        } catch(DataAccessException e){
            log.error("Error in ConfigurationManagerBD.save: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(),e);
        }
    }
    public void saveLibraryCategory(LibraryCategoryVO configurationVO) throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            configurationDAO.insertCategory(configurationVO);
        } catch(DataAccessException e){
            log.error("Error in ConfigurationManagerBD.save: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(),e);
        }
    }
    public void removeLibraryCategory(LibraryCategoryVO configurationVO) throws ApplicationException{
        try{
            LibraryDAO configurationDAO=new LibraryDAO();
            configurationDAO.deleteCategory(configurationVO);
        } catch(DataAccessException e){
            log.error("Error in ConfigurationManagerBD.save: " + e.toString());
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(),e);
        }
    }
}
