/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.service;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.bean.TriggerVO;
import com.pixalere.common.bean.TriggerCriteriaVO;
import com.pixalere.common.bean.TriggerProductsVO;
import com.pixalere.common.dao.TriggerDAO;
import java.util.Collection;
import javax.jws.WebService;
import java.util.List;
import java.util.ArrayList;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.guibeans.trigger.ProductTrigger;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.utils.Common;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * This is the trigger service implementation Refer to {@link com.pixalere.common.service.TriggerService
 * } to see if there are any web services available.
 *
 * <img src="TriggerServiceImpl.png"/>
 *
 *
 * @view TriggerServiceImpl
 *
 * @match class com.pixalere.common.
 *
 * @opt hide
 * @match class
 * com.pixalere.common\.(dao.TriggerDAO|bean.TriggerVO|service.TriggerService)
 * @opt !hide
 *
 */
@WebService(endpointInterface = "com.pixalere.common.service.TriggerService", serviceName = "TriggerService")
public class TriggerServiceImpl implements TriggerService {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TriggerServiceImpl.class);

    public TriggerServiceImpl() {
    }

    /**
     * This method returns all existing triggers.
     *
     * @return triggers a Collection of triggers (all)
     * @throws ApplicationException
     */
    public Collection<TriggerVO> getAllTriggers() throws ApplicationException {
        try {
            TriggerDAO trigDAO = new TriggerDAO();
            return (Collection<TriggerVO>) trigDAO.findAllByCriteria();

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in TriggerServieImpl.getAllTriggers(): " + e.toString(), e);
        }

    }
    /**
     * This method returns all existing trigger products.
     * 
     * @param product_id the products that is currently used by triggers.
     * @return triggers a Collection of products (all)
     * @throws ApplicationException
     */
    public Collection<TriggerProductsVO> getTriggerProducts(int product_id) throws ApplicationException {
        try {
            TriggerDAO trigDAO = new TriggerDAO();
            TriggerProductsVO t = new TriggerProductsVO();
            t.setProduct_id(product_id);
            return (Collection<TriggerProductsVO>) trigDAO.findAllProductTriggersByProduct(product_id);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in TriggerServieImpl.getAllTriggers(): " + e.toString(), e);
        }

    }
    
    /**
     * @param old_product_id the ID of an edited (old) product related to a trigger
     * 
     * @param new_product_id the ID of the updated (new) product related to a trigger
     * 
     * @return triggers a collection of all triggers for a given product
     * 
     * @throws ApplicationException
     */
    public void updateAllProductTriggers(int old_product_id, int new_product_id) throws ApplicationException {
        try {
            TriggerDAO trigDAO = new TriggerDAO();
            Collection<TriggerProductsVO> list = (Collection<TriggerProductsVO>) trigDAO.findAllProductTriggersByProduct(old_product_id);
            
            if (list != null && list.size() > 0){
                System.out.println("TriggerProducts found: " + list.size());
                for (TriggerProductsVO vo : list) {
                    vo.setProduct_id(new_product_id);
                    saveTriggerProducts(vo);
                }
            } else {
                System.out.println("NO TriggerProducts found!");
            }

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in TriggerServieImpl.updateAllProductTriggers(): " + e.toString(), e);
        }

    }

    /**
     * This method returns all existing triggers.
     *
     * @return triggers a Collection of triggers (all)
     * @throws ApplicationException
     */
    public TriggerVO getTrigger(int trigger_id) throws ApplicationException {
        try {
            TriggerDAO trigDAO = new TriggerDAO();
            return (TriggerVO) trigDAO.findByPK(trigger_id);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in TriggerServieImpl.getAllTriggers(): " + e.toString(), e);
        }

    }
/**
     * This method returns all existing triggers.
     *
     * @return triggers a Collection of triggers (all)
     * @throws ApplicationException
     */
    public TriggerVO getTrigger(TriggerVO t) throws ApplicationException {
        try {
            TriggerDAO trigDAO = new TriggerDAO();
            return (TriggerVO) trigDAO.findByCriteria(t);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in TriggerServieImpl.getAllTriggers(): " + e.toString(), e);
        }

    }
    /**
     * @param trigger_id the ID of the trigger we're retrieving all the options
     * for
     * @return trigger_criterias a collection of all trigger options for a given
     * trigger
     * @throws ApplicationException
     */
    public Collection<TriggerCriteriaVO> getAllOptions(int trigger_id) throws ApplicationException {
        try {
            TriggerDAO trigDAO = new TriggerDAO();
            return (Collection<TriggerCriteriaVO>) trigDAO.findAllByCriteria(trigger_id);

        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in TriggerServieImpl.getAllTriggers(): " + e.toString(), e);
        }

    }
    
    /**
     * Deleting the Trigger object
     *
     * @param triggger the object
     */
    public void deleteTrigger(TriggerVO trig) throws ApplicationException {
        try {
            TriggerDAO listDAO = new TriggerDAO();
            listDAO.delete(trig);
        } catch (DataAccessException e) {
            log.error("Error in TriggerServiceImpl.deleteTrigger: " + e.toString());
            throw new ApplicationException("Error in TriggerServiceImpl.deleteTrigger: " + e.toString(), e);
        }
    }

    /**
     * Saving the Trigger object
     *
     * @param triggger the object
     */
    public void saveTrigger(TriggerVO trig) throws ApplicationException {
        try {
            TriggerDAO listDAO = new TriggerDAO();
            listDAO.update(trig);
        } catch (DataAccessException e) {
            log.error("Error in ProductsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.addItem: " + e.toString(), e);
        }
    }
    /**
     * Saving the TriggerProducts object
     *
     * @param triggger the object
     */
    public void saveTriggerProducts(TriggerProductsVO trig) throws ApplicationException {
        try {
            TriggerDAO listDAO = new TriggerDAO();
            listDAO.insertProduct(trig);
        } catch (DataAccessException e) {
            log.error("Error in ProductsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.addItem: " + e.toString(), e);
        }
    }
    /**
     * Delete the TriggerProducts object
     *
     * @param triggger the object
     */
    public void deleteTriggerProducts(TriggerProductsVO trig) throws ApplicationException {
        try {
            TriggerDAO listDAO = new TriggerDAO();
            listDAO.deleteProduct(trig);
        } catch (DataAccessException e) {
            log.error("Error in TriggerProductsBD.deleteItem: " + e.toString());
            throw new ApplicationException("Error in TriggerProductsBD.deleteItem: " + e.toString(), e);
        }
    }
    /**
     * Saving the Trigger object
     *
     * @param triggger the object
     */
    public void saveTriggerOption(TriggerCriteriaVO trig) throws ApplicationException {
        try {
            TriggerDAO listDAO = new TriggerDAO();
            listDAO.insertOption(trig);
        } catch (DataAccessException e) {
            log.error("Error in ProductsBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductsBD.addItem: " + e.toString(), e);
        }
    }


    /**
     * Delete the TriggerCriteria object
     *
     * @param triggger the object
     */
    public void deleteTriggerCriteria(TriggerCriteriaVO criteria) throws ApplicationException {
        try {
            TriggerDAO listDAO = new TriggerDAO();
            listDAO.deleteCriteria(criteria);
        } catch (DataAccessException e) {
            log.error("Error in TriggerCriteriaBD.deleteItem: " + e.toString());
            throw new ApplicationException("Error in TriggerCriteriaBD.deleteItem: " + e.toString(), e);
        }
    }
    
    // List of recommendations
    private List<ProductTrigger> recommendations;

    /**
     * Returns a list of ProductTrigger objects by going over the collected
     * triggers id obtained during a previous processing of one or more 
     * assessments.
     * 
     * @param woundName
     * @return a list of ProductTrigger objects
     * @throws com.pixalere.common.ApplicationException
     */
    public List<ProductTrigger> getRecommendations(String woundName) throws ApplicationException {
        if (recommendations == null){
            recommendations = new ArrayList<ProductTrigger>();
        }
        if (appliedTriggers == null) {
            appliedTriggers = new HashMap<Integer, Boolean>();
        }
        
        try {
            ProductsServiceImpl pservice = new ProductsServiceImpl();
            List<Integer> globalProductsIds = new ArrayList<Integer>();
            
            for (Map.Entry<Integer, Boolean> entry : appliedTriggers.entrySet()) {
                Integer trigger_id = entry.getKey();
                Boolean applyTrigger = entry.getValue();
                
                if (applyTrigger){
                    TriggerVO trigger = getTrigger(trigger_id);

                    ProductTrigger recommendation = new ProductTrigger();
                    recommendation.setSummary(trigger.getDescription());
                    recommendation.setTrigger_id(trigger_id);
                    // TOFIX: Maybe setting the same name for all is not OK, how can we differentiate?
                    recommendation.setWound_name(woundName);

                    for(TriggerProductsVO product_id : trigger.getProduct_ids()){

                        // If it has not been added to any recommendations
                        if (!globalProductsIds.contains(product_id.getProduct_id())){
                            ProductsVO product = pservice.getProduct(product_id.getProduct_id());

                            if (recommendation.getProducts() == null){
                                recommendation.setProducts(new ArrayList<ProductsVO>());
                            }

                            recommendation.getProducts().add(product);

                            globalProductsIds.add(product_id.getProduct_id());
                        }                    
                    } // Finished product loop

                    // Add this recommendation only if has one or more products
                    if (recommendation.getProducts()!= null && !recommendation.getProducts().isEmpty()){
                        recommendations.add(recommendation);
                        System.out.println("Recommendation Triggered! - " 
                                + recommendation.getSummary() 
                                + " for: " + recommendation.getWound_name() 
                                + " with: " + recommendation.getProducts().size() 
                                + " product(s)");
                    }
                }
            }
        } catch (ApplicationException e) {
            log.error("Error in TriggerServiceImpl.getRecommendations: " + e.toString());
            throw new ApplicationException("Error in TriggerServiceimpl.getRecommendations:" + e.getMessage());
        }
        
        return recommendations;
    }

    public void setRecommendations(List<ProductTrigger> recommendations) {
        this.recommendations = recommendations;
    }

    //List of triggers validated
    private Map<Integer, Boolean> appliedTriggers;
    public Map<Integer, Boolean>  getAppliedTriggers(){return appliedTriggers;}
    
    /**
     * 
     * @param allCSValueResults
     * @param allCCACQuestionsValueResults
     * @throws com.pixalere.common.ApplicationException
     */
    public void processTreatmentTriggers(Vector allCSValueResults, Vector allCCACQuestionsValueResults, String alpha) throws ApplicationException {
        if (appliedTriggers == null) {
            appliedTriggers = new HashMap<Integer, Boolean>();
        }
        
        try {
            TriggerDAO listDAO = new TriggerDAO();
            GUIServiceImpl cservice = new GUIServiceImpl();
            ProductsServiceImpl pservice = new ProductsServiceImpl();
            AssessmentServiceImpl aservice = new AssessmentServiceImpl();
            Collection<TriggerVO> triggers = listDAO.findAllByCriteria(alpha);
            
            for (TriggerVO trigger : triggers) {
                // Has this trigger been evaluated before
                boolean evaluated = appliedTriggers.containsKey(trigger.getTrigger_id());
                
                boolean triggerApplies = evaluated
                        ? appliedTriggers.get(trigger.getTrigger_id())
                        : false;
                Collection<TriggerCriteriaVO> options = trigger.getOptions();
                
                // Global AND
                boolean andOperator = trigger.getLogic_operator() == null || trigger.getLogic_operator().equals("AND");
                
                // Should we process the trigger again?
                boolean process = true;
                if (evaluated) {
                    boolean prevOutcome = appliedTriggers.get(trigger.getTrigger_id());
                    process = (andOperator)     // If AND global
                            ? prevOutcome       // If previously was false, makes not sense to evaluate
                            : true;             // OR global: We process anyway
                }
                
                if (process){
                    // Map: component_id --> condition outcome (grouping of conditions)
                    Map<Integer, Boolean> validations = new HashMap<Integer, Boolean>();
                    
                    for (TriggerCriteriaVO option : options) {
                        boolean showProduct = false;
                        ComponentsVO component = cservice.getComponent(option.getComponent_id());

                        if (component != null) {
                            // Restrict to only Treatment options
                            if (component.getField_name().equals("ccac_questions")){
                                for (Object object : allCCACQuestionsValueResults) {
                                    String value = (String) object;
                                    int pos = value.indexOf(":");
                                    String lookupId = (pos > 0)
                                            ? value.substring(pos + 1).trim()
                                            : "";
                                    
                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = (lookupId.equals(option.getCriteria().trim()))? true : showProduct;
                                    } else {
                                        showProduct = (!lookupId.equals(option.getCriteria().trim()))? true : showProduct;
                                    }
                                }
                            } else if (component.getField_name().equals("csresult")) {
                                for (Object object : allCSValueResults) {
                                    String value = (String) object;
                                    int pos = value.indexOf(":");
                                    String lookupId = (pos > 0)
                                            ? value.substring(pos + 1).trim()
                                            : "";
                                    
                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = (lookupId.equals(option.getCriteria().trim()))? true : showProduct;
                                    } else {
                                        showProduct = (!lookupId.equals(option.getCriteria().trim()))? true : showProduct;
                                    }
                                }   
                            }
                        }

                        if (andOperator){
                            // Only register the type of conditions that apply here
                            if (component != null 
                                    && (component.getField_name().equals("ccac_questions") 
                                    || component.getField_name().equals("csresult"))
                                    ){
                                if (validations.containsKey(option.getComponent_id())){
                                    // OR-accumulate the validation associated to this component id
                                    // Note: Heal rate will always be component_id = 0
                                    // So we are OR-grouping also the heal rates.
                                    boolean groupValid = validations.get(option.getComponent_id());
                                    groupValid =  (showProduct)? true : groupValid;
                                    validations.put(option.getComponent_id(), groupValid);
                                } else {
                                    validations.put(option.getComponent_id(), showProduct);
                                }
                            }
                                
                        } else {
                            // At least one condition applies? Then set to true, otherwise keep value
                            triggerApplies = (showProduct)? true : triggerApplies;
                        }
                        
                    } // end options loop
                    
                    if (andOperator){
                        // If validations contains one (or more) false value(s), the trigger is not valid
                        if (!validations.isEmpty()){
                            triggerApplies = !validations.containsValue(false);
                        }
                    }
                }
                    
                // If evaluated
                if (evaluated) {
                    boolean prevOutcome = appliedTriggers.get(trigger.getTrigger_id());
                    triggerApplies = (andOperator)     // If AND global
                            ? (triggerApplies && prevOutcome)  // AND operator
                            : triggerApplies || prevOutcome;   // OR operator
                }
                
                appliedTriggers.put(trigger.getTrigger_id(), triggerApplies);
            }
            
        } catch (DataAccessException e) {
            log.error("Error in TriggerServiceImpl.processTreatmentTriggers: " + e.toString());
            throw new ApplicationException("Error in TriggerServiceimpl.processTreatmentTriggers:" + e.getMessage());
        }
    }
    
    /**
     * We're going to process the product triggers based on the trigger options
     * found in the trigger_criteria table. products and recommendation fields
     * will be populated and accessible via their respective methods.
     *
     * @param profile the current patient profile
     * @param wound  the current wound
     * @param ass  the current assessment
     * @param alpha the care_type we're searching for.
     * @param nptw the current nptw assessment
     * @throws com.pixalere.common.ApplicationException
     */
    public void processProductTriggers(PatientProfileVO profile, WoundProfileVO wound, AbstractAssessmentVO ass, String alpha, AssessmentNPWTVO nptw) throws ApplicationException {
        
        if (appliedTriggers == null) {
            appliedTriggers = new HashMap<Integer, Boolean>();
        }
        
        Object assess = ass;
        
        // We need to identify when the fields are in the assessment_nptw table
        List<String> nptwFields = Arrays.asList(
                                        // DB       type    resource?   CRITERIA
                "np_connector",         // int      INTEGER  w-resource lookup
                "pressure_reading",     // int      INTEGER  w-resource lookup
                "therapy_setting",      // int      INTEGER  w-resource lookup
                "initiated_by",         // int      INTEGER  w-resource lookup
                "serial_num",           // varchar  STRING no resource  integer
                "kci_num",              // varchar  STRING no resource  integer
                "reason_for_ending",    // int      INTEGER  w-resource lookup
                "goal_of_therapy",      // int      INTEGER  w-resource lookup
                "vendor_name",          // int      INTEGER  w-resource lookup
                "machine_acquirement"   // int      INTEGER  w-resource lookup
        );
        
        try {
            TriggerDAO listDAO = new TriggerDAO();
            GUIServiceImpl cservice = new GUIServiceImpl();
            Collection<TriggerVO> triggers = listDAO.findAllByCriteria(alpha);
            
            for (TriggerVO trigger : triggers) {
                List<TriggerCriteriaVO> options = new ArrayList<TriggerCriteriaVO>(trigger.getOptions());
                
                //System.out.println("\n ************* Trigger Name: "+ trigger.getName());
                // Global AND
                boolean andOperator = trigger.getLogic_operator() == null || trigger.getLogic_operator().equals("AND");
                boolean triggerApplies = false;
                
                // Map: component_id --> condition outcome (grouping of conditions)
                Map<Integer, Boolean> validations = new HashMap<Integer, Boolean>();

                for (TriggerCriteriaVO option : options) {
                    boolean showProduct = false;

                    ComponentsVO component = cservice.getComponent(option.getComponent_id());
                    //System.out.println("\n **** Condition: " + option.getDescription());

                    //System.out.println("Option type: " + option.getCriteria_type() + "  Value: " + option.getCriteria() + "  Operator: " + option.getMatch_type());
                    if (component != null && option.getCriteria_type().equals("lookup")) {
                        
                        if (component.getField_name().equals("ccac_questions") || component.getField_name().equals("csresult")) {
                            // We dont care about this two here, skip and do not change anything
                            continue;
                        }
                        
                        // First we look at the Patient Profile level (co-morbidities, factors interfering)
                        if (component.getField_name().equals("co_morbidities") && profile!=null && profile.getPatient_profile_arrays()!=null) {
                            //System.out.println("Comorbidities here");
                            // Get and filter from profile arrays
                           
                            PatientProfileArraysVO[] array = profile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[profile.getPatient_profile_arrays().size()]);
                            List<LookupVO> patient_comorbidities = Common.filterArrayObjects2Lookup(array, LookupVO.COMORBIDITIES);
                            
                            // Find if there's a match
                            Integer criteria = Integer.parseInt(option.getCriteria());
                            boolean found = false;
                            for (LookupVO lookup: patient_comorbidities) {
                                if (lookup.getId().equals(criteria)){
                                    found = true;
                                }
                            }
                            // Evaluate
                            if (option.getMatch_type().equals("=")) {
                                showProduct = found == true;
                            } else {
                                // When !=,  if there is no match the condition validates
                                showProduct = found == false;
                            }
                            
                            //System.out.println("\t **** Condition: " + option.getDescription() + "  criteria: " + criteria + "  showProduct: " + showProduct);
                        } else if (component.getField_name().equals("interfering_meds") && profile!=null && profile.getPatient_profile_arrays()!=null) {
                            //System.out.println("Factors interfering");
                            PatientProfileArraysVO[] array = profile.getPatient_profile_arrays().toArray(new PatientProfileArraysVO[profile.getPatient_profile_arrays().size()]);
                            List<ArrayValueObject> interfering_factors = Common.filterArrayObjects(array, LookupVO.INTERFERING_MEDS);
                            
                            // Find if there's a match
                            Integer criteria = Integer.parseInt(option.getCriteria());
                            boolean found = false;
                            for (ArrayValueObject vo: interfering_factors){
                                if (vo.getLookup().getId().equals(criteria)){
                                    found = true;
                                }
                            }
                            // Evaluate
                            if (option.getMatch_type().equals("=")) {
                                showProduct = found == true;
                            } else {
                                // When !=,  if there is no match the condition validates
                                showProduct = found == false;
                            }
                            //System.out.println("\t **** Condition: " + option.getDescription()  + "  criteria: " + criteria + "  showProduct: " + showProduct);
                        } else
                        // Then we look at the Wound level (etiology, goals)
                        if(component.getField_name().equals("etiology")){

                            Collection<EtiologyVO> etiologies = wound.getEtiologies();
                            for (EtiologyVO etiologyVO : etiologies) {
                                if (etiologyVO.getAlpha_id().equals(ass.getAlpha_id())){
                                    Integer etiology_id = etiologyVO.getLookup_id();
                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = etiology_id.equals(Integer.parseInt(option.getCriteria()));
                                    } else {
                                        showProduct = !etiology_id.equals(Integer.parseInt(option.getCriteria()));
                                    }
                                }
                            }


                        } else if(component.getField_name().equals("goals")){
                            Collection<GoalsVO> goals = wound.getGoals();
                            for (GoalsVO goalsVO : goals) {
                                if (goalsVO.getAlpha_id().equals(ass.getAlpha_id())){
                                    Integer goals_id = goalsVO.getLookup_id();
                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = goals_id.equals(Integer.parseInt(option.getCriteria()));
                                    } else {
                                        showProduct = !goals_id.equals(Integer.parseInt(option.getCriteria()));
                                    }
                                }
                            }
                        } else {
                            // For lookup only (= || !=) comparators
                            if (option.getMatch_type().equals("=") || option.getMatch_type().equals("!=")) {
                                if (component.getResource_id() <= 0) {
                                    if (component.getComponent_type().equals("YN")) {
                                        Object value = com.pixalere.utils.Common.getFieldValue(component.getField_name(), assess);
                                        Integer criteria = Integer.parseInt(option.getCriteria());
                                        if (value != null) {
                                            if (criteria == 2) {
                                                showProduct = ((Integer) value).equals(2);
                                            } else {
                                                // Not selected can be anything but 2
                                                showProduct = !((Integer) value).equals(2);
                                            }
                                        } else {
                                            // Null is also equivalent to not selected
                                            showProduct = criteria != 2;
                                        }

                                    } else if (component.getComponent_type().equals("AT")) {
                                        // The field we want is full_assessment
                                        Integer value = (Integer) com.pixalere.utils.Common.getFieldValue("full_assessment", assess);
                                        if (value != null) {
                                            if (option.getMatch_type().equals("=")) {
                                                showProduct = value.equals(Integer.parseInt(option.getCriteria()));
                                            } else {
                                                showProduct = !value.equals(Integer.parseInt(option.getCriteria()));
                                            }
                                        }

                                    } 

                                } else if (component.getField_type().equals("INTEGER")) {
                                    // Is a field from NPTW?
                                    if (nptwFields.contains(component.getField_name())){
                                        if (nptw != null ) {
                                            // we can only get integers here (lookups' ids)
                                            Integer value = (Integer) com.pixalere.utils.Common.getFieldValue(component.getField_name(), nptw);
                                            if (value != null) {
                                                if (option.getMatch_type().equals("=")) {
                                                    showProduct = value.equals(Integer.parseInt(option.getCriteria()));
                                                } else {
                                                    showProduct = !value.equals(Integer.parseInt(option.getCriteria()));
                                                }
                                            }
                                       }

                                    } else {
                                        // Atrribute from assessment
                                        Object value = com.pixalere.utils.Common.getFieldValue(component.getField_name(), assess);
                                        if (value != null) {
                                            Integer num = (Integer) value;
                                            if (option.getMatch_type().equals("=")) {
                                                showProduct = num.equals(new Integer(option.getCriteria()));
                                            } else {
                                                showProduct = !num.equals(new Integer(option.getCriteria()));
                                            } 
                                        }
                                    }
                                } else if (component.getField_type().equals("STRING") && component.getComponent_type().equals("CHKB")) {
                                    Object value = com.pixalere.utils.Common.getFieldValue(component.getField_name(), assess);
                                    if (value != null) {
                                        String str = (String) value;

                                        if (option.getMatch_type().equals("=")) {
                                            showProduct = str.indexOf("\"" + option.getCriteria() + "\"") > -1;

                                        }else if (option.getMatch_type().equals("!=")) {
                                            showProduct = str.indexOf("\"" + option.getCriteria() + "\"") == -1;
                                        } 
                                    }
                                } else if (component.getField_type().equals("STRING") && component.getComponent_type().equals("MPER")) {
                                    AssessmentEachwoundVO a = (AssessmentEachwoundVO) assess;
                                    Collection<AssessmentWoundBedVO> woundbed = a.getWoundbed();
                                    boolean found = false;
                                    for (AssessmentWoundBedVO ab : woundbed) {
                                        if (ab.getLookup().getId() == Integer.parseInt(option.getCriteria())) {
                                            found = true;
                                        }
                                    }
                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = found == true;
                                    } else {
                                        // When !=,  if there is no match the condition validates
                                        showProduct = found == false;
                                    }

                                } 

                                // TODO: Add here other validations
                                // like MDDA, REF and PROD types

                            }
                        }
                    } else if (component != null && option.getCriteria_type().equals("integer")) {
                        // Is a field from NPTW?
                         if (nptwFields.contains(component.getField_name())){
                            if (nptw != null) {
                                // The only possible fields falling here are of type varchar
                                // Only make sense = && !=
                                String value = (String) com.pixalere.utils.Common.getFieldValue(component.getField_name(), nptw);
                                if (value != null) {
                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = value.equals(option.getCriteria());
                                    } else {
                                        showProduct = !value.equals(option.getCriteria());
                                    }
                                }
                             }

                        } else {
                            // Field from assessment
                            Object value = com.pixalere.utils.Common.getFieldValue(component.getField_name(), assess);
                            if (value != null) {
                                if (component.getField_type().equals("DOUBLE")){
                                    // Use primitives to avoid == comparator problems
                                    double num = (Double) value;
                                    double criteria = Double.parseDouble(option.getCriteria());

                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = num == criteria;
                                    } else if (option.getMatch_type().equals("!=")) {
                                        showProduct = num != criteria;
                                    } else if (option.getMatch_type().equals(">")) {
                                        showProduct = num > criteria;
                                    } else if (option.getMatch_type().equals("<")) {
                                        showProduct = num < criteria;
                                    } else if (option.getMatch_type().equals(">=")) {
                                        showProduct = num >= criteria;
                                    } else if (option.getMatch_type().equals("<=")) {
                                        showProduct = num <= criteria;
                                    }
                                } else if (component.getField_type().equals("INTEGER")){
                                    // Use primitives to avoid == comparator problems
                                    int num = (Integer) value;
                                    int criteria = Integer.parseInt(option.getCriteria());

                                    if (option.getMatch_type().equals("=")) {
                                        showProduct = num == criteria;
                                    } else if (option.getMatch_type().equals("!=")) {
                                        showProduct = num != criteria;
                                    } else if (option.getMatch_type().equals(">")) {
                                        showProduct = num > criteria;
                                    } else if (option.getMatch_type().equals("<")) {
                                        showProduct = num < criteria;
                                    } else if (option.getMatch_type().equals(">=")) {
                                        showProduct = num >= criteria;
                                    } else if (option.getMatch_type().equals("<=")) {
                                        showProduct = num <= criteria;
                                    }
                                }
                            }
                         }                 
                    } else if (option.getCriteria_type().equals("healrate") && alpha.equals("A")) {
                        // healrate only applies to wounds
                        double criteria = Double.parseDouble(option.getCriteria());
                        AssessmentEachwoundVO currentAssessment = (AssessmentEachwoundVO) assess;

                        // TODO: What happens if null? Should we assume 0% rate? Or new wound doesnt apply?
                        if (currentAssessment.getHeal_rate() != null){
                            double num = currentAssessment.getHeal_rate();

                            if (option.getMatch_type().equals("=")) {
                                showProduct = num == criteria;
                            } else if (option.getMatch_type().equals("!=")) {
                                showProduct = num != criteria;
                            } else if (option.getMatch_type().equals(">")) {
                                showProduct = num > criteria;
                            } else if (option.getMatch_type().equals("<")) {
                                showProduct = num < criteria;
                            } else if (option.getMatch_type().equals(">=")) {
                                showProduct = num >= criteria;
                            } else if (option.getMatch_type().equals("<=")) {
                                showProduct = num <= criteria;
                            }
                        }
                    }

                    if (andOperator){
                        if (validations.containsKey(option.getComponent_id())){
                            // OR-accumulate the validation associated to this component id
                            // Note: Heal rate will always be component_id = 0
                            // So we are OR-grouping also the heal rates.
                            boolean groupValid = validations.get(option.getComponent_id());
                            groupValid =  (showProduct)? true : groupValid;
                            validations.put(option.getComponent_id(), groupValid);
                        } else {
                            validations.put(option.getComponent_id(), showProduct);
                        }
                    } else {
                        // At least one condition applies? Then set to true, otherwise keep value
                        triggerApplies = (showProduct)? true : triggerApplies;
                    }
                    
                } // end options loop

                if (andOperator){
                    // If validations contains one (or more) false value(s), the trigger is not valid
                    triggerApplies = !validations.containsValue(false);
                }
                
                
                System.out.println("\n[AssessProcess]-->Trigger Id: " + trigger.getTrigger_id() + " Trigger Name: " +  trigger.getName() + "\nOutcome: " + triggerApplies);
                // keep track of already applied triggers
                if (!appliedTriggers.containsKey(trigger.getTrigger_id())){
                    // We save the result of the evaluation for future processing
                    appliedTriggers.put(trigger.getTrigger_id(), triggerApplies);
                }
            }
            
        } catch (DataAccessException e) {
            log.error("Error in TriggerServiceImpl.processTrigger: " + e.toString());
            throw new ApplicationException("Error in TriggerServiceimpl.processTrigger:" + e.getMessage());
        }
    }
}
