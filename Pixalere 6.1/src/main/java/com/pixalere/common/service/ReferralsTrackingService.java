package com.pixalere.common.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Hashtable;
import com.pixalere.common.bean.ReferralsTrackingVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface ReferralsTrackingService {
    public ReferralsTrackingVO getReferralsTracking(@WebParam(name="crit") ReferralsTrackingVO crit) throws ApplicationException;
    public void saveReferralsTracking(@WebParam(name="updateRecord") ReferralsTrackingVO updateRecord) throws ApplicationException;
    public Hashtable getAllReferralsTracking(@WebParam(name="wound_id") int wound_id,@WebParam(name="wound_profile_type_id") int wound_profile_type_id) throws ApplicationException;
 
    
}