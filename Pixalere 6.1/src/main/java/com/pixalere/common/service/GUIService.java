package com.pixalere.common.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.common.bean.InformationPopupVO;
import com.pixalere.common.bean.ComponentsVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface GUIService {
    public ComponentsVO getComponent(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveComponent(@WebParam(name="updateRecord") ComponentsVO updateRecord) throws ApplicationException;
    public Collection<ComponentsVO> getAllComponents(@WebParam(name="crit") ComponentsVO crit) throws ApplicationException;
    public InformationPopupVO getInformationPopup(@WebParam(name="crit") InformationPopupVO crit) throws ApplicationException;
    public void saveInformationPopup(@WebParam(name="updateRecord") InformationPopupVO updateRecord) throws ApplicationException;
    public Collection<InformationPopupVO> getAllInformationPopups() throws ApplicationException;
 
}