
package com.pixalere.common.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.common.bean.LibraryVO;
import javax.jws.WebService;
import javax.jws.WebParam;
import java.util.Vector;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface LibraryService {
    public void saveLibrary(@WebParam(name="updateRecord") LibraryVO updateRecord) throws ApplicationException;
    public Vector<LibraryVO> getAllPdfFiles() throws ApplicationException;
}
