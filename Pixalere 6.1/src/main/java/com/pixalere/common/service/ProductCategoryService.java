package com.pixalere.common.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.common.bean.ProductCategoryVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface ProductCategoryService {
    public ProductCategoryVO getProductCategory(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveProductCategory(@WebParam(name="updateRecord") ProductCategoryVO updateRecord) throws ApplicationException;
    public Collection<ProductCategoryVO> getAllProductCategoriesByCriteria(@WebParam(name="vo")ProductCategoryVO vo) throws ApplicationException;
    public Collection<ProductCategoryVO> getAllProductCategories() throws ApplicationException;
}