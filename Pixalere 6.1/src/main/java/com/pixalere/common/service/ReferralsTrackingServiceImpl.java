package com.pixalere.common.service;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.common.*;
import com.pixalere.common.bean.*;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Collections;
import java.util.Date;
import com.pixalere.common.dao.*;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.utils.*;

import com.pixalere.guibeans.ReferralsList;

import java.util.HashMap;

import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.mail.SendMailUsingAuthentication;
import java.util.Hashtable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import javax.jws.WebService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import com.pixalere.auth.bean.PositionVO;
/**
 * This is the referrals tracking service implementation Refer to {@link com.pixalere.common.service.ReferralsTrackingService
 * } to see if there are any web services available.
 *
 * <img src="ReferralsTrackingServiceImpl.png"/>
 *
 *
 * @view ReferralsTrackingServiceImpl
 *
 * @match class com.pixalere.common.
 *
 * @opt hide
 * @match class
 * com.pixalere.common\.(dao.ReferralsTrackingDAO|bean.ReferralsTrackingVO|service.ReferralsTrackingService)
 * @opt !hide
 *
 */
@WebService(endpointInterface = "com.pixalere.common.service.ReferralsTrackingService", serviceName = "ReferralsTrackingService")
public class ReferralsTrackingServiceImpl implements ReferralsTrackingService {

    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReferralsTrackingServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();

    private Integer language = 1;
    private String locale = "en";

    public ReferralsTrackingServiceImpl(Integer language) {
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
    }

    public ReferralsTrackingServiceImpl() {
    }

    public Vector getAllWoundAssessmentsByReferral(ReferralsTrackingVO vo) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        Vector vect = new Vector();
        try {
            Collection coll = dao.findAllByCriteria(vo);
            Iterator iter = coll.iterator();
            while (iter.hasNext()) {
                ReferralsTrackingVO refer = (ReferralsTrackingVO) iter.next();
                WoundAssessmentVO wa = refer.getWoundAssessment();
                vect.add(wa);
            }
        } catch (DataAccessException e) {
            log.error("Error in ProductCategoryBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductCategoryBD.addItem: " + e.toString(), e);
        }
        return vect;
    }

    public void removeReferral(ReferralsTrackingVO vo) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        try {
            dao.delete(vo);
            WoundServiceImpl wservice = new WoundServiceImpl(language);
            WoundVO w = wservice.getWound(vo.getWound_id());
            if (w != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), w.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "referrals_tracking", w));
            }
        } catch (DataAccessException e) {
            log.error("Error in ReferralsTrackingManagerBD.getReferrals(): " + e.toString());
            throw new ApplicationException("Error in ReferralsTrackingManagerBD.getReferrals(): " + e.toString(), e);
        }
    }

    public ReferralsTrackingVO getReferralsTracking(ReferralsTrackingVO vo) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        ReferralsTrackingVO ref = null;
        try {
            ref = (ReferralsTrackingVO) dao.findByCriteria(vo);
        } catch (DataAccessException e) {
            log.error("Error in ReferralsTrackingManagerBD.getReferrals(): " + e.toString());
            throw new ApplicationException("Error in ReferralsTrackingManagerBD.getReferrals(): " + e.toString(), e);
        }
        return ref;
    }

    public Collection<ReferralsTrackingVO> getAllReferralsByCriteria(ReferralsTrackingVO vo) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        Collection referrals = null;
        try {
            referrals = dao.findAllByCriteria(vo);
        } catch (DataAccessException e) {
            log.error("Error in ProductCategoryBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductCategoryBD.addItem: " + e.toString(), e);
        }
        return referrals;
    }

    public AutoReferralVO getAutoReferral(AutoReferralVO vo) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        AutoReferralVO ref = null;
        try {
            ref = (AutoReferralVO) dao.findAutoReferralByCriteria(vo);
        } catch (DataAccessException e) {
            log.error("Error in ReferralsTrackingManagerBD.getReferrals(): " + e.toString());
            throw new ApplicationException("Error in ReferralsTrackingManagerBD.getReferrals(): " + e.toString(), e);
        }
        return ref;
    }

    public Collection<ReferralsTrackingVO> getAllAutoReferralsByCriteria(AutoReferralVO vo) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        Collection referrals = null;
        try {
            referrals = dao.findAllAutoReferralsByCriteria(vo);
        } catch (DataAccessException e) {
            log.error("Error in ProductCategoryBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductCategoryBD.addItem: " + e.toString(), e);
        }
        return referrals;
    }

    public void saveReferralsTracking(ReferralsTrackingVO refVO, int patient_id) throws ApplicationException {
        try {
            ReferralsTrackingDAO refDAO = new ReferralsTrackingDAO();
            if (refVO.getActive() == null) {
                refVO.setActive(1);
                refVO.setCurrent(1);
            }
            refDAO.update(refVO, patient_id);
            ReferralsTrackingVO new_id = getReferralsTracking(refVO);

            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(refVO.getProfessional_id(), patient_id, (refVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "referrals_tracking", new_id));
            } else {
                log.error("Find out why we're NULL ; saveReferralsTracking: " + refVO.getAssessment_id());
            }
        } catch (DataAccessException e) {
            log.error("Error in ReferralsTrackingManagerBD.updateReferral: " + e.toString());
            throw new ApplicationException("Error in ReferralsTrackingManagerBD.updateReferral: " + e.toString(), e);
        }
    }

    public void saveReferralsTracking(ReferralsTrackingVO refVO) throws ApplicationException {
        try {
            ReferralsTrackingDAO refDAO = new ReferralsTrackingDAO();
            
            
            WoundServiceImpl wservice = new WoundServiceImpl(language);
            ListServiceImpl lservice = new ListServiceImpl(language);
            PatientServiceImpl pservice = new PatientServiceImpl(language);
            WoundVO w = wservice.getWound(refVO.getWound_id());
            if (w != null) {
                refVO.setPatient_id(w.getPatient_id());
            }
            
            refDAO.update(refVO);
            ReferralsTrackingVO new_id = getReferralsTracking(refVO);
            //@todo temporary until Offline supports sending emails
            
            if (refVO.getEntry_type().equals(new Integer(0)) && refVO.getId() == null && Common.getConfig("allowReferralEmails") != null && Common.getConfig("allowReferralEmails").equals("1")) {
                //if new referral (null), we know its coming from Offline.
                int assigned_to = -1;
                if (refVO.getAssigned_to() != null) {
                    assigned_to = refVO.getAssigned_to();
                }
                LookupVO priority = null;
                if(refVO.getTop_priority()!=null){priority=lservice.getListItem(refVO.getTop_priority());}
                PatientAccountVO pat = pservice.getPatient(refVO.getPatient_id());
                if(assigned_to != -1){
                    sendReferralEmail(pat, assigned_to, (priority == null ? "" : priority.getName(language)), locale);
                }
            }
            if (new_id != null) {

                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(refVO.getProfessional_id(), w.getPatient_id(), (refVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "referrals_tracking", new_id));
            }
        } catch (DataAccessException e) {
            log.error("Error in ReferralsTrackingManagerBD.updateReferral: " + e.toString());
            throw new ApplicationException("Error in ReferralsTrackingManagerBD.updateReferral: " + e.toString(), e);
        }
    }

    public void saveAutoReferral(AutoReferralVO refVO) throws ApplicationException {
        try {
            ReferralsTrackingDAO refDAO = new ReferralsTrackingDAO();
            refDAO.updateAutoReferral(refVO);
            ReferralsTrackingVO t = new ReferralsTrackingVO();
            t.setId(refVO.getReferral_id());
            ReferralsTrackingVO ref = getReferralsTracking(t);
            WoundServiceImpl wservice = new WoundServiceImpl(language);
            WoundVO w = wservice.getWound(ref.getWound_id());
            AutoReferralVO new_id = getAutoReferral(refVO);
            if (new_id != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(ref.getProfessional_id(), w.getPatient_id(), (refVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "auto_referrals", refVO));
            }
        } catch (DataAccessException e) {
            log.error("Error in ReferralsTrackingManagerBD.updateReferral: " + e.toString());
            throw new ApplicationException("Error in ReferralsTrackingManagerBD.updateReferral: " + e.toString(), e);
        }
    }

    /**
     * Sort the Referrals List by criteria defined in ReferralsList.
     *
     * @see ReferralsList
     */
    public Vector sortReferrals(Vector v) {
        Collections.sort(v, new ReferralsList());
        return v;
    }

    /**
     * Get the timestamp of the last Referral entered.
     *
     * @param wound_id
     * @return
     * @throws ApplicationException
     */
    public Date getLatestReferralTimestamp(Integer wound_id) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        ReferralsTrackingVO refVO = new ReferralsTrackingVO();
        refVO.setWound_id(wound_id);
        refVO.setEntry_type(0);
        refVO.setActive(1);
        Date lngLatestReferral = null;
        try {
            ReferralsTrackingVO refer = (ReferralsTrackingVO) dao.findByCriteria(refVO);
            if (refer != null) {
                lngLatestReferral = refer.getCreated_on();
            }

        } catch (DataAccessException e) {
            log.error("Error in SummarySetupAction: " + e.toString());
            throw new ApplicationException("Error in SummarySetupAction: " + e.toString(), e);
        }
        return lngLatestReferral;
    }

    /**
     *
     * @todo Need to refactor this method, most if not all filtering is no
     * longer needed.
     * @param entry_type
     * @param user
     * @return
     * @throws ApplicationException
     */
    public Vector getAllReferrals(Integer entry_type, ProfessionalVO user) throws ApplicationException {
        Vector returnV = new Vector();
        PatientServiceImpl pbd = new PatientServiceImpl(language);
        WoundServiceImpl wpmanager = new WoundServiceImpl(language);
        Collection<ReferralsTrackingVO> results = null;
        try {
            ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
            results = dao.sortAllByCriteria(entry_type, user, true);

            int count = 0;
            PDate pdate = new PDate(user != null ? user.getTimezone() : Constants.TIMEZONE);
            for (ReferralsTrackingVO vo : results) {
                count++;
                WoundVO wpvo = vo.getWound();
                PatientAccountVO pavo = vo.getPatient();
                //PatientAccountVO ensureActive = pbd.getPatientByID(vo.getPatient_account_id().intValue());
                String treatment_location = "";
                LookupVO lookupVO = null;
                ListServiceImpl listBD = new ListServiceImpl(language);
                LookupVO treatmentList = new LookupVO();
                Integer treatment_id = new Integer(0);
                try {
                    if (pavo != null && wpvo != null) {
                        treatment_id = pavo.getTreatment_location_id();
                        if (treatment_id.equals(new Integer("-1"))) {
                            treatment_location = Common.getLocalizedString("pixalere.TIP", "en");
                        } else {
                            treatmentList = pavo.getTreatmentLocation();       //listBD.getListItem(treatment_id);//
                            if (treatmentList == null) {
                                treatment_location = Common.getLocalizedString("pixalere.na", locale);
                            } else {
                                treatment_location = treatmentList.getName(language);
                            }
                        }
                    }

                    if (vo.getTop_priority() != null && vo.getTop_priority().intValue() != 0) {
                        lookupVO = vo.getPriorityList();
                    }
                    if (lookupVO == null) {
                        lookupVO = new LookupVO();
                        LookupLocalizationVO l = new LookupLocalizationVO();
                        l.setName("");
                        l.setLanguage_id(Common.IDLANG_EN);//English.. should add to constants or make configurable
                        Collection<LookupLocalizationVO> lColl = new ArrayList();
                        lColl.add(l);
                        lookupVO.setNames(lColl);
                    }
                    if (wpvo != null) {
                        if (vo.getWound_id() == null || wpvo.getPatient_id() == null || lookupVO.getName(language) == null || vo.getCreated_on() == null || vo.getAssessment_id() == null) {    //dont list referral, corrupt data.
                        } else {
                            try {
                                String priority = "";
                                if(lookupVO != null){priority=lookupVO.getName(language);}
                                
                                if (vo.getEntry_type() == Constants.MESSAGE_FROM_NURSE || vo.getEntry_type() == Constants.MESSAGE_FROM_CLINICIAN) {
                                    priority = "Message";
                                }
                                if (vo.getAuto_referrals() != null && !priority.equals("Message")) {
                                    String auto_string = "";
                                    for (AutoReferralVO auto : vo.getAuto_referrals()) {
                                        if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Heal Rate")) {
                                            auto_string = auto_string + "" + auto.getAuto_referral_type() + ", ";
                                        } else if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Dressing Change Frequency")) {
                                            auto_string = auto_string + "DCF, ";
                                        } else if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Antimicrobial")) {
                                            auto_string = auto_string + "AMx, ";
                                        }

                                    }
                                    if (auto_string.contains(",")) {
                                        auto_string = auto_string.substring(0, auto_string.length() - 1);
                                    }
                                    if (auto_string != null && auto_string.length() > 0) {
                                        priority = priority + " (" + auto_string + ")";
                                    }
                                }
                                WoundProfileTypeVO type = new WoundProfileTypeVO();
                                type.setId(vo.getWound_profile_type_id());
                                WoundProfileTypeVO wptv = wpmanager.getWoundProfileType(type);
                                ReferralsList refVO = new ReferralsList(vo.getWound_id(), vo.getWound_profile_type_id(), wpvo.getPatient_id(), (pavo.getPatientName()), priority, pdate.getDateString(vo.getCreated_on(), locale), vo.getAssessment_id(), treatment_location, wpvo.getWound_location_detailed() + " - " + Common.getWoundProfileType(wptv.getAlpha_type(), locale));
                                int error = 0;
                                //int error = pbd.validatePatientId(user.getId().intValue(), refVO.getPatient_id().intValue());
                                //log.info("#########9");
                                if (error == 0) {
                                    returnV.add(refVO);
                                }
                            } catch (NullPointerException exp) {
                                if (wpvo != null) {
                                    log.info("Getting Referrals for: " + wpvo.getPatient_id() + " Exception: " + exp.getMessage());
                                } else {
                                    log.info("Getting Referrals for: unknown as WOUnd Profile is NULL");
                                }
                            }
                        }
                    }
                } catch (NullPointerException exe) {
                    exe.printStackTrace();
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                log.info("ReferralsTrackingManagerBD.getAllReferralsByWOCN: Done Adding ReferralsTracking");
            }
        } catch (DataAccessException e) {
            log.error("DataAccessException in ReferralsTrackingManagerBD.getAllReferralsByWOCN: " + e.toString());
            e.printStackTrace();
            throw new ApplicationException("Error in ReferralsTrackingManagerBD.getAllReferralsByWOCN: " + e.toString(), e);
        } catch (Exception e) {
            log.error("Error in ReferralsTrackingManagerBD.getAllReferralsByWOCN: " + e.getMessage());
            e.printStackTrace();
        }

        return returnV;
    }

    /**
     * Get all REferrals, store in hashtable with the key being the
     * assessment_id. This will allow us to extract the Referral object for each
     * assessment on the progress notes page.
     *
     * @since 4.0
     * @param wound_id
     * @param wound_profile_type_id
     * @return hashtable
     * @throws ApplicationException
     */
    public Hashtable getAllReferralsTracking(int wound_id, int wound_profile_type_id) throws ApplicationException {
        ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        Hashtable hash = new Hashtable();
        try {
            Collection<PositionVO> positions = userBD.getPositions(new PositionVO());
            HashMap positionMap = new HashMap();
            for(PositionVO p : positions){
                positionMap.put(""+p.getId(),p.getTitle());
            }
            
            WoundAssessmentServiceImpl bd = new WoundAssessmentServiceImpl();
            WoundAssessmentVO wat = new WoundAssessmentVO();
            wat.setWound_id(new Integer(wound_id));
            Collection assessment = bd.getAllAssessments(wat);
            Iterator iter = assessment.iterator();
            while (iter.hasNext()) {
                WoundAssessmentVO ass = (WoundAssessmentVO) iter.next();
                ReferralsTrackingVO refer = new ReferralsTrackingVO();
                refer.setAssessment_id(ass.getId());
                refer.setWound_profile_type_id(wound_profile_type_id);

                Collection coll = dao.findAllByCriteria(refer);
                //need to get professional signature
                Vector tmp = new Vector();
                Iterator it = coll.iterator();
                while (it.hasNext()) {
                    ReferralsTrackingVO rtmp = (ReferralsTrackingVO) it.next();
                    int pro_id = 0;
                    if (rtmp != null && rtmp.getProfessional_id() != null) {
                        pro_id = rtmp.getProfessional_id().intValue();
                    }
                    ProfessionalVO refPro = userBD.getProfessional(pro_id);
                    PDate pdate = new PDate(refPro.getTimezone());
                    ReferralsTrackingVO r = rtmp;
                    
                    
                    r.setType(Common.getLocalizedString("pixalere.viewer.form.tracker_" + rtmp.getEntry_type(), "en"));

                    //r.setCreated_on(new Date());
                    if (!rtmp.getEntry_type().equals(new Integer(3))) {
                        r.setUser_signature(pdate.getProfessionalSignature(r.getCreated_on(), refPro, "en"));
                        tmp.add(r);
                    }
                }
                hash.put(ass.getId() + "", tmp);
            }
            hash.put("positions",positionMap);
        } catch (DataAccessException e) {
            log.error("Error in ProductCategoryBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ProductCategoryBD.addItem: " + e.toString(), e);
        }
        return hash;
    }

    /**
     * Sends an email to professionals who have access to th Referrals popup. if
     * position_id != -1 it will send to only professionals with access to popup
     * and with designatd position.
     *
     * @param patient
     * @param position_id
     * @param priority
     * @param locale
     * @param request
     */
    public void sendReferralEmail(PatientAccountVO patient, int position_id, String priority, String locale) {
        try {
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            SendMailUsingAuthentication mailservice = new SendMailUsingAuthentication();
            if (patient != null) {
                //send an email to all clincians assigned to this
                Collection<ProfessionalVO> professionals = pservice.getProfessionalsByAccess(position_id, patient.getTreatment_location_id());

                for (ProfessionalVO prof : professionals) {
                    if (prof != null && prof.getEmail() != null && !prof.getEmail().equals("")) {
                    //send email about referral to Clinician

                        String subject = Common.getLocalizedString("pixalere.referral.email.subject", locale) + ": " + Common.getConfig("customer_name") + " on " + PDate.getDate(new Date(), true);
                        String body1 = Common.getLocalizedString("pixalere.referral.email.body1", locale) + " "+ (priority!=""?priority:"N/A") + " "+Common.getLocalizedString("pixalere.for",locale)+" "+Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale)+":"+patient.getPatient_id()+".\n";
                        

                        mailservice.postMail(prof.getEmail(), subject, body1, null);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
