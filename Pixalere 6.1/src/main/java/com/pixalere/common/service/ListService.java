package com.pixalere.common.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.LanguageVO;
import com.pixalere.common.bean.LookupVO;
import java.util.Collection;
import com.pixalere.admin.bean.ResourcesVO;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface ListService {
    public LookupVO getListItem(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveListItem(@WebParam(name="updateRecord") LookupVO updateRecord) throws ApplicationException;
    public Collection<LookupVO> getLists(@WebParam(name="listItem") int listItem) throws ApplicationException;
    public Collection<LanguageVO> getLanguages() throws ApplicationException;
    public Collection<LookupVO> getAllListsForOffline() throws ApplicationException;
}