/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.common.service;
import com.pixalere.common.*;
import com.pixalere.common.dao.PatientReportDAO;
import com.pixalere.common.bean.PatientReportVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.Common;
import java.util.Collection;
import java.util.Vector;
import java.util.Hashtable;
import javax.jws.WebService;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;
import java.io.File;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService(endpointInterface = "com.pixalere.common.service.LibraryService", serviceName = "LibraryService")
public class PatientReportService {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LibraryServiceImpl.class);
    
    public PatientReportService(){
           
    }
    
    public void savePatientReportUpdate(PatientReportVO configurationVO) throws ApplicationException{
        try{
            PatientReportDAO configurationDAO=new PatientReportDAO();
            configurationDAO.insert(configurationVO);
        } catch(DataAccessException e){
            log.error("Error in PatientReportService.save: " + e.toString());
            throw new ApplicationException("Error in PatientReportService.save: " + e.toString(),e);
        }
    }
     public PatientReportVO getPatientReportUpdate(int primaryKey) throws ApplicationException{
        try{
            PatientReportDAO listDAO = new PatientReportDAO();
            PatientReportVO pr = new PatientReportVO();
            pr.setPatient_id(primaryKey);
            return (PatientReportVO) listDAO.findByCriteria(pr);
        } catch (DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientReportService.getPatientReportUpdate(): " + e.toString(),e);
        }
    }
    public Collection getAllPatientChanges()throws ApplicationException{
        String today = new PDate().getEpochTime()+"";
        try{
        /*String day = PDate.getDay(today);
        String month = PDate.getMonthNum(today);
        String year = PDate.getYear(today);
        String start_time = PDate.getEpochFromDate(Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(year), 0,0);
        String end_time = PDate.getEpochFromDate(Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(year), 23,59);
          */
        PatientReportDAO pr = new PatientReportDAO();
        Collection<PatientReportVO> patients = pr.findAllByCriteria();
        return patients;
        }catch (DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientReportService.getAllPatientChanges(): " + e.toString(),e);
        }catch(Exception e){
            throw new ApplicationException("DataAccessException Error in PatientReportService.getAllPatientChanges(): " + e.toString(),e);
        }
    }
}
