package com.pixalere.common.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.ProductsVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface ProductsService {
    public ProductsVO getProduct(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveProduct(@WebParam(name="updateRecord") ProductsVO updateRecord) throws ApplicationException;
    //public Collection<ProductsVO> getAllProducts(@WebParam(name="professional") ProfessionalVO professional) throws ApplicationException;
    public Collection<ProductsVO> getAllProductsByCriteria(@WebParam(name="vo") ProductsVO vo) throws ApplicationException;

    public Collection<ProductsVO> getAllProducts() throws ApplicationException;
}