package com.pixalere.common.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.admin.bean.VendorVO;
import java.util.Collection;
import java.util.List;
import com.pixalere.common.bean.ConfigurationVO;
import com.pixalere.common.bean.TableUpdatesVO;
import javax.jws.WebService;
import javax.jws.WebParam;
import java.util.TimeZone;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface ConfigurationService {
    public ConfigurationVO getConfiguration(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveConfiguration(@WebParam(name="updateRecord") ConfigurationVO updateRecord) throws ApplicationException;
    public List<ConfigurationVO> getAllConfigurations() throws ApplicationException;
    public TableUpdatesVO getTableUpdates() throws ApplicationException;
    public String getServerTimeZone() throws ApplicationException;
    public void saveVendor(@WebParam(name="updateRecord") VendorVO updateRecord) throws ApplicationException;
    public List<VendorVO> getAllVendors() throws ApplicationException;
}