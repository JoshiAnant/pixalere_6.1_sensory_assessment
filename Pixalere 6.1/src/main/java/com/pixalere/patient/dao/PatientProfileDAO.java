package com.pixalere.patient.dao;
import java.util.Date;

import com.pixalere.patient.bean.DiagnosticInvestigationVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.guibeans.PatientSearchVO;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.utils.*;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbUpperAssessmentVO;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.patient.bean.PhysicalExamVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;

import java.util.Vector;
import java.util.Iterator;
import java.util.Collection;
import java.util.Date;

import com.pixalere.patient.bean.ExternalReferralVO;

import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.ReportQueryByCriteria;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;

import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.bean.OfflineVO;
import com.pixalere.guibeans.WoundProfileItem;

import java.util.ArrayList;
import java.util.List;
/**
 * This   class is responsible for handling all CRUD logic associated with the
 * <<table>>    table.
 *
 * @todo Need to implement this class as a singleton.
 */
public class PatientProfileDAO implements DataAccessObject {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(PatientProfileDAO.class);
    public PatientProfileDAO() {
    }
    /**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<PatientProfileVO> findPatientsFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PatientProfileVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on", PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(PatientProfileVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findPatientBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date != null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(PatientProfileVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (PatientProfileVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection<PhysicalExamVO> findAllByCriteria(PhysicalExamVO pVO, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PhysicalExamVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (pVO != null && !Common.getConfig("showDeleted").equals("1")) {
                pVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(pVO);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            r = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection<PatientProfileVO> findAllByCriteria(PatientProfileVO patientProfileVO, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PatientProfileVO> r = null;
        PatientAccountVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
   
            if (patientProfileVO != null && !Common.getConfig("showDeleted").equals("1")) {
           
                patientProfileVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patientProfileVO);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            r = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
 /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection<DiagnosticInvestigationVO> findAllByCriteria(DiagnosticInvestigationVO patientProfileVO, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<DiagnosticInvestigationVO> r = null;
        PatientAccountVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
   
            if (patientProfileVO != null && !Common.getConfig("showDeleted").equals("1")) {
           
                patientProfileVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patientProfileVO);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            r = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    public Collection findAllByCriteria(BradenVO bradenVO, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (bradenVO != null && !Common.getConfig("showDeleted").equals("1")) {
                bradenVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(bradenVO);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Used for Conversion
     */
    public Collection findAllByCriteriaOrderBy(PatientProfileVO patientProfileVO, String strOrderField, boolean blnDescending) throws DataAccessException {
        PersistenceBroker broker = null;
        Collection<PatientProfileVO> r = null;
        PatientAccountVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (patientProfileVO != null && !Common.getConfig("showDeleted").equals("1")) {
                patientProfileVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patientProfileVO);
            if (blnDescending) {
                query.addOrderByDescending(strOrderField);
            } else {
                query.addOrderByAscending(strOrderField);
            }
            r = broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection findAllFeetByCriteria(FootAssessmentVO foot, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<FootAssessmentVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (foot != null && !Common.getConfig("showDeleted").equals("1")) {
                foot.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(foot);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return results;
    }
    
    /********************************************************************************************************/
    
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection findAllSensoryByCriteria(SensoryAssessmentVO sensory, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<SensoryAssessmentVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (sensory != null && !Common.getConfig("showDeleted").equals("1")) {
                sensory.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(sensory);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return results;
    }
    
    
    /********************************************************************************************************/
    
    
    
    
    
    
    
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection findAllAdvLimbsByCriteria(LimbAdvAssessmentVO limb, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LimbAdvAssessmentVO> results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (limb != null && limb.getDeleted() == null && !Common.getConfig("showDeleted").equals("1")) {
                limb.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(limb);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection findAllUpperLimbsByCriteria(LimbUpperAssessmentVO limb, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LimbUpperAssessmentVO> results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (limb != null && limb.getDeleted() == null && !Common.getConfig("showDeleted").equals("1")) {
                limb.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(limb);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return results;
    }
/**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     *                         parameters in the SQL statement.
     * @since 3.0
     */
    public Collection findAllBasicLimbsByCriteria(LimbBasicAssessmentVO limb, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LimbBasicAssessmentVO> results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (limb != null && limb.getDeleted() == null && !Common.getConfig("showDeleted").equals("1")) {
                limb.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(limb);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return results;
    }
    public Collection findAllBradenByCriteria(BradenVO braden, int limit) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<BradenVO> results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (braden != null && braden.getDeleted() == null && !Common.getConfig("showDeleted").equals("1")) {
                braden.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(braden);
            query.addOrderByDescending("created_on");
            if (limit > 0) {
                query.setStartAtIndex(1);
                query.setEndAtIndex(limit);
            }
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all <<table>> records by criteria, and professional.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @param prof           Professional Accounts object
     * @todo rename ProfessionalVO to offlineVO, and this should be in its own CRUD.
     * @since 3.0
     */
    public Vector findAllOfflineByCriteria(OfflineVO off2, ProfessionalVO prof, boolean ignoreDups) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findAllOfflineByCriteria()***********************");
        PersistenceBroker broker = null;
        Vector results = new Vector();

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(off2);
            query.addOrderByAscending("wound_id");
            query.addOrderByAscending("assessment_id");
            Collection<OfflineVO> coll = (Collection) broker.getCollectionByQuery(query);
            com.pixalere.auth.service.ProfessionalServiceImpl userBD = new com.pixalere.auth.service.ProfessionalServiceImpl();
            int patient_id_check = 0;
            if (ignoreDups == true) {
                for (OfflineVO off : coll) {
                    results.add(off);
                }
            } else {
                for (OfflineVO off : coll) {
                    int patient_id = -1;
                    int wound_id = -1;
                    int assessment_id = -1;
                    if (off.getPatient_id() != null) {
                        patient_id = off.getPatient_id().intValue();
                    }
                    if (off.getWound_id() != null) {
                        wound_id = off.getWound_id().intValue();
                    }
                    if (off.getAssessment_id() != null) {
                        assessment_id = off.getAssessment_id().intValue();
                    }
                    if (patient_id_check != off.getPatient_id().intValue() && ((prof == null || prof.getId() == null) || prof.getId().equals(off.getProfessional_id()))) {
                        results.add(off);
                    }
                    //ignore permissions
                    if (patient_id == -1 && wound_id == -1 && assessment_id == -1) {
                        results.add(off);
                    }
                    patient_id_check = off.getPatient_id().intValue();
                }
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findAllOfflineByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findAllOfflineByCriteria()", e);
        } catch (Exception e) {
            e.printStackTrace();
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findAllOfflineByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findAllOfflineByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findAllOfflineByCriteria()***********************");
        return results;
    }
    public Vector getAllPatientProfileSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector vecResults = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(PatientProfileVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "created_on", "id"});
            q.addOrderByDescending("created_on");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                vecResults.add(iter.next());

            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return vecResults;
    }
    public Vector getAllInvestigationSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector vecResults = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(DiagnosticInvestigationVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "created_on", "id"});
            q.addOrderByDescending("created_on");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                vecResults.add(iter.next());

            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return vecResults;
    }
    public Vector getAllPhysicalExamsSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector vecResults = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(PhysicalExamVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "created_on", "id"});
            q.addOrderByDescending("created_on");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                vecResults.add(iter.next());

            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return vecResults;
    }
    
    public Vector getAllSensoryAssessmentsSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector vecResults = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(SensoryAssessmentVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "created_on", "id"});
            q.addOrderByDescending("created_on");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                vecResults.add(iter.next());

            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return vecResults;
    }

    
    
    
    public Vector getAllBradenSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(BradenVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "created_on", "id"});
            q.addOrderByDescending("created_on");
            // q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                if (limit_start > 0) // Skip the first ones if limit is set
                {
                    limit_start--;
                    iter.next();
                } else {
                    v.add(iter.next());
                }
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    public Vector getAllBasicLimbSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(LimbBasicAssessmentVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "id"});
            q.addOrderByDescending("created_on");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    public Vector getAllUpperLimbSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(LimbUpperAssessmentVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "id"});
            q.addOrderByDescending("created_on");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
public Vector getAllAdvLimbSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(LimbAdvAssessmentVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "id"});
            q.addOrderByDescending("created_on");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    public Vector getAllFootSignatures(int patient_id, int limit_start) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("active", 1);
            crit.addEqualTo("deleted", 0);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(FootAssessmentVO.class, crit);
            q.setAttributes(new String[]{"user_signature", "id"});
            q.addOrderByDescending("id");
            q.setStartAtIndex(limit_start);
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    /* @TODO Move this out of DAO, and into the Manager.. */
    public Vector<PatientSearchVO> searchPatients(ProfessionalVO userVO, String last_name, int patient_id, String client_id, String treatment_location_id, String day,
            String month, int year, int professional_id,Integer language) throws DataAccessException {
        PersistenceBroker broker = null;
        Vector<PatientSearchVO> patients = new Vector();
        PatientAccountVO pavo = new PatientAccountVO();
        try {
            Criteria crit = new Criteria();
            QueryByCriteria q = null;
            WoundProfileDAO dao = new WoundProfileDAO();
            PDate pdate = new PDate(userVO != null ? userVO.getTimezone() : Constants.TIMEZONE);
            if (patient_id != 0) {
                crit.addEqualTo("patient_id", new Integer(patient_id));
            }
            if (!client_id.equals("")) {
                crit.addEqualTo("phn", pavo.getEncrypted(client_id, "pass4phn"));
            }
            if (!treatment_location_id.equals("0")) {
                crit.addEqualTo("treatment_location_id", treatment_location_id);
            }
            if ((day != null && !day.equals("")) && (month != null && !month.equals("")) && (year > 0)) {
                crit.addEqualTo("dob", PDate.getXTimestamp(month, day, year + ""));
            }
            if (!last_name.equals("")) {
                crit.addEqualTo("lastName_search", pavo.getEncrypted(Common.stripName(last_name), "pass4lastname"));
            }
            // added for search by professional id
            if (professional_id != 0) {
                crit.addEqualTo("created_by", new Integer(professional_id));
            }
            crit.addEqualTo("current_flag", new Integer(1));
            crit.addEqualTo("account_status", new Integer(1));
            q = QueryFactory.newQuery(PatientAccountVO.class, crit);
            broker = ConnectionLocator.getInstance().findBroker();
            q.addOrderByAscending("treatment_location_id");
            Collection<PatientAccountVO> results = broker.getCollectionByQuery(q);
            PatientServiceImpl bd = new PatientServiceImpl(language);

            for (PatientAccountVO account : results) {
                try {
                    int error = 0;
                    error = bd.validatePatientId(userVO.getId().intValue(), account.getPatient_id().intValue());
                    if (error == 0 || error == 5) // error 5 = TIP
                    {
                        WoundProfileVO vo = new WoundProfileVO();
                        // PatientProfileVO
                        // profile=(PatientProfileVO)((account.getPatientProfiles()).get(0));
                        PatientSearchVO search = new PatientSearchVO();
                        search.setPatient_id(account.getPatient_id().intValue());
                        PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(language);
                        PatientProfileVO tmpVO = new PatientProfileVO();
                        tmpVO.setPatient_id(account.getPatient_id());
                        tmpVO.setDeleted(0);
                        PatientProfileVO profile = pmanager.getPatientProfile(tmpVO);
                        ListServiceImpl listbd = new ListServiceImpl(language);
                        if (account.getTreatment_location_id() > 0) {
                            LookupVO lookupVO = (LookupVO) listbd.getListItem(new Integer(account.getTreatment_location_id()).intValue());
                            if (lookupVO != null) {
                                search.setTreatment_location(lookupVO.getName(language));
                            }
                        } else {
                            search.setTreatment_location(Common.getLocalizedString("pixalere.TIP","en"));
                        }
                        search.setStatus(account.getAccount_status() + "");
                        vo.setPatient_id(account.getPatient_id());
                        vo.setCurrent_flag(new Integer(1));
                        Collection<WoundProfileVO> wounds = dao.findAllByCriteria(vo, 0);
                        if (wounds != null && wounds.size() != 0) {
                            logs.info("*************** PatientProfileDAO.searchPatients: Patient has wounds.");
                            Vector woundage = new Vector();
                            for (WoundProfileVO wound : wounds) {
                                try {
                                    WoundProfileItem item = new WoundProfileItem();
                                    item.setId(wound.getWound_id().intValue());
                                    item.setName(Common.getLocalizedString(wound.getWound().getWound_location_detailed(),"en"));
                                    item.setStatus(wound.getWound().getStatus());
                                    //TODO: move into SQL call.
                                    //	if(!wound.getStatus().equals("Closed")){
                                    if (wound.getWound().getStatus() == null) {
                                        woundage.add(item);
                                    }
                                } catch (Exception e) {
                                    logs.error(e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                            search.setWounds(woundage);
                        }
                        patients.add(search);
                    }
                } catch (Exception e) {
                    logs.error(e.getMessage());
                    e.printStackTrace();
                }
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.searchPatients(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.searchPatients(): " + e.toString(), e);
        } catch (ApplicationException e) {
            logs.error("DataAccessException thrown in PatientProfileDAO.searchPatients(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************** PatientProfileDAO.searchPatients: Number of Patients " + patients.size());
        //@todo add a collections sort here instead
        for (int intSort1 = 0; intSort1 < patients.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < patients.size() - intSort1 - 1; intSort2++) {
                PatientSearchVO sortRec1 = (PatientSearchVO) patients.get(intSort2);
                PatientSearchVO sortRec2 = (PatientSearchVO) patients.get(intSort2 + 1);
                if (sortRec1.getPatient_id() > sortRec2.getPatient_id()) {
                    patients.setElementAt(sortRec2, intSort2);
                    patients.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        return patients;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @todo rename patient_id to patientVO
     * @since 3.0
     */
    public ValueObject findByCriteria(PatientProfileVO patient_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (patient_id != null && !Common.getConfig("showDeleted").equals("1")) {
                patient_id.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (PatientProfileVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @todo rename patient_id to patientVO
     * @since 4.2
     */
    public ArrayValueObject findByCriteria(PatientProfileArraysVO patient_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientProfileArraysVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (PatientProfileArraysVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @todo rename patient_id to patientVO
     * @since 3.0
     */
    public ValueObject findByCriteria(PhysicalExamVO patient_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PhysicalExamVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (patient_id != null && !Common.getConfig("showDeleted").equals("1")) {
                patient_id.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (PhysicalExamVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @todo rename patient_id to patientVO
     * @since 3.0
     */
    public ArrayValueObject findByCriteria(InvestigationsVO patient_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        InvestigationsVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (InvestigationsVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
/**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 6.0
     */
    public DiagnosticInvestigationVO findByCriteria(DiagnosticInvestigationVO obj) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        DiagnosticInvestigationVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(obj);
            query.addOrderByDescending("id");
            patientVO = (DiagnosticInvestigationVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    public ValueObject findByCriteria(BradenVO patient_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        BradenVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (patient_id != null && !Common.getConfig("showDeleted").equals("1")) {
                patient_id.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (BradenVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
/**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @todo rename patient_id to patientVO
     * @since 3.0
     */
    public ValueObject findByCriteria(DiagnosticInvestigationVO patient_id, boolean showDeleted) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        DiagnosticInvestigationVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (patient_id != null && showDeleted == false) {
                patient_id.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (DiagnosticInvestigationVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @todo rename patient_id to patientVO
     * @since 3.0
     */
    public ValueObject findByCriteria(PatientProfileVO patient_id, boolean showDeleted) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (patient_id != null && showDeleted == false) {
                patient_id.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (PatientProfileVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    public ValueObject findByCriteria(BradenVO patient_id, boolean showDeleted) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        BradenVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (patient_id != null && showDeleted == false) {
                patient_id.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(patient_id);
            query.addOrderByDescending("id");
            patientVO = (BradenVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    public ValueObject findOfflineByCriteria(OfflineVO off) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        OfflineVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(off);
            query.addOrderByDescending("id");
            patientVO = (OfflineVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a single <<table>> record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param id the primary key of the <<table>> table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public ValueObject findById(int id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            patientVO = new PatientProfileVO();
            patientVO.setId(new Integer(id));
            Query query = new QueryByCriteria(patientVO);
            patientVO = (PatientProfileVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a single <<table>> record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id the patient_id of the <<table>> table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public ValueObject findByPK(int patient_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            patientVO = new PatientProfileVO();
            patientVO.setPatient_id(new Integer(patient_id));
            patientVO.setCurrent_flag(new Integer(1));
            Query query = new QueryByCriteria(patientVO);
            patientVO = (PatientProfileVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a single patient-profile_tmp record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id      the patient_id of the patient_profile_tmp table.
     * @param professional_id the currently logged in professional
     * @todo refactor out tmp table, just add a 'live' flag to the main table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public ValueObject findTMPByPK(int patient_id, int professional_id) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientProfileVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            patientVO = new PatientProfileVO();
            patientVO.setPatient_id(new Integer(patient_id));
            patientVO.setProfessional_id(new Integer(professional_id));
            patientVO.setActive(new Integer(0));
            Query query = new QueryByCriteria(patientVO);
            patientVO = (PatientProfileVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Inserts a single <<ValueObject>> object into the <<table>> table.  The value object passed
     * in has to be of type <<ValueObject>>.
     *
     * @param insertRecord fields in ValueObject represent fields in the <<table>> record.
     * @todo refactor should be in manager.
     * @since 3.0
     */
    public void update(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            PatientProfileVO patient = (PatientProfileVO) insert;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            
            PatientProfileVO vo = new PatientProfileVO();
            vo.setPatient_id(patient.getPatient_id());
            vo.setCurrent_flag(new Integer("1"));
            Collection<PatientProfileVO> patients = findAllByCriteria(vo, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            patient.setCurrent_flag(new Integer(1));
            for (PatientProfileVO currentPatient : patients) {
                currentPatient.setCurrent_flag(new Integer("0"));
                patient.setStart_date(currentPatient.getStart_date()); //Carry start date forward; PIX-1944
                broker.beginTransaction();
                broker.store(currentPatient);
                broker.commitTransaction();
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
            // throw new DataAccessException("Error in PatientDAO.insert():
            // "+e.toString(),e);
        } catch (DataAccessException e) {
            logs.error("DataAccessException thrown in PatientProfileDAO.insert(): " + e.toString(), e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insert()*************");
    }
    public void deleteOffline(ValueObject delete) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.deleteOffline()************");
        PersistenceBroker broker = null;
        try {
            OfflineVO patient = (OfflineVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            Vector<OfflineVO> result = findAllOfflineByCriteria(patient, null, true);
            broker = ConnectionLocator.getInstance().findBroker();
            for (OfflineVO tmpResultVO : result) {
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.deleteOffline(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.deleteOffline()*************");
    }
    //@todo refactr out into its own crud
    public void insertOffline(ValueObject update) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.insertOffline()************");
        PersistenceBroker broker = null;
        try {
            OfflineVO patient = (OfflineVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.insertOffline(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insertOffline()*************");
    }
    public void insert(ValueObject update) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            PatientProfileVO patient = (PatientProfileVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);
            //} catch (SQLException e) {
            //  throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.update()*************");
    }
    public void insertBraden(ValueObject update) throws DataAccessException {
        logs.info("************Entering WoundProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            BradenVO braden = (BradenVO) update;
            if(braden!=null && braden.getDeleted() == null){
                braden.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(braden);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.insertFoot()*************");

    }
    public void saveBraden(ValueObject update) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            BradenVO braden = (BradenVO) update;
            if(braden!=null && braden.getDeleted() == null){
                braden.setDeleted(0);
            }
            BradenVO vo = new BradenVO();
            vo.setPatient_id(braden.getPatient_id());
            vo.setCurrent_flag(new Integer("1"));
            Collection<BradenVO> bradens = findAllByCriteria(vo, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            braden.setCurrent_flag(new Integer(1));
            for (BradenVO currentBraden : bradens) {
                currentBraden.setCurrent_flag(new Integer("0"));
                broker.beginTransaction();
                broker.store(currentBraden);
                broker.commitTransaction();
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(braden);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);
            //} catch (SQLException e) {
            //  throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.update()*************");
    }
    public void deleteTMPByObject(PatientProfileVO delete) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.deleteTMPByObject()************");
        PersistenceBroker broker = null;
       
        try {
            delete.setActive(new Integer(0));//ensure only tmp are removed
            Collection<PatientProfileVO> result = findAllByCriteria(((PatientProfileVO) delete), 0);
            broker = ConnectionLocator.getInstance().findBroker();
            
            for (PatientProfileVO tmpResultVO : result) {
                broker.beginTransaction();
                broker.delete(tmpResultVO);
                broker.commitTransaction();
             
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.deleteTMPByObject(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.delete()*************");
    }
    public  List<Integer>  deleteFoot(ValueObject delete, Date purge_days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            Collection<FootAssessmentVO> result = findAllFeetByCriteria((FootAssessmentVO) delete, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (FootAssessmentVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (purge_days == null || last_update.before(purge_days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    
    /**********************************************************************************************************/
    
    public  List<Integer>  deleteSensory(ValueObject delete, Date purge_days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            Collection<SensoryAssessmentVO> result = findAllSensoryByCriteria((SensoryAssessmentVO) delete, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (SensoryAssessmentVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (purge_days == null || last_update.before(purge_days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    
    
    /*********************************************************************************************************/
    
    
    public  List<Integer>  deleteExam(ValueObject delete, Date purge_days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            Collection<PhysicalExamVO> result = findAllByCriteria((PhysicalExamVO) delete, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (PhysicalExamVO tmpResultVO : result) {
                java.util.Date last_update = tmpResultVO.getCreated_on();
               // long days = Long.parseLong(purge_days + "");
                if (purge_days == null || last_update.before(purge_days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    public  List<Integer>  deleteLimbBasic(ValueObject delete, Date purge_days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            Collection<LimbBasicAssessmentVO> result = findAllBasicLimbsByCriteria((LimbBasicAssessmentVO) delete, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (LimbBasicAssessmentVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (purge_days == null || last_update.before(purge_days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    public  List<Integer>  deleteLimbAdv(ValueObject delete, Date days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Collection<LimbAdvAssessmentVO> result = findAllAdvLimbsByCriteria((LimbAdvAssessmentVO) delete, 0);
            
            for (LimbAdvAssessmentVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (days == null || last_update.before(days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    public  List<Integer>  deleteLimbUpper(ValueObject delete, Date days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Collection<LimbUpperAssessmentVO> result = findAllUpperLimbsByCriteria((LimbUpperAssessmentVO) delete, 0);
            
            for (LimbUpperAssessmentVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (days == null || last_update.before(days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    public  List<Integer>  deleteBraden(ValueObject delete, Date days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Collection<BradenVO> result = findAllBradenByCriteria((BradenVO) delete, 0);
            
            for (BradenVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (days == null || last_update.before(days)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    public  List<Integer>  deleteDiagnosticInvestigation(ValueObject delete, Date date) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            Collection<DiagnosticInvestigationVO> result = findAllByCriteria((DiagnosticInvestigationVO) delete, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            for (DiagnosticInvestigationVO tmpResultVO : result) {
                Date last_update = tmpResultVO.getCreated_on();
                //int days = Integer.parseInt(purge_days + "");
                if (date == null || last_update.before(date)) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
    }
    public List<Integer> delete(ValueObject delete, Date purge_days) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        List<Integer> deleted_ids = new ArrayList();
        try {
            Collection<PatientProfileVO> result = findAllByCriteria(((PatientProfileVO) delete), 0);
            broker = ConnectionLocator.getInstance().findBroker();
            
            for (PatientProfileVO tmpResultVO : result) {
                Date last_update=null;
                boolean nfe = false;
                if (tmpResultVO.getCreated_on() == null) {
                    nfe = true;
                } else {
                    last_update = tmpResultVO.getCreated_on();
                }
               
                if (purge_days == null || (nfe == false && last_update.before(purge_days))) {
                    broker.beginTransaction();
                    broker.delete(tmpResultVO);
                    broker.commitTransaction();
                    deleted_ids.add(tmpResultVO.getId());
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            throw new DataAccessException("Error in PatientProfileDAO.insert(): " + e.toString(), e);

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return deleted_ids;
        
    }
    public ValueObject findFootByCriteria(FootAssessmentVO woundVO) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
      
        PersistenceBroker broker = null;
        FootAssessmentVO returnVO = null;
        try {
        	broker = ConnectionLocator.getInstance().findBroker();
        	if (woundVO != null && woundVO.getDeleted() == null) {
            woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (FootAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findFootByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findFootByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();                                                                                                                 
            }
        }
        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }
    /***************************************************************************************/
    public ValueObject findSensoryByCriteria(SensoryAssessmentVO woundVO) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
      
        PersistenceBroker broker = null;
        SensoryAssessmentVO returnVO = null;
        try {
        	broker = ConnectionLocator.getInstance().findBroker();
        	if (woundVO != null && woundVO.getDeleted() == null) {
            woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (SensoryAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findSensoryByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findSensoryByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving WoundProfileDAO.findByCriteria()***********************");
        return returnVO;
    }
  
    /*******************************************************************************************/
    public ValueObject findFootByCriteria(FootAssessmentVO woundVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        FootAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && !Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (FootAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findFootByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findFootByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return returnVO;
    }
    
    // method for sensory flowchart
    
    public ValueObject findSensoryByCriteria(SensoryAssessmentVO woundVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        SensoryAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && !Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (SensoryAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findSenosryByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findSensoryByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return returnVO;
    }
    
    
    
    public ValueObject findExamByCriteria(PhysicalExamVO woundVO) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        PhysicalExamVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            
            if (woundVO != null && woundVO.getDeleted() == null) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (PhysicalExamVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findFootByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findFootByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return returnVO;
    }
    public ValueObject findExamByCriteria(PhysicalExamVO woundVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findExamByCriteria()***********************");
        PersistenceBroker broker = null;
        PhysicalExamVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && !Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (PhysicalExamVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findFootByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findFootByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findExamByCriteria()***********************");
        return returnVO;
    }
    public ValueObject findAdvLimbByCriteria(LimbAdvAssessmentVO woundVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        LimbAdvAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && !Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (LimbAdvAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findLimbByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findLimbByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findLimbByCriteria()***********************");
        return returnVO;
    }
    public ValueObject findUpperLimbByCriteria(LimbUpperAssessmentVO woundVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        LimbUpperAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && !Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (LimbUpperAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findLimbByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findLimbByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findLimbByCriteria()***********************");
        return returnVO;
    }
    public ValueObject findBasicLimbByCriteria(LimbBasicAssessmentVO woundVO, boolean showDeleted) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        LimbBasicAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && !Common.getConfig("showDeleted").equals("1")) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (LimbBasicAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in WoundProfileDAO.findLimbByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findLimbByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findLimbByCriteria()***********************");
        return returnVO;
    }
    public ValueObject findBasicLimbByCriteria(LimbBasicAssessmentVO woundVO) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        LimbBasicAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && woundVO.getDeleted() == null) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (LimbBasicAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findLimbByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findLimbByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findLimbByCriteria()***********************");
        return returnVO;
    }
public ValueObject findAdvLimbByCriteria(LimbAdvAssessmentVO woundVO) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        LimbAdvAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && woundVO.getDeleted() == null) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (LimbAdvAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findLimbByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findLimbByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findLimbByCriteria()***********************");
        return returnVO;
    }
public ValueObject findUpperLimbByCriteria(LimbUpperAssessmentVO woundVO) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findFootByCriteria()***********************");
        PersistenceBroker broker = null;
        LimbUpperAssessmentVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (woundVO != null && woundVO.getDeleted() == null) {
                woundVO.setDeleted(0);
            }
            QueryByCriteria query = new QueryByCriteria(woundVO);
            query.addOrderByDescending("id");
            returnVO = (LimbUpperAssessmentVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findLimbByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in WoundProfileDAO.findLimbByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findLimbByCriteria()***********************");
        return returnVO;
    }
    /**
     * Inserts a single FootAssessmentVO object into the foot_assesmsent table.  The value object passed
     * in has to be of type FootAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the foot_assessment record.
     * @since 3.0
     */
    public void insertFoot(ValueObject update) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            FootAssessmentVO patient = (FootAssessmentVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insertFoot()*************");

    }
    /**
     * Inserts a single FootAssessmentVO object into the foot_assesmsent table.  The value object passed
     * in has to be of type FootAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the foot_assessment record.
     * @since 3.0
     */
    /****************************************************************/
    
    public void insertSensory(ValueObject update) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.update()************");
    
        PersistenceBroker broker = null;
        try {
            SensoryAssessmentVO patient = (SensoryAssessmentVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insertSensory()*************");

    }
    /**
     * Inserts a single SensoryAssessmentVO object into the Sensory_assesmsent table.  The value object passed
     * in has to be of type SensoryAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the Sensory_assessment record.
     * @since 3.0
     */
    
    
    
    
    /*****************************************************************/
    public void insertExam(ValueObject update) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            PhysicalExamVO patient = (PhysicalExamVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insertFoot()*************");

    }
    /**
     * Inserts a single FootAssessmentVO object into the foot_assesmsent table.  The value object passed
     * in has to be of type FootAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the foot_assessment record.
     * @since 3.0
     */
    public void updateExam(ValueObject update) throws DataAccessException {
        logs.info("************Entering PatientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            PhysicalExamVO patient = (PhysicalExamVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            PhysicalExamVO vo = new PhysicalExamVO();
            vo.setPatient_id(patient.getPatient_id());
            vo.setCurrent_flag(new Integer("1"));
            Collection<PhysicalExamVO> exams = findAllByCriteria(vo, 0);
            broker = ConnectionLocator.getInstance().findBroker();
            patient.setCurrent_flag(new Integer(1));
            for (PhysicalExamVO currentExam : exams) {
                currentExam.setCurrent_flag(new Integer("0"));
                broker.beginTransaction();
                broker.store(currentExam);
                broker.commitTransaction();
               
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
           
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insertFoot()*************");

    }
    /**
     * Inserts a single vLimbAssessmentVO object into the limb_assessment table.  The value object passed
     * in has to be of type LimbAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the limb_assessment record.
     * @since 3.0
     */
    public void insertLimbBasic(ValueObject update) throws DataAccessException {
        logs.info("************Entering WoundProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            LimbBasicAssessmentVO patient = (LimbBasicAssessmentVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.insertFoot()*************");

    }
 /**
     * Inserts a single vLimbAssessmentVO object into the limb_assessment table.  The value object passed
     * in has to be of type LimbAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the limb_assessment record.
     * @since 3.0
     */
    public void insertLimbAdv(ValueObject update) throws DataAccessException {
        logs.info("************Entering WoundProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            LimbAdvAssessmentVO patient = (LimbAdvAssessmentVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.insertFoot()*************");

    }
    /**
     * Inserts a single vLimbAssessmentVO object into the limb_assessment table.  The value object passed
     * in has to be of type LimbAssessmentVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the limb_assessment record.
     * @since 3.0
     */
    public void insertLimbUpper(ValueObject update) throws DataAccessException {
        logs.info("************Entering WoundProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            LimbUpperAssessmentVO patient = (LimbUpperAssessmentVO) update;
            if(patient!=null && patient.getDeleted() == null){
                patient.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with WoundProfileDAO.insertFoot()*************");

    }
    /**
     * PlaceHolder
     *
     */
    public void delete(ValueObject delete) {
        PersistenceBroker broker = null;
        try {
            // NOT Used
        } catch (Exception ex) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Deletes a single PatientProfileArraysVO object into the patient_profile_arrays table.  The value object passed
     * in has to be of type PatientProfileArraysVO.
     *
     * @param delete fields in ValueObject represent fields in the patient_profile_arrays record.
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deletePatientProfileArrays(PatientProfileArraysVO delete) {
        logs.info("************Entering patientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            PatientProfileArraysVO s = (PatientProfileArraysVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getPatient_profile_id() != null)) {
                Collection<PatientProfileArraysVO> result = findAllByCriteria(s);

                for (PatientProfileArraysVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                    
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in patientProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in patientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.delete()*************");
    }
    /**
     * Updates a single PatientProfileArraysVO object into the patient_profiles_arrays table.  The value object passed
     * in has to be of type PatientProfileArraysVO.
     *
     * @param update fields in ValueObject represent fields in the patient_profiles_arrays record.
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 4.1.2
     */
    public void savePatientProfileArrays(PatientProfileArraysVO update) throws DataAccessException {
        logs.info("************Entering patientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            PatientProfileArraysVO assessment = (PatientProfileArraysVO) update;

            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in patientProfileDAO.update(): " + e.toString(), e);
            throw new DataAccessException("patientProfileDAO throwing SQLException: " + e.getMessage());

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in patientProfileDAO.update(): " + e.toString(), e);
        }  catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.update()*************");

    }
    /**
     * Deletes a single InvestigationsVO object into the investigations table.  The value object passed
     * in has to be of type InvestigationsVO.
     *
     * @param delete fields in ValueObject represent fields in the investigations record.
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deleteInvestigation(InvestigationsVO delete) {
        logs.info("************Entering patientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            InvestigationsVO s = (InvestigationsVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getDiagnostic_id() != null)) {
                Collection<InvestigationsVO> result = findAllByCriteria(s);

                for (InvestigationsVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                    
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in patientProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in patientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.delete()*************");
    }
    /**
     * Updates a single InvestigationsVO object into the inigations table.  The value object passed
     * in has to be of type InvestigationsVO.
     *
     * @param update fields in ValueObject represent fields in the patient_profiles_arrays record.
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 4.1.2
     */
    public void saveInvestigation(InvestigationsVO update) throws DataAccessException {
        logs.info("************Entering patientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            InvestigationsVO assessment = (InvestigationsVO) update;

            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in patientProfileDAO.update(): " + e.toString(), e);
            throw new DataAccessException("patientProfileDAO throwing SQLException: " + e.getMessage());

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in patientProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.update ");
    }
  /**
     * Deletes a single InvestigationsVO object into the investigations table.  The value object passed
     * in has to be of type InvestigationsVO.
     *
     * @param delete fields in ValueObject represent fields in the investigations record.
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 4.1.2
     */
    public void deleteDiagnosticInvestigation(DiagnosticInvestigationVO delete) {
        logs.info("************Entering patientProfileDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            DiagnosticInvestigationVO s = (DiagnosticInvestigationVO) delete;
            broker = ConnectionLocator.getInstance().findBroker();
            if (s != null && (s.getId() != null)) {
                Collection<DiagnosticInvestigationVO> result = findAllByCriteria(s);

                for (DiagnosticInvestigationVO prodResultVO : result) {
                    broker.beginTransaction();
                    broker.delete(prodResultVO);
                    broker.commitTransaction();
                    
                }
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in patientProfileDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in patientProfileDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.delete()*************");
    }
    /**
     * Updates a single DiagnosticInvestigationVO object into the diagnostic_investigation table.  The value object passed
     * in has to be of type InvestigationsVO.
     *
     * @param update fields in ValueObject represent fields in the patient_profiles_arrays record.
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @todo refactor into its own AssessmentDAO
     * @since 6.0
     */
    public void saveDiagnosticInvestigation(DiagnosticInvestigationVO update) throws DataAccessException {
        logs.info("************Entering patientProfileDAO.update()************");
        PersistenceBroker broker = null;
        try {
            DiagnosticInvestigationVO assessment = (DiagnosticInvestigationVO) update;
            if(assessment!=null && assessment.getDeleted() == null){
                assessment.setDeleted(0);
            }
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(assessment);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in patientProfileDAO.update(): " + e.toString(), e);
            throw new DataAccessException("patientProfileDAO throwing SQLException: " + e.getMessage());

        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in patientProfileDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with patientProfileDAO.update()*************");

    }
    public Collection findAllByCriteria(PatientProfileArraysVO crit) throws
            DataAccessException {
        logs.info("***********************Entering patientProfileDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in patientProfileDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in patientProfileDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving patientProfileDAO.findAllByCriteria()***********************");
        return results;
    }
    public Collection findAllByCriteria(InvestigationsVO crit) throws
            DataAccessException {
        logs.info("***********************Entering patientProfileDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in patientProfileDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in patientProfileDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving patientProfileDAO.findAllByCriteria()***********************");
        return results;
    }
    public Collection findAllByCriteria(DiagnosticInvestigationVO crit) throws
            DataAccessException {
        logs.info("***********************Entering patientProfileDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(crit);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in patientProfileDAO.findAllByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in patientProfileDAO.findAllByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving patientProfileDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<PhysicalExamVO> findExamFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PhysicalExamVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on", PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(PhysicalExamVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findExamBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        PhysicalExamVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(PhysicalExamVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (PhysicalExamVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<BradenVO> findBradenFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<BradenVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on", PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(BradenVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findBradenBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        BradenVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(BradenVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (BradenVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<LimbBasicAssessmentVO> findBasicLimbFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LimbBasicAssessmentVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on",  PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(LimbBasicAssessmentVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
/**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<LimbAdvAssessmentVO> findAdvLimbFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LimbAdvAssessmentVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on",  PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(LimbAdvAssessmentVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<LimbUpperAssessmentVO> findUpperLimbFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<LimbUpperAssessmentVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on",  PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(LimbUpperAssessmentVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findAdvLimbBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        LimbAdvAssessmentVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(LimbAdvAssessmentVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (LimbAdvAssessmentVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findUpperLimbBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        LimbUpperAssessmentVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(LimbUpperAssessmentVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (LimbUpperAssessmentVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    
/**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findBasicLimbBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        LimbBasicAssessmentVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(LimbBasicAssessmentVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (LimbBasicAssessmentVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a all <<table>> records, as specified by the date, and professional.  If no record is found a null value is returned.
     *
     * @param date the day we need to search for records.   We take this date, and find the start/end value of the day.
     * @param professional_id we only find records done by this professional
     * @since 4.2
     */
    public Collection<FootAssessmentVO> findFootFromDate(int patient_id, Date date, int professional_id) throws DataAccessException {
        logs.info("***********************Entering PatientProfileDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<FootAssessmentVO> r = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date!=null && professional_id > 0) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addEqualTo("professional_id", professional_id);
                crit.addGreaterOrEqualThan("created_on", PDate.getStartOfDay(date));
                crit.addLessOrEqualThan("created_on", PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(FootAssessmentVO.class, crit);
                r = (Collection) broker.getCollectionByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in PatientProfileDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in PatientProfileDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving PatientProfileDAO.findByCriteria()***********************");
        return r;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 4.2
     */
    public ValueObject findFootBeforeDate(int patient_id, Date date) throws DataAccessException {
        logs.info("********* Entering the PatientProfileDAO.findByPK****************");
        PersistenceBroker broker = null;
        FootAssessmentVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            if (date !=null) {
                Criteria crit = new Criteria();
                QueryByCriteria query = null;
                crit.addEqualTo("active", new Integer(1));
                crit.addEqualTo("deleted", new Integer(0));
                crit.addEqualTo("patient_id", patient_id);
                crit.addLessOrEqualThan("created_on",PDate.getEndOfDay(date));
                query = QueryFactory.newQuery(FootAssessmentVO.class, crit);
                query.addOrderByDescending("created_on");
                patientVO = (FootAssessmentVO) broker.getObjectByQuery(query);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientProfileDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patient_id fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @todo rename patient_id to patientVO
     * @since 4.2
    */
  public ExternalReferralVO findByCriteria(ExternalReferralVO patient_id) throws DataAccessException {
      logs.info("********* Entering the PatientProfileDAO.findByPK****************");
      PersistenceBroker broker = null;
      ExternalReferralVO patientVO = null;
      try {
          broker = ConnectionLocator.getInstance().findBroker();
 
          QueryByCriteria query = new QueryByCriteria(patient_id);
          query.addOrderByDescending("id");
          patientVO = (ExternalReferralVO) broker.getObjectByQuery(query);
      } catch (ConnectionLocatorException e) {
          logs.error("PersistanceBrokerException thrown in PatientProfileDAO.findByPK(): " + e.toString(), e);
          throw new DataAccessException("Error in PatientProfileDAO.findByPK(): " + e.toString(), e);
      } finally {
          if (broker != null) {
              broker.close();
          }
      }
      logs.info("********* Done the PatientProfileDAO.findByPK");
      return patientVO;
  }
  public  void  deleteExternalReferral(ValueObject delete)  {
      logs.info("************Entering PatientProfileDAO.delete()************");
      PersistenceBroker broker = null;
      try {
          broker = ConnectionLocator.getInstance().findBroker();
          
          ExternalReferralVO s = (ExternalReferralVO)delete;
          Collection<ExternalReferralVO> result = findAllByCriteria(s);
              for (ExternalReferralVO prodResultVO : result) {
                  broker.beginTransaction();
                  broker.delete(prodResultVO);
                  broker.commitTransaction();
              }
      } catch (PersistenceBrokerException e) {
          broker.abortTransaction();
          logs.error("PersistanceBrokerException thrown in patientProfileDAO.delete(): " + e.toString(), e);
          e.printStackTrace();
 
      } catch (ConnectionLocatorException e) {
          logs.error("connectionLocatorException thrown in PatientProfileDAO.delete(): " + e.toString(), e);
      } catch (com.pixalere.common.DataAccessException e) {
          logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
      }  finally {
          if (broker != null) {
              broker.close();
          }
      }
     
  }
  /**
   * Inserts a single ExternalReferralVO object into the limb_assessment table.  The value object passed
   * in has to be of type ExternalReferralVO.
   *
   * @param insertRecord fields in ValueObject represent fields in the limb_assessment record.
   * @since 3.0
   */
  public void insertExternalReferral(ValueObject update) throws DataAccessException {
      logs.info("************Entering WoundProfileDAO.update()************");
      PersistenceBroker broker = null;
 
      try {
          ExternalReferralVO patient = (ExternalReferralVO) update;
          broker = ConnectionLocator.getInstance().findBroker();
          broker.beginTransaction();
          broker.store(patient);
          broker.commitTransaction();
          
      } catch (PersistenceBrokerException e) {
          broker.abortTransaction();
          logs.error("PersistanceBrokerException thrown in WoundProfileDAO.update(): " + e.toString(), e);
          e.printStackTrace();
      } catch (ConnectionLocatorException e) {
          logs.error("connectionLocatorException thrown in WoundProfileDAO.update(): " + e.toString(), e);
      } catch (com.pixalere.common.ApplicationException e) {
          logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
      } finally {
          if (broker != null) {
              broker.close();
          }
      }
      logs.info("*************Done with WoundProfileDAO.insertFoot()*************");

  }
  public Collection<ExternalReferralVO> findAllByCriteria(ExternalReferralVO crit) throws
          DataAccessException {
      logs.info("***********************Entering patientProfileDAO.findAllByCriteria()***********************");
      PersistenceBroker broker = null;
      Collection<ExternalReferralVO> results = null;
 
      try {
          broker = ConnectionLocator.getInstance().findBroker();
          QueryByCriteria query = new QueryByCriteria(crit);
          results = (Collection) broker.getCollectionByQuery(query);
      } catch (ConnectionLocatorException e) {
          logs.error("ServiceLocatorException thrown in patientProfileDAO.findAllByCriteria(): "
                  + e.toString(), e);
          throw new DataAccessException(
                  "ServiceLocatorException in patientProfileDAO.findAllByCriteria()", e);
      } finally {
          if (broker != null) {
              broker.close();
          }
      }
      logs.info("***********************Leaving patientProfileDAO.findAllByCriteria()***********************");
      return results;
  }
}
