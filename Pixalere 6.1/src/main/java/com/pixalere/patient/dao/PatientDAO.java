package com.pixalere.patient.dao;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.bean.PatientsVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.utils.*;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.PBFactoryException;
import org.apache.ojb.broker.query.*;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Collection;
import java.util.Vector;
import com.pixalere.utils.Constants;
/**
 * This   class is responsible for handling all CRUD logic associated with the
 * patient_accounts    table.
 *
 * @todo Need to implement this class as a singleton.
 */
public class PatientDAO implements DataAccessObject {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(PatientDAO.class);
    public PatientDAO() {
    }
    /**
     * Inserts a single PatientAccountsVO object into the patient_accounts table.  The value object passed
     * in has to be of type PatientAccountsVO.
     *
     * @param pats fields in ValueObject represent fields in the patient_accounts record.
     * @return int returns the patient_id of the newly created Patient.
     * @since 3.0
     */
    public int insertNewPatient(PatientsVO pats) {
        logs.info("************Entering WoundProfileDAO.insert()************");
        PersistenceBroker broker = null;
        PatientsVO returnVO = null;
        try {
            System.out.println("Pat1 "+pats.getCreated_on());
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(pats);
            broker.commitTransaction();
           // PatientsVO p = new PatientsVO();
            System.out.println("Pat2 "+pats.getCreated_on());
            //p.setCreated_on(pats.getCreated_on());
            //p.setProfessional_id(pats.getProfessional_id());
            
            QueryByCriteria query = new QueryByCriteria(pats);
            query.addOrderByDescending("id");
            returnVO = (PatientsVO) broker.getObjectByQuery(query);
            
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in WoundProfileDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientProfileDAO.insert()*************");
        return returnVO.getId().intValue();
    }
    /**
     * Finds a all patient_accounts records by anyone of the passed in values.
     *
     * @param id       search by patient_id
     * @param lastname search patients last name
     * @param phn      search by patients phn
     * @todo refactor this out, use findAllByCriteria instead.
     * @since 3.0
     */
    public List findAllBySearch(int id, String lastname, String phn, int treatment_location_id, String otherSearch, String parisSearch, String oidSearch,  boolean includeInactive) throws DataAccessException {
        logs.info("************Entering the PatientDAO.findAllByCriteria()***************");
        PersistenceBroker broker = null;
        Collection<PatientAccountVO> results = null;
        ArrayList<PatientAccountVO> patients = new ArrayList<PatientAccountVO>();
        PatientAccountVO patientVO = null;
        try {
            Criteria crit = new Criteria();
            crit.addEqualTo("current_flag", "1");
            //crit.addEqualTo("account_status", "1"); get all regardless of status
            //clean garbage out of phn
            if(phn!=null && !phn.equals("")){
                phn = phn.trim();
                phn = phn.replaceAll("-", "");
            }

            if (id > 0) {
                crit.addEqualTo("patient_id", id);
            } else {
                if (phn != null && !phn.equals("")) {
                    crit.addEqualTo("phn", phn);
                }else {
                    if (lastname!=null && !lastname.equals("")) {
                        
                        crit.addEqualTo("lastname_search",Common.stripName(lastname.toUpperCase()));
                        //crit.addLike("lastname","%"+(Common.isOffline()?decrypt.getDecrypted(lastname, Constants.LASTNAME_KEY):Common.stripName(lastname.toLowerCase()))+"%");
                    }
                    if (treatment_location_id > 0) {
                        crit.addEqualTo("treatment_location_id",treatment_location_id);
                    }
                }
            
            }
                
            if (otherSearch!=null && !otherSearch.equals("")) {
                crit.addEqualTo("phn", otherSearch);
            }
            if (parisSearch!=null && !parisSearch.equals("")) {
                crit.addEqualTo("phn3", parisSearch);
            }
            if (oidSearch!=null && !oidSearch.equals("")) {
                crit.addEqualTo("phn2", oidSearch);
            }
            if (includeInactive == false) {
                crit.addEqualTo("account_status","1");
            }
            QueryByCriteria query = QueryFactory.newQuery(PatientAccountVO.class, crit);
            query.addOrderByAscending("lastname");query.addOrderByDescending("patient_id");
            broker = ConnectionLocator.getInstance().findBroker();
            query.setStartAtIndex(0);
            query.setEndAtIndex(150);
            results = (Collection) broker.getCollectionByQuery(query);
            
            //decrypt
            for (PatientAccountVO p : results) {
                p.setFirstName(p.getDecrypted(p.getFirstName(), Constants.FIRSTNAME_KEY));
                p.setLastName(p.getDecrypted(p.getLastName(), Constants.LASTNAME_KEY));
                p.setPhn(p.getDecrypted(p.getPhn(), Constants.PHN_KEY));
                patients.add(p);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in StoryDAO.findAllStories(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException error in StoryDAO.findAllStories()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("************Done with StoryDAO.findAllStories()***************");
        return patients;
    }
    /**
     * Finds a all patient_accounts records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 3.0
     * @deprecated
     * @todo check if its still used, delete if not.
     */
    public List findAllByCriteria(PatientAccountVO pa, boolean desc) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PatientAccountVO> results = null;
        PatientAccountVO returnVO = null;
        ArrayList<PatientAccountVO> patients = new ArrayList<PatientAccountVO>();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new PatientAccountVO();
            pa.setCurrent_flag(new Integer(1));
            if (pa.getLastName() != null && !pa.getLastName().equals("")) {
                pa.setLastName(pa.getEncrypted(pa.getLastName(), Constants.LASTNAME_KEY));
            }
            if (pa.getFirstName() != null && !pa.getFirstName().equals("")) {
                pa.setFirstName(pa.getEncrypted(pa.getFirstName(), Constants.FIRSTNAME_KEY));
            }
            if (pa.getLastName_search() != null && !pa.getLastName_search().equals("")) {
                pa.setLastName_search(pa.getEncrypted(pa.getLastName_search(), Constants.LASTNAME_KEY));
            }
            if (pa.getPhn() != null && !pa.getPhn().equals("")) {
                pa.setPhn(pa.getEncrypted(pa.getPhn(), Constants.PHN_KEY));
            }
            QueryByCriteria query = new QueryByCriteria(pa);
            if (desc) {
                query.addOrderByDescending("patient_id");
            } else {
                query.addOrderByAscending("patient_id");
            }
            results = (Collection) broker.getCollectionByQuery(query);
            //decrypt
            for (PatientAccountVO p : results) {
                p.setFirstName(p.getDecrypted(p.getFirstName(), Constants.FIRSTNAME_KEY));
                p.setLastName(p.getDecrypted(p.getLastName(), Constants.LASTNAME_KEY));
                p.setPhn(p.getDecrypted(p.getPhn(), Constants.PHN_KEY));
                patients.add(p);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return patients;
    }
    /**
     * Finds a all patient_accounts records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 3.0
     */
    public List findAllByCriteria(PatientAccountVO pa, int start, int end, boolean desc) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PatientAccountVO> results = null;
        PatientAccountVO returnVO = null;
        ArrayList<PatientAccountVO> patients = new ArrayList<PatientAccountVO>();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new PatientAccountVO();
            pa.setCurrent_flag(new Integer(1));
            QueryByCriteria query = new QueryByCriteria(pa);
            if (desc) {
                query.addOrderByDescending("patient_id");
            } else {
                query.addOrderByAscending("patient_id");
            }
            query.setStartAtIndex(start);
            query.setEndAtIndex(end);
            results = (Collection) broker.getCollectionByQuery(query);
            //decrypt
            for (PatientAccountVO p : results) {
                p.setFirstName(p.getDecrypted(p.getFirstName(), Constants.FIRSTNAME_KEY));
                p.setLastName(p.getDecrypted(p.getLastName(), Constants.LASTNAME_KEY));
                p.setPhn(p.getDecrypted(p.getPhn(), Constants.PHN_KEY));
                patients.add(p);
            }
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return patients;
    }
    /**
     * Finds an patient_accounts record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param patientsVO fields in ValueObject which are not null will be used as
     *                   parameters in the SQL statement.
     * @since 3.0
     */
    public int findLast(PatientAccountVO patientVO) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findLast****************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(patientVO);
            if (com.pixalere.utils.Common.isOffline() == false) {
                patientVO.setPatient_id(null);
            }
            query.addOrderByDescending("patient_id");
            patientVO = (PatientAccountVO) broker.getObjectByQuery(query);
            //patientVO.setFirstName(patientVO.getDecrypted(patientVO.getFirstName(), Constants.FIRSTNAME_KEY));
            //patientVO.setLastName(patientVO.getDecrypted(patientVO.getLastName(), Constants.LASTNAME_KEY));
            //patientVO.setPhn(patientVO.getDecrypted(patientVO.getPhn(), Constants.PHN_KEY));
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByPK");
        //legacy not needed.. need to remove.
        return (patientVO.getPatient_id()).intValue();
    }
    /**
     * Finds a single patient_accounts record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param PatientAccount the object contain the criteria
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.3
     */
    public ValueObject findByCriteria(ValueObject vo) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientAccountVO patientVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            patientVO = (PatientAccountVO) vo;
            Query query = new QueryByCriteria(patientVO);
            patientVO = (PatientAccountVO) broker.getObjectByQuery(query);
            if (patientVO != null) {
                patientVO.setFirstName(patientVO.getDecrypted(patientVO.getFirstName(), Constants.FIRSTNAME_KEY));
                patientVO.setLastName(patientVO.getDecrypted(patientVO.getLastName(), Constants.LASTNAME_KEY));
                patientVO.setPhn(patientVO.getDecrypted(patientVO.getPhn(), Constants.PHN_KEY));
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByPK");
        return patientVO;
    }
    /**
     * Finds a single patient_accounts record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param phn        the primary phn
     * @param mrn        is like mrn or urn
     * @param validation is the scheme used to get the data
     * @since 4.1.1
     */
    public Hashtable findByCriteria(String phn, String mrn, String validation) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientAccountVO patientVO = null;
        Hashtable result = new Hashtable();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //patientVO = new PatientAccountVO();
            Criteria crit = new Criteria();
            crit.addEqualTo("current_flag", "1");
            //crit.addEqualTo("account_status", "1"); get all regardless of status
            //clean garbage out of phn
            if(phn!=null){
                phn = phn.trim();
                phn = phn.replaceAll("-", "");
            }
            if (validation.equals("phn") && phn != null && !phn.equals("")) {
                crit.addEqualTo("phn", phn);
                Query query = QueryFactory.newQuery(PatientAccountVO.class, crit);
                patientVO = (PatientAccountVO) broker.getObjectByQuery(query);
                    result.put("criteria","phn");
            } else if (validation.equals("secondary_phn") && mrn != null && !mrn.equals("")) {
                crit.addEqualTo("phn2", mrn);
                Query query = QueryFactory.newQuery(PatientAccountVO.class, crit);
                patientVO = (PatientAccountVO) broker.getObjectByQuery(query);
                    result.put("criteria","secondary_phn");
            } else if (validation.equals("secondary_phn or phn")) {
                if (mrn != null && !mrn.equals("")) {
                    crit.addEqualTo("phn2", mrn);
                    Query query = QueryFactory.newQuery(PatientAccountVO.class, crit);
                    patientVO = (PatientAccountVO) broker.getObjectByQuery(query);
                    result.put("criteria","secondary_phn");
                }
                if(patientVO == null && phn != null && !phn.equals("")) {
                    Criteria crit2 = new Criteria();
                    crit2.addEqualTo("phn", phn);
                    crit2.addEqualTo("current_flag", "1");
                    Query query2 = QueryFactory.newQuery(PatientAccountVO.class, crit2);
                    patientVO = (PatientAccountVO) broker.getObjectByQuery(query2);
                    result.put("criteria","phn");
                }
            } else if (validation.equals("phn or secondary_phn")) {
                if (phn != null && !phn.equals("")) {
                    crit.addEqualTo("phn", phn);
                    Query query = QueryFactory.newQuery(PatientAccountVO.class, crit);
                    patientVO = (PatientAccountVO) broker.getObjectByQuery(query);
                    result.put("criteria","phn");
                }
                if (patientVO == null && mrn != null && !mrn.equals("")) {
                    Criteria crit2 = new Criteria();
                    crit2.addEqualTo("current_flag", "1");
                    crit2.addEqualTo("phn2", mrn);
                    Query query2 = QueryFactory.newQuery(PatientAccountVO.class, crit2);
                    patientVO = (PatientAccountVO) broker.getObjectByQuery(query2);
                    result.put("criteria","secondary_phn");
                }
            }
            if (patientVO != null) {
                patientVO.setFirstName(patientVO.getDecrypted(patientVO.getFirstName(), Constants.FIRSTNAME_KEY));
                patientVO.setLastName(patientVO.getDecrypted(patientVO.getLastName(), Constants.LASTNAME_KEY));
                patientVO.setPhn(patientVO.getDecrypted(patientVO.getPhn(), Constants.PHN_KEY));
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        if(patientVO!=null){
            result.put("object",patientVO);
        }
        logs.info("********* Done the PatientDAO.findByPK");
        return result;
    }
    /**
     * Finds a single patient_accounts record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the patient_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public ValueObject findByPK(int patient_id) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByPK****************");
        PersistenceBroker broker = null;
        PatientAccountVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            PatientAccountVO patientVO = new PatientAccountVO();
            patientVO.setPatient_id(new Integer(patient_id));
            patientVO.setCurrent_flag(new Integer(1));
            Query query = new QueryByCriteria(patientVO);
            resultVO = (PatientAccountVO) broker.getObjectByQuery(query);
            if (resultVO != null) {
                resultVO.setFirstName(resultVO.getDecrypted(resultVO.getFirstName(), Constants.FIRSTNAME_KEY));
                resultVO.setLastName(resultVO.getDecrypted(resultVO.getLastName(), Constants.LASTNAME_KEY));
                resultVO.setPhn(resultVO.getDecrypted(resultVO.getPhn(), Constants.PHN_KEY));
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByPK");
        return resultVO;
    }
    /**
     * Finds a single patient_accounts record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the patient_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 3.0
     */
    public ValueObject findByID(int rec_id) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        PatientAccountVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            PatientAccountVO patientVO = new PatientAccountVO();
            patientVO.setId(new Integer(rec_id));
            Query query = new QueryByCriteria(patientVO);
            resultVO = (PatientAccountVO) broker.getObjectByQuery(query);
            if (resultVO != null) {
                resultVO.setFirstName(resultVO.getDecrypted(resultVO.getFirstName(), Constants.FIRSTNAME_KEY));
                resultVO.setLastName(resultVO.getDecrypted(resultVO.getLastName(), Constants.LASTNAME_KEY));
                resultVO.setPhn(resultVO.getDecrypted(resultVO.getPhn(), Constants.PHN_KEY));
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }
    /**
     * Inserts a single PatientAccountsVO object into the patient_accounts table.  The value object passed
     * in has to be of type PatientAccountsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the patient_accounts record.
     * @since 3.0
     */
    public void insert(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            PatientAccountVO patient = (PatientAccountVO) insert;
            patient.setPhn(patient.getEncrypted(patient.getPhn(), Constants.PHN_KEY));
            patient.setFirstName(patient.getEncrypted(patient.getFirstName(), Constants.FIRSTNAME_KEY));
            patient.setLastName(patient.getEncrypted(patient.getLastName(), Constants.LASTNAME_KEY));
            patient.setLastName_search(patient.getEncrypted(patient.getLastName_search(), Constants.LASTNAME_KEY));
            PatientAccountVO vo = new PatientAccountVO();
            if (patient.getPatient_id() == null) {
                vo.setPhn(patient.getPhn());//if patient_id is null use phn -- for meditech interface.
                if (patient.getPhn().equals("")) {
                    vo.setPhn2(patient.getPhn2());
                }
            }
            vo.setPatient_id(patient.getPatient_id());
            vo.setCurrent_flag(new Integer("1"));
            Collection patients = findAllByCriteria(vo, true);
            Iterator iter = patients.iterator();

            while (iter.hasNext()) {
                PatientAccountVO currentPatient = (PatientAccountVO) iter.next();
                currentPatient.setCurrent_flag(new Integer("0"));
                broker.beginTransaction();
                broker.store(currentPatient);
                broker.commitTransaction();
            }

            broker.beginTransaction();
            patient.setId(null);
            broker.store(patient);
            broker.commitTransaction();
         } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single PatientAccountsVO object into the patient_accounts table.  The value object passed
     * in has to be of type PatientAccountsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the patient_accounts record.
     * @since 3.0
     */
    public void update(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            PatientAccountVO patient = (PatientAccountVO) update;
            patient.setPhn(patient.getEncrypted(patient.getPhn(), Constants.PHN_KEY));
            patient.setFirstName(patient.getEncrypted(patient.getFirstName(), Constants.FIRSTNAME_KEY));
            patient.setLastName(patient.getEncrypted(patient.getLastName(), Constants.LASTNAME_KEY));
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(patient);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Deletes patient_accounts records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 3.0
     */
    public void delete(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            PatientAccountVO patient = (PatientAccountVO) delete;
            Collection patients = findAllByCriteria(patient, true);
            Iterator iter = patients.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                PatientAccountVO currentPatient = (PatientAccountVO) iter.next();
                broker.beginTransaction();
                broker.delete(currentPatient);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    public int getPatientCount(int treatment_id) throws DataAccessException {
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            PatientAccountVO vo = new PatientAccountVO();
            vo.setTreatment_location_id(treatment_id);
            QueryByCriteria query = new QueryByCriteria(vo);
            return broker.getCount(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
    }
    public Collection findAllByCriteria(ValueObject vo) throws DataAccessException {
        PersistenceBroker broker = null;
        PatientAccountVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            PatientAccountVO productsVO = (PatientAccountVO) vo;
            QueryByCriteria query = new QueryByCriteria(productsVO);
            Collection<PatientAccountVO> col = (Collection) broker.getCollectionByQuery(query);
            return col;
        } catch (ConnectionLocatorException e) {
        } catch (Exception e) {
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return null;
    }
    public Vector findAllBySearch(int patient_status, String[] treatmentLocations) throws DataAccessException {
        
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            //crit.addEqualTo("patient_id", new Integer(patient_id));
            //crit.addEqualTo("wound_id", new Integer(wound_id));
            //crit.addEqualTo("visit", 1);
            crit.addEqualTo("current_flag", 1);
            if (patient_status == 1) {
                crit.addEqualTo("account_status", 1);
            } else if (patient_status == 2) {
                crit.addEqualTo("account_status", 0);
            }
            // TREATMENT LOCATIONS
            //System.out.println(treatmentLocations);
            try {
                if (!(treatmentLocations == null)) {
                    if (treatmentLocations.length > 0) {
                        //System.out.println("treatmentlocations found");
                        Criteria crit2 = new Criteria();
                        for (int intTreatmentLocations = 0; intTreatmentLocations < treatmentLocations.length; intTreatmentLocations++) {
                            Criteria crit3 = new Criteria();
                            crit3.addEqualTo("treatment_location_id", treatmentLocations[intTreatmentLocations]);
                            crit2.addOrCriteria(crit3);
                        }
                        crit.addAndCriteria(crit2);
                        //System.out.println("treatmentlocations out:" + crit.toString());
                    }
                }
            } catch (Exception e) {
                logs.error("problem add:"+e.getMessage());
            }
            
            ReportQueryByCriteria q = QueryFactory.newReportQuery(PatientAccountVO.class, crit);
            q.setAttributes(new String[]{"id", "firstName", "lastname", "account_status", "discharge_reason", "patient_id", "treatment_location_id"});
            q.addOrderByAscending("firstName");
            q.addOrderByAscending("lastname");
             q.setStartAtIndex(0);
            q.setEndAtIndex(600);
            iter = broker.getReportQueryIteratorByQuery(q);
            
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
}
