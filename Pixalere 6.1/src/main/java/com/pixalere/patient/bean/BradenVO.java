package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
import java.util.Date;
import java.io.Serializable;
import com.pixalere.auth.bean.ProfessionalVO;
 /**
 *     This is the BradenVO  bean for the braden  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientProfileServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 */
public class BradenVO extends ValueObject implements Serializable {
    private Integer braden_moisture;
    private Integer braden_friction;
    private Integer braden_nutrition;
    private Integer braden_mobility;
    private Integer braden_sensory;
    private Integer braden_activity;
    private Integer id;
    private Integer current_flag;
    
    
    private Date lastmodified_on;
    private Integer patient_id;
    private ProfessionalVO professional;
    private Integer professional_id;
    private Date created_on;
    private String user_signature;
    private Integer active;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
     public Integer getDeleted() {
         return deleted;
     }
     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }
     public String getDeleted_reason() {
         return deleted_reason;
     }
     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }
     public String getDelete_signature() {
         return delete_signature;
     }
     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    public BradenVO (){
        
    }
    
    public Integer getBraden_moisture() {
        return braden_moisture;
    }
    public void setBraden_moisture(Integer braden_moisture) {
        this.braden_moisture = braden_moisture;
    }
    public Integer getBraden_friction() {
        return braden_friction;
    }
    public void setBraden_friction(Integer braden_friction) {
        this.braden_friction = braden_friction;
    }
    public Integer getBraden_nutrition() {
        return braden_nutrition;
    }
    public void setBraden_nutrition(Integer braden_nutrition) {
        this.braden_nutrition = braden_nutrition;
    }
    public Integer getBraden_mobility() {
        return braden_mobility;
    }
    public void setBraden_mobility(Integer braden_mobility) {
        this.braden_mobility = braden_mobility;
    }
    public Integer getBraden_sensory() {
        return braden_sensory;
    }
    public void setBraden_sensory(Integer braden_sensory) {
        this.braden_sensory = braden_sensory;
    }
    public Integer getBraden_activity() {
        return braden_activity;
    }
    public void setBraden_activity(Integer braden_activity) {
        this.braden_activity = braden_activity;
    }
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }

    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getCurrent_flag() {
        return current_flag;
    }
    public void setCurrent_flag(Integer current_flag) {
        this.current_flag = current_flag;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
  private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
    
}