/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.pixalere.auth.bean.ProfessionalVO;

/**
 *
 * @author travis
 */
public class PhysicalExamVO extends ValueObject implements Serializable {
    private Integer id;
    private Date created_on;
    private Integer professional_id;
    private Integer patient_id;
    private Double temperature;
    private Integer resp_rate;
    private Integer pulse;
    
    private Double height;
    private Double weight;
    private  String blood_pressure;
    private Date lastmodified_on;
    private String user_signature;
    private Integer active;
    private Integer deleted;
    private String deleted_reason;
    private String delete_signature;
    private Date deleted_on;
    private ProfessionalVO professional;
    private Integer current_flag;
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
    public String getDate(Date date) {
        if (date != null) {
            DateFormat d = new SimpleDateFormat("dd/MMMMM/yyyy");
            String value = d.format(date);
            return value;
        }
        return "";
    }
   
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    /**
     * @return the temperature
     */
    public Double getTemperature() {
        return temperature;
    }
    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }
    /**
     * @return the resp_rate
     */
    public Integer getResp_rate() {
        return resp_rate;
    }
    /**
     * @param resp_rate the resp_rate to set
     */
    public void setResp_rate(Integer resp_rate) {
        this.resp_rate = resp_rate;
    }
    /**
     * @return the pulse
     */
    public Integer getPulse() {
        return pulse;
    }
    /**
     * @param pulse the pulse to set
     */
    public void setPulse(Integer pulse) {
        this.pulse = pulse;
    }
    /**
     * @return the user_signature
     */
    public String getUser_signature() {
        return user_signature;
    }
    /**
     * @param user_signature the user_signature to set
     */
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }
    /**
     * @return the deleted
     */
    public Integer getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    /**
     * @return the delete_reason
     */
    public String getDeleted_reason() {
        return deleted_reason;
    }
    /**
     * @param delete_reason the delete_reason to set
     */
    public void setDeleted_reason(String deleted_reason) {
        this.deleted_reason = deleted_reason;
    }
    /**
     * @return the delete_signature
     */
    public String getDelete_signature() {
        return delete_signature;
    }
    /**
     * @param delete_signature the delete_signature to set
     */
    public void setDelete_signature(String delete_signature) {
        this.delete_signature = delete_signature;
    }
    /**
     * @return the height
     */
    public Double getHeight() {
        return height;
    }
    /**
     * @param height the height to set
     */
    public void setHeight(Double height) {
        this.height = height;
    }
    /**
     * @return the weight
     */
    public Double getWeight() {
        return weight;
    }
    /**
     * @param weight the weight to set
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }
    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }
    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    /**
     * @return the blood_pressure
     */
    public String getBlood_pressure() {
        return blood_pressure;
    }
    /**
     * @param blood_pressure the blood_pressure to set
     */
    public void setBlood_pressure(String blood_pressure) {
        this.blood_pressure = blood_pressure;
    }

    /**
     * @return the current_flag
     */
    public Integer getCurrent_flag() {
        return current_flag;
    }
    /**
     * @param current_flag the current_flag to set
     */
    public void setCurrent_flag(Integer current_flag) {
        this.current_flag = current_flag;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
      private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
}
