/*
 * LimbAssessmentVO.java
 *
 * Created on September 17, 2007, 2:42 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Date;
/**
 *     This is the LimbAssessmentVO  bean for the limb_assessment  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientProfileServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class LimbUpperAssessmentVO extends ValueObject implements Serializable{
    private String user_signature;
    private String limb_comments;
    private Date created_on;
    private Integer left_less_capillary;
    private Integer right_less_capillary;
    private Integer left_more_capillary;
    private Integer right_more_capillary;
    private Integer left_range_motion_arm;
    private Integer right_range_motion_arm;
    private Integer left_range_motion_hand;
    private Integer right_range_motion_hand;
    private Integer left_range_motion_fingers;
    private Integer right_range_motion_fingers;
 private Date deleted_on;
     private Date lastmodified_on;
    private Integer limb_show;
    private Integer active;
    private Integer professional_id;
    private ProfessionalVO professional;
    private String left_missing_limbs;
    private String right_missing_limbs;
    private Integer id;
    private Integer patient_id;
    private String left_pain_assessment;
    private String right_pain_assessment;
    private String pain_comments;
    private String left_skin_assessment;
    private String right_skin_assessment;
    private Integer left_temperature_arm;
    private Integer right_temperature_arm;
    private Integer left_temperature_hand;
    private Integer right_temperature_hand;
    private Integer left_temperature_fingers;
    private Integer right_temperature_fingers;
    
    private Integer left_skin_colour_arm;
    private Integer right_skin_colour_arm;
    private Integer left_skin_colour_hand;
    private Integer right_skin_colour_hand;
    private Integer left_skin_colour_fingers;
    private Integer right_skin_colour_fingers;
    
    private Integer left_dorsalis_pedis_palpation;
    private Integer right_dorsalis_pedis_palpation;
    private Integer left_posterior_tibial_palpation;
    private Integer right_posterior_tibial_palpation;
    
   
    private Integer left_edema_assessment;
    private Integer right_edema_assessment;
    
    private Integer left_edema_severity;
    private Integer right_edema_severity;
    
    private Integer left_edema_location;
    private Integer right_edema_location;
    
    private String sleep_positions;
    private Integer left_wrist_cm;
    private Integer left_wrist_mm;
    private Integer right_wrist_cm;
    private Integer right_wrist_mm;
    private Integer left_elbow_cm;
    private Integer left_elbow_mm;
    private Integer right_elbow_cm;
    private Integer right_elbow_mm;
    
    private String left_sensory;
    
    private String right_sensory;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
     public Integer getDeleted() {
         return deleted;
     }
     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }
     public String getDeleted_reason() {
         return deleted_reason;
     }
     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }
     public String getDelete_signature() {
         return delete_signature;
     }
     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    /** Creates a new instance of LimbAssessmentVO */
    public LimbUpperAssessmentVO() {
    }
    public String getLeft_missing_limbs() {
        return left_missing_limbs;
    }
    public void setLeft_missing_limbs(String left_missing_limbs) {
        this.left_missing_limbs = left_missing_limbs;
    }
    public String getRight_missing_limbs() {
        return right_missing_limbs;
    }
    public void setRight_missing_limbs(String right_missing_limbs) {
        this.right_missing_limbs = right_missing_limbs;
    }
    public String getLeft_pain_assessment() {
        return left_pain_assessment;
    }
    public void setLeft_pain_assessment(String left_pain_assessment) {
        this.left_pain_assessment = left_pain_assessment;
    }
    public String getRight_pain_assessment() {
        return right_pain_assessment;
    }
    public void setRight_pain_assessment(String right_pain_assessment) {
        this.right_pain_assessment = right_pain_assessment;
    }
  
    public String getPain_comments() {
        return pain_comments;
    }
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
    public String getLeft_skin_assessment() {
        return left_skin_assessment;
    }
    public void setLeft_skin_assessment(String left_skin_assessment) {
        this.left_skin_assessment = left_skin_assessment;
    }
    public String getRight_skin_assessment() {
        return right_skin_assessment;
    }
    public void setRight_skin_assessment(String right_skin_assessment) {
        this.right_skin_assessment = right_skin_assessment;
    }
   
    public Integer getLeft_dorsalis_pedis_palpation() {
        return left_dorsalis_pedis_palpation;
    }
    public void setLeft_dorsalis_pedis_palpation(Integer left_dorsalis_pedis_palpation) {
        this.left_dorsalis_pedis_palpation = left_dorsalis_pedis_palpation;
    }
    public Integer getRight_dorsalis_pedis_palpation() {
        return right_dorsalis_pedis_palpation;
    }
    public void setRight_dorsalis_pedis_palpation(Integer right_dorsalis_pedis_palpation) {
        this.right_dorsalis_pedis_palpation = right_dorsalis_pedis_palpation;
    }
    public Integer getLeft_posterior_tibial_palpation() {
        return left_posterior_tibial_palpation;
    }
    public void setLeft_posterior_tibial_palpation(Integer left_posterior_tibial_palpation) {
        this.left_posterior_tibial_palpation = left_posterior_tibial_palpation;
    }
    public Integer getRight_posterior_tibial_palpation() {
        return right_posterior_tibial_palpation;
    }
    public void setRight_posterior_tibial_palpation(Integer right_posterior_tibial_palpation) {
        this.right_posterior_tibial_palpation = right_posterior_tibial_palpation;
    }

    public Integer getLeft_edema_severity() {
        return left_edema_severity;
    }
    public void setLeft_edema_severity(Integer left_edema_severity) {
        this.left_edema_severity = left_edema_severity;
    }
    public Integer getRight_edema_severity() {
        return right_edema_severity;
    }
    public void setRight_edema_severity(Integer right_edema_severity) {
        this.right_edema_severity = right_edema_severity;
    }
    public Integer getLeft_edema_location() {
        return left_edema_location;
    }
    public void setLeft_edema_location(Integer left_edema_location) {
        this.left_edema_location = left_edema_location;
    }
    public Integer getRight_edema_location() {
        return right_edema_location;
    }
    public void setRight_edema_location(Integer right_edema_location) {
        this.right_edema_location = right_edema_location;
    }
    public String getSleep_positions() {
        return sleep_positions;
    }
    public void setSleep_positions(String sleep_positions) {
        this.sleep_positions = sleep_positions;
    }
  

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    

    public String getLeft_sensory() {
        return left_sensory;
    }
    public void setLeft_sensory(String left_sensory) {
        this.left_sensory = left_sensory;
    }

    
    public String getRight_sensory() {
        return right_sensory;
    }
    public void setRight_sensory(String right_sensory) {
        this.right_sensory = right_sensory;
    }
 
  
    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
 
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public Integer getLimb_show() {
        return limb_show;
    }
    public void setLimb_show(Integer limb_show) {
        this.limb_show = limb_show;
    }
    public Integer getLeft_less_capillary() {
        return left_less_capillary;
    }
    public void setLeft_less_capillary(Integer left_less_capillary) {
        this.left_less_capillary = left_less_capillary;
    }
    public Integer getRight_less_capillary() {
        return right_less_capillary;
    }
    public void setRight_less_capillary(Integer right_less_capillary) {
        this.right_less_capillary = right_less_capillary;
    }
    public Integer getLeft_more_capillary() {
        return left_more_capillary;
    }
    public void setLeft_more_capillary(Integer left_more_capillary) {
        this.left_more_capillary = left_more_capillary;
    }
    public Integer getRight_more_capillary() {
        return right_more_capillary;
    }
    public void setRight_more_capillary(Integer right_more_capillary) {
        this.right_more_capillary = right_more_capillary;
    }
   
    public String getLimb_comments() {
        return limb_comments;
    }
    public void setLimb_comments(String limb_comments) {
        this.limb_comments = limb_comments;
    }

    /**
     * @return the right_skin_colour_arm
     */
    public Integer getRight_skin_colour_arm() {
        return right_skin_colour_arm;
    }
    /**
     * @param right_skin_colour_arm the right_skin_colour_arm to set
     */
    public void setRight_skin_colour_arm(Integer right_skin_colour_arm) {
        this.right_skin_colour_arm = right_skin_colour_arm;
    }
    /**
     * @return the left_skin_colour_hand
     */
    public Integer getLeft_skin_colour_hand() {
        return left_skin_colour_hand;
    }
    /**
     * @param left_skin_colour_hand the left_skin_colour_hand to set
     */
    public void setLeft_skin_colour_hand(Integer left_skin_colour_hand) {
        this.left_skin_colour_hand = left_skin_colour_hand;
    }
    /**
     * @return the right_skin_colour_hand
     */
    public Integer getRight_skin_colour_hand() {
        return right_skin_colour_hand;
    }
    /**
     * @param right_skin_colour_hand the right_skin_colour_hand to set
     */
    public void setRight_skin_colour_hand(Integer right_skin_colour_hand) {
        this.right_skin_colour_hand = right_skin_colour_hand;
    }
    /**
     * @return the left_skin_colour_fingers
     */
    public Integer getLeft_skin_colour_fingers() {
        return left_skin_colour_fingers;
    }
    /**
     * @param left_skin_colour_fingers the left_skin_colour_fingers to set
     */
    public void setLeft_skin_colour_fingers(Integer left_skin_colour_fingers) {
        this.left_skin_colour_fingers = left_skin_colour_fingers;
    }
    /**
     * @return the right_skin_colour_fingers
     */
    public Integer getRight_skin_colour_fingers() {
        return right_skin_colour_fingers;
    }
    /**
     * @param right_skin_colour_fingers the right_skin_colour_fingers to set
     */
    public void setRight_skin_colour_fingers(Integer right_skin_colour_fingers) {
        this.right_skin_colour_fingers = right_skin_colour_fingers;
    }
    /**
     * @return the left_wrist_cm
     */
    public Integer getLeft_wrist_cm() {
        return left_wrist_cm;
    }
    /**
     * @param left_wrist_cm the left_wrist_cm to set
     */
    public void setLeft_wrist_cm(Integer left_wrist_cm) {
        this.left_wrist_cm = left_wrist_cm;
    }
    /**
     * @return the left_wrist_mm
     */
    public Integer getLeft_wrist_mm() {
        return left_wrist_mm;
    }
    /**
     * @param left_wrist_mm the left_wrist_mm to set
     */
    public void setLeft_wrist_mm(Integer left_wrist_mm) {
        this.left_wrist_mm = left_wrist_mm;
    }
    /**
     * @return the right_wrist_cm
     */
    public Integer getRight_wrist_cm() {
        return right_wrist_cm;
    }
    /**
     * @param right_wrist_cm the right_wrist_cm to set
     */
    public void setRight_wrist_cm(Integer right_wrist_cm) {
        this.right_wrist_cm = right_wrist_cm;
    }
    /**
     * @return the right_wrist_mm
     */
    public Integer getRight_wrist_mm() {
        return right_wrist_mm;
    }
    /**
     * @param right_wrist_mm the right_wrist_mm to set
     */
    public void setRight_wrist_mm(Integer right_wrist_mm) {
        this.right_wrist_mm = right_wrist_mm;
    }
    /**
     * @return the left_elbow_cm
     */
    public Integer getLeft_elbow_cm() {
        return left_elbow_cm;
    }
    /**
     * @param left_elbow_cm the left_elbow_cm to set
     */
    public void setLeft_elbow_cm(Integer left_elbow_cm) {
        this.left_elbow_cm = left_elbow_cm;
    }
    /**
     * @return the left_elbow_mm
     */
    public Integer getLeft_elbow_mm() {
        return left_elbow_mm;
    }
    /**
     * @param left_elbow_mm the left_elbow_mm to set
     */
    public void setLeft_elbow_mm(Integer left_elbow_mm) {
        this.left_elbow_mm = left_elbow_mm;
    }
    /**
     * @return the right_elbow_cm
     */
    public Integer getRight_elbow_cm() {
        return right_elbow_cm;
    }
    /**
     * @param right_elbow_cm the right_elbow_cm to set
     */
    public void setRight_elbow_cm(Integer right_elbow_cm) {
        this.right_elbow_cm = right_elbow_cm;
    }
    /**
     * @return the right_elbow_mm
     */
    public Integer getRight_elbow_mm() {
        return right_elbow_mm;
    }
    /**
     * @param right_elbow_mm the right_elbow_mm to set
     */
    public void setRight_elbow_mm(Integer right_elbow_mm) {
        this.right_elbow_mm = right_elbow_mm;
    }
    /**
     * @return the left_temperature_arm
     */
    public Integer getLeft_temperature_arm() {
        return left_temperature_arm;
    }
    /**
     * @param left_temperature_arm the left_temperature_arm to set
     */
    public void setLeft_temperature_arm(Integer left_temperature_arm) {
        this.left_temperature_arm = left_temperature_arm;
    }
    /**
     * @return the right_temperature_arm
     */
    public Integer getRight_temperature_arm() {
        return right_temperature_arm;
    }
    /**
     * @param right_temperature_arm the right_temperature_arm to set
     */
    public void setRight_temperature_arm(Integer right_temperature_arm) {
        this.right_temperature_arm = right_temperature_arm;
    }
    /**
     * @return the left_temperature_hand
     */
    public Integer getLeft_temperature_hand() {
        return left_temperature_hand;
    }
    /**
     * @param left_temperature_hand the left_temperature_hand to set
     */
    public void setLeft_temperature_hand(Integer left_temperature_hand) {
        this.left_temperature_hand = left_temperature_hand;
    }
    /**
     * @return the right_temperature_hand
     */
    public Integer getRight_temperature_hand() {
        return right_temperature_hand;
    }
    /**
     * @param right_temperature_hand the right_temperature_hand to set
     */
    public void setRight_temperature_hand(Integer right_temperature_hand) {
        this.right_temperature_hand = right_temperature_hand;
    }
    /**
     * @return the left_temperature_fingers
     */
    public Integer getLeft_temperature_fingers() {
        return left_temperature_fingers;
    }
    /**
     * @param left_temperature_fingers the left_temperature_fingers to set
     */
    public void setLeft_temperature_fingers(Integer left_temperature_fingers) {
        this.left_temperature_fingers = left_temperature_fingers;
    }
    /**
     * @return the right_temperature_fingers
     */
    public Integer getRight_temperature_fingers() {
        return right_temperature_fingers;
    }
    /**
     * @param right_temperature_fingers the right_temperature_fingers to set
     */
    public void setRight_temperature_fingers(Integer right_temperature_fingers) {
        this.right_temperature_fingers = right_temperature_fingers;
    }
    /**
     * @return the left_skin_colour_arm
     */
    public Integer getLeft_skin_colour_arm() {
        return left_skin_colour_arm;
    }
    /**
     * @param left_skin_colour_arm the left_skin_colour_arm to set
     */
    public void setLeft_skin_colour_arm(Integer left_skin_colour_arm) {
        this.left_skin_colour_arm = left_skin_colour_arm;
    }
    /**
     * @return the left_range_motion_arm
     */
    public Integer getLeft_range_motion_arm() {
        return left_range_motion_arm;
    }
    /**
     * @param left_range_motion_arm the left_range_motion_arm to set
     */
    public void setLeft_range_motion_arm(Integer left_range_motion_arm) {
        this.left_range_motion_arm = left_range_motion_arm;
    }
    /**
     * @return the right_range_motion_arm
     */
    public Integer getRight_range_motion_arm() {
        return right_range_motion_arm;
    }
    /**
     * @param right_range_motion_arm the right_range_motion_arm to set
     */
    public void setRight_range_motion_arm(Integer right_range_motion_arm) {
        this.right_range_motion_arm = right_range_motion_arm;
    }
    /**
     * @return the left_range_motion_hand
     */
    public Integer getLeft_range_motion_hand() {
        return left_range_motion_hand;
    }
    /**
     * @param left_range_motion_hand the left_range_motion_hand to set
     */
    public void setLeft_range_motion_hand(Integer left_range_motion_hand) {
        this.left_range_motion_hand = left_range_motion_hand;
    }
    /**
     * @return the right_range_motion_hand
     */
    public Integer getRight_range_motion_hand() {
        return right_range_motion_hand;
    }
    /**
     * @param right_range_motion_hand the right_range_motion_hand to set
     */
    public void setRight_range_motion_hand(Integer right_range_motion_hand) {
        this.right_range_motion_hand = right_range_motion_hand;
    }
    /**
     * @return the left_range_motion_fingers
     */
    public Integer getLeft_range_motion_fingers() {
        return left_range_motion_fingers;
    }
    /**
     * @param left_range_motion_fingers the left_range_motion_fingers to set
     */
    public void setLeft_range_motion_fingers(Integer left_range_motion_fingers) {
        this.left_range_motion_fingers = left_range_motion_fingers;
    }
    /**
     * @return the right_range_motion_fingers
     */
    public Integer getRight_range_motion_fingers() {
        return right_range_motion_fingers;
    }
    /**
     * @param right_range_motion_fingers the right_range_motion_fingers to set
     */
    public void setRight_range_motion_fingers(Integer right_range_motion_fingers) {
        this.right_range_motion_fingers = right_range_motion_fingers;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
  
}
