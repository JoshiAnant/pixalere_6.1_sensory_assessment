/*
 * FootAssessmentVO.java
 *
 * Created on September 17, 2007, 2:42 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Date;
 /**
 *     This is the FootAssessmentVO  bean for the foot_assessment  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientProfileServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *
 */
public class FootAssessmentVO extends ValueObject implements Serializable{
    private String user_signature;
    private String foot_comments;
    private Integer foot_show;
    private Date created_on;
    private Integer professional_id;
    private ProfessionalVO professional;
    private Integer active;
    private Integer id;
 
    private Date lastmodified_on;
    private Integer weight_bearing;
    private Integer balance;
    private Integer calf_muscle_pump;
    private Integer mobility_aids;
    private String calf_muscle_comments;
    private String mobility_aids_comments;
    private String gait_pattern_comments;
    private String walking_distance_comments;
    private String walking_endurance_comments;
    private Integer indoor_footwear;
    private Integer outdoor_footwear;
    private String indoor_footwear_comments;
    private String outdoor_footwear_comments;
    private Integer right_proprioception;
    private Integer left_proprioception;
    private Integer orthotics;
    private String orthotics_comments;
    private Integer muscle_tone_functional;
    private Integer arches_foot_structure;
    private Integer supination_foot_structure;
    private Integer pronation_foot_structure;
    private String dorsiflexion_active;
    private String dorsiflexion_passive;
    private String plantarflexion_active;
    private String plantarflexion_passive;
    private String greattoe_active;
    private String greattoe_passive;
    private Integer patient_id;
    private Integer wound_profile_id;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
     public Integer getDeleted() {
         return deleted;
     }
     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }
     public String getDeleted_reason() {
         return deleted_reason;
     }
     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }
     public String getDelete_signature() {
         return delete_signature;
     }
     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    
    /** Creates a new instance of FootAssessmentVO */
    public FootAssessmentVO() {
    }
   
    public Integer getWeight_bearing() {
        return weight_bearing;
    }
    public void setWeight_bearing(Integer weight_bearing) {
        this.weight_bearing = weight_bearing;
    }
    public Integer getBalance() {
        return balance;
    }
    public void setBalance(Integer balance) {
        this.balance = balance;
    }
    public Integer getCalf_muscle_pump() {
        return calf_muscle_pump;
    }
    public void setCalf_muscle_pump(Integer calf_muscle_pump) {
        this.calf_muscle_pump = calf_muscle_pump;
    }
    public Integer getMobility_aids() {
        return mobility_aids;
    }
    public void setMobility_aids(Integer mobility_aids) {
        this.mobility_aids = mobility_aids;
    }
    public String getCalf_muscle_comments() {
        return calf_muscle_comments;
    }
    public void setCalf_muscle_comments(String calf_muscle_comments) {
        this.calf_muscle_comments = calf_muscle_comments;
    }
    public String getMobility_aids_comments() {
        return mobility_aids_comments;
    }

    public void setMobility_aids_comments(String mobility_aids_comments) {
        this.mobility_aids_comments = mobility_aids_comments;
    }
    public String getGait_pattern_comments() {
        return gait_pattern_comments;
    }
    public void setGait_pattern_comments(String gait_pattern_comments) {
        this.gait_pattern_comments = gait_pattern_comments;
    }
    public String getWalking_distance_comments() {
        return walking_distance_comments;
    }
    public void setWalking_distance_comments(String walking_distance_comments) {
        this.walking_distance_comments = walking_distance_comments;
    }
    public Integer getIndoor_footwear() {
        return indoor_footwear;
    }
    public void setIndoor_footwear(Integer indoor_footwear) {
        this.indoor_footwear = indoor_footwear;
    }
    public Integer getOutdoor_footwear() {
        return outdoor_footwear;
    }
    public void setOutdoor_footwear(Integer outdoor_footwear) {
        this.outdoor_footwear = outdoor_footwear;
    }
    public Integer getOrthotics() {
        return orthotics;
    }
    public void setOrthotics(Integer orthotics) {
        this.orthotics = orthotics;
    }
    
    public Integer getMuscle_tone_functional() {
        return muscle_tone_functional;
    }
    public void setMuscle_tone_functional(Integer muscle_tone_functional) {
        this.muscle_tone_functional = muscle_tone_functional;
    }
    public Integer getArches_foot_structure() {
        return arches_foot_structure;
    }
    public void setArches_foot_structure(Integer arches_foot_structure) {
        this.arches_foot_structure = arches_foot_structure;
    }
    public Integer getSupination_foot_structure() {
        return supination_foot_structure;
    }
    public void setSupination_foot_structure(Integer supination_foot_structure) {
        this.supination_foot_structure = supination_foot_structure;
    }
    public Integer getPronation_foot_structure() {
        return pronation_foot_structure;
    }
    public void setPronation_foot_structure(Integer pronation_foot_structure) {
        this.pronation_foot_structure = pronation_foot_structure;
    }
    public String getDorsiflexion_active() {
        return dorsiflexion_active;
    }
    public void setDorsiflexion_active(String dorsiflexion_active) {
        this.dorsiflexion_active = dorsiflexion_active;
    }
    public String getDorsiflexion_passive() {
        return dorsiflexion_passive;
    }
    public void setDorsiflexion_passive(String dorsiflexion_passive) {
        this.dorsiflexion_passive = dorsiflexion_passive;
    }
    public String getPlantarflexion_active() {
        return plantarflexion_active;
    }
    public void setPlantarflexion_active(String plantarflexion_active) {
        this.plantarflexion_active = plantarflexion_active;
    }
    public String getPlantarflexion_passive() {
        return plantarflexion_passive;
    }
    public void setPlantarflexion_passive(String plantarflexion_passive) {
        this.plantarflexion_passive = plantarflexion_passive;
    }
    public String getGreattoe_active() {
        return greattoe_active;
    }
    public void setGreattoe_active(String greattoe_active) {
        this.greattoe_active = greattoe_active;
    }
    public String getGreattoe_passive() {
        return greattoe_passive;
    }
    public void setGreattoe_passive(String greattoe_passive) {
        this.greattoe_passive = greattoe_passive;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public Integer getWound_profile_id() {
        return wound_profile_id;
    }
    public void setWound_profile_id(Integer wound_profile_id) {
        this.wound_profile_id = wound_profile_id;
    }

    public String getWalking_endurance_comments() {
        return walking_endurance_comments;
    }
    public void setWalking_endurance_comments(String walking_endurance_comments) {
        this.walking_endurance_comments = walking_endurance_comments;
    }
    public String getIndoor_footwear_comments() {
        return indoor_footwear_comments;
    }
    public void setIndoor_footwear_comments(String indoor_footwear_comments) {
        this.indoor_footwear_comments = indoor_footwear_comments;
    }
    public String getOutdoor_footwear_comments() {
        return outdoor_footwear_comments;
    }
    public void setOutdoor_footwear_comments(String outdoor_footwear_comments) {
        this.outdoor_footwear_comments = outdoor_footwear_comments;
    }
    public String getOrthotics_comments() {
        return orthotics_comments;
    }
    public void setOrthotics_comments(String orthotics_comments) {
        this.orthotics_comments = orthotics_comments;
    }
    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }

    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public Integer getFoot_show() {
        return foot_show;
    }
    public void setFoot_show(Integer foot_show) {
        this.foot_show = foot_show;
    }
    public String getFoot_comments() {
        return foot_comments;
    }
    public void setFoot_comments(String foot_comments) {
        this.foot_comments = foot_comments;
    }
  
    /**
     * @return the right_proprioception
     */
    public Integer getRight_proprioception() {
        return right_proprioception;
    }
    /**
     * @param right_proprioception the right_proprioception to set
     */
    public void setRight_proprioception(Integer right_proprioception) {
        this.right_proprioception = right_proprioception;
    }
    /**
     * @return the left_proprioception
     */
    public Integer getLeft_proprioception() {
        return left_proprioception;
    }
    /**
     * @param left_proprioception the left_proprioception to set
     */
    public void setLeft_proprioception(Integer left_proprioception) {
        this.left_proprioception = left_proprioception;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
      private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
}
