package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;
 /**
 *     This is the Patients  bean for the patients  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class PatientsVO extends ValueObject implements Serializable {
    private Integer id;
    private Date created_on;
    private Integer professional_id;
    
    public PatientsVO(){}
    public Integer getProfessional_id(){
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id){
        this.professional_id=professional_id;
    }
    public Date getCreated_on() {
        return created_on;
    }
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
}