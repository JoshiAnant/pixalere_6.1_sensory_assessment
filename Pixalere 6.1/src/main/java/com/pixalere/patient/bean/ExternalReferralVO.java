/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
/**
 *
 * @author travis
 */
public class ExternalReferralVO extends ValueObject{
    public ExternalReferralVO(){}
    public ExternalReferralVO(Integer patient_profile_id,Integer external_referral_id,Integer arranged_id){
        this.setExternal_referral_id(external_referral_id);
        this.patient_profile_id=patient_profile_id;
        this.setArranged_id(arranged_id);
    }
    private Integer external_referral_id;
    private Integer arranged_id;
    private Integer patient_profile_id;
    
    public Integer getPatient_profile_id() {
        return patient_profile_id;
    }
    public void setPatient_profile_id(Integer patient_profile_id) {
        this.patient_profile_id = patient_profile_id;
    }
    /**
     * @return the external_referral_id
     */
    public Integer getExternal_referral_id() {
        return external_referral_id;
    }
    /**
     * @param external_referral_id the external_referral_id to set
     */
    public void setExternal_referral_id(Integer external_referral_id) {
        this.external_referral_id = external_referral_id;
    }
    /**
     * @return the arranged_id
     */
    public Integer getArranged_id() {
        return arranged_id;
    }
    /**
     * @param arranged_id the arranged_id to set
     */
    public void setArranged_id(Integer arranged_id) {
        this.arranged_id = arranged_id;
    }
    
}