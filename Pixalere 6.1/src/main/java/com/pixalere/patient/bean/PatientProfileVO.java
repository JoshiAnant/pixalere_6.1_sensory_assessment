package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.utils.PDate;
import java.util.Vector;
import java.util.List;
import java.util.TimeZone;
import java.util.ArrayList;
import com.pixalere.utils.Common;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Collection;
import com.pixalere.utils.Constants;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.TripleDes;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
 /**
 *     This is the PatientProfileVO  bean for the patient_profile  (DB table)
 *     Refer to {@link com.pixalere.common.service.VersionServiceImpl } for the calls
 *     which utilize this bean.
 *
  *    @has 1..1 Has 1..1 com.pixalere.patient.bean.PatientProfileServiceImpl
 *
 */
public class PatientProfileVO extends ValueObject implements Serializable {
    private Integer review_done;
    private Integer treatment_location_id;
    private Integer purs_score;
    private Date lastmodified_on;
    private Integer hga1c;
    /** identifier field */
    private Integer id;
    private String professionals;
    private Integer offline_flag;
    private String surgical_history;
    /** nullable persistent field */
    private Integer current_flag;
    
    /** nullable persistent field */
    private Integer patient_id;
    private ProfessionalVO professional;
    
    /** nullable persistent field */
    private Integer referral_id;
    
    private Integer professional_id;
    /** nullable persistent field */
    private Collection<PatientProfileArraysVO> patient_profile_arrays;
    /** nullable persistent field */
    private Date start_date;
    
    /** nullable persistent field */
    private Date created_on;
    private Date deleted_on;
    private String user_signature;
 
    /** nullable persistent field */
    private Integer active;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Collection<ExternalReferralVO> externalReferrals;
    private Integer discharge_planning_initiated; 
    private Integer chronic_disease_initiated ;
    private Integer barriers_addressed ;
    private String barriers_addressed_comments ;
        private String system_barriers_comments ;  
    private Integer quality_of_life_addressed;
    private String quality_of_life_addressed_comments;
  private Integer root_cause_addressed;
    private String root_cause_addressed_comments;
    public Integer getDeleted() {
         return deleted;
    }
     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }
     public String getDeleted_reason() {
         return deleted_reason;
     }
     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }
     public String getDelete_signature() {
         return delete_signature;
     }
     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    private static final int INTERFERING_MEDS=1; //this is really interfering >>>FACTORS<<<
    private static final int COMO=2;
    private static final int MED_COMMENTS = 3; //this is really interfering meds
    
    public String getProfessionals() {
        return professionals;
    }
    
    public void setProfessionals(String professionals) {
        this.professionals = professionals;
    }
    
    /**
     * @method getProfessional_id
     * @field professional_id - Integer
     * @return Returns the professional_id.
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    
    /**
     * @method setProfessional_id
     * @field professional_id - Integer
     * @param professional_id The professional_id to set.
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    
  
  
    /** full constructor */
    public PatientProfileVO(Integer current_flag,
            Integer active, 
            Integer patient_id, 
            String user_signature, 
            Integer referral_id,   
            String professionals,
            Date start_date, 
            Date last_update, 
            String surgical_history,
           Integer review_done,Integer purs_score) {
        this.setCurrent_flag(current_flag);
        this.setPurs_score(purs_score);
        this.setActive(active);
        this.setPatient_id(patient_id);
        this.setUser_signature(user_signature);
        this.setReferral_id(referral_id);
        
        this.setProfessionals(professionals);
   
        this.setStart_date(start_date);
        this.setCreated_on(last_update);
        this.surgical_history=surgical_history;
        this.review_done=review_done;
        
        
    }
    
   
    
    /** default constructor */
    public PatientProfileVO() {
    }
    public Integer getTreatment_location_id() {
		return this.treatment_location_id;
    }
    public void setTreatment_location_id(Integer treatment_location_id) {
		this.treatment_location_id=treatment_location_id;
    }
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    
    
    public Integer getCurrent_flag() {
        return this.current_flag;
    }
    
    public void setCurrent_flag(Integer current_flag) {
        this.current_flag = current_flag;
    }
    
    public Integer getPatient_id() {
        return this.patient_id;
    }
    
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    
    
    public Integer getReferral_id() {
        return this.referral_id;
    }
    
    public void setReferral_id(Integer referral_id) {
        this.referral_id = referral_id;
    }
    
    
    public Date getStart_date() {
        return start_date;
    }
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    
    public String getUser_signature() {
        return this.user_signature;
    }
    public int parseString(String value){
        try{
            return Integer.parseInt(value);
        }catch(NumberFormatException ex){return 0;}
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    
	public String getEncrypted(String value,String password){
		try{
		  	if(Common.getLocalizedString("pixalere.encrypted","en").equals("1"))
		  		{
				TripleDes des=new TripleDes(password);
				return des.encrypt(value);
				}
			else
				{
				return value;  
				}
			
		}catch(Exception e){e.printStackTrace();return "";}
	}
	
	public String getDecrypted(String value,String password){
			try{
		  	if(Common.getLocalizedString("pixalere.encrypted","en").equals("1") && value != null && value != "")
		  		{
				TripleDes des=new TripleDes(password);
				return des.decrypt(value);
				}
			else
				{
				return value;  
				}
			
		}catch(Exception e){e.printStackTrace();return "";}
	}
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public static int getINTERFERING_MEDS() {
        return INTERFERING_MEDS;
    }
    public static int getCOMO() {
        return COMO;
    }
    public static int getMED_COMMENTS() {
        return MED_COMMENTS;
    }
    public String getSurgical_history() {
        return surgical_history;
    }
    public void setSurgical_history(String surgical_history) {
        this.surgical_history = surgical_history;
    }
    public Integer getOffline_flag() {
        return offline_flag;
    }
    public void setOffline_flag(Integer offline_flag) {
        this.offline_flag = offline_flag;
    }

    public Integer getReview_done(){
        return review_done;
    }
    public void setReview_done(Integer review_done){
        this.review_done=review_done;
    }

    /**
     * @return the patient_profile_arrays
     */
    public Collection<PatientProfileArraysVO> getPatient_profile_arrays() {
        return patient_profile_arrays;
    }
    /**
     * @param patient_profile_arrays the patient_profile_arrays to set
     */
    public void setPatient_profile_arrays(Collection<PatientProfileArraysVO> patient_profile_arrays) {
        this.patient_profile_arrays = patient_profile_arrays;
    }
    /**
     * @return the purs_score
     */
    public Integer getPurs_score() {
        return purs_score;
    }
    /**
     * @param purs_score the purs_score to set
     */
    public void setPurs_score(Integer purs_score) {
        this.purs_score = purs_score;
    }
    
   
/**
     * @return the external-referrals
     */
    public Collection<ExternalReferralVO> getExternalReferrals() {
        return externalReferrals;
    }
 
     /**
     * @param investigations the investigations to set
     */
    public void setExternalReferrals(Collection<ExternalReferralVO> externalReferrals) {
        this.externalReferrals = externalReferrals;
    }
/**
     * @return the discharge_planning_initiated
     */
    public Integer getDischarge_planning_initiated() {
        return discharge_planning_initiated;
    }
    /**
     * @param discharge_planning_initiated the discharge_planning_initiated to set
     */
    public void setDischarge_planning_initiated(Integer discharge_planning_initiated) {
        this.discharge_planning_initiated = discharge_planning_initiated;
    }
    /**
     * @return the chronic_disease_initiated
     */
    public Integer getChronic_disease_initiated() {
        return chronic_disease_initiated;
    }
    /**
     * @param chronic_disease_initiated the chronic_disease_initiated to set
     */
    public void setChronic_disease_initiated(Integer chronic_disease_initiated) {
        this.chronic_disease_initiated = chronic_disease_initiated;
    }
    /**
     * @return the barriers_addressed
     */
    public Integer getBarriers_addressed() {
        return barriers_addressed;
    }
    /**
     * @param barriers_addressed the barriers_addressed to set
     */
    public void setBarriers_addressed(Integer barriers_addressed) {
        this.barriers_addressed = barriers_addressed;
    }
    /**
     * @return the barriers_addressed_comments
     */
    public String getBarriers_addressed_comments() {
        return barriers_addressed_comments;
    }
    /**
     * @param barriers_addressed_comments the barriers_addressed_comments to set
     */
    public void setBarriers_addressed_comments(String barriers_addressed_comments) {
        this.barriers_addressed_comments = barriers_addressed_comments;
    }
    /**
     * @return the system_barriers_comments
     */
    public String getSystem_barriers_comments() {
        return system_barriers_comments;
    }
    /**
     * @param system_barriers_comments the system_barriers_comments to set
     */
    public void setSystem_barriers_comments(String system_barriers_comments) {
        this.system_barriers_comments = system_barriers_comments;
    }
    /**
     * @return the quality_of_life_addressed
     */
    public Integer getQuality_of_life_addressed() {
        return quality_of_life_addressed;
    }
    /**
     * @param quality_of_life_addressed the quality_of_life_addressed to set
     */
    public void setQuality_of_life_addressed(Integer quality_of_life_addressed) {
        this.quality_of_life_addressed = quality_of_life_addressed;
    }
    /**
     * @return the quality_of_life_addressed_comments
     */
    public String getQuality_of_life_addressed_comments() {
        return quality_of_life_addressed_comments;
    }
    /**
     * @param quality_of_life_addressed_comments the quality_of_life_addressed_comments to set
     */
    public void setQuality_of_life_addressed_comments(String quality_of_life_addressed_comments) {
        this.quality_of_life_addressed_comments = quality_of_life_addressed_comments;
    }

    /**
     * @return the root_caused_addressed
     */
    public Integer getRoot_cause_addressed() {
        return root_cause_addressed;
    }
    /**
     * @param root_caused_addressed the root_caused_addressed to set
     */
    public void setRoot_cause_addressed(Integer root_cause_addressed) {
        this.root_cause_addressed = root_cause_addressed;
    }
    /**
     * @return the root_caused_addressed_comments
     */
    public String getRoot_cause_addressed_comments() {
        return root_cause_addressed_comments;
    }
    /**
     * @param root_caused_addressed_comments the root_caused_addressed_comments to set
     */
    public void setRoot_cause_addressed_comments(String root_cause_addressed_comments) {
        this.root_cause_addressed_comments = root_cause_addressed_comments;
    }
    
    /**
     * @return the hga1c
     */
    public Integer getHga1c() {
        return hga1c;
    }
    /**
     * @param hga1c the hga1c to set
     */
    public void setHga1c(Integer hga1c) {
        this.hga1c = hga1c;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
  private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
}
