package com.pixalere.patient.bean;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.ValueObject;
import java.util.List;
import java.util.ArrayList;
import com.pixalere.common.ArrayValueObject;
import java.io.Serializable;
/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Mar 9, 2010
 * Time: 5:20:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class PatientProfileArraysVO extends ArrayValueObject{
    public PatientProfileArraysVO(){}
    public PatientProfileArraysVO(Integer patient_profile_id,Integer lookup_id,String other){
        setLookup_id(lookup_id);
        this.patient_profile_id=patient_profile_id;
        setOther(other);
    }
    private Integer patient_profile_id;
    
    public Integer getPatient_profile_id() {
        return patient_profile_id;
    }
    public void setPatient_profile_id(Integer patient_profile_id) {
        this.patient_profile_id = patient_profile_id;
    }
    
   
}
