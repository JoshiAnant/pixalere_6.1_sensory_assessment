package com.pixalere.patient.bean;
import java.io.Serializable;
import java.util.Date;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ValueObject;

public class SensoryAssessmentVO extends ValueObject implements Serializable{
	private Integer id;
	private String user_signature;
	private Date lastmodified_on;
	private ProfessionalVO professional;
	private Integer score_totals;
	private Date created_on;
    private Integer patient_id;
    private Date deleted_on;
    private Integer active;
    private Integer deleted;
    private String delete_signature;
    private Date visited_on;
    private String deleted_reason;
    private Integer skin_leftfoot;
    private Integer temphot_leftfoot;
    private Integer temphot_rightfoot;
	private String temphot_comments;
    private Integer tempcold_leftfoot;
    private Integer tempcold_rightfoot;
    private String tempcold_comments;
    private Integer pedal_pulse_leftfoot;
    private Integer pedal_pulse_rightfoot;
    private String pedal_pulse_comments;
    private Integer dependent_rubor_leftfoot;
    private Integer dependent_rubor_rightfoot;
    private String dependent_rubor_comments;
    private Integer erythema_leftfoot;
    private Integer erythema_rightfoot;
    private String erythema_comments;
   

	

	 private Integer leftfoot_score;
     private Integer rightfoot_score;
     private Integer leftfoot_image_score;
     private Integer rightfoot_image_score;
     private Integer skin_rightfoot;
     private String skin_comments;
     private Integer nails_leftfoot;
     private Integer nails_rightfoot;
     private String nails_comments;
     private Integer deformity_leftfoot;
     private Integer deformity_rightfoot;
     private String deformity_comments;
     private Integer footwear_leftfoot;
     private Integer footwear_rightfoot;
     private String footwear_comments;
  
     private Integer rangeofmotion_leftfoot;
     private Integer rangeofmotion_rightfoot;
     private String rangeofmotion_comments;
     private Integer sensation_monofilament_leftfoot;
     private Integer sensation_monofilament_rightfoot;
     private String sensation_monofilament_comments;
     private Integer sensation_question_leftfoot;
     private Integer sensation_question_rightfoot;
     private String sensation_question_comments;

	   
	   
	public Integer getScore_totals() {
		return score_totals;
	}

	public void setScore_totals(Integer score_totals) {
		this.score_totals = score_totals;
	}

	public void setProfessional(ProfessionalVO professional) {
		this.professional = professional;
	}

	public Date getLastmodified_on() {
		return lastmodified_on;
	}

	public void setLastmodified_on(Date lastmodified_on) {
		this.lastmodified_on = lastmodified_on;
	}

	private String sensory_comments;

	public String getSensory_comments() {
		return sensory_comments;
	}

	public void setSensory_comments(String sensory_comments) {
		this.sensory_comments = sensory_comments;
	}

	public Integer getProfessional_id() {
		return professional_id;
	}

	public void setProfessional_id(Integer professional_id) {
		this.professional_id = professional_id;
	}

	private Integer professional_id;

	public String getUser_signature() {
		return user_signature;
	}

	public void setUser_signature(String user_signature) {
		this.user_signature = user_signature;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public Integer getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(Integer patient_id) {
		this.patient_id = patient_id;
	}

	public Date getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(Date deleted_on) {
		this.deleted_on = deleted_on;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getDeleted() {
		return deleted;
	}

	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public String getDelete_signature() {
		return delete_signature;
	}

	public void setDelete_signature(String delete_signature) {
		this.delete_signature = delete_signature;
	}

	public Date getVisited_on() {
		return visited_on;
	}

	public void setVisited_on(Date visited_on) {
		this.visited_on = visited_on;
	}

	public String getDeleted_reason() {
		return deleted_reason;
	}

	public void setDeleted_reason(String deleted_reason) {
		this.deleted_reason = deleted_reason;
	}

	public Integer getSkin_leftfoot() {
		return skin_leftfoot;
	}

	public void setSkin_leftfoot(Integer skin_leftfoot) {
		this.skin_leftfoot = skin_leftfoot;
	}

	public Integer getSkin_rightfoot() {
		return skin_rightfoot;
	}

	public void setSkin_rightfoot(Integer skin_rightfoot) {
		this.skin_rightfoot = skin_rightfoot;
	}

	public String getSkin_comments() {
		return skin_comments;
	}

	public void setSkin_comments(String skin_comments) {
		this.skin_comments = skin_comments;
	}

	public Integer getNails_leftfoot() {
		return nails_leftfoot;
	}

	public void setNails_leftfoot(Integer nails_leftfoot) {
		this.nails_leftfoot = nails_leftfoot;
	}

	public Integer getNails_rightfoot() {
		return nails_rightfoot;
	}

	public void setNails_rightfoot(Integer nails_rightfoot) {
		this.nails_rightfoot = nails_rightfoot;
	}

	public String getNails_comments() {
		return nails_comments;
	}

	public void setNails_comments(String nails_comments) {
		this.nails_comments = nails_comments;
	}

	public Integer getDeformity_leftfoot() {
		return deformity_leftfoot;
	}

	public void setDeformity_leftfoot(Integer deformity_leftfoot) {
		this.deformity_leftfoot = deformity_leftfoot;
	}

	public Integer getDeformity_rightfoot() {
		return deformity_rightfoot;
	}

	public void setDeformity_rightfoot(Integer deformity_rightfoot) {
		this.deformity_rightfoot = deformity_rightfoot;
	}

	public String getDeformity_comments() {
		return deformity_comments;
	}

	public void setDeformity_comments(String deformity_comments) {
		this.deformity_comments = deformity_comments;
	}

	public Integer getFootwear_leftfoot() {
		return footwear_leftfoot;
	}

	public void setFootwear_leftfoot(Integer footwear_leftfoot) {
		this.footwear_leftfoot = footwear_leftfoot;
	}

	public Integer getFootwear_rightfoot() {
		return footwear_rightfoot;
	}

	public void setFootwear_rightfoot(Integer footwear_rightfoot) {
		this.footwear_rightfoot = footwear_rightfoot;
	}

	public String getFootwear_comments() {
		return footwear_comments;
	}

	public void setFootwear_comments(String footwear_comments) {
		this.footwear_comments = footwear_comments;
	}

	public Integer getTempcold_leftfoot() {
		return tempcold_leftfoot;
	}

	public void setTempcold_leftfoot(Integer tempcold_leftfoot) {
		this.tempcold_leftfoot = tempcold_leftfoot;
	}

	public Integer getTempcold_rightfoot() {
		return tempcold_rightfoot;
	}

	public void setTempcold_rightfoot(Integer tempcold_rightfoot) {
		this.tempcold_rightfoot = tempcold_rightfoot;
	}

	public String getTempcold_comments() {
		return tempcold_comments;
	}

	public void setTempcold_comments(String tempcold_comments) {
		this.tempcold_comments = tempcold_comments;
	}

	public Integer getRangeofmotion_leftfoot() {
		return rangeofmotion_leftfoot;
	}

	public void setRangeofmotion_leftfoot(Integer rangeofmotion_leftfoot) {
		this.rangeofmotion_leftfoot = rangeofmotion_leftfoot;
	}

	public Integer getRangeofmotion_rightfoot() {
		return rangeofmotion_rightfoot;
	}

	public void setRangeofmotion_rightfoot(Integer rangeofmotion_rightfoot) {
		this.rangeofmotion_rightfoot = rangeofmotion_rightfoot;
	}

	public String getRangeofmotion_comments() {
		return rangeofmotion_comments;
	}

	public void setRangeofmotion_comments(String rangeofmotion_comments) {
		this.rangeofmotion_comments = rangeofmotion_comments;
	}

	public Integer getSensation_monofilament_leftfoot() {
		return sensation_monofilament_leftfoot;
	}

	public void setSensation_monofilament_leftfoot(
			Integer sensation_monofilament_leftfoot) {
		this.sensation_monofilament_leftfoot = sensation_monofilament_leftfoot;
	}

	public Integer getSensation_monofilament_rightfoot() {
		return sensation_monofilament_rightfoot;
	}

	public void setSensation_monofilament_rightfoot(
			Integer sensation_monofilament_rightfoot) {
		this.sensation_monofilament_rightfoot = sensation_monofilament_rightfoot;
	}

	public String getSensation_monofilament_comments() {
		return sensation_monofilament_comments;
	}

	public void setSensation_monofilament_comments(
			String sensation_monofilament_comments) {
		this.sensation_monofilament_comments = sensation_monofilament_comments;
	}

	public Integer getSensation_question_leftfoot() {
		return sensation_question_leftfoot;
	}

	public void setSensation_question_leftfoot(
			Integer sensation_question_leftfoot) {
		this.sensation_question_leftfoot = sensation_question_leftfoot;
	}

	public Integer getSensation_question_rightfoot() {
		return sensation_question_rightfoot;
	}

	public void setSensation_question_rightfoot(
			Integer sensation_question_rightfoot) {
		this.sensation_question_rightfoot = sensation_question_rightfoot;
	}

	public String getSensation_question_comments() {
		return sensation_question_comments;
	}

	public void setSensation_question_comments(
			String sensation_question_comments) {
		this.sensation_question_comments = sensation_question_comments;
	}

	public Integer getTemphot_leftfoot() {
		return temphot_leftfoot;
	}

	public void setTemphot_leftfoot(Integer temphot_leftfoot) {
		this.temphot_leftfoot = temphot_leftfoot;
	}

	public Integer getTemphot_rightfoot() {
		return temphot_rightfoot;
	}

	public void setTemphot_rightfoot(Integer temphot_rightfoot) {
		this.temphot_rightfoot = temphot_rightfoot;
	}

	public String getTemphot_comments() {
		return temphot_comments;
	}

	public void setTemphot_comments(String temphot_comments) {
		this.temphot_comments = temphot_comments;
	}

	public Integer getPedal_pulse_leftfoot() {
		return pedal_pulse_leftfoot;
	}

	public void setPedal_pulse_leftfoot(Integer pedal_pulse_leftfoot) {
		this.pedal_pulse_leftfoot = pedal_pulse_leftfoot;
	}

	public Integer getPedal_pulse_rightfoot() {
		return pedal_pulse_rightfoot;
	}

	public void setPedal_pulse_rightfoot(Integer pedal_pulse_rightfoot) {
		this.pedal_pulse_rightfoot = pedal_pulse_rightfoot;
	}

	public String getPedal_pulse_comments() {
		return pedal_pulse_comments;
	}

	public void setPedal_pulse_comments(String pedal_pulse_comments) {
		this.pedal_pulse_comments = pedal_pulse_comments;
	}

	public Integer getDependent_rubor_leftfoot() {
		return dependent_rubor_leftfoot;
	}

	public void setDependent_rubor_leftfoot(Integer dependent_rubor_leftfoot) {
		this.dependent_rubor_leftfoot = dependent_rubor_leftfoot;
	}

	public Integer getDependent_rubor_rightfoot() {
		return dependent_rubor_rightfoot;
	}

	public void setDependent_rubor_rightfoot(Integer dependent_rubor_rightfoot) {
		this.dependent_rubor_rightfoot = dependent_rubor_rightfoot;
	}

	public String getDependent_rubor_comments() {
		return dependent_rubor_comments;
	}

	public void setDependent_rubor_comments(String dependent_rubor_comments) {
		this.dependent_rubor_comments = dependent_rubor_comments;
	}

	public Integer getErythema_leftfoot() {
		return erythema_leftfoot;
	}

	public void setErythema_leftfoot(Integer erythema_leftfoot) {
		this.erythema_leftfoot = erythema_leftfoot;
	}

	public Integer getErythema_rightfoot() {
		return erythema_rightfoot;
	}

	public void setErythema_rightfoot(Integer erythema_rightfoot) {
		this.erythema_rightfoot = erythema_rightfoot;
	}

	public String getErythema_comments() {
		return erythema_comments;
	}

	public void setErythema_comments(String erythema_comments) {
		this.erythema_comments = erythema_comments;
	}

	public ProfessionalVO getProfessional() {
		return professional;
	}
	public Integer getLeftfoot_score() {
		return leftfoot_score;
	}

	public void setLeftfoot_score(Integer leftfoot_score) {
		this.leftfoot_score = leftfoot_score;
	}

	public Integer getRightfoot_score() {
		return rightfoot_score;
	}

	public void setRightfoot_score(Integer rightfoot_score) {
		this.rightfoot_score = rightfoot_score;
	}
	public Integer getLeftfoot_image_score() {
		return leftfoot_image_score;
	}

	public void setLeftfoot_image_score(Integer leftfoot_image_score) {
		this.leftfoot_image_score = leftfoot_image_score;
	}

	public Integer getRightfoot_image_score() {
		return rightfoot_image_score;
	}

	public void setRightfoot_image_score(Integer rightfoot_image_score) {
		this.rightfoot_image_score = rightfoot_image_score;
	}
}
