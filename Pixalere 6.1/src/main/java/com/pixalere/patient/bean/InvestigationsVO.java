package com.pixalere.patient.bean;
import com.pixalere.common.ArrayValueObject;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.pixalere.common.bean.LookupVO;
/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Mar 11, 2010
 * Time: 11:07:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class InvestigationsVO  extends ArrayValueObject implements Serializable {
    public InvestigationsVO(){}
 
    private Date investigation_date;
    private Integer diagnostic_id;
   
    public Date getInvestigation_date() {
        return investigation_date;
    }
    public void setInvestigation_date(Date investigation_date) {
        this.investigation_date = investigation_date;
    }
   
    public String getDate(){
        if(investigation_date!=null){
           DateFormat d = new SimpleDateFormat("dd/MMMMM/yyyy");
           String value = d.format(investigation_date);
            return value;
        }
        return "";
    }
    /**
     * @return the diagnostic_id
     */
    public Integer getDiagnostic_id() {
        return diagnostic_id;
    }
    /**
     * @param diagnostic_id the diagnostic_id to set
     */
    public void setDiagnostic_id(Integer diagnostic_id) {
        this.diagnostic_id = diagnostic_id;
    }
    
}
