/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.patient.bean;
import java.util.Calendar;
import java.util.GregorianCalendar;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.utils.PDate;
import java.util.Vector;
import java.util.List;
import java.util.TimeZone;
import java.util.ArrayList;
import com.pixalere.utils.Common;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Collection;
import com.pixalere.utils.Constants;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.utils.TripleDes;
import java.util.Date;
/**
 *     This is the DiagnosticInvestigationVO  bean for the diagnostic_investigation  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientProfileServiceImpl } for the calls
 *     which utilize this bean.
 *
  *    @has 1..1 Has 1..1 com.pixalere.patient.bean.PatientProfileServiceImpl
 *
 */
public class DiagnosticInvestigationVO extends ValueObject implements Serializable {
    private Date albumin_date;
    private Double albumin;
    private Double pre_albumin;
    private Date prealbumin_date;
    private Integer purs_score;
    private Date lastmodified_on;
    private Date created_on;
    private Double blood_sugar;
    private String blood_sugar_meal;
    private Date blood_sugar_date;
    private Integer hga1c;
    private Integer id;
    private Integer patient_id;
    private ProfessionalVO professional;
    private Integer professional_id;
    private Collection<InvestigationsVO> investigations;
    private Date start_date;
  
    private String user_signature;
    private Integer active;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
    public DiagnosticInvestigationVO(){}
    /**
     * @return the albumin_date
     */
    public Date getAlbumin_date() {
        return albumin_date;
    }
    /**
     * @param albumin_date the albumin_date to set
     */
    public void setAlbumin_date(Date albumin_date) {
        this.albumin_date = albumin_date;
    }
    /**
     * @return the albumin
     */
    public Double getAlbumin() {
        return albumin;
    }
    /**
     * @param albumin the albumin to set
     */
    public void setAlbumin(Double albumin) {
        this.albumin = albumin;
    }
    /**
     * @return the pre_albumin
     */
    public Double getPre_albumin() {
        return pre_albumin;
    }
    /**
     * @param pre_albumin the pre_albumin to set
     */
    public void setPre_albumin(Double pre_albumin) {
        this.pre_albumin = pre_albumin;
    }
    /**
     * @return the prealbumin_date
     */
    public Date getPrealbumin_date() {
        return prealbumin_date;
    }
    /**
     * @param prealbumin_date the prealbumin_date to set
     */
    public void setPrealbumin_date(Date prealbumin_date) {
        this.prealbumin_date = prealbumin_date;
    }
  
    /**
     * @return the blood_sugar
     */
    public Double getBlood_sugar() {
        return blood_sugar;
    }
    /**
     * @param blood_sugar the blood_sugar to set
     */
    public void setBlood_sugar(Double blood_sugar) {
        this.blood_sugar = blood_sugar;
    }
    /**
     * @return the blood_sugar_meal
     */
    public String getBlood_sugar_meal() {
        return blood_sugar_meal;
    }
    /**
     * @param blood_sugar_meal the blood_sugar_meal to set
     */
    public void setBlood_sugar_meal(String blood_sugar_meal) {
        this.blood_sugar_meal = blood_sugar_meal;
    }
    /**
     * @return the blood_sugar_date
     */
    public Date getBlood_sugar_date() {
        return blood_sugar_date;
    }
    /**
     * @param blood_sugar_date the blood_sugar_date to set
     */
    public void setBlood_sugar_date(Date blood_sugar_date) {
        this.blood_sugar_date = blood_sugar_date;
    }
    /**
     * @return the hga1c
     */
    public Integer getHga1c() {
        return hga1c;
    }
    /**
     * @param hga1c the hga1c to set
     */
    public void setHga1c(Integer hga1c) {
        this.hga1c = hga1c;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }
    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    /**
     * @return the professional
     */
    public ProfessionalVO getProfessional() {
        return professional;
    }
    /**
     * @param professional the professional to set
     */
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    /**
     * @return the investigations
     */
    public Collection<InvestigationsVO> getInvestigations() {
        return investigations;
    }
    /**
     * @param investigations the investigations to set
     */
    public void setInvestigations(Collection<InvestigationsVO> investigations) {
        this.investigations = investigations;
    }
    /**
     * @return the start_date
     */
    public Date getStart_date() {
        return start_date;
    }
    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }
   
    /**
     * @return the user_signature
     */
    public String getUser_signature() {
        return user_signature;
    }
    /**
     * @param user_signature the user_signature to set
     */
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }
    /**
     * @return the deleted
     */
    public Integer getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    /**
     * @return the delete_signature
     */
    public String getDelete_signature() {
        return delete_signature;
    }
    /**
     * @param delete_signature the delete_signature to set
     */
    public void setDelete_signature(String delete_signature) {
        this.delete_signature = delete_signature;
    }
    /**
     * @return the deleted_reason
     */
    public String getDeleted_reason() {
        return deleted_reason;
    }
    /**
     * @param deleted_reason the deleted_reason to set
     */
    public void setDeleted_reason(String deleted_reason) {
        this.deleted_reason = deleted_reason;
    }

 
    public Vector getAlbuminDate(ProfessionalVO userVO){
        Vector<String> dates = new Vector<String>();
        try {if (getAlbumin_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
           // calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
            calendar.setTime(new Date(Long.parseLong(getAlbumin_date().getTime() + "")));
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        }  catch (NumberFormatException ex) {ex.printStackTrace();
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {e.printStackTrace();
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getPreAlbuminDate(ProfessionalVO userVO){
         Vector<String> dates = new Vector<String>();
        try {if (getPrealbumin_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
           // calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
            calendar.setTime(new Date(Long.parseLong(getPrealbumin_date().getTime() + "")));
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        }  catch (NumberFormatException ex) {ex.printStackTrace();
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {e.printStackTrace();
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    public Vector getBloodSugarDate() {
        Vector<String> dates = new Vector<String>();
        try {
             if (getBlood_sugar_date() == null) {
                return new Vector();
            }
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date(Long.parseLong(getBlood_sugar_date().getTime() + "")));
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (NumberFormatException ex) {ex.printStackTrace();
            dates = new Vector();//empty date, return empty vector.
        } catch (Exception e) {e.printStackTrace();
            e.printStackTrace();
            dates = new Vector<String>();
        }
        return dates;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
      private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }

    /**
     * @return the purs_score
     */
    public Integer getPurs_score() {
        return purs_score;
    }

    /**
     * @param purs_score the purs_score to set
     */
    public void setPurs_score(Integer purs_score) {
        this.purs_score = purs_score;
    }
}
