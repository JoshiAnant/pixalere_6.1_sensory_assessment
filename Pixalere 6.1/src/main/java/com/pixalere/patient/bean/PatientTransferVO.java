package com.pixalere.patient.bean;


import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;

public class PatientTransferVO extends ValueObject implements Serializable {
    public PatientTransferVO(){}
    private Integer id;
    private Integer patient_id;
    private Integer professional_id;
    private Integer source_location_id;
    private Integer destination_location_id;
    private Date transfer_on;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }

    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }

    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }

    /**
     * @return the source_location_id
     */
    public Integer getSource_location_id() {
        return source_location_id;
    }

    /**
     * @param source_location_id the source_location_id to set
     */
    public void setSource_location_id(Integer source_location_id) {
        this.source_location_id = source_location_id;
    }

    /**
     * @return the destination_location_id
     */
    public Integer getDestination_location_id() {
        return destination_location_id;
    }

    /**
     * @param destination_location_id the destination_location_id to set
     */
    public void setDestination_location_id(Integer destination_location_id) {
        this.destination_location_id = destination_location_id;
    }

    /**
     * @return the transfer_on
     */
    public Date getTransfer_on() {
        return transfer_on;
    }

    /**
     * @param transfer_on the transfer_on to set
     */
    public void setTransfer_on(Date transfer_on) {
        this.transfer_on = transfer_on;
    }
    
}