/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.patient.bean;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ValueObject;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.io.Serializable;
import java.util.Vector;
import java.util.Date;
/**
 *
 * @author travis
 */
public class LimbAdvAssessmentVO extends ValueObject implements Serializable {
    private Integer active;
    private Integer professional_id;
    private Integer referral_vascular_assessment;
     private Date lastmodified_on;
      private Integer deleted;
    private String delete_signature;
    private String limb_comments;
    private String deleted_reason;
     private Date created_on;
     private String user_signature;
     private Integer left_stemmers;
     private Integer right_stemmers;
     private Integer left_homans;
     private Integer right_homans;
    private Integer transcutaneous_lab;
    private Date transcutaneous_date;
    private Integer right_digit_total;
    private Integer left_digit_total;
    private Double left_api_score;
    private Double right_api_score;
    private String left_pain_assessment;
    private String right_pain_assessment;
    private Integer left_temperature_leg;
    private Integer right_temperature_leg;
    private Integer left_temperature_foot;
    private Integer right_temperature_foot;
    private Integer left_temperature_toes;
    private Integer right_temperature_toes;
   
     private Integer id;
    private Integer patient_id;
    private Integer doppler_lab;
    private String left_dorsalis_pedis_doppler;
    private String right_dorsalis_pedis_doppler;
    private String left_posterior_tibial_doppler;
    private String right_posterior_tibial_doppler;
    private String left_interdigitial_doppler;
    private String right_interdigitial_doppler;
    
    private Integer ankle_brachial_lab;
    private Date ankle_brachial_date;
    private Integer left_tibial_pedis_ankle_brachial;
    private Integer right_tibial_pedis_ankle_brachial;
    private Integer left_dorsalis_pedis_ankle_brachial;
    private Integer right_dorsalis_pedis_ankle_brachial;
    private Integer left_ankle_brachial;
    private Integer right_ankle_brachial;
    private Integer toe_brachial_lab;
    private Date toe_brachial_date;
    private String left_toe_pressure;
    private String right_toe_pressure;
    private String left_brachial_pressure;
    private String right_brachial_pressure;
    private String left_limb_shape;
    private String right_limb_shape;
    private Integer left_transcutaneous_pressure;
    private Integer right_transcutaneous_pressure;
    private String left_sensation;
    private String left_score_sensation;
    private String right_sensation;
    private String right_score_sensation;
    private String left_foot_deformities;
    private String right_foot_deformities;
    private String left_foot_skin;
    private String right_foot_skin;
    private String left_foot_toes;
    private String right_foot_toes;
    private ProfessionalVO professional;
    private Date deleted_on;
    public LimbAdvAssessmentVO(){}
    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }
    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    
    /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
    /**
     * @return the user_signature
     */
    public String getUser_signature() {
        return user_signature;
    }
    /**
     * @param user_signature the user_signature to set
     */
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }
    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    /**
     * @return the doppler_lab
     */
    public Integer getDoppler_lab() {
        return doppler_lab;
    }
    /**
     * @param doppler_lab the doppler_lab to set
     */
    public void setDoppler_lab(Integer doppler_lab) {
        this.doppler_lab = doppler_lab;
    }
    /**
     * @return the left_dorsalis_pedis_doppler
     */
    public String getLeft_dorsalis_pedis_doppler() {
        return left_dorsalis_pedis_doppler;
    }
    /**
     * @param left_dorsalis_pedis_doppler the left_dorsalis_pedis_doppler to set
     */
    public void setLeft_dorsalis_pedis_doppler(String left_dorsalis_pedis_doppler) {
        this.left_dorsalis_pedis_doppler = left_dorsalis_pedis_doppler;
    }
    /**
     * @return the right_dorsalis_pedis_doppler
     */
    public String getRight_dorsalis_pedis_doppler() {
        return right_dorsalis_pedis_doppler;
    }
    /**
     * @param right_dorsalis_pedis_doppler the right_dorsalis_pedis_doppler to set
     */
    public void setRight_dorsalis_pedis_doppler(String right_dorsalis_pedis_doppler) {
        this.right_dorsalis_pedis_doppler = right_dorsalis_pedis_doppler;
    }
    /**
     * @return the left_posterior_tibial_doppler
     */
    public String getLeft_posterior_tibial_doppler() {
        return left_posterior_tibial_doppler;
    }
    /**
     * @param left_posterior_tibial_doppler the left_posterior_tibial_doppler to set
     */
    public void setLeft_posterior_tibial_doppler(String left_posterior_tibial_doppler) {
        this.left_posterior_tibial_doppler = left_posterior_tibial_doppler;
    }
    /**
     * @return the right_posterior_tibial_doppler
     */
    public String getRight_posterior_tibial_doppler() {
        return right_posterior_tibial_doppler;
    }
    /**
     * @param right_posterior_tibial_doppler the right_posterior_tibial_doppler to set
     */
    public void setRight_posterior_tibial_doppler(String right_posterior_tibial_doppler) {
        this.right_posterior_tibial_doppler = right_posterior_tibial_doppler;
    }
    /**
     * @return the left_interdigitial_doppler
     */
    public String getLeft_interdigitial_doppler() {
        return left_interdigitial_doppler;
    }
    /**
     * @param left_interdigitial_doppler the left_interdigitial_doppler to set
     */
    public void setLeft_interdigitial_doppler(String left_interdigitial_doppler) {
        this.left_interdigitial_doppler = left_interdigitial_doppler;
    }
    /**
     * @return the right_interdigitial_doppler
     */
    public String getRight_interdigitial_doppler() {
        return right_interdigitial_doppler;
    }
    /**
     * @param right_interdigitial_doppler the right_interdigitial_doppler to set
     */
    public void setRight_interdigitial_doppler(String right_interdigitial_doppler) {
        this.right_interdigitial_doppler = right_interdigitial_doppler;
    }
    /**
     * @return the ankle_brachial_lab
     */
    public Integer getAnkle_brachial_lab() {
        return ankle_brachial_lab;
    }
    /**
     * @param ankle_brachial_lab the ankle_brachial_lab to set
     */
    public void setAnkle_brachial_lab(Integer ankle_brachial_lab) {
        this.ankle_brachial_lab = ankle_brachial_lab;
    }
    /**
     * @return the ankle_brachial_date
     */
    public Date getAnkle_brachial_date() {
        return ankle_brachial_date;
    }
    /**
     * @param ankle_brachial_date the ankle_brachial_date to set
     */
    public void setAnkle_brachial_date(Date ankle_brachial_date) {
        this.ankle_brachial_date = ankle_brachial_date;
    }
    /**
     * @return the left_tibial_pedis_ankle_brachial
     */
    public Integer getLeft_tibial_pedis_ankle_brachial() {
        return left_tibial_pedis_ankle_brachial;
    }
    /**
     * @param left_tibial_pedis_ankle_brachial the left_tibial_pedis_ankle_brachial to set
     */
    public void setLeft_tibial_pedis_ankle_brachial(Integer left_tibial_pedis_ankle_brachial) {
        this.left_tibial_pedis_ankle_brachial = left_tibial_pedis_ankle_brachial;
    }
    /**
     * @return the right_tibial_pedis_ankle_brachial
     */
    public Integer getRight_tibial_pedis_ankle_brachial() {
        return right_tibial_pedis_ankle_brachial;
    }
    /**
     * @param right_tibial_pedis_ankle_brachial the right_tibial_pedis_ankle_brachial to set
     */
    public void setRight_tibial_pedis_ankle_brachial(Integer right_tibial_pedis_ankle_brachial) {
        this.right_tibial_pedis_ankle_brachial = right_tibial_pedis_ankle_brachial;
    }
    /**
     * @return the left_dorsalis_pedis_ankle_brachial
     */
    public Integer getLeft_dorsalis_pedis_ankle_brachial() {
        return left_dorsalis_pedis_ankle_brachial;
    }
    /**
     * @param left_dorsalis_pedis_ankle_brachial the left_dorsalis_pedis_ankle_brachial to set
     */
    public void setLeft_dorsalis_pedis_ankle_brachial(Integer left_dorsalis_pedis_ankle_brachial) {
        this.left_dorsalis_pedis_ankle_brachial = left_dorsalis_pedis_ankle_brachial;
    }
    /**
     * @return the right_dorsalis_pedis_ankle_brachial
     */
    public Integer getRight_dorsalis_pedis_ankle_brachial() {
        return right_dorsalis_pedis_ankle_brachial;
    }
    /**
     * @param right_dorsalis_pedis_ankle_brachial the right_dorsalis_pedis_ankle_brachial to set
     */
    public void setRight_dorsalis_pedis_ankle_brachial(Integer right_dorsalis_pedis_ankle_brachial) {
        this.right_dorsalis_pedis_ankle_brachial = right_dorsalis_pedis_ankle_brachial;
    }
    /**
     * @return the left_ankle_brachial
     */
    public Integer getLeft_ankle_brachial() {
        return left_ankle_brachial;
    }
    /**
     * @param left_ankle_brachial the left_ankle_brachial to set
     */
    public void setLeft_ankle_brachial(Integer left_ankle_brachial) {
        this.left_ankle_brachial = left_ankle_brachial;
    }
    /**
     * @return the right_ankle_brachial
     */
    public Integer getRight_ankle_brachial() {
        return right_ankle_brachial;
    }
    /**
     * @param right_ankle_brachial the right_ankle_brachial to set
     */
    public void setRight_ankle_brachial(Integer right_ankle_brachial) {
        this.right_ankle_brachial = right_ankle_brachial;
    }
    /**
     * @return the toe_brachial_lab
     */
    public Integer getToe_brachial_lab() {
        return toe_brachial_lab;
    }
    /**
     * @param toe_brachial_lab the toe_brachial_lab to set
     */
    public void setToe_brachial_lab(Integer toe_brachial_lab) {
        this.toe_brachial_lab = toe_brachial_lab;
    }
    /**
     * @return the toe_brachial_date
     */
    public Date getToe_brachial_date() {
        return toe_brachial_date;
    }
    /**
     * @param toe_brachial_date the toe_brachial_date to set
     */
    public void setToe_brachial_date(Date toe_brachial_date) {
        this.toe_brachial_date = toe_brachial_date;
    }
    /**
     * @return the left_toe_pressure
     */
    public String getLeft_toe_pressure() {
        return left_toe_pressure;
    }
    /**
     * @param left_toe_pressure the left_toe_pressure to set
     */
    public void setLeft_toe_pressure(String left_toe_pressure) {
        this.left_toe_pressure = left_toe_pressure;
    }
    /**
     * @return the right_toe_pressure
     */
    public String getRight_toe_pressure() {
        return right_toe_pressure;
    }
    /**
     * @param right_toe_pressure the right_toe_pressure to set
     */
    public void setRight_toe_pressure(String right_toe_pressure) {
        this.right_toe_pressure = right_toe_pressure;
    }
    /**
     * @return the left_brachial_pressure
     */
    public String getLeft_brachial_pressure() {
        return left_brachial_pressure;
    }
    /**
     * @param left_brachial_pressure the left_brachial_pressure to set
     */
    public void setLeft_brachial_pressure(String left_brachial_pressure) {
        this.left_brachial_pressure = left_brachial_pressure;
    }
    /**
     * @return the right_brachial_pressure
     */
    public String getRight_brachial_pressure() {
        return right_brachial_pressure;
    }
    /**
     * @param right_brachial_pressure the right_brachial_pressure to set
     */
    public void setRight_brachial_pressure(String right_brachial_pressure) {
        this.right_brachial_pressure = right_brachial_pressure;
    }
    /**
     * @return the left_transcutaneous_pressure
     */
    public Integer getLeft_transcutaneous_pressure() {
        return left_transcutaneous_pressure;
    }
    /**
     * @param left_transcutaneous_pressure the left_transcutaneous_pressure to set
     */
    public void setLeft_transcutaneous_pressure(Integer left_transcutaneous_pressure) {
        this.left_transcutaneous_pressure = left_transcutaneous_pressure;
    }
    /**
     * @return the right_transcutaneous_pressure
     */
    public Integer getRight_transcutaneous_pressure() {
        return right_transcutaneous_pressure;
    }
    /**
     * @param right_transcutaneous_pressure the right_transcutaneous_pressure to set
     */
    public void setRight_transcutaneous_pressure(Integer right_transcutaneous_pressure) {
        this.right_transcutaneous_pressure = right_transcutaneous_pressure;
    }
    /**
     * @return the left_sensation
     */
    public String getLeft_sensation() {
        return left_sensation;
    }
    /**
     * @param left_sensation the left_sensation to set
     */
    public void setLeft_sensation(String left_sensation) {
        this.left_sensation = left_sensation;
    }
    /**
     * @return the left_score_sensation
     */
    public String getLeft_score_sensation() {
        return left_score_sensation;
    }
    /**
     * @param left_score_sensation the left_score_sensation to set
     */
    public void setLeft_score_sensation(String left_score_sensation) {
        this.left_score_sensation = left_score_sensation;
    }
    /**
     * @return the right_sensation
     */
    public String getRight_sensation() {
        return right_sensation;
    }
    /**
     * @param right_sensation the right_sensation to set
     */
    public void setRight_sensation(String right_sensation) {
        this.right_sensation = right_sensation;
    }
    /**
     * @return the right_score_sensation
     */
    public String getRight_score_sensation() {
        return right_score_sensation;
    }
    /**
     * @param right_score_sensation the right_score_sensation to set
     */
    public void setRight_score_sensation(String right_score_sensation) {
        this.right_score_sensation = right_score_sensation;
    }
    /**
     * @return the left_foot_deformities
     */
    public String getLeft_foot_deformities() {
        return left_foot_deformities;
    }
    /**
     * @param left_foot_deformities the left_foot_deformities to set
     */
    public void setLeft_foot_deformities(String left_foot_deformities) {
        this.left_foot_deformities = left_foot_deformities;
    }
    /**
     * @return the right_foot_deformities
     */
    public String getRight_foot_deformities() {
        return right_foot_deformities;
    }
    /**
     * @param right_foot_deformities the right_foot_deformities to set
     */
    public void setRight_foot_deformities(String right_foot_deformities) {
        this.right_foot_deformities = right_foot_deformities;
    }
    /**
     * @return the left_foot_skin
     */
    public String getLeft_foot_skin() {
        return left_foot_skin;
    }
    /**
     * @param left_foot_skin the left_foot_skin to set
     */
    public void setLeft_foot_skin(String left_foot_skin) {
        this.left_foot_skin = left_foot_skin;
    }
    /**
     * @return the right_foot_skin
     */
    public String getRight_foot_skin() {
        return right_foot_skin;
    }
    /**
     * @param right_foot_skin the right_foot_skin to set
     */
    public void setRight_foot_skin(String right_foot_skin) {
        this.right_foot_skin = right_foot_skin;
    }
    /**
     * @return the left_foot_toes
     */
    public String getLeft_foot_toes() {
        return left_foot_toes;
    }
    /**
     * @param left_foot_toes the left_foot_toes to set
     */
    public void setLeft_foot_toes(String left_foot_toes) {
        this.left_foot_toes = left_foot_toes;
    }
    /**
     * @return the right_foot_toes
     */
    public String getRight_foot_toes() {
        return right_foot_toes;
    }
    /**
     * @param right_foot_toes the right_foot_toes to set
     */
    public void setRight_foot_toes(String right_foot_toes) {
        this.right_foot_toes = right_foot_toes;
    }
    /**
     * @return the professional
     */
    public ProfessionalVO getProfessional() {
        return professional;
    }
    /**
     * @param professional the professional to set
     */
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    
    
    public Vector getTranscutaneousDate() {
        PDate pdate = new PDate(Common.getConfig("timezone"));
        Vector dates= pdate.getDateInVector(getTranscutaneous_date());
        return dates;
    }
    
    public Vector getAnkleBrachialDate() {
        PDate pdate = new PDate(Common.getConfig("timezone"));
        Vector dates= pdate.getDateInVector(ankle_brachial_date);
        return dates;
    }
   
    public Vector getToeBrachialDate() {
        PDate pdate = new PDate(Common.getConfig("timezone"));
        Vector dates= pdate.getDateInVector(toe_brachial_date);
        return dates;
    }
    /**
     * @return the transcutaneous_lab
     */
    public Integer getTranscutaneous_lab() {
        return transcutaneous_lab;
    }
    /**
     * @param transcutaneous_lab the transcutaneous_lab to set
     */
    public void setTranscutaneous_lab(Integer transcutaneous_lab) {
        this.transcutaneous_lab = transcutaneous_lab;
    }
    /**
     * @return the transcutaneous_date
     */
    public Date getTranscutaneous_date() {
        return transcutaneous_date;
    }
    /**
     * @param transcutaneous_date the transcutaneous_date to set
     */
    public void setTranscutaneous_date(Date transcutaneous_date) {
        this.transcutaneous_date = transcutaneous_date;
    }
    /**
     * @return the deleted
     */
    public Integer getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    /**
     * @return the delete_signature
     */
    public String getDelete_signature() {
        return delete_signature;
    }
    /**
     * @param delete_signature the delete_signature to set
     */
    public void setDelete_signature(String delete_signature) {
        this.delete_signature = delete_signature;
    }
    /**
     * @return the deleted_reason
     */
    public String getDeleted_reason() {
        return deleted_reason;
    }
    /**
     * @param deleted_reason the deleted_reason to set
     */
    public void setDeleted_reason(String deleted_reason) {
        this.deleted_reason = deleted_reason;
    }
    
    /**
     * @return the left_stemmers
     */
    public Integer getLeft_stemmers() {
        return left_stemmers;
    }
    /**
     * @param left_stemmers the left_stemmers to set
     */
    public void setLeft_stemmers(Integer left_stemmers) {
        this.left_stemmers = left_stemmers;
    }
    /**
     * @return the right_stemmers
     */
    public Integer getRight_stemmers() {
        return right_stemmers;
    }
    /**
     * @param right_stemmers the right_stemmers to set
     */
    public void setRight_stemmers(Integer right_stemmers) {
        this.right_stemmers = right_stemmers;
    }
    /**
     * @return the left_homans
     */
    public Integer getLeft_homans() {
        return left_homans;
    }
    /**
     * @param left_homans the left_homans to set
     */
    public void setLeft_homans(Integer left_homans) {
        this.left_homans = left_homans;
    }
    /**
     * @return the right_homans
     */
    public Integer getRight_homans() {
        return right_homans;
    }
    /**
     * @param right_homans the right_homans to set
     */
    public void setRight_homans(Integer right_homans) {
        this.right_homans = right_homans;
    }
    /**
     * @return the referral_vascular_assessment
     */
    public Integer getReferral_vascular_assessment() {
        return referral_vascular_assessment;
    }
    /**
     * @param referral_vascular_assessment the referral_vascular_assessment to set
     */
    public void setReferral_vascular_assessment(Integer referral_vascular_assessment) {
        this.referral_vascular_assessment = referral_vascular_assessment;
    }
    /**
     * @return the left_pain_assessment
     */
    public String getLeft_pain_assessment() {
        return left_pain_assessment;
    }
    /**
     * @param left_pain_assessment the left_pain_assessment to set
     */
    public void setLeft_pain_assessment(String left_pain_assessment) {
        this.left_pain_assessment = left_pain_assessment;
    }
    /**
     * @return the right_pain_assessment
     */
    public String getRight_pain_assessment() {
        return right_pain_assessment;
    }
    /**
     * @param right_pain_assessment the right_pain_assessment to set
     */
    public void setRight_pain_assessment(String right_pain_assessment) {
        this.right_pain_assessment = right_pain_assessment;
    }
    /**
     * @return the left_temperature_leg
     */
    public Integer getLeft_temperature_leg() {
        return left_temperature_leg;
    }
    /**
     * @param left_temperature_leg the left_temperature_leg to set
     */
    public void setLeft_temperature_leg(Integer left_temperature_leg) {
        this.left_temperature_leg = left_temperature_leg;
    }
    /**
     * @return the right_temperature_leg
     */
    public Integer getRight_temperature_leg() {
        return right_temperature_leg;
    }
    /**
     * @param right_temperature_leg the right_temperature_leg to set
     */
    public void setRight_temperature_leg(Integer right_temperature_leg) {
        this.right_temperature_leg = right_temperature_leg;
    }
    /**
     * @return the left_temperature_foot
     */
    public Integer getLeft_temperature_foot() {
        return left_temperature_foot;
    }
    /**
     * @param left_temperature_foot the left_temperature_foot to set
     */
    public void setLeft_temperature_foot(Integer left_temperature_foot) {
        this.left_temperature_foot = left_temperature_foot;
    }
    /**
     * @return the right_temperature_foot
     */
    public Integer getRight_temperature_foot() {
        return right_temperature_foot;
    }
    /**
     * @param right_temperature_foot the right_temperature_foot to set
     */
    public void setRight_temperature_foot(Integer right_temperature_foot) {
        this.right_temperature_foot = right_temperature_foot;
    }
    /**
     * @return the left_temperature_toes
     */
    public Integer getLeft_temperature_toes() {
        return left_temperature_toes;
    }
    /**
     * @param left_temperature_toes the left_temperature_toes to set
     */
    public void setLeft_temperature_toes(Integer left_temperature_toes) {
        this.left_temperature_toes = left_temperature_toes;
    }
    /**
     * @return the right_temperature_toes
     */
    public Integer getRight_temperature_toes() {
        return right_temperature_toes;
    }
    /**
     * @param right_temperature_toes the right_temperature_toes to set
     */
    public void setRight_temperature_toes(Integer right_temperature_toes) {
        this.right_temperature_toes = right_temperature_toes;
    }
    /**
     * @return the limb_comments
     */
    public String getLimb_comments() {
        return limb_comments;
    }
    /**
     * @param limb_comments the limb_comments to set
     */
    public void setLimb_comments(String limb_comments) {
        this.limb_comments = limb_comments;
    }
    /**
     * @return the right_digit_total
     */
    public Integer getRight_digit_total() {
        return right_digit_total;
    }
    /**
     * @param right_digit_total the right_digit_total to set
     */
    public void setRight_digit_total(Integer right_digit_total) {
        this.right_digit_total = right_digit_total;
    }
    /**
     * @return the left_digit_total
     */
    public Integer getLeft_digit_total() {
        return left_digit_total;
    }
    /**
     * @param left_digit_total the left_digit_total to set
     */
    public void setLeft_digit_total(Integer left_digit_total) {
        this.left_digit_total = left_digit_total;
    }
    /**
     * @return the left_api_score
     */
    public Double getLeft_api_score() {
        return left_api_score;
    }
    /**
     * @param left_api_score the left_api_score to set
     */
    public void setLeft_api_score(Double left_api_score) {
        this.left_api_score = left_api_score;
    }
    /**
     * @return the right_api_score
     */
    public Double getRight_api_score() {
        return right_api_score;
    }
    /**
     * @param right_api_score the right_api_score to set
     */
    public void setRight_api_score(Double right_api_score) {
        this.right_api_score = right_api_score;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
      private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }

    /**
     * @return the left_limb_shape
     */
    public String getLeft_limb_shape() {
        return left_limb_shape;
    }

    /**
     * @param left_limb_shape the left_limb_shape to set
     */
    public void setLeft_limb_shape(String left_limb_shape) {
        this.left_limb_shape = left_limb_shape;
    }

    /**
     * @return the right_limb_shape
     */
    public String getRight_limb_shape() {
        return right_limb_shape;
    }

    /**
     * @param right_limb_shape the right_limb_shape to set
     */
    public void setRight_limb_shape(String right_limb_shape) {
        this.right_limb_shape = right_limb_shape;
    }
}
