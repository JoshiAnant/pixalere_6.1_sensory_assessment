package com.pixalere.patient.bean;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.utils.TripleDes;
import com.pixalere.utils.Common;
import java.sql.Timestamp;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import javax.xml.datatype.XMLGregorianCalendar;
/**
 *     This is the PatientAccountVO  bean for the patient_accounts  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientServiceImpl } for the calls
 *     which utilize this bean.
 *
@has 1..1 Has 1..1 com.pixalere.patient.bean.PatientAccountVO
 */
public class PatientAccountVO extends ValueObject implements Serializable {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientAccountVO.class);
    /** identifier field */
    private Integer patient_residence;
    private Integer gender;//@todo.. change to string, store as M or F or Male or Female
    private String phn2;
    private String phn3;
    private String dob;
    private Integer patient_id;
    private String phn;
    private Integer outofprovince;
    
    private Integer funding_source;
    /** nullable persistent field */
    private String firstName;
    private String middleName;
    private String lastName;
    private Integer isAdmitted;
    private String lastName_search;
    private String version_code;
    /*private String addressLine1;
    private String addressLine2;
    private String addressCity;
    private String addressProvince;
    private String addressPostalCode;
     */
    /** nullable persistent field */
    private Integer account_status;
    /** nullable persistent field */
    private Integer treatment_location_id;
    private Date discharge_date;
    private Integer id;
    private Integer current_flag;
    private Integer visits;
    /** nullable persistent field */
    private Integer created_by;
    private String user_signature;
    private String action;
    /** nullable persistent field */
    private PatientsVO patient;
    /** nullable persistent field */
    private Integer discharge_reason;
    private String allergies;
    private Date created_on;
    
    /* Patient Consent Contract fields */
    private String patient_consent_name;
    private String patient_consent_sig;
    private String patient_consent_blurb;
    private Integer consent_services;
    private Integer consent_information;
    private Integer consent_verbal;
    private Date consent_date;
    
    /*public Vector getUnserializedString(String database){
		String serialized="";
		com.pixalere.admin.dao.ListsDAO listBD=new com.pixalere.admin.dao.ListsDAO();
		LookupVO lookupVO=new LookupVO();
		com.pixalere.admin.ListBD listDAO=new com.pixalere.admin.ListBD();
		try{
    return Common.unserializeWithTextForCategories(getDecrypted(treatment_location_id+"","pass4treat"),listDAO.findAllResourcesWithCategories(lookupVO.TREATMENT_LOCATION));
    }catch(Exception e){e.printStackTrace();return new Vector();}
    }*/
    public Integer getCurrent_flag() {
        return current_flag;
    }
    public void setCurrent_flag(Integer current_flag) {
        this.current_flag = current_flag;
    }

    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getVisits() {
        return visits;
    }
    public void setVisits(Integer visits) {
        this.visits = visits;
    }
    public Integer getOutofprovince() {
        return outofprovince;
    }
    public void setOutofprovince(Integer outofprovince) {
        this.outofprovince = outofprovince;
    }
    private LookupVO treatmentLocation;
    /** default constructor */
    public PatientAccountVO() {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public LookupVO getTreatmentLocation() {
        return this.treatmentLocation;
    }
    public void setTreatmentLocation(LookupVO treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    public String getPhn() {
        return this.phn;
    }
    public void setPhn(String phn) {
        this.phn = phn;
    }
    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getLastName_search() {
        return this.lastName_search;
    }
    public void setLastName_search(String lastName_search) {
        this.lastName_search = lastName_search;
    }
    public Integer getAccount_status() {
        return this.account_status;
    }
    public void setAccount_status(Integer account_status) {
        this.account_status = account_status;
    }
    public Integer getTreatment_location_id() {
        return this.treatment_location_id;
    }
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }
    public Integer getCreated_by() {
        return this.created_by;
    }
    public void setCreated_by(Integer created_by) {
        this.created_by = created_by;
    }
    public int parseString(String value) {
        return Integer.parseInt(value);
    }
    public String getEncrypted(String value, String password) {
        try {
            if (Common.getLocalizedString("pixalere.encrypted","en").equals("1")) {
                TripleDes des = new TripleDes(password);
                return des.encrypt(value);
            } else {
                return value;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    public String getDecrypted(String strInput, String strPassword) {
        try {
            if (Common.getLocalizedString("pixalere.encrypted","en").equals("1")) {
                TripleDes des = new TripleDes(strPassword);
                return des.decrypt(strInput);
            } else {
                return strInput;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    public boolean isPatientConsent(){
        boolean hasConsent = false;
            
        boolean approvedConsent = 
                (this.patient_consent_sig != null && 
                !this.patient_consent_sig.isEmpty()) || 
                (this.consent_verbal != null &&
                !this.consent_verbal.equals(0));
        boolean terms = this.consent_services != null && 
                this.consent_services.equals(1) && 
                this.consent_information != null &&
                this.consent_information.equals(1);
        if (approvedConsent && terms){
            hasConsent = true;
        }

        return hasConsent;
    }
    
    public String getPatientName() {
        String p =  getLastName()+", "+getFirstName();
        
        return p;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public PatientsVO getPatient() {
        return patient;
    }
    public void setPatient(PatientsVO patient) {
        this.patient = patient;
    }
    public Integer getPatient_residence() {
        return patient_residence;
    }
    public void setPatient_residence(Integer patient_residence) {
        this.patient_residence = patient_residence;
    }
    public String getDob() {
        return dob;
    }
    public void setDob(String dob) {
        this.dob = dob;
    }
    public String getPhn3() {
        return phn3;
    }
    public void setPhn3(String phn3) {
        this.phn3 = phn3;
    }
    public String getPhn2() {
        return phn2;
    }
    public void setPhn2(String phn2) {
        this.phn2 = phn2;
    }
    public void setGender(Integer gender) {
        this.gender = gender;
    }
    public Integer getGender() {
        return gender;
    }
    /*
    public String getAddressLine1() {
    return addressLine1;
    }
    public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
    }
    public String getAddressLine2() {
    return addressLine2;
    }
    public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
    }
    public String getAddressCity() {
    return addressCity;
    }
    public void setAddressCity(String addressCity) {
    this.addressCity = addressCity;
    }
    public String getAddressProvince() {
    return addressProvince;
    }
    public void setAddressProvince(String addressProvince) {
    this.addressProvince = addressProvince;
    }
    public String getAddressPostalCode() {
    return addressPostalCode;
    }
    public void setAddressPostalCode(String addressPostalCode) {
    this.addressPostalCode = addressPostalCode;
    }*/
    public String getYear(){
        try{
        if(dob!=null){
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ss");
            Date date = sf.parse(dob);
           SimpleDateFormat sff = new SimpleDateFormat("yyyy");
            String date2 = sff.format(date);
            return date2;
        }else {
            return "";
        }
        }catch(ParseException e){
            //e.printStackTrace();
            return "";
        }

    }
    public String getMonth(){
        try{
        if(dob!=null){
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ss");
            Date date = sf.parse(dob);
           SimpleDateFormat sff = new SimpleDateFormat("M");
            String date2 = sff.format(date);
            return date2;
        }else {
            return "";
        }
        }catch(ParseException e){
            //e.printStackTrace();
            return "";
        }

    }
    public String getDay(){
        try{
        if(dob!=null){
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ss");
            Date date = sf.parse(dob);
           SimpleDateFormat sff = new SimpleDateFormat("d");
            String date2 = sff.format(date);
            return date2;
        }else {
            return "";
        }
        }catch(ParseException e){
            //e.printStackTrace();
            return "";
        }

    }
    public Date getDateOfBirth(){
        try{
        if(dob!=null && !dob.equals("")){
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ss");
            Date date = sf.parse(dob);
            return date;
        }else {
            return null;
        }
        }catch(ParseException e){
            e.printStackTrace();
            return null;
        }
    }
    public String getDateOfBirthStr(){
        try{
        if(dob!=null && !dob.equals("")){
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ss");
            Date date = sf.parse(dob);
            SimpleDateFormat sff = new SimpleDateFormat("dd/MMM/yyyy");
            String date2 = sff.format(date);
            return date2;
        }else {
            return null;
        }
        }catch(ParseException e){
            e.printStackTrace();
            return null;
        }
    }
    /**
     * @return the allergies
     */
    public String getAllergies() {
        return allergies;
    }
    /**
     * @param allergies the allergies to set
     */
    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }
    /**
     * @return the time_stamp
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param time_stamp the time_stamp to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    /**
     * @return the isAdmitted
     */
    public Integer getIsAdmitted() {
        return isAdmitted;
    }
    /**
     * @param isAdmitted the isAdmitted to set
     */
    public void setIsAdmitted(Integer isAdmitted) {
        this.isAdmitted = isAdmitted;
    }
    /**
     * @return the version_code
     */
    public String getVersion_code() {
        return version_code;
    }
    /**
     * @param version_code the version_code to set
     */
    public void setVersion_code(String version_code) {
        this.version_code = version_code;
    }
    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return the discharge_reason
     */
    public Integer getDischarge_reason() {
        return discharge_reason;
    }
    /**
     * @param discharge_reason the discharge_reason to set
     */
    public void setDischarge_reason(Integer discharge_reason) {
        this.discharge_reason = discharge_reason;
    }
    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * @return the discharge_date
     */
    public Date getDischarge_date() {
        return discharge_date;
    }
    /**
     * @param discharge_date the discharge_date to set
     */
    public void setDischarge_date(Date discharge_date) {
        this.discharge_date = discharge_date;
    }
    /**
     * @return the funding_source
     */
    public Integer getFunding_source() {
        return funding_source;
    }
    /**
     * @param funding_source the funding_source to set
     */
    public void setFunding_source(Integer funding_source) {
        this.funding_source = funding_source;
    }

    public String getPatient_consent_sig() {
        return patient_consent_sig;
    }

    public void setPatient_consent_sig(String patient_consent_sig) {
        this.patient_consent_sig = patient_consent_sig;
    }

    public String getPatient_consent_blurb() {
        return patient_consent_blurb;
    }

    public void setPatient_consent_blurb(String patient_consent_blurb) {
        this.patient_consent_blurb = patient_consent_blurb;
    }

    public Integer getConsent_services() {
        return consent_services;
    }

    public void setConsent_services(Integer consent_services) {
        this.consent_services = consent_services;
    }

    public Integer getConsent_information() {
        return consent_information;
    }

    public void setConsent_information(Integer consent_information) {
        this.consent_information = consent_information;
    }

    public Integer getConsent_verbal() {
        return consent_verbal;
    }

    public void setConsent_verbal(Integer consent_verbal) {
        this.consent_verbal = consent_verbal;
    }

    public Date getConsent_date() {
        return consent_date;
    }

    public void setConsent_date(Date consent_date) {
        this.consent_date = consent_date;
    }

    public String getPatient_consent_name() {
        return patient_consent_name;
    }

    public void setPatient_consent_name(String patient_consent_name) {
        this.patient_consent_name = patient_consent_name;
    }

}
