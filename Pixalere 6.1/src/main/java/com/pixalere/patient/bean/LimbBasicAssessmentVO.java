/*
 * LimbAssessmentVO.java
 *
 * Created on September 17, 2007, 2:42 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.patient.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Date;
/**
 *     This is the LimbAssessmentVO  bean for the limb_assessment  (DB table)
 *     Refer to {@link com.pixalere.patient.service.PatientProfileServiceImpl } for the calls
 *     which utilize this bean.
 *
 *
 */
public class LimbBasicAssessmentVO extends ValueObject implements Serializable{
    private String user_signature;
    private String limb_comments;
    private Date created_on;
    private Integer left_less_capillary;
    private Integer right_less_capillary;
    private Integer left_more_capillary;
    private Integer right_more_capillary;
    private Integer left_range_motion_ankle;
    private Integer right_range_motion_ankle;
    private Integer left_range_motion_knee;
    private Integer right_range_motion_knee;
    private Integer left_range_motion_toe;
    private Integer right_range_motion_toe;
     private Date lastmodified_on;
    private Integer limb_show;
    private Integer active;
    private Integer professional_id;
    private ProfessionalVO professional;
    private String left_missing_limbs;
    private String right_missing_limbs;
    private Integer id;
    private Integer patient_id;
    private String left_pain_assessment;
    private String right_pain_assessment;
    private String pain_comments;
    private String left_skin_assessment;
    private String right_skin_assessment;
    private Integer left_temperature_leg;
    private Integer right_temperature_leg;
    private Integer left_temperature_foot;
    private Integer right_temperature_foot;
    private Integer left_temperature_toes;
    private Integer right_temperature_toes;
    
    private Integer left_skin_colour_leg;
    private Integer right_skin_colour_leg;
    private Integer left_skin_colour_foot;
    private Integer right_skin_colour_foot;
    private Integer left_skin_colour_toes;
    private Integer right_skin_colour_toes;
    
    private Integer left_dorsalis_pedis_palpation;
    private Integer right_dorsalis_pedis_palpation;
    private Integer left_posterior_tibial_palpation;
    private Integer right_posterior_tibial_palpation;
    
   
    private Integer left_edema_assessment;
    private Integer right_edema_assessment;
    
    private Integer left_edema_severity;
    private Integer right_edema_severity;
    
    private Integer left_edema_location;
    private Integer right_edema_location;
    
    private String sleep_positions;
    private Integer left_ankle_cm;
    private Integer left_ankle_mm;
    private Integer right_ankle_cm;
    private Integer right_ankle_mm;
    private Integer left_midcalf_cm;
    private Integer left_midcalf_mm;
    private Integer right_midcalf_cm;
    private Integer right_midcalf_mm;
    
    private String left_sensory;
    
    private String right_sensory;
    private Integer deleted;
    private String delete_signature;
    private String deleted_reason;
    private Date deleted_on;
        /**
     * @return the deleted_on
     */
    public Date getDeleted_on() {
        return deleted_on;
    }
    /**
     * @param deleted_on the deleted_on to set
     */
    public void setDeleted_on(Date deleted_on) {
        this.deleted_on = deleted_on;
    }
     public Integer getDeleted() {
         return deleted;
     }
     public void setDeleted(Integer deleted) {
         this.deleted = deleted;
     }
     public String getDeleted_reason() {
         return deleted_reason;
     }
     public void setDeleted_reason(String deleted_reason) {
         this.deleted_reason = deleted_reason;
     }
     public String getDelete_signature() {
         return delete_signature;
     }
     public void setDelete_signature(String delete_signature) {
         this.delete_signature = delete_signature;
     }
    /** Creates a new instance of LimbAssessmentVO */
    public LimbBasicAssessmentVO() {
    }
    public String getLeft_missing_limbs() {
        return left_missing_limbs;
    }
    public void setLeft_missing_limbs(String left_missing_limbs) {
        this.left_missing_limbs = left_missing_limbs;
    }
    public String getRight_missing_limbs() {
        return right_missing_limbs;
    }
    public void setRight_missing_limbs(String right_missing_limbs) {
        this.right_missing_limbs = right_missing_limbs;
    }
    public String getLeft_pain_assessment() {
        return left_pain_assessment;
    }
    public void setLeft_pain_assessment(String left_pain_assessment) {
        this.left_pain_assessment = left_pain_assessment;
    }
    public String getRight_pain_assessment() {
        return right_pain_assessment;
    }
    public void setRight_pain_assessment(String right_pain_assessment) {
        this.right_pain_assessment = right_pain_assessment;
    }
  
    public String getPain_comments() {
        return pain_comments;
    }
    public void setPain_comments(String pain_comments) {
        this.pain_comments = pain_comments;
    }
    public String getLeft_skin_assessment() {
        return left_skin_assessment;
    }
    public void setLeft_skin_assessment(String left_skin_assessment) {
        this.left_skin_assessment = left_skin_assessment;
    }
    public String getRight_skin_assessment() {
        return right_skin_assessment;
    }
    public void setRight_skin_assessment(String right_skin_assessment) {
        this.right_skin_assessment = right_skin_assessment;
    }
   
    public Integer getLeft_temperature_leg() {
        return left_temperature_leg;
    }
    public void setLeft_temperature_leg(Integer left_temperature_leg) {
        this.left_temperature_leg = left_temperature_leg;
    }
    public Integer getRight_temperature_leg() {
        return right_temperature_leg;
    }
    public void setRight_temperature_leg(Integer right_temperature_leg) {
        this.right_temperature_leg = right_temperature_leg;
    }
    public Integer getLeft_temperature_foot() {
        return left_temperature_foot;
    }
    public void setLeft_temperature_foot(Integer left_temperature_foot) {
        this.left_temperature_foot = left_temperature_foot;
    }
    public Integer getRight_temperature_foot() {
        return right_temperature_foot;
    }
    public void setRight_temperature_foot(Integer right_temperature_foot) {
        this.right_temperature_foot = right_temperature_foot;
    }
    public Integer getLeft_temperature_toes() {
        return left_temperature_toes;
    }
    public void setLeft_temperature_toes(Integer left_temperature_toes) {
        this.left_temperature_toes = left_temperature_toes;
    }
    public Integer getRight_temperature_toes() {
        return right_temperature_toes;
    }
    public void setRight_temperature_toes(Integer right_temperature_toes) {
        this.right_temperature_toes = right_temperature_toes;
    }
    
    public Integer getLeft_skin_colour_leg() {
        return left_skin_colour_leg;
    }
    public void setLeft_skin_colour_leg(Integer left_skin_colour_leg) {
        this.left_skin_colour_leg = left_skin_colour_leg;
    }
    public Integer getRight_skin_colour_leg() {
        return right_skin_colour_leg;
    }
    public void setRight_skin_colour_leg(Integer right_skin_colour_leg) {
        this.right_skin_colour_leg = right_skin_colour_leg;
    }
    public Integer getLeft_skin_colour_foot() {
        return left_skin_colour_foot;
    }
    public void setLeft_skin_colour_foot(Integer left_skin_colour_foot) {
        this.left_skin_colour_foot = left_skin_colour_foot;
    }
    public Integer getRight_skin_colour_foot() {
        return right_skin_colour_foot;
    }
    public void setRight_skin_colour_foot(Integer right_skin_colour_foot) {
        this.right_skin_colour_foot = right_skin_colour_foot;
    }
    public Integer getLeft_skin_colour_toes() {
        return left_skin_colour_toes;
    }
    public void setLeft_skin_colour_toes(Integer left_skin_colour_toes) {
        this.left_skin_colour_toes = left_skin_colour_toes;
    }
    public Integer getRight_skin_colour_toes() {
        return right_skin_colour_toes;
    }
    public void setRight_skin_colour_toes(Integer right_skin_colour_toes) {
        this.right_skin_colour_toes = right_skin_colour_toes;
    }

    public Integer getLeft_dorsalis_pedis_palpation() {
        return left_dorsalis_pedis_palpation;
    }
    public void setLeft_dorsalis_pedis_palpation(Integer left_dorsalis_pedis_palpation) {
        this.left_dorsalis_pedis_palpation = left_dorsalis_pedis_palpation;
    }
    public Integer getRight_dorsalis_pedis_palpation() {
        return right_dorsalis_pedis_palpation;
    }
    public void setRight_dorsalis_pedis_palpation(Integer right_dorsalis_pedis_palpation) {
        this.right_dorsalis_pedis_palpation = right_dorsalis_pedis_palpation;
    }
    public Integer getLeft_posterior_tibial_palpation() {
        return left_posterior_tibial_palpation;
    }
    public void setLeft_posterior_tibial_palpation(Integer left_posterior_tibial_palpation) {
        this.left_posterior_tibial_palpation = left_posterior_tibial_palpation;
    }
    public Integer getRight_posterior_tibial_palpation() {
        return right_posterior_tibial_palpation;
    }
    public void setRight_posterior_tibial_palpation(Integer right_posterior_tibial_palpation) {
        this.right_posterior_tibial_palpation = right_posterior_tibial_palpation;
    }

    public Integer getLeft_edema_severity() {
        return left_edema_severity;
    }
    public void setLeft_edema_severity(Integer left_edema_severity) {
        this.left_edema_severity = left_edema_severity;
    }
    public Integer getRight_edema_severity() {
        return right_edema_severity;
    }
    public void setRight_edema_severity(Integer right_edema_severity) {
        this.right_edema_severity = right_edema_severity;
    }
    public Integer getLeft_edema_location() {
        return left_edema_location;
    }
    public void setLeft_edema_location(Integer left_edema_location) {
        this.left_edema_location = left_edema_location;
    }
    public Integer getRight_edema_location() {
        return right_edema_location;
    }
    public void setRight_edema_location(Integer right_edema_location) {
        this.right_edema_location = right_edema_location;
    }
    public String getSleep_positions() {
        return sleep_positions;
    }
    public void setSleep_positions(String sleep_positions) {
        this.sleep_positions = sleep_positions;
    }
  
    public Integer getLeft_ankle_cm() {
        return left_ankle_cm;
    }
    public void setLeft_ankle_cm(Integer left_ankle_cm) {
        this.left_ankle_cm = left_ankle_cm;
    }
    public Integer getLeft_ankle_mm() {
        return left_ankle_mm;
    }
    public void setLeft_ankle_mm(Integer left_ankle_mm) {
        this.left_ankle_mm = left_ankle_mm;
    }
    public Integer getRight_ankle_cm() {
        return right_ankle_cm;
    }
    public void setRight_ankle_cm(Integer right_ankle_cm) {
        this.right_ankle_cm = right_ankle_cm;
    }
    public Integer getRight_ankle_mm() {
        return right_ankle_mm;
    }
    public void setRight_ankle_mm(Integer right_ankle_mm) {
        this.right_ankle_mm = right_ankle_mm;
    }
    public Integer getLeft_midcalf_cm() {
        return left_midcalf_cm;
    }
    public void setLeft_midcalf_cm(Integer left_midcalf_cm) {
        this.left_midcalf_cm = left_midcalf_cm;
    }
    public Integer getLeft_midcalf_mm() {
        return left_midcalf_mm;
    }
    public void setLeft_midcalf_mm(Integer left_midcalf_mm) {
        this.left_midcalf_mm = left_midcalf_mm;
    }
    public Integer getRight_midcalf_cm() {
        return right_midcalf_cm;
    }
    public void setRight_midcalf_cm(Integer right_midcalf_cm) {
        this.right_midcalf_cm = right_midcalf_cm;
    }
    public Integer getRight_midcalf_mm() {
        return right_midcalf_mm;
    }
    public void setRight_midcalf_mm(Integer right_midcalf_mm) {
        this.right_midcalf_mm = right_midcalf_mm;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    public String getLeft_sensory() {
        return left_sensory;
    }
    public void setLeft_sensory(String left_sensory) {
        this.left_sensory = left_sensory;
    }
    public String getRight_sensory() {
        return right_sensory;
    }
    public void setRight_sensory(String right_sensory) {
        this.right_sensory = right_sensory;
    }
    public String getUser_signature() {
        return user_signature;
    }
    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }

    public Integer getActive() {
        return active;
    }
    public void setActive(Integer active) {
        this.active = active;
    }
    public Integer getProfessional_id() {
        return professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public ProfessionalVO getProfessional() {
        return professional;
    }
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    public Integer getLimb_show() {
        return limb_show;
    }
    public void setLimb_show(Integer limb_show) {
        this.limb_show = limb_show;
    }
    public Integer getLeft_less_capillary() {
        return left_less_capillary;
    }
    public void setLeft_less_capillary(Integer left_less_capillary) {
        this.left_less_capillary = left_less_capillary;
    }
    public Integer getRight_less_capillary() {
        return right_less_capillary;
    }
    public void setRight_less_capillary(Integer right_less_capillary) {
        this.right_less_capillary = right_less_capillary;
    }
    public Integer getLeft_more_capillary() {
        return left_more_capillary;
    }
    public void setLeft_more_capillary(Integer left_more_capillary) {
        this.left_more_capillary = left_more_capillary;
    }
    public Integer getRight_more_capillary() {
        return right_more_capillary;
    }
    public void setRight_more_capillary(Integer right_more_capillary) {
        this.right_more_capillary = right_more_capillary;
    }
   
    public String getLimb_comments() {
        return limb_comments;
    }
    public void setLimb_comments(String limb_comments) {
        this.limb_comments = limb_comments;
    }
    /**
     * @return the left_range_motion_ankle
     */
    public Integer getLeft_range_motion_ankle() {
        return left_range_motion_ankle;
    }
    /**
     * @param left_range_motion_ankle the left_range_motion_ankle to set
     */
    public void setLeft_range_motion_ankle(Integer left_range_motion_ankle) {
        this.left_range_motion_ankle = left_range_motion_ankle;
    }
    /**
     * @return the right_range_motion_ankle
     */
    public Integer getRight_range_motion_ankle() {
        return right_range_motion_ankle;
    }
    /**
     * @param right_range_motion_ankle the right_range_motion_ankle to set
     */
    public void setRight_range_motion_ankle(Integer right_range_motion_ankle) {
        this.right_range_motion_ankle = right_range_motion_ankle;
    }
    /**
     * @return the left_range_motion_knee
     */
    public Integer getLeft_range_motion_knee() {
        return left_range_motion_knee;
    }
    /**
     * @param left_range_motion_knee the left_range_motion_knee to set
     */
    public void setLeft_range_motion_knee(Integer left_range_motion_knee) {
        this.left_range_motion_knee = left_range_motion_knee;
    }
    /**
     * @return the right_range_motion_knee
     */
    public Integer getRight_range_motion_knee() {
        return right_range_motion_knee;
    }
    /**
     * @param right_range_motion_knee the right_range_motion_knee to set
     */
    public void setRight_range_motion_knee(Integer right_range_motion_knee) {
        this.right_range_motion_knee = right_range_motion_knee;
    }
    /**
     * @return the left_range_motion_toes
     */
    public Integer getLeft_range_motion_toe() {
        return left_range_motion_toe;
    }
    /**
     * @param left_range_motion_toes the left_range_motion_toes to set
     */
    public void setLeft_range_motion_toe(Integer left_range_motion_toe) {
        this.left_range_motion_toe = left_range_motion_toe;
    }
    /**
     * @return the right_range_motion_toes
     */
    public Integer getRight_range_motion_toe() {
        return right_range_motion_toe;
    }
    /**
     * @param right_range_motion_toes the right_range_motion_toes to set
     */
    public void setRight_range_motion_toe(Integer right_range_motion_toe) {
        this.right_range_motion_toe = right_range_motion_toe;
    }
    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    /**
     * @return the lastmodified_on
     */
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    /**
     * @param lastmodified_on the lastmodified_on to set
     */
    public void setLastmodified_on(Date lastmodified_on) {
        this.lastmodified_on = lastmodified_on;
    }
  
    private Date visited_on;
    /**
     * @return the visited_on
     */
    public Date getVisited_on() {
        return visited_on;
    }
    /**
     * @param visited_on the visited_on to set
     */
    public void setVisited_on(Date visited_on) {
        this.visited_on = visited_on;
    }
}
