package com.pixalere.patient.client;
import com.pixalere.patient.service.PatientService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class PatientClient extends AbstractClient{
    public PatientClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  PatientService createPatientClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(PatientService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"PatientService");
        PatientService service = (PatientService) getClient();       
        return service;
    }
}