package com.pixalere.patient.client;
import com.pixalere.patient.service.PatientProfileService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class PatientProfileClient extends AbstractClient{
    public PatientProfileClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  PatientProfileService createPatientProfileClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(PatientProfileService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"PatientProfileService");
        PatientProfileService service = (PatientProfileService) getClient();       
        return service;
    }
}