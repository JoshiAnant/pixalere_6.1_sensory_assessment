package com.pixalere.patient.service;

import java.util.HashMap;
import java.util.Date;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.guibeans.WoundProfileItem;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.bean.PatientsVO;
import com.pixalere.admin.dao.AssignPatientsDAO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.patient.dao.*;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.assessment.service.NursingCarePlanServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.guibeans.PatientList;
import com.pixalere.common.*;
import com.pixalere.utils.*;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.common.bean.ComponentsVO;
import java.util.Vector;
import java.util.Collection;
import com.pixalere.guibeans.CareType;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;
import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import com.pixalere.integration.bean.EventVO;
import com.pixalere.patient.bean.PhysicalExamVO;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This is the patient service implementation Refer to {@link com.pixalere.patient.service.PatientService
 * } to see if there are any web services available.
 *
 * <img src="PatientServiceImpl.png"/>
 *
 *
 * @view PatientServiceImpl
 *
 * @match class com.pixalere.patient.
 *
 * @opt hide
 * @match class
 * com.pixalere.patient\.(dao.PatientDAO|bean.PatientAccountVO|service.PatientService)
 * @opt !hide
 *
 */
public class PatientServiceImpl implements PatientService {
    // Create Log4j category instance for logging

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientServiceImpl.class);
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    private Integer language = 1;

    public PatientServiceImpl(Integer language) {
        this.language = language;
    }

    public PatientServiceImpl() {
    }

    /**
     * Create XML file with Patient demographic information, and point to PDF
     * file
     *
     * @param patient
     * @param filename
     */
    public void dumpPatient(PatientAccountVO patient, String filename) {
        try {
            ProfessionalServiceImpl profImpl = new ProfessionalServiceImpl();
            ProfessionalVO user = profImpl.getProfessional(7);
            PDate pdate = new PDate(user != null ? user.getTimezone() : Constants.TIMEZONE);
            String time = "";
            Date now = new Date();
            time = pdate.getYear(now) + "-" + pdate.getMonthNum(now) + "-" + pdate.getDay(now) + "T" + pdate.getHour(now) + ":" + pdate.getMin(now) + ":00.100-08:00";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc = builder.newDocument();
            Element e1 = doc.createElement("patient");
            doc.appendChild(e1);
            e1.setAttribute("mrn", patient.getPhn2());
            e1.appendChild(Common.createElement("firstname", patient.getFirstName(), doc));
            e1.appendChild(Common.createElement("lastname", patient.getLastName(), doc));
            int female = Integer.parseInt(Common.getConfig("female"));
            int male = Integer.parseInt(Common.getConfig("male"));
            if (patient != null && patient.getGender() != null && patient.getGender().equals(male)) {
                e1.appendChild(Common.createElement("gender", Constants.MALE, doc));
            } else if (patient != null && patient.getGender() != null && patient.getGender().equals(female)) {
                e1.appendChild(Common.createElement("gender", Constants.FEMALE, doc));
            } else {
                e1.appendChild(Common.createElement("gender", Constants.UNKNOWN, doc));
            }
            e1.appendChild(Common.createElement("dob", patient.getDob(), doc));
            e1.appendChild(Common.createElement("phn", patient.getPhn(), doc));
            e1.appendChild(Common.createElement("dateTime", time, doc));
            e1.appendChild(Common.createElement("pdf", Common.getConfig("UNCPath") + "\\" + filename, doc));
            OutputFormat format = new OutputFormat(doc);
            format.setIndenting(true);
            File xml = new File(Common.getConfig("dataPath") + "\\" + patient.getPatient_id() + ".xml");
            XMLSerializer serializer = new XMLSerializer(
                    new FileOutputStream(xml), format);
            serializer.serialize(doc);
            xml.setWritable(true);
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Verify if the Professional has permission to access this patient. 0 = No
     * Error 1 = Account doesn't exist 2 = Patient isn't in Professionals
     * treatment location list 3 = Patient isn't assigned to the Professional. 4
     * = Patient account is disabled. 5 = Patient is in TIP.
     *
     * @param userId
     * @param patientId
     * @return error
     * @throws ApplicationException
     */
    public int validatePatientId(int userId, int patientId) throws ApplicationException {
        int error = 0;
        try {
            PatientAccountVO accountVO = (PatientAccountVO) getPatient(patientId);
            if (accountVO == null) {
                error = 1; // account does not exist.
            }
            ProfessionalServiceImpl manager = new ProfessionalServiceImpl();
            ProfessionalVO userVO = (ProfessionalVO) manager.getProfessional(userId);
            if (userVO != null && !userVO.getAccess_superadmin().equals("1")) {//if user is a super admin dont bother checking.
                if (error == 0 && accountVO.getTreatment_location_id().intValue() > 0 && manager.validate(userVO.getRegions(), accountVO.getTreatment_location_id()) == false) {
                    error = 2; // patient does not exist in professionals assigned treatment locations.
                }
                // log.info("Regions ERror: "+userVO.getRegions()+" == " +
                // accountVO.getDecrypted(accountVO.getTreatment_location_id(),
                // "pass4treat"));
                AssignPatientsDAO assignPatientsDAO = new AssignPatientsDAO();
                AssignPatientsVO criteria = new AssignPatientsVO();
                criteria.setPatientId(new Integer(patientId));
                criteria.setProfessionalId(new Integer(userId));
                AssignPatientsVO assignVO = (AssignPatientsVO) assignPatientsDAO.findByCriteria(criteria);
                if (assignVO == null && error == 2) {  //Not assigned to Patient
                    error = 3;
                } else if (error == 2 && assignVO != null) {
                    error = 0;
                }
                if (error == 0 && accountVO.getAccount_status().equals(new Integer(0))) {
                    error = 4; // patients account has been disabled
                }
                if (error == 0) {
                    if(userVO.getTraining_flag().equals(new Integer(1)) && !accountVO.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)){
                        error = 6;
                    }else if (accountVO.getTreatment_location_id().intValue() == -1) {//Patient belongs to TIP
                        error = 5;
                    }
                }
            }
            log.info("Account Status Error: " + error);
        } catch (DataAccessException e) {
            log.error("Error in UserBD.validateUserId(): " + e.toString(), e);
            throw new ApplicationException("Error in UserBD.validateUserId(): " + e.toString(), e);
        }
        return error;
    }

    public int getPatientId(PatientsVO p) {
        PatientDAO dao = new PatientDAO();
        return dao.insertNewPatient(p);
    }

    /**
     * Creates a new Patient ID, by inserting to PatientsVO then, setting the
     * patient_id field in patient_accounts. Inserts the PatientAccountVO object
     * into patient_accounts table. It will always do an insert, and update all
     * prior records if they exist to current_flag=0.
     *
     * @param accountVO an patient_accounts record to be inserted.
     * @see PatientAccountVO
     */
    public int addPatient(PatientAccountVO accountVO) throws ApplicationException {
        PatientDAO patientDAO = new PatientDAO();
        accountVO.setCurrent_flag(new Integer(1));
        try {
            PDate pdate = new PDate();
            PatientsVO p = new PatientsVO();
            p.setCreated_on(new Date());
            p.setProfessional_id(accountVO.getCreated_by());
            int patient_id = patientDAO.insertNewPatient(p);
            accountVO.setPatient_id(new Integer(patient_id));
            patientDAO.insert(accountVO);
            PatientAccountVO currentPatient = (PatientAccountVO) getPatient(accountVO);
            if (currentPatient != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getCreated_by(), currentPatient.getPatient_id(), (currentPatient.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "patient_accounts", accountVO));
            }
            return patient_id;
        } catch (DataAccessException e) {
            log.error("Error in UserBD.addPatient(): " + e.toString(), e);
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
    }

    /**
     * Merge patient data from one patient do another, and disable the old
     * patient.
     *
     * @param patient_from patient merging from
     * @param patient_to patient merging to
     */
    public void mergePatient(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        try {
            AssessmentServiceImpl assess = new AssessmentServiceImpl(language);
            AssessmentImagesServiceImpl images = new AssessmentImagesServiceImpl();
            AssessmentCommentsServiceImpl comms = new AssessmentCommentsServiceImpl();
            NursingCarePlanServiceImpl ncps = new NursingCarePlanServiceImpl();
            TreatmentCommentsServiceImpl tcomms = new TreatmentCommentsServiceImpl();
            WoundAssessmentLocationServiceImpl wals = new WoundAssessmentLocationServiceImpl();
            WoundAssessmentServiceImpl was = new WoundAssessmentServiceImpl();
            PatientProfileServiceImpl pps = new PatientProfileServiceImpl(language);
            WoundServiceImpl wps = new WoundServiceImpl(language);
            //move to each individual service.. can use later for data maintenance module.
            images.mergeImages(patient_from, patient_to, event);
            comms.mergeComments(patient_from, patient_to, event);
            ncps.mergeNursingCarePlans(patient_from, patient_to, event);
            ncps.mergeDressingChangeFrequencies(patient_from, patient_to, event);
            tcomms.mergeTreatmentComments(patient_from, patient_to, event);
            assess.mergeAssessments(patient_from, patient_to, event);
            assess.mergeProducts(patient_from, patient_to, event);
            wals.mergeAlphas(patient_from, patient_to, event);
            was.mergeAssessments(patient_from, patient_to, event);
            wps.mergeWoundProfiles(patient_from, patient_to, event);
            wps.mergeWoundProfileTypes(patient_from, patient_to, event);
            pps.mergeBradens(patient_from, patient_to, event);
            pps.mergeFootAssessments(patient_from, patient_to, event);
            pps.mergeLimbBasicAssessments(patient_from, patient_to, event);
            pps.mergeLimbAdvAssessments(patient_from, patient_to, event);
            pps.mergePatientProfiles(patient_from, patient_to, event);
            //merge patient accounts
            Collection<PatientAccountVO> patients = getAllPatients(patient_from);
            for (PatientAccountVO patient : patients) {
                //patient.setPatient_id(patient_to);//set all patient_from ids to patient_to
                patient.setCurrent_flag(0);
                patient.setPhn("Patient Merged with: " + patient_to);
                patient.setPhn2("Patient Merged with: " + patient_to);
                savePatient(patient);
            }
            //
        } catch (ApplicationException e) {
            log.error("Error in UserBD.addPatient(): " + e.toString(), e);
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
    }

    /**
     * Inserts the PatientAccountVO object into patient_accounts table. It will
     * always do an insert, and update all prior records if they exist to
     * current_flag=0.
     *
     * @param accountVO an patient_accounts record to be inserted.
     * @see PatientAccountVO
     */
    public void savePatient(PatientAccountVO accountVO) throws ApplicationException {
        try {
            if (accountVO != null) {
                accountVO.setCreated_on(new Date());
            }
            if (accountVO.getPatient_id() == null) {
                addPatient(accountVO);
            } else {
                PatientDAO patientDAO = new PatientDAO();
                accountVO.setCurrent_flag(new Integer(1));
                PatientAccountVO currentPatient = getPatient(accountVO);
                if (currentPatient != null) // When downloading to Offline, there might not be a previous record
                {
                    accountVO.setVisits(currentPatient.getVisits());
                } else {
                    accountVO.setVisits(0);
                }
                patientDAO.insert(accountVO);
                PatientAccountVO t = (PatientAccountVO) getPatient(accountVO);
                if (t != null) {
                    logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(t.getCreated_by(), t.getPatient_id(), (accountVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "patient_accounts", t));
                }
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
            log.error("Error in UserBD.addPatient(): " + e.toString(), e);
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
    }

    /**
     * Converts and Saves the signature of the Patient Consent contract as an
     * image. The name of the resulting image is stored in the field
     * patient_consent_sig of the object.
     *
     * @param patientAccountVo The patient_account record
     * @param signatureData The signature encoded as String
     *
     * @return The filename of the saved image as a String
     */
    public String recordSignatureAsImage(PatientAccountVO patientAccountVO, String fullSignature) throws ApplicationException {
        if (fullSignature == null || fullSignature.isEmpty()) {
            return null;
        }
        String filename = "consent_signature.png";
        String signature = fullSignature.split(",")[1];
        try {
            byte[] imageByteArray = Base64.decodeBase64(signature);
            InputStream is = new ByteArrayInputStream(imageByteArray);
            //BufferedImage acquiredImage = ImageIO.read(new ByteArrayInputStream(imageByteArray));
            String fileName = Common.getPhotosPath() + "/" + patientAccountVO.getPatient_id() + "/" + filename;
            Common.writeToFile(fileName, is, true);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error recording signature: PatientConsent: " + e.toString(), e);
            throw new ApplicationException("PatientManagerBD - Error recording signature as image: " + e.getMessage(), e);
        }

        return filename;
    }

    /**
     * updates the PatientAccountVO object into patient_accounts table. Only
     * used by meditech to not add patient_id to patient record until after they
     * are transferred to their location.
     *
     * @param accountVO an patient_accounts record to be inserted.
     * @see PatientAccountVO
     */
    public void updatePatient(PatientAccountVO accountVO) throws ApplicationException {
        try {
            //if (accountVO.getPatient_id() == null) {
            //    addPatient(accountVO);
            //} else {
            PatientDAO patientDAO = new PatientDAO();
            patientDAO.update(accountVO);
            PatientAccountVO currentPatient = (PatientAccountVO) getPatient(accountVO);
            if (currentPatient != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getCreated_by(), currentPatient.getPatient_id(), (currentPatient.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "patient_accounts", currentPatient));
            }
            //}
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error in UserBD.addPatient(): " + e.toString(), e);
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
    }

    /**
     * This method removes the passed patient record. Sets the record to
     * inactive (the record is never completely removed.
     *
     * @param patient_id the patient's id to be deleted.
     * @param delete_reason why the patient was deleted.
     * @see PatientAccountVO
     */
    public void removePatient(String patient_id, Integer delete_reason) throws ApplicationException {
        try {
            com.pixalere.patient.bean.PatientAccountVO accountVO = new com.pixalere.patient.bean.PatientAccountVO();
            accountVO = getPatient(new Integer(patient_id).intValue());
            PatientDAO patientDAO = new PatientDAO();
            accountVO.setCurrent_flag(0);
            patientDAO.insert(accountVO);
            accountVO.setId(null);
            accountVO.setAccount_status(new Integer(0));
            accountVO.setCurrent_flag(new Integer(1));
            accountVO.setDischarge_reason(delete_reason);
            patientDAO.insert(accountVO);
            PatientAccountVO currentPatient = (PatientAccountVO) getPatient(accountVO);
            if (currentPatient != null) {
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getCreated_by(), currentPatient.getPatient_id(), com.pixalere.utils.Constants.INACTIVATE_RECORD, "patient_accounts", accountVO));
            }
        } catch (DataAccessException e) {
            log.error("PatientManagerBD.removePatient error: " + e.getMessage());
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
    }

    /**
     * This method removes the passed patient record. Physically removes the
     * record from the database.
     *
     * @param patient_id the patient's id to be deleted.
     * @see PatientAccountVO
     */
    public void removePatient(String patient_id) throws ApplicationException {
        com.pixalere.patient.bean.PatientAccountVO accountVO = new com.pixalere.patient.bean.PatientAccountVO();
        accountVO.setPatient_id(new Integer(patient_id));
        PatientDAO patientDAO = new PatientDAO();
        patientDAO.delete(accountVO);
        if (accountVO != null) {
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(accountVO.getCreated_by(), accountVO.getPatient_id(), com.pixalere.utils.Constants.DELETE_RECORD, "patient_accounts", accountVO));
        }
    }

    /**
     * Returns a Vector of PatientAccountVO objects that can be shown on the
     * screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param lastname the lastname of the patient
     * @param ProfessionalVO the professional who created the user
     * @param phn the PHN of the patient
     * @return the patient_account records
     * @see PatientAccountVO
     */
    public Vector findAllBySearch(int id, String last_name, ProfessionalVO userVO, String phn, int treatment_location_id, String otherSearch, String parisSearch, String mrnSearch, boolean includeInactive, Integer language) throws ApplicationException {
        Vector results = new Vector();
        try {
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            PatientDAO dao = new PatientDAO();
            WoundServiceImpl wservice = new WoundServiceImpl();

            List<PatientAccountVO> collection = dao.findAllBySearch(id, last_name, phn, treatment_location_id, otherSearch, parisSearch, mrnSearch, includeInactive);
            ListServiceImpl listBD = new ListServiceImpl(language);
            for (PatientAccountVO accVO : collection) {
                LookupVO lookupVO = null;
                //inserts bogus treatmentLocation record if TIP
                if (accVO.getTreatment_location_id() > 0) {
                    lookupVO = (LookupVO) listBD.getListItem(new Integer(accVO.getTreatment_location_id()).intValue());
                } else {
                    lookupVO = new LookupVO();
                    lookupVO.setId(new Integer("-1"));
                    Collection<LookupLocalizationVO> lColl = Common.getLocalizationFromBundleKey("pixalere.TIP");

                    lookupVO.setNames(lColl);
                    lookupVO.setResourceId(new Integer("-1"));
                }

                // START CHECK ACCESS PERMISSIONS
                accVO.setTreatmentLocation(lookupVO);
                boolean access = false;
                if (accVO.getTreatment_location_id() == -1 || pservice.validate(userVO.getRegions(), accVO.getTreatment_location_id()) == true) {
                    access = true;
                }

                AssignPatientsServiceImpl apManagerBD = new AssignPatientsServiceImpl();
                AssignPatientsVO apvo = new AssignPatientsVO();
                apvo.setPatientId(accVO.getPatient_id());
                apvo.setProfessionalId(userVO.getId());
                AssignPatientsVO findAPVO = apManagerBD.getAssignedPatient(apvo);
                if (findAPVO != null) {
                    access = true;
                }
                // END CHECK ACCESS PERMISSIONS
                boolean active = false;
                if (accVO.getAccount_status() != null && accVO.getAccount_status().equals(new Integer(1))) {
                    active = true;
                }

                PatientList pat = new PatientList(accVO, access, active, accVO.getTreatmentLocation().getName(language));
                if (accVO.getPatient_id() != null) {
                    pat.setWounds(wservice.getWoundProfilesDropdown(accVO.getPatient_id(), true, userVO.getId()));
                }
                if ((userVO.getTraining_flag() != null && userVO.getTraining_flag() == 1 && accVO.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)) || (userVO.getTraining_flag() != null && userVO.getTraining_flag() != 1  && !accVO.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID))) {

                    results.add(pat);

                }
            }
        } catch (DataAccessException e) {
            log.error("PatientManagerBD.findAllBySearch error: " + e.getMessage());
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
        return results;
    }

    public Vector getOpenWoundsData(Vector vecData, Vector vecOpenWounds) {
        Vector vecResult = new Vector();
        for (int intData = 0; intData < vecData.size(); intData++) {
            String strDataOption = (String) vecData.get(intData);
            for (int x = 0; x < vecOpenWounds.size(); x++) {
                String alpha = (String) vecOpenWounds.get(x);
                if (strDataOption.indexOf(alpha + ":") != -1) {
                    vecResult.add(strDataOption);
                }
            }
        }
        return vecResult;
    }

    /**
     * @param patient_status
     * @param treatment_locations
     *
     * @return An array of patients
     * @throws ApplicationException
     * @deprecated
     * @todo check if its still used.
     * @since 6.0
     * @author travis morris
     */
    public Vector getAllPatients(int patient_status, String[] treatment_locations) {
        Vector results = new Vector();
        try {
            PatientDAO dao = new PatientDAO();
            results = dao.findAllBySearch(patient_status, treatment_locations);	//PatientAccountsVO
        } catch (DataAccessException e) {
            log.error("Error: " + e.getMessage());
        }
        return results;
    }

    /**
     *
     *
     * @param patient_status
     * @param care_type_status
     * @param care_type_all
     * @param care_type_wound
     * @param care_type_incision
     * @param care_type_tube
     * @param care_type_ostomy
     * @param care_type_burn
     * @param wound_type
     * @param incision_type
     * @param tube_type
     * @param ostomy_type
     * @param burn_type
     * @param treatmentLocations
     * @return
     * @throws ApplicationException
     *
     * Jluu
     */
    public Vector findAllOverviewBySearch(int patient_status, int care_type_status, String[] caretypes, int skin_goal, int wound_goal, int ostomy_goal, int tubedrain_goal, int incision_goal, String[] treatmentLocations, String locale) throws ApplicationException {
        Vector results = new Vector();
        try {
            PatientDAO dao = new PatientDAO();
            WoundProfileDAO wound_dao = new WoundProfileDAO();
            WoundServiceImpl wservice = new WoundServiceImpl();
            //com.pixalere.assessment.dao.WoundAssessmentLocationDAO locationDAO = new com.pixalere.assessment.dao.WoundAssessmentLocationDAO();
            //com.pixalere.assessment.dao.AssessmentDAO assessDAO = new com.pixalere.assessment.dao.AssessmentDAO();
            Vector iter1 = dao.findAllBySearch(patient_status, treatmentLocations);	//PatientAccountsVO
            //LookupVO lookupVO = new LookupVO();
            ListsDAO listDAO = new ListsDAO();

            if (caretypes != null) {
                //System.out.println("caretpes: " + caretypes.length);
            }
            if (treatmentLocations != null) {
                log.info(iter1.size() + " ---- " + treatmentLocations.length);
            }
            for (int x = 0; x < iter1.size(); x++) {
                boolean includePatient = false;

                Object[] patientProfile = (Object[]) iter1.get(x);
                //boolean foundPatient = false;
                PatientAccountVO accVO = new PatientAccountVO();
                Integer pixId = ((Integer) patientProfile[5]);
                Integer account_status = ((Integer) patientProfile[3]);
                accVO.setPatient_id(pixId);
                accVO.setFirstName(((String) patientProfile[1]).trim());
                accVO.setLastName(((String) patientProfile[2]).trim());
                accVO.setTreatment_location_id((Integer) patientProfile[6]);
                accVO.setAccount_status(account_status);
                Integer delete_reason = null;
                try {
                    delete_reason = (Integer) patientProfile[4];
                } catch (NullPointerException e) {
                }//change this later to make ""}
                //Get all Caretypes for this patient.
                List<WoundProfileItem> filtered_wounds = new ArrayList();
                WoundVO tmp = new WoundVO();
                tmp.setPatient_id(pixId);
                tmp.setActive(1);

                if (care_type_status != -1 && care_type_status != -2) {
                    if (care_type_status == 0) {
                        tmp.setStatus("");
                    } else {
                        tmp.setStatus("Closed");
                    }
                }
                Collection<WoundVO> wounds = wservice.getWounds(tmp);//wound_dao.findAllWoundProfileTypesBySearch(pixId, care_type_status, caretypes, wound_goal, ostomy_goal, tubedrain_goal, incision_goal);

                LookupVO treatment = (LookupVO) listDAO.findByPK(accVO.getTreatment_location_id());
                PatientList plist = new PatientList(accVO, true, (accVO.getAccount_status() == 1 ? true : false), (treatment != null ? treatment.getName(language) : ""));
                plist.setDelete_reason((accVO.getAccount_status().equals(new Integer(0)) ? ("Inactive: " + delete_reason) : "Active"));

                //IF we're to get all care types regardless of status and there's no care types.. show patient.
                //if (care_type_status == -1 && (wounds == null || caretypes == null || wounds.size() == 0)) {
                ///   includePatient = true;
                //}
                if (wounds != null) {
                    for (WoundVO wound : wounds) {
                        WoundProfileTypeVO t = new WoundProfileTypeVO();
                        t.setWound_id(wound.getId());
                        Collection<WoundProfileTypeVO> types = wservice.getWoundProfileTypes(t, -1);
                        boolean includeWound = false;
                        if (caretypes != null && caretypes.length > 0) {
                            for (WoundProfileTypeVO type : types) {
                                for (String caretype : caretypes) {
                                    if (caretype.equals("all")) {
                                        includeWound = true;
                                        break;
                                    }

                                    if (caretype.equals(type.getAlpha_type())) {
                                        //check goals too.
                                        WoundProfileVO wprofileVO = new WoundProfileVO();
                                        wprofileVO.setWound_id(wound.getId());
                                        wprofileVO.setActive(1);
                                        wprofileVO.setCurrent_flag(1);
                                        WoundProfileVO wp = wservice.getWoundProfile(wprofileVO);
                                        if (wp != null) {
                                            Collection<GoalsVO> goalsc = wp.getGoals();
                                            if (caretype.equals(Constants.WOUND_PROFILE_TYPE)) {
                                                if (wound_goal > 0) {
                                                    for (GoalsVO goal : goalsc) {
                                                        if (wound_goal == goal.getLookup_id().intValue()) {
                                                            includeWound = true;
                                                        }
                                                    }
                                                } else {
                                                    includeWound = true;
                                                }
                                            }
                                            if (caretype.equals(Constants.SKIN_PROFILE_TYPE)) {
                                                if (skin_goal > 0) {
                                                    for (GoalsVO goal : goalsc) {
                                                        if (skin_goal == goal.getLookup_id().intValue()) {
                                                            includeWound = true;
                                                        }
                                                    }
                                                } else {
                                                    includeWound = true;
                                                }
                                            } else if (caretype.equals(Constants.OSTOMY_PROFILE_TYPE)) {
                                                if (ostomy_goal > 0) {
                                                    for (GoalsVO goal : goalsc) {
                                                        if (ostomy_goal == goal.getLookup_id().intValue()) {
                                                            includeWound = true;
                                                        }
                                                    }

                                                } else {
                                                    includeWound = true;
                                                }
                                            } else if (caretype.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                                                if (tubedrain_goal > 0) {
                                                    for (GoalsVO goal : goalsc) {
                                                        if (tubedrain_goal == goal.getLookup_id().intValue()) {
                                                            includeWound = true;
                                                        }
                                                    }

                                                } else {
                                                    includeWound = true;
                                                }
                                            } else if (caretype.equals(Constants.INCISION_PROFILE_TYPE)) {
                                                if (incision_goal > 0) {
                                                    for (GoalsVO goal : goalsc) {
                                                        if (incision_goal == goal.getLookup_id().intValue()) {
                                                            includeWound = true;
                                                        }
                                                    }
                                                } else {
                                                    includeWound = true;
                                                }
                                            }

                                        }
                                    }
                                }

                            }
                        } else {
                            includeWound = true;
                        }
                        if (includeWound) {
                            WoundProfileItem i = new WoundProfileItem();
                            i.setName(wound.getWound_location_detailed());
                            i.setStatus(wound.getStatus());
                            if (wound.getStatus() == null || !wound.getStatus().equals("Closed")) {
                                i.setStatus(Common.getLocalizedString("pixalere.active", locale));
                            }
                            filtered_wounds.add(i);

                            includePatient = true;
                        }

                    }
                }
                if (care_type_status == -2) {
                    includePatient = true;
                }
                plist.setWounds(filtered_wounds);
                //include all patient's who fit the criteria
                if (includePatient) {
                    results.add(plist);
                }
            }

        } catch (DataAccessException e) {
            log.error("PatientManagerBD.findAllBySearch error: " + e.getMessage());
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
        return results;
    }

    /**
     * Returns an PatientAccountVO object that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param primaryKey the id of the patient_accounts record
     * @return the patient_account record
     * @todo duplicate method, need to refactor one out.
     * @see PatientAccountsVO
     */
    public PatientAccountVO getPatient(int primaryKey) throws ApplicationException {
        LookupVO lookupVO = null;
        try {
            PatientDAO patientDAO = new PatientDAO();
            PatientAccountVO accVO = (PatientAccountVO) patientDAO.findByPK(primaryKey);
            //Inserts bogus TreatmentLocation object if they belong to TIP
            if (accVO != null) {
                ListServiceImpl listBD = new ListServiceImpl(language);
                if (accVO.getTreatment_location_id() > 0) {
                    lookupVO = listBD.getListItem(new Integer(accVO.getTreatment_location_id()).intValue());
                } else {
                    lookupVO = new LookupVO();
                    lookupVO.setId(new Integer("-1"));
                    LookupLocalizationVO l = new LookupLocalizationVO();
                    Collection<LookupLocalizationVO> lColl = Common.getLocalizationFromBundleKey("pixalere.TIP");

                    lookupVO.setNames(lColl);
                    lookupVO.setResourceId(new Integer("-1"));
                }
                accVO.setTreatmentLocation(lookupVO);
            }
            return accVO;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }

    /**
     * Returns an PatientAccountVO object that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param primaryKey the id of the patient_accounts record
     * @return the patient_account record
     * @see PatientAccountsVO
     */
    public PatientAccountVO getPatientByID(int primaryKey) throws ApplicationException {
        LookupVO lookupVO = null;
        try {
            PatientDAO patientDAO = new PatientDAO();
            PatientAccountVO accVO = (PatientAccountVO) patientDAO.findByID(primaryKey);
            //Inserts bogus TreatmentLocation object if they belong to TIP
            if (accVO != null) {
                ListServiceImpl listBD = new ListServiceImpl(language);
                if (accVO.getTreatment_location_id() > 0) {
                    lookupVO = listBD.getListItem(new Integer(accVO.getTreatment_location_id()).intValue());
                } else {
                    lookupVO = new LookupVO();
                    lookupVO.setId(new Integer("-1"));
                    Collection<LookupLocalizationVO> lColl = Common.getLocalizationFromBundleKey("pixalere.TIP");

                    lookupVO.setNames(lColl);
                    lookupVO.setResourceId(new Integer("-1"));
                }
                accVO.setTreatmentLocation(lookupVO);

            }
            return accVO;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }

    /**
     * Returns an PatientAccountVO object that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param primaryKey the id of the patient_accounts record
     * @return the patient_account record
     * @see PatientAccountsVO
     */
    public PatientAccountVO getPatient(PatientAccountVO vo) throws ApplicationException {
        LookupVO lookupVO = null;
        try {
            PatientDAO patientDAO = new PatientDAO();
            PatientAccountVO accVO = (PatientAccountVO) patientDAO.findByCriteria(vo);
            //Inserts bogus TreatmentLocation object if they belong to TIP
            if (accVO != null) {
                ListServiceImpl listBD = new ListServiceImpl(language);
                if (accVO.getTreatment_location_id() > 0) {
                    lookupVO = listBD.getListItem(new Integer(accVO.getTreatment_location_id()).intValue());
                } else {
                    lookupVO = new LookupVO();
                    lookupVO.setId(new Integer("-1"));
                    Collection<LookupLocalizationVO> lColl = Common.getLocalizationFromBundleKey("pixalere.TIP");

                    lookupVO.setNames(lColl);
                    lookupVO.setResourceId(new Integer("-1"));
                }
                accVO.setTreatmentLocation(lookupVO);

            }
            return accVO;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }

    public boolean validatePHN(String phn) throws Exception {
        try {
            if (phn == null) {
                return false;
            }
            if (Common.getConfig("checkPHN").equals("BC") && phn != null) {
                boolean blnResult = false;
                blnResult = (phn.length() == 10 && phn.charAt(0) == '9' && Integer.parseInt(phn.charAt(9) + "") == 11 - (((Integer.parseInt(phn.charAt(1) + "") * 2) % 11 + (Integer.parseInt(phn.charAt(2) + "") * 4) % 11 + (Integer.parseInt(phn.charAt(3) + "") * 8) % 11 + (Integer.parseInt(phn.charAt(4) + "") * 5) % 11 + (Integer.parseInt(phn.charAt(5) + "") * 10) % 11 + (Integer.parseInt(phn.charAt(6) + "") * 9) % 11 + (Integer.parseInt(phn.charAt(7) + "") * 7) % 11 + (Integer.parseInt(phn.charAt(8) + "") * 3) % 11) % 11));
                return blnResult;
            } else {
                return true;
            }
        } catch (NullPointerException e) {
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        }
    }

    public Vector getPHN(PatientAccountVO vo, String[] phnFormat) throws Exception {
        Vector tmp = new Vector();
        try {
            if (vo.getOutofprovince().intValue() == 1) {
                tmp.add(vo.getPhn());
            } else if (vo.getOutofprovince().intValue() == 2) {
                tmp.add(vo.getPhn());
            } else if (vo.getPhn() != null && !vo.getPhn().equals("")) {
                int cnt = 0;
                String[] nums = Common.getConfig("phnFormat").split("-");
                for (int x = 0; x < Common.getConfig("phnFormat").split("-").length; x++) {
                    String num = nums[x];
                    if (num != null && !num.equals("")) {
                        int len = Integer.parseInt(num);
                        try {
                            tmp.add((vo.getPhn()).substring(cnt, (len + cnt)));
                        } catch (Exception e) {
                            tmp.add(vo.getPhn());
                            e.printStackTrace();//old PHN, invalid format.
                            break;
                        }
                        cnt = cnt + len;
                    }

                }
            }
        } catch (NullPointerException e) {
            throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        } catch (NumberFormatException e) {
            //throw new ApplicationException("PatientManagerBD threw an error: " + e.getMessage(), e);
        } catch (Exception e) {
            log.error("Exception " + e.getMessage() + " with: " + vo.getPhn());
        }
        return tmp;
    }

    /**
     * Returns an PatientAccountVO object that can be shown on the screen.
     * <p/>
     * This method always returns immediately, whether or not the record exists.
     * The Object will return NULL if the record does not exist.
     *
     * @param phn
     * @param mrn (like mrn or urn)
     * @param validation is the scheme used to get the data
     * @return the patient_account record
     *
     * @see PatientAccountVO
     */
    public Hashtable getPatient(String phn, String mrn, String validation) throws ApplicationException {
        LookupVO lookupVO = null;
        try {
            PatientDAO patientDAO = new PatientDAO();
            Hashtable accVO = patientDAO.findByCriteria(phn, mrn, validation);
            return accVO;
            /*//Inserts bogus TreatmentLocation object if they belong to TIP
             //@todo move this code into listdata and create actual record much like 3200.
             if (accVO != null) {
             ListServiceImpl listBD = new ListServiceImpl(language);
             if (accVO.getTreatment_location_id() == null) {
             //System.out.println(accVO.getId()+"Patient: "+accVO.getTreatment_location_id());
             }
             if (accVO.getTreatment_location_id() > 0) {
             lookupVO = listBD.getListItem(new Integer(accVO.getTreatment_location_id()).intValue());
             } else {
             lookupVO = new LookupVO();
             lookupVO.setId(new Integer("-1"));
             lookupVO.setName(Common.getLocalizedString("pixalere.TIP","en"));
             lookupVO.setResourceId(new Integer("-1"));
             }
             accVO.setTreatmentLocation(lookupVO);
             }*/
        } catch (DataAccessException e) {
            e.printStackTrace();
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }

    /* gets all patients in the database.. typically only used for offline
     *
     */
    public Collection getAllPatients(PatientAccountVO p) throws ApplicationException {
        try {
            PatientDAO dao = new PatientDAO();

            return (Collection) dao.findAllByCriteria(p);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
    /* get all patient_accounts by patient_id - includes historical
     * @deprecated
     * @todo check if its still used..
     */

    public Collection getAllPatients(int patient_id) throws ApplicationException {
        try {
            PatientDAO dao = new PatientDAO();
            PatientAccountVO p = new PatientAccountVO();
            p.setPatient_id(patient_id);
            return (Collection) dao.findAllByCriteria(p);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }

    // public static boolean hasChanged=false;
    /**
     * Get all records for this patient, using reflection we get the fields
     * dynamically, and pass them back as a Colleciton of hashmaps for
     * conversion to a json. similar to flowsheet method... gt all classes using
     * components, build a collection of hashmaps for json
     *
     * @since 6.1
     * @version 6.1
     * @param patient_id = the ID of the patient we want to retrieve
     */
    public List<List> getPatientFlowsheet(Collection<PatientAccountVO> results, String locale) {
        List<List> rows = null;
        GUIServiceImpl gui = new GUIServiceImpl();
        ListServiceImpl lservice = new ListServiceImpl();
        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        rows = new ArrayList();
        List<String> title_hash = new ArrayList();
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.PATIENT_ACCOUNT);

        Collection<ComponentsVO> components = null;
        try {
            components = gui.getAllComponents(comTMP);
            int cnt = 2;
            title_hash.add("ID");
            for (ComponentsVO component : components) {
                if (component.getHide_flowchart() == 0) {
                    String title = Common.getLocalizedString(component.getLabel_key(), locale);
                    if(component.getField_name().equals("phn")){
                        title = Common.getConfig("phn1_name");
                    }else if(component.getField_name().equals("phn2")){
                        title = Common.getConfig("phn2_name");
                    }else if(component.getField_name().equals("phn3")){
                        title = Common.getConfig("phn3_name");
                    }else if(component.getField_name().equals("treatment_location_id")){
                        title = Common.getConfig("treatmentLocationName");
                    }
                    title_hash.add( title);
                }
            }
        } catch (ApplicationException e) {
            e.printStackTrace();
        }
        rows.add(title_hash);
        for (PatientAccountVO entry : results) {
            List<String> entry_hash = new ArrayList();
            try {
                ProfessionalVO userVO = pservice.getProfessional(entry.getCreated_by());
                
                entry_hash.add( entry.getId()+"");

                for (ComponentsVO component : components) {

                    if (component.getHide_flowchart() == 0) {

                        //Get all columns for each component
                        String value = "";
                        FieldValues field = new FieldValues();
                        Hashtable v = Common.getPrintableValue(entry, userVO, component, false, -1, language);
                        field.setDb_value((String) v.get("db_value"));
                        Vector va = (Vector) v.get("value");
                        if (va != null && va.size() > 0) {
                            value = (String) va.get(0);
                        }
                        //value = value + Common.showNursingFixes(am.getNursingFixes(patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode, locale);
                        entry_hash.add( value);

                    }
                }
            } catch (ApplicationException e) {
                e.printStackTrace();
            }
            rows.add(entry_hash);
        }
        return rows;
    }
}
