package com.pixalere.patient.service;
import java.util.Date;

import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.patient.bean.DiagnosticInvestigationVO;
import com.pixalere.patient.service.PatientServiceImpl;

import java.util.Iterator;

import com.pixalere.patient.bean.ExternalReferralVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.common.service.GUIServiceImpl;
import com.pixalere.common.bean.OfflineVO;
import com.pixalere.common.bean.ComponentsVO;

import java.text.SimpleDateFormat;

import com.pixalere.patient.dao.*;
import com.pixalere.patient.bean.PhysicalExamVO;

import java.util.Hashtable;

import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;
import com.pixalere.common.*;
import com.pixalere.utils.*;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.patient.bean.LimbUpperAssessmentVO;

import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.sql.Timestamp;

import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.integration.dao.IntegrationDAO;
import com.pixalere.integration.bean.EventVO;
/**
 *     This is the patient profile  service implementation
 *     Refer to {@link com.pixalere.patient.service.PatientProfileService } to see if there are
 *     any web services available.
 *
 *     <img src="PatientProfileServiceImpl.png"/>
 *
 *
 *     @view  PatientProfieServiceImpl
 *
 *     @match class com.pixalere.patient.*
 *     @opt hide
 *     @match class com.pixalere.patient\.(dao.PatientDAO|bean.PatientProfileVO|service.PatientProfileService)
 *     @opt !hide
 *
 */
public class PatientProfileServiceImpl implements PatientProfileService {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PatientProfileServiceImpl.class);
    private PatientProfileDAO dao = null;
    private Integer language = 1;
    private String locale = "en";
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    
    public PatientProfileServiceImpl(Integer language) {
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
        
        dao = new PatientProfileDAO();
    }
    
    public PatientProfileServiceImpl() {
        dao = new PatientProfileDAO();
    }
    /**
     * Get all patient profiles, both active and inactive (if done by professional_id).
     *
     * @param patient_id
     * @param date
     * @param professional_id
     * @return
     * @throws ApplicationException
     */
    public Collection<PatientProfileVO> getPatientProfileFromDate(int patient_id,Date date,int professional_id) throws ApplicationException{
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return patientDAO.findPatientsFromDate(patient_id,date,professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: "+ e.getMessage());
        }
    }
    public PatientProfileVO getPatientProfileBeforeDate(int patient_id,Date date) throws ApplicationException {
        try {
            PatientProfileVO tmp = (PatientProfileVO) dao.findPatientBeforeDate(patient_id,date);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public Collection<PhysicalExamVO> getExamFromDate(int patient_id,Date date,int professional_id) throws ApplicationException{
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return patientDAO.findExamFromDate(patient_id,date,professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: "+ e.getMessage());
        }
    }
    public PhysicalExamVO getExamBeforeDate(int patient_id,Date date) throws ApplicationException {
        try {
            PhysicalExamVO tmp = (PhysicalExamVO) dao.findExamBeforeDate(patient_id,date);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public Collection<BradenVO> getBradenFromDate(int patient_id,Date date,int professional_id) throws ApplicationException{
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return patientDAO.findBradenFromDate(patient_id,date,professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: "+ e.getMessage());
        }
    }
    public BradenVO getBradenBeforeDate(int patient_id,Date date) throws ApplicationException {
        try {
            BradenVO tmp = (BradenVO) dao.findBradenBeforeDate(patient_id,date);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public Collection<LimbAdvAssessmentVO> getLimbAdvFromDate(int patient_id,Date date,int professional_id) throws ApplicationException{
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return patientDAO.findAdvLimbFromDate(patient_id,date,professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: "+ e.getMessage());
        }
    }
    public LimbAdvAssessmentVO getLimbAdvBeforeDate(int patient_id,Date date) throws ApplicationException {
        try {
            LimbAdvAssessmentVO tmp = (LimbAdvAssessmentVO) dao.findAdvLimbBeforeDate(patient_id,date);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public Collection<LimbUpperAssessmentVO> getLimbUpperFromDate(int patient_id,Date date,int professional_id) throws ApplicationException{
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return patientDAO.findUpperLimbFromDate(patient_id,date,professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: "+ e.getMessage());
        }
    }
    public LimbUpperAssessmentVO getLimbUpperBeforeDate(int patient_id,Date date) throws ApplicationException {
        try {
            LimbUpperAssessmentVO tmp = (LimbUpperAssessmentVO) dao.findUpperLimbBeforeDate(patient_id,date);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public Collection<LimbBasicAssessmentVO> getLimbBasicFromDate(int patient_id,Date date,int professional_id) throws ApplicationException{
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return patientDAO.findBasicLimbFromDate(patient_id,date,professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: "+ e.getMessage());
        }
    }
    public LimbBasicAssessmentVO getLimbBasicBeforeDate(int patient_id,Date date) throws ApplicationException {
        try {
            LimbBasicAssessmentVO tmp = (LimbBasicAssessmentVO) dao.findBasicLimbBeforeDate(patient_id,date);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public Collection<FootAssessmentVO> getFootFromDate(int patient_id,Date date,int professional_id) throws ApplicationException{
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return patientDAO.findFootFromDate(patient_id,date,professional_id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: "+ e.getMessage());
        }
    }
    public FootAssessmentVO getFootBeforeDate(int patient_id,Date date) throws ApplicationException {
        try {
            FootAssessmentVO tmp = (FootAssessmentVO) dao.findFootBeforeDate(patient_id,date);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public void savePatientProfile(PatientProfileVO profileVO) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            patientDAO.insert(profileVO);
            PatientProfileVO currentPatient = (PatientProfileVO)getPatientProfile(profileVO);
            if(currentPatient!=null){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (profileVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "patient_profiles", currentPatient));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void updatePhysicalExam(PhysicalExamVO p) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            patientDAO.updateExam(p);
            PhysicalExamVO currentPatient = (PhysicalExamVO)getExam(p);
            if(currentPatient!=null){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (p.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "physical_exam", currentPatient));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void savePhysicalExam(PhysicalExamVO profileVO) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            patientDAO.insertExam(profileVO);
            PhysicalExamVO currentPatient = (PhysicalExamVO)getExam(profileVO);
            if(currentPatient!=null){
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (profileVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "physical_exam", currentPatient));
            }else{
                System.out.println("failed to save exam");
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void insertBraden(BradenVO braden) throws ApplicationException {
        try {
            if (braden.getBraden_moisture() > 0
                    && braden.getBraden_friction() > 0
                    && braden.getBraden_nutrition() > 0
                    && braden.getBraden_mobility() > 0
                    && braden.getBraden_sensory() > 0
                    && braden.getBraden_activity() > 0) {
                PatientProfileDAO patientDAO = new PatientProfileDAO();
                patientDAO.insertBraden(braden);
                BradenVO currentPatient = (BradenVO)getBraden(braden);
                if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (braden.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "braden", currentPatient));
                }
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void saveBraden(BradenVO braden) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            patientDAO.saveBraden(braden);
            BradenVO currentPatient = (BradenVO)getBraden(braden);
            if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (braden.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "braden", currentPatient));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public FootAssessmentVO getFootAssessment(FootAssessmentVO footassessment) throws ApplicationException {
        try {
        	
            FootAssessmentVO tmp = (FootAssessmentVO) dao.findFootByCriteria(footassessment);
           
            return tmp; 
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public SensoryAssessmentVO getSensoryAssessment(SensoryAssessmentVO sensoryassessment) throws ApplicationException {
        
        
    	try {
        	SensoryAssessmentVO tmp = (SensoryAssessmentVO) dao.findSensoryByCriteria(sensoryassessment);
          
            return tmp; 
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
       
    }
 public DiagnosticInvestigationVO getDiagnosticInvestigation(DiagnosticInvestigationVO f) throws ApplicationException {
        try {
            DiagnosticInvestigationVO tmp = (DiagnosticInvestigationVO) dao.findByCriteria(f);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public DiagnosticInvestigationVO getDiagnosticInvestigation(DiagnosticInvestigationVO d, boolean showDeleted) throws ApplicationException {
        try {
            DiagnosticInvestigationVO tmp = (DiagnosticInvestigationVO) dao.findByCriteria(d, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public FootAssessmentVO getFootAssessment(FootAssessmentVO footassessment, boolean showDeleted) throws ApplicationException {
        try {
            FootAssessmentVO tmp = (FootAssessmentVO) dao.findFootByCriteria(footassessment, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    
    // method for sensory assessment flowchart
    
    public SensoryAssessmentVO getSensoryAssessment(SensoryAssessmentVO sensoryassessment, boolean showDeleted) throws ApplicationException {
        try {
            SensoryAssessmentVO tmp = (SensoryAssessmentVO) dao.findSensoryByCriteria(sensoryassessment, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    
    // method end 
    
    public PhysicalExamVO getExam(PhysicalExamVO examassessment) throws ApplicationException {
        try {
            PhysicalExamVO tmp = (PhysicalExamVO) dao.findExamByCriteria(examassessment);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public PhysicalExamVO getExam(PhysicalExamVO footassessment, boolean showDeleted) throws ApplicationException {
        try {
            PhysicalExamVO tmp = (PhysicalExamVO) dao.findExamByCriteria(footassessment, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public OfflineVO getOffline(OfflineVO off) throws ApplicationException {
        try {
            OfflineVO tmp = (OfflineVO) dao.findOfflineByCriteria(off);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    /**
     * @deprecated
     * @param patient_id
     * @param professional_id
     * @return
     * @throws ApplicationException
     */
    public LimbBasicAssessmentVO getTemporaryLimbBasic(int patient_id, int professional_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbBasicAssessmentVO t = new LimbBasicAssessmentVO();
            t.setPatient_id(new Integer(patient_id));
            t.setProfessional_id(new Integer(professional_id));
            t.setActive(new Integer(0));
            return (LimbBasicAssessmentVO) patientDAO.findBasicLimbByCriteria(t);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * @deprecated
     * @param patient_id
     * @param professional_id
     * @return
     * @throws ApplicationException
     */
    public LimbUpperAssessmentVO getTemporaryLimbUpper(int patient_id, int professional_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbUpperAssessmentVO t = new LimbUpperAssessmentVO();
            t.setPatient_id(new Integer(patient_id));
            t.setProfessional_id(new Integer(professional_id));
            t.setActive(new Integer(0));
            return (LimbUpperAssessmentVO) patientDAO.findUpperLimbByCriteria(t);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * @deprecated
     * @param patient_id
     * @param professional_id
     * @return
     * @throws ApplicationException
     */
    public LimbAdvAssessmentVO getTemporaryLimbAdv(int patient_id, int professional_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbAdvAssessmentVO t = new LimbAdvAssessmentVO();
            t.setPatient_id(new Integer(patient_id));
            t.setProfessional_id(new Integer(professional_id));
            t.setActive(new Integer(0));
            return (LimbAdvAssessmentVO) patientDAO.findAdvLimbByCriteria(t);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * @deprecated
     */
    @Deprecated
    public FootAssessmentVO getTemporaryFoot(int patient_id, int professional_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            FootAssessmentVO t = new FootAssessmentVO();
            t.setPatient_id(new Integer(patient_id));
            t.setProfessional_id(new Integer(professional_id));
            t.setActive(new Integer(0));
            return (FootAssessmentVO) patientDAO.findFootByCriteria(t);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
	 * @deprecated
	 */ 
	/**
	 */
    @Deprecated
	 public SensoryAssessmentVO getTemporarySensory(int patient_id, int professional_id) throws ApplicationException {
		 try {
	            
			 	PatientProfileDAO patientDAO = new PatientProfileDAO();
	            SensoryAssessmentVO t = new SensoryAssessmentVO();
	            t.setPatient_id(new Integer(patient_id));
	            t.setProfessional_id(new Integer(professional_id));
	            t.setActive(new Integer(0));
	            return (SensoryAssessmentVO) patientDAO.findSensoryByCriteria(t);
	        } catch (DataAccessException e) {
	            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
	        }
	}
/*********************************************************************************************/
    public PhysicalExamVO getTemporaryExam(int patient_id, int professional_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            PhysicalExamVO t = new PhysicalExamVO();
            t.setPatient_id(new Integer(patient_id));
            t.setProfessional_id(new Integer(professional_id));
            t.setActive(new Integer(0));
            return (PhysicalExamVO) patientDAO.findByCriteria(t);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    
    public LimbBasicAssessmentVO getLimbBasicAssessment(LimbBasicAssessmentVO limbassessment, boolean showDeleted) throws ApplicationException {
        try {
            LimbBasicAssessmentVO tmp = (LimbBasicAssessmentVO) dao.findBasicLimbByCriteria(limbassessment, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public LimbBasicAssessmentVO getLimbBasicAssessment(LimbBasicAssessmentVO limbassessment) throws ApplicationException {
        try {
            LimbBasicAssessmentVO tmp = (LimbBasicAssessmentVO) dao.findBasicLimbByCriteria(limbassessment);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public void saveBasicLimb(LimbBasicAssessmentVO vo) throws ApplicationException {
        try {
            dao.insertLimbBasic(vo);
            LimbBasicAssessmentVO currentPatient = (LimbBasicAssessmentVO)getLimbBasicAssessment(vo);
            if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "limb_assessment", vo));
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }
    public LimbAdvAssessmentVO getLimbAdvAssessment(LimbAdvAssessmentVO limbassessment, boolean showDeleted) throws ApplicationException {
        try {
            LimbAdvAssessmentVO tmp = (LimbAdvAssessmentVO) dao.findAdvLimbByCriteria(limbassessment, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
public LimbUpperAssessmentVO getLimbUpperAssessment(LimbUpperAssessmentVO limbassessment, boolean showDeleted) throws ApplicationException {
        try {
            LimbUpperAssessmentVO tmp = (LimbUpperAssessmentVO) dao.findUpperLimbByCriteria(limbassessment, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public LimbAdvAssessmentVO getLimbAdvAssessment(LimbAdvAssessmentVO limbassessment) throws ApplicationException {
        try {
            LimbAdvAssessmentVO tmp = (LimbAdvAssessmentVO) dao.findAdvLimbByCriteria(limbassessment);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public void saveAdvLimb(LimbAdvAssessmentVO vo) throws ApplicationException {
        try {
            dao.insertLimbAdv(vo);
            LimbAdvAssessmentVO currentPatient = (LimbAdvAssessmentVO)getLimbAdvAssessment(vo);
            if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "limb_assessment", vo));
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }
public LimbUpperAssessmentVO getLimbAdvAssessment(LimbUpperAssessmentVO limbassessment, boolean showDeleted) throws ApplicationException {
        try {
            LimbUpperAssessmentVO tmp = (LimbUpperAssessmentVO) dao.findUpperLimbByCriteria(limbassessment, showDeleted);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public LimbUpperAssessmentVO getLimbUpperAssessment(LimbUpperAssessmentVO limbassessment) throws ApplicationException {
        try {
            LimbUpperAssessmentVO tmp = (LimbUpperAssessmentVO) dao.findUpperLimbByCriteria(limbassessment);
            return tmp;
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in WoundManagerBD.retrieveWoundProfile(): " + e.toString(), e);
        }
    }
    public void saveUpperLimb(LimbUpperAssessmentVO vo) throws ApplicationException {
        try {
            dao.insertLimbUpper(vo);
            LimbUpperAssessmentVO currentPatient = (LimbUpperAssessmentVO)getLimbUpperAssessment(vo);
            if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "limb_assessment", currentPatient));
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }
    public void saveFoot(FootAssessmentVO vo) throws ApplicationException {
        try {
            dao.insertFoot(vo);
            FootAssessmentVO currentPatient = (FootAssessmentVO)getFootAssessment(vo);
            if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "foot_assessment", currentPatient));
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }
    /*************************************************************************/
    
   public void saveSensory(SensoryAssessmentVO vo) throws ApplicationException {
        try {
        	dao.insertSensory(vo);
            SensoryAssessmentVO currentPatient = (SensoryAssessmentVO)getSensoryAssessment(vo);
            if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (vo.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "sensory_assessment", currentPatient));
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in WoundManagerBD(): " + e.toString(), e);
        }
    }
    
    /*******************************************************************************/
    public void updatePatientProfile(PatientProfileVO profileVO) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            patientDAO.update(profileVO);
            PatientProfileVO currentPatient = (PatientProfileVO)getPatientProfile(profileVO);
            if(currentPatient!=null){logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(currentPatient.getProfessional_id(), currentPatient.getPatient_id(), (profileVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "patient_profiles", currentPatient));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void saveOffline(OfflineVO off) throws ApplicationException {
        try {
            PatientProfileDAO pat = new PatientProfileDAO();
            pat.insertOffline(off);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public boolean doesExist(PatientProfileVO profile, int professional_id) throws ApplicationException {
        boolean exists = false;
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            PatientProfileVO patientProfile = (PatientProfileVO) patientDAO.findByCriteria(profile);
            if (patientProfile != null && patientProfile.getProfessional_id().intValue() == professional_id) {
                exists = true;
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
        return exists;
    }
    /*
     * getFlowchartRow returns an entire row for the fl owchart (all 3 columns).
     * This allows us to dynamically create the flowcharts.
     */
    private Vector getFlowchartRow(int column, int row, String value, Vector vector) throws ApplicationException {
        try {
            if (value == null) {
                value = "";
            }
            if (column == 0) {
                Hashtable t = new Hashtable();
                t.put("title", value);
                vector.add(row, t);
            } else if (column > 0) {
                Hashtable t = (Hashtable) vector.get(row);
                t.put("profile" + column, value);
                vector.set(row, t);
            }
        } catch (NullPointerException e) {
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
        return vector;
    }
    public void mergePatientProfiles(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        PatientProfileVO i = new PatientProfileVO();
        i.setPatient_id(patient_from);
        Collection<PatientProfileVO> many = getAllPatientProfiles(i, -1);
        for (PatientProfileVO a : many) {
            a.setPatient_id(patient_to);
            savePatientProfile(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_burn");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void mergeLimbBasicAssessments(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        LimbBasicAssessmentVO i = new LimbBasicAssessmentVO();
        i.setPatient_id(patient_from);
        Collection<LimbBasicAssessmentVO> many = getAllBasicLimbAssessments(i, -1);
        for (LimbBasicAssessmentVO a : many) {
            a.setPatient_id(patient_to);
            saveBasicLimb(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_burn");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void mergeLimbUpperAssessments(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        LimbUpperAssessmentVO i = new LimbUpperAssessmentVO();
        i.setPatient_id(patient_from);
        Collection<LimbUpperAssessmentVO> many = getAllUpperLimbAssessments(i, -1);
        for (LimbUpperAssessmentVO a : many) {
            a.setPatient_id(patient_to);
            saveUpperLimb(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("limb_upper_assessment");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void mergeLimbAdvAssessments(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        LimbAdvAssessmentVO i = new LimbAdvAssessmentVO();
        i.setPatient_id(patient_from);
        Collection<LimbAdvAssessmentVO> many = getAllAdvLimbAssessments(i, -1);
        for (LimbAdvAssessmentVO a : many) {
            a.setPatient_id(patient_to);
            saveAdvLimb(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_burn");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void mergeExam(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        PhysicalExamVO i = new PhysicalExamVO();
        i.setPatient_id(patient_from);
        Collection<PhysicalExamVO> many = getAllExams(i, -1);
        for (PhysicalExamVO a : many) {
            a.setPatient_id(patient_to);
            savePhysicalExam(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_burn");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void mergeFootAssessments(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        FootAssessmentVO i = new FootAssessmentVO();
        i.setPatient_id(patient_from);
        Collection<FootAssessmentVO> many = getAllFootAssessments(i, -1);
        for (FootAssessmentVO a : many) {
            a.setPatient_id(patient_to);
            saveFoot(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_burn");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void mergeBradens(int patient_from, int patient_to, EventVO event) throws ApplicationException {
        IntegrationDAO integrationDAO = new IntegrationDAO();
        BradenVO i = new BradenVO();
        i.setPatient_id(patient_from);
        Collection<BradenVO> many = getAllBradens(i, -1);
        for (BradenVO a : many) {
            a.setPatient_id(patient_to);
            saveBraden(a);
            event.setMerge(1);
            event.setPatient_to(patient_to);
            event.setPatient_from(patient_from);
            event.setTable_name("assessment_burn");
            event.setRow_id(a.getId());
            try {
                integrationDAO.insert(event);// saving event, so we can rollback if need be.
            } catch (DataAccessException ex) {
                ex.printStackTrace();
            }
        }
    }
    public Collection<FootAssessmentVO> getAllFootAssessments(FootAssessmentVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllFeetByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    
    //method for sensory flowchart 
    
    public Collection<SensoryAssessmentVO> getAllSensoryAssessments(SensoryAssessmentVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllSensoryByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    
    // method end....
    
    
public Collection<PhysicalExamVO> getAllExams(PhysicalExamVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllByCriteria(crit, limit);
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    public Collection<LimbBasicAssessmentVO> getAllBasicLimbAssessments(LimbBasicAssessmentVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllBasicLimbsByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    public Collection<LimbAdvAssessmentVO> getAllAdvLimbAssessments(LimbAdvAssessmentVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllAdvLimbsByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    public Collection<LimbUpperAssessmentVO> getAllUpperLimbAssessments(LimbUpperAssessmentVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllUpperLimbsByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    public Collection<DiagnosticInvestigationVO> getAllDiagnosticAssessments(DiagnosticInvestigationVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    public Collection<PatientProfileVO> getAllPatientProfiles(PatientProfileVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    public Collection<BradenVO> getAllBradens(BradenVO crit, int limit) throws ApplicationException {
        Collection results = null;
        try {
            results = dao.findAllByCriteria(crit, limit);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    public Vector<OfflineVO> getAllOffline(OfflineVO crit, ProfessionalVO user, boolean ignoreDups) throws ApplicationException {
        Vector<OfflineVO> results = null;
        try {
            results = dao.findAllOfflineByCriteria(crit, user, ignoreDups);
        } catch (DataAccessException e) {
            log.error("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
            throw new ApplicationException("Error in PatientProfileManagerBD.getPatientProfiles(): " + e.toString(), e);
        }
        return results;
    }
    /*
     * The following  method gets all patient_profile records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllPatientProfilesForFlowchart(Collection<PatientProfileVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode,boolean showNursingFixes) throws ApplicationException {
        RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        PatientProfileVO vo = new PatientProfileVO();
        PDate pdate = new PDate(currentProfessional != null ? currentProfessional.getTimezone() : Constants.TIMEZONE);
        ListServiceImpl lservice = new ListServiceImpl(language);
        
        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.PATIENT_PROFILE);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
       

        //////////////////////////////////////////////////////
        // START SETUP STATIC VARIABLES FROM PATIENT ACCOUNT
        //////////////////////////////////////////////////////
        int patient_id = 0;
        String gender = "";
        String patient_residence = "";
        String allergies = "";
        String phn = "";
        String phn2 = "";
        String phn3 = "";
        String funding_source= "";
        boolean hasConsent = false;
        PatientAccountVO patientAcc = null;
        try {
            for (PatientProfileVO patientprofile : results) {
                patient_id = patientprofile.getPatient_id().intValue();
            }
            LookupVO lookupVO = new LookupVO();
            ListsDAO listDAO = new ListsDAO();
            PatientServiceImpl pamanager = new PatientServiceImpl(language);
             patientAcc = pamanager.getPatient(patient_id);
            if (results != null && results.size() > 0) {
                phn = patientAcc.getPhn();
                if(!Common.getConfig("hideVersionCode").equals("1") && patientAcc.getVersion_code()!=null){
                    phn=phn+" #"+patientAcc.getVersion_code();
                }
                phn2 = patientAcc.getPhn2();
                phn3 = patientAcc.getPhn3();
                // SETUP NEW STATIC DATA (GENDER)
                Collection<LookupVO> co = null;
                lookupVO.setResourceId(new Integer(lookupVO.GENDER));
                lookupVO.setId(patientAcc.getGender());
                co = listDAO.findAllByCriteria(lookupVO);
                for (Iterator itera = co.iterator(); itera.hasNext();) {
                    LookupVO colookupVO = (LookupVO) itera.next();
                    gender = colookupVO.getName(language);
                }
                // SETUP NEW STATIC DATA (PATIENT RESIDENCE)
                lookupVO.setResourceId(new Integer(lookupVO.PATIENT_RESIDENCE));
                lookupVO.setId(patientAcc.getPatient_residence());
                co = listDAO.findAllByCriteria(lookupVO);
                for (Iterator itera = co.iterator(); itera.hasNext();) {
                    LookupVO colookupVO = (LookupVO) itera.next();
                    patient_residence = colookupVO.getName(language);
                }
                lookupVO.setResourceId(new Integer(lookupVO.FUNDING_SOURCE));
                lookupVO.setId(patientAcc.getFunding_source());
                co = listDAO.findAllByCriteria(lookupVO);
                for (Iterator itera = co.iterator(); itera.hasNext();) {
                    LookupVO colookupVO = (LookupVO) itera.next();
                    funding_source = colookupVO.getName(language);
                }
                allergies = patientAcc.getAllergies();
                hasConsent = patientAcc.isPatientConsent();
            }
        } catch (com.pixalere.common.DataAccessException e) {
        }
        //////////////////////////////////////////////////////
        // END SETUP STATIC VARIABLES FROM PATIENT ACCOUNT
        //////////////////////////////////////////////////////

        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            boolean useComponent = true;
            if ((component.getField_name().equals("phn2")) && (Common.getConfig("hidephn2").equals("1"))) {
                useComponent = false;
            } else if ((component.getField_name().equals("phn3")) && (Common.getConfig("hidephn3").equals("1"))) {
                useComponent = false;
            }
            if (useComponent) {
                for (PatientProfileVO patientprofile : results) {
                    
                    ProfessionalVO userVO = null;
                    if (patientprofile != null && patientprofile.getProfessional() != null) {
                        userVO = patientprofile.getProfessional();
                    }
                    if (cnt == 2) {
                        List<FieldValues> fields = new ArrayList();
                        FieldValues f = new FieldValues();
                        f.setTitle("id");
                        String valuet2 = patientprofile.getId() + "";
                        f.setValue(valuet2);
                        fields.add(f);
                        FieldValues f2 = new FieldValues();
                        f2.setTitle(Common.getLocalizedString("pixalere.patientprofile.form.pageheader",locale));
                        String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + patientprofile.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (patientprofile.getId() != null ? (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale):"") : "");
                        f2.setValue(valuet22);

                        fields.add(f2);
                        rows[count] = new RowData();
                        rows[count].setUser_signature(patientprofile.getUser_signature());
                        rows[count].setId(patientprofile.getId() != null ? patientprofile.getId().intValue() : 0);
                        rows[count].setFields(fields);
                        rows[count].setHeader("pixalere.patientprofile.form.pageheader");
                        rows[count].setDeleted((patientprofile.getDeleted()!=null?patientprofile.getDeleted():0));
                        rows[count].setDelete_signature(patientprofile.getDelete_signature());
                        rows[count].setDelete_reason(patientprofile.getDeleted_reason());
                    }
                    List<FieldValues> fields = rows[count].getFields();
                    //Get all columns for each component
                    String value = "";
                    if (component.getComponent_type().equals(Constants.VISIT_COMPONENT_TYPE)) {
                        FieldValues field = new FieldValues();
                        PatientServiceImpl pm = new PatientServiceImpl(language);
                        PatientAccountVO pa = pm.getPatient(patientprofile.getPatient_id().intValue());
                        value = pa.getVisits() + "";
                        field.setValue(value);
                        field.setDb_value(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    } else if (component.getComponent_type().equals(Constants.AGE_COMPONENT_TYPE)) {
                        PatientServiceImpl pm = new PatientServiceImpl(language);
                        PatientAccountVO pa = pm.getPatient(patientprofile.getPatient_id().intValue());
                        FieldValues field = new FieldValues();
                        PDate pd = new PDate();
                        String age = pd.getAge(pa.getYear(), pa.getMonth(), pa.getDay());
                        value = age;
                        field.setValue(value);
                        field.setDb_value(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                        /*}else if(component.getComponent_type().equals(Constants.BRADEN_TOTAL_COMPONENT_TYPE)){
                        FieldValues field = new FieldValues();
                        value = Integer.toString(patientprofile.getBraden_moisture()+patientprofile.getBraden_friction()+patientprofile.getBraden_nutrition()+patientprofile.getBraden_sensory()+patientprofile.getBraden_activity()+patientprofile.getBraden_mobility())+" ("+patientprofile.getBraden_moisture()+"/"+patientprofile.getBraden_friction()+"/"+patientprofile.getBraden_nutrition()+"/"+patientprofile.getBraden_sensory()+"/"+patientprofile.getBraden_activity()+"/"+patientprofile.getBraden_mobility()+")";
                        field.setValue(value);
                        field.setDb_value(value);
                        field.setTitle(title);
                        field.setAllow_edit(0);
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields[cnt]=field;*/
                    } else if (component.getField_name().equals("treatment_location_id")) {
                        FieldValues field = new FieldValues();
                        value = "";
                        if (patientAcc != null && patientAcc.getTreatment_location_id() != null) {
                            LookupVO tl = lservice.getListItem(new Integer(patientAcc.getTreatment_location_id()));
                            if (tl != null) {
                                value = tl.getName(language);
                            }
                        }
                        field.setValue(value);
                        field.setDb_value(value);
                        field.setTitle(Common.getConfig("treatmentLocationName"));
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.PP_ARRAY_SECTION)) {
                        FieldValues field = new FieldValues();
                        List<ArrayValueObject> list_items = new ArrayList();
                        PatientProfileArraysVO t = new PatientProfileArraysVO();
                        t.setPatient_profile_id(patientprofile.getId());
                        try {
                            Collection<PatientProfileArraysVO> arrays = patientprofile.getPatient_profile_arrays();
                            PatientProfileArraysVO[] rr = new PatientProfileArraysVO[arrays.size()];
                            rr = arrays.toArray(rr);
                            if (component.getField_name().equals("co_morbidities")) {
                                list_items = Common.filterArrayObjects(rr, LookupVO.COMORBIDITIES);
                            } else {
                                list_items = Common.filterArrayObjects(rr, component.getResource_id());
                            }
                        } catch (ClassCastException e) {
                            e.printStackTrace();
                        }
                        value = Serialize.serialize(list_items, (blnEmbedCode == true ? "<br/>" : ","),language);
                        field.setDb_value(value);
                        value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(t != null && patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale):"");
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    } else if (component.getTable_id() != null && component.getTable_id().equals(Constants.EXTERNAL_REFERRALS_SECTION)) {
                      
                      FieldValues field = new FieldValues();
                      List<String> list_items = new ArrayList();
                      ExternalReferralVO t = new ExternalReferralVO();
                      t.setPatient_profile_id(patientprofile.getId());
                      try {
                          Collection<ExternalReferralVO> arrays = patientprofile.getExternalReferrals();
                          
                          if(arrays!=null){
                              for(ExternalReferralVO arr : arrays){
                                  if(arr!=null && arr.getExternal_referral_id()!=null){
                                      LookupVO item1 = lservice.getListItem(arr.getExternal_referral_id());
                                      LookupVO item2 = lservice.getListItem(arr.getArranged_id());
                                      if(item1!=null && item2!=null){
                                          list_items.add(item1.getName(language)+" : "+item2.getName(language));
                                      }
                                  }
                              }
                          }
                          
                      } catch (ClassCastException e) {
                          e.printStackTrace();
                      }
                      value = Serialize.serialize(list_items, (blnEmbedCode == true ? "<br/>" : ","));
                      field.setDb_value(value);
                      value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(t != null && patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale):"");
                      field.setValue(value);
                      field.setTitle(title);
                      field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                      field.setField_name(component.getField_name());
                      field.setComponent_id(component.getId().intValue());
                      fields.add(field);
                     } else if (component.getComponent_type().equals(Constants.DOB_COMPONENT_TYPE)) {
                        FieldValues field = new FieldValues();
                        PatientServiceImpl pm = new PatientServiceImpl(language);
                        PatientAccountVO pa = pm.getPatient(patientprofile.getPatient_id().intValue());
                        if (pa.getDob() != null) {
                            value = pa.getDateOfBirthStr();
                        } else {
                            value = "";
                        }
                        field.setDb_value(value);
                        //value=value+Common.showNursingFixes(am.getNursingFixes(patientprofile.getId()!=null?patientprofile.getId().intValue()+"":"0",component.getId()+""), blnEmbedCode,locale);
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(0);
                        //field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0,false, Common.isOffline()?1:0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    } else if (component.getComponent_type().equals(Constants.VIEW_PATIENT_CONSENT_COMPONENT_TYPE) ) {
                        if (Common.getConfig("patientConsentEnabled").equals("1") && hasConsent){
                            FieldValues field = new FieldValues();
                            field.setTitle(title);
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());

                            String displayValue = "<input type=\"button\" id=\"view_consent\" value=\"" + 
                                    Common.getLocalizedString("pixalere.patientprofile.form.view_patient_consent_button", locale) + 
                                    "\" onClick=\"loadWindow('PatientConsentViewer.do',600,700)\">";
                            field.setValue(displayValue);

                            fields.add(field);
                        }
                        // If not enabled/no consent exists - do nothing with this component
                        
                    } else {
                        // SET STATIC VARIABLES FROM PATIENT ACCOUNT
                        if (component.getField_name().equals("patient_res_id")) {
                            FieldValues field = new FieldValues();
                            field.setDb_value(patient_residence);
                            field.setValue(patient_residence);
                            field.setTitle(title);
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("gender")) {
                            FieldValues field = new FieldValues();
                            field.setDb_value(gender);
                            field.setValue(gender);
                            field.setTitle(title);
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("allergies")) {
                            FieldValues field = new FieldValues();
                            field.setDb_value(allergies);
                            field.setValue(allergies);
                            field.setTitle(title);
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("funding_source")) {
                            FieldValues field = new FieldValues();
                            field.setDb_value(funding_source);
                            field.setValue(funding_source);
                            field.setTitle(title);
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("phn")) {
                            FieldValues field = new FieldValues();
                            field.setDb_value(phn);
                            field.setValue(phn);
                            field.setTitle(Common.getConfig("phn1_name"));
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("phn2")) {
                            FieldValues field = new FieldValues();
                            field.setDb_value(phn2);
                            field.setValue(phn2);
                            field.setTitle(Common.getConfig("phn2_name"));
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else if (component.getField_name().equals("phn3")) {
                            FieldValues field = new FieldValues();
                            field.setDb_value(phn3);
                            field.setValue(phn3);
                            field.setTitle(Common.getConfig("phn3_name"));
                            field.setAllow_edit(0);
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        } else {
                            FieldValues field = new FieldValues();
                            Hashtable v = Common.getPrintableValue(patientprofile, userVO, component, blnEmbedCode, count,language);
                            field.setDb_value((String) v.get("db_value"));
                            Vector va = (Vector) v.get("value");
                            if (va != null && va.size() > 0) {
                                value = (String) va.get(0);
                                value = value + (showNursingFixes?Common.showNursingFixes(am.getNursingFixes(patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale):"");
                            }
                            field.setValue(value);
                            field.setTitle(title);
                            field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                            field.setField_name(component.getField_name());
                            field.setComponent_id(component.getId().intValue());
                            fields.add(field);
                        }
                    }
                    rows[count].setFields(fields);
                    count++;
                }
                cnt++;
                cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
            }
        }

        return rows;
    }
    /*
     * The following  method gets all patient_profile records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllBradenForFlowchart(Collection<BradenVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        BradenVO vo = new BradenVO();
        ListServiceImpl lservice = new ListServiceImpl();
        
        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.BRADEN_SCORE_SECTION);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (BradenVO patientprofile : results) {
                ProfessionalVO userVO = null;
                userVO = patientprofile.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = patientprofile.getId() + "";
                    f.setValue(valuet2);
                    fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.viewer.form.braden_scale",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + patientprofile.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (patientprofile.getId() != null ? Common.showNursingFixes(am.getNursingFixes(patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                    fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(patientprofile.getUser_signature());
                    rows[count].setId(patientprofile.getId() != null ? patientprofile.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.braden");
                    rows[count].setDeleted(patientprofile.getDeleted());
                    rows[count].setDelete_signature(patientprofile.getDelete_signature());
                    rows[count].setDelete_reason(patientprofile.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                FieldValues field = new FieldValues();
                Hashtable v = Common.getPrintableValue(patientprofile, userVO, component, blnEmbedCode, count,language);
                field.setDb_value((String) v.get("db_value"));
                Vector va = (Vector) v.get("value");
                value = (String) va.get(0);
                value = value + Common.showNursingFixes(am.getNursingFixes(patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                field.setValue(value);
                field.setTitle(title);
                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                field.setField_name(component.getField_name());
                field.setComponent_id(component.getId().intValue());
                fields.add(field);
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }


        return rows;
    }
    /*
     * The following  method gets all patient_profile records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllExamsForFlowchart(Collection<PhysicalExamVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        PhysicalExamVO vo = new PhysicalExamVO();
        ListServiceImpl lservice = new ListServiceImpl();
        
        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.PHYSICAL_EXAM);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (PhysicalExamVO patientprofile : results) {
                ProfessionalVO userVO = null;
                userVO = patientprofile.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = patientprofile.getId() + "";
                    f.setValue(valuet2);
                    fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.viewer.form.physical_exam",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + patientprofile.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (patientprofile.getId() != null ? Common.showNursingFixes(am.getNursingFixes(patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                    fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(patientprofile.getUser_signature());
                    rows[count].setId(patientprofile.getId() != null ? patientprofile.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.physical_exam");
                    rows[count].setDeleted(patientprofile.getDeleted());
                    rows[count].setDelete_signature(patientprofile.getDelete_signature());
                    rows[count].setDelete_reason(patientprofile.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                FieldValues field = new FieldValues();
                Hashtable v = Common.getPrintableValue(patientprofile, userVO, component, blnEmbedCode, count,language);
                field.setDb_value((String) v.get("db_value"));
                Vector va = (Vector) v.get("value");
                if(va !=null && va.size()>0){
                    value = (String) va.get(0);
                }
                value = value + Common.showNursingFixes(am.getNursingFixes(patientprofile.getId() != null ? patientprofile.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                field.setValue(value);
                field.setTitle(title);
                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                field.setField_name(component.getField_name());
                field.setComponent_id(component.getId().intValue());
                fields.add(field);
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }


        return rows;
    }
    /*
     * The following  method gets all limb_assessments records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllBasicLimbAssessmentsForFlowchart(Collection<LimbBasicAssessmentVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        //PatientProfileVO vo = new PatientProfileVO();

        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.LIMB_ASSESSMENT_BASIC);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        int flowchart_row_count=0;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
                flowchart_row_count++;
            }
        }
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (LimbBasicAssessmentVO limbassessment : results) {
                ProfessionalVO userVO = null;
                userVO = limbassessment.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = limbassessment.getId() + "";
                    f.setValue(valuet2);
                     fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.patientprofile.form.limb_assessment",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + limbassessment.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (limbassessment.getId() != null ? Common.showNursingFixes(am.getNursingFixes(limbassessment.getId() != null ? limbassessment.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                     fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(limbassessment.getUser_signature());
                    rows[count].setId(limbassessment.getId() != null ? limbassessment.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.pageheader");
                    rows[count].setDeleted(limbassessment.getDeleted());
                    rows[count].setDelete_signature(limbassessment.getDelete_signature());
                    rows[count].setDelete_reason(limbassessment.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                
                    FieldValues field = new FieldValues();
                    Hashtable v = Common.getPrintableValue(limbassessment, userVO, component, blnEmbedCode, count,language);
                    field.setDb_value((String) v.get("db_value"));
                    Vector va = (Vector) v.get("value");
                    if(va!=null && va.size()>0){
                        value = (String) va.get(0);
                    }
                    value = value + Common.showNursingFixes(am.getNursingFixes(limbassessment.getId() != null ? limbassessment.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                    field.setValue(value);
                    field.setTitle(title);
                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                    field.setField_name(component.getField_name());
                    field.setComponent_id(component.getId().intValue());
                    fields.add(field);
                
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }


        return rows;
    }
    /*
     * The following  method gets all limb_assessments records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllUpperLimbAssessmentsForFlowchart(Collection<LimbUpperAssessmentVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        //PatientProfileVO vo = new PatientProfileVO();

        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.LIMB_ASSESSMENT_UPPER);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        int flowchart_row_count=0;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
                flowchart_row_count++;
            }
        }
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (LimbUpperAssessmentVO limbassessment : results) {
                ProfessionalVO userVO = null;
                userVO = limbassessment.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = limbassessment.getId() + "";
                    f.setValue(valuet2);
                    fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.patientprofile.form.limb_assessment",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + limbassessment.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (limbassessment.getId() != null ? Common.showNursingFixes(am.getNursingFixes(limbassessment.getId() != null ? limbassessment.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                    fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(limbassessment.getUser_signature());
                    rows[count].setId(limbassessment.getId() != null ? limbassessment.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.pageheader");
                    rows[count].setDeleted(limbassessment.getDeleted());
                    rows[count].setDelete_signature(limbassessment.getDelete_signature());
                    rows[count].setDelete_reason(limbassessment.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                    FieldValues field = new FieldValues();
                    Hashtable v = Common.getPrintableValue(limbassessment, userVO, component, blnEmbedCode, count,language);
                    field.setDb_value((String) v.get("db_value"));
                    Vector va = (Vector) v.get("value");
                    if(va!=null && va.size()>0){
                    value = (String) va.get(0);
                    }
                    value = value + Common.showNursingFixes(am.getNursingFixes(limbassessment.getId() != null ? limbassessment.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                    field.setValue(value);
                    field.setTitle(title);
                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                    field.setField_name(component.getField_name());
                    field.setComponent_id(component.getId().intValue());
                    fields.add(field);
                
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }


        return rows;
    }
    /*
     * The following  method gets all limb_assessments records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllDiagnosticAssessmentsForFlowchart(Collection<DiagnosticInvestigationVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        //PatientProfileVO vo = new PatientProfileVO();
        ListServiceImpl lservice = new ListServiceImpl(language);
        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.DIAGNOSTIC_INVESTIGATION);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        int flowchart_row_count=0;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
                flowchart_row_count++;
            }
        }
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (DiagnosticInvestigationVO diag : results) {
                ProfessionalVO userVO = null;
                userVO = diag.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = diag.getId() + "";
                    f.setValue(valuet2);
                    fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.patientprofile.form.investigations",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + diag.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (diag.getId() != null ? Common.showNursingFixes(am.getNursingFixes(diag.getId() != null ? diag.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                    fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(diag.getUser_signature());
                    rows[count].setId(diag.getId() != null ? diag.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.pageheader");
                    rows[count].setDeleted(diag.getDeleted());
                    rows[count].setDelete_signature(diag.getDelete_signature());
                    rows[count].setDelete_reason(diag.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                 if (component.getTable_id() != null && component.getTable_id().equals(Constants.INVESTIGATION_SECTION)) {
                        FieldValues field = new FieldValues();
                        Collection<InvestigationsVO> investigations = diag.getInvestigations();
                        List<String> list = new ArrayList();
                        if (investigations != null) {
                            SimpleDateFormat sf = new SimpleDateFormat(Constants.DATE_FORMAT);
                            for (InvestigationsVO investigation : investigations) {
                                if (investigation.getLookup() == null) {
                                    LookupVO lit = lservice.getListItem(investigation.getLookup_id());
                                    investigation.setLookup(lit);
                                }
                                if (investigation.getLookup() != null) {
                                    list.add(investigation.getLookup().getName(language) + " : " + (investigation.getInvestigation_date()!=null?sf.format(investigation.getInvestigation_date()):""));
                                }
                            }
                        }
                        value = Serialize.serialize(list, ",");
                        Hashtable v = Common.getPrintableValue(diag.getInvestigations(), userVO, component, blnEmbedCode, count,language);
                        field.setDb_value(value);
                        value = value + Common.showNursingFixes(am.getNursingFixes(diag.getId() != null ? diag.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    }else{
                        FieldValues field = new FieldValues();
                        Hashtable v = Common.getPrintableValue(diag, userVO, component, blnEmbedCode, count,language);
                        field.setDb_value((String) v.get("db_value"));
                        Vector va = (Vector) v.get("value");
                        if(va!=null && va.size()>0){
                            value = (String) va.get(0);
                        }
                        value = value + Common.showNursingFixes(am.getNursingFixes(diag.getId() != null ? diag.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                        field.setValue(value);
                        field.setTitle(title);
                        field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                        field.setField_name(component.getField_name());
                        field.setComponent_id(component.getId().intValue());
                        fields.add(field);
                    }
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }


        return rows;
    }
    /*
     * The following  method gets all limb_assessments records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllAdvLimbAssessmentsForFlowchart(Collection<LimbAdvAssessmentVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        System.out.println("in getAllAdvLimbAssessmentsForFlowchart method start in PatientProfileServiceImpl");
    	RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        //PatientProfileVO vo = new PatientProfileVO();

        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.LIMB_ASSESSMENT_ADVANCED);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        int flowchart_row_count=0;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
                flowchart_row_count++;
            }
        }
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (LimbAdvAssessmentVO limbassessment : results) {
                ProfessionalVO userVO = null;
                userVO = limbassessment.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = limbassessment.getId() + "";
                    f.setValue(valuet2);
                     fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.patientprofile.form.limb_assessment",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + limbassessment.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (limbassessment.getId() != null ? Common.showNursingFixes(am.getNursingFixes(limbassessment.getId() != null ? limbassessment.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                     fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(limbassessment.getUser_signature());
                    rows[count].setId(limbassessment.getId() != null ? limbassessment.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.pageheader");
                    rows[count].setDeleted(limbassessment.getDeleted());
                    rows[count].setDelete_signature(limbassessment.getDelete_signature());
                    rows[count].setDelete_reason(limbassessment.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                if (component.getComponent_type().equals(Constants.BRACHIAL_LEFT_SCORE)) {
                    FieldValues field = new FieldValues();
                    int num1 = 0;
                    int num2 = 0;
                    int num3 = 0;
                    int num4 = 0;
                    if (limbassessment.getLeft_dorsalis_pedis_ankle_brachial() != null ) {
                        num1 = Integer.parseInt(limbassessment.getLeft_dorsalis_pedis_ankle_brachial()+"");
                    }
                    if (limbassessment.getLeft_tibial_pedis_ankle_brachial() != null ) {
                        num2 = Integer.parseInt(limbassessment.getLeft_tibial_pedis_ankle_brachial()+"");
                    }
                    if (limbassessment.getLeft_ankle_brachial() != null  ) {
                        num3 = Integer.parseInt(limbassessment.getLeft_ankle_brachial()+"");
                    }
                    if (limbassessment.getRight_ankle_brachial() != null  ) {
                        num4 = Integer.parseInt(limbassessment.getRight_ankle_brachial()+"");
                    }
                    if (num1 < num2) {
                        num1 = num2;
                    }
                    if (num3 < num4) {
                        num3 = num4;
                    }
                    value = Common.calculateLimbScore(num1, num3);
                    
                    //value = limbassessment.getLeft_api_score()+"";
                    field.setValue(value);
                    field.setDb_value(value);
                    field.setTitle(title);
                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                    field.setField_name(component.getField_name());
                    field.setComponent_id(component.getId().intValue());
                     fields.add(field);
                } else if (component.getComponent_type().equals(Constants.BRACHIAL_RIGHT_SCORE)) {
                    FieldValues field = new FieldValues();
                    int num1 = 0;
                    int num2 = 0;
                    int num3 = 0;
                    int num4 = 0;
                    if (limbassessment.getRight_dorsalis_pedis_ankle_brachial() != null ) {
                        num1 = Integer.parseInt(limbassessment.getRight_dorsalis_pedis_ankle_brachial()+"");
                    }
                    if (limbassessment.getRight_tibial_pedis_ankle_brachial() != null) {
                        num2 = Integer.parseInt(limbassessment.getRight_tibial_pedis_ankle_brachial()+"");
                    }
                    if (limbassessment.getLeft_ankle_brachial() != null ) {
                        num3 = Integer.parseInt(limbassessment.getLeft_ankle_brachial()+"");
                    }
                    if (limbassessment.getRight_ankle_brachial() != null ) {
                        num4 = Integer.parseInt(limbassessment.getRight_ankle_brachial()+"");
                    }
                    if (num1 < num2) {
                        num1 = num2;
                    }
                    if (num3 < num4) {
                        num3 = num4;
                    }
                    value = Common.calculateLimbScore(num1, num3);
                    //value = limbassessment.getRight_api_score()+"";
                    field.setValue(value);
                    field.setDb_value(value);
                    field.setTitle(title);
                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                    field.setField_name(component.getField_name());
                    field.setComponent_id(component.getId().intValue());
                     fields.add(field);
                } else if (component.getComponent_type().equals(Constants.TBI_LEFT_SCORE)) {
                    FieldValues field = new FieldValues();
                    int num1 = 0;
                    int num2 = 0;
                    int num3 = 0;
                    if (limbassessment.getLeft_toe_pressure() != null && !limbassessment.getLeft_toe_pressure().equals("")) {
                        num1 = Integer.parseInt(limbassessment.getLeft_toe_pressure());
                    }
                    if (limbassessment.getLeft_brachial_pressure() != null && !limbassessment.getLeft_brachial_pressure().equals("")) {
                        num2 = Integer.parseInt(limbassessment.getLeft_brachial_pressure());
                    }
                    if (limbassessment.getRight_brachial_pressure() != null && !limbassessment.getRight_brachial_pressure().equals("")) {
                        num3 = Integer.parseInt(limbassessment.getRight_brachial_pressure());
                    }
                    if (num2 < num3) {
                        num2 = num3;
                    }
                    value = Common.calculateLimbScore(num1, num2);
                    field.setValue(value);
                    field.setDb_value(value);
                    field.setTitle(title);
                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                    field.setField_name(component.getField_name());
                    field.setComponent_id(component.getId().intValue());
                     fields.add(field);
                } else if (component.getComponent_type().equals(Constants.TBI_RIGHT_SCORE)) {
                    FieldValues field = new FieldValues();
                    int num1 = 0;
                    int num2 = 0;
                    int num3 = 0;
                    if (limbassessment.getRight_toe_pressure() != null && !limbassessment.getRight_toe_pressure().equals("")) {
                        num1 = Integer.parseInt(limbassessment.getRight_toe_pressure());
                    }
                    if (limbassessment.getRight_brachial_pressure() != null && !limbassessment.getRight_brachial_pressure().equals("")) {
                        num2 = Integer.parseInt(limbassessment.getRight_brachial_pressure());
                    }
                    if (limbassessment.getLeft_brachial_pressure() != null && !limbassessment.getLeft_brachial_pressure().equals("")) {
                        num3 = Integer.parseInt(limbassessment.getLeft_brachial_pressure());
                    }
                    if (num2 < num3) {
                        num2 = num3;
                    }
                    value = Common.calculateLimbScore(num1, num2);
                    field.setValue(value);
                    field.setDb_value(value);
                    field.setTitle(title);
                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                    field.setField_name(component.getField_name());
                    field.setComponent_id(component.getId().intValue());
                     fields.add(field);
                } else {
                    FieldValues field = new FieldValues();
                    Hashtable v = Common.getPrintableValue(limbassessment, userVO, component, blnEmbedCode, count,language);
                    field.setDb_value((String) v.get("db_value"));
                    Vector va = (Vector) v.get("value");
                    if(va!=null && va.size()>0){
                    value = (String) va.get(0);
                    }
                    value = value + Common.showNursingFixes(am.getNursingFixes(limbassessment.getId() != null ? limbassessment.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                    field.setValue(value);
                    field.setTitle(title);
                    field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                    field.setField_name(component.getField_name());
                    field.setComponent_id(component.getId().intValue());
                     fields.add(field);
                }
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }


        return rows;
    }
    /*
     * The following  method gets all limb_assessments records by the criteria specified
     * and organized the data to be present in the flowcharts.  This way the flowchart GUI does not need to know
     * how many rows/columns it needs to be displayed, therefore causing less changes when customization occurs.
     *
     * @return Vector the vector of flowchart columns
     * @param patient_id the Patient to be used to figure out what the criteria we need returned is
     * @param limit the # of records we require
     * @param id the
     * @todo in V4 this needs to be completely automated based on the page layout customization
     * @since 3.2
     */
    public RowData[] getAllFootAssessmentsForFlowchart(Collection<FootAssessmentVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        //PatientProfileVO vo = new PatientProfileVO();

        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.FOOT_ASSESSMENT);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (FootAssessmentVO footassessment : results) {
                ProfessionalVO userVO = null;
                userVO = footassessment.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = footassessment.getId() + "";
                    f.setValue(valuet2);
                    fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.patientprofile.form.foot_assessment",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + footassessment.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (footassessment.getId() != null ? Common.showNursingFixes(am.getNursingFixes(footassessment.getId() != null ? footassessment.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                    fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(footassessment.getUser_signature());
                    rows[count].setId(footassessment.getId() != null ? footassessment.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.pageheader");
                    rows[count].setDeleted(footassessment.getDeleted());
                    rows[count].setDelete_signature(footassessment.getDelete_signature());
                    rows[count].setDelete_reason(footassessment.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                FieldValues field = new FieldValues();
                Hashtable v = Common.getPrintableValue(footassessment, userVO, component, blnEmbedCode, count,language);
                field.setDb_value((String) v.get("db_value"));
                Vector va = (Vector) v.get("value");
                value = (String) va.get(0);
                value = value + Common.showNursingFixes(am.getNursingFixes(footassessment.getId() != null ? footassessment.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                field.setValue(value);
                field.setTitle(title);
                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                field.setField_name(component.getField_name());
                field.setComponent_id(component.getId().intValue());
                fields.add(field);
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }


        return rows;
    }
    
    /****************************************************************************************************************/
    
    public RowData[] getAllSensoryAssessmentsForFlowchart(Collection<SensoryAssessmentVO> results, ProfessionalVO currentProfessional, boolean blnEmbedCode) throws ApplicationException {
        System.out.println("in getAllSensoryAssessmentFlowchart method ---------------------------------------------");
        /*****************************************************************/
        Iterator<SensoryAssessmentVO> itr = results.iterator();
        while(itr.hasNext())
        {
        	System.out.println("printing collections - " +itr.next());
        }
        System.out.println("value in ProfessionalVO is - " +currentProfessional.toString());
        System.out.println("value in blnEmbedCode ------" +blnEmbedCode);
        /****************************************************************/
    	RowData[] rows = null;
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        GUIServiceImpl gui = new GUIServiceImpl();
        AssessmentServiceImpl am = new AssessmentServiceImpl(language);
        //PatientProfileVO vo = new PatientProfileVO();

        rows = new RowData[results.size()];
        ComponentsVO comTMP = new ComponentsVO();
        comTMP.setFlowchart(Constants.SENSORY_ASSESSMENT);
        Collection<ComponentsVO> components = gui.getAllComponents(comTMP);
        int cnt = 2;
        for (ComponentsVO component : components) {
            if(component.getHide_flowchart() == 0){
            String title = Common.getLocalizedString(component.getLabel_key(),locale);
            int extend_count = 0;
            int count = 0;
            for (SensoryAssessmentVO sensoryassessment : results) {
                ProfessionalVO userVO = null;
                userVO = sensoryassessment.getProfessional();
                if (cnt == 2) {
                    List<FieldValues> fields = new ArrayList();
                    FieldValues f = new FieldValues();
                    f.setTitle("id");
                    String valuet2 = sensoryassessment.getId() + "";
                    f.setValue(valuet2);
                    fields.add(f);
                    FieldValues f2 = new FieldValues();
                    f2.setTitle(Common.getLocalizedString("pixalere.patientprofile.form.sensory_assessment",locale));
                    String valuet22 = (blnEmbedCode == true ? "<label class=\"hintanchor\" onMouseover=\"showhint('" + (userVO != null ? userVO.getFirstname() : "") + " " + (userVO != null ? userVO.getLastname() : "") + "', this, event, '250px')\">" : "") + sensoryassessment.getUser_signature() + (blnEmbedCode == true ? "</label>" : "") + (sensoryassessment.getId() != null ? Common.showNursingFixes(am.getNursingFixes(sensoryassessment.getId() != null ? sensoryassessment.getId().intValue() + "" : "0", "backdate"), blnEmbedCode,locale) : "");
                    f2.setValue(valuet22);
                    fields.add(f2);
                    rows[count] = new RowData();
                    rows[count].setUser_signature(sensoryassessment.getUser_signature());
                    rows[count].setId(sensoryassessment.getId() != null ? sensoryassessment.getId().intValue() : 0);
                    rows[count].setFields(fields);
                    rows[count].setHeader("pixalere.patientprofile.form.pageheader");
                    rows[count].setDeleted(sensoryassessment.getDeleted());
                    rows[count].setDelete_signature(sensoryassessment.getDelete_signature());
                    rows[count].setDelete_reason(sensoryassessment.getDeleted_reason());
                }
                List<FieldValues> fields = rows[count].getFields();
                //Get all columns for each component
                String value = "";
                FieldValues field = new FieldValues();
                Hashtable v = Common.getPrintableValue(sensoryassessment, userVO, component, blnEmbedCode, count,language);
                field.setDb_value((String) v.get("db_value"));
                Vector va = (Vector) v.get("value");
                value = (String) va.get(0);
                value = value + Common.showNursingFixes(am.getNursingFixes(sensoryassessment.getId() != null ? sensoryassessment.getId().intValue() + "" : "0", component.getId() + ""), blnEmbedCode,locale);
                field.setValue(value);
                field.setTitle(title);
                field.setAllow_edit(Common.allowFlowchartEdit(component.getAllow_edit().intValue(), currentProfessional, userVO, 0, false, Common.isOffline() ? 1 : 0));
                field.setField_name(component.getField_name());
                field.setComponent_id(component.getId().intValue());
                fields.add(field);
                rows[count].setFields(fields);
                count++;
            }
            cnt++;
            cnt = cnt + extend_count;//in case I have to split out a row into 2
            }
        }

        System.out.println("leaving method before return");
        return rows;
    }

    
    
    
    /****************************************************************************************************************/
    
    /**
     * Returns an PatientProfileVO object that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  primaryKey  the patient which which need retrieved
     *
     * @return      the patientProfile record
     * @see         PatientProfileVO
     */
    public PatientProfileVO getPatientProfile(PatientProfileVO tmpVO) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return (PatientProfileVO) patientDAO.findByCriteria(tmpVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * Returns an PatientProfileVO object that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  primaryKey  the patient which which need retrieved
     *
     * @return      the patientProfile record
     * @see         PatientProfileVO
     */
    public PatientProfileVO getPatientProfile(PatientProfileVO tmpVO, boolean showDeleted) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return (PatientProfileVO) patientDAO.findByCriteria(tmpVO, showDeleted);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    public BradenVO getBraden(BradenVO tmpVO) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return (BradenVO) patientDAO.findByCriteria(tmpVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    public BradenVO getBraden(BradenVO tmpVO, boolean showDeleted) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            return (BradenVO) patientDAO.findByCriteria(tmpVO, showDeleted);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * Returns an PatientProfileVO object that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  patient_id  the patient which which need retrieved
     * @param  professional_id the professional who did the tmp record.
     * @return      the patientProfile record
     * @see         PatientProfileVO
     */
    public PatientProfileVO getTemporaryPatientProfile(int patient_id, int professional_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            PatientProfileVO t = new PatientProfileVO();
            t.setPatient_id(new Integer(patient_id));
            t.setProfessional_id(new Integer(professional_id));
            t.setActive(new Integer(0));
            return (PatientProfileVO) patientDAO.findByCriteria(t);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    public BradenVO getTemporaryBraden(int patient_id, int professional_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            BradenVO t = new BradenVO();
            t.setPatient_id(new Integer(patient_id));
            t.setProfessional_id(new Integer(professional_id));
            t.setActive(new Integer(0));
            return (BradenVO) patientDAO.findByCriteria(t);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
        }
    }
    public void dropTMP(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            PatientProfileVO vo = new PatientProfileVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.delete(vo, purge_days);
            for(int id : deleted_ids){
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "patient_profiles", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void dropTMPInvestigation(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            DiagnosticInvestigationVO vo = new DiagnosticInvestigationVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteDiagnosticInvestigation(vo, purge_days);
            for(int id : deleted_ids){
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "diagnostic_investigation", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public  void  dropTMPAdvLimb(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbAdvAssessmentVO vo = new LimbAdvAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteLimbAdv(vo, purge_days);
            for(int id : deleted_ids){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "limb_assessment", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public  void  dropTMPBasicLimb(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbBasicAssessmentVO vo = new LimbBasicAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteLimbBasic(vo, purge_days);
            for(int id : deleted_ids){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "limb_assessment", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public  void  dropTMPUpperLimb(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbUpperAssessmentVO vo = new LimbUpperAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteLimbUpper(vo, purge_days);
            for(int id : deleted_ids){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "limb_assessment", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public  void  dropTMPExam(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            PhysicalExamVO vo = new PhysicalExamVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteExam(vo, purge_days);
            for(int id : deleted_ids){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "physical_exam", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public  void  dropTMPFoot(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            FootAssessmentVO vo = new FootAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteFoot(vo, purge_days);
            for(int id : deleted_ids){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "foot_assessment", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    
    /********************************************************************************************************/
    public  void  dropTMPSensory(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            SensoryAssessmentVO vo = new SensoryAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteSensory(vo, purge_days);
            for(int id : deleted_ids){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "foot_assessment", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    
    /********************************************************************************************************/
    
    
    
    public  void  dropTMPBraden(String patient_id, String user_id, Date purge_days) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            BradenVO vo = new BradenVO();
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(0));
            if (user_id != null) {
                vo.setProfessional_id(new Integer(user_id));
            }
            List<Integer> deleted_ids = patientDAO.deleteBraden(vo, purge_days);
            for(int id : deleted_ids){
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(vo.getProfessional_id(), vo.getPatient_id(), com.pixalere.utils.Constants.DROP_TMP_RECORD, "braden", id));
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    /** OFFLINE ONLY */
    public void removeOffline(OfflineVO off) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            patientDAO.deleteOffline(off);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileServiceImpl threw an error: " + e.getMessage(), e);
        }
    }
    /** OFFLINE ONLY */
    public void removePatientProfile(String patient_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            PatientProfileVO vo = new PatientProfileVO();
            vo.setPatient_id(new Integer(patient_id));
            patientDAO.delete(vo, null);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    /** OFFLINE ONLY */
    public void removeFootAssessment(String patient_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            FootAssessmentVO vo = new FootAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            patientDAO.deleteFoot(vo, null);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    /** OFFLINE ONLY */
    public void removeLimbBasicAssessment(String patient_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbBasicAssessmentVO vo = new LimbBasicAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            patientDAO.deleteLimbBasic(vo, null);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    /** OFFLINE ONLY */
    public void removeLimbAdvAssessment(String patient_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbAdvAssessmentVO vo = new LimbAdvAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            patientDAO.deleteLimbAdv(vo, null);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    /** OFFLINE ONLY */
    public void removeDiagnosticInvestigation(String patient_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            DiagnosticInvestigationVO vo = new DiagnosticInvestigationVO();
            vo.setPatient_id(new Integer(patient_id));
            patientDAO.deleteDiagnosticInvestigation(vo, null);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void removeLimbUpperAssessment(String patient_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            LimbUpperAssessmentVO vo = new LimbUpperAssessmentVO();
            vo.setPatient_id(new Integer(patient_id));
            patientDAO.deleteLimbUpper(vo, null);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    /** OFFLINE ONLY */
 public void removeExam(String patient_id) throws ApplicationException {
        try {
            PatientProfileDAO patientDAO = new PatientProfileDAO();
            PhysicalExamVO vo = new PhysicalExamVO();
            vo.setPatient_id(new Integer(patient_id));
            patientDAO.deleteExam(vo,null);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
        }
    }
    public void saveInvestigation(InvestigationsVO vo) throws ApplicationException {
        try {
            PatientProfileDAO ppDAO = new PatientProfileDAO();
            ppDAO.saveInvestigation(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }
    public void saveDiagnosticInvestigation(DiagnosticInvestigationVO vo) throws ApplicationException {
        try {
            PatientProfileDAO ppDAO = new PatientProfileDAO();
            ppDAO.saveDiagnosticInvestigation(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }
    public void removeInvestigation(InvestigationsVO p) {
        PatientProfileDAO dao = new PatientProfileDAO();
        dao.deleteInvestigation(p);
    }
    public void removeDiagnosticInvestigation(DiagnosticInvestigationVO p) {
        PatientProfileDAO dao = new PatientProfileDAO();
        dao.deleteDiagnosticInvestigation(p);
    }
    public Collection getAllPatientProfileArray(PatientProfileArraysVO assess) throws ApplicationException {
        try {
            PatientProfileDAO ppDAO = new PatientProfileDAO();
            return ppDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    public void savePatientProfileArray(PatientProfileArraysVO vo) throws ApplicationException {
        try {
            PatientProfileDAO ppDAO = new PatientProfileDAO();
            ppDAO.savePatientProfileArrays(vo);
        } catch (DataAccessException e) {
            throw new ApplicationException("saveNursingFixes has thrown an error:" + e.getMessage());
        }
    }
    public void removePatientArray(PatientProfileArraysVO p) {
        PatientProfileDAO dao = new PatientProfileDAO();
        dao.deletePatientProfileArrays(p);
    }
    public void saveExternalReferral(ExternalReferralVO profileVO) throws ApplicationException {
      try {
          PatientProfileDAO patientDAO = new PatientProfileDAO();
          patientDAO.insertExternalReferral(profileVO);
         
      } catch (DataAccessException e) {
          log.error(e.getMessage());
          throw new ApplicationException("PatientProfileManagerBD threw an error: " + e.getMessage(), e);
      }
  }
    public Collection<ExternalReferralVO> getAllExternalReferrals(ExternalReferralVO crit) throws ApplicationException {
      Collection<ExternalReferralVO> results = null;
 
      try {
          results = dao.findAllByCriteria(crit);
      } catch (DataAccessException e) {
          log.error("Error in PatientProfileManagerBD.getAllExternalReferrals(): " + e.toString(), e);
          throw new ApplicationException("Error in PatientProfileManagerBD.getAllExternalReferrals(): " + e.toString(), e);
      }
      return results;
  }
    /**
   * Returns an ExternalReferralVO object that can be shown on the screen.
   * <p>
   * This method always returns immediately, whether or not the
   * record exists.  The Object will return NULL if the record does not
   * exist.
   *
   * @param  primaryKey  the patient which which need retrieved
   *
   * @return      the externalReferral record
   * @see         ExternalReferralVO
   */
  public ExternalReferralVO getExternalReferral(ExternalReferralVO tmpVO) throws ApplicationException {
      try {
          PatientProfileDAO patientDAO = new PatientProfileDAO();
 
          return (ExternalReferralVO) patientDAO.findByCriteria(tmpVO);
      } catch (DataAccessException e) {
          throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(), e);
      }
  }
  public void removeExternalReferral(ExternalReferralVO p) {
        PatientProfileDAO dao = new PatientProfileDAO();
        dao.deleteExternalReferral(p);
    }
  
}
