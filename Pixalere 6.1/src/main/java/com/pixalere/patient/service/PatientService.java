package com.pixalere.patient.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.patient.bean.PatientAccountVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface PatientService {
    public PatientAccountVO getPatient(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void savePatient(@WebParam(name="updateRecord") PatientAccountVO updateRecord) throws ApplicationException;//Meditech interface only
    public int validatePatientId(@WebParam(name="userId") int userId,@WebParam(name="patientId") int patientId) throws ApplicationException;
    
    
}