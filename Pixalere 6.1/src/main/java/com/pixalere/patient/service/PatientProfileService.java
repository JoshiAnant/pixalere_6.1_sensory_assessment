package com.pixalere.patient.service;
import com.pixalere.common.ApplicationException;
import java.util.Collection;
import com.pixalere.patient.bean.*;
import com.pixalere.common.bean.OfflineVO;
import javax.jws.WebService;
import javax.jws.WebParam;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.patient.bean.ExternalReferralVO;
import com.pixalere.patient.bean.PhysicalExamVO;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface PatientProfileService {
    public PatientProfileVO getPatientProfile(@WebParam(name="crit") PatientProfileVO crit) throws ApplicationException;
    public void updatePatientProfile(@WebParam(name="updateRecord") PatientProfileVO updateRecord) throws ApplicationException;
    public Collection<PatientProfileVO> getAllPatientProfiles(@WebParam(name="crit") PatientProfileVO crit, @WebParam(name="limit") int limit) throws ApplicationException;
    public ExternalReferralVO getExternalReferral(@WebParam(name="crit") ExternalReferralVO crit) throws ApplicationException;
    public void saveExternalReferral(@WebParam(name="updateRecord") ExternalReferralVO updateRecord) throws ApplicationException;
     
    public void removeOffline(@WebParam(name="crit") OfflineVO crit) throws ApplicationException;
    
    public DiagnosticInvestigationVO getDiagnosticInvestigation(@WebParam(name="crit") DiagnosticInvestigationVO crit) throws ApplicationException;
    
    public FootAssessmentVO getFootAssessment(@WebParam(name="crit") FootAssessmentVO crit) throws ApplicationException;
    public void saveSensory(@WebParam(name="updateRecord") SensoryAssessmentVO updateRecord) throws ApplicationException;
    public void saveFoot(@WebParam(name="updateRecord") FootAssessmentVO updateRecord) throws ApplicationException;
    public LimbBasicAssessmentVO getLimbBasicAssessment(@WebParam(name="crit") LimbBasicAssessmentVO crit) throws ApplicationException;
    public LimbAdvAssessmentVO getLimbAdvAssessment(@WebParam(name="crit") LimbAdvAssessmentVO crit) throws ApplicationException;
    public LimbUpperAssessmentVO getLimbUpperAssessment(@WebParam(name="crit") LimbUpperAssessmentVO crit) throws ApplicationException;
    public PhysicalExamVO getExam(@WebParam(name="crit") PhysicalExamVO crit) throws ApplicationException;
    public void saveBasicLimb(@WebParam(name="updateRecord") LimbBasicAssessmentVO updateRecord) throws ApplicationException;
    public void saveAdvLimb(@WebParam(name="updateRecord") LimbAdvAssessmentVO updateRecord) throws ApplicationException;
public void saveUpperLimb(@WebParam(name="updateRecord") LimbUpperAssessmentVO updateRecord) throws ApplicationException;
    public BradenVO getBraden(@WebParam(name="crit") BradenVO crit) throws ApplicationException;
    public void saveBraden(@WebParam(name="updateRecord") BradenVO updateRecord) throws ApplicationException;
    public void savePatientProfileArray(@WebParam(name="updateRecord") PatientProfileArraysVO updateRecord) throws ApplicationException;
    public void saveInvestigation(@WebParam(name="updateRecord") InvestigationsVO updateRecord) throws ApplicationException;
    public void saveDiagnosticInvestigation(@WebParam(name="updateRecord") DiagnosticInvestigationVO updateRecord) throws ApplicationException;
    public void savePhysicalExam(@WebParam(name="updateRecord") PhysicalExamVO updateRecord) throws ApplicationException;
}