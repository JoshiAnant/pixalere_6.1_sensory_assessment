package com.pixalere.imaging;

import javax.swing.filechooser.FileFilter;
import java.io.File;


/**
 * <p>Title: Pixalere</p>
 * <p>Description: HealthCare Solution</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: WebMed Technologies</p>
 * @author Travis Morris
 * @version 1.0
 */



/* ImageFilter.java is a 1.4 example used by FileChooserDemo2.java. */
public class ImageFilter extends FileFilter {

    //Accept all directories and all gif, jpg, tiff, or png files.
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = ImageUtils.getExtension(f);
        if (extension != null) {
            if (extension.equalsIgnoreCase(ImageUtils.gif) ||
                extension.equalsIgnoreCase(ImageUtils.jpeg) ||
                extension.equalsIgnoreCase(ImageUtils.jpg)) {
                    return true;
            } else {
                return false;
            }
        }

        return false;
    }

    //The description of this filter
    public String getDescription() {
        return ".jpg, .jpeg, .gif";
    }
}

