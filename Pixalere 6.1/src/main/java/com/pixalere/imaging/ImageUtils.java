package com.pixalere.imaging;

import java.io.File;

/**
 * <p>Title: Pixalere</p>
 * <p>Description: HealthCare Solution</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: WebMed Technologies</p>
 * @author Travis Morris
 * @version 1.0
 */


/* Utils.java is a 1.4 example used by FileChooserDemo2.java. */
public class ImageUtils {
    public final static String jpeg = "jpeg";
    public final static String jpg = "jpg";
    public final static String gif = "gif";
    public final static String png = "png";

    /*
     * Get the extension of a file.
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }


}

