/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils.excel;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;
import com.pixalere.utils.MD5;
import java.util.Hashtable;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import java.util.Vector;
import com.pixalere.utils.Common;
import java.io.*;
import java.util.Locale;
import java.util.Collection;
import com.pixalere.common.service.ProductsServiceImpl;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ProductCategoryVO;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.DateCell;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.integration.service.IntegrationServiceImpl;
import com.pixalere.integration.bean.Patient;
import com.pixalere.integration.bean.Event;
import com.pixalere.integration.bean.Gender;

import javax.xml.datatype.XMLGregorianCalendar;

public class ExcelReader {

    public ExcelReader() {
    }

    /*2
     public void readText(String path) {

     //probably more efficient to grab all the patients in Pixalere then doing 1.7 million db calls to check :P
     PatientServiceImpl patientService = new PatientServiceImpl(language);
     PatientAccountVO pa = new PatientAccountVO();
     pa.setCurrent_flag(1);
     //pa.setTreatment_location_id(5);
     try {
     //System.out.println("Getting All Patients");
     Collection<PatientAccountVO> patients = patientService.getAllPatients(pa);
     //System.out.println("Got all Patients");

     WorkbookSettings ws = null;
     Workbook workbook = null;
     Sheet s = null;
     Cell rowData[] = null;
     int rowCount = '0';
     int columnCount = '0';
     DateCell dc = null;
     int totalSheet = 0;


     ws = new WorkbookSettings();
     ws.setLocale(new Locale("en", "EN"));
     FileInputStream fileInputStream = new FileInputStream(path);
     workbook = Workbook.getWorkbook(fileInputStream, ws);

     totalSheet = workbook.getNumberOfSheets();
     if (totalSheet > 0) {
     //System.out.println("Total Sheet Found:" + totalSheet);
     for (int j = 0; j < totalSheet; j++) {
     //System.out.println("Sheet Name:" + workbook.getSheet(j).getName());
     }
     }

     //Getting Default Sheet i.e. 0
     s = workbook.getSheet(0);

     //Reading Individual Cell
     getHeadingFromXlsFile(s);

     //Total Total No Of Rows in Sheet, will return you no of rows that are occupied with some data
     //System.out.println("Total Rows inside Sheet:" + s.getRows());
     rowCount = s.getRows();
     PatientServiceImpl pat = new PatientServiceImpl(language);
     //Total Total No Of Columns in Sheet
     //System.out.println("Total Column inside Sheet:" + s.getColumns());
     columnCount = s.getColumns();
     for (PatientAccountVO patient : patients) {
     Cell mrnCell = s.findCell(patient.getPhn());
     if (mrnCell != null) {//we Found a PHN
     //System.out.println("Found PHN: "+patient.getPhn());
     int row = mrnCell.getRow();
     int COLUMN = 1;//column 2
     Cell mrn = s.getCell(row, COLUMN);
     if (mrn != null) {
     System.out.print(" Found MRN: " + mrn.getContents());
     patient.setPhn2(mrn.getContents());
     pat.savePatient(patient);
     }
     }

     }
     //Reading Individual Row Content


     workbook.close();
     } catch (ApplicationException e) {
     e.printStackTrace();
     } catch (IOException e) {
     e.printStackTrace();
     } catch (BiffException e) {
     e.printStackTrace();
     }


     }*/
    //Returns the Headings used inside the excel sheet
    public void getHeadingFromXlsFile(Sheet sheet) {
        int columnCount = sheet.getColumns();
        for (int i = 0; i < columnCount; i++) {
            System.out.println(sheet.getCell(i, 0).getContents());
        }
    }
    private Workbook workbook = null;

    private void closeWorkbook() {
        workbook.close();
    }

    private Sheet contentReading(InputStream fileInputStream) {
        WorkbookSettings ws = null;
        Sheet s = null;

        int totalSheet = 0;

        try {
            ws = new WorkbookSettings();
            ws.setLocale(new Locale("en", "EN"));
            workbook = Workbook.getWorkbook(fileInputStream, ws);

            totalSheet = workbook.getNumberOfSheets();
            if (totalSheet > 0) {
                System.out.println("Total Sheet Found:" + totalSheet);
                for (int j = 0; j < totalSheet; j++) {
                    System.out.println("Sheet Name:" + workbook.getSheet(j).getName());
                }
            }

            //Getting Default Sheet i.e. 0
            s = workbook.getSheet(0);

            //Reading Individual Cell
            getHeadingFromXlsFile(s);

            return s;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * @todo refactor class, needs to be streamlined
     * @param file
     * @return
     */
    public Vector readPatients(InputStream file) {
        String dob_format = "yyyddmm";
        //column orders
        int PHN = 0;
        int MRN = 1;
        int LASTNAME = 2;
        int FIRSTNAME = 3;
        int DOB = 4;
        int GENDER = 5;
        int ALLERGIES = 6;
        int STATUS = 7;
        Sheet sheet = contentReading(file);
        if(sheet == null){ return null; }
        //Total Total No Of Rows in Sheet, will return you no of rows that are occupied with some data
        //System.out.println("Total Rows inside Sheet:" + s.getRows());
        int rowCount = sheet.getRows();

        //Total Total No Of Columns in Sheet
        //System.out.println("Total Column inside Sheet:" + s.getColumns());
        int columnCount = sheet.getColumns();

        Vector<PatientAccountVO> prodVect = new Vector<PatientAccountVO>();

        //Reading Individual Row Content
        Cell rowData[] = null;
        columnCount = 6;   //should be only 8 columns.
        for (int i = 1; i < rowCount; i++) {
            //Get Individual Row
            rowData = sheet.getRow(i);
            
            PatientAccountVO patient = new PatientAccountVO();
            for (int j = 0; j < columnCount; j++) {

                String value = rowData[j].getContents();
                //System.out.println(value);
                if (j == PHN) {
                    patient.setPhn(value);
                }
                if (j == MRN) {
                    patient.setPhn2(value);
                } else if (j == LASTNAME) {
                   /* String[] names = value.split(" ");
                    if(names.length >=2){
                        patient.setFirstName(names[0]);
                        patient.setLastName(names[1]);
                    }else{*/
                        patient.setLastName(value);
                    //}
                } else if (j == FIRSTNAME) {
                    patient.setFirstName(value);
                } else if (j == DOB) {
                    if (value != null && !value.equals("")) {

                        try {
                            java.util.Date d = PDate.getDateFromFormatted(value, dob_format);
                            if (d != null) {
                                patient.setDob(value);
                            }
                        } catch (Exception e) {

                        }
                    }
                } else if (j == GENDER) {

                    int gend = 0;
                    if (value.equals(Constants.MALE) || (value).equals(Constants.MALE_SHORT)) {
                        gend = Integer.parseInt(Common.getConfig("male"));
                        patient.setGender(gend);
                    } else if (value.equals(Constants.FEMALE) || (value).equals(Constants.FEMALE_SHORT)) {
                        gend = Integer.parseInt(Common.getConfig("female"));
                        patient.setGender(gend);
                    } else if (value.equals(Constants.UNKNOWN) || (value).equals(Constants.UNKNOWN_SHORT)) {
                        gend = Integer.parseInt(Common.getConfig("unknownGender"));
                        patient.setGender(gend);
                    } else if ((value).equals(Constants.TRANSGENDER) || (value).equals(Constants.TRANSGENDER_SHORT)) {
                        gend = Integer.parseInt(Common.getConfig("transGender"));
                        patient.setGender(gend);
                    }
                    prodVect.add(patient);

                } /*else if (j == ALLERGIES) {
                    patient.setAllergies(value);
                } else if (j == STATUS) {
                    patient.setAction(value);
                    prodVect.add(patient);
                }*/
            }
            //save new product...
        }
        closeWorkbook();
        return prodVect;
    }
    private final int NAME = 2;
    private final int UNITS = 7;
    private final int PRICE = 3;
    private final int STOCK_ID = 0;
    private final int URL = 6;
    private final int COMPANY = 4;
    private final int DESCRIPTION = 5;
    private final int CATEGORY = 1;

    public Vector readProducts(InputStream fileInputStream) {

        Sheet sheet = contentReading(fileInputStream);
        //Total Total No Of Rows in Sheet, will return you no of rows that are occupied with some data
        //System.out.println("Total Rows inside Sheet:" + s.getRows());
        int rowCount = sheet.getRows();

        //Total Total No Of Columns in Sheet
        //System.out.println("Total Column inside Sheet:" + s.getColumns());
        int columnCount = sheet.getColumns();

        Vector<ProductsVO> prodVect = new Vector<ProductsVO>();
        ProductsServiceImpl pservice = new ProductsServiceImpl();
        //Reading Individual Row Content
        Cell rowData[] = null;
        columnCount = 8;   //should be only 8 columns.
        for (int i = 1; i < rowCount; i++) {
            //Get Individual Row
            rowData = sheet.getRow(i);

            ProductsVO product = new ProductsVO();
            for (int j = 0; j < columnCount; j++) {
                try {
                    String value = rowData[j].getContents();

                    if (j == STOCK_ID) {
                        product.setUid(value);

                    }

                    if (j == NAME) {
                        value = value.replaceAll("[^a-zA-Z0-9().,:' ]+", "");
                        value = value.replaceAll("'", "");
                        product.setTitle(value);
                    } else if (j == PRICE) {
                        String units = rowData[UNITS].getContents();
                        try {
                            double num = 1.0;
                            if (units != null && !units.equals("")) {
                                num = Double.parseDouble(units);
                            }

                            if (num > 1.0) {
                                double price = Double.parseDouble(value);
                                product.setCost((price / num));
                            } else {
                                System.out.println("blah: " + num);
                                double price = Double.parseDouble(value);
                                product.setCost(price);
                            }
                        } catch (NumberFormatException e) {
                            //e.printStackTrace();
                            product.setCost(0.0);

                        }

                    } else if (j == COMPANY) {
                        product.setCompany(value);
                    } else if (j == DESCRIPTION) {
                        //product.setDescription(value);
                    } else if (j == URL) {
                        product.setUrl(value);
                    } else if (j == UNITS) {
                        prodVect.add(product);
                    } else if (j == CATEGORY) {
                        ProductCategoryVO c = new ProductCategoryVO();
                        c.setTitle(value);
                        product.setCategory(c);
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    //ignore column if it doesn't exist.
                }

            }

            //save new product...
        }
        closeWorkbook();
        return prodVect;
    }

    public Vector readProfessional(InputStream fileInputStream) {

        Sheet sheet = contentReading(fileInputStream);
        //Total Total No Of Rows in Sheet, will return you no of rows that are occupied with some data
        //System.out.println("Total Rows inside Sheet:" + s.getRows());
        int rowCount = sheet.getRows();

        //Total Total No Of Columns in Sheet
        //System.out.println("Total Column inside Sheet:" + s.getColumns());
        int columnCount = sheet.getColumns();

        Vector<ProfessionalVO> prodVect = new Vector<ProfessionalVO>();
        //Reading Individual Row Content
        Cell rowData[] = null;
        columnCount = 18;   //should be only 18 columns.
        
        for (int i = 1; i < rowCount; i++) {
            //Get Individual Row
            rowData = sheet.getRow(i);

            ProfessionalVO prf = new ProfessionalVO();
            for (int j = 0; j < columnCount; j++) {
                try {
                    String value = rowData[j].getContents();

                    if (j == 0) {//first name
                        prf.setFirstname(value);
                        prf.setFirstname_search(Common.stripName(value));
                    }else if(j == 1){
                        prf.setLastname(value);
                        prf.setLastname_search(Common.stripName(value));
                    }else if(j == 2){
                        prf.setUser_name(value);
                    }else if(j == 3){//position
                        prf.setAnswer_one(value);//temp placement
                    }else if(j == 4){
                        prf.setEmail(value);
                    }else if(j == 5){
                        prf.setPager(value);
                    }else if(j == 6){
                        prf.setEmployeeid(value);
                    }else if(j == 7){
                        prf.setPassword((MD5.hash(value)).toLowerCase());
                    }else if(j == 8){//treatent
                        prf.setAnswer_two(value);//temp placement
                    }else if(j == 9){
                        prf.setAccess_allpatients(value.equals("1")?1:0);
                    }else if(j == 10){
                        prf.setAccess_patients(value.equals("1")?1:0);
                    }else if(j == 11){
                        prf.setAccess_uploader(value.equals("1")?1:0);
                    }else if(j == 12){
                        prf.setAccess_viewer(value.equals("1")?1:0);
                    }else if(j == 13){
                        prf.setAccess_admin(value.equals("1")?1:0);
                    }else if(j == 14){
                        prf.setAccess_create_patient(value.equals("1")?1:0);
                    }else if(j == 15){
                        prf.setAccess_assigning(value.equals("1")?1:0);
                    }else if(j == 16){
                        prf.setTraining_flag(value.equals("1")?1:0);
                    }else if(j == 17){//remote flag
                        //prf.set(value.equals("1")?1:0);
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    //ignore column if it doesn't exist.
                }catch (ApplicationException e) {
                    //ignore column if it doesn't exist.
                }

            }
            prf.setLocked(0);
            prf.setInvalid_password_count(0);
            prf.setAccount_status(1);
            prf.setNew_user(1);
            prodVect.add(prf);
            //save new product...
        }
        closeWorkbook();
        return prodVect;
    }
}
/* RUN
 java  -cp ../lib/jxl.jar:.:../lib/commons-lang-2.4.jar:../lib/commons-beanutils-1.7.0.jar:../../main/resources/lib/servlet2_3.jar:../lib/mysql-connector-java-5.1.14-bin.jar:../lib/log4j-1.2.15.jar:../lib/db-ojb-1.0.4-tools.jar:../lib/db-ojb-1.0.4.jar:../lib/commons-resources.jar:../lib/commons-collections-3.2.1.jar:../lib/commons-digester-1.7.jar:../lib/commons-logging-1.1.1.jar:../lib/commons-pool-1.2.jar:../lib/commons-dbcp-1.2.1.jar:../lib/struts.jar com.pixalere.utils.FixPatients

 */
