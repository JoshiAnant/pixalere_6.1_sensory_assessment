/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.utils;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
/**
 * Get the last X lines of file.
 *
 * @since 5.0.1
 * @author travis
 */
public class TailUtil {
    public TailUtil(){}
    public String tail( File file, int lines_count) {
    try {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String lines = "";
    
    for (String line=reader.readLine(); line != null; line=reader.readLine()) {
        
        lines= lines+"\n"+line;
    }
    return lines;
    } catch( java.io.FileNotFoundException e ) {
        e.printStackTrace();
        return null;
    } catch( java.io.IOException e ) {
        e.printStackTrace();
        return null;
    }
}
}
