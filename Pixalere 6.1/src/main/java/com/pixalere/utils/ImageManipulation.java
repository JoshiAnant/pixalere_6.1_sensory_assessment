package com.pixalere.utils;
import java.util.HashSet;
import java.util.Set;
import java.util.Collection;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import org.apache.struts.upload.FormFile;
import com.pixalere.common.ApplicationException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import java.util.Date;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.jpeg.JpegSegmentReader;
import com.drew.imaging.jpeg.JpegSegmentData;
import com.drew.imaging.jpeg.JpegSegmentType;
import com.drew.metadata.exif.ExifSubIFDDescriptor;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.makernotes.RicohMakernoteDescriptor;
import com.drew.metadata.exif.makernotes.RicohMakernoteDirectory;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.util.Iterator;
import java.util.Locale;
import javax.imageio.metadata.*;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.ImageWriteParam;
import javax.imageio.IIOImage;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.ImageIcon;

/**
 * Resizes and crops jpeg image files to match new size.
 * Uses the com.sun.image.codec.jpeg package shipped
 * by Sun with Java 2 Standard Edition.
 */
public class ImageManipulation {
    //default constructor

    String patient_id = "";
    String signature = "";
    
    String path_absolute = "";
    String assessment_id = "";
    String path = "";
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImageManipulation.class);

    public ImageManipulation() {

    }
    public ImageManipulation(String path_absolute){
        
        this.path_absolute = path_absolute;
    }
    public ImageManipulation(String patient_id, String signature,  String path_absolute, String assessment_id) {

        this.patient_id = patient_id;
        this.signature = signature;
        
        this.path_absolute = path_absolute;
        this.assessment_id = assessment_id;
        path = path_absolute + "/" + patient_id + "/" + assessment_id;
    }
    public void createFolders(String folder) throws ApplicationException {
        File pathoff = new File(path_absolute + "/");
        if (!pathoff.exists()) {
            try {
                pathoff.mkdir();
            } catch (Exception e) {
                throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);

            }
        }
        File path = new File(path_absolute + "/" + folder);
        if (!path.exists()) {
            try {
                path.mkdir();
            } catch (Exception e) {
                throw new ApplicationException("Error in ImageManipulatin.createFolders(): " + e.toString(), e);

            }
        }
    }
    public void createFolders() throws ApplicationException {
        File pathoff = new File(path_absolute + "/");
        if (!pathoff.exists()) {
            try {
                pathoff.mkdir();
            } catch (Exception e) {
                throw new ApplicationException("Error in ImageManipulation.createFolders: " + e.toString(), e);

            }
        }
        File path = new File(path_absolute + "/" + patient_id);
        if (!path.exists()) {
            try {
                path.mkdir();
            } catch (Exception e) {
                throw new ApplicationException("Error in ImageManipulatin.createFolders(): " + e.toString(), e);

            }
        }
        File path2 = new File(path_absolute + "/" + patient_id + "/" + assessment_id);

        if (!path2.exists()) {
            try {
                path2.mkdir();
            } catch (Exception e) {
                throw new ApplicationException("Error in ImageManipulation(): " + e.toString(), e);
            }
        }
    }
    /**
     * Resize uploaded image
     *
     * @param file
     * @param image_name
     * @param professional_id
     * @return
     * @throws ApplicationException
     */
    public String processUploadedFile(FormFile file, String image_name, String folder) throws ApplicationException {
        OutputStream bos = null;
        InputStream stream = null;
        try {
            createFolders(folder);
            //save original


            //compress image first
            stream = file.getInputStream();
            bos = new FileOutputStream(path_absolute+"/"+folder + "/" + image_name);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            bos.close();
            //close the stream
            stream.close();

            
        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                File f = new File(path + "/" + image_name);
                if (f.exists()) {
                    f.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Not a Valid Image Format (Please use JPEG, GIF or PNG only): " + ex.getMessage();

        } finally {

            try {
                if (bos != null) {

                    bos.close();
                }
                if (stream != null) {

                    stream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";

    }
    /**
     * Read the metadata from the file.
     */
    public void readMetadata(File file){
        try{
            //path_absolute = "/opt/pixalere_storage/";
            path_absolute = "/home/ricoh/";
            WoundAssessmentLocationServiceImpl wservice = new WoundAssessmentLocationServiceImpl();
            AssessmentImagesServiceImpl aservice = new AssessmentImagesServiceImpl();
            Metadata metadata = ImageMetadataReader.readMetadata(file);
            String barcode = "";
            for (Directory directory : metadata.getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    if(tag.getTagName().equals("User Comment")){
                        if(tag.getDescription().equals("")){
                            //throw email/error about no barcode info
                        }else{
                            barcode = tag.getDescription();
                            barcode = barcode.replaceAll("GCM_TAG", "");
                            break;
                        }
                    }else{
                       // System.out.println("["+tag.getTagName()+"]");
                    }
                    
                }
            }
            //parse barcode string
            //sample U:7,P:1,C:test,As:140,A:24
            String user_id = "";
            patient_id = "";
            String customer = "";
            String assess_id = "";
            String alpha_id = "";
            System.out.println("Barcode: "+barcode);
            if(barcode.indexOf("U:")!=-1){
                int index = barcode.indexOf("U:");
                String tmp = barcode.substring(index+2);
                //System.out.println(tmp);
                int index2 = barcode.indexOf(",");
                String id = tmp.substring(0,index2);
                if(id!=null && !id.equals("")){
                    user_id = id;
                }
            }
            if(barcode.indexOf("P:")!=-1){
                int index = barcode.indexOf("P:");
                String tmp = barcode.substring(index+2);
                //System.out.println(tmp);
                int index2 = tmp.indexOf(",");
                String id = tmp.substring(0,index2);
                if(id!=null && !id.equals("")){
                    patient_id = id;
                }
            }
            if(barcode.indexOf("C:")!=-1){
                int index = barcode.indexOf("C:");
                String tmp = barcode.substring(index+2);
                //System.out.println(tmp);
                int index2 = tmp.indexOf(",");
                String id = tmp.substring(0,index2);
                if(id!=null && !id.equals("")){
                    customer = id;
                }
            }
            if(barcode.indexOf("As:")!=-1){
                int index = barcode.indexOf("As:");
                String tmp = barcode.substring(index+3);
                //System.out.println(tmp);
                int index2 = tmp.indexOf(",");
                String id = tmp.substring(0,index2);
                if(id!=null && !id.equals("")){
                    assess_id = id;
                }
            }
            if(barcode.indexOf("A:")!=-1){
                int index = barcode.indexOf("A:");
                String tmp = barcode.substring(index+2);
               // System.out.println(tmp);
               // int index2 = barcode.indexOf(",");
                String id = tmp;
                if(id!=null && !id.equals("")){
                    alpha_id = id;
                }
            }
            System.out.println("patient_id: "+patient_id);
            System.out.println("alpha_id: "+alpha_id);
            System.out.println("assess_id: "+assess_id);
            System.out.println("user_id: "+user_id);
            path_absolute=path_absolute+""+customer;
            if(!alpha_id.equals("") && !patient_id.equals("") && !assess_id.equals("") && !user_id.equals("")){
                AssessmentImagesVO img = new AssessmentImagesVO();
                img.setPatient_id(new Integer(patient_id));
                img.setActive(1);
                img.setAlpha_id(new Integer(alpha_id));
                img.setAssessment_id(new Integer(assess_id));
                Collection<AssessmentImagesVO> imgs = aservice.getAllImages(img);
                if(imgs!=null){
                    img.setWhichImage(imgs.size()+1);
                   
                }else{
                    img.setWhichImage(1);
                }
                img.setImages(file.getName());
                WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
                wal.setId(new Integer(alpha_id));
                WoundAssessmentLocationVO alpha = wservice.getAlpha(wal);
               // System.out.println("alpha"+alpha);
                if(alpha!=null){
                    img.setWound_id(alpha.getWound_id());
                    img.setWound_profile_type_id(alpha.getWound_profile_type_id());
                    assessment_id=assess_id;
                    createFolders();
                    if(file.renameTo(new File("/opt/pixalere_storage/"+customer+"/"+patient_id+"/"+assess_id+"/"+file.getName()))){
                         System.out.println("File is moved successful!");
                         aservice.saveImage(img);
                    }else{
                         System.out.println("File is failed to move!");
                         //move file to error folder
                         
                         file.renameTo(new File("/home/ricoh/errors/"));
                    }
                }
            }
            //parse barcode string
            //pass customer code.. customer 
        }catch(IOException e){e.printStackTrace();}catch(Exception e){e.printStackTrace();}
    }
    private   void displayMetadata(Node root) {
        displayMetadata(root, 0);
    }

    private void indent(int level) {
        for (int i = 0; i < level; i++)
            System.out.print("    ");
    }

    private void displayMetadata(Node node, int level) {
        // print open tag of element
        indent(level);
        //System.out.print("<" + node.getNodeName());
        NamedNodeMap map = node.getAttributes();
        if (map != null) {

            // print attribute values
            int length = map.getLength();
            for (int i = 0; i < length; i++) {
                Node attr = map.item(i);
                //System.out.print(" " + attr.getNodeName() +
                //                 "=\"" + attr.getNodeValue() + "\"");
            }
        }

        Node child = node.getFirstChild();
        if (child == null) {
            // no children, so close element and return
          //  System.out.println("/>");
            return;
        }

        // children, so close current tag
       // System.out.println(">");
        while (child != null) {
            // print children recursively
            displayMetadata(child, level + 1);
            child = child.getNextSibling();
        }

        // print close tag of element
        indent(level);
       // System.out.println("</" + node.getNodeName() + ">");
    }
    /**
     * Resize uploaded image
     *
     * @param file
     * @param image_name
     * @param professional_id
     * @return
     * @throws ApplicationException
     */
    public String processUploadedFile(FormFile file, String image_name, int professional_id) throws ApplicationException {
        OutputStream bos = null;
        InputStream stream = null;
        try {
            createFolders();
            //save original


            //compress image first
            stream = file.getInputStream();
            bos = new FileOutputStream(path + "/" + image_name);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            bos.close();
            //close the stream
            stream.close();

            //compress image first

            ImageIcon image = new ImageIcon(path + "/" + image_name);
            int width = 640;
            try{
                //If Image is less then width we resize to, grab next lowest image width.
                if(image.getIconWidth()<Integer.parseInt(Common.getConfig("image_width"))){
                    for(int img_width : Constants.IMG_WIDTHS){
                        if(Integer.parseInt(Common.getConfig("image_width")) > img_width){
                            width = img_width;
                            break;
                        }
                    }
                }else{
                    width = Integer.parseInt(Common.getConfig("image_width"));
                }
            }catch(NumberFormatException e){
                //not a valid integer.
            }
            ImageIcon thumb = new ImageIcon(
                    image.getImage().getScaledInstance(
                            width, -1, Image.SCALE_FAST)
            );
            String error = saveFile(thumb, path + "/" + image_name);
            if (!error.equals("")) {
                File f = new File(path + "/" + image_name);
                if (f.exists()) {
                    f.delete();
                }
                return error;
            }
            addText(image_name, professional_id);

        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                File f = new File(path + "/" + image_name);
                if (f.exists()) {
                    f.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "Not a Valid Image Format (Please use JPEG, GIF or PNG only): " + ex.getMessage();

        } finally {

            try {
                if (bos != null) {

                    bos.close();
                }
                if (stream != null) {

                    stream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";

    }
    /**
     * Save Image
     * @param thumb
     * @param pathImage
     * @return
     */
    private String saveFile(ImageIcon thumb, String pathImage) {
        if (thumb != null) {
            BufferedImage bi = new BufferedImage(
                    thumb.getIconWidth(),
                    thumb.getIconHeight(),
                    BufferedImage.TYPE_INT_RGB
            );
            Graphics g = bi.getGraphics();
            g.drawImage(thumb.getImage(), 0, 0, null);

            thumb = null;
            g = null;

            try {


                //find jpeg writer
                ImageWriter writer = null;
                Iterator iter = ImageIO.getImageWritersByFormatName("JPG");

                if (iter.hasNext()) {
                    writer = (ImageWriter) iter.next();
                }
                //set compression quality
                ImageWriteParam iwparam = writer.getDefaultWriteParam();
                iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                //iwparam.setCompressionType("JPEG");
                iwparam.setCompressionQuality(0.8f);


                //Prepare  output file
                //if(file.exists()){
                //    file.delete();
                //}
                File outputFile = new File(pathImage);
                FileOutputStream fos = new FileOutputStream(outputFile, false);
                ImageOutputStream ios = ImageIO.createImageOutputStream(fos);
                writer.setOutput(ios);


                //write compressed image
                if (bi == null) {
                    //System.out.println("Image is NULL.  Likely an invalid Image format.  Please ensure your uploading a JPEG, PNG, or GIF.");
                    return "Image is NULL.  Likely an invalid Image format.  Please ensure your uploading a JPEG, PNG, or GIF.";
                }
                writer.write(null, new IIOImage(bi, null, null), iwparam);
                ios.flush();
                fos.close();
                writer.dispose();
                ios.close();


                //ImageIO.write(bi, "jpg", new File(path));
            }
            catch (FileNotFoundException e) {
                return "File Not Found: " + path;

            } catch (IOException ioe) {
                return ioe.getMessage();
            }
        }
        return "";
    }
    /**
     * Read the uploaded image, and add the signature, and timestamp to the bottom.
     * 
     * @param image_name
     * @param professional_id
     * @throws ApplicationException
     */
    private void addText(String image_name, int professional_id) throws ApplicationException {
        try {
            BufferedImage image = ImageIO.read(new File(path + "/" + image_name));

            int x1 = 5;
            int x2 = image.getWidth() - 12;
            int y1 = image.getHeight() - 30;
            int y2 = image.getHeight();
            int transparency = Integer.parseInt("50");
            // do the highlighting
            Graphics graphics = image.getGraphics();
            Color color = new Color(0, 0, 255, 255 * transparency / 100);
            graphics.setColor(color);
            graphics.fillRect(x1, y1, x2, 20);
            graphics.setColor(Color.white);
            graphics.setFont(new Font("Dialog", Font.BOLD, 14));

            graphics.drawString(signature, image.getWidth() - 265,
                    image.getHeight() - 16);
            // save modified image
            String format = "JPG";

            ImageIO.write(image, format, new File(path + "/" + image_name));
        } catch (Exception ex) {
            throw new ApplicationException("Error in addtext: " + ex.toString(), ex);
        }
    }
    /*private void displayMetadata(Node root) {
        displayMetadata(root, 0);
    }

    private void indent(int level) {
        for (int i = 0; i < level; i++)
            System.out.print("    ");
    }

    private void displayMetadata(Node node, int level) {
        // print open tag of element
        indent(level);
        System.out.print("<" + node.getNodeName());
        NamedNodeMap map = node.getAttributes();
        if (map != null) {

            // print attribute values
            int length = map.getLength();
            for (int i = 0; i < length; i++) {
                Node attr = map.item(i);
                System.out.print(" " + attr.getNodeName() +
                                 "=\"" + attr.getNodeValue() + "\"");
            }
        }

        Node child = node.getFirstChild();
        if (child == null) {
            // no children, so close element and return
            System.out.println("/>");
            return;
        }

        // children, so close current tag
        System.out.println(">");
        while (child != null) {
            // print children recursively
            displayMetadata(child, level + 1);
            child = child.getNextSibling();
        }

        // print close tag of element
        indent(level);
        System.out.println("</" + node.getNodeName() + ">");
    }*/
}


