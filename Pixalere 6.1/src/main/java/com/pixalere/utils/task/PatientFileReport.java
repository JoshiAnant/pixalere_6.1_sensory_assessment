/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.utils.task;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.pixalere.reporting.ReportBuilder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Collection;
import com.pixalere.utils.Constants;
import com.pixalere.common.service.PatientReportService;
import com.pixalere.common.bean.PatientReportVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.reporting.ReportingManager;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.service.WoundAssessmentLocationServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.guibeans.PatientList;
import javax.servlet.ServletContext;
/**
 * PatientFileReport quartz job, run's every 15 minutes and generates a PDF
 * for import into Meditech PCI.
 *
 * @since 5.0
 * @author travis morris
 */
public class PatientFileReport implements Job {
    
     public void execute(JobExecutionContext context)  throws JobExecutionException {
         try {
            ServletContext cont = (ServletContext)context.getScheduler().getContext().get("servletContext");
            Collection<PatientReportVO> results = getAllUpdatedPatients();
            ProfessionalServiceImpl usermanager = new ProfessionalServiceImpl();
            PatientReportService prservice = new PatientReportService();
            PatientServiceImpl managerpa = new PatientServiceImpl(Constants.ENGLISH);
            for (PatientReportVO patient : results) {

                ProfessionalVO user = usermanager.getProfessional(7);
                ReportingManager manager = new ReportingManager(user, null, null, "", "", "wound_management_plan", "", "", "", null, "", null, "", null, patient.getPatient_id()+"", cont, 1);
                    
               // ReportingManager rm = new ReportingManager(user,patient.getPatient_id(),context,1);//,"wound_management_plan",Constants.CLIENTSIDE_REPORT);

                String filename = "" + patient.getPatient_id() + ".pdf";
                PatientAccountVO patientVO = managerpa.getPatient(patient.getPatient_id());
                manager.callPatientFileReport(filename, patient.getPatient_id()+"");
                managerpa.dumpPatient(patientVO, filename);
                patient.setActive(0);
                prservice.savePatientReportUpdate(patient);

            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(0);

        }


    }
      public Collection<PatientReportVO> getAllUpdatedPatients() {
        PatientReportService prservice = new PatientReportService();
        try {


            Collection<PatientReportVO> r = prservice.getAllPatientChanges();
            return r;
        } catch (ApplicationException e) {
            e.printStackTrace();
            return null;
        }
    }
}
