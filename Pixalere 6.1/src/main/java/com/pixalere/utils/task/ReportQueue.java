/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils.task;

import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.mail.SendMailUsingAuthentication;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.reporting.ReportingManager;
import com.pixalere.reporting.bean.DashboardByEtiologyVO;
import com.pixalere.reporting.bean.DashboardByLocationVO;
import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.ReportQueueEtiologyVO;
import com.pixalere.reporting.bean.ReportQueueTreatmentsVO;
import com.pixalere.reporting.bean.ReportQueueVO;
import com.pixalere.reporting.bean.ReportQueueWoundTypeVO;
import com.pixalere.reporting.bean.ReportScheduleVO;
import com.pixalere.reporting.bean.ScheduleConfigVO;
import com.pixalere.reporting.bean.ScheduleEtiologyVO;
import com.pixalere.reporting.bean.ScheduleLocationVO;
import com.pixalere.reporting.service.DashboardReportService;
import com.pixalere.reporting.service.ReportSchedulerService;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.utils.Common;
import static com.pixalere.utils.Common.DEFAULT_LANGUAGE_CONF;
import com.pixalere.utils.FiscalQuarter;
import com.pixalere.utils.PDate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * DashboardReport quartz job. Run at the first of each month, and quarter and
 * emailed out to the defined users.
 *
 * @since 5.0
 * @author travis
 */
public class ReportQueue implements Job {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportQueue.class);

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        try {
            
            ServletContext ctx = (ServletContext)context.getScheduler().getContext().get("servletContext");
            
            ReportingServiceImpl rservice = new ReportingServiceImpl();
            
            String locale = Common.getConfig(DEFAULT_LANGUAGE_CONF);
            Integer language = Common.getLanguageIdFromLocale(locale);
            
            ReportBuilder rbuilder = new ReportBuilder(ctx);
            Collection<DashboardReportVO> c = rservice.getDashboardReports(new DashboardReportVO());
            Collection<ReportQueueVO> queue = rservice.getReportQueues(new ReportQueueVO());
            Date start_date = null;
            Date end_date = null;
            String groupby = null;
            String breakdown = null;

            String full_assessment = null;
            String goal_days = null;
            String goals = null;
            //String print_patient_details = parameterForm.getPrintPatientDetails();
            //String apply_date_range = parameterForm.getPrintPatientDetails2();
            String includeTIP = null;
            
            String patient_id = null;
            for (ReportQueueVO q : queue) {
                String report_type = q.getReport_type();
                Integer report_language = 
                        (q.getUser_language() != null)? 
                        q.getUser_language(): 
                        language;
                
                rbuilder.setLanguage(report_language);
                Collection<ReportQueueEtiologyVO> et = q.getEtiologies();
                Collection<ReportQueueTreatmentsVO> loc = q.getTreatments();
                Collection<ReportQueueWoundTypeVO> types = q.getTypes();
                start_date = q.getStart_date();
                end_date = q.getEnd_date();
                if(et == null){
                    ReportQueueEtiologyVO eeVO = new ReportQueueEtiologyVO();
                    eeVO.setReport_id(q.getId());
                    et = rservice.getReportQueueEtiologies(eeVO);
                    ReportQueueTreatmentsVO ttVO = new ReportQueueTreatmentsVO();
                    ttVO.setReport_id(q.getId());
                    loc = rservice.getReportQueueTreatments(ttVO);
                    ReportQueueWoundTypeVO wVO = new ReportQueueWoundTypeVO();
                    wVO.setReport_id(q.getId());
                    types = rservice.getReportQueueTypes(wVO);
                }
                if (q.getReport_type().equals("report_dashboard") || q.getReport_type().equals("report_dashboard_4quarters")) {
                    DashboardReportVO dashboardReportVO = new DashboardReportVO();
                    dashboardReportVO.setEmail(q.getEmail());
                    
                    Collection<DashboardByEtiologyVO> etiologies = new ArrayList();
                    if (et != null) {
                        for (ReportQueueEtiologyVO e : et) {
                            DashboardByEtiologyVO etiology = new DashboardByEtiologyVO();
                            etiology.setDashboard_report_id(-1);
                            etiology.setEtiology_id(e.getLookup_id());
                            etiologies.add(etiology);
                        }
                    }
                    dashboardReportVO.setEtiologies(etiologies);
                    Collection<DashboardByLocationVO> treatments = new ArrayList();
                    if (loc != null) {
                        for (ReportQueueTreatmentsVO e : loc) {
                            DashboardByLocationVO l = new DashboardByLocationVO();
                            l.setDashboard_report_id(-1);
                            l.setTreatment_location_id(e.getTreatment_location_id());
                            treatments.add(l);
                        }
                    }
                    dashboardReportVO.setTreatmentLocations(treatments);
                    dashboardReportVO.setWound_type("All");
                    // OLD generate PDF.
                    // rbuilder.generateDashboard(dashboardReportVO, quarters);
                    
                    // New Dashboard PDF
                    DashboardReportService dashboardService = new DashboardReportService(ctx, report_language);
                    rservice.removeReportQueue(q);//clear before running it, otherwise we get duplicates if its a long report
                    List<FiscalQuarter> quarters;
                    boolean isExtended = false;
                    // Old dashboard - 8 quarters - 2 years - landscape
                    if (q.getReport_type().equals("report_dashboard")) {
                        quarters = Common.getQuarters(7, start_date);
                        dashboardService.generateDashboardMail(dashboardReportVO, quarters);
                    } else {
                        // Extended dashboard - Get the Custom 5 columns dates from Extra Filter
                        quarters = PDate.getUnserializedCustomDatesDashboard(q.getExtra_filter());
                        boolean displayLocations = q.getDisplay_locations() == null
                                ? false
                                : q.getDisplay_locations() == 1;
                        dashboardService.generateExtendedDashboardMail(dashboardReportVO, quarters, displayLocations, q.getCustom_title());
                    }
                    
                    //rservice.removeReportQueue(q);

                } else if(q.getReport_type().equals("report_patient_outcomes")){
                    // FIXME: Remove wound tracking code
                    String[] treatmentLocationArr = null;
                    if (loc != null ) {
                        treatmentLocationArr = new String[loc.size()];
                    }
                    int count = 0;
                    
                    ProfessionalVO prof = new ProfessionalVO();
                    prof.setId(7);
                    Collection<UserAccountRegionsVO> regions = new ArrayList();
                    if (loc != null) {
                        for (ReportQueueTreatmentsVO e : loc) {
                            treatmentLocationArr[count] = e.getTreatment_location_id() + "";
                            regions.add(new UserAccountRegionsVO(-1, 7, e.getTreatment_location_id()));
                            count++;
                        }
                    }
                    rservice.removeReportQueue(q);
                    rbuilder.generatePatientOutcomesReport(regions,q);
                    //rservice.removeReportQueue(q);
                
                }else {

                    String[] treatmentLocationArr = null;
                    if (loc != null ) {
                        treatmentLocationArr = new String[loc.size()];
                    }
                    String[] etiologyArr = null;
                    if (et != null ) {
                        etiologyArr = new String[et.size()];
                    }
                    String[] caretypesArr = null;
                    if (types != null ) {
                        caretypesArr = new String[types.size()];
                    }
                    int count = 0;
                    if (et != null) {
                        for (ReportQueueEtiologyVO e : et) {
                            etiologyArr[count] = e.getLookup_id() + "";
                            count++;
                        }
                    }
                    count = 0;
                    ProfessionalVO prof = new ProfessionalVO();
                    prof.setId(7);
                    Collection<UserAccountRegionsVO> regions = new ArrayList();
                    if (loc != null) {
                        for (ReportQueueTreatmentsVO e : loc) {
                            treatmentLocationArr[count] = e.getTreatment_location_id() + "";
                            regions.add(new UserAccountRegionsVO(-1, 7, e.getTreatment_location_id()));
                            count++;
                        }
                    }
                    prof.setRegions(regions);
                    count = 0;
                    if (types != null) {
                        for (ReportQueueWoundTypeVO e : types) {
                            caretypesArr[count] = e.getWound_type() + "";
                            count++;
                        }
                    }
                    
                    ReportingManager manager = new ReportingManager(
                            prof, 
                            q.getStart_date(), 
                            q.getEnd_date(), 
                            "treatmentLocation", 
                            breakdown, 
                            report_type, 
                            full_assessment, 
                            goal_days, 
                            null, 
                            treatmentLocationArr, 
                            includeTIP, 
                            etiologyArr, 
                            goals, 
                            caretypesArr, 
                            patient_id, 
                            ctx, 
                            report_language);
                    rservice.removeReportQueue(q);
                    byte[] bytes = manager.callReport(null, null);
                    System.out.println("Building report.");
                    SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
                    try {
                        if (q.getEmail() != null && !q.getEmail().equals("")) {
                            String[] emails = {q.getEmail()};
                            if (bytes != null) {
                                mail.postMail(
                                        emails, 
                                        Common.getLocalizedString("pixalere.reporting.form." + q.getReport_type(), "en") + " " + PDate.getMonthYear(PDate.getEpochTime() + ""), 
                                        "Report Server email",
                                        bytes, 
                                        "reportserver");//String[] recipients, String subject, String message,byte[] byteArray, String attachment_name
                            }
                        }
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                    //rservice.removeReportQueue(q);
                }
            }
            /*ReportBuilder rservice = new ReportBuilder(Constants.DASHBOARD_REPORT, cont);
            
             //While we're at it, lets check if its the first of the month, and send off the user statistics.
             SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
             mail.postMail("travis@pixalere.com", Common.getConfig("customer_name") + " User Stats for: " + PDate.getDate(new Date()), user_stats, "");
             ReportingServiceImpl service = new ReportingServiceImpl();
             //check if we have a new quarter starting, if so generate a new report for last quarter etc.
             Collection<DashboardReportVO> reports = service.getDashboardReports(new DashboardReportVO());
             for(DashboardReportVO report : reports){
             FiscalYear[] quarters = Common.getQuarters(7);
             rservice.generateDashboard(report,quarters);//generate PDF.

             }
             //Generate a global report.. 1 for each treatment area.   Store
             //in database for future archival... broke down by etiology.. 1 page per etiology per treatment area.. TOC.
             */


        } catch (Exception e) {
            e.printStackTrace();
        }
        
        /**
         * Scheduled Reports
         */
        try {
            ServletContext ctx = (ServletContext) context.getScheduler().getContext().get("servletContext");
            //System.out.println("class path=" + System.getProperty("java.class.path"));
            
            ReportSchedulerService service = new ReportSchedulerService();
            ReportBuilder rbuilder = new ReportBuilder(ctx);
            
            String formatExecuteTimeStamp = "dd/MM/yyyy hh:mm a";
            SimpleDateFormat timestampFormat = new SimpleDateFormat(formatExecuteTimeStamp);
            
            // Get the reports for execution
            List<ScheduleConfigVO> configs = service.getScheduledForExecution(new Date());
            
            // START: Count left executions, send alert email
            Set<ReportScheduleVO> schedulesToProcess = new HashSet<ReportScheduleVO>();
            for (Iterator<ScheduleConfigVO> it = configs.iterator(); it.hasNext();) {
                ScheduleConfigVO configVO = it.next();
                // Procces only those which belong to an Active schedule
                ReportScheduleVO schedule = configVO.getSchedule();
                if (schedule.getActive() != null && schedule.getActive() == 1) {
                    if (schedule.getEnable_reminder() != null && schedule.getEnable_reminder() == 1){
                        schedulesToProcess.add(schedule);
                    }
                } else {
                    // Schedule is currently disabled, mark the config as processed and set status
                    configVO.setProcessed(1);
                    configVO.setExit_status(
                            timestampFormat.format(new Date()) 
                                + " EXPIRED: This report was not generated. "
                                + "At the execution time: " 
                                    + timestampFormat.format(configVO.getExecution_on()) 
                                    + ", the corresponding scheduled report was disabled.");
                    service.saveConfig(configVO);
                    // remove config from current execution list
                    it.remove();
                }
                    
            }
            
            // Get the schedules for which an email alert has to be sent
            List<ReportScheduleVO> schedulesToAlert = service.getSchedulesToAlert(schedulesToProcess);
            
            // Send alert emails
            if (!schedulesToAlert.isEmpty()){
                SendMailUsingAuthentication mailer = new SendMailUsingAuthentication();    
                for (ReportScheduleVO scheduleToAlert : schedulesToAlert) {
                    String emails = scheduleToAlert.getReminder_emails();
                    String locale = Common.getLanguageLocale(scheduleToAlert.getUser_language());
                    String reportTitle = Common.getLocalizedString("pixalere.reporting.form." + scheduleToAlert.getReport_type(), locale);
                    if (scheduleToAlert.getCustom_title() != null || !scheduleToAlert.getCustom_title().trim().isEmpty()){
                        reportTitle += " - " + scheduleToAlert.getCustom_title();
                    }
                    try {
                        String alert_msg = 
                                Common.getLocalizedString("pixalere.reporting.scheduler.alert_email_body1", locale)
                                + " " + reportTitle 
                                + " " 
                                + Common.getLocalizedString("pixalere.reporting.scheduler.alert_email_body2", locale);
                        
                        //System.out.println("ALERT to " + emails + ":\n" + alert_msg);
                        mailer.postMail(emails, 
                            Common.getLocalizedString("pixalere.reporting.scheduler.alert_email_header", locale),
                            alert_msg, 
                            "");
                    } catch (Exception e) {
                        log.error("Error Sending Notification Email: ",e);
                    }
                }
            }
            // END: Count left executions, send alert email
            
            // START: Generate the reports
            for (ScheduleConfigVO configVO : configs) {
                ReportScheduleVO schedule = configVO.getSchedule();
                
                // Log execution
                Date date = configVO.getExecution_on();
                String execution = " -- ";
                String now_time = " -- ";
                try {
                    execution = timestampFormat.format(date);
                    now_time = timestampFormat.format(new Date());
                } catch (Exception e) {
                }
                System.out.println(now_time + " - Executing Scheduled Report [" + schedule.getReport_type() + "]: " + schedule.getCustom_title() + " Execution: " + execution);
                
                // Generate by type
                Integer report_language = 
                        (schedule.getUser_language() != null)? 
                        schedule.getUser_language(): 
                        Common.getLanguageIdFromLocale("en");
                
                rbuilder.setLanguage(report_language);
                Collection<ScheduleLocationVO> locations = schedule.getLocations();
                Collection<ScheduleEtiologyVO> et = schedule.getEtiologies();
                Date start_date = configVO.getStart_date();
                Date end_date = configVO.getEnd_date();
                
                // TODO: ADD MORE SCHEDULED REPORT TYPES HERE

                if (schedule.getReport_type().equals("report_dashboard") || schedule.getReport_type().equals("report_dashboard_4quarters")) {
                    DashboardReportVO dashboardReportVO = new DashboardReportVO();
                    dashboardReportVO.setEmail(schedule.getRecipients_emails());
                    
                    Collection<DashboardByEtiologyVO> etiologies = new ArrayList();
                    if (et != null) {
                        for (ScheduleEtiologyVO e : et) {
                            DashboardByEtiologyVO etiology = new DashboardByEtiologyVO();
                            etiology.setDashboard_report_id(-1);
                            etiology.setEtiology_id(e.getLookup_id());
                            etiologies.add(etiology);
                        }
                    }
                    dashboardReportVO.setEtiologies(etiologies);
                    Collection<DashboardByLocationVO> treatments = new ArrayList();
                    if (locations != null) {
                        for (ScheduleLocationVO e : locations) {
                            DashboardByLocationVO l = new DashboardByLocationVO();
                            l.setDashboard_report_id(-1);
                            l.setTreatment_location_id(e.getTreatment_location_id());
                            treatments.add(l);
                        }
                    }
                    dashboardReportVO.setTreatmentLocations(treatments);
                    dashboardReportVO.setWound_type("All");
                    
                    // New Dashboard PDF
                    DashboardReportService dashboardService = new DashboardReportService(ctx, report_language);
                    
                    // Mark as processed before running it, otherwise we get duplicates if its a long report
                    configVO.setProcessed(1);
                    service.saveConfig(configVO);
                    
                    
                    try {
                        List<FiscalQuarter> quarters;
                        // Old dashboard - 8 quarters - 2 years - landscape
                        if (schedule.getReport_type().equals("report_dashboard")) {
                            quarters = Common.getQuarters(7, start_date);
                            dashboardService.generateDashboardMail(dashboardReportVO, quarters);
                        } else {
                            // Extended dashboard - Get the Custom 5 columns dates from Extra Filter
                            quarters = PDate.getUnserializedCustomDatesDashboard(configVO.getExtra_options());
                            boolean displayLocations = schedule.getDisplay_locations() == null
                                    ? false
                                    : schedule.getDisplay_locations() == 1;
                            String custom_title = schedule.getUse_title() != null && schedule.getUse_title() == 1
                                    ? schedule.getCustom_title()
                                    : "";
                            dashboardService.generateExtendedDashboardMail(dashboardReportVO, quarters, displayLocations, custom_title);
                        }
                        
                        // Update status
                        String successStatus = "SUCCESS: " + timestampFormat.format(new Date()) + " Report generated. Recipients: " + schedule.getRecipients_emails();
                        configVO.setExit_status(successStatus);
                        service.saveConfig(configVO);
                    } catch (ApplicationException e) {
                        String errorMsg = "ERROR: " + timestampFormat.format(new Date()) + " Error generating Scheduled Report - Id: " + schedule.getId() + "  config id: " + configVO.getId();
                        log.error(errorMsg , e);
                        errorMsg += " Check logs for more details. " + e.getMessage();
                        // Record the error
                        configVO.setExit_status(errorMsg);
                        service.saveConfig(configVO);
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
