/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils.task;

import org.quartz.Job;
import com.pixalere.common.ApplicationException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.utils.Constants;
import com.pixalere.mail.SendMailUsingAuthentication;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.util.Date;
import java.util.Collection;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.reporting.bean.DashboardReportVO;

import com.pixalere.utils.FiscalYear;
import com.pixalere.utils.FiscalQuarter;
/**
 * DashboardReport quartz job.  Run at the first of each month, and quarter
 * and emailed out to the defined users.
 *
 * @since 5.0
 * @author travis
 */
public class WeeklyUserStats implements Job {

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        try {
            ServletContext cont = (ServletContext)context.getScheduler().getContext().get("servletContext");
            ReportBuilder rservice = new ReportBuilder(Constants.DASHBOARD_REPORT, cont);
            if(Common.getConfig("customer_name").indexOf("Acme")==-1){
                String user_stats="";
                //System.out.println(context.getJobDetail().getName()+"= "+context.getJobDetail().getName().indexOf("Monthly User Stats"));
                if(((String)context.getJobDetail().getJobDataMap().get("name")).indexOf("Monthly User Stats")!=-1){
                    user_stats = rservice.getUserStats(true,12);
                }else{
                     user_stats = rservice.getUserStats(false,12);
                    //String user_stats = rservice.getPilotStats(false);
                }
                //While we're at it, lets check if its the first of the month, and send off the user statistics.
                System.out.println(user_stats);
                SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
                mail.postMail("travis@pixalere.com", "Path: "+Common.getConfig("server_ip")+"\nServer: "+Common.getConfig("server_ip")+"\n\n"+Common.getConfig("customer_name") + " User Stats for: " + PDate.getDate(new Date(),false), user_stats, "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
