/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils.task;

import ca.uhn.hl7v2.model.DataTypeException;
import java.io.BufferedReader;
import org.apache.commons.io.FileUtils;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import ca.uhn.hl7v2.parser.*;
import ca.uhn.hl7v2.model.AbstractMessage;
import ca.uhn.hl7v2.model.v26.datatype.CWE;
import ca.uhn.hl7v2.model.v26.datatype.ST;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.segment.PID;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.segment.PV1;
import ca.uhn.hl7v2.model.v26.segment.AL1;
import ca.uhn.hl7v2.model.v26.datatype.CX;
import ca.uhn.hl7v2.model.v26.message.ADT_A01;
import ca.uhn.hl7v2.model.v26.message.ADT_A02;
import ca.uhn.hl7v2.model.v26.message.ADT_A03;
import ca.uhn.hl7v2.model.v26.message.ADT_A05;
import ca.uhn.hl7v2.model.v26.segment.PID;
import ca.uhn.hl7v2.model.v26.segment.IN1;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.group.ADT_A01_INSURANCE;
import ca.uhn.hl7v2.model.v26.group.ADT_A03_INSURANCE;
import ca.uhn.hl7v2.model.v26.group.ADT_A05_INSURANCE;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import java.io.StringReader;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Collection;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.*;
import net.sourceforge.jtds.jdbc.*;
import java.sql.*;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import java.util.HashMap;
import ca.uhn.hl7v2.parser.CanonicalModelClassFactory;
import java.util.Set;
import javax.xml.datatype.XMLGregorianCalendar;
import com.pixalere.utils.PDate;
import java.util.Iterator;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Schema;
import javax.xml.bind.Unmarshaller;
import org.xml.sax.SAXException;
import com.pixalere.integration.bean.paris.SearchClientResult;
import com.pixalere.integration.bean.paris.MPISearchClientResult;
import com.pixalere.integration.bean.paris.Allergy;
import com.pixalere.integration.bean.Patient;
import com.pixalere.integration.bean.Gender;
import com.pixalere.integration.bean.Event;
import java.io.IOException;
import java.io.FileReader;
import com.pixalere.integration.client.IntegrationClient;
import com.pixalere.integration.service.IntegrationService;
import com.pixalere.assessment.client.AssessmentClient;
import com.pixalere.assessment.service.AssessmentService;

/**
 * If Pixalere is interfaced with an EHR which requires the import of a XML data
 * extract, This Quartz job will import the XML, and call Pixalere's webservice
 * every 2 minutes.
 *
 * @since 5.0
 * @author travis
 */
public class PushXML implements Job {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PushXML.class);

    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            JAXBContext cont = JAXBContext.newInstance(MPISearchClientResult.class);
            File[] files = getFiles();
            SchemaFactory sf = SchemaFactory.newInstance(
                    javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);

            //validtes xml file.
            //Schema schema = sf.newSchema(new File("mpi.xsd"));
            Unmarshaller um = cont.createUnmarshaller();
            //um.setSchema(schema);
            System.out.println("Files: " + files.length);
            for (File file : files) {
                if(file.isFile()){
                FileReader fileObj = new FileReader(file.getAbsolutePath());
                String content = FileUtils.readFileToString(file);
                boolean error = false;
                
                if (file.getName().indexOf(".xml") != -1) {
                    if (content.indexOf("<MPISearchClientResult") != -1) {
                        MPISearchClientResult xml_file = (MPISearchClientResult) um.unmarshal(fileObj);
                        log.info("Reading " + xml_file.getSearchClientResults().size() + " Patient records");
                        for (SearchClientResult result : xml_file.getSearchClientResults()) {
                            Patient patient = new Patient();
                            // patient.setAccountNumber();
                            if (result.getFirstName() != null) {
                                patient.setFirstName(result.getFirstName());
                            }
                            if (result.getLastName() != null) {
                                patient.setLastName(result.getLastName());
                            }
                            if (result.getPhn() != null) {
                                patient.setPhn(result.getPhn());
                            }
                            patient.setMrn(result.getClientId() + "");
                            if (result.getBirthDate() != null) {
                                result.getBirthDate().setHour(0);
                                result.getBirthDate().setMinute(0);
                                result.getBirthDate().setSecond(0);
                                patient.setDateOfBirth(result.getBirthDate());
                            }
                            if (result.getGender() != null) {
                                patient.setGender(Gender.fromValue(result.getGender()));
                            }
                            Event event = new Event();
                            event.setSendingFacility("PARIS");
                            event.setMessageDateTime(PDate.epochToGregorian(PDate.getEpochTime() + ""));
                            event.setReason(result.getSource());
                            callWebService(patient, event);
                        }

                    } else {
                        CanonicalModelClassFactory mcf = new CanonicalModelClassFactory("2.6");
                        PipeParser pipeParser = new PipeParser(mcf);
                        pipeParser.setValidationContext(new NoValidation());
                        try {
                            //parse the message string into a Message object
                            System.out.println("Message: " + content);
                            if (content.indexOf("FHS") != -1) {
                                BufferedReader b = new BufferedReader(new StringReader(content));
                                String line = "";
                                StringBuilder msgString = new StringBuilder();
                                while ((line = b.readLine()) != null) {
                                    if (line.indexOf("FHS") != -1 || line.indexOf("BHS") != -1) {
                                        msgString = new StringBuilder();
                                    } else if (line.indexOf("FTS") == -1 && line.indexOf("BTS") == -1) {
                                        msgString.append(line + "\r");
                                    }
                                    if (line.indexOf("FTS") != -1) {
                                        System.out.println("msg;;; " + msgString);
                                        Message message = pipeParser.parse(msgString.toString());
                                        extractMessage(message);
                                    }
                                }
                            } else {
                                Message message = pipeParser.parse(content);
                                extractMessage(message);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            error = true;
                        }
                    }

                    //delete File
                    try {
                        if (error == false) {
                            boolean deleted = file.delete();
                            if (deleted == false) {
                                System.out.println("Failed to delete: " + file.getAbsolutePath());
                            } else {
                                System.out.println("Deleted: " + file.getAbsolutePath());
                            }
                        }
                    } catch (SecurityException e) {
                        e.printStackTrace();
                        System.out.println("Failed to delete: " + file.getAbsolutePath());
                    }
                }
                //System.out.println("End of FOR");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void extractMessage(Message message) {
        PID pid = null;
        MSH msh = null;
        IN1 ins = null;
        PV1 pv1 = null;
        AL1 al1 = null;
        if (message instanceof ADT_A01) {
            ADT_A01 ack = (ADT_A01) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            ADT_A01_INSURANCE tmp = ack.getINSURANCE();
            ins = tmp.getIN1();
            pv1 = ack.getPV1();
            al1 = ack.getAL1();
        } else if (message instanceof ADT_A02) {
            ADT_A02 ack = (ADT_A02) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            pv1 = ack.getPV1();
        } else if (message instanceof ADT_A03) {
            ADT_A03 ack = (ADT_A03) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            ADT_A03_INSURANCE tmp = ack.getINSURANCE();
            ins = tmp.getIN1();
            pv1 = ack.getPV1();
            al1 = ack.getAL1();
        } else if (message instanceof ADT_A05) {
            ADT_A05 ack = (ADT_A05) message;
            pid = ack.getPID();
            msh = ack.getMSH();
            ADT_A05_INSURANCE tmp = ack.getINSURANCE();
            ins = tmp.getIN1();
            pv1 = ack.getPV1();
            al1 = ack.getAL1();
        }

        Patient patient = new Patient();
        // patient.setAccountNumber();
        if (pid.getPatientName() != null && pid.getPatientName().length >= 1) {
            patient.setFirstName(pid.getPatientName()[0].getGivenName().getValue());
        }
        if (pid.getPatientName() != null && pid.getPatientName().length >= 1) {
            patient.setLastName(pid.getPatientName()[0].getFamilyName().getSurname().getValue());
        }
        if (pid.getPatientID() != null) {
            patient.setPhn(pid.getPatientAccountNumber().getIDNumber().getValue());
        }
        if (pv1.getDischargeDateTime() != null) {
            try {
                java.util.Date date = pv1.getDischargeDateTime().getValueAsDate();
                patient.setDischargeDate(date);
            } catch (DataTypeException e) {
            }
        }
        if (pv1.getAdmitDateTime() != null) {
            try {
                java.util.Date date = pv1.getAdmitDateTime().getValueAsDate();
                patient.setAdmitDate(date);
            } catch (DataTypeException e) {
            }
        }
        if (al1 != null && al1.getAllergenCodeMnemonicDescription() != null) {
            CWE allergies = al1.getAllergenCodeMnemonicDescription();
            if (allergies != null) {
                ST all = allergies.getText();
                if (all != null) {
                    patient.setAllergies(all.getValue());
                }
            }
        }
        CX[] ids = pid.getPatientIdentifierList();
        String mrn = null;
        for (CX cx : ids) {
            //System.out.println(cx.getIdentifierCheckDigit() + "=======" + cx.getIDNumber() + "----" + cx.getIdentifierTypeCode().getValue());
            if (cx.getIdentifierTypeCode().getValue() != null && cx.getIdentifierTypeCode().getValue().equals("PN")) {
                mrn = cx.getIDNumber().getValue();
            }
        }
        if (ids.length == 1) {
            //grab the first id and use it for an mrn

        }
        if (pid.getPatientAccountNumber() != null) {

            //patient.setPhn(pid.getPatientAccountNumber().getIDNumber().getValue().replaceAll(" ", ""));
        }
        patient.setMrn(mrn);
        if (pid.getDateTimeOfBirth() != null) {
            PDate pdate = new PDate();
            patient.setDateOfBirth(pdate.HL7DOBToGregorian(pid.getDateTimeOfBirth().getValue()));
        }
        if (pid.getAdministrativeSex() != null) {
            String gend = pid.getAdministrativeSex().getValue();
            if (gend.equals("M")) {
                patient.setGender(Gender.fromValue("Male"));
            } else if (gend.equals("F")) {
                patient.setGender(Gender.fromValue("Female"));
            } else if (gend.equals("T")) {
                patient.setGender(Gender.fromValue("Transgender"));
            } else if (gend.equals("U")) {
                patient.setGender(Gender.fromValue("Unknown"));
            }
        }
        Event event = new Event();
        event.setSendingFacility(msh.getSendingFacility().getNamespaceID().getValue());
        event.setMessageDateTime(PDate.epochToGregorian(PDate.getEpochTime() + ""));
        event.setReason(msh.getMessageType().getMessageCode().getValue());
        try {
            callWebService(patient, event);
        } catch (Exception e) {
            log.error("Error saving service: " + e.getMessage());
        }
    }

    private void callWebService(Patient patient, Event event) throws Exception {
        IntegrationClient ac = new IntegrationClient();

        IntegrationService remoteService = ac.createIntegrationClient();
        //remoteService.setDatabase("live");

        remoteService.savePatient(patient, event);

    }

    private File[] getFiles() throws IOException {
        String folder = Common.getConfig("dataPath") + "/";// + Common.getConfig("customer_id")+"/filedrop";
        System.out.println(folder);
        File sourceDirectory = new File(folder);
        File[] files = sourceDirectory.listFiles();
        Arrays.sort(files, new Comparator<File>() {

            public int compare(File a, File b) {
                return new Long(a.lastModified()).compareTo(new Long(b.lastModified()));

            }
        });

        return files;
    }
}
