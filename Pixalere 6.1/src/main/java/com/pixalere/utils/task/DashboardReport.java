/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils.task;

import org.quartz.Job;
import com.pixalere.common.ApplicationException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.pixalere.reporting.ReportBuilder;
import com.pixalere.utils.Constants;
import com.pixalere.mail.SendMailUsingAuthentication;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.util.List;
import java.util.Collection;
import com.pixalere.reporting.service.ReportingServiceImpl;
import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.service.DashboardReportService;

import com.pixalere.utils.FiscalYear;
import com.pixalere.utils.FiscalQuarter;
/**
 * DashboardReport quartz job.  Run at the first of each month, and quarter
 * and emailed out to the defined users.
 * 
 * @since 5.0
 * @author travis
 */
public class DashboardReport implements Job {

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        try {
            // Default language (no session in jobs)
            Integer language = Common.getLanguageIdFromSession(null);
            
            ServletContext ctx = (ServletContext)context.getScheduler().getContext().get("servletContext");
            // Old dashboard
            // ReportBuilder rservice = new ReportBuilder(Constants.DASHBOARD_REPORT, ctx);
            
            // New Dashboard service
            DashboardReportService dashboardService = new DashboardReportService(ctx, language);
            
            //String user_stats = rservice.getUserStats(true,8);
            //While we're at it, lets check if its the first of the month, and send off the user statistics.
            //SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
            //mail.postMail("travis@pixalere.com", "Server: "+Common.getConfig("server_ip")+"\n\n"+Common.getConfig("customer_name") + " User Stats for: " + PDate.getDate(new Date()), user_stats, "");
            ReportingServiceImpl service = new ReportingServiceImpl();
            //check if we have a new quarter starting, if so generate a new report for last quarter etc.
            Collection<DashboardReportVO> reports = service.getDashboardReports(new DashboardReportVO());
                for(DashboardReportVO report : reports){
                    List<FiscalQuarter> quarters = Common.getQuarters(7,null);
                    // Old service
                    // rservice.generateDashboard(report,quarters);//generate PDF.
                    
                    // New service, not extended
                    dashboardService.generateDashboardMail(report, quarters);
                }
                //Generate a global report.. 1 for each treatment area.   Store
                //in database for future archival... broke down by etiology.. 1 page per etiology per treatment area.. TOC.

            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
