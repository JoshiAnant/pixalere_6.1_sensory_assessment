/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.pixalere.utils.task;

import org.quartz.Job;

import javax.servlet.ServletContext;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.pixalere.utils.Common;
import com.pixalere.utils.ImageManipulation;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import org.apache.commons.io.FileUtils;

/**
 * A task that runs every 30 seconds.
 *
 * @since 6.2
 * @author travis
 */
public class Task30Seconds implements Job {

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        try {
            ImageManipulation imgservice  = new ImageManipulation();
            //ServletContext cont = (ServletContext) context.getScheduler().getContext().get("servletContext");
            if (Common.getConfig("showBarcode").equals("1")) {
                //check /opt/cameraqueue and process images
                // /opt/cameraqueue/customer_id
                File[] files = getFiles();
                if(files!=null){
                    System.out.println("Files: "+files);
                    for (File file : files) {
                        
                        if (file.isFile()) {
                            //FileReader fileObj = new FileReader(file.getAbsolutePath());
                            imgservice.readMetadata(file);
                           // String content = FileUtils.readFileToString(file);
                            //read metadata
                            
                            //move image
                            //save assessment record to proper assessment
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private File[] getFiles() throws IOException {
        String folder = "/home/ricoh/queue";//+ Common.getConfig("customer_id")+"/";
        System.out.println(folder);
        File sourceDirectory = new File(folder);
        if(sourceDirectory != null){
            File[] files = sourceDirectory.listFiles();
            Arrays.sort(files, new Comparator<File>() {

                public int compare(File a, File b) {
                    return new Long(a.lastModified()).compareTo(new Long(b.lastModified()));

                }
            });
        

        return files;
        }return null;
    }
}
