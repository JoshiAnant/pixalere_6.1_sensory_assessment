/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils.xml;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ProductCategoryVO;
import org.xml.sax.helpers.DefaultHandler;
import java.util.Vector;

/**
 * A DefaultHandler created specifically for parsing Product XML documents using
 * the SAX Parser.
 * @since 5.0
 * @author tmorris
 */
public class ProductHandler extends DefaultHandler {

    boolean bactive = false;
    boolean bcategory = false;
    boolean bcompany = false;
    boolean bquantity = false;
    boolean bdescription = false;
    boolean bimage = false;
    boolean btype = false;
    boolean burl = false;
    boolean bid = false;
    boolean bname = false;
    boolean bprice = false;
    private Vector<ProductsVO> products = new Vector<ProductsVO>();
    private int import_count = 0;
    private int error_count = 0;
    private String error_log = "";
    ProductsVO product = new ProductsVO();

    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        //reinitial product object
        if (qName.equalsIgnoreCase("product")) {
            product = new ProductsVO();
        }
        int length = attributes.getLength();
        //Each attribute
        for (int i = 0; i < length; i++) {
            // Get names and values to each attribute
            String name = attributes.getQName(i);
            String value = attributes.getValue(i);
            if (name.equalsIgnoreCase("ID")) {
                product.setUid(value);
            }
            if (name.equalsIgnoreCase("NAME")) {
                value=value.replaceAll("'","");
                product.setTitle(value);
            }
            if (name.equalsIgnoreCase("PRICE")) {
                try{
                product.setCost(Double.parseDouble(value));
                }catch(NumberFormatException e){
                    product.setCost(0.0);
                }
            }
        }
        if (qName.equalsIgnoreCase("ACTIVE")) {
            bactive = true;
        }

        if (qName.equalsIgnoreCase("CATEGORY")) {
            bcategory = true;
        }

        if (qName.equalsIgnoreCase("COMPANY")) {
            bcompany = true;
        }

        if (qName.equalsIgnoreCase("QUANTITY")) {
            bquantity = true;
        }
        if (qName.equalsIgnoreCase("DESCRIPTION")) {
            bdescription = true;
        }
        if (qName.equalsIgnoreCase("IMAGE")) {
            bimage = true;
        }
        if (qName.equalsIgnoreCase("URL")) {
            burl = true;
        }
        if (qName.equalsIgnoreCase("TYPE")) {
            btype = true;
        }

    }

    public void endElement(String uri, String localName,
            String qName) throws SAXException {
        if (qName.equalsIgnoreCase("PRODUCT")) {
            products.add(product);
            import_count++;
        }
        //System.out.println("End Element :" + qName);

    }
    public void error(SAXException e){
        error_count++;
         error_log=error_log+"\n\n"+e.getMessage();
    }
    public void fatalError(SAXException e){
         error_count++;
         error_log=error_log+"\n\n"+e.getMessage();
    }
    public void characters(char ch[], int start, int length) throws SAXException {




        if (bactive) {
            product.setActive(1);
            String value = new String(ch, start, length);
            if (!value.equals("")) {
                product.setActive(new Integer(value));
            }
            bactive = false;
        }

        if (bcategory) {
            ProductCategoryVO c = new ProductCategoryVO();
            String value = new String(ch, start, length);
            if (!value.equals("")) {
                c.setTitle(stripSingle(value));
                product.setCategory(c);
            }
            bcategory = false;
        }

        if (bcompany) {
            product.setCompany("");
            String value = new String(ch, start, length);
            if (!value.equals("")) {
                product.setCompany((stripSingle(value)));
            }
            bcompany = false;
        }

        if (bquantity) {
            product.setQuantity("1");
            String value = new String(ch, start, length);
            if (!value.equals("")) {
                
                //product.setQuantity((value));
            }
            
            bquantity = false;
        }
        if (bdescription) {
            product.setDescription("");
            String value = new String(ch, start, length);
            if (!value.equals("")) {
                product.setDescription((stripSingle(value)));
            }
            bdescription = false;
        }
        if (bimage) {
            //System.out.println("Image : " + value);
            bimage = false;
        }
        if (btype) {
            product.setType("");
            String value = new String(ch, start, length);
            if (!value.equals("")) {
                product.setType((stripSingle(value)));
            }
            btype = false;
        }
        if (burl) {
            product.setUrl("");
            String value = new String(ch, start, length);
            if (!value.equals("") && value.indexOf("http:")!=-1) {
                product.setUrl((stripSingle(value)));
            }
            burl = false;
        }



    }

    /**
     * @return the product
     */
    public Vector<ProductsVO> getProducts() {
        return products;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Vector<ProductsVO> products) {
        this.products = products;
    }

    /**
     * @return the import_count
     */
    public int getImport_count() {
        return import_count;
    }

    /**
     * @param import_count the import_count to set
     */
    public void setImport_count(int import_count) {
        this.import_count = import_count;
    }

    /**
     * @return the error_count
     */
    public int getError_count() {
        return error_count;
    }

    /**
     * @param error_count the error_count to set
     */
    public void setError_count(int error_count) {
        this.error_count = error_count;
    }

    /**
     * @return the error_log
     */
    public String getError_log() {
        return error_log;
    }

    /**
     * @param error_log the error_log to set
     */
    public void setError_log(String error_log) {
        this.error_log = error_log;
    }
    private String stripSingle(String value){
        value = value.replaceAll("' ", "ft ");
        value = value.replaceAll("'", "");
        return value;
    }
}
