/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.utils.xml;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.io.*;
/**
 * Generic class for reading XML files using the SAX parser (uses less memory).
 *
 * @since 5.0.1
 * @author tmorris
 */
public class XMLReader {
    public XMLReader(){}
    /*
     * Reading the XML file using the sax parser.
     *
     * @param products inputstream derived from the form.
     */
    public void readXML(InputStream inputStream, DefaultHandler handler){
        
        try {
            
            SAXParserFactory parserFact = SAXParserFactory.newInstance();
            SAXParser parser = parserFact.newSAXParser();
            
            parser.parse(inputStream, handler);
            

        }catch(ParserConfigurationException e){

        }catch(SAXException e){

        }catch(IOException e){
            
        }
    }
}
