package com.pixalere.utils;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;

/**
* This program generates a AES key, retrieves its raw bytes, and
 * then reinstantiates a AES key from the key bytes.
 * The reinstantiated key is used to initialize a AES cipher for
 * encryption and decryption.
 */

public class TripleDes {
	
	Cipher ecipher;
    Cipher dcipher;
 
    // 8-byte Salt
    byte[] salt = {
        (byte)0xA9, (byte)0x9B, (byte)0xC8, (byte)0x32,
        (byte)0x56, (byte)0x35, (byte)0xE3, (byte)0x03
    };
 
    // Iteration count
    int iterationCount = 1001;//19
 
    public TripleDes(String passPhrase) {
        try {
            // Create the key
            KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
            SecretKey key = SecretKeyFactory.getInstance(
                "PBEWithMD5AndDES").generateSecret(keySpec);
            ecipher = Cipher.getInstance(key.getAlgorithm());
            dcipher = Cipher.getInstance(key.getAlgorithm());
 
            // Prepare the parameter to the ciphers
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
 
            // Create the ciphers
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
        } catch (Exception e) {e.printStackTrace();
        }
    }
 
    public String encrypt(String str) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes();
	
 
            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);
			
            // Encode bytes to base64 to get a string
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (Exception e) {
			e.printStackTrace();
        }
        return null;
    }
 
    public String decrypt(String str) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
 
            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);
 
            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (Exception e) {
		//logs.error("Decryption error"+e.getMessage());
        }
        return null;
    }
 }