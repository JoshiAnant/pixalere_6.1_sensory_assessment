package com.pixalere.utils;

import java.util.Collection;
import java.util.Hashtable;
import com.pixalere.common.ArrayValueObject;
import java.util.Vector;
import java.util.List;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.bean.LookupVO;
import java.util.StringTokenizer;
/**
 * Serialize class is needed to unserialize the old serialized based fields from
 * v2 of Pixalere when it use to be written in PHP.   This class is now deprecated
 * and we're slowly normalizing all the serialized fields out of Pixalere with each
 * new release.
 *
 * @deprecated
 * @author travis
 * @since 3.0
 */
public class Serialize {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Serialize.class);

    public static String serialize(Collection<ArrayValueObject> vect,String delimiter,Integer language) throws ApplicationException{
        String result="";
        if(vect!=null){
            for(ArrayValueObject item : vect){
                result=result+delimiter+item.getLookup().getName(language)+(item.getOther()!=null && !item.getOther().equals("")?";"+item.getOther():"");
            }
        }
      
        if(result.length() > 0){
            
            return result.substring(delimiter.length(),result.length());
        }else{
            return "";
        }
    }
    public static String serialize(List<String> vect,String delimiter) throws ApplicationException{
        String result="";
        if(vect!=null){
            for(String item : vect){
                result=result+delimiter+item;
            }
        }
        if(result.length() > 0){
            return result.substring(delimiter.length(),result.length());
        }else{
            return "";
        }
    }
    public static String serialize(String[] vect,String delimiter) throws ApplicationException{
        String result="";
        if(vect!=null){
            for(String item : vect){
                result=result+delimiter+item;
            }
        }
        if(result.length() > 0){
            return result.substring(delimiter.length(),result.length());
        }else{
            return "";
        }
    }
    /**
     * @todo Move to Common.class
     * @param array
     * @param delimiter
     * @return
     * @throws ApplicationException
     */
    public static String arrayDelimiter(Vector array, String delimiter) throws ApplicationException{
        String returnStr = "";
        for (int x = 0; x < array.size(); x++) {
            returnStr = returnStr + array.get(x) + delimiter + " ";
            
        }
        int endLength = 0;
        if (returnStr.length() != 0) {
            endLength = returnStr.length() - (delimiter.length()+1);
        }
        return returnStr.substring(0, endLength);
    }
    
    public static Hashtable hashIze(String str)  throws ApplicationException{
        String s = str;
        String st = "";
        int flag = 1;
        int index1;
        int index2 = 0;
        Hashtable tmpV = new Hashtable();
        while (index2 < s.lastIndexOf("\"")) {
            index1 = s.indexOf("\"");
            s = s.substring(index1 + 1, s.length());
            index2 = s.indexOf("\"");
            String tmp = s.substring(0, index2);
            if (flag == 1) {
                tmpV.put(tmp, tmp);
                flag = 0;
            } else {
                tmpV.put(tmp, tmp);
            }
            s = s.substring(index2 + 1, s.length());
        }
        return tmpV;
    }
    
    public static Vector arrayIze(String str)  throws ApplicationException{
        String s = str;
        String st = "";
        int flag = 1;
        int index1;
        int index2 = 0;
        Vector tmpV = new Vector();
        if (s != null) {
            s=s.replace("\\\\","");
            while ( s.lastIndexOf("\"")!=-1) {
                
                index1 = s.indexOf("\"");
                try{
                    s = s.substring(index1 + 1, s.length());

                    index2 = s.indexOf("\"");

                    String tmp = s.substring(0, index2);
                    if (flag == 1) {
                        tmpV.add(tmp);
                        flag = 0;
                    } else {
                        tmpV.add(tmp);
                    }

                    s = s.substring(index2 + 1, s.length());
                }catch(StringIndexOutOfBoundsException e){
                    log.error("Serialize.arrayIze: StringIndex Exception"+ e.getMessage());
                }
                
            }
        }
        return tmpV;
    }
    public static Vector arrayIzeWithNames(String str,Integer language)  throws ApplicationException{
        String s = str;
        String st = "";
        int flag = 1;
        int index1;
        int index2 = 0;
        Vector tmpV = new Vector();
        if(str != null){
            s = s.replaceAll("\\\\","");
            while (index2 < s.lastIndexOf("\"")) {
                index1 = s.indexOf("\"");
                s = s.substring(index1 + 1, s.length());
                index2 = s.indexOf("\"");
                String tmp = s.substring(0, index2);
                try {
                    if (flag == 1) {
                        ListServiceImpl bd = new ListServiceImpl(language);
                        LookupVO vo = (LookupVO) bd.getListItem(Integer.parseInt(tmp));
	                    tmpV.add(vo.getName(language));
                        flag = 0;
                    } else {
                        ListServiceImpl bd = new ListServiceImpl(language);
                        LookupVO vo = (LookupVO) bd.getListItem(Integer.parseInt(tmp));
	                    tmpV.add(vo.getName(language));
                    }
                } catch (Exception e) {
                }
                s = s.substring(index2 + 1, s.length());
            }
        }
        return tmpV;
    }
    public static Vector arrayIzeWithList(String str,Integer language)  throws ApplicationException{
        String s = str;
        String st = "";
        int flag = 1;
        int index1;
        int index2 = 0;
        Vector tmpV = new Vector();
        if(str != null){
            s = s.replaceAll("\\\\","");
            while (index2 < s.lastIndexOf("\"")) {
                index1 = s.indexOf("\"");
                s = s.substring(index1 + 1, s.length());
                index2 = s.indexOf("\"");
                String tmp = s.substring(0, index2);
                try {
                    if (flag == 1) {
                        ListServiceImpl bd = new ListServiceImpl(language);
                        LookupVO vo = (LookupVO) bd.getListItem(Integer.parseInt(tmp));
                        tmpV.add(vo);
                        flag = 0;
                    } else {
                        ListServiceImpl bd = new ListServiceImpl(language);
                        LookupVO vo = (LookupVO) bd.getListItem(Integer.parseInt(tmp));
                        tmpV.add(vo);
                    }
                } catch (Exception e) {
                    //@todo throw
                }
                s = s.substring(index2 + 1, s.length());
            }
        }
        return tmpV;
    }
    public static String serializeFromVector(Vector tmpList)  throws ApplicationException{
        String serial = "";
        if (tmpList == null) {
            return "a:0:{}";
        } else {
            for (int x = 0; x < tmpList.size(); x++) {
                serial = serial + "i:" + x + ";s:\"" + (String) tmpList.get(x) + "\";";
            }
            String serial_all = "a:" + tmpList.size() + ":{" + serial + "}";
            return serial_all;
        }
    }
    /**
     * The new serializer class, trying to leave the old php methods behind.. :)
     * @param serial the serialized string
     * @return vector the vector containing all the items
     *
     */
    public static Vector unserialize(String serial)  throws ApplicationException{
        Vector vect=new Vector();
        if(serial!=null){
            StringTokenizer st=new StringTokenizer(serial,"|");
            while(st.hasMoreTokens()){
                String item = st.nextToken();
                vect.add(item);
            }
            
        }
        return vect;
    }
    public static Vector unserializeLegacy(String str)  throws ApplicationException{
        String s = str;
        String st = "";
        int flag = 1;
        int index1;
        int index2 = 0;
        Vector tmpV = new Vector();
        if (s != null) {
            s=s.replace("\\\\","");
            while ( s.lastIndexOf("\"")!=-1) {

                index1 = s.indexOf("\"");
                try{
                    s = s.substring(index1 + 1, s.length());

                    index2 = s.indexOf("\"");

                    String tmp = s.substring(0, index2);
                    if (flag == 1) {
                        tmpV.add(tmp);
                        flag = 0;
                    } else {
                        tmpV.add(tmp);
                    }

                    s = s.substring(index2 + 1, s.length());
                }catch(StringIndexOutOfBoundsException e){
                    log.error("Serialize.arrayIze: StringIndex Exception"+ e.getMessage());
                }

            }
        }
        return tmpV;
    }
    public static Vector unserialize(String serial,String delimiter)  throws ApplicationException{
        Vector<String> vect=new Vector<String>();
        if(serial!=null){
            StringTokenizer st=new StringTokenizer(serial,delimiter);
            while(st.hasMoreTokens()){
                String item = st.nextToken();
                vect.add(item);
            }

        }
        return vect;
    }
    public static String serialize(String[] tmpList)  throws ApplicationException{
        String serial = "";
        if (tmpList == null) {
            return "a:0:{}";
        } else {
            for (int x = 0; x < tmpList.length; x++) {
                serial = serial + "i:" + x + ";s:" + tmpList[x].length() + ":\"" + (String) tmpList[x] + "\";";
            }
            String serial_all = "a:" + tmpList.length + ":{" + serial + "}";
            return serial_all;
        }
    }
    
    public static String serializeString(String tmpString)  throws ApplicationException{
        return "a:1:{i:0;s:" + tmpString.length() + ":\"" + tmpString + "\"}";
    }
    
    
    public static String serializeStraight(Object[] tmpList)  throws ApplicationException{
        String serial = "";
        if (tmpList == null) {
            return "";
        } else {
            for (Object t : tmpList) {
                serial = serial + t + "--,--;";
            }
            return serial;
        }
    }
    
    public static Vector unserializeWithTextForCategories(String serial, Vector l,Integer language)  throws ApplicationException{
        Vector result = new Vector();
        if (serial != null) {
            for (int x = 0; x < l.size(); x++) {
                LookupVO tmp = (LookupVO) l.get(x);
                String value = "";
                if (tmp.getName(language) != null) {
                    value = tmp.getName(language);
                }
                if (value != null && !value.equals("") && serial.indexOf("\"" + value + "\"") != -1) {
                    
                    result.add("" + value);
                }
                
            }
        }
        return result;
    }
    
    public static Vector unserializeWithText(String serial, Collection<LookupVO> l,Integer language)  throws ApplicationException{
        Vector result = new Vector();
        if (serial != null) {
            for (LookupVO tmp : l) {
                String value = "";
                if (tmp.getName(language) != null) {
	                     value = tmp.getName(language);
                }
                
                if (value != null && !value.equals("") && serial.indexOf("\"" + value + "\"") != -1) {
                    
                    result.add("" + tmp.getName(language));
                }
                
            }
        }
        return result;
        
    }
    public static String[] unserializeByDelimiter(String serial,String delimiter) throws ApplicationException{
        Vector result = new Vector();
        //if( serial!=null){
            String[] t = serial.split(delimiter);
            return t;
        //}else{
        //    return null;
        //}
    }
    public static Vector unserializeStraight(String serial)  throws ApplicationException{
        Vector result = new Vector();
        if(serial!=null){
            String [] tempStraight = null;
            tempStraight = serial.split("--,--;");
            for (String value : tempStraight){
                if (value != null && !value.equals("")) {
                    result.add("" + value);
                }
            }
        }
        return result;
    }
    
    public static String commaIze(Vector<String> vect) throws ApplicationException{
        String result="";
        for(String item : vect){
            result=result+", "+item;
        }
        if(result.length() > 0){
            return result.substring(1,result.length());
        }else{
            return "";
        }
    }

    public static String commaIze(String str) throws ApplicationException {
        String s = str;
        String st = "";
        int flag = 1;
        int index1;
        int index2 = 0;
        while (index2 < s.lastIndexOf("\"")) {
            index1 = s.indexOf("\"");
            s = s.substring(index1 + 1, s.length());
            index2 = s.indexOf("\"");
            String tmp = s.substring(0, index2);
            if (flag == 1) {
                st = tmp;
                flag = 0;
            } else {
                st = st + ", " + tmp;
            }
            s = s.substring(index2 + 1, s.length());
        }
        return st;
    }

    public static String commaIzeList(String str,Integer language) throws ApplicationException {
        String s = str;
        String st = "";
        int flag = 1;
        int index1;
        int index2 = 0;
        if(s!=null){
            while (index2 < s.lastIndexOf("\"")) {
                index1 = s.indexOf("\"");
                s = s.substring(index1 + 1, s.length());
                index2 = s.indexOf("\"");
                String tmp = s.substring(0, index2);
                if (flag == 1) {
                    try {
                        flag = 0;
                        ListServiceImpl bd = new ListServiceImpl(language);
                        LookupVO vo = (LookupVO) bd.getListItem(Integer.parseInt(tmp));
                        st = vo.getName(language);
                    } catch (NumberFormatException e) {
                        st = tmp; // Incase it's an old text value
                        //log.error("NumberFormatException: " + e.getMessage());
                    } catch (Exception e) {
                        log.error("Exception: " + e.getMessage());
                        
                    }
                } else {
                    try {
                        ListServiceImpl bd = new ListServiceImpl(language);

                        LookupVO vo = (LookupVO) bd.getListItem(Integer.parseInt(tmp));
                        
                        if(vo!=null){
                            st = st + ", " + vo.getName(language);
                        }
                    } catch (NumberFormatException e) {
                        st = st + ", " + tmp;  // Incase it's an old text value
                        //log.error("NumberFormatException: " + e.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                s = s.substring(index2 + 1, s.length());
            }
        }
        return st;
    }
    
    public static String printSerializedString(String serial)  throws ApplicationException{
        String tmp = "";
        if (serial == null) {
            return "";
        }
        tmp = arrayDelimiter(arrayIze(serial), ",");
        return tmp;
    }
    
}
