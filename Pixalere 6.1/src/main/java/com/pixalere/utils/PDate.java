package com.pixalere.utils;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Vector;
import java.text.SimpleDateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.DateFormat;
import com.pixalere.common.ApplicationException;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.struts.reporting.DetailsSchedulerForm;
import com.pixalere.struts.reporting.ParameterForm;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Collections;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PDate {
    private static String regex_pattern_ddmmyyyy = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20|21)\\d\\d)";
    
    public PDate() {
    }
    private String timezone;
     public static Date subtractDays(Date d, int days) {
         MyCalendar calendar1 = new MyCalendar();
         calendar1.setTime(new Date((getEpochTime(d) - Long.parseLong((days * Constants.DAY)+""))*1000));
       // d.setTime((getEpochTime(d) - Long.parseLong((days * Constants.DAY)+""))*1000);//seconds ot milliseconds
        return calendar1.getTime();
    
    }
    
    public static Date addDays(Date d, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
    
    public static Date addHours(Date d, int hrs) {
        d.setTime(getEpochTime(d) + Long.parseLong((hrs * Constants.DAY) + "") * 1000);
        return d;
    }
    
    public PDate(String timezone) {
        if (timezone != null && !timezone.equals("")) {
            Constants.TIMEZONE = timezone;
        }
    }
    
    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    /**
     * Counts days between two dates ignoring time (hour, minutes)
     * i.e. Days between 01/Aug/2015 13:10 and 02/Aug/2015 10:15 = 1
     * 
     * @param startDate
     * @param endDate
     * @return 
     */
    public static long getDaysBetween(Date startDate, Date endDate) {
        Calendar date = Calendar.getInstance();
        date.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);
        
        long daysBetween = 0;
        if (startDate.before(endDate)) {
            while (date.before(end)) {
                date.add(Calendar.DAY_OF_MONTH, 1);
                daysBetween++;
            }
            // if we are in the same day but is no longer before, add 1 day
            if (date.get(Calendar.MONTH) == end.get(Calendar.MONTH)
                    && date.get(Calendar.DAY_OF_MONTH) == end.get(Calendar.DAY_OF_MONTH)){
                daysBetween++;
            }
        }
        return daysBetween;
    }
    
    public static long getAge(Date date1) {
        
        long days = getDaysBetweenDates(new Date(), date1);
        
        return days = days / 365;
        
    }
    
    private static class MyCalendar extends GregorianCalendar {
        
        public MyCalendar() {
            this.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        }
        
        public long getTimeInMillis() {
            return super.getTimeInMillis();
        }
    }
    
    public static Date getDate(int month, int day, int year) {
        if (day > 0 && year > 0) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH, day);
            c.set(Calendar.MONTH, month - 1);
            c.set(Calendar.YEAR, year);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            return c.getTime();
        } else {
            return null;
        }
        
    }
    
    public static String getDateTime(Date t) {
        if (t != null) {
            MyCalendar calendar1 = new MyCalendar();
            calendar1.setTime(t);
            calendar1.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
            DateFormat d = new SimpleDateFormat("HHmm'h' dd/MMM/yyyy");
            String value = d.format(calendar1.getTime());
            return value;
        }
        return "";
    }
    public static Date convertTimezoneDate(Date d){
        try{
            
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
            String value = df.format(d);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sf.parse(value);
      
            return date;
        }catch (Exception e) {
            return null;
            //parse exception.
        }
    }
   public static String getDateIgnoreZone(Date t,boolean isTimestamp) {
        if (t != null) {
            try {

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                if (isTimestamp) {
                    df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                }
                String value = df.format(t);
                return value;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String getDate(Date t,boolean isTimestamp) {
        if (t != null) {
            try {

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                if (isTimestamp) {
                    // FIXME (?): Potential bug - Dates are shown in 12h format
                    // should it be in 24h format for usage in SQL ranges?
                    df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                }
                df.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
                String value = df.format(t);
                return value;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }
    public static List<FiscalQuarter> getFiscalYears(int num_quarters, Date now, String fiscalStart, String fiscalQuarter) {
        //adding a year incase we're midway through a year, and need to go back 1 extra year to get enough quarters.
        int num_years = (int) Math.ceil(num_quarters / 4.0) + 1;
        //if the fiscal year starts in april we need another year
        num_years = (fiscalStart.equals("apr1st"))? num_years + 1: num_years; 
        
        List<FiscalQuarter> quarters = new ArrayList();
        int quarter_count = 0;
        for (int x = 0; x < num_years; x++) {
            //years[x] = new FiscalYear();
            if (fiscalQuarter.equals("3months")) {
                if (fiscalStart.equals("jan1st")) {
                    //FiscalQuarter[] qs = new FiscalQuarter[4];
                    //ignore quarters which are current or in the future.
                    if (getDate(1900 + now.getYear() - x + 1, 0, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(4);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 10, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x + 1, 1, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    if (getDate(1900 + now.getYear() - x, 9, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(3);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 7, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 10, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    if (getDate(1900 + now.getYear() - x, 6, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(2);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 4, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 7, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    if (getDate(1900 + now.getYear() - x, 3, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(1);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 1, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 4, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    
                } else if (fiscalStart.equals("apr1st")) {
                    //ignore quarters which are current or in the future.
                    if (getDate(1900 + now.getYear() - x + 1, 3, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(4);
                        qs.setStartDate(getDate(1900 + now.getYear() - x + 1, 1, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x + 1, 4, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    if (getDate(1900 + now.getYear() - x + 1, 1, 1, 0, 0, 0).getTime() <= now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(3);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 10, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x + 1, 1, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                        
                    }
                    if (getDate(1900 + now.getYear() - x, 9, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(2);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 7, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 10, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                        
                    }
                    if (getDate(1900 + now.getYear() - x, 6, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(1);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 4, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 7, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                        
                    }
                    
                }
            } else if (fiscalQuarter.equals("12weeks")) {
                if (fiscalStart.equals("jan1st")) {
                    // FIXME: This combination never happens? 
                    // NOTE: Should we show a warning in the config page?
                } else if (fiscalStart.equals("apr1st")) {
                    //ignore quarters which are current or in the future.
                    if (getDate(1900 + now.getYear() - x + 1, 3, 1, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(4);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 12, 9, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x + 1, 4, 1, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    if (getDate(1900 + now.getYear() - x, 11, 9, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(3);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 9, 16, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 12, 9, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    if (getDate(1900 + now.getYear() - x, 8, 16, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(2);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 6, 24, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 9, 16, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                    if (getDate(1900 + now.getYear() - x, 5, 24, 0, 0, 0).getTime() < now.getTime() && num_quarters >= quarter_count) {
                        FiscalQuarter qs = new FiscalQuarter();
                        qs.setQuarter(1);
                        qs.setStartDate(getDate(1900 + now.getYear() - x, 4, 1, 0, 0, 0));
                        qs.setEndDate(getDate(1900 + now.getYear() - x, 6, 24, 0, 0, 0));
                        qs.setOrder((quarter_count + 1));
                        quarters.add(qs);
                        quarter_count++;
                    }
                       
                }
                
            }
            
        }
        Collections.sort(quarters);
        
        
        return quarters;
    }
    
    public static FiscalYear getFiscalYear(Date current_date, String fiscalStart, String fiscalQuarter, int minus_years) {
        FiscalYear year = new FiscalYear();
        if (fiscalQuarter.equals("3months")) {
            if (fiscalStart.equals("jan1st")) {
                Date date = getDate(1900 + current_date.getYear() - minus_years, 0, 1, 0, 0, 0);
                year.setEndYear(date);
                
                date = getDate(1900 + current_date.getYear() - 1 - minus_years, 0, 1, 0, 0, 0);
                year.setStartYear(date);
            } else if (fiscalStart.equals("apr1st")) {
                int yr = current_date.getYear();
                yr = 1900 + yr;
                if (current_date.getMonth() == 0 || current_date.getMonth() == 1 || current_date.getMonth() == 2) {
                    yr = yr - 1;
                }
                Date date = getDate(yr - minus_years, 3, 1, 0, 0, 0);
                year.setEndYear(date);
                
                date = getDate(yr - minus_years - 1, 3, 1, 0, 0, 0);
                year.setStartYear(date);
            }
        } else if (fiscalQuarter.equals("12weeks")) {
            int yr = current_date.getYear();
            yr = 1900 + yr;
            if (current_date.getMonth() == 0 || current_date.getMonth() == 1 || current_date.getMonth() == 2) {
                yr = yr - 1;
            }
            Date date = getDate(yr - minus_years, 3, 1, 0, 0, 0);
            year.setEndYear(date);
            
            date = getDate(yr - minus_years - 1, 3, 1, 0, 0, 0);
            year.setStartYear(date);
        }
        return year;
    }
    
    public static List<FiscalQuarter> getLastXQuarters(int num_quarters, String fiscalStart, String fiscalQuarter, Date start_date) {
        Date now = null;
        if (start_date == null) {
            now = new Date(112, 2, 1, 0, 0, 0);
        } else {
            now = start_date;
        }
        List<FiscalQuarter> years = getFiscalYears(num_quarters, now, fiscalStart, fiscalQuarter);
        
        return years;
    }
    
    public static Date getDate(int year, int month, int day, int hour, int minute, int second) {
        try {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH, day);
            c.set(Calendar.MONTH, month - 1);
            c.set(Calendar.YEAR, year);
            c.set(Calendar.HOUR_OF_DAY, hour);
            c.set(Calendar.MINUTE, minute);
            c.set(Calendar.SECOND, second);
            c.set(Calendar.MILLISECOND, 0);
            return c.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static String getDate(long d) {
        if (d != 0) {
            try {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date t = new Date();
                t.setTime(d * 1000);
                String value = df.format(t);
                return value;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }
    
    public static String getMonthYear(String epoch) {
        try {
            DateFormat df = new SimpleDateFormat("MMM yyyy");
            Date t = new Date();
            t.setTime(Long.parseLong(epoch) * 1000);
            String value = df.format(t);
            return value;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    
    public static Date getStartOfDay(Date date) {
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);//the date we want to set initially.
            c.set(Calendar.HOUR, 0);
            c.set(Calendar.MINUTE, 0);
            return c.getTime();
            
        }
        return null;
    }
    
    public static Date getEndOfDay(Date date) {
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);//the date we want to set initially.
            c.set(Calendar.HOUR, 23);
            c.set(Calendar.MINUTE, 59);
            return c.getTime();
            
        }
        return null;
    }
    
    public static XMLGregorianCalendar epochToGregorian(String dt) {
        DatatypeFactory dataTypeFactory;
        try {
            dataTypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        
        Date date = new Date(Long.parseLong(dt) * 1000);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        return dataTypeFactory.newXMLGregorianCalendar(gc);
        
    }
    
    public static long gregorianToEpoch(XMLGregorianCalendar calendar) {
        try {
            GregorianCalendar greg = calendar.toGregorianCalendar();
            Date dateTime = greg.getTime();
            
            return dateTime.getTime();
        } catch (Exception e) {
            
            return -1;
        }
    }
    
    public static Date getFirstDayOfMonth(Date date, int month) {
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);//the date we want to set initially.
            if (month < 0) {
                c.add(Calendar.MONTH, month);
            }
            c.set(Calendar.HOUR, 0);
            c.set(Calendar.MINUTE, 1);
            
            int day = c.getActualMinimum(Calendar.DAY_OF_MONTH);
            c.set(Calendar.DAY_OF_MONTH, day);
            Date lastMonth1 = (c.getTime());
            return lastMonth1;
            
        }
        return null;
    }
    
    public static Date getLastDayOfMonth(Date date, int month) {
        if (date != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);//the date we want to set initially.
            if (month < 0) {
                c.add(Calendar.MONTH, month);
                
            }
            c.set(Calendar.HOUR, 23);
            c.set(Calendar.MINUTE, 59);
            
            int day = c.getActualMaximum(Calendar.DAY_OF_MONTH);
            c.set(Calendar.DAY_OF_MONTH, day);
            Date lastMonth2 = (c.getTime());
            return lastMonth2;
        }
        return null;
    }
    
    public static long getDaysBetweenDates(Date date1, Date date2) {
        long seperation = date1.getTime() - date2.getTime();
        return (seperation / (24 * 60 * 60 * 1000));
    }
     public static Date convertToUTC(Date d){
        try{
            
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            String value = df.format(d);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sf.parse(value);
      
            return date;
        }catch (Exception e) {
            return null;
            //parse exception.
        }
    }
    //Only used for automated viewer edits... Need to refactor out.
    public static Vector getDateInVector(Date time) {
        Vector dates = new Vector();
        try {
            if (time == null) {
                return new Vector();
            }
            MyCalendar calendar = new MyCalendar();
            calendar.setTime(time);
            
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
        } catch (Exception e) {
            dates = new Vector();
        }
        return dates;
    }

    //Only used for automated viewer edits... Need to refactor out.
    public static Vector getDateTimeInVector(Date time) {
        Vector dates = new Vector();
        try {
            if (time == null) {
                return new Vector();
            }
            MyCalendar calendar = new MyCalendar();
            calendar.setTime(time);
            calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
            NumberFormat minFormat = NumberFormat.getNumberInstance();

            //Set the minimum number of digits to 2 for minFormat.
            minFormat.setMinimumIntegerDigits(2);
            dates.add((calendar.get(Calendar.MONTH) + 1) + "");
            dates.add(calendar.get(Calendar.DAY_OF_MONTH) + "");
            dates.add(calendar.get(Calendar.YEAR) + "");
            dates.add(minFormat.format(calendar.get(Calendar.HOUR_OF_DAY) + ""));
            dates.add(minFormat.format(calendar.get(Calendar.MINUTE) + ""));
        } catch (Exception e) {
            dates = new Vector();
        }
        return dates;
    }
    
    public static long getEpochTime() {
        Date date = new Date();
        MyCalendar calendar1 = new MyCalendar();
        calendar1.setTime(date);
        calendar1.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        long epoch = calendar1.getTimeInMillis() / (long) 1000;
        return epoch;
    }
    
    public static long getEpochTime(Date date) {
        
        MyCalendar calendar1 = new MyCalendar();
        calendar1.setTime(date);
        calendar1.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        long epoch = calendar1.getTimeInMillis() / (long) 1000;
        return epoch;
    }
    
    public Date reverseSignature(String signature) throws ApplicationException {
        int index = signature.indexOf("h ");
        String hourdate = signature.substring(0, index);
        String date = signature.substring(index + 2, signature.length());
        int index3 = date.indexOf("/");
        int day = Integer.parseInt(date.substring(0, index3));
        date = date.substring(index3 + 1, date.length());
        int index4 = date.indexOf("/");
        String monthstr = date.substring(0, index4);
        int month = 0;
        if (monthstr.equals("Jan")) {
            month = 1;
        } else {
            if (monthstr.equals("Feb")) {
                month = 2;
            } else {
                if (monthstr.equals("Mar")) {
                    month = 3;
                } else {
                    if (monthstr.equals("Apr")) {
                        month = 4;
                    } else {
                        if (monthstr.equals("May")) {
                            month = 5;
                        } else {
                            if (monthstr.equals("Jun")) {
                                month = 6;
                            } else {
                                if (monthstr.equals("Jul")) {
                                    month = 7;
                                } else {
                                    if (monthstr.equals("Aug")) {
                                        month = 8;
                                    } else {
                                        if (monthstr.equals("Sep")) {
                                            month = 9;
                                        } else {
                                            if (monthstr.equals("Oct")) {
                                                month = 10;
                                            } else {
                                                if (monthstr.equals("Nov")) {
                                                    month = 11;
                                                } else {
                                                    if (monthstr.equals("Dec")) {
                                                        month = 12;
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        int year = Integer.parseInt(date.substring(index4 + 1,
                date.length()));
        
        int hour = Integer.parseInt(hourdate.substring(0, 2));
        int minute = Integer.parseInt(hourdate.substring(2,
                hourdate.length()));
        
        Date last_update = getDate(month, day, year, hour,
                minute);
        
        return last_update;
        
    }
    
    public Timestamp gregorianToTimestamp(XMLGregorianCalendar calendar) {
        try {
            GregorianCalendar greg = calendar.toGregorianCalendar();
            Date dateTime = greg.getTime();
            Timestamp t = (dateTime instanceof Timestamp) ? (Timestamp) dateTime : new Timestamp(dateTime.getTime());
            return t;
        } catch (Exception e) {
            
            return null;
        }
    }
    
    public XMLGregorianCalendar HL7DOBToGregorian(String dt) {
        DatatypeFactory dataTypeFactory;
        try {
            dataTypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
            if (dt == null || dt.equals("")) {
                return null;
            }
            Date date = sf.parse(dt);
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            return dataTypeFactory.newXMLGregorianCalendar(gc);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        
    }
    
    public XMLGregorianCalendar timestampToGregorian(String dt) {
        DatatypeFactory dataTypeFactory;
        try {
            dataTypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = sf.parse(dt);
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);
            return dataTypeFactory.newXMLGregorianCalendar(gc);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        
    }
    
    public static String getFormattedDateFromEpoch(Date date, String format) throws ApplicationException {
        try {
            if (date != null) {
                SimpleDateFormat sf = new SimpleDateFormat(format);
                
                String formatted_date = sf.format(date);
                //System.out.println(date.getTime() + "Pre-convert: " + date.toString());

                //System.out.println(datehour + " Epoch: " + epoch);
                return formatted_date + "";
            }
        } catch (Exception e) {
            return "";
            //parse exception.
        }
        return "";
    }
    public static Date getDateFromFormatted(String date, String format) throws ApplicationException {
        try {
            if (date != null) {

                SimpleDateFormat sf = new SimpleDateFormat(format);

                Date formatted_date = sf.parse(date);
  
                return formatted_date;
            }
        } catch (Exception e) {
            return null;
            //parse exception.
        }
        return null;
    }
    public static String getFormattedDate(Date date, String format) throws ApplicationException {
        try {
            if (date != null) {
                
                SimpleDateFormat sf = new SimpleDateFormat(format);
                
                String formatted_date = sf.format(date);
                //System.out.println(date.getTime() + "Pre-convert: " + date.toString());

                //System.out.println(datehour + " Epoch: " + epoch);
                return formatted_date + "";
            }
        } catch (Exception e) {
            return "";
            //parse exception.
        }
        return "";
    }
    
    public static Date getLicenseDate(String d) {
        if (d != null && !d.equals("")) {
            try {
                SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
                Date date = sf.parse(d);
                return date;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new Date();
    }
    
    public static String getEpochFromString(String datehour) throws ApplicationException {
        try {
            if (datehour != null && !datehour.equals("")) {
                SimpleDateFormat sf = new SimpleDateFormat("dd/MMM/yyyy HH:mm");
                Date date = sf.parse(datehour);
                //System.out.println(date.getTime() + "Pre-convert: " + date.toString());
                MyCalendar calendar1 = new MyCalendar();
                calendar1.setTime(date);
                calendar1.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
                long epoch = calendar1.getTimeInMillis() / (long) 1000;
                //System.out.println(datehour + " Epoch: " + epoch);
                return epoch + "";
            }
        } catch (Exception e) {
            System.out.println(datehour + " Excpoch: " + getEpochTime() + "");
            return null;
            //parse exception.
        }
        return null;
    }
    
    public static Date getDateFromString(String datehour, boolean timestamp) throws ApplicationException {
        try {
            if (datehour != null && !datehour.equals("")) {
                String format = "dd/MMM/yyyy";
                if (timestamp) {
                    format = "yyyy/MM/dd hh:mm:ss";//2013-05-31 10:53:01
                }
                SimpleDateFormat sf = new SimpleDateFormat(format);
                Date date = sf.parse(datehour);
                //System.out.println(date.getTime() + "Pre-convert: " + date.toString());
                /*MyCalendar calendar1 = new MyCalendar();
                 calendar1.setTime(date);
                 calendar1.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
                 long epoch = calendar1.getTimeInMillis() / (long) 1000;
                 //System.out.println(datehour + " Epoch: " + epoch);*/
                return date;
                //return epoch + "";
            }
        } catch (Exception e) {
            System.out.println(datehour + " Excpoch: " + getEpochTime() + "");
            return null;
            //parse exception.
        }
        return null;
    }
    
    public static Date getDate(String epoch) throws ApplicationException {
        try {
            if (epoch != null && !epoch.equals("")) {
                Date date = new Date(Long.parseLong(epoch));
                return date;
                //return epoch + "";
            }
        } catch (Exception e) {
            System.out.println(" Excpoch: " + getEpochTime() + "");
            return null;
            //parse exception.
        }
        return null;
    }
    
    public static String getEpochFromAnyTime(String datehour, boolean startOfDay) throws ApplicationException {
        try {
            if (datehour != null && !datehour.equals("")) {
                SimpleDateFormat sf = new SimpleDateFormat("dd/MMM/yyyy");
                Date date = sf.parse(datehour);
                System.out.println(date.getTime() + "Pre-convert: " + date.toString());
                if (startOfDay == true) {
                    date.setHours(0);
                    date.setMinutes(0);
                } else {
                    date.setHours(23);
                    date.setMinutes(59);
                }
                MyCalendar calendar1 = new MyCalendar();
                calendar1.setTime(date);
                calendar1.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
                
                long epoch = calendar1.getTimeInMillis() / (long) 1000;
                //System.out.println(datehour + " Epoch: " + epoch);
                return epoch + "";
            }
        } catch (Exception e) {
            System.out.println(datehour + " Excpoch: " + getEpochTime() + "");
            return getEpochTime() + "";
            //parse exception.
        }
        return getEpochTime() + "";
    }
    
    public static Date getDate(int month, int day, int year,
            int hour, int minute) throws ApplicationException {
        
        Date tmpEpoch = null;
        
        Timestamp d1 = null;
        MyCalendar epoch_result = new MyCalendar();
        epoch_result.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        if ((month != 0 && year != 0 && day != 0) && (month != -1 && year != -1 && day != -1)) {
            try {
                epoch_result.set(year, month - 1, day, hour,
                        minute, 1);

                //epoch_result.setTimeZone(TimeZone.);
            } catch (Exception excepti) {
                excepti.printStackTrace();
            }
            try {
                
                tmpEpoch = epoch_result.getTime();
            } catch (NumberFormatException e) {
                tmpEpoch = null;
            }
        } else {
            //return null since a date cannot be set without all three parameters being
            //selected.
            tmpEpoch = null;
        }
        
        return tmpEpoch;
    }
    
    public String padHour(int expr) throws ApplicationException {
        String result = "";
        if (expr < 10) {
            result = result.concat(String.valueOf("0"));
        }
        
        result = result.concat(String.valueOf(expr));
        return (result);
    }
    
    private String getTimezoneStr(Date time, TimeZone zone) {
        try {
            String timezone = zone.getDisplayName(zone.inDaylightTime(time), TimeZone.SHORT);
            return timezone;
        } catch (Exception e) {
            return "";
            
        }
    }
    
    public String getProfessionalSignature(Date time,
        ProfessionalVO userVO, String locale) throws ApplicationException {
        
        GregorianCalendar calendar = new GregorianCalendar();
        
        TimeZone timezone = TimeZone.getTimeZone(Constants.TIMEZONE);
        if(userVO.getTimezone()!=null){
            timezone = TimeZone.getTimeZone(userVO.getTimezone());
        }
       
        String zonestr = getTimezoneStr(time, timezone);
        calendar.setTimeZone(timezone);
        calendar.setTime(time);
        int hour = (calendar.get(Calendar.HOUR_OF_DAY));
        try {
            return padHour(hour) + "" + "" + (((calendar.get(Calendar.MINUTE)) + "").length() == 1 ? "0" : "") + "" + calendar.get(Calendar.MINUTE) + "h " + calendar.get(Calendar.DAY_OF_MONTH) + "/" + getMMM((calendar.get(Calendar.MONTH) + 1),locale) + "/" + calendar.get(Calendar.YEAR) + " " + zonestr + " - " + userVO.getPosition().getTitle() + "#" + userVO.getId() + " " + userVO.getFirstname().substring(0, 1) + "" + userVO.getLastname().substring(0, 1);
            
        } catch (Exception e) {
            return padHour(hour) + "" + "" + (((calendar.get(Calendar.MINUTE)) + "").length() == 1 ? "0" : "") + "" + calendar.get(Calendar.MINUTE) + "h " + calendar.get(Calendar.DAY_OF_MONTH) + "/" + getMMM((calendar.get(Calendar.MONTH) + 1),locale) + "/" + calendar.get(Calendar.YEAR) + " - " + Common.getLocalizedString("pixalere.na", locale);
        }
      
    }
    
    public String getMMM(int month,String locale) {
    	
        if (month == 1) {
            return Common.getLocalizedString("pixalere.mon1",locale);
        } else {
            if (month == 2) {
                return "Feb";
            } else {
                if (month == 3) {
                    return "Mar";
                } else {
                    if (month == 4) {
                        return "Apr";
                    } else {
                        if (month == 5) {
                            return "May";
                        } else {
                            if (month == 6) {
                                return "Jun";
                            } else {
                                if (month == 7) {
                                    return "Jul";
                                } else {
                                    if (month == 8) {
                                        return "Aug";
                                    } else {
                                        if (month == 9) {
                                            return "Sep";
                                        } else {
                                            if (month == 10) {
                                                return "Oct";
                                            } else {
                                                if (month == 11) {
                                                    return "Nov";
                                                } else {
                                                    if (month == 12) {
                                                        return "Dec";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return "";
        
    }
    
    public String getDateStringWithHourAndSeconds(Date dates,String locale) throws ApplicationException {
    
        
        if (dates == null) {
            return "";
        }
        return getHour(dates) + "" + getMin(dates) + ":" + getSeconds(dates) + " " + getDay(dates) + "/" + getMonth(dates,locale) + "/" + getYear(dates) + "";
    }

    public String getDateStringWithHourReverse(Date dates) throws ApplicationException {
    	
        if (dates == null) {
            return "";
        }
        return getYear(dates) + "/" + getMonthNum(dates) + "/" + getDay(dates) + " " + getHour(dates) + "" + getMin(dates) + "h ";
    }

    public String getDateStringWithHour(Date dates,String locale) throws ApplicationException {
    	
        
        if (dates == null) {
            return "";
        }
        return getHour(dates) + "" + getMin(dates) + "h " + getDay(dates) + "/" + getMonth(dates,locale) + "/" + getYear(dates) + "";
    }
    
    public static String getXTimestamp(String month, String day, String year) throws ApplicationException {
    	
        return year + "-" + month + "-" + day + "T00:00:00";
    }
    
    public static String getXTimestampWithHour(String month, String day, String year, String hour, String min) throws ApplicationException {
        if (hour != null && hour.length() == 1) {
            hour = "0" + hour;
            
        }
        if (min != null && min.length() == 1) {
            min = "0" + min;
        }
        return year + "-" + month + "-" + day + "T" + hour + ":" + min + ":00";
    }
    
    public static String getXTimestamp(String dob) throws ApplicationException {
        String[] dob_array = dob.split("/");
        String dob_date = "";
        if (dob_array.length == 3) {
            dob_date = dob_array[2] + "-" + dob_array[1] + "-" + dob_array[0] + "T00:00:00";
        }
        //String dob = dob.replaceAll("/","-")
        return dob_date;
    }
    
    /**
    * Formats a date to the provided locale and the current timezone. Used
    * in Report generation. If the date and/or the locale are null, an empty
    * String is returned.
    *
    * @param date  A date
    * @param locale The current locale of the user
    * @return A string formatted in the locale and local timezone
    * 
    * @see com.pixalere.reporting.ReportBuilder
    * @see com.pixalere.reporting.ReportingManager
    * 
    * @since 6.0 - Spanish localization
    */
    public String getLocalizedDateString(Date date, Locale locale) throws ApplicationException {
        if (date == null || locale == null){
            return "";
        }
        
        TimeZone tz = TimeZone.getTimeZone(Common.getConfig("timezone"));
        Calendar cal = Calendar.getInstance(tz, locale);
        
        cal.setTime(date);
        
        DateFormat df = new SimpleDateFormat("dd/MMM/yyyy", locale);
        return df.format(cal.getTime());
    }
    
    public String getDateString(Date dates,String locale) throws ApplicationException {
        if (dates == null) {
            return "";
        }
        return getDay(dates) + "/" + getMonth(dates,locale) + "/" + getYear(dates);
    }
    
    public String getDateString(String d,String locale) throws ApplicationException {
        if (d == null || d.equals("")) {
            return "";
        }
        Date dates = null;
        if (d != null) {
            dates = PDate.getDateFromString(d, false);
        }
        return getDay(dates) + "/" + getMonth(dates,locale) + "/" + getYear(dates);
    }
    
    public String getDateStringWithNum(Date dates) {
        
        try {
            
            if (dates == null) {
                
                return "";
                
            }
            
            return getDay(dates) + "/" + getMonthNum(dates) + "/" + getYear(dates);
            
        } catch (NullPointerException e) {
            
            return "";
            
        } catch (ApplicationException e) {
            
            return "";
            
        }
        
    }
    
    public static String getDateAsStringWithFormat(Date date, String format, String language){
        SimpleDateFormat sf = new SimpleDateFormat(format, new Locale(language));
        
        try {
            if (date != null) {
                return sf.format(date);
            }
        } catch (Exception e) {
            return "";
        }
        return "";
    }
    
    public static String getSerializedCustomDatesDashboard(ParameterForm form){
        String dates1;

        String start_1_date_day = form.getStart_1_date_day();
        String start_1_date_month = form.getStart_1_date_month();
        String start_1_date_year = form.getStart_1_date_year();
        String end_1_date_day = form.getEnd_1_date_day();
        String end_1_date_month = form.getEnd_1_date_month();
        String end_1_date_year = form.getEnd_1_date_year();

        if ((start_1_date_day == null || start_1_date_day.isEmpty()) ||
            (start_1_date_month == null || start_1_date_month.isEmpty()) ||
            (start_1_date_year == null || start_1_date_year.isEmpty()) ||
            (end_1_date_day == null || end_1_date_day.isEmpty()) ||
            (end_1_date_month == null || end_1_date_month.isEmpty()) ||
            (end_1_date_year == null || end_1_date_year.isEmpty())) 
            {
                dates1 = "1--";
        } else {
            dates1 = "1" 
            + "-" +
            start_1_date_day  + "/" + start_1_date_month + "/" + start_1_date_year 
            + "-" +
            end_1_date_day + "/" + end_1_date_month + "/" + end_1_date_year;
        }

        String dates2;

        String start_2_date_day = form.getStart_2_date_day();
        String start_2_date_month = form.getStart_2_date_month();
        String start_2_date_year = form.getStart_2_date_year();
        String end_2_date_day = form.getEnd_2_date_day();
        String end_2_date_month = form.getEnd_2_date_month();
        String end_2_date_year = form.getEnd_2_date_year();

        if ((start_2_date_day == null || start_2_date_day.isEmpty()) ||
            (start_2_date_month == null || start_2_date_month.isEmpty()) ||
            (start_2_date_year == null || start_2_date_year.isEmpty()) ||
            (end_2_date_day == null || end_2_date_day.isEmpty()) ||
            (end_2_date_month == null || end_2_date_month.isEmpty()) ||
            (end_2_date_year == null || end_2_date_year.isEmpty())) 
            {
                dates2 = "2--";
        } else {
            dates2 = "2" 
            + "-" +
            start_2_date_day  + "/" + start_2_date_month + "/" + start_2_date_year 
            + "-" +
            end_2_date_day + "/" + end_2_date_month + "/" + end_2_date_year;
        }

        String dates3;

        String start_3_date_day = form.getStart_3_date_day();
        String start_3_date_month = form.getStart_3_date_month();
        String start_3_date_year = form.getStart_3_date_year();
        String end_3_date_day = form.getEnd_3_date_day();
        String end_3_date_month = form.getEnd_3_date_month();
        String end_3_date_year = form.getEnd_3_date_year();

        if ((start_3_date_day == null || start_3_date_day.isEmpty()) ||
            (start_3_date_month == null || start_3_date_month.isEmpty()) ||
            (start_3_date_year == null || start_3_date_year.isEmpty()) ||
            (end_3_date_day == null || end_3_date_day.isEmpty()) ||
            (end_3_date_month == null || end_3_date_month.isEmpty()) ||
            (end_3_date_year == null || end_3_date_year.isEmpty())) 
            {
                dates3 = "3--";
        } else {
            dates3 = "3" 
            + "-" +
            start_3_date_day  + "/" + start_3_date_month + "/" + start_3_date_year 
            + "-" +
            end_3_date_day + "/" + end_3_date_month + "/" + end_3_date_year;
        }

        String dates4;

        String start_4_date_day = form.getStart_4_date_day();
        String start_4_date_month = form.getStart_4_date_month();
        String start_4_date_year = form.getStart_4_date_year();
        String end_4_date_day = form.getEnd_4_date_day();
        String end_4_date_month = form.getEnd_4_date_month();
        String end_4_date_year = form.getEnd_4_date_year();

        if ((start_4_date_day == null || start_4_date_day.isEmpty()) ||
            (start_4_date_month == null || start_4_date_month.isEmpty()) ||
            (start_4_date_year == null || start_4_date_year.isEmpty()) ||
            (end_4_date_day == null || end_4_date_day.isEmpty()) ||
            (end_4_date_month == null || end_4_date_month.isEmpty()) ||
            (end_4_date_year == null || end_4_date_year.isEmpty())) 
            {
                dates4 = "4--";
        } else {
            dates4 = "4" 
            + "-" +
            start_4_date_day  + "/" + start_4_date_month + "/" + start_4_date_year 
            + "-" +
            end_4_date_day + "/" + end_4_date_month + "/" + end_4_date_year;
        }

        String dates5;

        String start_5_date_day = form.getStart_5_date_day();
        String start_5_date_month = form.getStart_5_date_month();
        String start_5_date_year = form.getStart_5_date_year();
        String end_5_date_day = form.getEnd_5_date_day();
        String end_5_date_month = form.getEnd_5_date_month();
        String end_5_date_year = form.getEnd_5_date_year();

        if ((start_5_date_day == null || start_5_date_day.isEmpty()) ||
            (start_5_date_month == null || start_5_date_month.isEmpty()) ||
            (start_5_date_year == null || start_5_date_year.isEmpty()) ||
            (end_5_date_day == null || end_5_date_day.isEmpty()) ||
            (end_5_date_month == null || end_5_date_month.isEmpty()) ||
            (end_5_date_year == null || end_5_date_year.isEmpty())) 
            {
                dates5 = "5--";
        } else {
            dates5 = "5" 
            + "-" +
            start_5_date_day  + "/" + start_5_date_month + "/" + start_5_date_year 
            + "-" +
            end_5_date_day + "/" + end_5_date_month + "/" + end_5_date_year;
        }

        String dates = dates1 + "*" + dates2 + "*" + dates3 + "*" + dates4 + "*" + dates5;
        return dates;
    }
    
    public static String getSerializedCustomDatesDashboard(DetailsSchedulerForm form){
        String dates1;
        if (isStringDateFormatDdMmYyyy(form.getStart1()) && isStringDateFormatDdMmYyyy(form.getEnd1())){
            dates1 = "1-" + form.getStart1() + "-" + form.getEnd1();
        } else {
            dates1 = "1--";
        }
        String dates2;
        if (isStringDateFormatDdMmYyyy(form.getStart2()) && isStringDateFormatDdMmYyyy(form.getEnd2())){
            dates2 = "2-" + form.getStart2() + "-" + form.getEnd2();
        } else {
            dates2 = "2--";
        }
        String dates3;
        if (isStringDateFormatDdMmYyyy(form.getStart3()) && isStringDateFormatDdMmYyyy(form.getEnd3())){
            dates3 = "3-" + form.getStart3() + "-" + form.getEnd3();
        } else {
            dates3 = "3--";
        }
        String dates4;
        if (isStringDateFormatDdMmYyyy(form.getStart4()) && isStringDateFormatDdMmYyyy(form.getEnd4())){
            dates4 = "4-" + form.getStart4() + "-" + form.getEnd4();
        } else {
            dates4 = "4--";
        }
        String dates5;
        if (isStringDateFormatDdMmYyyy(form.getStart5()) && isStringDateFormatDdMmYyyy(form.getEnd5())){
            dates5 = "5-" + form.getStart5() + "-" + form.getEnd5();
        } else {
            dates5 = "5--";
        }
        
        String dates = dates1 + "*" + dates2 + "*" + dates3 + "*" + dates4 + "*" + dates5;  
        return dates;
    }
    
    public static boolean isStringDateFormatDdMmYyyy(String string){
        if (string == null || string.isEmpty()) return false;
        
        Pattern pattern_ddmmyyyy = Pattern.compile(PDate.regex_pattern_ddmmyyyy);
        
        Matcher matcher = pattern_ddmmyyyy.matcher(string);
        return matcher.matches();
    }
    
    public static List<FiscalQuarter> getUnserializedCustomDatesDashboard(String string){
        List<FiscalQuarter> list = new ArrayList<FiscalQuarter>();
        
        String[] columns = string.split("\\*");
        
        for (String column : columns) {
            String nums = column.substring(0, 1);
            int ncol = Integer.parseInt(nums);
            
            FiscalQuarter q = new FiscalQuarter();
            q.setQuarter(ncol);
            q.setOrder(ncol);
            q.setIsBlank(true);
            if (column.length() > 3){
                // Parse dates    
                String[] dates = column.split("-");
                Date start = serializedStringToDate(dates[1]);
                Date end = serializedStringToDate(dates[2]);
                if (start != null && end != null && start.before(end)){
                    q.setStartDate(start);
                    q.setEndDate(end);
                    q.setIsBlank(false);
                }
            }
            
            list.add(q);
        }
        
        return list;
    }
    
    private static Date serializedStringToDate(String string){
        String[] datearray = string.split("/");
        Date date = null;
        try {
            date = PDate.getDate(Integer.parseInt(datearray[1]), Integer.parseInt(datearray[0]), Integer.parseInt(datearray[2]));
        } catch (NumberFormatException ex) {
        }
        return date;
    }
    
    public String getDay(Date dates) throws ApplicationException {
        String diplayMilli = "";
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int day = calendar.get(5);
        diplayMilli = "".concat(String.valueOf(String.valueOf(day)));
        
        return diplayMilli;
    }
    
    public static String getYear(Date dates, int minus_years) throws ApplicationException {
        String diplayMilli = "";
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int year = calendar.get(Calendar.YEAR);
        if (minus_years != 0) {
            year = year + minus_years;
        }
        diplayMilli = "".concat(String.valueOf(String.valueOf(year)));
        
        return diplayMilli;
    }
    
    public static String getYear(Date dates) {
        String diplayMilli = "";
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int day = calendar.get(Calendar.YEAR);
        diplayMilli = "".concat(String.valueOf(String.valueOf(day)));
        
        return diplayMilli;
    }

    /**
     * Description:
     *
     * @param dates
     * @return
     */
    public String getHour(Date dates) throws ApplicationException {
        String diplayMilli = "";
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        diplayMilli = padHour(hour);
        
        return diplayMilli;
    }
    
    public String getSeconds(Date dates) throws ApplicationException {
        String diplayMilli = "";
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int hour = calendar.get(Calendar.SECOND);
        int hr2 = calendar.get(Calendar.MILLISECOND);
        diplayMilli = hour + "." + hr2;
        
        return diplayMilli;
    }

    /**
     * Description:
     *
     * @param dates
     * @return
     */
    public String getMin(Date dates) throws ApplicationException {
        
        String diplayMilli = "";
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int min = calendar.get(Calendar.MINUTE);
        diplayMilli = "".concat((String.valueOf(String.valueOf(min)).length() == 1 ? "0" + min : min + ""));
        
        return diplayMilli;
    }

    /**
     * Description:
     *
     * @param dates
     * @return
     */
    public String getMonth(Date dates,String locale) throws ApplicationException {
        String diplayMilli = "";
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int month = calendar.get(2);
        diplayMilli = getMMM((month + 1),locale);
        
        return diplayMilli;
    }
    
    public String getMonthNum(Date dates) throws ApplicationException {
        String diplayMilli = "";
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Common.getConfig("timezone")));
        calendar.setTime(dates);
        int month = calendar.get(2);
        diplayMilli = (month + 1) + "";
        
        return diplayMilli;
    }
    
    public static String getAge(String strYear, String strMonth,
            String strDay) {
        
        String strReturnAge = "";
        if (strYear != null && strMonth != null && strDay != null && !strYear.equals("0") && !strMonth.equals("0") && !strDay.equals("0")) {
            try {
                int intDobYear = Integer.parseInt(strYear);
                int intDobMonth = Integer.parseInt(strMonth);
                int intDobDay = Integer.parseInt(strDay);
                //Date date = new Date();
                MyCalendar updateDate = new MyCalendar();
                //updateDate.setGregorianChange(date);
                updateDate.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE));
                long today = getEpochTime();
                updateDate.set(updateDate.get(Calendar.YEAR), updateDate.get(2), updateDate.get(5), 1, 1, 1);
                
                MyCalendar birthDate = new MyCalendar();
                birthDate.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE));
                
                if ((intDobMonth != 0 && intDobYear != 0 && intDobDay != 0) && (intDobMonth != -1 && intDobYear != -1 && intDobDay != -1)) {
                    try {
                        birthDate.set(intDobYear,
                                intDobMonth - 1, intDobDay, 1, 1, 1);
                        
                    } catch (Exception excepti) {
                        excepti.printStackTrace();
                    }
                } else {
                    //return null since a date cannot be set without all three parameters being
                    //selected.
                }
                
                int intAge = updateDate.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);
                
                if (intAge < 0) {
                    strReturnAge = "Invalid Birth Date";
                } else {
                    if (updateDate.get(Calendar.DAY_OF_YEAR) < birthDate.get(Calendar.DAY_OF_YEAR)) {
                        intAge--;
                    }
                }
                
                strReturnAge = "" + intAge;
                if (intAge < 1) {
                    int intUpdateMonth = updateDate.get(Calendar.MONTH) + 1;
                    if (intDobMonth > intUpdateMonth) {
                        intUpdateMonth = intUpdateMonth + 12;
                    }
                    int intLessThanYear = intUpdateMonth - intDobMonth;
                    if (updateDate.get(Calendar.DAY_OF_MONTH) < intDobDay) {
                        intLessThanYear--;
                    }
                    if (intLessThanYear == 1) {
                        strReturnAge = "1 Month";
                    } else {
                        strReturnAge = intLessThanYear + " Months";
                    }
                }
                
            } catch (Exception exception) {
                
                strReturnAge = "Invalid Birth Date";
            }
        }
        return strReturnAge;
    }
}
