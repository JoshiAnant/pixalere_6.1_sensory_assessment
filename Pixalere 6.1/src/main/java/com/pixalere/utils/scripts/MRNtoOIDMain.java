/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils.scripts;

import com.pixalere.common.ApplicationException;
import com.pixalere.utils.*;
import net.sourceforge.jtds.jdbc.*;
import java.sql.*;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import java.util.HashMap;
import java.util.Set;
import com.pixalere.utils.PDate;
import java.util.Iterator;

/**
 * @deprecated
 * @author travis
 */
public class MRNtoOIDMain {
    private static HashMap<String, String> map = new HashMap();
    private void init() throws Exception {
        
        
        //Retrieve all oids
//
        //System.out.println("Getting all patients " + new java.util.Date().getTime() + "on pixalere_live");

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");

        } catch (java.lang.ClassNotFoundException e) {
           System.out.println(e.getMessage());
        }
        //UPDATE patient_accounts set secondary_phn =  REPLACE(secondary_phn,'AR','MO');

        Connection con;

        con = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost/pixalere_live", "pixalere", "donthugth@3");
        PreparedStatement state = con.prepareStatement("select id,secondary_phn from patient_accounts where patient_id IS NULL AND secondary_phn NOT LIKE '%-%'");
        ResultSet result = state.executeQuery();
        int cnt = 0;
        while (result.next()) {
            cnt++;
            if (cnt % 1000 == 0) {
                System.out.println(cnt + " patients are done.");
            }
            int count = 0;
            int id = result.getInt("id");
            String secondary_phn = result.getString("secondary_phn");
            if(secondary_phn!=null && secondary_phn.indexOf("-")!=-1){//do nothing
                System.out.println("Patient Already contains a dash: "+secondary_phn);
            }else{
                String oid = getOID(secondary_phn);
                if(oid==null || oid.equals("")){
                    System.out.println("OID Error or empty: "+oid+" from : "+secondary_phn);
                }else{
                    String sql = "update patient_accounts set secondary_phn='" + oid + "' where id=" + id;
                    //System.out.println(sql);
                    Statement stmt = con.createStatement();
                    //sql = "update patient_accounts set name='"+firstname+"',lastname='"+lastname+"',treatment_location_id='"+treatment+"' where id="+id;
                    stmt.executeUpdate(sql);
                }
            }
        }





    }

    public String getOID(String mrn) {

        String oid = "";
        if (mrn!=null && !mrn.equals("")) {
            //facility prefix
            String facility = mrn.substring(0, 1);
            String second_char_check = "";
            if(mrn.length()>1){
                second_char_check= mrn.substring(1, 2);
            }//checking if this mrn has 2 alpha characters..
            Set keys = map.keySet();
            Iterator iter = keys.iterator();
            boolean found = false;
            try {
                Integer.parseInt(facility);
                //no facility included, otherwise would have thrown exception.
                oid = "KG" + mrn;
            } catch (NumberFormatException e) {
                //facility included
                try {
                    Integer.parseInt(second_char_check);

                    while (iter.hasNext()) {
                        String key = (String) iter.next();
                        if (key.equals(facility)) {
                            String newFacility = (String) map.get(key);
                            String unitNumber = mrn.substring(1, mrn.length());
                            oid = newFacility + unitNumber;
                            found = true;
                        }
                    }
                    if (found == false) {
                        System.out.println("Facililty was not found for: " + mrn);
                        oid=mrn;
                    }
                } catch (NumberFormatException er) {
                    oid = mrn;
                    //System.out.println("MRN already seems to have a legit facility: "+mrn);
                    //this is where we would have added AR to MO.. but there's no AR's in pixaler so its not needed.
                } catch (Exception ex){
                    //ex.printStackTrace();
                    System.out.println("Exception: "+ex.getMessage()+" for: "+mrn);
                    oid=mrn;
                }
            }

        } else {
            System.out.println("MRN is null: " + mrn);
            oid=mrn;
        }
        if(oid!=null){
            oid = "A"+getEvenOdd(oid)+"-." +oid;
        }
        return oid;
    }
    public int getEvenOdd(String oid){
        String lastDigit = oid.substring(oid.length()-1,oid.length());
        int binaryNum=1;
        try{
            int num = Integer.parseInt(lastDigit);
            if (num % 2 == 0) {
                //even
                binaryNum = 0;
            }


        }catch(NumberFormatException ex){

        }
        return binaryNum;
    }
    public void testConversion() throws Exception {
         try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");

        } catch (java.lang.ClassNotFoundException e) {
           System.out.println(e.getMessage());
        }

        Connection con;

        con = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost/pixalere_live", "pixalere", "donthugth@3");
        PreparedStatement state = con.prepareStatement("select p1.patient_id,p1.secondary_phn as urn,p2.secondary_phn as oid from v411_pixalere_live.dbo.patient_accounts as p1 LEFT JOIN v4112_pixalere_live.dbo.patient_accounts as p2 ON p1.id=p2.id WHERE p1.current_flag=1");
        ResultSet result = state.executeQuery();
        int cnt = 0;
        while (result.next()) {
            cnt++;
            if (cnt % 1000 == 0) {
                System.out.println(cnt + " patients are done.");
            }
            int count = 0;
            Integer id = result.getInt("patient_id");
            String urn = result.getString("urn");
            String oid = result.getString("oid");

            if((oid==null || urn==null)){
                System.out.println("Error"+cnt+":  URN: or OID is NULL");
            }else{
                String oid2 = oid.replaceAll("A0-.", "");
                oid2 = oid2.replaceAll("A1-.", "");
                Set keys = map.keySet();
                Iterator iter = keys.iterator();
                while(iter.hasNext()){
                    String key = (String)iter.next();
                    String value = map.get(key);
                    if(oid2.indexOf(value)!= -1){
                        oid2 = oid2.replaceAll(value,"");
                    
                        oid2=key+oid2;
                    }
                }
                
                oid2=oid2.trim();
                oid2=oid2.replaceAll("KG", "");
                if(!urn.equals(oid2)){
                    System.out.println("Error "+cnt+": URN does not match deconstructed OID: urn: "+urn+" oid "+oid2);
                }
            }


        }






    }
    public static void main(String[] args) {
        try {
            MRNtoOIDMain xlReader = new MRNtoOIDMain();
            map.put("F", "FE");
            map.put("H", "SG");
            map.put("K", "SZ");
            map.put("M", "BM");
            map.put("N", "PR");
            map.put("Q", "PG");
            map.put("Z", "SO");
            if (args[0].equals("run") ) {
                xlReader.init();
            }else if(args[0].equals("testConversion")){

               // xlReader.testConversion();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
