package com.pixalere.utils.scripts;

/**
 * Meditech patient dump, ensure patient data in Pixalere matches.
 *
 * @deprecated
 */
import com.pixalere.auth.service.ProfessionalServiceImpl;
import java.io.*;
import java.util.Locale;
import java.util.Collection;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.DateCell;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.integration.service.IntegrationServiceImpl;
import com.pixalere.integration.bean.Patient;
import com.pixalere.integration.bean.Event;
import com.pixalere.integration.bean.Gender;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import javax.xml.datatype.XMLGregorianCalendar;

public class FixPatients {

    public void runme() {
        DataSource ds = null;
        System.out.println("Migrate from Java");
        /* try {
         Context initContext = new InitialContext();
         Context envContext = (Context) initContext.lookup("java:/comp/env");
         ds = (DataSource) envContext.lookup("jdbc/pixDS");
         } catch (NamingException ne) {
         throw new RuntimeException("Unable to aquire data source", ne);

         }*/
        try {

            Class.forName("net.sourceforge.jtds.jdbc.Driver");

        //UPDATE patient_accounts set secondary_phn =  REPLACE(secondary_phn,'AR','MO');
            Connection con;

            con = DriverManager.getConnection("jdbc:jtds:sqlserver://wssql05v02/pixalere;instance=prdinst02", "pixalere", "p1xy");
            /*Connection con;
             System.out.println("conn: ");
             con = ds.getConnection();*/
            System.out.println("conn: " + con);

            Statement stmt = con.createStatement();

            PreparedStatement alphas_stmt
                    = con.prepareStatement(
                            "select  "
                            + "patient_id"
                            
                            + " from patient_accounts "
                            + " where account_status=1 and current_flag=1 "
                            + "  and  not exists (select id from wounds where patient_id=patient_accounts.patient_id and (wounds.status IS NULL OR wounds.status = '')) and exists (select id from wounds where patient_id=patient_accounts.patient_id)"
                            ,
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);

            ResultSet alpha_query = alphas_stmt.executeQuery();
            alpha_query.setFetchSize(100);
            PrintWriter writer = new PrintWriter("c:\\runscripts.sql", "UTF-8");

            while (alpha_query.next()) {
                int patient_id = alpha_query.getInt("patient_id");
                writer.println("update patient_accounts set current_flag=0 where patient_id="+patient_id+";");
                writer.println("insert into patient_accounts (current_flag,account_status,patient_id,name,lastname,lastname_search,treatment_location_id,phn,phn2,phn3,outofprovince,discharge_reason,created_on,created_by,user_signature,gender,patient_residence,dob,allergies,funding_source,discharge_date,middle_name,action,version_code) select top 1 1,0,"+patient_id+",name,lastname,lastname_search,treatment_location_id,phn,phn2,phn3,outofprovince,3210,'2015-06-23 00:00:00',6,'0000h 23/Jun/2015 - UpdateScripts #6 PS',gender,patient_residence,dob,allergies,funding_source,'2015-06-23',middle_name,'DISCHARGE',version_code  from patient_accounts where patient_id="+patient_id+"   order by created_on desc;");
                //hide referrals/recommendations
                writer.println("update referrals_tracking set current_flag=0 where (entry_type=3 or entry_type=0 OR entry_type=1) and current_flag=1 and patient_id="+patient_id+";");
                /*int alpha_id = alpha_query.getInt("id");
                int patient = alpha_query.getInt("patient_id");
                int wound_profile_type_id = alpha_query.getInt("wound_profile_type_id");
                int wound_id = alpha_query.getInt("wound_id");*/
                //String wassess = "insert into wound_assessment (patient_id,wound_id,created_on,professional_id,active,user_signature,lastmodified_on,visited_on) values (" + patient + "," + wound_id + ",'2015-06-23 00:00:00',6,1,'0000h Jun/26/2015 - UpdateScripts #6 PS','2015-06-23 00:00:00','2015-06-23 00:00:00')";
                //System.out.println(wassess);
                try {
                    /*stmt.executeUpdate(wassess);
                    String get = "select top 1 id from wound_assessment where wound_id=" + wound_id + " order by created_on desc";
                    PreparedStatement stateback = con.prepareStatement(get, ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);

                    ResultSet resultback = stateback.executeQuery();
                    resultback.setFetchSize(100);
                    while (resultback.next()) {
                        int assessment_id = resultback.getInt("id");
                        if (assessment_id > 0) {
                            String assess = "insert into assessment_wound (patient_id, wound_id, alpha_id,assessment_id,pain,length_cm,depth_cm,width_cm,length_mm,depth_mm,width_mm,exudate_odour,wound_edge, periwound_skin,exudate_amount,exudate_colour,status,discharge_reason,exudate_type,active,recurrent,full_assessment,wound_profile_type_id,created_on,closed_date) values (" + patient + "," + wound_id + "," + alpha_id + "," + assessment_id + ",0,0,0,0,0,0,0,-1,'a:0:{}','a:0:{}',0,'a:0:{}',258,3211,'a:0:{}',1,1,2," + wound_profile_type_id + ",'2015-06-23 00:00:00','2015-06-23 00:00:00');";
                            writer.println(assess);
                            //alphas_stmt.executeUpdate(assess);
                        }
                    }
                    String update_alpha = "update wound_assessment_location set discharge=1,close_time_stamp='2015-06-23 00:00:00' where id=" + alpha_id + ";";
                    writer.println(update_alpha);
                    //alphas_stmt.executeUpdate(update_alpha);
                    String update_wpt = "update wound_profile_type set closed=1,closed_date='2015-06-23 00:00:00' where id=" + wound_profile_type_id + " and NOT EXISTS (select id from wound_assessment_location where discharge=0 and wound_profile_type_id=" + wound_profile_type_id + ");";
                    writer.println(update_wpt);
                    //alphas_stmt.executeUpdate(update_wpt);
                    String update_wp = "update wounds set status='Closed', closed_date='2015-06-23 00:00:00' where id=" + wound_id + " and NOT EXISTS (select id from wound_profile_type where wound_id=" + wound_id + " and closed=0);";
                    //alphas_stmt.executeUpdate(update_wp);
                    writer.println(update_wp);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            writer.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        try {
            FixPatients xlReader = new FixPatients();

            xlReader.runme();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
/* RUN
 java  -cp c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\jxl.jar;.;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-lang-2.4.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-beanutils-1.7.0.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\jtds-1.2.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\log4j-1.2.15.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\db-ojb-1.0.4-tools.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\db-ojb-1.0.4.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-resources.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-collections-3.2.1.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-digester-1.7.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-logging-1.1.1.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-pool-1.2.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-dbcp-1.2.1.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\struts.jar com.pixalere.utils.FixPatients

 */
