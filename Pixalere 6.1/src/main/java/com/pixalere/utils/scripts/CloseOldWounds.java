/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.pixalere.utils.scripts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Set;
import java.io.*;

/**
 *
 * @author travis
 */
public class CloseOldWounds {

    private final int CLOSED = 246;
    private final int OPEN = 245;
    private final int DISCHARGE_REASON = 3416;
    private final String TIME = "2015-04-01";
    private final int PROF_ID = 6;
    private final String SIG = "2015/Apr/01 00:00h PDT - #6 Upgrade Scripts";
    private final String TODAY = "2015-09-16 00:00:27.000";

    private void init() throws Exception {

        //Retrieve all oids
//
        //System.out.println("Getting all patients " + new java.util.Date().getTime() + "on pixalere_live");
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");

        } catch (java.lang.ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        //UPDATE patient_accounts set secondary_phn =  REPLACE(secondary_phn,'AR','MO');

        
        Connection con;
        StringBuffer str = new StringBuffer();
        StringBuffer str2 = new StringBuffer();
        //con = DriverManager.getConnection("jdbc:jtds:sqlserver://DC1Serv903/pixalere_live", "pixalere", "donthugth@3");
        con = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost/pixalere_testv6", "pixalere", "donthugth@3");
        PreparedStatement state = con.prepareStatement("select alpha_id,max(created_on) as created_on from assessment_wound where  exists(select id from patients where id=patient_id) and exists(select id from wound_assessment_location where id=alpha_id) and NOT EXISTS (select id from assessment_wound a where a.active=1 and a.alpha_id=assessment_wound.alpha_id and a.status = " + CLOSED + ") and NOT EXISTS (select id from assessment_wound a where a.alpha_id=assessment_wound.alpha_id and a.created_on>='" + TIME + "') group by alpha_id order by created_on desc");
        System.out.println("select alpha_id,max(created_on) as created_on from assessment_wound where  exists(select id from patients where id=patient_id) and exists(select id from wound_assessment_location where id=alpha_id) and NOT EXISTS (select id from assessment_wound a where a.active=1 and a.alpha_id=assessment_wound.alpha_id and a.status = " + CLOSED + ") and NOT EXISTS (select id from assessment_wound a where a.alpha_id=assessment_wound.alpha_id and a.created_on>='" + TIME + "') group by alpha_id order by created_on desc");
        ResultSet result = state.executeQuery();
        int cnt = 0;
        while (result.next()) {
            try {
                cnt++;
                if (cnt % 1000 == 0) {
                    System.out.println(cnt + " assessments are done.");
                }
                int count = 0;

                int alpha_id = result.getInt("alpha_id");

                PreparedStatement state7 = con.prepareStatement("select top 1 patient_id,id,wound_id,wound_profile_type_id,alpha_id from assessment_wound where alpha_id=" + alpha_id + " order by created_on desc");
                System.out.println("select top 1 patient_id,id,wound_id,wound_profile_type_id,alpha_id from assessment_wound where alpha_id=" + alpha_id + " order by created_on desc");
                ResultSet result7 = state7.executeQuery();
                result7.next();
                int id = result7.getInt("id");
                int wound_id = result7.getInt("wound_id");
                int patient_id = result7.getInt("patient_id");
                int wound_profile_type_id = result7.getInt("wound_profile_type_id");

                String wassess = "insert into wound_assessment (patient_id,treatment_location_id,wound_id,active,professional_id,user_signature,has_comments,visit,reset_visit,visited_on,created_on,lastmodified_on) select top 1 " + patient_id + ",treatment_location_id," + wound_id + ",1," + PROF_ID + ",'" + SIG + "',0,0,0,'" + TODAY + "','" + TODAY + "','" + TODAY + "' from patient_accounts where patient_id=" + patient_id + " and current_flag=1 ";
                System.out.println(wassess);
                Statement stmt3 = con.createStatement();
                stmt3.executeUpdate(wassess);
                str.append(wassess);
                stmt3.close();
                PreparedStatement state2 = con.prepareStatement("select top 1 id from wound_assessment where wound_id=" + wound_id + " order by created_on desc");
                ResultSet result2 = state2.executeQuery();
                
                while (result2.next()) {
                    int assess_id = result2.getInt("id");
                    String assess = "insert into assessment_wound(patient_id,wound_id,wound_profile_type_id,assessment_id,alpha_id,closed_date,status,discharge_reason,created_on,visited_on,lastmodified_on) values (" + patient_id + "," + wound_id + "," + wound_profile_type_id + "," + assess_id + "," + alpha_id + ",'" + TODAY + "'," + CLOSED + "," + DISCHARGE_REASON + ",'" + TODAY + "','" + TODAY + "','" + TODAY + "')";

                    String alpha = "update wound_assessment_location set discharge=1,close_time_stamp='" + TIME + "' where id=" + alpha_id;
                    str.append(assess);
                    str.append(alpha);
                    Statement stmt = con.createStatement();
                    stmt.executeUpdate(assess);
                    stmt.close();
                    Statement stmt2 = con.createStatement();
                    stmt2.executeUpdate(alpha);
                    stmt2.close();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        String update_wpt = "update wound_profile_type set closed=1,closed_date='" + TODAY + "' where   closed=0 and NOT eXISTS (select * from wound_assessment_location wal where wound_profile_type_id=wound_profile_type.id and discharge=0)";
        String update_wp = "update wounds set status='Closed',closed_date='" + TODAY + "' where start_date<='"+TIME+"' and status IS NULL and NOT eXISTS (select * from wound_assessment_location wal where wound_id=wounds.id and discharge=0)";
        Statement stmt4 = con.createStatement();
        stmt4.executeUpdate(update_wpt);
        str2.append(update_wpt);
        stmt4.close();

        Statement stmt6 = con.createStatement();
        stmt6.executeUpdate(update_wp);
        str2.append(update_wp);
        
        //System.out.println(str);
        writeToFile("sql1.sql", str);
        writeToFile("sql2.sql", str2);

        //System.out.println("=-=-=-=-=-=-=-=-=-=");
        //System.out.println(str2);
    }

    public static void writeToFile(String pFilename, StringBuffer pData) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(pFilename));
        out.write(pData.toString());
        out.flush();
        out.close();
    }

    public static StringBuffer readFromFile(String pFilename) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(pFilename));
        StringBuffer data = new StringBuffer();
        int c = 0;
        while ((c = in.read()) != -1) {
            data.append((char) c);
        }
        in.close();
        return data;
    }

    public static void main(String[] args) {
        try {
            CloseOldWounds xlReader = new CloseOldWounds();

            if (args[0].equals("run")) {
                xlReader.init();
            } else if (args[0].equals("testConversion")) {

                // xlReader.testConversion();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
/* RUN
 java  -cp "c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\jxl.jar;.;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-lang-2.4.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-beanutils-1.7.0.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\jtds-1.2.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\log4j-1.2.15.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\db-ojb-1.0.4-tools.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\db-ojb-1.0.4.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-resources.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-collections-3.2.1.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-digester-1.7.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-logging-1.1.1.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-pool-1.2.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\commons-dbcp-1.2.1.jar;c:\Program Files\Apache Software Foundation\Tomcat 7.0\shared\libs\\struts.jar" com.pixalere.utils.scripts.CloseOldWounds run

 */
