/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.utils;

import com.pixalere.utils.task.*;
import javax.servlet.ServletContextListener;
import java.util.TimerTask;
import java.util.Timer;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.quartz.Trigger;
import org.quartz.JobBuilder;
import org.quartz.CronScheduleBuilder;
import org.quartz.TriggerBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

/**
 * A Quartz scheduler.
 *
 * @since 5.0
 * @author travis morris
 */
public class SchedulerListener implements ServletContextListener {

    ServletContext servletContext;
    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private Scheduler sche = null;

    /**
     * Overriding contextInitialized, creating all the crontriggers needed for
     * Pixalere. We moved all the cron/windows based scheduelrs to Quartz for
     * v5.
     *
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            servletContext = servletContextEvent.getServletContext();
            //verify database is update to date.. if its not don't start any schedulers.
            //if (Common.isDBPatched() == true) {
                SchedulerFactory sf = new StdSchedulerFactory();
                sche = sf.getScheduler();
                sche.getContext().put("servletContext", servletContext);
                sche.start();
                //"0 0 12 * * ?" Fire at 12pm (noon) every day
                //"0/2 * * * * ?" Fire at every 2 seconds every day
                // 0 0 1 1  * ? Fire at 1am on the first of the month.
                // secs mins hours day month year

                //Dashboard Report
                /*JobDetail jDetail = JobBuilder.newJob(DashboardReport.class).withIdentity("Dashboard" + new java.util.Date().getTime(), "dTask").build();
                //JobDetail jDetail = new JobDetail("Dashboard" + new java.util.Date().getTime(), "dTask", DashboardReport.class);
                Trigger crTrigger = TriggerBuilder
                        .newTrigger()
                        .withIdentity("cronTrigger"+ new java.util.Date().getTime(), "NJob")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0 0 2 1 * ?")).build();
                sche.scheduleJob(jDetail, crTrigger);*/
                
                /*JobDetail jsecDetail = JobBuilder.newJob(Task30Seconds.class).withIdentity("30Seconds" + new java.util.Date().getTime(), "dTask").build();
                Trigger crsecTrigger =   TriggerBuilder
                        .newTrigger()
                        .withIdentity("cronTrigger30Seconds"+ new java.util.Date().getTime(), "NJob")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0,30 * * * * ?")).build();
                sche.scheduleJob(jsecDetail, crsecTrigger);*/
                
                //User Stats
                if(Common.getConfig("userStats").equals("1")){
                    JobDetail jDetailStats =JobBuilder.newJob(WeeklyUserStats.class).withIdentity("UserStats" + new java.util.Date().getTime(), "dTask").build();
                    JobDetail juserStats =JobBuilder.newJob(WeeklyUserStats.class).withIdentity("Monthly User Stats" + new java.util.Date().getTime(), "dTask").build();

                    Trigger crMonthlyStats = TriggerBuilder
                        .newTrigger()
                        .withIdentity("cronMonthlyStats"+ new java.util.Date().getTime(), "NJob")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0 7 8 1 * ?")).build();
                    Trigger crTriggerStats = TriggerBuilder
                        .newTrigger()
                        .withIdentity("cronTriggerStats"+ new java.util.Date().getTime(), "NJob")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0 29 7 ? * TUE")).build();
                    sche.scheduleJob(jDetailStats, crTriggerStats);
                    sche.scheduleJob(juserStats, crMonthlyStats);
                }
                //CronTrigger crTrigger = new CronTrigger("cronTrigger", "NJob", "0 53 * * * ?");

                if (Common.getConfig("integration").equals("flatfile")) {
                    JobDetail jdetail =JobBuilder.newJob(PushXML.class).withIdentity("XMLPush" + new java.util.Date().getTime(), "dTask").build();

                    Trigger crtrigger2 = TriggerBuilder
                        .newTrigger()
                        .withIdentity("ctrigger3"+ new java.util.Date().getTime(), "NJob")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0 4,8,12,15,17,20,22,25,30,34,35,42,45,48,50,53,55,59 * * * ?")).build();
                    sche.scheduleJob(jdetail, crtrigger2);
                }

                if (Common.getConfig("enableDataExtraction").equals("1")) {
                    JobDetail jDetail2 = JobBuilder.newJob(PatientFileReport.class).withIdentity("PatientFile" + new java.util.Date().getTime(), "dTask").build();
                    Trigger crtrigger2 = TriggerBuilder
                        .newTrigger()
                        .withIdentity("cronTrigger2"+ new java.util.Date().getTime(), "NJob")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0 15,30,45,0 * * * ?")).build();
                    sche.scheduleJob(jDetail2, crtrigger2);
                }
                //check database for reports currently queued.
                JobDetail jdetail = JobBuilder.newJob(ReportQueue.class).withIdentity("ReportQueue" + new java.util.Date().getTime(), "dTask").build();
                Trigger ctrig = TriggerBuilder
                        .newTrigger()
                        .withIdentity("cronTrigger1"+ new java.util.Date().getTime(), "NJob")
                        .withSchedule(CronScheduleBuilder.cronSchedule("0 0,10,20,30,40,50 * * * ?")).build();
                sche.scheduleJob(jdetail, ctrig);
                //System.out.println("Scheduled Report Queue");
           // }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

        scheduler.shutdownNow();
        try {
            sche.shutdown();
        } catch (Exception e) {
        }
    }
}