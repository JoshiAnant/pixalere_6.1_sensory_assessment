/*
 * Constants.java
 *
 * Created on June 11, 2007, 10:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.utils;


/*
 *
 * @author travismorris
 */
public class Constants {
    //service password
    public static final int[] IMG_WIDTHS = {2000,1600,1280,640};
    public static final String ADMIN_PASS="cyclo.carbon1!";//password used by Pixalere developers only for upgrading db.
    public static final String PW_TEXT="cu@chwk10s";
    public static final String PHN_KEY="pass4phn";
    public static final String FIRSTNAME_KEY="pass4firstname";
    public static final String LASTNAME_KEY="pass4lastname";
  
    public final static long SIX_MONTHS 		= 15552000; //six months (180 days, actually) in seconds
    public final static long ONE_DAY			= 86400;	//one day in seconds
    public final static long EXPIRY_INTERVAL	= SIX_MONTHS; //passwords expire after this many seconds
    public final static int DAY=86400;
    public final static int HOUR=3600;
    public final static int FOUR_MONTHS=10364000;
    public final static int NO_TREATMENT_LOCATION=999999;//in 4.2.. move to listdata
    public static final int PATIENT_RESIDENCE_OUTOFPROVINCE = 40;
    public static  String TIMEZONE="America/Vancouver";//EST: Eastern/Montreal//PST
    //alphas
    public static final String[] ALPHAS ={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    public static final String INCISION_ALPHA_GLOBAL="incs";
    public static final String[] INCISION_ALPHA = {"tag1","tag2","tag3","tag4","tag5","tag6","tag7","tag8","tag9"};
    public static final String[] OSTOMY_ALPHAS = {"ostu","ostf"};
    public static final String[] OSTOMY_ALPHAS_TITLE = {Common.getLocalizedString("pixalere.viewer.form.assessment_urinalstoma","en"),
                                                        Common.getLocalizedString("pixalere.viewer.form.assessment_fecalstoma","en")};
    public static final String[] DRAIN_ALPHAS = {"td1","td2","td3","td4","td5","td6","td7","td8","td9"};
    public static final String BURN_ALPHA_GLOBAL="burn";

    public static final int ENGLISH=1;

    public static final String[] SKIN_ALPHAS={"s1","s2","s3","s4","s5","s6","s7","s8","s9","s10","s11","s12","s13","s14","s15","s16","s17","s18","s19","s20"};
   
    public static final String INCISION_PROFILE_TYPE="I";
    public static final String INCISIONTAG_PROFILE_TYPE="T";
    public static final String TUBESANDDRAINS_PROFILE_TYPE="D";
    public static final String OSTOMY_PROFILE_TYPE="O";
    public static final String WOUND_PROFILE_TYPE="A";
    public static final String BURN_PROFILE_TYPE="B";
    public static final String SKIN_PROFILE_TYPE="S";
    
//need to find out which sites are part of each below to write conversion script.
    //incision typeslcrc
    public static final String[] INCISION_IMG_ALPHAS = {"horz","hcrz","hcrt","hcrb","hclt","cubr","culb","cult","cutr","lbrt","ltrb","vclb","vclt","vcrb","vcrt","vert","lcrc","tcbc","lcbc"};
    
    //current format
    public static final int REFERRAL=0;
    public static final int RECOMMENDATION =1;
    public static final int ACKNOWLEDGE_RECOMMENDATION=2;
    public static final int MESSAGE_FROM_NURSE=3;
    public static final int MESSAGE_FROM_CLINICIAN=4;
    public static final int ACKNOWLEDGE_MESSAGE=5;
    public static final String AUTO_REFERRAL_DCF="Dressing Change Frequency";
    public static final String AUTO_REFERRAL_HEALRATE="Heal Rate";
    public static final String AUTO_REFERRAL_ANTIMICROBIAL="Antimicrobial";
    
    /*should switch to this format later....
    public static final String MESSAGE="MSG";
    public static final String ACKNOWLEDGE_MESSAGE="ACK_MSG";
    public static final String NO_MESSAGE="NONE";*/
    
    public static final int YES = 1;
    public static final int NO=0;
    
    //pages
    public static final int PATIENT_PROFILE=1;
    public static final int WOUND_PROFILE=2;//should only show on wound specific pages
    public static final int WOUND_ASSESSMENT=3;
    public static final int OSTOMY_ASSESSMENT=4;
    public static final int INCISION_ASSESSMENT=5;
    public static final int DRAIN_ASSESSMENT=6;
    public static final int WOUND=7;//wounds data
    public static final int OSTOMY_PROFILE=8;//should only show ostoy specific data
    public static final int POSTOP_PROFILE=9;
    public static final int TUBES_DRAINS_PROFILE=10;
    public static final int LIMB_ASSESSMENT_BASIC=11;
    public static final int LIMB_ASSESSMENT_ADVANCED=52;
    public static final int LIMB_ASSESSMENT_UPPER=53;
    public static final int DIAGNOSTIC_INVESTIGATION=60;
    public static final int FOOT_ASSESSMENT=12;
    public static final int SENSORY_ASSESSMENT=301;
    public static final int BRADEN_SCORE_SECTION=13;
    public static final int WOUND_PROFILE_COMMON=14;
    public static final int BURN_PROFILE=41;
    public static final int SKIN_PROFILE=42;
    //global assessment
    public static final int ASSESSMENT=14; // wound_assessment (we should rename table to assessment)
    //mini pages
    public static final int TUBESDRAINS_SECTION=15;
    public static final int TREATMENT_COMMENTS_SECTION=16;

    public static final int SINUS_TRACTS_SECTION=19;
    public static final int UNDERMINING_SECTION=20;
    public static final int WOUNDBED_SECTION=21;
    public static final int SEPERATION_SECTION=23;
    public static final int PP_ARRAY_SECTION=24;
    public static final int FOOT_ARRAY_SECTION=25;
    public static final int LIMB_ARRAY_SECTION=26;
    public static final int TREATMENT_ARRAY_SECTION=27;
    public static final int ASS_WOUND_ARRAY_SECTION=28;
    public static final int ASS_INCISION_ARRAY_SECTION=29;
    public static final int ASS_DRAIN_ARRAY_SECTION=30;
    public static final int ASS_OSTOMY_ARRAY_SECTION=31;
    public static final int ASSESSMENT_ANTIBIOTICS=38;
    public static final int WP_ARRAY_SECTION=32;
    public static final int ETIOLOGY_SECTION=33;
    public static final int ACQUIRED_SECTION=43;
    public static final int GOALS_SECTION=34;
    public static final int INVESTIGATION_SECTION=35;
    public static final int ASSESSMENT_TREATMENT=36;
    public static final int DCF_SECTION=37;
    public static final int BURN_ASSESSMENT=17;
    public static final int NURSING_CARE_PLAN=18;
    public static final int EXTERNAL_REFERRALS_SECTION=61;
    public static final int PHYSICAL_EXAM=40;
    public static final int SKIN_ASSESSMENT=50;
    public static final int NPWT_SECTION=51;
    public static final int PATIENT_ACCOUNT=100;
    public static final String DATE_FORMAT="dd/MMM/yyyy";
    public static final String DATETIME_FORMAT="HH:mm'h' dd/MMM/yyyy";
    //component types
    public static final String NUMBER_COMPONENT_TYPE="NUMBER";
    public static final String REFERRAL_COMPONENT_TYPE="REF";
    public static final String ASSESSMENT_TYPE_COMPONENT_TYPE="AT";
    public static final String MULTI_DROPDOWN_DATE_COMPONENT_TYPE="MDDA";//antibiotics
    public static final String DROPDOWN_DATE_COMPONENT_TYPE="DDA";//investgations
    public static final String TEXTFIELD_COMPONENT_TYPE="TXT";
    public static final String TEXTAREA_COMPONENT_TYPE="TXTA";
    public static final String DROPDOWN_COMPONENT_TYPE="DRP";
    public static final String MULTI_DROPDOWN_COMPONENT_TYPE="MDRP";
    public static final String DATE_COMPONENT_TYPE="DATE";
    public static final String DATETIME_COMPONENT_TYPE="DATETIME";
    public static final String NEWDATE_COMPONENT_TYPE="NEWDATE";
    public static final String RADIO_COMPONENT_TYPE="RADO";
    public static final String MULTI_MEASUREMENT_COMPONENT_TYPE="MMAS";
    public static final String MEASUREMENT_COMPONENT_TYPE="MEAS";
    public static final String ONE_MEASUREMENT_COMPONENT_TYPE="OMEA";//single measurement like mm isntead of cm.mm
    public static final String MULTI_PERCENTAGES_COMPONENT_TYPE="MPER";
    public static final String MULTI_BURN_ASSESSMENT_COMPONENT_TYPE="MBAS";
    public static final String CHECKBOX_COMPONENT_TYPE="CHKB";
    public static final String COMMENTS_CHECK="COMM";
    public static final String Y_COMPONENT_TYPE="Y";
    public static final String ONOFFNA_COMPONENT_TYPE="OON";
    public static final String YN_COMPONENT_TYPE="YN";
  
    public static final String YNN_COMPONENT_TYPE="YNN";
    public static final String YNWHY_COMPONENT_TYPE = "YNWHY";
    public static final String SHOWHIDE_COMPONENT_TYPE="SHOWHIDE";
    public static final String AN_COMPONENT_TYPE="AN";
    public static final String IMAGE_COMPONENT_TYPE="IMG";
    public static final String IGNORE_COMPONENT_TYPE="IGNO";
    public static final String PRODUCTS_COMPONENT_TYPE="PROD";
    public static final String MULTILIST_COMPONENT_TYPE="MLST";
    public static final String ASSIGN_AND_SELECT_COMPONENT_TYPE="ASSS";
    public static final String VISIT_COMPONENT_TYPE="VISI";
    public static final String MULTIPLE_TEXTAREA_COMPONENT_TYPE="MTXT";
    public static final String AGE_COMPONENT_TYPE="AGE";
    public static final String DOB_COMPONENT_TYPE="DOB";
    public static final String BRADEN_SCORE_COMPONENT_TYPE="BRS";
    public static final String BRADEN_TOTAL_COMPONENT_TYPE="BRT";
    public static final String BRACHIAL_RIGHT_SCORE="BRSC";
    public static final String TBI_RIGHT_SCORE="TRSC";
    public static final String BRACHIAL_LEFT_SCORE="BLSC";
    public static final String TBI_LEFT_SCORE="TLSC";
    public static final String SELECT_BY_ALPHA_COMPONENT_TYPE="SA";
    public static final String SELECT_BY_ALPHA_GOALS_COMPONENT_TYPE="SALP";
    public static final String SELECT_BY_ALPHA_MULTIPLELIST_COMPONENT_TYPE="SALM";
    public static final String CLOCK_COMPONENT_TYPE="CLK";
    public static final String NUMERIC_COMPONENT_TYPE="NUM";
    public static final int TRAINING_TREATMENT_ID=201;
    public static final int TRAINING_TREATMENT_CATEGORY_ID=200;
    public static final String ASSESSMENT_DATA_MAINTENANCE_COMPONENT_TYPE="ADM";
    public static final String PDF_SUMMARY="PDF";
    public static final String VIEW_PATIENT_CONSENT_COMPONENT_TYPE="VPCO";
    
    //field types
    public static final String INTEGER="INTEGER";
    public static final String STRING="STRING";
    public static final String DOUBLE="DOUBLE";
    public static final String DATE="DATE";
    public static final String NEWDATE="NEWDATE";

    public static final boolean SHOW_DELETED=true;
    public static final boolean HIDE_DELETED=false;
    public static final boolean HIDE_INCISIONS=true;
    public static final boolean SHOW_INCISIONS=false;
    
    public static final int IMAGE_1=1;
    public static final int IMAGE_2=2;
    public static final int IMAGE_3=3;
    public static final boolean DESC_ORDER=true;
    public static final boolean ASC_ORDER=true;
    

    public static final boolean TEMP_RECORDS=true;
    public static final boolean LIVE_RECORDS=false;
    public static final int IGNORE_INT=-1;
    public static final String IGNORE_STR=null;
    public static final long IGNORE_LNG=-1;
    public static final int EXTERNAL=3260;
    public static final int INTERNAL=3259;
    public static final String ADMIT="ADMIT";
    public static final String DISCHARGE="DISCHARGE";

    public static final String LOCATION_MOVE="Location Change";
    public static final String WOUNDS_HEALED="All Wounds Healed";
    public static final int ACTIVE=1;
    public static final int INACTIVE=0;
    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
    public static final String UNKNOWN = "Unknown";

    public static final String COMMENT_TYPE_NOTE="COMMENT";
    public static final String COMMENT_TYPE_RECOMMENDATION="RECOMMENDATION";
    public static final String COMMENT_TYPE_DOCTORS_ORDERS="DOCTORS_ORDERS";
    public static final String COMMENT_TYPE_REFERRALNOTE="REFERRAL_NOTE";
    public static final String PROFESSIONAL = "userVO";
    public static final boolean CLIENTSIDE_REPORT=false;
    public static final boolean WEBBASED_REPORT=true;
    //reporting output types
    public static final int DASHBOARD_REPORT=5;
    public static final int PATIENT_FILE_REPORT=2;
    public static final int WOUND_ACQUIRED_REPORT=9;
    public static final int SUMMARY_REPORT=7;
    public static final int REPORTING_WEB=1;
    public static final int EXPORT_PDF=3;
    public static final int PATIENT_OUTCOMES_REPORT=11;
    
    public static final int CCAC_REPORT=8;
    //log audit identifiers
    public static final String LOGGED_IN = "Logged In";
    public static final String LOCKED_OUT = "Too many failed logins. Account Locked.";
    public static final String ACCESSED_PATIENT = "Accessed Patient";
    public static final String CREATING_RECORD = "Creating Record";
    public static final String SEARCH_PATIENT = "Searching Patient (flagged same lastname)";
    public static final String UPDATE_RECORD = "Updating Record";
    public static final String COMMIT_RECORD = "Committing Record";
    public static final String DELETE_RECORD = "Deleting Record";
    public static final String INACTIVATE_RECORD = "Inactivate Record";
    public static final String DROP_TMP_RECORD = "Delete Temporary Record";
    public static final String MOVE_RECORD = "Moving Assessment";
    public static final String REOPEN_RECORD = "Reopen Alpha";
    public static final String TRIGGER_GEN = "trigger.generated";
    public static final String TRANSGENDER = "Transgender";

    public static final String MALE_SHORT = "M";
    public static final String FEMALE_SHORT = "F";
    public static final String UNKNOWN_SHORT = "U";
    public static final String TRANSGENDER_SHORT = "T";

    public static final byte[] DIGEST = new byte[]{
        (byte) 0x16,
        (byte) 0xA1, (byte) 0x60, (byte) 0x5B, (byte) 0xFB, (byte) 0xBE, (byte) 0x98, (byte) 0x4B, (byte) 0xD4,
        (byte) 0x19, (byte) 0x4A, (byte) 0x70, (byte) 0xF8, (byte) 0x13, (byte) 0x86, (byte) 0x5D, (byte) 0x2A,
        (byte) 0x75, (byte) 0x83, (byte) 0xA9, (byte) 0xF2, (byte) 0x07, (byte) 0xF0, (byte) 0x53, (byte) 0x62,
        (byte) 0x40, (byte) 0xCA, (byte) 0x93, (byte) 0xAC, (byte) 0xF7, (byte) 0xBC, (byte) 0x2E
    };
    /** Creates a new instance of Constants */
    public Constants() {
    }
    
}
