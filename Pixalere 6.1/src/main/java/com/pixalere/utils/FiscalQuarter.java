/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.utils;
import java.io.Serializable; 
import java.util.Date;
/**
 *
 * @author travis
 */
public class FiscalQuarter  implements Comparable,Serializable{
    private Date startDate;
    private Date endDate;
    private int quarter;
    private int order;
    private boolean isBlank;
    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the quarter
     */
    public int getQuarter() {
        return quarter;
    }

    /**
     * @param quarter the quarter to set
     */
    public void setQuarter(int quarter) {
        this.quarter = quarter;
    }

    /**
     * @return the order
     */
    public int getOrder() {
        return order;
    }

    public boolean isIsBlank() {
        return isBlank;
    }

    public void setIsBlank(boolean isBlank) {
        this.isBlank = isBlank;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }
public int compareTo(Object o1) {
        FiscalQuarter ref1 = (FiscalQuarter) o1;
        //Comments ref2 = (Comments) o2;
        if (this.getStartDate().after(ref1.getStartDate())) {
            return -1;
        } else if (this.getStartDate().before(ref1.getStartDate())){
                return 1;
        }
        return 0;
 
    }
    public boolean equals(Object o) {
        return true;
    }

    @Override
    public String toString() {
        return "FiscalQuarter{" + "startDate=" + startDate + ", endDate=" + endDate + ", quarter=" + quarter + ", order=" + order + '}';
    }
}
