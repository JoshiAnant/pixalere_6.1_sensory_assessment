/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.utils;
import java.util.Date;
/**
 *
 * @author travis
 */
public class FiscalYear {
    public FiscalYear(){

    }

    private FiscalQuarter[] quarters = new FiscalQuarter[4];

    private String fiscalYear;
    private Date startYear;
    private Date endYear;
   

    /**
     * @return the fiscalYear
     */
    public String getFiscalYear() {
        return fiscalYear;
    }

    /**
     * @param fiscalYear the fiscalYear to set
     */
    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    /**
     * @return the quarters
     */
    public FiscalQuarter[] getQuarters() {
        return quarters;
    }

    /**
     * @param quarters the quarters to set
     */
    public void setQuarters(FiscalQuarter[] quarters) {
        this.quarters = quarters;
    }

    /**
     * @return the startYear
     */
    public Date getStartYear() {
        return startYear;
    }

    /**
     * @param startYear the startYear to set
     */
    public void setStartYear(Date startYear) {
        this.startYear = startYear;
    }

    /**
     * @return the endYear
     */
    public Date getEndYear() {
        return endYear;
    }

    /**
     * @param endYear the endYear to set
     */
    public void setEndYear(Date endYear) {
        this.endYear = endYear;
    }
    
}
