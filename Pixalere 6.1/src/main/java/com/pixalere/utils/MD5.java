package com.pixalere.utils;
import com.pixalere.common.ApplicationException;
/**
 * <p>Title: Pixalere</p>
 * <p>Description: HealthCare Solution</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: WebMed Technologies</p>
 * @author Travis Morris
 * @version 1.0
 */

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5
    extends Object {
    /**	 * Private function to turn md5 result to 32 hex-digit string	 */
  private static String asHex(byte hash[]) {
    StringBuffer buf = new StringBuffer(hash.length * 2);
    int i;
    for (i = 0; i < hash.length; i++) {
      if ( ( (int) hash[i] & 0xff) < 0x10) {
        buf.append("0");
      }
      buf.append(Long.toString( (int) hash[i] & 0xff, 16));
    }
    return buf.toString().toUpperCase();
  } /**	 * Take a string and return its md5 hash as a hex digit string	 */

  public static String hash(String arg) throws ApplicationException{
    return hash(arg.getBytes());
  } /**	 * Non static version for VB	 */

  public String doHash(String arg) throws ApplicationException{
    return hash(arg.getBytes());
  } /**	 * Take a byte array and return its md5 hash as a hex digit string	 */

  public static String hash(byte barray[]) throws ApplicationException{
    String restring = "";
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(barray);
      byte[] result = md.digest();
      restring = asHex(result);
    }
    catch (NoSuchAlgorithmException e) {
        throw new ApplicationException("Error in MD5.hash(): " + e.toString(), e);
    }
    return restring;
  }
}
