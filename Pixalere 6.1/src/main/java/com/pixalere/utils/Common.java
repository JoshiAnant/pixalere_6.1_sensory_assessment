package com.pixalere.utils;


import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.AssessmentMucocSeperationsVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentProductVO;
import com.pixalere.assessment.bean.AssessmentSinustractsVO;
import com.pixalere.assessment.bean.AssessmentUnderminingVO;
import com.pixalere.assessment.bean.AssessmentWoundBedVO;
import com.pixalere.assessment.bean.NursingFixesVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.service.AssessmentCommentsServiceImpl;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.bean.ComponentsVO;
import com.pixalere.common.bean.ConfigurationVO;
import com.pixalere.common.bean.LanguageVO;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.bean.TableUpdatesVO;
import com.pixalere.common.client.ConfigurationClient;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.service.ConfigurationService;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.LocationDepthVO;
import com.pixalere.guibeans.RowData;
import com.pixalere.patient.bean.BradenVO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.InvestigationsVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbUpperAssessmentVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PhysicalExamVO;
import com.pixalere.patient.bean.SensoryAssessmentVO;
import com.pixalere.reporting.bean.DashboardVO;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.wound.bean.WoundProfileVO;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class Common {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Common.class);

    public static ActionErrors exceptionsMsg = new ActionErrors();
    /* Configurations */
    public static Hashtable CONFIG = null;
    
    /* Languages Map */
    public static final Map<Integer,String> LANGUAGES_MAP;
    public static final int IDLANG_EN;
    public static final int IDLANG_FR;
    public static final int IDLANG_ES;
    public static final int IDLANG_ES_ES;
    
    // Language Session Key property
    public static final String SESSION_LANGUAGE = "language";
    // Configuration language setting
    public static final String DEFAULT_LANGUAGE_CONF = "defaultLanguage";
    
    //TODO all constants should be moved to the constants class.
    public static boolean TEST_FILE = true;
    public static boolean imagesFailedUpload = false;
    final public static int ARRAYIZELIST = 1;
    final public static int ARRAYIZE = 2;
    final public static int VALUE = 3;
    final public static int LISTVALUE = 4;
    final public static int DATE = 5;
    final public static int UNSERIALIZE_STRAIGHT = 6;    //name of the resource bundle containing localized strings
    private static final String RESOURCE_BUNDLE = "ApplicationResources";
    private static final String REPORTS_BUNDLE = "ReportResources";
    
    // Static loading of language table
    static {
        Map<Integer,String> tmp = new HashMap<Integer, String>();
        // Default values just in case
        int idEn = 1;
        int idFr = 2;
        int idEs = 3;
        int idEsEs = 4;
        
        // Protect against potential changes in Language Ids in the future
        try {
            // On load, get ids and languages from DB
            ListsDAO listDao = new ListsDAO();
            Collection<LanguageVO> langs = listDao.findLanguagesByCriteria(new LanguageVO());
            
            // Create map of languages and ids
            for (LanguageVO lang: langs){
                tmp.put(lang.getId(), lang.getName());
                
                if (lang.getName().equals("en")) {
                    idEn = lang.getId();
                }
                if (lang.getName().equals("fr")) {
                    idFr = lang.getId();
                }
                if (lang.getName().equals("es")) {
                    idEs = lang.getId();
                }
                if (lang.getName().equals("es_ES")) {
                    idEsEs = lang.getId();
                }
            }
        } catch (DataAccessException e) {
            log.error("DataAccessException: Error in LookupVO Static init: " + e.toString());
            // Failed DB load, use hardcoded
            tmp.put(idEn, "en");
            tmp.put(idFr, "fr");
            tmp.put(idEs, "es");
            tmp.put(idEsEs, "es_ES");
        } catch (Exception e) {
            // First deployment - Language table still doesn't exist
            log.error("**** GENERAL EXCEPTION: Migrating missing? *********\n" + e.toString());
            // Failed DB load, use hardcoded
            tmp.put(idEn, "en");
            tmp.put(idFr, "fr");
            tmp.put(idEs, "es");
            tmp.put(idEs, "es_ES");
        } finally {
            IDLANG_EN = idEn;
            IDLANG_FR = idFr;
            IDLANG_ES = idEs;
            IDLANG_ES_ES = idEsEs;
        }
        
        LANGUAGES_MAP = Collections.unmodifiableMap(tmp);
    }
    
    public static String getLanguageLocale(Integer idLanguage){
        if (LANGUAGES_MAP.get(idLanguage) != null) {
            return LANGUAGES_MAP.get(idLanguage);
        } else {
            return Common.getConfig(DEFAULT_LANGUAGE_CONF);
        }
    }
    
    public static Integer getLanguageIdFromLocale(String locale){
        int id = -1;
        if (!LANGUAGES_MAP.containsValue(locale)){
            locale = Common.getConfig(DEFAULT_LANGUAGE_CONF);
        } 
        
        Iterator it = LANGUAGES_MAP.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, String> entry = (Map.Entry)it.next();
            if (entry.getValue().equals(locale)){
                id = entry.getKey();
            }
        }
        
        return id;
    }
    
    /**
     * This method is used for getting the general language id from a
     * country language id (ie es_ES to es), is used to get an id that
     * can be used for direct queries to the database (mostly in Reports);
     * it should be used to convert an id before using it directly in a
     * SQL query.
     * 
     * @param idLanguage
     * @return 
     */
    public static Integer getLanguageIdFallbackFromLanguageId(Integer idLanguage){
        String locale = Common.getLanguageLocale(idLanguage);
        if (locale.indexOf("_") > 0){
            String sublocale = locale.substring(0, locale.indexOf("_"));
            return Common.getLanguageIdFromLocale(sublocale);
        } else {
            return idLanguage;
        }
    }
    public static Integer getLanguageIdFromSession(HttpSession session){
        // Default value
        int id = -1;
        try {
            if (session != null && session.getAttribute(SESSION_LANGUAGE) != null){
                id = (Integer) session.getAttribute(SESSION_LANGUAGE);
            }
        } catch (ClassCastException e) {}
        if (id < 0){
            id = getLanguageIdFromLocale(Common.getConfig(DEFAULT_LANGUAGE_CONF));
        }
        return id;
    }
    public static boolean isNumeric(String s) {  
        return s.matches("[-+]?\\d*\\.?\\d+");  
    }  
    public static String buildException(Exception e) {
        String exception = e.toString();
        StackTraceElement[] elements = e.getStackTrace();
        for (StackTraceElement element : elements) {
            exception = exception + "<br/>" + element;
        }
        return exception.replaceAll("'", "");
    }

    /**
     * Since each table contains multiple data fields we need to filter out each
     * a given field. For instance with Patient Profile, the array table
     * contains interfering_meds, medicationc omments, and comorbidities, I may
     * only want to use intefering_meds though.
     *
     * @param avo
     * @param resource_id
     * @return
     */
    public static List<ArrayValueObject> filterArrayObjects(ArrayValueObject[] avo, int resource_id) {
        List<ArrayValueObject> returnList = new ArrayList();
        try {

            for (ArrayValueObject a : avo) {
                if (a.getLookup() != null && a.getLookup().getResourceId() == resource_id) {
                    returnList.add(a);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnList;
    }

    /**
     * Since each table contains multiple data fields we need to filter out each
     * a given field. For instance with Patient Profile, the array table
     * contains interfering_meds, medicationc omments, and comorbidities, I may
     * only want to use intefering_meds though.
     *
     * @param avo
     * @param resource_id
     * @return
     */
    public static List<LookupVO> filterArrayObjects2Lookup(ArrayValueObject[] avo, int[] ids) {
        List<LookupVO> returnList = new ArrayList();
        try {

            for (ArrayValueObject a : avo) {
                for (int resource_id : ids) {
                    if (a.getLookup() != null && a.getLookup().getResourceId() == resource_id) {

                        returnList.add(a.getLookup());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnList;
    }

    /**
     * Calculate the customer's fiscal quarters by retrieving their configurable
     * for fiscal year start, and fiscal quarter range.
     *
     * @return array of quarters
     * @since 5.1
     * @auther travis.morris
     *
     */
    public static List<FiscalQuarter> getQuarters(int num_quarters, Date start_date) {

        String fiscal_year = Common.getConfig("fiscalStart");
        String fiscal_quarters = Common.getConfig("fiscalQuarter");
        //System.out.println("fiscal_year"+fiscal_year+" fiscal_quarter: "+fiscal_quarters);
        List<FiscalQuarter> quarters = PDate.getLastXQuarters(num_quarters, fiscal_year, fiscal_quarters, start_date);

        return quarters;
    }
    public static List<String> getResults(ArrayValueObject[] avo, int[] ids){
        List<String> strs = new ArrayList();
        try {

            for (ArrayValueObject a : avo) {
                for (int resource_id : ids) {
                    if (a.getLookup() != null && a.getLookup().getResourceId() == resource_id) {
                        strs.add(a.getLookup_id()+"");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strs;
    }
    /**
     * Since each table contains multiple data fields we need to filter out each
     * a given field. For instance with Patient Profile, the array table
     * contains interfering_meds, medicationc omments, and comorbidities, I may
     * only want to use intefering_meds though.
     *
     * @param avo
     * @param ids some fields like como have multiple resource_ids
     * @return
     */
    public static List<ArrayValueObject> filterArrayObjects(ArrayValueObject[] avo, int[] ids) {
        List<ArrayValueObject> returnList = new ArrayList();
        try {

            for (ArrayValueObject a : avo) {
                for (int resource_id : ids) {
                    if (a.getLookup() != null && a.getLookup().getResourceId() == resource_id) {
                        returnList.add(a);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public static void invalidatePatientSession(HttpSession session) {
        session.removeAttribute("syncsuccess");
        
        session.removeAttribute("simulateSave");
        session.removeAttribute("patientsync");
        session.removeAttribute("assessments");
        session.removeAttribute("patientAccount");
        session.removeAttribute("patientAccountAdd");
        session.removeAttribute("patient_id");
        session.removeAttribute("wound_id");
        session.removeAttribute("referrals");
        session.removeAttribute("assessments");
        session.removeAttribute("assessment_id");
        session.removeAttribute("assessments_table");
        session.removeAttribute("alpha_id");
        session.removeAttribute("wound_profile_type_id");
        session.removeAttribute("new_wound_profile");
        session.removeAttribute("wound_ass_string");
        session.removeAttribute("savedComments");
        session.removeAttribute("ncp");
        session.removeAttribute("wound_changed");
        session.removeAttribute("patient_changed");
        session.removeAttribute("foot_changed");
        session.removeAttribute("wpti_wound");
        session.removeAttribute("skin_changed");
        session.removeAttribute("wpti_skin");
        session.removeAttribute("wpti_incision");
        session.removeAttribute("wpti_drain");
        session.removeAttribute("wpti_ostomy");
        session.removeAttribute("wpti_burn");
        session.removeAttribute("limb_changed");
        session.removeAttribute("offlineVO");
        session.removeAttribute("treatment_comments");
        session.removeAttribute("savedComments");
        session.removeAttribute("viewerOnly");
        session.removeAttribute("backdated");
    }

    public static void invalidateWoundSession(HttpSession session) {
        session.removeAttribute("syncsuccess");
        session.removeAttribute("patientsync");
        session.removeAttribute("simulateSave");
        session.removeAttribute("assessments");
        session.removeAttribute("patientAccountAdd");
        session.removeAttribute("wound_id");
        session.removeAttribute("referrals");
        session.removeAttribute("assessments");
        session.removeAttribute("wpti_wound");
        session.removeAttribute("wpti_skin");
        session.removeAttribute("wpti_incision");
        session.removeAttribute("wpti_drain");
        session.removeAttribute("wpti_ostomy");
        session.removeAttribute("wpti_burn");
        session.removeAttribute("assessment_id");
        session.removeAttribute("assessments_table");
        session.removeAttribute("alpha_id");
        session.removeAttribute("new_wound_profile");
        session.removeAttribute("wound_ass_string");
        session.removeAttribute("savedComments");
        session.removeAttribute("ncp");
        session.removeAttribute("wound_changed");
        session.removeAttribute("skin_changed");
        session.removeAttribute("patient_changed");
        session.removeAttribute("foot_changed");
        session.removeAttribute("wound_profile_type_id");
        session.removeAttribute("limb_changed");
        session.removeAttribute("treatment_comments");
        session.removeAttribute("savedComments");
        session.removeAttribute("backdated");
    }

    public static Element createElement(String name, String value, Document dom) {
        Element parentElement = dom.createElement(name);
        Text parentText = dom.createTextNode(value);
        parentElement.appendChild(parentText);
        return parentElement;
    }

    /**
     *
     * @param alpha
     * @param list
     * @return
     * @throws ApplicationException
     */
    public static Vector parseSelectByAlpha(String alpha, String[] list,String locale) throws ApplicationException {

        Vector select_list = new Vector();
        //check ostomy
        if (alpha.indexOf(Constants.OSTOMY_ALPHAS[0]) != -1) {
            alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.urinalstoma", locale);//'+alpha.substring(4,alpha.length());

        }

        if (alpha.indexOf(Constants.OSTOMY_ALPHAS[1]) != -1) {
            alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.fecalstoma", locale);//+alpha.substring(4,alpha.length());

        }
        for (String w : Constants.ALPHAS) {
            if (w.equals(alpha)) {
                alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.woundcare", locale)+" " + alpha;
            }
        }
        //check burn
        if (alpha.indexOf(Constants.BURN_ALPHA_GLOBAL) != -1) {
            alpha = "Burn";
        }

        //check incision
        for (String inc : Constants.INCISION_ALPHA) {
            if (inc.equals(alpha)) {
                String num = inc.substring(3, alpha.length());
                alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.incision", locale)+" " + num;
            }
        }

        //check tube or drain
        for (String inc : Constants.DRAIN_ALPHAS) {
            if (inc.equals(alpha)) {
                String num = inc.substring(2, alpha.length());
                alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.drain", locale)+" " + num;
            }
        }
        //check tube or drain
        for (String inc : Constants.SKIN_ALPHAS) {
            if (inc.equals(alpha)) {
                String num = inc.substring(1, alpha.length());
                alpha = "Skin " + num;
            }
        }
        if (list != null) {
            for (String l : list) {
                int x = -1;
                if (l != null) {
                    x = l.indexOf(alpha + ": ");

                    if (x != -1) {
                        x = x + ((alpha + ": ").length());

                    }
                }

                if (x != -1 && l.length() >= x) {
                    select_list.add(l.substring(x, l.length()));
                }
            }
        }
        return select_list;
    }
    /*
     * Find out if an Alpha_type exists in a wound profile, rather then regetting the collection and
     * sorting it, its faster just to check the location str.
     */

    public static boolean getAlphaTypeExists(WoundProfileTypeVO[] types, String alpha_type) {
        for (WoundProfileTypeVO type : types) {

            if (type.getAlpha_type().equals(alpha_type)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWoundProfileTypeHealed(Integer professional_id, Collection<WoundAssessmentLocationVO> all, Collection<WoundAssessmentLocationVO> allActive) {
        boolean healed = false;
        if (all.size() == 0) {
            healed = false;
        } else {

            if (allActive.size() == 0) {
                healed = true;
            } else {
                healed = true;
                /*for (WoundAssessmentLocationVO alp : allActive) {
                 if (alp.getActive().equals(new Integer(1))) {
                 healed = false;
                 } else if (alp.getProfessional_id().equals(professional_id)) {
                 healed = false;
                 }
                 }*/
                //if there's existing active alphas, we can't close
                if (allActive.size() > 0) {
                    healed = false;
                }
            }

        }
        return healed;
    }

    public static String getWoundProfileType(String alpha_type, String locale) {
        if (alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {
            return getLocalizedString("pixalere.woundprofile.form.woundcare", locale);
        } else if (alpha_type.equals(Constants.OSTOMY_PROFILE_TYPE)) {
            return getLocalizedString("pixalere.woundprofile.form.ostomy", locale);
        } else if (alpha_type.equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
            return getLocalizedString("pixalere.woundprofile.form.postop", locale);
        } else if (alpha_type.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
            return getLocalizedString("pixalere.woundprofile.form.tubesanddrains", locale);

        } else if (alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {
            return getLocalizedString("pixalere.woundprofile.form.skin", locale);
        } else if (alpha_type.equals(Constants.BURN_PROFILE_TYPE)) {
            return getLocalizedString("pixalere.woundprofile.form.burn", locale);
        }
        return "";
    }

    public static String getAlphaText(String display_alpha, String locale) {
        if (display_alpha != null) {
            if (display_alpha.length() > 0) {

                if (display_alpha.length() == 1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.woundcare", locale) + " " + display_alpha;
                } else if (display_alpha.indexOf("ostu") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.urinalstoma", locale);
                } else if (display_alpha.indexOf("ostf") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.fecalstoma", locale);
                } else if (display_alpha.indexOf("td") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.drain", locale) + " " + display_alpha.substring(2);

                } else if (display_alpha.indexOf("s") == 0) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.skin", locale) + " " + display_alpha.substring(1);
                } else if (display_alpha.indexOf("tag") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.incision_tag", locale) + " " + display_alpha.substring(3);
                } else if (display_alpha.indexOf("burn") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.burn", locale);

                }

            }

        }

        return display_alpha;
    }
    
    public static String getAssessmentTableNameFromAlpha(String alpha){
        String tableName = "assessment_wound";
        
        if (alpha.indexOf("ostu") != -1) {
            tableName = "assessment_ostomy";
            
        } else if (alpha.indexOf("ostf") != -1) {
            tableName = "assessment_ostomy";
            
        } else if (alpha.indexOf("td") != -1) {
            tableName = "assessment_drain";
            
        } else if (alpha.indexOf("s") == 0) {
            tableName = "assessment_skin";
            
        } else if (alpha.indexOf("tag") != -1) {
            tableName = "assessment_incision";
            
        } else if (alpha.indexOf("burn") != -1) {
            tableName = "assessment_burn";

        }
        return tableName;
    }

    public static int allowFlowchartEdit(int allow, ProfessionalVO currentProf, ProfessionalVO recordProf, int intEnabled_closed, boolean blnClosed, int intOffline) {
        int edit = 0;
        if (allow == 1 && (!isOffline() || intOffline == 0) && (currentProf != null && recordProf != null && (currentProf.getId().equals(recordProf.getId()) || currentProf != null && currentProf.getAccess_superadmin().equals(new Integer(1)))) && ((intEnabled_closed == 1 && blnClosed) || (intEnabled_closed == 0))) {
            edit = 1;
        }
        return edit;
    }

    public static Hashtable<String, ComponentsVO> hashComponents(Collection<ComponentsVO> components) {
    	Hashtable<String, ComponentsVO> hash = new Hashtable();
        for (ComponentsVO component : components) {
            //if (component.getInfopopup() != null) {
            hash.put(component.getField_name(), component);
            //}
        }
        return hash;
    }

    public static boolean isSpecialist(PositionVO strPosition) {
        try {

            if (strPosition != null && strPosition.getRecommendation_popup() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public static String checkForNull(String item) {
        if (item == null) {
            return "";
        } else {
            return item;
        }
    }
    /*
     * Get the current temp
     * record, and the last live record, and compare them, to highlight the changes
     * in blue on the summary page.
     * @param rows the array of wound_profiles
     * 
     * @return rowdata the array returned back with the state of the changes updated.
     */

    public static List<FieldValues> compareGlobal(RowData[] tmp) {
        List<FieldValues> liveFields = null;
        List<FieldValues> tmpFields = null;
        if (tmp.length == 1) {
            RowData tmpRecord = tmp[0];
            tmpFields = tmpRecord.getFields();
        } else if (tmp.length > 1) {
            RowData liveRecord = tmp[0];
            RowData tmpRecord = tmp[1];
            liveFields = liveRecord.getFields();
            tmpFields = tmpRecord.getFields();
        }
        if (tmpFields != null) {
            for (int x = 0; x < tmpFields.size(); x++) {

                if (x > 1) {//skip first two empty rows of flowchart
                    if (tmpFields.get(x) != null) {
                        if (liveFields != null) {//if not null check if values equal

                            FieldValues l = liveFields.get(x);
                            FieldValues t = tmpFields.get(x);
                            //Ignore fields which change every record like last update
                            if (l != null && (!l.getField_name().equals("created_on") && !l.getField_name().equals("startdate_signature") && !l.getField_name().equals("start_date") && !l.getField_name().equals("user_signature") && !l.getField_name().equals("age"))) {
                                //limb comment check... hack until we can change the way it works.

                                if ((l.getField_name().equals("limb_comments") && t.getDb_value().equals("")) || (l.getField_name().equals("foot_comments") && t.getDb_value().equals(""))) {
                                    tmpFields.get(x).setValue_changed(0);
                                } else if (l.getField_name().equals("review_done")) {
                                    if (t.getDb_value().equals("2") || t.getDb_value().equals("1")) {//if table contains review_done.. force submission.
                                        tmpFields.get(x).setValue_changed(1);

                                    } else {
                                        tmpFields.get(x).setValue_changed(0);
                                    }
                                } else {

                                    if (t.getDb_value() == null) {
                                        t.setDb_value("");
                                    }
                                    if (l.getDb_value() == null) {
                                        l.setDb_value("");
                                    }
                                    if (l.getDb_value().equals(t.getDb_value())) {
                                        tmpFields.get(x).setValue_changed(0);
                                    } else {
                                        tmpFields.get(x).setValue_changed(1);
                                        //System.out.println(l.getDb_value()+"="+l.getField_name()+"=="+t.getDb_value());
                                    }
                                }
                            } else {
                                tmpFields.get(x).setValue_changed(0);
                            }
                            tmpFields.get(x).setOld_value(l.getValue());//set old value so we can display it in summary popup

                        } else {
                            if (tmpFields.get(x) != null && tmpFields.get(x).getValue() != null && !tmpFields.get(x).getValue().equals("") && !tmpFields.get(x).getValue().equals("N/A")) {
                                tmpFields.get(x).setValue_changed(1);
                                tmpFields.get(x).setOld_value("");
                            } else {
                                tmpFields.get(x).setValue_changed(0);
                                tmpFields.get(x).setOld_value("");
                            }
                        }
                    } else {
                        System.out.println("I'm null;");

                    }
                }
            }
        }
        return tmpFields;
    }

    public static Vector paging(Vector records, int offset, int count) {
        Vector returnVect = new Vector();
        for (int x = 0; x < records.size(); x++) {
            if ((x >= offset) && (x < (offset + count))) {
                returnVect.add(records.get(x));
            }
            if ((offset + count) == x) {
                break;
            }
        }
        return returnVect;
    }

    public static String getYesNoFromRadio(Integer value, String locale) {
        if (value.equals(new Integer("2"))) {
            return getLocalizedString("pixalere.yes", locale);
        } else if (value.equals(new Integer("1"))) {
            return getLocalizedString("pixalere.no", locale);
        } else {
            return getLocalizedString("pixalere.na", locale);
        }
    }
    public static String stripHTML(String strInput){
        if(strInput == null){return null;}
        else{
            return strInput.replaceAll("<", "&lt;").replace(">", "&gt;");
        }
    }
    public static String stripName(String strInput) {
        String strValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String strResult = "";
        strInput = strInput.toUpperCase();
        for (int intChar = 0; intChar < strInput.length(); intChar++) {
            if (strValidChars.indexOf(strInput.charAt(intChar)) != -1) {
                strResult = strResult + strInput.charAt(intChar);
            }
        }
        return strResult;
    }

    public static String stripPHN(String strInput) {
        String strValidChars = "0123456789";
        String strResult = "";
        for (int intChar = 0; intChar < strInput.length(); intChar++) {
            if (strValidChars.indexOf(strInput.charAt(intChar)) != -1) {
                strResult = strResult + strInput.charAt(intChar);
            }
        }
        return strResult;
    }

    public static String getProfessionalsName(String sig, String locale) {
        if (!sig.equals(getLocalizedString("pixalere.na", locale))) {
            int num = Common.parseSignatureForID(sig);
            com.pixalere.auth.service.ProfessionalServiceImpl bd = new com.pixalere.auth.service.ProfessionalServiceImpl();
            com.pixalere.auth.bean.ProfessionalVO vo = new com.pixalere.auth.bean.ProfessionalVO();
            try {
                vo = bd.getProfessional(num);
            } catch (Exception e) {// if professional does not exist
            }
            if (vo == null) {
                return getLocalizedString("pixalere.na", locale);
            }
            String fullname = escapeSingleQuote(vo.getFirstname() + " " + vo.getLastname());
            return fullname;
        } else {
            return getLocalizedString("pixalere.na", locale);
        }

    }
    private static boolean bool;

    // @todo is this used anymore?
    public static int parseSignatureForID(String signature) {
        int a = signature.indexOf("ID#:");
        String tmpString = "";

        try {
            tmpString = signature.substring(a + 4, signature.length());
            int b = tmpString.indexOf(" ");
            tmpString = tmpString.substring(0, b);
        } catch (StringIndexOutOfBoundsException e) {
        }
        if (tmpString.equals("")) {
            int c = signature.indexOf(":ID#");//older records

            try {
                tmpString = signature.substring(c + 4, signature.length());

                int b = tmpString.indexOf(" ");
                tmpString = tmpString.substring(0, b);
            } catch (StringIndexOutOfBoundsException e) {
            }
        }
        int returnedInt = 0;
        try {
            returnedInt = new Integer(tmpString).intValue();
        } catch (NumberFormatException ex) {
            // Incorrect # parsed from signature (likely an old patient);
        }
        return returnedInt;
    }

    public static Vector mergeImages(Collection<AssessmentImagesVO> images, boolean withTitle, String locale) throws ApplicationException {

        Vector img = new Vector();
        for (AssessmentImagesVO imagesVO : images) {
            //Vector<String> tmp = Serialize.arrayIze(imagesVO.getImages());
            //for (String tmpStr : tmp) {
            if (withTitle == true) {
                Hashtable hash = new Hashtable();
                hash.put("image", imagesVO.getImages());
                hash.put("which", imagesVO.getWhichImage());
                int num = 0;
                if (imagesVO.getImages().indexOf("-1") != -1) {
                    num = 1;
                } else if (imagesVO.getImages().indexOf("-2") != -1) {
                    num = 2;
                } else if (imagesVO.getImages().indexOf("-3") != -1) {
                    num = 3;
                }
                String[] alphas = Constants.ALPHAS;

                String alpha = "";

                for (int y = 0; y < Constants.INCISION_ALPHA.length; y++) {
                    if (imagesVO.getImages().indexOf(Constants.INCISION_ALPHA[y] + "-") != -1) {
                        alpha = Common.getLocalizedString("pixalere.viewer.form.assessment_incision", locale);
                        String n = Constants.INCISION_ALPHA[y].substring(3, 4);
                        alpha = alpha + " " + n;
                    }
                }
                if (alpha.equals("")) {
                    for (int y = 0; y < Constants.OSTOMY_ALPHAS.length; y++) {
                        if (imagesVO.getImages().indexOf("ostf") != -1) {

                            alpha = Common.getLocalizedString("pixalere.viewer.form.assessment_fecalstoma", locale);
                        } else if (imagesVO.getImages().indexOf("ostu") != -1) {

                            alpha = Common.getLocalizedString("pixalere.viewer.form.assessment_urinalstoma", locale);
                        }
                    }
                }
                if (alpha.equals("")) {
                    for (int y = 0; y < Constants.DRAIN_ALPHAS.length; y++) {
                        if (imagesVO.getImages().indexOf(Constants.DRAIN_ALPHAS[y] + "-") != -1) {
                            alpha = Common.getLocalizedString("pixalere.viewer.form.assessment_drain", locale);
                            String n = Constants.DRAIN_ALPHAS[y].substring(2, 3);
                            alpha = alpha + " " + n;
                        }
                    }
                }

                if (alpha.equals("")) {
                    for (int y = 0; y < alphas.length; y++) {
                        if (imagesVO.getImages().toUpperCase().indexOf(alphas[y] + "-") != -1) {
                            alpha = Common.getLocalizedString("pixalere.viewer.form.assessment_woundcare", locale) + " " + alphas[y];

                        }

                    }
                }

                hash.put("imageTitle", alpha + "<br/>Image " + num);
                img.add(hash);
            } else {
                img.add(imagesVO.getImages());
            }
        }
        //}

        return img;
    }

    public static void createFolder(String path) {
        File pathoff = new File(path);

        if (!pathoff.exists()) {
            try {

                pathoff.mkdir();
            } catch (Exception e) {
                // logs.error("Error in AssessmentEachwound.perform. :" +
                // e.getMessage());
                e.printStackTrace();

            }
        }
    }

    public boolean checkConnection() {
        // Download the table "TableUpdates" (which only has 1 record), just to check the connection
        boolean online = false;
        try {
            //System.out.println("Common.java: checkConnection for database ");
            ConfigurationClient cc = new ConfigurationClient();
            ConfigurationService remoteConfigurationService = cc.createConfigurationClient();

            TableUpdatesVO updatesServer = remoteConfigurationService.getTableUpdates();
           
            if (exceptionsMsg.size() == 0 && updatesServer != null) {
                online = true;
                // Check for existing configuration table; if not, then download this
                ConfigurationServiceImpl localConfigurationService = new ConfigurationServiceImpl();
                TableUpdatesVO updatesLocal = localConfigurationService.getTableUpdates();
                if (updatesLocal == null || updatesLocal.getConfiguration() == null || updatesServer.getConfiguration().intValue() > updatesLocal.getConfiguration().intValue()) {
                    System.out.println("No config loaded yet");
                    // There's no configuration table downloaded yet
                    List<ConfigurationVO> configuration = remoteConfigurationService.getAllConfigurations();
                    if (configuration != null) {
                        for (ConfigurationVO configurationVO : configuration) {
                            if (!configurationVO.getComponent_type().equals("CAT")) {
                                localConfigurationService.saveConfiguration(configurationVO);
                            }
                        }
                    }
                    if (updatesLocal == null) {
                        updatesLocal = new TableUpdatesVO();
                    }
                    updatesLocal.setConfiguration(updatesServer.getConfiguration());
                    localConfigurationService.saveTableUpdates(updatesLocal);
                } else if (updatesLocal.getConfiguration() < updatesServer.getConfiguration()) {
                    Collection<ConfigurationVO> configuration = remoteConfigurationService.getAllConfigurations();
                    if (configuration != null) {
                        for (ConfigurationVO configurationVO : configuration) {
                            if (!configurationVO.getComponent_type().equals("CAT")) {
                                localConfigurationService.saveConfiguration(configurationVO);
                            }
                        }
                    }

                    updatesLocal.setConfiguration(updatesServer.getConfiguration());
                    localConfigurationService.saveTableUpdates(updatesLocal);
                }
                System.out.println("Done check config");
            }
        } catch (Exception e) {
            System.out.println("Common.java: no connection established in checkConnection; " + e.toString());
            
        }
        return online;
    }


    /* Get the value of a list item */
    public static String getListValue(int list_id, Integer language) {
        try {
            ListServiceImpl bd = new ListServiceImpl(language);
            LookupVO vo = (LookupVO) bd.getListItem(list_id);
            if (vo == null || vo.getName(language) == null) {
                return "";

            } else {
                String value = vo.getName(language);
                if (vo.getCpt() != null && !vo.getCpt().equals("")) {
                    value = value + Common.getLocalizedString("pixalere.cpt", "en") + ": " + vo.getCpt();
                }
                if (vo.getIcd9() != null && !vo.getIcd9().equals("")) {
                    value = value + Common.getLocalizedString("pixalere.icd9", "en") + ": " + vo.getIcd9();
                }
                if (vo.getIcd10() != null && !vo.getIcd10().equals("")) {
                    value = value + Common.getLocalizedString("pixalere.icd10", "en") + ": " + vo.getIcd10();
                }
                return value;

            }
        } catch (Exception ex) {
            return "";
        }

    }

    //@todo refactor to use getPrintableValue instead
    private static String getDisplayValue(Object obj, int which, Integer language) throws ApplicationException {
        String title = "";
        if (obj != null) {
            if (ARRAYIZELIST == which) {
                title = Serialize.arrayDelimiter(Serialize.arrayIzeWithNames(obj + "", language), ",");
            } else if (ARRAYIZE == which) {
                title = Serialize.arrayDelimiter(Serialize.arrayIze(obj + ""), ",");
            } else if (LISTVALUE == which) {
                title = getListValue(new Integer(obj.toString()).intValue(), language);
            } else if (VALUE == which) {
                title = obj + "";
            } else if (DATE == which) {
                title = new PDate().getDateString(obj + "",Common.getLanguageLocale(language));
            } else if (UNSERIALIZE_STRAIGHT == which) {
                title = Serialize.arrayDelimiter(Serialize.unserializeStraight((String) obj), ",");
            }
        }
        return title;
    }

    public static Vector printProducts(Collection<AssessmentProductVO> products) {
        Vector printProducts = new Vector();
        if (products != null) {
            for (AssessmentProductVO product : products) {
                ProductsVO product_name = product.getProduct();
                if (product_name == null) {
                    product_name = new ProductsVO();
                }
                if (product.getProduct_id().equals(new Integer("-1"))) {

                    printProducts.add(product.getQuantity() + " " + product.getOther());

                } else {
                    printProducts.add(product.getQuantity() + " "
                            + product_name.getTitle());
                }

            }
        }

        return printProducts;
    }

    public static boolean findString(String strFullString, String strSubString) {
        strFullString = "," + strFullString + ",";
        return strFullString.indexOf("," + strSubString + ",") != -1;
    }

    /**
     * Description:
     *
     * @param num
     * @param decimalplaces
     * @return
     */
    public static float Round(float num, int decimalplaces) {
        float mynum = (float) num;
        NumberFormat nf;
        nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(decimalplaces);
        float r = 0;
        try {
            String n = nf.format(mynum);
            r = Float.parseFloat(n);
            return r;
        } catch (NumberFormatException e) {
            return r;
        }
    }

    /**
     * Description:
     *
     * @param num
     * @param decimalplaces
     * @return
     */
    public static double Round(double num, int decimalplaces) {
        double mynum = (double) num;
        NumberFormat nf;
        nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(decimalplaces);
        double r = 0;
        try {
            String n = nf.format(mynum);
            r = Double.parseDouble(n);
            return r;
        } catch (NumberFormatException e) {
            return r;
        }
    }

    public String[] explode(String value, boolean space) {
        String[] tmp = value.split("\\|");

        if (space == true) {
            String[] tmp2 = new String[(tmp.length) + 1];
            for (int x = 0; x < tmp.length; x++) {
                tmp2[x + 1] = tmp[x];
            }
            tmp2[0] = "";
            tmp = tmp2;
        }
        return tmp;
    }

    public static String formatNumber(Integer number1, Integer number2) {
        String numberfinal = "";
        String number1str = "";
        String number2str = "";
        if (number1 == null || number1.intValue() == -1) {
            number1str = "0";
        } else {
            number1str = number1 + "";
        }
        if (number2 == null || number2.intValue() == -1) {
            number2str = "0";
        } else {
            number2str = number2 + "";
        }
        if ((number1 == null && number2 == null) || number1.intValue() == -1 && number2.intValue() == -1) {
            numberfinal = "";
        } else {
            numberfinal = number1str + "." + number2str;
        }
        return numberfinal;
    }

    public static String formatLocation(Integer number1, Integer number2) {
        String numberfinal = "";
        String number1str = "";
        String number2str = "";
        if (number1.intValue() == -1) {
            number1str = "0";
        } else {
            number1str = number1 + "";
        }
        if (number2.intValue() == -1) {
            number2str = "0";
        } else {
            number2str = number2 + "";
        }
        numberfinal = number1str + " to " + number2str;
        return numberfinal;
    }

    private static boolean compareStringTrimage(Object trim, Object trim2) {
        boolean bool = false;
        if (trim instanceof String) {
            String str1 = (String) trim;
            String str2 = (String) trim2;
            if (str1 != null) {
                str1 = str1.trim();
                str1 = str1.replaceAll("\r\n|\n", "");
            } else {
                str1 = "";
            }
            if (str2 != null) {
                str2 = str2.trim();
                str2 = str2.replaceAll("\r\n|\n", "");
            } else {
                str2 = "";
            }
            if (str2.equals(str1)) {
                return true;
            } else {
                return false;
            }

        } else {
            if (trim.equals(trim2)) {
                return true;
            } else {
                return false;
            }
        }

    }

    public static void removeFiles(String path) {
        try {
            File dir = new File(path);
            if (dir.isDirectory()) {
                File dirFiles[] = dir.listFiles();
                for (int i = 0; i < dirFiles.length; i++) {
                    dirFiles[i].delete();
                }

            }
        } catch (Exception e) {
            // Common.printException(e, "cache.err");
        }
    }

    public static void remove(String path) {
        try {
            File dir = new File(path);
            if (dir.isDirectory()) {
                File dirFiles[] = dir.listFiles();
                for (int i = 0; i < dirFiles.length; i++) {
                    if (dirFiles[i].isDirectory()) {
                        File dir2Files[] = dirFiles[i].listFiles();
                        for (int x = 0; x < dir2Files.length; x++) {
                            dir2Files[x].delete();
                        }
                    }
                    dirFiles[i].delete();
                }

                dir.delete();
            }
        } catch (Exception e) {
            // Common.printException(e, "cache.err");
        }

    }

    public static boolean isOffline() {
        if (getLocalizedString("pixalere.offline", "en").equals("1")) {
            return true;
        } else {
            return false;
        }
    }

    public static String getBasePath(javax.servlet.ServletContext context) {
        if(context != null){
        return context.getRealPath("/");
        }else{
            return "";
        }
    }

    public static String getPhotosPath() {
        if (Common.isOffline() == true) {
            if (new File(getLocalizedString("pixalere.photospath", "en")).exists()) {
                return getLocalizedString("pixalere.photospath", "en") + "/" + getConfig("customer_id");
            } else {
                return "c:/Program Files (x86)/Pixalere Offline/photos/" + getConfig("customer_id");
            }
        } else {
            return Common.getConfig("photos_path") + "/" + getConfig("customer_id");
        }
    }

    public static String getDownloadsPath() {
        if (Common.isOffline() == true) {
            return getLocalizedString("pixalere.photospath", "en") + "/" + getConfig("customer_id") + "/";
        } else {
            return Common.getConfig("photos_path") + "/" + getConfig("customer_id") + "/";
        }
    }

    /**
     * Used for auto generating the flowchart, and summary page.  This dynamicaly
     * generates by order the displayable values of the corresponding field from
     * the database.
     * 
     * @version 4.1
     * @since 4.0
     * Returns the field output
     */
    public static Hashtable getPrintableValue(Object assess, ProfessionalVO currentProfessional, ComponentsVO component, boolean blnEmbedCode, int intColumn, Integer language) throws ApplicationException {
        Vector result = new Vector();
        Vector field_name = new Vector();
        String locale = Common.getLanguageLocale(language);
        
        //if(component.getComponent_type().equals(Constants.IMAGE_COMPONENT_TYPE) || Common.getFieldValue(component.getField_name(),assess) != null){
        String db_value = "";

        PDate pdate = new PDate(currentProfessional != null ? currentProfessional.getTimezone() : Constants.TIMEZONE);

        if (component.getComponent_type().equals(Constants.COMMENTS_CHECK)) {
            AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;
            WoundAssessmentVO v = ass.getWoundAssessment();
            // shouldnt do this here.. but oh well :p

            AssessmentCommentsServiceImpl bd = new AssessmentCommentsServiceImpl();

            int count = 0;
            if (v != null) {
                count = bd.getCommentsCount(v);
            }
            //if assessment has comments, state so
            boolean comments_exist = false;
            if (count > 0) {
                comments_exist = true;
            }

            result.add(comments_exist == true ? "" + Common.getLocalizedString("pixalere.yes", locale) + "" : "" + Common.getLocalizedString("pixalere.no", locale) + "");
            field_name.add(component.getField_name() + "");
        } else if (component.getComponent_type().equals(Constants.REFERRAL_COMPONENT_TYPE)) {
            AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;
            ReferralsTrackingServiceImpl bd = new ReferralsTrackingServiceImpl(language);

            ReferralsTrackingVO r = new ReferralsTrackingVO();
            r.setAssessment_id(ass.getAssessment_id());
            r.setEntry_type(Constants.REFERRAL);

            ReferralsTrackingVO rt = bd.getReferralsTracking(r);
            if (!isOffline()) {
                result.add(rt != null ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale));
            } else {
                result.add("N/A");
            }
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.DROPDOWN_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            result.add(getListValue(Integer.parseInt(value.equals("") || value.equals("null") ? "0" : value), language));
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.ASSESSMENT_TYPE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue("full_assessment", assess) + "";
            if (value.equals("1")) {
                db_value = db_value + Common.getLocalizedString("pixalere.woundassessment.form.full_assessment", locale);
            } else if (value.equals("0")) {
                db_value = db_value + Common.getLocalizedString("pixalere.woundassessment.form.partial_assessment", locale);
            } else if (value.equals("2")) {
                db_value = db_value + Common.getLocalizedString("pixalere.woundassessment.form.wound_not_assessed.title", locale);
            } else if (value.equals("3")) {
                db_value = db_value + Common.getLocalizedString("pixalere.woundassessment.form.phone_visit.title", locale);

            }

            result.add(db_value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTIPLE_TEXTAREA_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            Vector items = Serialize.unserializeStraight(value);
            db_value = value;
            String results = "";
            int count = 1;

            for (Object item : items) {
                results = results + "" + Common.getLocalizedString(component.getLabel_key(), locale) + " #" + count + ": " + item + "<br/>";
                count++;
            }

            result.add(results);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            Vector items = Serialize.unserializeStraight(value);
            db_value = value;
            String results = "";
            int count = 1;

            for (Object item : items) {
                String title = getListValue(Integer.parseInt(value.equals("") || value.equals("null") ? "0" : value), language);
                results = results + "" + Common.getLocalizedString(component.getLabel_key(), locale) + " #" + count + ": " + title + "<br/>";
                count++;
            }

            result.add(results);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.CLOCK_COMPONENT_TYPE)) {
            String value_start = Common.getFieldValue(component.getField_name() + "_start", assess) + "";
            String value_end = Common.getFieldValue(component.getField_name() + "_end", assess) + "";
            if (value_start == null || value_start.equals("-1")) {
                value_start = "0";
            }
            if (value_end == null || value_end.equals("-1")) {
                value_end = "0";
            }
            String value = value_start + ":00 to " + value_end + ":00";
            if (value.equals("0:00 to 0:00")) {
                value = "";
            }
            result.add(value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTILIST_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;
            result.add(Serialize.commaIzeList(value, language));
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTI_BURN_ASSESSMENT_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;
            String strReturnValue = "";
            StringTokenizer strAreas = new StringTokenizer(value, ";");
            while (strAreas.hasMoreTokens()) {
                String strAreaDetails = strAreas.nextToken();
                if (!strReturnValue.equals("")) {
                    strReturnValue = strReturnValue + "<br/>";
                }
                strReturnValue = strReturnValue + Common.getLocalizedString("pixalere.burn.area." + strAreaDetails.substring(0, 2), locale) + ":&nbsp;";
                strAreaDetails = strAreaDetails.substring(3);
                StringTokenizer strAreaDetail = new StringTokenizer(strAreaDetails, ",");
                String strThisAreaDetail = "";
                while (strAreaDetail.hasMoreTokens()) {
                    if (!strThisAreaDetail.equals("")) {
                        strThisAreaDetail = strThisAreaDetail + ",&nbsp;";
                    }
                    strThisAreaDetail = strThisAreaDetail + getListValue(Integer.parseInt(strAreaDetail.nextToken()), language);
                }
                strReturnValue = strReturnValue + strThisAreaDetail;
            }

            result.add(strReturnValue);
            field_name.add(component.getField_name() + "");
        } else if (component.getComponent_type().equals(Constants.DROPDOWN_DATE_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;

            result.add(Serialize.commaIze(Serialize.unserializeStraight(value)));
            field_name.add(component.getId() + "");
        } /*NEWDATE_COMPONENT_TYPEelse if (component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_MULTIPLELIST_COMPONENT_TYPE)) {
         String value = (String) Common.getFieldValue(component.getField_name(), assess);
         db_value = value;
         String em = ",";
         if (blnEmbedCode == true) {
         em = "<br/>";
         }
         result.add(Serialize.arrayDelimiter(Serialize.arrayIze(value), em));
         field_name.add(component.getId() + "");
         } */ else if (component.getComponent_type().equals(Constants.DATE_COMPONENT_TYPE)) {
            String value = null;
            try {
                Date dvalue = (Date) Common.getFieldValue(component.getField_name(), assess);
                value = PDate.getFormattedDate(dvalue, Constants.DATE_FORMAT);
            } catch (ClassCastException e) {
                e.printStackTrace();
                /*String d = (Date) Common.getFieldValue(component.getField_name(), assess);
                 String value = PDate.getDate(d);
                 System.out.println("VACSTART:FSYR" + value);
                 if (value == null || value.equals("null")) {
                 value = "";
                 }*/
            }
            db_value = value;
            result.add(value);
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.DATETIME_COMPONENT_TYPE)) {
            String value = null;
            try {
                Date dvalue = (Date) Common.getFieldValue(component.getField_name(), assess);
                value = PDate.getDateTime(dvalue);
            } catch (ClassCastException e) {
                e.printStackTrace();
                /*String d = (Date) Common.getFieldValue(component.getField_name(), assess);
                 String value = PDate.getDate(d);
                 System.out.println("VACSTART:FSYR" + value);
                 if (value == null || value.equals("null")) {
                 value = "";
                 }*/
            }
            //db_value =dvalue;
            result.add(value);
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.NEWDATE_COMPONENT_TYPE)) {

            String value = "";
            Date d = (Date) Common.getFieldValue(component.getField_name(), assess);
            value = PDate.getDate(d, false);
            System.out.println("VACSTART:FSYR" + value);
            if (value == null || value.equals("null")) {
                value = "";
            }

            db_value = value;
            result.add(value);
            field_name.add(component.getId() + "");

        } else if (component.getComponent_type().equals(Constants.YN_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.Y_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {
                result.add("");
            } else if (value.equals("2")) {
                result.add(Common.getLocalizedString("pixalere.yes", locale));
            } else if (value.equals("1")) {
                result.add(Common.getLocalizedString("pixalere.no", locale));
            } else {
                result.add("");
            }

            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.YNWHY_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {
                result.add("");
            } else if (value.equals("2")) {
                result.add(Common.getLocalizedString("pixalere.yes", locale));
            } else if (value.equals("1")) {
                Object comments = Common.getFieldValue(component.getField_name() + "_comments", assess);
                if (comments == null) {
                    comments = "";
                }
                result.add(Common.getLocalizedString("pixalere.no", locale)+": " + comments);
            } else {
                result.add("");
            }

            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.SHOWHIDE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {
                result.add("");
            } else if (value.equals("2")) {
                result.add("Show");
            } else if (value.equals("1")) {
                result.add("Hide");
            } else {
                result.add("");
            }

            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.AN_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {

                result.add("");
            } else if (value.equals("2")) {
                result.add("Assessed");
            } else if (value.equals("1")) {
                result.add("Not Assessed");
            } else {
                result.add("");
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.ONOFFNA_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("-1")) {

                result.add("N/A");

            } else {
                if (value.equals("2")) {
                    result.add("On");
                } else if (value.equals("1")) {
                    result.add("Off");
                } else {
                    result.add("N/A");
                }
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.YNN_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("-1")) {

                result.add("N/A");

            } else {
                if (value.equals("2")) {
                    result.add(Common.getLocalizedString("pixalere.yes", locale));
                } else if (value.equals("1")) {
                    result.add(Common.getLocalizedString("pixalere.no", locale));
                } else {
                    result.add("N/A");
                }
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.CHECKBOX_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;
            result.add(Serialize.commaIzeList(value, language));
            field_name.add(component.getId() + "");
        }   else if (component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {

            if (component.getField_name().equals("undermining_location") || component.getField_name().equals("undermining_depth")) {

                AssessmentEachwoundVO a = (AssessmentEachwoundVO) assess;

                Collection<AssessmentUnderminingVO> undermining = a.getUnderminings();

                field_name.add("11");

                field_name.add("12");

                db_value = Common.parseMultipleUndermining(undermining, true, locale) + "." + Common.parseMultipleUndermining(undermining, false, locale);

                result.add(Common.parseMultipleUndermining(undermining, false, locale));

                result.add(Common.parseMultipleUndermining(undermining, true, locale));

            } else if (component.getField_name().equals("sinus_location") || component.getField_name().equals("sinus_depth")) {

                AssessmentEachwoundVO a = (AssessmentEachwoundVO) assess;

                Collection<AssessmentSinustractsVO> sinustracts = a.getSinustracts();

                db_value = Common.parseMultipleTracts(sinustracts, true) + "." + Common.parseMultipleTracts(sinustracts, true);

                result.add(Common.parseMultipleTracts(sinustracts, false));

                result.add(Common.parseMultipleTracts(sinustracts, true));

                field_name.add("141");

                field_name.add("13");

            } else if (component.getField_name().equals("mucocutaneous_margin_location") || component.getField_name().equals("mucocutaneous_margin_depth")) {

                AssessmentOstomyVO a = (AssessmentOstomyVO) assess;

                Collection<AssessmentMucocSeperationsVO> m = a.getSeperations();

                result.add(Common.parseMultipleSeperations(m, true, locale));

                result.add(Common.parseMultipleSeperations(m, false, locale));

                field_name.add("456");

                field_name.add("457");

            }

        }  else if (component.getComponent_type().equals(Constants.ONE_MEASUREMENT_COMPONENT_TYPE)) {
            if ((!component.getFlowchart().equals(new Integer(3)) && !component.getFlowchart().equals(new Integer(4)) && !component.getFlowchart().equals(new Integer(5)) && !component.getFlowchart().equals(new Integer(6))) || (Common.getFieldValue("full_assessment", assess) + "").equals("1") || Common.getConfig("disableMeasurentsForPartial").equals("0")) {

                String mm = Common.getFieldValue(component.getField_name(), assess) + "";
                if (mm == null || mm.equals("null")) {
                    mm = "0";
                }
                result.add(mm + "");
                field_name.add(component.getId() + "");
            } else {
                db_value = "";
                result.add("");
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MEASUREMENT_COMPONENT_TYPE)) {
            if ((!component.getFlowchart().equals(new Integer(3)) && !component.getFlowchart().equals(new Integer(4)) && !component.getFlowchart().equals(new Integer(5)) && !component.getFlowchart().equals(new Integer(6))) || (Common.getFieldValue("full_assessment", assess) + "").equals("1") || Common.getConfig("disableMeasurentsForPartial").equals("0")) {
                String cm = Common.getFieldValue(component.getField_name() + "_cm", assess) + "";
                String mm = Common.getFieldValue(component.getField_name() + "_mm", assess) + "";
                db_value = cm + "." + mm;
                Integer cm_i = (cm == null || cm.equals("null") ? 0 : new Integer(cm));
                Integer mm_i = (mm == null || mm.equals("null") ? 0 : new Integer(mm));
                String numb = Common.formatNumber(new Integer(cm_i), new Integer(mm_i));
                if (component.getFlowchart().equals(new Integer(11)) || component.getFlowchart().equals(new Integer(53))) {
                    result.add(numb.equals("0.0") || numb.equals("") ? "" : numb + "");
                } else {
                    result.add(numb.equals("0.0") || numb.equals("") ? "N/A" : numb + "");
                }
            } else {
                db_value = "";
                result.add("N/A");
            }
            field_name.add(component.getId() + "");
        }else if (component.getComponent_type().equals(Constants.TEXTFIELD_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.TEXTAREA_COMPONENT_TYPE)) {

            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("null")) {
                value = "";
            }
            result.add(value);
            field_name.add(component.getId() + "");
        }else if (component.getComponent_type().equals(Constants.NUMBER_COMPONENT_TYPE)) {

            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("null")) {
                value = "";
            }
            result.add(value);
            field_name.add(component.getId() + "");
        }  else if (component.getComponent_type().equals(Constants.IMAGE_COMPONENT_TYPE)) {
            AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;

            AssessmentImagesServiceImpl bd = new AssessmentImagesServiceImpl();

            AssessmentImagesVO images = new AssessmentImagesVO();
            images.setAssessment_id(ass.getAssessment_id());
            images.setAlpha_id(ass.getAlpha_id());
            //images.setActive(1);
            String strImages = "";
            if (blnEmbedCode == true) {

                WoundAssessmentVO wnd = ass.getWoundAssessment();
                if (!Common.isOffline() && ((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1)))) {
                    strImages = "<a href=\"javascript:imagesPopup('" + ass.getPatient_id() + "','" + ass.getAssessment_id() + "','" + ass.getWound_id() + "','" + ass.getAlpha_id() + "')\"><img class=\"edit\" border=\"0\"></a>";
                }
            }

            try {
                Collection imgs = bd.getAllImages(images);
                strImages = strImages + " " + imgs.size() + " " + Common.getLocalizedString("pixalere.attached_images", locale);
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }

            result.add(strImages);
            field_name.add(component.getId() + "");

        } else if (component.getComponent_type().equals(Constants.PDF_SUMMARY)) {
            if (blnEmbedCode == true && !Common.isOffline()) {
                AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;

                result.add("<input type=\"image\" src=\"images/pdf.gif\" id=\"pdfbtn_" + ass.getAssessment_id() + "\"  onClick=\"javascript:loadwindow('PdfSummarySetup.do?id=" + "" + (Common.getConfig("useWMPForSummaryReport").equals("1") ? ass.getPatient_id() + "&report_type=wmp" : ass.getAssessment_id()) + "',400,400);\" />");
                field_name.add(component.getField_name() + "");
            } else {
                result.add("");
                field_name.add("");
            }
        } else if (component.getComponent_type().equals(Constants.ASSESSMENT_DATA_MAINTENANCE_COMPONENT_TYPE)) {
            //@todo all these need refactoring into 1 method.
            if (blnEmbedCode && (component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT))) {
                AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;

                WoundAssessmentVO wnd = ass.getWoundAssessment();
                Date now = new Date();
                int timeframe = 0;

                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && ass.getDeleted() != 1 && wnd.getProfessional_id() != null && (((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) && ( (wnd.getCreated_on().after(now)) || timeframe == 0))) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + ass.getId() + "\"  onClick=\"document.delete_form.id.value=" + ass.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + ass.getId() + "\"  onClick=\"document.move_form.id.value=" + ass.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.PATIENT_PROFILE)) {
                PatientProfileVO p = (PatientProfileVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && p.getProfessional_id() != null && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.WOUND_PROFILE)) {
                WoundProfileVO p = (WoundProfileVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);
                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.FOOT_ASSESSMENT)) {
                FootAssessmentVO p = (FootAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            }else if (blnEmbedCode && component.getFlowchart().equals(Constants.SENSORY_ASSESSMENT)) {
                SensoryAssessmentVO p = (SensoryAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.PHYSICAL_EXAM)) {
                PhysicalExamVO p = (PhysicalExamVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_BASIC)) {
                LimbBasicAssessmentVO p = (LimbBasicAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_UPPER)) {

                LimbUpperAssessmentVO p = (LimbUpperAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_ADVANCED)) {
                LimbAdvAssessmentVO p = (LimbAdvAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.BRADEN_SCORE_SECTION)) {
                BradenVO p = (BradenVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else {
                result.add("");
            }
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.MULTI_PERCENTAGES_COMPONENT_TYPE)) {

            AssessmentEachwoundVO t = (AssessmentEachwoundVO) assess;
            Collection<AssessmentWoundBedVO> bed = t.getWoundbed();
            String v = "";
            for (AssessmentWoundBedVO wndbase : bed) {//cycle through wound_bed options and create printable list
                v = v + wndbase.getLookup().getName(language) + ": " + wndbase.getPercentage() + "%, ";

            }
            if (v != "") {
                v = v.substring(0, v.length() - 2); // Remove comma at end
            }

            result.add(v);
            db_value = v;
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.NUMERIC_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            if (value == null) {
                value = "";
            }
            db_value = value;
            result.add(value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.IGNORE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            result.add(value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.RADIO_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("null")) {
                result.add("");
            } else {
                result.add(getListValue(Integer.parseInt(value.equals("") ? "0" : value), language));
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.BRADEN_SCORE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("null") || value.equals("0")) {
                result.add("");
            } else {
                result.add(value);
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.BRADEN_TOTAL_COMPONENT_TYPE)) {
            String value = Integer.toString(Integer.parseInt(Common.getFieldValue("braden_moisture", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_nutrition", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_friction", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_mobility", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_sensory", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_activity", assess) + ""));
            db_value = value;
            if (value.equals("null")) {
                result.add("");
            } else {
                result.add(value);
            }
            field_name.add(component.getId() + "");
        }
        //}
        Hashtable retArray = new Hashtable();
        retArray.put("value", result);
        retArray.put("field", field_name);
        if (db_value == null || db_value.equals("null")) {
            db_value = "";
        }
        retArray.put("db_value", db_value);
        return retArray;
    }
  /**
     * Used for auto generating the flowchart, and summary page.  This dynamicaly
     * generates by order the displayable values of the corresponding field from
     * the database.
     * 
     * @version 6.1
     * @since 6.1
     * Returns the field output
     */
    public static Object getFieldForVelocity(Object assess, ProfessionalVO currentProfessional, ComponentsVO component, boolean blnEmbedCode, int intColumn, Integer language) throws ApplicationException {
        Vector result = new Vector();
        Vector field_name = new Vector();
        String locale = "en";
        ListServiceImpl lservice = new ListServiceImpl(language);
        com.pixalere.common.bean.LanguageVO lang = lservice.getLanguage(language);
        if (lang != null) {
            locale = lang.getName();
        }
        //if(component.getComponent_type().equals(Constants.IMAGE_COMPONENT_TYPE) || Common.getFieldValue(component.getField_name(),assess) != null){
        String db_value = "";

        PDate pdate = new PDate(currentProfessional != null ? currentProfessional.getTimezone() : Constants.TIMEZONE);

        if (component.getComponent_type().equals(Constants.COMMENTS_CHECK)) {
            AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;
            WoundAssessmentVO v = ass.getWoundAssessment();
            // shouldnt do this here.. but oh well :p

            AssessmentCommentsServiceImpl bd = new AssessmentCommentsServiceImpl();

            int count = 0;
            if (v != null) {
                count = bd.getCommentsCount(v);
            }
            //if assessment has comments, state so
            boolean comments_exist = false;
            if (count > 0) {
                comments_exist = true;
            }

            result.add(comments_exist == true ? "" + Common.getLocalizedString("pixalere.yes", locale) + "" : "" + Common.getLocalizedString("pixalere.no", locale) + "");
            field_name.add(component.getField_name() + "");
        } else if (component.getComponent_type().equals(Constants.REFERRAL_COMPONENT_TYPE)) {
            AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;
            ReferralsTrackingServiceImpl bd = new ReferralsTrackingServiceImpl(language);

            ReferralsTrackingVO r = new ReferralsTrackingVO();
            r.setAssessment_id(ass.getAssessment_id());
            r.setEntry_type(Constants.REFERRAL);

            ReferralsTrackingVO rt = bd.getReferralsTracking(r);
            if (!isOffline()) {
                result.add(rt != null ? "Yes" : "No");
            } else {
                result.add("N/A");
            }
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.DROPDOWN_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            result.add(getListValue(Integer.parseInt(value.equals("") || value.equals("null") ? "0" : value), language));
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.ASSESSMENT_TYPE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue("full_assessment", assess) + "";
            if (value.equals("1")) {
                db_value = db_value + "Full Assessment";
            } else if (value.equals("0")) {
                db_value = db_value + "Partial Assessment";
            } else if (value.equals("2")) {
                db_value = db_value + "Wound Not Assessed";
            } else if (value.equals("3")) {
                db_value = db_value + "Phone Visit";

            }

            result.add(db_value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTIPLE_TEXTAREA_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            Vector items = Serialize.unserializeStraight(value);
            db_value = value;
            String results = "";
            int count = 1;

            for (Object item : items) {
                results = results + "" + Common.getLocalizedString(component.getLabel_key(), locale) + " #" + count + ": " + item + "<br/>";
                count++;
            }

            result.add(results);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTI_DROPDOWN_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            Vector items = Serialize.unserializeStraight(value);
            db_value = value;
            String results = "";
            int count = 1;

            for (Object item : items) {
                String title = getListValue(Integer.parseInt(value.equals("") || value.equals("null") ? "0" : value), language);
                results = results + "" + Common.getLocalizedString(component.getLabel_key(), locale) + " #" + count + ": " + title + "<br/>";
                count++;
            }

            result.add(results);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.CLOCK_COMPONENT_TYPE)) {
            String value_start = Common.getFieldValue(component.getField_name() + "_start", assess) + "";
            String value_end = Common.getFieldValue(component.getField_name() + "_end", assess) + "";
            if (value_start == null || value_start.equals("-1")) {
                value_start = "0";
            }
            if (value_end == null || value_end.equals("-1")) {
                value_end = "0";
            }
            String value = value_start + ":00 to " + value_end + ":00";
            if (value.equals("0:00 to 0:00")) {
                value = "";
            }
            result.add(value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTILIST_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;
            result.add(Serialize.commaIzeList(value, language));
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTI_BURN_ASSESSMENT_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;
            String strReturnValue = "";
            StringTokenizer strAreas = new StringTokenizer(value, ";");
            while (strAreas.hasMoreTokens()) {
                String strAreaDetails = strAreas.nextToken();
                if (!strReturnValue.equals("")) {
                    strReturnValue = strReturnValue + "<br/>";
                }
                strReturnValue = strReturnValue + Common.getLocalizedString("pixalere.burn.area." + strAreaDetails.substring(0, 2), locale) + ":&nbsp;";
                strAreaDetails = strAreaDetails.substring(3);
                StringTokenizer strAreaDetail = new StringTokenizer(strAreaDetails, ",");
                String strThisAreaDetail = "";
                while (strAreaDetail.hasMoreTokens()) {
                    if (!strThisAreaDetail.equals("")) {
                        strThisAreaDetail = strThisAreaDetail + ",&nbsp;";
                    }
                    strThisAreaDetail = strThisAreaDetail + getListValue(Integer.parseInt(strAreaDetail.nextToken()), language);
                }
                strReturnValue = strReturnValue + strThisAreaDetail;
            }

            result.add(strReturnValue);
            field_name.add(component.getField_name() + "");
        } else if (component.getComponent_type().equals(Constants.DROPDOWN_DATE_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;

            result.add(Serialize.commaIze(Serialize.unserializeStraight(value)));
            field_name.add(component.getId() + "");
        } /*NEWDATE_COMPONENT_TYPEelse if (component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.SELECT_BY_ALPHA_MULTIPLELIST_COMPONENT_TYPE)) {
         String value = (String) Common.getFieldValue(component.getField_name(), assess);
         db_value = value;
         String em = ",";
         if (blnEmbedCode == true) {
         em = "<br/>";
         }
         result.add(Serialize.arrayDelimiter(Serialize.arrayIze(value), em));
         field_name.add(component.getId() + "");
         } */ else if (component.getComponent_type().equals(Constants.DATE_COMPONENT_TYPE)) {
            String value = null;
            try {
                Date dvalue = (Date) Common.getFieldValue(component.getField_name(), assess);
                value = PDate.getFormattedDate(dvalue, Constants.DATE_FORMAT);
            } catch (ClassCastException e) {
                e.printStackTrace();
                /*String d = (Date) Common.getFieldValue(component.getField_name(), assess);
                 String value = PDate.getDate(d);
                 System.out.println("VACSTART:FSYR" + value);
                 if (value == null || value.equals("null")) {
                 value = "";
                 }*/
            }
            db_value = value;
            result.add(value);
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.DATETIME_COMPONENT_TYPE)) {
            String value = null;
            try {
                Date dvalue = (Date) Common.getFieldValue(component.getField_name(), assess);
                value = PDate.getDateTime(dvalue);
            } catch (ClassCastException e) {
                e.printStackTrace();
                /*String d = (Date) Common.getFieldValue(component.getField_name(), assess);
                 String value = PDate.getDate(d);
                 System.out.println("VACSTART:FSYR" + value);
                 if (value == null || value.equals("null")) {
                 value = "";
                 }*/
            }
            //db_value =dvalue;
            result.add(value);
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.NEWDATE_COMPONENT_TYPE)) {

            String value = "";
            Date d = (Date) Common.getFieldValue(component.getField_name(), assess);
            value = PDate.getDate(d, false);
            System.out.println("VACSTART:FSYR" + value);
            if (value == null || value.equals("null")) {
                value = "";
            }

            db_value = value;
            result.add(value);
            field_name.add(component.getId() + "");

        } else if (component.getComponent_type().equals(Constants.YN_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.Y_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {
                result.add("");
            } else if (value.equals("2")) {
                result.add("Yes");
            } else if (value.equals("1")) {
                result.add("No");
            } else {
                result.add("");
            }

            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.YNWHY_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {
                result.add("");
            } else if (value.equals("2")) {
                result.add("Yes");
            } else if (value.equals("1")) {
                Object comments = Common.getFieldValue(component.getField_name() + "_comments", assess);
                if (comments == null) {
                    comments = "";
                }
                result.add("No: " + comments);
            } else {
                result.add("");
            }

            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.SHOWHIDE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {
                result.add("");
            } else if (value.equals("2")) {
                result.add("Show");
            } else if (value.equals("1")) {
                result.add("Hide");
            } else {
                result.add("");
            }

            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.AN_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("-1")) {

                result.add("");
            } else if (value.equals("2")) {
                result.add("Assessed");
            } else if (value.equals("1")) {
                result.add("Not Assessed");
            } else {
                result.add("");
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.ONOFFNA_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("-1")) {

                result.add("N/A");

            } else {
                if (value.equals("2")) {
                    result.add("On");
                } else if (value.equals("1")) {
                    result.add("Off");
                } else {
                    result.add("N/A");
                }
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.YNN_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("-1")) {

                result.add("N/A");

            } else {
                if (value.equals("2")) {
                    result.add("Yes");
                } else if (value.equals("1")) {
                    result.add("No");
                } else {
                    result.add("N/A");
                }
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.CHECKBOX_COMPONENT_TYPE)) {
            String value = (String) Common.getFieldValue(component.getField_name(), assess);
            db_value = value;
            result.add(Serialize.commaIzeList(value, language));
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.MULTI_MEASUREMENT_COMPONENT_TYPE)) {

            if (component.getField_name().equals("undermining_location") || component.getField_name().equals("undermining_depth")) {

                AssessmentEachwoundVO a = (AssessmentEachwoundVO) assess;

                Collection<AssessmentUnderminingVO> undermining = a.getUnderminings();

                field_name.add("11");

                field_name.add("12");

                db_value = Common.parseMultipleUndermining(undermining, true, locale) + "." + Common.parseMultipleUndermining(undermining, false, locale);

                result.add(Common.parseMultipleUndermining(undermining, false, locale));

                result.add(Common.parseMultipleUndermining(undermining, true, locale));

            } else if (component.getField_name().equals("sinus_location") || component.getField_name().equals("sinus_depth")) {

                AssessmentEachwoundVO a = (AssessmentEachwoundVO) assess;

                Collection<AssessmentSinustractsVO> sinustracts = a.getSinustracts();

                db_value = Common.parseMultipleTracts(sinustracts, true) + "." + Common.parseMultipleTracts(sinustracts, true);

                result.add(Common.parseMultipleTracts(sinustracts, false));

                result.add(Common.parseMultipleTracts(sinustracts, true));

                field_name.add("141");

                field_name.add("13");

            } else if (component.getField_name().equals("mucocutaneous_margin_location") || component.getField_name().equals("mucocutaneous_margin_depth")) {

                AssessmentOstomyVO a = (AssessmentOstomyVO) assess;

                Collection<AssessmentMucocSeperationsVO> m = a.getSeperations();

                result.add(Common.parseMultipleSeperations(m, true, locale));

                result.add(Common.parseMultipleSeperations(m, false, locale));

                field_name.add("456");

                field_name.add("457");

            }

        } else if (component.getComponent_type().equals(Constants.TEXTFIELD_COMPONENT_TYPE) || component.getComponent_type().equals(Constants.TEXTAREA_COMPONENT_TYPE)) {

            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value == null || value.equals("null")) {
                value = "";
            }
            result.add(value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.IMAGE_COMPONENT_TYPE)) {
            AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;

            AssessmentImagesServiceImpl bd = new AssessmentImagesServiceImpl();

            AssessmentImagesVO images = new AssessmentImagesVO();
            images.setAssessment_id(ass.getAssessment_id());
            images.setAlpha_id(ass.getAlpha_id());
            //images.setActive(1);
            String strImages = "";
            if (blnEmbedCode == true) {

                WoundAssessmentVO wnd = ass.getWoundAssessment();
                if ((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) {
                    strImages = "<a href=\"javascript:imagesPopup('" + ass.getPatient_id() + "','" + ass.getAssessment_id() + "','" + ass.getWound_id() + "','" + ass.getAlpha_id() + "')\"><img class=\"edit\" border=\"0\"></a>";
                }
            }

            try {
                Collection imgs = bd.getAllImages(images);
                strImages = strImages + " " + imgs.size() + " " + Common.getLocalizedString("pixalere.attached_images", locale);
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }

            result.add(strImages);
            field_name.add(component.getId() + "");

        } else if (component.getComponent_type().equals(Constants.PDF_SUMMARY)) {
            if (blnEmbedCode == true && !Common.isOffline()) {
                AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;

                result.add("<input type=\"image\" src=\"images/pdf.gif\" id=\"pdfbtn_" + ass.getAssessment_id() + "\"  onClick=\"javascript:loadwindow('PdfSummarySetup.do?id=" + "" + (Common.getConfig("useWMPForSummaryReport").equals("1") ? ass.getPatient_id() + "&report_type=wmp" : ass.getAssessment_id()) + "',400,400);\" />");
                field_name.add(component.getField_name() + "");
            } else {
                result.add("");
                field_name.add("");
            }
        } else if (component.getComponent_type().equals(Constants.ASSESSMENT_DATA_MAINTENANCE_COMPONENT_TYPE)) {
            //@todo all these need refactoring into 1 method.
            if (blnEmbedCode && (component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT))) {
                AbstractAssessmentVO ass = (AbstractAssessmentVO) assess;

                WoundAssessmentVO wnd = ass.getWoundAssessment();
                Date now = new Date();
                int timeframe = 0;

                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && ass.getDeleted() != 1 && wnd.getProfessional_id() != null && (((wnd.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (wnd.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + ass.getId() + "\"  onClick=\"document.delete_form.id.value=" + ass.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + ass.getId() + "\"  onClick=\"document.move_form.id.value=" + ass.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.PATIENT_PROFILE)) {
                PatientProfileVO p = (PatientProfileVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && p.getProfessional_id() != null && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.WOUND_PROFILE)) {
                WoundProfileVO p = (WoundProfileVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);
                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.FOOT_ASSESSMENT)) {
                FootAssessmentVO p = (FootAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.PHYSICAL_EXAM)) {
                PhysicalExamVO p = (PhysicalExamVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_BASIC)) {
                LimbBasicAssessmentVO p = (LimbBasicAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_UPPER)) {

                LimbUpperAssessmentVO p = (LimbUpperAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.LIMB_ASSESSMENT_ADVANCED)) {
                LimbAdvAssessmentVO p = (LimbAdvAssessmentVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else if (blnEmbedCode && component.getFlowchart().equals(Constants.BRADEN_SCORE_SECTION)) {
                BradenVO p = (BradenVO) assess;
                Date now = new Date();
                int timeframe = 0;
                try {
                    int timeframe_config = Integer.parseInt(Common.getConfig("deleteAssessmentTimeframe"));
                    now = PDate.subtractDays(now, timeframe_config);

                    if (!isOffline() && p.getDeleted() != 1 && (((p.getProfessional_id()).equals(currentProfessional.getId()) || currentProfessional.getAccess_superadmin().equals(new Integer(1))) || currentProfessional.getAccess_superadmin().equals(new Integer(1)) && (p.getCreated_on().after(now)) || timeframe == 0)) {

                        result.add("<input type=\"button\"  id=\"delbtn_" + p.getId() + "\"  onClick=\"document.delete_form.id.value=" + p.getId() + ";document.delete_form.type.value=" + component.getFlowchart() + ";$( '#deleteconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.del", locale) + "\"/>" + ((component.getFlowchart().equals(Constants.SKIN_ASSESSMENT) || component.getFlowchart().equals(Constants.OSTOMY_ASSESSMENT) || component.getFlowchart().equals(Constants.DRAIN_ASSESSMENT) || component.getFlowchart().equals(Constants.INCISION_ASSESSMENT) || component.getFlowchart().equals(Constants.WOUND_ASSESSMENT)) ? "<input type=\"button\" id=\"movebtn_" + p.getId() + "\"  onClick=\"document.move_form.id.value=" + p.getId() + ";document.move_form.type.value=" + component.getFlowchart() + ";$( '#moveconfirm' ).dialog( 'open' );return false;\" value=\"" + Common.getLocalizedString("pixalere.move", locale) + "\"/>" : ""));

                    } else {
                        result.add("");
                    }
                } catch (Exception e) {
                    result.add("");
                }
            } else {
                result.add("");
            }
            field_name.add(component.getField_name() + "");

        } else if (component.getComponent_type().equals(Constants.MULTI_PERCENTAGES_COMPONENT_TYPE)) {

            AssessmentEachwoundVO t = (AssessmentEachwoundVO) assess;
            Collection<AssessmentWoundBedVO> bed = t.getWoundbed();
            String v = "";
            for (AssessmentWoundBedVO wndbase : bed) {//cycle through wound_bed options and create printable list
                v = v + wndbase.getLookup().getName(language) + ": " + wndbase.getPercentage() + "%, ";

            }
            if (v != "") {
                v = v.substring(0, v.length() - 2); // Remove comma at end
            }

            result.add(v);
            db_value = v;
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.NUMERIC_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            if (value == null) {
                value = "";
            }
            db_value = value;
            result.add(value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.IGNORE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            result.add(value);
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.RADIO_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("null")) {
                result.add("");
            } else {
                result.add(getListValue(Integer.parseInt(value.equals("") ? "0" : value), language));
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.BRADEN_SCORE_COMPONENT_TYPE)) {
            String value = Common.getFieldValue(component.getField_name(), assess) + "";
            db_value = value;
            if (value.equals("null") || value.equals("0")) {
                result.add("");
            } else {
                result.add(value);
            }
            field_name.add(component.getId() + "");
        } else if (component.getComponent_type().equals(Constants.BRADEN_TOTAL_COMPONENT_TYPE)) {
            String value = Integer.toString(Integer.parseInt(Common.getFieldValue("braden_moisture", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_nutrition", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_friction", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_mobility", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_sensory", assess) + "")
                    + Integer.parseInt(Common.getFieldValue("braden_activity", assess) + ""));
            db_value = value;
            if (value.equals("null")) {
                result.add("");
            } else {
                result.add(value);
            }
            field_name.add(component.getId() + "");
        }
        //}
        Hashtable retArray = new Hashtable();
        retArray.put("value", result);
        retArray.put("field", field_name);
        if (db_value == null || db_value.equals("null")) {
            db_value = "";
        }
        retArray.put("db_value", db_value);
        return retArray;
    }
    public static ComponentsVO populateLocalizedTitles(ComponentsVO component, String locale) {
        String TEXT_START = "$text.get(\"";
        String TEXT_END = "\")";
        String result = component.getValidation();
        String validation = component.getValidation();
        if (validation != null) {
            while (validation.indexOf(TEXT_END) != -1) {
                int i = validation.indexOf(TEXT_START);
                int j = validation.indexOf(TEXT_END);
                String t = validation.substring(i + 11, j);
                String text = TEXT_START + t + TEXT_END;
                validation = validation.substring(j + 2, validation.length());

                String title = Common.getLocalizedString(t, locale);
                result = StringUtils.replace(result, text, title);

            }
        }
        component.setValidation(result);

        return component;
    }

    public static String printVector(Vector<String> items, boolean blnEmbedCode) throws ApplicationException {
        String printer = "";

        for (String item : items) {
            if (printer != "") {
                if (blnEmbedCode == true) {
                    printer = printer + "<br/> ";
                } else {
                    printer = printer + ", ";
                }
            }
            printer = printer + item;

        }
        /*(System.out.println(printer);
         if (printer.length() > 1) {
         printer = printer.substring(0, printer.length() - 1);
         }
         System.out.println(printer);*/
        return printer;
    }
    /*
     * Check if an assessment_eachwound serialized field contains a specified item
     *
     * @return boolean true if it exists false if it doesnt
     * @param serial is the serialized field in assessment_eachwound
     * @param item is the needle in the haystack we need to find.
     *
     * @since 3.2
     */

    private static boolean ifExists(String serial, String item) throws ApplicationException {
        if ((serial).indexOf("\"" + item + "\"") == -1) {
            return false;
        } else {
            return true;
        }
    }
    /*
     * Retrieve the percentage of a wound_bed item
     *
     * @return int the percentage of the wound_bed item
     * @param percentages the serialized array of wound_bed selections/percentages
     * @param item the specific wound_bed item we wantt eh percentage of
     *
     * @since 3.2
     */

    private static int getPercentagesByItem(String percentages, String item) throws ApplicationException {
        String tmpString = "";
        if (percentages.indexOf("\"" + item.toLowerCase().replaceAll(" ", "_") + "\"") != -1) {
            int num = percentages.indexOf("\"" + item.toLowerCase().replaceAll(" ", "_") + "\"");
            String parsed = percentages.substring(num + ("\"" + item.toLowerCase().replaceAll(" ", "_") + "\"").length() + 6, percentages.length());
            tmpString = parsed.substring(0, parsed.indexOf("\""));
        }
        if (tmpString.equals("")) {
            return 0;
        }
        try {

            return (Integer.parseInt(tmpString));
        } catch (NumberFormatException f) {
            return 0;
        }
    }

    /**
     * Convenience method in order to properly instantiate a new Locale object, from
     * strings such as "es", "en", etc, or "es_ES", "en_US", etc.
     *
     * @param locale Locale in string format to be used
     * @return A locale object appropriately initialized
     */
    public static Locale getLocaleFromLocaleString(String locale) {
        Locale loc = null;
        if (locale.indexOf("_") > 0){
            String lang = locale.substring(0, locale.indexOf("_"));
            String country = locale.substring(locale.indexOf("_") + 1, locale.length());
            loc = new Locale(lang, country);
        } else {
            loc = new Locale(locale);
        }
        return loc;
    }
    
    /**
     * Returns the localized string for the given property
     *
     * @param propertyName Property whose value is to be retrieved
     * @return The localized value for the property, or null if not found.
     */
    public static String getLocalizedReportString(final String propertyName, String locale) {
        String value = "";
        String lang = "";
        ResourceBundle bundle = null;
        
        if (locale.indexOf("_") > 0){            
            try {
                bundle = ResourceBundle.getBundle(REPORTS_BUNDLE + "_" + locale);
                return bundle.getString(propertyName);
            } catch (MissingResourceException e) {
                // Fallback for country resource to language 
                lang = locale.substring(0, locale.indexOf("_"));
            }
        } else {
            lang = locale;
        }

        
        try {
            if (lang.equals("en")) {
                bundle = ResourceBundle.getBundle(REPORTS_BUNDLE);
            } else {
                bundle = ResourceBundle.getBundle(REPORTS_BUNDLE + "_" + lang);
            }
            value = bundle.getString(propertyName);
        } catch (MissingResourceException e) {
            if (!propertyName.equals("")) {
                value = "Missing Resource: " + propertyName;
            }
        }
        return value;
    }

    /**
     * Returns the localized string for the given property
     *
     * @param propertyName Property whose value is to be retrieved
     * @param locale
     * @return The localized value for the property, or null if not found.
     */
    public static String getLocalizedString(final String propertyName, String locale) {
        String value = "";
        String lang = "";
    
        ResourceBundle bundle = null;
        
        if (locale.indexOf("_") > 0){
            try {
                bundle = ResourceBundle.getBundle(RESOURCE_BUNDLE + "_" + locale);
                return bundle.getString(propertyName);
            } catch (MissingResourceException e) {
                // Fallback for country resource to language 
                lang = locale.substring(0, locale.indexOf("_"));
            }
        } else {
            lang = locale;
        }
        
        try {
            if (lang.equals("en")) {
                bundle = ResourceBundle.getBundle(RESOURCE_BUNDLE);
            } else {
                bundle = ResourceBundle.getBundle(RESOURCE_BUNDLE + "_" + lang);
            }
            value = bundle.getString(propertyName);
        } catch (MissingResourceException e) {
            if (!propertyName.equals("")) {
                value = "Missing Resource: " + propertyName;
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return value;
    }
    
    /**
     * Returns a collection of Lookup localizations for the given bundleKey
     * @param bundleKey Resource whose translations need to be retrieved
     * @return A collection of translations, empty if nothing is found
     */
    public static Collection<LookupLocalizationVO> getLocalizationFromBundleKey(String bundleKey){
        Collection<LookupLocalizationVO> localizations = new ArrayList();
        // Get all the existent languages using the bundle key
        Iterator it = LANGUAGES_MAP.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<Integer,String> entry = (Map.Entry)it.next();
            LookupLocalizationVO l = new LookupLocalizationVO();
            l.setName(Common.getLocalizedString(bundleKey, entry.getValue()));
            l.setLanguage_id(entry.getKey());
            localizations.add(l);
        }
        return localizations;
    }

    public static String getConfig(String strPropertyName) {
        String strValue = "";
        try {
            ConfigurationServiceImpl c = new ConfigurationServiceImpl();

            if (CONFIG == null || CONFIG.get(strPropertyName).equals("")) {
                Hashtable config = c.getConfiguration();

                CONFIG = config;
                if (CONFIG.get(strPropertyName).equals("")) {
                    return "-1";
                }
            }
            strValue = (String) CONFIG.get(strPropertyName);
        } catch (Exception e) {
            return "";

        }
        if (strValue == null) {
            strValue = "";
        }
        return strValue;
    }

    public static ArrayList getSortedAlphaList(ArrayList alphaslist) throws ApplicationException {
        ArrayList tmpList = alphaslist;
        Collections.sort(tmpList, new Comparator() {
            public int compare(Object o1, Object o2) {
                WoundAssessmentLocationVO obj1 = (WoundAssessmentLocationVO) o1;
                WoundAssessmentLocationVO obj2 = (WoundAssessmentLocationVO) o2;
                String combinedAlpha1 = "";
                String combinedAlpha2 = "";

                //for incision tags
                if ("T".equalsIgnoreCase(obj1.getWound_profile_type().getAlpha_type())) {
                    combinedAlpha1 = "A" + obj1.getAlpha();
                }
                if ("T".equalsIgnoreCase(obj2.getWound_profile_type().getAlpha_type())) {
                    combinedAlpha2 = "A" + obj2.getAlpha();
                }

                //for alphas
                if ("A".equalsIgnoreCase(obj1.getWound_profile_type().getAlpha_type())) {
                    combinedAlpha1 = "B" + obj1.getAlpha();
                }
                if ("A".equalsIgnoreCase(obj2.getWound_profile_type().getAlpha_type())) {
                    combinedAlpha2 = "B" + obj2.getAlpha();
                }

                //for stomas
                if ("O".equalsIgnoreCase(obj1.getWound_profile_type().getAlpha_type())) {
                    if (obj1.getAlpha().indexOf("ostu") != -1) {
                        combinedAlpha1 = "C" + "1";
                    } else {
                        combinedAlpha1 = "C" + "2";
                    }
                }
                if ("O".equalsIgnoreCase(obj2.getWound_profile_type().getAlpha_type())) {
                    if (obj2.getAlpha().indexOf("ostu") != -1) {
                        combinedAlpha2 = "C" + "1";
                    } else {
                        combinedAlpha2 = "C" + "2";
                    }
                }

                //for drains
                if ("D".equalsIgnoreCase(obj1.getWound_profile_type().getAlpha_type())) {
                    combinedAlpha1 = "D" + obj1.getAlpha();
                }
                if ("D".equalsIgnoreCase(obj2.getWound_profile_type().getAlpha_type())) {
                    combinedAlpha2 = "D" + obj2.getAlpha();
                }

                return combinedAlpha1.compareTo(combinedAlpha2);
            }
        });

        return tmpList;
    }
    /*

     * Organizes the Sinus Tract results for the Flowchart

     *

     * @return String the sinus tract results organized to be shown in the interface

     * @param vector the vector of sinus_tract results

     * @param which whether the passed in sinus_tract array is the sinus_tract dimensions or clock

     *

     * @since 3.2

     */

    public static String parseMultipleTracts(Collection<AssessmentSinustractsVO> vector, boolean which) throws ApplicationException {

        String value = "";
        int count = 1;
        for (AssessmentSinustractsVO sinus : vector) {
            if (which == true) {
                value = value + " #" + count + " " + sinus.getDepth_cm() + "." + sinus.getDepth_mm() + " ,";
            } else {
                value = value + " #" + count + " " + sinus.getLocation_start() + ":00 ,";
            }
            count++;
        }
//              remove comma from value
        if (!value.equals("")) {
            return value.substring(0, value.length() - 1);
        } else {
            return "";
        }
    }
    /*
     * Organizes the Undermining results for the Flowchart
     *
     * @return String the undermining results organized to be shown in the interface
     * @param vector the vector of undermining results
     * @param which whether the passed in undermining array is the undermining dimensions or clock
     *
     * @since 3.2
     */

    public static String parseMultipleUndermining(Collection<AssessmentUnderminingVO> vector, boolean which, String locale) throws ApplicationException {

        String value = "";
        int count = 1;
        if (vector != null) {
            for (AssessmentUnderminingVO undermining : vector) {
                if (which == true) {
                    value = value + "#" + (count) + " " + undermining.getDepth_cm() + "." + undermining.getDepth_mm() + " ,";
                } else {
                    value = value + "#" + (count) + " " + undermining.getLocation_start() + ":00 " + Common.getLocalizedString("pixalere.woundassessment.form.to", locale) + " " + undermining.getLocation_end() + ":00 ,";
                }
                count++;
            }
        }
        //remove comma from value
        if (!value.equals("")) {
            return value.substring(0, value.length() - 1);
        } else {
            return "";
        }
    }

    public static String escapeSingleQuote(String value) {
        value = value.replaceAll("'", "\\\\'");
        return value;
    }
    /*
     * Organizes the Undermining results for the Flowchart
     *
     * @return String the undermining results organized to be shown in the interface
     * @param vector the vector of undermining results
     * @param which whether the passed in undermining array is the undermining dimensions or clock
     *
     * @since 3.2
     */

    public static String parseMultipleUndermining(Vector<LocationDepthVO> vector, boolean which, String locale) throws ApplicationException {

        String value = "";
        int count = 1;
        if (vector != null) {
            for (LocationDepthVO undermining : vector) {
                if (which == true) {
                    value = value + "#" + (count) + " " + undermining.getDepth_cm() + "." + undermining.getDepth_mm() + " ,";
                } else {
                    value = value + "#" + (count) + " " + undermining.getLocation_start() + ":00 " + Common.getLocalizedString("pixalere.woundassessment.form.to", locale) + " " + undermining.getLocation_end() + ":00 ,";
                }
                count++;
            }
        }
        //remove comma from value
        if (!value.equals("")) {
            return value.substring(0, value.length() - 1);
        } else {
            return "";
        }
    }

    /**
     * Using reflection to get fields from an Object without knowing their name.
     * This method simply checks if a method exists.
     *
     * @param field_name the name of the field we need to extract
     * @param assess the object we're extracting it from.
     * @return if method exists
     */
    public boolean ifMethodExists(String field_name, Object assess) throws ApplicationException {

        try {
            Class thisClass = assess.getClass();

            String first_char = field_name.substring(0, 1);
            String remaining = field_name.substring(1, field_name.length());
            String method_name = "get" + first_char.toUpperCase() + remaining;
            Method method = thisClass.getMethod(method_name);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Using reflection to get fields from an Object without knowing their name.
     *
     * @param field_name the name of the field we need to extract
     * @param assess the object we're extracting it from.
     * @return the value we extracted.
     */
    public static Object getFieldValue(String field_name, Object assess) throws ApplicationException {
        Object result = null;
        try {
            Class thisClass = assess.getClass();
            String first_char = field_name.substring(0, 1);
            String remaining = field_name.substring(1, field_name.length());
            String method_name = "get" + first_char.toUpperCase() + remaining;
            Method method = thisClass.getMethod(method_name);
            result = method.invoke(assess);

            return result;
        } catch (NoSuchMethodException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Using reflection to set fields in an Object without knowing their name.
     *
     * @param component data on the field component we're setting.
     * @param assess the object we're setting
     * @param value the value we're inputing.
     * @return the new object we've updated
     */
    public static Object setFieldValue(ComponentsVO component, Object assess, String value) throws ApplicationException {

        try {
            Class thisClass = assess.getClass();

            String first_char = component.getField_name().substring(0, 1);
            String remaining = component.getField_name().substring(1, component.getField_name().length());
            String method_name = "set" + first_char.toUpperCase() + remaining;
            Class partypes[] = new Class[1];
            if (component.getField_type().equals(Constants.INTEGER)) {
                partypes[0] = Integer.class;
            } else if (component.getField_type().equals(Constants.STRING)) {
                partypes[0] = String.class;
            } else if (component.getField_type().equals(Constants.DOUBLE)) {
                partypes[0] = Double.class;
            } else if (component.getField_type().equals(Constants.DATE)) {
                partypes[0] = Date.class;
            }
            Method method = thisClass.getMethod(method_name, partypes);
            Object[] args = new Object[1];
            if (component.getField_type().equals(Constants.STRING) && component.getComponent_type().equals(Constants.CHECKBOX_COMPONENT_TYPE)) {
                args[0] = new String(value);
            } else if (component.getField_type().equals(Constants.INTEGER)) {
                if (value.equals("")) {
                    value = "0";
                }
                args[0] = new Integer(value);
            } else if (component.getField_type().equals(Constants.STRING)) {
                args[0] = new String(value);
            } else if (component.getField_type().equals(Constants.DATE)) {
                System.out.println("DATER: " + value);
                args[0] = new Date(Long.parseLong(value));//passing epoch.
            } else if (component.getField_type().equals(Constants.DOUBLE)) {
                args[0] = new Double(value);
            }
            method.invoke(assess, args);

            return assess;
        } catch (Exception e) {
            e.printStackTrace();
            //return orig. object.
            return assess;
        }
    }

    public static boolean recordChanged(List<FieldValues> fields) throws ApplicationException {
        if (fields != null) {
            for (FieldValues f : fields) {
                if (f.getValue_changed() == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String calculateLimbScore(int num1, int num2) throws ApplicationException {
        if (num2 == 0) {
            return "";
        } else if (num1 == 0 && num2 == 0) {
            return "";
        }
        Float f1 = Float.parseFloat(num1 + "");
        Float f2 = Float.parseFloat(num2 + "");

        Float value = (f1 / f2);
        value = Round(value, 2);
        return value + "";
    }

    public static String stripCarriageReturns(String data) {
        //legacy - strip carriage returns..
        //reusing legacy method to purge invalid chars.
        data.replaceAll("\\", "/");
        return data;
    }

    public static String convertCarriageReturns(String data, boolean reverse) {
        if(data!=null){
            data.replaceAll("Â", "");
            data.replaceAll("\u00a0", " ");
        }
        //legacy, no longer needed.
        return data;
    }

    /*
     * Returns the Flowchart row to be shown in the interface
     *
     * @return Vector the vector of hashtables which contain each title/column for the flowchart.
     * @param column the column of the flowchart
     * @param row the row of the flowchart
     * @param value the value to be shown in x/y of the flowchart
     * @param vector the vector which we are accumlating to draw the flowchart.
     *
     * @since 3.2
     */
    public static Vector getFlowchartRow(int column, int row, String value, Vector vector) {
        if (value == null) {
            value = "";
        }
        if (column == 0) {
            Hashtable t = new Hashtable();
            t.put("title", value);
            vector.add(row, t);
        } else if (column > 0) {
            Hashtable t = (Hashtable) vector.get(row);
            t.put("profile" + column, value);
            vector.set(row, t);
        }
        return vector;
    }
    /*
     * Return the nursing fixes html code for the interface
     *
     * @return String the html code with the nursing signature/fix
     * @param col the collection of nursing fixes
     * @refactor should seperate out into Common.class
     * @since 3.2
     */

    public static String showNursingFixes(Collection<NursingFixesVO> coll, boolean blnEmbedCode, String locale) throws ApplicationException {
        String result = "";
        if (coll != null) {
            for (NursingFixesVO nursing : coll) {
                // Viewer: blnEmbedCode = true
                // Reporting: blnEmbedCode = false
                String strCorrectionStart = "";
                String strCorrectionEnd = "";
                String strOldValue = "";

                if (!nursing.getField().equals("226")) { // If not "alpha moved"
                    strCorrectionStart = "<del>";
                    strCorrectionEnd = "</del>";

                    strOldValue = Common.getLocalizedString("pixalere.viewer.old_value", locale) + ": ";
                }
                if (blnEmbedCode) {

                    result = result + " <label class=\"hintanchor\" onMouseover=\"showhint('" + Common.getLocalizedString("pixalere.viewer.correction", locale) + ": " + nursing.getInitials() + ((nursing.getDelete_reason() != null && !nursing.getDelete_reason().equals("")) ? (Common.getLocalizedString("pixalere.viewer.form.delete_reason", locale) + ": " + (nursing.getDelete_reason() != null ? nursing.getDelete_reason() : "")) : "") + "', this, event, '250px')\"><font color=red>" + strCorrectionStart + nursing.getError().trim() + strCorrectionEnd + "</font></label> ";
                } else {
                    result = result + " (" + Common.getLocalizedString("pixalere.viewer.correction", locale) + ": " + nursing.getInitials() + "; " + strOldValue + nursing.getError() + ")";
                }
            }
        }
        return result;
    }

    public static void writeToFile(String fileName, InputStream iStream,
            boolean createDir)
            throws IOException {
        String me = "FileUtils.WriteToFile";
        if (fileName == null) {
            throw new IOException(me + ": filename is null");
        }
        if (iStream == null) {
            throw new IOException(me + ": InputStream is null");
        }

        File theFile = new File(fileName);
        System.out.println("PAth: " + fileName);
        // Check if a file exists.
        if (theFile.exists()) {
            String msg
                    = theFile.isDirectory() ? "directory" : (!theFile.canWrite() ? "not writable" : null);
            if (msg != null) {
                throw new IOException(me + ": file '" + fileName + "' is " + msg);
            }
        }

        // Create directory for the file, if requested.
        if (createDir && theFile.getParentFile() != null) {
            theFile.getParentFile().mkdirs();
        }

        // Save InputStream to the file.
        BufferedOutputStream fOut = null;
        try {
            fOut = new BufferedOutputStream(new FileOutputStream(theFile));
            byte[] buffer = new byte[32 * 1024];
            int bytesRead = 0;
            while ((bytesRead = iStream.read(buffer)) != -1) {
                fOut.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            throw new IOException(me + " failed, got: " + e.toString());
        } finally {
            iStream.close();
            fOut.close();
        }
    }

    public static void saveFile(DataHandler dh, String fname) {
        if (dh != null) {
            Object o;
            try {
                o = dh.getContent();
            } catch (IOException ioe) {
                o = ioe;
                ioe.printStackTrace();
            }

            DataSource ds = dh.getDataSource();

            if (!fname.equals("")) {

                try {
                    Common.writeToFile(fname, ds.getInputStream(), true);
                } catch (IOException ioe) {

                    ioe.printStackTrace(System.err);
                }

            }
        }
    }

    public static String convertToFixedDigits(int intInput, int intDigits) {
        if (intInput < 0) {
            intInput = 0;
        }
        String strResult = Integer.toString(intInput);
        while (strResult.length() < intDigits) {
            strResult = "0" + strResult;
        }
        return strResult;
    }

    public static String convertToFixedLength(String strInput, int intDigits) {
        int intTemp = 0;
        try {
            intTemp = Integer.parseInt(strInput);
        } catch (NumberFormatException e) {
            intTemp = 0;
        }
        if (intTemp < 0) {
            intTemp = 0;
        }
        String strResult = Integer.toString(intTemp);
        while (strResult.length() < intDigits) {
            strResult = "0" + strResult;
        }
        return strResult;
    }

    public static String replaceSubString(String strInput, String strSubString, String strNewSubString) {
        int intStartPos = strInput.indexOf(strSubString);
        while (strSubString != "" && intStartPos > -1) {
            strInput = strInput.substring(0, intStartPos) + strNewSubString + strInput.substring(intStartPos + strSubString.length());
            intStartPos = strInput.indexOf(strSubString);
        }
        return strInput;
    }

    public static String removeEndString(String strInput, String strSubString) {
        strInput = strInput.trim();
        while (strInput.length() > 0 && strSubString.length() <= strInput.length() && strInput.substring(strInput.length() - strSubString.length()).equals(strSubString)) {
            strInput = strInput.substring(0, strInput.length() - strSubString.length());
        }
        return strInput;
    }

    public static String removeSubString(String strInput, String strSubString) {
        int intStartPos = strInput.indexOf(strSubString);
        while (strSubString != "" && intStartPos > -1) {
            strInput = strInput.substring(0, intStartPos) + strInput.substring(intStartPos + strSubString.length());
            intStartPos = strInput.indexOf(strSubString);
        }
        return strInput;
    }

    public static boolean getLocationAccess(String strLocation, String[] strTreatmentLocations, String strIncludeTIP) {
        boolean blnItemFound = false;
        if (strLocation.equals("-1")) {
            if (strIncludeTIP.equals("1")) {
                blnItemFound = true;
            }
        } else {
            if (strTreatmentLocations != null) {
                int intItem = 0;
                while (intItem < strTreatmentLocations.length && blnItemFound == false) {
                    if (strTreatmentLocations[intItem].equals(strLocation)) {
                        blnItemFound = true;
                    }
                    intItem++;
                }
            }
        }
        return blnItemFound;
    }

    public static float getFloatFromString(String strValue) {
        float fltResult = 0.0F;
        if (strValue == null) {
            strValue = "";
        }
        strValue = strValue.trim();
        // First check the string
        int intPoint = 0;
        boolean blnInvalidChar = false;
        int intChar = 0;
        while (intChar < strValue.length() && blnInvalidChar == false && intPoint < 2) {
            int intCharValue = (int) strValue.charAt(intChar);
            if (intCharValue == 46) {
                intPoint++;
            } else {
                if (intCharValue < 48 || intCharValue > 57) {
                    blnInvalidChar = true;
                }
            }
            intChar++;
        }

        if (strValue.length() > 0 && blnInvalidChar == false && intPoint < 2) {
            try {
                fltResult = Float.valueOf(strValue).floatValue();
            } catch (Exception e) {
            }
        }
        return fltResult;
    }

    public static String parseWoundBed(Collection<AssessmentWoundBedVO> woundBed, Integer language) {
        ListsDAO dao = new ListsDAO();
        LookupVO listVO = new LookupVO();
        listVO.setResourceId(new Integer(listVO.WOUND_BED_PERCENT));
        Collection<LookupVO> wound_base_percentages = null;
        try {
            wound_base_percentages = dao.findAllIncludingInactiveByCriteria(listVO);// get all wound_bed items
        } catch (Exception e) {
            e.printStackTrace();
        }

        String v = "";
        for (AssessmentWoundBedVO w : woundBed) {
            try {
                LookupVO look = (LookupVO) dao.findByPK(w.getWound_bed());
                if (look != null) {
                    v = v + "" + look.getName(language) + ": " + w.getPercentage() + "% ,";
                }
            } catch (DataAccessException e) {
            }
            // System.out.println("WoundParse: "+v);
        }

        if (v != "") {
            v = v.substring(0, v.length() - 2); // Remove comma at end
        }
        return v;
    }
    /*

     * Organizes the Seperations results for the Flowchart

     *

     * @return String the seperations results organized to be shown in the interface

     * @param vector the vector of seperations results

     * @param which whether the passed in seperations array is the seperations dimensions or clock

     *

     * @since 3.2

     */

    public static String parseMultipleSeperations(Collection<AssessmentMucocSeperationsVO> vector, boolean which, String locale) throws ApplicationException {

        String value = "";
        int count = 1;
        if (vector != null) {
            for (AssessmentMucocSeperationsVO m : vector) {
                if (which == true) {
                    value = value + "#" + (count) + " " + m.getDepth_cm() + "." + m.getDepth_mm() + " ,";
                } else {
                    value = value + "#" + (count) + " " + m.getLocation_start() + ":00 " + Common.getLocalizedString("pixalere.woundassessment.form.to", locale) + " " + m.getLocation_end() + ":00 ,";
                }
                count++;
            }
        }
        //remove comma from value
        if (!value.equals("")) {
            return value.substring(0, value.length() - 1);
        } else {
            return "";
        }
    }

    public static boolean checkWoundIsOpen(String strStatus) {
        boolean blnResult = true;
        if (strStatus != null) {
            if (strStatus.equalsIgnoreCase("Closed")) {
                blnResult = false;
            }
        }
        return blnResult;
    }

    public boolean getPartialSelectedLocations(String[] strTreatmentLocations, Collection<UserAccountRegionsVO> regions) throws ApplicationException {
        boolean blnSelection = false; // false = no selection, all locations
        if (strTreatmentLocations != null) {

            int intItem = 0;
            for (UserAccountRegionsVO region : regions) {

                try {
                    ListServiceImpl listBD = new ListServiceImpl(Constants.ENGLISH);
                    LookupVO vo = listBD.getListItem(region.getTreatment_location_id());
                    if (vo != null && vo.getActive() == 1 && blnSelection == false) //
                    {
                        int intItem2 = 0;
                        boolean blnItemFound = false;
                        while (intItem2 < strTreatmentLocations.length && blnItemFound == false) {
                            if (strTreatmentLocations[intItem2].equals(region.getTreatment_location_id())) {
                                blnItemFound = true;
                            }
                            intItem2++;
                        }
                        if (blnItemFound == false) {
                            blnSelection = true;
                        }
                    }
                    intItem++;
                } catch (Exception e) {
                }
            }
            return blnSelection;
        } else {
            return true; // Nothing selected, result is "selection" because not all locations were selected
        }
    }

    public static String getClientInfo() {

        javax.sql.DataSource ds = null;
        String clientinfo = "";
        try {

            Context initContext = new InitialContext();

            Context envContext = (Context) initContext.lookup("java:/comp/env");

            ds = (javax.sql.DataSource) envContext.lookup("jdbc/pixDS");

        } catch (NamingException ne) {

            throw new RuntimeException("Unable to aquire data source", ne);

        }

        try {

            int count = 0;

            java.sql.Connection con;
            con = ds.getConnection();
            clientinfo = con.toString();
            con.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return clientinfo;
    }

    public static int getInt(ResultSet set, String field) {
        try {
            if (set != null) {
                set.beforeFirst();
                while (set.next()) {
                    if (set.getInt(field) == -1) {
                        return 0;
                    } else {
                        return set.getInt(field);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
        } catch (Exception s) {
            s.printStackTrace();
        }
        return 0;
    }

    public static double getDouble(ResultSet set, String field) {
        try {
            if (set != null) {
                set.beforeFirst();
                while (set.next()) {
                    if (set.getDouble(field) == -1) {
                        return 0;
                    } else {
                        return set.getDouble(field);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
        } catch (Exception s) {
            s.printStackTrace();
        }
        return 0;
    }

    public static String getString(ResultSet set, String field) {
        try {
            if (set != null) {
                set.beforeFirst();
                while (set.next()) {
                    String s = set.getString(field);
                    if (s == null) {
                        s = "";
                    }

                    return s;
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
            e.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }
        return "";
    }

    public static String getDashboardValue(
            HashMap<String, DashboardVO> dashboard, 
            String row_order,
            String key
        ) {
        String value = "";
        
        DashboardVO row = dashboard.get(row_order);
        if (row != null){
            if (key.indexOf("q1") != -1) {
                value = row.getQuarter1();
            } else if (key.indexOf("q2") != -1) {
                value = row.getQuarter2();
            } else if (key.indexOf("q3") != -1) {
                value = row.getQuarter3();
            } else if (key.indexOf("q4") != -1) {
                value = row.getQuarter4();
            } else if (key.indexOf("q5") != -1) {
                value = row.getQuarter5();
            } else if (key.indexOf("q6") != -1) {
                value = row.getQuarter6();
            } else if (key.indexOf("q7") != -1) {
                value = row.getQuarter7();
            } else if (key.indexOf("q8") != -1) {
                value = row.getQuarter8();
            } else if (key.equals("ly")) {
                value = row.getLastYear();
            } else if (key.equals("yb")) {
                value = row.getYearBeforeLast();
            }
        }
        
        return value;
    }
    
    public static HashMap<String, DashboardVO> parseDashboardResult(
            String report_name, 
            int orderby, 
            String[][] column_names, 
            String key, 
            ResultSet result, 
            HashMap<String, DashboardVO> tmpList, 
            String format,
            boolean rightAlign) {
        try {
            if (result != null) {

                result.beforeFirst();
                while (result.next()) {
                    for (String[] column_name : column_names) {
                        String name = column_name[0];

                        if (tmpList.get("row" + orderby) == null) {
                            DashboardVO h = new DashboardVO();
                            h.setFormat(format);
                            h.setReportName(report_name);
                            h.setName(name);
                            h.setOrderby(orderby);
                            h.setRightAlign(rightAlign);
                            tmpList.put("row" + orderby, h);
                        }

                        DashboardVO row = (DashboardVO) tmpList.get("row" + orderby);

                        double count = result.getDouble(column_name[1]);
                        //long countLong = Math.round(count);
                        //System.out.println("Result: "+name+" :::"+count+" = "+column_name[1]+" - "+result.getString(column_name[1]));
                        row = setDashboardField(row, key, count + "");
                        tmpList.put("row" + orderby, row);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }
        return tmpList;
    }

    public static HashMap<String, DashboardVO> parseDashboardResult(
            String report_name, 
            int orderby, 
            String[][] column_names, 
            String key, 
            double result, 
            HashMap<String, DashboardVO> tmpList, 
            String format,
            boolean rightAlign) {
        try {

            for (String[] column_name : column_names) {
                String name = column_name[0];

                if (tmpList.get("row" + orderby) == null) {
                    DashboardVO h = new DashboardVO();
                    h.setFormat(format);
                    h.setReportName(report_name);
                    h.setName(name);
                    h.setOrderby(orderby);
                    h.setRightAlign(rightAlign);
                    tmpList.put("row" + orderby, h);
                }

                DashboardVO row = (DashboardVO) tmpList.get("row" + orderby);

                double count = result;
                //long countLong = Math.round(count);
                //System.out.println("Result: "+name+" :::"+count+" = "+column_name[1]+" - "+result.getString(column_name[1]));
                row = setDashboardField(row, key, count + "");
                tmpList.put("row" + orderby, row);
            }

        } catch (Exception s) {
            s.printStackTrace();
        }
        return tmpList;
    }
    
    // String as Value - Blank columns
    public static HashMap<String, DashboardVO> parseDashboardResult(
            String report_name, 
            int orderby, 
            String[][] column_names, 
            String key, 
            String result, 
            HashMap<String, DashboardVO> tmpList, 
            String format,
            boolean rightAlign) {
        try {

            for (String[] column_name : column_names) {
                String name = column_name[0];

                if (tmpList.get("row" + orderby) == null) {
                    DashboardVO h = new DashboardVO();
                    h.setFormat(format);
                    h.setReportName(report_name);
                    h.setName(name);
                    h.setOrderby(orderby);
                    h.setRightAlign(rightAlign);
                    tmpList.put("row" + orderby, h);
                }

                DashboardVO row = (DashboardVO) tmpList.get("row" + orderby);
                // Blank columns
                
                row = setDashboardField(row, key, result);    
                tmpList.put("row" + orderby, row);
            }

        } catch (Exception s) {
            s.printStackTrace();
        }
        return tmpList;
    }

    /**
     * @todo Move to service
     * @param row
     * @param key
     * @return
     */
    public static DashboardVO setDashboardField(DashboardVO row, String key, String count) {
        try {
            //count = Common.Round(count,2);
            if (key.indexOf("q1") != -1) {
                row.setQuarter1(count + "");
            } else if (key.indexOf("q2") != -1) {
                row.setQuarter2(count + "");
            } else if (key.indexOf("q3") != -1) {
                row.setQuarter3(count + "");
            } else if (key.indexOf("q4") != -1) {
                row.setQuarter4(count + "");
            } else if (key.indexOf("q5") != -1) {
                row.setQuarter5(count + "");
            } else if (key.indexOf("q6") != -1) {
                row.setQuarter6(count + "");
            } else if (key.indexOf("q7") != -1) {
                row.setQuarter7(count + "");
            } else if (key.indexOf("q8") != -1) {
                row.setQuarter8(count + "");
            } else if (key.equals("ly")) {
                row.setLastYear(count + "");
            } else if (key.equals("yb")) {
                row.setYearBeforeLast(count + "");
            }
        } catch (Exception e) {
        }
        return row;
    }

    /**
     * @todo Move to service
     * @param row
     * @param key
     * @return
     */
    public static String getDashboardField(DashboardVO row, String key) {
        String count = "";
        try {

            if (key.indexOf("q1") != -1) {
                count = row.getQuarter1();
            } else if (key.indexOf("q2") != -1) {
                count = row.getQuarter2();
            } else if (key.indexOf("q3") != -1) {
                count = row.getQuarter3();
            } else if (key.indexOf("q4") != -1) {
                count = row.getQuarter4();
            } else if (key.indexOf("q5") != -1) {
                count = row.getQuarter5();
            } else if (key.indexOf("q6") != -1) {
                count = row.getQuarter6();
            } else if (key.indexOf("q7") != -1) {
                count = row.getQuarter7();
            } else if (key.indexOf("q8") != -1) {
                count = row.getQuarter8();
            } else if (key.equals("ly")) {
                count = row.getLastYear();
            } else if (key.equals("yb")) {
                count = row.getYearBeforeLast();
            }
        } catch (Exception e) {
        }
        return count;
    }

    public static int checkForInt(String item) {
        if (item == null || item.equals("")) {
            return -1;
        } else {
            try {
                return Integer.parseInt(item);
            } catch (ClassCastException e) {
                return -1;
            }
        }
    }

    /**
     * Compare Pixalere Value Objects from Lists.
     *
     * @param first
     * @param second
     * @return true if they match, false otherwise.
     */
    public static boolean equals(Object[] first, Object[] second) {
        boolean equals = true;
        try {
            for (Object item1 : first) {
                boolean match_found = false;
                for (Object item2 : second) {
                    if (item1 instanceof InvestigationsVO) {
                        InvestigationsVO i1 = (InvestigationsVO) item1;
                        if (item2 instanceof InvestigationsVO) {
                            InvestigationsVO i2 = (InvestigationsVO) item2;
                            if (i1.getLookup_id().equals(i2.getLookup_id()) && i1.getInvestigation_date().equals(i2.getInvestigation_date())) {
                                match_found = true;
                            }
                        }
                    }
                }
                if (match_found == false) {
                    equals = false;
                    break;
                }
            }
        } catch (NullPointerException e) {
            //log.error("NullPointerException: Common.equals() : "+e.getMessage());
        }
        return equals;

    }
    
    public static boolean translateInformationPopup(String locale){
        boolean result = false;
        String DATASOURCE_CONTEXT = "java:comp/env/jdbc/pixDS";
        String fileName = "/db/update/information_popup_" + locale + ".sql";
        
        List<String> queries = getQueriesFromFile(fileName);
        
        if (!queries.isEmpty()){
            try {
                Context initialContext = new InitialContext();
                javax.sql.DataSource datasource = (javax.sql.DataSource) initialContext.lookup(DATASOURCE_CONTEXT);
                Connection conn = datasource.getConnection();

                if (conn != null) {
                    Statement st = conn.createStatement();
                    for (String query : queries) {
                        try {
                            st.executeUpdate(query);
                        } catch (MySQLSyntaxErrorException e) {
                            System.out.println("ERROR at: " + query);
                        }
                        
                    }
                    System.out.println("Table information_popup translated to " + locale + " - Entries: " + queries.size());
                    result = true;
                } else {
                    System.out.println("Failed to obtain connnection.");
                }
            } catch (NamingException ex) {
                ex.printStackTrace();
                System.out.println("Cannot find JDBC resource: " + ex);
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.out.println("SQL Error: " + ex);
            } 
        }
        return result;
    }
    
    private static List<String> getQueriesFromFile(String fileName){
        List<String> list = new ArrayList<String>();
        String line = "";
        
        try {
            // FileReader fr = new FileReader(new File(fileName));
            InputStream ir = Common.class.getResourceAsStream(fileName);
            InputStreamReader reader = new InputStreamReader(ir, "UTF-8");
            BufferedReader br = new BufferedReader(reader);
            
            while((line = br.readLine()) != null) {
                line = line.trim();
                
                // Ignore lines starting with '#'
                if (line.startsWith("#")) line = "";
                // Ignore lines starting with '--'
                if (line.startsWith("--")) line = "";
                // Ignore lines that DON'T finish with ';'
                if (!line.endsWith(";")) line = "";
                // Add only non empty lines
                if (!line.isEmpty()) list.add(line);
            }
            br.close();
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Cannot open file: " + fileName + "\n" + ex);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Error reading file: " + fileName + "\n" + ex);
        }
        return list;
    }

    public static boolean isDBPatched() {
        String DATASOURCE_CONTEXT = "java:comp/env/jdbc/pixDS";

        Connection result = null;
        try {
            Context initialContext = new InitialContext();
            if (initialContext == null) {
                System.out.println("JNDI problem. Cannot get InitialContext.");
            }
            javax.sql.DataSource datasource = (javax.sql.DataSource) initialContext.lookup(DATASOURCE_CONTEXT);
            if (datasource != null) {
                result = datasource.getConnection();
                Flyway flyway = new Flyway();
                flyway.setDataSource(datasource);
                //flyway.setLocations("com.pixalere.migration");

                if (flyway.info().current().getState().isFailed()) {
                    return false;
                } else {
                    return true;
                }

            } else {
                System.out.println("Failed to lookup datasource.");
                return false;
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
            System.out.println("Cannot get connection: " + ex);
            return false;
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Cannot get connection: " + ex);
            return false;
        }

    }

    public static String migrationStatus() {
        String DATASOURCE_CONTEXT = "java:comp/env/jdbc/pixDS";

        Connection result = null;
        try {
            Context initialContext = new InitialContext();
            if (initialContext == null) {
                System.out.println("JNDI problem. Cannot get InitialContext.");
            }
            javax.sql.DataSource datasource = (javax.sql.DataSource) initialContext.lookup(DATASOURCE_CONTEXT);
            if (datasource != null) {
                result = datasource.getConnection();
                Flyway flyway = new Flyway();
                flyway.setDataSource(datasource);
                //flyway.setLocations("com.pixalere.migration");
                String info = "";
                for (MigrationInfo i : flyway.info().all()) {
                    info = "migrate task: " + i.getVersion() + " : "
                            + i.getDescription() + " from file: " + i.getScript();
                }
                return info;
            } else {
                System.out.println("Failed to lookup datasource.");
                return "";
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
            System.out.println("Cannot get connection: " + ex);
            return "";
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Cannot get connection: " + ex);
            return "";
        }

    }

    public static int getRandomNumber(int high) {
        //note a single Random object is reused here
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(high);
        return randomInt;

    }
}
