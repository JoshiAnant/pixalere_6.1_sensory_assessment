/*
 * MemberDAO.java
 *
 * Created on August 24, 2002, 5:25 PM
 */
package com.pixalere.auth.dao;

import com.pixalere.utils.Constants;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.QueryByCriteria;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.auth.bean.PasswordHistoryVO;
import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.utils.Common;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.common.bean.EmailVO;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.ValueObject;
import com.pixalere.auth.bean.ProfessionalVO;

/**
 * This class is responsible for handling all CRUD logic associated with the
 * professional_accounts table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class UserDAO implements DataAccessObject {
    // Create Log4j category instance for logging

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UserDAO.class);

    public UserDAO() {
    }

    /**
     * Deletes professional_accounts records, as specified by the value object
     * passed in. Fields in Value Object which are not null will be used as
     * parameters in the SQL statement. Retrieves all records by criteria, then
     * deletes them.
     *
     * @param deleteRecord the value object which specifies which records should
     * be deleted
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering UserDAO.delete()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ProfessionalVO userVO = (ProfessionalVO) deleteRecord;
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(userVO);
            broker.commitTransaction();

        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.delete()***********************");
    }

    public void deletePosition(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering UserDAO.delete()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            PositionVO userVO = (PositionVO) deleteRecord;
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(userVO);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.delete()***********************");
    }

    /**
     * Finds a all professional_accounts records by the parameters. If no record
     * is found a null value is returned.
     *
     * @param id of the professional_account
     * @param lastname of the professional
     * @param treatment_id of the professional
     * @todo should be refactored out and included in findAllByCriteriaa
     *
     * @since 3.0
     */
    public Vector findAllBySearch(int id, String firstname, String lastname, int position, String treatment_id,Collection<UserAccountRegionsVO> regions,String employeeid) throws DataAccessException {
        // logs.info("************Entering the
        // PatientDAO.findAllByCriteria()***************");
        PersistenceBroker broker = null;
        Vector<ProfessionalVO> results = new Vector();
        Vector<ProfessionalVO> newResults = new Vector();
        ProfessionalVO user = null;
        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        try {
            user = new ProfessionalVO();
            if (id != 0) {
                user.setId(new Integer(id));
            }
            if (!lastname.equals("")) {
                user.setLastname_search(Common.stripName(lastname));
            }
            if (!firstname.equals("")) {
                user.setFirstname_search(Common.stripName(firstname));
            }
            if (!employeeid.equals("")) {
                user.setEmployeeid(Common.stripName(employeeid));
            }
            if (position != 0 && position != -1) {
                user.setPosition_id(position);
            }
            //user.setAccount_status(new Integer(1));
            QueryByCriteria query = new QueryByCriteria(user);
            query.addOrderByAscending("lastname");
            broker = ConnectionLocator.getInstance().findBroker();
            Collection r = (Collection) broker.getCollectionByQuery(query);
            Iterator iter = r.iterator();
            while (iter.hasNext()) {
                ProfessionalVO u = (ProfessionalVO) iter.next();
                if (!treatment_id.equals("") && !treatment_id.equals("0") && u.getRegions() != null) {
                    if ((pservice.validate(u.getRegions(), new Integer(treatment_id)) == true)) {
                        results.add(u);
                    }
                } else {
                    results.add(u);
                }
            }
            if(regions!=null){
                for(ProfessionalVO p : results){
                    newResults.add(p);
                }
                int cnt = 1;

                for(ProfessionalVO p : results){
                    boolean found = false;
                    for(UserAccountRegionsVO reg : p.getRegions()){
                        for(UserAccountRegionsVO myr : regions){
                            if(reg.getTreatment_location_id().equals(myr.getTreatment_location_id()) && !myr.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)){
                                found = true;
                            }
                        }
                    }
                    if(!found){
                        newResults.remove(p);
                    }else{
                        cnt++;
                    }
                    
                }
            }else{
                for(ProfessionalVO p : results){
                    newResults.add(p);
                }
            }
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException error in User.findAllBySearch(): " + e);
            throw new DataAccessException("ServiceLocatorException error in User.findAllBySearch()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        // logs.info("************Done with
        // StoryDAO.findAllStories()***************");
        return newResults;
    }

    /**
     * Finds a single professional_accounts record, as specified by the primary
     * key value passed in. If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the professional_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        log.info("***********************nooya UserDAO.findByPK()**************====*********");
        PersistenceBroker broker = null;
        ProfessionalVO userVO = new ProfessionalVO();
        ProfessionalVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            userVO.setId(new Integer(primaryKey));
            Query query = new QueryByCriteria(userVO);
            returnVO = (ProfessionalVO) broker.getObjectByQuery(query);
            log.info("****************** UserDAO.findByPK() ********************** ::: " + userVO.getFirstname());
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByPK()", e);
        } catch (NullPointerException e) {
            return returnVO;
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByPK()*************====**********");
        return returnVO;
    }
    //legacy, not really used any longer

    public int getCount() throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        int count = 0;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            Criteria crit = new Criteria();
            QueryByCriteria query2 = null;
            crit.addEqualTo("account_status", new Integer(1));
            query2 = QueryFactory.newQuery(ProfessionalVO.class, crit);
            count = broker.getCount(query2);

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return count;
    }

    public Collection findAllEmailByCriteria() throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        // EmailVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // returnVO = new EmailVO();
            QueryByCriteria query = new QueryByCriteria(new EmailVO());
            query.addOrderByAscending("lastname");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds a all professional_accounts records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection<ProfessionalVO> findAllByCriteria(ProfessionalVO userVO) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ProfessionalVO> results = null;
        ProfessionalVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ProfessionalVO();
            QueryByCriteria query = new QueryByCriteria(userVO);
            query.addOrderByAscending("lastname");
            results = (Collection<ProfessionalVO>) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
    //legacy.. don't use email in pixalere'

    public Vector findAllEmail() throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Vector results = new Vector();
        ProfessionalVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ProfessionalVO();
            ProfessionalVO tmp = new ProfessionalVO();
            tmp.setAccount_status(new Integer(1));
            QueryByCriteria query = new QueryByCriteria(tmp);
            Collection resultsCol = (Collection) broker.getCollectionByQuery(query);
            Iterator iter = resultsCol.iterator();
            while (iter.hasNext()) {
                returnVO = (ProfessionalVO) iter.next();
                if (!returnVO.getEmail().equals("") || !returnVO.getPager().equals("")) {
                    results.add(new EmailVO((returnVO.getEmail() == null) ? "" : returnVO.getEmail(), (returnVO.getPager() == null) ? "" : returnVO.getPager(), returnVO.getFirstname() + " " + returnVO.getLastname()));
                }
            }
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }

    public Collection findAllByCriteria(PositionVO posVO) throws DataAccessException {
        log.info("***********************Entering listsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<PositionVO> results = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();

            QueryByCriteria query = new QueryByCriteria(posVO);
            query.addOrderByAscending("orderby");
            results = broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error(
                    "ServiceLocatorException thrown in ListsDAO.findByCriteria(): "
                    + e.toString(), e);
            throw new DataAccessException(
                    "ServiceLocatorException in ListsDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log
                .info("***********************Leaving ListsDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds a all password_history records, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param pass fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllPasswordsByPK(PasswordHistoryVO pass) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        PasswordHistoryVO returnVO = new PasswordHistoryVO();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pass);
            query.addOrderByDescending("created_on");
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds an professional_accounts record, as specified by the value object
     * passed in. If no record is found a null value is returned.
     *
     * @param ProfessionalVO fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(ProfessionalVO userVO) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ProfessionalVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ProfessionalVO();
            Query query = new QueryByCriteria(userVO);
            returnVO = (ProfessionalVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Finds an password_reset record, as specified by the value object passed
     * in. If no record is found a null value is returned.
     *
     * @param PasswordResetVO fields in ValueObject which are not null will be
     * used as parameters in the SQL statement.
     *
     * @since 6.0
     * @version 6.0
     */
    public ValueObject findByCriteria(PasswordResetVO userVO) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        PasswordResetVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new PasswordResetVO();
            Query query = new QueryByCriteria(userVO);
            returnVO = (PasswordResetVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return returnVO;
    }

    public ValueObject findByCriteria(PositionVO userVO) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        PositionVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new PositionVO();
            Query query = new QueryByCriteria(userVO);
            returnVO = (PositionVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Inserts a single ProfessionalVO object into the professional_accounts
     * table. The value object passed in has to be of type userVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * professional_accounts record.
     *
     * @since 3.0
     */
    public void insert(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        ProfessionalVO userVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            userVO = (ProfessionalVO) insertRecord;
            broker.beginTransaction();
            broker.store(userVO);
            broker.commitTransaction();

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in UserDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in UserDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in UserDAO.insert()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.insert()***********************");
    }

    /**
     * Inserts a single ProfessionalVO object into the professional_accounts
     * table. The value object passed in has to be of type userVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * professional_accounts record.
     *
     * @since 3.0
     */
    public void insertPosition(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        PositionVO userVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            userVO = (PositionVO) insertRecord;
            broker.beginTransaction();
            broker.store(userVO);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in UserDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in UserDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in UserDAO.insert()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.insert()***********************");
    }
    //legacy.. remove.

    public ValueObject findEmailByCriteria(EmailVO ProfessionalVO) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        EmailVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new EmailVO();
            Query query = new QueryByCriteria(ProfessionalVO);
            returnVO = (EmailVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return returnVO;
    }
    //legacy.. remove..

    public void insertEmail(ValueObject insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insert()***********************");
        EmailVO userVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            userVO = (EmailVO) insertRecord;
            EmailVO tmp = (EmailVO) findEmailByCriteria(userVO);
            if (tmp == null) {
                broker.beginTransaction();
                broker.store(userVO);
                broker.commitTransaction();
            }
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.insert(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in UserDAO.insert()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in UserDAO.insert(): " + e.toString());
            throw new DataAccessException("Error in UserDAO.insert()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.insert()***********************");
    }
    //legacy...

    public void deleteEmailForOffline(EmailVO ProfessionalVO) throws DataAccessException {
        log.info("***********************Entering UserDAO.deleteEmail()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(ProfessionalVO);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.delete()***********************");
    }

    public void clearPasswords(int id) {
        log.info(" UserDAO.clearPasswords(): Starting");
        try {
            PasswordHistoryVO vo = new PasswordHistoryVO();
            vo.setProfessional_id(new Integer(id));
            Collection col = findAllPasswordsByPK(vo);
            Iterator iter = col.iterator();
            int count = 0;
            log.info(" UserDAO.clearPasswords(): Collection count:" + col.size());
            while (iter.hasNext()) {
                count++;
                log.info(" UserDAO.clearPasswords(): Delete Patient count: " + count);
                PasswordHistoryVO passVO = (PasswordHistoryVO) iter.next();
                if (count > 6) {
                    log.info(" UserDAO.clearPasswords(): delete patient password record: " + passVO.getId());
                    deletePassword(passVO.getId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("UserDAO.clearPasswords():" + e.getMessage());
        }
    }

    public void deletePassword(Integer id) throws DataAccessException {
        log.info("***********************Entering UserDAO.delete()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            PasswordHistoryVO userVO = new PasswordHistoryVO();
            userVO.setId(id);
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(userVO);
            broker.commitTransaction();

        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.delete()***********************");
    }

    public void insertPasswordForHistory(PasswordHistoryVO insertRecord) throws DataAccessException {
        log.info("***********************Entering MemberDAO.insertPasswordForHistory()***********************");
        PasswordHistoryVO userVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            userVO = (PasswordHistoryVO) insertRecord;
            broker.beginTransaction();
            broker.store(userVO);
            broker.commitTransaction();
            clearPasswords(userVO.getProfessional_id().intValue());

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.insertPasswordForHistory(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in UserDAO.insertPasswordForHistory()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            e.printStackTrace();
            log.error("PersistenceBrokerException thrown in UserDAO.insertPasswordForHistory(): " + e.toString());
            throw new DataAccessException("Error in UserDAO.insertPasswordForHistory()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.insertPasswordForHistory()***********************");
    }

    /**
     * Inserts a single ProfessionalVO object into the professional_accounts
     * table. The value object passed in has to be of type userVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the
     * professional_accounts record.
     *
     * @since 3.0
     */
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering UserDAO.update()***********************");
        ProfessionalVO userVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            userVO = (ProfessionalVO) updateRecord;
            broker.beginTransaction();
            broker.store(userVO);
            broker.commitTransaction();

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in UserDAO.update()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.update(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in UserDAO.update()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Entering UserDAO.update()***********************");
    }

    /**
     * Inserts a single ProfessionalTreatmentAccessVO object into the
     * professional_treatment_access table. The value object passed in has to be
     * of type userVO.
     *
     * @param updateRecord fields in ArrayValueObject represent fields in the
     * professional_accounts record.
     *
     * @since 4.1.2
     */
    public void updateRegion(UserAccountRegionsVO updateRecord) throws DataAccessException {
        log.info("***********************Entering UserDAO.update()***********************");
        UserAccountRegionsVO userVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(updateRecord);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in UserDAO.update()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.update(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in UserDAO.update()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Entering UserDAO.update()***********************");
    }

    /**
     * Inserts a single PasswordResetVO object into the password_reset table.
     *
     * @param updateRecord fields in PasswordResetVO represent fields in the
     * password_reset record.
     *
     * @since 6.0
     */
    public void updateResetToken(PasswordResetVO updateRecord) throws DataAccessException {
        log.info("***********************Entering UserDAO.update()***********************");
        PasswordResetVO userVO = null;
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(updateRecord);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in UserDAO.update()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.update(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in UserDAO.update()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Entering UserDAO.update()***********************");
    }

    /**
     * Find all professionals who belong to a patient's treatment location, and
     * or position assigned
     *
     * @param position_id position assigned
     * @param treatment_location_id treatment location assigned
     *
     * @since 6.0
     */
    public Collection findAllByCriteria(int position_id, int treatment_location_id) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ProfessionalVO> results = null;
        List<ProfessionalVO> professionals = new ArrayList();
        try {
            Criteria crit = new Criteria();
            broker = ConnectionLocator.getInstance().findBroker();
            ProfessionalVO userVO = new ProfessionalVO();

            if (position_id > 0) {
                crit.addEqualTo("position_id", position_id);
            } else {
                Collection<PositionVO> positions = new ArrayList();
                PositionVO pos = new PositionVO();
                pos.setActive(1);
                pos.setReferral_popup(1);
                positions = (Collection<PositionVO>) findAllByCriteria(pos);
                Criteria crit3= new Criteria();
                for (PositionVO posi : positions) {
                    Criteria crit2 = new Criteria();
                    crit2.addEqualTo("position_id",posi.getId());
                    crit3.addOrCriteria(crit2);
                }
                crit.addAndCriteria(crit3);
            }
            try {
                //if (treatment_location_id > 0) {
                //    crit.addEqualTo("treatment_location_id", treatment_location_id);
                //}
            } catch (Exception e) {
                log.error("problem add:" + e.getMessage());
            }
            QueryByCriteria query = QueryFactory.newQuery(ProfessionalVO.class, crit);
            broker = ConnectionLocator.getInstance().findBroker();
            results = (Collection) broker.getCollectionByQuery(query);
            for (ProfessionalVO prof : results) {
                    Collection<UserAccountRegionsVO> regions = prof.getRegions();
                    if (regions != null) {
                        for (UserAccountRegionsVO region : regions) {
                            if (region.getTreatment_location_id().equals(treatment_location_id) && treatment_location_id != Constants.TRAINING_TREATMENT_ID) {
                                professionals.add(prof);
                            }
                        }
                    }
                }
            // }
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return professionals;

    }

    /**
     * Finds a all user_account_regions records, as specified by the value
     * object passed in. If no record is found a null value is returned.
     *
     * @param userVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     *
     * @since 3.0
     */
    public Collection findAllByCriteria(UserAccountRegionsVO userVO) throws DataAccessException {
        log.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(userVO);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Deletes professional_treatment_access records, as specified by the value
     * object passed in. Fields in Value Object which are not null will be used
     * as parameters in the SQL statement. Retrieves all records by criteria,
     * then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should
     * be deleted
     * @see
     * com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ArrayValueObject)
     *
     * @since 3.0
     */
    public void delete(UserAccountRegionsVO deleteRecord) throws DataAccessException {
        log.info("***********************Entering UserDAO.delete()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(deleteRecord);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in UserDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in userVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in UserDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving UserDAO.delete()***********************");
    }
}
