package com.pixalere.auth.client;
import com.pixalere.auth.service.ProfessionalService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class ProfessionalClient extends AbstractClient{
    public ProfessionalClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  ProfessionalService createProfessionalClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(ProfessionalService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"ProfessionalService");
        ProfessionalService service = (ProfessionalService) getClient();       
        return service;
    }
}