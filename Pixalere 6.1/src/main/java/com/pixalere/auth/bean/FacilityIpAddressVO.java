/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.pixalere.auth.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 *
 * @author travis
 */
public class FacilityIpAddressVO  extends ValueObject implements Serializable {
    private Integer ipaddress_id;
    private Integer treatment_location_id;
    private String ip_address;
    public FacilityIpAddressVO() {
    }

    /**
     * @return the ipaddress_id
     */
    public Integer getIpaddress_id() {
        return ipaddress_id;
    }

    /**
     * @param ipaddress_id the ipaddress_id to set
     */
    public void setIpaddress_id(Integer ipaddress_id) {
        this.ipaddress_id = ipaddress_id;
    }

    /**
     * @return the treatment_location_id
     */
    public Integer getTreatment_location_id() {
        return treatment_location_id;
    }

    /**
     * @param treatment_location_id the treatment_location_id to set
     */
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }

    /**
     * @return the ip_address
     */
    public String getIp_address() {
        return ip_address;
    }

    /**
     * @param ip_address the ip_address to set
     */
    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }
}
