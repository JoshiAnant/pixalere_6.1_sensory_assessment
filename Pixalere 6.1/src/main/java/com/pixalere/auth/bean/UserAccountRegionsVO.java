package com.pixalere.auth.bean;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 * Created by IntelliJ IDEA.
 * User: travdes
 * Date: Mar 9, 2010
 * Time: 6:08:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserAccountRegionsVO  implements Serializable {
    private Integer user_account_id;
    private Integer id;
    private Integer treatment_location_id;
    public UserAccountRegionsVO(){}
    public UserAccountRegionsVO(int id,int user_account_id,int treatment_location_id){
        this.id=id;
        this.user_account_id=user_account_id;
        this.treatment_location_id=treatment_location_id;
    }
    public Integer getUser_account_id() {
        return user_account_id;
    }
    public void setUser_account_id(Integer user_account_id) {
        this.user_account_id = user_account_id;
    }
    public Integer getTreatment_location_id() {
        return treatment_location_id;
    }
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }
    private LookupVO treatmentLocation;
    /**
     * @return the lookup
     */
    public LookupVO getTreatmentLocation() {
        return treatmentLocation;
    }
    /**
     * @param lookup the lookup to set
     */
    public void setTreatmentLocation(LookupVO treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
   
}
