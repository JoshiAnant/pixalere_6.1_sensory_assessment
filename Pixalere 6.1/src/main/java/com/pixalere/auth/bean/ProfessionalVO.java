package com.pixalere.auth.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
/** 
 *           professional_accounts
 *          @author Travis Morris (with help from Hibernate)
 *        
*/
public class ProfessionalVO extends ValueObject implements Serializable {
    /** identifier field */
    private Integer id;
    private Integer access_ccac_reporting;
    private Integer question_one;
    private Integer question_two;
    private String answer_one;
    private String answer_two;
    private Date deleted_on;
    private Date lastmodified_on;
    private Date created_on;
    private String user_name;
    /** nullable persistent field */
    private String password;
    private Integer training_flag;
    /** nullable persistent field */
    private Integer account_status;
    private Integer usepatient_timezone;
    private String employeeid;
    /** nullable persistent field */
    private Integer position_id;
    private Integer invalid_password_count;
    private Integer locked;
    /** nullable persistent field */
    private String firstname;
    /** nullable persistent field */
    private String firstname_search;
    /** nullable persistent field */
    private String lastname;
    /** nullable persistent field */
    private String lastname_search;
    /** nullable persistent field */
    private String pager;
    /** nullable persistent field */
    private String email;
    /** nullable persistent field */
    private Integer access_uploader;
    /** nullable persistent field */
    private Integer access_reporting;
    
    /** nullable persistent field */
    private Integer access_viewer;
    /** nullable persistent field */
    private Integer access_admin;
    /** nullable persistent field */
    private Integer access_allpatients;
    /** nullable persistent field */
    private Integer access_assigning;
    /** nullable persistent field */
    private Collection<UserAccountRegionsVO> regions;
    /** nullable persistent field */
    private String individuals;
    /** nullable persistent field */
    private Integer access_professionals;
    /** nullable persistent field */
    private Integer access_patients;
    /** nullable persistent field */
    private Integer access_regions;
    /** nullable persistent field */
    private Integer access_referrals;
    /** nullable persistent field */
    private Integer access_listdata;
    /** nullable persistent field */
    private Integer access_audit;
    /** nullable persistent field */
    private Integer new_user;
    /** nullable persistent field */
    private Integer access_superadmin;
    private Integer access_create_patient;
    private Integer reset_password_method;
    private String timezone;
    private PositionVO position;
    private Integer allow_remote_access;
    private String ip_address;//@todo 4.3 feature, allow lock down to specific ip address or range
    /** default constructor */
    public ProfessionalVO() {
    }
    public ProfessionalVO(Integer id){
        this.id=id;
    }
    /**
	 * @method getUser_name
	 * @field user_name - String
	 * @return Returns the user_name.
	 */
	public String getUser_name() {
		return user_name;
	}
	/**
	 * @method setUser_name
	 * @field user_name - String
	 * @param user_name The user_name to set.
	 */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Integer getAccount_status() {
        return this.account_status;
    }
    public void setAccount_status(Integer account_status) {
        this.account_status = account_status;
    }
    public PositionVO getPosition() {
        return this.position;
    }
    public void setPosition(PositionVO position) {
        this.position = position;
    }
    public String getFirstname() {
        return this.firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getFirstname_search() {
        return this.firstname_search;
    }
    public void setFirstname_search(String firstname_search) {
        this.firstname_search = firstname_search;
    }
    public String getLastname() {
        return this.lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public String getLastname_search() {
        return this.lastname_search;
    }
    public void setLastname_search(String lastname_search) {
        this.lastname_search = lastname_search;
    }
    public String getPager() {
        return this.pager;
    }
    public void setPager(String pager) {
        this.pager = pager;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public Integer getAccess_uploader() {
        return this.access_uploader;
    }
    public void setAccess_uploader(Integer access_uploader) {
        this.access_uploader = access_uploader;
    }
    public Integer getAccess_reporting() {
        return this.access_reporting;
    }
    public void setAccess_reporting(Integer access_reporting) {
        this.access_reporting = access_reporting;
    }
    public Integer getAccess_viewer() {
        return this.access_viewer;
    }
    public void setAccess_viewer(Integer access_viewer) {
        this.access_viewer = access_viewer;
    }
    public Integer getAccess_admin() {
        return this.access_admin;
    }
    public void setAccess_admin(Integer access_admin) {
        this.access_admin = access_admin;
    }
    public Integer getAccess_allpatients() {
        return this.access_allpatients;
    }
    public void setAccess_allpatients(Integer access_allpatients) {
        this.access_allpatients = access_allpatients;
    }
    public Integer getAccess_assigning() {
        return this.access_assigning;
    }
    public void setAccess_assigning(Integer access_assigning) {
        this.access_assigning = access_assigning;
    }
   
    public String getIndividuals() {
        return this.individuals;
    }
    public void setIndividuals(String individuals) {
        this.individuals = individuals;
    }
    public Integer getAccess_professionals() {
        return this.access_professionals;
    }
    public void setAccess_professionals(Integer access_professionals) {
        this.access_professionals = access_professionals;
    }
    public Integer getAccess_patients() {
        return this.access_patients;
    }
    public void setAccess_patients(Integer access_patients) {
        this.access_patients = access_patients;
    }
    public Integer getAccess_regions() {
        return this.access_regions;
    }
    public void setAccess_regions(Integer access_regions) {
        this.access_regions = access_regions;
    }
    public Integer getAccess_referrals() {
        return this.access_referrals;
    }
    public void setAccess_referrals(Integer access_referrals) {
        this.access_referrals = access_referrals;
    }
    public Integer getAccess_listdata() {
        return this.access_listdata;
    }
    public void setAccess_listdata(Integer access_listdata) {
        this.access_listdata = access_listdata;
    }
    public Integer getAccess_audit() {
        return this.access_audit;
    }
    public void setAccess_audit(Integer access_audit) {
        this.access_audit = access_audit;
    }
    
    public Integer getNew_user() {
        return this.new_user;
    }
    public void setNew_user(Integer new_user) {
        this.new_user = new_user;
    }
    public Integer getAccess_superadmin() {
        return this.access_superadmin;
    }
    public void setAccess_superadmin(Integer access_superadmin) {
        this.access_superadmin = access_superadmin;
    }
    public Date getDeleted_on() {
        return deleted_on;
    }
    public void setDeleted_on(Date deletion_date) {
        this.deleted_on = deletion_date;
    }
    public Date getLastmodified_on() {
        return lastmodified_on;
    }
    public void setLastmodified_on(Date updated_date) {
        this.lastmodified_on = updated_date;
    }
    public Date getCreated_on() {
        return created_on;
    }
    public void setCreated_on(Date creation_date) {
        this.created_on = creation_date;
    }
    public Integer getTraining_flag() {
        return training_flag;
    }
    public void setTraining_flag(Integer training_flag) {
        this.training_flag = training_flag;
    }
    public String getFullName(){
        String fullname=getFirstname()+" "+getLastname();
        fullname = fullname.replaceAll("'","\\\\'");
        return fullname;
    }
    /**
     * @return the timezone
     */
    public String getTimezone() {
        return timezone;
    }
    /**
     * @param timezone the timezone to set
     */
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
   /**
     * @return the usepatient_timezone
     */
    public Integer getUsepatient_timezone() {
        return usepatient_timezone;
    }
    /**
     * @param usepatient_timezone the usepatient_timezone to set
     */
    public void setUsepatient_timezone(Integer usepatient_timezone) {
        this.usepatient_timezone = usepatient_timezone;
    }
    /**
     * @return the position_id
     */
    public Integer getPosition_id() {
        return position_id;
    }
    /**
     * @param position_id the position_id to set
     */
    public void setPosition_id(Integer position_id) {
        this.position_id = position_id;
    }
    /**
     * @return the regions
     */
    public Collection<UserAccountRegionsVO> getRegions() {
        return regions;
    }
    /**
     * @param regions the regions to set
     */
    public void setRegions(Collection<UserAccountRegionsVO> regions) {
        this.regions = regions;
    }
public Integer getAccess_create_patient(){
        return access_create_patient;
    }
    public void setAccess_create_patient(Integer access_create_patient){
        this.access_create_patient=access_create_patient;
    }
    /**
     * @return the invalid_password_count
     */
    public Integer getInvalid_password_count() {
        return invalid_password_count;
    }
    /**
     * @param invalid_password_count the invalid_password_count to set
     */
    public void setInvalid_password_count(Integer invalid_password_count) {
        this.invalid_password_count = invalid_password_count;
    }
    /**
     * @return the locked
     */
    public Integer getLocked() {
        return locked;
    }
    /**
     * @param locked the locked to set
     */
    public void setLocked(Integer locked) {
        this.locked = locked;
    }
    /**
     * @return the ip_address
     */
    public String getIp_address() {
        return ip_address;
    }
    /**
     * @param ip_address the ip_address to set
     */
    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }
    public String getName(){
        return firstname+" "+lastname;
    }
    /**
     * @return the access_ccac_reporting
     */
    public Integer getAccess_ccac_reporting() {
        return access_ccac_reporting;
    }
    /**
     * @param access_ccac_reporting the access_ccac_reporting to set
     */
    public void setAccess_ccac_reporting(Integer access_ccac_reporting) {
        this.access_ccac_reporting = access_ccac_reporting;
    }

    /**
     * @return the question_one
     */
    public Integer getQuestion_one() {
        return question_one;
    }

    /**
     * @param question_one the question_one to set
     */
    public void setQuestion_one(Integer question_one) {
        this.question_one = question_one;
    }

    /**
     * @return the question_two
     */
    public Integer getQuestion_two() {
        return question_two;
    }

    /**
     * @param question_two the question_two to set
     */
    public void setQuestion_two(Integer question_two) {
        this.question_two = question_two;
    }

    /**
     * @return the answer_one
     */
    public String getAnswer_one() {
        return answer_one;
    }

    /**
     * @param answer_one the answer_one to set
     */
    public void setAnswer_one(String answer_one) {
        this.answer_one = answer_one;
    }

    /**
     * @return the answer_two
     */
    public String getAnswer_two() {
        return answer_two;
    }

    /**
     * @param answer_two the answer_two to set
     */
    public void setAnswer_two(String answer_two) {
        this.answer_two = answer_two;
    }

    /**
     * @return the reset_password_method
     */
    public Integer getReset_password_method() {
        return reset_password_method;
    }

    /**
     * @param reset_password_method the reset_password_method to set
     */
    public void setReset_password_method(Integer reset_password_method) {
        this.reset_password_method = reset_password_method;
    }

    /**
     * @return the employeeid
     */
    public String getEmployeeid() {
        return employeeid;
    }

    /**
     * @param employeeid the employeeid to set
     */
    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    /**
     * @return the allow_remote_access
     */
    public Integer getAllow_remote_access() {
        return allow_remote_access;
    }

    /**
     * @param allow_remote_access the allow_remote_access to set
     */
    public void setAllow_remote_access(Integer allow_remote_access) {
        this.allow_remote_access = allow_remote_access;
    }
}
