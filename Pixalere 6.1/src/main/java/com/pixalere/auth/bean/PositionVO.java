/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.auth.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 *
 * @author travis
 */
public class PositionVO extends ValueObject implements Serializable{
    private Integer id;
    private Integer orderby;
    private String title;
    private Integer active;
    private Integer referral_popup;
    private Integer recommendation_popup;
    private String colour;
    private Integer enforce_images;
    public  PositionVO(){}
    public PositionVO(int active){
        this.active=active;
    }
    /**
     * @return the orderby
     */
    public Integer getOrderby() {
        return orderby;
    }
    /**
     * @param orderby the orderby to set
     */
    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }
    /**
     * @return the referral_popup
     */
    public Integer getReferral_popup() {
        return referral_popup;
    }
    /**
     * @param referral_popup the referral_popup to set
     */
    public void setReferral_popup(Integer referral_popup) {
        this.referral_popup = referral_popup;
    }
    /**
     * @return the recommendation_popup
     */
    public Integer getRecommendation_popup() {
        return recommendation_popup;
    }
    /**
     * @param recommendation_popup the recommendation_popup to set
     */
    public void setRecommendation_popup(Integer recommendation_popup) {
        this.recommendation_popup = recommendation_popup;
    }
    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }
    /**
     * @param colour the colour to set
     */
    public void setColour(String colour) {
        this.colour = colour;
    }

    /**
     * @return the enforce_images
     */
    public Integer getEnforce_images() {
        return enforce_images;
    }

    /**
     * @param enforce_images the enforce_images to set
     */
    public void setEnforce_images(Integer enforce_images) {
        this.enforce_images = enforce_images;
    }
}
