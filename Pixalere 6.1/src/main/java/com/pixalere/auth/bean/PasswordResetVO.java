package com.pixalere.auth.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;
/** 
 *  Allow a nurse to reset passwords on their own.  This persistence object 
 * represents the database table password_reset.  
 * @author Travis Morris
 * @version 6.0
 * @since 6.0
 *        
*/
public class PasswordResetVO extends ValueObject implements Serializable {
    private String token;
    private Date created_on;
    private Integer professional_id;
    private Integer password_reset_id;
    public PasswordResetVO(){}


    /**
     * @return the created_on
     */
    public Date getCreated_on() {
        return created_on;
    }

    /**
     * @param created_on the created_on to set
     */
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }

    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

 
    /**
     * @return the password_reset_id
     */
    public Integer getPassword_reset_id() {
        return password_reset_id;
    }

    /**
     * @param password_reset_id the password_reset_id to set
     */
    public void setPassword_reset_id(Integer password_reset_id) {
        this.password_reset_id = password_reset_id;
    }

    
}
