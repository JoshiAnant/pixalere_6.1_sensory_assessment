package com.pixalere.auth.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;
/** 
 *           professional_accounts
 *          @author Travis Morris (with help from Hibernate)
 *        
*/
public class PasswordHistoryVO extends ValueObject implements Serializable {
    /** identifier field */
    private Integer id;
    /** nullable persistent field */
    private String password;
    /** nullable persistent field */
    private Integer professional_id;
    /** nullable persistent field */
    private Date created_on;
	
    /** full constructor */
    
    /** default constructor */
    public PasswordHistoryVO() {
    }
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Integer getProfessional_id() {
        return this.professional_id;
    }
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    public Date getCreated_on() {
        return this.created_on;
    }
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
}
