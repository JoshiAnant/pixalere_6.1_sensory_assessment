package com.pixalere.auth.service;
import java.io.IOException;
import java.util.ResourceBundle;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import org.apache.ws.security.WSPasswordCallback;
public class ClientPasswordCallback implements CallbackHandler {
	//private static final String BUNDLE_LOCATION = "com.company.auth.authClient";
	//private static final String PASSWORD_PROPERTY_NAME = "auth.manager.password";	
	private static String password=Constants.PW_TEXT;
	/*static {
		final ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_LOCATION);
		password = bundle.getString(PASSWORD_PROPERTY_NAME);
	}*/	
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
        // set the password for our message.
        pc.setPassword(password);
    }
}