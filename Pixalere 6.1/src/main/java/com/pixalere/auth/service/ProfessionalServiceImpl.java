package com.pixalere.auth.service;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.auth.bean.PasswordResetVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.bean.PasswordHistoryVO;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.assessment.service.WoundAssessmentServiceImpl;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.auth.dao.UserDAO;
import com.pixalere.utils.MD5;
import com.pixalere.utils.PDate;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import javax.jws.WebService;
/**
 *     This is the professional service implementation
 *     Refer to {@link com.pixalere.auth.service.ProfessionalService } to see if there are
 *     any web services available.
 *
 *     <img src="ProfessionalServiceImpl.png"/>
 *
 *
 *     @view  ProfessionalServiceImpl
 *
 *     @match class com.pixalere.auth.*
 *     @opt hide
 *     @match class com.pixalere.auth\.(dao.ProfessionalDAO|bean.ProfessionalVO|service.ProfessionalService)
 *     @opt !hide
 *
 */
@WebService(endpointInterface = "com.pixalere.auth.service.ProfessionalService", serviceName = "ProfessionalService")
public class ProfessionalServiceImpl implements ProfessionalService {
    // Create Log4j category instance for logging
    com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
    
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProfessionalServiceImpl.class);
    /**
     * Used when determining a password's expiry status.
     */
    public enum PasswordStatus {
        EXPIRED(-1), NOT_EXPIRED(0), ONE_DAY_LEFT(1), TWO_DAYS_LEFT(2), THREE_DAYS_LEFT(3),
        FOUR_DAYS_LEFT(4), FIVE_DAYS_LEFT(5);
        private int status;
        private PasswordStatus(int status) {
            this.status = status;
        }
        /**
         * @return
         * <ul>
         * <li>"-1" for expired
         * <li>"1" through "5" for 1 to 5 days from expiry respectively
         * <li>"0" for more than 5 days from expiry
         * </ul>
         * <br/><br/>
         * This is intended to be used for printing out text informing the user
         * how many days are left until expiry. It's not expected that the "0" and
         * "-1" return values will be useful, but they're there for completeness.
         */
        public String toString() {
            return String.valueOf(status);
        }
    }
    public ProfessionalServiceImpl() {
    }
    /**
     * Retrieves the given professional's record (if present) from the database.
     * The database is first queried for a professional_accounts record with a
     * <code>user_name</code> value of <code>userId</code>, and a <code>password</code>
     * value of <code>password</code>. If no such record is found, a second query is
     * made where <code>userId</code> is treated as a <code>userId</code> value, rather
     * than a user name. If either query yields a record, it is returned. Otherwise,
     * null is returned
     *
     * @param userId The user name or user id of the professional
     * @param password The cleartext password of the professional
     * @return The professional's record, or null if not found
     * @throws ApplicationException Thrown if a database i/o error occurs
     */
    public ProfessionalVO validateUserId(String userId, String password) throws ApplicationException {
        UserDAO userDAO = new UserDAO();
        ProfessionalVO returnVO = null;
        //look up the professional based on their username and password
        ProfessionalVO userVO = new ProfessionalVO();
        userVO.setPassword((MD5.hash(password)).toLowerCase());
        userVO.setUser_name(userId);
        userVO.setAccount_status(new Integer(1));
        try {
            
            returnVO = (ProfessionalVO) userDAO.findByCriteria(userVO);
            //if no such user exists, try treating <code>userId</code> as a userId
            //(rather than a user NAME as above)
            if (returnVO == null) {
                ProfessionalVO vo = new ProfessionalVO();
                vo.setPassword((MD5.hash(password)).toLowerCase());
                vo.setId(new Integer(userId));
                vo.setAccount_status(new Integer(1));
                returnVO = (ProfessionalVO) userDAO.findByCriteria(vo);
            }
        } catch (NumberFormatException e) {
            log.warn("Invalid username: "+PDate.getDateTime(new Date()));
        } catch (DataAccessException e) {
            log.error("Error in UserBD.validateUserId(): " + e.toString());
            throw new ApplicationException("Error in UserBD.validateUserId(): " + e.toString(), e);
        }
        //return the retrieved record, or null if not found
        return returnVO;
    }
    /**
     * Retrieves the given professional's record (if present) from the database.
     * The database is first queried for a professional_accounts record with a
     * <code>user_name</code> value of <code>userId</code>, and a <code>password</code>
     * value of <code>password</code>. If no such record is found, a second query is
     * made where <code>userId</code> is treated as a <code>userId</code> value, rather
     * than a user name. If either query yields a record, it is returned. Otherwise,
     * null is returned
     *
     * @param userId The user name or user id of the professional
     * @return The professional's record, or null if not found
     * @throws ApplicationException Thrown if a database i/o error occurs
     */
    public ProfessionalVO getProfessionalByUsernameOrId(String userId) throws ApplicationException {
        UserDAO userDAO = new UserDAO();
        ProfessionalVO returnVO = null;
        //look up the professional based on their username and password
        ProfessionalVO userVO = new ProfessionalVO();
        userVO.setUser_name(userId);
        userVO.setAccount_status(new Integer(1));
        try {
            returnVO = (ProfessionalVO) userDAO.findByCriteria(userVO);
            //if no such user exists, try treating <code>userId</code> as a userId
            //(rather than a user NAME as above)
            if (returnVO == null) {
                ProfessionalVO vo = new ProfessionalVO();
                vo.setId(new Integer(userId));
                vo.setAccount_status(new Integer(1));
                returnVO = (ProfessionalVO) userDAO.findByCriteria(vo);
            }
        } catch (NumberFormatException e) {
            log.warn("Invalid username: "+PDate.getDateTime(new Date()));
        } catch (DataAccessException e) {
            log.error("Error in UserBD.validateUserId(): " + e.toString());
            throw new ApplicationException("Error in UserBD.validateUserId(): " + e.toString(), e);
        }
        //return the retrieved record, or null if not found
        return returnVO;
    }
    /**
     * Determines if a patient profile with the given patient id and professional id exists.
     * If so, returns true.
     * <br/>
     * If not, determines if a wound profile with the given wound id and professional id exists.
     * If so, returns true.
     * <br/>
     * If not, determines if an assessment with the given assessment id and professional id exists.
     * If so, returns true.
     * <br/>
     * If not, returns false.
     * <br/><br/>
     * If <code>patient_id</code>, <code>wound_id</code>, or <code>assessment_id</code> is -1, the
     * check for that particular type of record will be skipped. Behaviour will be the same as if a
     * record of that type had not been found.
     *
     * @param patient_id
     * @param wound_id
     * @param assessment_id
     * @param professional_id
     * @return See method description
     *
     * TODO: (Doug Olsen 06/13/06) Travis: I think someone needs to inspect this method to
     * confirm that its current logic is correct. Specifically: is it intended that "true"
     * is returned if you (for example) pass in a bad patient_id and/or wound_id along with
     * a valid assessment_id?
     */
    public boolean validateProfessionalOnUpload(int patient_id, int wound_id, int assessment_id, int professional_id) throws ApplicationException {
        boolean isAllowed = false;
        PatientProfileServiceImpl pm = new PatientProfileServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundServiceImpl wm = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundAssessmentServiceImpl ae = new WoundAssessmentServiceImpl();
        if (patient_id != -1 && isAllowed == false) {
            PatientProfileVO pp = new PatientProfileVO();
            pp.setPatient_id(patient_id);
            pp.setCurrent_flag(1);
            isAllowed = pm.doesExist(pp, professional_id);
        }
        if (wound_id != -1 && isAllowed == false) {
            WoundProfileVO pp = new com.pixalere.wound.bean.WoundProfileVO();
            pp.setWound_id(wound_id);
            pp.setCurrent_flag(1);
            isAllowed = wm.doesExist(pp, professional_id);
        }
        if (assessment_id != -1 && isAllowed == false) {
            isAllowed = ae.checkAssessmentExistForProfId(assessment_id, professional_id);
        }
        return isAllowed;
    }
    /**
     * Adds/updates a professional's record in the professional_accounts table
     * @param ProfessionalVO The professional's data
     * @throws ApplicationException Thrown if a database i/o error occurs.
     *
     * NOTE: this method expects the password field to be hashed before calling
     * this method!
     */
    public void saveProfessional(ProfessionalVO userVO, int user_id) throws ApplicationException {
        //6441
        try {
            UserDAO userDAO = new UserDAO();
            String pass = userVO.getPassword();
            pass = pass.toLowerCase();//why are we doing this?
            userVO.setPassword(pass);
            userDAO.insert(userVO);
            ProfessionalVO currentProf = (ProfessionalVO)userDAO.findByCriteria(userVO);
            if(currentProf!=null){
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(user_id, currentProf.getId(), (userVO.getId() != null ? com.pixalere.utils.Constants.UPDATE_RECORD : com.pixalere.utils.Constants.CREATING_RECORD), "professional_accounts", currentProf));
            }
        } catch (DataAccessException e) {
            log.error("Error in UserBD.addProfessional(): " + e.toString());
            throw new ApplicationException("Error in UserBD.addProfessional(): " + e.toString(), e);
        }
    }
    /**
     * Adds/updates a position's record in the position table
     * @param Position The position's data
     * @throws ApplicationException Thrown if a database i/o error occurs.
     *
     */
    public void savePosition(PositionVO userVO) throws ApplicationException {
        //6441
        try {
            UserDAO userDAO = new UserDAO();
            userDAO.insertPosition(userVO);
        } catch (DataAccessException e) {
            log.error("Error in UserBD.addProfessional(): " + e.toString());
            throw new ApplicationException("Error in UserBD.addProfessional(): " + e.toString(), e);
        }
    }
    /**
     * Set the <code>account_status</code> field for the given professional
     * to 0 (i.e. disable their account).
     *
     * @param ProfessionalVO VO representing the professional to be deactivated.
     * Only the <code>id</code> value needs to be set.
     *
     * @throws ApplicationException Thrown if a database i/o error occurs.
     *
     * TODO: (Doug Olsen 06/13/06) It seems wrong to be passing in the entire VO
     * when only the id is required. Change to accept an int parameter.
     */
    public void removeProfessional(ProfessionalVO userVO,int user_id) throws ApplicationException {
        try {
            userVO = getProfessional(userVO.getId());
            UserDAO userDAO = new UserDAO();
            userVO.setAccount_status(0);
            userDAO.update(userVO);
            if(userVO!=null && userVO.getId()!=null){
                logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(user_id, userVO.getId(), com.pixalere.utils.Constants.DELETE_RECORD, "professional_accounts", userVO));
            }
        } catch (DataAccessException e) {
            log.error("Error in UserBD.removeProfessional(): " + e.toString());
            throw new ApplicationException("Error in UserBD.removeProfessional(): " + e.toString(), e);
        }
    }
    public void removePosition(PositionVO userVO) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            userDAO.insertPosition(userVO);
        } catch (DataAccessException e) {
            log.error("Error in UserBD.removeProfessional(): " + e.toString());
            throw new ApplicationException("Error in UserBD.removeProfessional(): " + e.toString(), e);
        }
    }
    /**
     * Inserts/updates the given PasswordHistory record in the database.
     * @param password PasswordHistory data to be persisted
     * @throws ApplicationException Thrown if a database i/o error occurs
     */
    public void insertPasswordForHistory(PasswordHistoryVO password) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            userDAO.insertPasswordForHistory(password);
        } catch (DataAccessException e) {
            log.error("UserDAO.insertPasswordForHistory: " + e.getMessage());
            throw new ApplicationException("userBD threw an error: " + e.getMessage(), e);
        }
    }
    /**
     * Determines if a record with the given professional id and password exists in
     * the password_history table.
     *
     * @param prof_id Professional id to search for
     * @param password Password to search for, must be hashed and then forced to lower-case
     * @return true if the record exists, else false.
     */
    public boolean passwordHistoryCheck(String prof_id, String password) {
        try {
            UserDAO userDAO = new UserDAO();
            PasswordHistoryVO hist = new PasswordHistoryVO();
            hist.setProfessional_id(new Integer(prof_id));
            hist.setPassword(password);
            Collection result = userDAO.findAllPasswordsByPK(hist);
            if (result == null || result.size() == 0) {
                return false;
            } else {
                return true;
            }
        } catch (DataAccessException e) {
            log.error("UserDAO.retrievePasswords: " + e.getMessage());
            return false;
        } catch (Exception e) {
            log.error("UserDAO.findAllPasswords");
            return false;
        }
    }
    /**
     * Determine if the password for the given professional has expired.
     * Expiry period is currently hardcoded to 6 months.
     *
     * @param prof_id An ID value from the professional_accounts table.
     * @return <ul>
     * <li><code>PasswordStatus.EXPIRED</code> for an expired password
     * <li><code>PasswordStatus.NOT_EXPIRED</code> for a valid password
     * <li>One of <code>PasswordStatus.ONE_DAY_LEFT</code> through <code>PasswordStatus.FIVE_DAYS_LEFT</code>
     * for an imminently-expiring password
     * </ul>
     */
    public PasswordStatus passwordExpired(String prof_id) {
        try {
            UserDAO userDAO = new UserDAO();
            PasswordHistoryVO hist = new PasswordHistoryVO();
            hist.setProfessional_id(new Integer(prof_id));
            Collection<PasswordHistoryVO> result = userDAO.findAllPasswordsByPK(hist);
            ProfessionalVO user = (ProfessionalVO)userDAO.findByPK(Integer.parseInt(prof_id));
            if(user!=null && user.getTraining_flag() != null && user.getTraining_flag()==1){
                return PasswordStatus.NOT_EXPIRED;
            }
            Object[] his = result.toArray();
            if (result == null || result.size() < 1) {
                return PasswordStatus.NOT_EXPIRED;
            }
            hist = (PasswordHistoryVO) his[0];
            //when the password expires
            //when the password expires
            String expiry = Common.getConfig("passwordExpiringDays");
            long EXPIRY = PDate.getEpochTime(hist.getCreated_on());//+ Constants.EXPIRY_INTERVAL;
            
            try {
                if (!expiry.equals("")) {
                    EXPIRY = EXPIRY + (Long.parseLong(expiry) * Constants.ONE_DAY);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            //now
            final long NOW = new PDate().getEpochTime();
            final long EXPIRY_MINUS_NOW = EXPIRY - NOW;
            if (EXPIRY_MINUS_NOW <= 0) {
                return PasswordStatus.EXPIRED;
            } else if (EXPIRY_MINUS_NOW <= Constants.ONE_DAY) {
                return PasswordStatus.ONE_DAY_LEFT;
            } else if (EXPIRY_MINUS_NOW <= 2 * Constants.ONE_DAY) {
                return PasswordStatus.TWO_DAYS_LEFT;
            } else if (EXPIRY_MINUS_NOW <= 3 * Constants.ONE_DAY) {
                return PasswordStatus.THREE_DAYS_LEFT;
            } else if (EXPIRY_MINUS_NOW <= 4 * Constants.ONE_DAY) {
                return PasswordStatus.FOUR_DAYS_LEFT;
            } else if (EXPIRY_MINUS_NOW <= 5 * Constants.ONE_DAY) {
                return PasswordStatus.FIVE_DAYS_LEFT;
            } else {
                return PasswordStatus.NOT_EXPIRED;
            }
        } catch (DataAccessException e) {
            log.error("UserDAO.retrievePasswords: " + e.getMessage());
            return PasswordStatus.NOT_EXPIRED;
        } catch (Exception e) {
            log.error("UserDAO.retrievePasswords: " + e.getMessage());
            e.printStackTrace();
            return PasswordStatus.NOT_EXPIRED;
        }
    }
    /**
     * Retrieve the professional_accounts record for the given professional id
     *
     * @param primaryKey id of the professional to be retrieved
     * @return Value object for that professional, or null if not found.
     * @throws ApplicationException Thrown if a database i/o error occurrs.
     */
    public ProfessionalVO getProfessional(int primaryKey) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            return (ProfessionalVO) userDAO.findByPK(primaryKey);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in UserBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve the professional_accounts record for the given criteria
     *
     * @param criteria
     * @return Value object for that professional, or null if not found.
     * @throws ApplicationException Thrown if a database i/o error occurrs.
     * @since 6.0
     * @version 6.0
     */
    public ProfessionalVO getProfessional(ProfessionalVO criteria) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            return (ProfessionalVO) userDAO.findByCriteria(criteria);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in UserBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * Retrieve the password_reset token record for the given criteria
     *
     * @param criteria
     * @return Value object for that passwod_reset, or null if not found.
     * @throws ApplicationException Thrown if a database i/o error occurrs.
     * @since 6.0
     * @version 6.0
     */
    public PasswordResetVO getPasswordReset(PasswordResetVO criteria) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            return (PasswordResetVO) userDAO.findByCriteria(criteria);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in UserBD.retrievePatient(): " + e.toString(), e);
        }
    }
    public int getCount() {
        try {
            UserDAO dao = new UserDAO();
            return dao.getCount();
        } catch (DataAccessException e) {
            //throw new ApplicationException("DataAccessException Error in UserBD.retrievePatient(): " + e.toString(),e);
        }
        return 0;
    }
    public PositionVO getPosition(PositionVO pos) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            return (PositionVO) userDAO.findByCriteria(pos);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in UserBD.retrievePatient(): " + e.toString(), e);
        }
    }
    /**
     * Retrieves all professional_accounts records with the given id, last name, and treatment id.
     * @param id
     * @param last_name
     * @param treatment_id
     * @return Vector containing all professional_accounts records matching the search criteria.
     * @throws ApplicationException
     */
    public Vector findAllBySearch(int id, String first_name, String last_name, int position, String treatment_id,Collection<UserAccountRegionsVO> regions,String employeeid) throws ApplicationException {
        Vector results = new Vector();
        try {
            UserDAO dao = new UserDAO();
            results = dao.findAllBySearch(id, first_name, last_name, position, treatment_id,regions,employeeid);
        } catch (DataAccessException e) {
            log.error("UserBD error: " + e.getMessage());
            throw new ApplicationException("UserBD threw an error: " + e.getMessage(), e);
        }
        return results;
    }
    public Collection<PositionVO> getPositions(PositionVO pos) throws ApplicationException {
        try {
            UserDAO dao = new UserDAO();
            return (Collection) dao.findAllByCriteria(pos);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
    public Collection<ProfessionalVO> getProfessionals(ProfessionalVO pos) throws ApplicationException {
        try {
            UserDAO dao = new UserDAO();
            return (Collection) dao.findAllByCriteria(pos);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
    public Collection<ProfessionalVO> getProfessionalsByAccess(int position_id,int treatment_location_id) throws ApplicationException {
        try {
            UserDAO dao = new UserDAO();
            return (Collection) dao.findAllByCriteria(position_id,treatment_location_id);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
    public boolean validate(Collection<UserAccountRegionsVO> regions, Integer id) {
        if (regions != null) {
            for (UserAccountRegionsVO region : regions) {
                if (region != null && region.getTreatment_location_id() != null && region.getTreatment_location_id().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }
    public Collection getAllTreatmentLocations(UserAccountRegionsVO assess) throws ApplicationException {
        try {
            UserDAO woundAssessmentDAO = new UserDAO();
            return woundAssessmentDAO.findAllByCriteria(assess);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    public void removeTreatmentLocation(int professional_id) throws ApplicationException {
        try {
            UserDAO dao = new UserDAO();
            if (professional_id > 0) {
                UserAccountRegionsVO p = new UserAccountRegionsVO();
                p.setUser_account_id(professional_id);
                Collection<UserAccountRegionsVO> regions = getAllTreatmentLocations(p);
                for (UserAccountRegionsVO region : regions) {
                    dao.delete(region);
                }
            }
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in AssessmentManagerBD.retrieveWoundAssessment(): " + e.toString(), e);
        }
    }
    /**
     * Adds/updates a professional treatment location record in the professional_treatment_access table
     * @param userVO The professional's data
     * @throws ApplicationException Thrown if a database i/o error occurs.
     *
     *
     */
    public void saveTreatmentLocation(UserAccountRegionsVO userVO) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            userDAO.updateRegion(userVO);
        } catch (DataAccessException e) {
            log.error("Error in UserBD.addProfessional(): " + e.toString());
            throw new ApplicationException("Error in UserBD.addProfessional(): " + e.toString(), e);
        }
    }
     /**
     * Adds/updates a password token record in the password_reset table
     * @param token data
     * @throws ApplicationException Thrown if a database i/o error occurs.
     *
     *
     */
    public void savePasswordReset(PasswordResetVO userVO) throws ApplicationException {
        try {
            UserDAO userDAO = new UserDAO();
            userDAO.updateResetToken(userVO);
        } catch (DataAccessException e) {
            log.error("Error in UserBD.addProfessional(): " + e.toString());
            throw new ApplicationException("Error in UserBD.addProfessional(): " + e.toString(), e);
        }
    }
    public  Collection<PositionVO> getReferralPositions() throws ApplicationException {
        try {
            UserDAO dao = new UserDAO();
            PositionVO pos = new PositionVO();
            pos.setActive(1);
            pos.setReferral_popup(1);
            return (Collection) dao.findAllByCriteria(pos);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ListBD.retrieveAllItemsByResourceId(): " + e.toString(), e);
        }
    }
}
