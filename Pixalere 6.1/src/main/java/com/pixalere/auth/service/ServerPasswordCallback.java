package com.pixalere.auth.service;
/**
 *
 * @author aruld
 */
import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.WSSecurityException;
/*
 * Web service authentication.  Returns web service password for WSSec to compare
 * client / server password and handle appropriately.
 *
 * @author tmorris
 * @version 4.1
 */
public class ServerPasswordCallback implements CallbackHandler {
    //this can be moved to a resource bundle
    private static final String username = "pix";
    private static final String password = Constants.PW_TEXT;
    
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
   
        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
        String strClientPwd = pc.getPassword();
        int usage = pc.getUsage();
        if(pc.getIdentifier().equals(username)) {
            pc.setPassword(password);
        }       
    }
}
