package com.pixalere.auth.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.auth.bean.ProfessionalVO;
import javax.jws.WebService;
import javax.jws.WebParam;
import com.pixalere.auth.bean.PositionVO;
@WebService
public interface ProfessionalService {
    public ProfessionalVO getProfessional(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveProfessional(@WebParam(name="updateRecord") ProfessionalVO updateRecord,@WebParam(name="user_id") int user_id) throws ApplicationException;
    public ProfessionalVO validateUserId(@WebParam(name="user_id") String user_id, @WebParam(name="password") String password) throws ApplicationException;
    public Collection<PositionVO> getPositions(@WebParam(name="position")PositionVO position) throws ApplicationException;
}