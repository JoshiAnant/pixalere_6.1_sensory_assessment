package com.pixalere.admin.client;
import com.pixalere.common.service.ListService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class ListClient extends AbstractClient{
    public ListClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  ListService createListClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(ListService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"ListService");
        ListService service = (ListService) getClient();       
        return service;
    }
}