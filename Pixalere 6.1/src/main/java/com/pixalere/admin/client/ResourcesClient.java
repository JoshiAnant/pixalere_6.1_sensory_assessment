
package com.pixalere.admin.client;
import com.pixalere.admin.service.ResourcesService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class ResourcesClient extends AbstractClient{
    public ResourcesClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  ResourcesService createResourceClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(ResourcesService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"ResourcesService");
        ResourcesService service = (ResourcesService) getClient();       
        return service;
    }
}
