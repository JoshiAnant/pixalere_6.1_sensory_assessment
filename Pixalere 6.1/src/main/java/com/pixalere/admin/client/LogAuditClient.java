package com.pixalere.admin.client;
import com.pixalere.admin.service.LogAuditService;
import com.pixalere.common.AbstractClient;
import com.pixalere.utils.Common;
/**
 * JAX-WS bean to define the web service url and inherits the AbstractClient.
 *
 * @author travis morris
 * @since 4.2
 * @see AbstractClient
 *
 */
public class LogAuditClient extends AbstractClient{
    public LogAuditClient (){ }
    /**
     * Setup the client, and define the web service URL.
     *
     * @return service JAX-WS Proxy for remote invocation
     */
    public  LogAuditService createLogAuditClient(){//int user_id,String password) {
    	
    	factory.setServiceClass(LogAuditService.class);
    	factory.setAddress(Common.getLocalizedString("pixalere.service_address","en")+"LogAuditService");
        LogAuditService service = (LogAuditService) getClient();       
        return service;
    }
}