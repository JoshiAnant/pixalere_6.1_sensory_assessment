package com.pixalere.admin.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.admin.bean.LogAuditVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface LogAuditService {
    public LogAuditVO getLogAudit(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveLogAudit(@WebParam(name="updateRecord") LogAuditVO updateRecord) throws ApplicationException;
    
}