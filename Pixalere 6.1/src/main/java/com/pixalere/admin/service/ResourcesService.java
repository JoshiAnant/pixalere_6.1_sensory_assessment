package com.pixalere.admin.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.admin.bean.ResourcesVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface ResourcesService {
    public ResourcesVO getResource(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveResource(@WebParam(name="updateRecord") ResourcesVO updateRecord) throws ApplicationException;
    public Collection<ResourcesVO> getAllResources() throws ApplicationException;
}