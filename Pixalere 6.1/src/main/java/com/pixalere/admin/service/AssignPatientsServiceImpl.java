package com.pixalere.admin.service;
import com.pixalere.admin.bean.AssignPatientsVO;
import org.apache.log4j.Logger;
import java.util.Collection;
import com.pixalere.common.*;
import com.pixalere.admin.dao.AssignPatientsDAO;
import javax.jws.WebService;
/**
 *     This is the assign patients service implementation
 *     Refer to {@link com.pixalere.admin.service.AssignPatientsService } to see if there are
 *     any web services available.
 *
 *     <img src="AssignPatientsServiceImpl.png"/>
 *
 *
 *     @view  AssignPatientsServiceImpl
 *
 *     @match class com.pixalere.admin.*
 *     @opt hide
 *     @match class com.pixalere.admin\.(dao.AssignPatientsDAO|bean.AssignPatientsVO|service.AssignPatientsService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.admin.service.AssignPatientsService", serviceName = "AssignPatientsService")
public class AssignPatientsServiceImpl implements AssignPatientsService {
	
	public AssignPatientsServiceImpl() {
		
	}
        
    
	// Create Log4j category instance for logging
	static private Logger log = org.apache.log4j.Logger.getLogger(AssignPatientsServiceImpl.class);
	/**
	 * Inserts the Assigned Patient / Professional link to the
	 * account_assignments table, this provides the linkage between patients and
	 * professionals if the professional does not have 'all_patients' access.
	 * 
	 * @param assignPatientsVO
	 *            an account_assignments record to be inserted.
	 * @see AssignPatientsVO
     * @stereotype update
	 */
	public void saveAssignedPatient(AssignPatientsVO assignPatientsVO) throws ApplicationException {
		try {
			AssignPatientsDAO assignPatientsDAO = new AssignPatientsDAO();
			assignPatientsDAO.update(assignPatientsVO);
		} catch (DataAccessException e) {
			log.error("Error in AssignPatientsBD.addItem: " + e.toString());
			throw new ApplicationException("Error in AssignPatientsBD.addItem: " + e.toString(), e);
		}
	}
        /**
	 * Retrieve the Assigned Patient / Professional link to the
	 * account_assignments table, this provides the linkage between patients and
	 * professionals if the professional does not have 'all_patients' access.
	 * 
	 * @param assignPatientsVO
	 *            an account_assignments record to be retrieved.
	 * @see AssignPatientsVO
       @dependency <<instantiate>> AssignPatientsVO
       @stereotype query
	 */
	public AssignPatientsVO getAssignedPatient(AssignPatientsVO assignPatientsVO) throws ApplicationException {
		try {
			AssignPatientsDAO assignPatientsDAO = new AssignPatientsDAO();
			AssignPatientsVO pat=(AssignPatientsVO)assignPatientsDAO.findByCriteria(assignPatientsVO);
                        return pat;
		} catch (DataAccessException e) {
			log.error("Error in AssignPatientsBD.addItem: " + e.toString());
			throw new ApplicationException("Error in AssignPatientsBD.addItem: " + e.toString(), e);
		}
	}
	/**
	 * This method removes the passed account_assignement record completely from
	 * the database.
	 * 
	 * @param assignPatientsVO
	 *            a account_assignment record set by the criteria from the
	 *            interface.
	 * @see AssignPatientsVO
	 */
	public void removeAssignedPatient(AssignPatientsVO assignPatientsVO) throws ApplicationException {
		try {
			AssignPatientsDAO assignPatientsDAO = new AssignPatientsDAO();
			// assignPatientsVO.setActive(new Integer(0));
			assignPatientsDAO.delete(assignPatientsVO);
		} catch (DataAccessException e) {
			log.error("Error in AssignPatientsBD.removeItem: " + e.toString());
			throw new ApplicationException("Error in AssignPatientsBD.removeItem: " + e.toString(), e);
		}
	}
	/**
	 * Returns an AssignPatientsVO object that can be shown on the screen.
	 * <p>
	 * This method always returns immediately, whether or not the 
	 * record exists.  The Object will return NULL if the record does not
	 * exist.  
	 *
	 * @param  primaryKey  the id of the account_assignment record
	 * @return      the account_assignment record
	 * @see         AssignPatientsVO
	 */
	public AssignPatientsVO getAssignedPatient(int primaryKey) throws ApplicationException {
		try {
			AssignPatientsDAO assignPatientsDAO = new AssignPatientsDAO();
			return (AssignPatientsVO) assignPatientsDAO.findByPK(primaryKey);
		} catch (DataAccessException e) {
			throw new ApplicationException("DataAccessException Error in AssignPatientsBD.retrieveItem(): " + e.toString(), e);
		}
	}
        /**
	 * Returns an AssignPatientsVO objects that can be shown on the screen.
	 * <p>
	 * This method always returns immediately, whether or not the 
	 * record exists.  The Object will return NULL if the record does not
	 * exist.  
	 *
	 * @param  AssignPatientsVO  
	 * @return      the account_assignment records
	 * @see         AssignPatientsVO
	 */
	public Collection<AssignPatientsVO> getAllAssignedPatients(AssignPatientsVO a) throws ApplicationException {
		try {
			AssignPatientsDAO assignPatientsDAO = new AssignPatientsDAO();
			return (Collection<AssignPatientsVO>) assignPatientsDAO.findAllByCriteria(a);
		} catch (DataAccessException e) {
			throw new ApplicationException("DataAccessException Error in AssignPatientsBD.retrieveItem(): " + e.toString(), e);
		}
	}
}
