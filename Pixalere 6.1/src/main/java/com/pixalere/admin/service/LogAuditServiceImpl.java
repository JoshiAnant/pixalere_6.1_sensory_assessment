package com.pixalere.admin.service;
import java.util.Collection;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.common.*;
import com.pixalere.admin.dao.LogAuditDAO;
import javax.jws.WebService;
/**
 *     This is the assign patients service implementation
 *     Refer to {@link com.pixalere.admin.service.AssignPatientsService } to see if there are
 *     any web services available.
 *     <img src="ListServiceImpl.png"/>
 *
 *     @view ListServiceImpl
 *
 *     @match class com.pixalere.admin.*
 *     @opt hide
 *     @match class com.pixalere.admin\.(dao.ListsDAO|bean.LookupVO|service.ListService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.admin.service.LogAuditService", serviceName = "LogAuditService")
public class LogAuditServiceImpl implements LogAuditService{
	// Create Log4j category instance for logging
	static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LogAuditServiceImpl.class);
	
	
        public LogAuditServiceImpl(){}
    
	/**
	 * Inserts the LogAudit object to the logs_access table, each time a patient is searched
	 * in the 'patient id" field next to the search button (a patient is accessed). A new logaudit record is 
	 * isnerted into the database.
	 * 
	 * @param logAuditVO the logs_access record
	 * @see LogAuditVO
	 */
	public void saveLogAudit(LogAuditVO logAuditVO) throws ApplicationException {
		try {
			LogAuditDAO logAuditDAO = new LogAuditDAO();
			logAuditDAO.update(logAuditVO);
		} catch (DataAccessException e) {
			log.error("Error in LogAuditBD.addItem: " + e.toString());
			throw new ApplicationException("Error in LogAuditBD.addItem: " + e.toString(), e);
		}
	}
	/**
	 * Returns an LogAuditVO object that can be shown on the screen.
	 * <p>
	 * This method always returns immediately, whether or not the 
	 * record exists.  The Object will return NULL if the record does not
	 * exist.  
	 *
	 * @param  primaryKey  the id of the logs_access record
	 * @return      the logs_access record
	 * @see         LogAuditVO
	 */
	public LogAuditVO getLogAudit(int primaryKey) throws ApplicationException {
		try {
			LogAuditDAO logAuditDAO = new LogAuditDAO();
			return (LogAuditVO) logAuditDAO.findByPK(primaryKey);
		} catch (DataAccessException e) {
			throw new ApplicationException("DataAccessException Error in LogAuditBD.retrieveItem(): " + e.toString(), e);
		}
	}
        public java.util.List<String> getTableNames() throws ApplicationException {
		try {
			LogAuditDAO logAuditDAO = new LogAuditDAO();
			return  logAuditDAO.getTableNames();
		} catch (DataAccessException e) {
			throw new ApplicationException("DataAccessException Error in LogAuditBD.retrieveItem(): " + e.toString(), e);
		}
	}
        /**
         * Returns an array of LogAudit recirds
         * <p/>
         * This method always returns whether or not the records exist.   The array will return NULL if the records do not exist.
         *
         * @param logtmp the criteria we're looking for.
         * @return logs
         * @see LogAuditVO
         */
        public Collection<LogAuditVO> getLogs(LogAuditVO logtmp) throws ApplicationException{
            try {
			LogAuditDAO logAuditDAO = new LogAuditDAO();
			Collection<LogAuditVO> logs = logAuditDAO.findAllByCriteria(logtmp,null,null,null);
                        return logs;
		} catch (DataAccessException e) {
			throw new ApplicationException("DataAccessException Error in LogAuditBD.retrieveItem(): " + e.toString(), e);
		}
        }
        
        /**
         * Returns an array of LogAudit recirds
         * <p/>
         * This method always returns whether or not the records exist.   The array will return NULL if the records do not exist.
         *
         * @param logtmp the criteria we're looking for.
         * @param start_date the start date for the criteria
         * @param end_date the end date for the criteria
         * @return logs
         * @see LogAuditVO
         */
        public Collection<LogAuditVO> getLogs(LogAuditVO logtmp,java.util.Date start_date,java.util.Date end_date,String table_name) throws ApplicationException{
            try {
			LogAuditDAO logAuditDAO = new LogAuditDAO();
			Collection<LogAuditVO> logs = logAuditDAO.findAllByCriteria(logtmp,start_date,end_date,table_name);
                        return logs;
		} catch (DataAccessException e) {
			throw new ApplicationException("DataAccessException Error in LogAuditBD.retrieveItem(): " + e.toString(), e);
		}
        }
}
