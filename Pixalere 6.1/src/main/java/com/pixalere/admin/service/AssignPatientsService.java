package com.pixalere.admin.service;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.admin.bean.AssignPatientsVO;
import javax.jws.WebService;
import javax.jws.WebParam;
/**
 * Web services defined for our remote invocation
 *
 * @author travis morris
 * @since 4.2
 *
 */
@WebService
public interface AssignPatientsService {
    //@todo validation on data
    public AssignPatientsVO getAssignedPatient(@WebParam(name="primaryKey") int primaryKey) throws ApplicationException;
    public void saveAssignedPatient(@WebParam(name="updateRecord") AssignPatientsVO updateRecord) throws ApplicationException;
    public Collection<AssignPatientsVO> getAllAssignedPatients(@WebParam(name="assignedPatients") AssignPatientsVO assignedPatients) throws ApplicationException;
  
}