package com.pixalere.admin.service;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.admin.dao.*;
import com.pixalere.common.*;
import com.pixalere.utils.*;
import java.util.Vector;
import java.util.Collection;
import javax.jws.WebService;
import com.pixalere.common.bean.LookupLocalizationVO;
/**
 *     This is the resources service implementation
 *     Refer to {@link com.pixalere.admin.service.ResourcesService } to see if there are
 *     any web services available.
 *
 *     <img src="ResourcesServiceImpl.png"/>
 *
 *
 *     @view  ResourcesServiceImpl
 *
 *     @match class com.pixalere.admin.*
 *     @opt hide
 *     @match class com.pixalere.admin\.(dao.ResourcesDAO|bean.ResourcesVO|service.ResourcesService)
 *     @opt !hide
 *
*/
@WebService(endpointInterface = "com.pixalere.admin.service.ResourcesService", serviceName = "ResourcesService")
public class ResourcesServiceImpl implements ResourcesService{
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ResourcesServiceImpl.class);
    
    
    //default constructor
    
    public ResourcesServiceImpl(){}
    
    /**
     * Inserts the Resources from manage resources into the Resources table.
     *
     * @param LookupVO
     *            an resouces record to be inserted.
     * @see ResourcesVO
     */
    public void saveResource(ResourcesVO lookupVO) throws ApplicationException{
        try{
            ResourcesDAO listDAO=new ResourcesDAO();
            listDAO.update(lookupVO);
        } catch(DataAccessException e){
            log.error("Error in ListBD.addItem: " + e.toString());
            throw new ApplicationException("Error in ListBD.addItem: " + e.toString(),e);
        }
    }
    /**
     * Returns an ResourcesVO object that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @param  id  the id of the resources record
     * @return      the resources record
     * @see         ResourcesVO
     */
    public ResourcesVO getResource(int id) throws ApplicationException{
        try{
            ResourcesDAO listDAO = new ResourcesDAO();
            return (ResourcesVO) listDAO.findByPK(id);
        } catch (DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(),e);
        }
    }
    /**
     * Returns all ResourcesVO object that can be shown on the screen.
     * <p>
     * This method always returns immediately, whether or not the
     * record exists.  The Object will return NULL if the record does not
     * exist.
     *
     * @return      all resources record
     * @see         ResourcesVO
     */
    public Collection<ResourcesVO> getAllResources() throws ApplicationException{
        try{
            ResourcesDAO listDAO = new ResourcesDAO();
            ResourcesVO r = new ResourcesVO();
            Collection<ResourcesVO> c = listDAO.findAllByCriteria(r);
            return c;
        } catch (DataAccessException e){
            throw new ApplicationException("DataAccessException Error in PatientManagerBD.retrievePatient(): " + e.toString(),e);
        }
    }
    /**
     * Returns a Vector of Resources
     * <p>
     * This method always returns immediately, whether or not the
     * vector is empty.  The method is meant to be called with multiple resource_id's. A
     * good exmaple of this is treatment_categories.
     *
     * @param  array an array of resource_ids
     * @return      Vector a vector of LookupVO objects contain categories
     */
    public Vector getAllResources(int[] array) throws ApplicationException{
        Vector results=new Vector();
        ResourcesDAO resBD=new ResourcesDAO();
        ListsDAO listdao=new ListsDAO();
        
        for(int x : array){
            try{
                ResourcesVO tmpVO=new ResourcesVO();
                LookupVO fakeVO=new LookupVO();
                LookupVO vo=new LookupVO();
                
                tmpVO.setResourceId(new Integer(x));
                ResourcesVO resVO=(ResourcesVO)resBD.findByCriteria(tmpVO);
                fakeVO.setId(resVO.getResourceId()) ;
                fakeVO.setResourceId(new Integer(x));
                fakeVO.setTitle(new Integer(1));
                Collection<LookupLocalizationVO> lColl = Common.getLocalizationFromBundleKey("pixalere.TIP");
                
                fakeVO.setNames(lColl);
                results.add(fakeVO);//adding category to LookupVO Array
                
            }catch(DataAccessException e){throw new ApplicationException("DataAccessException Error in PatientManagerBD.findAllResourcesWithCateories(): " + e.toString(),e);}
        }
        return results;
        
    }
}
