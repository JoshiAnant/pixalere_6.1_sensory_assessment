package com.pixalere.admin.bean;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
/**
 *     This is the resources (DB table) bean
 *     Refer to {@link com.pixalere.admin.service.ResourcesServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @opt types
 *     @opt attributes
 *     @opt operations
 *
 *     @has 1..1 Has 1..* com.pixalere.common.bean.LookupVO
*/
public class ResourcesVO  extends ValueObject implements Serializable {
    /** identifier field */
    private String name;
    private Integer lock_add_n_del;
    private Integer resourceId;
    
    private String report_value_label_key;
    private String report_value_component_type;
    private String report_value_field_type;
    
    /** full constructor */
    public ResourcesVO(Integer resourceId,String name) {
        this.name = name;
	this.resourceId=resourceId;
    }
    /** default constructor */
    public ResourcesVO() {
    }
	public Integer getResourceId() {
        return this.resourceId;
    }
    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getReport_value_label_key() {
        return this.report_value_label_key;
    }
    public void setReport_value_label_key(String report_value_label_key) {
        this.report_value_label_key = report_value_label_key;
    }
    public String getReport_value_component_type() {
        return this.report_value_component_type;
    }
    public void setReport_value_component_type(String report_value_component_type) {
        this.report_value_component_type = report_value_component_type;
    }
    public String getReport_value_field_type() {
        return this.report_value_field_type;
    }
    public void setReport_value_field_type(String report_value_field_type) {
        this.report_value_field_type = report_value_field_type;
    }
    /**
     * @return the lock_add_n_del
     */
    public Integer getLock_add_n_del() {
        return lock_add_n_del;
    }
    /**
     * @param lock_add_n_del the lock_add_n_del to set
     */
    public void setLock_add_n_del(Integer lock_add_n_del) {
        this.lock_add_n_del = lock_add_n_del;
    }
}
