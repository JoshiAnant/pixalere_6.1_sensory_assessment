package com.pixalere.admin.bean;
import java.util.*;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.utils.Common;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;
/**
 *     This is the log_access (DB table) bean
 *     Refer to {@link com.pixalere.admin.service.LogAuditServiceImpl } for the calls
 *     which utilize this bean.
 *
*/
public class LogAuditVO  extends ValueObject implements Serializable {
    /** identifier field */
    private Integer professionalId;
    private Integer patientId;
    private Date created_on;
    private Integer id;
    private String action;
    private String table_name;
    private Integer record_id;
    private String description;
    private String ip_address;//@todo place holder, need to implement in 4.3
/** full constructor */
    public LogAuditVO(Integer professionalId, Integer patientId,String action) {
        
        this.created_on = new Date();
        this.professionalId = professionalId;
        this.patientId = patientId;
        this.created_on = created_on;
        this.action=action;
        this.table_name="";
        this.ip_address="";
        this.record_id=null;
        
    }
    public LogAuditVO(Integer professionalId, Integer patientId,String action,String ip_address) {
       
        this.created_on = new Date();
        this.professionalId = professionalId;
        this.patientId = patientId;
        this.created_on = created_on;
        this.action=action;
        this.table_name="";
        this.ip_address=ip_address;
        this.record_id=null;
    }
    /** full constructor */
    public LogAuditVO(Integer professionalId, Integer patientId,String action,String table_name, int record_id) {
        PDate pdate = new PDate();
        this.created_on = new Date();
        this.ip_address="";
        this.professionalId = professionalId;
        this.patientId = patientId;
        this.created_on = created_on;
        this.action=action;
        this.table_name=table_name;
        this.record_id=record_id;
        if(table_name == null){
            table_name = "";
        }
    }
    public LogAuditVO(Integer professionalId, Integer patientId,String action,String table_name, int record_id,String desc) {
        PDate pdate = new PDate();
        this.created_on = new Date();
        this.ip_address="";
        this.professionalId = professionalId;
        this.patientId = patientId;
        this.created_on = created_on;
        this.action=action;
        this.table_name=table_name;
        this.record_id=record_id;
        this.description=desc;
        if(table_name == null){
            table_name = "";
        }
    }
    /** full constructor */
    public LogAuditVO(Integer professionalId, Integer patientId,String action,String table_name, ValueObject new_object) {
        PDate pdate = new PDate();
        this.created_on = new Date();
        this.professionalId = professionalId;
        this.patientId = patientId;
        this.created_on = created_on;
        this.action=action;
        this.table_name=table_name;
        this.ip_address="";
        this.record_id=null;
        if(new_object!=null){
            this.record_id = new_object.getId();
        }
        if(table_name == null){
            table_name = "";
        }
    }
    /** full constructor */
    public LogAuditVO(Integer professionalId, Integer patientId,String action,String table_name, ArrayValueObject new_object) {
        PDate pdate = new PDate();
        this.created_on = new Date();
        this.professionalId = professionalId;
        this.patientId = patientId;
        this.created_on = created_on;
        this.action=action;
        this.table_name=table_name;
        this.ip_address="";
        this.record_id=null;
        if(new_object!=null){
            this.record_id = new_object.getId();
        }
        if(table_name == null){
            table_name = "";
        }
    }
    /** default constructor */
    public LogAuditVO() {
    }
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getProfessionalId(){
        return this.professionalId;
    }
    public void setProfessionalId(Integer professionalId){
        this.professionalId=professionalId;
    }
    public Integer getPatientId() {
        return this.patientId;
    }
    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }
    public Date getCreated_on() {
        return this.created_on;
    }
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    public String getAction(){
        return action;
    }
    public void setAction(String action){
        this.action=action;
    }
    public void setTable_name(String table_name){
        this.table_name=table_name;
    }
    public String getTable_name(){
        return table_name;
    }
    public void setRecord_id(Integer record_id){
        this.record_id=record_id;
    }
    public Integer getRecord_id(){
        return record_id;
    }
    public String getDateStringWithHour() {
        if (created_on == null) {
            return "";
        }
        try{
            return new PDate().getDateStringWithHour(created_on,"en");
        }catch(Exception e){
            return "";
        }
    }
    /**
     * @return the ip_address
     */
    public String getIp_address() {
        return ip_address;
    }
    /**
     * @param ip_address the ip_address to set
     */
    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
 
    
}
