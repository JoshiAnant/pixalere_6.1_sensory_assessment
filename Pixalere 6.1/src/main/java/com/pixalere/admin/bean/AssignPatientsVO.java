package com.pixalere.admin.bean;
import java.util.Date;
import java.io.Serializable;
import com.pixalere.common.ValueObject;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.patient.bean.PatientAccountVO;
/**
 *     This is the assigned_patients (DB table) bean
 *     Refer to {@link com.pixalere.admin.service.AssignPatientsServiceImpl } for the calls
 *     which utilize this bean.
 *
 *     @has 1..1 Has 1..1 com.pixalere.professional.bean.ProfessionalVO
 *     @has 1..1 Has 1..1 com.pixalere.patient.bean.PatientAccountVO
*/
public class AssignPatientsVO  extends ValueObject implements Serializable {
    
    private String accountType;
    private Integer professionalId;
    private Integer patientId;
    private Date created_on;
    private Integer assignedBy;
    private Integer id;
    private ProfessionalVO professional;
    private PatientAccountVO patient;
    
    /** default constructor */
    public AssignPatientsVO() {
    }
    
    public ProfessionalVO getProfessional() {
        return this.professional;
    }
    public void setProfessional( ProfessionalVO  professional) {
        this.professional = professional;
    }
    
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getAccountType() {
        return this.accountType;
    }
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    public Integer getProfessionalId(){
        return this.professionalId;
    }
    public void setProfessionalId(Integer professionalId){
        this.professionalId=professionalId;
    }
    public Integer getPatientId() {
        return this.patientId;
    }
    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }
    public Date getCreated_on() {
        return this.created_on;
    }
    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    public Integer getAssignedBy() {
       return this.assignedBy;
    }
    public void setAssignedBy(Integer assignedBy) {
       this.assignedBy = assignedBy;
    }
    public PatientAccountVO getPatient() {
        return patient;
    }
    public void setPatient(PatientAccountVO patient) {
        this.patient = patient;
    }
}
