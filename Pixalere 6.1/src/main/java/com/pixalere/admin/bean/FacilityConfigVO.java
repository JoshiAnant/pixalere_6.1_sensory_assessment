/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.admin.bean;
import com.pixalere.common.ValueObject;
/**
 * @deprecated
 * @author travis
 */
public class FacilityConfigVO extends ValueObject{
    private Integer id;
    private Integer home;
    private Integer clinic;
    private Integer update_access;
    private Integer category_id;
    public FacilityConfigVO(){}
    /**
     * @return the home
     */
    public Integer getHome() {
        return home;
    }
    /**
     * @param home the home to set
     */
    public void setHome(Integer home) {
        this.home = home;
    }
    /**
     * @return the clinic
     */
    public Integer getClinic() {
        return clinic;
    }
    /**
     * @param clinic the clinic to set
     */
    public void setClinic(Integer clinic) {
        this.clinic = clinic;
    }
    /**
     * @return the update_access
     */
    public Integer getUpdate_access() {
        return update_access;
    }
    /**
     * @param update_access the update_access to set
     */
    public void setUpdate_access(Integer update_access) {
        this.update_access = update_access;
    }
    /**
     * @return the category_id
     */
    public Integer getCategory_id() {
        return category_id;
    }
    /**
     * @param category_id the category_id to set
     */
    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
}
