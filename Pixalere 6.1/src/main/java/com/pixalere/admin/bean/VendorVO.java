/*
 * Copyright (C) Pixalere Healthcare Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.pixalere.admin.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 *
 * @author travis
 */
public class VendorVO   extends ValueObject implements Serializable {
    public VendorVO() {
    }
    private Integer vendor_id;
    private String header_colour;
    private String bg_colour;
    private String logo;
    private String name;

    /**
     * @return the vendor_id
     */
    public Integer getVendor_id() {
        return vendor_id;
    }

    /**
     * @param vendor_id the vendor_id to set
     */
    public void setVendor_id(Integer vendor_id) {
        this.vendor_id = vendor_id;
    }

    /**
     * @return the header_colour
     */
    public String getHeader_colour() {
        return header_colour;
    }

    /**
     * @param header_colour the header_colour to set
     */
    public void setHeader_colour(String header_colour) {
        this.header_colour = header_colour;
    }

    /**
     * @return the bg_colour
     */
    public String getBg_colour() {
        return bg_colour;
    }

    /**
     * @param bg_colour the bg_colour to set
     */
    public void setBg_colour(String bg_colour) {
        this.bg_colour = bg_colour;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    
}
