package com.pixalere.admin.dao;
import java.util.List;
import java.util.ArrayList;
import org.apache.ojb.broker.PersistenceBrokerFactory;
import java.sql.Connection;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryFactory;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.Criteria;
import java.util.Collection;
import java.util.Iterator;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.ValueObject;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.utils.PDate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  logs_access    table.
 *
 * @todo Need to implement this class as a singleton.
 * @version 3.2
 */
public class LogAuditDAO implements DataAccessObject {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LogAuditDAO.class);
	
	public LogAuditDAO(){
		
	}
    /**
     * Deletes logs_access records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * 
     * @since 3.0
     */
    public void delete(ValueObject delete) throws DataAccessException {
        log.info("***********************Entering LogAuditDAO.delete()***********************");
        PersistenceBroker broker = null;
        
        log.info("***********************Leaving LogAuditDAO.delete()***********************");
    }
    /**
     * Get all Table_names, to use as a filter
     * 
     * @since 6.0
     * @version 6.0.8
     */
    public List<String> getTableNames() throws DataAccessException {
        List<String> results = new ArrayList();
        PersistenceBroker broker=null;
        try {
             broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            // do something

            Connection con = broker.serviceConnectionManager().getConnection();
            String query="select distinct(table_name) as table_name from logs_access group by table_name";
            PreparedStatement state = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
            if (result != null) {
                while (result.next()) {
                   String s = result.getString("table_name");
                   if(s!=null){
                       results.add(s);
                   }
                  
                }
            }
            broker.close();
            return results;
        } catch (SQLException s) {
            s.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }finally{
             if (broker!=null) broker.close();
        }
        return  null;
    }
    /**
     * Finds a single logs_access record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the logs_access table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * 
     * @since 3.0
     */
     public ValueObject findByPK(int primaryKey) throws DataAccessException {
         log.info("***********************Entering LogAuditDAO.findByPK()***********************");
         PersistenceBroker broker = null;
         LogAuditVO logAuditVO   = new LogAuditVO();
         try{
             broker = ConnectionLocator.getInstance().findBroker();
             logAuditVO.setId(new Integer(primaryKey));
             Query query = new QueryByCriteria(logAuditVO);
             log.debug("LogAuditDAO.findByPK(): "+query.toString()+" :::: Value="+primaryKey);
             logAuditVO = (LogAuditVO)broker.getObjectByQuery(query);
        }
        catch(ConnectionLocatorException e){
             log.error("ServiceLocatorException thrown in LogAuditDAO.findByPK(): " + e.toString());
             throw new DataAccessException("ServiceLocatorException in LogAuditDAO.findByPK()",e);
        }
        finally{
             if (broker!=null) broker.close();
        }
        log.info("***********************Leaving LogAuditDAO.findByPK()***********************");
        return logAuditVO;
    }
/**
     * Finds a all logs_access records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param LogAuditVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(LogAuditVO logAuditVO,java.util.Date start_date,java.util.Date end_date,String table_name) throws DataAccessException{
        log.info("***********************Entering logAuditDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        LogAuditVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LogAuditVO();
            Criteria crit = new Criteria();
            QueryByCriteria query = null;
            if(logAuditVO.getPatientId()!=null){
                crit.addEqualTo("patient_id", logAuditVO.getPatientId());
            }
            if(logAuditVO.getProfessionalId()!=null){
                crit.addEqualTo("professional_id", logAuditVO.getProfessionalId());
            }
            if(table_name != null && table_name.equals("blank")){
                crit.addEqualTo("table_name","");
            }else if(table_name !=null && !table_name.equals("") && !table_name.equals("all")){
                crit.addEqualTo("table_name",table_name);
            }
            if(start_date == null ){
                
                start_date = PDate.subtractDays(new java.util.Date(),90);//start - 7776000;//back 90 days.
                
            }
            if(end_date == null ){
                end_date = new java.util.Date();
            }
            if (start_date!=null) {
                crit.addGreaterOrEqualThan("created_on", start_date);
            }
            if (end_date!=null) {
               crit.addLessOrEqualThan("created_on", end_date);
            }
            query = QueryFactory.newQuery(LogAuditVO.class, crit);
            query.addOrderByDescending("created_on");
            query.setStartAtIndex(1);
            query.setEndAtIndex(500);
            
            
            results = (Collection) broker.getCollectionByQuery(query);
        }
        catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in LogAuditDAO.findAllByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in LogAuditDAO.findAllByCriteria()",e);
        }
        finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving LogAuditDAO.findAllByCriteria()***********************");
        return results;
    }
/**
     * Finds an logs_access record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param logAuditVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * 
     * @since 3.0
     */
    public ValueObject findByCriteria(LogAuditVO logAuditVO) throws DataAccessException{
        log.info("***********************Entering LogAuditDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        LogAuditVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new LogAuditVO();
            Query query = new QueryByCriteria(logAuditVO);
            returnVO = (LogAuditVO) broker.getObjectByQuery(query);
        }
        catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in LogAuditDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in LogAuditDAO.findByCriteria()",e);
        }
        finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving LogAuditDAO.findByCriteria()***********************");
        return returnVO;
    }

     /**
     * update a single LogAuditVO object into the logs_access table.  The value object passed
     * in has to be of type LogAuditVO.
     * 
     * @param updateRecord fields in ValueObject represent fields in the logs_access record.
     * @see com.pixalere.common.DataAccessObject#insert(com.pixalere.common.ValueObject)
     * 
     * @since 3.0
     */
      public void update(ValueObject updateRecord) throws DataAccessException {
          log.info("***********************Entering LogAuditDAO.update()***********************");
          LogAuditVO logAuditVO = null;
          PersistenceBroker broker = null;
          try{
              broker = ConnectionLocator.getInstance().findBroker();
              logAuditVO = (LogAuditVO) updateRecord;
              broker.beginTransaction();
              broker.store(logAuditVO);
              broker.commitTransaction();
          }
          catch(ConnectionLocatorException e){
              log.error("ServiceLocatorException thrown in LogAuditDAO.update(): " + e.toString());
              throw new DataAccessException("ServiceLocator in Error in LogAuditDAO.update()",e);
          }
          catch (PersistenceBrokerException e){
              // if something went wrong: rollback
              broker.abortTransaction();broker.close();
              log.error("PersistenceBrokerException thrown in LogAuditDAO.update(): " + e.toString());
              e.printStackTrace();
              throw new DataAccessException("Error in LogAuditDAO.update()",e);
          }
          finally{
              if (broker!=null){ broker.close(); }
          }
          log.info("***********************Leaving LogAuditDAO.update()***********************");
      }
}
