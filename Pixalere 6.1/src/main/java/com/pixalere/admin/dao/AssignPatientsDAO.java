package com.pixalere.admin.dao;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import java.util.Collection;
import java.util.Vector;
import com.pixalere.utils.Constants;
import java.util.Iterator;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.ValueObject;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.patient.*;
/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  <<table>>    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class AssignPatientsDAO implements DataAccessObject {
    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ResourcesDAO.class);
    
    public AssignPatientsDAO(){
        
    }
    /**
     * Deletes <<table>> records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * 
     * @since 3.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering AssignPatientsDAO.delete()***********************");
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            AssignPatientsVO assignPatientsVO = (AssignPatientsVO) deleteRecord;
            //Begin the transaction.
            Vector<AssignPatientsVO> assigned = findAllByCriteria(assignPatientsVO);
            for(AssignPatientsVO a : assigned){
            broker.beginTransaction();
            broker.delete(a);
            broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in AssignPatientsDAO.delete(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in AssignPatientsVO.delete()",e);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in AssignPatientsDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in AssignPatientsVO.delete()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving AssignPatientsDAO.delete()***********************");
    }
    
    /**
     * Finds a single <<table>> record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the <<table>> table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * 
     * @since 3.0
     */
    
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        log.info("***********************Entering AssignPatientsDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        AssignPatientsVO assignPatientsVO   = new AssignPatientsVO();
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            assignPatientsVO.setId(new Integer(primaryKey));
            Query query = new QueryByCriteria(assignPatientsVO);
            log.debug("AssignPatientsDAO.findByPK(): "+query.toString()+" :::: Value="+primaryKey);
            assignPatientsVO = (AssignPatientsVO)broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in AssignPatientsDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in AssignPatientsDAO.findByPK()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving AssignPatientsDAO.findByPK()***********************");
        return assignPatientsVO;
    }
    
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * 
     * @since 3.0
     */
    public Vector findAllByCriteria(AssignPatientsVO assignPatientsVO) throws DataAccessException{
        log.info("***********************Entering assignPatientsDAO.findAllByCriteria()***********************");
        PersistenceBroker broker = null;
        Vector results = new Vector();
        AssignPatientsVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssignPatientsVO();
            
            QueryByCriteria query = new QueryByCriteria(assignPatientsVO);
            query.addOrderByAscending("patientId");
            Collection<AssignPatientsVO> cool = (Collection) broker.getCollectionByQuery(query);
            for(AssignPatientsVO assign : cool){
                try{
                    
                    PatientServiceImpl patientBD=new  PatientServiceImpl(Constants.ENGLISH);//defaulting to english
                    ProfessionalServiceImpl userBD=new ProfessionalServiceImpl();
                    PatientAccountVO patientVO=(PatientAccountVO) patientBD.getPatient(assign.getPatientId().intValue());
                    assign.setPatient(patientVO);
                    results.add(assign);
                }catch(Exception ex){ log.error("AssignPatientsDAO.findAllByCriteria: Getting patients/professionals"+ex.getMessage());}
                
            }
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in AssignPatientsDAO.findAllByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in AssignPatientsDAO.findAllByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving AssignPatientsDAO.findAllByCriteria()***********************");
        return results;
    }
    /**
     * Finds an <<table>> record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param assignPatientsVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * 
     * @since 3.0
     */
    public ValueObject findByCriteria(AssignPatientsVO assignPatientsVO) throws DataAccessException{
        log.info("***********************Entering AssignPatientsDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        AssignPatientsVO returnVO   = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new AssignPatientsVO();
            Query query = new QueryByCriteria(assignPatientsVO);
            returnVO = (AssignPatientsVO) broker.getObjectByQuery(query);
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in AssignPatientsDAO.findByCriteria(): " + e.toString(),e);
            throw new DataAccessException("ServiceLocatorException in AssignPatientsDAO.findByCriteria()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving AssignPatientsDAO.findByCriteria()***********************");
        return returnVO;
    }
    
    
    
    /**
     * Updates a single <<table>> record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the <<table>> table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     * @version 3.2
     * @since 3.0
     */
    
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering AssignPatientsDAO.update()***********************");
        AssignPatientsVO assignPatientsVO = null;
        PersistenceBroker broker = null;
        try{
            broker = ConnectionLocator.getInstance().findBroker();
            assignPatientsVO = (AssignPatientsVO) updateRecord;
            broker.beginTransaction();
            broker.store(assignPatientsVO);
            broker.commitTransaction();
        } catch(ConnectionLocatorException e){
            log.error("ServiceLocatorException thrown in AssignPatientsDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in AssignPatientsDAO.update()",e);
        } catch (PersistenceBrokerException e){
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in AssignPatientsDAO.update(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in AssignPatientsDAO.update()",e);
        } finally{
            if (broker!=null) broker.close();
        }
        log.info("***********************Leaving AssignPatientsDAO.update()***********************");
    }
}
