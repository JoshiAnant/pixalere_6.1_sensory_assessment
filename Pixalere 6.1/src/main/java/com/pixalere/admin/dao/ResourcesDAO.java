package com.pixalere.admin.dao;

import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import com.pixalere.utils.Constants;
import java.util.Collection;

import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.ValueObject;
import com.pixalere.admin.bean.ResourcesVO;

/**
 *  This   class is responsible for handling all CRUD logic associated with the
 *  resources    table.
 *
 * @todo Need to implement this class as a singleton.
 *
 */
public class ResourcesDAO implements DataAccessObject {

    // Create Log4j category instance for logging
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ResourcesDAO.class);
    

    public ResourcesDAO() {
        
    }

    /**
     * Deletes resources records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.
     *
     * @param deleteRecord  the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        log.info("***********************Entering ResourcesDAO.delete()***********************");
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ResourcesVO resourcesVO = (ResourcesVO) deleteRecord;

            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(resourcesVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ResourcesDAO.delete(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in ResourcesVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ResourcesDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ResourcesVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("***********************Leaving ResourcesDAO.delete()***********************");
    }

    /**
     * Finds a single resources record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the resources table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        log.info("***********************Entering ResourcesDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        ResourcesVO resourcesVO = new ResourcesVO();

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            resourcesVO.setResourceId(new Integer(primaryKey));

            Query query = new QueryByCriteria(resourcesVO);
            log.debug("ResourcesDAO.findByPK(): " + query.toString() + " :::: Value=" + primaryKey);

            resourcesVO = (ResourcesVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ResourcesDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in ResourcesDAO.findByPK()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ResourcesDAO.findByPK()***********************");
        return resourcesVO;
    }

    /**
     * Finds a all resources records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param resourcesVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @verson 3.2
     * @since 3.0
     */
    public Collection findAllByCriteria(ResourcesVO resourcesVO) throws DataAccessException {
        log.info("***********************Entering resourcesDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        ResourcesVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ResourcesVO();

            Query query = new QueryByCriteria(resourcesVO);
            results = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ResourcesDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ResourcesDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ResourcesDAO.findByCriteria()***********************");
        return results;
    }

    /**
     * Finds an resources record, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param resourcesVO  fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     *
     * @since 3.0
     */
    public ValueObject findByCriteria(ResourcesVO resourcesVO) throws DataAccessException {
        log.info("***********************Entering resourcesDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        ResourcesVO returnVO = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ResourcesVO();

            Query query = new QueryByCriteria(resourcesVO);
            returnVO = (ResourcesVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ResourcesDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ResourcesDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Leaving ResourcesDAO.findByCriteria()***********************");
        return returnVO;
    }

    /**
     * Updates a single resources record in the Pixalere database.  If id is Null in
     * ValueObject the update method can also be insert records.
     *
     * @param updateRecord the object which represents a record in the resources table to be inserted
     * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
     *
     * @since 3.0
     */
    public void update(ValueObject updateRecord) throws DataAccessException {
        log.info("***********************Entering ResourcesDAO.update()***********************");
        ResourcesVO resourcesVO = null;
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            resourcesVO = (ResourcesVO) updateRecord;

            broker.beginTransaction();
            broker.store(resourcesVO);
            broker.commitTransaction();
            
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ResourcesDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ResourcesDAO.update()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ResourcesDAO.update(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in ResourcesDAO.update()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        log.info("***********************Entering ResourcesDAO.update()***********************");
    }
}
