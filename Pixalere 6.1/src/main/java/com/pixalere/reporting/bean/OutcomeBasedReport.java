/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
/**
 *
 * @author travis
 */
public class OutcomeBasedReport {
    public OutcomeBasedReport(){
        
    }
   
    private String patient_name="";
    private String phn="";
    private String type_of_report="";
    private String week="";
    private String change_status="";
    private String discharge="";
    private String initial_visit="";
    private String delivery="";
    private String suitable_transfer="";
    private String transfer_date="";
    private String discharge_reason="";
    private String discharge_date="";
    private String last_visit="";
    private String discharge_disposition="";
    private String care_pathway="";
    private String num_wounds="";
    private String pathway_locations="";
    private String pathway_confirmed="";
    private String confirmed_no="";
    private String healability="";
    private String target_discharge_date="";
    private String dimensions="";
    private String dimension_progress="";
    private String chronic_disease="";
    private String undermining="";
    private String holistic_client="";
    private String wound_therapy_initiated="";
    private String wound_therapy_reassessed="";
    private String discharge_planning="";
    private String arterial_lower_limb="";
    private String diabetic_lower_limb="";
    private String arterial_abpi_date="";
    private String diabetic_abpi_date="";
    private String referral_vascular="";
    private String pressure_redistribi="";
    private String longterm_pressure="";
    private String redistribution_system="";
    private String venous_lower_limb="";
    private String venous_abpi_date="";
    private String compression_therapy="";
    private String longterm_compression="";
    private String client_independent="";
    private String pressure_ulcer="";
    private String redistribution_measures="";
    private String wound_related_symptoms="";
    private String quality_of_life="";
    private String qol_no="";
    private String change_in_wound="";
    private String barriers_addressed="";
    private String barriers_addressed_comments="";
    private String root_cause_addressed_no="";
    private String root_cause_addressed="";
    private String clinical_therapy="";
    private String iv_therapy="";
    private String system_barriers="";
    private String non_adherent_care_plan="";
    private String clinical_risk_factors="";
    private String psychosocial_risk_factors="";
    private String referrals="";
    private String community_referrals="";
    private String dcf="";
    private String comments="";
    private String date="";
    private String nurse_name="";
    private String agency="";
    private String wound_change_reason="";
    /**
     * @return the patient_name
     */
    public String getPatient_name() {
        return patient_name;
    }
    /**
     * @param patient_name the patient_name to set
     */
    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }
    /**
     * @return the phn
     */
    public String getPhn() {
        return phn;
    }
    /**
     * @param phn the phn to set
     */
    public void setPhn(String phn) {
        this.phn = phn;
    }
    /**
     * @return the type_of_report
     */
    public String getType_of_report() {
        return type_of_report;
    }
    /**
     * @param type_of_report the type_of_report to set
     */
    public void setType_of_report(String type_of_report) {
        this.type_of_report = type_of_report;
    }
    /**
     * @return the week
     */
    public String getWeek() {
        return week;
    }
    /**
     * @param week the week to set
     */
    public void setWeek(String week) {
        this.week = week;
    }
    /**
     * @return the change_status
     */
    public String getChange_status() {
        return change_status;
    }
    /**
     * @param change_status the change_status to set
     */
    public void setChange_status(String change_status) {
        this.change_status = change_status;
    }
    /**
     * @return the discharge
     */
    public String getDischarge() {
        return discharge;
    }
    /**
     * @param discharge the discharge to set
     */
    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }
    /**
     * @return the initial_visit
     */
    public String getInitial_visit() {
        return initial_visit;
    }
    /**
     * @param initial_visit the initial_visit to set
     */
    public void setInitial_visit(String initial_visit) {
        this.initial_visit = initial_visit;
    }
    /**
     * @return the delivery
     */
    public String getDelivery() {
        return delivery;
    }
    /**
     * @param delivery the delivery to set
     */
    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }
    /**
     * @return the suitable_transfer
     */
    public String getSuitable_transfer() {
        return suitable_transfer;
    }
    /**
     * @param suitable_transfer the suitable_transfer to set
     */
    public void setSuitable_transfer(String suitable_transfer) {
        this.suitable_transfer = suitable_transfer;
    }
  
    /**
     * @return the discharge_date
     */
    public String getDischarge_date() {
        return discharge_date;
    }
    /**
     * @param discharge_date the discharge_date to set
     */
    public void setDischarge_date(String discharge_date) {
        this.discharge_date = discharge_date;
    }
    /**
     * @return the last_visit
     */
    public String getLast_visit() {
        return last_visit;
    }
    /**
     * @param last_visit the last_visit to set
     */
    public void setLast_visit(String last_visit) {
        this.last_visit = last_visit;
    }
    /**
     * @return the discharge_disposition
     */
    public String getDischarge_disposition() {
        return discharge_disposition;
    }
    /**
     * @param discharge_disposition the discharge_disposition to set
     */
    public void setDischarge_disposition(String discharge_disposition) {
        this.discharge_disposition = discharge_disposition;
    }
    /**
     * @return the healability
     */
    public String getHealability() {
        return healability;
    }
    /**
     * @param healability the healability to set
     */
    public void setHealability(String healability) {
        this.healability = healability;
    }
    /**
     * @return the target_discharge_date
     */
    public String getTarget_discharge_date() {
        return target_discharge_date;
    }
    /**
     * @param target_discharge_date the target_discharge_date to set
     */
    public void setTarget_discharge_date(String target_discharge_date) {
        this.target_discharge_date = target_discharge_date;
    }
    /**
     * @return the dimensions
     */
    public String getDimensions() {
        return dimensions;
    }
    /**
     * @param dimensions the dimensions to set
     */
    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }
    /**
     * @return the dimension_progress
     */
    public String getDimension_progress() {
        return dimension_progress;
    }
    /**
     * @param dimension_progress the dimension_progress to set
     */
    public void setDimension_progress(String dimension_progress) {
        this.dimension_progress = dimension_progress;
    }
    /**
     * @return the undermining
     */
    public String getUndermining() {
        return undermining;
    }
    /**
     * @param undermining the undermining to set
     */
    public void setUndermining(String undermining) {
        this.undermining = undermining;
    }
    /**
     * @return the holistic_client
     */
    public String getHolistic_client() {
        return holistic_client;
    }
    /**
     * @param holistic_client the holistic_client to set
     */
    public void setHolistic_client(String holistic_client) {
        this.holistic_client = holistic_client;
    }
  
    /**
     * @return the discharge_planning
     */
    public String getDischarge_planning() {
        return discharge_planning;
    }
    /**
     * @param discharge_planning the discharge_planning to set
     */
    public void setDischarge_planning(String discharge_planning) {
        this.discharge_planning = discharge_planning;
    }
    /**
     * @return the arterial_lower_limb
     */
    public String getArterial_lower_limb() {
        return arterial_lower_limb;
    }
    /**
     * @param arterial_lower_limb the arterial_lower_limb to set
     */
    public void setArterial_lower_limb(String arterial_lower_limb) {
        this.arterial_lower_limb = arterial_lower_limb;
    }
    /**
     * @return the diabetic_lower_limb
     */
    public String getDiabetic_lower_limb() {
        return diabetic_lower_limb;
    }
    /**
     * @param diabetic_lower_limb the diabetic_lower_limb to set
     */
    public void setDiabetic_lower_limb(String diabetic_lower_limb) {
        this.diabetic_lower_limb = diabetic_lower_limb;
    }
    /**
     * @return the arterial_abpi_date
     */
    public String getArterial_abpi_date() {
        return arterial_abpi_date;
    }
    /**
     * @param arterial_abpi_date the arterial_abpi_date to set
     */
    public void setArterial_abpi_date(String arterial_abpi_date) {
        this.arterial_abpi_date = arterial_abpi_date;
    }
    /**
     * @return the diabetic_abpi_date
     */
    public String getDiabetic_abpi_date() {
        return diabetic_abpi_date;
    }
    /**
     * @param diabetic_abpi_date the diabetic_abpi_date to set
     */
    public void setDiabetic_abpi_date(String diabetic_abpi_date) {
        this.diabetic_abpi_date = diabetic_abpi_date;
    }
    /**
     * @return the referral_vascular
     */
    public String getReferral_vascular() {
        return referral_vascular;
    }
    /**
     * @param referral_vascular the referral_vascular to set
     */
    public void setReferral_vascular(String referral_vascular) {
        this.referral_vascular = referral_vascular;
    }
    /**
     * @return the pressure_redistrib
     */
    public String getPressure_redistribi() {
        return pressure_redistribi;
    }
    /**
     * @param pressure_redistrib the pressure_redistrib to set
     */
    public void setPressure_redistribi(String pressure_redistrib) {
        this.pressure_redistribi = pressure_redistrib;
    }
    /**
     * @return the longterm_pressure
     */
    public String getLongterm_pressure() {
        return longterm_pressure;
    }
    /**
     * @param longterm_pressure the longterm_pressure to set
     */
    public void setLongterm_pressure(String longterm_pressure) {
        this.longterm_pressure = longterm_pressure;
    }
    /**
     * @return the redistribution_system
     */
    public String getRedistribution_system() {
        return redistribution_system;
    }
    /**
     * @param redistribution_system the redistribution_system to set
     */
    public void setRedistribution_system(String redistribution_system) {
        this.redistribution_system = redistribution_system;
    }
    /**
     * @return the venous_lower_limb
     */
    public String getVenous_lower_limb() {
        return venous_lower_limb;
    }
    /**
     * @param venous_lower_limb the venous_lower_limb to set
     */
    public void setVenous_lower_limb(String venous_lower_limb) {
        this.venous_lower_limb = venous_lower_limb;
    }
    /**
     * @return the venous_abpi_date
     */
    public String getVenous_abpi_date() {
        return venous_abpi_date;
    }
    /**
     * @param venous_abpi_date the venous_abpi_date to set
     */
    public void setVenous_abpi_date(String venous_abpi_date) {
        this.venous_abpi_date = venous_abpi_date;
    }
    /**
     * @return the compression_therapy
     */
    public String getCompression_therapy() {
        return compression_therapy;
    }
    /**
     * @param compression_therapy the compression_therapy to set
     */
    public void setCompression_therapy(String compression_therapy) {
        this.compression_therapy = compression_therapy;
    }
    /**
     * @return the longterm_compression
     */
    public String getLongterm_compression() {
        return longterm_compression;
    }
    /**
     * @param longterm_compression the longterm_compression to set
     */
    public void setLongterm_compression(String longterm_compression) {
        this.longterm_compression = longterm_compression;
    }
    /**
     * @return the client_independent
     */
    public String getClient_independent() {
        return client_independent;
    }
    /**
     * @param client_independent the client_independent to set
     */
    public void setClient_independent(String client_independent) {
        this.client_independent = client_independent;
    }
    /**
     * @return the pressure_ulcer
     */
    public String getPressure_ulcer() {
        return pressure_ulcer;
    }
    /**
     * @param pressure_ulcer the pressure_ulcer to set
     */
    public void setPressure_ulcer(String pressure_ulcer) {
        this.pressure_ulcer = pressure_ulcer;
    }
    /**
     * @return the redistribution_measures
     */
    public String getRedistribution_measures() {
        return redistribution_measures;
    }
    /**
     * @param redistribution_measures the redistribution_measures to set
     */
    public void setRedistribution_measures(String redistribution_measures) {
        this.redistribution_measures = redistribution_measures;
    }
    /**
     * @return the wound_related_symptoms
     */
    public String getWound_related_symptoms() {
        return wound_related_symptoms;
    }
    /**
     * @param wound_related_symptoms the wound_related_symptoms to set
     */
    public void setWound_related_symptoms(String wound_related_symptoms) {
        this.wound_related_symptoms = wound_related_symptoms;
    }
    /**
     * @return the quality_of_life
     */
    public String getQuality_of_life() {
        return quality_of_life;
    }
    /**
     * @param quality_of_life the quality_of_life to set
     */
    public void setQuality_of_life(String quality_of_life) {
        this.quality_of_life = quality_of_life;
    }
    /**
     * @return the qol_no
     */
    public String getQol_no() {
        return qol_no;
    }
    /**
     * @param qol_no the qol_no to set
     */
    public void setQol_no(String qol_no) {
        this.qol_no = qol_no;
    }
    /**
     * @return the change_in_wound
     */
    public String getChange_in_wound() {
        return change_in_wound;
    }
    /**
     * @param change_in_wound the change_in_wound to set
     */
    public void setChange_in_wound(String change_in_wound) {
        this.change_in_wound = change_in_wound;
    }
    /**
     * @return the system_barriers
     */
    public String getSystem_barriers() {
        return system_barriers;
    }
    /**
     * @param system_barriers the system_barriers to set
     */
    public void setSystem_barriers(String system_barriers) {
        this.system_barriers = system_barriers;
    }
   


    /**
     * @return the clinical_therapy
     */
    public String getClinical_therapy() {
        return clinical_therapy;
    }
    /**
     * @param clinical_therapy the clinical_therapy to set
     */
    public void setClinical_therapy(String clinical_therapy) {
        this.clinical_therapy = clinical_therapy;
    }
    /**
     * @return the iv_therapy
     */
    public String getIv_therapy() {
        return iv_therapy;
    }
    /**
     * @param iv_therapy the iv_therapy to set
     */
    public void setIv_therapy(String iv_therapy) {
        this.iv_therapy = iv_therapy;
    }

    /**
     * @return the non_adherent_care_plan
     */
    public String getNon_adherent_care_plan() {
        return non_adherent_care_plan;
    }
    /**
     * @param non_adherent_care_plan the non_adherent_care_plan to set
     */
    public void setNon_adherent_care_plan(String non_adherent_care_plan) {
        this.non_adherent_care_plan = non_adherent_care_plan;
    }
    /**
     * @return the clinical_risk_factors
     */
    public String getClinical_risk_factors() {
        return clinical_risk_factors;
    }
    /**
     * @param clinical_risk_factors the clinical_risk_factors to set
     */
    public void setClinical_risk_factors(String clinical_risk_factors) {
        this.clinical_risk_factors = clinical_risk_factors;
    }
    /**
     * @return the psychosocial_risk_factors
     */
    public String getPsychosocial_risk_factors() {
        return psychosocial_risk_factors;
    }
    /**
     * @param psychosocial_risk_factors the psychosocial_risk_factors to set
     */
    public void setPsychosocial_risk_factors(String psychosocial_risk_factors) {
        this.psychosocial_risk_factors = psychosocial_risk_factors;
    }
    /**
     * @return the referrals
     */
    public String getReferrals() {
        return referrals;
    }
    /**
     * @param referrals the referrals to set
     */
    public void setReferrals(String referrals) {
        this.referrals = referrals;
    }
    /**
     * @return the community_referrals
     */
    public String getCommunity_referrals() {
        return community_referrals;
    }
    /**
     * @param community_referrals the community_referrals to set
     */
    public void setCommunity_referrals(String community_referrals) {
        this.community_referrals = community_referrals;
    }
    /**
     * @return the dcf
     */
    public String getDcf() {
        return dcf;
    }
    /**
     * @param dcf the dcf to set
     */
    public void setDcf(String dcf) {
        this.dcf = dcf;
    }
    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }
    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }
    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }
    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }
    /**
     * @return the nurse_name
     */
    public String getNurse_name() {
        return nurse_name;
    }
    /**
     * @param nurse_name the nurse_name to set
     */
    public void setNurse_name(String nurse_name) {
        this.nurse_name = nurse_name;
    }
    /**
     * @return the agency
     */
    public String getAgency() {
        return agency;
    }
    /**
     * @param agency the agency to set
     */
    public void setAgency(String agency) {
        this.agency = agency;
    }
    /**
     * @return the transfer_date
     */
    public String getTransfer_date() {
        return transfer_date;
    }
    /**
     * @param transfer_date the transfer_date to set
     */
    public void setTransfer_date(String transfer_date) {
        this.transfer_date = transfer_date;
    }
    /**
     * @return the discharge_reason
     */
    public String getDischarge_reason() {
        return discharge_reason;
    }
    /**
     * @param discharge_reason the discharge_reason to set
     */
    public void setDischarge_reason(String discharge_reason) {
        this.discharge_reason = discharge_reason;
    }
    /**
     * @return the care_pathway
     */
    public String getCare_pathway() {
        return care_pathway;
    }
    /**
     * @param care_pathway the care_pathway to set
     */
    public void setCare_pathway(String care_pathway) {
        this.care_pathway = care_pathway;
    }
    /**
     * @return the num_wounds
     */
    public String getNum_wounds() {
        return num_wounds;
    }
    /**
     * @param num_wounds the num_wounds to set
     */
    public void setNum_wounds(String num_wounds) {
        this.num_wounds = num_wounds;
    }
    /**
     * @return the pathway_locations
     */
    public String getPathway_locations() {
        return pathway_locations;
    }
    /**
     * @param pathway_locations the pathway_locations to set
     */
    public void setPathway_locations(String pathway_locations) {
        this.pathway_locations = pathway_locations;
    }
    /**
     * @return the pathway_confirmed
     */
    public String getPathway_confirmed() {
        return pathway_confirmed;
    }
    /**
     * @param pathway_confirmed the pathway_confirmed to set
     */
    public void setPathway_confirmed(String pathway_confirmed) {
        this.pathway_confirmed = pathway_confirmed;
    }
    /**
     * @return the confirmed_no
     */
    public String getConfirmed_no() {
        return confirmed_no;
    }
    /**
     * @param confirmed_no the confirmed_no to set
     */
    public void setConfirmed_no(String confirmed_no) {
        this.confirmed_no = confirmed_no;
    }
    /**
     * @return the wound_therapy_initiated
     */
    public String getWound_therapy_initiated() {
        return wound_therapy_initiated;
    }
    /**
     * @param wound_therapy_initiated the wound_therapy_initiated to set
     */
    public void setWound_therapy_initiated(String wound_therapy_initiated) {
        this.wound_therapy_initiated = wound_therapy_initiated;
    }
    /**
     * @return the wound_therapy_reassessed
     */
    public String getWound_therapy_reassessed() {
        return wound_therapy_reassessed;
    }
    /**
     * @param wound_therapy_reassessed the wound_therapy_reassessed to set
     */
    public void setWound_therapy_reassessed(String wound_therapy_reassessed) {
        this.wound_therapy_reassessed = wound_therapy_reassessed;
    }
    /**
     * @return the chronic_disease
     */
    public String getChronic_disease() {
        return chronic_disease;
    }
    /**
     * @param chronic_disease the chronic_disease to set
     */
    public void setChronic_disease(String chronic_disease) {
        this.chronic_disease = chronic_disease;
    }
    /**
     * @return the root_cause_addressed_no
     */
    public String getRoot_cause_addressed_no() {
        return root_cause_addressed_no;
    }
    /**
     * @param root_cause_addressed_no the root_cause_addressed_no to set
     */
    public void setRoot_cause_addressed_no(String root_cause_addressed_no) {
        this.root_cause_addressed_no = root_cause_addressed_no;
    }
    /**
     * @return the root_cause_addressed
     */
    public String getRoot_cause_addressed() {
        return root_cause_addressed;
    }
    /**
     * @param root_cause_addressed the root_cause_addressed to set
     */
    public void setRoot_cause_addressed(String root_cause_addressed) {
        this.root_cause_addressed = root_cause_addressed;
    }
    /**
     * @return the barriers_addressed
     */
    public String getBarriers_addressed() {
        return barriers_addressed;
    }
    /**
     * @param barriers_addressed the barriers_addressed to set
     */
    public void setBarriers_addressed(String barriers_addressed) {
        this.barriers_addressed = barriers_addressed;
    }
    /**
     * @return the barriers_addressed_comments
     */
    public String getBarriers_addressed_comments() {
        return barriers_addressed_comments;
    }
    /**
     * @param barriers_addressed_comments the barriers_addressed_comments to set
     */
    public void setBarriers_addressed_comments(String barriers_addressed_comments) {
        this.barriers_addressed_comments = barriers_addressed_comments;
    }
    /**
     * @return the wound_change_reason
     */
    public String getWound_change_reason() {
        return wound_change_reason;
    }
    /**
     * @param wound_change_reason the wound_change_reason to set
     */
    public void setWound_change_reason(String wound_change_reason) {
        this.wound_change_reason = wound_change_reason;
    }
    
    
}