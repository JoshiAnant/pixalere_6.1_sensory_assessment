/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 *
 * @author travis
 */
public class ReportQueueEtiologyVO   extends ValueObject implements Serializable {
    private Integer id;
    private Integer report_id;
    private Integer lookup_id;
    public ReportQueueEtiologyVO(){}
    
    public ReportQueueEtiologyVO(Integer report_id){this.report_id=report_id;}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the report_id
     */
    public Integer getReport_id() {
        return report_id;
    }
    /**
     * @param report_id the report_id to set
     */
    public void setReport_id(Integer report_id) {
        this.report_id = report_id;
    }
    /**
     * @return the lookup_id
     */
    public Integer getLookup_id() {
        return lookup_id;
    }
    /**
     * @param lookup_id the lookup_id to set
     */
    public void setLookup_id(Integer lookup_id) {
        this.lookup_id = lookup_id;
    }
}
