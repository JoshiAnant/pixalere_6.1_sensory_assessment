/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 *
 * @author travis
 */
public class ReportQueueWoundTypeVO   extends ValueObject implements Serializable {
    private Integer id;
    private Integer report_id;
    private String wound_type;
    public ReportQueueWoundTypeVO(){}
    public ReportQueueWoundTypeVO(Integer report_id){this.report_id=report_id;}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the report_id
     */
    public Integer getReport_id() {
        return report_id;
    }
    /**
     * @param report_id the report_id to set
     */
    public void setReport_id(Integer report_id) {
        this.report_id = report_id;
    }

    /**
     * @return the wound_type
     */
    public String getWound_type() {
        return wound_type;
    }
    /**
     * @param wound_type the wound_type to set
     */
    public void setWound_type(String wound_type) {
        this.wound_type = wound_type;
    }
}
