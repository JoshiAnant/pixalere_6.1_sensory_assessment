/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
/**
 * A persistance bean for teh table dashboard_by_etiology.  Refer to @link{com.pixalere.reporting.service.ReportingServiceImpl}
 * for methods which utilize this bean.
 *
 * @since 5.0
 * @author travis
 */
public class DashboardByEtiologyVO extends ValueObject{
  private Integer id;
  private Integer dashboard_report_id;
  private Integer etiology_id;
  public DashboardByEtiologyVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the dashboard_report_id
     */
    public Integer getDashboard_report_id() {
        return dashboard_report_id;
    }
    /**
     * @param dashboard_report_id the dashboard_report_id to set
     */
    public void setDashboard_report_id(Integer dashboard_report_id) {
        this.dashboard_report_id = dashboard_report_id;
    }
    /**
     * @return the etiology_id
     */
    public Integer getEtiology_id() {
        return etiology_id;
    }
    /**
     * @param etiology_id the etiology_id to set
     */
    public void setEtiology_id(Integer etiology_id) {
        this.etiology_id = etiology_id;
    }
}
