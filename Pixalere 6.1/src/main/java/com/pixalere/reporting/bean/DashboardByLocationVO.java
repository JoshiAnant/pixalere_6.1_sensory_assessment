/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
import com.pixalere.common.bean.LookupVO;
/**
 * A persistance bean for the table dashboard_by_location.  Refer to
 * @link{com.pixalere.reporting.service.ReportingServiceImpl} for methods which
 * utilize this bean.
 *
 * @since 5.0
 * @author travis
 */
public class DashboardByLocationVO extends ValueObject{
 
    private Integer id;
    private Integer dashboard_report_id;
    private Integer treatment_location_id;
    private LookupVO lookup;
    public DashboardByLocationVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the dashboard_report_id
     */
    public Integer getDashboard_report_id() {
        return dashboard_report_id;
    }
    /**
     * @param dashboard_report_id the dashboard_report_id to set
     */
    public void setDashboard_report_id(Integer dashboard_report_id) {
        this.dashboard_report_id = dashboard_report_id;
    }
    /**
     * @return the treatment_location_id
     */
    public Integer getTreatment_location_id() {
        return treatment_location_id;
    }
    /**
     * @param treatment_location_id the treatment_location_id to set
     */
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }
    /**
     * @return the lookup
     */
    public LookupVO getLookup() {
        return lookup;
    }
    /**
     * @param lookup the lookup to set
     */
    public void setLookup(LookupVO lookup) {
        this.lookup = lookup;
    }
}
