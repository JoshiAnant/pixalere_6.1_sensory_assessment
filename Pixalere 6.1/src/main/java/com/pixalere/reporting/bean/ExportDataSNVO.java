/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.bean;

import com.pixalere.common.ValueObject;
import java.util.Comparator;

/**
 * @since 6.1
 * @author Jose
 */
public class ExportDataSNVO extends ValueObject implements Comparator {
    private String patient_id;
    private String discharge_reason;
    private String funding_source;
    private String patient_residence;
    private String co_morbidities_endocrine;
    private String co_morbidities_heart;
    private String co_morbidities_musculoeskeletal;
    private String co_morbidities_neurological;
    private String co_morbidities_psychiatrric_mood;
    private String co_morbidities_pulmonary;
    private String co_morbidities_sensory;
    private String co_morbidities_infections;
    private String co_morbidities_other;
    private String interfering_factors;
    private String interfering_medications;
    private String gender;
    private String albumin;
    private String pre_albumin;
    private String labs_investigations;
    private String external_referrals;
    private String arranged;
    private String basic_limb_temperature;
    private String limb_edema;
    private String limb_edema_severity;
    private String palpation;
    private String missing_limbs;
    private String color;
    private String basic_pain_assessments;
    private String adv_pain_assessments;
    private String basic_skin_assessments;
    private String sensory;
    private String proprioception;
    private String deformities;
    private String toes;
    private String weight_bearing_status;
    private String balance;
    private String calf_muscle_function;
    private String mobility_aids;
    private String muscle_tone;
    private String arches;
    private String foot_active;
    private String foot_passive;
    private String lab_done;
    private String trancutaneous_pressures;
    private String missing_upper_limb;
    private String upper_limb_edema_location;
    private String range_of_motion;
    private String adv_lim_temperature;
    private String woundprofile_id;
    private String patient_limitations;
    private String teaching_goals;
    private String pressure_ulcer_stage;
    private String alpha_id;
    private String wound_etiology;
    private String ostomy_etiology;
    private String type_of_ostomy;
    private String postop_etiology;
    private String tubes_drains_etiology;
    private String skin_etiology;
    private String goal_of_healing_wound;
    private String goal_of_healing_incision;
    private String goal_of_healing_ostomy;
    private String goal_of_healing_drain;
    private String goal_of_healing_skin;
    private String acquired;
    private String woundassessmentid;
    private String c_s_result;
    private String c_s_route;
    private String adjunctive_treatment_modalities;
    private String pressure_reading;
    private String initiated_by;
    private String negative_pressure_connector;
    private String reason_for_ending;
    private String goal_of_therapy;
    private String dressing_change_frequency;
    private String bnursing_vistit_frecuency;
    private String reading_variable;
    private String clinical_barriers;
    private String iv_therapy_barriers;
    private String vendor_name;
    private String machine_acquirement;
    private String assessmentwoundid;
    private String exudate_type;
    private String exudate_amount;
    private String wound_discharge_reason;
    private String wound_edge;
    private String wound_base;
    private String wound_s_s_infection;
    private String periwound_skin;
    private String time_blood_sugar;
    private String priority;
    private String incisionassessmentid;
    private String postop_management;
    private String incision_exudate;
    private String incision_s_s_infection;
    private String incision_status;
    private String incision_discharge_reason;
    private String pain;
    private String incision_closure_type;
    private String incision_exudate_amount;
    private String drainassessmentid;
    private String drainage_odour;
    private String type_of_drain;
    private String peri_drain_skin;
    private String characteristics;
    private String exudate;
    private String tubes_exudate_amount;
    private String drainage_amount_desc;
    private String tubes_discharge_reason;
    private String drain_site;
    private String ostomyassessmentid;
    private String construction;
    private String drainage;
    private String mucocutaneous_margin;
    private String peri_fistula_skin;
    private String devices;
    private String ostomy_discharge_reason;
    private String concerns_for_pouching;
    private String profile;
    private String flange_pouch;
    private String urine_colour;
    private String urine_type;
    private String urine_quantity;
    private String skin_appearance;
    private String skin_contour;
    private String self_care_progress;
    private String peri_ostomy_skin;
    private String stoma_shape;
    private String nutritional_status;
    private String status;

    public ExportDataSNVO() {
        // Global Initialization
        patient_id = "0";
        discharge_reason = "0";
        funding_source = "0";
        patient_residence = "0";
        co_morbidities_endocrine = "0";
        co_morbidities_heart = "0";
        co_morbidities_musculoeskeletal = "0";
        co_morbidities_neurological = "0";
        co_morbidities_psychiatrric_mood = "0";
        co_morbidities_pulmonary = "0";
        co_morbidities_sensory = "0";
        co_morbidities_infections = "0";
        co_morbidities_other = "0";
        interfering_factors = "0";
        interfering_medications = "0";
        gender = "0";
        albumin = "0";
        pre_albumin = "0";
        labs_investigations = "0";
        external_referrals = "0";
        arranged = "0";
        basic_limb_temperature = "0";
        limb_edema = "0";
        limb_edema_severity = "0";
        palpation = "0";
        missing_limbs = "0";
        color = "0";
        basic_pain_assessments = "0";
        adv_pain_assessments = "0";
        basic_skin_assessments = "0";
        sensory = "0";
        proprioception = "0";
        deformities = "0";
        toes = "0";
        weight_bearing_status = "0";
        balance = "0";
        calf_muscle_function = "0";
        mobility_aids = "0";
        muscle_tone = "0";
        arches = "0";
        foot_active = "0";
        foot_passive = "0";
        lab_done = "0";
        trancutaneous_pressures = "0";
        missing_upper_limb = "0";
        upper_limb_edema_location = "0";
        range_of_motion = "0";
        adv_lim_temperature = "0";
        woundprofile_id = "0";
        patient_limitations = "0";
        teaching_goals = "0";
        pressure_ulcer_stage = "0";
        alpha_id = "0";
        wound_etiology = "0";
        ostomy_etiology = "0";
        type_of_ostomy = "0";
        postop_etiology = "0";
        tubes_drains_etiology = "0";
        skin_etiology = "0";
        goal_of_healing_wound = "0";
        goal_of_healing_incision = "0";
        goal_of_healing_ostomy = "0";
        goal_of_healing_drain = "0";
        goal_of_healing_skin = "0";
        acquired = "0";
        woundassessmentid = "0";
        c_s_result = "0";
        c_s_route = "0";
        adjunctive_treatment_modalities = "0";
        pressure_reading = "0";
        initiated_by = "0";
        negative_pressure_connector = "0";
        reason_for_ending = "0";
        goal_of_therapy = "0";
        dressing_change_frequency = "0";
        bnursing_vistit_frecuency = "0";
        reading_variable = "0";
        clinical_barriers = "0";
        iv_therapy_barriers = "0";
        vendor_name = "0";
        machine_acquirement = "0";
        assessmentwoundid = "0";
        exudate_type = "0";
        exudate_amount = "0";
        wound_discharge_reason = "0";
        wound_edge = "0";
        wound_base = "0";
        wound_s_s_infection = "0";
        periwound_skin = "0";
        time_blood_sugar = "0";
        priority = "0";
        incisionassessmentid = "0";
        postop_management = "0";
        incision_exudate = "0";
        incision_s_s_infection = "0";
        incision_status = "0";
        incision_discharge_reason = "0";
        pain = "0";
        incision_closure_type = "0";
        incision_exudate_amount = "0";
        drainassessmentid = "0";
        drainage_odour = "0";
        type_of_drain = "0";
        peri_drain_skin = "0";
        characteristics = "0";
        exudate = "0";
        tubes_exudate_amount = "0";
        drainage_amount_desc = "0";
        tubes_discharge_reason = "0";
        drain_site = "0";
        ostomyassessmentid = "0";
        construction = "0";
        drainage = "0";
        mucocutaneous_margin = "0";
        peri_fistula_skin = "0";
        devices = "0";
        ostomy_discharge_reason = "0";
        concerns_for_pouching = "0";
        profile = "0";
        flange_pouch = "0";
        urine_colour = "0";
        urine_type = "0";
        urine_quantity = "0";
        skin_appearance = "0";
        skin_contour = "0";
        self_care_progress = "0";
        peri_ostomy_skin = "0";
        stoma_shape = "0";
        nutritional_status = "0";
        status = "0";
    }
    
    public void copyAssessmentsDetailsData(ExportDataSNVO vo){
        // wound details
        this.assessmentwoundid = vo.assessmentwoundid;
        this.exudate_amount = vo.exudate_amount;
        this.wound_discharge_reason = vo.wound_discharge_reason;
        this.priority = vo.priority;
        this.exudate_type = vo.exudate_type;
        this.wound_edge = vo.wound_edge;
        this.periwound_skin = vo.periwound_skin;
        this.wound_base = vo.wound_base;
        
        // incision details
        this.incisionassessmentid = vo.incisionassessmentid;
        this.incision_status = vo.incision_status;
        this.incision_exudate_amount = vo.incision_exudate_amount;
        this.incision_discharge_reason = vo.incision_discharge_reason;
        this.pain = vo.pain;
        this.postop_management = vo.postop_management;
        this.incision_exudate = vo.incision_exudate;
        this.incision_closure_type = vo.incision_closure_type;
        
        // drain details
        this.drainassessmentid = vo.drainassessmentid;
        this.type_of_drain = vo.type_of_drain;
        this.tubes_discharge_reason = vo.tubes_discharge_reason;
        this.drainage_odour = vo.drainage_odour;
        this.peri_drain_skin = vo.peri_drain_skin;
        this.drain_site = vo.drain_site;
        this.characteristics = vo.characteristics;
        
        // ostomy details
        this.ostomyassessmentid = vo.ostomyassessmentid;
        this.construction = vo.construction;
        this.devices = vo.devices;
        this.ostomy_discharge_reason = vo.ostomy_discharge_reason;
        this.flange_pouch = vo.flange_pouch;
        this.urine_quantity = vo.urine_quantity;
        this.stoma_shape = vo.stoma_shape;
        this.nutritional_status = vo.nutritional_status;
        this.status = vo.status;
        this.drainage = vo.drainage;
        this.mucocutaneous_margin = vo.mucocutaneous_margin;
        this.peri_fistula_skin = vo.peri_fistula_skin;
        this.concerns_for_pouching = vo.concerns_for_pouching;
        this.profile = vo.profile;
        this.urine_colour = vo.urine_colour;
        this.urine_type = vo.urine_type;
        this.skin_appearance = vo.skin_appearance;
        this.skin_contour = vo.skin_contour;
        this.self_care_progress = vo.self_care_progress;
        this.peri_ostomy_skin = vo.peri_ostomy_skin;
    }
    
    public void cleanUpNPTWValues(){
        this.pressure_reading = "0";
        this.initiated_by = "0";
        this.negative_pressure_connector = "0";
        this.reason_for_ending = "0";
        this.goal_of_therapy = "0";
        this.vendor_name = "0";
        this.machine_acquirement = "0";
    }
    
    public void copyTreatmentArraysData(ExportDataSNVO vo){
        this.c_s_result = vo.c_s_result;
        this.adjunctive_treatment_modalities = vo.adjunctive_treatment_modalities;
    }
    
    public void copyPatientGoalsEtiologiesAcquiredData(ExportDataSNVO vo){
        this.wound_etiology = vo.wound_etiology;
        this.ostomy_etiology = vo.ostomy_etiology;
        this.postop_etiology = vo.postop_etiology;
        this.tubes_drains_etiology = vo.tubes_drains_etiology;
        this.skin_etiology = vo.skin_etiology;
        
        this.goal_of_healing_wound = vo.goal_of_healing_wound;
        this.goal_of_healing_ostomy = vo.goal_of_healing_ostomy;
        this.goal_of_healing_incision = vo.goal_of_healing_incision;
        this.goal_of_healing_drain = vo.goal_of_healing_drain;
        this.goal_of_healing_skin = vo.goal_of_healing_skin;
        
        this.acquired = vo.acquired;
    }
    
    public void copyPatientArraysData(ExportDataSNVO vo){
        this.co_morbidities_endocrine = vo.co_morbidities_endocrine;
        this.co_morbidities_heart = vo.co_morbidities_heart;
        this.co_morbidities_musculoeskeletal = vo.co_morbidities_musculoeskeletal;
        this.co_morbidities_neurological = vo.co_morbidities_neurological;
        this.co_morbidities_psychiatrric_mood = vo.co_morbidities_psychiatrric_mood;
        this.co_morbidities_pulmonary = vo.co_morbidities_pulmonary;
        this.co_morbidities_sensory = vo.co_morbidities_sensory;
        this.co_morbidities_infections = vo.co_morbidities_infections;
        this.co_morbidities_other = vo.co_morbidities_other;
        
        this.interfering_factors = vo.interfering_factors;
        this.interfering_medications = vo.interfering_medications;
        
        this.external_referrals =  vo.external_referrals;
        this.arranged =  vo.arranged;
        
        this.labs_investigations = vo.labs_investigations;
    }
    
    public void copyLimbMobilityData(ExportDataSNVO vo){
        this.basic_limb_temperature = vo.basic_limb_temperature;
        this.limb_edema = vo.limb_edema;
        this.limb_edema_severity = vo.limb_edema_severity;
        this.palpation = vo.palpation;
        this.missing_limbs = vo.missing_limbs;
        this.color = vo.color;
        this.basic_pain_assessments = vo.basic_pain_assessments;
        this.adv_pain_assessments = vo.adv_pain_assessments;
        this.basic_skin_assessments = vo.basic_skin_assessments;
        this.sensory = vo.sensory;
        this.proprioception = vo.proprioception;
        this.deformities = vo.deformities;
        this.toes = vo.toes;
        this.weight_bearing_status = vo.weight_bearing_status;
        this.balance = vo.balance;
        this.calf_muscle_function = vo.calf_muscle_function;
        this.mobility_aids = vo.mobility_aids;
        this.muscle_tone = vo.muscle_tone;
        this.arches = vo.arches;
        this.foot_active = vo.foot_active;
        this.foot_passive = vo.foot_passive;
        this.lab_done = vo.lab_done;
        this.missing_upper_limb = vo.missing_upper_limb;
        this.upper_limb_edema_location = vo.upper_limb_edema_location;
        this.adv_lim_temperature = vo.adv_lim_temperature;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getDischarge_reason() {
        return discharge_reason;
    }

    public void setDischarge_reason(String discharge_reason) {
        this.discharge_reason = discharge_reason;
    }

    public String getFunding_source() {
        return funding_source;
    }

    public void setFunding_source(String funding_source) {
        this.funding_source = funding_source;
    }

    public String getPatient_residence() {
        return patient_residence;
    }

    public void setPatient_residence(String patient_residence) {
        this.patient_residence = patient_residence;
    }

    public String getCo_morbidities_endocrine() {
        return co_morbidities_endocrine;
    }

    public void setCo_morbidities_endocrine(String co_morbidities_endocrine) {
        this.co_morbidities_endocrine = co_morbidities_endocrine;
    }

    public String getCo_morbidities_heart() {
        return co_morbidities_heart;
    }

    public void setCo_morbidities_heart(String co_morbidities_heart) {
        this.co_morbidities_heart = co_morbidities_heart;
    }

    public String getCo_morbidities_musculoeskeletal() {
        return co_morbidities_musculoeskeletal;
    }

    public void setCo_morbidities_musculoeskeletal(String co_morbidities_musculoeskeletal) {
        this.co_morbidities_musculoeskeletal = co_morbidities_musculoeskeletal;
    }

    public String getCo_morbidities_neurological() {
        return co_morbidities_neurological;
    }

    public void setCo_morbidities_neurological(String co_morbidities_neurological) {
        this.co_morbidities_neurological = co_morbidities_neurological;
    }

    public String getCo_morbidities_psychiatrric_mood() {
        return co_morbidities_psychiatrric_mood;
    }

    public void setCo_morbidities_psychiatrric_mood(String co_morbidities_psychiatrric_mood) {
        this.co_morbidities_psychiatrric_mood = co_morbidities_psychiatrric_mood;
    }

    public String getCo_morbidities_pulmonary() {
        return co_morbidities_pulmonary;
    }

    public void setCo_morbidities_pulmonary(String co_morbidities_pulmonary) {
        this.co_morbidities_pulmonary = co_morbidities_pulmonary;
    }

    public String getCo_morbidities_sensory() {
        return co_morbidities_sensory;
    }

    public void setCo_morbidities_sensory(String co_morbidities_sensory) {
        this.co_morbidities_sensory = co_morbidities_sensory;
    }

    public String getCo_morbidities_infections() {
        return co_morbidities_infections;
    }

    public void setCo_morbidities_infections(String co_morbidities_infections) {
        this.co_morbidities_infections = co_morbidities_infections;
    }

    public String getCo_morbidities_other() {
        return co_morbidities_other;
    }

    public void setCo_morbidities_other(String co_morbidities_other) {
        this.co_morbidities_other = co_morbidities_other;
    }

    public String getInterfering_factors() {
        return interfering_factors;
    }

    public void setInterfering_factors(String interfering_factors) {
        this.interfering_factors = interfering_factors;
    }

    public String getInterfering_medications() {
        return interfering_medications;
    }

    public void setInterfering_medications(String interfering_medications) {
        this.interfering_medications = interfering_medications;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAlbumin() {
        return albumin;
    }

    public void setAlbumin(String albumin) {
        this.albumin = albumin;
    }

    public String getPre_albumin() {
        return pre_albumin;
    }

    public void setPre_albumin(String pre_albumin) {
        this.pre_albumin = pre_albumin;
    }

    public String getLabs_investigations() {
        return labs_investigations;
    }

    public void setLabs_investigations(String labs_investigations) {
        this.labs_investigations = labs_investigations;
    }

    public String getExternal_referrals() {
        return external_referrals;
    }

    public void setExternal_referrals(String external_referrals) {
        this.external_referrals = external_referrals;
    }

    public String getArranged() {
        return arranged;
    }

    public void setArranged(String arranged) {
        this.arranged = arranged;
    }

    public String getBasic_limb_temperature() {
        return basic_limb_temperature;
    }

    public void setBasic_limb_temperature(String basic_limb_temperature) {
        this.basic_limb_temperature = basic_limb_temperature;
    }

    public String getLimb_edema() {
        return limb_edema;
    }

    public void setLimb_edema(String limb_edema) {
        this.limb_edema = limb_edema;
    }

    public String getLimb_edema_severity() {
        return limb_edema_severity;
    }

    public void setLimb_edema_severity(String limb_edema_severity) {
        this.limb_edema_severity = limb_edema_severity;
    }

    public String getPalpation() {
        return palpation;
    }

    public void setPalpation(String palpation) {
        this.palpation = palpation;
    }

    public String getMissing_limbs() {
        return missing_limbs;
    }

    public void setMissing_limbs(String missing_limbs) {
        this.missing_limbs = missing_limbs;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBasic_pain_assessments() {
        return basic_pain_assessments;
    }

    public void setBasic_pain_assessments(String basic_pain_assessments) {
        this.basic_pain_assessments = basic_pain_assessments;
    }

    public String getAdv_pain_assessments() {
        return adv_pain_assessments;
    }

    public void setAdv_pain_assessments(String adv_pain_assessments) {
        this.adv_pain_assessments = adv_pain_assessments;
    }

    public String getBasic_skin_assessments() {
        return basic_skin_assessments;
    }

    public void setBasic_skin_assessments(String basic_skin_assessments) {
        this.basic_skin_assessments = basic_skin_assessments;
    }

    public String getSensory() {
        return sensory;
    }

    public void setSensory(String sensory) {
        this.sensory = sensory;
    }

    public String getProprioception() {
        return proprioception;
    }

    public void setProprioception(String proprioception) {
        this.proprioception = proprioception;
    }

    public String getDeformities() {
        return deformities;
    }

    public void setDeformities(String deformities) {
        this.deformities = deformities;
    }

    public String getToes() {
        return toes;
    }

    public void setToes(String toes) {
        this.toes = toes;
    }

    public String getWeight_bearing_status() {
        return weight_bearing_status;
    }

    public void setWeight_bearing_status(String weight_bearing_status) {
        this.weight_bearing_status = weight_bearing_status;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCalf_muscle_function() {
        return calf_muscle_function;
    }

    public void setCalf_muscle_function(String calf_muscle_function) {
        this.calf_muscle_function = calf_muscle_function;
    }

    public String getMobility_aids() {
        return mobility_aids;
    }

    public void setMobility_aids(String mobility_aids) {
        this.mobility_aids = mobility_aids;
    }

    public String getMuscle_tone() {
        return muscle_tone;
    }

    public void setMuscle_tone(String muscle_tone) {
        this.muscle_tone = muscle_tone;
    }

    public String getArches() {
        return arches;
    }

    public void setArches(String arches) {
        this.arches = arches;
    }

    public String getFoot_active() {
        return foot_active;
    }

    public void setFoot_active(String foot_active) {
        this.foot_active = foot_active;
    }

    public String getFoot_passive() {
        return foot_passive;
    }

    public void setFoot_passive(String foot_passive) {
        this.foot_passive = foot_passive;
    }

    public String getLab_done() {
        return lab_done;
    }

    public void setLab_done(String lab_done) {
        this.lab_done = lab_done;
    }

    public String getTrancutaneous_pressures() {
        return trancutaneous_pressures;
    }

    public void setTrancutaneous_pressures(String trancutaneous_pressures) {
        this.trancutaneous_pressures = trancutaneous_pressures;
    }

    public String getMissing_upper_limb() {
        return missing_upper_limb;
    }

    public void setMissing_upper_limb(String missing_upper_limb) {
        this.missing_upper_limb = missing_upper_limb;
    }

    public String getUpper_limb_edema_location() {
        return upper_limb_edema_location;
    }

    public void setUpper_limb_edema_location(String upper_limb_edema_location) {
        this.upper_limb_edema_location = upper_limb_edema_location;
    }

    public String getRange_of_motion() {
        return range_of_motion;
    }

    public void setRange_of_motion(String range_of_motion) {
        this.range_of_motion = range_of_motion;
    }

    public String getAdv_lim_temperature() {
        return adv_lim_temperature;
    }

    public void setAdv_lim_temperature(String adv_lim_temperature) {
        this.adv_lim_temperature = adv_lim_temperature;
    }

    public String getWoundprofile_id() {
        return woundprofile_id;
    }

    public void setWoundprofile_id(String woundprofile_id) {
        this.woundprofile_id = woundprofile_id;
    }

    public String getPatient_limitations() {
        return patient_limitations;
    }

    public void setPatient_limitations(String patient_limitations) {
        this.patient_limitations = patient_limitations;
    }

    public String getTeaching_goals() {
        return teaching_goals;
    }

    public void setTeaching_goals(String teaching_goals) {
        this.teaching_goals = teaching_goals;
    }

    public String getPressure_ulcer_stage() {
        return pressure_ulcer_stage;
    }

    public void setPressure_ulcer_stage(String pressure_ulcer_stage) {
        this.pressure_ulcer_stage = pressure_ulcer_stage;
    }

    public String getAlpha_id() {
        return alpha_id;
    }

    public void setAlpha_id(String alpha_id) {
        this.alpha_id = alpha_id;
    }

    public String getWound_etiology() {
        return wound_etiology;
    }

    public void setWound_etiology(String wound_etiology) {
        this.wound_etiology = wound_etiology;
    }

    public String getOstomy_etiology() {
        return ostomy_etiology;
    }

    public void setOstomy_etiology(String ostomy_etiology) {
        this.ostomy_etiology = ostomy_etiology;
    }

    public String getType_of_ostomy() {
        return type_of_ostomy;
    }

    public void setType_of_ostomy(String type_of_ostomy) {
        this.type_of_ostomy = type_of_ostomy;
    }

    public String getPostop_etiology() {
        return postop_etiology;
    }

    public void setPostop_etiology(String postop_etiology) {
        this.postop_etiology = postop_etiology;
    }

    public String getTubes_drains_etiology() {
        return tubes_drains_etiology;
    }

    public void setTubes_drains_etiology(String tubes_drains_etiology) {
        this.tubes_drains_etiology = tubes_drains_etiology;
    }

    public String getSkin_etiology() {
        return skin_etiology;
    }

    public void setSkin_etiology(String skin_etiology) {
        this.skin_etiology = skin_etiology;
    }

    public String getGoal_of_healing_wound() {
        return goal_of_healing_wound;
    }

    public void setGoal_of_healing_wound(String goal_of_healing_wound) {
        this.goal_of_healing_wound = goal_of_healing_wound;
    }

    public String getGoal_of_healing_incision() {
        return goal_of_healing_incision;
    }

    public void setGoal_of_healing_incision(String goal_of_healing_incision) {
        this.goal_of_healing_incision = goal_of_healing_incision;
    }

    public String getGoal_of_healing_ostomy() {
        return goal_of_healing_ostomy;
    }

    public void setGoal_of_healing_ostomy(String goal_of_healing_ostomy) {
        this.goal_of_healing_ostomy = goal_of_healing_ostomy;
    }

    public String getGoal_of_healing_drain() {
        return goal_of_healing_drain;
    }

    public void setGoal_of_healing_drain(String goal_of_healing_drain) {
        this.goal_of_healing_drain = goal_of_healing_drain;
    }

    public String getGoal_of_healing_skin() {
        return goal_of_healing_skin;
    }

    public void setGoal_of_healing_skin(String goal_of_healing_skin) {
        this.goal_of_healing_skin = goal_of_healing_skin;
    }

    public String getAcquired() {
        return acquired;
    }

    public void setAcquired(String acquired) {
        this.acquired = acquired;
    }

    public String getWoundassessmentid() {
        return woundassessmentid;
    }

    public void setWoundassessmentid(String woundassessmentid) {
        this.woundassessmentid = woundassessmentid;
    }

    public String getC_s_result() {
        return c_s_result;
    }

    public void setC_s_result(String c_s_result) {
        this.c_s_result = c_s_result;
    }

    public String getC_s_route() {
        return c_s_route;
    }

    public void setC_s_route(String c_s_route) {
        this.c_s_route = c_s_route;
    }

    public String getAdjunctive_treatment_modalities() {
        return adjunctive_treatment_modalities;
    }

    public void setAdjunctive_treatment_modalities(String adjunctive_treatment_modalities) {
        this.adjunctive_treatment_modalities = adjunctive_treatment_modalities;
    }

    public String getPressure_reading() {
        return pressure_reading;
    }

    public void setPressure_reading(String pressure_reading) {
        this.pressure_reading = pressure_reading;
    }

    public String getInitiated_by() {
        return initiated_by;
    }

    public void setInitiated_by(String initiated_by) {
        this.initiated_by = initiated_by;
    }

    public String getNegative_pressure_connector() {
        return negative_pressure_connector;
    }

    public void setNegative_pressure_connector(String negative_pressure_connector) {
        this.negative_pressure_connector = negative_pressure_connector;
    }

    public String getReason_for_ending() {
        return reason_for_ending;
    }

    public void setReason_for_ending(String reason_for_ending) {
        this.reason_for_ending = reason_for_ending;
    }

    public String getGoal_of_therapy() {
        return goal_of_therapy;
    }

    public void setGoal_of_therapy(String goal_of_therapy) {
        this.goal_of_therapy = goal_of_therapy;
    }

    public String getDressing_change_frequency() {
        return dressing_change_frequency;
    }

    public void setDressing_change_frequency(String dressing_change_frequency) {
        this.dressing_change_frequency = dressing_change_frequency;
    }

    public String getBnursing_vistit_frecuency() {
        return bnursing_vistit_frecuency;
    }

    public void setBnursing_vistit_frecuency(String bnursing_vistit_frecuency) {
        this.bnursing_vistit_frecuency = bnursing_vistit_frecuency;
    }

    public String getReading_variable() {
        return reading_variable;
    }

    public void setReading_variable(String reading_variable) {
        this.reading_variable = reading_variable;
    }

    public String getClinical_barriers() {
        return clinical_barriers;
    }

    public void setClinical_barriers(String clinical_barriers) {
        this.clinical_barriers = clinical_barriers;
    }

    public String getIv_therapy_barriers() {
        return iv_therapy_barriers;
    }

    public void setIv_therapy_barriers(String iv_therapy_barriers) {
        this.iv_therapy_barriers = iv_therapy_barriers;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getMachine_acquirement() {
        return machine_acquirement;
    }

    public void setMachine_acquirement(String machine_acquirement) {
        this.machine_acquirement = machine_acquirement;
    }

    public String getAssessmentwoundid() {
        return assessmentwoundid;
    }

    public void setAssessmentwoundid(String assessmentwoundid) {
        this.assessmentwoundid = assessmentwoundid;
    }

    public String getExudate_type() {
        return exudate_type;
    }

    public void setExudate_type(String exudate_type) {
        this.exudate_type = exudate_type;
    }

    public String getExudate_amount() {
        return exudate_amount;
    }

    public void setExudate_amount(String exudate_amount) {
        this.exudate_amount = exudate_amount;
    }

    public String getWound_discharge_reason() {
        return wound_discharge_reason;
    }

    public void setWound_discharge_reason(String wound_discharge_reason) {
        this.wound_discharge_reason = wound_discharge_reason;
    }

    public String getWound_edge() {
        return wound_edge;
    }

    public void setWound_edge(String wound_edge) {
        this.wound_edge = wound_edge;
    }

    public String getWound_base() {
        return wound_base;
    }

    public void setWound_base(String wound_base) {
        this.wound_base = wound_base;
    }

    public String getWound_s_s_infection() {
        return wound_s_s_infection;
    }

    public void setWound_s_s_infection(String wound_s_s_infection) {
        this.wound_s_s_infection = wound_s_s_infection;
    }

    public String getPeriwound_skin() {
        return periwound_skin;
    }

    public void setPeriwound_skin(String periwound_skin) {
        this.periwound_skin = periwound_skin;
    }

    public String getTime_blood_sugar() {
        return time_blood_sugar;
    }

    public void setTime_blood_sugar(String time_blood_sugar) {
        this.time_blood_sugar = time_blood_sugar;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getIncisionassessmentid() {
        return incisionassessmentid;
    }

    public void setIncisionassessmentid(String incisionassessmentid) {
        this.incisionassessmentid = incisionassessmentid;
    }

    public String getPostop_management() {
        return postop_management;
    }

    public void setPostop_management(String postop_management) {
        this.postop_management = postop_management;
    }

    public String getIncision_exudate() {
        return incision_exudate;
    }

    public void setIncision_exudate(String incision_exudate) {
        this.incision_exudate = incision_exudate;
    }

    public String getIncision_s_s_infection() {
        return incision_s_s_infection;
    }

    public void setIncision_s_s_infection(String incision_s_s_infection) {
        this.incision_s_s_infection = incision_s_s_infection;
    }

    public String getIncision_status() {
        return incision_status;
    }

    public void setIncision_status(String incision_status) {
        this.incision_status = incision_status;
    }

    public String getIncision_discharge_reason() {
        return incision_discharge_reason;
    }

    public void setIncision_discharge_reason(String incision_discharge_reason) {
        this.incision_discharge_reason = incision_discharge_reason;
    }

    public String getPain() {
        return pain;
    }

    public void setPain(String pain) {
        this.pain = pain;
    }

    public String getIncision_closure_type() {
        return incision_closure_type;
    }

    public void setIncision_closure_type(String incision_closure_type) {
        this.incision_closure_type = incision_closure_type;
    }

    public String getIncision_exudate_amount() {
        return incision_exudate_amount;
    }

    public void setIncision_exudate_amount(String incision_exudate_amount) {
        this.incision_exudate_amount = incision_exudate_amount;
    }

    public String getDrainassessmentid() {
        return drainassessmentid;
    }

    public void setDrainassessmentid(String drainassessmentid) {
        this.drainassessmentid = drainassessmentid;
    }

    public String getDrainage_odour() {
        return drainage_odour;
    }

    public void setDrainage_odour(String drainage_odour) {
        this.drainage_odour = drainage_odour;
    }

    public String getType_of_drain() {
        return type_of_drain;
    }

    public void setType_of_drain(String type_of_drain) {
        this.type_of_drain = type_of_drain;
    }

    public String getPeri_drain_skin() {
        return peri_drain_skin;
    }

    public void setPeri_drain_skin(String peri_drain_skin) {
        this.peri_drain_skin = peri_drain_skin;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getExudate() {
        return exudate;
    }

    public void setExudate(String exudate) {
        this.exudate = exudate;
    }

    public String getTubes_exudate_amount() {
        return tubes_exudate_amount;
    }

    public void setTubes_exudate_amount(String tubes_exudate_amount) {
        this.tubes_exudate_amount = tubes_exudate_amount;
    }

    public String getDrainage_amount_desc() {
        return drainage_amount_desc;
    }

    public void setDrainage_amount_desc(String drainage_amount_desc) {
        this.drainage_amount_desc = drainage_amount_desc;
    }

    public String getTubes_discharge_reason() {
        return tubes_discharge_reason;
    }

    public void setTubes_discharge_reason(String tubes_discharge_reason) {
        this.tubes_discharge_reason = tubes_discharge_reason;
    }

    public String getDrain_site() {
        return drain_site;
    }

    public void setDrain_site(String drain_site) {
        this.drain_site = drain_site;
    }

    public String getOstomyassessmentid() {
        return ostomyassessmentid;
    }

    public void setOstomyassessmentid(String ostomyassessmentid) {
        this.ostomyassessmentid = ostomyassessmentid;
    }

    public String getConstruction() {
        return construction;
    }

    public void setConstruction(String construction) {
        this.construction = construction;
    }

    public String getDrainage() {
        return drainage;
    }

    public void setDrainage(String drainage) {
        this.drainage = drainage;
    }

    public String getMucocutaneous_margin() {
        return mucocutaneous_margin;
    }

    public void setMucocutaneous_margin(String mucocutaneous_margin) {
        this.mucocutaneous_margin = mucocutaneous_margin;
    }

    public String getPeri_fistula_skin() {
        return peri_fistula_skin;
    }

    public void setPeri_fistula_skin(String peri_fistula_skin) {
        this.peri_fistula_skin = peri_fistula_skin;
    }

    public String getDevices() {
        return devices;
    }

    public void setDevices(String devices) {
        this.devices = devices;
    }

    public String getOstomy_discharge_reason() {
        return ostomy_discharge_reason;
    }

    public void setOstomy_discharge_reason(String ostomy_discharge_reason) {
        this.ostomy_discharge_reason = ostomy_discharge_reason;
    }

    public String getConcerns_for_pouching() {
        return concerns_for_pouching;
    }

    public void setConcerns_for_pouching(String concerns_for_pouching) {
        this.concerns_for_pouching = concerns_for_pouching;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getFlange_pouch() {
        return flange_pouch;
    }

    public void setFlange_pouch(String flange_pouch) {
        this.flange_pouch = flange_pouch;
    }

    public String getUrine_colour() {
        return urine_colour;
    }

    public void setUrine_colour(String urine_colour) {
        this.urine_colour = urine_colour;
    }

    public String getUrine_type() {
        return urine_type;
    }

    public void setUrine_type(String urine_type) {
        this.urine_type = urine_type;
    }

    public String getUrine_quantity() {
        return urine_quantity;
    }

    public void setUrine_quantity(String urine_quantity) {
        this.urine_quantity = urine_quantity;
    }

    public String getSkin_appearance() {
        return skin_appearance;
    }

    public void setSkin_appearance(String skin_appearance) {
        this.skin_appearance = skin_appearance;
    }

    public String getSkin_contour() {
        return skin_contour;
    }

    public void setSkin_contour(String skin_contour) {
        this.skin_contour = skin_contour;
    }

    public String getSelf_care_progress() {
        return self_care_progress;
    }

    public void setSelf_care_progress(String self_care_progress) {
        this.self_care_progress = self_care_progress;
    }

    public String getPeri_ostomy_skin() {
        return peri_ostomy_skin;
    }

    public void setPeri_ostomy_skin(String peri_ostomy_skin) {
        this.peri_ostomy_skin = peri_ostomy_skin;
    }

    public String getStoma_shape() {
        return stoma_shape;
    }

    public void setStoma_shape(String stoma_shape) {
        this.stoma_shape = stoma_shape;
    }

    public String getNutritional_status() {
        return nutritional_status;
    }

    public void setNutritional_status(String nutritional_status) {
        this.nutritional_status = nutritional_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int compare(Object o1, Object o2) {
        ExportDataSNVO r1 = (ExportDataSNVO) o1;
        ExportDataSNVO r2 = (ExportDataSNVO) o2;
        
        if (!r1.patient_id.equals(r2.patient_id)){
            return Integer.getInteger(r1.patient_id).compareTo(Integer.getInteger(r2.patient_id));
        } else {
            if (!r1.woundprofile_id.equals(r2.woundprofile_id)){
                return Integer.getInteger(r1.woundprofile_id).compareTo(Integer.getInteger(r2.woundprofile_id));
            } else {
                if (!r1.alpha_id.equals(r2.alpha_id)){
                    return Integer.getInteger(r1.alpha_id).compareTo(Integer.getInteger(r2.alpha_id));
                } else {
                    if (!r1.woundassessmentid.equals(r2.woundassessmentid)){
                        return Integer.getInteger(r1.woundassessmentid).compareTo(Integer.getInteger(r2.woundassessmentid));
                    } else {
                        if (r1.assessmentwoundid != null && r2.assessmentwoundid != null && !r1.assessmentwoundid.equals(r2.assessmentwoundid)){
                            return Integer.getInteger(r1.assessmentwoundid).compareTo(Integer.getInteger(r2.assessmentwoundid));
                        } else {
                            if (r1.incisionassessmentid != null && r2.incisionassessmentid != null && !r1.incisionassessmentid.equals(r2.incisionassessmentid)){
                                return Integer.getInteger(r1.incisionassessmentid).compareTo(Integer.getInteger(r2.incisionassessmentid));
                            } else {
                                if (r1.drainassessmentid != null && r2.drainassessmentid != null && !r1.drainassessmentid.equals(r2.drainassessmentid)){
                                    return Integer.getInteger(r1.drainassessmentid).compareTo(Integer.getInteger(r2.drainassessmentid));
                                } else {
                                    if (r1.ostomyassessmentid != null && r2.ostomyassessmentid != null && !r1.ostomyassessmentid.equals(r2.ostomyassessmentid)){
                                        return Integer.getInteger(r1.ostomyassessmentid).compareTo(Integer.getInteger(r2.ostomyassessmentid));
                                    } else {
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    
}
