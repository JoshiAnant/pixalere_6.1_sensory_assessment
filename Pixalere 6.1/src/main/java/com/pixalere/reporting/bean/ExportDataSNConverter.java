/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.bean;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @since 6.1
 * @author Jose
 */
public class ExportDataSNConverter {
    
    public static ExportDataSNVO convertToSNValues(ExportDataSNVO vo){
        
        vo.setDischarge_reason(convertValue(discharge_reasonMap, vo.getDischarge_reason()));
        vo.setFunding_source(convertValue(funding_sourceMap, vo.getFunding_source()));
        vo.setCo_morbidities_endocrine(convertValue(co_morbidities_endocrineMap, vo.getCo_morbidities_endocrine()));
        vo.setCo_morbidities_heart(convertValue(co_morbidities_heartMap, vo.getCo_morbidities_heart()));
        vo.setCo_morbidities_musculoeskeletal(convertValue(co_morbidities_musculoeskeletalMap, vo.getCo_morbidities_musculoeskeletal()));
        vo.setCo_morbidities_neurological(convertValue(co_morbidities_neurologicalMap, vo.getCo_morbidities_neurological()));
        vo.setCo_morbidities_psychiatrric_mood(convertValue(co_morbidities_psychiatrric_moodMap, vo.getCo_morbidities_psychiatrric_mood()));
        vo.setCo_morbidities_pulmonary(convertValue(co_morbidities_pulmonaryMap, vo.getCo_morbidities_pulmonary()));
        vo.setCo_morbidities_sensory(convertValue(co_morbidities_sensoryMap, vo.getCo_morbidities_sensory()));
        vo.setCo_morbidities_infections(convertValue(co_morbidities_infectionsMap, vo.getCo_morbidities_infections()));
        vo.setCo_morbidities_other(convertValue(co_morbidities_otherMap, vo.getCo_morbidities_other()));
        vo.setInterfering_factors(convertValue(interfering_factorsMap, vo.getInterfering_factors()));
        vo.setInterfering_medications(convertValue(interfering_medicationsMap, vo.getInterfering_medications()));
        vo.setGender(convertValue(genderMap, vo.getGender()));
        vo.setAlbumin(convertValue(albuminMap, vo.getAlbumin()));
        vo.setPre_albumin(convertValue(pre_albuminMap, vo.getPre_albumin()));
        vo.setLabs_investigations(convertValue(labs_investigationsMap, vo.getLabs_investigations()));
        vo.setExternal_referrals(convertValue(external_referralsMap, vo.getExternal_referrals()));
        vo.setArranged(convertValue(arrangedMap, vo.getArranged()));
        vo.setBasic_limb_temperature(convertValue(basic_limb_temperatureMap, vo.getBasic_limb_temperature()));
        vo.setLimb_edema(convertValue(limb_edemaMap, vo.getLimb_edema()));
        vo.setLimb_edema_severity(convertValue(limb_edema_severityMap, vo.getLimb_edema_severity()));
        vo.setPalpation(convertValue(palpationMap, vo.getPalpation()));
        vo.setMissing_limbs(convertValue(missing_limbsMap, vo.getMissing_limbs()));
        
        vo.setColor(convertValue(colorMap, vo.getColor()));
        vo.setBasic_pain_assessments(convertValue(basic_pain_assessmentsMap, vo.getBasic_pain_assessments()));
        vo.setAdv_pain_assessments(convertValue(adv_pain_assessmentsMap, vo.getAdv_pain_assessments()));
        vo.setBasic_skin_assessments(convertValue(basic_skin_assessmentsMap, vo.getBasic_skin_assessments()));
        vo.setSensory(convertValue(sensoryMap, vo.getSensory()));
        vo.setProprioception(convertValue(proprioceptionMap, vo.getProprioception()));
        vo.setDeformities(convertValue(deformitiesMap, vo.getDeformities()));
        vo.setToes(convertValue(toesMap, vo.getToes()));
        vo.setWeight_bearing_status(convertValue(weight_bearing_statusMap, vo.getWeight_bearing_status()));
        vo.setBalance(convertValue(balanceMap, vo.getBalance()));
        vo.setCalf_muscle_function(convertValue(calf_muscle_functionMap, vo.getCalf_muscle_function()));
        vo.setMobility_aids(convertValue(mobility_aidsMap, vo.getMobility_aids()));
        vo.setMuscle_tone(convertValue(muscle_toneMap, vo.getMuscle_tone()));
        vo.setArches(convertValue(archesMap, vo.getArches()));
        vo.setFoot_active(convertValue(foot_activeMap, vo.getFoot_active()));
        vo.setFoot_passive(convertValue(foot_passiveMap, vo.getFoot_passive()));
        vo.setLab_done(convertValue(lab_doneMap, vo.getLab_done()));
        vo.setMissing_upper_limb(convertValue(missing_upper_limbMap, vo.getMissing_upper_limb()));
        vo.setUpper_limb_edema_location(convertValue(upper_limb_edema_locationMap, vo.getUpper_limb_edema_location()));
        vo.setRange_of_motion(convertValue(range_of_motionMap, vo.getRange_of_motion()));
        vo.setAdv_lim_temperature(convertValue(adv_lim_temperatureMap, vo.getAdv_lim_temperature()));
        vo.setPatient_limitations(convertValue(patient_limitationsMap, vo.getPatient_limitations()));
        vo.setTeaching_goals(convertValue(teaching_goalsMap, vo.getTeaching_goals()));
        vo.setPressure_ulcer_stage(convertValue(pressure_ulcer_stageMap, vo.getPressure_ulcer_stage()));
        vo.setWound_etiology(convertValue(wound_etiologyMap, vo.getWound_etiology()));
        vo.setOstomy_etiology(convertValue(ostomy_etiologyMap, vo.getOstomy_etiology()));
        vo.setType_of_ostomy(convertValue(type_of_ostomyMap, vo.getType_of_ostomy()));
        vo.setPostop_etiology(convertValue(postop_etiologyMap, vo.getPostop_etiology()));
        vo.setTubes_drains_etiology(convertValue(tubes_drains_etiologyMap, vo.getTubes_drains_etiology()));
        vo.setSkin_etiology(convertValue(skin_etiologyMap, vo.getSkin_etiology()));
        vo.setGoal_of_healing_wound(convertValue(goal_of_healing_woundMap, vo.getGoal_of_healing_wound()));
        vo.setGoal_of_healing_incision(convertValue(goal_of_healing_incisionMap, vo.getGoal_of_healing_incision()));
        vo.setGoal_of_healing_ostomy(convertValue(goal_of_healing_ostomyMap, vo.getGoal_of_healing_ostomy()));
        vo.setGoal_of_healing_drain(convertValue(goal_of_healing_drainMap, vo.getGoal_of_healing_drain()));
        vo.setGoal_of_healing_skin(convertValue(goal_of_healing_skinMap, vo.getGoal_of_healing_skin()));
        vo.setAcquired(convertValue(acquiredMap, vo.getAcquired()));
        vo.setC_s_result(convertValue(c_s_resultMap, vo.getC_s_result()));
        vo.setAdjunctive_treatment_modalities(convertValue(adjunctive_treatment_modalitiesMap, vo.getAdjunctive_treatment_modalities()));
        vo.setPressure_reading(convertValue(pressure_readingMap, vo.getPressure_reading()));
        vo.setInitiated_by(convertValue(initiated_byMap, vo.getInitiated_by()));
        vo.setNegative_pressure_connector(convertValue(negative_pressure_connectorMap, vo.getNegative_pressure_connector()));
        vo.setReason_for_ending(convertValue(reason_for_endingMap, vo.getReason_for_ending()));
        vo.setGoal_of_therapy(convertValue(goal_of_therapyMap, vo.getGoal_of_therapy()));
        vo.setDressing_change_frequency(convertValue(dressing_change_frequencyMap, vo.getDressing_change_frequency()));
        vo.setBnursing_vistit_frecuency(convertValue(bnursing_vistit_frecuencyMap, vo.getBnursing_vistit_frecuency()));
        vo.setVendor_name(convertValue(vendor_nameMap, vo.getVendor_name()));
        vo.setMachine_acquirement(convertValue(machine_acquirementMap, vo.getMachine_acquirement()));
        
        vo.setExudate_type(convertValue(exudate_typeMap, vo.getExudate_type()));
        vo.setExudate_amount(convertValue(exudate_amountMap, vo.getExudate_amount()));
        vo.setWound_discharge_reason(convertValue(wound_discharge_reasonMap, vo.getWound_discharge_reason()));
        vo.setWound_edge(convertValue(wound_edgeMap, vo.getWound_edge()));
        vo.setWound_base(convertValue(wound_baseMap, vo.getWound_base()));
        vo.setPeriwound_skin(convertValue(periwound_skinMap, vo.getPeriwound_skin()));
        vo.setPriority(convertValue(priorityMap, vo.getPriority()));
        vo.setPostop_management(convertValue(postop_managementMap, vo.getPostop_management()));
        vo.setIncision_exudate(convertValue(incision_exudateMap, vo.getIncision_exudate()));
        vo.setIncision_status(convertValue(incision_statusMap, vo.getIncision_status()));
        vo.setIncision_discharge_reason(convertValue(incision_discharge_reasonMap, vo.getIncision_discharge_reason()));
        vo.setPain(convertValue(painMap, vo.getPain()));
        vo.setIncision_closure_type(convertValue(incision_closure_typeMap, vo.getIncision_closure_type()));
        vo.setIncision_exudate_amount(convertValue(incision_exudate_amountMap, vo.getIncision_exudate_amount()));
        vo.setDrainage_odour(convertValue(drainage_odourMap, vo.getDrainage_odour()));
        vo.setType_of_drain(convertValue(type_of_drainMap, vo.getType_of_drain()));
        vo.setPeri_drain_skin(convertValue(peri_drain_skinMap, vo.getPeri_drain_skin()));
        vo.setCharacteristics(convertValue(characteristicsMap, vo.getCharacteristics()));
        //vo.setTubes_discharge_reason(convertValue(tubes_discharge_reasonMap, vo.getTubes_discharge_reason())); // Missing req values - The S&N system don't have the req values
        vo.setDrain_site(convertValue(drain_siteMap, vo.getDrain_site()));
        
        // Ostomy mapping was not defined by S&N
        
        return vo;
    }
    
    private static String convertValue(Map<String, String> map, String val){
        String res = "";
        if (val.indexOf(',') > 0){
            String[] array = val.split(",");
            for (String string : array) {
                String part = convertValue(map, string);
                res = part.equals("0")? res: res + part + ",";
            }
            res = res.endsWith(",")? res.substring(0, res.length()-1): res;
        } else {
            res = map.get(val);
            // Comment - Sanity check for testing
            // res = (res == null)? "U": res;
            // Uncomment - real result
            res = (res == null)? "0": res;
        }
        res = res.isEmpty()? "0": res;
        return res;
    }
    
    private static final Map<String, String> discharge_reasonMap;
    private static final Map<String, String> funding_sourceMap;
    private static final Map<String, String> patient_residenceMap;
    private static final Map<String, String> co_morbidities_endocrineMap;
    private static final Map<String, String> co_morbidities_heartMap;
    private static final Map<String, String> co_morbidities_musculoeskeletalMap;
    private static final Map<String, String> co_morbidities_neurologicalMap;
    private static final Map<String, String> co_morbidities_psychiatrric_moodMap;
    private static final Map<String, String> co_morbidities_pulmonaryMap;
    private static final Map<String, String> co_morbidities_sensoryMap;
    private static final Map<String, String> co_morbidities_infectionsMap;
    private static final Map<String, String> co_morbidities_otherMap;
    private static final Map<String, String> interfering_factorsMap;
    private static final Map<String, String> interfering_medicationsMap;
    private static final Map<String, String> genderMap;
    private static final Map<String, String> albuminMap;
    private static final Map<String, String> pre_albuminMap;
    private static final Map<String, String> labs_investigationsMap;
    private static final Map<String, String> external_referralsMap;
    private static final Map<String, String> arrangedMap;
    private static final Map<String, String> basic_limb_temperatureMap;
    private static final Map<String, String> limb_edemaMap;
    private static final Map<String, String> limb_edema_severityMap;
    private static final Map<String, String> palpationMap;
    private static final Map<String, String> missing_limbsMap;
    
    private static final Map<String, String> colorMap;
    private static final Map<String, String> basic_pain_assessmentsMap;
    private static final Map<String, String> adv_pain_assessmentsMap;
    private static final Map<String, String> basic_skin_assessmentsMap;
    private static final Map<String, String> sensoryMap;
    private static final Map<String, String> proprioceptionMap;
    private static final Map<String, String> deformitiesMap;
    private static final Map<String, String> toesMap;
    private static final Map<String, String> weight_bearing_statusMap;
    private static final Map<String, String> balanceMap;
    private static final Map<String, String> calf_muscle_functionMap;
    private static final Map<String, String> mobility_aidsMap;
    private static final Map<String, String> muscle_toneMap;
    private static final Map<String, String> archesMap;
    private static final Map<String, String> foot_activeMap;
    private static final Map<String, String> foot_passiveMap;
    private static final Map<String, String> lab_doneMap;
    private static final Map<String, String> missing_upper_limbMap;
    private static final Map<String, String> upper_limb_edema_locationMap;
    private static final Map<String, String> range_of_motionMap;
    private static final Map<String, String> adv_lim_temperatureMap;
    private static final Map<String, String> patient_limitationsMap;
    private static final Map<String, String> teaching_goalsMap;
    private static final Map<String, String> pressure_ulcer_stageMap;
    private static final Map<String, String> wound_etiologyMap;
    private static final Map<String, String> ostomy_etiologyMap;
    private static final Map<String, String> type_of_ostomyMap;
    private static final Map<String, String> postop_etiologyMap;
    private static final Map<String, String> tubes_drains_etiologyMap;
    private static final Map<String, String> skin_etiologyMap;
    private static final Map<String, String> goal_of_healing_woundMap;
    private static final Map<String, String> goal_of_healing_incisionMap;
    private static final Map<String, String> goal_of_healing_ostomyMap;
    private static final Map<String, String> goal_of_healing_drainMap;
    private static final Map<String, String> goal_of_healing_skinMap;
    private static final Map<String, String> acquiredMap;
    private static final Map<String, String> c_s_resultMap;
    private static final Map<String, String> adjunctive_treatment_modalitiesMap;
    private static final Map<String, String> pressure_readingMap;
    private static final Map<String, String> initiated_byMap;
    private static final Map<String, String> negative_pressure_connectorMap;
    private static final Map<String, String> reason_for_endingMap;
    private static final Map<String, String> goal_of_therapyMap;
    private static final Map<String, String> dressing_change_frequencyMap;
    private static final Map<String, String> bnursing_vistit_frecuencyMap;
    private static final Map<String, String> vendor_nameMap;
    private static final Map<String, String> machine_acquirementMap;
    
    private static final Map<String, String> exudate_typeMap;
    private static final Map<String, String> exudate_amountMap;
    private static final Map<String, String> wound_discharge_reasonMap;
    private static final Map<String, String> wound_edgeMap;
    private static final Map<String, String> wound_baseMap;
    private static final Map<String, String> periwound_skinMap;
    private static final Map<String, String> priorityMap;
    private static final Map<String, String> postop_managementMap;
    private static final Map<String, String> incision_exudateMap;
    private static final Map<String, String> incision_statusMap;
    private static final Map<String, String> incision_discharge_reasonMap;
    private static final Map<String, String> painMap;
    private static final Map<String, String> incision_closure_typeMap;
    private static final Map<String, String> incision_exudate_amountMap;
    private static final Map<String, String> drainage_odourMap;
    private static final Map<String, String> type_of_drainMap;
    private static final Map<String, String> peri_drain_skinMap;
    private static final Map<String, String> characteristicsMap;
    private static final Map<String, String> tubes_discharge_reasonMap;
    private static final Map<String, String> drain_siteMap;
    
    private static final Map<String, String> constructionMap;
    private static final Map<String, String> drainageMap;
    private static final Map<String, String> mucocutaneous_marginMap;
    private static final Map<String, String> peri_fistula_skinMap;
    private static final Map<String, String> devicesMap;
    private static final Map<String, String> ostomy_discharge_reasonMap;
    private static final Map<String, String> concerns_for_pouchingMap;
    private static final Map<String, String> profileMap;
    private static final Map<String, String> flange_pouchMap;
    private static final Map<String, String> urine_colourMap;
    private static final Map<String, String> urine_typeMap;
    private static final Map<String, String> urine_quantityMap;
    private static final Map<String, String> skin_appearanceMap;
    private static final Map<String, String> skin_contourMap;
    private static final Map<String, String> self_care_progressMap;
    private static final Map<String, String> peri_ostomy_skinMap;
    private static final Map<String, String> stoma_shapeMap;
    private static final Map<String, String> nutritional_statusMap;
    private static final Map<String, String> statusMap;

    static {
        Map<String, String> map = new HashMap<String, String>();
        map.put("0", "0");
        map.put("3695", "1");
        map.put("3694", "2");
        map.put("3692", "3");
        map.put("3696", "4");
        discharge_reasonMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0", "0");
        map.put("3440", "1");
        map.put("3637", "2");
        map.put("3444", "3");
        map.put("3635", "4");
        map.put("3636", "5");
        map.put("3638", "6");
        map.put("3639", "7");
        funding_sourceMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0", "0");
        patient_residenceMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0", "0");
        map.put("44", "1");
        map.put("45", "2");
        map.put("898", "3");
        map.put("510", "4");
        map.put("511", "5");
        map.put("3810", "6");
        co_morbidities_endocrineMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0", "0");
        map.put("523", "1");
        map.put("535", "2");
        map.put("536", "3");
        map.put("3809", "4");
        map.put("296", "5");
        map.put("48", "6");
        map.put("51", "7");
        map.put("3697", "8");
        co_morbidities_heartMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("538","1");
        map.put("539","2");
        map.put("55","3");
        map.put("3698","4");
        map.put("3808","5");
        co_morbidities_musculoeskeletalMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("512","1");
        map.put("541","2");
        map.put("542","3");
        map.put("543","4");
        map.put("544","5");
        map.put("545","6");
        map.put("546","7");
        map.put("547","8");
        map.put("548","9");
        map.put("550","10");
        map.put("551","11");
        map.put("63","12");
        map.put("70","13");
        map.put("72","14");
        map.put("3699","15");
        map.put("3811","16");
        co_morbidities_neurologicalMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("552","1");
        map.put("553","2");
        map.put("78","3");
        map.put("80","4");
        map.put("3812","5");
        co_morbidities_psychiatrric_moodMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("82","1");
        map.put("83","2");
        map.put("3508","3");
        co_morbidities_pulmonaryMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("554","1");
        map.put("555","2");
        map.put("85","3");
        map.put("87","4");
        co_morbidities_sensoryMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("531","1");
        map.put("532","2");
        map.put("533","3");
        map.put("297","4");
        map.put("93","5");
        map.put("94","6");
        map.put("95","7");
        map.put("96","8");
        map.put("97","9");
        map.put("99","10");
        map.put("3815","11");
        map.put("3816","12");
        co_morbidities_infectionsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("528","1");
        map.put("529","2");
        map.put("831","3");
        map.put("832","4");
        map.put("833","5");
        map.put("104","6");
        map.put("105","7");
        map.put("1690","8");
        map.put("1691","9");
        map.put("3700","10");
        co_morbidities_otherMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3604","1");
        map.put("3701","2");
        map.put("3603","3");
        map.put("3602","4");
        map.put("3601","5");
        map.put("3600","6");
        map.put("3599","7");
        map.put("3598","8");
        map.put("3597","9");
        map.put("3596","10");
        map.put("3595","11");
        map.put("3594","12");
        map.put("3593","13");
        map.put("3822","14");
        map.put("3702","15");
        map.put("3590","16");
        map.put("3589","17");
        interfering_factorsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3524","1");
        map.put("777","2");
        map.put("779","3");
        map.put("780","4");
        map.put("3525","5");
        map.put("1605","6");
        map.put("3526","7");
        map.put("3527","8");
        map.put("3528","9");
        map.put("3529","10");
        map.put("3530","11");
        map.put("3531","12");
        interfering_medicationsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("130","1");
        map.put("131","2");
        genderMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("155","1");
        map.put("156","2");
        map.put("157","3");
        map.put("158","4");
        albuminMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("159","1");
        map.put("160","2");
        map.put("161","3");
        map.put("162","4");
        pre_albuminMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("178","1");
        map.put("179","2");
        map.put("180","3");
        map.put("181","4");
        map.put("182","5");
        map.put("3704","6");
        map.put("3705","7");
        map.put("3706","8");
        map.put("3707","9");
        map.put("3708","10");
        map.put("3709","11");
        map.put("3710","12");
        labs_investigationsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3616","1");
        map.put("3711","2");
        map.put("3715","3");
        map.put("3489","4");
        map.put("3713","5");
        map.put("3490","6");
        map.put("3491","7");
        map.put("3716","8");
        map.put("3497","9");
        map.put("3501","10");
        external_referralsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3718","1");
        map.put("3503","2");
        map.put("3504","3");
        map.put("3505","4");
        map.put("3719","5");
        arrangedMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3829","1");
        map.put("3828","2");
        map.put("3827","3");
        basic_limb_temperatureMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3864","1");
        map.put("3865","2");
        limb_edemaMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3856","1");
        map.put("3857","2");
        map.put("3858","3");
        map.put("3859","4");
        limb_edema_severityMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3874","1");
        map.put("3875","2");
        map.put("3876","3");
        map.put("3877","4");
        map.put("3879","5");
        palpationMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1319","1");
        map.put("3887","2");
        map.put("3888","3");
        map.put("3889","4");
        map.put("3890","5");
        map.put("3891","6");
        map.put("4074","7");
        map.put("4071","8");
        map.put("4072","9");
        map.put("4073","10");
        missing_limbsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3866","1");
        map.put("3867","2");
        map.put("3868","3");
        map.put("3869","4");
        map.put("3870","5");
        colorMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3896","1");
        map.put("3897","2");
        map.put("3906","3");
        map.put("3907","4");
        map.put("3908","5");
        map.put("3909","6");
        map.put("3910","7");
        map.put("3911","8");
        map.put("3912","9");
        map.put("3913","10");
        map.put("3914","11");
        basic_pain_assessmentsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1171","1");
        map.put("1173","2");
        map.put("1174","3");
        map.put("3322","4");
        map.put("3472","5");
        map.put("3473","6");
        map.put("1179","7");
        map.put("3314","8");
        map.put("3474","9");
        map.put("3475","10");
        map.put("3476","11");
        map.put("3584","12");
        adv_pain_assessmentsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3925","1");
        map.put("3926","2");
        map.put("3927","3");
        map.put("3382","4");
        map.put("3918","5");
        map.put("3923","6");
        map.put("3928","7");
        map.put("3366","8");
        map.put("3373","9");
        basic_skin_assessmentsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1192","1");
        map.put("3929","2");
        map.put("3940","3");
        map.put("3941","4");
        map.put("3942","5");
        map.put("3567","6");
        sensoryMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3539","1");
        map.put("3540","2");
        proprioceptionMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3939","1");
        map.put("3573","2");
        map.put("1203","3");
        map.put("3932","4");
        map.put("3933","5");
        map.put("1426","6");
        map.put("1427","7");
        deformitiesMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3945","1");
        map.put("3947","2");
        map.put("3951","3");
        map.put("3478","4");
        map.put("3380","5");
        toesMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1610","1");
        map.put("1226","2");
        map.put("3559","3");
        weight_bearing_statusMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1227","1");
        map.put("1228","2");
        balanceMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1229","1");
        map.put("1230","2");
        calf_muscle_functionMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1430","1");
        map.put("1232","2");
        map.put("1233","3");
        map.put("1234","4");
        map.put("1235","5");
        map.put("3953","6");
        mobility_aidsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1240","1");
        map.put("1241","2");
        map.put("1242","3");
        map.put("1243","4");
        muscle_toneMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1245","1");
        map.put("3959","2");
        map.put("3961","3");
        map.put("3962","4");
        map.put("3963","5");
        map.put("3964","6");
        map.put("3965","7");
        map.put("3966","8");
        map.put("3967","9");
        map.put("3968","10");
        map.put("3969","11");
        map.put("3970","12");
        archesMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3971","1");
        map.put("3972","2");
        map.put("3975","3");
        map.put("3976","4");
        map.put("3977","5");
        map.put("3978","6");
        foot_activeMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1612","1");
        map.put("1613","2");
        foot_passiveMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3981","1");
        map.put("3982","2");
        lab_doneMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3262","1");
        map.put("4076","2");
        map.put("4077","3");
        map.put("4078","4");
        map.put("4079","5");
        map.put("4080","6");
        map.put("4081","7");
        map.put("4082","8");
        map.put("4083","9");
        map.put("4084","10");
        map.put("4085","11");
        map.put("4086","12");
        map.put("4087","13");
        map.put("4088","14");
        map.put("4089","15");
        missing_upper_limbMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4103","1");
        map.put("4104","2");
        map.put("4105","3");
        map.put("4106","4");
        map.put("4107","5");
        map.put("4108","6");
        upper_limb_edema_locationMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0", "0");
        range_of_motionMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4111","1");
        map.put("4112","2");
        map.put("4113","3");
        adv_lim_temperatureMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1513","1");
        map.put("1356","2");
        map.put("4121","3");
        map.put("1045","4");
        map.put("1046","5");
        map.put("1047","6");
        map.put("1048","7");
        map.put("1049","8");
        map.put("3399","9");
        map.put("1672","10");
        map.put("4119","11");
        map.put("3400","12");
        patient_limitationsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3777","1");
        map.put("3773","2");
        teaching_goalsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3740","1");
        map.put("3741","2");
        map.put("3742","3");
        map.put("3743","4");
        map.put("3744","5");
        map.put("3746","6");
        pressure_ulcer_stageMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1673","1");
        map.put("1674","2");
        map.put("134","3");
        map.put("404","4");
        map.put("318","5");
        map.put("139","6");
        map.put("3619","7");
        map.put("3627","8");
        map.put("3628","9");
        map.put("3547","10");
        map.put("142","11");
        map.put("1273","12");
        map.put("1274","13");
        map.put("1275","14");
        map.put("1276","15");
        map.put("414","16");
        map.put("3629","17");
        map.put("3620","18");
        map.put("3630","19");
        map.put("3631","20");
        map.put("3632","21");
        map.put("417","22");
        map.put("801","23");
        map.put("135","24");
        map.put("3555","25");
        map.put("3556","26");
        wound_etiologyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1064","1");
        map.put("1065","2");
        map.put("1069","3");
        map.put("1723","4");
        map.put("1588","5");
        map.put("1614","6");
        map.put("1072","7");
        map.put("1724","8");
        map.put("1071","9");
        map.put("1067","10");
        map.put("1059","11");
        map.put("1063","12");
        map.put("1062","13");
        map.put("1075","14");
        map.put("1070","15");
        map.put("1068","16");
        map.put("1066","17");
        map.put("1073","18");
        map.put("1074","19");
        map.put("1060","20");
        map.put("1058","21");
        map.put("1076","22");
        map.put("3546","23");
        ostomy_etiologyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1539","1");
        map.put("1540","2");
        map.put("1636","3");
        map.put("1637","4");
        map.put("1010","5");
        map.put("1011","6");
        map.put("3783","7");
        map.put("3784","8");
        map.put("3785","9");
        map.put("3786","10");
        map.put("3787","11");
        map.put("3788","12");
        map.put("3789","13");
        map.put("3790","14");
        map.put("3791","15");
        map.put("3792","16");
        map.put("3793","17");
        map.put("3794","18");
        type_of_ostomyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1617","1");
        map.put("1618","2");
        map.put("1367","3");
        map.put("1711","4");
        map.put("3401","5");
        map.put("1632","6");
        map.put("1633","7");
        map.put("1628","8");
        map.put("1615","9");
        map.put("1630","10");
        map.put("1616","11");
        map.put("1629","12");
        map.put("1631","13");
        map.put("1634","14");
        map.put("3545","15");
        postop_etiologyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1368","1");
        map.put("1422","2");
        map.put("1635","3");
        map.put("1656","4");
        map.put("1749","5");
        tubes_drains_etiologyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3332","1");
        map.put("3543","2");
        map.put("3544","3");
        map.put("3735","4");
        map.put("3736","5");
        map.put("3737","6");
        map.put("3738","7");
        map.put("3739","8");
        skin_etiologyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3747","1");
        map.put("3748","2");
        map.put("3753","3");
        map.put("3755","4");
        //map.put("1081","1081"); // Heal the wound - Commented out
        goal_of_healing_woundMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3758","1");
        map.put("3759","2");
        map.put("3760","3");
        goal_of_healing_incisionMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1425","1");
        map.put("3762","2");
        goal_of_healing_ostomyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3765","1");
        map.put("3763","2");
        map.put("3764","3");
        map.put("3766","4");
        goal_of_healing_drainMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3338","1");
        map.put("3771","2");
        map.put("3768","3");
        map.put("3772","4");
        goal_of_healing_skinMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4432","1");
        map.put("4433","2");
        acquiredMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("163","1");
        map.put("164","2");
        map.put("165","3");
        map.put("166","4");
        map.put("167","5");
        map.put("168","6");
        map.put("169","7");
        map.put("1712","8");
        c_s_resultMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("183","1");
        map.put("184","2");
        map.put("185","3");
        map.put("186","4");
        map.put("187","5");
        map.put("4124","6");
        map.put("4125","7");
        map.put("4123","8");
        adjunctive_treatment_modalitiesMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("189","1");
        map.put("190","2");
        map.put("191","3");
        map.put("192","4");
        map.put("193","5");
        map.put("194","6");
        pressure_readingMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4126","1");
        map.put("4127","2");
        map.put("4128","3");
        map.put("3387","4");
        initiated_byMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3252","1");
        map.put("3253","2");
        map.put("4421","3");
        negative_pressure_connectorMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4132","1");
        map.put("4129","2");
        map.put("4133","3");
        map.put("4136","4");
        map.put("4137","5");
        map.put("3390","6");
        reason_for_endingMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4418","1");
        map.put("3259","2");
        map.put("3260","3");
        map.put("3261","4");
        map.put("3388","5");
        goal_of_therapyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("886","1");
        map.put("233","2");
        map.put("234","3");
        map.put("236","4");
        map.put("237","5");
        map.put("238","6");
        map.put("239","7");
        map.put("240","8");
        map.put("241","9");
        dressing_change_frequencyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("887","1");
        map.put("243","2");
        map.put("244","3");
        map.put("246","4");
        map.put("247","5");
        map.put("248","6");
        map.put("249","7");
        map.put("250","8");
        map.put("251","9");
        bnursing_vistit_frecuencyMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3328","1");
        map.put("3327","2");
        map.put("4151","3");
        map.put("4152","4");
        map.put("4153","5");
        map.put("4154","6");
        map.put("4155","7");
        map.put("4156","8");
        map.put("4157","9");
        vendor_nameMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3329","1");
        map.put("3330","2");
        map.put("4158","3");
        machine_acquirementMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4435","1");
        map.put("4437","2");
        map.put("4438","3");
        map.put("4436","4");
        exudate_typeMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("225","1");
        map.put("4439","2");
        map.put("4255","3");
        map.put("4434","4");
        exudate_amountMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4174","1");
        map.put("4180","2");
        map.put("854","3");
        map.put("4175","4");
        map.put("4177","5");
        map.put("4181","6");
        wound_discharge_reasonMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4196","1");
        map.put("4199","2");
        map.put("4200","3");
        map.put("4202","4");
        map.put("4204","5");
        map.put("4207","6");
        map.put("4208","7");
        wound_edgeMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4264","1");
        map.put("4263","2");
        map.put("4214","3");
        map.put("4267","4");
        map.put("4268","5");
        map.put("4269","6");
        wound_baseMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4243","1");
        map.put("4244","2");
        map.put("4245","3");
        map.put("4246","4");
        map.put("4247","5");
        map.put("4248","6");
        map.put("4249","7");
        map.put("4250","8");
        map.put("4251","9");
        map.put("1572","10");
        periwound_skinMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("3447","1");
        map.put("3448","2");
        map.put("4252","3");
        priorityMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4279","1");
        map.put("4280","2");
        map.put("4281","3");
        postop_managementMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4285","1");
        map.put("4286","2");
        map.put("4287","3");
        map.put("4288","4");
        incision_exudateMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4319","1");
        map.put("4320","2");
        map.put("4321","3");
        map.put("4322","4");
        incision_statusMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4325","1");
        map.put("4329","2");
        map.put("4330","3");
        map.put("4331","4");
        map.put("4332","5");
        map.put("4333","6");
        incision_discharge_reasonMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1113","0");
        map.put("1114","1");
        map.put("1115","2");
        map.put("1116","3");
        map.put("1117","4");
        map.put("1118","5");
        map.put("1119","6");
        map.put("1120","7");
        map.put("1121","8");
        map.put("1122","9");
        map.put("1123","10");
        painMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1126","1");
        map.put("1324","2");
        map.put("4335","3");
        map.put("4336","4");
        incision_closure_typeMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1128","1");
        map.put("1328","2");
        map.put("4343","3");
        map.put("4344","4");
        incision_exudate_amountMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4345","1");
        map.put("4346","2");
        drainage_odourMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1097","1");
        map.put("1360","2");
        map.put("1623","3");
        map.put("1624","4");
        map.put("1688","5");
        map.put("1722","6");
        map.put("1748","7");
        map.put("1357","8");
        map.put("1358","9");
        map.put("1703","10");
        map.put("1359","11");
        map.put("1619","12");
        map.put("1620","13");
        map.put("4347","14");
        map.put("4348","15");
        map.put("1747","16");
        map.put("1625","17");
        map.put("1720","18");
        map.put("1718","19");
        map.put("4349","20");
        map.put("4350","21");
        map.put("4351","22");
        type_of_drainMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4362","1");
        map.put("4363","2");
        map.put("4364","3");
        map.put("4365","4");
        map.put("4366","5");
        map.put("4367","6");
        map.put("4368","7");
        map.put("4369","8");
        map.put("4370","9");
        map.put("4371","10");
        peri_drain_skinMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("4383","1");
        map.put("4384","2");
        map.put("4385","3");
        map.put("4386","4");
        map.put("4387","5");
        map.put("4388","6");
        map.put("4389","7");
        characteristicsMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0", "0");
        tubes_discharge_reasonMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("0","0");
        map.put("1129","1");
        map.put("1365","2");
        map.put("1366","3");
        drain_siteMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        constructionMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        drainageMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        mucocutaneous_marginMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        peri_fistula_skinMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        devicesMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        ostomy_discharge_reasonMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        concerns_for_pouchingMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        profileMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        flange_pouchMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        urine_colourMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        urine_typeMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        urine_quantityMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        skin_appearanceMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        skin_contourMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        self_care_progressMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        peri_ostomy_skinMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        stoma_shapeMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        nutritional_statusMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");
        statusMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();
        map.put("", "one");


    }
}
