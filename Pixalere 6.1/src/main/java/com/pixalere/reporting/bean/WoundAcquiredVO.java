/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
/**
 *
 * @author travis
 */
public class WoundAcquiredVO {
    private Integer locationId;
    private Integer etiologyId;
    private String name;
    private Integer external;
    private Integer internal;
    private Integer not_defined;
    private Integer total;
    private Integer new_external;
    private Integer new_internal;
    private Integer new_not_defined;
    private Integer new_total;
    private String grouping;
    
    public WoundAcquiredVO(){}
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the external
     */
    public Integer getExternal() {
        return external;
    }
    /**
     * @param external the external to set
     */
    public void setExternal(Integer external) {
        this.external = external;
    }
    /**
     * @return the internal
     */
    public Integer getInternal() {
        return internal;
    }
    /**
     * @param internal the internal to set
     */
    public void setInternal(Integer internal) {
        this.internal = internal;
    }
    /**
     * @return the total
     */
    public Integer getTotal() {
        return total;
    }
    /**
     * @param total the total to set
     */
    public void setTotal(Integer total) {
        this.total = total;
    }
    /**
     * @return the new_external
     */
    public Integer getNew_external() {
        return new_external;
    }
    /**
     * @param new_external the new_external to set
     */
    public void setNew_external(Integer new_external) {
        this.new_external = new_external;
    }
    /**
     * @return the new_internal
     */
    public Integer getNew_internal() {
        return new_internal;
    }
    /**
     * @param new_internal the new_internal to set
     */
    public void setNew_internal(Integer new_internal) {
        this.new_internal = new_internal;
    }
    /**
     * @return the new_total
     */
    public Integer getNew_total() {
        return new_total;
    }
    /**
     * @param new_total the new_total to set
     */
    public void setNew_total(Integer new_total) {
        this.new_total = new_total;
    }
    /**
     * @return the grouping
     */
    public String getGrouping() {
        return grouping;
    }
    /**
     * @param grouping the grouping to set
     */
    public void setGrouping(String grouping) {
        this.grouping = grouping;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getEtiologyId() {
        return etiologyId;
    }

    public void setEtiologyId(Integer etiologyId) {
        this.etiologyId = etiologyId;
    }

    public Integer getNot_defined() {
        return not_defined;
    }

    public void setNot_defined(Integer not_defined) {
        this.not_defined = not_defined;
    }

    public Integer getNew_not_defined() {
        return new_not_defined;
    }

    public void setNew_not_defined(Integer new_not_defined) {
        this.new_not_defined = new_not_defined;
    }
    
    
    
}
