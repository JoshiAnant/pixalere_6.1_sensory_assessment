/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
import java.io.Serializable;
/**
 *
 * @author travis
 */
public class ReportQueueTreatmentsVO   extends ValueObject implements Serializable {
    private Integer id;
    private Integer treatment_location_id;
    private Integer report_id;
    public ReportQueueTreatmentsVO(){}
    
    public ReportQueueTreatmentsVO(Integer report_id){this.report_id=report_id;}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the treatment_location_id
     */
    public Integer getTreatment_location_id() {
        return treatment_location_id;
    }
    /**
     * @param treatment_location_id the treatment_location_id to set
     */
    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }
    /**
     * @return the report_id
     */
    public Integer getReport_id() {
        return report_id;
    }
    /**
     * @param report_id the report_id to set
     */
    public void setReport_id(Integer report_id) {
        this.report_id = report_id;
    }
}
