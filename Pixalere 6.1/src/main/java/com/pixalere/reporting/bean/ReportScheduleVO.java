package com.pixalere.reporting.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author Jose
 */
public class ReportScheduleVO extends ValueObject implements Serializable {
    private Integer active;
    private Date created_on;
    
    private String report_type;
    private Integer user_language;
    private String custom_title;
    
    private String recipients_emails;
    private Integer enable_reminder;
    private String reminder_emails;
    private Integer reminder_limit;

    // Custom Dashboard attributes
    private Integer use_title;
    private Integer display_locations;
    
    // Display attributes
    private String locationsDisplay;
    private String nextDateDisplay;
    private String remainingDisplay;
    
    private Collection<ScheduleLocationVO> locations;
    private Collection<ScheduleEtiologyVO> etiologies;
    private Collection<ScheduleConfigVO> configs;

    public ReportScheduleVO() {
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }

    public Integer getUser_language() {
        return user_language;
    }

    public void setUser_language(Integer user_language) {
        this.user_language = user_language;
    }

    public String getCustom_title() {
        return custom_title;
    }

    public void setCustom_title(String custom_title) {
        this.custom_title = custom_title;
    }

    public String getRecipients_emails() {
        return recipients_emails;
    }

    public void setRecipients_emails(String recipients_emails) {
        this.recipients_emails = recipients_emails;
    }

    public Integer getEnable_reminder() {
        return enable_reminder;
    }

    public void setEnable_reminder(Integer enable_reminder) {
        this.enable_reminder = enable_reminder;
    }

    public String getReminder_emails() {
        return reminder_emails;
    }

    public void setReminder_emails(String reminder_emails) {
        this.reminder_emails = reminder_emails;
    }

    public Integer getReminder_limit() {
        return reminder_limit;
    }

    public void setReminder_limit(Integer reminder_limit) {
        this.reminder_limit = reminder_limit;
    }
    
    public Integer getDisplay_locations() {
        return display_locations;
    }

    public void setDisplay_locations(Integer display_locations) {
        this.display_locations = display_locations;
    }

    public Integer getUse_title() {
        return use_title;
    }

    public void setUse_title(Integer use_title) {
        this.use_title = use_title;
    }

    public Collection<ScheduleLocationVO> getLocations() {
        return locations;
    }

    public void setLocations(Collection<ScheduleLocationVO> locations) {
        this.locations = locations;
    }

    public String getLocationsDisplay() {
        return locationsDisplay;
    }

    public void setLocationsDisplay(String locationsDisplay) {
        this.locationsDisplay = locationsDisplay;
    }

    public String getNextDateDisplay() {
        return nextDateDisplay;
    }

    public void setNextDateDisplay(String nextDateDisplay) {
        this.nextDateDisplay = nextDateDisplay;
    }

    public String getRemainingDisplay() {
        return remainingDisplay;
    }

    public void setRemainingDisplay(String remainingDisplay) {
        this.remainingDisplay = remainingDisplay;
    }

    public Collection<ScheduleConfigVO> getConfigs() {
        return configs;
    }

    public void setConfigs(Collection<ScheduleConfigVO> configs) {
        this.configs = configs;
    }

    public Collection<ScheduleEtiologyVO> getEtiologies() {
        return etiologies;
    }

    public void setEtiologies(Collection<ScheduleEtiologyVO> etiologies) {
        this.etiologies = etiologies;
    }
    
}
