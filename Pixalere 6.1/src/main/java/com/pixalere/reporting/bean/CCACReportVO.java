/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
import java.util.Date;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.utils.Common;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.bean.WoundVO;
/**
 *
 * @author travis
 */
public class CCACReportVO  extends ValueObject  {
    private Integer id;
    private Integer active;
    private Integer patient_id;
    private Integer wound_id;
    private Integer wound_profile_id;
    private Integer patient_profile_id;
    private Integer limb_assessment_id;
    private Integer professional_id;
    private Date time_stamp;
    private Date date_of_transfer;
    private String change_in_wound;
    private String wound_change_comments;
    private String wound_progress;
    private String suitable_transfer;
    private String comments;
    private String discharge;
    private String wound_status;
    private String type_of_report;
    private Integer assessment_id;
    private Integer alpha_id;
    private Integer already_retrieved;
    private String correct_pathway_confirmed;
    private Integer week;
    private Integer patient_account_id;
    private WoundVO wound;
    private PatientAccountVO patient;
    private WoundAssessmentLocationVO alpha;
    private ProfessionalVO professional;
    public CCACReportVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }
    /**
     * @return the patient_id
     */
    public Integer getPatient_id() {
        return patient_id;
    }
    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }
    /**
     * @return the wound_id
     */
    public Integer getWound_id() {
        return wound_id;
    }
    /**
     * @param wound_id the wound_id to set
     */
    public void setWound_id(Integer wound_id) {
        this.wound_id = wound_id;
    }
    /**
     * @return the professional_id
     */
    public Integer getProfessional_id() {
        return professional_id;
    }
    /**
     * @param professional_id the professional_id to set
     */
    public void setProfessional_id(Integer professional_id) {
        this.professional_id = professional_id;
    }
    /**
     * @return the time_stamp
     */
    public Date getTime_stamp() {
        return time_stamp;
    }
    /**
     * @param time_stamp the time_stamp to set
     */
    public void setTime_stamp(Date time_stamp) {
        this.time_stamp = time_stamp;
    }
    /**
     * @return the date_of_transfer
     */
    public Date getDate_of_transfer() {
        return date_of_transfer;
    }
    /**
     * @param date_of_transfer the date_of_transfer to set
     */
    public void setDate_of_transfer(Date date_of_transfer) {
        this.date_of_transfer = date_of_transfer;
    }
    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }
    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }
    /**
     * @return the discharge
     */
    public String getDischarge() {
        return discharge;
    }
    /**
     * @param discharge the discharge to set
     */
    public void setDischarge(String discharge) {
        this.discharge = discharge;
    }
    /**
     * @return the wound_status
     */
    public String getWound_status() {
        return wound_status;
    }
    /**
     * @param wound_status the wound_status to set
     */
    public void setWound_status(String wound_status) {
        this.wound_status = wound_status;
    }
    /**
     * @return the type_of_report
     */
    public String getType_of_report() {
        return type_of_report;
    }
    /**
     * @param type_of_report the type_of_report to set
     */
    public void setType_of_report(String type_of_report) {
        this.type_of_report = type_of_report;
    }
  
    /**
     * @return the assessment_id
     */
    public Integer getAssessment_id() {
        return assessment_id;
    }
    /**
     * @param assessment_id the assessment_id to set
     */
    public void setAssessment_id(Integer assessment_id) {
        this.assessment_id = assessment_id;
    }
    /**
     * @return the alpha_id
     */
    public Integer getAlpha_id() {
        return alpha_id;
    }
    /**
     * @param alpha_id the alpha_id to set
     */
    public void setAlpha_id(Integer alpha_id) {
        this.alpha_id = alpha_id;
    }
    /**
     * @return the already_retrieved
     */
    public Integer getAlready_retrieved() {
        return already_retrieved;
    }
    /**
     * @param already_retrieved the already_retrieved to set
     */
    public void setAlready_retrieved(Integer already_retrieved) {
        this.already_retrieved = already_retrieved;
    }
    /**
     * @return the patient_account_id
     */
    public Integer getPatient_account_id() {
        return patient_account_id;
    }
    /**
     * @param patient_account_id the patient_account_id to set
     */
    public void setPatient_account_id(Integer patient_account_id) {
        this.patient_account_id = patient_account_id;
    }
    /**
     * @return the patient
     */
    public PatientAccountVO getPatient() {
        return patient;
    }
    /**
     * @param patient the patient to set
     */
    public void setPatient(PatientAccountVO patient) {
        this.patient = patient;
    }
    /**
     * @return the alpha
     */
    public WoundAssessmentLocationVO getAlpha() {
        return alpha;
    }
    /**
     * @param alpha the alpha to set
     */
    public void setAlpha(WoundAssessmentLocationVO alpha) {
        this.alpha = alpha;
    }

    /**
     * @return the correct_pathway_confirmed
     */
    public String getCorrect_pathway_confirmed() {
        return correct_pathway_confirmed;
    }
    /**
     * @param correct_pathway_confirmed the correct_pathway_confirmed to set
     */
    public void setCorrect_pathway_confirmed(String correct_pathway_confirmed) {
        this.correct_pathway_confirmed = correct_pathway_confirmed;
    }
    /**
     * @return the week
     */
    public Integer getWeek() {
        return week;
    }
    /**
     * @param week the week to set
     */
    public void setWeek(Integer week) {
        this.week = week;
    }
    public String getAlphaText(String locale){
        String display_alpha = getAlpha().getAlpha();
        if (display_alpha != null) {
            if (display_alpha.length() > 0) {

                if (display_alpha.length() == 1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.woundcare", locale) + " " + display_alpha;
                } else if (display_alpha.indexOf("ostu") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.urinalstoma", locale);
                } else if (display_alpha.indexOf("ostf") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.fecalstoma", locale);
                } else if (display_alpha.indexOf("td") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.drain", locale) + " " + display_alpha.substring(2);

                } else if (display_alpha.indexOf("s") == 0) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.skin", locale) + " " + display_alpha.substring(1);
                } else if (display_alpha.indexOf("tag") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.incision_tag", locale) + " " + display_alpha.substring(3);
                } else if (display_alpha.indexOf("burn") != -1) {
                    display_alpha = Common.getLocalizedString("pixalere.woundprofile.drawingiconhint.burn", locale);

                }

            }

        }
        return display_alpha;
    }
    /**
     * @return the professional
     */
    public ProfessionalVO getProfessional() {
        return professional;
    }
    /**
     * @param professional the professional to set
     */
    public void setProfessional(ProfessionalVO professional) {
        this.professional = professional;
    }
    /**
     * @return the wound_profile_id
     */
    public Integer getWound_profile_id() {
        return wound_profile_id;
    }
    /**
     * @param wound_profile_id the wound_profile_id to set
     */
    public void setWound_profile_id(Integer wound_profile_id) {
        this.wound_profile_id = wound_profile_id;
    }
    /**
     * @return the patient_profile_id
     */
    public Integer getPatient_profile_id() {
        return patient_profile_id;
    }
    /**
     * @param patient_profile_id the patient_profile_id to set
     */
    public void setPatient_profile_id(Integer patient_profile_id) {
        this.patient_profile_id = patient_profile_id;
    }
    /**
     * @return the limb_assessment_id
     */
    public Integer getLimb_assessment_id() {
        return limb_assessment_id;
    }
    /**
     * @param limb_assessment_id the limb_assessment_id to set
     */
    public void setLimb_assessment_id(Integer limb_assessment_id) {
        this.limb_assessment_id = limb_assessment_id;
    }
    /**
     * @return the wound
     */
    public WoundVO getWound() {
        return wound;
    }
    /**
     * @param wound the wound to set
     */
    public void setWound(WoundVO wound) {
        this.wound = wound;
    }
    /**
     * @return the suitable_transfer
     */
    public String getSuitable_transfer() {
        return suitable_transfer;
    }
    /**
     * @param suitable_transfer the suitable_transfer to set
     */
    public void setSuitable_transfer(String suitable_transfer) {
        this.suitable_transfer = suitable_transfer;
    }
    /**
     * @return the change_in_wound
     */
    public String getChange_in_wound() {
        return change_in_wound;
    }
    /**
     * @param change_in_wound the change_in_wound to set
     */
    public void setChange_in_wound(String change_in_wound) {
        this.change_in_wound = change_in_wound;
    }
    /**
     * @return the wound_change_comments
     */
    public String getWound_change_comments() {
        return wound_change_comments;
    }
    /**
     * @param wound_change_comments the wound_change_comments to set
     */
    public void setWound_change_comments(String wound_change_comments) {
        this.wound_change_comments = wound_change_comments;
    }
    /**
     * @return the wound_progress
     */
    public String getWound_progress() {
        return wound_progress;
    }
    /**
     * @param wound_progress the wound_progress to set
     */
    public void setWound_progress(String wound_progress) {
        this.wound_progress = wound_progress;
    }
}