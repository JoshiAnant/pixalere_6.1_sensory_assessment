/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
import java.util.Collection;
import com.pixalere.reporting.bean.DashboardReportVO;
/**
 * A persistence bean for the table dashboard_report.  Refer to
 * @link{com.pixalere.reporting.service.ReportingServiceImpl} for methods which utilize
 * this bean.
 * This table keeps track of each user's dashboard configuration.
 *
 * @since 5.0
 * @author travis
 */
public class DashboardReportVO  extends ValueObject{
    
    private Integer id;
    private String email;
    private String wound_type;
    private Integer etiology_all;
    private Collection<DashboardByLocationVO> treatmentLocations;
    private Collection<DashboardByEtiologyVO> etiologies;
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * @return the wound_type
     */
    public String getWound_type() {
        return wound_type;
    }
    /**
     * @param wound_type the wound_type to set
     */
    public void setWound_type(String wound_type) {
        this.wound_type = wound_type;
    }
    /**
     * @return the etiology_all
     */
    public Integer getEtiology_all() {
        return etiology_all;
    }
    /**
     * @param etiology_all the etiology_all to set
     */
    public void setEtiology_all(Integer etiology_all) {
        this.etiology_all = etiology_all;
    }
    /**
     * @return the treatmentLocations
     */
    public Collection<DashboardByLocationVO> getTreatmentLocations() {
        return treatmentLocations;
    }
    /**
     * @param treatmentLocations the treatmentLocations to set
     */
    public void setTreatmentLocations(Collection<DashboardByLocationVO> treatmentLocations) {
        this.treatmentLocations = treatmentLocations;
    }
    /**
     * @return the etiologies
     */
    public Collection<DashboardByEtiologyVO> getEtiologies() {
        return etiologies;
    }
    /**
     * @param etiologies the etiologies to set
     */
    public void setEtiologies(Collection<DashboardByEtiologyVO> etiologies) {
        this.etiologies = etiologies;
    }

}
