/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import java.util.Comparator;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Locale;
import com.pixalere.common.ValueObject;
import java.util.List;
/**
 *
 * @author travis
 */
public class SummaryVO  extends ValueObject implements Comparator {
    private String page;
    private String title;
    private String value;
    private Integer order;
    private List<WoundPhotosVO> photos;
    public SummaryVO(String page, String title, String value, Integer order,List<WoundPhotosVO> photos) {
        this.page = page;
        this.title = title;
        this.value = value;
        this.order = order;
        this.photos=photos;
    }
    public SummaryVO(String page, String title, String value, Integer order) {
        this.page = page;
        this.title = title;
        this.value = value;
        this.order = order;
    }
    public void setPage(String page) {
        this.page = page;
    }
    public String getPage() {
        return page;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
    public void setOrder(Integer order) {
        this.order = order;
    }
    public Integer getOrder() {
        return order;
    }
    public void setPhotos(List<WoundPhotosVO> photos){
        this.photos=photos;
    }
    public List<WoundPhotosVO> getPhotos(){
        return photos;
    }
    
    public int compare(Object o1, Object o2) {
        SummaryVO ref1 = (SummaryVO) o1;
        SummaryVO ref2 = (SummaryVO) o2;
        if (!ref1.getPage().equals(ref2.getPage())) {
            return ref1.getPage().compareTo(ref2.getPage());
        } else {
            return ref1.getOrder().compareTo(ref2.getOrder());
        }

    }
    public boolean equals(Object o) {
        return true;
    }
    
}
