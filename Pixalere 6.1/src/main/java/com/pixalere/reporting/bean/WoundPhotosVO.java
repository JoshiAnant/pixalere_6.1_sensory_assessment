/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import java.util.Comparator;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Locale;
import com.pixalere.common.ValueObject;
import java.util.List;
/**
 * 
 * @author travis
 */
public class WoundPhotosVO   extends ValueObject implements Comparator {
    private String title;
    private String photo;
    public WoundPhotosVO (String title, String photo){
        this.photo=photo;
        this.title=title;
    }
    public String getTitle(){
        return title;
    }
    
    public String getPhoto(){
        return photo;
    }
    public void setTitle(String title){
        this.title=title;
    }
    public void setPhoto(String photo){
        this.photo=photo;
    }
   
     public int compare(Object o1, Object o2) {
        WoundPhotosVO ref1 = (WoundPhotosVO) o1;
        WoundPhotosVO ref2 = (WoundPhotosVO) o2;
        if (!ref1.getTitle().equals(ref2.getTitle())) {
            return ref1.getTitle().compareTo(ref2.getTitle());
        } else {
            return ref1.getPhoto().compareTo(ref2.getPhoto());
        }

    }
    public boolean equals(Object o) {
        return true;
    }
}
