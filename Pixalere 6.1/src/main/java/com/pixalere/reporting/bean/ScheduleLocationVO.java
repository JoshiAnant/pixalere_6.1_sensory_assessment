package com.pixalere.reporting.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 *
 * @author Jose
 */
public class ScheduleLocationVO extends ValueObject implements Serializable {
    private Integer treatment_location_id;
    private Integer report_schedule_id;

    public ScheduleLocationVO() {
    }

    public ScheduleLocationVO(Integer treatment_location_id, Integer report_schedule_id) {
        this.treatment_location_id = treatment_location_id;
        this.report_schedule_id = report_schedule_id;
    }

    public Integer getTreatment_location_id() {
        return treatment_location_id;
    }

    public void setTreatment_location_id(Integer treatment_location_id) {
        this.treatment_location_id = treatment_location_id;
    }

    public Integer getReport_schedule_id() {
        return report_schedule_id;
    }

    public void setReport_schedule_id(Integer report_schedule_id) {
        this.report_schedule_id = report_schedule_id;
    }
    
}
