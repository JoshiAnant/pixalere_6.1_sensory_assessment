/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
import com.pixalere.reporting.bean.*;
import java.util.Collection;
import java.util.Date;
import java.io.Serializable;
/**
 * A Peristance bean for the table dashboard_queue
 * Refer to {@link com.pixalere.reporting.service.ReportingServiceImpl } for the calls
 *     which utilize this bean.
 * @since 5.1
 * @author travis morris
 */
public class ReportQueueVO  extends ValueObject implements Serializable {
    
    private Integer id;
    private Integer active;
    private String report_type;
    private Collection<ReportQueueEtiologyVO> etiologies;
    private Collection<ReportQueueTreatmentsVO> treatments;
    private Collection<ReportQueueWoundTypeVO> types;
    private Date start_date;
    private Date created_on;
    private Date end_date;
    private String email;
    private String extra_filter;
    private Integer user_language;
    private Integer display_locations;
    private String custom_title;

    public ReportQueueVO(){}
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the report_type
     */
    public String getReport_type() {
        return report_type;
    }
    /**
     * @param report_type the report_type to set
     */
    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }
    /**
     * @return the etiologies
     */
    public Collection<ReportQueueEtiologyVO> getEtiologies() {
        return etiologies;
    }
    /**
     * @param etiologies the etiologies to set
     */
    public void setEtiologies(Collection<ReportQueueEtiologyVO> etiologies) {
        this.etiologies = etiologies;
    }
    /**
     * @return the treatments
     */
    public Collection<ReportQueueTreatmentsVO> getTreatments() {
        return treatments;
    }
    /**
     * @param treatments the treatments to set
     */
    public void setTreatments(Collection<ReportQueueTreatmentsVO> treatments) {
        this.treatments = treatments;
    }
    /**
     * @return the types
     */
    public Collection<ReportQueueWoundTypeVO> getTypes() {
        return types;
    }
    /**
     * @param types the types to set
     */
    public void setTypes(Collection<ReportQueueWoundTypeVO> types) {
        this.types = types;
    }
    /**
     * @return the start_date
     */
    public Date getStart_date() {
        return start_date;
    }
    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }
    /**
     * @return the end_date
     */
    public Date getEnd_date() {
        return end_date;
    }
    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
    /**
     * @return the timestamp
     */
    public Date getCreated_on() {
        return created_on;
    }
    /**
     * @param timestamp the timestamp to set
     */
    public void setCreated_on(Date timestamp) {
        this.created_on = timestamp;
    }
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the extra_filter
     */
    public String getExtra_filter() {
        return extra_filter;
    }

    /**
     * @param extra_filter the extra_filter to set
     */
    public void setExtra_filter(String extra_filter) {
        this.extra_filter = extra_filter;
    }

    /**
     * @return the active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return the user_language
     */
    public Integer getUser_language() {
        return user_language;
    }

    /**
     * @param user_language the user_language to set
     */
    public void setUser_language(Integer user_language) {
        this.user_language = user_language;
    }

    public Integer getDisplay_locations() {
        return display_locations;
    }

    public void setDisplay_locations(Integer display_locations) {
        this.display_locations = display_locations;
    }

    public String getCustom_title() {
        return custom_title;
    }

    public void setCustom_title(String custom_title) {
        this.custom_title = custom_title;
    }
    
}
