/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import java.util.Comparator;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Locale;
import com.pixalere.common.ValueObject;
/**
 *
 * @author travis
 */
public class DashboardVO  extends ValueObject implements Comparator {
    private String reportName;
    private String category;
    private String quarter1="";
    private String quarter2="";
    private String quarter3="";
    private String quarter4="";
    private String quarter5="";
    private String quarter6="";
    private String quarter7="";
    private String quarter8="";
    private String lastYear="";
    private String yearBeforeLast="";
    private String name;
    private Integer orderby = 1;
    private String format;
    private boolean rightAlign = false;
    public DashboardVO() {
    }
    /**
     * @return the reportName
     */
    public String getReportName() {
        return reportName;
    }
    /**
     * @param reportName the reportName to set
     */
    public void setReportName(String reportName) {
        this.reportName = reportName;
    }
    
    /**
     * @return the orderby
     */
    public Integer getOrderby() {
        return orderby;
    }
    /**
     * @param orderby the orderby to set
     */
    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    public int compare(Object o1, Object o2) {
        DashboardVO ref1 = (DashboardVO) o1;
        DashboardVO ref2 = (DashboardVO) o2;
        if (!ref1.getReportName().equals(ref2.getReportName())) {
            return ref1.getReportName().compareTo(ref2.getReportName());
        } else {
            return ref1.getOrderby().compareTo(ref2.getOrderby());
        }

    }
    public boolean equals(Object o) {
        return true;
    }
    
    /**
     * @return the format
     */
    public String getFormat() {
        return format;
    }
    /**
     * @param format the format to set
     */
    public void setFormat(String format) {
        this.format = format;
    }
    
    /**
    * Initializes the quarterN attributes to '0' when empty.
    */
    public void ifEmptyQuartersSetToZero(){
        if (getQuarter1().equals("")) {
            setQuarter1("0.0");
        }
        if (getQuarter2().equals("")) {
            setQuarter2("0.0");
        }
        if (getQuarter3().equals("")) {
            setQuarter3("0.0");
        }
        if (getQuarter4().equals("")) {
            setQuarter4("0.0");
        }
        if (getQuarter5().equals("")) {
            setQuarter5("0.0");
        }
        if (getQuarter6().equals("")) {
            setQuarter6("0.0");
        }
        if (getQuarter7().equals("")) {
            setQuarter7("0.0");
        }
        if (getQuarter8().equals("")) {
            setQuarter8("0.0");
        }
    }
    
    public void format() {
        try{
        if (getFormat().equals("currency")) {
            NumberFormat df = NumberFormat.getCurrencyInstance(Locale.US);
            df.setMaximumFractionDigits(2);
            setQuarter1( (getQuarter1() != null && !getQuarter1().isEmpty()? df.format(new Double(getQuarter1().trim())) : "" ));
            setQuarter2( (getQuarter2() != null && !getQuarter2().isEmpty()? df.format(new Double(getQuarter2().trim())) : "" ));
            setQuarter3( (getQuarter3() != null && !getQuarter3().isEmpty()? df.format(new Double(getQuarter3().trim())) : "" ));
            setQuarter4( (getQuarter4() != null && !getQuarter4().isEmpty()? df.format(new Double(getQuarter4().trim())) : "" ));
            setQuarter5( (getQuarter5() != null && !getQuarter5().isEmpty()? df.format(new Double(getQuarter5().trim())) : "" ));
            setQuarter6( (getQuarter6() != null && !getQuarter6().isEmpty()? df.format(new Double(getQuarter6().trim())) : "" ));
            setQuarter7( (getQuarter7() != null && !getQuarter7().isEmpty()? df.format(new Double(getQuarter7().trim())) : "" ));
            setQuarter8( (getQuarter8() != null && !getQuarter8().isEmpty()? df.format(new Double(getQuarter8().trim())) : "" ));
            setLastYear( (getLastYear() != null && !getLastYear().isEmpty()? df.format(new Double(getLastYear().trim())) : "" ));
            setYearBeforeLast( ((getYearBeforeLast() != null && !getYearBeforeLast().isEmpty() )? df.format(new Double(getYearBeforeLast().trim())) : "" ));
        } else if (getFormat().equals("float")) {
            NumberFormat df = DecimalFormat.getInstance(Locale.US);
            df.setMaximumFractionDigits(2);
            setQuarter1( (getQuarter1() != null && !getQuarter1().isEmpty()? df.format(new Double(getQuarter1().trim())) : "" ));
            setQuarter2( (getQuarter2() != null && !getQuarter2().isEmpty()? df.format(new Double(getQuarter2().trim())) : "" ));
            setQuarter3( (getQuarter3() != null && !getQuarter3().isEmpty()? df.format(new Double(getQuarter3().trim())) : "" ));
            setQuarter4( (getQuarter4() != null && !getQuarter4().isEmpty()? df.format(new Double(getQuarter4().trim())) : "" ));
            setQuarter5( (getQuarter5() != null && !getQuarter5().isEmpty()? df.format(new Double(getQuarter5().trim())) : "" ));
            setQuarter6( (getQuarter6() != null && !getQuarter6().isEmpty()? df.format(new Double(getQuarter6().trim())) : "" ));
            setQuarter7( (getQuarter7() != null && !getQuarter7().isEmpty()? df.format(new Double(getQuarter7().trim())) : "" ));
            setQuarter8( (getQuarter8() != null && !getQuarter8().isEmpty()? df.format(new Double(getQuarter8().trim())) : "" ));
            setLastYear( (getLastYear() != null && !getLastYear().isEmpty()? df.format(new Double(getLastYear().trim())) : "" ));
            setYearBeforeLast( ((getYearBeforeLast() != null && !getYearBeforeLast().isEmpty() )? df.format(new Double(getYearBeforeLast().trim())) : "" ));
        } else if (getFormat().equals("integer")) {
            NumberFormat df = NumberFormat.getIntegerInstance(Locale.US);
            df.setMaximumFractionDigits(0);
            setQuarter1( (getQuarter1() != null && !getQuarter1().isEmpty()? df.format(new Double(getQuarter1().trim())) : "" ));
            setQuarter2( (getQuarter2() != null && !getQuarter2().isEmpty()? df.format(new Double(getQuarter2().trim())) : "" ));
            setQuarter3( (getQuarter3() != null && !getQuarter3().isEmpty()? df.format(new Double(getQuarter3().trim())) : "" ));
            setQuarter4( (getQuarter4() != null && !getQuarter4().isEmpty()? df.format(new Double(getQuarter4().trim())) : "" ));
            setQuarter5( (getQuarter5() != null && !getQuarter5().isEmpty()? df.format(new Double(getQuarter5().trim())) : "" ));
            setQuarter6( (getQuarter6() != null && !getQuarter6().isEmpty()? df.format(new Double(getQuarter6().trim())) : "" ));
            setQuarter7( (getQuarter7() != null && !getQuarter7().isEmpty()? df.format(new Double(getQuarter7().trim())) : "" ));
            setQuarter8( (getQuarter8() != null && !getQuarter8().isEmpty()? df.format(new Double(getQuarter8().trim())) : "" ));
            setLastYear( (getLastYear() != null && !getLastYear().isEmpty()? df.format(new Double(getLastYear().trim())) : "" ));
            setYearBeforeLast( ((getYearBeforeLast() != null && !getYearBeforeLast().isEmpty() )? df.format(new Double(getYearBeforeLast().trim())) : "" ));
        } else if (getFormat().startsWith("percent")) {
            NumberFormat df = NumberFormat.getPercentInstance(Locale.US);
            int digits = getFormat().equals("percent2")
                    ? 2
                    : 0;
            df.setMaximumFractionDigits(digits);
            setQuarter1( (getQuarter1() != null && !getQuarter1().isEmpty()? df.format(new Double(getQuarter1().trim())) : "" ));
            setQuarter2( (getQuarter2() != null && !getQuarter2().isEmpty()? df.format(new Double(getQuarter2().trim())) : "" ));
            setQuarter3( (getQuarter3() != null && !getQuarter3().isEmpty()? df.format(new Double(getQuarter3().trim())) : "" ));
            setQuarter4( (getQuarter4() != null && !getQuarter4().isEmpty()? df.format(new Double(getQuarter4().trim())) : "" ));
            setQuarter5( (getQuarter5() != null && !getQuarter5().isEmpty()? df.format(new Double(getQuarter5().trim())) : "" ));
            setQuarter6( (getQuarter6() != null && !getQuarter6().isEmpty()? df.format(new Double(getQuarter6().trim())) : "" ));
            setQuarter7( (getQuarter7() != null && !getQuarter7().isEmpty()? df.format(new Double(getQuarter7().trim())) : "" ));
            setQuarter8( (getQuarter8() != null && !getQuarter8().isEmpty()? df.format(new Double(getQuarter8().trim())) : "" ));
            setLastYear( (getLastYear() != null && !getLastYear().isEmpty()? df.format(new Double(getLastYear().trim())) : "" ));
            setYearBeforeLast( ((getYearBeforeLast() != null && !getYearBeforeLast().isEmpty() )? df.format(new Double(getYearBeforeLast().trim())) : "" ));
        }else if (getFormat().equals("prevalence")) {
            NumberFormat df = DecimalFormat.getInstance(Locale.US);
            df.setMaximumFractionDigits(4);
            setQuarter1(getQuarter1());
            setQuarter2(getQuarter2());
            setQuarter3(getQuarter3());
            setQuarter4(getQuarter4());
            setQuarter5(getQuarter5());
            setQuarter6(getQuarter6());
            setQuarter7(getQuarter7());
            setQuarter8(getQuarter8());
            setLastYear(getLastYear());
            setYearBeforeLast(getYearBeforeLast());
        }
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        //DecimalFormat.getInstance().format(lastMonth):(format.equals("integer")?NumberFormat.getInstance().format(lastMonth):(format.equals("prevalence")?NumberFormat.getInstance().format(lastMonth):lastMonth ))
    }
    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }
    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }
    /**
     * @return the quarter1
     */
    public String getQuarter1() {
        return quarter1;
    }
    /**
     * @param quarter1 the quarter1 to set
     */
    public void setQuarter1(String quarter1) {
        this.quarter1 = quarter1;
    }
    /**
     * @return the quarter2
     */
    public String getQuarter2() {
        return quarter2;
    }
    /**
     * @param quarter2 the quarter2 to set
     */
    public void setQuarter2(String quarter2) {
        this.quarter2 = quarter2;
    }
    /**
     * @return the quarter3
     */
    public String getQuarter3() {
        return quarter3;
    }
    /**
     * @param quarter3 the quarter3 to set
     */
    public void setQuarter3(String quarter3) {
        this.quarter3 = quarter3;
    }
    /**
     * @return the quarter4
     */
    public String getQuarter4() {
        return quarter4;
    }
    /**
     * @param quarter4 the quarter4 to set
     */
    public void setQuarter4(String quarter4) {
        this.quarter4 = quarter4;
    }
    /**
     * @return the quarter5
     */
    public String getQuarter5() {
        return quarter5;
    }
    /**
     * @param quarter5 the quarter5 to set
     */
    public void setQuarter5(String quarter5) {
        this.quarter5 = quarter5;
    }
    /**
     * @return the quarter6
     */
    public String getQuarter6() {
        return quarter6;
    }
    /**
     * @param quarter6 the quarter6 to set
     */
    public void setQuarter6(String quarter6) {
        this.quarter6 = quarter6;
    }
    /**
     * @return the quarter7
     */
    public String getQuarter7() {
        return quarter7;
    }
    /**
     * @param quarter7 the quarter7 to set
     */
    public void setQuarter7(String quarter7) {
        this.quarter7 = quarter7;
    }
    /**
     * @return the quarter8
     */
    public String getQuarter8() {
        return quarter8;
    }
    /**
     * @param quarter8 the quarter8 to set
     */
    public void setQuarter8(String quarter8) {
        this.quarter8 = quarter8;
    }
    /**
     * @return the lastYear
     */
    public String getLastYear() {
        return lastYear;
    }
    /**
     * @param lastYear the lastYear to set
     */
    public void setLastYear(String lastYear) {
        this.lastYear = lastYear;
    }
    /**
     * @return the yearBeforeLast
     */
    public String getYearBeforeLast() {
        return yearBeforeLast;
    }
    /**
     * @param yearBeforeLast the yearBeforeLast to set
     */
    public void setYearBeforeLast(String yearBeforeLast) {
        this.yearBeforeLast = yearBeforeLast;
    }

    public boolean isRightAlign() {
        return rightAlign;
    }

    public void setRightAlign(boolean rightAlign) {
        this.rightAlign = rightAlign;
    }

    @Override
    public String toString() {
        return "DashVO{" + 
                name + 
                ", q1=" + quarter1 + 
                ", q2=" + quarter2 + 
                ", q3=" + quarter3 + 
                ", q4=" + quarter4 + 
                ", q5=" + quarter5 + 
                ", q6=" + quarter6 + 
                ", q7=" + quarter7 + 
                ", q8=" + quarter8 + 
                ", lastYear=" + lastYear +
                ", beforeLast" + yearBeforeLast +
                "}";
    }
   
    
}
