/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
/**
 *
 * @author travis
 */
public class FacilityComparisonVO {
    private Integer locationId;
    private Integer etiologyId;
    private String location;
    private String etiology;
    private Integer external;
    private Integer internal;
    private Integer not_defined;
    private Integer total;
    private Double heal_rate;
    private Double total_cost;
    private String alphaList;
    
    public FacilityComparisonVO(){}

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getEtiologyId() {
        return etiologyId;
    }

    public void setEtiologyId(Integer etiologyId) {
        this.etiologyId = etiologyId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEtiology() {
        return etiology;
    }

    public void setEtiology(String etiology) {
        this.etiology = etiology;
    }

    public Integer getExternal() {
        return external;
    }

    public void setExternal(Integer external) {
        this.external = external;
    }

    public Integer getInternal() {
        return internal;
    }

    public void setInternal(Integer internal) {
        this.internal = internal;
    }

    public Integer getNot_defined() {
        return not_defined;
    }

    public void setNot_defined(Integer not_defined) {
        this.not_defined = not_defined;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Double getHeal_rate() {
        return heal_rate;
    }

    public void setHeal_rate(Double heal_rate) {
        this.heal_rate = heal_rate;
    }

    public Double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(Double total_cost) {
        this.total_cost = total_cost;
    }

    public String getAlphaList() {
        return alphaList;
    }

    public void setAlphaList(String alphaList) {
        this.alphaList = alphaList;
    }
    
}
