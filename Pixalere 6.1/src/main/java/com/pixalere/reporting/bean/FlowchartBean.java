/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author travis
 */
public class FlowchartBean {
    public FlowchartBean(String title, List<FlowchartRow> row){
        this.title=title;
        this.row=row;
    }
    private String title;
    private List<FlowchartRow> row;
    private WoundPhotosVO photos;
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the row
     */
    public List<FlowchartRow> getRow() {
        return row;
    }
    /**
     * @param row the row to set
     */
    public void setRow(List<FlowchartRow> row) {
        this.row = row;
    }
    /**
     * @return the photos
     */
    public WoundPhotosVO getPhotos() {
        return photos;
    }
    /**
     * @param photos the photos to set
     */
    public void setPhotos(WoundPhotosVO photos) {
        this.photos = photos;
    }
    
}
