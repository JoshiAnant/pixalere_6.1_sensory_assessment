package com.pixalere.reporting.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;

/**
 *
 * @author User
 */
public class ScheduleEtiologyVO extends ValueObject implements Serializable {
    private Integer report_schedule_id;
    private Integer lookup_id;

    public ScheduleEtiologyVO() {
    }

    public Integer getReport_schedule_id() {
        return report_schedule_id;
    }

    public void setReport_schedule_id(Integer report_schedule_id) {
        this.report_schedule_id = report_schedule_id;
    }

    public Integer getLookup_id() {
        return lookup_id;
    }

    public void setLookup_id(Integer lookup_id) {
        this.lookup_id = lookup_id;
    }
    
    
}
