/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import java.util.List;
import java.util.ArrayList;
/**
 * A JasperReport Bean
 * 
 * @since 5.1
 * @version 5.1
 * @author travis
 */
public class PatientFile {
    private String patientName;
    private String phn;
    private String dob;
    private String treatmentLocation;
    private List<FlowchartBean> patientProfile = new ArrayList<FlowchartBean>();
    private List<FlowchartBean> woundProfile = new ArrayList<FlowchartBean>();
    private List<FlowchartBean> foot = new ArrayList<FlowchartBean>();
    private List<FlowchartBean> limb = new ArrayList<FlowchartBean>();
    private List<FlowchartBean> braden = new ArrayList<FlowchartBean>();
    private List<FlowchartBean> physical_exam = new ArrayList<FlowchartBean>();
    private List<FlowchartBean> assessments = new ArrayList<FlowchartBean>();
    public PatientFile(){
        
    }
    /**
     * @return the patientName
     */
    public String getPatientName() {
        return patientName;
    }
    /**
     * @param patientName the patientName to set
     */
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }
    /**
     * @return the phn
     */
    public String getPhn() {
        return phn;
    }
    /**
     * @param phn the phn to set
     */
    public void setPhn(String phn) {
        this.phn = phn;
    }
    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }
    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }
    /**
     * @return the treatmentLocation
     */
    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    /**
     * @param treatmentLocation the treatmentLocation to set
     */
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation = treatmentLocation;
    }
    /**
     * @return the patientProfile
     */
    public List<FlowchartBean> getPatientProfile() {
        return patientProfile;
    }
    /**
     * @param patientProfile the patientProfile to set
     */
    public void setPatientProfile(List<FlowchartBean> patientProfile) {
        this.patientProfile = patientProfile;
    }
    /**
     * @return the woundProfile
     */
    public List<FlowchartBean> getWoundProfile() {
        return woundProfile;
    }
    /**
     * @param woundProfile the woundProfile to set
     */
    public void setWoundProfile(List<FlowchartBean> woundProfile) {
        this.woundProfile = woundProfile;
    }
    /**
     * @return the foot
     */
    public List<FlowchartBean> getFoot() {
        return foot;
    }
    /**
     * @param foot the foot to set
     */
    public void setFoot(List<FlowchartBean> foot) {
        this.foot = foot;
    }
    /**
     * @return the limb
     */
    public List<FlowchartBean> getLimb() {
        return limb;
    }
    /**
     * @param limb the limb to set
     */
    public void setLimb(List<FlowchartBean> limb) {
        this.limb = limb;
    }
    /**
     * @return the braden
     */
    public List<FlowchartBean> getBraden() {
        return braden;
    }
    /**
     * @param braden the braden to set
     */
    public void setBraden(List<FlowchartBean> braden) {
        this.braden = braden;
    }
    /**
     * @return the assessments
     */
    public List<FlowchartBean> getAssessments() {
        return assessments;
    }
    /**
     * @param assessments the assessments to set
     */
    public void setAssessments(List<FlowchartBean> assessments) {
        this.assessments = assessments;
    }
    /**
     * @return the physical_exam
     */
    public List<FlowchartBean> getPhysical_exam() {
        return physical_exam;
    }
    /**
     * @param physical_exam the physical_exam to set
     */
    public void setPhysical_exam(List<FlowchartBean> physical_exam) {
        this.physical_exam = physical_exam;
    }
    
}
