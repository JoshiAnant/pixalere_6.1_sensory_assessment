/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.bean;

import com.pixalere.common.ValueObject;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Jose
 */
public class ScheduleConfigVO extends ValueObject implements Serializable {
    private Integer report_schedule_id;
    private Date execution_on;
    private Date created_on;
    
    // For single period reports
    private Date start_date;
    private Date end_date;
    // Multi-column / Other options
    private String extra_options;
    
    // Outcome - error tracking
    private Integer processed;
    private String exit_status;
    
    private ReportScheduleVO schedule;
    
    // Only display attributes
    private String displayText;
    
    public ScheduleConfigVO() {
    }

    public Integer getReport_schedule_id() {
        return report_schedule_id;
    }

    public void setReport_schedule_id(Integer report_schedule_id) {
        this.report_schedule_id = report_schedule_id;
    }

    public Date getExecution_on() {
        return execution_on;
    }

    public void setExecution_on(Date execution_on) {
        this.execution_on = execution_on;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getExtra_options() {
        return extra_options;
    }

    public void setExtra_options(String extra_options) {
        this.extra_options = extra_options;
    }

    public Integer getProcessed() {
        return processed;
    }

    public void setProcessed(Integer processed) {
        this.processed = processed;
    }

    public String getExit_status() {
        return exit_status;
    }

    public void setExit_status(String exit_status) {
        this.exit_status = exit_status;
    }

    public ReportScheduleVO getSchedule() {
        return schedule;
    }

    public void setSchedule(ReportScheduleVO schedule) {
        this.schedule = schedule;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
    
}
