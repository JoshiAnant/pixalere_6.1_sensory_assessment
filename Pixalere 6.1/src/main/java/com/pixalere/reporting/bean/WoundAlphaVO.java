/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
import com.pixalere.common.ValueObject;
/**
 *
 * @author travis
 */
public class WoundAlphaVO   extends ValueObject {
    public WoundAlphaVO(){
    }
    private String title;
    private String data;
    private String photo;
    private String expandedText;
    /**
     * @return the data
     */
    public String getData() {
        return data;
    }
    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }
    /**
     * @return the photo
     */
    public String getPhoto() {
        return photo;
    }
    /**
     * @param photo the photo to set
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the expandedText
     */
    public String getExpandedText() {
        return expandedText;
    }
    /**
     * @param expandedText the expandedText to set
     */
    public void setExpandedText(String expandedText) {
        this.expandedText = expandedText;
    }
}
