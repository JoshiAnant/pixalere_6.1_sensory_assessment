/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.bean;

/**
 *
 * @author User
 */
public class ReferralVO {
    private Integer id;
    private String name;
    private String referral_date;
    private String state;
    private String referral_for;
    private String recommendation_date;
    private String automated;

    public ReferralVO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReferral_date() {
        return referral_date;
    }

    public void setReferral_date(String referral_date) {
        this.referral_date = referral_date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReferral_for() {
        return referral_for;
    }

    public void setReferral_for(String referral_for) {
        this.referral_for = referral_for;
    }

    public String getRecommendation_date() {
        return recommendation_date;
    }

    public void setRecommendation_date(String recommendation_date) {
        this.recommendation_date = recommendation_date;
    }

    public String getAutomated() {
        return automated;
    }

    public void setAutomated(String automated) {
        this.automated = automated;
    }
    
    
}
