/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.bean;
/**
 *
 * @author travis
 */
public class FlowchartRow {
    public FlowchartRow(String title,String value){
        this.title=title;
        this.value=value;
    }
    private String title;
    private String value;
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
    
}
