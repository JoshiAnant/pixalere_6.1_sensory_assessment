/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting;

import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.common.DataAccessException;
import com.pixalere.utils.Common;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.ServletContext;

/**
 * Generates images from WoundProfiles entries for Report generation.
 * 
 * @TODO: The two methods below were two different implementations, used for 
 * different reports. More than half of their code was the same, but there are 
 * important differences that needed to be addressed when looping through 
 * the locations of a wound profile. The 'getFromProfileSummary' method seemed 
 * to be a more recent implementation but it was only used to generate one Report 
 * (PDFSummary); the 'getFromProfile' methods appeared to be older but it was used
 * to generate two reports (Patient Profile, Wound Management Plan).
 * 
 * In the new consolidated method, the 'getFromProfileSummary' loop is in the
 * true side of the flag 'summary'; the other reports go through the 
 * false or else branch inside the loop.
 * 
 * @since 6.0
 * @author Jose M
 */
public class BlueImageCreator extends Panel{
    
    private String basepath = null;
    
    public BlueImageCreator(ServletContext ctx){
        if (ctx != null) {
            basepath = Common.getBasePath(ctx);
        }
    }
    
    private void loadImage(Image imgTemp) {
        try {
            MediaTracker tracker = new MediaTracker(this);
            tracker.addImage(imgTemp, 0);
            tracker.waitForID(0);
        } catch (Exception e) {
        }
    }
    
    // Reports: Patient Profile, Wound Management Plan
    public Image getFromProfile(int intWoundProfileId, boolean blnIncludeUnsaved) {
        return getFromProfile(intWoundProfileId, blnIncludeUnsaved, false);
    }
    
    // Reports: PDFSummary
    public Image getFromProfileSummary(int intWoundProfileId, boolean blnIncludeUnsaved) {
        return getFromProfile(intWoundProfileId, blnIncludeUnsaved, true);
    }
    
    private Image getFromProfile(int intWoundProfileId, boolean blnIncludeUnsaved, boolean summary) {
        BufferedImage bimComposedBluemodel = null;
        
        try {
            // Retrieve Wound Profile to get the correct blue man image
            com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
            woundProfile.setWound_id(intWoundProfileId);
            woundProfile.setCurrent_flag(1);
            Collection lcol1 = null;
            WoundProfileDAO woundProfileDB = new WoundProfileDAO();
            WoundAssessmentLocationDAO woundAssessmentLocationDB = new WoundAssessmentLocationDAO();
            lcol1 = woundProfileDB.findAllByCriteria(woundProfile, 1); // One record
            Iterator iter1 = lcol1.iterator();
            if (iter1.hasNext()) {
                WoundProfileVO wprf = (WoundProfileVO) iter1.next();
                Image imgBluemodel = Toolkit.getDefaultToolkit().getImage(basepath + "images/bluemodel/" + wprf.getWound().getImage_name());
                loadImage(imgBluemodel);
                bimComposedBluemodel = new BufferedImage(327, 266, BufferedImage.TYPE_INT_RGB);
                Graphics2D g2dComposedBluemodel = bimComposedBluemodel.createGraphics();
                //Load the original image into the BufferedImage.
                g2dComposedBluemodel.drawImage(imgBluemodel, 0, 0, this);
                Graphics grp = bimComposedBluemodel.getGraphics();
                // Remove the blue Left/Right bar from the Blue model image
                grp.setColor(Color.white); // Set background
                grp.fillRect(0, 0, 327, 17);
                WoundAssessmentLocationVO woundAssessmentLocation = new WoundAssessmentLocationVO();
                woundAssessmentLocation.setWound_id(intWoundProfileId);
                if (blnIncludeUnsaved == false) {
                    woundAssessmentLocation.setActive(1); // Only submitted alpha's based on boolean
                }
                Collection lcol2 = null;
                lcol2 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation, 0, false, "");//4th parameter (extra sort) is ignored
                Iterator iter2 = lcol2.iterator();
                while (iter2.hasNext()) {
                    WoundAssessmentLocationVO wal = (WoundAssessmentLocationVO) iter2.next();
                    
                    if (summary){
                        // *** Summary Report ***
                        int intXBox = 0;//details[intIcons].getBox() - 1;
                        int intYBox = 0;
                        intXBox = wal.getX();
                        intYBox = wal.getY() + 50;
                        Color color = Color.YELLOW;
                        Font font = new Font("Arial", Font.BOLD, 16);
                        g2dComposedBluemodel.setFont(font);
                        String strColourCode = "y"; // Yellow for active alpha's
                        if (wal.getDischarge() == 1) {
                            color = Color.GREEN;
                            strColourCode = "g"; // Green for closed alpha's
                        }
                        if (wal.getActive() == 0) {
                            color = Color.RED;
                            strColourCode = "r"; // Red for un-saved alpha's
                            // An image name is e.g. "a_y.gif"; "a" is tha alpha, "y" is the colour (y=yellow, r=red, g=groen)
                        }
                        if (wal.getAlpha().length() == 1 || wal.getAlpha().toLowerCase().indexOf("tag") != -1) {
                            g2dComposedBluemodel.setColor(color);
                            if (wal.getAlpha().length() == 1) {
                                g2dComposedBluemodel.drawString(wal.getAlpha(), intXBox, intYBox);
                            } else {
                                String alpha = wal.getAlpha().substring(2, wal.getAlpha().length());
                                g2dComposedBluemodel.drawString(alpha, intXBox, intYBox);
                            }
                        } else {
                            Image alphaImage = Toolkit.getDefaultToolkit().getImage(basepath + "images/Alphas/" + wal.getAlpha().toLowerCase() + "_" + strColourCode + ".gif");
                            loadImage(alphaImage);
                            //Load the alpha image into the BufferedImage
                            g2dComposedBluemodel.drawImage(alphaImage, intXBox, intYBox, this);
                        }
                        //System.out.println("Drawing image");
                        if (wal.getAlpha().equals("incs")) {
                            WoundLocationDetailsVO[] details = wal.getAlpha_details();
                            for (int intIcons = 0; intIcons < details.length; intIcons++) {
                                intXBox = 0;//details[intIcons].getBox() - 1;
                                intYBox = 0;
                                intXBox = details[intIcons].getX();
                                intYBox = details[intIcons].getY() + 50;
                                Image alphaImage = Toolkit.getDefaultToolkit().getImage(basepath + "images/Alphas/" + details[intIcons].getImage().toLowerCase() + ".gif");
                                loadImage(alphaImage);
                                //Load the alpha image into the BufferedImage
                                g2dComposedBluemodel.drawImage(alphaImage, intXBox, intYBox, this);
                            }
                        }
                    } else {
                        // *** Other Reports ***
                        
                        // An Alpha image is 20 pixels wide and 19 pixels high.
                        // The blue man image has 12 rows of each 16 boxes, each 20 x 20 pixels.
                        // Boxes are numbered 1 through 192.
                        //WoundLocationDetailsVO[] details = wal.getAlpha_details();
                        //for (int intIcons = 0; intIcons < details.length; intIcons++) {
                        String strColourCode = "y"; // Yellow for active alpha's
                        if (wal.getDischarge() == 1) {
                            strColourCode = "g"; // Green for closed alpha's
                        }
                        if (wal.getActive() == 0) {
                            strColourCode = "r"; // Red for un-saved alpha's
                            // An image name is e.g. "a_y.gif"; "a" is tha alpha, "y" is the colour (y=yellow, r=red, g=groen)
                        }
                        Image alphaImage = Toolkit.getDefaultToolkit().getImage(basepath + "images/Alphas/report_alphas/" + wal.getAlpha() + "_" + strColourCode + ".gif");
                        loadImage(alphaImage);
                        //Load the alpha image into the BufferedImage
                        g2dComposedBluemodel.drawImage(alphaImage, wal.getX(), wal.getY()+17, this);
                        //System.out.println("Drawing image " + basepath + "images/Alphas/" + wal.getAlpha() + "_" + strColourCode + ".gif");
                    }
                    
                }
           }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return bimComposedBluemodel;
    }
   
}
