/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.manager;

/**
 *
 * @author travis
 */
public class UsageStats {

    private String name;
    private Integer patient;
    private Integer wound;
    private Integer visits;
    private Integer comments;
    private Integer id;
    public UsageStats(){}

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the patient
     */
    public Integer getPatient() {
        return patient;
    }

    /**
     * @param patient the patient to set
     */
    public void setPatient(Integer patient) {
        this.patient = patient;
    }

    /**
     * @return the wound
     */
    public Integer getWound() {
        return wound;
    }

    /**
     * @param wound the wound to set
     */
    public void setWound(Integer wound) {
        this.wound = wound;
    }

    /**
     * @return the visits
     */
    public Integer getVisits() {
        return visits;
    }

    /**
     * @param visits the visits to set
     */
    public void setVisits(Integer visits) {
        this.visits = visits;
    }

  

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the comments
     */
    public Integer getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(Integer comments) {
        this.comments = comments;
    }

   
    
}
