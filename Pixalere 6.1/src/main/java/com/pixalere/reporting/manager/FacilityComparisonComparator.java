/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.manager;

import com.pixalere.reporting.bean.FacilityComparisonVO;
import java.util.Comparator;

/**
 * Sorts the WoundTracking objects by Location and then by Patient Name
 * @author Jose Miron
 * @since 6.0
 */
public class FacilityComparisonComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        FacilityComparisonVO ref1 = (FacilityComparisonVO) o1;
        FacilityComparisonVO ref2 = (FacilityComparisonVO) o2;
        if (!ref1.getLocation().equals(ref2.getLocation())) {
            return ref1.getLocation().compareTo(ref2.getLocation());
        } else {
            if(ref1.getEtiology()!=null && ref2.getEtiology()!=null){
                return ref1.getEtiology().compareTo(ref2.getEtiology());
            } else return 0;
        }
    }
    
}
