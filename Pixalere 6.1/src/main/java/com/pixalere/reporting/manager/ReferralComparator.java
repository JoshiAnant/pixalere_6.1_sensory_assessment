/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.manager;

import com.pixalere.common.ApplicationException;
import com.pixalere.reporting.bean.ReferralVO;
import com.pixalere.utils.PDate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Sorts ReferralVO object by patient name and then by date ascending.
 * 
 * @author Jose
 * @since 6.0
 */
public class ReferralComparator implements Comparator<ReferralVO> {

    @Override
    public int compare(ReferralVO o1, ReferralVO o2) {
        if (!o1.getId().equals(o2.getId())){
            return o1.getId().compareTo(o2.getId());
        } else {
            if (o1.getReferral_date() == null) return 1;
            if (o2.getReferral_date() == null) return -1;
            Date d1 = reverseSignature(o1.getReferral_date());
            Date d2 = reverseSignature(o2.getReferral_date());
            if (d1 == null) return 1;
            if (d2 == null) return -1;
            return d1.compareTo(d2);
        }
    }
    
    private Date reverseSignature(String signature) {
        int idx = signature.indexOf(" - ");
        String dateString = signature.substring(0, idx);
        
        String pattern = "HHmm'h' d/MMM/yyyy z";
        DateFormat df = new SimpleDateFormat(pattern);
        Date dateRes = null;
        try {
            dateRes = df.parse(dateString);
            
        } catch (ParseException e) {
            System.out.println("ReferralComparator.reverseSignature - Error parsing.. " + signature);
            
            int index = signature.indexOf("h ");
            String hourdate = signature.substring(0, index);

            String date = signature.substring(index + 2, signature.length());
            int index3 = date.indexOf("/");
            int day = Integer.parseInt(date.substring(0, index3));
            date = date.substring(index3 + 1, date.length());
            int index4 = date.indexOf("/");
            String monthstr = date.substring(0, index4);
            int month = 0;
            if (monthstr.equals("Jan")) {
                month = 1;
            } else {
                if (monthstr.equals("Feb")) {
                    month = 2;
                } else {
                    if (monthstr.equals("Mar")) {
                        month = 3;
                    } else {
                        if (monthstr.equals("Apr")) {
                            month = 4;
                        } else {
                            if (monthstr.equals("May")) {
                                month = 5;
                            } else {
                                if (monthstr.equals("Jun")) {
                                    month = 6;
                                } else {
                                    if (monthstr.equals("Jul")) {
                                        month = 7;
                                    } else {
                                        if (monthstr.equals("Aug")) {
                                            month = 8;
                                        } else {
                                            if (monthstr.equals("Sep")) {
                                                month = 9;
                                            } else {
                                                if (monthstr.equals("Oct")) {
                                                    month = 10;
                                                } else {
                                                    if (monthstr.equals("Nov")) {
                                                        month = 11;
                                                    } else {
                                                        if (monthstr.equals("Dec")) {
                                                            month = 12;
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            int year = Integer.parseInt(date.substring(index4 + 1,
                    date.length()));

            int hour = Integer.parseInt(hourdate.substring(0, 2));
            int minute = Integer.parseInt(hourdate.substring(2,
                    hourdate.length()));

            try {
                dateRes = PDate.getDate(month, day, year, hour, minute);
            } catch (ApplicationException ex) {
            }

            
        }
        //System.out.println("Parsed: " + signature + "   - Date: " + (dateRes==null? "null":dateRes.toString()));
        
        return dateRes;
    }
}
