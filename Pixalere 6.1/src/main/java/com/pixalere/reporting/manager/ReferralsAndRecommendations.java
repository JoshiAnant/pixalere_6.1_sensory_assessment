package com.pixalere.reporting.manager;

public class ReferralsAndRecommendations {
	private int tip;
    private String grouping;
    private String treatmentLocation;
    private String priority;
	private int referralsCount;
	private int recommendationsCount;
	private int recommendationsResponse;
	private int recommendationsWithinPriority;
	private int acknowledgementsCount;
	private int acknowledgementsResponse;
	private int totalTurnaround;
	private int totalTurnaroundCount;
    
    public ReferralsAndRecommendations(
	int tip,
    String grouping,
	String treatmentLocation,
    String priority,
	int referralsCount,
	int recommendationsCount,
	int recommendationsResponse,
	int recommendationsWithinPriority,
    int acknowledgementsCount,
    int acknowledgementsResponse,
    int totalTurnaround,
    int totalTurnaroundCount){

    this.tip=tip;
	this.grouping=grouping;
    this.treatmentLocation=treatmentLocation;
    this.priority=priority;
    this.referralsCount=referralsCount;
	this.recommendationsCount=recommendationsCount;
	this.recommendationsResponse=recommendationsResponse;
	this.recommendationsWithinPriority=recommendationsWithinPriority;
	this.acknowledgementsCount=acknowledgementsCount;
	this.acknowledgementsResponse=acknowledgementsResponse;
    this.totalTurnaround=totalTurnaround;
    this.totalTurnaroundCount=totalTurnaroundCount;}
	
    public ReferralsAndRecommendations() {
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation=treatmentLocation;
    }

    public String getPriority() {
        return priority;
    }
    public void setPriority(String priority) {
        this.priority=priority;
    }

    public int getReferralsCount() {
        return referralsCount;
    }
    public void setReferralsCount(int referralsCount) {
        this.referralsCount=referralsCount;
    }

    public int getRecommendationsCount() {
        return recommendationsCount;
    }
    public void setRecommendationsCount(int recommendationsCount) {
        this.recommendationsCount=recommendationsCount;
    }

    public int getRecommendationsResponse() {
        return recommendationsResponse;
    }
    public void setRecommendationsResponse(int recommendationsResponse) {
        this.recommendationsResponse=recommendationsResponse;
    }

    public int getRecommendationsWithinPriority() {
        return recommendationsWithinPriority;
    }
    public void setRecommendationsWithinPriority(int recommendationsWithinPriority) {
        this.recommendationsWithinPriority=recommendationsWithinPriority;
    }

    public int getAcknowledgementsCount() {
        return acknowledgementsCount;
    }
    public void setAcknowledgementsCount(int acknowledgementsCount) {
        this.acknowledgementsCount=acknowledgementsCount;
    }

    public int getAcknowledgementsResponse() {
        return acknowledgementsResponse;
    }
    public void setAcknowledgementsResponse(int acknowledgementsResponse) {
        this.acknowledgementsResponse=acknowledgementsResponse;
    }

    public int getTotalTurnaround() {
        return totalTurnaround;
    }
    public void setTotalTurnaround(int totalTurnaround) {
        this.totalTurnaround=totalTurnaround;
    }

    public int getTotalTurnaroundCount() {
        return totalTurnaroundCount;
    }
    public void setTotalTurnaroundCount(int totalTurnaroundCount) {
        this.totalTurnaroundCount=totalTurnaroundCount;
    }
}
