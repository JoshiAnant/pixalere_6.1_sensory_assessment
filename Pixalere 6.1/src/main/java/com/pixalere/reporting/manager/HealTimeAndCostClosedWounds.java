package com.pixalere.reporting.manager;

public class HealTimeAndCostClosedWounds {
	private int tip;
    private String grouping;
    private String treatmentLocation;
    private String etiology;
    private String careType;
    private int careTypeSort;
    private int alphaClosedCount;
	private int assessmentCount;
	private int healDaysFromStart;
	private float totalCost;

    public HealTimeAndCostClosedWounds(
	int tip,
    String grouping,
	String treatmentLocation,
	String etiology,
    String careType,
    int careTypeCode,
    int alphaClosedCount,
	int assessmentCount,
    int healDaysFromStart,
	float totalCost){
    this.grouping=grouping;
    this.treatmentLocation=treatmentLocation;
	this.etiology=etiology;
    this.careType=careType;
    this.careTypeSort=careTypeSort;
    this.alphaClosedCount=alphaClosedCount;
    this.assessmentCount=assessmentCount;
	this.healDaysFromStart=healDaysFromStart;
	this.totalCost=totalCost;}
	
    public HealTimeAndCostClosedWounds() {
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation=treatmentLocation;
    }

    public String getEtiology() {
        return etiology;
    }
    public void setEtiology(String etiology) {
		this.etiology=etiology;
    }

    public String getCareType() {
        return careType;
    }
    public void setCareType(String careType) {
		this.careType=careType;
    }

    public int getCareTypeSort() {
        return careTypeSort;
    }
    public void setCareTypeSort(int careTypeSort) {
		this.careTypeSort=careTypeSort;
    }

    public int getAlphaClosedCount() {
        return alphaClosedCount;
    }
    public void setAlphaClosedCount(int alphaClosedCount) {
        this.alphaClosedCount=alphaClosedCount;
    }

    public int getAssessmentCount() {
        return assessmentCount;
    }
    public void setAssessmentCount(int assessmentCount) {
        this.assessmentCount=assessmentCount;
    }

    public int getHealDaysFromStart() {
        return healDaysFromStart;
    }
    public void setHealDaysFromStart(int healDaysFromStart) {
        this.healDaysFromStart=healDaysFromStart;
    }

    public float getTotalCost() {
        return totalCost;
    }
    public void setTotalCost(float totalCost) {
        this.totalCost=totalCost;
    }
}
