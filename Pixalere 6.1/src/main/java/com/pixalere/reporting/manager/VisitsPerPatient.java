package com.pixalere.reporting.manager;

import java.util.Date;

public class VisitsPerPatient {
	private int tip;
    private String treatmentLocation;
	private int noClinicians;
    private String clinicians;
	private int patientId;
	private String patientId_sort;
    private String woundProfiles;
    private String startDate;
    private String endDate;
    private Date startDateVal;
    private Date endDateVal;
    private int programStayDays;
    private int physicalNurseVisits;
    private int physicalClinicianVisits;
    private int virtualClinicianConsults;
    private int totalPhysicalVisits;
    
    public VisitsPerPatient(
    String treatmentLocation,
	int tip,
	int noClinicians,
	String clinicians,
	int patientId,
	String patientId_sort,
    String woundProfiles,
    String startDate,
    String endDate,
    Date startDateVal,
    Date endDateVal,
    int programStayDays,
    int physicalNurseVisits,
    int physicalClinicianVisits,
    int virtualClinicianConsults,
    int totalPhysicalVisits){

	this.treatmentLocation=treatmentLocation;
    this.tip=tip;
	this.noClinicians=noClinicians;
    this.clinicians=clinicians;
    this.patientId=patientId;
	this.patientId_sort=patientId_sort;
    this.woundProfiles=woundProfiles;
    this.startDate=startDate;
    this.endDate=endDate;
    this.startDateVal=startDateVal;
    this.endDateVal=endDateVal;
    this.programStayDays=programStayDays;
    this.physicalNurseVisits=physicalNurseVisits;
    this.physicalClinicianVisits=physicalClinicianVisits;
    this.virtualClinicianConsults=virtualClinicianConsults;
    this.totalPhysicalVisits=totalPhysicalVisits;}

    public VisitsPerPatient() {
    }

	public int getNoClinicians() {
        return noClinicians;
    }
    public void setNoClinicians(int noClinicians) {
        this.noClinicians=noClinicians;
    }

    public String getClinicians() {
        return clinicians;
    }
    public void setClinicians(String clinicians) {
        this.clinicians=clinicians;
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation=treatmentLocation;
    }

    public int getPatientId() {
        return patientId;
    }
    public void setPatientId(int patientId) {
        this.patientId=patientId;
    }
    
    public String getPatientId_sort() {
        return patientId_sort;
    }
    public void setPatientId_sort(String patientId_sort) {
        this.patientId_sort=patientId_sort;
    }

    public String getWoundProfiles() {
        return woundProfiles;
    }
    public void setWoundProfiles(String woundProfiles) {
        this.woundProfiles=woundProfiles;
    }

    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate=startDate;
    }

    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate=endDate;
    }

    public Date getStartDateVal() {
        return startDateVal;
    }
    public void setStartDateVal(Date startDateVal) {
        this.startDateVal=startDateVal;
    }

    public Date getEndDateVal() {
        return endDateVal;
    }
    public void setEndDateVal(Date endDateVal) {
        this.endDateVal=endDateVal;
    }

	public int getProgramStayDays() {
        return programStayDays;
    }
    public void setProgramStayDays(int programStayDays) {
        this.programStayDays=programStayDays;
    }
    
	public int getPhysicalNurseVisits() {
        return physicalNurseVisits;
    }
    public void setPhysicalNurseVisits(int physicalNurseVisits) {
        this.physicalNurseVisits=physicalNurseVisits;
    }
    
	public int getPhysicalClinicianVisits() {
        return physicalClinicianVisits;
    }
    public void setPhysicalClinicianVisits(int physicalClinicianVisits) {
        this.physicalClinicianVisits=physicalClinicianVisits;
    }
    
	public int getVirtualClinicianConsults() {
        return virtualClinicianConsults;
    }
    public void setVirtualClinicianConsults(int virtualClinicianConsults) {
        this.virtualClinicianConsults=virtualClinicianConsults;
    }
    
	public int getTotalPhysicalVisits() {
        return totalPhysicalVisits;
    }
    public void setTotalPhysicalVisits(int totalPhysicalVisits) {
        this.totalPhysicalVisits=totalPhysicalVisits;
    }
    
}
