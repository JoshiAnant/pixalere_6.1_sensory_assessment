package com.pixalere.reporting.manager;

public class PressureWoundsCost {
	private int tip;
    private String grouping;
    private String treatmentLocation;
    private String patientNameFull;
    private int patientAccountId;
	private String patientAccountId_sort;
    private String woundProfile;
    private String pressureStageStart;
    private String pressureStageEnd;
    private String woundStartDate;
    private int woundStartTimeStamp; // For sorting
    private String woundCloseDate;
    private int patientCount;
    private int woundCount;
	private float totalCost;

    public PressureWoundsCost(
	int tip,
    String grouping,
	String treatmentLocation,
    String patientNameFull,
    int patientAccountId,
	String patientAccountId_sort,
    String woundProfile,
    String pressureStageStart,
    String pressureStageEnd,
    String woundStartDate,
    int woundStartTimeStamp,
    String woundCloseDate,
    int patientCount,
    int woundCount,
	float totalCost){
	  
    this.tip=tip;
	this.grouping=grouping;
    this.treatmentLocation=treatmentLocation;
    this.patientNameFull=patientNameFull;
    this.patientAccountId=patientAccountId;
	this.patientAccountId_sort=patientAccountId_sort;
    this.woundProfile=woundProfile;
    this.pressureStageStart=pressureStageStart;
    this.pressureStageEnd=pressureStageEnd;
    this.woundStartDate=woundStartDate;
    this.woundStartTimeStamp=woundStartTimeStamp;
    this.woundCloseDate=woundCloseDate;
    this.patientCount=patientCount;
    this.woundCount=woundCount;
	this.totalCost=totalCost;}
	
    public PressureWoundsCost() {
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation=treatmentLocation;
    }

    public String getPatientNameFull() {
        return patientNameFull;
    }
    public void setPatientNameFull(String patientNameFull) {
		this.patientNameFull=patientNameFull;
    }

    public int getPatientAccountId() {
        return patientAccountId;
    }
    public void setPatientAccountId(int patientAccountId) {
        this.patientAccountId=patientAccountId;
    }

    public String getPatientAccountId_sort() {
        return patientAccountId_sort;
    }
    public void setPatientAccountId_sort(String patientAccountId_sort) {
        this.patientAccountId_sort=patientAccountId_sort;
    }


    public String getWoundProfile() {
        return woundProfile;
    }
    public void setWoundProfile(String woundProfile) {
        this.woundProfile=woundProfile;
    }

	public String getPressureStageStart() {
        return pressureStageStart;
    }
    public void setPressureStageStart(String pressureStageStart) {
        this.pressureStageStart=pressureStageStart;
    }

	public String getPressureStageEnd() {
        return pressureStageEnd;
    }
    public void setPressureStageEnd(String pressureStageEnd) {
        this.pressureStageEnd=pressureStageEnd;
    }

	public String getWoundStartDate() {
        return woundStartDate;
    }
    public void setWoundStartDate(String woundStartDate) {
        this.woundStartDate=woundStartDate;
    }

	public int getWoundStartTimeStamp() {
        return woundStartTimeStamp;
    }
    public void setWoundStartTimeStamp(int woundStartTimeStamp) {
        this.woundStartTimeStamp=woundStartTimeStamp;
    }

	public String getWoundCloseDate() {
        return woundCloseDate;
    }
    public void setWoundCloseDate(String woundCloseDate) {
        this.woundCloseDate=woundCloseDate;
    }

    public int getPatientCount() {
        return patientCount;
    }
    public void setPatientCount(int patientCount) {
        this.patientCount=patientCount;
    }

    public int getWoundCount() {
        return woundCount;
    }
    public void setWoundCount(int woundCount) {
        this.woundCount=woundCount;
    }

    public float getTotalCost() {
        return totalCost;
    }
    public void setTotalCost(float totalCost) {
        this.totalCost=totalCost;
    }
}
