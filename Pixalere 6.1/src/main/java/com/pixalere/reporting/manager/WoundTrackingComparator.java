/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.manager;

import java.util.Comparator;

/**
 * Sorts the WoundTracking objects by Location and then by Patient Name
 * @author Jose Miron
 * @since 6.0
 */
public class WoundTrackingComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        WoundTracking ref1 = (WoundTracking) o1;
        WoundTracking ref2 = (WoundTracking) o2;
        if (!ref1.getLocation().equals(ref2.getLocation())) {
            return ref1.getLocation().compareTo(ref2.getLocation());
        } else {
            if (ref1.getName() != null && ref2.getName() != null) {
                if (ref1.getName().equals(ref2.getName())){
                    if(ref1.getEtiology()!=null && ref2.getEtiology()!=null){
                        return ref1.getEtiology().compareTo(ref2.getEtiology());
                    } else return 0;
                } else return ref1.getName().compareTo(ref2.getName());
            } else return 0; 
        }
    }
    
}
