package com.pixalere.reporting.manager;

public class ActivePatients {
	private int tip;
    private String grouping;
    private String treatmentLocation;
    private String patientNameFull;
    private String woundProfiles;
    private String dobFull;
    private int patientAccountId;
    private int woundCount;
    
    public ActivePatients(
	int tip,
	String grouping,
	String treatmentLocation,
	String patientNameFull,
	String woundProfiles,
	String dobFull,
	int patientAccountId,
	int woundCount){

    this.tip=tip;
    this.grouping=grouping;
    this.treatmentLocation=treatmentLocation;
	this.patientNameFull=patientNameFull;
	this.woundProfiles=woundProfiles;
	this.dobFull=dobFull;
	this.patientAccountId=patientAccountId;
	this.woundCount=woundCount;
        }

    public ActivePatients() {
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation=treatmentLocation;
    }

    public String getPatientNameFull() {
        return patientNameFull;
    }
    public void setPatientNameFull(String patientNameFull) {
        this.patientNameFull=patientNameFull;
    }

    public String getWoundProfiles() {
        return woundProfiles;
    }
    public void setWoundProfiles(String woundProfiles) {
        this.woundProfiles=woundProfiles;
    }

    public String getDobFull() {
        return dobFull;
    }
    public void setDobFull(String dobFull) {
        this.dobFull=dobFull;
    }

    public int getPatientAccountId() {
        return patientAccountId;
    }
    public void setPatientAccountId(int patientAccountId) {
        this.patientAccountId=patientAccountId;
    }

    public int getWoundCount() {
        return woundCount;
    }
    public void setWoundCount(int woundCount) {
        this.woundCount=woundCount;
    }


}
