package com.pixalere.reporting.manager;
import java.awt.*;

public class PatientFile {
    private int patientId;
    private int recordLevel;
    private String rowNumber;
    private String recordInfoTitle;
    private String recordInfoData;
    private String woundProfile;
    private String woundProfileId;
    private String woundProfileRec;
    private Image bluemodelImage;
    private String alpha;
    private String alphaTitle;
    private String assessment;
    private String assessmentId;
    private int imagesRowCount;
    private String rowTitle;
    private String rowData;
    private String image1;
    private String image2;
    private String image3;
    private String image4;
    
    public PatientFile(
    int patientId,
    int recordLevel,
    String rowNumber,
    String recordInfoTitle,
    String recordInfoData,
    String woundProfile,
    String woundProfileId,
    String woundProfileRec,
    Image bluemodelImage,
    String alpha,
    String alphaTitle,
    String assessment,
    String assessmentId,
    int imagesRowCount,
    String rowTitle,
    String rowData,
    String image1,
    String image2,
    String image3,
    String image4) {
    
	this.patientId=patientId;
	this.recordLevel=recordLevel;
    this.rowNumber=rowNumber;
	this.recordInfoTitle=recordInfoTitle;
    this.recordInfoData=recordInfoData;
    this.woundProfile=woundProfile;
    this.woundProfileId=woundProfileId;
    this.woundProfileRec=woundProfileRec;
    this.bluemodelImage=bluemodelImage;
    this.alpha=alpha;
    this.alphaTitle=alphaTitle;
    this.assessment=assessment;
    this.assessmentId=assessmentId;
	this.imagesRowCount=imagesRowCount;
    this.rowTitle=rowTitle;
    this.rowData=rowData;
    this.image1=image1; 
    this.image2=image2; 
    this.image3=image3; 
    this.image4=image4; }

    public PatientFile() {
    }

    public int getPatientId() {
        return patientId;
    }
    public void setPatientId(int patientId) {
        this.patientId=patientId;
    }

    public int getRecordLevel() {
        return recordLevel;
    }
    public void setRecordLevel(int recordLevel) {
        this.recordLevel=recordLevel;
    }

    public String getRowNumber() {
        return rowNumber;
    }
    public void setRowNumber(String rowNumber) {
        this.rowNumber=rowNumber;
    }

    public String getRecordInfoTitle() {
        return recordInfoTitle;
    }
    public void setRecordInfoTitle(String recordInfoTitle) {
        this.recordInfoTitle=recordInfoTitle;
    }

    public String getRecordInfoData() {
        return recordInfoData;
    }
    public void setRecordInfoData(String recordInfoData) {
        this.recordInfoData=recordInfoData;
    }

    public String getWoundProfile() {
        return woundProfile;
    }
    public void setWoundProfile(String woundProfile) {
        this.woundProfile=woundProfile;
    }

    public String getWoundProfileId() {
        return woundProfileId;
    }
    public void setWoundProfileId(String woundProfileId) {
        this.woundProfileId=woundProfileId;
    }

    public String getWoundProfileRec() {
        return woundProfileRec;
    }
    public void setWoundProfileRec(String woundProfileRec) {
        this.woundProfileRec=woundProfileRec;
    }

    public Image getBluemodelImage() {
        return bluemodelImage;
    }
    public void setBluemodelImage(Image bluemodelImage) {
        this.bluemodelImage=bluemodelImage;
    }

    public String getAssessment() {
        return assessment;
    }
    public void setAssessment(String assessment) {
        this.assessment=assessment;
    }

    public String getAlpha() {
        return alpha;
    }
    public void setAlpha(String alpha) {
        this.alpha=alpha;
    }

    public String getAlphaTitle() {
        return alphaTitle;
    }
    public void setAlphaTitle(String alphaTitle) {
        this.alphaTitle=alphaTitle;
    }

    public String getAssessmentId() {
        return assessmentId;
    }
    public void setAssessmentId(String assessmentId) {
        this.assessmentId=assessmentId;
    }

    public int getImagesRowCount() {
        return imagesRowCount;
    }
    public void setImagesRowCount(int imagesRowCount) {
        this.imagesRowCount=imagesRowCount;
    }

    public String getRowTitle() {
        return rowTitle;
    }
    public void setRowTitle(String rowTitle) {
        this.rowTitle=rowTitle;
    }

    public String getRowData() {
        return rowData;
    }
    public void setRowData(String rowData) {
        this.rowData=rowData;
    }

    public String getImage1() {
        return image1;
    }
    public void setImage1(String image1) {
        this.image1=image1;
    }

    public String getImage2() {
        return image2;
    }
    public void setImage2(String image2) {
        this.image2=image2;
    }

    public String getImage3() {
        return image3;
    }
    public void setImage3(String image3) {
        this.image3=image3;
    }

    public String getImage4() {
        return image4;
    }
    public void setImage4(String image4) {
        this.image4=image4;
    }

}
