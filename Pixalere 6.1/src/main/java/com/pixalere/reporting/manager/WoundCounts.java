package com.pixalere.reporting.manager;

public class WoundCounts {
	private int tip;
    private String grouping;
    private String subGrouping;
    private String careType;
    private int careTypeNr;
    private int patientCount;
    private int woundCount;
    private int woundExistingCount;
    private int woundNewCount;
    private int woundClosedCount;
    private int woundCurrentActiveCount;
    private int visitsCount;
    private int woundAssessmentsCount;
    private int dressings;
    private Double dressings_percentage;
    private Double cost;
    
    private String breakDown;
    private int patientCountBreakDown;
    private int woundCountBreakDown;
    private int woundExistingCountBreakDown;
    private int woundNewCountBreakDown;
    private int woundClosedCountBreakDown;
    private int woundCurrentActiveCountBreakDown;
    private int visitsCountBreakDown;
    private int woundAssessmentsCountBreakDown;
    
    public WoundCounts(
	int tip,
	String grouping,
	String subGrouping,
    String careType,
    int careTypeNr,
    int patientCount,
    int woundCount,
    int woundExistingCount,
    int woundNewCount,
    int woundClosedCount,
	int woundCurrentActiveCount,
    int visitsCount,
    int woundAssessmentsCount,
    String breakDown,
    int patientCountBreakDown,
    int woundCountBreakDown,
    int woundExistingCountBreakDown,
    int woundNewCountBreakDown,
    int woundClosedCountBreakDown,
	int woundCurrentActiveCountBreakDown,
    int visitsCountBreakDownBreakDown,
    int woundAssessmentsCountBreakDown){

    this.tip=tip;
    this.grouping=grouping;
    this.subGrouping=subGrouping;
    this.careType=careType;
    this.careTypeNr=careTypeNr;
    this.patientCount=patientCount;
    this.woundCount=woundCount;
    this.woundExistingCount=woundExistingCount;
    this.woundNewCount=woundNewCount;
    this.woundClosedCount=woundClosedCount;
	this.woundCurrentActiveCount=woundCurrentActiveCount;
    this.visitsCount=visitsCount;
    this.woundAssessmentsCount=woundAssessmentsCount;
    this.breakDown=breakDown;
    this.patientCountBreakDown=patientCountBreakDown;
    this.woundExistingCountBreakDown=woundExistingCountBreakDown;
    this.woundNewCountBreakDown=woundNewCountBreakDown;
    this.woundClosedCountBreakDown=woundClosedCountBreakDown;
	this.woundCurrentActiveCountBreakDown=woundCurrentActiveCountBreakDown;
    this.visitsCountBreakDown=visitsCountBreakDown;
    this.woundAssessmentsCountBreakDown=woundAssessmentsCountBreakDown;}
    public WoundCounts() {
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }
    
    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getSubGrouping() {
        return subGrouping;
    }
    public void setSubGrouping(String subGrouping) {
        this.subGrouping=subGrouping;
    }

    public String getCareType() {
        return careType;
    }
    public void setCareType(String careType) {
        this.careType=careType;
    }

    public int getCareTypeNr() {
        return careTypeNr;
    }
    public void setCareTypeNr(int careTypeNr) {
        this.careTypeNr=careTypeNr;
    }

    public int getPatientCount() {
        return patientCount;
    }
    public void setPatientCount(int patientCount) {
        this.patientCount=patientCount;
    }

    public int getWoundCount() {
        return woundCount;
    }
    public void setWoundCount(int woundCount) {
        this.woundCount=woundCount;
    }

    public int getWoundExistingCount() {
        return woundExistingCount;
    }
    public void setWoundExistingCount(int woundExistingCount) {
        this.woundExistingCount=woundExistingCount;
    }

    public int getWoundNewCount() {
        return woundNewCount;
    }
    public void setWoundNewCount(int woundNewCount) {
        this.woundNewCount=woundNewCount;
    }

    public int getWoundClosedCount() {
        return woundClosedCount;
    }
    public void setWoundClosedCount(int woundClosedCount) {
        this.woundClosedCount=woundClosedCount;
    }

    public int getWoundCurrentActiveCount() {
        return woundCurrentActiveCount;
    }
    public void setWoundCurrentActiveCount(int woundCurrentActiveCount) {
        this.woundCurrentActiveCount=woundCurrentActiveCount;
    }

    public int getVisitsCount() {
        return visitsCount;
    }
    public void setVisitsCount(int visitsCount) {
        this.visitsCount=visitsCount;
    }

    public int getWoundAssessmentsCount() {
        return woundAssessmentsCount;
    }
    public void setWoundAssessmentsCount(int woundAssessmentsCount) {
        this.woundAssessmentsCount=woundAssessmentsCount;
    }

    public int getDressings() {
        return dressings;
    }

    public void setDressings(int dressings) {
        this.dressings = dressings;
    }

    public Double getDressings_percentage() {
        return dressings_percentage;
    }

    public void setDressings_percentage(Double dressings_percentage) {
        this.dressings_percentage = dressings_percentage;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getBreakDown() {
        return breakDown;
    }
    public void setBreakDown(String breakDown) {
        this.breakDown=breakDown;
    }

    public int getPatientCountBreakDown() {
        return patientCountBreakDown;
    }
    public void setPatientCountBreakDown(int patientCountBreakDown) {
        this.patientCountBreakDown=patientCountBreakDown;
    }

    public int getWoundCountBreakDown() {
        return woundCountBreakDown;
    }
    public void setWoundCountBreakDown(int woundCountBreakDown) {
        this.woundCountBreakDown=woundCountBreakDown;
    }

    public int getWoundExistingCountBreakDown() {
        return woundExistingCountBreakDown;
    }
    public void setWoundExistingCountBreakDown(int woundExistingCountBreakDown) {
        this.woundExistingCountBreakDown=woundExistingCountBreakDown;
    }

    public int getWoundNewCountBreakDown() {
        return woundNewCountBreakDown;
    }
    public void setWoundNewCountBreakDown(int woundNewCountBreakDown) {
        this.woundNewCountBreakDown=woundNewCountBreakDown;
    }

    public int getWoundClosedCountBreakDown() {
        return woundClosedCountBreakDown;
    }
    public void setWoundClosedCountBreakDown(int woundClosedCountBreakDown) {
        this.woundClosedCountBreakDown=woundClosedCountBreakDown;
    }

    public int getWoundCurrentActiveCountBreakDown() {
        return woundCurrentActiveCountBreakDown;
    }
    public void setWoundCurrentActiveCountBreakDown(int woundCurrentActiveCountBreakDown) {
        this.woundCurrentActiveCountBreakDown=woundCurrentActiveCountBreakDown;
    }

    public int getVisitsCountBreakDown() {
        return visitsCountBreakDown;
    }
    public void setVisitsCountBreakDown(int visitsCountBreakDown) {
        this.visitsCountBreakDown=visitsCountBreakDown;
    }

    public int getWoundAssessmentsCountBreakDown() {
        return woundAssessmentsCountBreakDown;
    }
    public void setWoundAssessmentsCountBreakDown(int woundAssessmentsCountBreakDown) {
        this.woundAssessmentsCountBreakDown=woundAssessmentsCountBreakDown;
    }

}
