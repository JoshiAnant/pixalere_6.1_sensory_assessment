/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.manager;

import java.util.Comparator;
/**
 *
 * @author travis
 */
public class WoundList  implements Comparator {
    private int pix_id;
    private String name;
    private String location;
    private String wound;
    private String etiology;
    private String new_wound;
    private String last_comment;
    private String heal_status;
    private String status;
    private String acquired;
    private String date_closed;



    
    /**
     * Order by Location and then by Etiology
     * @param o1
     * @param o2
     * @return 
     */
    public int compare(Object o1, Object o2) {
        WoundTracking ref1 = (WoundTracking) o1;
        WoundTracking ref2 = (WoundTracking) o2;
        if (!ref1.getLocation().equals(ref2.getLocation())) {
            return ref1.getLocation().compareTo(ref2.getLocation());
        } else {
            if(ref1.getEtiology()!=null && ref2.getEtiology()!=null){
                if (!ref1.getEtiology().equals(ref2.getEtiology())) {
                    return ref1.getEtiology().compareTo(ref2.getEtiology());
                } else {
                    if (ref1.getName() != null && ref2.getName() != null) {
                        return ref1.getName().compareTo(ref2.getName());
                    } else return 0;
                }
            } else return 0;
        }

    }
    public boolean equals(Object o) {
        WoundTracking other = (WoundTracking) o;
        
        // NOTE: A consistent equals() for the compare() method above
        if (this.getLocation().equals(other.getLocation())){
            if (other.getEtiology() == null) return true;
            if (this.getEtiology().equals(other.getEtiology())){
                if (other.getName() == null) return true;
                if (this.getName().equals(other.getName())) return true;
            }
        }
        
        return false;
    }
    /**
     * @return the pix_id
     */
    public int getPix_id() {
        return pix_id;
    }

    /**
     * @param pix_id the pix_id to set
     */
    public void setPix_id(int pix_id) {
        this.pix_id = pix_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the wound
     */
    public String getWound() {
        return wound;
    }

    /**
     * @param wound the wound to set
     */
    public void setWound(String wound) {
        this.wound = wound;
    }

    /**
     * @return the etiology
     */
    public String getEtiology() {
        return etiology;
    }

    /**
     * @param etiology the etiology to set
     */
    public void setEtiology(String etiology) {
        this.etiology = etiology;
    }

  

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAcquired() {
        return acquired;
    }

    public void setAcquired(String acquired) {
        this.acquired = acquired;
    }

    public String getDate_closed() {
        return date_closed;
    }

    public void setDate_closed(String date_closed) {
        this.date_closed = date_closed;
    }

    
    /**
     * @return the new_wound
     */
    public String getNew_wound() {
        return new_wound;
    }

    /**
     * @param new_wound the new_wound to set
     */
    public void setNew_wound(String new_wound) {
        this.new_wound = new_wound;
    }

    /**
     * @return the last_comment
     */
    public String getLast_comment() {
        return last_comment;
    }

    /**
     * @param last_comment the last_comment to set
     */
    public void setLast_comment(String last_comment) {
        this.last_comment = last_comment;
    }

    /**
     * @return the heal_status
     */
    public String getHeal_status() {
        return heal_status;
    }

    /**
     * @param heal_status the heal_status to set
     */
    public void setHeal_status(String heal_status) {
        this.heal_status = heal_status;
    }

   

}
