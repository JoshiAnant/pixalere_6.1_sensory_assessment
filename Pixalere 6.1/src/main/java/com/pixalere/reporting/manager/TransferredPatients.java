package com.pixalere.reporting.manager;

public class TransferredPatients {
    private String origPatientCareLocation;
    private String newPatientCareLocation;
    private int woundsCount;
    
    public TransferredPatients(
    String origPatientCareLocation,
    String newPatientCareLocation,
	int woundsCount){

    this.origPatientCareLocation=origPatientCareLocation;
    this.newPatientCareLocation=newPatientCareLocation;
	this.woundsCount=woundsCount;}

    public TransferredPatients() {
    }

    public String getOrigPatientCareLocation() {
        return origPatientCareLocation;
    }
    public void setOrigPatientCareLocation(String origPatientCareLocation) {
        this.origPatientCareLocation=origPatientCareLocation;
    }

    public String getNewPatientCareLocation() {
        return newPatientCareLocation;
    }
    public void setNewPatientCareLocation(String newPatientCareLocation) {
        this.newPatientCareLocation=newPatientCareLocation;
    }

    public int getWoundsCount() {
        return woundsCount;
    }
    public void setWoundsCount(int woundsCount) {
        this.woundsCount=woundsCount;
    }
    
}
