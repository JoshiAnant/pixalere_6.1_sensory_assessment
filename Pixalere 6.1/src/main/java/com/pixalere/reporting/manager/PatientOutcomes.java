/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.manager;

import java.util.Comparator;
import java.util.Date;
/**
 *
 * @author travis
 */
public class PatientOutcomes  implements Comparator {
    private int pix_id;
    private String name;
    private String location;
    private String wound;
    private String etiology;
    private Double heal_rate;
    private double cost;
    private String last_assessment;
    private String wound_status;
    private String closed_reason;
    private int days_to_heal;
    public PatientOutcomes(){}
    public PatientOutcomes(int pix_id,String name,String location,String wound,String etiology,double heal_rate,double cost,String last_assessment,String wound_status,String closed_reason){
        this.pix_id=pix_id;
        this.name=name;
        this.location=location;
        this.wound=wound;
        this.etiology=etiology;
        this.heal_rate=heal_rate;
        this.cost=cost;
        this.last_assessment=last_assessment;
        this.wound_status=wound_status;
        this.closed_reason=closed_reason;
    }
public int compare(Object o1, Object o2) {
        PatientOutcomes ref1 = (PatientOutcomes) o1;
        PatientOutcomes ref2 = (PatientOutcomes) o2;
        if (!ref1.getLocation().equals(ref2.getLocation())) {
            return ref1.getLocation().compareTo(ref2.getLocation());
        } else {
            if(ref1.getHeal_rate()!=null && ref2.getHeal_rate()!=null){
            return ref1.getHeal_rate().compareTo(ref2.getHeal_rate());
            }else{return 0;}
        }

    }
    public boolean equals(Object o) {
        return true;
    }
    /**
     * @return the pix_id
     */
    public int getPix_id() {
        return pix_id;
    }

    /**
     * @param pix_id the pix_id to set
     */
    public void setPix_id(int pix_id) {
        this.pix_id = pix_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the wound
     */
    public String getWound() {
        return wound;
    }

    /**
     * @param wound the wound to set
     */
    public void setWound(String wound) {
        this.wound = wound;
    }

    /**
     * @return the etiology
     */
    public String getEtiology() {
        return etiology;
    }

    /**
     * @param etiology the etiology to set
     */
    public void setEtiology(String etiology) {
        this.etiology = etiology;
    }

    /**
     * @return the heal_rate
     */
    public Double getHeal_rate() {
        return heal_rate;
    }

    /**
     * @param heal_rate the heal_rate to set
     */
    public void setHeal_rate(Double heal_rate) {
        this.heal_rate = heal_rate;
    }

    /**
     * @return the cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * @return the last_assessment
     */
    public String getLast_assessment() {
        return last_assessment;
    }

    /**
     * @param last_assessment the last_assessment to set
     */
    public void setLast_assessment(String last_assessment) {
        this.last_assessment = last_assessment;
    }

    /**
     * @return the wound_status
     */
    public String getWound_status() {
        return wound_status;
    }

    /**
     * @param wound_status the wound_status to set
     */
    public void setWound_status(String wound_status) {
        this.wound_status = wound_status;
    }

    /**
     * @return the closed_reason
     */
    public String getClosed_reason() {
        return closed_reason;
    }

    /**
     * @param closed_reason the closed_reason to set
     */
    public void setClosed_reason(String closed_reason) {
        this.closed_reason = closed_reason;
    }

    /**
     * @return the days_to_heal
     */
    public int getDays_to_heal() {
        return days_to_heal;
    }

    /**
     * @param days_to_heal the days_to_heal to set
     */
    public void setDays_to_heal(int days_to_heal) {
        this.days_to_heal = days_to_heal;
    }
}
