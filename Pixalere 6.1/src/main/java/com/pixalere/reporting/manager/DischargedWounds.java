package com.pixalere.reporting.manager;

public class DischargedWounds {
	private int tip;
    private String grouping;
    private String etiology;
    private String reason;
    private int patientCount;
	private int alphaClosedCount;
	private int assessmentsCount;
	private int visitsCount;
	private int healTime;

    public DischargedWounds(
	int tip,
	String grouping,
	String etiology,
    String reason,
    int patientCount,
	int alphaClosedCount,
	int assessmentsCount,
	int visitsCount,
	int healTime){

    this.tip=tip;
    this.grouping=grouping;
	this.etiology=etiology;
	this.reason=reason;
    this.patientCount=patientCount;
	this.alphaClosedCount=alphaClosedCount;
	this.assessmentsCount=assessmentsCount;
	this.visitsCount=visitsCount;
	this.healTime=healTime;}
	
    public DischargedWounds() {
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getEtiology() {
        return etiology;
    }
    public void setEtiology(String etiology) {
		this.etiology=etiology;
    }

    public String getReason() {
        return reason;
    }
    public void setReason(String reason) {
		this.reason=reason;
    }

    public int getPatientCount() {
        return patientCount;
    }
    public void setPatientCount(int patientCount) {
        this.patientCount=patientCount;
    }

    public int getAlphaClosedCount() {
        return alphaClosedCount;
    }
    public void setAlphaClosedCount(int alphaClosedCount) {
        this.alphaClosedCount=alphaClosedCount;
    }

    public int getAssessmentsCount() {
        return assessmentsCount;
    }
    public void setAssessmentsCount(int assessmentsCount) {
        this.assessmentsCount=assessmentsCount;
    }

    public int getVisitsCount() {
        return visitsCount;
    }
    public void setVisitsCount(int visitsCount) {
        this.visitsCount=visitsCount;
    }

    public int getHealTime() {
        return healTime;
    }
    public void setHealTime(int healTime) {
        this.healTime=healTime;
    }

}
