/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.manager;

import com.pixalere.reporting.bean.WoundAcquiredVO;
import java.util.Comparator;

/**
 *
 * @author User
 */
public class WoundAcquiredComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        WoundAcquiredVO ref1 = (WoundAcquiredVO) o1;
        WoundAcquiredVO ref2 = (WoundAcquiredVO) o2;
        if (!ref1.getGrouping().equals(ref2.getGrouping())) {
            return ref1.getGrouping().compareTo(ref2.getGrouping());
        } else {
            if(ref1.getName()!=null && ref2.getName()!=null){
                return ref1.getName().compareTo(ref2.getName());
            } else return 0;
        }
    }
    
    
}
