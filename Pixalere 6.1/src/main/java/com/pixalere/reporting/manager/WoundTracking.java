/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.manager;

import java.util.Comparator;
/**
 *
 * @author travis
 */
public class WoundTracking  implements Comparator {
    private int pix_id;
    private String name;
    private String location;
    private String wound;
    private String etiology;
    private Double heal_rate;
    private String status;
    private String acquired;
    private String date_onset;
    private String date_open;
    private String date_closed;
    // Fields for detailed report
    private Double cost;
    private int visit_frequency;
    private int dfc;
    private int open_referrals;
    
    /**
     * Order by Location and then by Etiology
     * @param o1
     * @param o2
     * @return 
     */
    public int compare(Object o1, Object o2) {
        WoundTracking ref1 = (WoundTracking) o1;
        WoundTracking ref2 = (WoundTracking) o2;
        if (!ref1.getLocation().equals(ref2.getLocation())) {
            return ref1.getLocation().compareTo(ref2.getLocation());
        } else {
            if(ref1.getEtiology()!=null && ref2.getEtiology()!=null){
                if (!ref1.getEtiology().equals(ref2.getEtiology())) {
                    return ref1.getEtiology().compareTo(ref2.getEtiology());
                } else {
                    if (ref1.getName() != null && ref2.getName() != null) {
                        return ref1.getName().compareTo(ref2.getName());
                    } else return 0;
                }
            } else return 0;
        }

    }
    public boolean equals(Object o) {
        WoundTracking other = (WoundTracking) o;
        
        // NOTE: A consistent equals() for the compare() method above
        if (this.getLocation().equals(other.getLocation())){
            if (other.getEtiology() == null) return true;
            if (this.getEtiology().equals(other.getEtiology())){
                if (other.getName() == null) return true;
                if (this.getName().equals(other.getName())) return true;
            }
        }
        
        return false;
    }
    /**
     * @return the pix_id
     */
    public int getPix_id() {
        return pix_id;
    }

    /**
     * @param pix_id the pix_id to set
     */
    public void setPix_id(int pix_id) {
        this.pix_id = pix_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the wound
     */
    public String getWound() {
        return wound;
    }

    /**
     * @param wound the wound to set
     */
    public void setWound(String wound) {
        this.wound = wound;
    }

    /**
     * @return the etiology
     */
    public String getEtiology() {
        return etiology;
    }

    /**
     * @param etiology the etiology to set
     */
    public void setEtiology(String etiology) {
        this.etiology = etiology;
    }

    /**
     * @return the heal_rate
     */
    public Double getHeal_rate() {
        return heal_rate;
    }

    /**
     * @param heal_rate the heal_rate to set
     */
    public void setHeal_rate(Double heal_rate) {
        this.heal_rate = heal_rate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAcquired() {
        return acquired;
    }

    public void setAcquired(String acquired) {
        this.acquired = acquired;
    }

    public String getDate_onset() {
        return date_onset;
    }

    public void setDate_onset(String date_onset) {
        this.date_onset = date_onset;
    }

    public String getDate_open() {
        return date_open;
    }

    public void setDate_open(String date_open) {
        this.date_open = date_open;
    }

    public String getDate_closed() {
        return date_closed;
    }

    public void setDate_closed(String date_closed) {
        this.date_closed = date_closed;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public int getVisit_frequency() {
        return visit_frequency;
    }

    public void setVisit_frequency(int visit_frequency) {
        this.visit_frequency = visit_frequency;
    }

    public int getDfc() {
        return dfc;
    }

    public void setDfc(int dfc) {
        this.dfc = dfc;
    }

    public int getOpen_referrals() {
        return open_referrals;
    }

    public void setOpen_referrals(int open_referrals) {
        this.open_referrals = open_referrals;
    }

}
