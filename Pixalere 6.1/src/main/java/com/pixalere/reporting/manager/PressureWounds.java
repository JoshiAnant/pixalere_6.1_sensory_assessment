package com.pixalere.reporting.manager;

public class PressureWounds {
	private int tip;
    private String grouping;
    private String treatmentLocation;
    private int patientAccountId;
   	private String patientNameFull;
   	private String woundProfile;
	private String woundStage;
	private int woundStage1;
	private int woundStage2;
	private int woundStage3;
	private int woundStage4;
	private int woundStage5;
    
    public PressureWounds(
	int tip,
    String grouping,
	String treatmentLocation,
	int patientAccountId,
	String patientNameFull,
	String woundProfile,
	String woundStage,
	int woundStage1,
	int woundStage2,
	int woundStage3,
	int woundStage4,
	int woundStage5){

    this.tip=tip;
    this.grouping=grouping;
    this.treatmentLocation=treatmentLocation;
    this.patientAccountId=patientAccountId;
    this.patientNameFull=patientNameFull;
    this.woundProfile=woundProfile;
    this.woundStage=woundStage;
    this.woundStage1=woundStage1;
    this.woundStage2=woundStage2;
    this.woundStage3=woundStage3;
    this.woundStage4=woundStage4;
    this.woundStage5=woundStage5;}

    public PressureWounds() {
	}

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation=treatmentLocation;
    }

    public int getPatientAccountId() {
        return patientAccountId;
    }
    public void setPatientAccountId(int patientAccountId) {
        this.patientAccountId=patientAccountId;
    }

	public String getPatientNameFull() {
        return patientNameFull;
    }
    public void setPatientNameFull(String patientNameFull) {
        this.patientNameFull=patientNameFull;
    }

	public String getWoundProfile() {
        return woundProfile;
    }
    public void setWoundProfile(String woundProfile) {
        this.woundProfile=woundProfile;
    }

	public String getWoundStage() {
        return woundStage;
    }
    public void setWoundStage(String woundStage) {
        this.woundStage=woundStage;
    }

	public int getWoundStage1() {
        return woundStage1;
    }
    public void setWoundStage1(int woundStage1) {
        this.woundStage1=woundStage1;
    }

	public int getWoundStage2() {
        return woundStage2;
    }
    public void setWoundStage2(int woundStage2) {
        this.woundStage2=woundStage2;
    }

	public int getWoundStage3() {
        return woundStage3;
    }
    public void setWoundStage3(int woundStage3) {
        this.woundStage3=woundStage3;
    }

	public int getWoundStage4() {
        return woundStage4;
    }
    public void setWoundStage4(int woundStage4) {
        this.woundStage4=woundStage4;
    }

	public int getWoundStage5() {
        return woundStage5;
    }
    public void setWoundStage5(int woundStage5) {
        this.woundStage5=woundStage5;
    }

}
