package com.pixalere.reporting.manager;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
public class LastAssessments implements Comparator, Serializable {
	private int tip;
    private String grouping;
    private String treatmentLocation;
	private int patientAccountId;
	private String patientAccountId_sort;
	private String patientNameFull;
	private String woundProfile;
	private String woundAlpha;
	private String woundGoal;
    private String lastAssessmentDate;
    private Date lastAssessmentTimeStamp; // For sorting
    
    public LastAssessments(
	int tip,
    String grouping,
	String treatmentLocation,
	int patientAccountId,
	String patientAccountId_sort,
	String patientNameFull,
	String woundProfile,
	String woundAlpha,
	String woundGoal,
	String lastAssessmentDate,
	Date lastAssessmentTimeStamp){

    this.tip=tip;
	this.grouping=grouping;
	this.treatmentLocation=treatmentLocation;
    this.patientAccountId=patientAccountId;
	this.patientAccountId_sort=patientAccountId_sort;
    this.patientNameFull=patientNameFull;
    this.woundProfile=woundProfile;
    this.woundAlpha=woundAlpha;
    this.woundGoal=woundGoal;
    this.lastAssessmentDate=lastAssessmentDate;
    this.lastAssessmentTimeStamp=lastAssessmentTimeStamp;}

    public LastAssessments() {
    }

	public int getTip() {
        return tip;
    }
    public void setTip(int tip) {
        this.tip=tip;
    }

    public String getGrouping() {
        return grouping;
    }
    public void setGrouping(String grouping) {
        this.grouping=grouping;
    }

    public String getTreatmentLocation() {
        return treatmentLocation;
    }
    public void setTreatmentLocation(String treatmentLocation) {
        this.treatmentLocation=treatmentLocation;
    }

    public int getPatientAccountId() {
        return patientAccountId;
    }
    public void setPatientAccountId(int patientAccountId) {
        this.patientAccountId=patientAccountId;
    }

    public String getPatientAccountId_sort() {
        return patientAccountId_sort;
    }
    public void setPatientAccountId_sort(String patientAccountId_sort) {
        this.patientAccountId_sort=patientAccountId_sort;
    }

    public String getPatientNameFull() {
        return patientNameFull;
    }
    public void setPatientNameFull(String patientNameFull) {
        this.patientNameFull=patientNameFull;
    }
	
	public String getWoundProfile() {
        return woundProfile;
    }
    public void setWoundProfile(String woundProfile) {
        this.woundProfile=woundProfile;
    }

	public String getWoundAlpha() {
        return woundAlpha;
    }
    public void setWoundAlpha(String woundAlpha) {
        this.woundAlpha=woundAlpha;
    }

	public String getWoundGoal() {
        return woundGoal;
    }
    public void setWoundGoal(String woundGoal) {
        this.woundGoal=woundGoal;
    }

    public String getLastAssessmentDate() {
        return lastAssessmentDate;
    }
    public void setLastAssessmentDate(String lastAssessmentDate) {
        this.lastAssessmentDate=lastAssessmentDate;
    }

    public Date getLastAssessmentTimeStamp() {
        return lastAssessmentTimeStamp;
    }
    public void setLastAssessmentTimeStamp(Date lastAssessmentTimeStamp) {
        this.lastAssessmentTimeStamp=lastAssessmentTimeStamp;
    }
    public int compare(Object o1, Object o2) {
        LastAssessments ref1 = (LastAssessments) o1;
        LastAssessments ref2 = (LastAssessments) o2;
        if (!ref1.getLastAssessmentTimeStamp().equals(ref2.getLastAssessmentTimeStamp())) {
            if (ref2.getLastAssessmentTimeStamp().before(ref1.getLastAssessmentTimeStamp())) {
                return -1;
            } else {
                return 1;
            }
        } else {
            return ref2.getTreatmentLocation().compareTo(ref1.getTreatmentLocation());
        }

    }

    public boolean equals(Object o) {
        return true;
    }
}
