/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.factory;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.reporting.bean.PatientFile;
import java.util.List;
import com.pixalere.utils.Common;
import java.util.ArrayList;
import com.pixalere.guibeans.RowData;
import com.pixalere.reporting.bean.FlowchartRow;
import com.pixalere.reporting.bean.FlowchartBean;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.Iterator;
/**
 * @todo future version of the patient file report...
 * @version 5.2
 * @author travis
 */
public class PatientFileFactory {
    private static PatientFile patients = new PatientFile();
    private static List<FlowchartBean> col = new ArrayList<FlowchartBean>();
    public static void addFlowchart(RowData[] rows,String flowchart, String base_path){
        if(flowchart.equals("woundprofile")){
            List<FlowchartBean> frows = getRows(Common.getLocalizedString("wound","en"),rows);
            patients.setWoundProfile(frows);
        }
    }
    public static void initializeBean(){
        col = new ArrayList<FlowchartBean>();
    }
    public static void addFlowchart(RowData[] rows,String flowchart,boolean initialize, String locale){
        if(initialize == true){col = new ArrayList<FlowchartBean>();}
        if(flowchart.equals("assessment")){
            List<FlowchartBean> frows = getRows("",rows);
            patients.setAssessments(frows);
        }else if(flowchart.equals("patientprofile")){
            List<FlowchartBean> frows = getRows(Common.getLocalizedString("patient", locale),rows);
            patients.setPatientProfile(frows);
        }else if(flowchart.equals("limb")){
            List<FlowchartBean> frows = getRows(Common.getLocalizedString("pixalere.patientprofile.form.limb_assessment", locale),rows);
            patients.setLimb(frows);
        }else if(flowchart.equals("foot")){
            List<FlowchartBean> frows = getRows(Common.getLocalizedString("pixalere.patientprofile.form.foot_assessment", locale),rows);
            patients.setFoot(frows);
        }else if(flowchart.equals("braden")){
            List<FlowchartBean> frows = getRows(Common.getLocalizedString("pixalere.alert.braden", locale),rows);
            patients.setBraden(frows);
        }else if(flowchart.equals("physical_exam")){
            List<FlowchartBean> frows = getRows(Common.getLocalizedString("pixalere.viewer.form.physical_exam", locale),rows);
            patients.setWoundProfile(frows);
        }
    }
    public static PatientFile getPatient(){
        return patients;
    }
    private static List<FlowchartBean> getRows(String title,RowData[] rows){
        
        for(RowData row : rows){
            int count = 0;
            List<FlowchartRow> frows = new ArrayList<FlowchartRow>();
            List<FieldValues> fields = row.getFields();
            for(FieldValues field : fields){
                if(count > 0){
                if(!field.getValue().equals("")){
                    frows.add(new FlowchartRow(field.getTitle(),field.getValue()));
                }
                }
                count++;
            }
            col.add(new FlowchartBean(title,frows));
        }
        return col;
    }
    private static List<FlowchartBean> getRowsWithBlueman(String title,RowData[] rows,String base_path){
        List<FlowchartBean> col = new ArrayList<FlowchartBean>();
        for(RowData row : rows){
            int count = 0;
            List<FlowchartRow> frows = new ArrayList<FlowchartRow>();
            
            List<FieldValues> fields = row.getFields();
            for(FieldValues field : fields){
                if(count > 0){
                if(!field.getValue().equals("")){
                    frows.add(new FlowchartRow(field.getTitle(),field.getValue()));
                }
                }
                count++;
            }
            col.add(new FlowchartBean(title,frows));
        }
        return col;
    }
    /*public Image composeBluemodelImage(int intWoundProfileId, boolean blnIncludeUnsaved) {
        BufferedImage bimComposedBluemodel = null;
        try {
            // Retrieve Wound Profile to get the correct blue man image
            com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
            woundProfile.setWound_id(intWoundProfileId);
            woundProfile.setCurrent_flag(1);
            Collection lcol1 = null;
            WoundProfileDAO woundProfileDB = new WoundProfileDAO();
            WoundAssessmentLocationDAO woundAssessmentLocationDB = new WoundAssessmentLocationDAO();
            lcol1 = woundProfileDB.findAllByCriteria(woundProfile, 1); // One record
            Iterator iter1 = lcol1.iterator();
            if (iter1.hasNext()) {
                WoundProfileVO wprf = (WoundProfileVO) iter1.next();
                Image imgBluemodel = Toolkit.getDefaultToolkit().getImage(basepath + "images/bluemodel/" + wprf.getWound().getImage_name());
                loadImage(imgBluemodel);
                bimComposedBluemodel = new BufferedImage(327, 266, BufferedImage.TYPE_INT_RGB);
                Graphics2D g2dComposedBluemodel = bimComposedBluemodel.createGraphics();
                //Load the original image into the BufferedImage.
                g2dComposedBluemodel.drawImage(imgBluemodel, 0, 0, this);
                Graphics grp = bimComposedBluemodel.getGraphics();
                // Remove the blue Left/Right bar from the Blue model image
                grp.setColor(Color.white); // Set background
                grp.fillRect(0, 0, 327, 17);
                WoundAssessmentLocationVO woundAssessmentLocation = new WoundAssessmentLocationVO();
                woundAssessmentLocation.setWound_id(intWoundProfileId);
                if (blnIncludeUnsaved == false) {
                    woundAssessmentLocation.setActive(1); // Only submitted alpha's based on boolean
                }
                Collection lcol2 = null;
                lcol2 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation, 0, false, "");//4th parameter (extra sort) is ignored
                Iterator iter2 = lcol2.iterator();
                while (iter2.hasNext()) {
                    WoundAssessmentLocationVO wal = (WoundAssessmentLocationVO) iter2.next();
                    int intXBox = 0;//details[intIcons].getBox() - 1;
                    int intYBox = 0;
                    intXBox = wal.getX();
                    intYBox = wal.getY() + 50;
                    Color color = Color.YELLOW;
                    Font font = new Font("Arial", Font.BOLD, 16);
                    g2dComposedBluemodel.setFont(font);
                    String strColourCode = "y"; // Yellow for active alpha's
                    if (wal.getDischarge() == 1) {
                        color = Color.GREEN;
                        strColourCode = "g"; // Green for closed alpha's
                    }
                    if (wal.getActive() == 0) {
                        color = Color.RED;
                        strColourCode = "r"; // Red for un-saved alpha's
                        // An image name is e.g. "a_y.gif"; "a" is tha alpha, "y" is the colour (y=yellow, r=red, g=groen)
                    }
                    if (wal.getAlpha().length() == 1 || wal.getAlpha().toLowerCase().indexOf("tag") != -1) {
                        g2dComposedBluemodel.setColor(color);
                        if (wal.getAlpha().length() == 1) {
                            g2dComposedBluemodel.drawString(wal.getAlpha(), intXBox, intYBox);
                        } else {
                            String alpha = wal.getAlpha().substring(2, wal.getAlpha().length());
                            g2dComposedBluemodel.drawString(alpha, intXBox, intYBox);
                        }
                    } else {
                        Image alphaImage = Toolkit.getDefaultToolkit().getImage(basepath + "images/Alphas/" + wal.getAlpha().toLowerCase() + "_" + strColourCode + ".gif");
                        loadImage(alphaImage);
                        //Load the alpha image into the BufferedImage
                        g2dComposedBluemodel.drawImage(alphaImage, intXBox, intYBox, this);
                    }
                    //System.out.println("Drawing image");
                    if (wal.getAlpha().equals("incs")) {
                        WoundLocationDetailsVO[] details = wal.getAlpha_details();
                        for (int intIcons = 0; intIcons < details.length; intIcons++) {
                            intXBox = 0;//details[intIcons].getBox() - 1;
                            intYBox = 0;
                            intXBox = details[intIcons].getX();
                            intYBox = details[intIcons].getY() + 50;
                            Image alphaImage = Toolkit.getDefaultToolkit().getImage(basepath + "images/Alphas/" + details[intIcons].getImage().toLowerCase() + ".gif");
                            loadImage(alphaImage);
                            //Load the alpha image into the BufferedImage
                            g2dComposedBluemodel.drawImage(alphaImage, intXBox, intYBox, this);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bimComposedBluemodel;
    }
     private void loadImage(Image imgTemp) {
        try {
            MediaTracker tracker = new MediaTracker(this);
            tracker.addImage(imgTemp, 0);
            tracker.waitForID(0);
        } catch (Exception e) {
        }
    }*/
}
