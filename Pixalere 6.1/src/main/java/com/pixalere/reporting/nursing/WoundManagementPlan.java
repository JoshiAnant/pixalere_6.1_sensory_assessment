package com.pixalere.reporting.nursing;
import com.pixalere.reporting.bean.WoundAlphaVO;
import java.util.List;
import java.awt.Image;
public class WoundManagementPlan {
    private String demographics;
    private Image blueman;
    private String woundLocation;
    private String title;
    private String data;
    private String photo;
    private String expandedText;
    private String photo_date;
    public WoundManagementPlan() {}

    /**
     * @return the demographics
     */
    public String getDemographics() {
        return demographics;
    }

    /**
     * @param demographics the demographics to set
     */
    public void setDemographics(String demographics) {
        this.demographics = demographics;
    }

    /**
     * @return the blueman
     */
    public Image getBlueman() {
        return blueman;
    }

    /**
     * @param blueman the blueman to set
     */
    public void setBlueman(Image blueman) {
        this.blueman = blueman;
    }



   

 

  

    /**
     * @return the woundLocation
     */
    public String getWoundLocation() {
        return woundLocation;
    }

    /**
     * @param woundLocation the woundLocation to set
     */
    public void setWoundLocation(String woundLocation) {
        this.woundLocation = woundLocation;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * @return the expandedText
     */
    public String getExpandedText() {
        return expandedText;
    }

    /**
     * @param expandedText the expandedText to set
     */
    public void setExpandedText(String expandedText) {
        this.expandedText = expandedText;
    }

    /**
     * @return the photo_date
     */
    public String getPhoto_date() {
        return photo_date;
    }

    /**
     * @param photo_date the photo_date to set
     */
    public void setPhoto_date(String photo_date) {
        this.photo_date = photo_date;
    }

}
