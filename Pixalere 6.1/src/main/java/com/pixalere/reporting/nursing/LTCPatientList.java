/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.nursing;

import java.util.Collection;
/**
 * The Long-Term Care Patient list bean to house all the data elements for the
 * LTC Patient report.   Each bean will contain a single patient's data.
 * 
 * @since 6.0
 * @version 1
 * @author travis
 */
public class LTCPatientList {
    private String name;
    private Integer id;
    private String location;
    private String funding_source;
    private String comorbidities;
    private Integer num_encounters;//# of events that caused a stay or visit.
    private Integer length_of_stay;//in days
    private Collection<WoundProfileInfo> wounds;
    
    public LTCPatientList(){}

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the funding_source
     */
    public String getFunding_source() {
        return funding_source;
    }

    /**
     * @param funding_source the funding_source to set
     */
    public void setFunding_source(String funding_source) {
        this.funding_source = funding_source;
    }

    /**
     * @return the comorbidities
     */
    public String getComorbidities() {
        return comorbidities;
    }

    /**
     * @param comorbidities the comorbidities to set
     */
    public void setComorbidities(String comorbidities) {
        this.comorbidities = comorbidities;
    }

    /**
     * @return the num_encounters
     */
    public Integer getNum_encounters() {
        return num_encounters;
    }

    /**
     * @param num_encounters the num_encounters to set
     */
    public void setNum_encounters(Integer num_encounters) {
        this.num_encounters = num_encounters;
    }

    /**
     * @return the length_of_stay
     */
    public Integer getLength_of_stay() {
        return length_of_stay;
    }

    /**
     * @param length_of_stay the length_of_stay to set
     */
    public void setLength_of_stay(Integer length_of_stay) {
        this.length_of_stay = length_of_stay;
    }

    /**
     * @return the wounds
     */
    public Collection<WoundProfileInfo> getWounds() {
        return wounds;
    }

    /**
     * @param wounds the wounds to set
     */
    public void setWounds(Collection<WoundProfileInfo> wounds) {
        this.wounds = wounds;
    }
    
    
}
