/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.nursing;

import java.util.Collection;
/**
 * Individual wounds profiles information with corresponding alphas.
 * 
 * @since 6.0
 * @version 1.0
 * @author travis
 */
public class WoundProfileInfo {
    private String wound_profile;
    private String start_date;
    private String end_date;
    private String cause_history;
    private int num_visits;
    private Collection<WoundAlphaInfo> alphas;
    public WoundProfileInfo(){}

    /**
     * @return the wound_profile
     */
    public String getWound_profile() {
        return wound_profile;
    }

    /**
     * @param wound_profile the wound_profile to set
     */
    public void setWound_profile(String wound_profile) {
        this.wound_profile = wound_profile;
    }

    /**
     * @return the start_date
     */
    public String getStart_date() {
        return start_date;
    }

    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     */
    public String getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    /**
     * @return the cause_history
     */
    public String getCause_history() {
        return cause_history;
    }

    /**
     * @param cause_history the cause_history to set
     */
    public void setCause_history(String cause_history) {
        this.cause_history = cause_history;
    }

    /**
     * @return the num_visits
     */
    public int getNum_visits() {
        return num_visits;
    }

    /**
     * @param num_visits the num_visits to set
     */
    public void setNum_visits(int num_visits) {
        this.num_visits = num_visits;
    }

    /**
     * @return the alphas
     */
    public Collection<WoundAlphaInfo> getAlphas() {
        return alphas;
    }

    /**
     * @param alphas the alphas to set
     */
    public void setAlphas(Collection<WoundAlphaInfo> alphas) {
        this.alphas = alphas;
    }
    
    
}
