/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.nursing;

/**
 * 
 * @since 6.0
 * @version 1
 * @author travis
 */
public class WoundAlphaInfo {
    private String alpha;
    private String start_date;
    private String end_date;
    private String etiology;
    private String goals;
    public WoundAlphaInfo(){}

    /**
     * @return the alpha
     */
    public String getAlpha() {
        return alpha;
    }

    /**
     * @param alpha the alpha to set
     */
    public void setAlpha(String alpha) {
        this.alpha = alpha;
    }

    /**
     * @return the start_date
     */
    public String getStart_date() {
        return start_date;
    }

    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     */
    public String getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }



    /**
     * @return the etiology
     */
    public String getEtiology() {
        return etiology;
    }

    /**
     * @param etiology the etiology to set
     */
    public void setEtiology(String etiology) {
        this.etiology = etiology;
    }

    /**
     * @return the goals
     */
    public String getGoals() {
        return goals;
    }

    /**
     * @param goals the goals to set
     */
    public void setGoals(String goals) {
        this.goals = goals;
    }
    
}
