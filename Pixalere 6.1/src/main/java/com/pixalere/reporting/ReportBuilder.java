
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;
import com.pixalere.reporting.manager.*;
import com.pixalere.reporting.nursing.*;
import com.pixalere.reporting.bean.ReportQueueVO;
import com.pixalere.patient.bean.ExternalReferralVO;
import com.pixalere.assessment.service.TreatmentCommentsServiceImpl;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.assessment.bean.AssessmentTreatmentVO;
import com.pixalere.reporting.bean.OutcomeBasedReport;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import java.util.Collection;
import com.pixalere.reporting.bean.PatientFile;
import com.pixalere.reporting.bean.WoundAcquiredVO;
import com.pixalere.mail.SendMailUsingAuthentication;
import com.pixalere.reporting.manager.LastAssessments;
import com.pixalere.reporting.service.ReportingServiceImpl;
import org.jfree.chart.plot.PlotOrientation;
import java.sql.ResultSet;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.common.DataAccessException;
import com.pixalere.reporting.bean.CCACReportVO;
import com.pixalere.reporting.manager.PatientOutcomes;
import java.util.Collections;
import java.util.*;
import com.pixalere.utils.FiscalYear;
import com.pixalere.utils.FiscalQuarter;
import java.io.*;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.reporting.bean.DashboardVO;
import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.DashboardByEtiologyVO;
import com.pixalere.reporting.bean.DashboardByLocationVO;
import java.awt.image.*;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Canvas;
import java.awt.Toolkit;
import java.awt.Font;
import com.pixalere.auth.bean.UserAccountRegionsVO;

import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;

import com.pixalere.reporting.dao.ReportDAO;
import com.pixalere.reporting.bean.WoundPhotosVO;
import com.pixalere.reporting.bean.SummaryVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.patient.bean.PhysicalExamVO;
import com.pixalere.utils.*;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.guibeans.RowData;
import com.pixalere.assessment.bean.WoundLocationDetailsVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.LimbAdvAssessmentVO;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.dao.AssessmentCommentsDAO;
import com.pixalere.assessment.dao.WoundAssessmentDAO;
import com.pixalere.assessment.service.*;
import com.pixalere.auth.bean.PositionVO;
import com.pixalere.common.bean.AutoReferralVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.common.dao.ReferralsTrackingDAO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.bean.*;
import com.pixalere.wound.bean.GoalsVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.reporting.bean.FacilityComparisonVO;
import com.pixalere.reporting.bean.ReferralVO;
import com.pixalere.reporting.factory.PatientFileFactory;
import com.pixalere.reporting.manager.WoundTracking;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import java.awt.MediaTracker;
import javax.imageio.ImageIO;
import java.awt.Panel;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import net.sf.jasperreports.engine.JRParameter;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.renderer.category.BarRenderer;

/**
 * Generate's the 5.0 reports
 *
 * @since 5.0
 * @author travis
 * @verion 6.0
 * @todo update all report outputs to allow PDF or Word.  Dressing is upated with example.
 */
public class ReportBuilder extends Panel {

    HttpServletRequest request = null;
    HttpServletResponse response = null;
    ServletContext servletContext = null;
    private HashMap parameters = new HashMap();
    private Canvas component = new Canvas();
    private Integer language = 1;
    private String locale = "en";
    private String doc_type = "pdf";

    public ReportBuilder(Integer language) {
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
        reportDAO = new ReportDAO();
    }

    /*private void loadImage(Image imgTemp) {
        try {
            MediaTracker tracker = new MediaTracker(this);
            tracker.addImage(imgTemp, 0);
            tracker.waitForID(0);
        } catch (Exception e) {
        }
    }*/

    public Image composeBluemodelImage(int intWoundProfileId, boolean blnIncludeUnsaved) {
        BufferedImage bimComposedBluemodel = null;
        try {
            // Retrieve Wound Profile to get the correct blue man image
            com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
            woundProfile.setWound_id(intWoundProfileId);
            woundProfile.setCurrent_flag(1);
            Collection lcol1 = null;
            WoundProfileDAO woundProfileDB = new WoundProfileDAO();
            WoundAssessmentLocationDAO woundAssessmentLocationDB = new WoundAssessmentLocationDAO();
            lcol1 = woundProfileDB.findAllByCriteria(woundProfile, 1); // One record
            Iterator iter1 = lcol1.iterator();
            if (iter1.hasNext()) {
                WoundProfileVO wprf = (WoundProfileVO) iter1.next();
                Image imgBluemodel = ImageIO.read(new File(basepath + "images/bluemodel/" + wprf.getWound().getImage_name()));
                //loadImage(imgBluemodel);
                bimComposedBluemodel = new BufferedImage(327, 266, BufferedImage.TYPE_INT_RGB);
                Graphics2D g2dComposedBluemodel = bimComposedBluemodel.createGraphics();
                //Load the original image into the BufferedImage.
                g2dComposedBluemodel.drawImage(imgBluemodel, 0, 0, this);
                Graphics grp = bimComposedBluemodel.getGraphics();
                // Remove the blue Left/Right bar from the Blue model image
                grp.setColor(Color.white); // Set background
                grp.fillRect(0, 0, 327, 17);
                WoundAssessmentLocationVO woundAssessmentLocation = new WoundAssessmentLocationVO();
                woundAssessmentLocation.setWound_id(intWoundProfileId);
                if (blnIncludeUnsaved == false) {
                    woundAssessmentLocation.setActive(1); // Only submitted alpha's based on boolean
                }
                Collection lcol2 = null;
                lcol2 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation, 0, false, "");//4th parameter (extra sort) is ignored
                Iterator iter2 = lcol2.iterator();
                while (iter2.hasNext()) {
                    WoundAssessmentLocationVO wal = (WoundAssessmentLocationVO) iter2.next();
                    int intXBox = 0;//details[intIcons].getBox() - 1;
                    int intYBox = 0;
                    intXBox = wal.getX();
                    intYBox = wal.getY() + 50;
                    Color color = Color.YELLOW;
                    Font font = new Font("Arial", Font.BOLD, 16);
                    g2dComposedBluemodel.setFont(font);
                    String strColourCode = "y"; // Yellow for active alpha's
                    if (wal.getDischarge() == 1) {
                        color = Color.GREEN;
                        strColourCode = "g"; // Green for closed alpha's
                    }
                    if (wal.getActive() == 0) {
                        color = Color.RED;
                        strColourCode = "r"; // Red for un-saved alpha's
                        // An image name is e.g. "a_y.gif"; "a" is tha alpha, "y" is the colour (y=yellow, r=red, g=groen)
                    }
                    if (wal.getAlpha().length() == 1 || wal.getAlpha().toLowerCase().indexOf("tag") != -1) {
                        g2dComposedBluemodel.setColor(color);
                        if (wal.getAlpha().length() == 1) {
                            g2dComposedBluemodel.drawString(wal.getAlpha(), intXBox, intYBox);
                        } else {
                            String alpha = wal.getAlpha().substring(2, wal.getAlpha().length());
                            g2dComposedBluemodel.drawString(alpha, intXBox, intYBox);
                        }
                    } else {
                        Image alphaImage = ImageIO.read(new File(basepath + "images/Alphas/" + wal.getAlpha().toLowerCase() + "_" + strColourCode + ".gif"));
                        //loadImage(alphaImage);
                        //Load the alpha image into the BufferedImage
                        g2dComposedBluemodel.drawImage(alphaImage, intXBox, intYBox, this);
                    }
                    //System.out.println("Drawing image");
                    if (wal.getAlpha().equals("incs")) {
                        WoundLocationDetailsVO[] details = wal.getAlpha_details();
                        for (int intIcons = 0; intIcons < details.length; intIcons++) {
                            intXBox = 0;//details[intIcons].getBox() - 1;
                            intYBox = 0;
                            intXBox = details[intIcons].getX();
                            intYBox = details[intIcons].getY() + 50;
                            Image alphaImage = ImageIO.read(new File(basepath + "images/Alphas/" + details[intIcons].getImage().toLowerCase() + ".gif"));
                            //loadImage(alphaImage);
                            //Load the alpha image into the BufferedImage
                            g2dComposedBluemodel.drawImage(alphaImage, intXBox, intYBox, this);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bimComposedBluemodel;
    }

    public ReportBuilder(ProfessionalVO userVO, int output, HttpServletRequest request, HttpServletResponse response, Integer language) {
        this.response = response;
        this.request = request;

        this.language = language;
        this.locale = Common.getLanguageLocale(language);

        reportDAO = new ReportDAO();
        this.output = output;
        this.userVO = userVO;
        ServletContext servletContext = (request != null ? request.getSession().getServletContext() : null);
        absolutepath = Common.getPhotosPath();
        if (servletContext != null) {
            basepath = Common.getBasePath(servletContext);
        }
    }

    public ReportBuilder(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public ReportBuilder(int output, ServletContext servletContext) {
        this.response = null;
        this.request = null;
        reportDAO = new ReportDAO();
        this.output = output;
        this.userVO = null;
        this.servletContext = servletContext;
        absolutepath = Common.getPhotosPath();
        if (servletContext != null) {
            basepath = Common.getBasePath(servletContext);
        }
    }

    public List<SummaryVO> organizeSummary(List<FieldValues> flowchart_rows, List<SummaryVO> summary, String page) {
        int count = 0;
        if (flowchart_rows != null) {
            for (FieldValues field : flowchart_rows) {
                if (field != null && field.getValue() != null && (!(field.getValue().trim()).equals(""))) {
                    count++;
                    if (count > 2) {
                        summary.add(new SummaryVO(page, field.getTitle(), field.getValue(), count));
                    }
                }
            }
        }
        return summary;
    }

    /**
     * Generate a CCAC Outcome Based Service report
     *
     * @param id
     * @version 5.1
     */
    public void generateCCACReport(String id) {
        if (id != null) {
            try {
                ReferralsTrackingServiceImpl rservice = new ReferralsTrackingServiceImpl();
                WoundServiceImpl wservice = new WoundServiceImpl();
                AssessmentServiceImpl aservice = new AssessmentServiceImpl();
                ListServiceImpl lservice = new ListServiceImpl();
                OutcomeBasedReport obr = new OutcomeBasedReport();
                CCACReportVO report = getCCACReport(id);
                WoundServiceImpl wpservice = new WoundServiceImpl();
                PatientProfileServiceImpl pservice = new PatientProfileServiceImpl();
                TreatmentCommentsServiceImpl tservice = new TreatmentCommentsServiceImpl();
                PatientServiceImpl paservice = new PatientServiceImpl();
                NursingCarePlanServiceImpl nservice = new NursingCarePlanServiceImpl();
                ProfessionalServiceImpl profservice = new ProfessionalServiceImpl();
                DressingChangeFrequencyVO dtmp = new DressingChangeFrequencyVO();
                dtmp.setAssessment_id(report.getAssessment_id());
                dtmp.setAlpha_id(report.getAlpha_id());
                DressingChangeFrequencyVO dcf = nservice.getDressingChangeFrequency(dtmp);

                WoundProfileVO wpt = new WoundProfileVO();
                wpt.setId(report.getWound_profile_id());
                WoundProfileVO wound = wpservice.getWoundProfile(wpt);
                LimbAdvAssessmentVO lt = new LimbAdvAssessmentVO();
                lt.setId(report.getLimb_assessment_id());
                LimbAdvAssessmentVO limb = null;
                if (report.getLimb_assessment_id() != null) {
                    limb = pservice.getLimbAdvAssessment(lt);
                } else {
                    lt.setPatient_id(report.getPatient_id());
                    limb = pservice.getLimbAdvAssessment(lt);
                }
                PatientProfileVO pt = new PatientProfileVO();
                pt.setId(report.getPatient_profile_id());
                PatientProfileVO profile = pservice.getPatientProfile(pt);

                AssessmentTreatmentVO atmp = new AssessmentTreatmentVO();
                atmp.setAlpha_id(report.getAlpha_id());
                atmp.setAssessment_id(report.getAssessment_id());
                AssessmentTreatmentVO treatment = tservice.getAssessmentTreatment(atmp);

                Collection<ArrayValueObject> treatment_arrays = treatment.getAssessment_treatment_arrays();//tservice.getAllTreatmentArray();
                WoundAssessmentLocationVO alpha = report.getAlpha();
                ProfessionalVO professional = report.getProfessional();
                Collection<GoalsVO> goals = wound.getGoals();
                Collection<EtiologyVO> etiologies = wound.getEtiologies();
                AssessmentEachwoundVO assestmp = new AssessmentEachwoundVO();
                assestmp.setActive(1);
                assestmp.setAlpha_id(alpha.getId());
                assestmp.setAssessment_id(report.getAssessment_id());
                AssessmentEachwoundVO assessment = (AssessmentEachwoundVO) aservice.getAssessment(assestmp);
                assestmp.setStatus(new Integer(Common.getConfig("woundActive")));
                AssessmentEachwoundVO lastAssessment = (AssessmentEachwoundVO) aservice.getAssessment(assestmp);
                String etiology = "";
                String location = "";
                String goal = "";
                boolean arterial = false;
                boolean venous = false;
                boolean malignant = false;
                boolean diabetic = false;
                boolean pressure = false;
                boolean trauma = false;
                boolean maintenance = false;
                int num_wounds = 0;
                for (EtiologyVO etio : etiologies) {
                    if (etio.getAlpha_id().equals(alpha.getId())) {

                        if (Arrays.asList(Common.getConfig("ccacArterialLegUlcer").split(",")).contains(etio.getLookup_id() + "")) {
                            arterial = true;
                        } else if (Arrays.asList(Common.getConfig("ccacVenousLegUlcer").split(",")).contains(etio.getLookup_id() + "")) {
                            venous = true;
                        } else if (Arrays.asList(Common.getConfig("ccacMalignant").split(",")).contains(etio.getLookup_id() + "")) {
                            malignant = true;
                        } else if (Arrays.asList(Common.getConfig("ccacDiabeticFootUlcer").split(",")).contains(etio.getLookup_id() + "")) {
                            diabetic = true;
                        } else if (Arrays.asList(Common.getConfig("ccacPressureUlcer").split(",")).contains(etio.getLookup_id() + "")) {
                            pressure = true;
                        } else if (Arrays.asList(Common.getConfig("ccacTrauma").split(",")).contains(etio.getLookup_id() + "")) {
                            trauma = true;
                        }
                        etiology = etio.getPrintable(language);
                        for (EtiologyVO other : etiologies) {
                            if (!other.getAlpha_id().equals(etio.getAlpha_id())) {
                                if (other.getLookup_id().equals(etio.getLookup_id())) {
                                    num_wounds++;
                                    location = location + other.getPrintable(language);
                                }
                            }
                        }
                        break;
                    }
                }
                for (GoalsVO g : goals) {
                    if (g.getAlpha_id().equals(alpha.getId())) {
                        if (Arrays.asList(Common.getConfig("ccacMaintenance").split(",")).contains(g.getLookup_id() + "")) {
                            maintenance = true;
                        }
                        goal = g.getPrintable(language);
                        break;
                    }
                }
                obr.setInitial_visit(PDate.getDateTime(assessment.getAlphaLocation().getOpen_timestamp()));
                obr.setChange_status(report.getWound_status());
                obr.setType_of_report(report.getType_of_report());
                obr.setNurse_name(professional.getFullName());
                obr.setPatient_name(report.getPatient().getPatientName());
                //obr.setDate(report.getTime_stamp().toString());
                obr.setAgency(Common.getConfig("customer_name"));
                obr.setPhn(report.getPatient().getPhn());
                obr.setWeek(report.getWeek() + "");
                obr.setDischarge(report.getDischarge());
                obr.setCare_pathway(etiology);
                LookupVO treatmentLookup = report.getPatient().getTreatmentLocation();
                obr.setDelivery(treatmentLookup != null && treatmentLookup.getUpdate_access().equals("1") ? Common.getConfig("patient_update_access") : Common.getConfig("no_patient_update_access"));
                obr.setNum_wounds(num_wounds + "");
                obr.setPathway_locations(location);
                obr.setDcf(dcf.getDressingChangeFrequency().getName(language));
                obr.setPathway_confirmed(report.getCorrect_pathway_confirmed());
                obr.setChronic_disease(profile.getChronic_disease_initiated() == 1 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale));
                obr.setDischarge_planning(profile.getDischarge_planning_initiated() == 1 ? Common.getLocalizedString("pixalere.yes", locale) : Common.getLocalizedString("pixalere.no", locale));
                obr.setDate(PDate.getDate(PDate.getEpochTime()));
                //obr.setSuitable_transfer(report.getSuitable_transfer());
                //obr.setTransfer_date(report.getDate_of_transfer().toString());
                String ref = "";
                for (ExternalReferralVO et : profile.getExternalReferrals()) {
                    LookupVO arr = lservice.getListItem(et.getArranged_id());
                    LookupVO referral = lservice.getListItem(et.getExternal_referral_id());
                    if (arr != null && referral != null) {
                        ref = ref + ", " + arr.getName(language) + " : " + referral.getName(language);
                    }
                }

                obr.setCommunity_referrals(ref);
                ReferralsTrackingVO refTMP = new ReferralsTrackingVO();
                refTMP.setWound_id(report.getWound_id());
                refTMP.setWound_profile_type_id(report.getAlpha().getWound_profile_type_id());
                Collection<ReferralsTrackingVO> refs = rservice.getAllReferralsByCriteria(refTMP);
                String pix_referrals = "";
                for (ReferralsTrackingVO r : refs) {
                    if (r.getEntry_type().equals(Constants.REFERRAL)) {
                        ProfessionalVO p = profservice.getProfessional(r.getProfessional_id());
                        pix_referrals = pix_referrals + ", " + p.getPosition().getTitle() + ": " + PDate.getDate(r.getCreated_on(), false);
                    }
                }
                obr.setReferrals(pix_referrals);
                obr.setComments(report.getComments());
                obr.setSuitable_transfer(report.getSuitable_transfer());
                obr.setTransfer_date(PDate.getDate(report.getDate_of_transfer(), false));
                if (assessment.getAlphaLocation().getDischarge().equals(1)) {
                    obr.setDischarge(PDate.getDate(assessment.getAlphaLocation().getClose_timestamp(), false));
                    obr.setDischarge_disposition(lservice.getListItem(assessment.getDischarge_reason()).getName(language));
                    obr.setLast_visit(PDate.getDate(lastAssessment.getCreated_on(), false));
                }
                // obr.setCorrect_pathway_no(report.getC);
                obr.setHealability(goal);
                if (assessment != null && assessment.getTarget_discharge_date() != null) {
                    obr.setTarget_discharge_date(PDate.getDate(assessment.getTarget_discharge_date(), false));
                }
                obr.setDimensions(assessment.getLength() + " x " + assessment.getWidth() + " x " + assessment.getDepth());
                obr.setDimension_progress(report.getWound_progress());
                obr.setChange_in_wound(report.getChange_in_wound());
                obr.setWound_change_reason(report.getWound_change_comments());
                obr.setUndermining(assessment.getUnderminings().toString());
                for (ArrayValueObject obj : treatment_arrays) {
                    if (Arrays.asList(Common.getConfig("ccacWoundTherapyInitiated").split(",")).contains(obj.getLookup_id() + "")) {

                        obr.setWound_therapy_initiated(Common.getLocalizedString("pixalere.yes", locale));
                    }
                    if (Arrays.asList(Common.getConfig("ccacWoundTherapyReassessed").split(",")).contains(obj.getLookup_id() + "")) {
                        obr.setWound_therapy_reassessed(Common.getLocalizedString("pixalere.yes", locale));
                    }

                }
                if (limb != null) {
                    if (arterial == true) {
                        obr.setArterial_lower_limb(Common.getLocalizedString("pixalere.yes", locale));
                        obr.setArterial_abpi_date(PDate.getDate(limb.getAnkle_brachial_date(), false));
                        obr.setReferral_vascular(limb.getReferral_vascular_assessment() != null && limb.getReferral_vascular_assessment() == 1 ? Common.getLocalizedString("pixalere.yes", locale) : "");
                    } else {
                        obr.setArterial_lower_limb(Common.getLocalizedString("pixalere.no", locale));
                    }
                    if (venous == true) {
                        obr.setVenous_lower_limb(Common.getLocalizedString("pixalere.yes", locale));
                        obr.setVenous_abpi_date(PDate.getDate(limb.getAnkle_brachial_date(), false));
                        for (ArrayValueObject obj : treatment_arrays) {
                            if (Arrays.asList(Common.getConfig("ccacCompressionTherapyInitiated").split(",")).contains(obj.getLookup_id() + "")) {
                                obr.setCompression_therapy(Common.getLocalizedString("pixalere.yes", locale));
                            }
                            if (Arrays.asList(Common.getConfig("ccacLongTermCompressionSystem").split(",")).contains(obj.getLookup_id() + "")) {
                                obr.setLongterm_compression(Common.getLocalizedString("pixalere.yes", locale));
                            }
                            if (Arrays.asList(Common.getConfig("ccacClientIndependent").split(",")).contains(obj.getLookup_id() + "")) {
                                obr.setClient_independent(Common.getLocalizedString("pixalere.yes", locale));
                            }
                        }
                    } else {
                        obr.setVenous_lower_limb(Common.getLocalizedString("pixalere.no", locale));
                    }
                    if (diabetic == true) {
                        obr.setDiabetic_lower_limb(Common.getLocalizedString("pixalere.yes", locale));
                        obr.setDiabetic_abpi_date(PDate.getDate(limb.getAnkle_brachial_date(), false));
                        for (ArrayValueObject obj : treatment_arrays) {
                            if (Arrays.asList(Common.getConfig("ccacReferralLongTermPressure").split(",")).contains(obj.getLookup_id() + "")) {
                                obr.setLongterm_pressure(Common.getLocalizedString("pixalere.yes", locale));
                            }
                            if (Arrays.asList(Common.getConfig("ccacAdheringPressureRedistribution").split(",")).contains(obj.getLookup_id() + "")) {
                                //obr.set(Common.getLocalizedString("pixalere.yes"));
                            }
                            if (Arrays.asList(Common.getConfig("ccacPressureRedistributionInitiated").split(",")).contains(obj.getLookup_id() + "")) {
                                obr.setPressure_redistribi(Common.getLocalizedString("pixalere.yes", locale));
                            }
                        }
                    } else {
                        obr.setDiabetic_lower_limb(Common.getLocalizedString("pixalere.no", locale));
                    }
                    if (pressure == true) {
                        for (ArrayValueObject obj : treatment_arrays) {
                            if (Arrays.asList(Common.getConfig("ccacRedistributionMeasures").split(",")).contains(obj.getLookup_id() + "")) {
                                obr.setRedistribution_measures(Common.getLocalizedString("pixalere.yes", locale));
                            }
                        }
                        obr.setPressure_ulcer(etiology);
                    }
                    if (malignant == true) {
                        for (ArrayValueObject obj : treatment_arrays) {
                            if (Arrays.asList(Common.getConfig("ccacWoundRelatedSymptoms").split(",")).contains(obj.getLookup_id() + "")) {
                                obr.setWound_related_symptoms(Common.getLocalizedString("pixalere.yes", locale));
                            }
                        }
                        obr.setQuality_of_life(profile.getQuality_of_life_addressed() != null && profile.getQuality_of_life_addressed() == 1 ? Common.getLocalizedString("pixalere.yes", locale) : "");
                        obr.setQol_no(profile.getQuality_of_life_addressed_comments());
                    }
                    if (maintenance == true) {
                        //obr.setChange_in_wound();

                        obr.setBarriers_addressed(profile.getBarriers_addressed() != null && profile.getBarriers_addressed() == 1 ? Common.getLocalizedString("pixalere.yes", locale) : "");
                        obr.setBarriers_addressed_comments(profile.getBarriers_addressed_comments());
                    }
                    if (trauma == true) {
                        obr.setRoot_cause_addressed(wound.getRoot_cause_addressed() != null && wound.getRoot_cause_addressed() == 1 ? Common.getLocalizedString("pixalere.yes", locale) : "");
                        obr.setRoot_cause_addressed_no(wound.getRoot_cause_addressed_comments());
                    }
                }

                obr.setSystem_barriers(profile.getSystem_barriers_comments());
                if (profile != null) {
                    for (PatientProfileArraysVO obj : profile.getPatient_profile_arrays()) {
                        if (Arrays.asList(Common.getConfig("ccacNotAdherentCarePlan").split(",")).contains(obj.getLookup_id() + "")) {
                            obr.setNon_adherent_care_plan(Common.getLocalizedString("pixalere.yes", locale));
                        }
                        if (Arrays.asList(Common.getConfig("ccacClinical").split(",")).contains(obj.getLookup_id() + "")) {
                            obr.setClinical_therapy(obj.getLookup().getName(language) + "," + obr.getClinical_therapy());

                        }
                        if (Arrays.asList(Common.getConfig("ccacSystemBarriers").split(",")).contains(obj.getLookup_id() + "")) {
                            obr.setSystem_barriers(obj.getLookup().getName(language) + "," + obr.getSystem_barriers());

                        }
                        obr.setSystem_barriers(obr.getSystem_barriers() + "; " + profile.getSystem_barriers_comments());
                        if (obj.getOther() != null || !obj.getOther().equals("")) {
                            obr.setClinical_therapy(obr.getClinical_therapy() + "," + obj.getOther());
                        }
                        if (obj.getLookup().getResourceId().equals(LookupVO.PATIENT_LIMITATIONS)) {
                            obr.setPsychosocial_risk_factors(obj.getLookup().getName(language) + "," + obr.getPsychosocial_risk_factors());
                        }

                    }
                }

                //obr.setClinical_risk_factors();
                List list = new ArrayList();
                list.add(obr);
                buildReport("ccac_report", list, null, null);

            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Generate's a PDF summary report of all patient data done on the given day
     * of the assessment.
     *
     * @param id
     */
    public void generatePDFSummary(String id) {
        if (id != null) {
            try {
                List<SummaryVO> summaryOverview = new ArrayList();
                int id_int = Integer.parseInt(id);
                WoundAssessmentServiceImpl wservice = new WoundAssessmentServiceImpl();
                WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
                NursingCarePlanServiceImpl ncpservice = new NursingCarePlanServiceImpl();
                AssessmentCommentsServiceImpl cservice = new AssessmentCommentsServiceImpl();
                AssessmentImagesServiceImpl iservice = new AssessmentImagesServiceImpl();
                AssessmentServiceImpl aeservice = new AssessmentServiceImpl(language);
                PatientProfileServiceImpl pservice = new PatientProfileServiceImpl(language);
                WoundServiceImpl woundservice = new WoundServiceImpl(language);
                PatientServiceImpl paservice = new PatientServiceImpl(language);
                WoundAssessmentVO tmp = new WoundAssessmentVO();
                tmp.setId(id_int);
                WoundAssessmentVO wassess = wservice.getAssessment(tmp);
                if (wassess != null && wassess.getCreated_on() != null) {
                    //Get Blueman.
                    parameters.put("blueman", composeBluemodelImage(wassess.getWound_id(), false));
                    // get patient_profiles in date range.
                    Collection<PatientProfileVO> patients = pservice.getPatientProfileFromDate(wassess.getPatient_id(), wassess.getCreated_on(), wassess.getProfessional_id());
                    for (PatientProfileVO patient : patients) {
                        List<FieldValues> compared = null;
                        Collection<PatientProfileVO> results = new ArrayList();
                        if (patient != null) {
                            PatientProfileVO oldPP = pservice.getPatientProfileBeforeDate(wassess.getPatient_id(), wassess.getCreated_on());
                            results.add(oldPP);
                            results.add(patient);
                            RowData[] faRecord = pservice.getAllPatientProfilesForFlowchart(results, wassess.getProfessional(), false, true);
                            compared = Common.compareGlobal(faRecord);
                            summaryOverview = organizeSummary(compared, summaryOverview, Common.getLocalizedString("pixalere.alert.patientprofile", locale) + " " + patient.getUser_signature());
                        }
                    }
                    // get wound_profiles in date range.
                    Collection<PhysicalExamVO> ws = pservice.getExamFromDate(wassess.getPatient_id(), wassess.getCreated_on(), wassess.getProfessional_id());
                    for (PhysicalExamVO w : ws) {
                        List<FieldValues> compared = null;
                        Collection<PhysicalExamVO> results = new ArrayList();
                        if (w != null) {
                            PhysicalExamVO oldPP = pservice.getExamBeforeDate(wassess.getPatient_id(), wassess.getCreated_on());
                            results.add(oldPP);
                            results.add(w);
                            RowData[] faRecord = pservice.getAllExamsForFlowchart(results, wassess.getProfessional(), false);
                            compared = Common.compareGlobal(faRecord);
                            summaryOverview = organizeSummary(compared, summaryOverview, Common.getLocalizedString("pixalere.alert.exam", locale) + " " + w.getUser_signature());
                        }
                    }
                    // get wound_profiles in date range.
                    Collection<BradenVO> bs = pservice.getBradenFromDate(wassess.getPatient_id(), wassess.getCreated_on(), wassess.getProfessional_id());
                    for (BradenVO w : bs) {
                        List<FieldValues> compared = null;
                        Collection<BradenVO> results = new ArrayList();
                        if (w != null) {
                            BradenVO oldPP = pservice.getBradenBeforeDate(wassess.getPatient_id(), wassess.getCreated_on());
                            results.add(oldPP);
                            results.add(w);
                            RowData[] faRecord = pservice.getAllBradenForFlowchart(results, wassess.getProfessional(), false);
                            compared = Common.compareGlobal(faRecord);
                            summaryOverview = organizeSummary(compared, summaryOverview, Common.getLocalizedString("pixalere.alert.braden", locale) + " " + w.getUser_signature());
                        }
                    }
                    // get wound_profiles in date range.
                    Collection<LimbBasicAssessmentVO> ls = pservice.getLimbBasicFromDate(wassess.getPatient_id(), wassess.getCreated_on(), wassess.getProfessional_id());
                    for (LimbBasicAssessmentVO w : ls) {
                        List<FieldValues> compared = null;
                        Collection<LimbBasicAssessmentVO> results = new ArrayList();
                        if (w != null) {
                            LimbBasicAssessmentVO oldPP = pservice.getLimbBasicBeforeDate(wassess.getPatient_id(), wassess.getCreated_on());
                            results.add(oldPP);
                            results.add(w);
                            RowData[] faRecord = pservice.getAllBasicLimbAssessmentsForFlowchart(results, wassess.getProfessional(), false);
                            compared = Common.compareGlobal(faRecord);
                            summaryOverview = organizeSummary(compared, summaryOverview, Common.getLocalizedString("pixalere.alert.limb", locale) + " " + w.getUser_signature());
                        }
                    }
                    // get wound_profiles in date range.
                    Collection<LimbAdvAssessmentVO> lsa = pservice.getLimbAdvFromDate(wassess.getPatient_id(), wassess.getCreated_on(), wassess.getProfessional_id());
                    for (LimbAdvAssessmentVO w : lsa) {
                        List<FieldValues> compared = null;
                        Collection<LimbAdvAssessmentVO> results = new ArrayList();
                        if (w != null) {
                            LimbAdvAssessmentVO oldPP = pservice.getLimbAdvBeforeDate(wassess.getPatient_id(), wassess.getCreated_on());
                            results.add(oldPP);
                            results.add(w);
                            RowData[] faRecord = pservice.getAllAdvLimbAssessmentsForFlowchart(results, wassess.getProfessional(), false);
                            compared = Common.compareGlobal(faRecord);
                            summaryOverview = organizeSummary(compared, summaryOverview, Common.getLocalizedString("pixalere.alert.limb", locale) + " " + w.getUser_signature());
                        }
                    }
                    // get wound_profiles in date range.
                    Collection<FootAssessmentVO> fs = pservice.getFootFromDate(wassess.getPatient_id(), wassess.getCreated_on(), wassess.getProfessional_id());
                    for (FootAssessmentVO w : fs) {
                        List<FieldValues> compared = null;
                        Collection<FootAssessmentVO> results = new ArrayList();
                        if (w != null) {
                            FootAssessmentVO oldPP = pservice.getFootBeforeDate(wassess.getPatient_id(), wassess.getCreated_on());
                            results.add(oldPP);
                            results.add(w);
                            RowData[] faRecord = pservice.getAllFootAssessmentsForFlowchart(results, wassess.getProfessional(), false);
                            compared = Common.compareGlobal(faRecord);
                            summaryOverview = organizeSummary(compared, summaryOverview, Common.getLocalizedString("pixalere.alert.foot", locale) + " " + w.getUser_signature());
                        }
                    }
                    // get wound_profiles in date range.
                    Collection<WoundProfileVO> wps = woundservice.getWoundProfileFromDate(wassess.getPatient_id(), wassess.getCreated_on(), wassess.getProfessional_id());
                    for (WoundProfileVO w : wps) {
                        List<FieldValues> compared = null;
                        Collection<WoundProfileVO> results = new ArrayList();
                        if (w != null) {
                            WoundProfileVO oldPP = woundservice.getWoundProfileBeforeDate(wassess.getPatient_id(), wassess.getCreated_on());
                            results.add(oldPP);
                            results.add(w);
                            RowData[] faRecord = woundservice.getAllWoundProfilesForFlowchart(results, wassess.getProfessional(), false, true);
                            compared = Common.compareGlobal(faRecord);
                            summaryOverview = organizeSummary(compared, summaryOverview, Common.getLocalizedString("pixalere.alert.woundprofile", locale) + " " + w.getUser_signature());
                        }
                    }
                    WoundAssessmentVO watmp = new WoundAssessmentVO();
                    watmp.setPatient_id(wassess.getPatient_id());
                    watmp.setWound_id(wassess.getWound_id());
                    watmp.setActive(1);
                    Collection<WoundAssessmentVO> wound_assessments = wservice.getAllAssessmentsByDateRange(watmp, PDate.getStartOfDay(wassess.getCreated_on()), PDate.getEndOfDay(wassess.getCreated_on()));
                    for (WoundAssessmentVO assess : wound_assessments) {
                        ProfessionalVO userVO = new ProfessionalVO();
                        userVO.setId(assess.getProfessional_id());
                        RowData[] woundAssessments;
                        RowData[] ostomyAssessments;
                        RowData[] incisionAssessments;
                        RowData[] drainAssessments;
                        RowData[] burnAssessments;
                        RowData[] skinAssessments;
                        AssessmentEachwoundVO assWound = new AssessmentEachwoundVO();
                        assWound.setAssessment_id(assess.getId());
                        AssessmentOstomyVO assOst = new AssessmentOstomyVO();
                        assOst.setAssessment_id(assess.getId());
                        AssessmentIncisionVO assIn = new AssessmentIncisionVO();
                        assIn.setAssessment_id(assess.getId());
                        AssessmentDrainVO assDr = new AssessmentDrainVO();
                        assDr.setAssessment_id(assess.getId());
                        AssessmentBurnVO assBn = new AssessmentBurnVO();
                        assBn.setAssessment_id(assess.getId());
                        AssessmentSkinVO assSn = new AssessmentSkinVO();
                        assSn.setAssessment_id(assess.getId());
                        Collection<AssessmentEachwoundVO> ae = aeservice.getAllAssessments(assWound, -1, Constants.ASC_ORDER);
                        Collection<AssessmentOstomyVO> oe = aeservice.getAllAssessments(assOst, -1, Constants.ASC_ORDER);
                        Collection<AssessmentIncisionVO> ie = aeservice.getAllAssessments(assIn, -1, Constants.ASC_ORDER);
                        Collection<AssessmentDrainVO> de = aeservice.getAllAssessments(assDr, -1, Constants.ASC_ORDER);
                        Collection<AssessmentBurnVO> be = aeservice.getAllAssessments(assBn, -1, Constants.ASC_ORDER);
                        Collection<AssessmentSkinVO> se = aeservice.getAllAssessments(assSn, -1, Constants.ASC_ORDER);
                        woundAssessments = aeservice.getAllAssessmentsForFlowchart(ae, userVO, false);
                        ostomyAssessments = aeservice.getAllOstomyAssessmentsForFlowchart(oe, userVO, false);
                        incisionAssessments = aeservice.getAllIncisionAssessmentsForFlowchart(ie, userVO, false);
                        drainAssessments = aeservice.getAllDrainAssessmentsForFlowchart(de, userVO, false);
                        burnAssessments = aeservice.getAllBurnAssessmentsForFlowchart(be, userVO, false);
                        skinAssessments = aeservice.getAllSkinAssessmentsForFlowchart(se, userVO, false);
                        for (RowData row : woundAssessments) {
                            WoundAssessmentLocationVO tmpAlpha = new WoundAssessmentLocationVO();
                            tmpAlpha.setId(row.getAlpha_id());
                            WoundAssessmentLocationVO alpha = walservice.getAlpha(tmpAlpha);
                            AssessmentEachwoundVO t = new AssessmentEachwoundVO();
                            t.setId(row.getId());
                            AssessmentEachwoundVO each = (AssessmentEachwoundVO) aeservice.getAssessment(t);
                            //String page, String title, String value, Integer order
                            List<FieldValues> fields = row.getFields();
                            int count = 0;
                            for (FieldValues field : fields) {
                                count++;
                                //System.out.println(field.getTitle()+" "+field.getValue());
                                // FIXME: Locate all the places where this values are used and set in order to do i18n
                                if ((!(field.getValue()).equals(""))
                                        && !(field.getTitle().equals("Printable Summary Report"))
                                        && !(field.getTitle().equals("Assessment Updates:"))
                                        && !(field.getTitle().equals("Assessment Images"))
                                        && !(field.getTitle().equals("Request for Referral"))
                                        && !(field.getTitle().equals("See Progress Notes")) && count > 3) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(
                                                    alpha.getAlpha(),
                                                    locale) + " - " + row.getUser_signature(),
                                            field.getTitle(),
                                            field.getValue(),
                                            count));
                                }
                            }
                            if (each != null) {
                                DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                                d.setAssessment_id(each.getAssessment_id());
                                d.setAlpha_id(each.getAlpha_id());
                                d.setActive(1);
                                DressingChangeFrequencyVO dd = ncpservice.getDressingChangeFrequency(d);
                                if (dd != null && dd.getDcf() != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.dressing_change_frequency", locale)
                                            + Common.getAlphaText(alpha.getAlpha(), locale),
                                            Common.getListValue(dd.getDcf().intValue(), language),
                                            count + 1));
                                }
                                //get Nursing care Plans..
                                NursingCarePlanVO ntmp = new NursingCarePlanVO();
                                ntmp.setAssessment_id(each.getAssessment_id());
                                ntmp.setWound_profile_type_id(each.getWound_profile_type_id());
                                ntmp.setActive(1);
                                NursingCarePlanVO careplan = ncpservice.getNursingCarePlan(ntmp);
                                if (careplan != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.nursing_care_plan", locale),
                                            careplan.getBody(),
                                            count + 2));
                                    if (careplan.getVisitFrequency() != null) {
                                        summaryOverview.add(new SummaryVO(
                                                Common.getAlphaText(alpha.getAlpha(), locale)
                                                + " - "
                                                + row.getUser_signature(),
                                                Common.getLocalizedReportString("pixalere.reports.summary.visit_frequency", locale),
                                                careplan.getVisitFrequency().getName(language),
                                                count + 3));
                                    }
                                }
                                //get Comments.. get end_date for comment range.
                                WoundAssessmentVO nextAssessment = wservice.getNextAssessment(assess.getId());
                                WoundAssessmentVO prevAssessment = wservice.getPrevAssessment(assess.getId());
                                Collection<AssessmentCommentsVO> comments = cservice.getAllComments((prevAssessment != null ? prevAssessment.getId() + "" : null), (nextAssessment != null ? nextAssessment.getId() + "" : null), each.getWound_profile_type_id());
                                int subcount = 0;
                                for (AssessmentCommentsVO comment : comments) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.assessment_comments", locale)
                                            + (each.getWound_profile_type() != null ? Common.getWoundProfileType(each.getWound_profile_type().getAlpha_type(), locale) : ""),
                                            comment.getUser_signature(),
                                            comment.getBody(),
                                            count + 4 + subcount));
                                    subcount++;
                                }
                                //Get all comments done before next assessment.
                                AssessmentImagesVO tmpimags = new AssessmentImagesVO();
                                tmpimags.setAlpha_id(each.getAlpha_id());
                                tmpimags.setAssessment_id(each.getAssessment_id());
                                Collection<AssessmentImagesVO> ph = iservice.getAllImages(tmpimags);
                                List<WoundPhotosVO> photos = new ArrayList();
                                for (AssessmentImagesVO photo : ph) {
                                    String strImageFile = Common.getConfig("photos_path") + "/" + Common.getConfig("customer_id") + "/" + photo.getPatient_id() + "/" + photo.getAssessment_id() + "/" + photo.getImages();
                                    photos.add(new WoundPhotosVO(Common.getAlphaText(alpha.getAlpha(), locale) + " - Image " + photo.getWhichImage(), strImageFile));
                                }
                                if (photos != null && photos.size() > 0) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.images", locale),
                                            "",
                                            "",
                                            count + 6,
                                            photos));
                                }
                            }
                        }
                        for (RowData row : ostomyAssessments) {
                            WoundAssessmentLocationVO tmpAlpha = new WoundAssessmentLocationVO();
                            tmpAlpha.setId(row.getAlpha_id());
                            WoundAssessmentLocationVO alpha = walservice.getAlpha(tmpAlpha);
                            AssessmentOstomyVO t = new AssessmentOstomyVO();
                            t.setId(row.getId());
                            AssessmentOstomyVO each = (AssessmentOstomyVO) aeservice.getAssessment(t);
                            //String page, String title, String value, Integer order
                            List<FieldValues> fields = row.getFields();
                            int count = 0;
                            for (FieldValues field : fields) {
                                count++;
                                // FIXME: Locate all the places where this values are used and set in order to do i18n
                                if ((!(field.getValue()).equals(""))
                                        && !(field.getTitle().equals("Printable Summary Report"))
                                        && !(field.getTitle().equals("Assessment Updates:"))
                                        && !(field.getTitle().equals("Assessment Images"))
                                        && !(field.getTitle().equals("Request for Referral"))
                                        && !(field.getTitle().equals("See Progress Notes")) && count > 3) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            field.getTitle(),
                                            field.getValue(),
                                            count));
                                }
                            }
                            if (each != null) {
                                DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                                d.setAssessment_id(each.getAssessment_id());
                                d.setAlpha_id(each.getAlpha_id());
                                d.setActive(1);
                                DressingChangeFrequencyVO dd = ncpservice.getDressingChangeFrequency(d);
                                if (dd != null && dd.getDcf() != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.dressing_change_frequency", locale)
                                            + Common.getAlphaText(alpha.getAlpha(), locale),
                                            Common.getListValue(dd.getDcf().intValue(), language),
                                            count + 1));
                                }
                                //get Nursing care Plans..
                                NursingCarePlanVO ntmp = new NursingCarePlanVO();
                                ntmp.setAssessment_id(each.getAssessment_id());
                                ntmp.setWound_profile_type_id(each.getWound_profile_type_id());
                                ntmp.setActive(1);
                                NursingCarePlanVO careplan = ncpservice.getNursingCarePlan(ntmp);
                                if (careplan != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.nursing_care_plan", locale),
                                            careplan.getBody(),
                                            count + 2));
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.visit_frequency", locale),
                                            careplan.getVisitFrequency().getName(language),
                                            count + 3));
                                }
                                //get Comments.. get end_date for comment range.
                                WoundAssessmentVO nextAssessment = wservice.getNextAssessment(assess.getId());
                                WoundAssessmentVO prevAssessment = wservice.getPrevAssessment(assess.getId());
                                Collection<AssessmentCommentsVO> comments = cservice.getAllComments((prevAssessment != null ? prevAssessment.getId() + "" : null), (nextAssessment != null ? nextAssessment.getId() + "" : null), each.getWound_profile_type_id());
                                int subcount = 0;
                                for (AssessmentCommentsVO comment : comments) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.assessment_comments", locale)
                                            + (each.getWound_profile_type() != null ? Common.getWoundProfileType(each.getWound_profile_type().getAlpha_type(), locale) : ""),
                                            comment.getUser_signature(),
                                            comment.getBody(),
                                            count + 4 + subcount));
                                    subcount++;
                                }
                                //Get all comments done before next assessment.
                                AssessmentImagesVO tmpimags = new AssessmentImagesVO();
                                tmpimags.setAlpha_id(each.getAlpha_id());
                                tmpimags.setAssessment_id(each.getAssessment_id());
                                Collection<AssessmentImagesVO> ph = iservice.getAllImages(tmpimags);
                                List<WoundPhotosVO> photos = new ArrayList();
                                for (AssessmentImagesVO photo : ph) {
                                    String strImageFile = Common.getConfig("photos_path") + "/" + Common.getConfig("customer_id") + "/" + photo.getPatient_id() + "/" + photo.getAssessment_id() + "/" + photo.getImages();
                                    photos.add(new WoundPhotosVO(Common.getAlphaText(alpha.getAlpha(), locale) + " - Image " + photo.getWhichImage(), strImageFile));
                                }
                                if (photos != null && photos.size() > 0) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.images", locale),
                                            "",
                                            "",
                                            count + 6,
                                            photos));
                                }
                            }
                        }
                        for (RowData row : skinAssessments) {
                            WoundAssessmentLocationVO tmpAlpha = new WoundAssessmentLocationVO();
                            tmpAlpha.setId(row.getAlpha_id());
                            WoundAssessmentLocationVO alpha = walservice.getAlpha(tmpAlpha);
                            AssessmentSkinVO t = new AssessmentSkinVO();
                            t.setId(row.getId());
                            AssessmentSkinVO each = (AssessmentSkinVO) aeservice.getAssessment(t);
                            //String page, String title, String value, Integer order
                            List<FieldValues> fields = row.getFields();
                            int count = 0;
                            for (FieldValues field : fields) {
                                count++;
                                // FIXME: Locate all the places where this values are used and set in order to do i18n
                                if ((!(field.getValue()).equals(""))
                                        && !(field.getTitle().equals("Printable Summary Report"))
                                        && !(field.getTitle().equals("Assessment Updates:"))
                                        && !(field.getTitle().equals("Assessment Images"))
                                        && !(field.getTitle().equals("Request for Referral"))
                                        && !(field.getTitle().equals("See Progress Notes")) && count > 3) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            field.getTitle(),
                                            field.getValue(),
                                            count));
                                }
                            }
                            if (each != null) {
                                DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                                d.setAssessment_id(each.getAssessment_id());
                                d.setAlpha_id(each.getAlpha_id());
                                d.setActive(1);
                                DressingChangeFrequencyVO dd = ncpservice.getDressingChangeFrequency(d);
                                if (dd != null && dd.getDcf() != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.dressing_change_frequency", locale)
                                            + Common.getAlphaText(alpha.getAlpha(), locale),
                                            Common.getListValue(dd.getDcf().intValue(), language),
                                            count + 1));
                                }
                                //get Nursing care Plans..
                                NursingCarePlanVO ntmp = new NursingCarePlanVO();
                                ntmp.setAssessment_id(each.getAssessment_id());
                                ntmp.setWound_profile_type_id(each.getWound_profile_type_id());
                                ntmp.setActive(1);
                                NursingCarePlanVO careplan = ncpservice.getNursingCarePlan(ntmp);
                                if (careplan != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.nursing_care_plan", locale),
                                            careplan.getBody(),
                                            count + 2));
                                    if (careplan.getVisitFrequency() != null) {
                                        summaryOverview.add(new SummaryVO(
                                                Common.getAlphaText(alpha.getAlpha(), locale)
                                                + " - "
                                                + row.getUser_signature(),
                                                Common.getLocalizedReportString("pixalere.reports.summary.visit_frequency", locale),
                                                careplan.getVisitFrequency().getName(language),
                                                count + 3));
                                    }
                                }
                                //get Comments.. get end_date for comment range.
                                WoundAssessmentVO nextAssessment = wservice.getNextAssessment(assess.getId());
                                WoundAssessmentVO prevAssessment = wservice.getPrevAssessment(assess.getId());
                                Collection<AssessmentCommentsVO> comments = cservice.getAllComments((prevAssessment != null ? prevAssessment.getId() + "" : null), (nextAssessment != null ? nextAssessment.getId() + "" : null), each.getWound_profile_type_id());
                                int subcount = 0;
                                for (AssessmentCommentsVO comment : comments) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.assessment_comments", locale)
                                            + (each.getWound_profile_type() != null ? Common.getWoundProfileType(each.getWound_profile_type().getAlpha_type(), locale) : ""),
                                            comment.getUser_signature(),
                                            comment.getBody(),
                                            count + 4 + subcount));
                                    subcount++;
                                }
                                //Get all comments done before next assessment.
                                AssessmentImagesVO tmpimags = new AssessmentImagesVO();
                                tmpimags.setAlpha_id(each.getAlpha_id());
                                tmpimags.setAssessment_id(each.getAssessment_id());
                                Collection<AssessmentImagesVO> ph = iservice.getAllImages(tmpimags);
                                List<WoundPhotosVO> photos = new ArrayList();
                                for (AssessmentImagesVO photo : ph) {
                                    String strImageFile = Common.getConfig("photos_path") + "/" + Common.getConfig("customer_id") + "/" + photo.getPatient_id() + "/" + photo.getAssessment_id() + "/" + photo.getImages();
                                    photos.add(new WoundPhotosVO(Common.getAlphaText(alpha.getAlpha(), locale) + " - Image " + photo.getWhichImage(), strImageFile));
                                }
                                if (photos != null && photos.size() > 0) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.images", locale),
                                            "",
                                            "",
                                            count + 6,
                                            photos));
                                }
                            }
                        }
                        for (RowData row : drainAssessments) {
                            WoundAssessmentLocationVO tmpAlpha = new WoundAssessmentLocationVO();
                            tmpAlpha.setId(row.getAlpha_id());
                            WoundAssessmentLocationVO alpha = walservice.getAlpha(tmpAlpha);
                            AssessmentDrainVO t = new AssessmentDrainVO();
                            t.setId(row.getId());
                            AssessmentDrainVO each = (AssessmentDrainVO) aeservice.getAssessment(t);
                            //String page, String title, String value, Integer order
                            List<FieldValues> fields = row.getFields();
                            int count = 0;
                            for (FieldValues field : fields) {
                                count++;
                                // FIXME: Locate all the places where this values are used and set in order to do i18n
                                if ((!(field.getValue()).equals(""))
                                        && !(field.getTitle().equals("Printable Summary Report"))
                                        && !(field.getTitle().equals("Assessment Updates:"))
                                        && !(field.getTitle().equals("Assessment Images"))
                                        && !(field.getTitle().equals("Request for Referral"))
                                        && !(field.getTitle().equals("See Progress Notes")) && count > 3) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            field.getTitle(),
                                            field.getValue(),
                                            count));
                                }
                            }
                            if (each != null) {
                                DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                                d.setAssessment_id(each.getAssessment_id());
                                d.setAlpha_id(each.getAlpha_id());
                                d.setActive(1);
                                DressingChangeFrequencyVO dd = ncpservice.getDressingChangeFrequency(d);
                                if (dd != null && dd.getDcf() != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.dressing_change_frequency", locale)
                                            + Common.getAlphaText(alpha.getAlpha(), locale),
                                            Common.getListValue(dd.getDcf().intValue(), language),
                                            count + 1));
                                }
                                //get Nursing care Plans..
                                NursingCarePlanVO ntmp = new NursingCarePlanVO();
                                ntmp.setAssessment_id(each.getAssessment_id());
                                ntmp.setWound_profile_type_id(each.getWound_profile_type_id());
                                ntmp.setActive(1);
                                NursingCarePlanVO careplan = ncpservice.getNursingCarePlan(ntmp);
                                if (careplan != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.nursing_care_plan", locale),
                                            careplan.getBody(),
                                            count + 2));
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.visit_frequency", locale),
                                            careplan.getVisitFrequency().getName(language),
                                            count + 3));
                                }
                                //get Comments.. get end_date for comment range.
                                WoundAssessmentVO nextAssessment = wservice.getNextAssessment(assess.getId());
                                WoundAssessmentVO prevAssessment = wservice.getPrevAssessment(assess.getId());
                                Collection<AssessmentCommentsVO> comments = cservice.getAllComments((prevAssessment != null ? prevAssessment.getId() + "" : null), (nextAssessment != null ? nextAssessment.getId() + "" : null), each.getWound_profile_type_id());
                                int subcount = 0;
                                for (AssessmentCommentsVO comment : comments) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.assessment_comments", locale)
                                            + (each.getWound_profile_type() != null ? Common.getWoundProfileType(each.getWound_profile_type().getAlpha_type(), locale) : ""),
                                            comment.getUser_signature(),
                                            comment.getBody(),
                                            count + 4 + subcount));
                                    subcount++;
                                }
                                //Get all comments done before next assessment.
                                AssessmentImagesVO tmpimags = new AssessmentImagesVO();
                                tmpimags.setAlpha_id(each.getAlpha_id());
                                tmpimags.setAssessment_id(each.getAssessment_id());
                                Collection<AssessmentImagesVO> ph = iservice.getAllImages(tmpimags);
                                List<WoundPhotosVO> photos = new ArrayList();
                                for (AssessmentImagesVO photo : ph) {
                                    String strImageFile = Common.getConfig("photos_path") + "/" + Common.getConfig("customer_id") + "/" + photo.getPatient_id() + "/" + photo.getAssessment_id() + "/" + photo.getImages();
                                    photos.add(new WoundPhotosVO(Common.getAlphaText(alpha.getAlpha(), locale) + " - Image " + photo.getWhichImage(), strImageFile));
                                }
                                if (photos != null && photos.size() > 0) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.images", locale),
                                            "",
                                            "",
                                            count + 6,
                                            photos));
                                }
                            }
                        }
                        for (RowData row : incisionAssessments) {
                            WoundAssessmentLocationVO tmpAlpha = new WoundAssessmentLocationVO();
                            tmpAlpha.setId(row.getAlpha_id());
                            WoundAssessmentLocationVO alpha = walservice.getAlpha(tmpAlpha);
                            AssessmentIncisionVO t = new AssessmentIncisionVO();
                            t.setId(row.getId());
                            AssessmentIncisionVO each = (AssessmentIncisionVO) aeservice.getAssessment(t);
                            //String page, String title, String value, Integer order
                            List<FieldValues> fields = row.getFields();
                            int count = 0;
                            for (FieldValues field : fields) {
                                count++;
                                // FIXME: Locate all the places where this values are used and set in order to do i18n
                                if ((!(field.getValue()).equals(""))
                                        && !(field.getTitle().equals("Printable Summary Report"))
                                        && !(field.getTitle().equals("Assessment Updates:"))
                                        && !(field.getTitle().equals("Assessment Images"))
                                        && !(field.getTitle().equals("Request for Referral"))
                                        && !(field.getTitle().equals("See Progress Notes")) && count > 3) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            field.getTitle(),
                                            field.getValue(),
                                            count));
                                }
                            }
                            if (each != null) {
                                DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                                d.setAssessment_id(each.getAssessment_id());
                                d.setAlpha_id(each.getAlpha_id());
                                d.setActive(1);
                                DressingChangeFrequencyVO dd = ncpservice.getDressingChangeFrequency(d);
                                if (dd != null && dd.getDcf() != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.dressing_change_frequency", locale)
                                            + Common.getAlphaText(alpha.getAlpha(), locale),
                                            Common.getListValue(dd.getDcf().intValue(), language),
                                            count + 1));
                                }
                                //get Nursing care Plans..
                                NursingCarePlanVO ntmp = new NursingCarePlanVO();
                                ntmp.setAssessment_id(each.getAssessment_id());
                                ntmp.setWound_profile_type_id(each.getWound_profile_type_id());
                                ntmp.setActive(1);
                                NursingCarePlanVO careplan = ncpservice.getNursingCarePlan(ntmp);
                                if (careplan != null) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.nursing_care_plan", locale),
                                            careplan.getBody(),
                                            count + 2));
                                    summaryOverview.add(new SummaryVO(
                                            Common.getAlphaText(alpha.getAlpha(), locale)
                                            + " - "
                                            + row.getUser_signature(),
                                            Common.getLocalizedReportString("pixalere.reports.summary.visit_frequency", locale),
                                            careplan.getVisitFrequency().getName(language),
                                            count + 3));
                                }
                                //get Comments.. get end_date for comment range.
                                WoundAssessmentVO nextAssessment = wservice.getNextAssessment(assess.getId());
                                WoundAssessmentVO prevAssessment = wservice.getPrevAssessment(assess.getId());
                                Collection<AssessmentCommentsVO> comments = cservice.getAllComments((prevAssessment != null ? prevAssessment.getId() + "" : null), (nextAssessment != null ? nextAssessment.getId() + "" : null), each.getWound_profile_type_id());
                                int subcount = 0;
                                for (AssessmentCommentsVO comment : comments) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.assessment_comments", locale)
                                            + (each.getWound_profile_type() != null ? Common.getWoundProfileType(each.getWound_profile_type().getAlpha_type(), locale) : ""),
                                            comment.getUser_signature(),
                                            comment.getBody(),
                                            count + 4 + subcount));
                                    subcount++;
                                }
                                //Get all comments done before next assessment.
                                AssessmentImagesVO tmpimags = new AssessmentImagesVO();
                                tmpimags.setAlpha_id(each.getAlpha_id());
                                tmpimags.setAssessment_id(each.getAssessment_id());
                                Collection<AssessmentImagesVO> ph = iservice.getAllImages(tmpimags);
                                List<WoundPhotosVO> photos = new ArrayList();
                                for (AssessmentImagesVO photo : ph) {
                                    String strImageFile = Common.getConfig("photos_path") + "/" + Common.getConfig("customer_id") + "/" + photo.getPatient_id() + "/" + photo.getAssessment_id() + "/" + photo.getImages();
                                    photos.add(new WoundPhotosVO(Common.getAlphaText(alpha.getAlpha(), locale) + " - Image " + photo.getWhichImage(), strImageFile));
                                }
                                if (photos != null && photos.size() > 0) {
                                    summaryOverview.add(new SummaryVO(
                                            Common.getLocalizedReportString("pixalere.reports.summary.images", locale),
                                            "", "",
                                            count + 6,
                                            photos));
                                }
                            }
                        }
                    }
                    PatientAccountVO patient = paservice.getPatient(wassess.getPatient_id());
                    //setting up titles
                    parameters.put("patient_id", "#" + wassess.getPatient_id() + (patient != null ? " " + patient.getPatientName() : ""));
                    //parameters.put("wound_location_name", woundVO.getWound_location_detailed());
                    parameters.put("date", PDate.getDate(wassess.getCreated_on(), false));
                    parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
                    parameters.put("nurse", "#" + wassess.getProfessional().getId() + " " + wassess.getProfessional().getFullName());
                    WoundVO woundVO = woundservice.getWound(wassess.getWound_id());
                    buildReport("summary", summaryOverview, null, null);

                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * todo: needs to be refactored to improve performance, we don't need all
     * the sub queries
     */
    public List getLastAssessmentReport(ProfessionalVO prof, int limit) {
        List<LastAssessments> results = new ArrayList();
        try {
            reportDAO.connect();

            String treatment_locations = "";
            for (UserAccountRegionsVO region : prof.getRegions()) {
                if (!region.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)) {
                    treatment_locations = treatment_locations + " treatment_location_id = " + region.getTreatment_location_id() + " OR ";
                }
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }
            String mysql_limit1 = " ";
            String mssql_top1 = " ";
            String mssql_top = "";
            String mysql_limit = "";
            if (reportDAO.isMysql() == true) {
                mysql_limit = "limit " + limit;
                mysql_limit1 = " limit 1 ";
                mssql_top1 = "";
                mssql_top = "";
            } else {

                mssql_top = "top " + limit;

                mssql_top1 = " top 1 ";
                mysql_limit1 = "";
                mysql_limit = "";
            }
            String days = Common.getConfig("lastAssessmentDays");

            Date intEpochPastDays = PDate.subtractDays(PDate.convertToUTC(PDate.getStartOfDay(new Date())), Integer.parseInt(days)-1);
            
            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);
            
            String alpha_query = "select " 
                    + mssql_top 
                    + " wal.id as id,"
                    + "alpha,"
                    + "wal.wound_id as wound_id,"
                    + "alpha_type,"
                    + "l.name as treatment_name,"
                    + "pa.name as name,"
                    + "lastName,"
                    + "wal.patient_id as patient_id,"
                    + "wounds.wound_location_detailed as wound_location_detailed "
                    + "from wound_assessment_location wal "
                    + "left join wounds on wounds.id=wal.wound_id "
                    + "left join wound_profile_type wpt on wpt.id=wal.wound_profile_type_id "
                    + "left join patient_accounts pa on pa.patient_id=wal.patient_id "
                    + "left join lookup_l10n l on l.lookup_id=pa.treatment_location_id "
                    + "and l.language_id=" + query_language +"  "
                    + "where wal.discharge=0 "
                    + "and wal.active=1 "
                    + "and pa.account_status=1 "
                    + "and pa.current_flag=1 "
                    + "and (" + treatment_locations + ")  "
                    + "AND (wpt.alpha_type='A' "
                    + "and NOT EXISTS "
                    + "(select " + mssql_top1 + " id "
                    + "from assessment_wound "
                    + "where alpha_id=wal.id "
                    + "and deleted=0 and full_assessment!=2 "
                    + "and active=1 "
                    + "and  created_on>'" + PDate.getDateIgnoreZone(intEpochPastDays, true) + "' " + mysql_limit1 + ") "
                    + "OR   "
                    + "(wpt.alpha_type='S' "
                    + "and NOT EXISTS "
                    + "(select " + mssql_top1 + " id "
                    + "from assessment_skin "
                    + "where alpha_id=wal.id "
                    + "and deleted=0  and full_assessment!=2 and  "
                    + "active=1 and  created_on>'" + PDate.getDateIgnoreZone(intEpochPastDays, true) + "' " + mysql_limit1 + ")) "
                    + "OR (wpt.alpha_type='O' AND NOT EXISTS (select  " + mssql_top1 + " id "
                    + "from assessment_ostomy where alpha_id=wal.id "
                    + "and   deleted=0  and full_assessment!=2 and active=1 "
                    + "and  created_on>'" + PDate.getDateIgnoreZone(intEpochPastDays, true) + "' " + mysql_limit1 + "))  "
                    + "OR (wpt.alpha_type='T' "
                    + "and NOT EXISTS (select  " + mssql_top1 + " id "
                    + "from assessment_drain where alpha_id=wal.id "
                    + "and deleted=0  and full_assessment!=2 "
                    + "and   active=1 "
                    + "and  created_on>'" + PDate.getDateIgnoreZone(intEpochPastDays, true) + "' " + mysql_limit1 + "))  "
                    + "OR (alpha_type='I' "
                    + "and  NOT EXISTS (select " + mssql_top1 + "  id "
                    + "from assessment_incision where alpha_id=wal.id "
                    + "and active=1  and full_assessment!=2 "
                    + "and   deleted=0 "
                    + "and  created_on>'" + PDate.getDateIgnoreZone(intEpochPastDays, true) + "' " + mysql_limit1 + "))) " + mysql_limit;
            
            ResultSet result = reportDAO.getResults(alpha_query);
            if (result != null) {
                while (result.next()) {
                    int alpha_id = result.getInt("id");
                    int wound_id = result.getInt("wound_id");
                    String alpha_type = result.getString("alpha_type");
                    String alpha = result.getString("alpha");
                    String strFirstName = result.getString("name");
                    String strLastName = result.getString("lastname");
                    String strTreatmentLocation = result.getString("treatment_name");
                    
                    
                    
                    int patient_id = result.getInt("patient_id");
                    String wound_location_detailed = result.getString("wound_location_detailed");
          
                    
                    /*ResultSet result4 = reportDAO.getResults("select l.name as name from wound_goals left join lookup_l10n l on l.lookup_id=wound_goals.lookup_id where wound_goals.lookup_id=" + treatment_location_id + " and l.language_id=" + language + " and alpha_id=" + alpha_id);
                    String goal = "";
  
                    if (result4.next()) {
                        goal = result4.getString("name");
                    }*/
            
                    LastAssessments lastAssessments = new LastAssessments();
                    lastAssessments.setTreatmentLocation(strTreatmentLocation);
                    lastAssessments.setPatientAccountId(patient_id);
                    String strPatientAccountId = Integer.toString(patient_id);
                    while (strPatientAccountId.length() < 10) {
                        strPatientAccountId = "0" + strPatientAccountId;
                    }
                    lastAssessments.setPatientAccountId_sort(strPatientAccountId);
                    String strFullName = "";
                    if ((strLastName.trim() + strFirstName.trim()).length() > 0) {
                        strFullName = strLastName + ", " + strFirstName;
                    }
                    lastAssessments.setPatientNameFull(strFullName);
                    lastAssessments.setWoundProfile(wound_location_detailed);
                    lastAssessments.setWoundAlpha(Common.getAlphaText(alpha, locale));
                    //lastAssessments.setWoundGoal(goal);
                    mssql_top = "";
                    mysql_limit = "";
                    if (reportDAO.isMysql() == true) {
                        mysql_limit = "limit 1";
                    } else {
                        mssql_top = "top 1 ";
                    }
                    String assess_query = "";

                    if (alpha_type.equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                        //assess_query = "select " + mssql_top + " wa.created_on as created_on from assessment_drain left join wound_assessment wa on wa.id=assessment_drain.assessment_id where alpha_id=" + alpha_id + " and wa.active=1 order by created_on desc " + mysql_limit;
                    } else if (alpha_type.equals(Constants.INCISION_PROFILE_TYPE)) {
                        //assess_query = "select " + mssql_top + " wa.created_on as created_on from assessment_incision left join wound_assessment wa on wa.id=assessment_incision.assessment_id where alpha_id=" + alpha_id + " and wa.active=1 order by created_on desc " + mysql_limit;
                    } else if (alpha_type.equals(Constants.WOUND_PROFILE_TYPE)) {
                        assess_query = "select " + mssql_top + " wa.created_on as created_on from assessment_wound left join wound_assessment wa on wa.id=assessment_wound.assessment_id where alpha_id=" + alpha_id + " and wa.active=1 order by created_on desc " + mysql_limit;
                    } else if (alpha_type.equals(Constants.OSTOMY_PROFILE_TYPE) && Common.getConfig("includeOstomyInOnLastAssessmentReport").equals("1")) {
                        //assess_query = "select " + mssql_top + " wa.created_on as created_on from assessment_ostomy left join wound_assessment wa on wa.id=assessment_ostomy.assessment_id where alpha_id=" + alpha_id + " and wa.active=1 order by created_on desc " + mysql_limit;
                    } else if (alpha_type.equals(Constants.SKIN_PROFILE_TYPE)) {
                        assess_query = "select " + mssql_top + " wa.created_on as created_on from assessment_skin left join wound_assessment wa on wa.id=assessment_skin.assessment_id where alpha_id=" + alpha_id + " and wa.active=1 order by created_on desc " + mysql_limit;
                    }
                    if (!assess_query.equals("")) {
                        ResultSet assessment = reportDAO.getResults(assess_query);
                        Date intLastFilteredAssessmentTimeStamp = null;
                        String strLastFilteredAssessmentDate = "";
                        if (assessment.next()) {
                            
                            intLastFilteredAssessmentTimeStamp = assessment.getTimestamp("created_on");
                            //System.out.println(patient_id+" == "+intLastFilteredAssessmentTimeStamp);
                            strLastFilteredAssessmentDate = PDate.getDate(assessment.getTimestamp("created_on"),true);
                            lastAssessments.setLastAssessmentTimeStamp(intLastFilteredAssessmentTimeStamp);
                        }

                        try {
                            
                            if (intLastFilteredAssessmentTimeStamp != null && intLastFilteredAssessmentTimeStamp.before(intEpochPastDays)) {
                                results.add(lastAssessments);
                            }
                        } catch (NumberFormatException e) {
                        }
                    }
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
        reportDAO.disconnect();
        Collections.sort(results,new LastAssessments());
        return results;
    }

    public String getUserStats(boolean month, int num_historical_records) {
        
        String body = "";
        
        try {
            if(month == true){
                num_historical_records = 2;
            }
            body = "<b>Customer: " + Common.getConfig("customer_name") + "</b><br/><table style=\"border-color:#000000;\">";
            reportDAO.connect();
            String[][] rows = new String[9][num_historical_records + 1];
            rows[0][0] = "";
            rows[1][0] = "<b>Patients</b>";
            rows[2][0] = "<b>Wounds</b>";
            rows[3][0] = "<b>Closed Wounds</b>";
            rows[4][0] = "<b>Visits</b>";
            rows[5][0] = "<b>Assessments</b>";
            rows[6][0] = "<b>Products</b>";
            rows[7][0] = "<b>Referral</b>";
            rows[8][0] = "<b>Professionals</b>";
            String thisYear = (PDate.getDate(1, 1, Integer.parseInt(PDate.getYear(new Date(), 0))).getTime() / 1000) + "";
                String thisYearEnd = (PDate.getDate(1, 1, Integer.parseInt(PDate.getYear(new Date(), 1))).getTime() / 1000) + "";
                String lastYear = (PDate.getDate(1, 1, Integer.parseInt(PDate.getYear(new Date(), -1))).getTime() / 1000) + "";
                String lastYearEnd = (PDate.getDate(1, 1, Integer.parseInt(PDate.getYear(new Date(), 0))).getTime() / 1000) + "";
            for (int num = 0; num < num_historical_records; num++) {
                int start = 7;
                int end = 0;
                if (num > 0) {
                    start = start * (num + 1);
                    end = 7 * (num);
                }
                Date first = (PDate.subtractDays(new Date(), start));
                Date last = (PDate.subtractDays(new Date(), end));
                if (month == true) {
                    first = PDate.getFirstDayOfMonth(new Date(), -(num_historical_records + 1));
                    last = PDate.getLastDayOfMonth(new Date(), -(num_historical_records + 1));
                }else if(month == true && num == 1){
                    first = PDate.getFirstDayOfMonth(new Date(), -(num_historical_records + 1));
                    last = PDate.getLastDayOfMonth(new Date(), -(num_historical_records + 1));
                    first = PDate.subtractDays(first, 365);
                    last = PDate.subtractDays(last, 365);
                }
                rows[0][num + 1] = PDate.getDate(last, true);//date
                
                // visit count
                String wound_count = "select count(distinct(wound_assessment.id)) as visit,count(distinct(wound_assessment.professional_id)) as prof_count,count(ap.id) as product_count,count(distinct wound_assessment.patient_id) as patient,count(distinct ap.alpha_id) as wound from wound_assessment left join assessment_product ap on ap.assessment_id=wound_assessment.id where wound_assessment.active=1 AND wound_assessment.created_on>='" + PDate.getDate(first, true) + "' AND wound_assessment.created_on<='" + PDate.getDate(last, true) + "'";

                String closed_wound_query = "select count(id) as closed_wound from wound_assessment_location where discharge=1 AND wound_assessment_location.close_time_stamp>='" + PDate.getDate(first, true) + "' AND wound_assessment_location.close_time_stamp<='" + PDate.getDate(last, true) + "'";
                ResultSet result = reportDAO.getResults(wound_count);
               
                ResultSet result3 = reportDAO.getResults(closed_wound_query);
                String ref_query = "select count(id) as referral from referrals_tracking where entry_type=0 and created_on>='" + PDate.getDate(first, true) + "' AND created_on<='" + PDate.getDate(last, true) + "'";
                ResultSet result4 = reportDAO.getResults(ref_query);
                String visit = reportDAO.getString(result, "visit");
                String patient = reportDAO.getString(result, "patient");
                String wound = reportDAO.getString(result, "wound");
                String product_count = reportDAO.getString(result, "product_count");
                String closed_wound = reportDAO.getString(result3, "closed_wound");
                String referral = reportDAO.getString(result4, "referral");
                
                    
                
                String prof_query = "SELECT count(distinct professional_id) as professional FROM professional_accounts left join logs_access on logs_access.professional_id=professional_accounts.id where logs_access.created_on>='" + PDate.getDate(first, true) + "' and  logs_access.created_on<='" + PDate.getDate(last, true) + "'";
                ResultSet result2 = reportDAO.getResults(prof_query);
                //get total # of individual assessments
                
                int assessment_count = 0;
                String[] tables = {"assessment_wound","assessment_incision","assessment_drain","assessment_ostomy","assessment_skin"};
                for(String table : tables){
                    String ass_count = "select count(DISTINCT(id)) as count from "+table+" where created_on>='" + PDate.getDate(first, true) + "' AND created_on<='" + PDate.getDate(last, true) + "'";
                    ResultSet result5 = reportDAO.getResults(ass_count);
                    String ass_cnt = reportDAO.getString(result5, "count");
                    try{
                        if(ass_cnt != null && !ass_cnt.equals("")){
                            assessment_count = assessment_count + Integer.parseInt(ass_cnt);
                        }
                    }catch(NumberFormatException e){
                        e.printStackTrace();
                    }
                }
                String prof_count = reportDAO.getString(result2, "professional");
                
                rows[1][num + 1] = patient;
                rows[2][num + 1] = wound;
                rows[3][num + 1] = closed_wound;
                rows[4][num + 1] = visit;
                rows[5][num + 1] = assessment_count+"";
                rows[6][num + 1] = product_count;
                rows[7][num + 1] = referral;
                rows[8][num + 1] = prof_count;
            }
            //# of profs this year
                String prof_query2 = "SELECT count(distinct professional_id) as professional FROM professional_accounts left join logs_access on logs_access.professional_id=professional_accounts.id where logs_access.created_on>='" + thisYear + "' and  logs_access.created_on<='" + thisYearEnd + "'";
                ResultSet result22 = reportDAO.getResults(prof_query2);
                String prof_count_year = reportDAO.getString(result22, "professional");
            //build table.
            int count = 0;
            for (String[] cols : rows) {
                int col_count = 0;
                String color = "style = \"background-color:#FFFFFF;\"";
                if (count == 0) {
                    color = "style = \"background-color:#CCCCCC;\"";
                }
                body = body + "<tr " + color + ">";
                for (String column : cols) {
                    body = body + "<td " + color + ">" + column + "</td>";
                    col_count++;
                }
                body = body + "</tr>";
                count++;
            }
            body = body + "</table>";
            String crit = "week";
            if (month == true) {
                crit = "month";
            }
            body = body + "<br/><br/><table><tr style=\"background-color:#000000;color:#FFFFFF;\"><td><b>LEGEND</b></td><td></td></tr>"
                    + "<tr><td>Patient</td><td># of Patients seen during the " + crit + "</td></tr>"
                    + "<tr><td>Wound</td><td># of Wounds seen during the " + crit + "</td></tr>"
                    + "<tr><td>Closed Wound</td><td># of Wounds closed during the " + crit + "</td></tr>"
                    + "<tr><td>Visit</td><td># of Visits done during the " + crit + "</td></tr>"
                    + "<tr><td>Assessments</td><td># of Indiidual Assessments done during the " + crit + "</td></tr>"
                    + "<tr><td>Products</td><td># of Products used during the " + crit + "</td></tr>"
                    + "<tr><td>Referrals</td><td># of Referrals during the " + crit + "</td></tr>"
                    + "<tr><td>Professionals</td><td># of Professionals who used Pixalere last " + crit + "</td></tr>"
                    + "<tr><td>Professionals</td><td># of Professionals who used Pixalere this year " + prof_count_year + "</td></tr>"
                    + "</table>";
            reportDAO.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return body;
    }

    /**
     * @todo, need to finish for next rev.
     *
     * @version 5.2
     * @param intEpochStartDate
     * @param intEpochEndDate
     * @param intPixalereID
     * @param strIncludeDetails
     */
    public void generatePatientFileReport(Date intEpochStartDate, Date intEpochEndDate, String intPixalereID, String strIncludeDetails) {
        List<PatientFile> files = new ArrayList<PatientFile>();
        try {
            PatientServiceImpl pservice = new PatientServiceImpl();
            PatientProfileServiceImpl ppservice = new PatientProfileServiceImpl();
            WoundServiceImpl wpservice = new WoundServiceImpl();
            PatientAccountVO patient = pservice.getPatient(Integer.parseInt(intPixalereID));
            if (patient != null) {
                PatientFile file = new PatientFile();
                parameters.put("patientName", patient.getPatientName());
                parameters.put("dob", Common.getLocalizedString("pixalere.search.form.dob", locale) + ": " + patient.getDateOfBirthStr());
                parameters.put("phn", Common.getConfig("phn1_name") + ": " + patient.getPhn());
                parameters.put("treatmentLocation", Common.getConfig("treatmentLocationName") + ": " + patient.getTreatmentLocation().getName(language));
                com.pixalere.patient.bean.PatientProfileVO patientProfile = new com.pixalere.patient.bean.PatientProfileVO();
                patientProfile.setPatient_id(Integer.parseInt(intPixalereID));
                patientProfile.setActive(1);
                if (strIncludeDetails.equals("pci")) {
                    patientProfile.setCurrent_flag(1);
                }
                Collection<PatientProfileVO> col2 = ppservice.getAllPatientProfiles(patientProfile, 0); // All recs
                RowData[] vecPatientProfiles = ppservice.getAllPatientProfilesForFlowchart(col2, userVO, false, true);
                PatientFileFactory.addFlowchart(vecPatientProfiles, "patientprofile", true, locale);
                FootAssessmentVO foott = new FootAssessmentVO();
                foott.setPatient_id(Integer.parseInt(intPixalereID));
                foott.setActive(1);
                Collection<FootAssessmentVO> col3 = ppservice.getAllFootAssessments(foott, 0); // All recs
                RowData[] vFoot = ppservice.getAllFootAssessmentsForFlowchart(col3, userVO, false);
                PatientFileFactory.addFlowchart(vFoot, "foot", true, locale);
                LimbBasicAssessmentVO limbt = new LimbBasicAssessmentVO();
                limbt.setPatient_id(Integer.parseInt(intPixalereID));
                limbt.setActive(1);
                Collection<LimbBasicAssessmentVO> col4 = ppservice.getAllBasicLimbAssessments(limbt, 0); // All recs
                RowData[] vecLimb = ppservice.getAllBasicLimbAssessmentsForFlowchart(col4, userVO, false);
                PatientFileFactory.addFlowchart(vecLimb, "limb", true, locale);
                LimbAdvAssessmentVO limbta = new LimbAdvAssessmentVO();
                limbta.setPatient_id(Integer.parseInt(intPixalereID));
                limbta.setActive(1);
                Collection<LimbAdvAssessmentVO> col4a = ppservice.getAllAdvLimbAssessments(limbta, 0); // All recs
                RowData[] vecLimba = ppservice.getAllAdvLimbAssessmentsForFlowchart(col4a, userVO, false);
                PatientFileFactory.addFlowchart(vecLimba, "limb_adv", true, locale);
                BradenVO bt = new BradenVO();
                bt.setPatient_id(Integer.parseInt(intPixalereID));
                bt.setActive(1);
                Collection<BradenVO> col5 = ppservice.getAllBradens(bt, 0); // All recs
                RowData[] vecB = ppservice.getAllBradenForFlowchart(col5, userVO, false);
                PatientFileFactory.addFlowchart(vecB, "braden", true, locale);
                PhysicalExamVO bp = new PhysicalExamVO();
                bp.setPatient_id(Integer.parseInt(intPixalereID));
                bp.setActive(1);
                Collection<PhysicalExamVO> col6 = ppservice.getAllExams(bp, 0); // All recs
                RowData[] vecBp = ppservice.getAllExamsForFlowchart(col6, userVO, false);
                PatientFileFactory.addFlowchart(vecBp, "physical_exam", true, locale);
                WoundProfileVO wnd = new WoundProfileVO();
                wnd.setPatient_id(Integer.parseInt(intPixalereID));
                wnd.setActive(1);
                Collection<WoundProfileVO> col7 = wpservice.getWoundProfiles(wnd, 0);// All recs
                RowData[] vecW = wpservice.getAllWoundProfilesForFlowchart(col7, userVO, false, true);
                PatientFileFactory.addFlowchart(vecW, "wound_profiles", basepath);

                PatientFile patientFile = PatientFileFactory.getPatient();
                files.add(patientFile);
            }
            String strReportDateRange = "";
            if (intEpochStartDate + "" != "0") {
                strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale)
                        + new PDate().getLocalizedDateString(intEpochStartDate, Common.getLocaleFromLocaleString(locale))
                        + " - "
                        + new PDate().getLocalizedDateString(intEpochEndDate, Common.getLocaleFromLocaleString(locale));
            }
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("nurse", "#" + userVO.getId() + " " + userVO.getFullName());
            parameters.put("ReportTitle", Common.getLocalizedString("pixalere.reporting.form.report_patient_file", locale));
            parameters.put("ReportSubTitle", strReportDateRange);
            buildReport("patient_file", files, null, null);
        } catch (ApplicationException e) {
        } catch (Exception e) {
        }
    }

    public void generatePatientOutcomesReport(Collection<UserAccountRegionsVO> locations, ReportQueueVO queue) {
        List<PatientOutcomes> report = new ArrayList();
        try {
            ListServiceImpl lservice = new ListServiceImpl(language);
            reportDAO = new ReportDAO();
            reportDAO.connect();
            output = -1;
            String mssql_top = "";
            String mysql_limit = "";
            String datediffdb = "";
            String full = "";

            if (reportDAO.isMysql() == true) {
                mysql_limit = "limit 1";
                full = "concat(pa.name,' ',pa.lastname)";
            } else {
                mssql_top = "top 1 ";
                datediffdb = "day,";
                full = "pa.name+' '+pa.lastname";
            }
            String treatment_locations = "";
            for (UserAccountRegionsVO treatment : locations) {
                treatment_locations = treatment_locations + " pa.treatment_location_id = " + treatment.getTreatment_location_id() + " OR ";
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }
            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);

            String query = "select "
                    + "wal.id as alpha_id,"
                    + "wal.patient_id as patient_id,"
                    + "" + full + " as fullname,"
                    + "wal.current_heal_rate as current_heal_rate,"
                    + "l.name as location,"
                    + "wal.discharge as discharge,"
                    + "wal.alpha as alpha,"
                    + "datediff(" + datediffdb + "open_time_stamp,close_time_stamp) as days_to_heal "
                    + "from wound_assessment_location wal "
                    + "left join wound_profile_type wpt on wpt.id=wal.wound_profile_type_id "
                    + "left join patient_accounts pa on pa.patient_id=wal.patient_id "
                    + "left join lookup_l10n l on l.lookup_id = pa.treatment_location_id and l.language_id=1 "
                    + "where " + (queue.getExtra_filter() != null && queue.getExtra_filter().equals("1") ? "" : "wpt.alpha_type='A' and ") 
                    + "  pa.current_flag=1 " 
                    + (queue.getExtra_filter() != null && queue.getExtra_filter().equals("1") ? "" : " and current_heal_rate IS NOT NULL") 
                    + " and (" + treatment_locations + ")";
            
            ResultSet results = reportDAO.getResults(query);
            while (results.next()) {
                int patient_id = results.getInt("patient_id");
                String fullname = results.getString("fullname");
                double heal_rate = results.getDouble("current_heal_rate");
                String location = results.getString("location");
                int alpha_id = results.getInt("alpha_id");
                int discharge = results.getInt("discharge");
                String alpha = results.getString("alpha");
                int days_to_heal = results.getInt("days_to_heal");
                PatientOutcomes p = new PatientOutcomes();
                p.setPix_id(patient_id);
                p.setName(fullname);
                if (discharge == 0) {
                    p.setHeal_rate(heal_rate);
                } else {
                    p.setHeal_rate(null);
                    p.setDays_to_heal(days_to_heal * -1);
                }

                p.setLocation(location);
                String ass_query = "select " + mssql_top + " wound_location_detailed,DATE_FORMAT(ae.visited_on,'%d/%m/%Y') as visited_on,ae.id as id,ae.status,ae.discharge_reason from assessment_wound ae left join wounds w on w.id=ae.wound_id where ae.alpha_id=" + alpha_id + " and ae.active=1 order by created_on desc " + mysql_limit;

                ResultSet results2 = reportDAO.getResults(ass_query);
                while (results2.next()) {
                    String wound = results2.getString("wound_location_detailed");
                    String visited_on = results2.getString("visited_on");
                    int id = results2.getInt("id");
                    p.setWound(wound + ": " + Common.getAlphaText(alpha, locale));
                    p.setLast_assessment(visited_on);
                    int status = results2.getInt("status");
                    int reason = results2.getInt("discharge_reason");
                    try {
                        LookupVO one = lservice.getListItem(status);
                        LookupVO two = lservice.getListItem(reason);
                        if (one != null) {
                            p.setWound_status(one.getName(language));
                        }
                        if (two != null) {
                            p.setClosed_reason(two.getName(language));
                        }
                    } catch (ApplicationException e) {
                    }

                }
                String etiologies = "";
                String et_query = "select " + mssql_top + " l.name as name from wound_etiology "
                        + "left join lookup_l10n l on l.lookup_id=wound_etiology.lookup_id and l.language_id=" + query_language + " "
                        + "left join wound_profiles wp on wp.id=wound_etiology.wound_profile_id "
                        + "where  wp.active=1 and alpha_id=" + alpha_id + " order by wp.created_on desc " + mysql_limit;
                ResultSet results3 = reportDAO.getResults(et_query);
                while (results3.next()) {
                    String name = results3.getString("name");
                    etiologies = etiologies + "" + name + ", ";
                }
                if (!etiologies.equals("") && etiologies.length() > 2) {
                    etiologies = etiologies.substring(0, etiologies.length() - 2);
                }
                p.setEtiology(etiologies);
                String prod_query = "select assessment_product.quantity as quantity, cost from assessment_product left join products p on p.id=assessment_product.product_id where assessment_product.active=1 and alpha_id=" + alpha_id;
                ResultSet results5 = reportDAO.getResults(prod_query);
                double total_cost = 0;
                while (results5.next()) {
                    int quantity = results5.getInt("quantity");
                    double cost = results5.getDouble("cost");
                    total_cost = total_cost + (quantity * cost);

                }
                p.setCost(total_cost);
                if (p.getWound() != null) {
                    report.add(p);
                }
            }
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("ReportTitle", Common.getLocalizedString("pixalere.reporting.form.report_patient_outcomes", locale));
            parameters.put("ReportSubTitle", "");
            Collections.sort(report, new PatientOutcomes());
            buildReport("patient_outcomes", report, null, (queue != null ? queue.getEmail() : null));
        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        }
    }

    public void generateFacilitiesComparisonReport(String[] locations, String[] etiologies, int year, int month,String groupby) {
        try {
            ListServiceImpl lservice = new ListServiceImpl(language);
            reportDAO = new ReportDAO();
            reportDAO.connect();
            String mssql_top = "";
            String mysql_limit = "";

            boolean isMysql = reportDAO.isMysql();
            if (isMysql) {
                mysql_limit = "limit 1";
            } else {
                mssql_top = "top 1 ";
            }
            String groupby_query = "";
            // if Groupby=treatmentArea
            if(groupby.equals("treatmentArea")){
                groupby_query = " LEFT JOIN lookup area on area.id=pa.treatment_location_id ";
            }
            String treatment_locations = "";
            for (String treatment : locations) {
                treatment_locations = treatment_locations + " pa.treatment_location_id = " + treatment + " OR ";
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }

            String etiology_query = "";
            for (String etiology : etiologies) {
                etiology_query = etiology_query + " et.lookup_id = " + etiology + " OR ";
            }
            if (etiology_query.length() > 4) {
                etiology_query = "(" + etiology_query.substring(0, etiology_query.length() - 4) + ")";
            }
            DateFormat df = new SimpleDateFormat("MMMMM", Common.getLocaleFromLocaleString(locale));

            // Date range - Start Date
            Calendar cal = GregorianCalendar.getInstance();
            cal.set(year, month, 1, 0, 0, 0);
            Date start_date = cal.getTime();

            String sMonth = df.format(start_date);

            // Date range - End Date
            cal.set(Calendar.MONTH, month + 1);
            Date end_date = cal.getTime();

            String date_query = "";
            if (start_date != null && end_date != null) {
                date_query = " wal.open_time_stamp<'"
                        + PDate.getDate(end_date, true)
                        + "' and (wal.close_time_stamp>'"
                        + PDate.getDate(start_date, true)
                        + "' OR wal.close_time_stamp IS NULL) ";
            }

            // Location, etiologies
            String location_etiology_query
                    = "AND  (" + treatment_locations + ") "
                    + "AND  " + etiology_query + " "
                    + "GROUP BY pa.treatment_location_id, et.lookup_id";

            // Costs subquery
            String costs_query
                    = "(SELECT "
                    + "	SUM(ROUND( (assessment_product.quantity * cost), 2 )) AS total_cost "
                    + "FROM wound_assessment "
                    + "LEFT JOIN assessment_product "
                    + "	ON assessment_product.assessment_id = wound_assessment.id "
                    + "LEFT JOIN wound_assessment_location AS wal "
                    + "	ON wal.id = assessment_product.alpha_id "
                    + "LEFT JOIN products "
                    + "	ON products.id = assessment_product.product_id "
                    + "LEFT JOIN wound_etiology w_et "
                    + "	ON w_et.alpha_id = assessment_product.alpha_id "
                    + "WHERE wound_assessment.created_on >=  '" + PDate.getDate(start_date, true) + "' "
                    + "AND wound_assessment.created_on <  '" + PDate.getDate(end_date, true) + "' "
                    + "AND wound_assessment.treatment_location_id=pa.treatment_location_id "
                    + "AND w_et.lookup_id = et.lookup_id "
                    + "AND assessment_product.quantity IS NOT NULL "
                    + "AND cost IS NOT NULL "
                    + "AND  NOT EXISTS ("
                    + "	SELECT 1 FROM wound_etiology w "
                    + "	WHERE w_et.alpha_id=w.alpha_id "
                    + "	AND w.id>w_et.id) "
                    + "AND wound_assessment.active =1 ) "
                    + "AS cost, ";

            // Select sections
            String select_with_acquired
                    = "SELECT "
                    + (groupby_query==""?"pa.treatment_location_id AS location_id, ":"area.category_id as location_id,")
                    + "et.lookup_id AS etiology_id, "
                    + "AVG(wal.current_heal_rate) as avg_heal_rate, "
                    + costs_query
                    + "COUNT(CASE "
                    + "WHEN " + date_query
                    + "THEN 1 END) AS total_wounds, "
                    + "COUNT(CASE WHEN wa.lookup_id="
                    + "(SELECT "
                    + "" + mssql_top + " lookup_id "
                    + "FROM lookup_l10n "
                    + "WHERE "
                    + "resource_id=175 AND name LIKE '%External%' "
                    + "order by id desc " + mysql_limit + ") "
                    + "AND  " + date_query
                    + "THEN 1 END) AS external,"
                    + "COUNT(CASE WHEN wa.lookup_id="
                    + "(SELECT " + mssql_top + " lookup_id "
                    + "FROM lookup_l10n "
                    + "WHERE "
                    + "resource_id=175 AND name LIKE '%Internal%' "
                    + "order by id desc " + mysql_limit + ")  "
                    + "AND " + date_query
                    + "THEN 1 END) AS internal ";

            String select_no_acquired
                    = "SELECT "
                    + (groupby_query==""?"pa.treatment_location_id AS location_id, ":"area.category_id as location_id,")
                    + "et.lookup_id AS etiology_id, "
                    + "AVG(wal.current_heal_rate) as avg_heal_rate, "
                    + costs_query
                    + "COUNT(CASE "
                    + "WHEN " + date_query
                    + "THEN 1 END) AS total_wounds ";

            String join_with_acquired
                    = "FROM wound_assessment_location wal "
                    + "LEFT JOIN wound_etiology et "
                    + "ON et.alpha_id=wal.id "
                    + "LEFT JOIN wound_acquired wa "
                    + "ON wa.alpha_id=wal.id "
                    + "left join patient_accounts pa "
                   
                    + "on pa.patient_id=wal.patient_id AND pa.current_flag=1 "
                    + groupby_query
                    + "WHERE " + date_query + " AND "
                    + " NOT EXISTS "
                    + "(SELECT 1 "
                    + "FROM wound_etiology w "
                    + "WHERE et.alpha_id=w.alpha_id AND w.id>et.id) "
                    + " AND NOT EXISTS "
                    + "(SELECT 1 "
                    + "FROM wound_acquired w "
                    + "WHERE wa.alpha_id=w.alpha_id AND w.id>wa.id) "
                    + "AND wa.lookup_id IS NOT NULL "
                    + "AND wal.active=1 "
                    + location_etiology_query;

            String join_no_acquired
                    = "FROM wound_assessment_location wal "
                    + "LEFT JOIN wound_etiology et "
                    + "ON et.alpha_id=wal.id "
                    + "left join patient_accounts pa "
                    + "on pa.patient_id=wal.patient_id AND pa.current_flag=1 "
                    + groupby_query
                    + "WHERE " + date_query + " AND "
                    + " NOT EXISTS "
                    + "(SELECT 1 "
                    + "FROM wound_etiology w "
                    + "WHERE et.alpha_id=w.alpha_id AND w.id>et.id) "
                    + "AND wal.active=1 "
                    + location_etiology_query;

            // Overview of data acquisition:
            // 1. Get wounds separating External/External/Other
            // 2. Get all wounds regardless of acquisition
            // 3. Merge results of 1 & 2
            // 4. Cycle through results to get aggregated data for charts
            // Count wounds with acquired entry
            String query_with_acquired
                    = select_with_acquired
                    + join_with_acquired;
            //System.out.println("\nQuery with acquired:\n" + query_with_acquired + "\n\n");

            // Counts all the wounds regardless of acquisition
            String query_no_acquired
                    = select_no_acquired
                    + join_no_acquired;
            //System.out.println("\nQuery no acquired:\n" + query_no_acquired + "\n\n");

            // Data acumulator (for merging)
            TreeMap<String, FacilityComparisonVO> data = new TreeMap<String, FacilityComparisonVO>();
            // Chart accumulator
            TreeMap<String, FacilityComparisonVO> dataChartMap = new TreeMap<String, FacilityComparisonVO>();
            TreeMap<String, FacilityComparisonVO> costsMap = new TreeMap<String, FacilityComparisonVO>();

            ResultSet results = reportDAO.getResults(query_with_acquired);

            if (results != null) {
                while (results.next()) {
                    int tcount = results.getInt("total_wounds");

                    // It only matters if it has any wound
                    if (tcount > 0) {
                        FacilityComparisonVO entry = new FacilityComparisonVO();

                        int etiology_id = results.getInt("etiology_id");
                        int location_id = results.getInt("location_id");
                        LookupVO etiology = lservice.getListItem(etiology_id);
                        LookupVO location = lservice.getListItem(location_id);
                        entry.setEtiologyId(etiology_id);
                        entry.setLocationId(location_id);
                        entry.setEtiology(etiology.getName(language));
                        entry.setLocation(location.getName(language));

                        int ecount = results.getInt("external");
                        int icount = results.getInt("internal");

                        int ncount = tcount - (ecount + icount);

                        entry.setExternal(ecount);
                        entry.setInternal(icount);
                        entry.setTotal(tcount);
                        entry.setNot_defined(ncount);

                        double heal_rate = results.getDouble("avg_heal_rate");
                        entry.setHeal_rate(heal_rate);

                        double cost = results.getDouble("cost");
                        entry.setTotal_cost(cost);

                        data.put(
                                entry.getLocationId() + "-" + entry.getEtiologyId(),
                                entry);
                    }

                }
            }

            // Second pass for wounds without acquisition
            ResultSet results2 = reportDAO.getResults(query_no_acquired);

            if (results2 != null) {
                while (results2.next()) {
                    int tcount = results2.getInt("total_wounds");

                    // It only matters if it has any wound
                    if (tcount > 0) {
                        int etiology_id = results2.getInt("etiology_id");
                        int location_id = results2.getInt("location_id");
                        FacilityComparisonVO entry;

                        if (data.containsKey(location_id + "-" + etiology_id)) {
                            // Already in data, merge wound count
                            entry = data.get(location_id + "-" + etiology_id);

                            int diffCount = tcount - entry.getTotal();
                            int ncount = entry.getNot_defined() + diffCount;

                            entry.setNot_defined(ncount);
                            entry.setTotal(tcount);

                        } else {
                            // New entry
                            entry = new FacilityComparisonVO();

                            LookupVO etiology = lservice.getListItem(etiology_id);
                            LookupVO location = lservice.getListItem(location_id);

                            entry.setEtiologyId(etiology_id);
                            entry.setLocationId(location_id);
                            entry.setEtiology(etiology.getName(language));
                            entry.setLocation(location.getName(language));

                            int ecount = 0;
                            int icount = 0;
                            int ncount = tcount;

                            entry.setExternal(ecount);
                            entry.setInternal(icount);
                            entry.setTotal(tcount);
                            entry.setNot_defined(ncount);

                            double heal_rate = results2.getDouble("avg_heal_rate");
                            entry.setHeal_rate(heal_rate);

                            double cost = results2.getDouble("cost");
                            entry.setTotal_cost(cost);
                        }
                        data.put(
                                entry.getLocationId() + "-" + entry.getEtiologyId(),
                                entry);
                    }
                } // end while(results2)
            }

            // Go through entries to get the aggregated data charts
            for (FacilityComparisonVO entry : data.values()) {
                // Accumulator for Wounds Chart
                if (dataChartMap.containsKey(entry.getLocation())) {
                    FacilityComparisonVO acc = dataChartMap.get(entry.getLocation());
                    acc.setExternal(acc.getExternal() + entry.getExternal());
                    acc.setInternal(acc.getInternal() + entry.getInternal());
                    acc.setNot_defined(acc.getNot_defined() + entry.getNot_defined());
                    acc.setTotal(acc.getTotal() + entry.getTotal());
                } else {
                    FacilityComparisonVO acc = new FacilityComparisonVO();
                    acc.setExternal(entry.getExternal());
                    acc.setInternal(entry.getInternal());
                    acc.setNot_defined(entry.getNot_defined());
                    acc.setTotal(entry.getTotal());

                    acc.setLocationId(entry.getLocationId());
                    acc.setLocation(entry.getLocation());
                    dataChartMap.put(entry.getLocation(), acc);
                }

                // Accumulator for Costs Chart
                if (costsMap.containsKey(entry.getLocation())) {
                    FacilityComparisonVO acc = costsMap.get(entry.getLocation());
                    acc.setTotal_cost(acc.getTotal_cost() + entry.getTotal_cost());
                } else {
                    FacilityComparisonVO acc = new FacilityComparisonVO();
                    acc.setTotal_cost(entry.getTotal_cost());

                    acc.setLocationId(entry.getLocationId());
                    acc.setLocation(entry.getLocation());
                    costsMap.put(entry.getLocation(), acc);
                }
            }

            // List of locations with zero wounds
            List<String> zeroLocationsList = new ArrayList<String>();
            for (String location : locations) {
                int locationId = Integer.valueOf(location.trim());
                LookupVO lookupVo = lservice.getListItem(locationId);
                String locName = lookupVo.getName(language);
                // If not in chart, zero wounds for this location
                if (!dataChartMap.containsKey(locName)) {
                    zeroLocationsList.add(locName);
                }
            }

            Collections.sort(zeroLocationsList);
            String zeroLocations = "";
            for (String location : zeroLocationsList) {
                zeroLocations += location + ", ";
            }
            zeroLocations = (zeroLocations.isEmpty())
                    ? ""
                    : zeroLocations.substring(0, zeroLocations.length() - 2) + ".";

            // Custom styles -> Small font
            StandardChartTheme theme = getChartTheme(true);

            // Costs Bar Chart
            DefaultCategoryDataset costsDataset = new DefaultCategoryDataset();
            for (String et : costsMap.navigableKeySet()) {
                BigDecimal d = new BigDecimal(costsMap.get(et).getTotal_cost());
                d = d.setScale(2, RoundingMode.UP);
                String dCost = d.toString();
                costsDataset.addValue(costsMap.get(et).getTotal_cost(), "", et + " (" + dCost + ")");
            }

            JFreeChart costsChart = ChartFactory.createBarChart(
                    "",
                    Common.getLocalizedReportString("f_comparison.costs_yaxis", locale),
                    Common.getLocalizedReportString("f_comparison.costs_xaxis", locale),
                    costsDataset,
                    PlotOrientation.HORIZONTAL, // the plot orientation
                    false, // include legend
                    true, // tooltips
                    false);

            // Custom styles
            theme.apply(costsChart);
            ((BarRenderer) ((CategoryPlot) costsChart.getPlot()).getRenderer()).setDrawBarOutline(true);
            // Create image
            BufferedImage etImage = costsChart.createBufferedImage(572, 300);
            ImageIO.write(etImage, "png", new File("chart_costs.png"));

            // Stacked Bar Chart
            DefaultCategoryDataset woundsByFacilityDataset = new DefaultCategoryDataset();
            String labelInternal = Common.getLocalizedReportString("f_comparison.internal", locale);
            String labelExternal = Common.getLocalizedReportString("f_comparison.external", locale);
            String labelNDefined = Common.getLocalizedReportString("f_comparison.not_defined", locale);

            for (String key : dataChartMap.navigableKeySet()) {
                FacilityComparisonVO acc = dataChartMap.get(key);
                woundsByFacilityDataset.addValue(acc.getInternal(), labelInternal, acc.getLocation() + " (" + acc.getTotal() + ")");
                woundsByFacilityDataset.addValue(acc.getExternal(), labelExternal, acc.getLocation() + " (" + acc.getTotal() + ")");
                woundsByFacilityDataset.addValue(acc.getNot_defined(), labelNDefined, acc.getLocation() + " (" + acc.getTotal() + ")");
            }

            JFreeChart chartWounds = ChartFactory.createStackedBarChart(
                    "",
                    Common.getLocalizedReportString("f_comparison.bar_yaxis", locale),
                    Common.getLocalizedReportString("f_comparison.bar_xaxis", locale),
                    woundsByFacilityDataset, // data
                    PlotOrientation.HORIZONTAL, // the plot orientation
                    true, // include legend
                    true, // tooltips
                    false // urls
            );

            // Custom styles -> Small font
            theme.apply(chartWounds);
            ((BarRenderer) ((CategoryPlot) chartWounds.getPlot()).getRenderer()).setDrawBarOutline(true);
            ((BarRenderer) ((CategoryPlot) chartWounds.getPlot()).getRenderer()).setBaseItemLabelsVisible(true);
            // Create image
            etImage = chartWounds.createBufferedImage(572, 310);
            ImageIO.write(etImage, "png", new File("chart_wounds.png"));

            String strReportDateRange = "";
            if (start_date != null) {
                strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale)
                        + new PDate().getLocalizedDateString(start_date, Common.getLocaleFromLocaleString(locale))
                        + " - "
                        + new PDate().getLocalizedDateString(end_date, Common.getLocaleFromLocaleString(locale));
            }
            // Sort data by textual location and then by etiology
            List<FacilityComparisonVO> dataList = new ArrayList<FacilityComparisonVO>();
            dataList.addAll(data.values());
            Collections.sort(dataList, new FacilityComparisonComparator());

            // TODO: Remove, temporal display for fixes
//            for (FacilityComparisonVO vo: dataList){
//                System.out.println("id: " 
//                        + vo.getLocationId() + " - "
//                        + vo.getLocation()
//                        + "  \t\t** id: " 
//                        + vo.getEtiologyId() + " - "
//                        + vo.getEtiology() 
//                        + "  \t\t** cost: " + vo.getTotal_cost()
//                        + "  \t\t** wounds: " + vo.getTotal());
//            }
            // Put parameters
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("ReportDateRange", strReportDateRange);
            parameters.put("ReportTitle", sMonth + " " + year);
            parameters.put("ReportLocationGrouping", Common.getConfig("treatmentLocationName"));
            parameters.put("ZeroLocationsList", zeroLocations);

            buildReport("facility_comparison", dataList, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }
        }
    }

    public void generateWoundListReport(String[] locations, int year, int month) {
        List<WoundList> report = new ArrayList();
        try {
            ListServiceImpl lservice = new ListServiceImpl(language);
            reportDAO = new ReportDAO();
            reportDAO.connect();
            output = -1;
            String mssql_top = "";
            String mysql_limit = "";
            String full = "";
            if (reportDAO.isMysql() == true) {
                mysql_limit = "limit 1";
                full = "concat(pa.name,' ',pa.lastname)";
            } else {
                mssql_top = "top 1 ";
                full = "pa.name+' '+pa.lastname";
            }

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Common.getLocaleFromLocaleString(locale));
         
            String treatment_locations = "";
            for (String strTreatmentLocationId : locations) {
                treatment_locations = treatment_locations + " pa.treatment_location_id = " + strTreatmentLocationId + " OR ";
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }
            TreeMap<String, Double> etiologiesMap = new TreeMap<String, Double>();
            // Date range - Start Date
            Calendar cal = GregorianCalendar.getInstance();
            cal.set(year, month, 1, 0, 0, 0);
            Date start_date = cal.getTime();
            // Date range month - End Date
            cal.set(Calendar.MONTH, month + 1);
            Date end_date = cal.getTime();
            // TEMP: Collect ids with null Wounds name
            // List<Integer> listNullAlphaIds = new ArrayList<Integer>();

            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);
            String query = "select wal.id as alpha_id,wal.patient_id as patient_id, "
                    + full + " as fullname, "
                    + "wal.current_heal_rate as current_heal_rate, "
                    + "l.name as location,wal.discharge as discharge, "
                    + "wal.alpha as alpha,wal.open_time_stamp as open_date, "
                    + "wal.close_time_stamp as closed_date, "
                    + "wpt.alpha_type as alpha_type "
                    + "from wound_assessment_location wal "
                    + "left join wound_profile_type wpt "
                    + "on wpt.id=wal.wound_profile_type_id "
                    + "left join patient_accounts pa "
                    + "on pa.patient_id=wal.patient_id "
                    + "left join lookup_l10n l "
                    + // FIXME: The language shouldn't be hardcoded, but we need to
                    // guarantee the location translation exists
                    // Currently this requirement is not met
                    "on l.lookup_id = pa.treatment_location_id and l.language_id= " + (query_language >= 0 ? query_language : 1)
                    + " where open_time_stamp<'" + PDate.getDate(end_date, true) + "' and (close_time_stamp IS NULL OR close_time_stamp>='" + PDate.getDate(start_date, true) + "')"
                    + " and  pa.current_flag=1 "// and exists (select id from wound_assessment where wound_id=wal.wound_id and created_on>='"+PDate.getDate(start_date, true)+"' and  created_on<='"+PDate.getDate(end_date, true)+"' )"
                    + " and (" + treatment_locations + ")";
            System.out.println(query);
            ResultSet results = reportDAO.getResults(query);
            while (results.next()) {
                int patient_id = results.getInt("patient_id");
                String fullname = results.getString("fullname");
                double heal_rate = results.getDouble("current_heal_rate");
                String location = results.getString("location");
                int alpha_id = results.getInt("alpha_id");
                int discharge = results.getInt("discharge");
                String alpha = results.getString("alpha");
                String alpha_type = results.getString("alpha_type");
                Date openDate = results.getDate("open_date");
                Date closedDate = results.getDate("closed_date");

                WoundList p = new WoundList();
                p.setPix_id(patient_id);
                p.setName(fullname);
                if (discharge == 0) {
                    if (heal_rate > 0) {
                        p.setHeal_status("arrow_up.png");
                    } else if (heal_rate < 0) {
                        p.setHeal_status("arrow_down.png");
                    }
                } else {
                    p.setHeal_status("");
                }
                System.out.println("Compare: " + openDate.getTime() + " = " + start_date.getTime());
                if (openDate.getTime() >= start_date.getTime()) {
                    p.setNew_wound(Common.getLocalizedString("pixalere.y", locale));
                } else {
                    p.setNew_wound(Common.getLocalizedString("pixalere.n", locale));
                }
                p.setLocation(location);

                String sClosed = (closedDate != null) ? df.format(closedDate) : "";
                p.setDate_closed(sClosed);

                // The wound name needs to be searched in different tables
                // depending on the alpha type.
                // The wound name needs to be searched in different tables
                // depending on the alpha type.
                String table_name = Common.getAssessmentTableNameFromAlpha(alpha);
                String ass_query
                        = "select " + mssql_top
                        + " wound_location_detailed, "
                        + "ae.id as id, ae.status"
                        + (table_name.equals("assessment_wound")
                        ? ", ae.wound_date "
                        : " ")
                        + "from " + table_name + " ae "
                        + "left join wounds w "
                        + "on w.id=ae.wound_id "
                        + "where ae.alpha_id=" + alpha_id
                        + " and ae.active=1 order by created_on desc "
                        + mysql_limit;

                // Status, Wound and Onset
                ResultSet results2 = reportDAO.getResults(ass_query);

                while (results2.next()) {
                    String wound = results2.getString("wound_location_detailed");
                    p.setWound(wound + ": " + Common.getAlphaText(alpha, locale));

                }

                // GET ETIOLOGIES
                String etiologies = "";
                String etiologiesCombined= "";
                String et_query = "select " + mssql_top + " l.name as name ,l.lookup_id as lookup_id,lk.category_id as category_id"
                        + " from wound_etiology "
                        + " left join lookup_l10n l "
                        // FIXME: The language shouldn't be hardcoded, but we need to
                        // guarantee the etiology name is always translated
                        // Currently this requirement is not met
                        + " on l.lookup_id=wound_etiology.lookup_id and l.language_id=" + query_language + " "
                        + " left join lookup lk on lk.id=l.lookup_id "
                        + " left join wound_profiles wp "
                        + " on wp.id=wound_etiology.wound_profile_id "
                        + " where  wp.active=1 and alpha_id=" + alpha_id + " order by wp.created_on desc " + mysql_limit;
                System.out.println(et_query);
                ResultSet results3 = reportDAO.getResults(et_query);
                while (results3.next()) {
                    String name = results3.getString("name");
                    String lookup_id = results3.getString("lookup_id");
                    String category_id = results3.getString("category_id");
                    try {
                        if (category_id != null && !category_id.equals("0")) {
                            LookupVO category = lservice.getListItem(new Integer(lookup_id));
                            if(category!=null && category.getName(language)!=null){
                            etiologiesCombined = category.getName(language);
                            }else{
                                 etiologiesCombined = name;
                            }
                        }else{ etiologiesCombined = name;}
                    } catch (NumberFormatException e) {e.printStackTrace();
                        // log.error("Invalid Count: ReportBuilder: "+e.getMessage());
                    } catch (ClassCastException e) {e.printStackTrace();
                        // log.error("Invalid Count: ReportBuilder: "+e.getMessage());
                    }
                    etiologies = etiologies + "" + name + ", ";
                    

                }
                // Maps for charts
                
                    if (etiologiesMap.containsKey(etiologiesCombined)) {
                        double d = etiologiesMap.get(etiologiesCombined);
                        etiologiesMap.put(etiologiesCombined, d + 1.0);
                    } else {
                        etiologiesMap.put(etiologiesCombined, 1.0);
                    }

                if (!etiologies.equals("") && etiologies.length() > 2) {
                    etiologies = etiologies.substring(0, etiologies.length() - 2);
                }
                p.setEtiology(etiologies);

                // GET ACQUIRED
                String acquired = "";
                String acquired_query = "select " + mssql_top + " l.name as acquired "
                        + "from wound_acquired "
                        + "left join lookup_l10n l "
                        + "on l.lookup_id=wound_acquired.lookup_id and l.language_id=" + query_language + " "
                        + "left join wound_profiles wp "
                        + "on wp.id=wound_acquired.wound_profile_id "
                        + "where  wp.active=1 and alpha_id=" + alpha_id + " "
                        + "order by wp.created_on desc " + mysql_limit;
                ResultSet rAcquired = reportDAO.getResults(acquired_query);
                while (rAcquired.next()) {
                    String data = rAcquired.getString("acquired");
                    acquired = acquired + "" + data + ", ";
                }
                if (!acquired.equals("") && acquired.length() > 2) {
                    acquired = acquired.substring(0, acquired.length() - 2);
                }
                p.setAcquired(acquired);

                //get last comment from this wound
                String comment_query = "select " + mssql_top + " body "
                        + " from assessment_comments "
                        + " left join wound_assessment_location wal on "
                        + " wal.wound_id=assessment_comments.wound_id "
                        + " where wal.id=" + alpha_id + " and created_on<'"+PDate.getDate(end_date, true)+"' order by created_on desc " + mysql_limit;
                ResultSet rComment = reportDAO.getResults(comment_query);
                while (rComment.next()) {
                    String comment = rComment.getString("body");
                    if (comment != null) {
                        p.setLast_comment(comment);
                    }
                }
                // If the Wound is not null
                if (p.getWound() != null) {

                    report.add(p);
                } else {
                    // TEMP: Store which are null
                    //listNullAlphaIds.add(alpha_id);
                }
            }
            StandardChartTheme theme = getChartTheme();
            // Etiologies Bar Chart
            DefaultCategoryDataset etiologiesDataset = new DefaultCategoryDataset();
            for (String et : etiologiesMap.navigableKeySet()) {
                String nWounds = String.valueOf(etiologiesMap.get(et).intValue());
                etiologiesDataset.addValue(etiologiesMap.get(et), et + " (" + nWounds + ")", "");
            }

            JFreeChart etiologiesChart = ChartFactory.createBarChart(
                    Common.getLocalizedReportString("w_tracking.barchart_title", locale),
                    Common.getLocalizedReportString("w_tracking.barchart_xaxis", locale),
                    Common.getLocalizedReportString("w_tracking.barchart_yaxis", locale),
                    etiologiesDataset,
                    PlotOrientation.VERTICAL,
                    true,
                    true,
                    false);
            // Custom styles
            theme.apply(etiologiesChart);
            ((BarRenderer) ((CategoryPlot) etiologiesChart.getPlot()).getRenderer()).setDrawBarOutline(true);
            // Create image
            BufferedImage etImage = etiologiesChart.createBufferedImage(572, 400);
           // ImageIO.write(etImage, "png", new File("etiologies.png"));
            parameters.put("etiologies_graph",etImage);

            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("ReportTitle", Common.getLocalizedString("pixalere.reporting.form.report_patient_outcomes", locale));

            // Set Subtitle by filter
            String subTitle = Common.getLocalizedReportString("w_tracking.subtitle_all", locale);
            parameters.put("ReportSubTitle", subTitle);

            // Patient sorting
            Collections.sort(report, new WoundListComparator());
            buildReport("wound_list", report, null, null);

        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        }
    }

    public void generateWoundTrackingReport(Collection<UserAccountRegionsVO> locations, String sortBy, String filterStatus, ReportQueueVO queue) {
        List<WoundTracking> report = new ArrayList();
        try {
            ListServiceImpl lservice = new ListServiceImpl(language);
            reportDAO = new ReportDAO();
            reportDAO.connect();
            output = -1;
            String mssql_top = "";
            String mysql_limit = "";
            String full = "";
            if (reportDAO.isMysql() == true) {
                mysql_limit = "limit 1";
                full = "concat(pa.name,' ',pa.lastname)";
            } else {
                mssql_top = "top 1 ";
                full = "pa.name+' '+pa.lastname";
            }
            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Common.getLocaleFromLocaleString(locale));

            String treatment_locations = "";
            for (UserAccountRegionsVO treatment : locations) {
                treatment_locations = treatment_locations + " pa.treatment_location_id = " + treatment.getTreatment_location_id() + " OR ";
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }

            TreeMap<String, Double> etiologiesMap = new TreeMap<String, Double>();
            TreeMap<String, Double> acquiredMap = new TreeMap<String, Double>();
            // TEMP: Collect ids with null Wounds name
            // List<Integer> listNullAlphaIds = new ArrayList<Integer>();

            String query = "select wal.id as alpha_id,wal.patient_id as patient_id, "
                    + full + " as fullname, "
                    + "wal.current_heal_rate as current_heal_rate, "
                    + "l.name as location,wal.discharge as discharge, "
                    + "wal.alpha as alpha,wal.open_time_stamp as open_date, "
                    + "wal.close_time_stamp as closed_date, "
                    + "wpt.alpha_type as alpha_type "
                    + "from wound_assessment_location wal "
                    + "left join wound_profile_type wpt "
                    + "on wpt.id=wal.wound_profile_type_id "
                    + "left join patient_accounts pa "
                    + "on pa.patient_id=wal.patient_id "
                    + "left join lookup_l10n l "
                    + // FIXME: The language shouldn't be hardcoded, but we need to
                    // guarantee the location translation exists
                    // Currently this requirement is not met
                    "on l.lookup_id = pa.treatment_location_id and l.language_id=1 "
                    + "where "
                    + "  pa.current_flag=1 "
                    + " and (" + treatment_locations + ")";
            ResultSet results = reportDAO.getResults(query);
            while (results.next()) {
                int patient_id = results.getInt("patient_id");
                String fullname = results.getString("fullname");
                double heal_rate = results.getDouble("current_heal_rate");
                String location = results.getString("location");
                int alpha_id = results.getInt("alpha_id");
                int discharge = results.getInt("discharge");
                String alpha = results.getString("alpha");
                String alpha_type = results.getString("alpha_type");

                Date openDate = results.getDate("open_date");
                Date closedDate = results.getDate("closed_date");

                WoundTracking p = new WoundTracking();
                p.setPix_id(patient_id);
                p.setName(fullname);
                if (discharge == 0) {
                    p.setHeal_rate(heal_rate);
                } else {
                    p.setHeal_rate(null);
                }

                p.setLocation(location);
                String sOpen = (openDate != null) ? df.format(openDate) : "";
                p.setDate_open(sOpen);
                String sClosed = (closedDate != null) ? df.format(closedDate) : "";
                p.setDate_closed(sClosed);

                // The wound name needs to be searched in different tables
                // depending on the alpha type.
                String table_name = Common.getAssessmentTableNameFromAlpha(alpha);
                String ass_query
                        = "select " + mssql_top
                        + " wound_location_detailed, "
                        + "ae.id as id, ae.status"
                        + (table_name.equals("assessment_wound")
                        ? ", ae.wound_date "
                        : " ")
                        + "from " + table_name + " ae "
                        + "left join wounds w "
                        + "on w.id=ae.wound_id "
                        + "where ae.alpha_id=" + alpha_id
                        + " and ae.active=1 order by created_on desc "
                        + mysql_limit;

                // Status, Wound and Onset
                ResultSet results2 = reportDAO.getResults(ass_query);
                String status = "";
                while (results2.next()) {
                    String wound = results2.getString("wound_location_detailed");
                    p.setWound(wound + ": " + Common.getAlphaText(alpha, locale));

                    if (table_name.equals("assessment_wound")) {
                        p.setDate_onset(results2.getString("wound_date"));
                    } else {
                        p.setDate_onset(sOpen);
                    }

                    int nStatus = results2.getInt("status");
                    try {
                        LookupVO one = lservice.getListItem(nStatus);
                        if (one != null) {
                            status = one.getName(language);
                            p.setStatus(status);
                        }
                    } catch (ApplicationException e) {
                    }
                }

                // Skip entry using status filter
                if (!filterStatus.equals("all")) {
                    // FIXME: Replace hardcoded comparisons with a comparison to a i18n resource
                    // This can only be done after adding missing French translations at Lookupl10n for Closed/Open
                    if (filterStatus.equals("onlyOpen")) {
                        if (status.isEmpty() || status.startsWith("Clos") || status.startsWith("Cerra") || status.startsWith("Ferm")) {
                            continue;
                        }
                    } else {
                        if (status.isEmpty() || status.startsWith("Open") || status.startsWith("Abier") || status.startsWith("Ouve")) {
                            continue;
                        }
                    }
                }

                // GET ETIOLOGIES
                String etiologies = "";
                String et_query = "select " + mssql_top + " l.name as name "
                        + "from wound_etiology "
                        + "left join lookup_l10n l "
                        // FIXME: The language shouldn't be hardcoded, but we need to
                        // guarantee the etiology name is always translated
                        // Currently this requirement is not met
                        + "on l.lookup_id=wound_etiology.lookup_id and l.language_id=" + query_language + " "
                        + "left join wound_profiles wp "
                        + "on wp.id=wound_etiology.wound_profile_id "
                        + "where  wp.active=1 and alpha_id=" + alpha_id + " order by wp.created_on desc " + mysql_limit;
                ResultSet results3 = reportDAO.getResults(et_query);
                while (results3.next()) {
                    String name = results3.getString("name");
                    etiologies = etiologies + "" + name + ", ";
                }
                if (!etiologies.equals("") && etiologies.length() > 2) {
                    etiologies = etiologies.substring(0, etiologies.length() - 2);
                }
                p.setEtiology(etiologies);

                // GET ACQUIRED
                String acquired = "";
                String acquired_query = "select " + mssql_top + " l.name as acquired "
                        + "from wound_acquired "
                        + "left join lookup_l10n l "
                        + "on l.lookup_id=wound_acquired.lookup_id and l.language_id=" + query_language + " "
                        + "left join wound_profiles wp "
                        + "on wp.id=wound_acquired.wound_profile_id "
                        + "where  wp.active=1 and alpha_id=" + alpha_id + " "
                        + "order by wp.created_on desc " + mysql_limit;
                ResultSet rAcquired = reportDAO.getResults(acquired_query);
                while (rAcquired.next()) {
                    String data = rAcquired.getString("acquired");
                    acquired = acquired + "" + data + ", ";
                }
                if (!acquired.equals("") && acquired.length() > 2) {
                    acquired = acquired.substring(0, acquired.length() - 2);
                }
                p.setAcquired(acquired);

                // Recategorization for acquired chart        
                acquired = (acquired.isEmpty())
                        ? Common.getLocalizedReportString("w_tracking.pieacquired_notdefined", locale)
                        : (acquired.startsWith("Ext"))
                        ? Common.getLocalizedReportString("w_tracking.pieacquired_external", locale)
                        : Common.getLocalizedReportString("w_tracking.pieacquired_internal", locale);

                // If the Wound is not null
                if (p.getWound() != null) {
                    // Maps for charts
                    if (etiologiesMap.containsKey(etiologies)) {
                        double d = etiologiesMap.get(etiologies);
                        etiologiesMap.put(etiologies, d + 1.0);
                    } else {
                        etiologiesMap.put(etiologies, 1.0);
                    }
                    if (acquiredMap.containsKey(acquired)) {
                        double d = acquiredMap.get(acquired);
                        acquiredMap.put(acquired, d + 1.0);
                    } else {
                        acquiredMap.put(acquired, 1.0);
                    }
                    report.add(p);
                } else {
                    // TEMP: Store which are null
                    //listNullAlphaIds.add(alpha_id);
                }
            }

            // TEMP: Null names' ids
//            Collections.sort(listNullAlphaIds);
//            System.out.println("Wound Tracking Report - " + sortBy + " - " + 
//                    filterStatus + 
//                    "\nNull wound name - wound_assessment_location ids:");
//            for (Integer id: listNullAlphaIds){
//                System.out.print(id + ",");
//            }
//            System.out.println("");
            // CHARTS
            StandardChartTheme theme = getChartTheme();

            // Acquired Pie Chart
            DefaultPieDataset acquiredDataset = new DefaultPieDataset();
            for (String ac : acquiredMap.navigableKeySet()) {
                String nAcquired = String.valueOf(acquiredMap.get(ac).intValue());
                acquiredDataset.setValue(ac + " (" + nAcquired + ")", acquiredMap.get(ac));
            }

            JFreeChart acquiredChart = ChartFactory.createPieChart(
                    Common.getLocalizedReportString("w_tracking.pieacquired_title", locale),
                    acquiredDataset,
                    false,
                    false,
                    false);
            // Custom styles
            theme.apply(acquiredChart);
            ((PiePlot) acquiredChart.getPlot()).setLabelFont(new Font("Arial", Font.PLAIN, 9));
            // Create image
            BufferedImage acquiredImage = acquiredChart.createBufferedImage(572, 180);
            ImageIO.write(acquiredImage, "png", new File("acquired.png"));

            // Etiologies Bar Chart
            DefaultCategoryDataset etiologiesDataset = new DefaultCategoryDataset();
            for (String et : etiologiesMap.navigableKeySet()) {
                String nWounds = String.valueOf(etiologiesMap.get(et).intValue());
                etiologiesDataset.addValue(etiologiesMap.get(et), et + " (" + nWounds + ")", "");
            }

            JFreeChart etiologiesChart = ChartFactory.createBarChart(
                    Common.getLocalizedReportString("w_tracking.barchart_title", locale),
                    Common.getLocalizedReportString("w_tracking.barchart_xaxis", locale),
                    Common.getLocalizedReportString("w_tracking.barchart_yaxis", locale),
                    etiologiesDataset,
                    PlotOrientation.VERTICAL,
                    true,
                    true,
                    false);
            // Custom styles
            theme.apply(etiologiesChart);
            ((BarRenderer) ((CategoryPlot) etiologiesChart.getPlot()).getRenderer()).setDrawBarOutline(true);
            // Create image
            BufferedImage etImage = etiologiesChart.createBufferedImage(572, 400);
            ImageIO.write(etImage, "png", new File("etiologies.png"));

            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("ReportTitle", Common.getLocalizedString("pixalere.reporting.form.report_patient_outcomes", locale));

            // Set Subtitle by filter
            String subTitle = (filterStatus.equals("all"))
                    ? Common.getLocalizedReportString("w_tracking.subtitle_all", locale)
                    : (filterStatus.equals("onlyOpen"))
                    ? Common.getLocalizedReportString("w_tracking.subtitle_open", locale)
                    : Common.getLocalizedReportString("w_tracking.subtitle_closed", locale);
            parameters.put("ReportSubTitle", subTitle);

            // Select layout by sorting condition
            if (sortBy.equals("byPatient")) {
                // Patient sorting
                Collections.sort(report, new WoundTrackingComparator());
                buildReport("wound_tracking_beta", report, null, (queue != null ? queue.getEmail() : null));
            } else {
                // Etiologies sorting
                Collections.sort(report, new WoundTracking());
                buildReport("wound_tracking_alpha", report, null, (queue != null ? queue.getEmail() : null));
            }

        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        }
    }

    public void generateWoundTrackingDetailsReport(Collection<UserAccountRegionsVO> locations, String filterStatus, ReportQueueVO queue, int year, int month) {
        List<WoundTracking> report = new ArrayList();
        try {
            ListServiceImpl lservice = new ListServiceImpl(language);
            reportDAO = new ReportDAO();
            reportDAO.connect();
            output = -1;
            String mssql_top = "";
            String mysql_limit = "";
            String full = "";
            boolean isMysql = reportDAO.isMysql();
            if (isMysql) {
                mysql_limit = "limit 1";
                full = "concat(pa.name,' ',pa.lastname)";
            } else {
                mssql_top = "top 1 ";
                full = "pa.name+' '+pa.lastname";
            }
            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);
            
            ReferralsTrackingDAO referralsDao = new ReferralsTrackingDAO();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Common.getLocaleFromLocaleString(locale));

            String treatment_locations = "";
            for (UserAccountRegionsVO treatment : locations) {
                treatment_locations = treatment_locations + " pa.treatment_location_id = " + treatment.getTreatment_location_id() + " OR ";
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }

            TreeMap<String, Double> etiologiesMap = new TreeMap<String, Double>();
            TreeMap<String, Double> acquiredMap = new TreeMap<String, Double>();

            // Date range - Start Date
            Calendar cal = GregorianCalendar.getInstance();
            cal.set(year, month, 1, 0, 0, 0);
            Date start_date = cal.getTime();

            // Date range - End Date
            cal.set(Calendar.MONTH, month + 1);
            Date end_date = cal.getTime();

            String date_query = "";
            if (start_date != null && end_date != null) {
                date_query = " wal.open_time_stamp<'"
                        + PDate.getDate(end_date, true)
                        + "' and (wal.close_time_stamp>'"
                        + PDate.getDate(start_date, true)
                        + "' OR wal.close_time_stamp IS NULL) ";
            }

            // Data query to verify later that the cost obtained
            // is from the same location of the current wound alpha
            String cost_location_query
                    = "(SELECT " + mssql_top + " distinct(treatment_location_id) "
                    + "FROM wound_assessment  "
                    + "LEFT JOIN assessment_product  "
                    + "	ON assessment_product.assessment_id=wound_assessment.id  "
                    + "LEFT JOIN products p  "
                    + "	ON p.id=assessment_product.product_id  "
                    + "WHERE wound_assessment.created_on>='2013-08-01 12:00:00'  "
                    + "AND wound_assessment.created_on<'2013-09-01 12:00:00'  "
                    + "AND    assessment_product.active=1     "
                    + "AND cost IS NOT NULL     "
                    + "AND alpha_id = wal.id " + mysql_limit
                    + ") AS cost_location,  ";

            String query
                    = "select wal.id as alpha_id,"
                    + "wal.patient_id as patient_id, "
                    + "wal.current_heal_rate as heal_rate, "
                    + "wal.wound_id as wound_id, "
                    + "wal.wound_profile_type_id as wound_profile_type_id, "
                    + full + " as fullname, "
                    + "l.name as location,"
                    + "wal.discharge as discharge, "
                    + "wal.alpha as alpha,"
                    + "wal.open_time_stamp as open_date, "
                    + "wal.close_time_stamp as closed_date, "
                    + "("
                    + "SELECT "
                    + "COALESCE("
                    + "   SUM(ROUND((assessment_product.quantity * cost),2)),"
                    + "   0,00)"
                    + "FROM wound_assessment "
                    + "LEFT JOIN assessment_product "
                    + "ON assessment_product.assessment_id=wound_assessment.id "
                    + "LEFT JOIN products p "
                    + "ON p.id=assessment_product.product_id "
                    + "WHERE "
                    // Restrict to cost generated this month
                    + "wound_assessment.created_on>='" + PDate.getDate(start_date, true) + "' "
                    + "AND wound_assessment.created_on<'" + PDate.getDate(end_date, true) + "' "
                    + "AND "
                    + "   assessment_product.active=1 "
                    + "   AND cost IS NOT NULL "
                    + "   AND alpha_id = wal.id"
                    + ") AS cost, "
                    + "pa.treatment_location_id as location_id, "
                    + cost_location_query
                    + "("
                    + "SELECT "
                    + "    COUNT(wass.id) "
                    + "FROM wound_assessment wass "
                    + "WHERE "
                    + "    wass.wound_id = wal.wound_id "
                    + "    and wass.visit = 1 "
                    + ") AS visits "
                    + "from wound_assessment_location wal "
                    + "left join patient_accounts pa "
                    + "on pa.patient_id=wal.patient_id AND pa.current_flag=1 "
                    + "left join lookup_l10n l "
                    + // FIXME: The language shouldn't be hardcoded, but we need to
                    // guarantee the location translation exists
                    // Currently this requirement is not met
                    "on l.lookup_id = pa.treatment_location_id and l.language_id=1 "
                    + "where pa.account_status=1 and pa.current_flag=1 and "
                    + date_query + " AND "
                    + " (" + treatment_locations + ")"
                    + " group by alpha_id";

            //System.out.println("Wound Tracking Details monthly:\n" + query);
            ResultSet results = reportDAO.getResults(query);
            while (results.next()) {
                int patient_id = results.getInt("patient_id");
                String fullname = results.getString("fullname");
                double heal_rate = results.getDouble("heal_rate");
                String location = results.getString("location");
                int alpha_id = results.getInt("alpha_id");
                int wound_id = results.getInt("wound_id");
                int wound_profile_type_id = results.getInt("wound_profile_type_id");
                int discharge = results.getInt("discharge");
                String alpha = results.getString("alpha");
                double cost = results.getDouble("cost");
                int visit_frequency = results.getInt("visits");

                Date openDate = results.getDate("open_date");
                Date closedDate = results.getDate("closed_date");

                // Validating data for costs
                int cost_location = results.getInt("cost_location");
                int location_id = results.getInt("location_id");

                if (cost != 0.0 && (cost_location != location_id)) {
                    // CORNER CASE:

                    // Part or all the cost was extracted from an 
                    // assessment in a different location
                    // Cause: The same wound can be moved between locations.
                    // The same wound can be reopened at a different location.
                    // A wound assessment can have costs in a location different
                    // that the current location of the wound. Since this report
                    // is grouped by location to include such cost doesn't make sense.
                    // This problem is solved by recalculating the cost filtering by
                    // both the location and the alpha_id.
                    // Note: A similar filter may possibly apply for other reports.
                    String cost_query
                            = "select "
                            + "   (sum(ROUND((assessment_product.quantity * cost),2))) as cost "
                            + "   from wound_assessment "
                            + "   left join assessment_product "
                            + "       ON assessment_product.assessment_id=wound_assessment.id  "
                            + "   LEFT JOIN wound_assessment_location as wal "
                            + "       ON wal.id=assessment_product.alpha_id "
                            + "   LEFT JOIN products "
                            + "       ON products.id=assessment_product.product_id  "
                            + "   where "
                            + "wound_assessment.created_on>='" + PDate.getDate(start_date, true) + "' "
                            + "AND wound_assessment.created_on<'" + PDate.getDate(end_date, true) + "' "
                            + "   AND wound_assessment.treatment_location_id=" + location_id + " "
                            + "and wal.id = " + alpha_id
                            + "   and wound_assessment.active=1 ";

                    ResultSet resultCost = reportDAO.getResults(cost_query);
                    while (resultCost.next()) {
                        cost = resultCost.getDouble("cost");
                    }
                }

                WoundTracking p = new WoundTracking();
                p.setPix_id(patient_id);
                p.setName(fullname);
                if (discharge == 0) {
                    p.setHeal_rate(heal_rate);
                } else {
                    p.setHeal_rate(null);
                }

                p.setLocation(location);
                String sOpen = (openDate != null) ? dateFormat.format(openDate) : "";
                p.setDate_open(sOpen);
                String sClosed = (closedDate != null) ? dateFormat.format(closedDate) : "";
                p.setDate_closed(sClosed);

                p.setCost(cost);
                p.setVisit_frequency(visit_frequency);
                
                // Get number of open referrals
                // It would be better to replace this with a query?
                ReferralsTrackingVO refer = new ReferralsTrackingVO();
                refer.setPatient_account_id(patient_id);
                refer.setWound_profile_type_id(wound_profile_type_id);
                refer.setWound_id(wound_id);
                refer.setCurrent(Constants.ACTIVE);
                refer.setEntry_type(Constants.REFERRAL);
                refer.setActive(Constants.ACTIVE);
                
                Collection<ReferralsTrackingVO> colReferrals = referralsDao.findAllByCriteria(refer);
                List<ReferralsTrackingVO> listReferrals = new ArrayList<ReferralsTrackingVO>();
                if (colReferrals != null){
                    listReferrals.addAll(colReferrals);
                }
                refer.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                colReferrals = referralsDao.findAllByCriteria(refer);
                if (colReferrals != null){
                    listReferrals.addAll(colReferrals);
                }
                p.setOpen_referrals(listReferrals.size());

                // The wound name needs to be searched in different tables
                // depending on the alpha type.
                String table_name = Common.getAssessmentTableNameFromAlpha(alpha);
                String ass_query
                        = "select " + mssql_top
                        + " wound_location_detailed, "
                        + "ae.id as id, ae.status"
                        + (table_name.equals("assessment_wound")
                        ? ", ae.wound_date "
                        : " ")
                        + "from " + table_name + " ae "
                        + "left join wounds w "
                        + "on w.id=ae.wound_id "
                        + "where ae.alpha_id=" + alpha_id
                        + " and ae.active=1 order by created_on desc "
                        + mysql_limit;

                // Status, Wound and Onset
                ResultSet results2 = reportDAO.getResults(ass_query);
                String status = "";
                while (results2.next()) {
                    String wound = results2.getString("wound_location_detailed");
                    p.setWound(wound + ": " + Common.getAlphaText(alpha, locale));

                    if (table_name.equals("assessment_wound")) {
                        p.setDate_onset(results2.getString("wound_date"));
                    } else {
                        p.setDate_onset(sOpen);
                    }

                    int nStatus = results2.getInt("status");
                    try {
                        LookupVO one = lservice.getListItem(nStatus);
                        if (one != null) {
                            status = one.getName(language);
                            p.setStatus(status);
                        }
                    } catch (ApplicationException e) {
                    }
                }

                // Skip entry using status filter
                if (!filterStatus.equals("all")) {
                    // FIXME: Replace hardcoded comparisons with a comparison to a i18n resource
                    // This can only be done after adding missing French translations at Lookupl10n for Closed/Open
                    if (filterStatus.equals("onlyOpen")) {
                        if (status.isEmpty() || status.startsWith("Clos") || status.startsWith("Cerra") || status.startsWith("Ferm")) {
                            continue;
                        }
                    } else {
                        if (status.isEmpty() || status.startsWith("Open") || status.startsWith("Abier") || status.startsWith("Ouve")) {
                            continue;
                        }
                    }
                }

                // GET ETIOLOGIES
                String etiologies = "";
                String et_query = "select " + mssql_top + " l.name as name "
                        + "from wound_etiology "
                        + "left join lookup_l10n l "
                        // FIXME: The language shouldn't be hardcoded, but we need to
                        // guarantee the etiology name is always translated
                        // Currently this requirement is not met
                        + "on l.lookup_id=wound_etiology.lookup_id and l.language_id=" + query_language + " "
                        + "left join wound_profiles wp "
                        + "on wp.id=wound_etiology.wound_profile_id "
                        + "where  wp.active=1 and alpha_id=" + alpha_id + " order by wp.created_on desc " + mysql_limit;
                ResultSet results3 = reportDAO.getResults(et_query);
                while (results3.next()) {
                    String name = results3.getString("name");
                    etiologies = etiologies + "" + name + ", ";
                }
                if (!etiologies.equals("") && etiologies.length() > 2) {
                    etiologies = etiologies.substring(0, etiologies.length() - 2);
                }
                p.setEtiology(etiologies);

                // GET ACQUIRED
                String acquired = "";
                String acquired_query = "select " + mssql_top + " l.name as acquired "
                        + "from wound_acquired "
                        + "left join lookup_l10n l "
                        + "on l.lookup_id=wound_acquired.lookup_id and l.language_id=" + query_language + " "
                        + "left join wound_profiles wp "
                        + "on wp.id=wound_acquired.wound_profile_id "
                        + "where  wp.active=1 and alpha_id=" + alpha_id + " "
                        + "order by wp.created_on desc " + mysql_limit;
                ResultSet rAcquired = reportDAO.getResults(acquired_query);
                while (rAcquired.next()) {
                    String data = rAcquired.getString("acquired");
                    acquired = acquired + "" + data + ", ";
                }
                if (!acquired.equals("") && acquired.length() > 2) {
                    acquired = acquired.substring(0, acquired.length() - 2);
                }
                p.setAcquired(acquired);

                // Recategorization for acquired chart        
                acquired = (acquired.isEmpty())
                        ? Common.getLocalizedReportString("w_tracking.pieacquired_notdefined", locale)
                        : (acquired.startsWith("Ext"))
                        ? Common.getLocalizedReportString("w_tracking.pieacquired_external", locale)
                        : Common.getLocalizedReportString("w_tracking.pieacquired_internal", locale);

                // Get Assessments / Dressing Change Count
                String dfc_query = "SELECT ("
                        + " (Select COUNT(assessment_wound.id) FROM assessment_wound WHERE assessment_wound.alpha_id=" + alpha_id + ") + "
                        + " (Select COUNT(assessment_skin.id) FROM assessment_skin WHERE assessment_skin.alpha_id=" + alpha_id + ") + "
                        + " (Select COUNT(assessment_ostomy.id) FROM assessment_ostomy WHERE assessment_ostomy.alpha_id=" + alpha_id + ") + "
                        + " (Select COUNT(assessment_incision.id) FROM assessment_incision WHERE assessment_incision.alpha_id=" + alpha_id + ") + "
                        + " (Select COUNT(assessment_burn.id) FROM assessment_burn WHERE assessment_burn.alpha_id=" + alpha_id + ") + "
                        + " (Select COUNT(assessment_drain.id) FROM assessment_drain WHERE assessment_drain.alpha_id=" + alpha_id + ")  "
                        + ") AS dfc";
                ResultSet rDfc = reportDAO.getResults(dfc_query);
                int dfc = 0;
                while (rDfc.next()) {
                    dfc = rDfc.getInt("dfc");
                }
                p.setDfc(dfc);

                // If the Wound is not null
                if (p.getWound() != null) {
                    // Maps for charts
                    if (etiologiesMap.containsKey(etiologies)) {
                        double d = etiologiesMap.get(etiologies);
                        etiologiesMap.put(etiologies, d + 1.0);
                    } else {
                        etiologiesMap.put(etiologies, 1.0);
                    }
                    if (acquiredMap.containsKey(acquired)) {
                        double d = acquiredMap.get(acquired);
                        acquiredMap.put(acquired, d + 1.0);
                    } else {
                        acquiredMap.put(acquired, 1.0);
                    }
                    report.add(p);
                }
            }

            // CHARTS
            StandardChartTheme theme = getChartTheme();

            // Acquired Pie Chart
            DefaultPieDataset acquiredDataset = new DefaultPieDataset();
            for (String ac : acquiredMap.navigableKeySet()) {
                String nAcquired = String.valueOf(acquiredMap.get(ac).intValue());
                acquiredDataset.setValue(ac + " (" + nAcquired + ")", acquiredMap.get(ac));
            }

            JFreeChart acquiredChart = ChartFactory.createPieChart(
                    Common.getLocalizedReportString("w_tracking.pieacquired_title", locale),
                    acquiredDataset,
                    false,
                    false,
                    false);
            // Custom styles
            theme.apply(acquiredChart);
            ((PiePlot) acquiredChart.getPlot()).setLabelFont(new Font("Arial", Font.PLAIN, 9));
            // Create image
            BufferedImage acquiredImage = acquiredChart.createBufferedImage(312, 360);
            ImageIO.write(acquiredImage, "png", new File("acquired.png"));

            // Etiologies Bar Chart
            DefaultCategoryDataset etiologiesDataset = new DefaultCategoryDataset();
            for (String et : etiologiesMap.navigableKeySet()) {
                String nWounds = String.valueOf(etiologiesMap.get(et).intValue());
                etiologiesDataset.addValue(etiologiesMap.get(et), et + " (" + nWounds + ")", "");
            }

            JFreeChart etiologiesChart = ChartFactory.createBarChart(
                    Common.getLocalizedReportString("w_tracking.barchart_title", locale),
                    Common.getLocalizedReportString("w_tracking.barchart_xaxis", locale),
                    Common.getLocalizedReportString("w_tracking.barchart_yaxis", locale),
                    etiologiesDataset,
                    PlotOrientation.VERTICAL,
                    true,
                    true,
                    false);
            // Custom styles
            theme.apply(etiologiesChart);
            ((BarRenderer) ((CategoryPlot) etiologiesChart.getPlot()).getRenderer()).setDrawBarOutline(true);
            // Create image
            BufferedImage etImage = etiologiesChart.createBufferedImage(462, 360);
            ImageIO.write(etImage, "png", new File("etiologies.png"));

            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("ReportTitle", Common.getLocalizedString("pixalere.reporting.form.report_patient_outcomes", locale));

            // Set Subtitle by filter
            String subTitle = (filterStatus.equals("all"))
                    ? Common.getLocalizedReportString("det_tracking.subtitle_all", locale)
                    : (filterStatus.equals("onlyOpen"))
                    ? Common.getLocalizedReportString("det_tracking.subtitle_open", locale)
                    : Common.getLocalizedReportString("det_tracking.subtitle_closed", locale);

            // Month for display in subtitle
            DateFormat df = new SimpleDateFormat("MMMMM", Common.getLocaleFromLocaleString(locale));
            String sMonth = df.format(start_date);
            subTitle += " - " + sMonth + " " + year;
            parameters.put("ReportSubTitle", subTitle);

            // Sort/Group by Etiologies 
            Collections.sort(report, new WoundTracking());
            buildReport("wound_tracking_details_monthly", report, null, (queue != null ? queue.getEmail() : null));

        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        }
    }
    
    public void generateReferralsReport(List<Integer> locationsList) {
        try {
            reportDAO.connect();
            // Get selected locations names
            String locations = "";
            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);
            
            for (Integer location_id : locationsList) {
                String q = "select name from lookup_l10n where language_id=" + query_language + " AND lookup_id=" + location_id;
                ResultSet result = reportDAO.getResults(q);
                String n = Common.getString(result, "name");
                locations = locations + n + ", ";
            }
            
            if (!locations.equals("")) {
                parameters.put("TreatmentLocations",
                        Common.getLocalizedReportString("pixalere.reports.dashboard.generated_for_locations", locale)
                        + locations.substring(0, locations.length() - 2) + ".");
            }
            
            
            // Get data
            ProfessionalServiceImpl professionalService = new ProfessionalServiceImpl();
            ReferralsTrackingDAO dao = new ReferralsTrackingDAO();
            
            List<ReferralsTrackingVO> referralList = new ArrayList<ReferralsTrackingVO>();
            // Get referrals/recommendations without filtering by user position
            referralList.addAll(dao.sortAllByCriteria(Constants.REFERRAL, userVO, false));
            referralList.addAll(dao.sortAllByCriteria(Constants.RECOMMENDATION, userVO, false));
            
            List<ReferralVO> dataList = new ArrayList();
            // LOGIC
            for (ReferralsTrackingVO vo : referralList) {
                ReferralVO data = new ReferralVO();
                PatientAccountVO pavo = vo.getPatient();
                
                // Filter by location, skip if not in list
                if (!locationsList.contains(pavo.getTreatment_location_id())){
                    continue;
                }
                
                data.setName(pavo.getPatientName());
                data.setId(pavo.getPatient_id());
                
                if(Common.getConfig("allowReferralByPositions")!=null 
                        && Common.getConfig("allowReferralByPositions").equals("1")
                        && vo.getAssigned_to()!=null){
                    PositionVO p = new PositionVO();
                    p.setId(vo.getAssigned_to());
                    PositionVO position = professionalService.getPosition(p);
                    if(position != null){
                        data.setReferral_for(position.getTitle());
                    }
                } else {
                    String referral_for = Common.getLocalizedReportString("report_referrals.not_assigned_to", locale);
                    data.setReferral_for(referral_for);
                }
                
                ProfessionalVO profesionalUser = professionalService.getProfessional(vo.getProfessional_id());
                PDate pdate = new PDate();
                String date_referral = pdate.getProfessionalSignature(vo.getCreated_on(), profesionalUser, locale);
                String state;
                String automated = Common.getLocalizedReportString("report_referrals.type_manual", locale);
                if (vo.getEntry_type().equals(Constants.REFERRAL) || vo.getEntry_type().equals(Constants.MESSAGE_FROM_NURSE)){
                    data.setReferral_date(date_referral);
                    state = Common.getLocalizedReportString("report_referrals.pending_recommendation", locale);
                    
                    if (vo.getAuto_referrals()!=null){
                        for (AutoReferralVO auto : vo.getAuto_referrals()) {
                            if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Heal Rate")) {
                                automated = Common.getLocalizedReportString("report_referrals.type_heal_rate", locale);
                            } else if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Dressing Change Frequency")) {
                                automated = Common.getLocalizedReportString("report_referrals.type_dcf", locale);
                            } else if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Antimicrobial")) {
                                automated = Common.getLocalizedReportString("report_referrals.type_microbial", locale);
                            }
                        }
                    }
                } else {
                    data.setRecommendation_date(date_referral);
                    state = Common.getLocalizedReportString("report_referrals.pending_ack", locale);
                    
                    // Get Referral date
                    ReferralsTrackingVO rtmp = new ReferralsTrackingVO();
                    rtmp.setWound_profile_type_id(vo.getWound_profile_type_id());
                    rtmp.setAssessment_id(vo.getAssessment_id());
                    rtmp.setEntry_type(Constants.REFERRAL);
                    rtmp.setCurrent(Constants.INACTIVE);
                    
                    ReferralsTrackingVO referral = (ReferralsTrackingVO) dao.findByCriteria(rtmp);
                    if (referral == null){
                        rtmp.setEntry_type(Constants.MESSAGE_FROM_NURSE);
                        referral = (ReferralsTrackingVO) dao.findByCriteria(rtmp);
                    }
                    
                    if (referral != null){
                        // Found previous referral for this assessment - Get comment
                        AssessmentCommentsDAO cdao = new AssessmentCommentsDAO();
                        AssessmentCommentsVO ctmp = new AssessmentCommentsVO();
                        
                        ctmp.setAssessment_id(vo.getAssessment_id());
                        ctmp.setReferral_id(referral.getId());
                        
                        AssessmentCommentsVO comment = (AssessmentCommentsVO) cdao.findByCriteria(ctmp);
                        if (comment != null) {
                            data.setReferral_date(comment.getUser_signature());
                        }
                        // Get if original referral was assigned to
                        if(Common.getConfig("allowReferralByPositions")!=null 
                                && Common.getConfig("allowReferralByPositions").equals("1")
                                && referral.getAssigned_to()!=null){
                            PositionVO p = new PositionVO();
                            p.setId(referral.getAssigned_to());
                            PositionVO position = professionalService.getPosition(p);

                            data.setReferral_for(position.getTitle());
                        }
                        // Identify if original referral was automated
                        for (AutoReferralVO auto : referral.getAuto_referrals()) {
                            if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Heal Rate")) {
                                automated = Common.getLocalizedReportString("report_referrals.type_heal_rate", locale);
                            } else if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Dressing Change Frequency")) {
                                automated = Common.getLocalizedReportString("report_referrals.type_dcf", locale);
                            } else if (auto.getAuto_referral_type() != null && auto.getAuto_referral_type().equals("Antimicrobial")) {
                                automated = Common.getLocalizedReportString("report_referrals.type_microbial", locale);
                            }
                        }
                    }
                }
                data.setState(state);
                data.setAutomated(automated);
                dataList.add(data);
            }
            
            // Sort data list
            Collections.sort(dataList, new ReferralComparator());
            
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("ReportUser", " #" + userVO.getId() + " " + userVO.getFullName());
            parameters.put("ReportTitle", Common.getLocalizedReportString("report_referrals.title", locale));
            buildReport("referrals", dataList, null, null);
        } catch (ApplicationException e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }
        }
    }

    public void generateWoundAcquiredReport(Date start_date, Date end_date, String[] locations, String[] etiologies) {
        try {

            reportDAO.connect();

            String mssql_top = "";
            String mysql_limit = "";
            if (reportDAO.isMysql() == true) {
                mysql_limit = "limit 1";
            } else {
                mssql_top = "top 1 ";
            }
            String treatment_locations = "";
            for (String treatment : locations) {
                treatment_locations = treatment_locations + " pa.treatment_location_id = " + treatment + " OR ";
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }
            ListServiceImpl bservice = new ListServiceImpl(language);
            //cycle etiologies.
            String etiology_query = "";
            for (String etiology : etiologies) {
                etiology_query = etiology_query + " et.lookup_id = " + etiology + " OR ";
            }
            if (etiology_query.length() > 4) {
                etiology_query = "(" + etiology_query.substring(0, etiology_query.length() - 4) + ")";
            }
            String date_query = "";
            if (start_date != null && end_date != null) {
                date_query = " wal.open_time_stamp<'"
                        + PDate.getDate(end_date, true)
                        + "' and (wal.close_time_stamp>'"
                        + PDate.getDate(start_date, true)
                        + "' OR wal.close_time_stamp IS NULL) ";
            }

            // Counting by using the table wound_acquired
            String query_with_acquired
                    = "select pa.treatment_location_id as location_id,"
                    + "et.lookup_id as etiology_id,"
                    + "COUNT(CASE "
                    + "WHEN " + date_query
                    + "THEN 1 END) AS total_wounds, "
                    + "COUNT(CASE "
                    + "WHEN wa.lookup_id="
                    + "(select " + mssql_top + " lookup_id "
                    + "from lookup_l10n "
                    + "where "
                    + "resource_id=175 and name LIKE '%External%' "
                    + "order by id desc " + mysql_limit + ") "
                    + "and "
                    + "wal.open_time_stamp>'" + PDate.getDate(start_date, true) + "' "
                    + "and wal.open_time_stamp<='" + PDate.getDate(end_date, true) + "' "
                    + "then 1 end) as  new_external,"
                    + "COUNT(CASE "
                    + "WHEN wa.lookup_id=(select " + mssql_top + " lookup_id "
                    + "from lookup_l10n "
                    + "where "
                    + "resource_id=175 and name LIKE '%Internal%' "
                    + "order by id desc " + mysql_limit + ")  "
                    + "and wal.open_time_stamp>'" + PDate.getDate(start_date, true) + "' "
                    + "and wal.open_time_stamp<='" + PDate.getDate(end_date, true) + "' "
                    + "then 1 end) as  new_internal, "
                    + "COUNT(CASE "
                    + "WHEN wa.lookup_id=(select " + mssql_top + " lookup_id "
                    + "from lookup_l10n "
                    + "where "
                    + "resource_id=175 and name LIKE '%External%' "
                    + "order by id desc " + mysql_limit + ") "
                    + "THEN 1 END) as external ,"
                    + "count(CASE "
                    + "WHEN wa.lookup_id=(select " + mssql_top + " lookup_id "
                    + "from lookup_l10n "
                    + "where "
                    + "resource_id=175 and name LIKE '%Internal%' "
                    + "order by id desc " + mysql_limit + ") "
                    + "THEN 1 END) as internal  "
                    + "FROM wound_assessment_location wal "
                    + "LEFT JOIN wound_etiology et "
                    + "ON et.alpha_id=wal.id "
                    + "LEFT JOIN wound_acquired wa "
                    + "ON wa.alpha_id=wal.id "
                    + "left join patient_accounts pa on pa.patient_id=wal.patient_id and pa.current_flag=1 "
                    + "WHERE " + date_query + " AND "
                    + " NOT EXISTS "
                    + "(select 1 "
                    + "from wound_etiology w "
                    + "where et.alpha_id=w.alpha_id "
                    + "and w.id>et.id) "
                    + "AND NOT EXISTS "
                    + "(select 1 from wound_acquired w "
                    + "where wa.alpha_id=w.alpha_id "
                    + "and w.id>wa.id) "
                    + "AND wa.lookup_id IS NOT NULL and wal.active=1 "
                    + "and  (" + treatment_locations + ") and  " + etiology_query
                    + " GROUP BY pa.treatment_location_id,et.lookup_id";

            //System.out.println("WOUND w ACQUIRED: \n" + query_with_acquired);
            // TODO: Replace with a map for merging
            TreeMap<String, WoundAcquiredVO> data = new TreeMap<String, WoundAcquiredVO>();

            ResultSet results = reportDAO.getResults(query_with_acquired);
            while (results.next()) {
                int tcount = results.getInt("total_wounds");

                if (tcount > 0) {
                    WoundAcquiredVO ac = new WoundAcquiredVO();

                    int etiology_id = results.getInt("etiology_id");
                    int location_id = results.getInt("location_id");
                    ac.setEtiologyId(etiology_id);
                    ac.setLocationId(location_id);
                    LookupVO etiology = bservice.getListItem(etiology_id);
                    LookupVO location = bservice.getListItem(location_id);
                    ac.setGrouping(location.getName(language));
                    ac.setName(etiology.getName(language));

                    int ecount = results.getInt("external");
                    int icount = results.getInt("internal");
                    int new_ecount = results.getInt("new_external");
                    int new_icount = results.getInt("new_internal");
                    // non defined
                    int ncount = tcount - (ecount + icount);
                    ac.setExternal(ecount);
                    ac.setInternal(icount);
                    ac.setTotal(tcount);
                    ac.setNot_defined(ncount);

                    ac.setNew_external(new_ecount);
                    ac.setNew_internal(new_icount);
                    ac.setNew_total(new_ecount + new_icount);

                    ac.setNew_not_defined(0);
                    data.put(
                            ac.getLocationId() + "-" + ac.getEtiologyId(),
                            ac);
                }

            }

            // Counting All wounds - No wound_acquired
            String query_no_acquired
                    = "select "
                    + "	pa.treatment_location_id as location_id, "
                    + "	et.lookup_id as etiology_id, "
                    + "	COUNT("
                    + "		CASE WHEN "
                    + "		wal.open_time_stamp>'" + PDate.getDate(start_date, true) + "' "
                    + "		and wal.open_time_stamp<='" + PDate.getDate(end_date, true) + "' "
                    + "		then 1 end) as  new_all_wounds, "
                    + "	COUNT(CASE WHEN "
                    + date_query
                    + "		then 1 end) as  total_all_wounds "
                    + "FROM wound_assessment_location wal "
                    + "LEFT JOIN wound_etiology et "
                    + "	ON et.alpha_id=wal.id "
                    + "left join patient_accounts pa "
                    + "	on pa.patient_id=wal.patient_id and pa.current_flag=1 "
                    + "WHERE "
                    + date_query
                    + "AND  NOT EXISTS "
                    + "	(select 1 "
                    + "		from wound_etiology w "
                    + "		where et.alpha_id=w.alpha_id "
                    + "		and w.id>et.id) "
                    + "and wal.active=1 "
                    + "and  (" + treatment_locations + ") and  " + etiology_query
                    + " GROUP BY pa.treatment_location_id,et.lookup_id";

            //System.out.println("WOUND no ACQUIRED: \n" + query_no_acquired);
            // TODO: Merge this query results with previous data
            ResultSet results2 = reportDAO.getResults(query_no_acquired);

            while (results2.next()) {
                int total_count = results2.getInt("total_all_wounds");
                int new_all = results2.getInt("new_all_wounds");

                if (total_count > 0) {
                    int etiology_id = results2.getInt("etiology_id");
                    int location_id = results2.getInt("location_id");

                    WoundAcquiredVO entry;
                    if (data.containsKey(location_id + "-" + etiology_id)) {
                        // Already in data, merge wound count
                        entry = data.get(location_id + "-" + etiology_id);

                        int diffCount = total_count - entry.getTotal();
                        int ncount = entry.getNot_defined() + diffCount;
                        entry.setNot_defined(ncount);
                        entry.setTotal(total_count);

                        int diffNew = new_all - entry.getNew_total();
                        int ncount_new = entry.getNew_not_defined() + diffNew;
                        entry.setNew_not_defined(ncount_new);
                        entry.setNew_total(new_all);
                    } else {
                        entry = new WoundAcquiredVO();
                        LookupVO etiology = bservice.getListItem(etiology_id);
                        LookupVO location = bservice.getListItem(location_id);

                        entry.setEtiologyId(etiology_id);
                        entry.setLocationId(location_id);
                        entry.setName(etiology.getName(language));
                        entry.setGrouping(location.getName(language));

                        entry.setExternal(0);
                        entry.setInternal(0);
                        entry.setNew_external(0);
                        entry.setNew_internal(0);

                        entry.setNot_defined(total_count);
                        entry.setTotal(total_count);

                        entry.setNew_not_defined(new_all);
                        entry.setNew_total(new_all);
                    }

                    data.put(
                            entry.getLocationId() + "-" + entry.getEtiologyId(),
                            entry);
                }

                //report.add(ac);
            }

            List<WoundAcquiredVO> dataList = new ArrayList();
            dataList.addAll(data.values());
            Collections.sort(dataList, new WoundAcquiredComparator());
            // TODO: Remove, temporal display for fixes
//            for (WoundAcquiredVO vo: dataList){
//                System.out.println("id: " 
//                        + vo.getLocationId() + " - "
//                        + vo.getGrouping()
//                        + "  \t\t** id: " 
//                        + vo.getEtiologyId() + " - "
//                        + vo.getName() 
//                        + "  \t\t** ndef: " + vo.getNot_defined()
//                        + "  \t\t** total: " + vo.getTotal());
//            }

            String strReportDateRange = "";
            if (start_date != null) {
                strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale)
                        + new PDate().getLocalizedDateString(start_date, Common.getLocaleFromLocaleString(locale))
                        + " - "
                        + new PDate().getLocalizedDateString(end_date, Common.getLocaleFromLocaleString(locale));
            }
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("nurse", "#" + userVO.getId() + " " + userVO.getFullName());
            parameters.put("ReportLocationGrouping", Common.getConfig("treatmentLocationName"));
            parameters.put("ReportTitle", Common.getLocalizedString("pixalere.report.woundacquired", locale));
            parameters.put("ReportSubTitle", Common.getLocalizedString("pixalere.reporting.groupby", locale) + " " + Common.getConfig("treatmentLocationName"));
            parameters.put("ReportDateRange", strReportDateRange);
            buildReport("woundacquired", dataList, null, null);
        } catch (ApplicationException e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        }
    }

    public void generateUsageStatsReport(Date start_date, Date end_date, Integer inactive, List<Integer> locationsList, Integer display_no_records) {
        List<UsageStats> report = new ArrayList();
        try {

            reportDAO.connect();

            String mssql_top = "";
            String mysql_limit = "";
            if (reportDAO.isMysql() == true) {
                mysql_limit = "limit 1";
            } else {
                mssql_top = "top 1 ";
            }
            
            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);
            // Get selected locations names
            String locations = "";
            for (Integer location_id : locationsList) {
                String q = "select name from lookup_l10n where language_id=" + query_language + " AND lookup_id=" + location_id;
                ResultSet result = reportDAO.getResults(q);
                String n = Common.getString(result, "name");
                locations = locations + n + ", ";
            }
            
            if (!locations.equals("")) {
                parameters.put("TreatmentLocations",
                        Common.getLocalizedReportString("pixalere.reports.dashboard.generated_for_locations", locale)
                        + locations.substring(0, locations.length() - 2) + ".");
            }
            
            String treatment_locations = "";
            //Automatically get all treatment locations from currently logged in user
            for (UserAccountRegionsVO region : userVO.getRegions()) {
                // filter out if not in locations requested
                if (locationsList.contains(region.getTreatment_location_id())){
                    treatment_locations = treatment_locations + " treatment_location_id = " + region.getTreatment_location_id() + " OR ";
                }
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }
            String query = "select professional_id,p.firstname as firstname,p.lastname as lastname from logs_access left join professional_accounts p on p.id=logs_access.professional_id where action='Logged in' and logs_access.created_on<'" + PDate.getDate(end_date, false) + "' and logs_access.created_on>'" + PDate.getDate(start_date, false) + "' and exists (select id from user_account_regions where user_account_id=p.id and (" + treatment_locations + ")) group by p.lastname";
            if(inactive!=null && inactive == 1){
                query = "select p.id as professional_id,"
                        + "p.firstname as firstname, "
                        + "p.lastname as lastname "
                        + "from professional_accounts p "
                        + "where NOT EXISTS "
                            + "(select id from logs_access "
                            + "where action='Logged in' "
                            + "and created_on<'" + PDate.getDate(end_date, false) + "' "
                            + "and created_on>'" + PDate.getDate(start_date, false) + "') "
                        + "and exists "
                            + "(select id from user_account_regions"
                            + " where user_account_id=p.id and (" + treatment_locations + ")) "
                        + "group by p.lastname";
            }
            
            ResultSet results = reportDAO.getResults(query);
            
            /*if (results != null) {
                parameters.put("professional_count", results.getFetchSize());
            }*/
            int cnt=0;
            if(results!=null){
            while (results.next()) {
                int id = results.getInt("professional_id");
                String firstname = results.getString("firstname");
                String lastname = results.getString("lastname");
                String comment_query = "select count(distinct(id)) as comment_count from assessment_comments where professional_id=" + id + " and created_on<'" + PDate.getDate(end_date, false) + "' and created_on>'" + PDate.getDate(start_date, false) + "'";
                ResultSet results3 = reportDAO.getResults(comment_query);
                int comment_count = 0;
                while (results3.next()) {
                    int comm_count = results3.getInt("comment_count");
                    comment_count = comm_count;
                }
                String count_query = 
                        "select "
                        + "count(distinct(patient_id)) as patient_count, "
                        + "count(distinct(wound_id)) as wound_count, "
                        + "count(distinct(id)) as visit_count "
                        + "from wound_assessment "
                        + "where professional_id=" + id + " "
                        + "and active=1 "
                        + "and ( " + treatment_locations + " )"
                        + "and created_on<'" + PDate.getDate(end_date, false) + "' "
                        + "and created_on>'" + PDate.getDate(start_date, false) + "'";

                ResultSet results2 = reportDAO.getResults(count_query);

                while (results2.next()) {
                    int patient = results2.getInt("patient_count");
                    int wound = results2.getInt("wound_count");
                    int visits = results2.getInt("visit_count");

                    boolean isNotZero = patient > 0 || wound > 0 || visits > 0;
                    if (isNotZero || (display_no_records!=null && display_no_records.equals(1))){
                        cnt++;
                        UsageStats ac = new UsageStats();
                        ac.setName(lastname+", "+firstname);
                        ac.setId(id);
                        ac.setPatient(patient);
                        ac.setWound(wound);
                        ac.setVisits(visits);
                        ac.setComments(comment_count);
                        report.add(ac);
                    }
                }
            }
            }
            parameters.put("professional_count", cnt);
            String strReportDateRange = "";
            if (start_date != null) {
                strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale)
                        + new PDate().getLocalizedDateString(start_date, Common.getLocaleFromLocaleString(locale))
                        + " - "
                        + new PDate().getLocalizedDateString(end_date, Common.getLocaleFromLocaleString(locale));
            }
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            parameters.put("ReportUser", "#" + userVO.getId() + " " + userVO.getFullName());
            parameters.put("ReportTitle", Common.getLocalizedString("pixalere.report.woundacquired", locale));
            parameters.put("ReportDateRange", strReportDateRange);
            buildReport("usage_stats", report, null, null);
        } catch (ApplicationException e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        }
    }
    
    
    public void generateWoundCountsWithDressings(String doc_type,String[] locations, Date start_date, Date end_date,int daysWithDressing) throws ApplicationException {
        String strTypeOfGrouping = "treatmentLocation";
        String[] careTypes = {"Wound", "Incision", "T&D", "Ostomy"};
        String[] careTypeAssessments = {"wound", "incision", "drain", "ostomy"};
        String[] careTypeLocations = {"A", "T", "D", "O"};
        this.doc_type=doc_type;
      
        
        // Date range - last Wednesday, Thursday and Friday of month
        // Get last friday of the month
        //int daysInRange = 3;        // Days in the range
      ///  int daysWithDressing = 3;   // How many distinct days with dressing in the range
                                    // are needed to include the wound in the count 
 

        List<WoundCounts> resultList = new ArrayList<WoundCounts>();
        try {
            reportDAO.connect();
            ListServiceImpl listBD = new ListServiceImpl(language);

            // SQL Server and MySQL format instructions for dates
            // (for counting dressings in distinct days)
            String convert_date_start = "";
            String convert_date_end = "";
            if (reportDAO.isMysql() == true) {
                // DATE([datetimecolumn]) as date_only
                convert_date_start = " count(distinct DATE (assessment_";
                convert_date_end = ".created_on)) as created ";
            } else {
                // convert(varchar(8), [datetimecolumn], 112) as date_only
                convert_date_start = " count(distinct convert(varchar(8), assessment_";
                convert_date_end = ".created_on, 112)) as created ";
            }

            for (String strTreatmentLocationId : locations) {

                String strTreatmentLocation = "";
                String strGrouping = "";
                String strSubGrouping = "";
                int intTip = 0;
                if (strTreatmentLocationId.equals("-1")) {
                    intTip = 1;
                    strTreatmentLocation = Common.getLocalizedString("pixalere.TIP", "en");
                    strSubGrouping = Common.getConfig("treatmentLocationName");
                } else {
                    LookupVO vo = listBD.getListItem(Integer.parseInt(strTreatmentLocationId));
                    strTreatmentLocation = vo.getName(language);
                }
                strSubGrouping = strTreatmentLocation;
                String timeLocationQuery
                        = " wound_assessment.created_on>='" + PDate.getDate(start_date, true)
                        + "' AND wound_assessment.created_on<='" + PDate.getDate(end_date, true)
                        + "' AND wound_assessment.treatment_location_id=" + strTreatmentLocationId;

                String patientsVisitQuery = "select count(DISTINCT wound_assessment.patient_id) as count from wound_assessment where active=1  AND " + timeLocationQuery;
                String totalVisitsQuery = "select count(wound_assessment.id) as count from wound_assessment where active=1 AND visit=1 AND " + timeLocationQuery;
                //System.out.println(patientsVisitQuery);

                int patientVisits = Common.getInt(reportDAO.getResults(patientsVisitQuery), "count");
                int totalVisits = Common.getInt(reportDAO.getResults(totalVisitsQuery), "count");

                // Calculate total cost for this location
                String cost_query
                        = "SELECT "
                        + "    (SUM(ROUND((assessment_product.quantity * cost),2))) as cost "
                        + "FROM wound_assessment "
                        + "LEFT JOIN assessment_product "
                        + "    ON assessment_product.assessment_id=wound_assessment.id  "
                        + "LEFT JOIN wound_assessment_location as wal "
                        + "    ON wal.id=assessment_product.alpha_id "
                        + "LEFT JOIN products "
                        + "    ON products.id=assessment_product.product_id  "
                        + "where "
                        + timeLocationQuery
                        + " and wound_assessment.active=1 ";
                double cost = Common.getDouble(reportDAO.getResults(cost_query), "cost");

                int cnt = 0;
                //System.out.println(patientsVisitQuery);
                boolean includedPatientCount = false;
                if (totalVisits != 0) {
                    for (String caretype : careTypes) {
                        //String woundVisitsQuery = "select count(wound_assessment.id) as count from wound_assessment LEFT JOIN assessment_" + careTypeAssessments[cnt] + " ON assessment_" + careTypeAssessments[cnt] + ".assessment_id=wound_assessment.id where wound_assessment.active=1 " + timeQuery+" AND wound_assessment.treatment_location_id="+strTreatmentLocationId;
                        String woundQuery
                                = "select distinct assessment_"
                                + careTypeAssessments[cnt] + ".alpha_id "
                                + "from wound_assessment "
                                + "LEFT JOIN assessment_" + careTypeAssessments[cnt]
                                + " ON assessment_" + careTypeAssessments[cnt] + ".assessment_id=wound_assessment.id "
                                + "WHERE wound_assessment.active=1 AND " + timeLocationQuery + " ";
                        //System.out.println(caretype + " - woundQuery: \n" + woundQuery + "\n");

                        ResultSet wounds = reportDAO.getResults(woundQuery);
                        String wquery = "";
                        if (wounds != null) {
                            while (wounds.next()) {
                                int wound_id = wounds.getInt("alpha_id");
                                wquery = wquery + " wound_assessment_location.id=" + wound_id + " OR ";
                            }
                            if (wquery.length() > 2) {
                                wquery = wquery.substring(0, wquery.length() - 4);
                                wquery = "(" + wquery + ") AND ";
                            }
                        }
                        //System.out.println(woundVisitsQuery);
                        String woundExistingQuery = "select count(wound_assessment_location.id) as count from wound_assessment_location "
                                + "where " + wquery + " "
                                + " open_time_stamp<'" + PDate.getDate(start_date, true) + "' "
                                + " AND open_time_stamp<'" + PDate.getDate(end_date, true) + "'";

                        String woundNewQuery = "select count(wound_assessment_location.id) as count from wound_assessment_location  "
                                + "where  " + wquery + "  "
                                + "  open_time_stamp>'" + PDate.getDate(start_date, true) + "' "
                                + " AND open_time_stamp<'" + PDate.getDate(end_date, true) + "'";

                        //System.out.println(woundNewQuery);
                        String woundClosedQuery = "select count(wound_assessment_location.id) as count from wound_assessment_location  "
                                + "where " + wquery + " wound_assessment_location.discharge=1  "
                                + " AND close_time_stamp>'" + PDate.getDate(start_date, true) + "'" + " "
                                + " AND close_time_stamp<'" + PDate.getDate(end_date, true) + "'";

                        String woundAssessmentsCountQuery = "select count(assessment_" + careTypeAssessments[cnt] + ".id) as count "
                                + "from assessment_" + careTypeAssessments[cnt]
                                + " LEFT JOIN wound_assessment ON wound_assessment.id=assessment_" + careTypeAssessments[cnt] + ".assessment_id "
                                + "where wound_assessment.active=1 AND" + timeLocationQuery
                                + " AND wound_assessment.treatment_location_id=" + strTreatmentLocationId;

                        //System.out.println(caretype + " - woundAssessmentsCountQuery: \n" + woundAssessmentsCountQuery + "\n");
                        // Counts the wounds in the range with at least daysWithDressing assessments in distinct days
                        //this should be refactored to give a count, needed a quick fix.
                        String woundsWithDressingsCountQuery =
                                "WITH ranged AS (" +
                                "  SELECT alpha_id,  c.visited_on as visited_on,  RangeId = DATEDIFF(dAY,0,c.visited_on)" +
                                "            - ROW_NUMBER() OVER (PARTITION BY alpha_id ORDER BY c.visited_on)" +
                                "  FROM assessment_" + careTypeAssessments[cnt] + " left join wound_assessment c on c.id=assessment_" + careTypeAssessments[cnt] + ".assessment_id where  c.visited_on>'" + PDate.getDate(start_date, true) + "' and c.visited_on<'" + PDate.getDate(end_date, true) + "' AND c.active=1 and c.treatment_location_id=" + strTreatmentLocationId+") SELECT" +
                                "  alpha_id,  StartDate = MIN(visited_on),  EndDate   = MAX(visited_on)," +
                                "  DayCount  = DATEDIFF( dAY,MIN(visited_on), MAX(visited_on)) + 1" +
                                " FROM ranged GROUP BY alpha_id, RangeId HAVING DATEDIFF( dAY,MIN(visited_on), MAX(visited_on)) + 1 >= " +daysWithDressing+
                                " ORDER BY alpha_id, MIN(visited_on)";
                        System.out.println(woundsWithDressingsCountQuery);
                                
                        
                        //int woundVisits = reportDAO.getInt(reportDAO.getResults(woundVisitsQuery), "count");
                        int woundExisting = Common.getInt(reportDAO.getResults(woundExistingQuery), "count");
                        int woundNew = Common.getInt(reportDAO.getResults(woundNewQuery), "count");
                        int woundClosed = Common.getInt(reportDAO.getResults(woundClosedQuery), "count");
                        int woundBoth = woundExisting + woundNew;
                        int assessmentVisits = Common.getInt(reportDAO.getResults(woundAssessmentsCountQuery), "count");
                        ResultSet results = reportDAO.getResults(woundsWithDressingsCountQuery);
                        int woundDressings = 0;
                        Hashtable alphaHash=new Hashtable();
                        if (results != null) {
                            while (results.next()) {
                                int alpha_id=results.getInt("alpha_id");
                                int days = results.getInt("DayCount");
                                if(alphaHash.get(alpha_id)==null){ 
                                    woundDressings++;
                                    alphaHash.put(alpha_id, days);
                                }
                            }
                        }
                        
                        // FIXME: Is this condition still applicable?
                        if (strTreatmentLocationId.equals("894") && caretype.equals("Wound")) {
                            System.out.println("Existing: " + woundExistingQuery + "\n");
                            System.out.println("New: " + woundNewQuery + "\n");
                            System.out.println("Patient: " + patientsVisitQuery + "\n");
                            System.out.println("Visits: " + totalVisitsQuery + "\n");
                            System.out.println("Closed: " + woundClosedQuery + "\n");
                            System.out.println("Assessment: " + woundAssessmentsCountQuery + "\n");
                            System.out.println("WoundQ: " + woundQuery + "\n");
                            System.out.println("woundDressings: " + woundDressings + "\n");
                        }
                        float avgVisits = 0;
                        if (totalVisits != 0) {
                            avgVisits = patientVisits / totalVisits;
                        }

                        //System.out.println("grouping: "+strGrouping+" careType: "+caretype+" woundVisits: "+woundVisits+" woundExists: "+woundExisting);
                        WoundCounts woundCounts = new WoundCounts();
                        woundCounts.setTip(intTip);
                        woundCounts.setGrouping(strGrouping);
                        woundCounts.setSubGrouping(strSubGrouping);
                        woundCounts.setCareType(caretype);
                        woundCounts.setCareTypeNr(cnt + 1);
                        if (includedPatientCount == false && woundBoth != 0) {//include count only on if row might show up.
                            woundCounts.setPatientCount(patientVisits);
                            woundCounts.setVisitsCount(totalVisits);
                            includedPatientCount = true;
                        } else {
                            woundCounts.setPatientCount(0);
                            woundCounts.setVisitsCount(0);
                        }
                        woundCounts.setWoundCurrentActiveCount(woundBoth - woundClosed);
                        woundCounts.setWoundCount(woundBoth);
                        woundCounts.setWoundExistingCount(woundExisting);
                        woundCounts.setWoundNewCount(woundNew);
                        woundCounts.setWoundClosedCount(woundClosed);
                        woundCounts.setWoundAssessmentsCount(assessmentVisits);

                        double dressingsPercentage = (woundBoth > 0) ? (double) woundDressings / woundBoth : (double) 0.0;
                        woundCounts.setDressings(woundDressings);
                        woundCounts.setDressings_percentage(dressingsPercentage);
                        //System.out.println(strTreatmentLocationId + " " + careTypeAssessments[cnt] + " dressings_percentage: " + dressingsPercentage);

                        if (careTypeAssessments[cnt].equals("wound")) {
                            // Add the total cost for the location to the first entry of it
                            woundCounts.setCost(cost);
                            //System.out.println(strTreatmentLocationId + " " + careTypeAssessments[cnt] + " cost: " + cost);
                        } else {
                            woundCounts.setCost(0.0);
                        }

                        resultList.add(woundCounts);
                        cnt++;
                    }
                }
            }

            List<WoundCounts> sortedWounds = new ArrayList<WoundCounts>();
            // Sort for main report
            for (int intSort1 = 0; intSort1 < resultList.size(); intSort1++) {
                for (int intSort2 = 0; intSort2 < resultList.size() - intSort1 - 1; intSort2++) {
                    WoundCounts sortRec1 = (WoundCounts) resultList.get(intSort2);
                    WoundCounts sortRec2 = (WoundCounts) resultList.get(intSort2 + 1);
                    if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getSubGrouping() + sortRec1.getCareTypeNr()).compareTo(
                            (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getSubGrouping() + sortRec2.getCareTypeNr())) > 0) {
                        resultList.set(intSort2, sortRec2);
                        resultList.set(intSort2 + 1, sortRec1);
                    }
                }
            }

            String strReportTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.title", locale);
            String strGroupingHeader = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.grouping_header", locale);
            String strReportSubTitle = Common.getLocalizedReportString("pixalere.reports.common.grouped_by", locale) + strGroupingHeader;

            // Month for display in subtitle
            DateFormat df = new SimpleDateFormat("MMMMM", Common.getLocaleFromLocaleString(locale));
            //String sMonth = df.format(start_date);
            String strReportDateRange = "";
            if (start_date != null) {
                strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale)
                        + new PDate().getLocalizedDateString(start_date, Common.getLocaleFromLocaleString(locale))
                        + " - "
                        + new PDate().getLocalizedDateString(end_date, Common.getLocaleFromLocaleString(locale));
            }
            
            parameters.put("ReportTitle", strReportTitle);
            parameters.put("ReportSubTitle", strReportSubTitle);
            parameters.put("ReportDateRange", strReportDateRange); // For displaying date ranges etc.
            parameters.put("ReportGroupingHeader", strGroupingHeader);
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));

            sortedWounds.addAll(resultList);
            buildReport("wound_count_with_dressings", sortedWounds, null, null);

        } catch (ApplicationException e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }

        }

    }

    public void generateDashboard(DashboardReportVO report, List<FiscalQuarter> quarters) throws ApplicationException {
        try {
            String fiscal_year = Common.getConfig("fiscalStart");
            String fiscal_quarters = Common.getConfig("fiscalQuarter");

            reportDAO = new ReportDAO();
            ListServiceImpl bservice = new ListServiceImpl(language);
            reportDAO.connect();
            String timeframes_definition = "";
            Hashtable sqlDateRanges = new Hashtable();
            //String connection = Common.getConfig("reporting_server");
            //Setting up date ranges.
            int quarter_count = 0;
            Integer[] quart = new Integer[8];
            Integer[] years = new Integer[8];
            for (FiscalQuarter quarter : quarters) {
                if (quarter != null) {

                    quart[quarter_count] = quarter.getQuarter();
                    years[quarter_count] = (quarter.getEndDate().getYear() - 2000);
                    //quart[quarter_count] = quarter.getQuarter();
                    parameters.put("quarter" + quarter.getOrder() + "_title", Common.getLocalizedString("pixalere.reporting.q" + quarter.getQuarter(), locale));
                    String[] dates = new String[2];
                    dates[0] = PDate.getDate(quarter.getStartDate(), false);
                    dates[1] = PDate.getDate(quarter.getEndDate(), false);
                    timeframes_definition = timeframes_definition + (timeframes_definition.equals("") ? "" : "     ") + Common.getLocalizedString("pixalere.reporting.q" + quarter.getQuarter(), locale) + " (" + PDate.getDate(quarter.getStartDate(), false) + " to " + PDate.getDate(quarter.getEndDate(), false) + ")";

                    sqlDateRanges.put(quarter.getQuarter() + "q" + quarter.getOrder(), dates);
                    quarter_count++;
                }
            }

            String quarter_content = "";
            for (int i : quart) {
                quarter_content += "[" + i + "] ";
            }

            parameters.put("lastyear_title", Common.getLocalizedString("pixalere.reporting.fy", locale));
            parameters.put("yearbeforelast_title", Common.getLocalizedString("pixalere.reporting.yb", locale));

            ReportingServiceImpl rservice = new ReportingServiceImpl();
            String[] dates12 = new String[2];
            FiscalYear lastyear = PDate.getFiscalYear(new Date(), fiscal_year, fiscal_quarters, 0);//(PDate.getDate(Integer.parseInt(PDate.getYear(PDate.getEpochTime() + "", 0)), 1, 1).getTime() / 1000) + "";
            FiscalYear year_before = PDate.getFiscalYear(new Date(), fiscal_year, fiscal_quarters, -1);
            dates12[0] = PDate.getDate(lastyear.getStartYear(), false);
            dates12[1] = PDate.getDate(lastyear.getEndYear(), false);
            //System.out.println("Lastyear_start: " + dates12[0] + " " + lastyear.getStartYear());
            //System.out.println("Lastyear_end: " + dates12[1] + " " + lastyear.getEndYear());
            timeframes_definition = timeframes_definition + "     FY1 (" + PDate.getDate(lastyear.getStartYear(), false) + " to " + PDate.getDate(lastyear.getEndYear(), false) + ")";
            sqlDateRanges.put("ly", dates12);
            //String beforeLastYear = (PDate.getDate(Integer.parseInt(PDate.getYear(PDate.getEpochTime() + "", -2)), 1, 1).getTime() / 1000) + "";
            String[] dates4 = new String[2];
            dates4[0] = PDate.getDate(year_before.getStartYear(), false);
            dates4[1] = PDate.getDate(year_before.getEndYear(), false);
            sqlDateRanges.put("yb", dates4);
            //System.out.println("Before_start: " + dates4[0]);
            //System.out.println("before_end: " + dates4[1]);
            timeframes_definition = timeframes_definition + "     FY2 (" + PDate.getDate(year_before.getStartYear(), false) + " to " + PDate.getDate(year_before.getEndYear(), false) + ")";
            parameters.put("timeframes", timeframes_definition);
            
            // Make sure we have a valid id language for querying the db directly
            int query_language = Common.getLanguageIdFallbackFromLanguageId(language);
            
            String treatment_query = "";
            if (report.getTreatmentLocations() != null && report.getTreatmentLocations().size() > 0) {
                treatment_query = " AND (";
                String locations = "";
                String t = "";
                if (report.getTreatmentLocations() != null) {
                    for (DashboardByLocationVO treatment : report.getTreatmentLocations()) {
                        t = t + " treatment_location_id=" + treatment.getTreatment_location_id() + " OR ";
                        String q = "select name from lookup_l10n where language_id=" + query_language + " AND lookup_id=" + treatment.getTreatment_location_id();
                        ResultSet result = reportDAO.getResults(q);
                        String n = Common.getString(result, "name");
                        locations = locations + n + ",";
                    }
                }
                if (!t.equals("")) {
                    treatment_query = treatment_query + " " + t.substring(0, t.length() - 3);
                    treatment_query = treatment_query + ")";
                    parameters.put("treatmentLocations",
                            Common.getLocalizedReportString("pixalere.reports.dashboard.generated_for_locations", locale)
                            + locations.substring(0, locations.length() - 1));
                }
            }

            //lists, in their final form when sent to report.
            List<DashboardVO> list = new ArrayList();
            HashMap<String, DashboardVO> tmpList = new HashMap();
            HashMap<String, DashboardVO> costList = new HashMap();
            HashMap<String, String> mapArray = new HashMap();
            //setting up titles
            parameters.put("customer_name", Common.getConfig("customer_name"));
            Enumeration e = sqlDateRanges.keys();
            /*parameters.put("minus2_month_title", PDate.getMonthYear(twoMonthAgo1));
             parameters.put("minus3_month_title", PDate.getMonthYear(threeMonthAgo1));*/
            parameters.put("fytd_title", "FYTD " + PDate.getYear((lastyear.getStartYear())));
            parameters.put("total2010_title", "Total " + PDate.getYear((year_before.getStartYear())));
            parameters.put("ReportGenerationDate", PDate.getDateTime(new Date()));

            PDate pdate = new PDate();
            DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
            DefaultPieDataset pieDataset = new DefaultPieDataset();
            //iterate through etiologies
            if (report.getEtiology_all() == null || report.getEtiology_all().equals(new Integer("1"))) {

                //iterate through Hashtable keys Enumeration
                Hashtable healrate_days = new Hashtable();
                while (e.hasMoreElements()) {
                    String key = (String) e.nextElement();
                    //parameters.put(key, "Q"+key.substring(0,1));
                    String[] dateArr = (String[]) sqlDateRanges.get(key);
                    //Visit Count by Patient for Last Month

                    tmpList = reportDAO.getDashboardReport(Common.getLocalizedString("pixalere.admin.dashboard.all_etiology", locale), -1, tmpList, report, key, dateArr[0], dateArr[1], treatment_query, locale);//NA
                    //creating a bar graph image to be embedded in the dashboard report.
                    for (DashboardVO dash : tmpList.values()) {

                        if (dash.getName() != null
                                && dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days1-30", locale))
                                || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days31-90", locale))
                                || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days91-183", locale))
                                || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days184-365", locale))
                                || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days366", locale))) {

                            //System.out.println("Setting Pie Value "+dash.getName()+" "+dash.getQuarter1());
                            pieDataset.setValue(dash.getName(), new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()));
                        }
                        if (dash.getName() != null && (dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_all_wounds", locale)))) {
                            categoryDataset.addValue(new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[0] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter2().equals("") ? "0" : dash.getQuarter2()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[1] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter3().equals("") ? "0" : dash.getQuarter3()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[2] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter4().equals("") ? "0" : dash.getQuarter4()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[3] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter5().equals("") ? "0" : dash.getQuarter5()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[4] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter6().equals("") ? "0" : dash.getQuarter6()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[5] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter7().equals("") ? "0" : dash.getQuarter7()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[6] + " '" + years[0], locale));
                            //categoryDataset.addValue(new Double(dash.getQuarter8().equals("") ? "0" : dash.getQuarter8()), dash.getName(), "#8 " + Common.getLocalizedString("pixalere.reporting.q" + quart[7]));
                            //categoryDataset.addValue(dash.getLastYear(), dash.getName(), Common.getLocalizedString("pixalere.reporting.fy"));
                            //categoryDataset.addValue(dash.getYearBeforeLast(), dash.getName(), Common.getLocalizedString("pixalere.reporting.yb"));
                        }

                        if (dash.getName() != null && (dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_open_wounds", locale)))) {
                            categoryDataset.addValue(new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[0] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter2().equals("") ? "0" : dash.getQuarter2()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[1] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter3().equals("") ? "0" : dash.getQuarter3()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[2] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter4().equals("") ? "0" : dash.getQuarter4()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[3] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter5().equals("") ? "0" : dash.getQuarter5()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[4] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter6().equals("") ? "0" : dash.getQuarter6()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[5] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter7().equals("") ? "0" : dash.getQuarter7()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[6] + " '" + years[0], locale));

                            //categoryDataset.addValue(dash.getQuarter8(), dash.getName(), "#8 " + Common.getLocalizedString("pixalere.reporting.q" + quart[7]));
                            //categoryDataset.addValue(dash.getLastYear(), dash.getName(), Common.getLocalizedString("pixalere.reporting.fy"));
                            //categoryDataset.addValue(dash.getYearBeforeLast(), dash.getName(), Common.getLocalizedString("pixalere.reporting.yb"));
                        }
                        if (dash.getName() != null && (dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_closed_wounds", locale)))) {
                            categoryDataset.addValue(new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[0] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter2().equals("") ? "0" : dash.getQuarter2()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[1] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter3().equals("") ? "0" : dash.getQuarter3()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[2] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter4().equals("") ? "0" : dash.getQuarter4()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[3] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter5().equals("") ? "0" : dash.getQuarter5()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[4] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter6().equals("") ? "0" : dash.getQuarter6()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[5] + " '" + years[0], locale));
                            categoryDataset.addValue(new Double(dash.getQuarter7().equals("") ? "0" : dash.getQuarter7()), dash.getName(), Common.getLocalizedString("pixalere.reporting.q" + quart[6] + " '" + years[0], locale));
                            //categoryDataset.addValue(dash.getQuarter8(), dash.getName(), "#8 " + Common.getLocalizedString("pixalere.reporting.q" + quart[7]));
                            //categoryDataset.addValue(dash.getLastYear(), dash.getName(), Common.getLocalizedString("pixalere.reporting.fy"));
                            //categoryDataset.addValue(dash.getYearBeforeLast(), dash.getName(), Common.getLocalizedString("pixalere.reporting.yb"));
                        }
                    }
                    //System.out.println("End of Round: " + pdate.getDateStringWithHour(new Date()));
                }
            } else {
                for (DashboardByEtiologyVO etiology : report.getEtiologies()) {
                    Hashtable healrate_days = new Hashtable();
                    while (e.hasMoreElements()) {
                        String key = (String) e.nextElement();
                        //parameters.put(key, "Q"+key.substring(0,1));
                        String[] dateArr = (String[]) sqlDateRanges.get(key);
                        //Visit Count by Patient for Last Month

                        LookupVO etLookup = bservice.getListItem(etiology.getEtiology_id());
                        if (etLookup != null) {
                            tmpList = reportDAO.getDashboardReport(etLookup.getName(language), etiology.getEtiology_id(), tmpList, report, key, dateArr[0], dateArr[1], treatment_query, locale);
                        } else {
                            tmpList = reportDAO.getDashboardReport(Common.getLocalizedString("pixalere.admin.dashboard.all_etiology", locale), -1, tmpList, report, key, dateArr[0], dateArr[1], treatment_query, locale);
                        }
                        for (DashboardVO dash : tmpList.values()) {
                            if (dash.getName() != null
                                    && dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days1-30", locale))
                                    || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days31-90", locale))
                                    || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days91-183", locale))
                                    || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days184-365", locale))
                                    || dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days366", locale))) {
                                pieDataset.setValue(dash.getName(), new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()));
                            }
                            //System.out.println("Quarter 1" + dash.getQuarter1());
                            if (dash.getQuarter1().equals("")) {
                                dash.setQuarter1("0");
                            }
                            if (dash.getQuarter2().equals("")) {
                                dash.setQuarter2("0");
                            }
                            if (dash.getQuarter3().equals("")) {
                                dash.setQuarter3("0");
                            }
                            if (dash.getQuarter4().equals("")) {
                                dash.setQuarter4("0");
                            }
                            if (dash.getQuarter5().equals("")) {
                                dash.setQuarter5("0");
                            }
                            if (dash.getQuarter6().equals("")) {
                                dash.setQuarter6("0");
                            }
                            if (dash.getQuarter7().equals("")) {
                                dash.setQuarter7("0");
                            }
                            if (dash.getName() != null && (dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_all_wounds", locale)))) {
                                //System.out.println("QUART: [:" + quart[0] + ":]" + quart[7]);
                                categoryDataset.addValue(new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()), dash.getName(), "#1 " + Common.getLocalizedString("pixalere.reporting.q" + quart[0], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter2().equals("") ? "0" : dash.getQuarter2()), dash.getName(), "#2 " + Common.getLocalizedString("pixalere.reporting.q" + quart[1], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter3().equals("") ? "0" : dash.getQuarter3()), dash.getName(), "#3 " + Common.getLocalizedString("pixalere.reporting.q" + quart[2], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter4().equals("") ? "0" : dash.getQuarter4()), dash.getName(), "#4 " + Common.getLocalizedString("pixalere.reporting.q" + quart[3], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter5().equals("") ? "0" : dash.getQuarter5()), dash.getName(), "#5 " + Common.getLocalizedString("pixalere.reporting.q" + quart[4], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter6().equals("") ? "0" : dash.getQuarter6()), dash.getName(), "#6 " + Common.getLocalizedString("pixalere.reporting.q" + quart[5], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter7().equals("") ? "0" : dash.getQuarter7()), dash.getName(), "#7 " + Common.getLocalizedString("pixalere.reporting.q" + quart[6], locale));
                                //categoryDataset.addValue(dash.getQuarter8(), dash.getName(), "#8 " + Common.getLocalizedString("pixalere.reporting.q" + quart[7]));
                                //categoryDataset.addValue(dash.getLastYear(), dash.getName(), Common.getLocalizedString("pixalere.reporting.fy"));
                                //categoryDataset.addValue(dash.getYearBeforeLast(), dash.getName(), Common.getLocalizedString("pixalere.reporting.yb"));
                            }
                            if (dash.getName() != null && (dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_open_wounds", locale)))) {
                                categoryDataset.addValue(new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()), dash.getName(), "#1 " + Common.getLocalizedString("pixalere.reporting.q" + quart[0], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter2().equals("") ? "0" : dash.getQuarter2()), dash.getName(), "#2 " + Common.getLocalizedString("pixalere.reporting.q" + quart[1], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter3().equals("") ? "0" : dash.getQuarter3()), dash.getName(), "#3 " + Common.getLocalizedString("pixalere.reporting.q" + quart[2], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter4().equals("") ? "0" : dash.getQuarter4()), dash.getName(), "#4 " + Common.getLocalizedString("pixalere.reporting.q" + quart[3], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter5().equals("") ? "0" : dash.getQuarter5()), dash.getName(), "#5 " + Common.getLocalizedString("pixalere.reporting.q" + quart[4], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter6().equals("") ? "0" : dash.getQuarter6()), dash.getName(), "#6 " + Common.getLocalizedString("pixalere.reporting.q" + quart[5], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter7().equals("") ? "0" : dash.getQuarter7()), dash.getName(), "#7 " + Common.getLocalizedString("pixalere.reporting.q" + quart[6], locale));
                                //categoryDataset.addValue(dash.getQuarter8(), dash.getName(), "#8 " + Common.getLocalizedString("pixalere.reporting.q" + quart[7]));
                                //categoryDataset.addValue(dash.getLastYear(), dash.getName(), Common.getLocalizedString("pixalere.reporting.fy"));
                                //categoryDataset.addValue(dash.getYearBeforeLast(), dash.getName(), Common.getLocalizedString("pixalere.reporting.yb"));
                            }
                            if (dash.getName() != null && (dash.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_closed_wounds", locale)))) {
                                categoryDataset.addValue(new Double(dash.getQuarter1().equals("") ? "0" : dash.getQuarter1()), dash.getName(), "#1 " + Common.getLocalizedString("pixalere.reporting.q" + quart[0], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter2().equals("") ? "0" : dash.getQuarter2()), dash.getName(), "#2 " + Common.getLocalizedString("pixalere.reporting.q" + quart[1], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter3().equals("") ? "0" : dash.getQuarter3()), dash.getName(), "#3 " + Common.getLocalizedString("pixalere.reporting.q" + quart[2], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter4().equals("") ? "0" : dash.getQuarter4()), dash.getName(), "#4 " + Common.getLocalizedString("pixalere.reporting.q" + quart[3], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter5().equals("") ? "0" : dash.getQuarter5()), dash.getName(), "#5 " + Common.getLocalizedString("pixalere.reporting.q" + quart[4], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter6().equals("") ? "0" : dash.getQuarter6()), dash.getName(), "#6 " + Common.getLocalizedString("pixalere.reporting.q" + quart[5], locale));
                                categoryDataset.addValue(new Double(dash.getQuarter7().equals("") ? "0" : dash.getQuarter7()), dash.getName(), "#7 " + Common.getLocalizedString("pixalere.reporting.q" + quart[6], locale));
                                //categoryDataset.addValue(dash.getQuarter8(), dash.getName(), "#8 " + Common.getLocalizedString("pixalere.reporting.q" + quart[7]));
                                //categoryDataset.addValue(dash.getLastYear(), dash.getName(), Common.getLocalizedString("pixalere.reporting.fy"));
                                //categoryDataset.addValue(dash.getYearBeforeLast(), dash.getName(), Common.getLocalizedString("pixalere.reporting.yb"));
                            }
                        }
                        //System.out.println("End of Round: " + pdate.getDateStringWithHour(new Date()));
                    }
                }
            }
            parameters.put("barchart_title1", Common.getLocalizedString("pixalere.reporting.cost_title", locale));
            parameters.put("xaxis_title1", Common.getLocalizedString("pixalere.reporting.cost_xaxis", locale));
            parameters.put("yaxis_title1", Common.getLocalizedString("pixalere.reporting.cost_yaxis", locale));
            parameters.put("categoryDataset1", categoryDataset);
            JFreeChart costBarChart = ChartFactory.createBarChart(Common.getLocalizedString("pixalere.reporting.cost_title", locale), Common.getLocalizedString("pixalere.reporting.cost_xaxis", locale), Common.getLocalizedString("pixalere.reporting.cost_yaxis", locale), categoryDataset, PlotOrientation.VERTICAL, true, true, false); //do trends.. and format text.
            BufferedImage costImage = costBarChart.createBufferedImage(453, 300);
            ImageIO.write(costImage, "png", new File("image.png"));
            JFreeChart pieBarChart = ChartFactory.createPieChart(Common.getLocalizedString("pixalere.admin.dashboard.avg_heal_days", locale), pieDataset, false, true, false); //do trends.. and format text.
            BufferedImage pieImage = pieBarChart.createBufferedImage(327, 300);
            ImageIO.write(pieImage, "png", new File("image2.png"));
            parameters.put("Cost", costImage);
            parameters.put("PieChart", pieImage);
            reportDAO.disconnect();
            list = reorganizeList(tmpList);
            buildReport("dashboard", list, null, report.getEmail());
        } catch (ApplicationException e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
                throw new ApplicationException("Exception: " + e.getMessage());
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();

                throw new ApplicationException("Exception: " + e.getMessage());
            } catch (Exception ex) {
            }
        }

    }

    public List<DashboardVO> reorganizeList(HashMap<String, DashboardVO> map) {
        List<DashboardVO> list = new ArrayList();
        Set rows = map.keySet();
        Iterator it = rows.iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            DashboardVO row = map.get(key);
            //key = key.replaceAll("_" + row.getReportName(), "");
            //row.setName(key);
            row.format();
            list.add(row);
        }
        Collections.sort(list, new DashboardVO());
        return list;
    }

    public String getTrend(double last_month, double compare_month) {
        double difference = (1 - (last_month / compare_month)) * 100;
        double result = Common.Round(difference, 2);
        return result + "%";
    }

    private StandardChartTheme getChartTheme() {
        return getChartTheme(false);
    }

    private StandardChartTheme getChartTheme(boolean useSmallFonts) {
        StandardChartTheme theme = (StandardChartTheme) org.jfree.chart.StandardChartTheme.createJFreeTheme();

        String fontName = "Arial";
        theme.setTitlePaint(Color.decode("#3333CC"));
        theme.setExtraLargeFont(new Font(fontName, Font.PLAIN, 15)); //title
        theme.setLargeFont(new Font(fontName, Font.PLAIN, 14)); //axis-title
        theme.setRegularFont(new Font(fontName, Font.PLAIN, 11));
        theme.setSmallFont(new Font(fontName, Font.PLAIN, 9));
        theme.setRangeGridlinePaint(Color.decode("#ADADAD"));
        theme.setPlotBackgroundPaint(Color.white);
        theme.setChartBackgroundPaint(Color.white);
        theme.setGridBandPaint(Color.red);
        theme.setAxisLabelPaint(Color.decode("#333333"));

        if (useSmallFonts) {
            theme.setExtraLargeFont(new Font(fontName, Font.PLAIN, 13)); //title
            theme.setLargeFont(new Font(fontName, Font.PLAIN, 12)); //axis-title
            theme.setRegularFont(new Font(fontName, Font.PLAIN, 10));
            theme.setSmallFont(new Font(fontName, Font.PLAIN, 8));
        }

        return theme;
    }

    public void buildReport(String report_type, List reportArray, String filename, String email) throws ServletException, IOException {
        try {

            /*String strLocationsSelection = "This report contains data from all treatment locations to which the professional who created it, has access to.";
             if (Common.getPartialSelectedLocations(treatment_location, userVO.getRegions()) == true) {
             strLocationsSelection = "This report contains data from selected Treatment Locations only";
             }
             parameters.put("ReportLocationsSelection", strLocationsSelection);*/

            parameters.put(JRParameter.REPORT_LOCALE, Common.getLocaleFromLocaleString(locale));
            
            com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(userVO.getId(), -1, "REPORT", report_type, -1));
           
            try {
                if (output == Constants.SUMMARY_REPORT) {
                    List<SummaryVO> dash = (List<SummaryVO>) reportArray;
                    HttpSession session = request.getSession();
                    ServletContext servletContext = session.getServletContext();
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    InputStream reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    byte[] bytes = null;
                    parameters.put("SubReportPath", servletContext.getRealPath("/reports") + "/");
                    parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                    bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanCollectionDataSource(dash));

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (output == Constants.CCAC_REPORT) {
                    List<CCACReportVO> dash = (List<CCACReportVO>) reportArray;
                    HttpSession session = request.getSession();
                    ServletContext servletContext = session.getServletContext();
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    InputStream reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    byte[] bytes = null;
                    parameters.put("SubReportPath", servletContext.getRealPath("/reports") + "/");
                    bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanCollectionDataSource(dash));

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (output == Constants.PATIENT_FILE_REPORT) {
                    List<PatientFile> dash = (List<PatientFile>) reportArray;
                    HttpSession session = request.getSession();
                    ServletContext servletContext = session.getServletContext();
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    InputStream reportStream = this.getClass().getClassLoader().getResourceAsStream("/reports/" + report_type.toLowerCase() + ".jasper");
                    byte[] bytes = null;
                    parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                    bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanCollectionDataSource(dash));

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (output == Constants.WOUND_ACQUIRED_REPORT) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();
                    List<WoundAcquiredVO> report = (List<WoundAcquiredVO>) reportArray;
                    InputStream reportStream = null;
                    try {
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }
                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(report.toArray()));
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (report_type.equals("wound_count_with_dressings")) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();
                    List<WoundCounts> dataList = (List<WoundCounts>) reportArray;

                    InputStream reportStream = null;
                    try {
                        parameters.put("SubReportPath", servletContext.getRealPath("/reports") + "/");
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }

                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }
                    JasperReport jasperReport = getCompiledReport(filename);
                    if(doc_type!=null && doc_type.equals("word")){
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,new JRBeanArrayDataSource(dataList.toArray()));
                        generateWordOutput(jasperPrint,request,response);
                    }else if(doc_type!=null && doc_type.equals("excel")){
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,new JRBeanArrayDataSource(dataList.toArray()));
                        generateExcelOutput(jasperPrint,request,response);
                    }else{
                        generatePDFOutput(jasperReport,response,parameters,new JRBeanArrayDataSource(dataList.toArray()));
                        /*byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(dataList.toArray()));
                        ServletOutputStream servletOutputStream = response.getOutputStream();
                        response.setContentType("application/pdf");
                        response.setContentLength(bytes.length);
                        response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                        servletOutputStream.write(bytes, 0, bytes.length);
                        servletOutputStream.flush();
                        servletOutputStream.close();*/
                    }
                } else if (report_type.equals("facility_comparison")) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();
                    List<FacilityComparisonVO> report = (List<FacilityComparisonVO>) reportArray;
                    InputStream reportStream = null;
                    try {
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(report.toArray()));
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (report_type.equals("referrals")) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();
                    List<ReferralVO> report = (List<ReferralVO>) reportArray;
                    InputStream reportStream = null;
                    try {
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(report.toArray()));
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (report_type.equals("usage_stats")) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();
                    List<UsageStats> report = (List<UsageStats>) reportArray;
                    InputStream reportStream = null;
                    try {
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }
                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(report.toArray()));
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (output == Constants.DASHBOARD_REPORT) {
                    List<DashboardVO> dash = (List<DashboardVO>) reportArray;
                    parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                    InputStream reportStream = null;
                    try {
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }
                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(dash.toArray()));
                    SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
                    String[] emails = email.split(",");
                    mail.postMail(emails, 
                            Common.getLocalizedString("pixalere.reporting.form.report_dashboard", locale) + " " + PDate.getMonthYear(PDate.getEpochTime() + ""), 
                            Common.getLocalizedString("pixalere.reporting.form.report_dashboard", locale) + " email",
                            bytes, "dashboardreport");//String[] recipients, String subject, String message,byte[] byteArray, String attachment_name

                } else if (report_type.equals("patient_outcomes")) {
                    List<PatientOutcomes> dash = (List<PatientOutcomes>) reportArray;
                    parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                    InputStream reportStream = null;
                    try {
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }
                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(dash.toArray()));
                    SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
                    String[] emails = email.split(",");
                    mail.postMail(emails, 
                            Common.getLocalizedString("pixalere.reporting.form.report_patient_outcomes", locale) + " " + PDate.getMonthYear(PDate.getEpochTime() + ""), 
                            Common.getLocalizedString("pixalere.reporting.form.report_patient_outcomes", locale) + " email", 
                            bytes, "patient_outcomes");//String[] recipients, String subject, String message,byte[] byteArray, String attachment_name

                } else if (report_type.equals("wound_tracking_mail")) {
                    // FIXME: Temporal code, report enqueued
                    List<WoundTracking> dash = (List<WoundTracking>) reportArray;
                    parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                    InputStream reportStream = null;
                    try {
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }
                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(dash.toArray()));
                    SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
                    String[] emails = email.split(",");
                    mail.postMail(emails, 
                            Common.getLocalizedString("pixalere.reporting.form.report_wound_tracking", locale) + " " + PDate.getMonthYear(PDate.getEpochTime() + ""), 
                            Common.getLocalizedString("pixalere.reporting.form.report_wound_tracking", locale) + " email", 
                            bytes, "wound_tracking");//String[] recipients, String subject, String message,byte[] byteArray, String attachment_name

                } else if (report_type.equals("wound_tracking_alpha") || report_type.equals("wound_tracking_beta")) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();

                    List<WoundTracking> dataList = (List<WoundTracking>) reportArray;

                    InputStream reportStream = null;
                    try {
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }

                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }

                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(dataList.toArray()));
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (report_type.equals("wound_list")) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();

                    List<WoundList> dataList = (List<WoundList>) reportArray;

                    InputStream reportStream = null;
                    try {
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }

                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }

                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(dataList.toArray()));
                    System.out.println("REPOT:" + "/reports/" + report_type.toLowerCase() + ".jasper" + bytes);
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (report_type.equals("wound_tracking_details_monthly")) {
                    HttpSession session = request.getSession();
                    servletContext = session.getServletContext();
                    List<WoundTracking> dataList = (List<WoundTracking>) reportArray;

                    InputStream reportStream = null;
                    try {
                        parameters.put("ImagesPath", servletContext.getRealPath("/images") + "/");
                        reportStream = new FileInputStream(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new FileInputStream(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }

                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }

                    byte[] bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(dataList.toArray()));
                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                } else if (output == Constants.EXPORT_PDF) {
                    List<DashboardVO> dash = (List<DashboardVO>) reportArray;
                    Properties properties = new Properties();
                    try {
                        properties.load(new FileInputStream("ApplicationResources.properties"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    parameters.put("ImagesPath", properties.getProperty("pixalere.report.images"));
                    InputStream reportStream = new FileInputStream(properties.getProperty("pixalere.report.patientfile") + report_type.toLowerCase() + ".jasper");
                    if (filename == null) {
                        filename = report_type.toLowerCase();
                    }
                    JasperPrint jasperPrintPDF = JasperFillManager.fillReport(reportStream, parameters, new JRBeanArrayDataSource(dash.toArray()));
                    JasperExportManager.exportReportToPdfFile(jasperPrintPDF, Common.getConfig("dataPath") + "/" + filename);
                }
            } catch (JRException ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public CCACReportVO getCCACReport(String id) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            CCACReportVO r = new CCACReportVO();
            r.setId(new Integer(id));
            return configurationDAO.findByCriteria(r);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }

    public Collection<CCACReportVO> getCCACReports(int active, int already_retrieved, Date start_date, Date end_date, int[] treatment_location_id) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            return (Collection) configurationDAO.findAllByCriteria(active, already_retrieved, start_date, end_date, treatment_location_id);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }

    public void saveCCACReport(CCACReportVO configurationVO) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            configurationDAO.insertCCACReport(configurationVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(), e);
        }
    }

    public void retrievedCCACReport(String id) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            CCACReportVO report = getCCACReport(id);
            report.setAlready_retrieved(1);
            configurationDAO.insertCCACReport(report);
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(), e);
        }
    }

    public void removeCCACReport(CCACReportVO configurationVO) throws ApplicationException {

        ReportDAO configurationDAO = new ReportDAO();
        configurationDAO.delete(configurationVO);

    }

    /**
     * The LTC report is a report with a list of patient, and their relavent LTC
     * information. Our enterprise (large cutomer's likel would not use this
     * report as it would be too large.
     *
     * @todo incomplete, it was a high priority and quickly shifted. This report
     * needs to be completed.
     * @param prof the professional, use this to decide what they can see
     * @param start
     * @param end
     * @return patients a collection of patients with their information
     * @throws ApplicationException
     */
    public Collection<LTCPatientList> getLTCPatientReport(ProfessionalVO prof, Date start, Date end) throws ApplicationException {
        Collection<LTCPatientList> patient_list = null;
        try {
            reportDAO = new ReportDAO();
            ListServiceImpl bservice = new ListServiceImpl(language);
            PatientProfileServiceImpl ppservice = new PatientProfileServiceImpl(language);
            PatientServiceImpl pservice = new PatientServiceImpl(language);
            WoundServiceImpl wservice = new WoundServiceImpl(language);
            WoundAssessmentServiceImpl aservice = new WoundAssessmentServiceImpl();
            WoundAssessmentLocationServiceImpl walservice = new WoundAssessmentLocationServiceImpl();
            reportDAO.connect();

            String mssql_top = "1000";
            String mysql_limit = "1000";
            if (reportDAO.isMysql() == true) {
                mysql_limit = "limit " + 500;
            } else {
                mssql_top = "top " + 500;
            }
            if (start == null) {
                start = PDate.subtractDays(new Date(), 365);
            }
            if (end == null) {
                end = new Date();
            }
            String treatment_locations = "";
            for (UserAccountRegionsVO region : prof.getRegions()) {
                if (!region.getTreatment_location_id().equals(Constants.TRAINING_TREATMENT_ID)) {
                    treatment_locations = treatment_locations + " treatment_location_id = " + region.getTreatment_location_id() + " OR ";
                }
            }
            if (treatment_locations.length() > 4) {
                treatment_locations = treatment_locations.substring(0, treatment_locations.length() - 4);
            }
            String getlist_query = "select " + mssql_top + " distinct(patient_id) as patient_id from wound_assessment where active=1 and created_on>=" + PDate.getDate(start, true) + " and created_on<=" + PDate.getDate(end, true) + " " + mysql_limit;
            ResultSet result = reportDAO.getResults(getlist_query);
            if (result != null) {
                while (result.next()) {
                    LTCPatientList patient = new LTCPatientList();
                    int patient_id = result.getInt("patient_id");
                    patient.setId(patient_id);
                    String patient_item_query = "select name,lastname,funding_source,treatment_location_id,dob from patient_accounts where current_flag=1 and patient_id=" + patient_id;
                    ResultSet result2 = reportDAO.getResults(patient_item_query);
                    if (result2 != null) {
                        while (result2.next()) {
                            String name = result2.getString("name");
                            String lastname = result2.getString("lastname");
                            int funding_id = result2.getInt("funding_source");
                            int treatment_id = result2.getInt("treatment_location_id");
                            String dob = result2.getString("dob");
                            LookupVO fund = bservice.getListItem(funding_id);
                            LookupVO treat = bservice.getListItem(treatment_id);
                            if (treat != null) {
                                patient.setLocation(treat.getName(language));
                            }
                            if (fund != null) {
                                patient.setFunding_source(fund.getName(language));
                            }
                            patient.setName(name + " " + lastname);
                            PatientProfileVO proftmp = new PatientProfileVO();
                            proftmp.setPatient_id(patient_id);
                            proftmp.setActive(1);
                            proftmp.setCurrent_flag(1);
                            PatientProfileVO latest = ppservice.getPatientProfile(proftmp);
                            if (latest != null) {
                                String como = "";
                                try {
                                    Collection<PatientProfileArraysVO> arrays = latest.getPatient_profile_arrays();
                                    PatientProfileArraysVO[] rr = new PatientProfileArraysVO[arrays.size()];
                                    rr = arrays.toArray(rr);
                                    List<ArrayValueObject> list = Common.filterArrayObjects(rr, LookupVO.COMORBIDITIES);
                                    como = Serialize.serialize(list, ",", language);
                                    patient.setComorbidities(como);

                                } catch (ClassCastException e) {
                                    e.printStackTrace();
                                }
                            }
                            WoundAssessmentDAO was = new WoundAssessmentDAO();
                            Collection<WoundAssessmentVO> assess = null; //get all assessments by user.
                            try {
                                WoundAssessmentVO wass = new WoundAssessmentVO();
                                wass.setPatient_id(patient_id);
                                wass.setActive(1);
                                assess = was.findAllByCriteriaByRange(wass, "", "");
                            } catch (Exception e) {
                            }
                            List filterVisits = aservice.getVisitCount(assess, null, null, language);
                            //get visit count
                            WoundVO wtmp = new WoundVO();
                            wtmp.setActive(1);
                            wtmp.setPatient_id(patient_id);
                            Collection<WoundVO> wounds = wservice.getWounds(wtmp);
                            Collection<WoundProfileInfo> info = new ArrayList();
                            for (WoundVO wound : wounds) {
                                WoundProfileInfo w = new WoundProfileInfo();
                                w.setWound_profile(wound.getWound_location_detailed());
                                w.setStart_date(PDate.getDateTime(wound.getStart_date()));
                                WoundAssessmentLocationVO atmp = new WoundAssessmentLocationVO();
                                atmp.setWound_id(wound.getId());
                                atmp.setActive(1);
                                Collection<WoundAssessmentLocationVO> alphas = walservice.getAllAlphas(atmp);
                                Collection<WoundAlphaInfo> alpha_info = new ArrayList();
                                WoundAssessmentVO assVisTmp = new WoundAssessmentVO();
                                assVisTmp.setWound_id(wound.getId());
                                assVisTmp.setActive(1);
                                assVisTmp.setVisit(1);
                                Collection<WoundAssessmentVO> ass = aservice.getAllAssessments(assVisTmp);
                                w.setNum_visits(ass.size());

                                for (WoundAssessmentLocationVO alpha : alphas) {
                                    EtiologyVO etmp = new EtiologyVO();
                                    GoalsVO gtmp = new GoalsVO();
                                    etmp.setAlpha_id(alpha.getId());
                                    gtmp.setAlpha_id(alpha.getId());
                                    EtiologyVO etiology = wservice.getEtiology(etmp);
                                    GoalsVO goals = wservice.getGoal(gtmp);
                                    WoundAlphaInfo al = new WoundAlphaInfo();
                                    al.setAlpha(alpha.getAlphaName(locale));
                                    al.setEnd_date(alpha.getClose_timestamp() + "");
                                    al.setStart_date(alpha.getOpen_timestamp() + "");
                                    al.setEtiology(etiology.getPrintable(language));
                                    al.setGoals(goals.getPrintable(language));
                                    alpha_info.add(al);
                                }
                                w.setAlphas(alpha_info);
                            }

                        }
                    }
                    patient_list.add(patient);
                }
            }
            //get all patients (limit 500)
            //String patient_query = "select firstname,lastname,funding_source,treatment_location_id,dob from patient_accounts ";
        } catch (ApplicationException e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();
                throw new ApplicationException("Exception: " + e.getMessage());
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                reportDAO.disconnect();

                throw new ApplicationException("Exception: " + e.getMessage());
            } catch (Exception ex) {
            }
        }
        return patient_list;
    }
    private void generatePDFOutput(
        JasperReport jasperReport,
        HttpServletResponse response,
        HashMap parameters,JRBeanArrayDataSource dataList)
        throws JRException, IOException {

            byte[] bytes = null;

            bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,dataList);

            response.setContentType("application/pdf");
            response.setContentLength(bytes.length);
  
            response.getOutputStream().write(bytes, 0, bytes.length);
            response.getOutputStream().flush();
            response.getOutputStream().close();

    }
    private void generateWordOutput(
        JasperPrint jasperPrint,
        HttpServletRequest request,
        HttpServletResponse response) throws JRException, IOException{

            if (jasperPrint != null) {
                jasperPrint.setOrientation(OrientationEnum.LANDSCAPE);  
                jasperPrint.setLeftMargin(0);jasperPrint.setRightMargin(0);
                ByteArrayOutputStream docxReport = new ByteArrayOutputStream();
                JRDocxExporter exporter = new JRDocxExporter();
                exporter.setParameter(JRDocxExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRDocxExporterParameter.OUTPUT_STREAM, docxReport);
                exporter.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
                exporter.setParameter(JRDocxExporterParameter.CHARACTER_ENCODING, "UTF-8");

                exporter.exportReport();
                // Send response
                byte[] bytes = docxReport.toByteArray();
        
                response.addHeader("Content-disposition",
                                "attachment;filename=pixalere.docx");
                response.setContentType("application/vnd.ms-word");
                response.setContentLength(bytes.length);
                response.getOutputStream().write(bytes, 0, bytes.length);
                response.getOutputStream().flush();
                response.getOutputStream().close();


            }
            else {
                Writer writer = response.getWriter();
                writer.write("Aucun rapport Ã  afficher");
                response.setContentType("text/HTML");
            }
    }
    private void generateExcelOutput(
        JasperPrint jasperPrint,
        HttpServletRequest request,
        HttpServletResponse response) throws JRException, IOException{

            if (jasperPrint != null) {
               
                ByteArrayOutputStream docxReport = new ByteArrayOutputStream();
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setParameter(JRDocxExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRDocxExporterParameter.OUTPUT_STREAM, docxReport);
                exporter.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
                exporter.setParameter(JRDocxExporterParameter.CHARACTER_ENCODING, "UTF-8");

                exporter.exportReport();
                // Send response
                byte[] bytes = docxReport.toByteArray();
        
                response.addHeader("Content-disposition",
                                "attachment;filename=pixalere.docx");
                response.setContentType("application/vnd.ms-word");
                response.setContentLength(bytes.length);
                response.getOutputStream().write(bytes, 0, bytes.length);
                response.getOutputStream().flush();
                response.getOutputStream().close();


            }
            else {
                Writer writer = response.getWriter();
                writer.write("Aucun rapport Ã  afficher");
                response.setContentType("text/HTML");
            }
    }
    private JasperReport getCompiledReport(String report_type)
            throws JRException {
            File reportStream = null;
            try {
                        
                        reportStream = new File(servletContext.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
                    } catch (Exception e) {
                        reportStream = new File(servletContext.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
                    }
            

            JasperReport jasperReport =
                    (JasperReport) JRLoader.loadObject(reportStream.getPath());

            return jasperReport;
    }
    /**
     * @param language the language to set
     */
    public void setLanguage(Integer language) {
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(HashMap parameters) {
        this.parameters = parameters;
    }

    ReportDAO reportDAO = null;
    private String report_type;
    private ProfessionalVO userVO;
    private static final String RESOURCE_BUNDLE = "ApplicationResources";
    private String basepath = null;
    private String absolutepath = null;
    private int output = 5;
    private final static String DASHBOARD = "dashboard";
}
