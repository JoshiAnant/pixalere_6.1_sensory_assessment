package com.pixalere.reporting.service;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.reporting.bean.ScheduleConfigVO;
import com.pixalere.reporting.bean.ScheduleLocationVO;
import com.pixalere.reporting.bean.ReportScheduleVO;
import com.pixalere.reporting.dao.ReportSchedulerDAO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose
 */
public class ReportSchedulerService {
    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportSchedulerService.class);

    
    private ReportSchedulerDAO dao;

    public ReportSchedulerService() {
        dao = new ReportSchedulerDAO();
    }
    
    public List<ReportScheduleVO> getAllScheduled(){
        List<ReportScheduleVO> list = new ArrayList<ReportScheduleVO>();
        try {
            ReportScheduleVO criteriaVO = new ReportScheduleVO();
            Collection<ReportScheduleVO> col = dao.findAllByCriteria(criteriaVO);
            list.addAll(col);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<ScheduleConfigVO> getScheduledForExecution(Date limit_date){
        List<ScheduleConfigVO> list = new ArrayList<ScheduleConfigVO>();
        
        try {
            Collection<ScheduleConfigVO> col = dao.findScheduledConfigExpired(limit_date);
            list.addAll(col);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return list;
    }
    
    /**
     * This method receives a set of schedules from ReportQueue job corresponding
     * to the configured executions about to be performed. It counts the amount
     * of pending executions in each schedule, and returns a list of those schedules
     * that will hit the limit after the current report generations.
     * 
     * @param schedules
     * @return 
     */
    public List<ReportScheduleVO> getSchedulesToAlert(Set<ReportScheduleVO> schedules) {
        List<ReportScheduleVO> list = new ArrayList<ReportScheduleVO>();
        
        for (ReportScheduleVO schedule : schedules) {
            // Next reports for this setup, Sorted by execution date
            List<ScheduleConfigVO> confs = new ArrayList<ScheduleConfigVO>();
            confs.addAll(schedule.getConfigs());
            Collections.sort(confs, new Comparator<ScheduleConfigVO>() {

                @Override
                public int compare(ScheduleConfigVO o1, ScheduleConfigVO o2) {
                    if (o1.getExecution_on() == null || o2.getExecution_on() == null) return 0;
                    return o1.getExecution_on().compareTo(o2.getExecution_on());
                }
            });
            // Cut off already executed 
            for (Iterator<ScheduleConfigVO> it = confs.iterator(); it.hasNext();) {
                ScheduleConfigVO configVO = it.next();
                if (configVO.getProcessed().equals(1)){
                    it.remove();
                }
            }
            
            // If the remaining number (size-1) is less or equal to the limit add the schedule to the list
            if (confs.size() - 1 <= schedule.getReminder_limit()){
                list.add(schedule);
            }
        }
        
        return list;
    }
    
    public ReportScheduleVO getScheduleByCriteria(ReportScheduleVO vo){
        ReportScheduleVO prov = null;
        try {
            prov = (ReportScheduleVO) dao.findByCriteria(vo);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }
    
    public ReportScheduleVO getScheduleById(Integer id){
        ReportScheduleVO prov = null;
        try {
            prov = (ReportScheduleVO) dao.findByPK(id);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }
    
    public ScheduleConfigVO getScheduleConfigById(Integer id) throws ApplicationException{
        ScheduleConfigVO config = new ScheduleConfigVO();
        config.setId(id);
        try {
            config = (ScheduleConfigVO) dao.findScheduleConfigByCriteria(config);
        } catch (DataAccessException ex) {
            log.error("Error in dao.getScheduleConfigById(): " + ex.toString());
            throw new ApplicationException("Error in ReportSchedulerService.getScheduleConfigById(): " + ex.toString(), ex);
        }
        
        return config;
    }
    
    public ScheduleConfigVO getScheduleConfigByCriteria(ScheduleConfigVO vo){
        ScheduleConfigVO prov = null;
        try {
            prov = (ScheduleConfigVO) dao.findScheduleConfigByCriteria(vo);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }
    
    public void removeLocation(ScheduleLocationVO location)throws ApplicationException {
        try {
            dao.deleteLocation(location);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("ReportSchedulerService threw an error: " + ex.getMessage());
        }
    }
    
    public void removeConfig(ScheduleConfigVO config)throws ApplicationException {
        try {
            dao.deleteConfig(config);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("ReportSchedulerService threw an error: " + ex.getMessage());
        }
    }
    
    public void removeSchedule(ReportScheduleVO provider)throws ApplicationException {
        try {
            dao.delete(provider);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("ReportSchedulerService threw an error: " + ex.getMessage());
        }
    }
    
    public void saveSchedule(ReportScheduleVO schedule) throws ApplicationException {
        try {
            dao.update(schedule);
        } catch (DataAccessException ex) {
            Logger.getLogger(ReportSchedulerService.class.getName()).log(Level.SEVERE, null, ex);
            throw new ApplicationException("ReportSchedulerService threw an error: " + ex.getMessage());
        }
    }
    
    public void saveConfig(ScheduleConfigVO vo) throws ApplicationException {
        try {
            dao.updateConfig(vo);
        } catch (DataAccessException e) {
            log.error("Error in dao.saveConfig(): " + e.toString());
            throw new ApplicationException("Error in ReportSchedulerService.saveConfig(): " + e.toString(), e);
        }
    }
    
    public void saveLocation(ScheduleLocationVO vo) throws ApplicationException {
        try {
            dao.updateLocation(vo);
        } catch (DataAccessException e) {
            log.error("Error in dao.saveLocation(): " + e.toString());
            throw new ApplicationException("Error in ReportSchedulerService.saveLocation(): " + e.toString(), e);
        }
    }
}
