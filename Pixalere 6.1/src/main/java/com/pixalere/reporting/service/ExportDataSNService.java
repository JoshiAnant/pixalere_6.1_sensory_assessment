/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.service;

import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.reporting.bean.ExportDataSNVO;
import com.pixalere.reporting.dao.ExportDataSNDAO;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Jose
 */
public class ExportDataSNService {
    private final Integer language;
    private final String locale;
    private final ServletContext ctx;
    HttpServletRequest request;
    HttpServletResponse response;
    
    // Services
    ListServiceImpl listService;
    
    // The map of parameters
    HashMap reportParams;

    public ExportDataSNService(Integer language, ServletContext ctx) {
        this.language = language;
        this.ctx = ctx;
        this.locale = Common.getLanguageLocale(language);
    }

    public ExportDataSNService(Integer language, HttpServletRequest request, HttpServletResponse response) {
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
        this.request = request;
        this.response = response;
        this.ctx = (request != null && request.getSession() != null ? request.getSession().getServletContext() : null);
    }
    
    public void exportData(Date start_date, Date end_date, List<Integer> locationsList){
        String startDate = PDate.getDate(start_date, false);
        String endDate = PDate.getDate(end_date, false);
        this.reportParams = new HashMap();
        
        ExportDataSNDAO dao = new ExportDataSNDAO();
        dao.connect();
        List<ExportDataSNVO> dataList = dao.getData(startDate, endDate, locationsList);
        dao.disconnect();
        
        excelToRequest(dataList);
    }
    
    private void excelToRequest(List<ExportDataSNVO> dataList){
        File reportStream = null;
        String report_type = "report_sn_extract";
        
        try {
            try {
                reportParams.put("SubReportPath", ctx.getRealPath("/reports") + "/");
                reportStream = new File(ctx.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
            } catch (Exception e) {
                reportStream = new File(ctx.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
            }
            
            reportParams.put(JRParameter.REPORT_LOCALE, Common.getLocaleFromLocaleString(locale));
            
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportStream.getPath());
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, reportParams, new JRBeanArrayDataSource(dataList.toArray()));
            
            if (jasperPrint != null) {

                ByteArrayOutputStream excelReport = new ByteArrayOutputStream();
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, excelReport);
                exporter.setParameter(JRXlsExporterParameter.CHARACTER_ENCODING, "UTF-8");
                exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, report_type + ".xls");
                exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
                exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE); 
                exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE); 
                exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, Boolean.FALSE);
                jasperPrint.setProperty("net.sf.jasperreports.export.xls.detect.cell.type", "false");
                jasperPrint.setProperty("net.sf.jasperreports.export.xls.size.fix.enabled", "false");
                
                exporter.exportReport();
                // Send response
                byte[] bytes = excelReport.toByteArray();

                response.addHeader("Content-disposition",
                                "attachment;filename=" + report_type + ".xls");
                response.setContentType("application/vnd.ms-excel");
                response.setContentLength(bytes.length);
                response.getOutputStream().write(bytes, 0, bytes.length);
                response.getOutputStream().flush();
                response.getOutputStream().close();

            } else {
                Writer writer = response.getWriter();
                writer.write("The report couldn't be generated / was empty");
                response.setContentType("text/HTML");
            }
            
        } catch (JRException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
            
    }
 
}
