/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pixalere.reporting.service;

import com.pixalere.common.DataAccessException;
import com.pixalere.reporting.bean.*;
import com.pixalere.reporting.dao.ReportDAO;
import com.pixalere.common.ApplicationException;
import java.util.Collection;
import java.util.Date;

/**
 * Retrieves and saves the Reporting specific configurations and user reports
 *
 * @since 5.0
 * @author travis
 */
public class ReportingServiceImpl {

    static private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportingServiceImpl.class);

    public ReportingServiceImpl() {
    }

    public void saveDashboardReport(DashboardReportVO dashboard) throws ApplicationException {
        try {
            ReportDAO dao = new ReportDAO();
            dao.insert(dashboard);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
    }

    public void removeDashboardReport(DashboardReportVO dashboard) throws ApplicationException {
        ReportDAO dao = new ReportDAO();
        dao.delete(dashboard);

    }

    public DashboardReportVO getDashboardReport(int id) throws ApplicationException {
        DashboardReportVO dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (DashboardReportVO) dao.findByID(id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public void saveDashboardByEtiology(DashboardByEtiologyVO dashboard) throws ApplicationException {
        try {
            ReportDAO dao = new ReportDAO();
            dao.insertDashboardByEtiology(dashboard);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
    }

    public void saveDashboardByLocation(DashboardByLocationVO dashboard) throws ApplicationException {
        try {
            ReportDAO dao = new ReportDAO();
            dao.insertDashboardByLocation(dashboard);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
    }

    public DashboardReportVO getDashboardReport(DashboardReportVO d) throws ApplicationException {
        DashboardReportVO dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (DashboardReportVO) dao.findByCriteria(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public ReportQueueVO getReportQueue(ReportQueueVO d) throws ApplicationException {
        ReportQueueVO dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (ReportQueueVO) dao.findQueueByCriteria(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public Collection<DashboardReportVO> getDashboardReports(DashboardReportVO d) throws ApplicationException {
        Collection<DashboardReportVO> dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (Collection<DashboardReportVO>) dao.findAllByCriteria(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public Collection<ReportQueueVO> getReportQueues(ReportQueueVO d) throws ApplicationException {
        Collection<ReportQueueVO> dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            d.setActive(1);
            dash = (Collection<ReportQueueVO>) dao.findAllQueueByCriteria(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public void saveReportQueue(ReportQueueVO d) throws ApplicationException {
        try {
            ReportDAO dao = new ReportDAO();
            dao.insertQueue(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
    }

    public void removeReportQueue(ReportQueueVO d) throws ApplicationException {
        ReportDAO dao = new ReportDAO();
        ReportQueueWoundTypeVO t1 = new ReportQueueWoundTypeVO();
        t1.setReport_id(d.getId());
        ReportQueueEtiologyVO t2 = new ReportQueueEtiologyVO();
        t2.setReport_id(d.getId());
        ReportQueueTreatmentsVO t3 = new ReportQueueTreatmentsVO();
        t3.setReport_id(d.getId());
        dao.deleteQueueEtiology(t2);
        dao.deleteQueueTreatment(t3);
        dao.deleteWoundType(t1);
        d.setActive(0);
        try {

            dao.insertQueue(d);

        } catch (DataAccessException e) {
            e.printStackTrace();
        }

    }

    public ReportQueueVO getReportQueue(int id) throws ApplicationException {
        ReportQueueVO dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (ReportQueueVO) dao.findQueueByID(id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }
    //

    public Collection<ReportQueueWoundTypeVO> getReportQueueTypes(ReportQueueWoundTypeVO d) throws ApplicationException {
        Collection<ReportQueueWoundTypeVO> dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (Collection<ReportQueueWoundTypeVO>) dao.findAllWoundTypeByCriteria(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public void saveReportQueueType(ReportQueueWoundTypeVO d) throws ApplicationException {
        try {
            ReportDAO dao = new ReportDAO();
            dao.insertWoundType(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
    }

    public void removeReportQueueType(ReportQueueWoundTypeVO d) throws ApplicationException {
        ReportDAO dao = new ReportDAO();
        dao.deleteWoundType(d);
    }

    public ReportQueueWoundTypeVO getReportQueueWoundType(int id) throws ApplicationException {
        ReportQueueWoundTypeVO dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (ReportQueueWoundTypeVO) dao.findWoundTypeByID(id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    //
    public Collection<ReportQueueEtiologyVO> getReportQueueEtiologies(ReportQueueEtiologyVO d) throws ApplicationException {
        Collection<ReportQueueEtiologyVO> dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (Collection<ReportQueueEtiologyVO>) dao.findAllQueueEtiologyByCriteria(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public void saveReportQueueEtiology(ReportQueueEtiologyVO d) throws ApplicationException {
        try {
            ReportDAO dao = new ReportDAO();
            dao.insertQueueEtiology(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
    }

    public void removeReportQueueEtiology(ReportQueueEtiologyVO d) throws ApplicationException {
        ReportDAO dao = new ReportDAO();
        dao.deleteQueueEtiology(d);
    }

    public ReportQueueEtiologyVO getReportQueueEtiology(int id) throws ApplicationException {
        ReportQueueEtiologyVO dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (ReportQueueEtiologyVO) dao.findQueueEtiologyByID(id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    //
    public Collection<ReportQueueTreatmentsVO> getReportQueueTreatments(ReportQueueTreatmentsVO d) throws ApplicationException {
        Collection<ReportQueueTreatmentsVO> dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (Collection<ReportQueueTreatmentsVO>) dao.findAllQueueTreatmentsByCriteria(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public void saveReportQueueTreatment(ReportQueueTreatmentsVO d) throws ApplicationException {
        try {
            ReportDAO dao = new ReportDAO();
            dao.insertQueueTreatment(d);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
    }

    public void removeReportQueueTreatment(ReportQueueTreatmentsVO d) throws ApplicationException {
        ReportDAO dao = new ReportDAO();
        dao.deleteQueueTreatment(d);
    }

    public ReportQueueTreatmentsVO getReportQueueTreatment(int id) throws ApplicationException {
        ReportQueueTreatmentsVO dash = null;
        try {
            ReportDAO dao = new ReportDAO();
            dash = (ReportQueueTreatmentsVO) dao.findQueueTreatmentsByID(id);
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            throw new ApplicationException("ReportingServiceImpl threw an error: " + e.getMessage());
        }
        return dash;
    }

    public CCACReportVO getCCACReport(String id) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            CCACReportVO r = new CCACReportVO();
            r.setId(new Integer(id));
            return configurationDAO.findByCriteria(r);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }

    public Collection<CCACReportVO> getCCACReports(int active, int already_retrieved, Date start_date, Date end_date, int[] treatment_location_id) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            return (Collection) configurationDAO.findAllByCriteria(active, already_retrieved, start_date, end_date, treatment_location_id);
        } catch (DataAccessException e) {
            throw new ApplicationException("DataAccessException Error in ConfigurationManagerBD.retrieveAll(): " + e.toString(), e);
        }
    }

    public void saveCCACReport(CCACReportVO configurationVO) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            configurationDAO.insertCCACReport(configurationVO);
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(), e);
        }
    }

    public void retrievedCCACReport(String id) throws ApplicationException {
        try {
            ReportDAO configurationDAO = new ReportDAO();
            CCACReportVO report = getCCACReport(id);
            report.setAlready_retrieved(1);
            configurationDAO.insert(report);
        } catch (DataAccessException e) {
            throw new ApplicationException("Error in ConfigurationManagerBD.save: " + e.toString(), e);
        }
    }

    public void removeCCACReport(CCACReportVO configurationVO) throws ApplicationException {

        ReportDAO configurationDAO = new ReportDAO();
        configurationDAO.delete(configurationVO);

    }
}
