/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.service;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.mail.SendMailUsingAuthentication;
import com.pixalere.reporting.bean.DashboardByEtiologyVO;
import com.pixalere.reporting.bean.DashboardByLocationVO;
import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.DashboardVO;
import com.pixalere.reporting.dao.DashboardReportBaseDAO;
import com.pixalere.reporting.dao.DashboardReportDAO;
import com.pixalere.reporting.dao.DashboardReportExtendedDAO;
import com.pixalere.utils.Common;
import com.pixalere.utils.FiscalQuarter;
import com.pixalere.utils.FiscalYear;
import com.pixalere.utils.PDate;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author User
 */
public class DashboardReportService {
    
    private final Integer language;
    private final String locale;
    private final ServletContext ctx;
    HttpServletRequest request;
    HttpServletResponse response;
    
    // Services
    ListServiceImpl listService;
    DashboardReportBaseDAO reportDAO;
    
    // The report configuration
    DashboardReportVO dashboardReport;
    // The map of values to be sent to report.
    private HashMap<String, DashboardVO> reportValues;
    // The map of parameters
    HashMap reportParams;
    
    // Graphic Elements
    DefaultCategoryDataset categoryDataset;
    DefaultPieDataset pieDataset;
    
    // Internal data structures for Time Periods
    LinkedHashMap<String, String[]> sqlDateRanges;
    Integer[] quartersArray;
    Integer[] yearsArray;
    String[] xAxisText;
    
    // Flag for extended report
    private boolean extended;
    
    public DashboardReportService(HttpServletRequest request, HttpServletResponse response, Integer language) {
        this.response = response;
        this.request = request;

        this.ctx = (request != null && request.getSession() != null ? request.getSession().getServletContext() : null);
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
        this.listService = new ListServiceImpl(language);
    }
    
    public DashboardReportService(ServletContext ctx, Integer language) {
        this.ctx = ctx;
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
        this.listService = new ListServiceImpl(language);
    }
    
    public void generateDashboardMail(
            DashboardReportVO report, 
            List<FiscalQuarter> quarters) throws ApplicationException {
        generateDashboard(report, quarters, false, true, false, null);
    }
    
    public void generateDashboardLive(
            DashboardReportVO report, 
            List<FiscalQuarter> quarters) throws ApplicationException {
        generateDashboard(report, quarters, false, false, false, null);
    }
    
    public void generateExtendedDashboardMail(
            DashboardReportVO report, 
            List<FiscalQuarter> quarters,
            boolean displayLocations,
            String customTitle) throws ApplicationException {
        generateDashboard(report, quarters, true, true, displayLocations, customTitle);
    }
    
    public void generateExtendedDashboardLive(
            DashboardReportVO report, 
            List<FiscalQuarter> quarters,
            boolean displayLocations,
            String customTitle) throws ApplicationException {
        generateDashboard(report, quarters, true, false, displayLocations, customTitle);
    }
    
    public void generateDashboard( 
            DashboardReportVO report, 
            List<FiscalQuarter> quarters,
            boolean isExtended,
            boolean sendMail,
            boolean displayLocations,
            String customTitle) throws ApplicationException {
        
        this.dashboardReport = report;
        this.reportValues = new HashMap();
        this.reportParams = new HashMap();
        
        this.sqlDateRanges = new LinkedHashMap<String, String[]>();
        this.extended = isExtended;
        
        if (extended) {
            this.quartersArray = new Integer[5];
            this.yearsArray = new Integer[5];
            this.xAxisText = new String[5];
        } else {
            this.quartersArray = new Integer[8];
            this.yearsArray = new Integer[8];
            this.xAxisText = new String[8];
        }
            
        try {
            this.reportDAO = (extended)
                    ? new DashboardReportExtendedDAO() 
                    : new DashboardReportDAO();
            reportDAO.connect();
            
            // Initialize quartersArray, yearsArray, sqlDateRanges and
            // create timeframes_definition string
            if (isExtended){
                initializeCustomRanges(quarters);
            } else {
                initializeRangesAndIntervals(quarters);
            }
            
            // Get localized treatment Locations
            String treatmentLocations = getLocationsFromReport();
            treatmentLocations = (!isExtended)
                    ? treatmentLocations 
                    : (displayLocations)
                    ? treatmentLocations 
                    : "";
            if (!treatmentLocations.isEmpty()){
                reportParams.put("treatmentLocations", 
                            Common.getLocalizedReportString("pixalere.reports.dashboard.generated_for_locations", locale) +
                                    treatmentLocations);
            }
            
            // Treatment SQL filter
            String treatmentQuery = getTreatmentQueryFromReport();
            List<Integer> locationsList = getLocationsList();
            
            // Titles to Params
            reportParams.put("customer_name", Common.getConfig("customer_name"));
            reportParams.put("ReportGenerationDate", PDate.getDateTime(new Date()));
            
            customTitle = (customTitle == null)? "": customTitle.trim();
            reportParams.put("custom_title", customTitle);
            
            String lastyear_title = (isExtended)
                    ? Common.getLocalizedString("pixalere.reporting.last_fy", locale)
                    : Common.getLocalizedString("pixalere.reporting.fy", locale);
            reportParams.put("lastyear_title", lastyear_title);
            reportParams.put("yearbeforelast_title", Common.getLocalizedString("pixalere.reporting.yb", locale));

            categoryDataset = new DefaultCategoryDataset();
            pieDataset = new DefaultPieDataset();
            
            // Get data by Etiology
            if (dashboardReport.getEtiology_all() == null || dashboardReport.getEtiology_all().equals(new Integer("1"))) {
          
                
                // Add data for All Etiologies - Send null as Etiology 
                processEtiology(null, treatmentQuery, locationsList);
            } else {
                // Iterate through etiologies
                for (DashboardByEtiologyVO etiology : dashboardReport.getEtiologies()) {
                    
                    // Add data for current Etiology
                    processEtiology(etiology, treatmentQuery, locationsList);
                }
            }
            
            if (!extended) {
                // Graphics titles and creation
                reportParams.put("barchart_title1", Common.getLocalizedString("pixalere.reporting.cost_title", locale));
                reportParams.put("xaxis_title1", Common.getLocalizedString("pixalere.reporting.cost_xaxis", locale));
                reportParams.put("yaxis_title1", Common.getLocalizedString("pixalere.reporting.cost_yaxis", locale));
                reportParams.put("categoryDataset1", categoryDataset);

                // Custom styles -> Small font
                StandardChartTheme theme = getChartTheme(true);

                JFreeChart costBarChart = ChartFactory.createBarChart(
                        Common.getLocalizedString("pixalere.reporting.cost_title", locale), 
                        Common.getLocalizedString("pixalere.reporting.cost_xaxis", locale), 
                        Common.getLocalizedString("pixalere.reporting.cost_yaxis", locale), 
                        categoryDataset, 
                        PlotOrientation.VERTICAL, 
                        true, 
                        true, 
                        false); //do trends.. and format text.

                // Custom styles
                theme.apply(costBarChart);
                ((BarRenderer) ((CategoryPlot) costBarChart.getPlot()).getRenderer()).setDrawBarOutline(true);

                BufferedImage costImage = (isExtended)
                        ? costBarChart.createBufferedImage(339, 225)
                        : costBarChart.createBufferedImage(453, 300);
                ImageIO.write(costImage, "png", new File("image.png"));

                String pieTitle = (!this.extended)
                        ? Common.getLocalizedString("pixalere.admin.dashboard.avg_heal_days", locale)
                        : Common.getLocalizedReportString("dash_extended.chart.assessment_by_type", locale);
                JFreeChart pieBarChart = ChartFactory.createPieChart(
                        pieTitle, 
                        pieDataset, 
                        false, 
                        true, 
                        false); //do trends.. and format text.

                // Custom styles -> Small font
                theme.apply(pieBarChart);
                ((PiePlot) pieBarChart.getPlot()).setLabelFont(new Font("Arial", Font.PLAIN, 8));

                BufferedImage pieImage =(isExtended)
                        ? pieBarChart.createBufferedImage(230, 225)
                        : pieBarChart.createBufferedImage(327, 300);
                ImageIO.write(pieImage, "png", new File("image2.png"));

                reportParams.put("Cost", costImage);
                reportParams.put("PieChart", pieImage);
            }
                
            
            List<DashboardVO> reportValuesList = organizeReportValues(reportValues);
            
            String dashboardType = (isExtended)? "dashboard_extended": "dashboard";
            
            if (sendMail) {
                treatmentLocations = 
                        Common.getLocalizedReportString("pixalere.reports.dashboard.generated_for_locations", locale)
                        + " " + treatmentLocations;
                sendMail(reportValuesList, dashboardType, dashboardReport.getEmail(), treatmentLocations, customTitle);
            } else {
                pdfToRequest(reportValuesList, dashboardType);
            }
            
        } catch (ApplicationException e) {
            e.printStackTrace();
            throw new ApplicationException("Error sending generation Dashboard report", e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException("Error sending generation Dashboard report", e);
        } finally {
            try {
                reportDAO.disconnect();
            } catch (Exception ex) {
            }
        }

    }
    
    private void initializeCustomRanges(List<FiscalQuarter> quarters){
        String timeframes_definition = "";

        //Setting up date ranges.
        //System.out.println("sqlDateRanges # " + quarters.size());
        int quarter_count = 0;
        for (FiscalQuarter quarter : quarters) {
            if (quarter != null) {
                
                quartersArray[quarter_count] = quarter.getQuarter();
                String[] dates = new String[2];
                
                //System.out.println("Quarter#" + quarter.getQuarter() + " is Blank: " + quarter.isIsBlank());
                
                if (!quarter.isIsBlank()) {
                    
                    // Fill with the year the quarter started (2 digits)
                    yearsArray[quarter_count] = 
                            (quarter.getStartDate().getYear() >= 100)? // getYear() substracts 1900 to the date
                            quarter.getStartDate().getYear()-100: // If we are in the XXI century
                            quarter.getStartDate().getYear();

                    dates[0] = PDate.getDate(quarter.getStartDate(), false);
                    dates[1] = PDate.getDate(quarter.getEndDate(), false);
                    
                    String interval = PDate.getDate(quarter.getStartDate(), false) + " " +
                            Common.getLocalizedReportString("pixalere.reports.common.to", locale) + 
                            PDate.getDate(quarter.getEndDate(), false);
                    
                    // Title
                    reportParams.put("quarter" + quarter.getOrder() + 
                        "_title", interval);

                    timeframes_definition = timeframes_definition + 
                            (timeframes_definition.equals("") ? "" : "     ") + 
                            Common.getLocalizedString("pixalere.reporting.c" + quarter.getQuarter(), locale) + 
                            " (" + interval + ")";
                } else {
                    reportParams.put("quarter" + quarter.getOrder() + 
                        "_title", "");
                    dates[0] = "";
                    dates[1] = "";
                }

                sqlDateRanges.put(quarter.getQuarter() + "q" + quarter.getOrder(), dates);
                //System.out.println(quarter.getQuarter() + "q" + quarter.getOrder() + ": " + dates[0] + " - " + dates[1]);
                quarter_count++;
            }
        }
        
        // Translations for xAxis
        for (int i = 0; i < xAxisText.length; i++) {
            xAxisText[i] = Common.getLocalizedString("pixalere.reporting.c" + quartersArray[i], locale);
        }

        reportParams.put("timeframes", timeframes_definition);
    }
    
    private void initializeRangesAndIntervals(List<FiscalQuarter> quarters){
        String fiscal_year = Common.getConfig("fiscalStart");
        String fiscal_quarters = Common.getConfig("fiscalQuarter");

        String timeframes_definition = "";

        //Setting up date ranges.
        int quarter_count = 0;
        for (FiscalQuarter quarter : quarters) {
            if (quarter != null) {

                quartersArray[quarter_count] = quarter.getQuarter();
                // Fill with the year the quarter started (2 digits)
                yearsArray[quarter_count] = 
                        (quarter.getStartDate().getYear() >= 100)? // getYear() substracts 1900 to the date
                        quarter.getStartDate().getYear()-100: // If we are in the XXI century
                        quarter.getStartDate().getYear();
                
                //quart[quarter_count] = quarter.getQuarter();
                reportParams.put("quarter" + quarter.getOrder() + 
                        "_title", Common.getLocalizedString("pixalere.reporting.q" + quarter.getQuarter(), locale));

                String[] dates = new String[2];
                dates[0] = PDate.getDate(quarter.getStartDate(), false);
                dates[1] = PDate.getDate(quarter.getEndDate(), false);

                timeframes_definition = timeframes_definition + 
                        (timeframes_definition.equals("") ? "" : "     ") + 
                        Common.getLocalizedString("pixalere.reporting.q" + quarter.getQuarter(), locale) + 
                        " (" + PDate.getDate(quarter.getStartDate(), false) + " " +
                        Common.getLocalizedReportString("pixalere.reports.common.to", locale) + 
                        PDate.getDate(quarter.getEndDate(), false) + ")";

                sqlDateRanges.put(quarter.getQuarter() + "q" + quarter.getOrder(), dates);
                quarter_count++;
            }
        }

        String[] dates12 = new String[2];
        FiscalYear lastyear = PDate.getFiscalYear(new Date(), fiscal_year, fiscal_quarters, 0);


        dates12[0] = PDate.getDate(lastyear.getStartYear(), false);
        dates12[1] = PDate.getDate(lastyear.getEndYear(), false);
        sqlDateRanges.put("ly", dates12);
        
        reportParams.put("fytd_title", "FYTD " + PDate.getYear((lastyear.getStartYear())));
        

        timeframes_definition += 
                "     FY1 (" + 
                PDate.getDate(lastyear.getStartYear(), false) + " " +
                Common.getLocalizedReportString("pixalere.reports.common.to", locale) + 
                PDate.getDate(lastyear.getEndYear(), false) + ")";

        if (!this.extended){
            FiscalYear year_before = PDate.getFiscalYear(new Date(), fiscal_year, fiscal_quarters, -1);
            reportParams.put("total2010_title", "Total " + PDate.getYear((year_before.getStartYear())));

            String[] dates4 = new String[2];
            dates4[0] = PDate.getDate(year_before.getStartYear(), false);
            dates4[1] = PDate.getDate(year_before.getEndYear(), false);
            sqlDateRanges.put("yb", dates4);

            timeframes_definition += 
                    "     FY2 (" + 
                    PDate.getDate(year_before.getStartYear(), false) + " " +
                    Common.getLocalizedReportString("pixalere.reports.common.to", locale) + 
                    PDate.getDate(year_before.getEndYear(), false) + ")";
        }
        
        // Translations for xAxis
        for (int i = 0; i < xAxisText.length; i++) {
            xAxisText[i] = Common.getLocalizedString("pixalere.reporting.q" + quartersArray[i], locale);
        }

        reportParams.put("timeframes", timeframes_definition);
    }
    
    private String getLocationsFromReport(){
        // Make sure we have a valid id language for querying the db directly
        int query_language = Common.getLanguageIdFallbackFromLanguageId(language);
        
        String locations = "";
        if (dashboardReport.getTreatmentLocations() != null && dashboardReport.getTreatmentLocations().size() > 0) {
            for (DashboardByLocationVO treatment : dashboardReport.getTreatmentLocations()) {
                String q = "select name from lookup_l10n where language_id=" + query_language + " AND lookup_id=" + treatment.getTreatment_location_id();
                ResultSet result = reportDAO.getResults(q);
                String n = Common.getString(result, "name");
                locations = locations + n.trim() + ", ";
            }
        }
        // Remove last ', '
        return locations.isEmpty()? "": locations.substring(0, locations.length() - 2);
    }
    
    private List<Integer> getLocationsList(){
        List<Integer> list = new ArrayList<Integer>();
        if (dashboardReport.getTreatmentLocations() != null && dashboardReport.getTreatmentLocations().size() > 0) {
            for (DashboardByLocationVO treatment : dashboardReport.getTreatmentLocations()) {
                list.add(treatment.getTreatment_location_id());
            }
        }
        return list;
    }
    
    private String getTreatmentQueryFromReport(){
        String treatmentQuery = "";
        if (dashboardReport.getTreatmentLocations() != null && dashboardReport.getTreatmentLocations().size() > 0) {
            String locationFilterQuery = "";
            for (DashboardByLocationVO treatment : dashboardReport.getTreatmentLocations()) {
                locationFilterQuery = locationFilterQuery + " treatment_location_id=" + treatment.getTreatment_location_id() + " OR ";
            }
            // Remove last 'OR '
            locationFilterQuery = locationFilterQuery.substring(0, locationFilterQuery.length() - 3);
            // Set query
            treatmentQuery = " AND (" + locationFilterQuery + ")";
        }
        return treatmentQuery;
    }
    
    private void processEtiology(DashboardByEtiologyVO etiologyVO, 
            String treatmentQuery, List<Integer> locationsList) throws ApplicationException {
        
        // Enumeration rangesKeys = sqlDateRanges.keySet();
        
        // Default values - When getting All Etiologies
        String category_name = Common.getLocalizedString("pixalere.admin.dashboard.all_etiology", locale);
        int etiology_id = -1;
        
        // Get localized Category
        LookupVO etLookup = (etiologyVO != null)? listService.getListItem(etiologyVO.getEtiology_id()): null;
        if (etLookup != null) {
            category_name = etLookup.getName(language);
            etiology_id = etiologyVO.getEtiology_id();
        }
        
        // System.out.println("processEtiology() category_name: " + category_name + " etiology_id: " + etiology_id);
        
        // Trasverse through all data ranges
        for (Map.Entry<String, String[]> range : sqlDateRanges.entrySet()) {
            String key = range.getKey();
            String[] dateArr = range.getValue();
            //System.out.println("\nGetting range [" + key +"]: " + dateArr[0] + " - " + dateArr[1]);
            
            reportValues = reportDAO.getDashboardReport(
                    category_name, 
                    etiology_id,
                    reportValues, 
                    dashboardReport, 
                    key, 
                    dateArr[0], 
                    dateArr[1], 
                    treatmentQuery, 
                    locationsList,
                    locale);
        }
        
        if (!this.extended){
            // Graphics data - Old Quarters
            for (DashboardVO dash : reportValues.values()) {
                if (!dash.getName().isEmpty()){
                    // For empty quarters set to zero
                    dash.ifEmptyQuartersSetToZero();
                    // System.out.println(dash);

                    // Add data to PieDataset
                    addDashboardToPieDataset(dash);

                    // Initialize the X axis for the Chart.
                    if (etiologyVO == null) {
                        // For All Etiologies add year, do NOT prefix with #N
                        addDashboardToCategoryDataset(dash, true, false);
                    } else {
                        // For Etiology: Do NOT add year, prefix with #N 
                        addDashboardToCategoryDataset(dash, false, true);
                    }
                }
            }

        }
    }
    
    private void addDashboardToPieDataset(DashboardVO dashboardVO){
        if (dashboardVO.getName() != null && !this.extended &&
            (dashboardVO.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days1-30", locale)) || 
            dashboardVO.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days31-90", locale)) || 
            dashboardVO.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days91-183", locale)) || 
            dashboardVO.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days184-365", locale)) || 
            dashboardVO.getName().equals(Common.getLocalizedString("pixalere.admin.dashboard.heal_days366", locale)))) {
            // System.out.println("*** addDashboardToPieDataset: " + dashboardVO.getName());
            // FIXME: Why the old report only added the first quarter?
            pieDataset.setValue(dashboardVO.getName(), new Double(dashboardVO.getQuarter1()));
        }
        if (dashboardVO.getName() != null && this.extended &&
                (
                dashboardVO.getName().equals(Common.getLocalizedReportString("dash_extended.row.wound_assessments", locale)) || 
                dashboardVO.getName().equals(Common.getLocalizedReportString("dash_extended.row.incision_assessments", locale)) || 
                dashboardVO.getName().equals(Common.getLocalizedReportString("dash_extended.row.tube_drain_assessments", locale)) || 
                dashboardVO.getName().equals(Common.getLocalizedReportString("dash_extended.row.ostomy_assessments", locale)) 
                )
            ) {
            // Remove text to fit in chart
            int pos = dashboardVO.getName().lastIndexOf("Ass");
            String title = (pos > 0)
                    ? dashboardVO.getName().substring(0, pos-1)
                    : dashboardVO.getName();
            pieDataset.setValue(title, new Double(dashboardVO.getQuarter1()));
            pieDataset.setValue(title, new Double(dashboardVO.getQuarter2()));
            pieDataset.setValue(title, new Double(dashboardVO.getQuarter3()));
            pieDataset.setValue(title, new Double(dashboardVO.getQuarter4()));
        }
    }
    
    private void addDashboardToCategoryDataset(
            DashboardVO dashboardVO,
            boolean useYearAll, boolean useNumberAll){
        
        String dashboardName = dashboardVO.getName();
        if (dashboardName != null) {
            String avg_cost_all_wounds_title = (extended)
                ? Common.getLocalizedReportString("dash_extended.row.average_all_wound_product_cost", locale)
                : Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_all_wounds", locale);
                
            String avg_cost_open_wounds_title = (extended)
                ? Common.getLocalizedReportString("dash_extended.row.average_open_wound_product_cost",locale)
                : Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_open_wounds",locale);
                
            String avg_cost_closed_wound_title = (extended)
                ? Common.getLocalizedReportString("dash_extended.row.average_closed_wound_product_cost", locale)
                : Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_closed_wounds", locale);
                

            // Localization for X axis with flags
            String[] texts = new String[xAxisText.length];
            for (int i = 0; i < xAxisText.length; i++) {
                // If any of the flags are true:
                // Add the # as a prefix and/or Add the year
                String year = (useYearAll)? " '" + yearsArray[i]: "";
                String number = (useNumberAll)? "#" + (i+1) + " ": "";
                texts[i] = number + xAxisText[i] + year;
            }

            // NOTE: The text for X Axis must match to display the chart correctly
            // FIXME: Is there any need for these conditions here?
            // FIXME: Why the old graph only showed 7 quarters?
            if (dashboardName.equals(avg_cost_all_wounds_title)) {
                categoryDataset.addValue(new Double(dashboardVO.getQuarter1()), dashboardName, texts[0]);
                categoryDataset.addValue(new Double(dashboardVO.getQuarter2()), dashboardName, texts[1]);
                categoryDataset.addValue(new Double(dashboardVO.getQuarter3()), dashboardName, texts[2]);
                categoryDataset.addValue(new Double(dashboardVO.getQuarter4()), dashboardName, texts[3]);
                if (!extended){
                    // Horizontal data 8 quarters
                    categoryDataset.addValue(new Double(dashboardVO.getQuarter5()), dashboardName, texts[4]);
                    categoryDataset.addValue(new Double(dashboardVO.getQuarter6()), dashboardName, texts[5]);
                    categoryDataset.addValue(new Double(dashboardVO.getQuarter7()), dashboardName, texts[6]);
                }
            }
            if (dashboardName.equals(avg_cost_open_wounds_title) ||
                dashboardName.equals(avg_cost_closed_wound_title)
                ) {
                categoryDataset.addValue(new Double(dashboardVO.getQuarter1()), dashboardName, texts[0]);
                categoryDataset.addValue(new Double(dashboardVO.getQuarter2()), dashboardName, texts[1]);
                categoryDataset.addValue(new Double(dashboardVO.getQuarter3()), dashboardName, texts[2]);
                categoryDataset.addValue(new Double(dashboardVO.getQuarter4()), dashboardName, texts[3]);
                if (!extended){
                    // Horizontal data 8 quarters
                    categoryDataset.addValue(new Double(dashboardVO.getQuarter5()), dashboardName, texts[4]);
                    categoryDataset.addValue(new Double(dashboardVO.getQuarter6()), dashboardName, texts[5]);
                    categoryDataset.addValue(new Double(dashboardVO.getQuarter7()), dashboardName, texts[6]);
                }
            }
            
        }        
    }
    
    private List<DashboardVO> organizeReportValues(HashMap<String, DashboardVO> map) {
        List<DashboardVO> list = new ArrayList();
        Set rows = map.keySet();
        Iterator it = rows.iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            DashboardVO row = map.get(key);
            if (!row.getName().isEmpty()) {
                // If it's not a separator, format
                row.format();
            }
                
            list.add(row);
        }
        Collections.sort(list, new DashboardVO());
        return list;
    }
    
    private StandardChartTheme getChartTheme() {
        return getChartTheme(false);
    }

    private StandardChartTheme getChartTheme(boolean useSmallFonts) {
        StandardChartTheme theme = (StandardChartTheme) org.jfree.chart.StandardChartTheme.createJFreeTheme();

        String fontName = "Arial";
        theme.setTitlePaint(Color.decode("#3333CC"));
        theme.setExtraLargeFont(new Font(fontName, Font.PLAIN, 15)); //title
        theme.setLargeFont(new Font(fontName, Font.PLAIN, 14)); //axis-title
        theme.setRegularFont(new Font(fontName, Font.PLAIN, 11));
        theme.setSmallFont(new Font(fontName, Font.PLAIN, 9));
        theme.setRangeGridlinePaint(Color.decode("#ADADAD"));
        theme.setPlotBackgroundPaint(Color.white);
        theme.setChartBackgroundPaint(Color.white);
        theme.setGridBandPaint(Color.red);
        theme.setAxisLabelPaint(Color.decode("#333333"));

        if (useSmallFonts) {
            theme.setExtraLargeFont(new Font(fontName, Font.PLAIN, 13)); //title
            theme.setLargeFont(new Font(fontName, Font.PLAIN, 12)); //axis-title
            theme.setRegularFont(new Font(fontName, Font.PLAIN, 10));
            theme.setSmallFont(new Font(fontName, Font.PLAIN, 8));
        }

        return theme;
    }
    
    private void pdfToRequest(List<DashboardVO> dash, String report_type){
        InputStream reportStream = null;
        
        try {
            try {
                reportParams.put("SubReportPath", ctx.getRealPath("/reports") + "/");
                reportParams.put("ImagesPath", ctx.getRealPath("/images") + "/");
                reportStream = new FileInputStream(ctx.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
            } catch (Exception e) {
                reportStream = new FileInputStream(ctx.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
            }

            reportParams.put(JRParameter.REPORT_LOCALE, Common.getLocaleFromLocaleString(locale));
            
            reportParams.put(JRParameter.REPORT_LOCALE, Common.getLocaleFromLocaleString(locale));
            
            byte[] bytes = JasperRunManager.runReportToPdf(reportStream, reportParams, new JRBeanArrayDataSource(dash.toArray()));
            ServletOutputStream servletOutputStream = response.getOutputStream();
            
            response.setContentType("application/pdf");
            response.setContentLength(bytes.length);
            response.setHeader("Content-disposition", "filename=" + report_type.toLowerCase() + ".pdf");

            servletOutputStream.write(bytes, 0, bytes.length);
            servletOutputStream.flush();
            servletOutputStream.close();
            
        } catch (JRException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
            
    }
    
    private void sendMail(List<DashboardVO> dash, String report_type, String email, String message, String custom_title) throws ApplicationException {
        reportParams.put("ImagesPath", ctx.getRealPath("/images") + "/");
        InputStream reportStream = null;
        
        try {
            try {
                reportStream = new FileInputStream(ctx.getRealPath("/reports/" + report_type.toLowerCase() + ".jasper"));
            } catch (Exception e) {
                reportStream = new FileInputStream(ctx.getRealPath("/pixalere/reports/" + report_type.toLowerCase() + ".jasper"));
            }
            
            
            reportParams.put(JRParameter.REPORT_LOCALE, Common.getLocaleFromLocaleString(locale));

            reportParams.put(JRParameter.REPORT_LOCALE, Common.getLocaleFromLocaleString(locale));

            byte[] bytes = JasperRunManager.runReportToPdf(reportStream, reportParams, new JRBeanArrayDataSource(dash.toArray()));
            SendMailUsingAuthentication mail = new SendMailUsingAuthentication();
            String[] emails = email.split(",");
            
            // Local Validation - If mail can't be sent
//            Date currentDate = new Date();
//            String localname = (custom_title.isEmpty())
//                    ? report_type +"_" + PDate.getDateAsStringWithFormat(currentDate, "HHmm'h'_dd-MMM-yyyy", locale)
//                    : custom_title+ "_" + PDate.getDateAsStringWithFormat(currentDate, "HHmm'h'_dd-MMM-yyyy", locale);
//            try {
//                OutputStream out = new FileOutputStream("C:\\pixalere\\reports_tmp\\"+localname+".pdf");
//                out.write(bytes);
//                out.close();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } finally {
//
//            }
            // END Local validation

            // String[] recipients, String subject, String message, byte[] byteArray, String attachment_name
            String subject = (custom_title.isEmpty())
                    ? "Dashboard Report " + PDate.getMonthYear(PDate.getEpochTime() + "")
                    : custom_title;
            String attachment_name  = (custom_title.isEmpty())
                    ? "dashboardreport"
                    : custom_title;
            mail.postMail(emails, 
                    subject, 
                    message, 
                    bytes, 
                    attachment_name);
            
        } catch (JRException ex) {
            ex.printStackTrace();
            throw new ApplicationException("Error generating Dashboard JRException", ex);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ApplicationException("Error sending Dashboard report by email", ex);
        }
    }
}
