package com.pixalere.reporting;
import com.pixalere.auth.bean.PositionVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.wound.bean.EtiologyVO;
import com.pixalere.wound.bean.GoalsVO;
import java.util.*;
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import java.util.ArrayList;
import java.util.List;
import com.pixalere.auth.bean.UserAccountRegionsVO;

import com.pixalere.guibeans.FieldValues;
import com.pixalere.guibeans.RowData;
import com.pixalere.admin.*;

import com.pixalere.reporting.dao.ReportDAO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.patient.dao.PatientProfileDAO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.assessment.bean.AssessmentProductVO;
import com.pixalere.assessment.dao.WoundAssessmentDAO;
import com.pixalere.assessment.dao.WoundAssessmentLocationDAO;
import com.pixalere.assessment.dao.AssessmentDAO;
import com.pixalere.assessment.dao.AssessmentCommentsDAO;
import com.pixalere.assessment.service.AssessmentImagesServiceImpl;
import com.pixalere.assessment.service.*;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.common.dao.ProductsDAO;
import com.pixalere.common.dao.ReferralsTrackingDAO;
import com.pixalere.common.ApplicationException;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.wound.bean.WoundVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.WoundAssessmentLocationVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ReferralsTrackingVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.AssessmentImagesVO;
import com.pixalere.assessment.bean.AssessmentCommentsVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.admin.service.ResourcesServiceImpl;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.reporting.manager.*;
import com.pixalere.reporting.nursing.*;
import com.pixalere.utils.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import com.pixalere.assessment.bean.TreatmentCommentVO;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.assessment.bean.DressingChangeFrequencyVO;
import javax.imageio.ImageIO;
import net.sf.jasperreports.engine.JRParameter;

/*
 * @deprecated
 * @todo this class needs a rewrite, most reports are inefficient.
 * 
 * @author Stefan Viveen
 */
public class ReportingManager extends Panel {

    private static final String RESOURCE_BUNDLE = "ApplicationResources";
    private String basepath = null;
    private String absolutepath = null;
    private Integer language = 1;
    private String locale = "en";
    private ServletContext context = null;

    private void loadImage(Image imgTemp) {
        try {
            MediaTracker tracker = new MediaTracker(this);
            tracker.addImage(imgTemp, 0);
            tracker.waitForID(0);
        } catch (Exception e) {
        }
    }

    public Image composeBluemodelImage(int intWoundProfileId, boolean blnIncludeUnsaved) {
        BufferedImage outputImage = new BufferedImage( 
                327, 266,
                BufferedImage.TYPE_INT_ARGB );
        try {
            
            // Retrieve Wound Profile to get the correct blue man image
           WoundProfileDAO woundProfileDB = new WoundProfileDAO();
            WoundAssessmentLocationDAO woundAssessmentLocationDB = new WoundAssessmentLocationDAO();
            com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
            woundProfile.setWound_id(intWoundProfileId);
            woundProfile.setActive(1);
            Collection lcol1 = null;
            lcol1 = woundProfileDB.findAllByCriteria(woundProfile, 1); // One record
            Iterator iter1 = lcol1.iterator();
            if (iter1.hasNext()) {
                WoundProfileVO wprf = (WoundProfileVO) iter1.next();
                BufferedImage blueman = ImageIO.read(new File(basepath + "images/bluemodel/" + wprf.getWound().getImage_name()));
                Graphics g = outputImage.getGraphics();
                g.drawImage(blueman, 0, 0, this);
                WoundAssessmentLocationVO woundAssessmentLocation = new WoundAssessmentLocationVO();
                woundAssessmentLocation.setWound_id(intWoundProfileId);
                if (blnIncludeUnsaved == false) {
                    woundAssessmentLocation.setActive(1); // Only submitted alpha's based on boolean
                }
                Collection lcol2 = null;
                lcol2 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation, 0, false, "");//4th parameter (extra sort) is ignored
                Iterator iter2 = lcol2.iterator();
                while (iter2.hasNext()) {
                    WoundAssessmentLocationVO wal = (WoundAssessmentLocationVO) iter2.next();
                    // An Alpha image is 20 pixels wide and 19 pixels high.
                    // The blue man image has 12 rows of each 16 boxes, each 20 x 20 pixels.
                    // Boxes are numbered 1 through 192.
                    //WoundLocationDetailsVO[] details = wal.getAlpha_details();
                    //for (int intIcons = 0; intIcons < details.length; intIcons++) {
                    String strColourCode = "y"; // Yellow for active alpha's
                    if (wal.getDischarge() == 1) {
                        strColourCode = "g"; // Green for closed alpha's
                    }
                    if (wal.getActive() == 0) {
                        strColourCode = "r"; // Red for un-saved alpha's
                        // An image name is e.g. "a_y.gif"; "a" is tha alpha, "y" is the colour (y=yellow, r=red, g=groen)
                    }
                    try{
                        //System.out.println(basepath + "images/Alphas/report_alphas/" + wal.getAlpha().toLowerCase() + "_" + strColourCode + ".gif");
                        BufferedImage alpha = ImageIO.read(new File(basepath + "images/Alphas/report_alphas/" + wal.getAlpha().toLowerCase() + "_" + strColourCode + ".gif"));
                        //System.out.println("X"+wal.getX()+" Y "+(wal.getY()+17));
                        g.drawImage(alpha, wal.getX(), (wal.getY()+17), null);
                        //File outputfile = new File("/tmp/"+wal.getAlpha()+".png");
                        //ImageIO.write(outputImage, "png", outputfile);
                    }catch(FileNotFoundException ioe){
                        ioe.printStackTrace();
                    }
                }
            }
            //File outputfile = new File("/home/travis/saved.png");
            //ImageIO.write(outputImage, "png", outputfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return outputImage;
    }

    public static Date getIntEpochNow() throws ApplicationException {
        Calendar cal = Calendar.getInstance();
        return new PDate().getDate(cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR));
    }

    public static Date intToDate(int time) {
        return (time != 0) ? new Date((long) time * 1000) : null;
    }
    WoundServiceImpl wservice = new WoundServiceImpl(language);
    //Used when bypassing filter for PatientFile report - PCI/care connect etc.

    public ReportingManager(ProfessionalVO userVO, String patient_id, ServletContext context, Integer language) {
        reportDAO = new ReportDAO();
        listBD = new ListServiceImpl(language);
        resourcesBD = new ResourcesServiceImpl();
        patientServiceImpl = new PatientServiceImpl(language);
        patientProfileServiceImpl = new PatientProfileServiceImpl(language);
        patientDB = new PatientDAO();
        patientProfileDB = new PatientProfileDAO();
        woundProfileDB = new WoundProfileDAO();
        woundAssessmentDB = new WoundAssessmentDAO();
        woundAssessmentLocationDB = new WoundAssessmentLocationDAO();
        assessmentEachwoundDB = new AssessmentDAO();
        assessmentCommentsDB = new AssessmentCommentsDAO();
        listDataDB = new ListsDAO();
        referralsTrackingManagerBD = new ReferralsTrackingServiceImpl(language);
        this.userVO = userVO;
        this.patient_id = patient_id;
        Common common = new Common();
        absolutepath = Common.getPhotosPath();
        basepath = common.getBasePath(context);
        this.language = language;
        this.locale = Common.getLanguageLocale(language);
    }

    public ReportingManager(
            ProfessionalVO userVO, 
            Date start_date, 
            Date end_date, 
            String group_by, 
            String breakdown, 
            String report_type, 
            String full_assessment, 
            String goal_days, 
            String print_patient_details, 
            String[] treatment_location, 
            String includeTIP, 
            String[] etiology, 
            String goals, 
            String[] caretypes, 
            String patient_id, 
            ServletContext context, 
            Integer language) {
        reportDAO = new ReportDAO();
        listBD = new ListServiceImpl(language);
        resourcesBD = new ResourcesServiceImpl();
        patientServiceImpl = new PatientServiceImpl(language);
        patientProfileServiceImpl = new PatientProfileServiceImpl(language);
        patientDB = new PatientDAO();
        patientProfileDB = new PatientProfileDAO();
        woundProfileDB = new WoundProfileDAO();
        woundAssessmentDB = new WoundAssessmentDAO();
        woundAssessmentLocationDB = new WoundAssessmentLocationDAO();
        assessmentEachwoundDB = new AssessmentDAO();
        assessmentCommentsDB = new AssessmentCommentsDAO();
        this.language = language;
        listDataDB = new ListsDAO();
        referralsTrackingManagerBD = new ReferralsTrackingServiceImpl(language);
        this.goals = goals;
        this.caretypes = caretypes;
        this.start_date = start_date;
        this.end_date = end_date;
        this.group_by = group_by;
        this.breakdown = breakdown;
        this.report_type = report_type;
        this.full_assessment = full_assessment;
        this.goal_days = goal_days;
        this.print_patient_details = print_patient_details;
        this.treatment_location = treatment_location;
        this.etiology = etiology;
        this.includeTIP = includeTIP;
        this.userVO = userVO;
        this.patient_id = patient_id;
        Common common = new Common();
        absolutepath = Common.getPhotosPath();
        basepath = common.getBasePath(context);
        this.context = context;
        
        this.locale = Common.getLanguageLocale(language);
    }

    public static boolean checkWoundIsOpen(String strStatus) {
        boolean blnResult = true;
        if (strStatus != null) {
            if (strStatus.equalsIgnoreCase("Closed")) {
                blnResult = false;
            }
        }
        return blnResult;
    }

    public float getTotalProductCost(Collection<AssessmentProductVO> strAssessmentProducts, int intPatientCareLocation) {
        float fltTotalProductCost = 0.0F;
        float fltQuantity = 0.0F;
        if (intPatientCareLocation < 1) {
            intPatientCareLocation = Integer.parseInt(Common.getConfig("community"));
        }
        ProductsDAO productsDB = new ProductsDAO();
        for (AssessmentProductVO assessmentproducts : strAssessmentProducts) {
            ProductsVO productdetails = new ProductsVO();
            if (assessmentproducts.getProduct_id() != -1) {
                productdetails.setId(assessmentproducts.getProduct_id());
                try {
                    ProductsVO prd = (ProductsVO) productsDB.findByCriteria(productdetails);
                    String strCost = "0.0";
                    strCost = prd.getCost() + "";

                    float fltCost = Float.parseFloat(strCost);
                    if (assessmentproducts.getQuantity() != null && assessmentproducts.getQuantity().length() > 0) {
                        fltQuantity = Float.valueOf(assessmentproducts.getQuantity()).floatValue();
                    }
                    if (fltQuantity > 0 && fltCost > 0) {
                        fltTotalProductCost = fltTotalProductCost + fltQuantity * fltCost;
                    }
                } catch (Exception exception2) {
                    System.out.println("Exception in getTotalProductCost: " + exception2);
                }
            }
        }
        return fltTotalProductCost;
    }

    public static String convertToFixedDigits(int intInput, int intDigits) {
        if (intInput < 0) {
            intInput = 0;
        }
        String strResult = Integer.toString(intInput);
        while (strResult.length() < intDigits) {
            strResult = "0" + strResult;
        }
        return strResult;
    }

    public static String convertToFixedLength(String strInput, int intDigits) {
        int intTemp = 0;
        try {
            intTemp = Integer.parseInt(strInput);
        } catch (NumberFormatException e) {
            intTemp = 0;
        }
        if (intTemp < 0) {
            intTemp = 0;
        }
        String strResult = Integer.toString(intTemp);
        while (strResult.length() < intDigits) {
            strResult = "0" + strResult;
        }
        return strResult;
    }

    public static String replaceSubString(String strInput, String strSubString, String strNewSubString) {
        int intStartPos = strInput.indexOf(strSubString);
        while (strSubString != "" && intStartPos > -1) {
            strInput = strInput.substring(0, intStartPos) + strNewSubString + strInput.substring(intStartPos + strSubString.length());
            intStartPos = strInput.indexOf(strSubString);
        }
        return strInput;
    }

    public static String removeEndString(String strInput, String strSubString) {
        strInput = strInput.trim();
        while (strInput.length() > 0 && strSubString.length() <= strInput.length() && strInput.substring(strInput.length() - strSubString.length()).equals(strSubString)) {
            strInput = strInput.substring(0, strInput.length() - strSubString.length());
        }
        return strInput;
    }

    public static String removeSubString(String strInput, String strSubString) {
        int intStartPos = strInput.indexOf(strSubString);
        while (strSubString != "" && intStartPos > -1) {
            strInput = strInput.substring(0, intStartPos) + strInput.substring(intStartPos + strSubString.length());
            intStartPos = strInput.indexOf(strSubString);
        }
        return strInput;
    }

    public boolean getPartialSelectedLocations(String[] strTreatmentLocations, Collection<UserAccountRegionsVO> regions) throws ApplicationException {
        boolean blnSelection = false; // false = no selection, all locations
        if (strTreatmentLocations != null) {
            int intItem = 0;
            for (UserAccountRegionsVO region : regions) {

                try {
                    LookupVO vo = listBD.getListItem(region.getTreatment_location_id());
                    if (vo != null && vo.getActive() == 1 && blnSelection == false) //
                    {
                        int intItem2 = 0;
                        boolean blnItemFound = false;
                        while (intItem2 < strTreatmentLocations.length && blnItemFound == false) {
                            if (strTreatmentLocations[intItem2].equals(region.getTreatment_location_id())) {
                                blnItemFound = true;
                            }
                            intItem2++;
                        }
                        if (blnItemFound == false) {
                            blnSelection = true;
                        }
                    }
                    intItem++;
                } catch (Exception e) {
                }
            }
            return blnSelection;
        } else {
            return true; // Nothing selected, result is "selection" because not all locations were selected
        }
    }

    public static boolean getLocationAccess(String strLocation, String[] strTreatmentLocations, String strIncludeTIP) {
        boolean blnItemFound = false;
        //System.out.println(":: "+strLocation+" "+strTreatmentLocations);
        if (strLocation.equals("-1")) {
            if (strIncludeTIP != null && strIncludeTIP.equals("1")) {
                blnItemFound = true;
            }
        } else {
            if (strTreatmentLocations != null) {
                int intItem = 0;
                while (intItem < strTreatmentLocations.length && blnItemFound == false) {
                    if (strTreatmentLocations[intItem].equals(strLocation)) {
                        blnItemFound = true;
                    }
                    intItem++;
                }
            }
        }
        return blnItemFound;
    }

    public static float getFloatFromString(String strValue) {
        float fltResult = 0.0F;
        if (strValue == null) {
            strValue = "";
        }
        strValue = strValue.trim();
        // First check the string
        int intPoint = 0;
        boolean blnInvalidChar = false;
        int intChar = 0;
        while (intChar < strValue.length() && blnInvalidChar == false && intPoint < 2) {
            int intCharValue = (int) strValue.charAt(intChar);
            if (intCharValue == 46) {
                intPoint++;
            } else {
                if (intCharValue < 48 || intCharValue > 57) {
                    blnInvalidChar = true;
                }
            }
            intChar++;
        }
        if (strValue.length() > 0 && blnInvalidChar == false && intPoint < 2) {
            try {
                fltResult = Float.valueOf(strValue).floatValue();
            } catch (Exception e) {
            }
        }
        return fltResult;
    }
    // REPORT 6: Referrals and recommendations \\

    public Vector generateReferralsAndRecommendations(String strTypeOfGrouping, Date intEpochStartDate, Date intEpochEndDate) {
        // strTypeOfGrouping = "None", "Area", "Treatment Location", "Service Area"
        Vector vecResultset = new Vector();
        try {
            reportDAO.connect();
            String timeQuery = "";
            String treatmentQuery = "";
            boolean operandBool = false;
            for (String treatment : treatment_location) {
                String operand = "AND (";
                if (operandBool == true) {
                    operand = "OR";
                }
                treatmentQuery = treatmentQuery + " " + operand + " wound_assessment.treatment_location_id=" + treatment;
                operandBool = true;
            }
            if (includeTIP != null && (includeTIP.equals("1"))) {
                treatmentQuery = treatmentQuery + " OR wound_assessment.treatment_location_id=-1";
            }
            if (intEpochStartDate != null) {
                timeQuery = " AND wound_assessment.created_on>='" + 
                        PDate.getDate(intEpochStartDate, true) + 
                        "' AND wound_assessment.created_on<='" + 
                        PDate.getDate(intEpochEndDate, true) + "'";
            }
            String assessmentQuery = "select id,treatment_location_id from wound_assessment where active=1" + timeQuery + " " + treatmentQuery + ")";
            System.out.println("Query: " + assessmentQuery);
            ResultSet assessments = reportDAO.getResults(assessmentQuery);
            ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
            Collection<PositionVO> positions = pservice.getPositions(new PositionVO());
            //System.out.println(assessments.getFetchSize());
            while (assessments.next()) {
                // Check the daterange
                int assessment_id = assessments.getInt("id");
                int treatment_location_id = assessments.getInt("treatment_location_id");
                // Get the assessment details to check the location
                int intRefCount = 0;
                int intRecCount = 0;
                int intAckCount = 0;
                int intRecommendationsResponse = 0;
                int intAcknowledgementsResponse = 0;
                int intRecommendationsWithinPriority = 0;
                int intMaxResponseTime = 0;
                int intTotalTurnaround = 0;
                int intTotalTurnaroundCount = 0;
                String position_name = " ";
                String strPriority = "Without referral request";
                ReferralsTrackingVO referrals = new ReferralsTrackingVO();
                referrals.setEntry_type(0);
                referrals.setAssessment_id(assessment_id);
                referrals.setActive(1);
                ReferralsTrackingDAO refDAO = new ReferralsTrackingDAO();
                Collection lcol3 = null;
                lcol3 = refDAO.findAllByCriteria(referrals);
                Iterator iter3 = lcol3.iterator();
                Date intRefTimestamp = null;
                while (iter3.hasNext()) {
                    ReferralsTrackingVO ref = (ReferralsTrackingVO) iter3.next();
                    intRefCount++;
                    intRefTimestamp = ref.getCreated_on();
                    
                    if (intRefCount > 0) {
                        if (ref.getTop_priority() == null || ref.getTop_priority() == 0) {
                            strPriority = "None";
                            for(PositionVO pos : positions){
                                if(pos.getId().equals(ref.getAssigned_to())){
                                    position_name=pos.getTitle();
                                }
                            }
                        } else {
                            LookupVO vo = ref.getPriorityList();
                            strPriority = vo.getName(language);
                            for(PositionVO pos : positions){
                                if(pos.getId().equals(ref.getAssigned_to())){
                                    position_name=pos.getTitle();
                                }
                            }
                            intMaxResponseTime = 0;
                            try {
                                if (vo.getReport_value() != null) {
                                    intMaxResponseTime = Integer.parseInt(vo.getReport_value());
                                }
                            } catch (Exception ex) {
                                intMaxResponseTime = 0;
                                ex.printStackTrace();
                            }
                            if (intMaxResponseTime == 0) {
                                intMaxResponseTime = 100000; // unlimited
                            }
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(intRefTimestamp);
                            int intDOW = calendar.get(Calendar.DAY_OF_WEEK);
                            // Day of week 6 = Friday
                            if (intDOW == 6) {
                                intMaxResponseTime = intMaxResponseTime + 48;
                            }
                            // Day of week 7 = Saturday
                            if (intDOW == 7 && intMaxResponseTime < 48) {
                                intMaxResponseTime = intMaxResponseTime + 24;
                            }
                        }
                    }
                }
                ReferralsTrackingVO recommendations = new ReferralsTrackingVO();
                recommendations.setEntry_type(1);
                recommendations.setAssessment_id(assessment_id);
                ReferralsTrackingDAO recDAO = new ReferralsTrackingDAO();
                Collection lcol4 = null;
                lcol4 = refDAO.findAllByCriteria(recommendations);
                Iterator iter4 = lcol4.iterator();
                Date intRecTimestamp = null;
                while (iter4.hasNext()) {
                    ReferralsTrackingVO rec = (ReferralsTrackingVO) iter4.next();
                    intRecCount++;
                    if (intRecTimestamp == null || rec.getCreated_on().before(intRecTimestamp)) {
                        intRecTimestamp = rec.getCreated_on();
                    }
                }
                ReferralsTrackingVO acknowledgement = new ReferralsTrackingVO();
                acknowledgement.setEntry_type(2);
                acknowledgement.setAssessment_id(assessment_id);
                ReferralsTrackingDAO ackDAO = new ReferralsTrackingDAO();
                Collection lcol5 = null;
                lcol5 = refDAO.findAllByCriteria(acknowledgement);
                Iterator iter5 = lcol5.iterator();
                Date intAckTimestamp = null;
                while (iter5.hasNext()) {
                    ReferralsTrackingVO ack = (ReferralsTrackingVO) iter5.next();
                    intAckCount++;
                    intAckTimestamp = ack.getCreated_on();
                }
                if (intRefTimestamp != null || intRecTimestamp != null) {
                    if (intRefTimestamp != null && intRecTimestamp != null) {
                        intRecommendationsResponse = Integer.parseInt((PDate.getEpochTime(intRecTimestamp) - PDate.getEpochTime(intRefTimestamp)) + "") / (60 * 60);
                        if (intRecommendationsResponse < 1) // Round to 1 hour if less than 1 hour
                        {
                            intRecommendationsResponse = 1;
                        }
                        if (intMaxResponseTime != 0 && intRecommendationsResponse <= intMaxResponseTime) {
                            intRecommendationsWithinPriority = 1;
                        }
                    }
                    if (intAckTimestamp != null) {
                        intAcknowledgementsResponse = Integer.parseInt((PDate.getEpochTime(intAckTimestamp) - PDate.getEpochTime(intRecTimestamp)) + "") / (60 * 60);
                        if (intAcknowledgementsResponse < 1) // Round to 1 hour if less than 1 hour
                        {
                            intAcknowledgementsResponse = 1;
                        }
                        intTotalTurnaround = (Integer.parseInt(PDate.getEpochTime(intAckTimestamp) + "") - Integer.parseInt(PDate.getEpochTime(intRefTimestamp) + "")) / (60 * 60);
                        intTotalTurnaroundCount++;
                        if (intTotalTurnaround < 1) // Round to 1 hour if less than 1 hour
                        {
                            intTotalTurnaround = 1;
                        }
                    }
                    String strTreatmentLocation = "";
                    String strGrouping = "";
                    int intTip = 0;
                    if (treatment_location_id == -1) {
                        strTreatmentLocation = "Transfer in Progress (TIP)";
                        intTip = 1;
                    } else {
                        LookupVO vo = listBD.getListItem(treatment_location_id);
                        strTreatmentLocation = vo.getName(language);
                    }

                    if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                        strGrouping = "";
                    }
                    if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                        if (treatment_location_id > -1) {
                            LookupVO lookupVO2 = new LookupVO();
                            lookupVO2.setId(treatment_location_id);
                            Collection lcol6 = null;
                            lcol6 = listDataDB.findAllByCriteria(lookupVO2);
                            for (Iterator iter6 = lcol6.iterator(); iter6.hasNext();) {
                                LookupVO lookupVO3 = (LookupVO) iter6.next();
                                ResourcesVO res = resourcesBD.getResource(lookupVO3.getResourceId());
                                strGrouping = res.getName();
                            }
                        }
                    }
                    if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                        if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                            strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                    strTreatmentLocation.indexOf(")"));
                            strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                        } else {
                            strGrouping = "";
                            intTip = 1; // List at end of report
                        }
                    }

                    ReferralsAndRecommendations referralsAndRecommendations = new ReferralsAndRecommendations();
                    referralsAndRecommendations.setTip(intTip);
                    referralsAndRecommendations.setGrouping(strGrouping);
                    referralsAndRecommendations.setTreatmentLocation(strTreatmentLocation);
                    referralsAndRecommendations.setPriority(strPriority);// +" - "+position_name);
                    referralsAndRecommendations.setReferralsCount(intRefCount);
                    referralsAndRecommendations.setRecommendationsCount(intRecCount);
                    referralsAndRecommendations.setRecommendationsResponse(intRecommendationsResponse);
                    referralsAndRecommendations.setRecommendationsWithinPriority(intRecommendationsWithinPriority);
                    referralsAndRecommendations.setAcknowledgementsCount(intAckCount);
                    referralsAndRecommendations.setAcknowledgementsResponse(intAcknowledgementsResponse);
                    referralsAndRecommendations.setTotalTurnaround(intTotalTurnaround);
                    referralsAndRecommendations.setTotalTurnaroundCount(intTotalTurnaroundCount);
                    vecResultset.add(referralsAndRecommendations);

                }
            }
            reportDAO.disconnect();
        } catch (SQLException ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort on grouping
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                ReferralsAndRecommendations sortRec1 = (ReferralsAndRecommendations) vecResultset.get(intSort2);
                ReferralsAndRecommendations sortRec2 = (ReferralsAndRecommendations) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getGrouping() + sortRec1.getTreatmentLocation() + sortRec1.getPriority()).compareTo(
                        (sortRec2.getGrouping() + sortRec2.getTreatmentLocation() + sortRec2.getPriority())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }
    /* a work in progress when I get time */

    public Vector generateWoundCountsGen2(String strTypeOfGrouping, String strBreakDown, String strBreakDownTitle, Date intEpochStartDate, Date intEpochEndDate, boolean blnAddSummary) {
        // strBreakDown = "none", "goals", "etiology", "patientCare", "diabetic", "lowerLimb", "diabLowExtr", "newPatients", "surgical", "pressure"
        boolean blnDoubleLineBreakDown = !strBreakDown.equalsIgnoreCase("none") && !strBreakDown.equalsIgnoreCase("patientCare") && !strBreakDown.equalsIgnoreCase("goals") && !strBreakDown.equalsIgnoreCase("etiology");
        String[] careTypes = {"Wound", "Incision", "T&D", "Ostomy"};
        String[] careTypeAssessments = {"wound", "incision", "drain", "ostomy"};
        String[] careTypeLocations = {"A", "T", "D", "O"};
        Vector vecResultset = new Vector();
        com.pixalere.common.service.ListServiceImpl listBD = new com.pixalere.common.service.ListServiceImpl(language);
        int intLocationsStart = 0;
        if (includeTIP != null && includeTIP.equals("1")) {
            intLocationsStart = -1;
        }
        for (String strTreatmentLocationId : treatment_location) {
            try {
                reportDAO.connect();
                String strTreatmentLocation = "";
                String strGrouping = "";
                String strSubGrouping = "";
                int intTip = 0;
                if (strTreatmentLocationId.equals("-1")) {
                    intTip = 1;
                    strTreatmentLocation = Common.getLocalizedString("pixalere.TIP", "en");
                    if (!blnDoubleLineBreakDown 
                            && !strTypeOfGrouping.equalsIgnoreCase("none") 
                            && (!(strTypeOfGrouping.equalsIgnoreCase("treatmentLocation") 
                            && strBreakDown.equalsIgnoreCase("none")))) {
                        strGrouping = Common.getConfig("treatmentLocationName");
                    }
                    strSubGrouping = Common.getConfig("treatmentLocationName");
                } else {
                    LookupVO vo = listBD.getListItem(Integer.parseInt(strTreatmentLocationId));
                    strTreatmentLocation = vo.getName(language);
                    //System.out.println(blnDoubleLineBreakDown+" "+strTypeOfGrouping+" "+strBreakDown);
                    if (strTypeOfGrouping != null) {
                        if (!blnDoubleLineBreakDown 
                                && !strTypeOfGrouping.equalsIgnoreCase("none") 
                                && (!(strTypeOfGrouping.equalsIgnoreCase("treatmentLocation") 
                                && strBreakDown.equalsIgnoreCase("none")))) {
                            strGrouping = Common.getConfig("treatmentLocationName");
                        }
                        if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                            //ResourcesVO res = resourcesBD.getResource(vo.getResourceId());
                            strGrouping = Common.getConfig("treatmentCatName");
                        }
                        if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                            if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                        strTreatmentLocation.indexOf(")"));
                                strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                            } else {
                                strGrouping = "";
                                intTip = 1; // List at end of report
                            }
                        }
                    }
                }
                strSubGrouping = strTreatmentLocation;
                String timeQuery = "";
                if (intEpochStartDate != null) {
                    timeQuery = 
                            " AND wound_assessment.created_on>='" + PDate.getDate(intEpochStartDate, true) + 
                            "' AND wound_assessment.created_on<='" + PDate.getDate(intEpochEndDate, true) + 
                            "' AND wound_assessment.treatment_location_id=" + strTreatmentLocationId;
                }
                String patientsVisitQuery = "select count(DISTINCT wound_assessment.patient_id) as count from wound_assessment where active=1  " + timeQuery;
                String totalVisitsQuery = "select count(wound_assessment.id) as count from wound_assessment where active=1 AND visit=1 " + timeQuery;
                //System.out.println(patientsVisitQuery);

                int patientVisits = Common.getInt(reportDAO.getResults(patientsVisitQuery), "count");
                int totalVisits = Common.getInt(reportDAO.getResults(totalVisitsQuery), "count");
                int cnt = 0;
                //System.out.println(patientsVisitQuery);
                boolean includedPatientCount = false;
                if (totalVisits != 0) {
                    for (String caretype : careTypes) {
                        //String woundVisitsQuery = "select count(wound_assessment.id) as count from wound_assessment LEFT JOIN assessment_" + careTypeAssessments[cnt] + " ON assessment_" + careTypeAssessments[cnt] + ".assessment_id=wound_assessment.id where wound_assessment.active=1 " + timeQuery+" AND wound_assessment.treatment_location_id="+strTreatmentLocationId;
                        String woundQuery = 
                                "select distinct assessment_" + 
                                careTypeAssessments[cnt] + 
                                ".alpha_id from wound_assessment "
                                + "LEFT JOIN assessment_" + careTypeAssessments[cnt] + " ON assessment_" + 
                                careTypeAssessments[cnt] + ".assessment_id=wound_assessment.id where  wound_assessment.active=1 " + timeQuery + " ";
                        ResultSet wounds = reportDAO.getResults(woundQuery);
                        String wquery = "";
                        if (wounds != null) {
                            while (wounds.next()) {
                                int wound_id = wounds.getInt("alpha_id");
                                wquery = wquery + " wound_assessment_location.id=" + wound_id + " OR ";
                            }
                            if (wquery.length() > 2) {
                                wquery = wquery.substring(0, wquery.length() - 4);
                                wquery = "(" + wquery + ") AND ";
                            }
                        }
                        //System.out.println(woundVisitsQuery);
                        String woundExistingQuery = "select count(wound_assessment_location.id) as count from wound_assessment_location where " + wquery + " " + (intEpochStartDate != null ? " open_time_stamp<'" + PDate.getDate(intEpochStartDate, true) : "") + "' " + (intEpochStartDate != null ? " AND open_time_stamp<'" + PDate.getDate(intEpochEndDate, true) + "'" : "");
                        String woundNewQuery = "select count(wound_assessment_location.id) as count from wound_assessment_location  where  " + wquery + "  " + (intEpochStartDate != null ? "  open_time_stamp>'" + PDate.getDate(intEpochStartDate, true) : "") + "' " + (intEpochStartDate != null ? " AND open_time_stamp<'" + PDate.getDate(intEpochEndDate, true) + "'" : "");
                        //System.out.println(woundNewQuery);
                        String woundClosedQuery = "select count(wound_assessment_location.id) as count from wound_assessment_location  where " + wquery + " wound_assessment_location.discharge=1  " + (intEpochStartDate != null ? " AND close_time_stamp>'" + PDate.getDate(intEpochStartDate, true) + "'" : "") + " " + (intEpochStartDate != null ? " AND close_time_stamp<'" + PDate.getDate(intEpochEndDate, true) + "'" : "");
                        String woundAssessmentsCountQuery = "select count(assessment_" + careTypeAssessments[cnt] + ".id) as count from assessment_" + careTypeAssessments[cnt] + " LEFT JOIN wound_assessment ON wound_assessment.id=assessment_" + careTypeAssessments[cnt] + ".assessment_id where wound_assessment.active=1 " + timeQuery + " AND wound_assessment.treatment_location_id=" + strTreatmentLocationId;
                        //int woundVisits = reportDAO.getInt(reportDAO.getResults(woundVisitsQuery), "count");
                        int woundExisting = Common.getInt(reportDAO.getResults(woundExistingQuery), "count");
                        int woundNew = Common.getInt(reportDAO.getResults(woundNewQuery), "count");
                        int woundClosed = Common.getInt(reportDAO.getResults(woundClosedQuery), "count");
                        int woundBoth = woundExisting + woundNew;
                        int assessmentVisits = Common.getInt(reportDAO.getResults(woundAssessmentsCountQuery), "count");
                        if (strTreatmentLocationId.equals("894") && caretype.equals("Wound")) {
                            System.out.println("Existing: " + woundExistingQuery + "\n");
                            System.out.println("New: " + woundNewQuery + "\n");
                            System.out.println("Patient: " + patientsVisitQuery + "\n");
                            System.out.println("Visits: " + totalVisitsQuery + "\n");
                            System.out.println("Closed: " + woundClosedQuery + "\n");
                            System.out.println("Assessment: " + woundAssessmentsCountQuery + "\n");
                            System.out.println("WoundQ: " + woundQuery + "\n");
                        }
                        float avgVisits = 0;
                        if (totalVisits != 0) {
                            avgVisits = patientVisits / totalVisits;
                        }
                        //System.out.println("grouping: "+strGrouping+" careType: "+caretype+" woundVisits: "+woundVisits+" woundExists: "+woundExisting);
                        WoundCounts woundCounts = new WoundCounts();
                        woundCounts.setTip(intTip);
                        woundCounts.setGrouping(strGrouping);
                        woundCounts.setSubGrouping(strSubGrouping);
                        String careTypeLocalized = Common.getWoundProfileType(careTypeLocations[cnt], locale);
                        woundCounts.setCareType(careTypeLocalized);
                        woundCounts.setCareTypeNr(cnt + 1);
                        if (includedPatientCount == false && woundBoth != 0) {//include count only on if row might show up.
                            woundCounts.setPatientCount(patientVisits);
                            woundCounts.setVisitsCount(totalVisits);
                            includedPatientCount = true;
                        } else {
                            woundCounts.setPatientCount(0);
                            woundCounts.setVisitsCount(0);
                        }
                        woundCounts.setWoundCurrentActiveCount(woundBoth - woundClosed);
                        woundCounts.setWoundCount(woundBoth);
                        woundCounts.setWoundExistingCount(woundExisting);
                        woundCounts.setWoundNewCount(woundNew);
                        woundCounts.setWoundClosedCount(woundClosed);
                        woundCounts.setWoundAssessmentsCount(assessmentVisits);
                        woundCounts.setBreakDown(strBreakDownTitle);
                        //woundCounts.setPatientCountBreakDown(intPatientCountBreakDown);
                        //woundCounts.setWoundCountBreakDown(vecAlphasBreakDown.size());
                        //woundCounts.setWoundExistingCountBreakDown(vecAlphasBreakDown.size() - vecAlphasNewBreakDown.size());
                        //woundCounts.setWoundNewCountBreakDown(vecAlphasNewBreakDown.size());
                        //woundCounts.setWoundClosedCountBreakDown(vecAlphasClosedBreakDown.size());
                        //woundCounts.setVisitsCountBreakDown(intVisitsCountBreakDown);
                        //woundCounts.setWoundAssessmentsCountBreakDown(intWoundAssessmentsCountBreakDown);
                        vecResultset.add(woundCounts);
                        cnt++;
                    }
                }
                //select count(id) from wound_assessment_location where alpha_type='A' AND start etc
            } catch (Exception e) {
                e.printStackTrace();
                reportDAO.disconnect();
            } finally {
                reportDAO.disconnect();
            }
        }
        Vector vecSortedResultsets = new Vector();
        // Sort for main report
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                WoundCounts sortRec1 = (WoundCounts) vecResultset.get(intSort2);
                WoundCounts sortRec2 = (WoundCounts) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getSubGrouping() + sortRec1.getCareTypeNr()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getSubGrouping() + sortRec2.getCareTypeNr())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        if (blnAddSummary) {
            // Sort for subreport
            for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
                for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                    WoundCounts sortRec1 = (WoundCounts) vecResultset.get(intSort2);
                    WoundCounts sortRec2 = (WoundCounts) vecResultset.get(intSort2 + 1);
                    if (strTypeOfGrouping.equalsIgnoreCase("none")) {
                        if ((sortRec1.getCareTypeNr() + "").compareTo(sortRec2.getCareTypeNr() + "") > 0) {
                            vecResultset.setElementAt(sortRec2, intSort2);
                            vecResultset.setElementAt(sortRec1, intSort2 + 1);
                        }
                    } else {
                        if ((sortRec1.getSubGrouping() + sortRec1.getCareTypeNr()).compareTo(sortRec2.getSubGrouping() + sortRec2.getCareTypeNr()) > 0) {
                            vecResultset.setElementAt(sortRec2, intSort2);
                            vecResultset.setElementAt(sortRec1, intSort2 + 1);
                        }
                    }
                    //    }
                }
            }
            vecSortedResultsets.add(vecResultset.toArray());
        }
        return vecSortedResultsets;
    }
    //=-=-=-=-=-=-=-=-=-
    // REPORT 1: Patients with pressure wounds \\

    public Vector generatePatientsWithPressureWoundsReport(String strTypeOfGrouping) {
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        patientAccount.setAccount_status(1); // Only accounts that are not closed
        patientAccount.setCurrent_flag(1);  // Only current account records
        Vector vecResultset = new Vector();
        Collection lcol = null;
        try {
            reportDAO.connect();
            lcol = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter = lcol.iterator();
            while (iter.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter.next();
                // Only patients that are in a treatment location that was selected
                if ((pat.getPatient_id() != null) && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    String strTreatmentLocation = "";
                    String strGrouping = "";
                    int intTip = 0;
                    if (pat.getTreatment_location_id() != null) {
                        if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                            if (!strTypeOfGrouping.equalsIgnoreCase("none")) {
                                intTip = 1;
                                strGrouping = Common.getLocalizedString("pixalere.TIP", "en");
                            }
                        } else {
                            LookupVO vo = pat.getTreatmentLocation();
                            strTreatmentLocation = vo.getName(language);
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                                strGrouping = Common.getConfig("treatmentLocationName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                                LookupVO lookupVO2 = pat.getTreatmentLocation();
                                //ResourcesVO res = resourcesBD.getResource(lookupVO2.getResourceId());
                                strGrouping = Common.getConfig("treatmentCatName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                                if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                    strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                            strTreatmentLocation.indexOf(")"));
                                    strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                                } else {
                                    strGrouping = "";
                                    intTip = 1; // List at end of report
                                }
                            }
                        }
                        com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
                        woundProfile.setCurrent_flag(1);
                        woundProfile.setPatient_id(pat.getPatient_id());
                        Collection lcol2 = null;
                        lcol2 = woundProfileDB.findAllByCriteria(woundProfile, 0); // Multiple records
                        Iterator iter2 = lcol2.iterator();
                        while (iter2.hasNext()) {
                            WoundProfileVO wprf = (WoundProfileVO) iter2.next();
                            if (checkWoundIsOpen(wprf.getWound().getStatus()) && (wprf.getPressure_ulcer() != 0)) { // Only open wounds, and pressure
                                LookupVO vo2 = listBD.getListItem(wprf.getPressure_ulcer());
                                String strStage = vo2.getName(language).trim();
                                PressureWounds pressureWounds = new PressureWounds();
                                pressureWounds.setWoundStage1(0);
                                pressureWounds.setWoundStage2(0);
                                pressureWounds.setWoundStage3(0);
                                pressureWounds.setWoundStage4(0);
                                pressureWounds.setWoundStage5(0);
                                if (strStage.startsWith("1")) {
                                    pressureWounds.setWoundStage1(1);
                                }
                                if (strStage.startsWith("2")) {
                                    pressureWounds.setWoundStage2(1);
                                }
                                if (strStage.startsWith("3")) {
                                    pressureWounds.setWoundStage3(1);
                                }
                                if (strStage.startsWith("4")) {
                                    pressureWounds.setWoundStage4(1);
                                }
                                if (strStage.startsWith("X")) {
                                    pressureWounds.setWoundStage5(1);
                                }
                                pressureWounds.setTip(intTip);
                                pressureWounds.setGrouping(strGrouping);
                                pressureWounds.setTreatmentLocation(strTreatmentLocation);
                                pressureWounds.setPatientAccountId(pat.getPatient_id());
                                pressureWounds.setPatientNameFull(pat.getLastName() + ", " + pat.getFirstName());
                                pressureWounds.setWoundProfile(wprf.getWound().getWound_location());
                                pressureWounds.setWoundStage(strStage);
                                vecResultset.add(pressureWounds);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort for main report: Treatment Location, Patient ID (required for grouping)
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                PressureWounds sortRec1 = (PressureWounds) vecResultset.get(intSort2);
                PressureWounds sortRec2 = (PressureWounds) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getTreatmentLocation() + sortRec1.getPatientAccountId()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getTreatmentLocation() + sortRec2.getPatientAccountId())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        // Sort for subreport: Treatment Location, Wound Stage, Patient ID, Wound Profile
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                PressureWounds sortRec1 = (PressureWounds) vecResultset.get(intSort2);
                PressureWounds sortRec2 = (PressureWounds) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getTreatmentLocation() + sortRec1.getWoundStage()
                        + sortRec1.getPatientAccountId() + sortRec1.getWoundProfile()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getTreatmentLocation()
                        + sortRec2.getWoundStage() + sortRec2.getPatientAccountId() + sortRec2.getWoundProfile())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }
// REPORT 2: Last documented assessment for maintenance and healing wounds \\

    public Vector generateLastAssessmentsReport(String strTypeOfGrouping, String strFullOrAll, Vector vecCaretypes, Vector vecGoals, Vector vecGoalDays) {
        // strFullOrAll = "Full", "All"
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        patientAccount.setAccount_status(1); // Only accounts that are not closed
        patientAccount.setCurrent_flag(1);  // Only current account records
        Vector vecResultset = new Vector();
        Collection lcol = null;
        try {
            reportDAO.connect();
            lcol = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter = lcol.iterator();
            while (iter.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter.next();
                // Only patients that are in a treatment location that was selected
                if ((pat.getPatient_id() != null)
                        && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    String strTreatmentLocation = "";
                    String strGrouping = "";
                    int intTip = 0;
                    if (pat.getTreatment_location_id() != null) {
                        if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                            if (!strTypeOfGrouping.equalsIgnoreCase("none")) {
                                intTip = 1;
                                strGrouping = Common.getLocalizedString("pixalere.TIP", "en");
                                strTreatmentLocation = Common.getLocalizedString("pixalere.TIP", "en");
                            }
                        } else {
                            //LookupVO vo = pat.getTreatmentLocation();
                            // strTreatmentLocation = vo.getName(language);
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                                strGrouping = Common.getConfig("treatmentLocationName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                                //ResourcesVO res = resourcesBD.getResource(vo.getResourceId());
                                strGrouping = Common.getConfig("treatmentCatName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                                if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                    strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                            strTreatmentLocation.indexOf(")"));
                                    strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                                } else {
                                    strGrouping = "";
                                    intTip = 1; // List at end of report
                                }
                            }
                        }
                        /*com.pixalere.patient.bean.PatientProfileVO patientProfile = new com.pixalere.patient.bean.PatientProfileVO();
                        patientProfile.setCurrent_flag(1);
                        patientProfile.setPatient_id(pat.getPatient_id());
                        Collection lcol2 = null;
                        lcol2 = patientProfileDB.findAllByCriteria(patientProfile, 1); // Max. 1 record
                        Iterator iter2 = lcol2.iterator();
                        while (iter2.hasNext()) {*/
                            //PatientProfileVO prf = (PatientProfileVO) iter2.next();
                            //System.out.println("Patient: "+prf.getPatient_id());
                            com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
                            woundProfile.setCurrent_flag(1);
                            woundProfile.setPatient_id(pat.getPatient_id());
                            Collection lcol3 = null;
                            lcol3 = woundProfileDB.findAllByCriteria(woundProfile, 0); // Multiple records
                            Iterator iter3 = lcol3.iterator();
                            while (iter3.hasNext()) {
                                WoundProfileVO wprf = (WoundProfileVO) iter3.next();
                                if (checkWoundIsOpen(wprf.getWound().getStatus())) { // Only open wounds
                                    //System.out.println("wound_profile: "+wprf.getWound_id());
                                    WoundAssessmentLocationVO woundAssessmentLocation = new WoundAssessmentLocationVO();
                                    woundAssessmentLocation.setWound_id(wprf.getWound_id());
                                    woundAssessmentLocation.setActive(1); // Only submitted alpha's based on boolean
                                    woundAssessmentLocation.setDischarge(0); // Only open alpah's
                                    Collection lcol4 = null;
                                    lcol4 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation, 0, false, "");//4th parameter (extra sort) is ignored
                                    Iterator iter4 = lcol4.iterator();
                                    while (iter4.hasNext()) {
                                        WoundAssessmentLocationVO wal = (WoundAssessmentLocationVO) iter4.next();
                                        String strAlphaGoal = "";
                                        for (GoalsVO goal : wprf.getGoals()) {
                                            if (goal.getAlpha_id().equals(wal.getId())) {
                                                strAlphaGoal = goal.getLookup().getName(language);
                                            }
                                        }
                                        //System.out.println("alplha: "+wal.getAlpha()+" ");
                                        if (!wal.getAlpha().equals("incs")
                                                && vecCaretypes.contains(wal.getWound_profile_type().getAlpha_type().toLowerCase().trim())
                                                && vecGoals.contains(strAlphaGoal.toLowerCase().trim())) {
                                            int intItem = 0;
                                            int intEpochPastDays = 0;
                                            while (intItem < vecGoals.size() && intEpochPastDays == 0) {
                                                if (vecGoals.get(intItem).equals(strAlphaGoal.toLowerCase().trim())) {
                                                    // Add 86399 to include the full day of the enddate, and substract 3661...
                                                    // just because it would go over 12:00 am otherwise....
                                                    intEpochPastDays = Integer.parseInt(PDate.getEpochTime(getIntEpochNow()) + "") - (Integer.parseInt((String) vecGoalDays.get(intItem)) * 60 * 60 * 24) + 86399 - 3661;
                                                }
                                                intItem++;
                                            }
                                            Date intLastFilteredAssessmentTimeStamp = null;
                                            String strLastFilteredAssessmentDate = "";
                                            boolean blnActive = false;
                                            // Get all last assessments for this Alpha
                                            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
                                            AbstractAssessmentVO ass = new AbstractAssessmentVO();
                                            String strLastAssStatus = "0";
                                            //System.out.println("WPT: "+wal.getWound_profile_type().getAlpha_type()+"  "+Constants.WOUND_PROFILE_TYPE);
                                            /////////////////////////////////////////////////////////////
                                            if (wal.getWound_profile_type().getAlpha_type().equals(Constants.WOUND_PROFILE_TYPE)) {
                                                com.pixalere.assessment.bean.AssessmentEachwoundVO assessmentcrit = new com.pixalere.assessment.bean.AssessmentEachwoundVO();
                                                assessmentcrit.setWound_id(wprf.getWound_id());
                                                assessmentcrit.setAlpha_id(wal.getId());
                                                ass = (AssessmentEachwoundVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                String strStatus = "0";
                                                if (ass != null && ass.getStatus() != null) {
                                                    strStatus = Integer.toString(ass.getStatus());
                                                }
                                                blnActive = strStatus.equals(Common.getConfig("woundActive"));
                                                //System.out.println("Status "+Common.getConfig("woundActive")+" "+strStatus);
                                                if (blnActive && strFullOrAll.equals("1")) {
                                                    assessmentcrit.setFull_assessment(new Integer(1));
                                                    ass = (AssessmentEachwoundVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                }
                                            }
                                            if (wal.getWound_profile_type().getAlpha_type().equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                                                com.pixalere.assessment.bean.AssessmentIncisionVO assessmentcrit = new com.pixalere.assessment.bean.AssessmentIncisionVO();
                                                assessmentcrit.setWound_id(wprf.getWound_id());
                                                assessmentcrit.setAlpha_id(wal.getId());
                                                ass = (AssessmentIncisionVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                String strStatus = "0";
                                                if (ass != null && ass.getStatus() != null) {
                                                    strStatus = Integer.toString(ass.getStatus());
                                                }
                                                blnActive = strStatus.equals(Common.getConfig("incisionActive"));
                                                if (blnActive && strFullOrAll.equals("1")) {
                                                    assessmentcrit.setFull_assessment(new Integer(1));
                                                    ass = (AssessmentIncisionVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                }
                                            }
                                            if (wal.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE)) {
                                                com.pixalere.assessment.bean.AssessmentOstomyVO assessmentcrit = new com.pixalere.assessment.bean.AssessmentOstomyVO();
                                                assessmentcrit.setWound_id(wprf.getWound_id());
                                                assessmentcrit.setAlpha_id(wal.getId());
                                                ass = (AssessmentOstomyVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                String strStatus = "0";
                                                if (ass != null && ass.getStatus() != null) {
                                                    strStatus = Integer.toString(ass.getStatus());
                                                }
                                                blnActive = strStatus.equals(Common.getConfig("ostomyActive"));
                                                if (blnActive && strFullOrAll.equals("1")) {
                                                    assessmentcrit.setFull_assessment(new Integer(1));
                                                    ass = (AssessmentOstomyVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                }
                                            }
                                            if (wal.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                                                com.pixalere.assessment.bean.AssessmentDrainVO assessmentcrit = new com.pixalere.assessment.bean.AssessmentDrainVO();
                                                assessmentcrit.setWound_id(wprf.getWound_id());
                                                assessmentcrit.setAlpha_id(wal.getId());
                                                ass = (AssessmentDrainVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                String strStatus = "0";
                                                if (ass != null && ass.getStatus() != null) {
                                                    strStatus = Integer.toString(ass.getStatus());
                                                }
                                                blnActive = strStatus.equals(Common.getConfig("drainActive"));
                                                if (blnActive && strFullOrAll.equals("1")) {
                                                    assessmentcrit.setFull_assessment(new Integer(1));
                                                    ass = (AssessmentDrainVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                }
                                            }
                                            if (wal.getWound_profile_type().getAlpha_type().equals(Constants.BURN_PROFILE_TYPE)) {
                                                com.pixalere.assessment.bean.AssessmentBurnVO assessmentcrit = new com.pixalere.assessment.bean.AssessmentBurnVO();
                                                assessmentcrit.setWound_id(wprf.getWound_id());
                                                assessmentcrit.setAlpha_id(wal.getId());
                                                ass = (AssessmentBurnVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                String strStatus = "0";
                                                if (ass != null && ass.getStatus() != null) {
                                                    strStatus = Integer.toString(ass.getStatus());
                                                }
                                                blnActive = strStatus.equals(Common.getConfig("burnActive"));
                                                if (blnActive && strFullOrAll.equals("1")) {
                                                    assessmentcrit.setFull_assessment(new Integer(1));
                                                    ass = (AssessmentBurnVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                }
                                            }
                                            if (wal.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE)) {
                                                com.pixalere.assessment.bean.AssessmentSkinVO assessmentcrit = new com.pixalere.assessment.bean.AssessmentSkinVO();
                                                assessmentcrit.setWound_id(wprf.getWound_id());
                                                assessmentcrit.setAlpha_id(wal.getId());
                                                ass = (AssessmentSkinVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                String strStatus = "0";
                                                if (ass != null && ass.getStatus() != null) {
                                                    strStatus = Integer.toString(ass.getStatus());
                                                }
                                                blnActive = strStatus.equals(Common.getConfig("skinActive"));
                                                if (blnActive && strFullOrAll.equals("1")) {
                                                    assessmentcrit.setFull_assessment(new Integer(1));
                                                    ass = (AssessmentSkinVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                                }
                                            }
                                            if (blnActive && ass != null) {
                                                WoundAssessmentVO was = ass.getWoundAssessment();
                                                intLastFilteredAssessmentTimeStamp = was.getCreated_on();
                                                strLastFilteredAssessmentDate = new PDate().getLocalizedDateString(was.getCreated_on(), Common.getLocaleFromLocaleString(locale));
                                                if (PDate.getEpochTime(intLastFilteredAssessmentTimeStamp) < intEpochPastDays) {
                                                    LastAssessments lastAssessments = new LastAssessments();
                                                    lastAssessments.setTip(intTip);
                                                    lastAssessments.setGrouping(strGrouping);
                                                    lastAssessments.setTreatmentLocation(strTreatmentLocation);
                                                    lastAssessments.setPatientAccountId(pat.getPatient_id().intValue());
                                                    String strPatientAccountId = Integer.toString(pat.getPatient_id());
                                                    while (strPatientAccountId.length() < 10) {
                                                        strPatientAccountId = "0" + strPatientAccountId;
                                                    }
                                                    lastAssessments.setPatientAccountId_sort(strPatientAccountId);
                                                    String strLastName = patientAccount.getDecrypted(pat.getLastName(), "pass4lastname");
                                                    String strFirstName = patientAccount.getDecrypted(pat.getFirstName(), "pass4firstname");
                                                    String strFullName = "";
                                                    if ((strLastName.trim() + strFirstName.trim()).length() > 0) {
                                                        strFullName = strLastName + ", " + strFirstName;
                                                    }
                                                    lastAssessments.setPatientNameFull(strFullName);
                                                    lastAssessments.setWoundProfile(wprf.getWound().getWound_location_detailed());
                                                    lastAssessments.setWoundAlpha(Common.getAlphaText(wal.getAlpha(), locale));
                                                    lastAssessments.setWoundGoal(strAlphaGoal);
                                                    lastAssessments.setLastAssessmentDate(strLastFilteredAssessmentDate);
                                                    lastAssessments.setLastAssessmentTimeStamp(intLastFilteredAssessmentTimeStamp);
                                                    vecResultset.add(lastAssessments);
                                                }
                                            }
                                        }
                                    }
                                //}
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Exception in generateLastAssessmentsReport: " + ex);
            ex.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        //    // Prevent from showing empty report
        //    if (vecResultset.size()==0) {
        //        LastAssessments lastAssessments = new LastAssessments();
        //        lastAssessments.setGrouping("");
        //        vecResultset.add(lastAssessments);
        //    }
        Vector vecSortedResultsets = new Vector();
        // Sort on Treatment Location, Last Assessmentdate, Patient ID, Wound Profile
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                LastAssessments sortRec1 = (LastAssessments) vecResultset.get(intSort2);
                LastAssessments sortRec2 = (LastAssessments) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping()
                        + sortRec1.getTreatmentLocation() + sortRec1.getPatientAccountId_sort()
                        + sortRec1.getWoundProfile() + sortRec1.getWoundAlpha()
                        + sortRec1.getLastAssessmentTimeStamp()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping()
                        + sortRec2.getTreatmentLocation() + sortRec2.getPatientAccountId_sort()
                        + sortRec2.getWoundProfile() + sortRec2.getWoundAlpha()
                        + sortRec2.getLastAssessmentTimeStamp())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }

    // REPORT 5: Transferred patients \\
    public Vector generateTransferredPatients(Date intEpochStartDate, Date intEpochEndDate) {
        Vector vecResultset = new Vector();
        try {
            reportDAO.connect();
            com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
            patientAccount.setCurrent_flag(1);  // Only current account records; do include closed accounts
            Collection lcol = null;
            lcol = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter = lcol.iterator();
            while (iter.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter.next();
                if ((pat.getPatient_id() != null) && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    int intOldCareLocation = -1;
                    int intWoundCountThisPatient = -1;
                    com.pixalere.patient.bean.PatientProfileVO patientProfile = new com.pixalere.patient.bean.PatientProfileVO();
                    patientProfile.setPatient_id(pat.getPatient_id()); // All profiles for this patient, incl. historical
                    Collection lcol2 = null;
                    lcol2 = patientProfileDB.findAllByCriteria(patientProfile, 0); // Multiple records allowed
                    Iterator iter2 = lcol2.iterator();
                    while (iter2.hasNext()) { // We assume that the profiles are in chronological order
                        PatientProfileVO prf = (PatientProfileVO) iter2.next();
                        // Find latest location before timeframe
                        if ((intEpochStartDate == null && intOldCareLocation < 0) || prf.getCreated_on().before(intEpochStartDate)) {
                            intOldCareLocation = Integer.parseInt(pat.getTreatmentLocation().getUpdate_access());                        // if within timeframe
                        }
                        if ((prf.getCreated_on().after(intEpochStartDate)) && (prf.getCreated_on().before(intEpochEndDate))) {
                            if (intOldCareLocation < 0) {
                                intOldCareLocation = Integer.parseInt(pat.getTreatmentLocation().getUpdate_access());
                            }
                            if (Integer.parseInt(pat.getTreatmentLocation().getUpdate_access()) != intOldCareLocation) // If moved from his location
                            {
                                TransferredPatients transferredPatients = new TransferredPatients();
                                LookupVO vo = listBD.getListItem(intOldCareLocation);
                                transferredPatients.setOrigPatientCareLocation("From " + vo.getName(language) + " to:");
                                vo = listBD.getListItem(Integer.parseInt(pat.getTreatmentLocation().getUpdate_access()));
                                transferredPatients.setNewPatientCareLocation(vo.getName(language));
                                if (intWoundCountThisPatient < 0) { // If not counted yet
                                    intWoundCountThisPatient = 0;
                                    com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
                                    woundProfile.setCurrent_flag(1);
                                    woundProfile.setPatient_id(pat.getPatient_id());
                                    Collection lcol3 = null;
                                    lcol3 = woundProfileDB.findAllByCriteria(woundProfile, 0); // Multiple records
                                    Iterator iter3 = lcol3.iterator();
                                    while (iter3.hasNext()) {
                                        WoundProfileVO wprf = (WoundProfileVO) iter3.next();
                                        intWoundCountThisPatient++;
                                    }
                                }
                                transferredPatients.setWoundsCount(intWoundCountThisPatient);
                                vecResultset.add(transferredPatients);
                                intOldCareLocation = Integer.parseInt(pat.getTreatmentLocation().getUpdate_access());
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort on "From", "To"
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                TransferredPatients sortRec1 = (TransferredPatients) vecResultset.get(intSort2);
                TransferredPatients sortRec2 = (TransferredPatients) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getOrigPatientCareLocation() + sortRec1.getNewPatientCareLocation()).compareTo(
                        (sortRec2.getOrigPatientCareLocation() + sortRec2.getNewPatientCareLocation())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }
    // REPORT 6: Referrals and recommendations \\

    public Vector generateReferralsAndRecommendationsGen1(String strTypeOfGrouping, Date intEpochStartDate, Date intEpochEndDate) {
        // strTypeOfGrouping = "None", "Area", "Treatment Location", "Service Area"
        System.out.println("REPORT 6: Referrals and recommendations");
        Vector vecResultset = new Vector();
        try {
            reportDAO.connect();
            com.pixalere.assessment.bean.WoundAssessmentVO woundAssessment = new com.pixalere.assessment.bean.WoundAssessmentVO();
            woundAssessment.setActive(1);
            Collection lcol1 = null;
            lcol1 = woundAssessmentDB.findAllByCriteria(woundAssessment); // Gives in descending order; latest assessment first
            Iterator iter1 = lcol1.iterator();
            while (iter1.hasNext()) {
                WoundAssessmentVO was = (WoundAssessmentVO) iter1.next();
                // Check the daterange
                if (intEpochStartDate == null || (was != null) && (was.getCreated_on() != null) && (was.getCreated_on().after(intEpochStartDate) && (was.getCreated_on().before(intEpochEndDate)))) {
                    // Get the assessment details to check the location
                    com.pixalere.assessment.bean.AssessmentEachwoundVO assessmentEachwound = new com.pixalere.assessment.bean.AssessmentEachwoundVO();
                    assessmentEachwound.setAssessment_id(was.getId());
                    Collection lcol2 = null;
                    lcol2 = assessmentEachwoundDB.findAllByCriteriaDescending(assessmentEachwound);
                    Iterator iter2 = lcol2.iterator();
                    if (iter2.hasNext()) {
                        AssessmentEachwoundVO aew = (AssessmentEachwoundVO) iter2.next();
                        if ((aew != null) && (aew.getWoundAssessment().getTreatment_location_id() != null) && (getLocationAccess(aew.getWoundAssessment().getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                            int intRefCount = 0;
                            int intRecCount = 0;
                            int intAckCount = 0;
                            int intRecommendationsResponse = 0;
                            int intAcknowledgementsResponse = 0;
                            int intRecommendationsWithinPriority = 0;
                            int intMaxResponseTime = 0;
                            int intTotalTurnaround = 0;
                            int intTotalTurnaroundCount = 0;
                            String strPriority = "Without referral request";
                            ReferralsTrackingVO referrals = new ReferralsTrackingVO();
                            referrals.setEntry_type(0);
                            referrals.setAssessment_id(was.getId());
                            ReferralsTrackingDAO refDAO = new ReferralsTrackingDAO();
                            Collection lcol3 = null;
                            lcol3 = refDAO.findAllByCriteria(referrals);
                            Iterator iter3 = lcol3.iterator();
                            Date intRefTimestamp = null;
                            while (iter3.hasNext()) {
                                ReferralsTrackingVO ref = (ReferralsTrackingVO) iter3.next();
                                intRefCount++;
                                intRefTimestamp = ref.getCreated_on();
                                if (intRefCount > 0) {
                                    if (ref.getTop_priority() == null || ref.getTop_priority() == 0) {
                                        strPriority = "None";
                                    } else {
                                        LookupVO vo = listBD.getListItem(ref.getTop_priority());
                                        strPriority = vo.getName(language);
                                        intMaxResponseTime = 0;
                                        try {
                                            if (vo.getReport_value() != null) {
                                                intMaxResponseTime = Integer.parseInt(vo.getReport_value());
                                            }
                                        } catch (Exception ex) {
                                            intMaxResponseTime = 0;
                                        }
                                        if (intMaxResponseTime == 0) {
                                            intMaxResponseTime = 100000; // unlimited
                                        }
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTime(intRefTimestamp);
                                        int intDOW = calendar.get(Calendar.DAY_OF_WEEK);
                                        // Day of week 6 = Friday
                                        if (intDOW == 6) {
                                            intMaxResponseTime = intMaxResponseTime + 48;
                                        }
                                        // Day of week 7 = Saturday
                                        if (intDOW == 7 && intMaxResponseTime < 48) {
                                            intMaxResponseTime = intMaxResponseTime + 24;
                                        }
                                    }
                                }
                            }
                            ReferralsTrackingVO recommendations = new ReferralsTrackingVO();
                            recommendations.setEntry_type(1);
                            recommendations.setAssessment_id(was.getId());
                            ReferralsTrackingDAO recDAO = new ReferralsTrackingDAO();
                            Collection lcol4 = null;
                            lcol4 = refDAO.findAllByCriteria(recommendations);
                            Iterator iter4 = lcol4.iterator();
                            Date intRecTimestamp = null;
                            while (iter4.hasNext()) {
                                ReferralsTrackingVO rec = (ReferralsTrackingVO) iter4.next();
                                intRecCount++;
                                if (intRecTimestamp == null || rec.getCreated_on().before(intRecTimestamp)) {
                                    intRecTimestamp = rec.getCreated_on();
                                }
                            }
                            ReferralsTrackingVO acknowledgement = new ReferralsTrackingVO();
                            acknowledgement.setEntry_type(2);
                            acknowledgement.setAssessment_id(was.getId());
                            ReferralsTrackingDAO ackDAO = new ReferralsTrackingDAO();
                            Collection lcol5 = null;
                            lcol5 = refDAO.findAllByCriteria(acknowledgement);
                            Iterator iter5 = lcol5.iterator();
                            Date intAckTimestamp = null;
                            while (iter5.hasNext()) {
                                ReferralsTrackingVO ack = (ReferralsTrackingVO) iter5.next();
                                intAckCount++;
                                intAckTimestamp = ack.getCreated_on();
                            }
                            if (intRefTimestamp != null || intRecTimestamp != null) {
                                if (intRefTimestamp != null && intRecTimestamp != null) {
                                    intRecommendationsResponse = Integer.parseInt((PDate.getEpochTime(intRecTimestamp) - PDate.getEpochTime(intRefTimestamp)) + "") / (60 * 60);
                                    if (intRecommendationsResponse < 1) // Round to 1 hour if less than 1 hour
                                    {
                                        intRecommendationsResponse = 1;
                                    }
                                    if (intMaxResponseTime != 0 && intRecommendationsResponse <= intMaxResponseTime) {
                                        intRecommendationsWithinPriority = 1;
                                    }
                                }
                                if (intAckTimestamp != null) {
                                    intAcknowledgementsResponse = Integer.parseInt((PDate.getEpochTime(intAckTimestamp) - PDate.getEpochTime(intRecTimestamp)) + "") / (60 * 60);
                                    if (intAcknowledgementsResponse < 1) // Round to 1 hour if less than 1 hour
                                    {
                                        intAcknowledgementsResponse = 1;
                                    }
                                    intTotalTurnaround = Integer.parseInt((PDate.getEpochTime(intAckTimestamp) - PDate.getEpochTime(intRefTimestamp)) + "") / (60 * 60);
                                    intTotalTurnaroundCount++;
                                    if (intTotalTurnaround < 1) // Round to 1 hour if less than 1 hour
                                    {
                                        intTotalTurnaround = 1;
                                    }
                                }
                                String strTreatmentLocation = "";
                                String strGrouping = "";
                                int intTip = 0;
                                if (aew.getWoundAssessment().getTreatment_location_id() != null) {
                                    if (aew.getWoundAssessment().getTreatment_location_id() == -1) {
                                        strTreatmentLocation = "Transfer in Progress (TIP)";
                                        intTip = 1;
                                    } else {
                                        LookupVO vo = listBD.getListItem(aew.getWoundAssessment().getTreatment_location_id());
                                        strTreatmentLocation = vo.getName(language);
                                    }
                                }
                                if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                                    strGrouping = "";
                                }
                                if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                                    if (aew.getWoundAssessment().getTreatment_location_id() != null && aew.getWoundAssessment().getTreatment_location_id() > -1) {
                                        LookupVO lookupVO2 = new LookupVO();
                                        lookupVO2.setId(aew.getWoundAssessment().getTreatment_location_id());
                                        Collection lcol6 = null;
                                        lcol6 = listDataDB.findAllByCriteria(lookupVO2);
                                        for (Iterator iter6 = lcol6.iterator(); iter6.hasNext();) {
                                            LookupVO lookupVO3 = (LookupVO) iter6.next();
                                            //ResourcesVO res = resourcesBD.getResource(lookupVO3.getResourceId());
                                            strGrouping = Common.getConfig("treatmentCatName");
                                        }
                                    }
                                }
                                if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                                    if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                        strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                                strTreatmentLocation.indexOf(")"));
                                        strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                                    } else {
                                        strGrouping = "";
                                        intTip = 1; // List at end of report
                                    }
                                }
                                System.out.println("Referrals " + intRefCount + " recomCount: " + intRecCount + " response: " + intRecommendationsResponse + " within priority " + intRecommendationsWithinPriority + " ");
                                System.out.println("Ack:" + intAckCount + " ackn response: " + intAcknowledgementsResponse + " total turnaround " + intTotalTurnaround + " total turncount " + intTotalTurnaroundCount);
                                ReferralsAndRecommendations referralsAndRecommendations = new ReferralsAndRecommendations();
                                referralsAndRecommendations.setTip(intTip);
                                referralsAndRecommendations.setGrouping(strGrouping);
                                referralsAndRecommendations.setTreatmentLocation(strTreatmentLocation);
                                referralsAndRecommendations.setPriority(strPriority);
                                referralsAndRecommendations.setReferralsCount(intRefCount);
                                referralsAndRecommendations.setRecommendationsCount(intRecCount);
                                referralsAndRecommendations.setRecommendationsResponse(intRecommendationsResponse);
                                referralsAndRecommendations.setRecommendationsWithinPriority(intRecommendationsWithinPriority);
                                referralsAndRecommendations.setAcknowledgementsCount(intAckCount);
                                referralsAndRecommendations.setAcknowledgementsResponse(intAcknowledgementsResponse);
                                referralsAndRecommendations.setTotalTurnaround(intTotalTurnaround);
                                referralsAndRecommendations.setTotalTurnaroundCount(intTotalTurnaroundCount);
                                vecResultset.add(referralsAndRecommendations);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort on grouping
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                ReferralsAndRecommendations sortRec1 = (ReferralsAndRecommendations) vecResultset.get(intSort2);
                ReferralsAndRecommendations sortRec2 = (ReferralsAndRecommendations) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getGrouping() + sortRec1.getTreatmentLocation() + sortRec1.getPriority()).compareTo(
                        (sortRec2.getGrouping() + sortRec2.getTreatmentLocation() + sortRec2.getPriority())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }
    // REPORT 7: Overview active patients \\

    public Vector generateOverviewActivePatients(String strTypeOfGrouping) {
        // strTypeOfGrouping = "None", "Area", "Treatment Location", "Service Area"
        System.out.println("REPORT 7: Overview active patients");
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        patientAccount.setAccount_status(1); // Only accounts that are not closed
        patientAccount.setCurrent_flag(1);  // Only current account records
        Vector<ActivePatients> vecResultset = new Vector();
        Collection lcol = null;
        PatientServiceImpl pservice = new PatientServiceImpl();
        try {
            reportDAO.connect();
            lcol = pservice.getAllPatients(patientAccount);
            //lcol = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter = lcol.iterator();
            System.out.println("patients: "+lcol.size()+" "+treatment_location);
            while (iter.hasNext()) {
                
                PatientAccountVO pat = (PatientAccountVO) iter.next();
                //System.out.println("Patient"+pat+" "+getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP));
                if ((pat.getPatient_id() != null) && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                   // System.out.println("Allowed in");
                    String strTreatmentLocation = "";
                    String strGrouping = "";
                    int intTip = 0;
                    if (pat.getTreatment_location_id() != null) {
                        if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                            strTreatmentLocation = Common.getLocalizedString("pixalere.TIP", "en");
                            intTip = 1;
                        } else {
                            LookupVO vo = pat.getTreatmentLocation();
                            strTreatmentLocation = vo.getName(language);
                        }
                    }
                    if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                        strGrouping = "";
                    }
                    if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                        if (pat.getTreatment_location_id() != null && "-1".indexOf(pat.getTreatment_location_id() + "") == -1) {
                            LookupVO lookupVO2 = pat.getTreatmentLocation();
                            //ResourcesVO res = resourcesBD.getResource(lookupVO2.getResourceId());
                            strGrouping = Common.getConfig("treatmentCatName");
                        }
                    }
                    if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                        if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                            strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                    strTreatmentLocation.indexOf(")"));
                            strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                        } else {
                            strGrouping = "";
                            intTip = 1; // List at end of report
                        }
                    }


                    ActivePatients activePatients = new ActivePatients();
                    activePatients.setGrouping(strGrouping);
                    activePatients.setTip(intTip);
                    activePatients.setTreatmentLocation(strTreatmentLocation);
                    activePatients.setPatientAccountId(pat.getPatient_id());
                    String strLastName = pat.getLastName();
                    String strFirstName = pat.getFirstName();
                    String strFullName = "";
                    Vector vecWounds = new Vector();
                    if ((strLastName.trim() + strFirstName.trim()).length() > 0) {
                        strFullName = strLastName + ", " + strFirstName;
                    }
                    activePatients.setPatientNameFull(strFullName);
                    activePatients.setDobFull(pat.getDateOfBirthStr());
                    com.pixalere.wound.bean.WoundVO woundProfile = new com.pixalere.wound.bean.WoundVO();
                    woundProfile.setActive(1);
                    woundProfile.setPatient_id(pat.getPatient_id());
                    Collection lcol3 = null;
                    lcol3 = woundProfileDB.findAllWoundsByCriteria(woundProfile); // Multiple records
                    Iterator iter3 = lcol3.iterator();
                    int intWoundCount = 0;
                    while (iter3.hasNext()) {
                        WoundVO wprf = (WoundVO) iter3.next();
                        if (checkWoundIsOpen(wprf.getStatus())) { // Only open wounds
                            vecWounds.add(wprf.getWound_location());
                            intWoundCount++;
                        }
                    }
                    // Build string of all wounds
                    String strWounds = "";
                    if (vecWounds.size() > 0) {
                        if (vecWounds.size() > 1) { // Sort the wounds first
                            for (int intSort1 = 0; intSort1 < vecWounds.size(); intSort1++) {
                                for (int intSort2 = 0; intSort2 < vecWounds.size() - intSort1 - 1; intSort2++) {
                                    String strSortRec1 = vecWounds.get(intSort2) + "";
                                    String strSortRec2 = vecWounds.get(intSort2 + 1) + "";
                                    if (strSortRec1.compareTo(strSortRec2) > 0) {
                                        vecWounds.setElementAt(strSortRec2, intSort2);
                                        vecWounds.setElementAt(strSortRec1, intSort2 + 1);
                                    }
                                }
                            }
                        }
                        String strCurrentWound = "";
                        int intCurrentWound = 1;
                        for (int intWound = 0; intWound < vecWounds.size(); intWound++) {
                            String strWound = (String) vecWounds.get(intWound);
                            if (strWound.equalsIgnoreCase(strCurrentWound)) {
                                intCurrentWound++;
                            }
                            // add to wounds string if new type of wound, or last wound in vector
                            if (!strWound.equalsIgnoreCase(strCurrentWound) || intWound == vecWounds.size() - 1) {
                                if (intCurrentWound > 1) {
                                    strWounds = strWounds + " (" + intCurrentWound + "X)";
                                    intCurrentWound = 1;
                                }
                                if (!strWound.equalsIgnoreCase(strCurrentWound)) {
                                    if (strWounds.length() > 0) {
                                        strWounds = strWounds + ", ";
                                    }
                                    strWounds = strWounds + strWound;
                                }
                            }
                            strCurrentWound = strWound;
                        }

                        activePatients.setWoundProfiles(strWounds);
                        activePatients.setWoundCount(intWoundCount);
                        
                    }
                    vecResultset.add(activePatients);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort on Patient name
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                ActivePatients sortRec1 = (ActivePatients) vecResultset.get(intSort2);
                ActivePatients sortRec2 = (ActivePatients) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getPatientNameFull().toLowerCase().trim()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getPatientNameFull().toLowerCase().trim())) > 0) { // Be really easy on all typo's....
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);

                }
            }
        }
        // Prevent from showing empty report
        if (vecResultset.size() == 0) {
            ActivePatients activePatients = new ActivePatients();
            activePatients.setPatientNameFull("");
            activePatients.setDobFull("");
            activePatients.setTreatmentLocation("");
            vecResultset.add(activePatients);
        }
        for (ActivePatients p : vecResultset) {
            System.out.println(p.getDobFull() + "---" + p.getPatientAccountId() + " === " + p.getTreatmentLocation());
        }
        vecSortedResultsets.add(vecResultset.toArray());
        System.out.println(vecSortedResultsets.toString());
        return vecSortedResultsets;
    }
    // REPORT 8: All historical data for one patient \\

    public Vector generatePatientFileReport(Date intEpochStartDate, Date intEpochEndDate, int intPixalereID, String strIncludeDetails) {
        System.out.println("REPORT 8: Full Patient File" + intPixalereID);
        // Record level:
        // 0 = Patient info for front page
        // 1 = Patient profile
        // 2 = Braden scale
        // 3 = Limb assessment
        // 4 = Foot assessment
        // 5 = Wound profile Blue Model
        // 6 = Wound profile
        // 7 = Assessment and Images
        // 8 = Comments/Recommendations
        // 9 = Treatment Comments
        // 10 = Nursing Care Plan
        // 11 = Dressing Change Frequency
        NursingCarePlanServiceImpl nmanager = new NursingCarePlanServiceImpl();
        ProfessionalServiceImpl pservice = new ProfessionalServiceImpl();
        TreatmentCommentsServiceImpl tmanager = new TreatmentCommentsServiceImpl();
        PatientProfileServiceImpl Pmanager = new PatientProfileServiceImpl(language);
        AssessmentImagesServiceImpl iservice = new AssessmentImagesServiceImpl();
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        patientAccount.setCurrent_flag(1);  // Only current account records
        patientAccount.setPatient_id(intPixalereID);
        boolean blnAccessOK = false;
        int intImagesRowCount = 0;
        Vector vecResultset = new Vector();
        PatientFile patientFile = new PatientFile();
        Collection lcol = null;
        try {
            reportDAO.connect();
            PatientAccountVO pat = patientServiceImpl.getPatient(patientAccount);
            if (pat != null && pat.getPatient_id() != null) {
                Collection<UserAccountRegionsVO> regions = userVO.getRegions();
                blnAccessOK = (pservice.validate(regions, pat.getTreatment_location_id()) == true || pat.getTreatment_location_id().equals(new Integer("-1")));

                //com.pixalere.admin.ListBD listBD = new com.pixalere.admin.ListBD();
                LookupVO vo = pat.getTreatmentLocation();
                com.pixalere.patient.bean.PatientProfileVO patientProfile = new com.pixalere.patient.bean.PatientProfileVO();
                patientProfile.setPatient_id(pat.getPatient_id());
                patientProfile.setActive(1);
                if (strIncludeDetails.equals("pci")) {
                    patientProfile.setCurrent_flag(1);
                }
                Collection lcol2 = null;
                lcol2 = Pmanager.getAllPatientProfiles(patientProfile, 0); // All recs
                Iterator iter2 = lcol2.iterator();
                String strProfile = "";
                String strProfileUpdate = "";
                // Note: if the report is showing only the current patient profile multiple times,
                // you will have to adjust the PatientProfileManagerBD to include the rec_id
                // in function getAllPatientProfiles: if (rec_id>0) vo.setId(new Integer(rec_id));

                boolean blnRecentPP = false;
                while (iter2.hasNext()) {//@todo why is it cycling everything twice?
                    PatientProfileVO prf = (PatientProfileVO) iter2.next();
                    // Get all patient profiles, most recent first
                    Date intLastUpdate;
                    if (prf.getCreated_on() != null) {
                        intLastUpdate = prf.getCreated_on();
                        // If no profile updates within daterange, include last update before start daterange
                        if (intEpochStartDate == null || intLastUpdate == null || (intLastUpdate.after(intEpochStartDate) && intLastUpdate.before(intEpochEndDate)) || (blnRecentPP == false && intLastUpdate.before(intEpochStartDate))) {
                            if (intEpochStartDate == null || intLastUpdate.before(intEpochStartDate)) {
                                blnRecentPP = true;
                            }
                            Collection items = new ArrayList();
                            items.add(prf);
                            RowData[] vecPatientProfiles = Pmanager.getAllPatientProfilesForFlowchart(items, userVO, false,true);
                            for (int intRecs = 0; intRecs < vecPatientProfiles.length; intRecs++) {
                                strProfileUpdate = vecPatientProfiles[intRecs].getUser_signature();
                                String strPatientTitle = Common.getLocalizedString("pixalere.patientprofile.form.title", locale) + ": " + strProfileUpdate;
                                for (int intFields = 2; intFields < vecPatientProfiles[intRecs].getFields().size(); intFields++) {
                                    List<FieldValues> fields = vecPatientProfiles[intRecs].getFields();
                                    if (blnAccessOK == true) {
                                        patientFile = new PatientFile();
                                        patientFile.setRecordLevel(1); // Patient Profile
                                        patientFile.setRowNumber(convertToFixedDigits(intFields, 3));
                                        patientFile.setPatientId(pat.getPatient_id());
                                        patientFile.setWoundProfile(Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale) + ": " + pat.getPatient_id() + "   " + Common.getLocalizedString("pixalere.patientprofile.form.patient_name", locale) + ": " + pat.getPatientName());
                                        patientFile.setWoundProfileId("0000000000");
                                        patientFile.setWoundProfileRec("0000000000");
                                        patientFile.setAlpha("");
                                        patientFile.setAlphaTitle("");
                                        patientFile.setAssessment(strPatientTitle);
                                        patientFile.setAssessmentId(convertToFixedDigits(prf.getId(), 10));
                                        patientFile.setImagesRowCount(intImagesRowCount);

                                        patientFile.setRowTitle(fields.get(intFields).getTitle());
                                        patientFile.setRowData(fields.get(intFields).getValue());
                                        vecResultset.add(patientFile);
                                        strPatientTitle = Common.getLocalizedString("pixalere.patientprofile.form.title", locale) + ": " + strProfileUpdate + " (" + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                    }
                                }

                                //@todo no longer part of pp table. wil need to be updated.
                                /*if (prf.getBraden_sensory() + prf.getBraden_moisture() + prf.getBraden_activity() + prf.getBraden_nutrition() + prf.getBraden_friction() + prf.getBraden_mobility() > 0) {
                                 RowData[] vecBradenScales = Pmanager.getAllBradenForFlowchart(items, userVO, false);
                                 System.out.println("BS's found: " + vecBradenScales.length);
                                 for (intRecs = 0; intRecs < vecBradenScales.length; intRecs++) {
                                 strProfileUpdate = vecBradenScales[intRecs].getUser_signature();
                                 String strBradenTitle = Common.getLocalizedString("pixalere.viewer.form.braden_scale") + ": " + strProfileUpdate;
                                 for (int intFields = 2; intFields < vecBradenScales[intRecs].getFields().size(); intFields++) {
                                 List<FieldValues> fields = vecBradenScales[intRecs].getFields();
                                 if (blnAccessOK == true) {
                                 patientFile = new PatientFile();
                                 patientFile.setRecordLevel(2); // Braden Scale
                                 patientFile.setRowNumber(convertToFixedDigits(intFields, 3));
                                 patientFile.setPatientId(pat.getPatient_id());
                                 patientFile.setWoundProfile(Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id") + ": " + pat.getPatient_id());
                                 patientFile.setWoundProfileId("0000000000");
                                 patientFile.setWoundProfileRec("0000000000");
                                 patientFile.setAlpha("");
                                 patientFile.setAlphaTitle("");
                                 patientFile.setAssessment(strBradenTitle);
                                 patientFile.setAssessmentId(convertToFixedDigits(prf.getId(), 10));
                                 patientFile.setImagesRowCount(intImagesRowCount);
                                 //System.out.println("PP: "+fields.get(intFields).getTitle()+" = "+fields.get(intFields).getValue());
                                 patientFile.setRowTitle(fields.get(intFields).getTitle());
                                 patientFile.setRowData(fields.get(intFields).getValue());

                                 // (sortRec1.getWoundProfileId()+sortRec1.getRecordLevel()+sortRec1.getAssessmentId()+sortRec1.getAlpha()).compareTo(
                                 System.out.println(patientFile.getWoundProfileId() + " - " + patientFile.getRecordLevel() + " - " + patientFile.getAssessmentId() + " - " + patientFile.getAlpha() + " - " + patientFile.getAlphaTitle() + "; " + patientFile.getRowTitle());

                                 vecResultset.add(patientFile);
                                 strBradenTitle = Common.getLocalizedString("pixalere.viewer.form.braden_scale") + ": " + strProfileUpdate + " (" + Common.getLocalizedString("pixalere.reporting.form.continued") + ")";
                                 }
                                 }
                                 }
                                 }*/
                            }
                        }
                        if (prf.getCurrent_flag() == 1) // If current data
                        {
                            // Create this record for printing at first page here (we need most recent date of birth)
                            patientFile = new PatientFile();
                            patientFile.setRecordLevel(0); // Patient info for front page
                            patientFile.setRowNumber("000");
                            patientFile.setPatientId(pat.getPatient_id());
                            patientFile.setWoundProfile("");
                            patientFile.setWoundProfileId("0000000000");
                            patientFile.setWoundProfileRec("0000000000");
                            if (blnAccessOK == true) {
                                //if (strIncludeDetails.equals("1")) {
                                patientFile.setRecordInfoTitle("\n\n\n\n\n\n" 
                                        + Common.getLocalizedString("pixalere.patientprofile.form.patient_name", locale) + "\n\n" 
                                        + Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale) + "\n\n"
                                        + Common.getLocalizedString("pixalere.patientprofile.form.date_of_birth", locale) + "\n\n"
                                        + Common.getLocalizedString("pixalere.patientprofile.form.phn_id", locale));
                                String strPHN = "";
                                String strPHNDecrypted = "";
                                if (pat.getPhn() != null && pat.getPhn().length() > 0) // It can be empty for old accounts or out of province
                                {
                                    strPHNDecrypted = pat.getPhn();
                                    if (pat.getOutofprovince().intValue() == 1) {
                                        strPHN = strPHNDecrypted;
                                    } else {
                                        if (strPHNDecrypted.length() == 10) {
                                            strPHN = strPHNDecrypted.substring(0, 4) + " "
                                                    + strPHNDecrypted.substring(4, 7) + " "
                                                    + strPHNDecrypted.substring(7, 10);
                                        }
                                    }
                                }
                                patientFile.setRecordInfoData("\n\n\n\n\n\n" + pat.getLastName() + ", " + pat.getFirstName() + "\n\n"
                                        + pat.getPatient_id() + "\n\n"
                                        + pat.getDateOfBirth() + "\n\n" + strPHN);

                                /*} else {
                                 patientFile.setRecordInfoTitle("\n\n\n\n\n\n" + Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id") + "\n\n" + Common.getLocalizedString("pixalere.patientprofile.form.date_of_birth"));
                                 patientFile.setRecordInfoData("\n\n\n\n\n\n" + pat.getPatient_id() + "\n\n" +
                                 convertToFixedLength(prf.getDob_month(), 2) + "/" + convertToFixedLength(prf.getDob_day(), 2) + "/" + convertToFixedLength(prf.getDob_year(), 4));
                                 }  */
                            } else {
                                patientFile.setRecordInfoTitle("\n\n\n\n\n\n" + Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale) + "\n\n" + Common.getLocalizedString("pixalere.patientprofile.form.date_of_birth", locale) + "\n\n" + removeSubString(removeSubString(Common.getLocalizedString("pixalere.patientsearch.error.assign", locale) + " " + Common.getConfig("treatmentLocationName"), "<li>"), "</li>"));
                                patientFile.setRecordInfoData("\n\n\n\n\n\n" + pat.getPatient_id() + "\n\n" + pat.getDateOfBirth());
                            }
                            vecResultset.add(patientFile);
                        }
                    }
                }
                if (!strIncludeDetails.equals("pci")) {
                    LimbBasicAssessmentVO critLAB = new LimbBasicAssessmentVO();
                    critLAB.setPatient_id(pat.getPatient_id());
                    critLAB.setActive(1);
                    Collection<LimbBasicAssessmentVO> lcolLAB = null;
                    if (strIncludeDetails.equals("pci")) {
                        lcolLAB = Pmanager.getAllBasicLimbAssessments(critLAB, 1);
                    } else {
                        lcolLAB = Pmanager.getAllBasicLimbAssessments(critLAB, 0);
                    }
                    Iterator iterLAB = lcolLAB.iterator();
                    Collection<LimbBasicAssessmentVO> itemsLAB = new ArrayList();
                    while (iterLAB.hasNext() && blnAccessOK == true) {
                        LimbBasicAssessmentVO limbAssessment = (LimbBasicAssessmentVO) iterLAB.next();
                        Date intLastUpdate = null;
                        if (limbAssessment.getCreated_on() != null) {
                            intLastUpdate = limbAssessment.getCreated_on();
                        }
                        if (intEpochStartDate == null || intLastUpdate == null || (intLastUpdate.after(intEpochStartDate) && intLastUpdate.before(intEpochEndDate))) {
                            Collection<LimbBasicAssessmentVO> tmp = new ArrayList();
                            tmp.add(limbAssessment);
                            RowData[] vecLimbAssessment = Pmanager.getAllBasicLimbAssessmentsForFlowchart(tmp, userVO, false);
                            for (int intRecs = 0; intRecs < vecLimbAssessment.length; intRecs++) {
                                strProfileUpdate = vecLimbAssessment[intRecs].getUser_signature();
                                String strLimbAssessmentTitle = Common.getLocalizedString("pixalere.patientprofile.form.limb_assessment", locale) + ": " + strProfileUpdate;
                                for (int intFields = 2; intFields < vecLimbAssessment[intRecs].getFields().size(); intFields++) {
                                    List<FieldValues> fields = vecLimbAssessment[intRecs].getFields();
                                    if (blnAccessOK == true) {
                                        patientFile = new PatientFile();
                                        patientFile.setRecordLevel(3); // Limb Assessment
                                        patientFile.setRowNumber(convertToFixedDigits(intFields, 3));
                                        patientFile.setPatientId(pat.getPatient_id());
                                        patientFile.setWoundProfile(Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale) + ": " + pat.getPatient_id() + "   " + Common.getLocalizedString("pixalere.patientprofile.form.patient_name", locale) + ": " + pat.getPatientName());
                                        patientFile.setWoundProfileId("0000000000");
                                        patientFile.setWoundProfileRec("0000000000");
                                        patientFile.setAlpha("");
                                        patientFile.setAlphaTitle("");
                                        patientFile.setAssessment(strLimbAssessmentTitle);
                                        patientFile.setAssessmentId(convertToFixedDigits(limbAssessment.getId(), 10));
                                        patientFile.setImagesRowCount(intImagesRowCount);
                                        patientFile.setRowTitle(fields.get(intFields).getTitle());
                                        patientFile.setRowData(fields.get(intFields).getValue());
                                        vecResultset.add(patientFile);
                                        strLimbAssessmentTitle = Common.getLocalizedString("pixalere.patientprofile.form.limb_assessment", locale) + ": " + strProfileUpdate + " (" + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                    }
                                }
                            }
                        }
                    }
                    FootAssessmentVO critFA = new FootAssessmentVO();
                    critFA.setPatient_id(pat.getPatient_id());
                    critFA.setActive(1);
                    Collection<FootAssessmentVO> lcolFA = null;
                    if (strIncludeDetails.equals("pci")) {
                        lcolFA = Pmanager.getAllFootAssessments(critFA, 1);
                    } else {
                        lcolFA = Pmanager.getAllFootAssessments(critFA, 0);
                    }
                    Iterator iterFA = lcolFA.iterator();
                    Collection<FootAssessmentVO> itemsFA = new ArrayList();
                    while (iterFA.hasNext() && blnAccessOK == true) {
                        FootAssessmentVO footAssessment = (FootAssessmentVO) iterFA.next();
                        Date intLastUpdate = null;
                        if (footAssessment.getCreated_on() != null) {
                            intLastUpdate = footAssessment.getCreated_on();
                        }
                        if (intEpochStartDate == null || intLastUpdate == null || (intLastUpdate.after(intEpochStartDate) && intLastUpdate.before(intEpochEndDate))) {
                            Collection<FootAssessmentVO> tmp = new ArrayList();
                            tmp.add(footAssessment);
                            RowData[] vecFootAssessment = Pmanager.getAllFootAssessmentsForFlowchart(tmp, userVO, false);
                            for (int intRecs = 0; intRecs < vecFootAssessment.length; intRecs++) {
                                strProfileUpdate = vecFootAssessment[intRecs].getUser_signature();
                                String strFootAssessmentTitle = Common.getLocalizedString("pixalere.patientprofile.form.foot_assessment", locale) + ": " + strProfileUpdate;
                                for (int intFields = 2; intFields < vecFootAssessment[intRecs].getFields().size(); intFields++) {
                                    List<FieldValues> fields = vecFootAssessment[intRecs].getFields();
                                    if (blnAccessOK == true) {
                                        patientFile = new PatientFile();
                                        patientFile.setRecordLevel(4); // Foot Assessment
                                        patientFile.setRowNumber(convertToFixedDigits(intFields, 3));
                                        patientFile.setPatientId(pat.getPatient_id());
                                        patientFile.setWoundProfile(Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale) + ": " + pat.getPatient_id() + "   " + Common.getLocalizedString("pixalere.patientprofile.form.patient_name", locale) + ": " + pat.getPatientName());
                                        patientFile.setWoundProfileId("0000000000");
                                        patientFile.setWoundProfileRec("0000000000");
                                        patientFile.setAlpha("");
                                        patientFile.setAlphaTitle("");
                                        patientFile.setAssessment(strFootAssessmentTitle);
                                        patientFile.setAssessmentId(convertToFixedDigits(footAssessment.getId(), 10));
                                        patientFile.setImagesRowCount(intImagesRowCount);
                                        patientFile.setRowTitle(fields.get(intFields).getTitle());
                                        patientFile.setRowData(fields.get(intFields).getValue());
                                        vecResultset.add(patientFile);
                                        strFootAssessmentTitle = Common.getLocalizedString("pixalere.patientprofile.form.foot_assessment", locale) + ": " + strProfileUpdate + " (" + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                    }
                                }
                            }
                        }
                    }
                }
                // First get all current woundprofiles to check on updated during daterange before finding each wound historical profile
                com.pixalere.wound.bean.WoundProfileVO woundProfile1 = new com.pixalere.wound.bean.WoundProfileVO();
                woundProfile1.setPatient_id(pat.getPatient_id());
                woundProfile1.setCurrent_flag(1);
                Collection lcol3 = null;
                lcol3 = woundProfileDB.findAllByCriteria(woundProfile1, 0); // Multiple records
                Iterator iter3 = lcol3.iterator();
                while (iter3.hasNext() && blnAccessOK == true) {
                    WoundProfileVO wprfc = (WoundProfileVO) iter3.next();
                    Date intLastUpdate = null;
                    if (wprfc.getCreated_on() != null) {
                        intLastUpdate = wprfc.getCreated_on();
                    }
                    if (intEpochStartDate == null || intLastUpdate == null || (intLastUpdate.after(intEpochStartDate) && intLastUpdate.before(intEpochEndDate)) || (intLastUpdate.before(intEpochStartDate) && checkWoundIsOpen(wprfc.getWound().getStatus()))) {
                        String strStartDate = wprfc.getWound().getStartdate_signature();
                        if (strStartDate.length() > 0) {
                            strStartDate = strStartDate + " -"; // Add dash to make sure...
                            strStartDate = strStartDate.substring(1, strStartDate.indexOf(" -"));
                            strStartDate = strStartDate.substring(strStartDate.indexOf(" "), strStartDate.length());
                        }
                        strProfile = Common.getLocalizedString("pixalere.patientprofile.form.pixalere_id", locale) + ": " + pat.getPatient_id() + "   " + Common.getLocalizedString("pixalere.patientprofile.form.patient_name", locale) + ": " + pat.getPatientName() + "\n" + Common.getLocalizedString("pixalere.woundprofile.form.title", locale) + ": " + wprfc.getWound().getWound_location_detailed() + "\n" + Common.getLocalizedString("pixalere.woundprofile.form.start_date", locale) + ": " + strStartDate;
                        // There has been an update during the daterange, or their were no updates while wound was "open";
                        // now get blue man image and all assessments during daterange
                        // Create blue man image
                        patientFile = new PatientFile();
                        patientFile.setRecordLevel(5); // Wound Profile blue man
                        patientFile.setRowNumber("000");
                        patientFile.setPatientId(pat.getPatient_id());
                        patientFile.setWoundProfile(strProfile);
                        patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                        patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));//"0000000000");

                        patientFile.setBluemodelImage(composeBluemodelImage(wprfc.getWound_id(), false)); // Only saved alphas
                        patientFile.setAlpha("");
                        patientFile.setAlphaTitle("");
                        patientFile.setAssessment(Common.getLocalizedString("pixalere.woundprofile.form.title", locale) + ": " + wprfc.getUser_signature());
                        patientFile.setAssessmentId("0000000000");
                        patientFile.setImagesRowCount(intImagesRowCount);
                        patientFile.setRowTitle("");
                        patientFile.setRowData("");
                        vecResultset.add(patientFile);
                        // Get all wound assessments for this wound
                        com.pixalere.assessment.bean.WoundAssessmentVO woundAssessment = new com.pixalere.assessment.bean.WoundAssessmentVO();
                        woundAssessment.setPatient_id(pat.getPatient_id());
                        woundAssessment.setWound_id(wprfc.getWound_id());
                        woundAssessment.setActive(1);
                        Collection lcol5 = null;
                        if (strIncludeDetails.equals("pci")) {
                            lcol5 = woundAssessmentDB.findAllByCriteriaForPatientFile(woundAssessment);
                        } else {
                            lcol5 = woundAssessmentDB.findAllByCriteria(woundAssessment); // Gives in descending order; latest assessment first
                        }
                        Iterator iter5 = lcol5.iterator();
                        int assessCount = 0;
                        while (iter5.hasNext()) {
                            assessCount++;

                            WoundAssessmentVO was = (WoundAssessmentVO) iter5.next();
                            String strAssessment = Common.getLocalizedString("pixalere.woundassessment.form.title", locale) + ": " + was.getUser_signature();
                            //AssessmentCommentsVO[] comments = was.getComments();// slowing up assess pages
                            //AssessmentRecommendationsVO[] recommendations = was.getRecommendations();
                            AssessmentCommentsVO c = new AssessmentCommentsVO();
                            c.setAssessment_id(was.getId());
                            NursingCarePlanVO n = new NursingCarePlanVO();
                            n.setAssessment_id(was.getId());
                            DressingChangeFrequencyVO d = new DressingChangeFrequencyVO();
                            d.setAssessment_id(was.getId());
                            TreatmentCommentVO t = new TreatmentCommentVO();
                            t.setAssessment_id(was.getId());
                            Collection<TreatmentCommentVO> treatmentcomment = tmanager.getAllTreatmentComments(t, 0);
                            Collection<NursingCarePlanVO> nursingcareplan = nmanager.getAllNursingCarePlans(n, 0);
                            Collection<DressingChangeFrequencyVO> dcfs = nmanager.getAllDressingChangeFrequencies(d, 0);
                            Collection<AssessmentCommentsVO> comments = assessmentCommentsDB.findAllByCriteria(c);
                            intLastUpdate = null;
                            if (was.getCreated_on() != null) {
                                intLastUpdate = was.getCreated_on();;
                            }
                            if (intEpochStartDate == null || intLastUpdate == null || (intLastUpdate.after(intEpochStartDate) && intLastUpdate.before(intEpochEndDate))) {

                                int intAlphaAssessmentCount = 0;
                                // See: AssessmentFlowchartSetupAction
                                AssessmentServiceImpl AEWmanager = new AssessmentServiceImpl(language);
                                AssessmentEachwoundVO aew = new AssessmentEachwoundVO();
                                // get all images for this assessment
                                Vector vecImages = new Vector();
                                Collection lcol6 = null;

                                /////////////////////////////////////////////////////////////////////
                                // Begin for IHA and others:
                                /////////////////////////////////////////////////////////////////////
                                AssessmentImagesVO ig = new AssessmentImagesVO();
                                ig.setAssessment_id(was.getId());
                                lcol6 = iservice.getAllImages(ig);

                                /////////////////////////////////////////////////////////////////////
                                // End for IHA and others
                                /////////////////////////////////////////////////////////////////////
                                /////////////////////////////////////////////////////////////////////
                                // Begin for FHA:
                                /////////////////////////////////////////////////////////////////////
                                //AssessmentImagesManagerBD imagesBD=new AssessmentImagesManagerBD();
                                //lcol6 = imagesBD.retrieveImages(was.getId()+"");
                                /////////////////////////////////////////////////////////////////////
                                // End for FHA
                                /////////////////////////////////////////////////////////////////////

                                Iterator iter6 = lcol6.iterator();
                                while (iter6.hasNext()) {
                                    AssessmentImagesVO aim = (AssessmentImagesVO) iter6.next();
                                    // First gather all images for this assessment
                                    if (aim != null && !aim.getImages().equals("") && !aim.getImages().equals("a:0:{}")) {

                                        
                                                
                                        String vecImagesTmp =  aim.getImages();
                                        //for (int intImages = 0; intImages < vecImagesTmp.size(); intImages++) {

                                            String strAlpha = " ";
                                            if (aim.getAlpha_id() != null && aim.getAlpha_id() > 0) // If image is for specific Alpha
                                            {
                                                WoundAssessmentLocationVO woundAssessmentLocation1 = new WoundAssessmentLocationVO();
                                                woundAssessmentLocation1.setWound_id(wprfc.getWound_id());
                                                woundAssessmentLocation1.setPatient_id(pat.getPatient_id());
                                                woundAssessmentLocation1.setId(aim.getAlpha_id());
                                                Collection lcol7 = null;
                                                lcol7 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation1, 1, false, "");//4th parameter (extra sort) is ignored
                                                Iterator iter7 = lcol7.iterator();
                                                if (iter7.hasNext()) {
                                                    WoundAssessmentLocationVO wal1 = (WoundAssessmentLocationVO) iter7.next();
                                                    strAlpha = wal1.getAlpha();
                                                }
                                            }
                                            vecImages.add(strAlpha + "||" + vecImagesTmp);//add all images to vector
                                       //}
                                    }
                                }
                                int intImageCount = 0;
                                int intAlphaCount = 0;
                                while (vecImages != null && intImageCount < vecImages.size()) { // For all images found for this assessment
                                    String strAlpha = " ";
                                    //if (intAlphaCount > 0) {System.out.println("IntAlphaCount"+(char) (64 + intAlphaCount));
                                    //    strAlpha = (char) (64 + intAlphaCount) + "";
                                    //}
                                    Vector vecImagesThisAlpha = new Vector(); // Build list of images for this Alpha
                                    for (int intImages = 0; intImages < vecImages.size(); intImages++) {
                                        strAlpha = ((String) vecImages.get(intImages)).substring(0, ((String) vecImages.get(intImages)).indexOf("||"));
                                        
                                        if ((vecImages.get(intImages) + "").indexOf(strAlpha) == 0) {
                                            vecImagesThisAlpha.add((vecImages.get(intImages) + "").substring(((String) vecImages.get(intImages)).indexOf("||") + 2, ((String) vecImages.get(intImages)).length()));
                                        }
                                    }
                                    if (vecImagesThisAlpha.size() > 0) {
                                        patientFile = new PatientFile();
                                        patientFile.setRecordLevel(7); // Images and assessments
                                        patientFile.setRowNumber("000");
                                        patientFile.setPatientId(pat.getPatient_id());
                                        patientFile.setWoundProfile(strProfile);
                                        patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                                        patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));
                                        patientFile.setAssessment(strAssessment);
                                        patientFile.setAssessmentId(convertToFixedDigits(was.getId(), 10));
                                        intImagesRowCount++;
                                        patientFile.setImagesRowCount(intImagesRowCount);
                                        patientFile.setAlpha(strAlpha);
                                        patientFile.setAlphaTitle("");
                                        patientFile.setRowTitle("");
                                        patientFile.setRowData("");
                                        String strImagePath = absolutepath + "/" + pat.getPatient_id() + "/" + was.getId() + "/";
                                        int intImagePosCount = 0;
                                        for (int intImages = 0; intImages < vecImagesThisAlpha.size(); intImages++) {
                                            {
                                                intImagePosCount++;
                                                if (intImagePosCount == 5) // If this record contains max images
                                                {
                                                    vecResultset.add(patientFile);
                                                    intImagePosCount = 1;
                                                }
                                                if (intImagePosCount == 1) // New record with images
                                                {
                                                    patientFile = new PatientFile();
                                                    patientFile.setRecordLevel(7); // Images and assessments
                                                    patientFile.setRowNumber("000");
                                                    patientFile.setPatientId(pat.getPatient_id());
                                                    patientFile.setWoundProfile(strProfile);
                                                    patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                                                    patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));
                                                    patientFile.setAssessment(strAssessment);
                                                    patientFile.setAssessmentId(convertToFixedDigits(was.getId(), 10));
                                                    intImagesRowCount++;
                                                    patientFile.setImagesRowCount(intImagesRowCount);
                                                    patientFile.setAlpha(strAlpha);
                                                    if (intAlphaCount > 0) // If images for specific Alpha
                                                    {
                                                        patientFile.setAlphaTitle(Common.getAlphaText(strAlpha, locale));
                                                    } else {
                                                        patientFile.setAlphaTitle("");
                                                    }
                                                    patientFile.setRowTitle("");
                                                    patientFile.setRowData("");
                                                }
                                                if (intImagePosCount == 1) {
                                                    patientFile.setImage1(strImagePath + (vecImagesThisAlpha.get(intImages) + ""));
                                                }
                                                if (intImagePosCount == 2) {
                                                    patientFile.setImage2(strImagePath + (vecImagesThisAlpha.get(intImages) + ""));
                                                }
                                                if (intImagePosCount == 3) {
                                                    patientFile.setImage3(strImagePath + (vecImagesThisAlpha.get(intImages) + ""));
                                                }
                                                if (intImagePosCount == 4) {
                                                    patientFile.setImage4(strImagePath + (vecImagesThisAlpha.get(intImages) + ""));
                                                }
                                                intImageCount++;
                                            }
                                        }
                                        vecResultset.add(patientFile);
                                        strAssessment = Common.getLocalizedString("pixalere.woundassessment.form.title", locale) + ": " + was.getUser_signature() + " (" + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                    }
                                    intAlphaCount++;
                                }
                                WoundAssessmentLocationVO woundAssessmentLocation2 = new WoundAssessmentLocationVO();
                                woundAssessmentLocation2.setWound_id(wprfc.getWound_id());
                                woundAssessmentLocation2.setPatient_id(pat.getPatient_id());
                                Collection lcol8 = null;
                                lcol8 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation2, 0, false, ""); // Gives in descending order
                                Iterator iter8 = lcol8.iterator();
                                Vector vecAlphas = new Vector();
                                while (iter8.hasNext()) {
                                    WoundAssessmentLocationVO wal2 = (WoundAssessmentLocationVO) iter8.next();

                                    RowData[] rowData = null;
                                    if (wal2.getWound_profile_type().getAlpha_type().equals(Constants.WOUND_PROFILE_TYPE)) {
                                        AssessmentEachwoundVO crit = new AssessmentEachwoundVO();
                                        crit.setAssessment_id(was.getId());
                                        crit.setAlpha_id(wal2.getId());
                                        crit.setActive(new Integer(1));
                                        Collection<AssessmentEachwoundVO> items = null;
                                        items = AEWmanager.getAllAssessments(crit, 1, true);
                                        rowData = AEWmanager.getAllAssessmentsForFlowchart(items, userVO, false);
                                    }
                                    if (wal2.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE)) {
                                        AssessmentOstomyVO crit = new AssessmentOstomyVO();
                                        crit.setAssessment_id(was.getId());
                                        crit.setAlpha_id(wal2.getId());
                                        crit.setActive(new Integer(1));
                                        Collection<AssessmentOstomyVO> items = null;
                                        items = AEWmanager.getAllAssessments(crit, 1, true);
                                        rowData = AEWmanager.getAllOstomyAssessmentsForFlowchart(items, userVO, false);
                                    }
                                    if (wal2.getWound_profile_type().getAlpha_type().equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                                        AssessmentIncisionVO crit = new AssessmentIncisionVO();
                                        crit.setAssessment_id(was.getId());
                                        crit.setAlpha_id(wal2.getId());
                                        crit.setActive(new Integer(1));
                                        Collection<AssessmentIncisionVO> items = null;
                                        items = AEWmanager.getAllAssessments(crit, 1, true);
                                        rowData = AEWmanager.getAllIncisionAssessmentsForFlowchart(items, userVO, false);
                                    }
                                    if (wal2.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                                        AssessmentDrainVO crit = new AssessmentDrainVO();
                                        crit.setAssessment_id(was.getId());
                                        crit.setAlpha_id(wal2.getId());
                                        crit.setActive(new Integer(1));
                                        Collection<AssessmentDrainVO> items = null;
                                        items = AEWmanager.getAllAssessments(crit, 1, true);
                                        rowData = AEWmanager.getAllDrainAssessmentsForFlowchart(items, userVO, false);
                                    }
                                    if (wal2.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE)) {
                                        AssessmentSkinVO crit = new AssessmentSkinVO();
                                        crit.setAssessment_id(was.getId());
                                        crit.setAlpha_id(wal2.getId());
                                        crit.setActive(new Integer(1));
                                        Collection<AssessmentSkinVO> items = null;
                                        items = AEWmanager.getAllAssessments(crit, 1, true);
                                        rowData = AEWmanager.getAllSkinAssessmentsForFlowchart(items, userVO, false);
                                    }
                                    if (wal2.getWound_profile_type().getAlpha_type().equals(Constants.BURN_PROFILE_TYPE)) {
                                        AssessmentBurnVO crit = new AssessmentBurnVO();
                                        crit.setAssessment_id(was.getId());
                                        crit.setAlpha_id(wal2.getId());
                                        crit.setActive(new Integer(1));
                                        Collection<AssessmentBurnVO> items = null;
                                        items = AEWmanager.getAllAssessments(crit, 1, true);
                                        rowData = AEWmanager.getAllBurnAssessmentsForFlowchart(items, userVO, false);
                                    }
                                    if (rowData != null) {
                                        String strAlphaTitle = Common.getAlphaText(wal2.getAlpha(), locale);
                                        for (int intRecs = 0; intRecs < rowData.length; intRecs++) {
                                            for (int intFields = 2; intFields < rowData[intRecs].getFields().size(); intFields++) {
                                                List<FieldValues> fields = rowData[intRecs].getFields();
                                                patientFile = new PatientFile();
                                                patientFile.setRecordLevel(7); // Images and assessments
                                                patientFile.setRowNumber(convertToFixedDigits(intFields, 3));
                                                patientFile.setPatientId(pat.getPatient_id());
                                                patientFile.setWoundProfile(strProfile);
                                                patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                                                patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));
                                                patientFile.setAssessment(strAssessment);
                                                patientFile.setAssessmentId(convertToFixedDigits(was.getId(), 10));
                                                patientFile.setImagesRowCount(intImagesRowCount);
                                                patientFile.setAlpha(Common.getAlphaText(wal2.getAlpha(), locale));
                                                patientFile.setAlphaTitle(strAlphaTitle);
                                                patientFile.setRowTitle(fields.get(intFields).getTitle());
                                                // Remove tags from products in Nursing fixes and other... ehhh... html codes
                                                String strRowData = removeSubString(fields.get(intFields).getValue() + "", "<option>");
                                                strRowData = removeSubString(strRowData, "</option>");
                                                strRowData = removeEndString(strRowData, "<br/>");
                                                if (strRowData != null) {
                                                    strRowData = replaceSubString(strRowData, "<br/>", "\n");
                                                }
                                                if (strRowData != null) {
                                                    strRowData = removeSubString(strRowData, "<br");
                                                }
                                                patientFile.setRowData(strRowData);
                                                vecResultset.add(patientFile);
                                                strAssessment = Common.getLocalizedString("pixalere.woundassessment.form.title", locale) + 
                                                        ": " + 
                                                        was.getUser_signature() + 
                                                        " (" + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                                strAlphaTitle = Common.getAlphaText(wal2.getAlpha(), locale) + " (" + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                            }
                                        }
                                    }
                                    intAlphaAssessmentCount++;
                                }
                                // Get comments

                                String strAlphaTitle = Common.getLocalizedReportString("pixalere.reports.patient_file.comment_and_recommendation", locale);
                                boolean blnCommentsAdded = false;
                                for (AssessmentCommentsVO acm : comments) {
                                    blnCommentsAdded = true;
                                    patientFile = new PatientFile();
                                    patientFile.setRecordLevel(8); // Comments and recommendations
                                    patientFile.setRowNumber("000");
                                    patientFile.setPatientId(pat.getPatient_id());
                                    patientFile.setWoundProfile(strProfile);
                                    patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                                    patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));
                                    patientFile.setAssessment(strAssessment);
                                    patientFile.setAssessmentId(convertToFixedDigits(was.getId(), 10));
                                    patientFile.setImagesRowCount(0);
                                    patientFile.setAlpha("");
                                    patientFile.setAlphaTitle(strAlphaTitle);
                                    patientFile.setRowTitle(Common.getLocalizedString("pixalere.viewer.form.comment_body", locale));
                                    patientFile.setRowData(acm.getUser_signature() + ": " + acm.getBody());
                                    vecResultset.add(patientFile);
                                    strAlphaTitle = Common.getLocalizedReportString("pixalere.reports.patient_file.comment_and_recommendation", locale) + 
                                            " (" 
                                            + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                }

                                strAlphaTitle = Common.getLocalizedReportString("pixalere.reports.patient_file.treatment_comments", locale);;
                                for (TreatmentCommentVO tc : treatmentcomment) {
                                    patientFile = new PatientFile();
                                    patientFile.setRecordLevel(9); // Comments and recommendations
                                    patientFile.setRowNumber("000");
                                    patientFile.setPatientId(pat.getPatient_id());
                                    patientFile.setWoundProfile(strProfile);
                                    patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                                    patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));
                                    patientFile.setAssessment(strAssessment);
                                    patientFile.setAssessmentId(convertToFixedDigits(was.getId(), 10));
                                    patientFile.setImagesRowCount(0);
                                    patientFile.setAlpha("");
                                    patientFile.setAlphaTitle(strAlphaTitle);
                                    patientFile.setRowTitle(Common.getLocalizedString("pixalere.treatment.form.treatment_comments", locale));
                                    patientFile.setRowData(tc.getUser_signature() + ": " + tc.getBody());
                                    vecResultset.add(patientFile);
                                    //strAlphaTitle = "Comment and recommendation (" + Common.getLocalizedString("pixalere.reporting.form.continued") + ")";
                                }
                                strAlphaTitle = Common.getLocalizedReportString("pixalere.reports.patient_file.nursing_care_plans", locale);
                                for (NursingCarePlanVO ncp : nursingcareplan) {
                                    if (ncp != null) {
                                        patientFile = new PatientFile();
                                        patientFile.setRecordLevel(9); // Comments and recommendations
                                        patientFile.setRowNumber("000");
                                        patientFile.setPatientId(pat.getPatient_id());
                                        patientFile.setWoundProfile(strProfile);
                                        patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                                        patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));
                                        patientFile.setAssessment(strAssessment);
                                        patientFile.setAssessmentId(convertToFixedDigits(was.getId(), 10));
                                        patientFile.setImagesRowCount(0);
                                        patientFile.setAlpha("");
                                        patientFile.setAlphaTitle(strAlphaTitle);
                                        patientFile.setRowTitle(Common.getLocalizedString("pixalere.viewer.form.nursing_care_plan", locale));
                                        patientFile.setRowData(ncp.getUser_signature() + 
                                                ": " + ncp.getBody() + 
                                                "\n" + 
                                                Common.getLocalizedString("pixalere.viewer.form.visit_frequency", locale) + 
                                                ": " + 
                                                (ncp.getVisitFrequency() != null ? ncp.getVisitFrequency().getName(language) : ""));
                                        vecResultset.add(patientFile);
                                    }
                                    //strAlphaTitle = "Comment and recommendation (" + Common.getLocalizedString("pixalere.reporting.form.continued") + ")";
                                }
                                strAlphaTitle = Common.getLocalizedReportString("pixalere.reports.patient_file.dressing_change_frequencies", locale);
                                for (DressingChangeFrequencyVO dcf : dcfs) {
                                    patientFile = new PatientFile();
                                    patientFile.setRecordLevel(9); // Comments and recommendations
                                    patientFile.setRowNumber("000");
                                    patientFile.setPatientId(pat.getPatient_id());
                                    patientFile.setWoundProfile(strProfile);
                                    patientFile.setWoundProfileId(convertToFixedDigits(wprfc.getWound_id(), 10));
                                    patientFile.setWoundProfileRec(convertToFixedDigits(wprfc.getId(), 10));
                                    patientFile.setAssessment(strAssessment);
                                    patientFile.setAssessmentId(convertToFixedDigits(was.getId(), 10));
                                    patientFile.setImagesRowCount(0);
                                    patientFile.setAlpha("");
                                    patientFile.setAlphaTitle(strAlphaTitle);
                                    patientFile.setRowTitle(Common.getLocalizedString("pixalere.viewer.form.dressing_change_frequency", locale));
                                    patientFile.setRowData(dcf.getUser_signature() + ": " + dcf.getDressingChangeFrequency().getName(language));
                                    vecResultset.add(patientFile);
                                    //strAlphaTitle = "Comment and recommendation (" + Common.getLocalizedString("pixalere.reporting.form.continued") + ")";
                                }
                            }
                        }
                        // Retrieve all wound profile updates during the daterange
                        com.pixalere.wound.bean.WoundProfileVO woundProfile2 = new com.pixalere.wound.bean.WoundProfileVO();
                        woundProfile2.setPatient_id(pat.getPatient_id());
                        woundProfile2.setWound_id(wprfc.getWound_id());
                        woundProfile2.setActive(1);
                        if (strIncludeDetails.equals("pci")) {
                            woundProfile2.setCurrent_flag(1);
                        }
                        Collection lcol4 = null;
                        lcol4 = woundProfileDB.findAllByCriteria(woundProfile2, 0); // Multiple records, newest first
                        Iterator iter4 = lcol4.iterator();
                        boolean blnRecentWP = false;
                        while (iter4.hasNext() && blnAccessOK == true) {
                            WoundProfileVO wprf = (WoundProfileVO) iter4.next();
                            intLastUpdate = null;
                            if (wprf.getCreated_on() != null) {
                                intLastUpdate = wprf.getCreated_on();
                                // If no profile updates within daterange, include last update before start daterange
                            }
                            if (intEpochStartDate == null || intLastUpdate == null || (intLastUpdate.after(intEpochStartDate) && intLastUpdate.before(intEpochEndDate)) || (blnRecentWP == false && intLastUpdate.before(intEpochStartDate))) {
                                if (intEpochStartDate == null || intLastUpdate == null || intLastUpdate.before(intEpochStartDate)) {
                                    blnRecentWP = true;
                                }
                                WoundServiceImpl Wmanager = new WoundServiceImpl(language);
                                Collection items = new ArrayList();
                                items.add(wprf);
                                RowData[] vecWoundProfiles = Wmanager.getAllWoundProfilesForFlowchart(items, userVO, false,true);
                                //@todo need to finish refactoring
                                String strWoundProfileTitle = Common.getLocalizedString("pixalere.woundprofile.form.title", locale) + ": " + wprf.getUser_signature();
                                for (int intRecs = 0; intRecs < vecWoundProfiles.length; intRecs++) {
                                    for (int intFields = 2; intFields < vecWoundProfiles[intRecs].getFields().size(); intFields++) {
                                        List<FieldValues> fields = vecWoundProfiles[intRecs].getFields();
                                        patientFile = new PatientFile();
                                        patientFile.setRecordLevel(6); // Wound Profile
                                        patientFile.setRowNumber(convertToFixedDigits(intFields, 3));
                                        patientFile.setPatientId(pat.getPatient_id());
                                        patientFile.setWoundProfile(strProfile);
                                        patientFile.setWoundProfileId(convertToFixedDigits(wprf.getWound_id(), 10));
                                        patientFile.setWoundProfileRec(convertToFixedDigits(wprf.getId(), 10));
                                        patientFile.setAlpha("");
                                        patientFile.setAlphaTitle("");
                                        patientFile.setAssessment(strWoundProfileTitle);
                                        patientFile.setAssessmentId("0000000000");
                                        patientFile.setImagesRowCount(intImagesRowCount);
                                        patientFile.setRowTitle(fields.get(intFields).getTitle());
                                        patientFile.setRowData(fields.get(intFields).getValue());
                                        vecResultset.add(patientFile);
                                        strWoundProfileTitle = Common.getLocalizedString("pixalere.woundprofile.form.title", locale) + ": " + wprf.getUser_signature() + " (" + Common.getLocalizedString("pixalere.reporting.form.continued", locale) + ")";
                                        //end of for
                                    }//end of for
                                }//end of if
                            }//end of while
                        }//end of if
                    }//end of while
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                PatientFile sortRec1 = (PatientFile) vecResultset.get(intSort2);
                PatientFile sortRec2 = (PatientFile) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getWoundProfileId() + sortRec1.getRecordLevel() + sortRec1.getWoundProfileRec() + sortRec1.getAssessmentId() + sortRec1.getAlpha() + sortRec1.getRowNumber()).compareTo(
                        (sortRec2.getWoundProfileId() + sortRec2.getRecordLevel() + sortRec2.getWoundProfileRec() + sortRec2.getAssessmentId() + sortRec2.getAlpha() + sortRec2.getRowNumber())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }

// List
        //System.out.println("getWoundProfileId();getRecordLevel();getWoundProfileRec();getAssessmentId();getAlpha();getAlphaTitle();getRowTitle();getRowData()");
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            PatientFile sortRec1 = (PatientFile) vecResultset.get(intSort1);
        }

        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }
    // REPORT 9: Discharged wounds \\

    public Vector generateDischargedWounds(String strTypeOfGrouping, Date intEpochStartDate, Date intEpochEndDate) {
        // strTypeOfGrouping = "None", "Treatment Location", "Treatment Area", "Service Area"
        Vector vecResultset = new Vector();
        com.pixalere.common.service.ListServiceImpl listBD = new com.pixalere.common.service.ListServiceImpl(language);
        try {
            reportDAO.connect();
            int intWoundClosed = Integer.parseInt(Common.getConfig("woundClosed")); // Closed Alpha's
            com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
            patientAccount.setCurrent_flag(1);  // Only current account records
            Collection lcol1 = null;
            lcol1 = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter1 = lcol1.iterator();
            while (iter1.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter1.next();
                // Only patients that are in a treatment location that was selected
                if ((pat.getPatient_id() != null) && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    com.pixalere.patient.bean.PatientProfileVO patientProfile = new com.pixalere.patient.bean.PatientProfileVO();
                    patientProfile.setCurrent_flag(1);
                    patientProfile.setPatient_id(pat.getPatient_id());
                    patientProfile.setActive(1);
                    Collection lcol2 = null;
                    lcol2 = patientProfileDB.findAllByCriteria(patientProfile, 1); // Max. 1 record
                    Iterator iter2 = lcol2.iterator();
                    while (iter2.hasNext()) {
                        PatientProfileVO prf = (PatientProfileVO) iter2.next();
                        // First check if patient existed before end of daterange
                        if (intEpochStartDate == null || (prf.getStart_date() != null && prf.getStart_date().before(intEpochEndDate))) {
                            String strTreatmentLocation = "";
                            String strGrouping = "";
                            int intTip = 0;
                            if (pat.getTreatment_location_id() != null) {
                                if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                                    if (!strTypeOfGrouping.equalsIgnoreCase("none")) {
                                        strGrouping = Common.getLocalizedString("pixalere.TIP", "en");
                                        intTip = 1;
                                    }
                                } else {
                                    LookupVO vo = pat.getTreatmentLocation();
                                    strTreatmentLocation = vo.getName(language);
                                    if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                                        strGrouping = Common.getConfig("treatmentLocationName");
                                    }
                                    if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                                        LookupVO lookupVO2 = pat.getTreatmentLocation();
                                        //ResourcesVO res = resourcesBD.getResource(lookupVO2.getResourceId());
                                        strGrouping = Common.getConfig("treatmentCatName");
                                    }
                                    if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                                        if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                            strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                                    strTreatmentLocation.indexOf(")"));
                                            strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                                        } else {
                                            strGrouping = "";
                                            intTip = 1; // List at end of report
                                        }
                                    }
                                }
                                com.pixalere.wound.bean.WoundProfileVO woundProfile = new com.pixalere.wound.bean.WoundProfileVO();
                                // Only latest version of wound profile
                                woundProfile.setCurrent_flag(1);
                                woundProfile.setPatient_id(pat.getPatient_id());
                                Collection lcol3 = null;
                                lcol3 = woundProfileDB.findAllByCriteria(woundProfile, 0); // Multiple records
                                Iterator iter3 = lcol3.iterator();
                                while (iter3.hasNext()) {
                                    WoundProfileVO wprf = (WoundProfileVO) iter3.next();
                                    // Do only wounds that were open at some point during timeframe
                                    if (intEpochStartDate == null
                                            || (wprf.getWound().getStart_date() != null
                                            && (wprf.getWound().getStart_date().before(intEpochEndDate))
                                            && !(!checkWoundIsOpen(wprf.getWound().getStatus()) && (wprf.getWound().getClosed_date() == null || wprf.getWound().getClosed_date().before(intEpochStartDate))))) {
                                        String strEtiology = "";
                                        if (wprf.getEtiologies().size() > 0) {
                                            for (EtiologyVO et : wprf.getEtiologies()) {
                                                strEtiology = strEtiology + " " + et.getLookup().getName(language);
                                            }
                                        }
                                        /*if (wprf.getWound_etiology_other().length()>0) {
                                         if (strEtiology.length()>0) strEtiology=strEtiology+", ";
                                         strEtiology=strEtiology+wprf.getWound_etiology_other();
                                         }*/
                                        if (strEtiology.length() == 0) {
                                            strEtiology = "Not given";
                                        }
                                        Vector vecResultsPerPatient = new Vector();
                                        // Count wound assessments for this wound from table wound_assessment
                                        com.pixalere.assessment.bean.WoundAssessmentVO woundAssessment = new com.pixalere.assessment.bean.WoundAssessmentVO();
                                        woundAssessment.setPatient_id(pat.getPatient_id());
                                        woundAssessment.setWound_id(wprf.getWound_id());
                                        Collection lcol5 = null;
                                        lcol5 = woundAssessmentDB.findAllByCriteria(woundAssessment); // Descending order; latest assessment first
                                        Iterator iter5 = lcol5.iterator();
                                        while (iter5.hasNext()) {
                                            WoundAssessmentVO was = (WoundAssessmentVO) iter5.next();
                                            // Check only assessments made before end of the timeframe
                                            if (intEpochStartDate == null
                                                    || (was.getCreated_on().after(intEpochStartDate)) && (was.getCreated_on().before(intEpochEndDate))) {
                                                // Count wound assessments per alpha for this wound from assessment_eachwound
                                                com.pixalere.assessment.bean.AssessmentEachwoundVO assessmentEachwound = new com.pixalere.assessment.bean.AssessmentEachwoundVO();
                                                assessmentEachwound.setActive(1); // Only those that were saved from Summary
                                                assessmentEachwound.setActive(intWoundClosed); // Closed Alpha's
                                                assessmentEachwound.setAssessment_id(was.getId());
                                                Collection lcol6 = null;
                                                lcol6 = assessmentEachwoundDB.findAllByCriteriaDescending(assessmentEachwound);
                                                Iterator iter6 = lcol6.iterator();
                                                while (iter6.hasNext()) {
                                                    AssessmentEachwoundVO aew = (AssessmentEachwoundVO) iter6.next();
                                                    String strReason = Common.getListValue(aew.getDischarge_reason().intValue(), language);
                                                    if (strReason.length() == 0) {
                                                        strReason = "Not given";
                                                    } else {
                                                        if (aew.getDischarge_other() != null && aew.getDischarge_other().length() > 0) {
                                                            strReason = strReason + ": " + aew.getDischarge_other();
                                                        }
                                                    }
                                                    // First store values as Float, to get result of division with decimals and allow rounding
                                                    float fltHealDaysFromStart = 0;
                                                    if (was.getCreated_on() != null && wprf.getWound().getStart_date() != null) {
                                                        fltHealDaysFromStart = PDate.getEpochTime(was.getCreated_on()) - PDate.getEpochTime(wprf.getWound().getStart_date());
                                                    }
                                                    // Add one to the result, to count both start date and end date as full
                                                    int intHealTime = (Math.round(fltHealDaysFromStart / fltSecondsPerDay) + 1);
                                                    int intPatientCount = 0;
                                                    // Count patient only first time for this combination of etiology and reason
                                                    if (!vecResultsPerPatient.contains(strEtiology + strReason)) {
                                                        vecResultsPerPatient.add(strEtiology + strReason);
                                                        intPatientCount = 1;
                                                    }
                                                    // Count assessments and visits for this closed alpha
                                                    com.pixalere.assessment.bean.AssessmentEachwoundVO assessmentEachwound2 = new com.pixalere.assessment.bean.AssessmentEachwoundVO();
                                                    assessmentEachwound2.setActive(1); // Only those that were saved from Summary
                                                    assessmentEachwound2.setAlpha_id(aew.getAlpha_id());
                                                    int intVisitsCount = 0;
                                                    int intAssessments = 0;
                                                    Vector vecVisits = new Vector();
                                                    Collection lcol7 = null;
                                                    lcol7 = assessmentEachwoundDB.findAllByCriteriaDescending(assessmentEachwound2);
                                                    Iterator iter7 = lcol7.iterator();
                                                    while (iter7.hasNext()) {
                                                        AssessmentEachwoundVO aew2 = (AssessmentEachwoundVO) iter7.next();
                                                        intAssessments++;
                                                        com.pixalere.assessment.bean.WoundAssessmentVO woundAssessment2 = new com.pixalere.assessment.bean.WoundAssessmentVO();
                                                        woundAssessment2.setId(aew2.getAssessment_id());
                                                        Collection lcol8 = null;
                                                        lcol8 = woundAssessmentDB.findAllByCriteria(woundAssessment2);
                                                        Iterator iter8 = lcol8.iterator();
                                                        if (iter8.hasNext()) {
                                                            WoundAssessmentVO was2 = (WoundAssessmentVO) iter8.next();
                                                            Date intTimeThisAssess = was2.getCreated_on();
                                                            boolean blnVisit = true;
                                                            int intVisits = 0;
                                                            while (blnVisit && intVisits < vecVisits.size()) {
                                                                int intCheckVisit = (Integer) vecVisits.get(intVisits);
                                                                if (Math.abs(intCheckVisit - Integer.parseInt(PDate.getEpochTime(intTimeThisAssess) + "")) < 7200) {
                                                                    blnVisit = false;
                                                                }
                                                                intVisits++;
                                                            }
                                                            if (blnVisit) {
                                                                intVisitsCount++;
                                                                vecVisits.add(intTimeThisAssess);
                                                            }
                                                        }
                                                    }
                                                    DischargedWounds dischargedWounds = new DischargedWounds();
                                                    dischargedWounds.setTip(intTip);
                                                    dischargedWounds.setGrouping(strGrouping);
                                                    dischargedWounds.setEtiology(strEtiology);
                                                    dischargedWounds.setReason(strReason);
                                                    dischargedWounds.setPatientCount(intPatientCount);
                                                    dischargedWounds.setAlphaClosedCount(1);
                                                    dischargedWounds.setAssessmentsCount(intAssessments);
                                                    dischargedWounds.setVisitsCount(intVisitsCount);
                                                    dischargedWounds.setHealTime(intHealTime);
                                                    vecResultset.add(dischargedWounds);
                                                } // All AssessmentEachwounds which are closed
                                            } // Wound assessments within timeframe
                                        } // Wound assessments loop
                                    } // Wounds opened before end of timeframe
                                } // Wound profile loop
                            } // Patient with valid treatment location
                        } // Patient Profile existed before end of daterange
                    } // Patient Profile loop
                } // If user has access to treatment location
            } // Patient Accounts loop
        } catch (Exception ex) {
            ex.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        // Prevent from showing "Null" on report
        if (vecResultset.size() == 0) {
            DischargedWounds dischargedWounds = new DischargedWounds();
            dischargedWounds.setReason("");
            vecResultset.add(dischargedWounds);
        }
        Vector vecSortedResultsets = new Vector();
        // Sort for main report
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                DischargedWounds sortRec1 = (DischargedWounds) vecResultset.get(intSort2);
                DischargedWounds sortRec2 = (DischargedWounds) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getReason() + sortRec1.getEtiology()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getReason() + sortRec2.getEtiology())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }

    /**
     * @todo legacy, need to rewrite.
     * @param strTypeOfGrouping
     * @param intEpochStartDate
     * @param intEpochEndDate
     * @return
     */
    public Vector generateHealTimeAndCost(String strTypeOfGrouping, Date intEpochStartDate, Date intEpochEndDate) {
        System.out.println("REPORT 10: Heal time and cost per Etiology");
        Vector vecResultset = new Vector();
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        patientAccount.setCurrent_flag(1);  // Only current account records
        Collection lcol0 = null;
        try {
            reportDAO.connect();
            int intWoundClosed = Integer.parseInt(Common.getConfig("woundClosed")); // Closed Alpha's
            boolean blnAllChecked = false;
            Vector vecEtiology = new Vector();
            if (etiology != null) {
                for (int intEtiology = 0; intEtiology < etiology.length; intEtiology++) {
                    if (!etiology[intEtiology].equals("0")) {
                        Integer intEtiologyID = Integer.parseInt(etiology[intEtiology]);
                        
                        LookupVO vo = listBD.getListItem(intEtiologyID);
                        if (vo != null && vo.getName(language) != null) {
                            
                            vecEtiology.add(vo.getName(language).trim().toLowerCase());
                        }
                    } else {
                        blnAllChecked = true;
                    }
                }
            }
            lcol0 = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter0 = lcol0.iterator();
            while (iter0.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter0.next();
                // Only patients that are in a treatment location that was selected
                if ((pat.getPatient_id() != null)
                        && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    String strTreatmentLocation = "";
                    String strGrouping = "";
                    int intTip = 0;
                    int intCostCat = 0;
                    if (pat.getTreatment_location_id() != null) {
                        if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                            if (!strTypeOfGrouping.equalsIgnoreCase("none")) {
                                intTip = 1;
                                strGrouping = Common.getLocalizedString("pixalere.TIP", "en");
                            }
                        } else {
                            LookupVO vo = pat.getTreatmentLocation();
                            strTreatmentLocation = vo.getName(language);
                            int intItemFilter = Integer.parseInt(vo.getUpdate_access());
                            if (intItemFilter > 0) {
                                intCostCat = intItemFilter;
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                                strGrouping = Common.getConfig("treatmentLocationName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                                ResourcesVO res = vo.getResource();
                                strGrouping = Common.getConfig("treatmentCatName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                                if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                    strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                            strTreatmentLocation.indexOf(")"));
                                    strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                                } else {
                                    strGrouping = "";
                                    intTip = 1; // List at end of report
                                }
                            }
                        }
                        WoundVO woundvo = new WoundVO();
                        woundvo.setPatient_id(pat.getPatient_id());
                        Collection lcol2 = null;
                        lcol2 = woundProfileDB.findAllWoundsByCriteria(woundvo, 0);
                        Iterator iter2 = lcol2.iterator();
                        while (iter2.hasNext()) {
                            WoundVO wnd = (WoundVO) iter2.next();
                            try {
                                // Only woundprofiles that were open during timeframe
                                if (intEpochEndDate == null || intEpochStartDate == null
                                        || (wnd.getStart_date() != null && wnd.getStart_date().before(intEpochEndDate))
                                        && (wnd.getClosed_date() == null || (wnd.getClosed_date() != null) && wnd.getClosed_date().after(intEpochStartDate))) {
                                    WoundAssessmentLocationVO woundAssessmentlocation = new WoundAssessmentLocationVO();
                                    woundAssessmentlocation.setWound_id(wnd.getId());
                                    woundAssessmentlocation.setActive(1); // Only submitted alpha's
                                    Collection lcol3 = null;
                                    lcol3 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentlocation, 0, false, "");//4th parameter (extra sort) is ignored
                                    Iterator iter3 = lcol3.iterator();
                                    
                                    while (iter3.hasNext()) {
                                        WoundAssessmentLocationVO wal = (WoundAssessmentLocationVO) iter3.next();
                                        Date intCloseDate = null;
                                        Boolean blnOstomyHold = false;
                                        if (wal.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE)
                                                && wal.getDischarge() == 0) {
                                            com.pixalere.assessment.bean.AssessmentOstomyVO assessmentcrit = new com.pixalere.assessment.bean.AssessmentOstomyVO();
                                            assessmentcrit.setWound_id(wnd.getId());
                                            assessmentcrit.setAlpha_id(wal.getId());
                                            AssessmentDAO woundAssessmentDAO = new AssessmentDAO();
                                            AbstractAssessmentVO ass = new AbstractAssessmentVO();
                                            ass = (AssessmentOstomyVO) woundAssessmentDAO.findLastActiveAssessment(assessmentcrit, true);
                                            String strStatus = "0";
                                            if (ass != null && ass.getStatus() != null) {
                                                strStatus = Integer.toString(ass.getStatus());
                                            }
                                            if (strStatus.equals(Common.getConfig("ostomyHold"))) {
                                                intCloseDate = ass.getWoundAssessment().getCreated_on();
                                                blnOstomyHold = (intCloseDate.before(intEpochEndDate) || intEpochEndDate == null)
                                                        && (intCloseDate.after(intEpochStartDate) || intEpochStartDate == null);
                                            }
                                        }
                                        
                                        // Check if alpha is open, or (if closed) was closed during timerange
                                        if (blnOstomyHold || ((wal.getDischarge() == 1 && wal.getClose_timestamp() != null) && (wal.getClose_timestamp().before(intEpochEndDate) || intEpochEndDate == null) && (wal.getClose_timestamp().after(intEpochStartDate) || intEpochStartDate == null))) {
                                            WoundProfileVO woundprofilevo2 = new WoundProfileVO();
                                            woundprofilevo2.setWound_id(wnd.getId());
                                            Collection lcol4 = null;
                                            lcol4 = woundProfileDB.findAllByCriteria(woundprofilevo2, 0);
                                            Iterator iter4 = lcol4.iterator();
                                            boolean blnEtiologyMatch = false;
                                            String strAlphaAndEtiology = "";
                                            String strAlphaEtiology = "";
                                           
                                           
                                            while (iter4.hasNext() && !blnEtiologyMatch) {
                                                WoundProfileVO wprf = (WoundProfileVO) iter4.next();
                                                if (wprf.getEtiologies().size() > 0) {
                                                    for (EtiologyVO et : wprf.getEtiologies()) {
                                                        if (et.getAlpha_id().equals(wal.getId()) && vecEtiology.contains(et.getLookup().getName(language).toLowerCase())) {
                                                            strAlphaEtiology = et.getLookup().getName(language);
                                                            blnEtiologyMatch = true;
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            if (blnEtiologyMatch || blnAllChecked) {
                                                // Get all wound assessments for this alpha
                                                float fltTotalProductCost = 0.0F;
                                                AssessmentProductVO assessmentProductVO = new AssessmentProductVO();
                                                assessmentProductVO.setAlpha_id(wal.getId());
                                                assessmentProductVO.setActive(1);
                                                Collection lcol5 = null;
                                                lcol5 = assessmentEachwoundDB.findAllProductsByCriteria(assessmentProductVO);
                                                for (Iterator iter5 = lcol5.iterator(); iter5.hasNext();) {
                                                    AssessmentProductVO apr = (AssessmentProductVO) iter5.next();
                                                    try {
                                                        if (apr.getProduct_id() != -1) {
                                                            // Get cost based on care location (community or acute)
                                                            String strCost = "0.0";
                                                            strCost = apr.getProduct().getCost() + "";

                                                            float fltCost = Float.parseFloat(strCost);
                                                            float fltQuantity = 0.0F;
                                                            if (apr.getQuantity() != null && apr.getQuantity().length() > 0) {
                                                                fltQuantity = Float.valueOf(apr.getQuantity()).floatValue();
                                                            }
                                                            //System.out.println(apr.getProduct().getId()+" "+apr.getProduct().getCost()+ " "+fltQuantity+" "+fltCost);
                                                            if (fltQuantity > 0 && fltCost > 0) {
                                                                
                                                                fltTotalProductCost = fltTotalProductCost + fltQuantity * fltCost;
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        System.out.println("Exception in calculate total products cost: " + e);
                                                    }
                                                }
                                                //System.out.println("Cost: "+lcol5.size()+" "+fltTotalProductCost);
                                                HealTimeAndCostClosedWounds healtimeandcostclosedwounds = new HealTimeAndCostClosedWounds();
                                                healtimeandcostclosedwounds.setTip(intTip);
                                                healtimeandcostclosedwounds.setGrouping(strTreatmentLocation);
                                                healtimeandcostclosedwounds.setTreatmentLocation(strTreatmentLocation);
                                                healtimeandcostclosedwounds.setEtiology(strAlphaEtiology);
                                               
                                                healtimeandcostclosedwounds.setCareType(Common.getWoundProfileType(wal.getWound_profile_type().getAlpha_type(), locale));
                                                Collection colAssessments = null;
                                                if (wal.getWound_profile_type().getAlpha_type().equals(Constants.WOUND_PROFILE_TYPE)) {
                                                    healtimeandcostclosedwounds.setCareTypeSort(1);
                                                    AssessmentEachwoundVO assessmentEachwoundVO = new AssessmentEachwoundVO();
                                                    assessmentEachwoundVO.setWound_id(wal.getWound_id());
                                                    assessmentEachwoundVO.setAlpha_id(wal.getId());
                                                    colAssessments = assessmentEachwoundDB.findLastAssessments(assessmentEachwoundVO);
                                                }
                                                if (wal.getWound_profile_type().getAlpha_type().equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                                                    healtimeandcostclosedwounds.setCareTypeSort(2);
                                                    AssessmentIncisionVO assessmentIncisionVO = new AssessmentIncisionVO();
                                                    assessmentIncisionVO.setWound_id(wal.getWound_id());
                                                    assessmentIncisionVO.setAlpha_id(wal.getId());
                                                    colAssessments = assessmentEachwoundDB.findLastAssessments(assessmentIncisionVO);
                                                }
                                                if (wal.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                                                    healtimeandcostclosedwounds.setCareTypeSort(3);
                                                    AssessmentDrainVO assessmentDrainVO = new AssessmentDrainVO();
                                                    assessmentDrainVO.setWound_id(wal.getWound_id());
                                                    assessmentDrainVO.setAlpha_id(wal.getId());
                                                    colAssessments = assessmentEachwoundDB.findLastAssessments(assessmentDrainVO);
                                                }
                                                if (wal.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE)) {
                                                    healtimeandcostclosedwounds.setCareTypeSort(4);
                                                    AssessmentOstomyVO assessmentOstomyVO = new AssessmentOstomyVO();
                                                    assessmentOstomyVO.setWound_id(wal.getWound_id());
                                                    assessmentOstomyVO.setAlpha_id(wal.getId());
                                                    colAssessments = assessmentEachwoundDB.findLastAssessments(assessmentOstomyVO);
                                                }
                                                if (wal.getWound_profile_type().getAlpha_type().equals(Constants.BURN_PROFILE_TYPE)) {
                                                    healtimeandcostclosedwounds.setCareTypeSort(4);
                                                    AssessmentBurnVO assessmentOstomyVO = new AssessmentBurnVO();
                                                    assessmentOstomyVO.setWound_id(wal.getWound_id());
                                                    assessmentOstomyVO.setAlpha_id(wal.getId());
                                                    colAssessments = assessmentEachwoundDB.findLastAssessments(assessmentOstomyVO);
                                                }
                                                if (wal.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE)) {
                                                    healtimeandcostclosedwounds.setCareTypeSort(4);
                                                    AssessmentSkinVO assessmentOstomyVO = new AssessmentSkinVO();
                                                    assessmentOstomyVO.setWound_id(wal.getWound_id());
                                                    assessmentOstomyVO.setAlpha_id(wal.getId());
                                                    colAssessments = assessmentEachwoundDB.findLastAssessments(assessmentOstomyVO);
                                                }
                                                healtimeandcostclosedwounds.setAlphaClosedCount(1);
                                                healtimeandcostclosedwounds.setAssessmentCount(colAssessments.size());
                                                /////////////////////////////////
                                                if (intCloseDate == null) {
                                                    intCloseDate = wal.getClose_timestamp();
                                                }
                                                Date intStartDate = wal.getOpen_timestamp();
                                                if (intCloseDate != null && intStartDate != null) {
                                                    // First store values as Float, to get result of division with decimals and allow rounding
                                                    float fltHealDaysFromStart = PDate.getEpochTime(intCloseDate) - PDate.getEpochTime(intStartDate);
                                                    // Add one to the result, to count both start date and end date as full
                                                    healtimeandcostclosedwounds.setHealDaysFromStart(Math.round(fltHealDaysFromStart / fltSecondsPerDay) + 1);
                                                }
                                                healtimeandcostclosedwounds.setTotalCost(fltTotalProductCost);
                                                vecResultset.add(healtimeandcostclosedwounds);
                                            }

                                        }
                                    }
                                }
                            } catch (Exception exception) {
                                System.out.println("EXCEPTION: " + exception);
                                exception.printStackTrace();
                            }
                        }
                    }
                }
            }
        } catch (Exception exception) {
            System.out.println("EXCEPTION: " + exception);
            exception.printStackTrace();
            reportDAO.disconnect();
        } finally {
            reportDAO.disconnect();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort for main report: Treatment Location, Patient ID (required for grouping)
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                HealTimeAndCostClosedWounds sortRec1 = (HealTimeAndCostClosedWounds) vecResultset.get(intSort2);
                HealTimeAndCostClosedWounds sortRec2 = (HealTimeAndCostClosedWounds) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getCareTypeSort() + sortRec1.getEtiology()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getCareTypeSort() + sortRec2.getEtiology())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        // Sort for subreport: Treatment Location, Wound Stage, Patient ID, Wound Profile
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                HealTimeAndCostClosedWounds sortRec1 = (HealTimeAndCostClosedWounds) vecResultset.get(intSort2);
                HealTimeAndCostClosedWounds sortRec2 = (HealTimeAndCostClosedWounds) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getCareTypeSort() + sortRec1.getEtiology()).compareTo((sortRec2.getCareTypeSort() + sortRec2.getEtiology())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }

    public Vector generateCostForPressureWounds(String strTypeOfGrouping, Date intEpochStartDate, Date intEpochEndDate) {
        System.out.println("REPORT 11: Cost for Pressure Wounds");
        Vector vecResultset = new Vector();
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        AssessmentServiceImpl amanager = new AssessmentServiceImpl(language);
        patientAccount.setCurrent_flag(1);  // Only current account records
        Collection lcol1 = null;
        try {
            lcol1 = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter1 = lcol1.iterator();
            while (iter1.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter1.next();
                // Only patients that are in a treatment location that was selected
                if ((pat.getPatient_id() != null) && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    String strTreatmentLocation = "";
                    String strGrouping = "";
                    int intTip = 0;
                    //if (pat.getTreatment_location_id()!=null && (pat.getTreatment_location_id()+"").length()>0) {
                    if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                        if (!strTypeOfGrouping.equalsIgnoreCase("none")) {
                            intTip = 1;
                            strGrouping = Common.getLocalizedString("pixalere.TIP", "en");
                        }
                    } else {
                        LookupVO vo = pat.getTreatmentLocation();
                        strTreatmentLocation = vo.getName(language);
                        if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                            strGrouping = Common.getConfig("treatmentLocationName");
                        }
                        if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                            //ResourcesVO res = resourcesBD.getResource(vo.getResourceId());
                            strGrouping = Common.getConfig("treatmentCatName");
                        }
                        if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                            if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                        strTreatmentLocation.indexOf(")"));
                                strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                            } else {
                                strGrouping = "";
                                intTip = 1; // List at end of report
                            }
                        }
                        //}
                        Vector vecPatientProfileDates = new Vector();
                        Vector vecPatientProfileCareLocs = new Vector();
                        boolean blnPatientCounted = false;
                        // First get the current wound profile, to see if this wp is within the daterange
                        WoundProfileVO woundProfile = new WoundProfileVO();
                        woundProfile.setPatient_id(pat.getPatient_id());
                        woundProfile.setCurrent_flag(1);
                        Collection locl3 = null;
                        locl3 = woundProfileDB.findAllByCriteria(woundProfile, 0);
                        Iterator iter3 = locl3.iterator();
                        while (iter3.hasNext()) {
                            WoundProfileVO wprf1 = (WoundProfileVO) iter3.next();
                            if ((wprf1.getPressure_ulcer() != 0)
                                    && ((intEpochStartDate == null)
                                    || (checkWoundIsOpen(wprf1.getWound().getStatus()) && wprf1.getCreated_on().before(intEpochEndDate)
                                    || (!checkWoundIsOpen(wprf1.getWound().getStatus()) && wprf1.getCreated_on().after(intEpochStartDate) && wprf1.getCreated_on().before(intEpochEndDate))))) {
                                int intWoundCount = 1;
                                String strPressureStageStart = "";
                                Date strWoundCloseDate = null;
                                if (vecPatientProfileDates.size() == 0) {
                                    com.pixalere.patient.bean.PatientProfileVO patientProfile = new com.pixalere.patient.bean.PatientProfileVO();
                                    patientProfile.setPatient_id(pat.getPatient_id());
                                    Collection lcol4 = null;
                                    lcol4 = patientProfileDB.findAllByCriteria(patientProfile, 0);
                                    Iterator iter4 = lcol4.iterator();
                                    int intThisProfileCareLoc = 0;
                                    while (iter4.hasNext()) {
                                        PatientProfileVO prf = (PatientProfileVO) iter4.next();
                                        if (Integer.parseInt(pat.getTreatmentLocation().getUpdate_access()) != intThisProfileCareLoc) {
                                            vecPatientProfileDates.add(prf.getCreated_on());
                                            vecPatientProfileCareLocs.add(Integer.parseInt(pat.getTreatmentLocation().getUpdate_access()));
                                            intThisProfileCareLoc = Integer.parseInt(pat.getTreatmentLocation().getUpdate_access());
                                        }
                                    }
                                }
                                float fltTotalCost = 0.0F; // For this wp
                                PressureWoundsCost pressureWoundsCost = new PressureWoundsCost();
                                // Retrieve same wp, but now all records and not only current
                                WoundProfileVO woundProfile2 = new WoundProfileVO();
                                woundProfile2.setWound_id(wprf1.getWound_id());
                                Collection locl5 = null;
                                locl5 = woundProfileDB.findAllByCriteria(woundProfile2, 0);
                                Iterator iter5 = locl5.iterator();
                                while (iter5.hasNext()) {
                                    WoundProfileVO wprf2 = (WoundProfileVO) iter5.next();
                                    if (!checkWoundIsOpen(wprf2.getWound().getStatus())) {
                                        strWoundCloseDate = wprf2.getCreated_on();                                    // Get all wound assessments for this WP (we need these records for the date)
                                    }
                                    com.pixalere.assessment.bean.WoundAssessmentVO woundAssessment = new com.pixalere.assessment.bean.WoundAssessmentVO();
                                    woundAssessment.setPatient_id(pat.getPatient_id());
                                    woundAssessment.setWound_id(wprf2.getWound_id());
                                    Collection lcol6 = null;
                                    lcol6 = woundAssessmentDB.findAllByCriteria(woundAssessment); // Gives in descending order; last assess first
                                    Iterator iter6 = lcol6.iterator();
                                    while (iter6.hasNext()) {
                                        WoundAssessmentVO was = (WoundAssessmentVO) iter6.next();
                                        int intPatientCareLoc = (Integer) vecPatientProfileCareLocs.get(0);
                                        if (vecPatientProfileCareLocs.size() > 1) {
                                            // Find the patient care location at time this assessment was made
                                            for (int intPP = 0; intPP < vecPatientProfileCareLocs.size(); intPP++) {
                                                if (((Date) vecPatientProfileDates.get(intPP)).before(was.getCreated_on())) {
                                                    intPatientCareLoc = (Integer) vecPatientProfileCareLocs.get(intPP);
                                                }
                                            }
                                        }
                                        AssessmentEachwoundVO assessmenteachwoundvo = new AssessmentEachwoundVO();
                                        assessmenteachwoundvo.setPatient_id(pat.getPatient_id());
                                        assessmenteachwoundvo.setWound_id(wprf1.getWound_id());
                                        assessmenteachwoundvo.setActive(1);
                                        Collection locl7 = null;
                                        locl7 = assessmentEachwoundDB.findAllByCriteriaDescending(assessmenteachwoundvo);
                                        for (Iterator iter7 = locl7.iterator(); iter7.hasNext();) {
                                            AssessmentEachwoundVO assessmenteachwoundvo1 = (AssessmentEachwoundVO) iter7.next();
                                            AssessmentProductVO tp = new AssessmentProductVO();
                                            tp.setAssessment_id(assessmenteachwoundvo1.getAssessment_id());
                                            tp.setAlpha_id(assessmenteachwoundvo1.getAlpha_id());
                                            Collection<AssessmentProductVO> products = amanager.getAllProducts(tp);
                                            fltTotalCost = fltTotalCost + getTotalProductCost(products, intPatientCareLoc);
                                        }
                                    }
                                }
                                LookupVO vo2 = listBD.getListItem(wprf1.getPressure_ulcer());
                                strPressureStageStart = vo2.getName(language).trim();
                                pressureWoundsCost.setTip(intTip);
                                pressureWoundsCost.setGrouping(strGrouping);
                                pressureWoundsCost.setTreatmentLocation(strTreatmentLocation);
                                if (!blnPatientCounted) {
                                    pressureWoundsCost.setPatientCount(1);
                                    blnPatientCounted = true;
                                } else {
                                    pressureWoundsCost.setPatientCount(0);
                                }
                                pressureWoundsCost.setWoundCount(1);
                                String strLastName = pat.getLastName();
                                String strFirstName = pat.getFirstName();
                                String strFullName = "";
                                if ((strLastName.trim() + strFirstName.trim()).length() > 0) {
                                    strFullName = strLastName + ", " + strFirstName;
                                }
                                pressureWoundsCost.setPatientAccountId(pat.getPatient_id());
                                pressureWoundsCost.setPatientNameFull(strFullName);
                                // pressureWoundsCost.setWoundProfile(wprf1.getWound_location());
                                pressureWoundsCost.setWoundProfile(wprf1.getWound().getWound_location_detailed());
                                pressureWoundsCost.setWoundStartDate(new PDate().getLocalizedDateString(wprf1.getWound().getStart_date(), Common.getLocaleFromLocaleString(locale)));
                                if (wprf1.getWound().getStart_date() != null) {
                                    pressureWoundsCost.setWoundStartTimeStamp(Integer.parseInt(PDate.getEpochTime(wprf1.getWound().getStart_date()) + "")); // For sorting
                                }
                                pressureWoundsCost.setWoundCloseDate(PDate.getEpochTime(strWoundCloseDate) + "");
                                pressureWoundsCost.setPressureStageStart(strPressureStageStart);
                                LookupVO vo3 = listBD.getListItem(wprf1.getPressure_ulcer());
                                pressureWoundsCost.setPressureStageEnd(vo3.getName(language).trim());
                                pressureWoundsCost.setTotalCost(fltTotalCost);
                                vecResultset.add(pressureWoundsCost);
                            }
                        }
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        Vector vecSortedResultsets = new Vector();
        // Sort: Treatment Location, Patient ID (required for grouping)
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                PressureWoundsCost sortRec1 = (PressureWoundsCost) vecResultset.get(intSort2);
                PressureWoundsCost sortRec2 = (PressureWoundsCost) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getPatientAccountId_sort() + sortRec1.getWoundProfile()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getPatientAccountId_sort() + sortRec2.getWoundProfile())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }

    public Vector generateCostPerPatient(String strTypeOfGrouping, Date intEpochStartDate, Date intEpochEndDate) {
        System.out.println("REPORT 12: Cost per Patient");
        Vector vecResultset = new Vector();
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        AssessmentServiceImpl amanager = new AssessmentServiceImpl(language);
        patientAccount.setCurrent_flag(1);  // Only current account records
        Collection lcol1 = null;
        try {
            lcol1 = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter1 = lcol1.iterator();
            while (iter1.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter1.next();
                // Only patients that are in a treatment location that was selected
                if ((pat.getPatient_id() != null) && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    String strTreatmentLocation = "";
                    String strGrouping = "";
                    int intTip = 0;
                    if (pat.getTreatment_location_id() != null) {
                        if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                            if (!strTypeOfGrouping.equalsIgnoreCase("none")) {
                                intTip = 1;
                                strGrouping = Common.getLocalizedString("pixalere.TIP", "en");
                                strTreatmentLocation = Common.getLocalizedString("pixalere.TIP", "en");
                            }
                        } else {
                            LookupVO vo = pat.getTreatmentLocation();
                            strTreatmentLocation = vo.getName(language);
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentLocation")) {
                                strGrouping = Common.getConfig("treatmentLocationName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("treatmentArea")) {
                                //ResourcesVO res = resourcesBD.getResource(vo.getResourceId());
                                strGrouping = Common.getConfig("treatmentCatName");
                            }
                            if (strTypeOfGrouping.equalsIgnoreCase("serviceArea")) {
                                if (strTreatmentLocation.contains("(") && strTreatmentLocation.contains(")")) {
                                    strGrouping = strTreatmentLocation.substring(strTreatmentLocation.indexOf("(") + 1,
                                            strTreatmentLocation.indexOf(")"));
                                    strTreatmentLocation = strTreatmentLocation.substring(0, strTreatmentLocation.indexOf("(")).trim();
                                } else {
                                    strGrouping = "";
                                    intTip = 1; // List at end of report
                                }
                            }
                        }
                        Vector vecPatientProfileDates = new Vector();
                        Vector vecPatientProfileCareLocs = new Vector();
                        boolean blnPatientCounted = false;
                        // First get the current wound profile, to see if this wp is within the daterange
                        WoundProfileVO woundProfile = new WoundProfileVO();
                        woundProfile.setPatient_id(pat.getPatient_id());
                        woundProfile.setCurrent_flag(1);
                        Collection locl3 = null;
                        locl3 = woundProfileDB.findAllByCriteria(woundProfile, 0);
                        Iterator iter3 = locl3.iterator();
                        while (iter3.hasNext()) {
                            WoundProfileVO wprf = (WoundProfileVO) iter3.next();
                            // Include wound profile:
                            // - if no daterange set, or
                            // - if startdate before or equal to end daterange, and not closed before begin daterange.
                            if (intEpochStartDate == null
                                    || (wprf.getWound().getStart_date() != null
                                    && (wprf.getWound().getStart_date()).before(intEpochEndDate))
                                    && !(!checkWoundIsOpen(wprf.getWound().getStatus()) && (wprf.getWound().getClosed_date() == null || wprf.getWound().getClosed_date().before(intEpochStartDate)))) {
                                int intWoundCount = 1;
                                String strWoundCloseDate = "";
                                if (vecPatientProfileDates.size() == 0) {
                                    com.pixalere.patient.bean.PatientProfileVO patientProfile = new com.pixalere.patient.bean.PatientProfileVO();
                                    patientProfile.setPatient_id(pat.getPatient_id());
                                    Collection lcol4 = null;
                                    lcol4 = patientProfileDB.findAllByCriteria(patientProfile, 0);
                                    Iterator iter4 = lcol4.iterator();
                                    int intThisProfileCareLoc = 0;
                                    while (iter4.hasNext()) {
                                        PatientProfileVO prf = (PatientProfileVO) iter4.next();
                                        if (Integer.parseInt(pat.getTreatmentLocation().getUpdate_access()) != intThisProfileCareLoc) {
                                            vecPatientProfileDates.add(prf.getCreated_on());
                                            vecPatientProfileCareLocs.add(Integer.parseInt(pat.getTreatmentLocation().getUpdate_access()));
                                            intThisProfileCareLoc = Integer.parseInt(pat.getTreatmentLocation().getUpdate_access());
                                        }
                                    }
                                }
                                float fltTotalCost = 0.0F; // For this wp
                                CostPerPatient costPerPatient = new CostPerPatient();
                                if (!checkWoundIsOpen(wprf.getWound().getStatus())) {
                                    strWoundCloseDate = PDate.getEpochTime(wprf.getWound().getClosed_date()) + "";
                                    // Get all wound assessments for this WP (we need these records for the date)
                                }
                                com.pixalere.assessment.bean.WoundAssessmentVO woundAssessment = new com.pixalere.assessment.bean.WoundAssessmentVO();
                                woundAssessment.setPatient_id(pat.getPatient_id());
                                woundAssessment.setWound_id(wprf.getWound_id());
                                Collection lcol6 = null;
                                lcol6 = woundAssessmentDB.findAllByCriteria(woundAssessment); // Gives in descending order; last assess first
                                Iterator iter6 = lcol6.iterator();
                                while (iter6.hasNext()) {
                                    WoundAssessmentVO was = (WoundAssessmentVO) iter6.next();
                                    int intPatientCareLoc = (Integer) vecPatientProfileCareLocs.get(0);
                                    if (vecPatientProfileCareLocs.size() > 1) {
                                        // Find the patient care location at time this assessment was made
                                        for (int intPP = 0; intPP < vecPatientProfileCareLocs.size(); intPP++) {
                                            if ((Integer) vecPatientProfileDates.get(intPP) <= PDate.getEpochTime(was.getCreated_on())) {
                                                intPatientCareLoc = (Integer) vecPatientProfileCareLocs.get(intPP);
                                            }
                                        }
                                    }
                                    AssessmentEachwoundVO assessmenteachwoundvo = new AssessmentEachwoundVO();
                                    assessmenteachwoundvo.setPatient_id(pat.getPatient_id());
                                    assessmenteachwoundvo.setWound_id(wprf.getWound_id());
                                    assessmenteachwoundvo.setAssessment_id(was.getId());
                                    assessmenteachwoundvo.setActive(1);
                                    Collection locl7 = null;
                                    locl7 = assessmentEachwoundDB.findAllByCriteriaDescending(assessmenteachwoundvo);
                                    for (Iterator iter7 = locl7.iterator(); iter7.hasNext();) {
                                        AssessmentEachwoundVO assessmenteachwoundvo1 = (AssessmentEachwoundVO) iter7.next();
                                        AssessmentProductVO tp = new AssessmentProductVO();
                                        tp.setAssessment_id(assessmenteachwoundvo1.getAssessment_id());
                                        tp.setAlpha_id(assessmenteachwoundvo1.getAlpha_id());
                                        Collection<AssessmentProductVO> products = amanager.getAllProducts(tp);
                                        fltTotalCost = fltTotalCost + getTotalProductCost(products, intPatientCareLoc);
                                    }
                                }
                                costPerPatient.setTip(intTip);
                                costPerPatient.setGrouping(strGrouping);
                                costPerPatient.setTreatmentLocation(strTreatmentLocation);
                                if (!blnPatientCounted) {
                                    costPerPatient.setPatientCount(1);
                                    blnPatientCounted = true;
                                } else {
                                    costPerPatient.setPatientCount(0);
                                }
                                costPerPatient.setWoundCount(1);
                                String strLastName = pat.getLastName();
                                String strFirstName = pat.getFirstName();
                                String strFullName = "";
                                if ((strLastName.trim() + strFirstName.trim()).length() > 0) {
                                    strFullName = strLastName + ", " + strFirstName;
                                }
                                costPerPatient.setPatientAccountId(pat.getPatient_id());
                                costPerPatient.setPatientNameFull(strFullName);
                                costPerPatient.setWoundProfile(wprf.getWound().getWound_location_detailed());

                                String strEtiology = "";
                                if (wprf.getEtiologies().size() > 0) {
                                    for (EtiologyVO et : wprf.getEtiologies()) {
                                        strEtiology = strEtiology + " " + et.getLookup().getName(language);
                                    }
                                }
                                /*if (wprf.getWound_etiology_other().length()>0) {
                                 if (strEtiology.length()>0) strEtiology=strEtiology+", ";
                                 strEtiology=strEtiology+wprf.getWound_etiology_other();
                                 }*/
                                if (strEtiology.length() == 0) {
                                    strEtiology = "Not given";
                                }
                                costPerPatient.setWoundEtiology(strEtiology);
                                costPerPatient.setWoundStartDate(new PDate().getLocalizedDateString(wprf.getWound().getStart_date(), Common.getLocaleFromLocaleString(locale)));
                                if (wprf.getWound().getStart_date() != null) {
                                    costPerPatient.setWoundStartTimeStamp(Integer.parseInt(PDate.getEpochTime(wprf.getWound().getStart_date()) + "")); // For sorting
                                }
                                costPerPatient.setWoundCloseDate(strWoundCloseDate);
                                costPerPatient.setTotalCost(fltTotalCost);
                                vecResultset.add(costPerPatient);
                            }
                        }
                    }
                }
            }
        } catch (Exception exception) {
            System.out.println("EXCEPTION");
            exception.printStackTrace();
        }
        // Prevent from showing empty report
        if (vecResultset.size() == 0) {
            CostPerPatient costPerPatient = new CostPerPatient();
            costPerPatient.setGrouping("");
            vecResultset.add(costPerPatient);
        }
        Vector vecSortedResultsets = new Vector();
        // Sort: Treatment Location, Patient ID (required for grouping)
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                CostPerPatient sortRec1 = (CostPerPatient) vecResultset.get(intSort2);
                CostPerPatient sortRec2 = (CostPerPatient) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getTip() + sortRec1.getGrouping() + sortRec1.getPatientAccountId_sort() + sortRec1.getWoundProfile()).compareTo(
                        (sortRec2.getTip() + sortRec2.getGrouping() + sortRec2.getPatientAccountId_sort() + sortRec2.getWoundProfile())) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }

    public Vector generateVisitsPerPatient(Date intEpochStartDate, Date intEpochEndDate) {
        System.out.println("REPORT 13: Visits per Patient");
        Vector vecResultset = new Vector();
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        patientAccount.setCurrent_flag(1);  // Only current account records
        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        Collection lcol1 = null;
        try {
            lcol1 = patientDB.findAllByCriteria(patientAccount, false);
            Iterator iter1 = lcol1.iterator();
            while (iter1.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter1.next();
                // Only patients that are in a treatment location that was selected
                if ((pat.getPatient_id() != null) && (getLocationAccess(pat.getTreatment_location_id() + "", treatment_location, includeTIP) == true)) {
                    String strTreatmentLocation = "";
                    int intTip = 0;
                    int intNoClinicians = 0;
                    int intPhysicalNurseVisits = 0;
                    int intPhysicalClinicianVisits = 0;
                    int intVirtualClinicianConsults = 0;
                    if (pat.getTreatment_location_id() != null) {
                        if ("-1".indexOf(pat.getTreatment_location_id() + "") > -1) {
                            intTip = 1;
                            strTreatmentLocation = "Transfer in Progress (TIP)";
                        } else {
                            LookupVO vo = pat.getTreatmentLocation();
                            strTreatmentLocation = vo.getName(language);
                        }
                        boolean blnWoundsClosed = false;
                        Vector vecWounds = new Vector();
                        Vector vecClinicians = new Vector();
                        Date intStartDate = null;
                        Date intEndDate = null;
                        WoundVO woundVO = new WoundVO();
                        woundVO.setPatient_id(pat.getPatient_id());
                        woundVO.setActive(1);
                        woundVO.setStatus("Closed");
                        Collection locl2 = null;
                        locl2 = woundProfileDB.findAllWoundsByCriteria(woundVO, 0);
                        Iterator iter2 = locl2.iterator();
                        while (iter2.hasNext()) {
                            WoundVO wound = (WoundVO) iter2.next();
                            // Include wound profile:
                            // - if no daterange set, or
                            // - if enddate within daterange.
                            Date intWoundCloseDate = null;
                            if (wound.getClosed_date() != null) {
                                intWoundCloseDate = wound.getClosed_date();
                            }
                            if (intEpochStartDate == null
                                    || (intWoundCloseDate != null
                                    && (intWoundCloseDate.before(intEpochEndDate))
                                    && (intWoundCloseDate.after(intEpochStartDate)))) {
                                blnWoundsClosed = true;
                                vecWounds.add(wound.getWound_location_detailed());
                                if (intWoundCloseDate.after(intEndDate)) {
                                    intEndDate = intWoundCloseDate;
                                }
                                Date intWoundOpenDate = null;
                                if (wound.getStart_date() != null) {
                                    intWoundOpenDate = wound.getStart_date();
                                    if (intStartDate == null || (intWoundOpenDate != null && intWoundOpenDate.before(intStartDate))) {
                                        intStartDate = intWoundOpenDate;
                                    }
                                }

                                //  ReferralsTrackingVO referrals = new ReferralsTrackingVO();
                                //  referrals.setType("Recommendation Complete");
                                //  referrals.setWound_id(wound.getWound_profile_id());
                                //  ReferralsTrackingDAO refDAO = new ReferralsTrackingDAO();
                                //  Collection lcol3 = null;
                                //  lcol3 = refDAO.findAllByCriteria(referrals);
                                //  Iterator iter3 = lcol3.iterator();
                                //  while (iter3.hasNext()) {
                                //      ReferralsTrackingVO ref = (ReferralsTrackingVO) iter3.next();
                                //      intVirtualClinicianConsults++;
                                //  if(!vecClinicians.contains(ref.getProfessional_id()))
                                //	   {
                                //       vecClinicians.add(ref.getProfessional_id());
                                //       }
                                //	}
                                // Count wound assessments for this wound from table wound_assessment
                                Vector iter4 = reportDAO.findAllWoundAssessments(pat.getPatient_id().intValue(), wound.getId().intValue(), 0, 0); // Descending order; latest assessment first
                                Vector vecVisits = new Vector();
                                for (int x = 0; x < iter4.size(); x++) {
                                    Object[] wound_assessment = (Object[]) iter4.get(x);
                                    int intTimeThisAssess = Integer.parseInt(((String) wound_assessment[0]).trim());
                                    boolean blnVisit = true;
                                    int intVisits = 0;
                                    while (blnVisit && intVisits < vecVisits.size()) {
                                        int intCheckVisit = (Integer) vecVisits.get(intVisits);
                                        if (Math.abs(intCheckVisit - intTimeThisAssess) < 7200) {
                                            blnVisit = false;
                                        }
                                        intVisits++;
                                    }
                                    if (blnVisit) {
                                        ProfessionalVO professional = userBD.getProfessional((Integer) wound_assessment[2]);
                                        boolean blnClinician = Common.isSpecialist(professional.getPosition());

                                        if (blnClinician == true) {
                                            intPhysicalClinicianVisits++;
                                            if (!vecClinicians.contains((Integer) wound_assessment[2])) {
                                                vecClinicians.add((Integer) wound_assessment[2]);
                                            }
                                        } else {
                                            intPhysicalNurseVisits++;
                                        }
                                        vecVisits.add(intTimeThisAssess);
                                    }
                                }

                            }
                        }
                        if (blnWoundsClosed) {
                            VisitsPerPatient visitsPerPatient = new VisitsPerPatient();
                            String strPatientId = Integer.toString(pat.getPatient_id());
                            while (strPatientId.length() < 10) {
                                strPatientId = "0" + strPatientId;
                            }
                            visitsPerPatient.setPatientId_sort(strPatientId);
                            visitsPerPatient.setPatientId(pat.getPatient_id());
                            visitsPerPatient.setTreatmentLocation(strTreatmentLocation);
                            // Build string of clincians responding
                            String strClinicians = "";
                            if (vecClinicians.size() == 0) {
                                intNoClinicians = 1;
                                strClinicians = "No visits or consults by ET";
                            } else {
                                intNoClinicians = 0;
                                Vector vecClinicianNames = new Vector();
                                // Get names
                                for (int intClinician = 0; intClinician < vecClinicians.size(); intClinician++) {
                                    int intProfessional = (Integer) vecClinicians.get(intClinician);
                                    ProfessionalVO clinician = userBD.getProfessional(intProfessional);
                                    vecClinicianNames.add(clinician.getFirstname() + " " + clinician.getLastname());
                                }
                                if (vecClinicians.size() > 1) {
                                    // Now sort by name
                                    for (int intSort1 = 0; intSort1 < vecClinicianNames.size(); intSort1++) {
                                        for (int intSort2 = 0; intSort2 < vecClinicianNames.size() - intSort1 - 1; intSort2++) {
                                            String strSortRec1 = vecClinicianNames.get(intSort2) + "";
                                            String strSortRec2 = vecClinicianNames.get(intSort2 + 1) + "";
                                            if (strSortRec1.compareTo(strSortRec2) > 0) {
                                                vecClinicianNames.setElementAt(strSortRec2, intSort2);
                                                vecClinicianNames.setElementAt(strSortRec1, intSort2 + 1);
                                            }
                                        }
                                    }
                                }
                                for (int intClinician = 0; intClinician < vecClinicianNames.size(); intClinician++) {
                                    if (!strClinicians.equals("")) {
                                        strClinicians = strClinicians + "/";
                                    }
                                    strClinicians = strClinicians + (String) vecClinicianNames.get(intClinician);
                                }
                            }
                            visitsPerPatient.setClinicians(strClinicians);
                            // Build string of all wounds
                            String strWounds = "";
                            if (vecWounds.size() > 0) {
                                if (vecWounds.size() > 1) { // Sort the wounds first
                                    for (int intSort1 = 0; intSort1 < vecWounds.size(); intSort1++) {
                                        for (int intSort2 = 0; intSort2 < vecWounds.size() - intSort1 - 1; intSort2++) {
                                            String strSortRec1 = vecWounds.get(intSort2) + "";
                                            String strSortRec2 = vecWounds.get(intSort2 + 1) + "";
                                            if (strSortRec1.compareTo(strSortRec2) > 0) {
                                                vecWounds.setElementAt(strSortRec2, intSort2);
                                                vecWounds.setElementAt(strSortRec1, intSort2 + 1);
                                            }
                                        }
                                    }
                                }
                                String strCurrentWound = "";
                                int intCurrentWound = 1;
                                for (int intWound = 0; intWound < vecWounds.size(); intWound++) {
                                    String strWound = (String) vecWounds.get(intWound);
                                    if (strWound.equalsIgnoreCase(strCurrentWound)) {
                                        intCurrentWound++;
                                    }
                                    // add to wounds string if new type of wound, or last wound in vector
                                    if (!strWound.equalsIgnoreCase(strCurrentWound) || intWound == vecWounds.size() - 1) {
                                        if (intCurrentWound > 1) {
                                            strWounds = strWounds + " (" + intCurrentWound + "X)";
                                            intCurrentWound = 1;
                                        }
                                        if (!strWound.equalsIgnoreCase(strCurrentWound)) {
                                            if (strWounds.length() > 0) {
                                                strWounds = strWounds + ", ";
                                            }
                                            strWounds = strWounds + strWound;
                                        }
                                    }
                                    strCurrentWound = strWound;
                                }
                            }
                            visitsPerPatient.setNoClinicians(intNoClinicians);
                            visitsPerPatient.setClinicians(strClinicians);
                            visitsPerPatient.setWoundProfiles(strWounds);
                            visitsPerPatient.setStartDate(new PDate().getLocalizedDateString(intStartDate, Common.getLocaleFromLocaleString(locale)));
                            visitsPerPatient.setEndDate(new PDate().getLocalizedDateString(intEndDate, Common.getLocaleFromLocaleString(locale)));
                            visitsPerPatient.setStartDateVal(intStartDate);
                            visitsPerPatient.setEndDateVal(intEndDate);
                            if (intStartDate != null && intEndDate != null && intEndDate.after(intStartDate)) {
                                float fltStartDate = PDate.getEpochTime(intStartDate);
                                float fltEndDate = PDate.getEpochTime(intEndDate);
                                visitsPerPatient.setProgramStayDays((Math.round((fltEndDate - fltStartDate) / 86400)) + 1);
                            } else {
                                visitsPerPatient.setProgramStayDays(0);
                            }
                            visitsPerPatient.setPhysicalNurseVisits(intPhysicalNurseVisits);
                            visitsPerPatient.setPhysicalClinicianVisits(intPhysicalClinicianVisits);
                            visitsPerPatient.setVirtualClinicianConsults(intVirtualClinicianConsults);
                            visitsPerPatient.setTotalPhysicalVisits(intPhysicalNurseVisits + intPhysicalClinicianVisits);
                            //  System.out.println("");
                            //  System.out.println("ID="+visitsPerPatient.getPatientId_sort());
                            //  System.out.println("TreatmentLocation="+visitsPerPatient.getTreatmentLocation());
                            //  System.out.println("Clinicians="+visitsPerPatient.getClinicians());
                            //  System.out.println("Wounds="+visitsPerPatient.getWoundProfiles());
                            //  System.out.println("Start="+visitsPerPatient.getStartDate());
                            //  System.out.println("End="+visitsPerPatient.getNamedDate());
                            //  System.out.println("Stay="+visitsPerPatient.getProgramStayDays());
                            //  System.out.println("PhysicalNurseVisits="+visitsPerPatient.getPhysicalNurseVisits());
                            //  System.out.println("PhysicalClinicianVisits="+visitsPerPatient.getPhysicalClinicianVisits());
                            //  System.out.println("VirtualClinicianConsults="+visitsPerPatient.getVirtualClinicianConsults());
                            //  System.out.println("TotalPhysicalVisits="+visitsPerPatient.getTotalPhysicalVisits());
                            vecResultset.add(visitsPerPatient);
                        }
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        // Prevent from showing empty report
        if (vecResultset.size() == 0) {
            VisitsPerPatient visitsPerPatient = new VisitsPerPatient();
            visitsPerPatient.setTreatmentLocation("");
            vecResultset.add(visitsPerPatient);
        }
        Vector vecSortedResultsets = new Vector();
        // Sort: Treatment Location, Patient ID (required for grouping)
        for (int intSort1 = 0; intSort1 < vecResultset.size(); intSort1++) {
            for (int intSort2 = 0; intSort2 < vecResultset.size() - intSort1 - 1; intSort2++) {
                VisitsPerPatient sortRec1 = (VisitsPerPatient) vecResultset.get(intSort2);
                VisitsPerPatient sortRec2 = (VisitsPerPatient) vecResultset.get(intSort2 + 1);
                if ((sortRec1.getNoClinicians() + sortRec1.getClinicians() + sortRec1.getTip() + sortRec1.getTreatmentLocation() + sortRec1.getPatientId_sort() + sortRec1.getWoundProfiles()).compareTo(
                        (sortRec2.getNoClinicians() + sortRec2.getClinicians() + (sortRec2.getTip() + sortRec2.getTreatmentLocation() + sortRec2.getPatientId_sort() + sortRec2.getWoundProfiles()))) > 0) {
                    vecResultset.setElementAt(sortRec2, intSort2);
                    vecResultset.setElementAt(sortRec1, intSort2 + 1);
                }
            }
        }
        vecSortedResultsets.add(vecResultset.toArray());
        return vecSortedResultsets;
    }

    public void callPatientFileReport(String filename, String patient_id) throws ServletException, IOException {
        try {
            Object[] objDataSource1 = null;
            Object[] objDataSource2 = null;
            String strJasperReportFilename = "";
            String strUserName = "Pixalere";
            String strGroupingHeader = "";
            String strFilterDetails = "";
            String strReportTitle = "";
            String strReportSubTitle = "";
            String strBreakDownTitle = "";
            String strReportDateRange = "";

            strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale) + new PDate().getLocalizedDateString(getIntEpochNow(), Common.getLocaleFromLocaleString(locale));

            Map parameters = new HashMap();

            // TODO: DELETE
            // strReportTitle = "Pixalere Patient File";
            strReportSubTitle = "Wound Management Plan";
            strJasperReportFilename = "wound_management_plan";
            int intPatient_id = 0;
            try {
                intPatient_id = Integer.parseInt(patient_id);
            } catch (Exception e) {
            }
            start_date = null;
            end_date = null;
            vecDataSource = generateWoundManagementPlan(intPatient_id);
  
            
            if (vecDataSource.size() >= 1) {
                objDataSource1 = (Object[]) vecDataSource.get(0);
            }
            if (vecDataSource.size() == 2) {
                objDataSource2 = (Object[]) vecDataSource.get(1);
                // Store the second object into the subreport parameter
                JRDataSource jrDataSource = new JRBeanArrayDataSource(objDataSource2); // For using the same data
                parameters.put("SubReportData", jrDataSource);
            }
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream("ApplicationResources.properties"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (objDataSource1 != null) {
                //need use start_date/end_date
                parameters.put("ReportDateRange", strReportDateRange); // For displaying date ranges etc.
                parameters.put("ReportUser", strUserName); // For footer.
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                parameters.put("ReportGenerationDate", getIntEpochNow().toString() + 
                        " " + convertToFixedLength(Integer.toString(calendar.get(Calendar.HOUR_OF_DAY)), 2) + 
                        ":" + convertToFixedLength(Integer.toString(calendar.get(Calendar.MINUTE)), 2)); // For footer
                // TODO: DELETE
                // parameters.put("ReportTitle", strReportTitle);
                parameters.put("ReportSubTitle", strReportSubTitle);
                parameters.put("ReportDateRange", strReportDateRange); // For displaying date ranges etc.
                parameters.put("ReportGroupingHeader", strGroupingHeader);
                parameters.put("SubReportPath", context.getRealPath("/reports/")+"/");
                parameters.put("ImagesPath", context.getRealPath("/images/")+"/");
                
                String strLocationsSelection = Common.getLocalizedReportString("pixalere.reports.common.locations_selection_all_full", locale);
                if (getPartialSelectedLocations(treatment_location, userVO.getRegions()) == true) {
                    strLocationsSelection = Common.getLocalizedReportString("pixalere.reports.common.locations_selection_full", locale);
                }
                parameters.put("ReportLocationsSelection", strLocationsSelection);

                try {
                    System.out.println("PATHTH"+context.getRealPath("/reports/") + "/"+strJasperReportFilename + ".jasper");
                    InputStream reportStream = null;
                    try{
                        reportStream = new FileInputStream(context.getRealPath("/reports/") + "/"+strJasperReportFilename + ".jasper");
                    System.out.println("PATHTH2"+context.getRealPath("/reports/") + "/"+strJasperReportFilename + ".jasper");
                    }catch(FileNotFoundException exe){
                         reportStream = new FileInputStream(context.getRealPath("/reports/") + "/"+strJasperReportFilename + ".jasper");
                    }
                    JasperPrint jasperPrintPDF = JasperFillManager.fillReport(reportStream, parameters, new JRBeanArrayDataSource(objDataSource1));
                    System.out.println("PATHTH3"+Common.getConfig("dataPath") +"/" + filename);
                    
                    JasperExportManager.exportReportToPdfFile(jasperPrintPDF, Common.getConfig("dataPath") + "/" + filename);
                    File pdf = new File(Common.getConfig("dataPath") + "/" + filename);
                    pdf.setWritable(true);
                    System.out.println("PATHTH4"+Common.getConfig("dataPath") + "/" + filename);
                   
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    // REPORT 14: Wound Management Plan \\

    public Vector generateWoundManagementPlan(int intPixalereID) {
        AssessmentServiceImpl amanager = new AssessmentServiceImpl(language);
        NursingCarePlanServiceImpl ncpService = new NursingCarePlanServiceImpl();
        AssessmentImagesServiceImpl imgService = new AssessmentImagesServiceImpl();
        System.out.println("REPORT 14: Wound Management Plan");
        com.pixalere.patient.bean.PatientAccountVO patientAccount = new com.pixalere.patient.bean.PatientAccountVO();
        patientAccount.setCurrent_flag(1);  // Only current account records
        patientAccount.setPatient_id(intPixalereID);
        Vector vecResultset = new Vector();
        Collection lcol1 = null;
        try {
            lcol1 = patientDB.findAllByCriteria(patientAccount);
            Iterator iter1 = lcol1.iterator();
            while (iter1.hasNext()) {
                PatientAccountVO pat = (PatientAccountVO) iter1.next();
                if (pat.getPatient_id() != null) {
                    WoundAssessmentLocationVO woundAssessmentLocation = new WoundAssessmentLocationVO();
                    woundAssessmentLocation.setPatient_id(intPixalereID);
                    woundAssessmentLocation.setActive(1);
                    woundAssessmentLocation.setDischarge(0);
                    // Retrieve the alpha details to get access to the wound details
                    Collection<WoundAssessmentLocationVO> lcol2 = null;
                    lcol2 = woundAssessmentLocationDB.findAllByCriteria(woundAssessmentLocation, 0, false, "wound_id");
                    
                    for (WoundAssessmentLocationVO wal : lcol2) {
                        WoundManagementPlan woundManagementPlan = new WoundManagementPlan();
                        String demo = ""
                                + "<b>" + Common.getLocalizedString("pixalere.patientprofile.form.patient_name", locale) 
                                + "</b> " + patientAccount.getDecrypted(pat.getFirstName(), "pass4firstname") 
                                + " " + patientAccount.getDecrypted(pat.getLastName(), "pass4lastname") + "<br/>"
                                + "<b>" + Common.getLocalizedString("pixalere.login.form.patient", locale) + "</b>" 
                                + patientAccount.getPatient_id() + "   " + Common.getConfig("phn1_name") + " " + pat.getPhn() + "<br/>"
                                + "<b>" + Common.getConfig("treatmentAreaName") + "</b><br/>"
                                + "<b>" + Common.getConfig("treatmentLocationName") + "</b> " + pat.getTreatmentLocation().getName(language) + "<br/>"
                                + "";

                        com.pixalere.wound.bean.WoundVO woundProfile = new com.pixalere.wound.bean.WoundVO();
                        woundProfile.setPatient_id(pat.getPatient_id());
                        woundProfile.setId(wal.getWound_id());
                        WoundVO wound = wservice.getWound(woundProfile);
                        woundManagementPlan.setDemographics(demo);
                        if (wound != null) {
                            WoundProfileVO t = new WoundProfileVO();
                            t.setWound_id(wound.getId());
                            t.setCurrent_flag(1);
                            t.setActive(1);
                            WoundProfileVO wpvo = wservice.getWoundProfile(t);
                            if (wpvo != null && wpvo.getReason_for_care() != null && !wpvo.getReason_for_care().equals("")) {
                                demo = demo + "<br/><b>" + Common.getLocalizedString("pixalere.viewer.form.wound_history", locale) + "</b> " + wpvo.getReason_for_care() + " <br/>";
                            }

                            woundManagementPlan.setDemographics(demo);
                            woundManagementPlan.setWoundLocation(wound.getWound_location_detailed());
                            woundManagementPlan.setBlueman(composeBluemodelImage(wound.getId(), false));
                            //System.out.println(" blueman: " + woundManagementPlan.getBlueman());
                            AbstractAssessmentVO aew = null;
                            if (wal.getWound_profile_type().getAlpha_type().equals(Constants.WOUND_PROFILE_TYPE)) {
                                AssessmentEachwoundVO tmp = new AssessmentEachwoundVO();
                                tmp.setActive(1); // Only those that were saved from Summary
                                tmp.setAlpha_id(wal.getId());
                                //tmp.setFull_assessment(1);
                                aew = (AssessmentEachwoundVO)assessmentEachwoundDB.findLastActiveAssessment(tmp,false);
                            } else if (wal.getWound_profile_type().getAlpha_type().equals(Constants.OSTOMY_PROFILE_TYPE)) {
                                AssessmentOstomyVO tmp = new AssessmentOstomyVO();
                                tmp.setActive(1); // Only those that were saved from Summary
                                tmp.setAlpha_id(wal.getId());
                                //tmp.setFull_assessment(1);
                                aew = (AssessmentOstomyVO)assessmentEachwoundDB.findLastActiveAssessment(tmp,false);
                            } else if (wal.getWound_profile_type().getAlpha_type().equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                                AssessmentDrainVO tmp = new AssessmentDrainVO();
                                tmp.setActive(1); // Only those that were saved from Summary
                                tmp.setAlpha_id(wal.getId());
                                //tmp.setFull_assessment(1);
                                aew = (AssessmentDrainVO)assessmentEachwoundDB.findLastActiveAssessment(tmp,false);
                            } else if (wal.getWound_profile_type().getAlpha_type().equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                                AssessmentIncisionVO tmp = new AssessmentIncisionVO();
                                tmp.setActive(1); // Only those that were saved from Summary
                                tmp.setAlpha_id(wal.getId());
                                //tmp.setFull_assessment(1);
                                aew = (AssessmentIncisionVO)assessmentEachwoundDB.findLastActiveAssessment(tmp,false);
                            } else if (wal.getWound_profile_type().getAlpha_type().equals(Constants.BURN_PROFILE_TYPE)) {
                                AssessmentBurnVO tmp = new AssessmentBurnVO();
                                tmp.setActive(1); // Only those that were saved from Summary
                                tmp.setAlpha_id(wal.getId());
                                //tmp.setFull_assessment(1);
                                aew = (AssessmentBurnVO)assessmentEachwoundDB.findLastActiveAssessment(tmp,false);
                            } else if (wal.getWound_profile_type().getAlpha_type().equals(Constants.SKIN_PROFILE_TYPE)) {
                                AssessmentSkinVO tmp = new AssessmentSkinVO();
                                tmp.setActive(1); // Only those that were saved from Summary
                                tmp.setAlpha_id(wal.getId());
                                //tmp.setFull_assessment(1);
                                aew = (AssessmentSkinVO)assessmentEachwoundDB.findLastActiveAssessment(tmp,false);
                            }
                            
                            
                            if (aew != null) {
                                    String alphaDemo = "";
                                    if (wpvo != null) {
                                        Collection<EtiologyVO> et = wpvo.getEtiologies();
                                        for (EtiologyVO g : et) {
                                            if (g != null && g.getAlpha_id().equals(aew.getAlpha_id())) {
                                                alphaDemo = alphaDemo + "<b>" + Common.getLocalizedString("pixalere.woundprofile.form.etiology", locale) + 
                                                        "</b> " + g.getPrintable(language) + " <br/>  ";
                                            }
                                        }
                                        Collection<GoalsVO> goals = wpvo.getGoals();
                                        for (GoalsVO g : goals) {
                                            
                                            if (g != null && g.getAlpha_id().equals(aew.getAlpha_id())) {
                                                //System.out.println("<b>" + Common.getLocalizedString("pixalere.woundprofile.form.goals", locale) + "</b> " + g.getPrintable(language) + " <br/> <br/>   ");
                                                alphaDemo = alphaDemo + "<b>" + Common.getLocalizedString("pixalere.woundprofile.form.goals", locale) + 
                                                        "</b> " + g.getPrintable(language) + " <br/>  ";
                                            }
                                        }
                                    }
                                    NursingCarePlanVO ncpTMP = new NursingCarePlanVO();
                                    ncpTMP.setWound_profile_type_id(aew.getWound_profile_type_id());
                                    ncpTMP.setActive(1);
                                    NursingCarePlanVO ncp = ncpService.getNursingCarePlan(ncpTMP);
                                    if (ncp != null && ncp.getBody() != null && !ncp.getBody().equals("")) {
                                        alphaDemo = alphaDemo + "<b>" + 
                                                Common.getLocalizedString("pixalere.viewer.form.nursing_care_plan", locale) + "</b> " + 
                                                Common.convertCarriageReturns(ncp.getBody(), true) + "  <br/> ";
                                    }
                                    DressingChangeFrequencyVO dcfTMP = new DressingChangeFrequencyVO();
                                    dcfTMP.setAlpha_id(aew.getAlpha_id());
                                    dcfTMP.setActive(1);
                                    DressingChangeFrequencyVO dc = ncpService.getDressingChangeFrequency(dcfTMP);
                                    if (dc != null && dc.getDressingChangeFrequency() != null && dc.getDressingChangeFrequency().getId() > 0) {
                                        alphaDemo = alphaDemo + "<b>" + 
                                                Common.getLocalizedString("pixalere.woundassessment.form.dressing_change_frequency", locale) + "</b> " + 
                                                dc.getDressingChangeFrequency().getName(language) + " <br/>  ";
                                    }
                                    String expandedAlpha = "";//"<b>" + Common.getLocalizedString("pixalere.reporting.form.report_last_assessment",locale) + "</b> " + aew.getWoundAssessment().getUser_signature() + " <br/>   ";
                                    if (aew instanceof AssessmentEachwoundVO) {
                                        Collection<AssessmentEachwoundVO> c = new ArrayList();
                                        AssessmentEachwoundVO e = (AssessmentEachwoundVO) aew;
                                        c.add(e);
                                        RowData[] woundAssessments = amanager.getAllAssessmentsForFlowchart(c, userVO, false);
                                        for (RowData row : woundAssessments) {
                                            String value = "";
                                            for (FieldValues field : row.getFields()) {
                                                if (field.getTitle().equals("id")) {
                                                } else if (field.getField_name()!=null && field.getField_name().equals("products")) {
                                                    if (Common.getConfig("hideProductsFromWMP").equals("0")) {
                                                        alphaDemo = alphaDemo + "<b>" + field.getTitle() + "</b> " + field.getValue() + " <br/> ";
                                                    }
                                                } else {
                                                    if(field.getValue()!=null && !field.getValue().equals("")){
                                                        expandedAlpha = expandedAlpha + "<b>" + field.getTitle() + "</b> :" + field.getValue() + " , ";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (aew instanceof AssessmentIncisionVO) {
                                        Collection<AssessmentIncisionVO> c = new ArrayList();
                                        AssessmentIncisionVO e = (AssessmentIncisionVO) aew;
                                        c.add(e);
                                        RowData[] woundAssessments = amanager.getAllIncisionAssessmentsForFlowchart(c, null, false);
                                        for (RowData row : woundAssessments) {
                                            String value = "";
                                            for (FieldValues field : row.getFields()) {
                                                if (field.getTitle().equals("id")) {
                                                } else if (field.getField_name()!=null && field.getField_name().equals("products")) {
                                                    if (Common.getConfig("hideProductsFromWMP").equals("0")) {
                                                        alphaDemo = alphaDemo + "<b>" + field.getTitle() + "</b> " + field.getValue() + " <br/><br/>    ";
                                                    }
                                                } else {
                                                    if(field.getValue()!=null && !field.getValue().equals("")){
                                                        expandedAlpha = expandedAlpha + "<b>" + field.getTitle() + "</b> :" + field.getValue() + " , ";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (aew instanceof AssessmentDrainVO) {
                                        Collection<AssessmentDrainVO> c = new ArrayList();
                                        AssessmentDrainVO e = (AssessmentDrainVO) aew;
                                        c.add(e);
                                        RowData[] woundAssessments = amanager.getAllDrainAssessmentsForFlowchart(c, null, false);
                                        for (RowData row : woundAssessments) {
                                            String value = "";
                                            for (FieldValues field : row.getFields()) {
                                                if (field.getTitle().equals("id")) {
                                                } else if (field.getField_name()!=null && field.getField_name().equals("products")) {
                                                    if (Common.getConfig("hideProductsFromWMP").equals("0")) {
                                                        alphaDemo = alphaDemo + "<b>" + field.getTitle() + "</b> " + field.getValue() + " <br/>  ";
                                                    }
                                                } else {
                                                    if(field.getValue()!=null && !field.getValue().equals("")){
                                                        expandedAlpha = expandedAlpha + "<b>" + field.getTitle() + "</b>: " + field.getValue() + " , ";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (aew instanceof AssessmentOstomyVO) {
                                        Collection<AssessmentOstomyVO> c = new ArrayList();
                                        AssessmentOstomyVO e = (AssessmentOstomyVO) aew;
                                        c.add(e);
                                        RowData[] woundAssessments = amanager.getAllOstomyAssessmentsForFlowchart(c, null, false);
                                        for (RowData row : woundAssessments) {
                                            String value = "";
                                            for (FieldValues field : row.getFields()) {
                                                if (field.getTitle().equals("id")) {
                                                } else if (field.getField_name()!=null && field.getField_name().equals("products")) {
                                                    if (Common.getConfig("hideProductsFromWMP").equals("0")) {
                                                        alphaDemo = alphaDemo + "<b>" + field.getTitle() + "</b> " + field.getValue() + " <br/> <br/>   ";
                                                    }
                                                } else {
                                                    if(field.getValue()!=null && !field.getValue().equals("")){
                                                        expandedAlpha = expandedAlpha + "<b>" + field.getTitle() + "</b> :" + field.getValue() + " , ";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if(alphaDemo == null || alphaDemo.equals("null")){alphaDemo = "";}
                                    
                                    woundManagementPlan.setData(alphaDemo);
                                    woundManagementPlan.setExpandedText(expandedAlpha);
                                    woundManagementPlan.setTitle(wound.getWound_location_detailed() + " - " + Common.getAlphaText(wal.getAlpha(), locale));
                                    AssessmentImagesVO imgTMP = new AssessmentImagesVO();
                                    //imgTMP.setAssessment_id(aew.getAssessment_id());
                                    imgTMP.setAlpha_id(aew.getAlpha_id());
                                    AssessmentImagesVO aim = imgService.getImage(imgTMP);
                                    // First gather all images for this assessment
                                    if (aim != null && !aim.getImages().equals("") && !aim.getImages().equals("a:0:{}")) {
                                        //Vector filenames = Serialize.arrayIze(aim.getImages());
                                        String filename = aim.getImages();
                                        String strImageFile = Common.getConfig("photos_path") + "/" + Common.getConfig("customer_id") + "/" + pat.getPatient_id() + "/" + aim.getAssessment_id() + "/" + filename;
                                        File imagefile = new File(strImageFile);
                                        if (imagefile.exists()) {
                                            woundManagementPlan.setPhoto(strImageFile);
                                            woundManagementPlan.setPhoto_date(aim.getWoundAssessment().getUser_signature());
                                        }

                                    } else {
                                        woundManagementPlan.setPhoto(basepath + "images/noimage.gif");
                                    }
                                    
                                    
                                }

                            vecResultset.add(woundManagementPlan);
                        }
                    }
                }
            }
        } catch (com.pixalere.common.ApplicationException eer) {
            eer.printStackTrace();
        } catch (com.pixalere.common.DataAccessException exr) {
            exr.printStackTrace();
        } catch (Exception er) {
            er.printStackTrace();
        }
        Vector vecSortedResultsets = new Vector();
        vecSortedResultsets.add(vecResultset.toArray());
        //Object[] o = vecResultset.toArray();

        return vecSortedResultsets;
    }

    public void buildReport() {
    }

    public byte[] callReport(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        byte[] bytes = null;
        try {
            com.pixalere.admin.service.LogAuditServiceImpl logbd = new com.pixalere.admin.service.LogAuditServiceImpl();
            logbd.saveLogAudit(new com.pixalere.admin.bean.LogAuditVO(userVO.getId(), -1, "REPORT", report_type, -1));
            System.out.println("ReportingManager.java: CallReport; report_type = " + report_type);
            Object[] objDataSource1 = null;
            Object[] objDataSource2 = null;
            String strJasperReportFilename = "";
            String strUserName = userVO.getFirstname() + " " + userVO.getLastname();
            String strGroupingHeader = "";
            String strFilterDetails = "";
            Locale currentLocale = Common.getLocaleFromLocaleString(locale);
            if (group_by != null) {
                if (group_by.equals("treatmentLocation")) {
                    strGroupingHeader = Common.getConfig("treatmentLocationName");
                }
                if (group_by.equals("serviceArea")) {
                    strGroupingHeader = Common.getConfig("treatmentCatName");
                }
                if (group_by.equals("treatmentArea")) {
                    strGroupingHeader = Common.getConfig("treatmentCatName");
                }
            }
            String strReportTitle = "";
            String strReportSubTitle = "";
            if (!strGroupingHeader.equals("")) {
                strReportSubTitle = Common.getLocalizedReportString("pixalere.reports.common.grouped_by", locale) + strGroupingHeader;
            }
            String strBreakDownTitle = "";
            String strReportDateRange = "";
            if (start_date != null) {
                strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale) + 
                        new PDate().getLocalizedDateString(start_date, currentLocale) + 
                        " - " + 
                        new PDate().getLocalizedDateString(end_date, currentLocale);
            } else {
                strReportDateRange = Common.getLocalizedReportString("pixalere.reports.common.as_per", locale) + 
                        new PDate().getLocalizedDateString(getIntEpochNow(), currentLocale);
            }
            Map parameters = new HashMap();
            if (report_type.equals(PATIENTS_WITH_PRESSURE_WOUNDS)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.with_pressure_wounds.title", locale);
                strJasperReportFilename = "patients_with_pressure_wounds";
                vecDataSource = generatePatientsWithPressureWoundsReport(group_by);
            }
            if (report_type.equals(LAST_ASSESSMENTS)) {
                if (full_assessment.equals("1")) {
                    strReportTitle = Common.getLocalizedReportString("pixalere.reports.last_assessments.title_full", locale);
                    parameters.put("ReportColumnDate", Common.getLocalizedReportString("pixalere.reports.last_assessments.column_date_full", locale)); // For displaying date ranges etc.
                } else {
                    strReportTitle = Common.getLocalizedReportString("pixalere.reports.last_assessments.title", locale);
                    parameters.put("ReportColumnDate", Common.getLocalizedReportString("pixalere.reports.last_assessments.column_date", locale)); // For displaying date ranges etc.
                }
                strReportSubTitle = Common.getLocalizedReportString("pixalere.reports.last_assessments.for_active_care", locale) 
                        + new PDate().getLocalizedDateString(getIntEpochNow(), currentLocale);
                strReportDateRange = "";
                Vector vecCaretypes = new Vector();
                if (caretypes != null) {
                    for (int intCaretypes = 0; intCaretypes < caretypes.length; intCaretypes++) {
                        if (!caretypes[intCaretypes].equals("0")) {
                            vecCaretypes.add(caretypes[intCaretypes].toLowerCase().trim());
                            if (strReportDateRange.length() != 0) {
                                strReportDateRange = strReportDateRange + ", ";
                            }
                            String strStatus = "";
                            if (caretypes[intCaretypes].equals(Constants.WOUND_PROFILE_TYPE)) {
                                strStatus = Common.getConfig("woundActive");
                            }
                            if (caretypes[intCaretypes].equals(Constants.INCISIONTAG_PROFILE_TYPE)) {
                                strStatus = Common.getConfig("incisionActive");
                            }
                            if (caretypes[intCaretypes].equals(Constants.TUBESANDDRAINS_PROFILE_TYPE)) {
                                strStatus = Common.getConfig("drainActive");
                            }
                            if (caretypes[intCaretypes].equals(Constants.OSTOMY_PROFILE_TYPE)) {
                                strStatus = Common.getConfig("ostomyActive");
                            }
                            if (caretypes[intCaretypes].equals(Constants.SKIN_PROFILE_TYPE)) {
                                strStatus = Common.getConfig("skinActive");
                            }
                            if (strFilterDetails.length() != 0) {
                                strFilterDetails = strFilterDetails + ", ";
                            }
                            strFilterDetails = strFilterDetails + Common.getWoundProfileType(caretypes[intCaretypes], locale)
                                    + " status \"" + Common.getListValue(Integer.parseInt(strStatus), language) + "\"";
                            strReportDateRange = strReportDateRange + Common.getWoundProfileType(caretypes[intCaretypes], locale);
                        }
                    }
                    strFilterDetails = strFilterDetails + ". ";
                }
                strReportDateRange = strReportDateRange + "; ";
                Vector vecGoals = new Vector();
                Vector vecGoalDays = new Vector();
      
                if (goal_days != null) {
                    StringTokenizer strDays = new StringTokenizer(goal_days, ",");
                    while (strDays.hasMoreTokens()) {
                        vecGoalDays.add(strDays.nextToken());
                    }
                }
                if (goals != null) {
                    StringTokenizer strGoals = new StringTokenizer(goals, ",");
                    int intItems = 0;
                    while (strGoals.hasMoreTokens()) {
                        String strToken = strGoals.nextToken();
                        if (!strToken.equals("0") && !strToken.equals("")) {
                            Integer intGoalsID = Integer.parseInt(strToken);
                            LookupVO vo = listBD.getListItem(intGoalsID);
                            vecGoals.add(vo.getName(language).toLowerCase().trim());
                            System.out.println("Goals: " + vecGoals.size() + " " + vecGoalDays.size() + " " + vecCaretypes.size());
                            if (vecGoalDays.size() < vecGoals.size()) {
                                vecGoalDays.add("8");
                            }
                            if (intItems > 0) {
                                strReportDateRange = strReportDateRange + ", ";
                            }
                            strReportDateRange = strReportDateRange + vo.getName(language) + " > " + vecGoalDays.get(intItems) + " ";
                            if (vecGoalDays.get(intItems).equals("1")) {
                                strReportDateRange = strReportDateRange + 
                                        Common.getLocalizedReportString("pixalere.reports.last_assessments.day", locale);
                            } else {
                                strReportDateRange = strReportDateRange + 
                                        Common.getLocalizedReportString("pixalere.reports.last_assessments.days", locale);
                            }
                        }
                        intItems++;
                    }
                }
                strReportDateRange = strReportDateRange + ".";
                strJasperReportFilename = "last_assessments";
                vecDataSource = generateLastAssessmentsReport(group_by, full_assessment, vecCaretypes, vecGoals, vecGoalDays);
            }
            if (report_type.equals(WOUND_COUNT)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.title", locale);
                strJasperReportFilename = "wounds_overview_no_breakdown_with_visits";
                strGroupingHeader = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.grouping_header", locale);
                Boolean blnAddSummary = false;
                if (breakdown == null) {
                    breakdown = "";
                }
                if (breakdown.equalsIgnoreCase("patientCare")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_patient_care", locale);
                    blnAddSummary = true;
                }
                if (breakdown.equalsIgnoreCase("goals")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_goals", locale);
                    strGroupingHeader = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.grouping_header_goal", locale);
                    strJasperReportFilename = "wounds_overview_no_breakdown";
                }
                if (breakdown.equalsIgnoreCase("etiology")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_etiology", locale);
                    strGroupingHeader = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.grouping_header_wound_profile", locale);
                    strJasperReportFilename = "wounds_overview_no_breakdown";
                }
                if (breakdown.equalsIgnoreCase("diabetic")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_diabetic_patients", locale);
                    strJasperReportFilename = "wounds_overview_with_breakdown";
                }
                if (breakdown.equalsIgnoreCase("lowerLimb")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_lower_limb_wounds", locale);
                    strJasperReportFilename = "wounds_overview_with_breakdown";
                }
                if (breakdown.equalsIgnoreCase("diabLowExtr")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_diabetic_lower", locale);
                    strJasperReportFilename = "wounds_overview_with_breakdown";
                }
                if (breakdown.equalsIgnoreCase("newPatients")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_new_patients", locale);
                    strJasperReportFilename = "wounds_overview_with_breakdown";
                }
                if (breakdown.equalsIgnoreCase("surgical")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_surgical_wounds", locale);
                    strJasperReportFilename = "wounds_overview_with_breakdown";
                }
                if (breakdown.equalsIgnoreCase("pressure")) {
                    strBreakDownTitle = Common.getLocalizedReportString("pixalere.reports.wounds_no_breakdown_visits.bdown_title_pressure_wounds", locale);
                    strJasperReportFilename = "wounds_overview_with_breakdown";
                }
                if (strReportSubTitle.equals("")) {
                    strReportSubTitle = strBreakDownTitle;
                } else {
                    if (!strBreakDownTitle.equals("")) {
                        strReportSubTitle = strBreakDownTitle + " - " + strReportSubTitle;
                    }
                }
                vecDataSource = generateWoundCountsGen2(group_by, breakdown, strBreakDownTitle, start_date, end_date, blnAddSummary);
            }
            if (report_type.equals(TRANSFERRED_PATIENTS)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.transferred_summary.title", locale);
                strReportSubTitle = Common.getLocalizedReportString("pixalere.reports.transferred_summary.subtitle", locale);
                strJasperReportFilename = "transferred_patients_summary";
                vecDataSource = generateTransferredPatients(start_date, end_date);
            }
            if (report_type.equals(REFERRALS_AND_RECOMMENDATIONS)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.referrals_recommendations.title", locale);
                strJasperReportFilename = "referrals_and_recommendations";
                vecDataSource = generateReferralsAndRecommendations(group_by, start_date, end_date);
            }
            if (report_type.equals(OVERVIEW_ACTIVE_PATIENTS)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.overview_active.title", locale);
                strJasperReportFilename = "overview_active_patients";
                vecDataSource = generateOverviewActivePatients(group_by);
            }
            if (report_type.equals(PATIENT_FILE)) {
                // TODO: DELETE
                // strReportTitle = "Pixalere Patient File";
                strReportSubTitle = Common.getLocalizedReportString("pixalere.reports.patient_file_old.subtitle", locale) + strReportDateRange;
                strJasperReportFilename = "patient_file_old";
                int intPatient_id = 0;
                try {
                    intPatient_id = Integer.parseInt(patient_id);
                } catch (Exception e) {
                }
                vecDataSource = generatePatientFileReport(start_date, end_date, intPatient_id, print_patient_details);
            }
            if (report_type.equals(DISCHARGED_WOUNDS)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.discharged_wounds.title", locale);
                strJasperReportFilename = "discharged_wounds";
                vecDataSource = generateDischargedWounds(group_by, start_date, end_date);
            }
            if (report_type.equals(WOUND_MANAGEMENT_PLAN)) {
                // TODO: DELETE
                // strReportTitle = Common.getLocalizedString("pixalere.reporting.form.report_wound_management_plan", locale);
                strJasperReportFilename = "wound_management_plan";
                int intPatient_id = 0;
                try {
                    intPatient_id = Integer.parseInt(patient_id);
                } catch (Exception e) {
                }
                vecDataSource = generateWoundManagementPlan(intPatient_id);
            }
            if (report_type.equals(HEALTIME_AND_COST_PER_ETIOLOGY)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.time_cost_etiology.title", locale);
                strJasperReportFilename = "healtime_and_cost_per_etiology";
                vecDataSource = generateHealTimeAndCost(group_by, start_date, end_date);
            }
            if (report_type.equals(TOTAL_COST_PER_PATIENT)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.total_per_patient.title", locale);
                strJasperReportFilename = "total_cost_per_patient";
                if (start_date != null) {
                    strReportDateRange = Common.getLocalizedReportString("pixalere.reports.total_per_patient.date_range_wounds_during", locale) +  
                            new PDate().getLocalizedDateString(start_date, currentLocale) + 
                            " - " + 
                            new PDate().getLocalizedDateString(end_date, currentLocale);
                } else {
                    strReportDateRange = Common.getLocalizedReportString("pixalere.reports.total_per_patient.date_range_wounds_all", locale) +  
                            new PDate().getLocalizedDateString(getIntEpochNow(), currentLocale);
                }
                vecDataSource = generateCostPerPatient(group_by, start_date, end_date);
            }
            if (report_type.equals(COST_FOR_PRESSURE_WOUNDS)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.cost_pressure_wounds.title", locale);
                strJasperReportFilename = "cost_for_pressure_wounds";
                vecDataSource = generateCostForPressureWounds(group_by, start_date, end_date);
            }
            if (report_type.equals(VISITS_PER_PATIENT)) {
                strReportTitle = Common.getLocalizedReportString("pixalere.reports.visits_per_patient.title", locale); 
                strReportSubTitle = Common.getLocalizedReportString("pixalere.reports.visits_per_patient.subtitle", locale);
                strJasperReportFilename = "visits_per_patient";
                vecDataSource = generateVisitsPerPatient(start_date, end_date);
            }
            if (vecDataSource.size() >= 1) {
                objDataSource1 = (Object[]) vecDataSource.get(0);
            }
            if (vecDataSource.size() == 2) {
                objDataSource2 = (Object[]) vecDataSource.get(1);
                // Store the second object into the subreport parameter
                JRDataSource jrDataSource = new JRBeanArrayDataSource(objDataSource2); // For using the same data
                parameters.put("SubReportData", jrDataSource);
            }
            if (request != null) {
                HttpSession session = request.getSession();
                context = session.getServletContext();
            }
            //need use start_date/end_date
            parameters.put("ReportDateRange", strReportDateRange); // For displaying date ranges etc.
            parameters.put("ReportUser", strUserName); // For footer.
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            parameters.put("ReportGenerationDate", 
                    new PDate().getLocalizedDateString(getIntEpochNow(), 
                            currentLocale) + 
                            " " + 
                            convertToFixedLength(Integer.toString(calendar.get(Calendar.HOUR_OF_DAY)), 2) + 
                            ":" + convertToFixedLength(Integer.toString(calendar.get(Calendar.MINUTE)), 2)); // For footer
            parameters.put("ReportTitle", strReportTitle);
            parameters.put("ReportSubTitle", strReportSubTitle);
            parameters.put("ReportDateRange", strReportDateRange); // For displaying date ranges etc.
            parameters.put("ReportGroupingHeader", strGroupingHeader);
            parameters.put("SubReportPath", context.getRealPath("/reports") + "/");
            parameters.put("ImagesPath", context.getRealPath("/images") + "/");
            
            String strLocationsSelection = 
                    Common.getLocalizedReportString("pixalere.reports.common.locations_selection_all", locale) + 
                    Common.getConfig("treatmentLocationName") + " " +
                    Common.getLocalizedReportString("pixalere.reports.common.locations_selection_all_access", locale);
            
            if (getPartialSelectedLocations(treatment_location, userVO.getRegions()) == true) {
                strLocationsSelection = 
                        Common.getLocalizedReportString("pixalere.reports.common.locations_selection", locale) +
                        Common.getConfig("treatmentLocationName") + " " +
                        Common.getLocalizedReportString("pixalere.reports.common.locations_selection_only", locale);
            }
            // i18n
            parameters.put(JRParameter.REPORT_LOCALE, Common.getLocaleFromLocaleString(locale));
            
            parameters.put("ReportLocationsSelection", strLocationsSelection);
            System.out.println("Report: "+context.getRealPath("/reports/" + strJasperReportFilename + ".jasper"));
            InputStream reportStream = new FileInputStream(context.getRealPath("/reports/" + strJasperReportFilename + ".jasper"));
            BufferedWriter writer = null;
            try {
                bytes = JasperRunManager.runReportToPdf(reportStream, parameters, new JRBeanArrayDataSource(objDataSource1));

                if (response != null) {
                    ServletOutputStream servletOutputStream = response.getOutputStream();

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    response.setHeader("Content-disposition", "filename=" + strJasperReportFilename + ".pdf");
                    servletOutputStream.write(bytes, 0, bytes.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                }
                try {
                    OutputStream out = new FileOutputStream("c:\\"+strJasperReportFilename+"-"+new Date().getTime()+".pdf");
                    //OutputStream out = new FileOutputStream("c:\\pixalere\\reports_tmp\\"+strJasperReportFilename+"-"+new Date().getTime()+".pdf");
                    out.write(bytes);
                    out.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    
                }
            
        } catch (JRException ex) {
            ex.printStackTrace();
        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bytes;
    }
    ReportDAO reportDAO = null;
    ListServiceImpl listBD = null;
    ResourcesServiceImpl resourcesBD = null;
    PatientDAO patientDB = null;
    PatientServiceImpl patientServiceImpl = null;
    PatientProfileServiceImpl patientProfileServiceImpl = null;
    PatientProfileDAO patientProfileDB = null;
    WoundProfileDAO woundProfileDB = null;
    WoundAssessmentDAO woundAssessmentDB = null;
    WoundAssessmentLocationDAO woundAssessmentLocationDB = null;
    AssessmentDAO assessmentEachwoundDB = null;
    AssessmentCommentsDAO assessmentCommentsDB = null;
    ListsDAO listDataDB = null;
    ReferralsTrackingServiceImpl referralsTrackingManagerBD = null;
    private Date start_date;
    private Date end_date;
    private String report_type;
    private String group_by;
    private String breakdown;
    private String filter;
    private String full_assessment;
    private String goal_days;
    private String print_patient_details;
    private String[] treatment_location;
    private String includeTIP;
    private String[] etiology;
    private String goals;
    private String[] caretypes;
    private String patient_id;
    private ProfessionalVO userVO;
    private final static String OVERVIEW_ACTIVE_PATIENTS = "report_overview_active_patients";
    private final static String WOUND_COUNT = "report_wound_count";
    private final static String WOUND_COUNT_DRESSINGS = "report_wound_count_with_dressings";
    private final static String PATIENTS_WITH_PRESSURE_WOUNDS = "report_patients_with_pressure_wounds";
    private final static String LAST_ASSESSMENTS = "report_last_assessments";
    private final static String TRANSFERRED_PATIENTS = "report_transferred_patients";
    private final static String REFERRALS_AND_RECOMMENDATIONS = "report_referrals_and_recommendations";
    private final static String TOTAL_COST_PER_PATIENT = "report_total_cost_per_patient";
    private final static String HEALTIME_AND_COST_PER_ETIOLOGY = "report_healtime_and_cost_per_etiology";
    private final static String PATIENT_FILE = "report_patient_file";
    private final static String DISCHARGED_WOUNDS = "report_discharged_wounds";
    private final static String COST_FOR_PRESSURE_WOUNDS = "report_cost_for_pressure_wounds";
    private final static String VISITS_PER_PATIENT = "report_visits_per_patient";
    private final static String WOUND_MANAGEMENT_PLAN = "report_wound_management_plan";
    private float fltSecondsPerDay = 60 * 60 * 24;
    private Vector vecDataSource;
}
