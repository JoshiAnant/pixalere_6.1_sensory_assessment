/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.dao;

import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.DashboardVO;
import com.pixalere.utils.Common;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Encapsulates common logic for the Dashboard reports.
 *
 * @author Jose
 */
public abstract class DashboardReportBaseDAO implements DashboardReportDAOInterface {
    private static final org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(DashboardReportBaseDAO.class);
    
    protected Connection con = null;
    protected boolean mysql=false;
    
    public DashboardReportBaseDAO(){
    }
    
    
    // Internal data
    protected String category_name; 
    protected int etiology_id; 
    protected HashMap<String, DashboardVO> list; 
    protected DashboardReportVO reportCrit; 
    protected String key; 
    protected String startDate; 
    protected String endDate; 
    protected String treatment_query;
    protected List<Integer> locationsList;
    protected String locale;
    
    protected String dateDiff;
    protected String patientDischargeClosedID;
    protected String patientDischargeOtherID;
    protected String patientDischargeSelfcareID;
    /**
     * Common query building
     * @param wound_type
     * @param etiology_id
     * @return query
     */
    protected String buildQuery(String wound_type, int etiology_id, String table_name) {
        String query = " WHERE ";
        String wound_type_query = "";
        if (wound_type.equals("All")) {
        } else if (wound_type.equals("Wound")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'A' and patient_id=" + table_name + ".patient_id) and ";
        } else if (wound_type.equals("Ostomy")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'O' and patient_id=" + table_name + ".patient_id) and ";
        } else if (wound_type.equals("Drain")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'D' and patient_id=" + table_name + ".patient_id) and ";
        } else if (wound_type.equals("Incision")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'T' and patient_id=" + table_name + ".patient_id) and ";
        }
        if (etiology_id > 0) {
            wound_type_query = wound_type_query + " exists (select id from wound_etiology et where patient_id=" + table_name + ".patient_id and et.etiology_id=" + etiology_id + ") and ";
        }
        if (!wound_type_query.equals("")) {
            query = query + wound_type_query;
        }
        return query;
    }
    
    public void disconnect() {
        try {
            if (con != null && con.isClosed()==false) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void connect() {
        DataSource ds = null;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
            con = ds.getConnection();
            
            // Detect DBMS and setup discharge ids
            checkIsMySQL();
            initializeDischargeIds();
            
        } catch (NamingException ne) {
            ne.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void initializeDischargeIds(){
        // Init Patient IDs
        patientDischargeClosedID="3507";
        if(Common.getConfig("patientDischargeReasonComplete")!=null && !Common.getConfig("patientDischargeReasonComplete").equals("")){
            try{
                patientDischargeClosedID = Common.getConfig("patientDischargeReasonComplete");
            }catch(NumberFormatException e){logs.error("Invalid Discharge ID"+e.getMessage());}
        }
        
        patientDischargeOtherID="3201";
        if(Common.getConfig("patientDischargeReasonOther")!=null && !Common.getConfig("patientDischargeReasonOther").equals("")){
            try{
                patientDischargeOtherID = Common.getConfig("patientDischargeReasonOther");
            }catch(NumberFormatException e){logs.error("Invalid Discharge ID"+e.getMessage());}
        }
        
        patientDischargeSelfcareID="1603";
        if(Common.getConfig("patientDischargeReasonSelfCare")!=null && !Common.getConfig("patientDischargeReasonSelfCare").equals("")){
            try{
                patientDischargeSelfcareID = Common.getConfig("patientDischargeReasonSelfCare");
            }catch(NumberFormatException e){logs.error("Invalid Discharge ID"+e.getMessage());}
        }
    }
    
    protected void checkIsMySQL(){
        //select query with top 1... if it fails.. its mysql.
        try{
            PreparedStatement state = con.prepareStatement("select top 1 from configuration", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
        }catch(com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException e){
            mysql=true;
        }catch(Exception e){
            if(e.getMessage().indexOf("MySQL")!=-1){
                mysql=true;
            }
        }
        System.out.println("Is MYSQL"+mysql);
        
    }
    
    protected HashMap<String, String> getColumns(ResultSet result, String columns) {
        HashMap<String, String> mapArray = new HashMap();
        try {
            if (result != null) {
                result.beforeFirst();
                while (result.next()) {
                    for (String column : columns.split(",")) {
                        String s = result.getString(column);
                        if (s == null) {
                            s = "";
                        }
                        mapArray.put(column, s);
                    }
                    return mapArray;
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
            logs.error("getColumns SQLException: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            logs.error("getColumns Exception: " + e.getMessage());
            e.printStackTrace();
        }
        return mapArray;
    }
    
    public ResultSet getResults(String query) {
        try {
            PreparedStatement state = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
            return result;
        } catch (SQLException s) {
            logs.error("getResults SQLException: " + s.getMessage());
            s.printStackTrace();
        } catch (Exception s) {
            logs.error("getResults Exception: " + s.getMessage());
            s.printStackTrace();
        }
        return null;
    }
}
