/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.dao;

import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.DashboardVO;
import java.util.HashMap;
import java.util.List;

/**
 * Interface for the Dashboard Report DAOs.
 *
 * @author Jose
 */
public interface DashboardReportDAOInterface {
    public HashMap<String, DashboardVO> getDashboardReport(
            String category_name, 
            int etiology_id, 
            HashMap<String, DashboardVO> list, 
            DashboardReportVO reportCrit, 
            String key, 
            String startDate, 
            String endDate, 
            String treatment_query,
            List<Integer> locationsList,
            String locale);
}
