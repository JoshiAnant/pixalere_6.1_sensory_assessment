package com.pixalere.reporting.dao;

import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.reporting.bean.ScheduleConfigVO;
import com.pixalere.reporting.bean.ScheduleLocationVO;
import com.pixalere.reporting.bean.ReportScheduleVO;
import java.util.Collection;
import java.util.Date;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;

/**
 *
 * @author Jose
 */
public class ReportSchedulerDAO implements DataAccessObject {

    // Create Log4j category instance for logging
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReportSchedulerDAO.class);

    @Override
    public ValueObject findByPK(int primaryKey) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.findByPK()***********************");
        PersistenceBroker broker = null;
        ReportScheduleVO reportScheduleVO = new ReportScheduleVO();

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            reportScheduleVO.setId(new Integer(primaryKey));

            Query query = new QueryByCriteria(reportScheduleVO);
            log.debug("ReportSchedulerDAO.findByPK(): " + query.toString() + " :::: Value=" + primaryKey);

            reportScheduleVO = (ReportScheduleVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.findByPK(): " + e.toString());
            throw new DataAccessException("ServiceLocatorException in ReportSchedulerDAO.findByPK()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Leaving ReportSchedulerDAO.findByPK()***********************");
        return reportScheduleVO;
    }

    @Override
    public void update(ValueObject updateRecord) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.update()***********************");
        ReportScheduleVO reportScheduleVO = null;
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            reportScheduleVO = (ReportScheduleVO) updateRecord;

            broker.beginTransaction();
            broker.store(reportScheduleVO);
            broker.commitTransaction();
            
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.update(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ReportSchedulerDAO.update()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ReportSchedulerDAO.update(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in ReportSchedulerDAO.update()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Entering ReportSchedulerDAO.update()***********************");
    }

    @Override
    public void delete(ValueObject deleteRecord) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.delete()***********************");
        PersistenceBroker broker = null;

        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportScheduleVO reportScheduleVO = (ReportScheduleVO) deleteRecord;

            //Begin the transaction.
            broker.beginTransaction();
            broker.delete(reportScheduleVO);
            broker.commitTransaction();
            
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ReportSchedulerDAO.delete(): " + e.toString());
            e.printStackTrace();

            throw new DataAccessException("Error in ReportScheduleVO.delete()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.delete(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in ReportScheduleVO.delete()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving ReportSchedulerDAO.delete()***********************");
    }
    
    /**
     * Finds a all report_schedule records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param reportScheduleVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 6.0
     */
    public Collection findAllByCriteria(ReportScheduleVO reportScheduleVO) throws DataAccessException {
        //log.info("***********************Entering sso_providersDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();

            Query query = new QueryByCriteria(reportScheduleVO);
            results = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ReportSchedulerDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Leaving ReportSchedulerDAO.findByCriteria()***********************");
        return results;
    }
    
    /**
     * Finds a all schedule_config records that are active and which
     * have an execution date earlier than the limit date
     * passed in.  If no record is found a null value is returned.
     *
     * @param reportScheduleVO fields in ValueObject which are not null will be used as
     * parameters in the SQL statement.
     * @param limit_date
     * @return 
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 6.0
     */
    public Collection findScheduledConfigExpired(Date limit_date) throws DataAccessException {
        //log.info("***********************Entering sso_providersDAO.findScheduledConfigExpired()***********************");
        PersistenceBroker broker = null;
        Collection results = null;
        
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            
            Criteria crit = new Criteria();
            crit.addEqualTo("processed", 0);
            crit.addLessOrEqualThan("execution_on", limit_date);

            QueryByCriteria query = QueryFactory.newQuery(ScheduleConfigVO.class, crit);
            results = (Collection) broker.getCollectionByQuery(query);

        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.findScheduledConfigExpired(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ReportSchedulerDAO.findScheduledConfigExpired()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }

        //log.info("***********************Leaving ReportSchedulerDAO.findScheduledConfigExpired()***********************");
        return results;
    }
    
    public ValueObject findScheduleConfigByCriteria(ScheduleConfigVO configVO) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.findScheduleConfigByCriteria()***********************");
        PersistenceBroker broker = null;
        ScheduleConfigVO returnVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            returnVO = new ScheduleConfigVO();
            Query query = new QueryByCriteria(configVO);
            returnVO = (ScheduleConfigVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.findScheduleConfigByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in ReportSchedulerDAO.findScheduleConfigByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving ReportSchedulerDAO.findScheduleConfigByCriteria()***********************");
        return returnVO;
    }
    
    public ValueObject findByCriteria(ValueObject vo) throws DataAccessException {
        log.info("********* Entering the ReportSchedulerDAO.findByPK****************");
        PersistenceBroker broker = null;
        ReportScheduleVO scheduleVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            scheduleVO = (ReportScheduleVO) vo;
            Query query = new QueryByCriteria(scheduleVO);
            scheduleVO = (ReportScheduleVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            log.error("PersistanceBrokerException thrown in ReportSchedulerDAO.findByPK(): " + e.toString(), e);
            throw new DataAccessException("Error in ReportSchedulerDAO.findByPK(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        log.info("********* Done the ReportSchedulerDAO.findByPK");
        return scheduleVO;
    }
    
    public void deleteLocation(ScheduleLocationVO deleteRecord) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.deleteLocation()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(deleteRecord);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ReportSchedulerDAO.deleteLocation(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ReportSchedulerDAO.deleteLocation()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.deleteLocation(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.deleteLocation()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving ReportSchedulerDAO.deleteLocation()***********************");
    }
    
    public void deleteConfig(ScheduleConfigVO deleteRecord) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.deleteConfig()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            // Begin the transaction.
            broker.beginTransaction();
            broker.delete(deleteRecord);
            broker.commitTransaction();
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ReportSchedulerDAO.deleteConfig(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ReportSchedulerDAO.deleteConfig()", e);
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.deleteConfig(): " + e.toString());
            throw new DataAccessException("ServiceLocator exception in userVO.deleteConfig()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Leaving ReportSchedulerDAO.deleteConfig()***********************");
    }
    
    public void updateLocation(ScheduleLocationVO vo) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.update()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(vo);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.updateLocation(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ReportSchedulerDAO.updateLocation()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ReportSchedulerDAO.updateLocation(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ReportSchedulerDAO.updateLocation()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Entering ReportSchedulerDAO.updateLocation()***********************");
    }
    
    public void updateConfig(ScheduleConfigVO vo) throws DataAccessException {
        //log.info("***********************Entering ReportSchedulerDAO.update()***********************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(vo);
            broker.commitTransaction();
        } catch (ConnectionLocatorException e) {
            log.error("ServiceLocatorException thrown in ReportSchedulerDAO.updateConfig(): " + e.toString());
            throw new DataAccessException("ServiceLocator in Error in ReportSchedulerDAO.updateConfig()", e);
        } catch (PersistenceBrokerException e) {
            // if something went wrong: rollback
            broker.abortTransaction();
            log.error("PersistenceBrokerException thrown in ReportSchedulerDAO.updateConfig(): " + e.toString());
            e.printStackTrace();
            throw new DataAccessException("Error in ReportSchedulerDAO.updateConfig()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        //log.info("***********************Entering ReportSchedulerDAO.updateConfig()***********************");
    }
    
    
}
