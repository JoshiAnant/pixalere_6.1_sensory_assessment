/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.dao;

import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.DashboardVO;
import com.pixalere.utils.Common;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author User
 */
public class DashboardReportDAO extends DashboardReportBaseDAO {
    private static final org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(DashboardReportDAO.class);
    
    public DashboardReportDAO() {
    }

    @Override
    public HashMap<String, DashboardVO> getDashboardReport(
            String category_name, 
            int etiology_id, 
            HashMap<String, DashboardVO> list, 
            DashboardReportVO reportCrit, 
            String key, 
            String startDate, 
            String endDate, 
            String treatment_query,
            List<Integer> locationsList,
            String locale) {

        // Move settings to private members to avoid unnecessary parameter
        // passing since we only are dealing with internal functions
        this.category_name = category_name;
        this.etiology_id = etiology_id;
        this.list = list;
        this.reportCrit = reportCrit;
        this.key = key;
        this.startDate = startDate;
        this.endDate = endDate;
        this.treatment_query = treatment_query;
        this.locationsList = locationsList;
        this.locale = locale;
        
        //if the database is MSSQL, we need to pass in the datepart
        this.dateDiff="";
        if(!mysql){dateDiff="DAY,";}

        // All Wounds
        getDataAllWounds();

        // Admitted / Discharged
        getAdmittedDischarged();

        // Wounds (existing, new, closed) - Days T oHeal
        getWoundsDaysToHeal();

        // Heal Rates 30% and 70%
        getHealRates();
        
        // All visits entries
        getVisits();
        
        // Cost of Products for Open and Closed wounds
        getProductCostsWounds();

        return this.list;
    }

    private void getProductCostsWounds(){
        //open wounds
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        ResultSet result = getResults("select (sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " open_time_stamp<='" + endDate + "' AND (close_time_stamp>='" + endDate + "' OR close_time_stamp IS NULL) and wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 33, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_open_wounds",locale), "avgcost_bywound_" + key},}, key, result, list, "currency", false);
        list = Common.parseDashboardResult(category_name, 34, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_product_cost_open_wounds",locale), "totalcost_bywound_" + key}}, key, result, list, "currency", false);
        
        //close wounds
        result = getResults("select  count(distinct(wal.id)) as closed_wound_count,(sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>='" + startDate + "' AND close_time_stamp<='" + endDate + "' and wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 35, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_closed_wounds",locale), "avgcost_bywound_" + key},}, key, result, list, "currency", false);
        list = Common.parseDashboardResult(category_name, 36, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_product_cost_closed_wounds",locale), "totalcost_bywound_" + key}}, key, result, list, "currency", false);
    }

    private void getVisits(){
        //All Home visits - Order 22
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        ResultSet result = getResults("select sum(lookup.clinic) as visit_clinic_count,(count(wound_assessment.id)-sum(lookup.clinic)) as visit_home_count from wound_assessment left join lookup on lookup.id=wound_assessment.treatment_location_id " + builtQuery + " visit = 1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 24, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_home_visits",locale), "visit_home_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 26, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_clinic_visits",locale), "visit_clinic_count"}}, key, result, list, "integer", false);

        //All Home Assessments - Order 23
        result = getResults("select sum(lookup.clinic) as visit_clinic_count,(count(wound_assessment.id)-sum(lookup.clinic)) as visit_home_count from wound_assessment left join lookup on lookup.id=wound_assessment.treatment_location_id " + builtQuery + " visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 25, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_home_assess",locale), "visit_home_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 27, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_clinic_assess",locale), "visit_clinic_count"}}, key, result, list, "integer", false);

        //visits  27/28 // if lookup.report_value >=3
        result = getResults("select count(distinct(dcf.alpha_id))  as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + "  wound_assessment.active=1 and wound_assessment.visited_on>='" + startDate + "' AND wound_assessment.visited_on<='" + endDate + "' " + treatment_query);
        HashMap m = getColumns(result, "visit_count");
        double total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("visit_count"));
            result = getResults("select count(distinct(dcf.alpha_id)) as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf   left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + " report_value IS NOT null and isnumeric(report_value)=1  and  cast(lookup.report_value as decimal(5,2))>=3 and wound_assessment.active=1 and wound_assessment.visited_on>'" + startDate + "' and wound_assessment.visited_on<'" + endDate + "' " + treatment_query + "");
            m = getColumns(result, "visit_count");
            double vis = Double.parseDouble((String) m.get("visit_count"));
            total = vis/cnt;
            total = 1 - total;
        }
        list = Common.parseDashboardResult(category_name, 29, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.visits_gr",locale), "visit_count"}}, key, total, list, "percent", false);
        
        result = getResults("select count(distinct(dcf.alpha_id))  as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + "  wound_assessment.active=1 and wound_assessment.visited_on>='" + startDate + "' AND wound_assessment.visited_on<='" + endDate + "' " + treatment_query);
        m = getColumns(result, "visit_count");
        total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("visit_count"));
            result = getResults("select count(distinct(dcf.alpha_id)) as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf   left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + "  report_value IS NOT null and isnumeric(report_value)=1  and  cast(lookup.report_value as decimal(5,2))<3 and  wound_assessment.active=1 and wound_assessment.visited_on>'" + startDate + "' and wound_assessment.visited_on<'" + endDate + "' " + treatment_query + "");
            m = getColumns(result, "visit_count");
            double vis = Double.parseDouble((String) m.get("visit_count"));
            total = vis/cnt;
        }
        list = Common.parseDashboardResult(category_name, 30, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.visits_less",locale), "visit_count"}}, key, total, list, "percent", false);
    }

    private void getHealRates(){
        //70% Heal Rate by 3rd week - Order 18
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "assessment_wound");
        ResultSet result = getResults("select (count(DISTINCT(ae.alpha_id))) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + "   exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
        
        HashMap m = getColumns(result, "heal_rate");
        double total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("heal_rate"));
            result = getResults("select count(distinct(ae.alpha_id)) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + " auto_referral_type = 'Heal Rate' and  exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
            m = getColumns(result, "heal_rate");
            double heal = Double.parseDouble((String) m.get("heal_rate"));
            total = heal/cnt;
        }
        list = Common.parseDashboardResult(category_name, 20, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_rate70",locale), "heal_rate"}}, key, total, list, "percent", false);
        
        //30% Heal Rate by 3rd week - Order 19
        builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "assessment_wound");
        result = getResults("select (count(DISTINCT(ae.alpha_id))) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + "   exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and  visited_on<'" + endDate + "' " + treatment_query + ")");
        
        m = getColumns(result, "heal_rate");
        total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("heal_rate"));
            result = getResults("select count(distinct(ae.alpha_id)) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + " auto_referral_type = 'Heal Rate' and  exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
            m = getColumns(result, "heal_rate");
            double heal = Double.parseDouble((String) m.get("heal_rate"));
            total = heal/cnt;
            total = 1 - total;
        }
        list = Common.parseDashboardResult(category_name, 21, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_rate30",locale), "heal_rate"}}, key, total, list, "percent", false);
    }
    
    private void getWoundsDaysToHeal(){

        //existing wounds
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment_location");
        ResultSet result = getResults("select count(wound_assessment_location.id) as count from wound_assessment_location " + builtQuery + "  close_time_stamp>'"+startDate+"' and open_time_stamp<'" + endDate + "'  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 8, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.existing_wounds",locale), "count"}}, key, result, list, "integer", false);
        
        //new wounds
        result = getResults("select count(wound_assessment_location.id) as count from wound_assessment_location "+  builtQuery  +"  open_time_stamp>'"+  startDate  +"'  AND open_time_stamp<'"  +endDate+  "' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 9, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.new_wounds",locale), "count"}}, key, result, list, "integer", false);
        
        //closed wounds
        result = getResults("select count(wound_assessment_location.id) as count from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp>'"+  startDate  +"'  AND close_time_stamp<'" + endDate+  "' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 10, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.closed_wounds",locale), "count"}}, key, result, list, "integer", false);
        
        //avg days to heal.. wounds closed during date range.
        result = getResults("select SUM(((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))))/SUM(discharge)  as time_to_heal from wound_assessment_location " + builtQuery+  " discharge=1 and close_time_stamp IS NOT NULL and open_time_stamp IS NOT NULL  and close_time_stamp>'"  +startDate+  "' and close_time_stamp<'"+  endDate  +"' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 12, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_heal_days",locale), "time_to_heal"}}, key, result, list, "integer", false);
        
        //avg days to heal between 1-30 wounds closed during date range.
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>0 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <30   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 13, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days1-30",locale), "time_to_heal"}}, key, result, list, "integer", false);
        
        //avg days to heal between 31-91 wounds closed during date range.
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=31 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <92   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 14, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days31-90",locale), "time_to_heal"}}, key, result, list, "integer", false);
        
        //avg days to heal between 92-183 wounds closed during date range.
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=93 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <183  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + "))");
        list = Common.parseDashboardResult(category_name, 15, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days91-183",locale), "time_to_heal"}}, key, result, list, "integer", false);
        
        //avg days to heal between 184-365 wounds closed during date range.
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location " + builtQuery + " discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'" + startDate + "' and close_time_stamp<'" + endDate + "' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=184 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <365  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + "))");
        list = Common.parseDashboardResult(category_name, 16, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days184-365",locale), "time_to_heal"}}, key, result, list, "integer", false);
        
        //avg days to heal greater then 365 wounds closed during date range.
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location " + builtQuery + " discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'" + startDate + "' and close_time_stamp<'" + endDate + "' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>366   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + "))");
        list = Common.parseDashboardResult(category_name, 17, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days366",locale), "time_to_heal"}}, key, result, list, "integer", false);
    }

    private void getAdmittedDischarged(){
        //# of admitted
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "patient_accounts");
        ResultSet result = getResults("select COUNT(id) as count from patient_accounts  " + builtQuery + " action='ADMITTED' and created_on>'" + startDate + "' and created_on<'" + endDate + "'  " + treatment_query+" ");
        list = Common.parseDashboardResult(category_name, 2, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.admitted_patients",locale), "count"}}, key, result, list, "integer", false);
        
        //# of discharged
        result = getResults("select COUNT(id) as count,sum(case when discharge_reason = "+patientDischargeClosedID+" then 1 else 0 end) as healed,sum(case when discharge_reason = "+patientDischargeSelfcareID+" then 1 else 0 end) as self_care,sum(case when discharge_reason = "+patientDischargeOtherID+" then 1 else 0 end) as other from patient_accounts " + builtQuery + " account_status=0 and created_on>'" + startDate + "' and created_on<'" + endDate + "' and action='DISCHARGED'" + treatment_query+" ");
        list = Common.parseDashboardResult(category_name, 3, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients",locale), "count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 4, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients_healed",locale), "healed"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 5, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients_selfcare",locale), "self_care"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 6, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients_other",locale), "other"}}, key, result, list, "integer", false);
    }

    private void getDataAllWounds(){
        //all wounds
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");

        ResultSet result = getResults("select count(distinct(wal.id))/nullif(count(distinct(wound_assessment.patient_id)),0) as  avg_wounds,sum(visit)/nullif(count(distinct(wound_assessment.patient_id)),0) as avg_visits,count(distinct(wound_assessment.patient_id)) as patient_count,count(distinct(wal.id)) as wound_count,sum(visit) as visit_count,count(wound_assessment.id) as assessment_count,(sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 1, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_patients",locale), "patient_count"}}, key, result, list, "integer", false);
        //--------------------
        list = Common.parseDashboardResult(category_name, 7, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_allwound",locale), "wound_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 11, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_wounds",locale), "avg_wounds"}}, key, result, list, "float", false);
        //--------------------
        list = Common.parseDashboardResult(category_name, 22, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.visit_count",locale), "visit_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 23, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.assessment_count",locale), "assessment_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 28, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_visit",locale), "avg_visits"}}, key, result, list, "integer", false);
        //--------------------
        list = Common.parseDashboardResult(category_name, 31, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_all_wounds",locale), "avgcost_bywound_" + key}}, key, result, list, "currency", false);
        list = Common.parseDashboardResult(category_name, 32, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_product_cost_all_wounds",locale), "totalcost_bywound_" + key}}, key, result, list, "currency", false);
    }
}
