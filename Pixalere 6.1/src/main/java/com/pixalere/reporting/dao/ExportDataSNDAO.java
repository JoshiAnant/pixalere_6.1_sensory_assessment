/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.dao;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.ArrayValueObject;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.patient.bean.ExternalReferralVO;
import com.pixalere.patient.bean.PatientProfileArraysVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.reporting.bean.ExportDataSNConverter;
import com.pixalere.reporting.bean.ExportDataSNVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.Serialize;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * 
 * @since 6.1
 * @author Jose
 */
public class ExportDataSNDAO {
    protected Connection con = null;
    protected boolean mysql=false;
    private PatientProfileServiceImpl ppservice;
    
    public ExportDataSNDAO() {
        ppservice = new PatientProfileServiceImpl();
    }
    
    
    
    
    public List<ExportDataSNVO> getData(
            String startDate, 
            String endDate, 
            List<Integer> locationsList){
        String treatmentQuery = "";
        
        if (!locationsList.isEmpty()) {
            String locationFilterQuery = "";
            for (Integer location_id : locationsList) {
                locationFilterQuery = locationFilterQuery + " wound_assessment.treatment_location_id=" + location_id + " OR ";
            }
            // Remove last 'OR '
            locationFilterQuery = locationFilterQuery.substring(0, locationFilterQuery.length() - 3);
            // Set query
            treatmentQuery = " AND (" + locationFilterQuery + ")";
        }
        
        
        List<ExportDataSNVO> dataList = new ArrayList<ExportDataSNVO>();
        
        String mainQuery = "SELECT "
            + "    patient_accounts.id AS patient_id, "
            + "    COALESCE(patient_accounts.discharge_reason, 0) AS discharge_reason, "
            + "    COALESCE(patient_accounts.funding_source, 0) AS funding_source, "
            + "    COALESCE(patient_accounts.patient_residence, 0) AS patient_residence, "
            + "    COALESCE(patient_accounts.gender, 0) AS gender, "
            + "    COALESCE(patient_profiles.albumin, 0) AS albumin, "
            + "    COALESCE(patient_profiles.pre_albumin, 0) AS pre_albumin, "
            + "    wound_profiles.id AS woundprofile_id, "
            + "    COALESCE(wound_profiles.patient_limitations, 0) AS patient_limitations, "
            + "    COALESCE(wound_profiles.teaching_goals, 0) AS teaching_goals, "
            + "    COALESCE(wound_profiles.pressure_ulcer, 0) AS pressure_ulcer_stage, "
            + "    COALESCE(wound_profiles.type_of_ostomy, 0) AS type_of_ostomy, "
            + "    wound_assessment_location.id AS alpha_id, "
            + "    wound_profile_type.id as wound_profile_type_id, "
            + "    wound_profile_type.alpha_type as alpha_type, "
            + "    wound_assessment.id AS woundassessmentid, "
            + "    wound_assessment.visited_on, "
            + "    wound_assessment.treatment_location_id, "
            + "    assessment_npwt.id AS npwt_id, "
            + "    COALESCE(assessment_npwt.pressure_reading, 0) AS pressure_reading, "
            + "    COALESCE(assessment_npwt.initiated_by, 0) AS initiated_by, "
            + "    COALESCE(assessment_npwt.np_connector, 0) AS negative_pressure_connector, "
            + "    COALESCE(assessment_npwt.reason_for_ending, 0) AS reason_for_ending, "
            + "    COALESCE(assessment_npwt.goal_of_therapy, 0) AS goal_of_therapy, "
            + "    COALESCE(dressing_change_frequency.dcf, 0) AS dressing_change_frequency, "
            + "    COALESCE(nursing_care_plan.visit_frequency, 0) AS bnursing_vistit_frecuency, "
            + "    COALESCE(assessment_npwt.vendor_name, 0) AS vendor_name, "
            + "    COALESCE(assessment_npwt.machine_acquirement, 0) AS machine_acquirement "
            + "FROM patient_accounts "
            + "LEFT JOIN patient_profiles  "
            + "    ON patient_accounts.id = patient_profiles.patient_id "
            + "LEFT JOIN wound_profiles "
            + "    ON patient_accounts.id = wound_profiles.patient_id "
            + "LEFT JOIN wounds "
            + "    ON patient_accounts.id = wounds.patient_id "
            + "LEFT JOIN wound_assessment "
            + "    ON wounds.id = wound_assessment.wound_id "
            + "LEFT JOIN nursing_care_plan "
            + "    ON wound_assessment.id = nursing_care_plan.assessment_id "
            + "LEFT JOIN assessment_npwt "
            + "    ON wound_assessment.id = assessment_npwt.assessment_id "
            + "LEFT JOIN wound_profile_type "
            + "    ON wounds.id = wound_profile_type.wound_id "
            + "LEFT JOIN wound_assessment_location "
            + "    ON wound_profile_type.id = wound_assessment_location.wound_profile_type_id "
            + "LEFT JOIN dressing_change_frequency "
            + "    ON wound_assessment.id = dressing_change_frequency.assessment_id "
            + "        AND wound_assessment_location.id = dressing_change_frequency.alpha_id "
            + "WHERE "
            + "    patient_profiles.current_flag = 1 "
            + "    AND wound_profiles.current_flag = 1 "
            + "    AND wound_assessment.active = 1 "
            + "    AND wound_assessment.visited_on >= '" + startDate + "' "
            + "    AND wound_assessment.visited_on < '" + endDate + "' "
            + treatmentQuery
            + "ORDER BY patient_accounts.id, wound_profiles.id, wound_assessment_location.id, wound_assessment.id ";
        
        
        ResultSet mainResult = getResults(mainQuery);
        
        if (mainResult != null){
            boolean firstEntry = true;
            int prev_patient_id = 0;
            int prev_woundprofile_id = 0;
            int prev_alpha_id = 0;
            int prev_woundassessmentid = 0;
            
            int patient_id;
            int woundprofile_id;
            int alpha_id;
            int wound_profile_type_id;
            int woundassessmentid;
            char alpha_type;
            int npwt_id;
            
            ExportDataSNVO dataPatientLimbsMob = new ExportDataSNVO();
            ExportDataSNVO dataPatientArrays = new ExportDataSNVO();
            ExportDataSNVO dataPatientGEA = new ExportDataSNVO();
            ExportDataSNVO dataTreatmentArrays = new ExportDataSNVO();
            ExportDataSNVO dataAssessmentDetails = new ExportDataSNVO();
            try {
                while (mainResult.next()) {
                    ExportDataSNVO entry = extractMainEntry(mainResult);
                    
                    patient_id = mainResult.getInt("patient_id");
                    woundprofile_id = mainResult.getInt("woundprofile_id");
                    alpha_id = mainResult.getInt("alpha_id");
                    wound_profile_type_id = mainResult.getInt("wound_profile_type_id");
                    woundassessmentid = mainResult.getInt("woundassessmentid");
                    npwt_id = mainResult.getInt("npwt_id");
                    String alpha_type_string = mainResult.getString("alpha_type");
                    alpha_type = (alpha_type_string.trim().length() > 0)? alpha_type_string.charAt(0): 'A';
                    
                    if (prev_patient_id != patient_id){
                        // Get patient data: limbs mob 1:1
                        String limbsMobilityQuery =
                                "SELECT "
                                + (mysql? limbMobilityMysqlSelect: limbMobilityMSSQLSelect)
                                + "FROM patient_accounts "
                                + "LEFT JOIN limb_basic_assessment "
                                + "    ON patient_accounts.id = limb_basic_assessment.patient_id "
                                + "LEFT JOIN limb_adv_assessment "
                                + "    ON patient_accounts.id = limb_adv_assessment.patient_id "
                                + "LEFT JOIN foot_assessment "
                                + "    ON patient_accounts.id = foot_assessment.patient_id "
                                + "LEFT JOIN limb_upper_assessment "
                                + "    ON patient_accounts.id = limb_upper_assessment.patient_id "
                                + "WHERE "
                                + "    patient_accounts.id = " + patient_id + " "
                                + "    AND (limb_basic_assessment.active = 1 OR limb_basic_assessment.active IS NULL) "
                                + "    AND (limb_adv_assessment.active = 1 OR limb_adv_assessment.id IS NULL) "
                                + "    AND (foot_assessment.active = 1 OR foot_assessment.id IS NULL) "
                                + "    AND (limb_upper_assessment.active = 1 OR limb_upper_assessment.active IS NULL) ";
                        
                        ResultSet limbsMobResult = getResults(limbsMobilityQuery);
                        while (limbsMobResult.next()) {
                            dataPatientLimbsMob = extractLimbsMobilityEntry(limbsMobResult);
                        }
                        entry.copyLimbMobilityData(dataPatientLimbsMob);
                        
                        // Current Patient Profile
                        PatientProfileVO proftmp = new PatientProfileVO();
                        proftmp.setPatient_id(patient_id);
                        proftmp.setActive(1);
                        proftmp.setCurrent_flag(1);
                        PatientProfileVO currentProfile = ppservice.getPatientProfile(proftmp);
                        
                        // Arrays: Comorbidities, Interfering factors, Interfering meds, Ext referrals & Arranged by, Investigations
                        dataPatientArrays = extractPatientArrays(currentProfile);
                        entry.copyPatientArraysData(dataPatientArrays);
                    } else {
                        // Replicate current patient data: limbs mob, patient arrays
                        entry.copyLimbMobilityData(dataPatientLimbsMob);
                        entry.copyPatientArraysData(dataPatientArrays);
                    }
                    
                    if (prev_woundprofile_id != woundprofile_id || prev_alpha_id != alpha_id) {
                        // Get Etiology, Goals, Acquired
                        dataPatientGEA = extractPatientGoalsEtiologiesAcquired(woundprofile_id, alpha_id, alpha_type);
                        entry.copyPatientGoalsEtiologiesAcquiredData(dataPatientGEA);
                    } else {
                        // Replicate current Etiology, Goals, Acquired
                        entry.copyPatientGoalsEtiologiesAcquiredData(dataPatientGEA);
                    }
                    
                    // Note tofix: This ELSE branch is ever accessed? 
                    // Maybe the IF is unnecessary, if always goes to true
                    if (prev_woundassessmentid != woundassessmentid || prev_alpha_id != alpha_id) {
                        // Verify that NPTW really was applied to this assessment/alpha set
                        if (npwt_id > 0){
                            entry = verifyCurrentNPTWValues(entry, woundassessmentid, alpha_id);
                        }
                        
                        // Get Treatment Arrays
                        dataTreatmentArrays = extractTreatmentArrays(wound_profile_type_id, woundassessmentid, alpha_id);
                        entry.copyTreatmentArraysData(dataTreatmentArrays);
                        
                        // Get specific assessments
                        switch(alpha_type) {
                            case 'A':
                                dataAssessmentDetails = extractWoundAssessmentDetails(woundassessmentid, alpha_id);
                                break;
                            case 'O':
                                dataAssessmentDetails = extractOstomyAssessmentDetails(woundassessmentid, alpha_id);
                                break;
                            case 'T':
                            case 'I':
                                dataAssessmentDetails = extractPostopAssessmentDetails(woundassessmentid, alpha_id);
                                break;
                            case 'D':
                                dataAssessmentDetails = extractTubes_drainsAssessmentDetails(woundassessmentid, alpha_id);
                                break;
                            case 'S':
                                // TODO: Empty method, S&N didn't ask for Skin assessments values
                                dataAssessmentDetails = extractSkinAssessmentDetails(woundassessmentid, alpha_id);
                                break;
                        }
                        entry.copyAssessmentsDetailsData(dataAssessmentDetails);
                        
                    } else {
                        // Replicate current
                        entry.copyTreatmentArraysData(dataTreatmentArrays);
                        entry.copyAssessmentsDetailsData(dataAssessmentDetails);
                    }
                    
                    
                    // Convert to S&N Values
                    ExportDataSNConverter.convertToSNValues(entry);
                    
                    dataList.add(entry);
                    
                    // Store previous values
                    prev_patient_id = patient_id;
                    prev_woundprofile_id = woundprofile_id;
                    prev_alpha_id = alpha_id;
                    prev_woundassessmentid = woundassessmentid;
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ApplicationException e){
                e.printStackTrace();
            }
        }
        
        return dataList;
    }
                
    private ExportDataSNVO extractOstomyAssessmentDetails(int woundassessmentid, int alpha_id) throws ApplicationException, SQLException {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        String query =
                "SELECT "
                + "    id AS ostomyassessmentid, "
                + "    COALESCE(construction, 0) AS construction, "
                + "    COALESCE(devices, 0) AS devices, "
                + "    COALESCE(discharge_reason, 0) AS ostomy_discharge_reason, "
                + "    COALESCE(flange_pouch, 0) AS flange_pouch, "
                + "    COALESCE(urine_quantity, 0) AS urine_quantity, "
                + "    COALESCE(shape, 0) AS stoma_shape, "
                + "    COALESCE(nutritional_status, 0) AS nutritional_status, "
                + "    COALESCE(status, 0) AS status, "
                + "    drainage AS drainage, "
                + "    mucocutaneous_margin AS mucocutaneous_margin, "
                + "    perifistula_skin AS peri_fistula_skin, "
                + "    pouching AS concerns_for_pouching, "
                + "    profile AS profile, "
                + "    urine_colour AS urine_colour, "
                + "    urine_type AS urine_type, "
                + "    appearance AS skin_appearance, "
                + "    contour AS skin_contour, "
                + "    self_care_progress AS self_care_progress, "
                + "    periostomy_skin AS peri_ostomy_skin "
                + "FROM assessment_ostomy "
                + "WHERE  "
                + "    assessment_id = " + woundassessmentid + " "
                + "    AND alpha_id = " + alpha_id + " "
                + "    AND active = 1 ";
        
        ResultSet result = getResults(query);
        if (result != null){
            while (result.next()){
                entry.setOstomyassessmentid(Integer.toString(result.getInt("ostomyassessmentid")));
                entry.setConstruction(Integer.toString(result.getInt("construction")));
                entry.setDevices(Integer.toString(result.getInt("devices")));
                entry.setOstomy_discharge_reason(Integer.toString(result.getInt("ostomy_discharge_reason")));
                entry.setFlange_pouch(Integer.toString(result.getInt("flange_pouch")));
                entry.setUrine_quantity(Integer.toString(result.getInt("urine_quantity")));
                entry.setStoma_shape(Integer.toString(result.getInt("stoma_shape")));
                entry.setNutritional_status(Integer.toString(result.getInt("nutritional_status")));
                entry.setStatus(Integer.toString(result.getInt("status")));
                
                String drainage = Serialize.commaIze(result.getString("drainage"));
                String mucocutaneous_margin = Serialize.commaIze(result.getString("mucocutaneous_margin"));
                String peri_fistula_skin = Serialize.commaIze(result.getString("peri_fistula_skin"));
                String concerns_for_pouching = Serialize.commaIze(result.getString("concerns_for_pouching"));
                String profile = Serialize.commaIze(result.getString("profile"));
                String urine_colour = Serialize.commaIze(result.getString("urine_colour"));
                String urine_type = Serialize.commaIze(result.getString("urine_type"));
                String skin_appearance = Serialize.commaIze(result.getString("skin_appearance"));
                String skin_contour = Serialize.commaIze(result.getString("skin_contour"));
                String self_care_progress = Serialize.commaIze(result.getString("self_care_progress"));
                String peri_ostomy_skin = Serialize.commaIze(result.getString("peri_ostomy_skin"));


                entry.setDrainage(drainage.isEmpty()? "0": drainage);
                entry.setMucocutaneous_margin(mucocutaneous_margin.isEmpty()? "0": mucocutaneous_margin);
                entry.setPeri_fistula_skin(peri_fistula_skin.isEmpty()? "0": peri_fistula_skin);
                entry.setConcerns_for_pouching(concerns_for_pouching.isEmpty()? "0": concerns_for_pouching);
                entry.setProfile(profile.isEmpty()? "0": profile);
                entry.setUrine_colour(urine_colour.isEmpty()? "0": urine_colour);
                entry.setUrine_type(urine_type.isEmpty()? "0": urine_type);
                entry.setSkin_appearance(skin_appearance.isEmpty()? "0": skin_appearance);
                entry.setSkin_contour(skin_contour.isEmpty()? "0": skin_contour);
                entry.setSelf_care_progress(self_care_progress.isEmpty()? "0": self_care_progress);
                entry.setPeri_ostomy_skin(peri_ostomy_skin.isEmpty()? "0": peri_ostomy_skin);
            }
        }
        return entry;
    }

    private ExportDataSNVO extractPostopAssessmentDetails(int woundassessmentid, int alpha_id) throws ApplicationException, SQLException {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        String query= "SELECT "
                + "    id AS incisionassessmentid, "
                + "    COALESCE(status,0) AS incision_status, "
                + "    COALESCE(exudate_amount,0) AS incision_exudate_amount, "
                + "    COALESCE(discharge_reason,0) AS incision_discharge_reason, "
                + "    COALESCE(pain,0) AS pain, "
                + "    postop_management AS postop_management, "
                + "    incision_exudate AS incision_exudate, "
                + "    incision_closure_methods AS incision_closure_type "
                + "FROM assessment_incision "
                + "WHERE  "
                + "    assessment_id = " + woundassessmentid + " "
                + "    AND alpha_id = " + alpha_id + " "
                + "    AND active = 1 ";

        ResultSet result = getResults(query);
        if (result != null){
            while (result.next()){  
                entry.setIncisionassessmentid(Integer.toString(result.getInt("incisionassessmentid")));
                entry.setIncision_status(Integer.toString(result.getInt("incision_status")));
                entry.setIncision_exudate_amount(Integer.toString(result.getInt("incision_exudate_amount")));
                entry.setIncision_discharge_reason(Integer.toString(result.getInt("incision_discharge_reason")));
                entry.setPain(Integer.toString(result.getInt("pain")));
                
                String incision_exudate = Serialize.commaIze(result.getString("incision_exudate"));
                String incision_closure_type = Serialize.commaIze(result.getString("incision_closure_type"));
                String postop_management = Serialize.commaIze(result.getString("postop_management"));
                
                entry.setIncision_exudate(incision_exudate.isEmpty()? "0": incision_exudate);
                entry.setIncision_closure_type(incision_closure_type.isEmpty()? "0": incision_closure_type);
                entry.setPostop_management(postop_management.isEmpty()? "0": postop_management);
            }
        }
        
        return entry;
    }

    private ExportDataSNVO extractTubes_drainsAssessmentDetails(int woundassessmentid, int alpha_id) throws ApplicationException, SQLException {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        String query =
                "SELECT "
                + "    id AS drainassessmentid, "
                + "    COALESCE(type_of_drain, 0) AS type_of_drain, "
                + "    COALESCE(discharge_reason, 0) AS tubes_discharge_reason, "
                + (mysql
                    ? "    COALESCE(CONCAT(drain_odour1, ',', drain_odour2, ',', drain_odour3, ',', drain_odour4, ',', drain_odour5), '0') AS drainage_odour, "
                    : "    COALESCE(drain_odour1 + ',' drain_odour2 + ',' drain_odour3 + ',' drain_odour4 + ',' drain_odour5, '0') AS drainage_odour, ")
                + "    drain_characteristics1, "
                + "    drain_characteristics2, "
                + "    drain_characteristics3, "
                + "    drain_characteristics4, "
                + "    drain_characteristics5, "
                + "    peri_drain_area AS peri_drain_skin, "
                + "    drain_site AS drain_site "
                + "FROM assessment_drain "
                + "WHERE  "
                + "    assessment_id = " + woundassessmentid + " "
                + "    AND alpha_id = " + alpha_id + " "
                + "    AND active = 1 ";
        
        ResultSet result = getResults(query);
        if (result != null){
            while (result.next()){  
                entry.setDrainassessmentid(Integer.toString(result.getInt("drainassessmentid")));
                entry.setType_of_drain(Integer.toString(result.getInt("type_of_drain")));
                entry.setTubes_discharge_reason(Integer.toString(result.getInt("tubes_discharge_reason")));
                entry.setDrainage_odour(result.getString("drainage_odour"));
                
                String drain_characteristics1 = Serialize.commaIze(result.getString("drain_characteristics1"));
                String drain_characteristics2 = Serialize.commaIze(result.getString("drain_characteristics2"));
                String drain_characteristics3 = Serialize.commaIze(result.getString("drain_characteristics3"));
                String drain_characteristics4 = Serialize.commaIze(result.getString("drain_characteristics4"));
                String drain_characteristics5 = Serialize.commaIze(result.getString("drain_characteristics5"));
                String peri_drain_skin = Serialize.commaIze(result.getString("peri_drain_skin"));
                String drain_site = Serialize.commaIze(result.getString("drain_site"));

                entry.setPeri_drain_skin(peri_drain_skin.isEmpty()? "0": peri_drain_skin);
                entry.setDrain_site(drain_site.isEmpty()? "0": drain_site);
                
                String characteristics  = 
                    (drain_characteristics1.isEmpty()? "": drain_characteristics1 + ",")
                    + (drain_characteristics2.isEmpty()? "": drain_characteristics2 + ",")
                    + (drain_characteristics3.isEmpty()? "": drain_characteristics3 + ",")
                    + (drain_characteristics4.isEmpty()? "": drain_characteristics4 + ",")
                    + (drain_characteristics5.isEmpty()? "": drain_characteristics5 + ",");
                
                characteristics = (characteristics.isEmpty())? "0" : characteristics.substring(0, characteristics.length() -1);
                entry.setCharacteristics(characteristics);
            }
        }
        
        return entry;
    }

    private ExportDataSNVO extractSkinAssessmentDetails(int woundassessmentid, int alpha_id) throws ApplicationException, SQLException {
        ExportDataSNVO entry = new ExportDataSNVO();

        return entry;
    }
    
    private ExportDataSNVO extractWoundAssessmentDetails(int woundassessmentid, int alpha_id) throws ApplicationException, SQLException {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        String query =
                "SELECT "
                + "    id AS assessmentwoundid, "
                + "    COALESCE(exudate_amount, 0) AS exudate_amount, "
                + "    COALESCE(discharge_reason, 0) AS wound_discharge_reason, "
                + "    COALESCE(priority, 0) AS priority, "
                + "    exudate_type, "
                + "    wound_edge, "
                + "    periwound_skin "
                + "FROM assessment_wound "
                + "WHERE  "
                + "    assessment_id = " + woundassessmentid + " "
                + "    AND alpha_id = " + alpha_id + " "
                + "    AND active = 1 ";
        ResultSet result = getResults(query);
        int assessment_wound_id = 0;
        if (result != null){
            while (result.next()){  
                assessment_wound_id = result.getInt("assessmentwoundid");
                entry.setAssessmentwoundid(Integer.toString(assessment_wound_id));
                entry.setExudate_amount(Integer.toString(result.getInt("exudate_amount")));
                entry.setWound_discharge_reason(result.getString("wound_discharge_reason"));
                entry.setPriority(Integer.toString(result.getInt("priority")));

                String exudate_type = Serialize.commaIze(result.getString("exudate_type"));
                String wound_edge = Serialize.commaIze(result.getString("wound_edge"));
                String periwound_skin = Serialize.commaIze(result.getString("periwound_skin"));

                entry.setExudate_type(exudate_type.isEmpty()? "0": exudate_type);
                entry.setWound_edge(wound_edge.isEmpty()? "0": wound_edge);
                entry.setPeriwound_skin(periwound_skin.isEmpty()? "0": periwound_skin);
            }
        }
        if (assessment_wound_id > 0){
            String baseQuery =
                    "SELECT "
                    + "    wound_bed "
                    + "FROM assessment_woundbed "
                    + "WHERE "
                    + "    assessment_wound_id = " + assessment_wound_id + " "
                    + "    AND alpha_id = " + alpha_id + " ";
            result = getResults(baseQuery);
            String wound_base = "";
            if (result != null){
                while (result.next()){ 
                    wound_base += Integer.toString(result.getInt("wound_bed")) + ",";
                }
            }
            wound_base = (wound_base.isEmpty())? "0" : wound_base.substring(0, wound_base.length() -1);
            entry.setWound_base(wound_base);
        }
        
        return entry;
    }
    
    private ExportDataSNVO verifyCurrentNPTWValues(ExportDataSNVO entry, int woundassessmentid, int alpha_id) throws ApplicationException, SQLException  {
        String countQuery =
                "SELECT  "
                + "    COUNT(id) as count "
                + "FROM assessment_npwt_alpha "
                + "WHERE "
                + "    assessment_npwt_id IN (SELECT id FROM assessment_npwt WHERE assessment_id = " + woundassessmentid + ") "
                + "    AND alpha_id = " + alpha_id;
        ResultSet result = getResults(countQuery);
        int count = 0;
        if (result != null){
            while (result.next()){ 
                count = result.getInt("count");
            }
        }
        // Not applied, set nptw values to "0"
        if (count == 0){
            entry.cleanUpNPTWValues();
        }
        
        return entry;
    }
    
    private ExportDataSNVO extractTreatmentArrays(int wound_profile_type_id, int woundassessmentid, int alpha_id) throws ApplicationException, SQLException  {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        String c_s_resultQuery = 
            "SELECT "
            + "    assessment_treatment_arrays.lookup_id AS c_s_result "
            + "FROM assessment_treatment "
            + "LEFT JOIN assessment_treatment_arrays "
            + "    ON assessment_treatment.id = assessment_treatment_arrays.assessment_treatment_id "
            + "WHERE "
            + "    assessment_treatment.wound_profile_type_id = " + wound_profile_type_id + " "
            + "    AND assessment_treatment.assessment_id = " + woundassessmentid + " "
            + "    AND assessment_treatment.alpha_id = " + alpha_id + " "
            + "    AND assessment_treatment_arrays.resource_id = 38 "
            + "    AND assessment_treatment_arrays.lookup_id IS NOT NULL ";
        String adjunctiveQuery =
            "SELECT "
            + "    assessment_treatment_arrays.lookup_id AS adjunctive_treatment_modalities "
            + "FROM assessment_treatment "
            + "LEFT JOIN assessment_treatment_arrays "
            + "    ON assessment_treatment.id = assessment_treatment_arrays.assessment_treatment_id "
            + "WHERE "
            + "    assessment_treatment.wound_profile_type_id = " + wound_profile_type_id + " "
            + "    AND assessment_treatment.assessment_id = " + woundassessmentid + " "
            + "    AND assessment_treatment.alpha_id = " + alpha_id + " "
            + "    AND assessment_treatment_arrays.resource_id = 17 "
            + "    AND assessment_treatment_arrays.lookup_id IS NOT NULL ";
        
        String c_s_result = "";
        String adjunctive_treatment_modalities = "";
        
        ResultSet result = getResults(c_s_resultQuery);
        if (result != null){
            while (result.next()){
                c_s_result = Integer.toString(result.getInt("c_s_result")) + ",";
            }
        }
        c_s_result = (c_s_result.isEmpty())? "0" : c_s_result.substring(0, c_s_result.length() -1);
        entry.setC_s_result(c_s_result);

        result = getResults(adjunctiveQuery);
        if (result != null){
            while (result.next()){
                adjunctive_treatment_modalities = Integer.toString(result.getInt("adjunctive_treatment_modalities")) + ",";
            }
        }
        adjunctive_treatment_modalities = (adjunctive_treatment_modalities.isEmpty())? "0" : adjunctive_treatment_modalities.substring(0, adjunctive_treatment_modalities.length() -1);
        entry.setAdjunctive_treatment_modalities(adjunctive_treatment_modalities);
        
        return entry;
    }
    
    private ExportDataSNVO extractPatientGoalsEtiologiesAcquired(int woundprofile_id, int alpha_id, char alpha_type) throws ApplicationException, SQLException {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        String etiology = "";
        String goals = "";
        String acquired = "";
        
        String queryEtiology =
            "SELECT "
            + "    lookup_id "
            + "FROM wound_etiology "
            + "WHERE "
            + "    wound_profile_id = " + woundprofile_id + " "
            + "    AND alpha_id = " + alpha_id + " ";
        String queryGoals =
            "SELECT "
            + "    lookup_id "
            + "FROM wound_goals "
            + "WHERE "
            + "    wound_profile_id = " + woundprofile_id + " "
            + "    AND alpha_id = " + alpha_id + " ";
        String queryAcquired =
            "SELECT "
            + "    lookup_id "
            + "FROM wound_acquired "
            + "WHERE "
            + "    wound_profile_id = " + woundprofile_id + " "
            + "    AND alpha_id = " + alpha_id + " ";
        
        ResultSet result = getResults(queryEtiology);
        String wound_etiology = "";
        String ostomy_etiology = "";
        String postop_etiology = "";
        String tubes_drains_etiology = "";
        String skin_etiology = "";
        if (result != null){
            while (result.next()){
                etiology = Integer.toString(result.getInt("lookup_id"));
            }
            switch(alpha_type) {
                case 'A':
                    wound_etiology = etiology;
                    break;
                case 'O':
                    ostomy_etiology = etiology;
                    break;
                case 'T':
                case 'I':
                    postop_etiology = etiology;
                    break;
                case 'D':
                    tubes_drains_etiology = etiology;
                    break;
                case 'S':
                    skin_etiology = etiology;
                    break;

            }
        }
        entry.setWound_etiology(wound_etiology.isEmpty()? "0": wound_etiology);
        entry.setOstomy_etiology(ostomy_etiology.isEmpty()? "0": ostomy_etiology);
        entry.setPostop_etiology(postop_etiology.isEmpty()? "0": postop_etiology);
        entry.setTubes_drains_etiology(tubes_drains_etiology.isEmpty()? "0": tubes_drains_etiology);
        entry.setSkin_etiology(skin_etiology.isEmpty()? "0": skin_etiology);
        
        result = getResults(queryGoals);
        String goal_of_healing_wound = "";
        String goal_of_healing_ostomy = "";
        String goal_of_healing_incision = "";
        String goal_of_healing_drain = "";
        String goal_of_healing_skin = "";
        if (result != null){
            while (result.next()){
                goals = Integer.toString(result.getInt("lookup_id"));
            }
            switch(alpha_type) {
                case 'A':
                    goal_of_healing_wound = goals;
                    break;
                case 'O':
                    goal_of_healing_ostomy = goals;
                    break;
                case 'T':
                case 'I':
                    goal_of_healing_incision = goals;
                    break;
                case 'D':
                    goal_of_healing_drain = goals;
                    break;
                case 'S':
                    goal_of_healing_skin = goals;
                    break;

            }
        }
        entry.setGoal_of_healing_wound(goal_of_healing_wound.isEmpty()? "0": goal_of_healing_wound);
        entry.setGoal_of_healing_ostomy(goal_of_healing_ostomy.isEmpty()? "0": goal_of_healing_ostomy);
        entry.setGoal_of_healing_incision(goal_of_healing_incision.isEmpty()? "0": goal_of_healing_incision);
        entry.setGoal_of_healing_drain(goal_of_healing_drain.isEmpty()? "0": goal_of_healing_drain);
        entry.setGoal_of_healing_skin(goal_of_healing_skin.isEmpty()? "0": goal_of_healing_skin);
        
        result = getResults(queryAcquired);
        if (result != null){
            while (result.next()){
                acquired = Integer.toString(result.getInt("lookup_id"));
            }
        }
        entry.setAcquired(acquired.isEmpty()? "0": acquired);
        
        return entry;
    }
    
   
    private ExportDataSNVO extractPatientArrays(PatientProfileVO currentProfile) throws ApplicationException, SQLException {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        if (currentProfile != null) {
            try {
                Collection<PatientProfileArraysVO> arrays = currentProfile.getPatient_profile_arrays();
                PatientProfileArraysVO[] rr = new PatientProfileArraysVO[arrays.size()];
                rr = arrays.toArray(rr);
                
                List<ArrayValueObject> listComorbidities = Common.filterArrayObjects(rr, LookupVO.COMORBIDITIES);
                String co_morbidities_endocrine = "";
                String co_morbidities_heart = "";
                String co_morbidities_musculoeskeletal = "";
                String co_morbidities_neurological = "";
                String co_morbidities_psychiatrric_mood = "";
                String co_morbidities_pulmonary = "";
                String co_morbidities_sensory = "";
                String co_morbidities_infections = "";
                String co_morbidities_other = "";
                for (ArrayValueObject vo : listComorbidities) {

                    switch (vo.getLookup().getResourceId()){
                        case 41: co_morbidities_endocrine += vo.getLookup_id() + ",";
                                break;
                        case 42: co_morbidities_heart += vo.getLookup_id() + ",";
                                break;
                        case 43: co_morbidities_musculoeskeletal += vo.getLookup_id() + ",";
                                break;
                        case 44: co_morbidities_neurological += vo.getLookup_id() + ",";
                                break;
                        case 45: co_morbidities_psychiatrric_mood += vo.getLookup_id() + ",";
                                break;
                        case 46: co_morbidities_pulmonary += vo.getLookup_id() + ",";
                                break;
                        case 47: co_morbidities_sensory += vo.getLookup_id() + ",";
                                break;
                        case 48: co_morbidities_infections += vo.getLookup_id() + ",";
                                break;
                        case 49: co_morbidities_other += vo.getLookup_id() + ",";
                                break;
                    }

                }

                co_morbidities_endocrine = (co_morbidities_endocrine.isEmpty())? "0" : co_morbidities_endocrine.substring(0, co_morbidities_endocrine.length() -1);
                co_morbidities_heart = (co_morbidities_heart.isEmpty())? "0" : co_morbidities_heart.substring(0, co_morbidities_heart.length() -1);
                co_morbidities_musculoeskeletal = (co_morbidities_musculoeskeletal.isEmpty())? "0" : co_morbidities_musculoeskeletal.substring(0, co_morbidities_musculoeskeletal.length() -1);
                co_morbidities_neurological = (co_morbidities_neurological.isEmpty())? "0" : co_morbidities_neurological.substring(0, co_morbidities_neurological.length() -1);
                co_morbidities_psychiatrric_mood = (co_morbidities_psychiatrric_mood.isEmpty())? "0" : co_morbidities_psychiatrric_mood.substring(0, co_morbidities_psychiatrric_mood.length() -1);
                co_morbidities_pulmonary = (co_morbidities_pulmonary.isEmpty())? "0" : co_morbidities_pulmonary.substring(0, co_morbidities_pulmonary.length() -1);
                co_morbidities_sensory = (co_morbidities_sensory.isEmpty())? "0" : co_morbidities_sensory.substring(0, co_morbidities_sensory.length() -1);
                co_morbidities_infections = (co_morbidities_infections.isEmpty())? "0" : co_morbidities_infections.substring(0, co_morbidities_infections.length() -1);
                co_morbidities_other = (co_morbidities_other.isEmpty())? "0" : co_morbidities_other.substring(0, co_morbidities_other.length() -1);

                entry.setCo_morbidities_endocrine(co_morbidities_endocrine);
                entry.setCo_morbidities_heart(co_morbidities_heart);
                entry.setCo_morbidities_musculoeskeletal(co_morbidities_musculoeskeletal);
                entry.setCo_morbidities_neurological(co_morbidities_neurological);
                entry.setCo_morbidities_psychiatrric_mood(co_morbidities_psychiatrric_mood);
                entry.setCo_morbidities_pulmonary(co_morbidities_pulmonary);
                entry.setCo_morbidities_sensory(co_morbidities_sensory);
                entry.setCo_morbidities_infections(co_morbidities_infections);
                entry.setCo_morbidities_other(co_morbidities_other);
                
                // Interfering Factors
                List<ArrayValueObject> listFactors = Common.filterArrayObjects(rr, LookupVO.INTERFERING_MEDS);
                String interfering_factors = "";
                for (ArrayValueObject vo : listFactors) {
                    interfering_factors += vo.getLookup_id() + ",";
                }
                interfering_factors = (interfering_factors.isEmpty())? "0" : interfering_factors.substring(0, interfering_factors.length() -1);
                entry.setInterfering_factors(interfering_factors);
                
                // Interfering Meds
                List<ArrayValueObject> listMeds = Common.filterArrayObjects(rr, LookupVO.MED_COMMENTS);
                String interfering_medications = "";
                for (ArrayValueObject vo : listMeds) {
                    interfering_medications += vo.getLookup_id() + ",";
                }
                interfering_medications = (interfering_medications.isEmpty())? "0" : interfering_medications.substring(0, interfering_medications.length() -1);
                entry.setInterfering_medications(interfering_medications);
                
                // External referrals & arranged
                String external_referrals = "";
                String arranged = "";
                for(ExternalReferralVO ref : currentProfile.getExternalReferrals()){
                    external_referrals += ref.getExternal_referral_id() + ",";
                    arranged += ref.getArranged_id() + ",";
                }
                external_referrals = (external_referrals.isEmpty())? "0" : external_referrals.substring(0, external_referrals.length() -1);
                arranged = (arranged.isEmpty())? "0" : arranged.substring(0, arranged.length() -1);
                
                entry.setExternal_referrals(external_referrals);
                entry.setArranged(arranged);
                
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            
            String query = 
                    "SELECT "
                    + "    patient_investigations.lookup_id AS labs_investigations "
                    + "FROM diagnostic_investigation "
                    + "LEFT JOIN patient_investigations "
                    + "    ON diagnostic_investigation.id = patient_investigations.diagnostic_id "
                    + "WHERE "
                    + "    diagnostic_investigation.patient_id = " + currentProfile.getPatient_id() + " "
                    + "    AND diagnostic_investigation.active = 1 "
                    + "    AND patient_investigations.lookup_id IS NOT NULL ";

            String labs_investigations = "0";
            ResultSet result = getResults(query);
            if (result != null){
                labs_investigations = "";
                while (result.next()){
                    int lookup_id = result.getInt("labs_investigations");
                    labs_investigations += lookup_id + ",";
                }
                labs_investigations = (labs_investigations.isEmpty())? "0" : labs_investigations.substring(0, labs_investigations.length() -1);
            }
            entry.setLabs_investigations(labs_investigations);
        }
        
        return entry;
    }
    
    private ExportDataSNVO extractLimbsMobilityEntry(ResultSet result) throws SQLException, ApplicationException {
        ExportDataSNVO entry = new ExportDataSNVO();
        // Single values
        entry.setWeight_bearing_status(Integer.toString(result.getInt("weight_bearing_status")));
        entry.setBalance(Integer.toString(result.getInt("balance")));
        entry.setCalf_muscle_function(Integer.toString(result.getInt("calf_muscle_function")));
        entry.setMobility_aids(Integer.toString(result.getInt("mobility_aids")));
        entry.setMuscle_tone(Integer.toString(result.getInt("muscle_tone")));
        entry.setArches(Integer.toString(result.getInt("arches")));
        
        // Concatenated string values
        entry.setBasic_limb_temperature(result.getString("basic_limb_temperature"));
        entry.setLimb_edema(result.getString("limb_edema"));
        entry.setLimb_edema_severity(result.getString("limb_edema_severity"));
        entry.setPalpation(result.getString("palpation"));
        entry.setColor(result.getString("color"));
        entry.setBasic_skin_assessments(result.getString("basic_skin_assessments"));
        entry.setProprioception(result.getString("proprioception"));
        entry.setLab_done(result.getString("lab_done"));
        entry.setUpper_limb_edema_location(result.getString("upper_limb_edema_location"));
        entry.setAdv_lim_temperature(result.getString("adv_lim_temperature"));

        // Unserialize Old fields
        String missing_limbs = Serialize.commaIze(result.getString("missing_limbs"));
        String basic_pain_assessments = Serialize.commaIze(result.getString("basic_pain_assessments"));
        String adv_pain_assessments = Serialize.commaIze(result.getString("adv_pain_assessments"));
        String sensory = Serialize.commaIze(result.getString("sensory"));
        String deformities = Serialize.commaIze(result.getString("deformities"));
        String toes = Serialize.commaIze(result.getString("toes"));
        String foot_active = Serialize.commaIze(result.getString("foot_active"));
        String foot_passive = Serialize.commaIze(result.getString("foot_passive"));
        String missing_upper_limb = Serialize.commaIze(result.getString("missing_upper_limb"));
        
        entry.setMissing_limbs(missing_limbs.isEmpty()? "0": missing_limbs);
        entry.setBasic_pain_assessments(basic_pain_assessments.isEmpty()? "0": basic_pain_assessments);
        entry.setAdv_pain_assessments(adv_pain_assessments.isEmpty()? "0": adv_pain_assessments);
        entry.setSensory(sensory.isEmpty()? "0": sensory);
        entry.setDeformities(deformities.isEmpty()? "0": deformities);
        entry.setToes(toes.isEmpty()? "0": toes);
        entry.setFoot_active(foot_active.isEmpty()? "0": foot_active);
        entry.setFoot_passive(foot_passive.isEmpty()? "0": foot_passive);
        entry.setMissing_upper_limb(missing_upper_limb.isEmpty()? "0": missing_upper_limb);
        
        return entry;
    }
    
    private ExportDataSNVO extractMainEntry(ResultSet result) throws SQLException, ApplicationException {
        ExportDataSNVO entry = new ExportDataSNVO();
        
        entry.setPatient_id(Integer.toString(result.getInt("patient_id")));
        entry.setDischarge_reason(Integer.toString(result.getInt("discharge_reason")));
        entry.setFunding_source(Integer.toString(result.getInt("funding_source")));
        entry.setPatient_residence(Integer.toString(result.getInt("patient_residence")));
        entry.setGender(Integer.toString(result.getInt("gender")));
        entry.setAlbumin(Integer.toString(result.getInt("albumin")));
        entry.setPre_albumin(Integer.toString(result.getInt("pre_albumin")));
        entry.setWoundprofile_id(Integer.toString(result.getInt("woundprofile_id")));
        entry.setPatient_limitations(Integer.toString(result.getInt("patient_limitations")));
        entry.setTeaching_goals(Integer.toString(result.getInt("teaching_goals")));
        entry.setPressure_ulcer_stage(Integer.toString(result.getInt("pressure_ulcer_stage")));
        entry.setAlpha_id(Integer.toString(result.getInt("alpha_id")));
        entry.setWoundassessmentid(Integer.toString(result.getInt("woundassessmentid")));
        entry.setPressure_reading(Integer.toString(result.getInt("pressure_reading")));
        entry.setInitiated_by(Integer.toString(result.getInt("initiated_by")));
        entry.setNegative_pressure_connector(Integer.toString(result.getInt("negative_pressure_connector")));
        entry.setReason_for_ending(Integer.toString(result.getInt("reason_for_ending")));
        entry.setGoal_of_therapy(Integer.toString(result.getInt("goal_of_therapy")));
        entry.setDressing_change_frequency(Integer.toString(result.getInt("dressing_change_frequency")));
        entry.setBnursing_vistit_frecuency(Integer.toString(result.getInt("bnursing_vistit_frecuency")));
        entry.setVendor_name(Integer.toString(result.getInt("vendor_name")));
        entry.setMachine_acquirement(Integer.toString(result.getInt("machine_acquirement")));
        
        String type_of_ostomy = Serialize.commaIze(result.getString("type_of_ostomy"));
        entry.setType_of_ostomy(type_of_ostomy.isEmpty()? "0": type_of_ostomy);
        
        return entry;
    }
    
    public ResultSet getResults(String query) {
        try {
            PreparedStatement state = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
            return result;
        } catch (SQLException s) {
            s.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }
        return null;
    }
    
    public void connect() {
        DataSource ds = null;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
            con = ds.getConnection();
            
            // Detect DBMS
            checkIsMySQL();
            
        } catch (NamingException ne) {
            ne.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void disconnect() {
        try {
            if (con != null && con.isClosed()==false) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected void checkIsMySQL(){
        //select query with top 1... if it fails.. its mysql.
        try{
            PreparedStatement state = con.prepareStatement("select top 1 from configuration", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
        }catch(com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException e){
            mysql=true;
        }catch(Exception e){
            if(e.getMessage().indexOf("MySQL")!=-1){
                mysql=true;
            }
        }
        
    }
    
    private static final String limbMobilityMysqlSelect = 
        "    COALESCE(limb_basic_assessment.active, 0) as limb_ba_active, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_temperature_leg, ',', limb_basic_assessment.right_temperature_leg, ',', limb_basic_assessment.left_temperature_foot, ',', limb_basic_assessment.right_temperature_foot, ',', limb_basic_assessment.left_temperature_toes, ',', limb_basic_assessment.right_temperature_toes), 0) AS basic_limb_temperature, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_edema_location, ',', limb_basic_assessment.right_edema_location), 0) AS limb_edema, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_edema_severity, ',', limb_basic_assessment.right_edema_severity), 0) AS limb_edema_severity, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_dorsalis_pedis_palpation, ',', limb_basic_assessment.right_dorsalis_pedis_palpation, ',', limb_basic_assessment.left_posterior_tibial_palpation, ',', limb_basic_assessment.right_posterior_tibial_palpation), 0) AS palpation, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_missing_limbs, ',', limb_basic_assessment.right_missing_limbs), 0) AS missing_limbs, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_skin_colour_leg, ',', limb_basic_assessment.right_skin_colour_leg, ',', limb_basic_assessment.left_skin_colour_foot, ',', limb_basic_assessment.right_skin_colour_foot, ',', limb_basic_assessment.left_skin_colour_toes, ',', limb_basic_assessment.right_skin_colour_toes), 0) AS color, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_pain_assessment, ',', limb_basic_assessment.right_pain_assessment), 0) AS basic_pain_assessments, "
        + "    COALESCE(limb_adv_assessment.active, 0) as limb_adv_active, "
        + "    COALESCE(CONCAT(limb_adv_assessment.left_pain_assessment, ',', limb_adv_assessment.right_pain_assessment), 0) AS adv_pain_assessments, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_skin_colour_leg, ',', limb_basic_assessment.right_skin_colour_leg, ',', limb_basic_assessment.left_skin_colour_foot, ',', limb_basic_assessment.right_skin_colour_foot, ',', limb_basic_assessment.left_skin_colour_toes, ',', limb_basic_assessment.right_skin_colour_toes), 0) AS basic_skin_assessments, "
        + "    COALESCE(CONCAT(limb_basic_assessment.left_sensory, ',', limb_basic_assessment.right_sensory), 0) AS sensory, "
        + "    COALESCE(CONCAT(foot_assessment.left_proprioception, ',', foot_assessment.right_proprioception), 0) AS proprioception, "
        + "    COALESCE(CONCAT(limb_adv_assessment.left_foot_deformities, ',', limb_adv_assessment.right_foot_deformities), 0) AS deformities, "
        + "    COALESCE(CONCAT(limb_adv_assessment.left_foot_toes, ',', limb_adv_assessment.right_foot_toes), 0) AS toes, "
        + "    COALESCE(foot_assessment.active, 0) as foot_ass_active, "
        + "    COALESCE(foot_assessment.weight_bearing, 0) AS weight_bearing_status, "
        + "    COALESCE(foot_assessment.balance, 0) AS balance, "
        + "    COALESCE(foot_assessment.calf_muscle_pump, 0) AS calf_muscle_function, "
        + "    COALESCE(foot_assessment.mobility_aids, 0) AS mobility_aids, "
        + "    COALESCE(foot_assessment.muscle_tone_functional, 0) AS muscle_tone, "
        + "    COALESCE(foot_assessment.arches_foot_structure, 0) AS arches, "
        + "    COALESCE(foot_assessment.dorsiflexion_active, 0) AS foot_active, "
        + "    COALESCE(foot_assessment.dorsiflexion_passive, 0) AS foot_passive, "
        + "    COALESCE(CONCAT(limb_adv_assessment.toe_brachial_lab, ',', limb_adv_assessment.ankle_brachial_lab), 0) AS lab_done, "
        + "    COALESCE(limb_upper_assessment.active, 0) as limb_up_active, "
        + "    COALESCE(CONCAT(limb_upper_assessment.left_missing_limbs, ',', limb_upper_assessment.right_missing_limbs), 0) AS missing_upper_limb, "
        + "    COALESCE(CONCAT(limb_upper_assessment.left_edema_location, ',', limb_upper_assessment.right_edema_location), 0) AS upper_limb_edema_location, "
        + "    COALESCE(CONCAT(limb_adv_assessment.left_temperature_leg, ',', limb_adv_assessment.right_temperature_leg, ',', limb_adv_assessment.left_temperature_foot, ',', limb_adv_assessment.right_temperature_foot, ',', limb_adv_assessment.left_temperature_toes, ',', limb_adv_assessment.right_temperature_toes), 0) AS adv_lim_temperature ";
    
    private static final String limbMobilityMSSQLSelect = 
        "    COALESCE(limb_basic_assessment.active, 0) AS limb_ba_active, "
        + "    COALESCE(limb_basic_assessment.left_temperature_leg + ',' + limb_basic_assessment.right_temperature_leg + ',' + limb_basic_assessment.left_temperature_foot + ',' + limb_basic_assessment.right_temperature_foot + ',' + limb_basic_assessment.left_temperature_toes + ',' + limb_basic_assessment.right_temperature_toes, 0) AS basic_limb_temperature, "
        + "    COALESCE(limb_basic_assessment.left_edema_location + ',' + limb_basic_assessment.right_edema_location, 0) AS limb_edema, "
        + "    COALESCE(limb_basic_assessment.left_edema_severity + ',' + limb_basic_assessment.right_edema_severity, 0) AS limb_edema_severity, "
        + "    COALESCE(limb_basic_assessment.left_dorsalis_pedis_palpation + ',' + limb_basic_assessment.right_dorsalis_pedis_palpation + ',' + limb_basic_assessment.left_posterior_tibial_palpation + ',' + limb_basic_assessment.right_posterior_tibial_palpation, 0) AS palpation, "
        + "    COALESCE(limb_basic_assessment.left_missing_limbs + ',' + limb_basic_assessment.right_missing_limbs, 0) AS missing_limbs, "
        + "    COALESCE(limb_basic_assessment.left_skin_colour_leg + ',' + limb_basic_assessment.right_skin_colour_leg + ',' + limb_basic_assessment.left_skin_colour_foot + ',' + limb_basic_assessment.right_skin_colour_foot + ',' + limb_basic_assessment.left_skin_colour_toes + ',' + limb_basic_assessment.right_skin_colour_toes, 0) AS color, "
        + "    COALESCE(limb_basic_assessment.left_pain_assessment + ',' + limb_basic_assessment.right_pain_assessment, 0) AS basic_pain_assessments, "
        + "    COALESCE(limb_adv_assessment.active, 0) AS limb_adv_active, "
        + "    COALESCE(limb_adv_assessment.left_pain_assessment + ',' + limb_adv_assessment.right_pain_assessment, 0) AS adv_pain_assessments, "
        + "    COALESCE(limb_basic_assessment.left_skin_colour_leg + ',' + limb_basic_assessment.right_skin_colour_leg + ',' + limb_basic_assessment.left_skin_colour_foot + ',' + limb_basic_assessment.right_skin_colour_foot + ',' + limb_basic_assessment.left_skin_colour_toes + ',' + limb_basic_assessment.right_skin_colour_toes, 0) AS basic_skin_assessments, "
        + "    COALESCE(limb_basic_assessment.left_sensory + ',' + limb_basic_assessment.right_sensory, 0) AS sensory, "
        + "    COALESCE(foot_assessment.left_proprioception + ',' + foot_assessment.right_proprioception, 0) AS proprioception, "
        + "    COALESCE(limb_adv_assessment.left_foot_deformities + ',' + limb_adv_assessment.right_foot_deformities, 0) AS deformities, "
        + "    COALESCE(limb_adv_assessment.left_foot_toes + ',' + limb_adv_assessment.right_foot_toes, 0) AS toes, "
        + "    COALESCE(foot_assessment.active, 0) AS foot_ass_active, "
        + "    COALESCE(foot_assessment.weight_bearing, 0) AS weight_bearing_status, "
        + "    COALESCE(foot_assessment.balance, 0) AS balance, "
        + "    COALESCE(foot_assessment.calf_muscle_pump, 0) AS calf_muscle_function, "
        + "    COALESCE(foot_assessment.mobility_aids, 0) AS mobility_aids, "
        + "    COALESCE(foot_assessment.muscle_tone_functional, 0) AS muscle_tone, "
        + "    COALESCE(foot_assessment.arches_foot_structure, 0) AS arches, "
        + "    COALESCE(foot_assessment.dorsiflexion_active, 0) AS foot_active, "
        + "    COALESCE(foot_assessment.dorsiflexion_passive, 0) AS foot_passive, "
        + "    COALESCE(limb_adv_assessment.toe_brachial_lab + ',' + limb_adv_assessment.ankle_brachial_lab, 0) AS lab_done, "
        + "    COALESCE(limb_upper_assessment.active, 0) AS limb_up_active, "
        + "    COALESCE(limb_upper_assessment.left_missing_limbs + ',' + limb_upper_assessment.right_missing_limbs, 0) AS missing_upper_limb, "
        + "    COALESCE(limb_upper_assessment.left_edema_location + ',' + limb_upper_assessment.right_edema_location, 0) AS upper_limb_edema_location, "
        + "    COALESCE(limb_adv_assessment.left_temperature_leg + ',' + limb_adv_assessment.right_temperature_leg + ',' + limb_adv_assessment.left_temperature_foot + ',' + limb_adv_assessment.right_temperature_foot + ',' + limb_adv_assessment.left_temperature_toes + ',' + limb_adv_assessment.right_temperature_toes, 0) AS adv_lim_temperature ";
            
}
