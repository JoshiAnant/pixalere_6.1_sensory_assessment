/*
 * ReportDAO.java
 *
 * Created on May 11, 2007, 5:11 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.pixalere.reporting.dao;
import java.util.Enumeration;
import com.pixalere.reporting.bean.CCACReportVO;
import java.util.Date;
import com.pixalere.reporting.bean.*;
import com.pixalere.reporting.bean.DashboardByEtiologyVO;
import com.pixalere.reporting.bean.DashboardByLocationVO;
import com.pixalere.reporting.bean.*;
import com.pixalere.reporting.bean.DashboardVO;
import com.pixalere.assessment.bean.WoundAssessmentVO;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.assessment.bean.AssessmentBurnVO;
import com.pixalere.assessment.bean.AssessmentSkinVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.service.ListServiceImpl;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.text.ParseException;
import com.pixalere.common.DataAccessException;
import com.pixalere.common.DataAccessObject;
import com.pixalere.common.ValueObject;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.guibeans.PatientSearchVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Common;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Iterator;
import java.util.Collection;
import org.apache.ojb.broker.PBFactoryException;
import org.apache.ojb.broker.PersistenceBroker;
import org.apache.ojb.broker.PersistenceBrokerException;
import org.apache.ojb.broker.query.Criteria;
import org.apache.ojb.broker.query.Query;
import org.apache.ojb.broker.query.ReportQueryByCriteria;
import org.apache.ojb.broker.query.QueryByCriteria;
import org.apache.ojb.broker.query.QueryFactory;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.common.bean.OfflineVO;
import com.pixalere.guibeans.WoundProfileItem;
import java.util.Collection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * This   class is responsible for handling all CRUD logic associated with the
 * <<table>>    table.
 *
 * @author Travis
 * @since 5.0
 */
public class ReportDAO {
    static private org.apache.log4j.Logger logs = org.apache.log4j.Logger.getLogger(ReportDAO.class);
    private Connection con = null;
    private boolean mysql=false;
    
    public void disconnect() {
        try {
            if (con != null && con.isClosed()==false) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getCatalog(){
        if(con!=null){
            try{
            return con.getCatalog();
            }catch(SQLException e){}
        }
        return "";
    }
    public String getClientInfo(){
        if(con!=null){
            //try{
            return "";//con.getSchema();
           // }catch(SQLException e){}
            
        }
        return "";
    }
    public void connect() {
        DataSource ds = null;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/pixDS");
            

            con = ds.getConnection();
            checkIsMySQL();
        } catch (NamingException ne) {
            ne.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ReportDAO() {
    }
    private void checkIsMySQL(){
        //select query with top 1... if it fails.. its mysql.
        try{
            PreparedStatement state = con.prepareStatement("select top 1 from configuration", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
        }catch(com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException e){
            mysql=true;
        }catch(Exception e){
            if(e.getMessage().indexOf("MySQL")!=-1){
                mysql=true;
            }
        }
        System.out.println("Is MYSQL"+mysql);
        
    }
    public boolean isMysql(){
        return mysql;
    }
    public ResultSet getResults(String query) {
        try {
            PreparedStatement state = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = state.executeQuery();
            return result;
        } catch (SQLException s) {
            s.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }
        return null;
    }
    public HashMap<String, DashboardVO> getDashboardReport(
            String category_name, 
            int etiology_id, 
            HashMap<String, DashboardVO> list, 
            DashboardReportVO reportCrit, 
            String key, 
            String startDate, 
            String endDate, 
            String treatment_query,
            String locale) {
        
        //all wounds
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        String dateDiff="";
        //if the database is MSSQL, we need to pass in the datepart
        if(!mysql){dateDiff="DAY,";}
        String patientDischargeClosedID="3507";
        //@todo should be pulling from confguration
        if(Common.getConfig("patientDischargeReasonComplete")!=null && !Common.getConfig("patientDischargeReasonComplete").equals("")){
            try{
                patientDischargeClosedID = Common.getConfig("patientDischargeReasonComplete");
            }catch(NumberFormatException e){logs.error("Invalid Discharge ID"+e.getMessage());}
        }
            String patientDischargeOtherID="3201";
        if(Common.getConfig("patientDischargeReasonOther")!=null && !Common.getConfig("patientDischargeReasonOther").equals("")){
            try{
                patientDischargeOtherID = Common.getConfig("patientDischargeReasonOther");
            }catch(NumberFormatException e){logs.error("Invalid Discharge ID"+e.getMessage());}
        }
            String patientDischargeSelfcareID="1603";
        if(Common.getConfig("patientDischargeReasonSelfCare")!=null && !Common.getConfig("patientDischargeReasonSelfCare").equals("")){
            try{
                patientDischargeSelfcareID = Common.getConfig("patientDischargeReasonSelfCare");
            }catch(NumberFormatException e){logs.error("Invalid Discharge ID"+e.getMessage());}
        }
        //System.out.println("#1 select count(distinct(wal.id))/nullif(count(distinct(wound_assessment.patient_id)),0) as avg_wounds,sum(visit)/count(distinct(wound_assessment.patient_id)) as avg_visits,count(distinct(wound_assessment.patient_id)) as patient_count,count(distinct(wal.id)) as wound_count,sum(visit) as visit_count,count(wound_assessment.id) as assessment_count,(sum(cost)/count(DISTINCT(wound_assessment.wound_id))) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        ResultSet result = getResults("select count(distinct(wal.id))/nullif(count(distinct(wound_assessment.patient_id)),0) as  avg_wounds,sum(visit)/nullif(count(distinct(wound_assessment.patient_id)),0) as avg_visits,count(distinct(wound_assessment.patient_id)) as patient_count,count(distinct(wal.id)) as wound_count,sum(visit) as visit_count,count(wound_assessment.id) as assessment_count,(sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 1, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_patients",locale), "patient_count"}}, key, result, list, "integer", false);
        //--------------------
        list = Common.parseDashboardResult(category_name, 7, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_allwound",locale), "wound_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 11, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_wounds",locale), "avg_wounds"}}, key, result, list, "float", false);
        //--------------------
        list = Common.parseDashboardResult(category_name, 22, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.visit_count",locale), "visit_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 23, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.assessment_count",locale), "assessment_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 28, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_visit",locale), "avg_visits"}}, key, result, list, "integer", false);
        //--------------------
        list = Common.parseDashboardResult(category_name, 31, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_all_wounds",locale), "avgcost_bywound_" + key}}, key, result, list, "currency", false);
        list = Common.parseDashboardResult(category_name, 32, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_product_cost_all_wounds",locale), "totalcost_bywound_" + key}}, key, result, list, "currency", false);
        //# of admitted
        //"select COUNT(id) from patient_accounts where account_status=1 and created_on>" + startDate + " and time_stamp<" + endDate + " and current_flag=1 and (1= (select COUNT(p.patient_id) from patient_accounts p where p.patient_id=patient_accounts.patient_id) OR exists (select patient_accounts.account_status from patient_accounts p where p.patient_id=patient_accounts.patient_id and p.id<patient_accounts.id ))" + treatment_query
       //System.out.println("select COUNT(id) as count from patient_accounts  " + builtQuery + " action='ADMITTED' and created_on>'" + startDate + "' and created_on<'" + endDate + "'  " + treatment_query+" ");
        builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "patient_accounts");
        result = getResults("select COUNT(id) as count from patient_accounts  " + builtQuery + " action='ADMITTED' and created_on>'" + startDate + "' and created_on<'" + endDate + "'  " + treatment_query+" ");
        //System.out.println("#2 select COUNT(id) as count from patient_accounts  " + builtQuery + " action='ADMIT' and  time_stamp>" + startDate + " and time_stamp<" + endDate + "  " + treatment_query+" ");
        list = Common.parseDashboardResult(category_name, 2, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.admitted_patients",locale), "count"}}, key, result, list, "integer", false);
        //# of discharged
        //System.out.println("select COUNT(id) as count,sum(case when discharge_reason = "+patientDischargeClosedID+" then 1 else 0 end) as healed,sum(case when discharge_reason = "+patientDischargeSelfcareID+" then 1 else 0 end) as self_care,sum(case when discharge_reason = "+patientDischargeOtherID+" then 1 else 0 end) as other from patient_accounts " + builtQuery + " account_status=0 and created_on>'" + startDate + "' and created_on<'" + endDate + "' and action='DISCHARGED'" + treatment_query+" ");
        result = getResults("select COUNT(id) as count,sum(case when discharge_reason = "+patientDischargeClosedID+" then 1 else 0 end) as healed,sum(case when discharge_reason = "+patientDischargeSelfcareID+" then 1 else 0 end) as self_care,sum(case when discharge_reason = "+patientDischargeOtherID+" then 1 else 0 end) as other from patient_accounts " + builtQuery + " account_status=0 and created_on>'" + startDate + "' and created_on<'" + endDate + "' and action='DISCHARGED'" + treatment_query+" ");
        list = Common.parseDashboardResult(category_name, 3, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients",locale), "count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 4, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients_healed",locale), "healed"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 5, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients_selfcare",locale), "self_care"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 6, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.discharged_patients_other",locale), "other"}}, key, result, list, "integer", false);
        //existing wounds
        //System.out.println("#5 select count(wound_assessment_location.id) as count from wound_assessment_location " + builtQuery + "  close_time_stamp>'"+startDate+"' and open_time_stamp<'" + endDate + "' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");//sub query.. if assessmen done in treamtnet location between dates.
        builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment_location");
        result = getResults("select count(wound_assessment_location.id) as count from wound_assessment_location " + builtQuery + "  close_time_stamp>'"+startDate+"' and open_time_stamp<'" + endDate + "'  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 8, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.existing_wounds",locale), "count"}}, key, result, list, "integer", false);
        //new wounds
        //System.out.println("#6 select count(wound_assessment_location.id) as count from wound_assessment_location "+  builtQuery  +"  open_time_stamp>'"+  startDate  +"'  AND open_time_stamp<'"+  endDate  +"' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select count(wound_assessment_location.id) as count from wound_assessment_location "+  builtQuery  +"  open_time_stamp>'"+  startDate  +"'  AND open_time_stamp<'"  +endDate+  "' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 9, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.new_wounds",locale), "count"}}, key, result, list, "integer", false);
        //closed wounds
        //System.out.println("#7 select count(wound_assessment_location.id) as count from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp>'"+  startDate  +"'  AND close_time_stamp<'"+  endDate  +"' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select count(wound_assessment_location.id) as count from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp>'"+  startDate  +"'  AND close_time_stamp<'" + endDate+  "' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 10, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.closed_wounds",locale), "count"}}, key, result, list, "integer", false);
        //avg days to heal.. wounds cloed during date range.
        //System.out.println("#8 select SUM(DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/SUM(discharge)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select SUM(((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))))/SUM(discharge)  as time_to_heal from wound_assessment_location " + builtQuery+  " discharge=1 and close_time_stamp IS NOT NULL and open_time_stamp IS NOT NULL  and close_time_stamp>'"  +startDate+  "' and close_time_stamp<'"+  endDate  +"' and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 12, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_heal_days",locale), "time_to_heal"}}, key, result, list, "integer", false);
        //avg days to heal between 1-30 wounds cloed during date range.
        //System.out.println("#9 select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>0 and  ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)<30  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>0 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <30   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 13, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days1-30",locale), "time_to_heal"}}, key, result, list, "integer", false);
        //avg days to heal between 31-91 wounds cloed during date range.
        //System.out.println("#10 select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=31 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)<92   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=31 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <92   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +"))");
        list = Common.parseDashboardResult(category_name, 14, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days31-90",locale), "time_to_heal"}}, key, result, list, "integer", false);
        //avg days to heal between 92-183 wounds cloed during date range.
        //System.out.println("#11 select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=93 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <183  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location "+  builtQuery  +" discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'"+  startDate  +"' and close_time_stamp<'"+  endDate  +"' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=93 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <183  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+  treatment_query  +") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + "))");
        list = Common.parseDashboardResult(category_name, 15, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days91-183",locale), "time_to_heal"}}, key, result, list, "integer", false);
        //avg days to heal between 184-365 wounds cloed during date range.
        //System.out.println("#12 select count(id)  as time_to_heal from wound_assessment_location " + builtQuery + " discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'" + startDate + "' and close_time_stamp<'" + endDate + "' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=184 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)<365  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location " + builtQuery + " discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'" + startDate + "' and close_time_stamp<'" + endDate + "' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>=184 and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge) <365  and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + "))");
        list = Common.parseDashboardResult(category_name, 16, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days184-365",locale), "time_to_heal"}}, key, result, list, "integer", false);
        //avg days to heal greater then 365 wounds cloed during date range.
        //System.out.println("#13 select count(id)  as time_to_heal from wound_assessment_location " + builtQuery + " discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'" + startDate + "' and close_time_stamp<'" + endDate + "' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>366   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 "+treatment_query+"))");
        result = getResults("select count(id)  as time_to_heal from wound_assessment_location " + builtQuery + " discharge=1 and close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>'" + startDate + "' and close_time_stamp<'" + endDate + "' and ((DATEDIFF("+dateDiff+"close_time_stamp,open_time_stamp))/discharge)>366   and (exists (select assessment_wound.id from assessment_wound left join wound_assessment on wound_assessment.id=assessment_wound.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_ostomy.id from assessment_ostomy left join wound_assessment on wound_assessment.id=assessment_ostomy.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_incision.id from assessment_incision left join wound_assessment on wound_assessment.id=assessment_incision.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + ") OR exists (select assessment_drain.id from assessment_drain left join wound_assessment on wound_assessment.id=assessment_drain.assessment_id where alpha_id=wound_assessment_location.id  and wound_assessment.active=1 " + treatment_query + "))");
        list = Common.parseDashboardResult(category_name, 17, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_days366",locale), "time_to_heal"}}, key, result, list, "integer", false);
        //70% Heal Rate by 3rd week - Order 18
        //System.out.println("HealRate70:  select count(DISTINCT(ae.alpha_id)) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + "   exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
        builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "assessment_wound");
        result = getResults("select (count(DISTINCT(ae.alpha_id))) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + "   exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
        HashMap m = getColumns(result, "heal_rate");
        double total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("heal_rate"));
            //System.out.println("#15 select count(distinct(ae.alpha_id)) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + " auto_referral_type = 'Heal Rate' and  exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
            result = getResults("select count(distinct(ae.alpha_id)) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + " auto_referral_type = 'Heal Rate' and  exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
            m = getColumns(result, "heal_rate");
            double heal = Double.parseDouble((String) m.get("heal_rate"));
            total = heal/cnt;
            //System.out.println("===-=-=-=-"+cnt+" / "+heal+" = "+total);
        }
        list = Common.parseDashboardResult(category_name, 20, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_rate70",locale), "heal_rate"}}, key, total, list, "percent", false);
        //30% Heal Rate by 3rd week - Order 19
       // System.out.println("HealRate70:  select count(DISTINCT(ae.alpha_id)) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + "   exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
        builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "assessment_wound");
        result = getResults("select (count(DISTINCT(ae.alpha_id))) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + "   exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and  visited_on<'" + endDate + "' " + treatment_query + ")");
        m = getColumns(result, "heal_rate");
        total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("heal_rate"));
            result = getResults("select count(distinct(ae.alpha_id)) as heal_rate from assessment_wound ae left join referrals_tracking ON ae.assessment_id=referrals_tracking.assessment_id left join auto_referrals on auto_referrals.referral_id=referrals_tracking.id " + builtQuery + " auto_referral_type = 'Heal Rate' and  exists (select id from wound_assessment where id=ae.assessment_id and active=1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query + ")");
            m = getColumns(result, "heal_rate");
            double heal = Double.parseDouble((String) m.get("heal_rate"));
            total = heal/cnt;
            total = 1 - total;
        }
        list = Common.parseDashboardResult(category_name, 21, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.heal_rate30",locale), "heal_rate"}}, key, total, list, "percent", false);
        //All Home visits - Order 22
        //System.out.println("#18 select sum(lookup.clinic) as visit_clinic_count,(count(wound_assessment.id)-sum(lookup.clinic)) as visit_home_count from wound_assessment left join lookup on lookup.id=wound_assessment.treatment_location_id " + builtQuery + " visit = 1 and visited_on>'" + startDate + "' and visited_on<'" + endDate +"' "+treatment_query);
        builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        result = getResults("select sum(lookup.clinic) as visit_clinic_count,(count(wound_assessment.id)-sum(lookup.clinic)) as visit_home_count from wound_assessment left join lookup on lookup.id=wound_assessment.treatment_location_id " + builtQuery + " visit = 1 and visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 24, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_home_visits",locale), "visit_home_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 26, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_clinic_visits",locale), "visit_clinic_count"}}, key, result, list, "integer", false);
        //All Home Assessments - Order 23
        //System.out.println("#19 select sum(lookup.clinic) as visit_clinic_count,(count(wound_assessment.id)-sum(lookup.clinic)) as visit_home_count from wound_assessment left join lookup on lookup.id=wound_assessment.treatment_location_id " + builtQuery + " visited_on>'" + startDate + "' and visited_on<'" + endDate+"' "+treatment_query);
        result = getResults("select sum(lookup.clinic) as visit_clinic_count,(count(wound_assessment.id)-sum(lookup.clinic)) as visit_home_count from wound_assessment left join lookup on lookup.id=wound_assessment.treatment_location_id " + builtQuery + " visited_on>'" + startDate + "' and visited_on<'" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 25, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_home_assess",locale), "visit_home_count"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 27, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_clinic_assess",locale), "visit_clinic_count"}}, key, result, list, "integer", false);
        //visits  27/28 // if lookup.report_value >=3
       //System.out.println("Visits: select count(distinct(dcf.alpha_id))  as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf   left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + "  wound_assessment.active=1 and wound_assessment.visited_on>='" + startDate + "' AND wound_assessment.visited_on<='" + endDate + "' " + treatment_query);
        result = getResults("select count(distinct(dcf.alpha_id))  as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + "  wound_assessment.active=1 and wound_assessment.visited_on>='" + startDate + "' AND wound_assessment.visited_on<='" + endDate + "' " + treatment_query);
        m = getColumns(result, "visit_count");
        total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("visit_count"));
            //System.out.println("select count(distinct(dcf.alpha_id)) as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf   left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + " report_value IS NOT null and isnumeric(report_value)=1 and cast(lookup.report_value as decimal(5,2))>=3 and  wound_assessment.active=1 and wound_assessment.visited_on>'" + startDate + "' and wound_assessment.visited_on<'" + endDate + "' " + treatment_query + "");
            result = getResults("select count(distinct(dcf.alpha_id)) as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf   left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + " report_value IS NOT null and isnumeric(report_value)=1  and  cast(lookup.report_value as decimal(5,2))>=3 and wound_assessment.active=1 and wound_assessment.visited_on>'" + startDate + "' and wound_assessment.visited_on<'" + endDate + "' " + treatment_query + "");
            m = getColumns(result, "visit_count");
            double vis = Double.parseDouble((String) m.get("visit_count"));
            total = vis/cnt;
            //System.out.println("Count: "+cnt+" "+vis);
            total = 1 - total;
        }
        list = Common.parseDashboardResult(category_name, 29, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.visits_gr",locale), "visit_count"}}, key, total, list, "percent", false);
        
        result = getResults("select count(distinct(dcf.alpha_id))  as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + "  wound_assessment.active=1 and wound_assessment.visited_on>='" + startDate + "' AND wound_assessment.visited_on<='" + endDate + "' " + treatment_query);
        m = getColumns(result, "visit_count");
        total = 0.0;
        if (m != null) {
            double cnt = Double.parseDouble((String) m.get("visit_count"));
            result = getResults("select count(distinct(dcf.alpha_id)) as visit_count from wound_assessment  left join dressing_change_frequency dcf ON dcf.assessment_id=wound_assessment.id left join lookup on lookup.id=dcf.dcf   left join wound_profile_type ON wound_profile_type.id=dcf.wound_profile_type_id  " + builtQuery + "  report_value IS NOT null and isnumeric(report_value)=1  and  cast(lookup.report_value as decimal(5,2))<3 and  wound_assessment.active=1 and wound_assessment.visited_on>'" + startDate + "' and wound_assessment.visited_on<'" + endDate + "' " + treatment_query + "");
            m = getColumns(result, "visit_count");
            double vis = Double.parseDouble((String) m.get("visit_count"));
            total = vis/cnt;
            //System.out.println("Count: "+cnt+" "+vis);
        }
        list = Common.parseDashboardResult(category_name, 30, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.visits_less",locale), "visit_count"}}, key, total, list, "percent", false);

        //open wounds
        //System.out.println("#20 select (sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " open_time_stamp<='" + endDate + "' AND (close_time_stamp>='" + endDate + "' OR close_time_stamp IS NULL) and wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        result = getResults("select (sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " open_time_stamp<='" + endDate + "' AND (close_time_stamp>='" + endDate + "' OR close_time_stamp IS NULL) and wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 33, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_open_wounds",locale), "avgcost_bywound_" + key},}, key, result, list, "currency", false);
        list = Common.parseDashboardResult(category_name, 34, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_product_cost_open_wounds",locale), "totalcost_bywound_" + key}}, key, result, list, "currency", false);
        //close wounds
        //System.out.println("#22 select  count(distinct(wal.id)) as closed_wound_count,(sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>='" + startDate + "' AND close_time_stamp<='" + endDate + "' and wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        result = getResults("select  count(distinct(wal.id)) as closed_wound_count,(sum(cost)/nullif(count(DISTINCT(wound_assessment.wound_id)),0)) as avgcost_bywound_" + key + ",(sum(cost)) as totalcost_bywound_" + key + " from wound_assessment left join assessment_product ON assessment_product.assessment_id=wound_assessment.id  LEFT JOIN wound_assessment_location as wal ON wal.id=assessment_product.alpha_id left join wound_profile_type ON wound_profile_type.id=wal.wound_profile_type_id LEFT JOIN products ON products.id=assessment_product.product_id  " + builtQuery + " close_time_stamp IS NOT NULL  and open_time_stamp IS NOT NULL  and close_time_stamp>='" + startDate + "' AND close_time_stamp<='" + endDate + "' and wound_assessment.active=1 and visited_on>='" + startDate + "' AND visited_on<='" + endDate + "' " + treatment_query);
        list = Common.parseDashboardResult(category_name, 35, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.avg_product_cost_closed_wounds",locale), "avgcost_bywound_" + key},}, key, result, list, "currency", false);
        list = Common.parseDashboardResult(category_name, 36, new String[][]{{Common.getLocalizedString("pixalere.admin.dashboard.total_product_cost_closed_wounds",locale), "totalcost_bywound_" + key}}, key, result, list, "currency", false);
        return list;
    }
    public HashMap<String, String> getColumns(HashMap<String, String> mapArray, ResultSet result, String columns) {
        try {
            if (result != null) {
                while (result.next()) {
                    for (String column : columns.split(",")) {
                        String s = result.getString(column);
                        if (s == null) {
                            s = "";
                        }
                        mapArray.put(column, s);
                    }
                    return mapArray;
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapArray;
    }
    public int getInt(ResultSet set, String field) {
        try {
            if (set != null) {
                set.beforeFirst();
                while (set.next()) {
                    if (set.getInt(field) == -1) {
                        return 0;
                    } else {
                        return set.getInt(field);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
        } catch (Exception s) {
            s.printStackTrace();
        }
        return 0;
    }
    public double getDouble(ResultSet set, String field) {
        try {
            if (set != null) {
                set.beforeFirst();
                while (set.next()) {
                    if (set.getDouble(field) == -1) {
                        return 0;
                    } else {
                        return set.getDouble(field);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
        } catch (Exception s) {
            s.printStackTrace();
        }
        return 0;
    }
    public String getString(ResultSet set, String field) {
        try {
            if (set != null) {
                set.beforeFirst();
                while (set.next()) {
                    String s = set.getString(field);
                    if (s == null) {
                        s = "";
                    }
                    return s;
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
            e.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }
        return "";
    }
    public HashMap<String, String> getColumns(ResultSet result, String columns) {
        HashMap<String, String> mapArray = new HashMap();
        try {
            if (result != null) {
                while (result.next()) {
                    for (String column : columns.split(",")) {
                        String s = result.getString(column);
                        if (s == null) {
                            s = "";
                        }
                        mapArray.put(column, s);
                    }
                    return mapArray;
                }
            }
        } catch (SQLException e) {
            System.out.println("Not able to grab Resulset" + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapArray;
    }
    /********  Everything below this line are  legacy reports and will eventually be removed *******************/
    /**
     * Finds a all <<table>> records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     *                         parameters in the SQL statement.
     * @since 3.0
     * @author stefan viveen
     * 
     */
    public Vector findAllWoundAssessments(int patient_id, int wound_id, int start_date, int end_date) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("patient_id", new Integer(patient_id));
            crit.addEqualTo("wound_id", new Integer(wound_id));
            // crit.addEqualTo("visit", 1);
            if (start_date != 0) {
                crit.addGreaterOrEqualThan("time_stamp", new Integer(start_date));
            }
            if (end_date != 0) {
                crit.addLessOrEqualThan("time_stamp", new Integer(end_date));
            }
            ReportQueryByCriteria q = QueryFactory.newReportQuery(WoundAssessmentVO.class, crit);
            q.setAttributes(new String[]{"time_stamp", "id", "professional_id"});
            q.addOrderByDescending("time_stamp");
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            System.out.println("Exception in ReportDAO: " + e);
            logs.error("PersistanceBrokerException thrown in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in PatientProfileDAO.getAllWoundProfileSignatures(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    public Vector findAllAssessments(int assessment_id, int intCareTypeNr) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("assessment_id", new Integer(assessment_id));
            crit.addEqualTo("active", new Integer(1));
            ReportQueryByCriteria q = null;
            if (intCareTypeNr == 1) {
                q = QueryFactory.newReportQuery(AssessmentEachwoundVO.class, crit);
            }
            if (intCareTypeNr == 2) {
                q = QueryFactory.newReportQuery(AssessmentIncisionVO.class, crit);
            }
            if (intCareTypeNr == 3) {
                q = QueryFactory.newReportQuery(AssessmentDrainVO.class, crit);
            }
            if (intCareTypeNr == 4) {
                q = QueryFactory.newReportQuery(AssessmentOstomyVO.class, crit);
            }if (intCareTypeNr ==5) {
                q = QueryFactory.newReportQuery(AssessmentBurnVO.class, crit);
            }if (intCareTypeNr == 6) {
                q = QueryFactory.newReportQuery(AssessmentSkinVO.class, crit);
            }
            q.setAttributes(new String[]{"alpha_id"});
            q.addOrderByDescending("id");
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in reportDAO.findAllAssessments(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in reportDAO.findAllAssessments(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    public Vector<Object[]> findAllPatients(int treatment_location_id) throws DataAccessException {
        Criteria crit = new Criteria();
        PersistenceBroker broker = null;
        Iterator iter = null;
        Vector v = new Vector();
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            crit.addEqualTo("current_flag", 1);
            crit.addEqualTo("treatment_location_id", treatment_location_id);
            ReportQueryByCriteria q = QueryFactory.newReportQuery(PatientAccountVO.class, crit);
            q.setAttributes(new String[]{"patient_id"});
            q.addOrderByDescending("patient_id");
            iter = broker.getReportQueryIteratorByQuery(q);
            while (iter.hasNext()) {
                v.add(iter.next());
            }
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in reportDAO.findAllAssessments(): " + e.toString(), e);
            e.printStackTrace();
            throw new DataAccessException("Error in reportDAO.findAllAssessments(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        return v;
    }
    /**
     * Deletes dashboard_report records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public void delete(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            DashboardReportVO report = (DashboardReportVO) delete;
            Collection reports = findAllByCriteria(report);
            Iterator iter = reports.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                DashboardReportVO current = (DashboardReportVO) iter.next();
                broker.beginTransaction();
                broker.delete(current);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Finds a single dashboard_reports record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the patient_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 5.0
     */
    public ValueObject findByID(int rec_id) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        DashboardReportVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            DashboardReportVO report = new DashboardReportVO();
            report.setId(new Integer(rec_id));
            Query query = new QueryByCriteria(report);
            resultVO = (DashboardReportVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }
    /**
     * Finds a single dashboard_reports record, as specified by the Object
     * passed in.  If no record is found a null value is returned.
     *
     * @param object the object containing the criteria for searching.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public ValueObject findByCriteria(ValueObject obj) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        DashboardReportVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            DashboardReportVO report = (DashboardReportVO) obj;
            Query query = new QueryByCriteria(report);
            resultVO = (DashboardReportVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }

    /**
     * Inserts a single DashboardReportVO object into the dashboard_reports table.  The value object passed
     * in has to be of type DashboardReportVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the dashboard_reports record.
     * @since 5.0
     */
    public void insert(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            DashboardReportVO report = (DashboardReportVO) insert;

            broker.beginTransaction();
            report.setId(null);
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single DashboardReport object into the dashboard_report table.  The value object passed
     * in has to be of type DashboardReport.
     *
     * @param insertRecord fields in ValueObject represent fields in the dashboard_report record.
     * @since 5.0
     */
    public void update(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            DashboardReportVO report = (DashboardReportVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Finds a all dashboard_report records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param DashboardReport fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 5.0
     */
    public Collection<DashboardReportVO> findAllByCriteria(DashboardReportVO pa) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<DashboardReportVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pa);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
   
    /**
     * Deletes dashboard_reports_etiology records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public void deleteDashboardByEtiology(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            DashboardByEtiologyVO report = (DashboardByEtiologyVO) delete;
            Collection reports = findAllByCriteria(report);
            Iterator iter = reports.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                DashboardByEtiologyVO current = (DashboardByEtiologyVO) iter.next();
                broker.beginTransaction();
                broker.delete(current);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Inserts a single DashboardByEtiologyVO object into the dashboard_reports_etiology table.  The value object passed
     * in has to be of type DashboardByEtiologyVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the dashboard_reports_etiology record.
     * @since 5.0
     */
    public void insertDashboardByEtiology(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            DashboardByEtiologyVO report = (DashboardByEtiologyVO) insert;

            broker.beginTransaction();
            report.setId(null);
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single DashboardByEtiologyVO object into the dashboard_reports_etiology table.  The value object passed
     * in has to be of type DashboardByEtiologyVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the dashboard_reports_etiology record.
     * @since 5.0
     */
    public void updateEtiology(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            DashboardByEtiologyVO report = (DashboardByEtiologyVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Finds a all dashboard_reports_etiology records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param DashboardByEtiologyVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 5.0
     */
    public Collection<DashboardByEtiologyVO> findAllByCriteria(DashboardByEtiologyVO pa) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<DashboardByEtiologyVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pa);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
    /**
     * Deletes dashboard_reports_location records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public void deleteDashboardByLocation(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            DashboardByLocationVO report = (DashboardByLocationVO) delete;
            Collection reports = findAllByCriteria(report);
            Iterator iter = reports.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                DashboardByLocationVO current = (DashboardByLocationVO) iter.next();
                broker.beginTransaction();
                broker.delete(current);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Inserts a single DashboardByLocationVO object into the dashboard_reports_location table.  The value object passed
     * in has to be of type DashboardByLocationVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the dashboard_reports_location record.
     * @since 5.0
     */
    public void insertDashboardByLocation(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            DashboardByLocationVO report = (DashboardByLocationVO) insert;

            broker.beginTransaction();
            report.setId(null);
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single DashboardByLocationVO object into the dashboard_reports_location table.  The value object passed
     * in has to be of type DashboardByLocationVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the dashboard_reports_location record.
     * @since 5.0
     */
    public void updateDashboardByLocation(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            DashboardByLocationVO report = (DashboardByLocationVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Finds a all dashboard_reports_location records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param DashboardByLocationVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 5.0
     */
    public Collection<DashboardByLocationVO> findAllByCriteria(DashboardByLocationVO pa) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<DashboardByLocationVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pa);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
    
    /**
     * Refactored some common query building
     * @param wound_type
     * @param etiology_id
     * @return query
     */
    public String buildQuery(String wound_type, int etiology_id, String table_name) {
        String query = " WHERE ";
        String wound_type_query = "";
        if (wound_type.equals("All")) {
        } else if (wound_type.equals("Wound")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'A' and patient_id=" + table_name + ".patient_id) and ";
        } else if (wound_type.equals("Ostomy")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'O' and patient_id=" + table_name + ".patient_id) and ";
        } else if (wound_type.equals("Drain")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'D' and patient_id=" + table_name + ".patient_id) and ";
        } else if (wound_type.equals("Incision")) {
            wound_type_query = " exists (select id from wound_profile_type.alpha_type = 'T' and patient_id=" + table_name + ".patient_id) and ";
        }
        if (etiology_id > 0) {
            wound_type_query = wound_type_query + " exists (select id from wound_etiology et where patient_id=" + table_name + ".patient_id and et.etiology_id=" + etiology_id + ") and ";
        }
        if (!wound_type_query.equals("")) {
            query = query + wound_type_query;
        }
        return query;
    }
    /***  ReportQueue ****/
    /**
     * Deletes report_queue records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public void deleteQueue(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueVO report = (ReportQueueVO) delete;
            Collection<ReportQueueVO> reports = findAllQueueByCriteria(report);
            Iterator iter = reports.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                ReportQueueVO current = (ReportQueueVO) iter.next();
                broker.beginTransaction();
                broker.delete(current);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Finds a single report_queue record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the patient_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 5.0
     */
    public ValueObject findQueueByID(int rec_id) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueVO report = new ReportQueueVO();
            report.setId(new Integer(rec_id));
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }
    /**
     * Finds a single report_queue record, as specified by the Object
     * passed in.  If no record is found a null value is returned.
     *
     * @param object the object containing the criteria for searching.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public ValueObject findQueueByCriteria(ValueObject obj) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueVO report = (ReportQueueVO) obj;
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }

    /**
     * Inserts a single ReportQueueVO object into the report_queue table.  The value object passed
     * in has to be of type ReportQueueVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void insertQueue(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueVO report = (ReportQueueVO) insert;
            broker.beginTransaction();
            
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single ReportQueueVO object into the report_queue table.  The value object passed
     * in has to be of type ReportQueueVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void updateQueue(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueVO report = (ReportQueueVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Finds a all report_queue records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ReportQueueVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 5.0
     */
    public Collection<ReportQueueVO> findAllQueueByCriteria(ReportQueueVO pa) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ReportQueueVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pa);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
    // ReportQueueTreatmentsVO
    /***  ReportQueueTreatmentsVO ****/
    /**
     * Deletes report_queue_treatments records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public void deleteQueueTreatment(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueTreatmentsVO report = (ReportQueueTreatmentsVO) delete;
            Collection<ReportQueueTreatmentsVO> reports = findAllQueueTreatmentsByCriteria(report);
            Iterator iter = reports.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                ReportQueueTreatmentsVO current = (ReportQueueTreatmentsVO) iter.next();
                broker.beginTransaction();
                broker.delete(current);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Finds a single report_queue_treatments record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the patient_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 5.0
     */
    public ValueObject findQueueTreatmentsByID(int rec_id) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueTreatmentsVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueTreatmentsVO report = new ReportQueueTreatmentsVO();
            report.setId(new Integer(rec_id));
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueTreatmentsVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }
    /**
     * Finds a single report_queue_treatments record, as specified by the Object
     * passed in.  If no record is found a null value is returned.
     *
     * @param object the object containing the criteria for searching.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public ValueObject findQueueTreatmentsByCriteria(ValueObject obj) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueTreatmentsVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueTreatmentsVO report = (ReportQueueTreatmentsVO) obj;
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueTreatmentsVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }

    /**
     * Inserts a single ReportQueueTreatmentsVO object into the report_queue_treatments table.  The value object passed
     * in has to be of type ReportQueueTreatmentsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void insertQueueTreatment(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueTreatmentsVO report = (ReportQueueTreatmentsVO) insert;
            broker.beginTransaction();
            report.setId(null);
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single ReportQueueTreatmentsVO object into the report_queue_treatments table.  The value object passed
     * in has to be of type ReportQueueTreatmentsVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void updateQueueTreatment(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueTreatmentsVO report = (ReportQueueTreatmentsVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Finds a all report_queue_treatments records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ReportQueueTreatmentsVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 5.0
     */
    public Collection<ReportQueueTreatmentsVO> findAllQueueTreatmentsByCriteria(ReportQueueTreatmentsVO pa) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ReportQueueTreatmentsVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pa);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
    /***  ReportQueueEtiologyVO ****/
    /**
     * Deletes report_queue_etiologies records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public void deleteQueueEtiology(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueEtiologyVO report = (ReportQueueEtiologyVO) delete;
            Collection<ReportQueueEtiologyVO> reports = findAllQueueEtiologyByCriteria(report);
            Iterator iter = reports.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                ReportQueueEtiologyVO current = (ReportQueueEtiologyVO) iter.next();
                broker.beginTransaction();
                broker.delete(current);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Finds a single report_queue_etiologies record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the patient_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 5.0
     */
    public ValueObject findQueueEtiologyByID(int rec_id) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueEtiologyVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueEtiologyVO report = new ReportQueueEtiologyVO();
            report.setId(new Integer(rec_id));
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueEtiologyVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }
    /**
     * Finds a single report_queue_etiologies record, as specified by the Object
     * passed in.  If no record is found a null value is returned.
     *
     * @param object the object containing the criteria for searching.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public ValueObject findQueueEtiologyByCriteria(ValueObject obj) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueEtiologyVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueEtiologyVO report = (ReportQueueEtiologyVO) obj;
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueEtiologyVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }

    /**
     * Inserts a single ReportQueueEtiologyVO object into the report_queue_etiologies table.  The value object passed
     * in has to be of type ReportQueueEtiologyVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void insertQueueEtiology(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueEtiologyVO report = (ReportQueueEtiologyVO) insert;
            broker.beginTransaction();
            report.setId(null);
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single ReportQueueEtiologyVO object into the report_queue_etiologies table.  The value object passed
     * in has to be of type ReportQueueEtiologyVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void updateQueueEtiology(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueEtiologyVO report = (ReportQueueEtiologyVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Finds a all report_queue_etiologies records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ReportQueueEtiologyVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 5.0
     */
    public Collection<ReportQueueEtiologyVO> findAllQueueEtiologyByCriteria(ReportQueueEtiologyVO pa) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ReportQueueEtiologyVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pa);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
    /***  ReportQueueWoundTypeVO ****/
    /**
     * Deletes report_queue_types records, as specified by the value object
     * passed in.  Fields in Value Object which are not null will be used as
     * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
     *
     * @param deleteRecord the value object which specifies which records should be deleted
     * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public void deleteWoundType(ValueObject delete) {
        logs.info("************Entering PatientDAO.delete()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueWoundTypeVO report = (ReportQueueWoundTypeVO) delete;
            Collection<ReportQueueWoundTypeVO> reports = findAllWoundTypeByCriteria(report);
            Iterator iter = reports.iterator();
            broker = ConnectionLocator.getInstance().findBroker();

            while (iter.hasNext()) {
                ReportQueueWoundTypeVO current = (ReportQueueWoundTypeVO) iter.next();
                broker.beginTransaction();
                broker.delete(current);
                broker.commitTransaction();
            }
        } catch (PersistenceBrokerException e) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + e.toString(), e);
            e.printStackTrace();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.delete(): " + e.toString(), e);
        } catch (com.pixalere.common.DataAccessException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } catch (Exception ex) {
            broker.abortTransaction();
            logs.error("PersistanceBrokerException thrown in PatientDAO.delete(): " + ex.toString(), ex);
            ex.printStackTrace();
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.delete()*************");

    }
    /**
     * Finds a single report_queue_types record, as specified by the primary key value
     * passed in.  If no record is found a null value is returned.
     *
     * @param primaryKey the primary key of the patient_accounts table.
     * @see com.pixalere.common.DataAccessObject#findByPK(java.lang.String)
     * @since 5.0
     */
    public ValueObject findWoundTypeByID(int rec_id) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueWoundTypeVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueWoundTypeVO report = new ReportQueueWoundTypeVO();
            report.setId(new Integer(rec_id));
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueWoundTypeVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }
    /**
     * Finds a single report_queue_types record, as specified by the Object
     * passed in.  If no record is found a null value is returned.
     *
     * @param object the object containing the criteria for searching.
     * @see com.pixalere.common.DataAccessObject#findByCriteria(com.pixalere.common.ValueObject)
     * @since 5.0
     */
    public ValueObject findWoundTypeByCriteria(ValueObject obj) throws DataAccessException {
        logs.info("********* Entering the PatientDAO.findByID****************");
        PersistenceBroker broker = null;
        ReportQueueWoundTypeVO resultVO = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueWoundTypeVO report = (ReportQueueWoundTypeVO) obj;
            Query query = new QueryByCriteria(report);
            resultVO = (ReportQueueWoundTypeVO) broker.getObjectByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("PersistanceBrokerException thrown in PatientDAO.findByID(): " + e.toString(), e);
            throw new DataAccessException("Error in PatientDAO.findByID(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("********* Done the PatientDAO.findByID");
        return resultVO;
    }

    /**
     * Inserts a single ReportQueueWoundTypeVO object into the report_queue_types table.  The value object passed
     * in has to be of type ReportQueueWoundTypeVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void insertWoundType(ValueObject insert) throws DataAccessException {
        logs.info("************Entering PatientDAO.insert()************");
        PersistenceBroker broker = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            ReportQueueWoundTypeVO report = (ReportQueueWoundTypeVO) insert;
            broker.beginTransaction();
            report.setId(null);
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.insert(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.insert()*************");

    }
    /**
     * Inserts a single ReportQueueWoundTypeVO object into the report_queue_types table.  The value object passed
     * in has to be of type ReportQueueWoundTypeVO.
     *
     * @param insertRecord fields in ValueObject represent fields in the report_queue record.
     * @since 5.0
     */
    public void updateWoundType(ValueObject update) {
        logs.info("************Entering PatientDAO.update()************");
        PersistenceBroker broker = null;
        try {
            ReportQueueWoundTypeVO report = (ReportQueueWoundTypeVO) update;
            broker = ConnectionLocator.getInstance().findBroker();
            broker.beginTransaction();
            broker.store(report);
            broker.commitTransaction();
        } catch (PBFactoryException ex) {
            ex.printStackTrace();
            broker.abortTransaction();
        } catch (PersistenceBrokerException e) {
            e.printStackTrace();
            broker.abortTransaction();
        } catch (ConnectionLocatorException e) {
            logs.error("connectionLocatorException thrown in PatientDAO.update(): " + e.toString(), e);
        } catch (com.pixalere.common.ApplicationException e) {
            logs.error("DataAccessException thrown in WoundProfileDAO.deleteTMP(): " + e.toString(), e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("*************Done with PatientDAO.update()*************");

    }
    /**
     * Finds a all report_queue_types records, as specified by the value object
     * passed in.  If no record is found a null value is returned.
     *
     * @param ReportQueueWoundTypeVO fields in ValueObject which are not null will be used as
     *                       parameters in the SQL statement.
     * @since 5.0
     */
    public Collection<ReportQueueWoundTypeVO> findAllWoundTypeByCriteria(ReportQueueWoundTypeVO pa) throws DataAccessException {
        logs.info("***********************Entering userDAO.findByCriteria()***********************");
        PersistenceBroker broker = null;
        Collection<ReportQueueWoundTypeVO> results = null;
        try {
            broker = ConnectionLocator.getInstance().findBroker();
            QueryByCriteria query = new QueryByCriteria(pa);
            results = (Collection) broker.getCollectionByQuery(query);
        } catch (ConnectionLocatorException e) {
            logs.error("ServiceLocatorException thrown in UserDAO.findByCriteria(): " + e.toString(), e);
            throw new DataAccessException("ServiceLocatorException in UserDAO.findByCriteria()", e);
        } finally {
            if (broker != null) {
                broker.close();
            }
        }
        logs.info("***********************Leaving UserDAO.findByCriteria()***********************");
        return results;
    }
    /**
   * Deletes <<table>> records, as specified by the value object
   * passed in.  Fields in Value Object which are not null will be used as
   * parameters in the SQL statement.  Retrieves all records by criteria, then deletes them.
   *
   * @param deleteRecord  the value object which specifies which records should be deleted
   * @see com.pixalere.common.DataAccessObject#delete(com.pixalere.common.ValueObject)
   *
   * @since 5.0
   */
  public void deleteCCACReport(ValueObject deleteRecord) throws DataAccessException {
      logs.info("***********************Entering ReportDAO.delete()***********************");
      PersistenceBroker broker = null;
 
      try{
          broker = ConnectionLocator.getInstance().findBroker();
          CCACReportVO lib = (CCACReportVO) deleteRecord;
          //Begin the transaction.
          broker.beginTransaction();
          broker.delete(lib);
          broker.commitTransaction();
          
      } catch (PersistenceBrokerException e){
          // if something went wrong: rollback
          broker.abortTransaction();
          logs.error("PersistenceBrokerException thrown in ReportDAO.delete(): " + e.toString());
          e.printStackTrace();
          throw new DataAccessException("Error in ReportDAO.delete()",e);
      } catch(ConnectionLocatorException e){
          logs.error("ServiceLocatorException thrown in ReportDAO.delete(): " + e.toString());
          throw new DataAccessException("ServiceLocator exception in ConfigurationVO.delete()",e);
      } finally{
          if (broker!=null) broker.close();
      }
      logs.info("***********************Leaving ReportDAO.delete()***********************");
  }
  /**
   * Finds a all <<table>> records, as specified by the value object
   * passed in.  If no record is found a null value is returned.
   *
   * @param report fields in ValueObject which are not null will be used as
   * parameters in the SQL statement.
   *
   * @since 5.0
   */
  public Collection<CCACReportVO> findAllByCriteria(int active,int already_retrieved,Date start_date,Date end_date,int[] treatment_location_id) throws DataAccessException{
      logs.info("***********************Entering ReportDAO.findAllByCriteria()***********************");
      PersistenceBroker broker = null;
      Collection<CCACReportVO> results = null;
      try{
          broker = ConnectionLocator.getInstance().findBroker();
          
          Criteria crit = new Criteria();
              QueryByCriteria query = null;
              crit.addEqualTo("active", active);
              if(already_retrieved != 1){
                  crit.addEqualTo("already_retrieved", 0);
              }
              if(start_date!=null){
                  crit.addGreaterOrEqualThan("time_stamp", start_date);
              }
              if(end_date !=null){
                  crit.addLessOrEqualThan("time_stamp",end_date);
              }
              //System.out.println("--- "+start_date+" "+end_date+" "+treatment_location_id);
              if(treatment_location_id != null){
                  Criteria grp = new Criteria();
                  for(int t : treatment_location_id){
                      Criteria treatCrit = new Criteria();
                      System.out.println("region: "+t);
                      treatCrit.addEqualTo("patient.treatment_location_id",t);
                      grp.addOrCriteria(treatCrit);
                  }
                  crit.addAndCriteria(grp);
              }
              query = QueryFactory.newQuery(CCACReportVO.class, crit);
              results = (Collection) broker.getCollectionByQuery(query);
      } catch(ConnectionLocatorException e){
          logs.error("ServiceLocatorException thrown in ReportDAO.findByCriteria(): " + e.toString(),e);
          throw new DataAccessException("ServiceLocatorException in ReportDAO.findByCriteria()",e);
      } finally{
          if (broker!=null) broker.close();
      }
      logs.info("***********************Leaving ReportDAO.findAllByCriteria()***********************");
      return results;
  }
   /**
   * Finds a all <<table>> records, as specified by the value object
   * passed in.  If no record is found a null value is returned.
   *
   * @param report fields in ValueObject which are not null will be used as
   * parameters in the SQL statement.
   *
   * @since 5.0
   */
  public Collection findAllByCriteria(CCACReportVO lib) throws DataAccessException{
      logs.info("***********************Entering ReportDAO.findAllByCriteria()***********************");
      PersistenceBroker broker = null;
      Collection results = null;
      try{
          broker = ConnectionLocator.getInstance().findBroker();
          
          QueryByCriteria query = new QueryByCriteria(lib);
          
          query.addOrderByAscending("wound_id");
          results = (Collection) broker.getCollectionByQuery(query);
      } catch(ConnectionLocatorException e){
          logs.error("ServiceLocatorException thrown in ReportDAO.findByCriteria(): " + e.toString(),e);
          throw new DataAccessException("ServiceLocatorException in ReportDAO.findByCriteria()",e);
      } finally{
          if (broker!=null) broker.close();
      }
      logs.info("***********************Leaving ReportDAO.findAllByCriteria()***********************");
      return results;
  }
  /**
   * Finds an <<table>> record, as specified by the value object
   * passed in.  If no record is found a null value is returned.
   *
   * @param report  fields in ValueObject which are not null will be used as
   * parameters in the SQL statement.
   *
   * @since 5.0
   */
  public CCACReportVO findByCriteria(CCACReportVO lib) throws DataAccessException{
      logs.info("***********************Entering productsDAO.findByCriteria()***********************");
      PersistenceBroker broker = null;
      CCACReportVO returnVO   = null;
      try{
          broker = ConnectionLocator.getInstance().findBroker();
          Query query = new QueryByCriteria(lib);
          returnVO = (CCACReportVO) broker.getObjectByQuery(query);
      } catch(ConnectionLocatorException e){
          logs.error("ServiceLocatorException thrown in ReportDAO.findByCriteria(): " + e.toString(),e);
          throw new DataAccessException("ServiceLocatorException in ReportDAO.findByCriteria()",e);
      } finally{
          if (broker!=null) broker.close();
      }
      logs.info("***********************Leaving ReportDAO.findByCriteria()***********************");
      return returnVO;
  }
  /**
   * Updates a single <<table>> record in the Pixalere database.  If id is Null in
   * ValueObject the update method can also be insert records.
   *
   * @param updateRecord the object which represents a record in the <<table>> table to be inserted
   * @see com.pixalere.common.DataAccessObject#update(com.pixalere.common.ValueObject)
   * @version 5.0
   * @since 5.0
   */
  public void insertCCACReport(ValueObject insertRecord) throws DataAccessException {
      logs.info("***********************Entering ReportDAO.insert()***********************");
      CCACReportVO lib = null;
      PersistenceBroker broker = null;
      try{
          broker = ConnectionLocator.getInstance().findBroker();
          lib = (CCACReportVO) insertRecord;
          broker.beginTransaction();
          broker.store(lib);
          broker.commitTransaction();
          
      } catch(ConnectionLocatorException e){
          logs.error("ServiceLocatorException thrown in ReportDAO.insert(): " + e.toString());
          throw new DataAccessException("ServiceLocator in Error in ReportDAO.insert()",e);
      } catch (PersistenceBrokerException e){
          // if something went wrong: rollback
          broker.abortTransaction();
          e.printStackTrace();
          logs.error("PersistenceBrokerException thrown in ReportDAO.insert(): " + e.toString());
          throw new DataAccessException("Error in ReportDAO.insert()",e);
      } finally{
          if (broker!=null) broker.close();
      }
      logs.info("***********************Leaving ReportDAO.insert()***********************");
  }
}
