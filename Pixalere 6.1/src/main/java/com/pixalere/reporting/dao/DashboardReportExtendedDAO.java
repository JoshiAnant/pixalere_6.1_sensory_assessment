/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.reporting.dao;

import com.pixalere.reporting.bean.DashboardReportVO;
import com.pixalere.reporting.bean.DashboardVO;
import com.pixalere.utils.Common;
import com.pixalere.utils.PDate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

/**
 * Gets the entries required for the Dashboard Report Extended (4 quarters),
 * portrait format.
 *
 * @author Jose
 */
public class DashboardReportExtendedDAO extends DashboardReportBaseDAO {
    
    private String total_patients_ids;
    
    private int total_patients_count;
    private int transfersIn;
    private int transfersOut;
    private int admitted_count;
    
    @Override
    public HashMap<String, DashboardVO> getDashboardReport(
            String category_name, 
            int etiology_id, 
            HashMap<String, DashboardVO> list, 
            DashboardReportVO reportCrit, 
            String key, 
            String startDate, 
            String endDate, 
            String treatment_query, 
            List<Integer> locationsList,
            String locale) {
        
        // Move settings to private members to avoid unnecessary parameter
        // passing since we only are dealing with internal functions
        this.category_name = category_name;
        this.etiology_id = etiology_id;
        this.list = list;
        this.reportCrit = reportCrit;
        this.key = key;
        this.startDate = startDate;
        this.endDate = endDate;
        this.treatment_query = treatment_query;
        this.locationsList = locationsList;
        this.locale = locale;
        
        if (startDate.isEmpty() || endDate.isEmpty()){
            // Blank column - Add null values
            addBlankColumn();
        } else {
            //if the database is MSSQL, we need to pass in the datepart
            this.dateDiff="";
            if(!mysql){dateDiff="DAY,";}

            // Initialize data - Gral searches for restricting in multiple rows
            getInitialData();

            // Patients section (existing, admitted, transferred, discharged)
            getPatientsData();

            // Visits & Assessments
            getVisitsAssessments();

            // Wounds
            // -- All wounds and totals
            getDataAllWounds();

            // Wounds healing
            getWoundHealing();

            // Antimicrobial usage
            getAntimicrobialUsage();

            // Product costs
            getProductCosts();
        }
        
        // Separators
        getSeparators();
        
        return this.list;
    }
    
    private void getInitialData(){
        String filterQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        total_patients_count = 0;
        this.transfersIn = 0;
        this.transfersOut = 0;
        String existingIdString = "";
        // Unique ids, ordered
        TreeSet<Integer> existingIdSet = new TreeSet<Integer>();
        // Get Patients Ids and Count going from Assessments tables
        // The purpose is to exclude from the count all patients that 
        // only had closing asssessments in the period
        String[] assessmentTables = {"wound", "incision", "drain", "ostomy"};
        for (String table : assessmentTables) {
            String existingAssessmentQuery =
                    "SELECT  "
                    + "    DISTINCT(ass.patient_id) "
                    + "FROM assessment_" + table + " ass "
                    + "LEFT JOIN wound_assessment wa "
                    + "    ON assessment_id = wa.id "
                    + filterQuery
                    + "    ass.active=1  "
                    + "    AND ass.visited_on >= '" + startDate + "' "
                    + "    AND ass.visited_on <= '" + endDate + "' "
                    + "    AND ass.closed_date IS NULL "
                    + treatment_query
                    + "GROUP BY ass.patient_id "
                    + "ORDER BY ass.patient_id ASC ";
            ResultSet resultAss = getResults(existingAssessmentQuery);
            if (resultAss != null) {
                try {
                    while (resultAss.next()) {
                        int id = resultAss.getInt("patient_id");
                        existingIdSet.add(id);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }
        total_patients_count = existingIdSet.size();
        existingIdString = "";
        for (Integer idPatient : existingIdSet) {
            existingIdString += idPatient + ", ";
        }
        if (!existingIdString.isEmpty()){
            existingIdString = existingIdString.substring(0, existingIdString.length() - 2);
        }
               
        this.total_patients_ids = existingIdString;
        list = Common.parseDashboardResult(category_name, 1, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_patients_w_visit",locale), ""}}, key, this.total_patients_count, list, "integer", false);
        
        if (total_patients_ids.isEmpty() || this.total_patients_count == 0){
            list = Common.parseDashboardResult(category_name, 3, new String[][]{{Common.getLocalizedReportString("dash_extended.row.admitted_patients",locale), ""}}, key, 0, list, "integer", true);
            list = Common.parseDashboardResult(category_name, 4, new String[][]{{Common.getLocalizedReportString("dash_extended.row.transferred_in",locale), ""}}, key, this.transfersIn, list, "integer", true);
            list = Common.parseDashboardResult(category_name, 5, new String[][]{{Common.getLocalizedReportString("dash_extended.row.transferred_out",locale), ""}}, key, this.transfersOut, list, "integer", true);
            return;
        }
        
        // Admitted Patients Process
        String mssql_top = mysql? "" : " top 1 ";
        String mysql_limit = mysql? " limit 1" : "";
        
        // Get the first ocurrence of patients with assessments
        // and with Admitted status in the the current period
        String admitQuery1 =
                "SELECT "
                    + "    min(patient_id) as patient_id,  "
                    + "    min(created_on) as admitted_on "
                    + "FROM patient_accounts p "
                    + "WHERE "
                    + "    created_on >= '" + startDate + "'  "
                    + "    and created_on <= '" + endDate + "'  "
                    + "    AND (action is null OR action not like '%DISCHARG%') "
                    + "    AND patient_id IN ( " + this.total_patients_ids + ") "
                    + treatment_query
                    + "group by patient_id "
                    + "order by patient_id ";
        ResultSet result = getResults(admitQuery1);
        
        String inLocationAdmittedString = "";
        List<Integer> inLocationAdmittedIdList = new ArrayList<Integer>();
        
        String transferInIds = "";
        String transferOutIds = "";        
        
        if (result != null) {
            try {
                // *** QUERY FOR the immediately previous entry (Earliest) --> Admitted
                String admitQuery2 = 
                        "SELECT " + mssql_top + " action, patient_id, treatment_location_id, created_on "
                        + "from patient_accounts "
                        + "WHERE "
                        + "    patient_id = ? " 
                        + "    AND (created_on IS NULL OR created_on <= ? ) "  // It's possible some entries have created_on NULL
                        + "ORDER BY id DESC " + mysql_limit;
                
                PreparedStatement ps_admit = con.prepareStatement(admitQuery2);
                String admittedIds = "";
                String rejectedIds = "";
                this.admitted_count = 0;
                
                // *** QUERY FOR the immediately previous entry (Earliest) --> Admitted
                String transferPreviousQuery = 
                        "SELECT " + mssql_top + " action, patient_id, treatment_location_id, created_on "
                        + "from patient_accounts "
                        + "WHERE "
                        + "    patient_id = ? " 
                        + "    AND (created_on IS NULL OR created_on <= ? ) "  // It's possible some entries were with created_on = NULL
                        + "ORDER BY id DESC " + mysql_limit;
                
                PreparedStatement ps_prev_transfers = con.prepareStatement(transferPreviousQuery);
                
                // *** QUERY FOR entries from Earliest to End of Period --> TransferIn && TranferOut
                String findTransferQuery = 
                        "SELECT id, action, patient_id, treatment_location_id, created_on "
                        + "from patient_accounts "
                        + "WHERE "
                        + "    patient_id = ? " 
                        + "    AND created_on >= ? AND created_on <= ? " 
                        + "ORDER BY id ASC ";
                
                PreparedStatement ps_transfers = con.prepareStatement(findTransferQuery, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                        
                // *** Checking the immediately previous entry --> Admitted AND TransferIn
                // For each result, check the previous instance to see if it was admitted or not
                while (result.next()) {
                    int patient_id = result.getInt("patient_id");
                    Date admitted_on = result.getDate("admitted_on");
                    
                    // We are evaluating this id, do not consider for special transfer out
                    inLocationAdmittedIdList.add(patient_id);
                    inLocationAdmittedString += patient_id + ", ";
                    
                    ps_admit.setInt(1, patient_id);
                    ps_admit.setDate(2, new java.sql.Date(admitted_on.getTime()));
                    
                    boolean admitted = false;
                    boolean tIn = false;
                    boolean tOut = false;
                    
                    if (ps_admit.execute()){
                        // If there is a previous record, we need to check it
                        ResultSet resultPrevious = ps_admit.getResultSet();
                        
                        if (resultPrevious != null && resultPrevious.next()){
                            String action = resultPrevious.getString("action");
                            int treatment_location_id = resultPrevious.getInt("treatment_location_id");
                            
                            // Previous was Discharged
                            if (action != null && action.startsWith("DISCHARG")){
                                admitted = true;
                            }
                            // Previous in different location
                            if (!locationsList.contains(treatment_location_id)){
                                tIn = true;
                            }
                        } else {
                            // No record
                            admitted = true;
                        }
                    } else {
                        // No previous record, count it
                        admitted = true;
                    }                        
                    
                    if (admitted){
                        this.admitted_count++;
                        admittedIds += patient_id + ", ";
                    } else {
                        rejectedIds += patient_id + ", ";
                    }
                    
                    
                    // *** Checking entry immediately previous to the period -->  TransferIn && TranferOut
                    ps_prev_transfers.setInt(1, patient_id);
                    ps_prev_transfers.setString(2, startDate);
                    
                    Boolean currentlyOut = null;
                    if (ps_prev_transfers.execute()){
                        // If there is a previous record, we need to check it
                        ResultSet resultPrevious = ps_prev_transfers.getResultSet();
                        
                        if (resultPrevious != null && resultPrevious.next()){
                            int treatment_location_id = resultPrevious.getInt("treatment_location_id");
                            
                            if (locationsList.contains(treatment_location_id)){
                                currentlyOut = false;
                            } else {
                                currentlyOut = true;
                            }
                        }
                    }
                        
                    // *** Checking all entries in the period Period --> TransferIn && TranferOut
                    ps_transfers.setInt(1, patient_id);
                    ps_transfers.setString(2, startDate);
                    ps_transfers.setString(3, endDate);
                    
                    ResultSet entries = ps_transfers.executeQuery();
                    //boolean currentlyOut = false;
                    
                    while (entries.next()) {
                        int treatment_location_id = entries.getInt("treatment_location_id");
                        int id = entries.getInt("id");
                        
                        // If no previous row was found, initialize currentlyOut
                        if (currentlyOut == null){
                            currentlyOut = !locationsList.contains(treatment_location_id);
                            continue;
                        }
                        
                        if (currentlyOut && locationsList.contains(treatment_location_id)){
                            tIn = true;
                            currentlyOut = false;
                        } else {
                            if (!currentlyOut && !locationsList.contains(treatment_location_id)){
                                tOut = true;
                                currentlyOut = true;
                            }
                        }
                        
                    }
                    
                    if (tIn){
                        this.transfersIn++;
                        transferInIds += patient_id + ", ";
                    }
                    if (tOut){
                        this.transfersOut++;
                        transferOutIds += patient_id + ", ";
                    }
                }
                
                
                
                if (startDate.startsWith("2014-04-01")) {
                    //System.out.println("Ids Admitted:\n" + admittedIds);
                    //System.out.println("Ids NOT Admitted:\n" + rejectedIds);
                    //System.out.println("\n ****ADMITTED # = " + admitted_count);
                }
                
                list = Common.parseDashboardResult(category_name, 3, new String[][]{{Common.getLocalizedReportString("dash_extended.row.admitted_patients",locale), ""}}, key, admitted_count, list, "integer", true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        }
        
        if (!inLocationAdmittedString.isEmpty()){
            inLocationAdmittedString = inLocationAdmittedString.substring(0, inLocationAdmittedString.length() - 2);
        }
        
        // Special case for Transfer OUT:
        // The client has at least one assessment in the location(s),
        // BUT it doesn't have any entry in the location(s) reported in the period
        // in patients accounts.
        // Is transfer out because the immediately previous entry
        // to the period was inside the reported location(s)
        
        //*** Check for special case Transfer Out
        if (!existingIdString.isEmpty() && !inLocationAdmittedString.isEmpty()){
            try {
                // Get subset: With Assessments but without records in period in the location as Admitted
                String specialTransferOutQuery =
                    "SELECT "
                    +"	distinct(patient_id) "
                    +"FROM patient_accounts "
                    +"WHERE "
                    + "    created_on >= '" + startDate + "'  "
                    + "    and created_on <= '" + endDate + "'  "
                    +"	AND (action is null OR action not like '%DISCHARG%') "
                    +"	AND patient_id IN ( " + existingIdString + " ) "
                    +"	AND patient_id NOT IN (	" + inLocationAdmittedString + " ) "
                    +"GROUP BY patient_id "
                    +"ORDER BY patient_id ";
                result = getResults(specialTransferOutQuery);
                
                if (result != null) {
                    // *** QUERY FOR the immediately previous entry
                    String transferPreviousQuery = 
                        "SELECT " + mssql_top + " action, patient_id, treatment_location_id, created_on "
                        + "from patient_accounts "
                        + "WHERE "
                        + "    patient_id = ? " 
                        + "    AND (created_on IS NULL OR created_on <= ? ) "  // It's possible some entries were with created_on = NULL
                        + "ORDER BY id DESC " + mysql_limit;
                    PreparedStatement ps_prev_transfers = con.prepareStatement(transferPreviousQuery);
                    
                    while (result.next()) {
                        int patient_id = result.getInt("patient_id");
                        
                        // *** Checking the immediately previous entry --> If in location then TransferOut
                        ps_prev_transfers.setInt(1, patient_id);
                        ps_prev_transfers.setString(2, startDate);
                        
                        if (ps_prev_transfers.execute()){
                            // If there is a previous record, we need to check it
                            ResultSet resultPrevious = ps_prev_transfers.getResultSet();

                            if (resultPrevious != null && resultPrevious.next()){
                                int treatment_location_id = resultPrevious.getInt("treatment_location_id");

                                if (locationsList.contains(treatment_location_id)){
                                    // Previously was on location, Transfer Out
                                    this.transfersOut++;
                                    transferOutIds += patient_id + ", ";
                                } 
                            }
                        }
                        
                    }
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
        if (startDate.startsWith("2014-04-01")) {
            //System.out.println("\n\nIds Transfer IN:\n" + transferInIds + "\n  # " + this.transfersIn);
            //System.out.println("Ids Transfer OUT:\n" + transferOutIds  + "\n  # " + this.transfersOut);
        }
        list = Common.parseDashboardResult(category_name, 4, new String[][]{{Common.getLocalizedReportString("dash_extended.row.transferred_in",locale), ""}}, key, this.transfersIn, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 5, new String[][]{{Common.getLocalizedReportString("dash_extended.row.transferred_out",locale), ""}}, key, this.transfersOut, list, "integer", true);
    }
    
    private void getDataAllWounds(){
        String mssql_top = mysql? "" : " top 1 ";
        String mysql_limit = mysql? " limit 1" : "";
        
        //-------------------- // Wounds by goals
        String healable_wounds_query = getWoundCountQuery("healing");
        ResultSet result = getResults(healable_wounds_query);
        HashMap m = getColumns(result, "healing");
        double healable_wounds = Double.parseDouble((String) m.get("healing"));
                
        String maintenance_wounds_query = getWoundCountQuery("maintain");
        result = getResults(maintenance_wounds_query);
        m = getColumns(result, "maintain");
        double maintenance_wounds = Double.parseDouble((String) m.get("maintain"));
        
        String manage_wounds_query = getWoundCountQuery("manage");
        result = getResults(manage_wounds_query);
        m = getColumns(result, "manage");
        double manage_wounds = Double.parseDouble((String) m.get("manage"));

        
        // Adjust for duplicates
        String wounds_goal_count_query = getWoundCountQuery("wounds_goal_count");
        String removeHealableAlphas = "";
        String removeMaintenanceAlphas = "";
        
        result = getResults(wounds_goal_count_query);
        if (result != null) {
            
            try {
                String findLatestGoalQuery =
                        "SELECT "
                        + mssql_top
                        + "    goals.lookup_id "
                        + "    ,goals.alpha_id "
                        + "FROM wound_goals goals  "
                        + "LEFT JOIN wound_profiles prof "
                        + "      ON prof.id = goals.wound_profile_id  "
                        + "WHERE  "
                        + "    goals.alpha_id = ? "
                        + "    AND prof.active = 1 "
                        + "    AND prof.created_on <= '" + endDate + "'  "
                        + "ORDER BY created_on DESC "
                        + mysql_limit;
                PreparedStatement ps_last_goal = con.prepareStatement(findLatestGoalQuery);
                
                while (result.next()) {
                    int count_goals = result.getInt("count_goals");
                    if (count_goals > 1){
                        int alpha_id = result.getInt("alpha_id");
                        // Get the latest valid goal, using wound_profiles.created_on <= end of current period
                        ps_last_goal.setInt(1, alpha_id);
                        
                        if (ps_last_goal.execute()){
                            // If there is a record, we need to check it
                            ResultSet resultPrevious = ps_last_goal.getResultSet();
                            if (resultPrevious != null && resultPrevious.next()){
                                String lookup_id = resultPrevious.getString("lookup_id").trim();
                                if (lookup_id.equals("1081")){
                                    // Remove/discount from Maintenance
                                    maintenance_wounds--;
                                    removeMaintenanceAlphas += alpha_id + ",";
                                } else {
                                    // Remove/discount from Healable
                                    healable_wounds--;
                                    removeHealableAlphas += alpha_id + ",";
                                }
                            }
                            
                        }

                    }
                }
                removeMaintenanceAlphas = removeMaintenanceAlphas.isEmpty()
                        ? ""
                        : removeMaintenanceAlphas.substring(0, removeMaintenanceAlphas.length() - 1);
                removeHealableAlphas = removeHealableAlphas.isEmpty()
                        ? ""
                        : removeHealableAlphas.substring(0, removeHealableAlphas.length() - 1);
            } catch (SQLException e) {
                e.printStackTrace();
            }
                
        }


        list = Common.parseDashboardResult(category_name, 29, new String[][]{{Common.getLocalizedReportString("dash_extended.row.healablewounds",locale), ""}}, key, healable_wounds, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 30, new String[][]{{Common.getLocalizedReportString("dash_extended.row.maintenance_wounds",locale), ""}}, key, maintenance_wounds, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 31, new String[][]{{Common.getLocalizedReportString("dash_extended.row.non_healable_wounds",locale), ""}}, key, manage_wounds, list, "integer", true);
        
        double total_wounds = healable_wounds + maintenance_wounds + manage_wounds;
        // Hidden row
        //list = Common.parseDashboardResult(category_name, 28, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_all_wounds",locale), ""}}, key, total_wounds, list, "integer", false);
        
        String patients_wounds_query = getWoundCountQuery("wound_patients");
        result = getResults(patients_wounds_query);
        m = getColumns(result, "wound_patients");
        double count_wound_patients = Double.parseDouble((String) m.get("wound_patients"));
        
        double avg_wounds_patient = count_wound_patients > 0? total_wounds / count_wound_patients : 0;
        list = Common.parseDashboardResult(category_name, 32, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_wounds_per_patient",locale), ""}}, key, avg_wounds_patient, list, "float", false);
        
        // Closed Healable AND Closed Maintenance
        String healableQuery = getToHealQuery("healable", removeHealableAlphas);
        String maintenanceQuery = getToHealQuery("maintenance", removeMaintenanceAlphas);
        
        if (startDate.startsWith("2014-04-01")) {
            //System.out.println("Closed Healable Query:\n" + healableQuery);
            //System.out.println("Closed Maintenance Query:\n" + maintenanceQuery);
        }
        
        result = getResults(healableQuery);
        list = Common.parseDashboardResult(category_name, 39, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_healable_wounds",locale), "total_closed"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 40, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_total_days_heal",locale), "total_days"}}, key, result, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 41, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_average_days_heal",locale), "avg_days_to_heal"}}, key, result, list, "float", true);
        
        result = getResults(maintenanceQuery);
        list = Common.parseDashboardResult(category_name, 42, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_maintenance_wounds",locale), "total_closed"}}, key, result, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 43, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_total_days_heal",locale), "total_days"}}, key, result, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 44, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_average_days_heal",locale), "avg_days_to_heal"}}, key, result, list, "float", true);
        
        
        
        // Open, New, Closed, Active Wounds (wounds)
        String new_opened_woundsQuery = getWoundByStatusQuery("new_opened_wounds");
        String existent_open_woundsQuery = getWoundByStatusQuery("existent_open_wounds");
        String closed_woundsQuery = getWoundByStatusQuery("closed_wounds");
        
        result = getResults(new_opened_woundsQuery);
        list = Common.parseDashboardResult(category_name, 34, new String[][]{{Common.getLocalizedReportString("dash_extended.row.new_open_wounds",locale), "new_opened_wounds"}}, key, result, list, "integer", true);
        result = getResults(existent_open_woundsQuery);
        list = Common.parseDashboardResult(category_name, 33, new String[][]{{Common.getLocalizedReportString("dash_extended.row.existing_open_wounds",locale), "existent_open_wounds"}}, key, result, list, "integer", true);
        result = getResults(closed_woundsQuery);
        list = Common.parseDashboardResult(category_name, 35, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_wounds",locale), "closed_wounds"}}, key, result, list, "integer", true);
        
        String new_opened_wounds_string = Common.getDashboardValue(list, "row34", key);
        String existent_open_wounds_string = Common.getDashboardValue(list, "row33", key);
        String closed_wounds_string = Common.getDashboardValue(list, "row35", key);
        
        double new_opened_wounds = new_opened_wounds_string.isEmpty()? 0: Double.valueOf(new_opened_wounds_string);
        double existent_open_wounds = existent_open_wounds_string.isEmpty()? 0: Double.valueOf(existent_open_wounds_string);
        double closed_wounds = closed_wounds_string.isEmpty()? 0: Double.valueOf(closed_wounds_string);
        
        double active_open_wounds = existent_open_wounds + new_opened_wounds - closed_wounds;
        list = Common.parseDashboardResult(category_name, 36, new String[][]{{Common.getLocalizedReportString("dash_extended.row.end_quarter_active_wounds",locale), ""}}, key, active_open_wounds, list, "integer", false);
    }
    
    private void getPatientsData(){
        String filterQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        
        if (this.total_patients_ids.isEmpty()){
            list = Common.parseDashboardResult(category_name, 2, new String[][]{{Common.getLocalizedReportString("dash_extended.row.existing_patients",locale), ""}}, key, 0, list, "integer", true);
            list = Common.parseDashboardResult(category_name, 6, new String[][]{{Common.getLocalizedReportString("dash_extended.row.discharged_patients",locale), ""}}, key, 0, list, "integer", true);
            list = Common.parseDashboardResult(category_name, 7, new String[][]{{Common.getLocalizedReportString("dash_extended.row.end_quarter_active_patients",locale), ""}}, key, 0, list, "integer", false);
        } else {
            // # Existing Patients
            String existingQuery = 
                    "SELECT "
                    + "COUNT(DISTINCT(patient_id)) AS patients_existing "
                    + "FROM wound_assessment "
                    + filterQuery
                    + "active=1 "
                    + "AND visited_on <= '" + startDate + "' "
                    + "AND patient_id IN (" + this.total_patients_ids + ") "
                    + treatment_query;

            //System.out.println("Existing: \n"+existingQuery);
            ResultSet result = getResults(existingQuery);
            list = Common.parseDashboardResult(category_name, 2, new String[][]{{Common.getLocalizedReportString("dash_extended.row.existing_patients",locale), "patients_existing"}}, key, result, list, "integer", true);
            String existing_string = Common.getDashboardValue(list, "row2", key);
            double existing_d = 0;
            if (!existing_string.isEmpty()){
                existing_d = Double.valueOf(existing_string);
            }
            int existing = (int) existing_d;

            //# of discharged
            String dischargedQuery = 
                    "select COUNT(distinct (patient_id)) as count "
                    + "from patient_accounts " 
                    + filterQuery 
                    + " account_status=0 "
                    + "and created_on >= '" + startDate + "' "
                    + "and created_on <= '" + endDate + "' "
                    + "and action like '%DISCHARG%' " 
                    + "AND patient_id IN (" + this.total_patients_ids + ") "
                    + treatment_query+" ";
            result = getResults(dischargedQuery);
            //System.out.println("dischargedQuery: \n" + dischargedQuery);
            list = Common.parseDashboardResult(category_name, 6, new String[][]{{Common.getLocalizedReportString("dash_extended.row.discharged_patients",locale), "count"}}, key, result, list, "integer", true);
            String discharged_string = Common.getDashboardValue(list, "row6", key);
            double discharged_d = 0;
            if (!discharged_string.isEmpty()){
                discharged_d = Double.valueOf(discharged_string);
            }
            int discharged = (int) discharged_d;

            int patients_active = existing + this.admitted_count + this.transfersIn - this.transfersOut - discharged;
            list = Common.parseDashboardResult(category_name, 7, new String[][]{{Common.getLocalizedReportString("dash_extended.row.end_quarter_active_patients",locale), ""}}, key, patients_active, list, "integer", false);

        }
        
    }

    
    private void getProductCosts(){
        String baseCostsQuery = 
            "SELECT  "
            + "    SUM(assp.quantity * prod.cost) AS total "
            + "FROM wound_assessment ass  "
            + "    LEFT JOIN assessment_product assp "
            + "        ON assp.assessment_id= ass.id "
            + "    LEFT JOIN products prod "
            + "        ON prod.id = assp.product_id "
            + "    LEFT JOIN wound_assessment_location as wal "
            + "        ON wal.id = assp.alpha_id  "
            + "WHERE  "
            + "        ass.active=1  "
            + "        AND visited_on >= '" + startDate + "' "
            + "        AND visited_on <= '" + endDate + "' " 
            + "        AND assp.product_id > 0 "
            + ((mysql)
                ? " AND CONCAT('', assp.quantity * 1) = assp.quantity AND CONCAT('', prod.cost * 1) = prod.cost "
                : " AND ISNUMERIC(assp.quantity) = 1 AND ISNUMERIC(prod.cost) = 1 ")
            + "        AND assp.quantity != '0' "
            + "        AND assp.product_id NOT IN (1568, 1375) " // Exclude NPWT products
            + "        AND prod.active = 1 "
            + "        AND prod.cost IS NOT NULL "
            + "        AND prod.cost > 0 "
            + treatment_query
            + "        AND wal.open_time_stamp IS NOT NULL "
            + "        AND wal.open_time_stamp <= '" + endDate + "' ";
        
        String openCostsQuery =
                baseCostsQuery
                + "AND (wal.close_time_stamp IS NULL "
                + "OR wal.close_time_stamp > '" + endDate + "')";
        String closedCostsQuery =
                baseCostsQuery
                + "AND wal.close_time_stamp IS NOT NULL "
                + "AND wal.close_time_stamp >= '" + startDate + "' "
                + "AND wal.close_time_stamp <= '" + endDate + "' ";
        
        //open wounds
        //String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        ResultSet result = getResults(openCostsQuery);
        HashMap m = getColumns(result, "total");
        double openCosts = ((String) m.get("total")).isEmpty()
                ? 0
                : Double.parseDouble((String) m.get("total"));
        
        // Calculate NPWT costs
        double npwt = getNPWTCosts("open");
        openCosts += npwt;
        list = Common.parseDashboardResult(category_name, 47, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_open_wound_product_cost",locale), ""}}, key, openCosts, list, "currency", false);
        // Count caretypes
        int openCaretypes = countCaretypes("open");
        double avg_open = openCaretypes > 0? openCosts / openCaretypes : 0;
        list = Common.parseDashboardResult(category_name, 48, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_open_wound_product_cost",locale), ""}}, key, avg_open, list, "currency", false);
        
        //closed wounds
        result = getResults(closedCostsQuery);
        m = getColumns(result, "total");
        double closedCosts = ((String) m.get("total")).isEmpty()
                ? 0
                : Double.parseDouble((String) m.get("total"));
        
        // Calculate NPWT costs
        npwt = getNPWTCosts("closed");
        closedCosts += npwt;
        list = Common.parseDashboardResult(category_name, 49, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_closed_wound_product_cost",locale), ""}}, key, closedCosts, list, "currency", false);
        // Count caretypes
        int closedCaretypes = countCaretypes("closed");
        double avg_closed = closedCaretypes > 0? closedCosts / closedCaretypes : 0;
        list = Common.parseDashboardResult(category_name, 50, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_closed_wound_product_cost",locale), ""}}, key, avg_closed, list, "currency", false);
        
        double totalCosts = openCosts + closedCosts;
        int totalCaretypes = openCaretypes + closedCaretypes;
        double avg_total = totalCaretypes > 0? totalCosts / totalCaretypes : 0;
        
        list = Common.parseDashboardResult(category_name, 51, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_all_wound_product_cost",locale), ""}}, key, totalCosts, list, "currency", false);
        list = Common.parseDashboardResult(category_name, 52, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_all_wound_product_cost",locale), ""}}, key, avg_total, list, "currency", false);
        
        // Get patient counting - Requires getInitialData() to be executed before
        String total_patients_string = Common.getDashboardValue(list, "row1", key);
        double total_patients = 0;
        if (!total_patients_string.isEmpty()){
            total_patients = Double.valueOf(total_patients_string);
        }
        
        double avg_patients = total_patients > 0? totalCosts / total_patients: 0;
        list = Common.parseDashboardResult(category_name, 53, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_cost_per_client",locale), ""}}, key, avg_patients, list, "currency", false);
    }
    
    private double getNPWTCosts(String type){
        String npwtQuery =
                "SELECT  "
            + "	    assp.alpha_id "
            + "     ,ass.id AS ass_id "
            + "     ,ass.created_on "
            + "     ,prod.cost "
            + "     ,nptw.machine_acquirement "
            + "     ,nptw.vac_start_date "
            + "     ,nptw.vac_end_date "
            + "FROM wound_assessment ass  "
            + "	LEFT JOIN assessment_product assp "
            + "        ON assp.assessment_id= ass.id "
            + " LEFT JOIN products prod "
            + "		ON prod.id = assp.product_id "
            + "	LEFT JOIN wound_assessment_location as wal "
            + "		ON wal.id = assp.alpha_id  "
            + "	LEFT JOIN assessment_npwt_alpha nptw_alpha "
            + "		ON nptw_alpha.alpha_id = assp.alpha_id "
            + "	LEFT JOIN assessment_npwt nptw "
            + "		ON nptw.id = nptw_alpha.assessment_npwt_id  "
            + "		AND nptw.assessment_id = ass.id "
            + "WHERE  "
            + "        ass.active=1  "
            + "        AND visited_on >= '" + startDate + "' "
            + "        AND visited_on <= '" + endDate + "' " 
            + ((mysql)
                ? " AND CONCAT('', prod.cost * 1) = prod.cost "
                : " AND ISNUMERIC(prod.cost) = 1 ")
            + "        AND assp.product_id > 0 "
            + "        AND assp.product_id IN (1568, 1375) " // Only npwt product prices
            + "        AND nptw.id IS NOT NULL "
            + "        AND machine_acquirement != 3350 "  // Exclude owned machines
            + treatment_query
            + "        AND wal.open_time_stamp IS NOT NULL "
            + "        AND wal.open_time_stamp <= '" + endDate + "' ";
        
        String openCondition =
                "AND (wal.close_time_stamp IS NULL "
                + "OR wal.close_time_stamp > '" + endDate + "') ";
        String closedCondition = 
                "        AND wal.close_time_stamp IS NOT NULL "
                + "        AND wal.close_time_stamp >= '" + startDate + "' "
                + "        AND wal.close_time_stamp <= '" + endDate + "' ";

        npwtQuery += (type.equals("open"))
                ? openCondition
                : closedCondition;
        
        npwtQuery += "ORDER BY assp.alpha_id, ass.id";
        
        double cost = 0;
        ResultSet result = getResults(npwtQuery);
        if (result != null) {
            try {
                int prev_alpha = 0;
                Date date_start_npwt = null;
                Date date_end_npwt = null;
                double npwt_cost = 0;
                Date tmp_start_npwt, tmp_end_npwt, created_on;
                
                while (result.next()) {
                    int alpha_id = result.getInt("alpha_id");
                    
                    // We just finished a set of assessments for the same alpha?
                    // Calculate costs for the previous alpha
                    if (prev_alpha != alpha_id && prev_alpha != 0){
                        // Calculate # days npwt was enabled
                        date_start_npwt = PDate.removeTime(date_start_npwt);
                        date_end_npwt = PDate.removeTime(date_end_npwt);
                        
                        long days = PDate.getDaysBetween(date_start_npwt, date_end_npwt);
                        //System.out.println(prev_alpha + " - Days NPTW! " + days);
                        cost += (days * npwt_cost);
                    }
                    
                    // Get values for current alpha
                    npwt_cost = result.getFloat("cost");
                    tmp_start_npwt = result.getDate("vac_start_date");
                    tmp_end_npwt = result.getDate("vac_end_date");
                    created_on = result.getDate("created_on");
                    
                    // Is this the first assessment for the alpha?
                    if (prev_alpha != alpha_id){
                        date_start_npwt = (tmp_start_npwt != null)
                                ? tmp_start_npwt
                                : created_on;
                    } 
                    
                    // In any case, get the end date
                    date_end_npwt = (tmp_end_npwt != null)
                            ? tmp_end_npwt
                            : created_on;
                    
                    prev_alpha = alpha_id;
                }
                
                // Add numbers for last alpha_id found
                if (prev_alpha != 0){
                    // Calculate # days npwt was enabled
                    date_start_npwt = PDate.removeTime(date_start_npwt);
                    date_end_npwt = PDate.removeTime(date_end_npwt);

                    long days = PDate.getDaysBetween(date_start_npwt, date_end_npwt);
                    //System.out.println(prev_alpha + " - Days NPTW! " + days);
                    cost += (days * npwt_cost);
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        //System.out.println("** NPWT Costs for " + type + ": " + cost);
        return cost;
    }
    
    private int countCaretypes(String type){
        String[] assessmentTables = {"wound", "incision", "drain", "ostomy"};
        
        String openCondition =
                "AND (wal.close_time_stamp IS NULL "
                + "OR wal.close_time_stamp > '" + endDate + "') ";
        String closedCondition = 
                "        AND wal.close_time_stamp IS NOT NULL "
                + "        AND wal.close_time_stamp >= '" + startDate + "' "
                + "        AND wal.close_time_stamp <= '" + endDate + "' ";
        
        int count = 0;
        for (String table : assessmentTables) {
            String countQuery = 
                    "SELECT  "
                + "	      COUNT(Distinct(wal.id)) AS alphas "
                + "FROM wound_assessment ass  "
                + "	  LEFT JOIN assessment_" + table + " asswound "
                + "        ON asswound.assessment_id = ass.id "
                + "	  LEFT JOIN wound_assessment_location as wal "
                + "	       ON wal.id = asswound.alpha_id  "
                + "WHERE  "
                + "        ass.active=1  "
                + "        AND ass.visited_on >= '" + startDate + "' "
                + "        AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query
                + "        AND wal.open_time_stamp IS NOT NULL "
                + "        AND wal.open_time_stamp <= '" + endDate + "' ";
            
            countQuery += type.equals("open")
                ? openCondition
                : type.equals("closed")
                    ? closedCondition
                    : "";
            
            
            ResultSet result = getResults(countQuery);
            HashMap m = getColumns(result, "alphas");
            count += Integer.parseInt((String) m.get("alphas"));
            
            
            
        }
        //System.out.println("# Caretypes " + type + ": " + count);
        return count;
    }
    
    private void getVisitsAssessments(){
        // Get patient counting - Requires getInitialData() to be executed before
        String total_patients_string = Common.getDashboardValue(list, "row1", key);
        double total_patients = 0;
        if (!total_patients_string.isEmpty()){
            total_patients = Double.valueOf(total_patients_string);
        }
        
        // Visits 
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        
        ResultSet result = getResults(
                "select "
                        + "count(wound_assessment.id) as visit_count, "
                        + "sum(lookup.clinic) as visit_clinic_count,"
                        + "(count(wound_assessment.id)-sum(lookup.clinic)) as visit_home_count "
                        + "from wound_assessment "
                        + "left join lookup on lookup.id=wound_assessment.treatment_location_id " 
                        + builtQuery 
                        + " visit = 1 "
                        + "and visited_on >= '" + startDate + "' "
                        + "and visited_on <= '" + endDate + "' " 
                        + treatment_query);
        list = Common.parseDashboardResult(category_name, 9, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_visits",locale), "visit_count"}}, key, result, list, "integer", false);
        String total_visits_string = Common.getDashboardValue(list, "row9", key);
        double total_visits = 0;
        if (!total_visits_string.isEmpty()){
            total_visits = Double.valueOf(total_visits_string);
        }

        double avg_visits = total_patients > 0? total_visits / total_patients: 0;
        list = Common.parseDashboardResult(category_name, 14, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_visits_client",locale), ""}}, key, avg_visits, list, "float", false);
        
        list = Common.parseDashboardResult(category_name, 10, new String[][]{{Common.getLocalizedReportString("dash_extended.row.home_visits",locale), "visit_home_count"}}, key, result, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 12, new String[][]{{Common.getLocalizedReportString("dash_extended.row.clinic_visits",locale), "visit_clinic_count"}}, key, result, list, "integer", true);

        
        //-------------------- // Count assessments per type
        String wound_ass_query = 
                "SELECT "
                + "    COUNT(ass.id) as count_ass, "
                + "    COUNT(CASE WHEN lookup.clinic = 1 THEN 1 END) as clinic "
                + "FROM assessment_wound ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "LEFT JOIN lookup "
                + "     ON lookup.id = wa.treatment_location_id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        result = getResults(wound_ass_query);
        

        list = Common.parseDashboardResult(category_name, 26, new String[][]{{Common.getLocalizedReportString("dash_extended.row.wound_assessments",locale), "count_ass"}}, key, result, list, "integer", true);
        String wound_count_string = Common.getDashboardValue(list, "row26", key);
        double wound_count = 0;
        if (!wound_count_string.isEmpty()){
            wound_count = Double.valueOf(wound_count_string);
        }

        HashMap m = getColumns(result, "clinic");
        double wound_count_clinic = Double.parseDouble((String) m.get("clinic"));

        String wound_caretypes_query = 
                "SELECT "
                + "   COUNT(DISTINCT(ass.alpha_id)) AS caretypes "
                + "FROM assessment_wound ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.closed_date IS NULL "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        result = getResults(wound_caretypes_query);

        list = Common.parseDashboardResult(category_name, 25, new String[][]{{Common.getLocalizedReportString("dash_extended.row.wound_caretypes", locale), "caretypes"}}, key, result, list, "integer", true);
        String wound_caretypes_count_string = Common.getDashboardValue(list, "row25", key);
        double wound_caretypes_count = 0;
        if (!wound_caretypes_count_string.isEmpty()){
            wound_caretypes_count = Double.valueOf(wound_caretypes_count_string);
        }
        
        String incision_ass_query = 
                "SELECT "
                + "    COUNT(ass.id) as count_ass, "
                + "    COUNT(CASE WHEN lookup.clinic = 1 THEN 1 END) as clinic "
                + "FROM assessment_incision ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "LEFT JOIN lookup "
                + "     ON lookup.id = wa.treatment_location_id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        result = getResults(incision_ass_query);
        

        list = Common.parseDashboardResult(category_name, 24, new String[][]{{Common.getLocalizedReportString("dash_extended.row.incision_assessments",locale), "count_ass"}}, key, result, list, "integer", true);
        String incision_count_string = Common.getDashboardValue(list, "row24", key);
        double incision_count = 0;
        if (!incision_count_string.isEmpty()){
            incision_count = Double.valueOf(incision_count_string);
        }

        m = getColumns(result, "clinic");
        double incision_count_clinic = Double.parseDouble((String) m.get("clinic"));

        String incision_caretypes_query = 
                "SELECT "
                + "   COUNT(DISTINCT(ass.alpha_id)) AS caretypes "
                + "FROM assessment_incision ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.closed_date IS NULL "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        result = getResults(incision_caretypes_query);

        list = Common.parseDashboardResult(category_name, 23, new String[][]{{Common.getLocalizedReportString("dash_extended.row.incision_caretypes", locale), "caretypes"}}, key, result, list, "integer", true);
        String incision_caretypes_count_string = Common.getDashboardValue(list, "row23", key);
        double incision_caretypes_count = 0;
        if (!incision_caretypes_count_string.isEmpty()){
            incision_caretypes_count = Double.valueOf(incision_caretypes_count_string);
        }
        
        String drain_ass_query = 
                "SELECT "
                + "    COUNT(ass.id) as count_ass, "
                + "    COUNT(CASE WHEN lookup.clinic = 1 THEN 1 END) as clinic "
                + "FROM assessment_drain ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "LEFT JOIN lookup "
                + "     ON lookup.id = wa.treatment_location_id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        result = getResults(drain_ass_query);
        

        list = Common.parseDashboardResult(category_name, 22, new String[][]{{Common.getLocalizedReportString("dash_extended.row.tube_drain_assessments",locale), "count_ass"}}, key, result, list, "integer", true);
        String drain_count_string = Common.getDashboardValue(list, "row22", key);
        double drain_count = 0;
        if (!drain_count_string.isEmpty()){
            drain_count = Double.valueOf(drain_count_string);
        }

        m = getColumns(result, "clinic");
        double drain_count_clinic = Double.parseDouble((String) m.get("clinic"));


        String drain_caretypes_query = 
                "SELECT "
                + "   COUNT(DISTINCT(ass.alpha_id)) AS caretypes "
                + "FROM assessment_drain ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.closed_date IS NULL "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        result = getResults(drain_caretypes_query);

        list = Common.parseDashboardResult(category_name, 21, new String[][]{{Common.getLocalizedReportString("dash_extended.row.tube_drain_caretypes", locale), "caretypes"}}, key, result, list, "integer", true);
        String tube_drain_caretypes_count_string = Common.getDashboardValue(list, "row21", key);
        double tube_drain_caretypes_count = 0;
        if (!tube_drain_caretypes_count_string.isEmpty()){
            tube_drain_caretypes_count = Double.valueOf(tube_drain_caretypes_count_string);
        }
        
        String ostomy_ass_query = 
                "SELECT "
                + "    COUNT(ass.id) as count_ass, "
                + "    COUNT(CASE WHEN lookup.clinic = 1 THEN 1 END) as clinic "
                + "FROM assessment_ostomy ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "LEFT JOIN lookup "
                + "     ON lookup.id = wa.treatment_location_id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        result = getResults(ostomy_ass_query);
        

        list = Common.parseDashboardResult(category_name, 20, new String[][]{{Common.getLocalizedReportString("dash_extended.row.ostomy_assessments",locale), "count_ass"}}, key, result, list, "integer", true);
        String ostomy_count_string = Common.getDashboardValue(list, "row20", key);
        double ostomy_count = 0;
        if (!ostomy_count_string.isEmpty()){
            ostomy_count = Double.valueOf(ostomy_count_string);
        }

        m = getColumns(result, "clinic");
        double ostomy_count_clinic = Double.parseDouble((String) m.get("clinic"));


        String ostomy_caretypes_query = 
                "SELECT "
                + "   COUNT(DISTINCT(ass.alpha_id)) AS caretypes "
                + "FROM assessment_ostomy ass "
                + "LEFT JOIN wound_assessment wa " 
                + "     ON assessment_id = wa.id "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND ass.closed_date IS NULL "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + treatment_query;
        
        result = getResults(ostomy_caretypes_query);

        list = Common.parseDashboardResult(category_name, 19, new String[][]{{Common.getLocalizedReportString("dash_extended.row.ostomy_caretypes", locale), "caretypes"}}, key, result, list, "integer", true);
        String ostomy_caretypes_count_string = Common.getDashboardValue(list, "row19", key);
        double ostomy_caretypes_count = 0;
        if (!ostomy_caretypes_count_string.isEmpty()){
            ostomy_caretypes_count = Double.valueOf(ostomy_caretypes_count_string);
        }
        
        double total_ass = wound_count + incision_count + drain_count + ostomy_count;
        double total_clinic = wound_count_clinic + incision_count_clinic + drain_count_clinic + ostomy_count_clinic;
        list = Common.parseDashboardResult(category_name, 18, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_assessments",locale), ""}}, key, total_ass, list, "integer", false);
        list = Common.parseDashboardResult(category_name, 13, new String[][]{{Common.getLocalizedReportString("dash_extended.row.clinic_assessments",locale), ""}}, key, total_clinic, list, "integer", true);

        double total_caretypes = wound_caretypes_count + incision_caretypes_count + tube_drain_caretypes_count + ostomy_caretypes_count;
        list = Common.parseDashboardResult(category_name, 17, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_caretypes", locale), ""}}, key, total_caretypes, list, "integer", false);

        double total_home = total_ass - total_clinic;
        list = Common.parseDashboardResult(category_name, 11, new String[][]{{Common.getLocalizedReportString("dash_extended.row.home_assessments",locale), ""}}, key, total_home, list, "integer", true);

        // Calculate AUTO REFERRASL GENERATED PGR#2 DFC > 3week
        // Dressing Changes Percentages Rows: 13 & 14        
        // Count alphas with more than 3 DC per week
        String queryGreaterThan3week =
                "SELECT "
                    + "COUNT(au.id) as pgr_generated "
                + "FROM auto_referrals au "
                + "LEFT JOIN referrals_tracking ref "
                + "    ON au.referral_id = ref.id "
                + "LEFT JOIN wound_assessment wa "
                + "    ON ref.assessment_id = wa.id "
                + "WHERE "
                    + "wa.visited_on >= '" + startDate + "' "
                    + "AND wa.visited_on <= '" + endDate + "' "
                    + "AND au.auto_referral_type LIKE '%Dress%' "
                    + "AND wa.active = 1 "
                    + treatment_query;
                
                
        result = getResults(queryGreaterThan3week);
        m = getColumns(result, "pgr_generated");
        double pgr2_triggered = Double.parseDouble((String) m.get("pgr_generated"));
        
        double percentage_pgr2 = (total_visits > 0.0)? pgr2_triggered / total_visits : 0.0;

        list = Common.parseDashboardResult(category_name, 15, new String[][]{{Common.getLocalizedReportString("dash_extended.row.pgr2_triggered",locale), ""}}, key, pgr2_triggered, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 16, new String[][]{{Common.getLocalizedReportString("dash_extended.row.percentage_pgr2",locale), ""}}, key, percentage_pgr2, list, "percent2", true);
        
    }
    
    private void getWoundHealing(){
        // By Heal rates
        // String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "assessment_wound");
        // Calculate AUTO REFERRALS GENERATED PGR#1 Heal Rate < 30%
        // Dressing Changes Percentages Rows: 37 & 38
        // Count alphas with less of 30% Heal Rate
        String queryPGR1Query =
                "SELECT "
                    + "COUNT(au.id) as pgr_generated "
                + "FROM auto_referrals au "
                + "LEFT JOIN referrals_tracking ref "
                + "    ON au.referral_id = ref.id "
                + "LEFT JOIN wound_assessment wa "
                + "    ON ref.assessment_id = wa.id "
                + "WHERE "
                    + "wa.visited_on >= '" + startDate + "' "
                    + "AND wa.visited_on <= '" + endDate + "' "
                    + "AND au.auto_referral_type LIKE '%Heal%' "
                    + "AND wa.active = 1 "
                    + treatment_query;
        
        ResultSet result = getResults(queryPGR1Query);
        HashMap m = getColumns(result, "pgr_generated");
        double pgr1_triggered = Double.parseDouble((String) m.get("pgr_generated"));
        
        String healable_wounds_string = Common.getDashboardValue(list, "row29", key);
        double healable_wounds = healable_wounds_string.isEmpty()? 0: Double.valueOf(healable_wounds_string);

        double percentage_pgr1 = (healable_wounds > 0.0)? pgr1_triggered / healable_wounds : 0.0;
        
        list = Common.parseDashboardResult(category_name, 37, new String[][]{{Common.getLocalizedReportString("dash_extended.row.pgr1_triggered",locale), ""}}, key, pgr1_triggered, list, "integer", true);
        list = Common.parseDashboardResult(category_name, 38, new String[][]{{Common.getLocalizedReportString("dash_extended.row.percentage_pgr1",locale), ""}}, key, percentage_pgr1, list, "percent2", true);
    }
    
    private void getAntimicrobialUsage() {
        //String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "assessment_wound");
        
        // Antimicrobial from autorreferals
        String antimicrobialQuery =
                "SELECT "
                + "COUNT(au.id) as pgr_generated "
                + "FROM auto_referrals au "
                + "LEFT JOIN referrals_tracking ref "
                + "    ON au.referral_id = ref.id "
                + "LEFT JOIN wound_assessment wa "
                + "    ON ref.assessment_id = wa.id "
                + "WHERE "
                    + "wa.visited_on >= '" + startDate + "' "
                    + "AND wa.visited_on <= '" + endDate + "' "
                    + "AND au.auto_referral_type LIKE '%micro%' "
                    + "AND ref.active = 1 "
                    + "AND wa.active = 1 "
                    +  treatment_query;
        ResultSet result = getResults(antimicrobialQuery);
        
        list = Common.parseDashboardResult(category_name, 46, new String[][]{{Common.getLocalizedReportString("dash_extended.row.antimicrobial_usage",locale), "pgr_generated"}}, key, result, list, "integer", false);
    }
    
    private String getToHealQuery(String type, String excludingAlphas){
        String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        String filterGoal =
                (type.equals("healable"))
                ? "        AND lookup_id = 1081 "
                : "        AND lookup_id = 1082 ";
        
        String dateDiffQuery = mysql
                ? "    SUM(((DATEDIFF(" + dateDiff + "close_time_stamp,open_time_stamp)))) "
                : "    SUM(((DATEDIFF(" + dateDiff + "open_time_stamp,close_time_stamp)))) ";
        
        return
                "SELECT "
                + dateDiffQuery + " AS total_days, "
                + "    COUNT(id) AS total_closed, "
                + dateDiffQuery + "/SUM(discharge) AS avg_days_to_heal "
                + "FROM wound_assessment_location "
                + builtQuery 
                + "close_time_stamp IS NOT NULL "
                + "AND close_time_stamp >= '" + startDate + "' "
                + "AND close_time_stamp <= '" + endDate + "' "
                + "AND id IN "
                + "    (SELECT DISTINCT wal.id "
                + "    FROM wound_assessment ass  "
                + "    LEFT JOIN assessment_wound asswound "
                + "        ON asswound.assessment_id = ass.id "
                + "    LEFT JOIN wound_goals goals " 
                + "        ON goals.alpha_id = asswound.alpha_id  "
                + "    LEFT JOIN wound_profiles prof "
                + "  	  ON prof.id = goals.wound_profile_id  "
                + "    LEFT JOIN wound_assessment_location wal " 
                + "        ON wal.id = asswound.alpha_id "                
                + "    WHERE "
                + "        ass.active=1 "
                + "        AND asswound.id IS NOT NULL "
                + "        AND asswound.closed_date IS NULL "
                + "        AND ass.visited_on >= '" + startDate + "' "
                + "        AND ass.visited_on <= '" + endDate + "' "
                + (excludingAlphas.isEmpty()
                    ? ""
                    : "        AND asswound.alpha_id NOT IN (" + excludingAlphas + ") ")
                + "        AND prof.active = 1 "
                + filterGoal
                + treatment_query  + ") ";
    }
    
    private String getWoundByStatusQuery(String status){
        //String builtQuery = buildQuery(reportCrit.getWound_type(), etiology_id, "wound_assessment");
        
        String coreQuery =
                "SELECT   "
                + "	COUNT(DISTINCT(wal.id)) AS " + status + " "
                + "FROM wound_assessment ass "
                + "        LEFT JOIN assessment_wound asswound " 
                + "             ON asswound.assessment_id = ass.id "
                + "        LEFT JOIN wound_assessment_location wal "
                + "             ON wal.id = asswound.alpha_id "
                + "WHERE "
                + "    ass.active = 1 "
                + "    AND ass.visited_on >= '" + startDate + "' "
                + "    AND ass.visited_on <= '" + endDate + "' " 
                + "    AND asswound.closed_date IS NULL "
                + treatment_query
                + "    AND wal.open_time_stamp IS NOT NULL ";
        
        
        String filterQuery = "";
        if (status.equals("new_opened_wounds")){
            // wounds opened during this period (with assessments done in the period)
            filterQuery =
                    "AND wal.open_time_stamp >= '" + startDate + "' "
                    + "AND wal.open_time_stamp <= '" + endDate + "' ";
        }
        if (status.equals("existent_open_wounds")){
            // wounds opened Before this period (with assessments done in the period)
            filterQuery =
                    "AND open_time_stamp < '" + startDate + "' ";
        }
        if (status.equals("closed_wounds")){
            // wounds closed in this period (with assessments done in the period)
            filterQuery = 
                    "AND wal.close_time_stamp IS NOT NULL  "
                    + "AND close_time_stamp >= '" + startDate + "' "
                    + "AND close_time_stamp <= '" + endDate + "' ";
        }
        
        return coreQuery + filterQuery;
    }
    
    private String getWoundCountQuery(String woundgoal){
        
        if (woundgoal.equals("wound_patients")) {
            return "SELECT "
                + "        COUNT(DISTINCT ass.patient_id) AS " + woundgoal + " "
                + "FROM wound_assessment ass "
                + "        LEFT JOIN assessment_wound asswound " 
                + "             ON asswound.assessment_id = ass.id "
                + "        LEFT JOIN wound_goals goals " 
                + "             ON goals.alpha_id = asswound.alpha_id "
                + "        LEFT JOIN wound_profiles prof "
                + "  	  ON prof.id = goals.wound_profile_id  "
                + "        LEFT JOIN wound_assessment_location wal "
                + "        ON wal.id = asswound.alpha_id "
                + "WHERE "
                + "        ass.active=1 "
                + "        AND asswound.closed_date IS NULL "
                + "        AND ass.visited_on >= '" + startDate + "'"
                + "        AND ass.visited_on <= '" + endDate + "' "
                + "        AND lookup_id IN (1081, 1082, 3007) "
                + "        AND prof.active = 1 " 
                + treatment_query;
        }
        
        if (woundgoal.equals("wounds_goal_count")) {
            // COUNT number of goals per alpha
            return 
                "SELECT  "
                + "    DISTINCT(asswound.alpha_id) as alpha_id "
                + "    ,COUNT(DISTINCT(lookup_id)) as count_goals "
                + "FROM assessment_wound asswound "
                + "LEFT JOIN wound_assessment ass "
                + "    ON asswound.assessment_id = ass.id "
                + "LEFT JOIN wound_goals goals  "
                + "    ON asswound.alpha_id = goals.alpha_id "
                + "LEFT JOIN wound_profiles prof "
                + "      ON prof.id = goals.wound_profile_id  "
                + "WHERE  "
                + "    ass.active=1  "
                + "    AND asswound.active = 1 "
                + "    AND asswound.closed_date IS NULL  "
                + "    AND ass.visited_on >= '" + startDate + "'"
                + "    AND ass.visited_on <= '" + endDate + "' "
                + "    AND goals.lookup_id IN (1081,1082) "
                + "    AND prof.active = 1 "
                + treatment_query
                + "GROUP BY asswound.alpha_id "
                + "ORDER by count_goals, alpha_id ";
        }
        
        String filter_id = (woundgoal.equals("healing"))
                ? "and lookup_id = 1081 "
                : (woundgoal.equals("maintain"))
                ? "and lookup_id = 1082 "
                : (woundgoal.equals("manage"))
                ? "and lookup_id = 3007 "
                : "";
        
        return "SELECT "
            + "        COUNT(DISTINCT wal.id) AS " + woundgoal + " "
            + "FROM wound_assessment ass "
            + "        LEFT JOIN assessment_wound asswound " 
            + "             ON asswound.assessment_id = ass.id "
            + "        LEFT JOIN wound_goals goals " 
            + "             ON goals.alpha_id = asswound.alpha_id "
            + "        LEFT JOIN wound_profiles prof "
            + "  	  ON prof.id = goals.wound_profile_id  "
            + "        LEFT JOIN wound_assessment_location wal "
            + "        ON wal.id = asswound.alpha_id "
            + "WHERE "
            + "        ass.active=1 "
            + "        and ass.visited_on >= '" + startDate + "' "
            + "        AND ass.visited_on <= '" + endDate + "' "
            + "        AND asswound.closed_date IS NULL "
            + filter_id
            + "        AND prof.active = 1 " 
            + treatment_query;
    }

    private void getSeparators(){
        addSeparator(8);
        addSeparator(27);
        addSeparator(45);
    }

    private void addSeparator(int orderBy){
        DashboardVO row = new DashboardVO();
        row.setFormat("separator");
        row.setReportName(category_name);
        row.setName("");
        row.setOrderby(orderBy);
        row.setQuarter1(null);
        row.setQuarter2(null);
        row.setQuarter3(null);
        row.setQuarter4(null);
        this.list.put("row" + orderBy, row);
    }
    
    private void addBlankColumn(){
        //System.out.println("BLANK COLUMN! key=" + key + "  ");
        list = Common.parseDashboardResult(category_name, 1, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_patients_w_visit",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 3, new String[][]{{Common.getLocalizedReportString("dash_extended.row.admitted_patients",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 4, new String[][]{{Common.getLocalizedReportString("dash_extended.row.transferred_in",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 5, new String[][]{{Common.getLocalizedReportString("dash_extended.row.transferred_out",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 29, new String[][]{{Common.getLocalizedReportString("dash_extended.row.healablewounds",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 30, new String[][]{{Common.getLocalizedReportString("dash_extended.row.maintenance_wounds",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 31, new String[][]{{Common.getLocalizedReportString("dash_extended.row.non_healable_wounds",locale), ""}}, key, "", list, "integer", true);
        // Hidden row
        //list = Common.parseDashboardResult(category_name, 28, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_all_wounds",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 32, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_wounds_per_patient",locale), ""}}, key, "", list, "float", false);
        list = Common.parseDashboardResult(category_name, 39, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_healable_wounds",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 40, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_total_days_heal",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 41, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_average_days_heal",locale), ""}}, key, "", list, "float", true);
        list = Common.parseDashboardResult(category_name, 42, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_maintenance_wounds",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 43, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_total_days_heal",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 44, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_average_days_heal",locale), ""}}, key, "", list, "float", true);
        list = Common.parseDashboardResult(category_name, 34, new String[][]{{Common.getLocalizedReportString("dash_extended.row.new_open_wounds",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 33, new String[][]{{Common.getLocalizedReportString("dash_extended.row.existing_open_wounds",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 35, new String[][]{{Common.getLocalizedReportString("dash_extended.row.closed_wounds",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 36, new String[][]{{Common.getLocalizedReportString("dash_extended.row.end_quarter_active_wounds",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 2, new String[][]{{Common.getLocalizedReportString("dash_extended.row.existing_patients",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 6, new String[][]{{Common.getLocalizedReportString("dash_extended.row.discharged_patients",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 7, new String[][]{{Common.getLocalizedReportString("dash_extended.row.end_quarter_active_patients",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 47, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_open_wound_product_cost",locale), ""}}, key, "", list, "currency", false);
        list = Common.parseDashboardResult(category_name, 48, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_open_wound_product_cost",locale), ""}}, key, "", list, "currency", false);
        list = Common.parseDashboardResult(category_name, 49, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_closed_wound_product_cost",locale), ""}}, key, "", list, "currency", false);
        list = Common.parseDashboardResult(category_name, 50, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_closed_wound_product_cost",locale), ""}}, key, "", list, "currency", false);
        list = Common.parseDashboardResult(category_name, 51, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_all_wound_product_cost",locale), ""}}, key, "", list, "currency", false);
        list = Common.parseDashboardResult(category_name, 52, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_all_wound_product_cost",locale), ""}}, key, "", list, "currency", false);
        list = Common.parseDashboardResult(category_name, 53, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_cost_per_client",locale), ""}}, key, "", list, "currency", false);
        list = Common.parseDashboardResult(category_name, 9, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_visits",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 14, new String[][]{{Common.getLocalizedReportString("dash_extended.row.average_visits_client",locale), ""}}, key, "", list, "float", false);
        list = Common.parseDashboardResult(category_name, 10, new String[][]{{Common.getLocalizedReportString("dash_extended.row.home_visits",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 12, new String[][]{{Common.getLocalizedReportString("dash_extended.row.clinic_visits",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 26, new String[][]{{Common.getLocalizedReportString("dash_extended.row.wound_assessments",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 25, new String[][]{{Common.getLocalizedReportString("dash_extended.row.wound_caretypes", locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 24, new String[][]{{Common.getLocalizedReportString("dash_extended.row.incision_assessments",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 23, new String[][]{{Common.getLocalizedReportString("dash_extended.row.incision_caretypes", locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 22, new String[][]{{Common.getLocalizedReportString("dash_extended.row.tube_drain_assessments",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 21, new String[][]{{Common.getLocalizedReportString("dash_extended.row.tube_drain_caretypes", locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 20, new String[][]{{Common.getLocalizedReportString("dash_extended.row.ostomy_assessments",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 19, new String[][]{{Common.getLocalizedReportString("dash_extended.row.ostomy_caretypes", locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 18, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_assessments",locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 13, new String[][]{{Common.getLocalizedReportString("dash_extended.row.clinic_assessments",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 17, new String[][]{{Common.getLocalizedReportString("dash_extended.row.total_caretypes", locale), ""}}, key, "", list, "integer", false);
        list = Common.parseDashboardResult(category_name, 11, new String[][]{{Common.getLocalizedReportString("dash_extended.row.home_assessments",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 15, new String[][]{{Common.getLocalizedReportString("dash_extended.row.pgr2_triggered",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 16, new String[][]{{Common.getLocalizedReportString("dash_extended.row.percentage_pgr2",locale), ""}}, key, "", list, "percent2", true);
        list = Common.parseDashboardResult(category_name, 37, new String[][]{{Common.getLocalizedReportString("dash_extended.row.pgr1_triggered",locale), ""}}, key, "", list, "integer", true);
        list = Common.parseDashboardResult(category_name, 38, new String[][]{{Common.getLocalizedReportString("dash_extended.row.percentage_pgr1",locale), ""}}, key, "", list, "percent2", true);
        list = Common.parseDashboardResult(category_name, 46, new String[][]{{Common.getLocalizedReportString("dash_extended.row.antimicrobial_usage",locale), ""}}, key, "", list, "integer", false);
    }
}
