<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="last_assessments"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="792"
		 columnWidth="552"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
                 resourceBundle="ReportResources" 
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="ReportUser" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ReportDateRange" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ReportGenerationDate" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ImagesPath" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="ReportGroupingHeader" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ReportTitle" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ReportSubTitle" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ReportLocationsSelection" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ReportColumnDate" isForPrompting="false" class="java.lang.String"/>

	<field name="patientNameFull" class="java.lang.String"/>
	<field name="treatmentLocation" class="java.lang.String"/>
	<field name="lastAssessmentDate" class="java.lang.String"/>
	<field name="patientAccountId" class="java.lang.Integer"/>
	<field name="woundProfile" class="java.lang.String"/>
	<field name="woundAlpha" class="java.lang.String"/>
	<field name="grouping" class="java.lang.String"/>
	<field name="woundGoal" class="java.lang.String"/>


		<group  name="grouping" >
			<groupExpression><![CDATA[$F{grouping}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="treatmentLocation" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{treatmentLocation}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="false" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="patientAccountId" >
			<groupExpression><![CDATA[$F{patientAccountId}]]></groupExpression>
			<groupHeader>
			<band height="5"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="184"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="75"
						width="552"
						height="25"
						forecolor="#FFFFFF"
						backcolor="#6866FF"
						key="staticText-8"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font  size="14" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{ReportSubTitle}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="33"
						width="552"
						height="39"
						forecolor="#FFFFFF"
						backcolor="#33CC34"
						key="staticText-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font size="31" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{ReportTitle}]]></textFieldExpression>
				</textField>
				<image  scaleImage="RetainShape" vAlign="Middle" hAlign="Center" isUsingCache="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="90"
						height="30"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="image-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<graphicElement stretchType="NoStretch" pen="None" fill="Solid" />
					<imageExpression class="java.lang.String"><![CDATA[$P{ImagesPath}+"pixalere_report_logo.gif"]]></imageExpression>
				</image>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="0"
						y="102"
						width="552"
						height="37"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"
						stretchType="RelativeToTallestObject"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{ReportDateRange}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement
						mode="Transparent"
						x="98"
						y="160"
						width="35"
						height="24"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="8" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression><![CDATA[$R{pixalere.reports.last_assessments.patient_pix_id}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement
						mode="Transparent"
						x="134"
						y="160"
						width="96"
						height="24"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="8" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression><![CDATA[$R{pixalere.reports.last_assessments.patient_name}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement
						mode="Transparent"
						x="231"
						y="160"
						width="112"
						height="24"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-7"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="8" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression><![CDATA[$R{pixalere.reports.last_assessments.wound_profile}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement
						mode="Transparent"
						x="344"
						y="160"
						width="63"
						height="24"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-11"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="8" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression><![CDATA[$R{pixalere.reports.last_assessments.care_type}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="139"
						width="349"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{grouping}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement
						mode="Transparent"
						x="0"
						y="160"
						width="98"
						height="24"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="8" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
					<textFieldExpression><![CDATA[$R{pixalere.reports.common.treatment_location}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement
						mode="Transparent"
						x="408"
						y="160"
						width="86"
						height="24"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="8" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression><![CDATA[$R{pixalere.reports.last_assessments.wound_goal}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="494"
						y="160"
						width="58"
						height="24"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font  size="8" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{ReportColumnDate}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="12"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="98"
						y="0"
						width="30"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{patientAccountId}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="134"
						y="0"
						width="96"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="patientAccountId"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{patientNameFull}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="494"
						y="0"
						width="58"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{lastAssessmentDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="231"
						y="0"
						width="112"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="patientAccountId"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{woundProfile}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="0"
						y="0"
						width="98"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-8"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{treatmentLocation}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="344"
						y="0"
						width="63"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{woundAlpha}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="408"
						y="0"
						width="86"
						height="12"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{woundGoal}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="20"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						mode="Opaque"
						x="0"
						y="8"
						width="552"
						height="0"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="line-1"/>
					<graphicElement stretchType="NoStretch" pen="Thin" fill="Solid" />
				</line>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="1"
						y="10"
						width="470"
						height="10"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-6"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$R{pixalere.reports.footer.generated} +$P{ReportGenerationDate}+ " " + $R{pixalere.reports.footer.generated_by} +$P{ReportUser}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="439"
						y="10"
						width="100"
						height="10"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$R{pixalere.reports.footer.page} + $V{PAGE_NUMBER} + " " + $R{pixalere.reports.footer.page_of}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="539"
						y="10"
						width="13"
						height="10"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-10"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER} + ""]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<lastPageFooter>
			<band height="50"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						mode="Opaque"
						x="0"
						y="38"
						width="552"
						height="0"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="line-2"/>
					<graphicElement stretchType="NoStretch" pen="Thin" fill="Solid" />
				</line>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="1"
						y="40"
						width="470"
						height="10"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$R{pixalere.reports.footer.generated} +$P{ReportGenerationDate}+ " " + $R{pixalere.reports.footer.generated_by} +$P{ReportUser}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="439"
						y="40"
						width="100"
						height="10"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-15"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$R{pixalere.reports.footer.page} + $V{PAGE_NUMBER} + " " + $R{pixalere.reports.footer.page_of}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="539"
						y="40"
						width="13"
						height="10"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER} + ""]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="0"
						y="9"
						width="552"
						height="25"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None" lineSpacing="Single">
						<font   size="8" isBold="false" isItalic="true" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$R{pixalere.reports.last_assessments.footer_note} + $P{ReportLocationsSelection}]]></textFieldExpression>
				</textField>
			</band>
		</lastPageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
