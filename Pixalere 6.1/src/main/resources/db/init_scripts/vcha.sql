CREATE TABLE schema_version (
    [version] NVARCHAR(20) PRIMARY KEY,
    [description] NVARCHAR(100),
    [type] NVARCHAR(10) NOT NULL,
    [script] NVARCHAR(200) NOT NULL UNIQUE,
    [checksum] INT,
    installed_by NVARCHAR(30) NOT NULL,
    installed_on DATETIME DEFAULT GETDATE(),
    execution_time INT,
    version_rank int,
    installed_rank int,
    success BIT NOT NULL
);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, success, installed_rank,version_rank)
VALUES
	('4.1.1','initial script','SQL','V4_1_1__Meditech_Integration.sql',-1597298252,'root',getdate(),1519,1,1,1);
select 'Alter table ' +b.name + ' drop constraint '+ a.name from sysobjects a,sysobjects b where a.xtype in ('C','D','F','PK','UQ') and (a.name like '%UQ__schema%' or a.name like '%DF__listdata%' or a.name like '%DF__professio%' or a.name like '%DF__assessmen__num_s%' or a.name like '%DF__assessmen__num_u%' or a.name like '%DF__component__colum%' or a.name like '%DF__component__ignor%' or a.name like '%DF__component__detai%' or a.name like '%DF__component__label%' or a.name like '%DF__diagnosti__data%' or a.name like '%DF__limb_adv___data%') and b.id = a.parent_obj
 -- insert to IHA/VCH on 6.0 upgrade
insert into schema_version values('4.1.0','Pre Meditech','SQL','V4_1_0__pre_meditech.sql','','root','2014-01-01 00:00:00',1519,1,1,1);
update schema_version set description='Meditech Integration' where version='4.1.1';
--cha
alter table patients add primary key(id);
ALTER SCHEMA dbo TRANSFER pixalere.assessment_burn;
ALTER SCHEMA dbo TRANSFER pixalere.assessment_drain;
ALTER SCHEMA dbo TRANSFER pixalere.assessment_incision;
ALTER SCHEMA dbo TRANSFER pixalere.assessment_ostomy;
ALTER SCHEMA dbo TRANSFER pixalere.assessment_product;
ALTER SCHEMA dbo TRANSFER pixalere.braden;
ALTER SCHEMA dbo TRANSFER pixalere.components;
ALTER SCHEMA dbo TRANSFER pixalere.configuration;
ALTER SCHEMA dbo TRANSFER pixalere.dbversion;
ALTER SCHEMA dbo TRANSFER pixalere.dressing_change_frequency;
ALTER SCHEMA dbo TRANSFER pixalere.event;
ALTER SCHEMA dbo TRANSFER pixalere.foot_assessment;
ALTER SCHEMA dbo TRANSFER pixalere.information_popup;
ALTER SCHEMA dbo TRANSFER pixalere.library;
ALTER SCHEMA dbo TRANSFER pixalere.limb_assessment;
ALTER SCHEMA dbo TRANSFER pixalere.location_images;
ALTER SCHEMA dbo TRANSFER pixalere.nursing_care_plan;
ALTER SCHEMA dbo TRANSFER pixalere.patient_report;
ALTER SCHEMA dbo TRANSFER pixalere.products;
ALTER SCHEMA dbo TRANSFER pixalere.products_categories;
ALTER SCHEMA dbo TRANSFER pixalere.resources;
ALTER SCHEMA dbo TRANSFER pixalere.table_updates;
ALTER SCHEMA dbo TRANSFER pixalere.treatment_comments;
ALTER SCHEMA dbo TRANSFER pixalere.update_log;
ALTER SCHEMA dbo TRANSFER pixalere.wound_location_details;
ALTER SCHEMA dbo TRANSFER pixalere.wound_profile_type;

update schema_version set description='Antibiotics',type='JDBC' where version='6.0.5.1';