update configuration set current_value='5-4' where config_setting='phnFormat';
insert into information_popup (title,description,component_id) values ('','<br/><b>Abscess:</b> A local collection of purulent and/or sanguineous drainage that may be incised and drained. 
<br/><b>Arterial Ulcer:</b> Caused by a disruption to arterial blood flow leading to moderate / severe tissue ischemia. 
<br/><b>Arterial/Venous Ulcer:</b> Caused by both venous insufficiency and disrupted arterial blood flow. 
<br/><b>Burn:</b> Tissue loss/damage as a result from thermal, chemical, electrical, and radiation sources. 
<br/><b>Diabetic/Neuropathic Ulcers:</b> A result of neuropathy, structural foot changes, and/or altered blood flow related to diabetes mellitus (or alcoholism, renal failure, HIV, late stage syphilis, trauma and surgery).
<br/><b>Donor Site:</b> An area of the patient’s skin (with or without deeper tissue structures) that is surgically removed and placed elsewhere on the body.
<br/><b>Drug Reaction:</b> A wound or skin reaction where the cause is related to the intake/absorption of a drug/medication.
<br/><b>Graft Site:</b> An area of the patient’s body where skin (with or without deeper tissue structures) is surgically grafted from another area of the body.
<br/><b>Fistula- Stomatized:</b> A fistula with a budded opening between the skin and the associated structure/space/organ (eg: small bowel).
<br/><b>Fistula Non Stomatized:</b> A fistula where the nurse cannot visualize a specific area that has connected with the skin. There may be several diffuse areas open on the skin (or in tunnel/undermining).
<br/><b>Incontinence Associated Dermatitis:</b> Skin damage associated to urine and/or fecal incontinence. 
<br/><b>Infectious:</b> Skin rash or ulcer from an infectious process and can be categorize according to the organism: fungus, bacteria, virus, or anthropod. 
<br/><b>Inflammatory Disease:</b> Present as atypical ulcers and can be misidentified as venous or arterial ulcers. (i.e.: Pyoderma Granulosum, Vasculitis, Drug-Induced Vasculitis). 
<br/><b>Irradiation:</b> Complications from radiation therapy that can present as erythema, dry desquamination,  moist desquamination, and/or permanent skin changes.
<br/><b>Lymphedema:</b> Lymphatic obstruction / blockage of the lymph vessels causing swelling. It is caused by damage to the lymph system, congenital defect to the lymph system, or parasitic infection (filariasis). 
<br/><b>Malignant:</b> Cancerous lesion/wound that may present as painful, fungating and/or friable wounds.
<br/><b>Pilonidial Sinus:</b> An abnormal tract/sinus extending from the skin, usually near the cleft of the buttock.
<br/><b>Pressure Ulcers:</b> Caused as a result of pressure, friction, shearing and often (though not always) located over a bony prominence. 
<br/><b>Stage 1</b>- Intact skin with localized nonblanchable erythema; darkly pigmented skin may not show visible blanching but will appear different than the colour of surrounding skin.
<br/><b>Stage 2</b>- Partial thickness wound presenting as a shallow open ulcer with a red / pink wound bed, slough may be present but does not obscure the depth of tissue loss; may also present as an intact or open/ruptured serum-filled or serosanguineous filled blister.
<br/><b>Stage 3</b>- Full thickness wound; subcutaneous tissue may be visible but bone, tendon and muscle are not exposed; may include undermining and/or sinus tracks; slough or eschar may be present but does not obscure the depth of tissue loss.
<br/><b>Stage 4</b>- Full thickness wound with exposed bone, tendon or muscle; often includes undermining and/or sinus tracks; slough or eschar may be present on some parts of the wound bed but does not obscure the depth of tissue loss.
<br/><b>Suspected Deep Tissue Injury (SDTI) - A localized purple or maroon area of intact skin or a blood filled blister that occur when underlying soft tissue is damaged from friction or shear.
<br/><b>Unstageable, Stage X - A wound in which the wound bed is covered by sufficient slough and / or eschar to preclude staging.
<br/><b>Skin Disease:</b> Wounds develop as a manifestation of a disease process or complication associated with the treatment of a disease.(i.e.: Epidermolysis Bullosa, Calciphylaxis, Graft-versus-Host Disease).
<br/><b>Skin Tear:</b> Skin tears are the result of trauma caused by shearing, friction, or blunt force to the skin. Consider risk factors (i.e.: advanced age, immobility, inadequate hydration/nutrition, falls) 
<br/><b>Surgical Wounds Primary intention:</b> Wound margins are approximated with sutures, tape or staples and wounds heal without the need for granulation (use the incision module in Pixalere).
<br/><b>Surgical Wounds Secondary intention:</b> Incision has dehisced and surgical closure is not possible; wounds must heal by granulation (use the wound module in Pixalere).
<br/><b>Trauma:</b> (Puncture, Abrasion, Stab, Bite, Gunshot, Laceration): Wound caused by trauma/force expected to heal with measureable, objective signs of improvement after three weeks of care. If not, re-review the etiology.
<br/><b>Venous Ulcer:</b> Caused by venous insufficiency due to valve dysfunction, complete or partial blockage of the deep veins, and / or failure of the calf muscle pump. 
',100);
update components set info_popup_id=133 where id=100;
update configuration set current_value='mail.albertahealthservices.ca' where config_setting='smtp_address';
update user_position set colour='#fde3f7' where title='ET' or title='ETRN';
update user_position set colour='#e6blfb' where title='WORP';
update configuration set current_value='0' where config_setting='reportLastAssessments';
update configuration set current_value='d:\\shared\\photos' where config_setting='photos_path';
update products set active=0;
update products_categories set active=0 where title='absorbtion' or title='Vac Dressings' or title='Impregnated – non-adherent' or title='Cleansers and skin prep/protectants';
delete from user_account_regions where exists (select id from professional_accounts where user_account_id=professional_accounts.id and training_flag=1);
INSERT INTO user_account_regions (treatment_location_id,user_account_id)  select 201,id from professional_accounts where training_flag=1;
update lookup set category_id=200 where id=201;
update configuration set current_value='<b>For Wound(s):</b><br/><b>Goal of Products:</b><br/><b>Wound Dressing Protocol:</b><br/><b>Next Full Assessment Due:</b><br/>' where config_setting='ncpTemplate';