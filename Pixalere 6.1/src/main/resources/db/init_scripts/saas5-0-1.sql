//switch datetie6
alter table account_assignments change created_on created_on  datetime(6) ;
alter table assessment_burn change column  created_on created_on datetime(6) ;
alter table assessment_wound change column  created_on created_on datetime(6) ;
alter table assessment_drain change column  created_on created_on datetime(6) ;
alter table assessment_incision change column  created_on created_on datetime(6) ;
alter table assessment_ostomy change column  created_on created_on datetime(6) ;
alter table assessment_skin change column  created_on created_on datetime(6) ;
alter table assessment_burn change lastmodified_on lastmodified_on  datetime(6)  ;
alter table assessment_wound change lastmodified_on lastmodified_on  datetime(6)   ;
alter table assessment_drain change lastmodified_on lastmodified_on  datetime(6)  ;
alter table assessment_incision change lastmodified_on lastmodified_on  datetime(6)  ;
alter table assessment_ostomy change lastmodified_on lastmodified_on  datetime(6)  ;
alter table assessment_skin change lastmodified_on lastmodified_on  datetime(6)   ;
alter table assessment_comments change column  created_on created_on  datetime(6) ;
alter table assessment_comments change deleted_on  deleted_on datetime(6)  ;
alter table assessment_burn change deleted_on  deleted_on datetime(6)  ;
alter table assessment_wound change deleted_on  deleted_on datetime(6)  ;
alter table assessment_drain change deleted_on  deleted_on datetime(6)  ;
alter table assessment_incision change deleted_on  deleted_on datetime(6)   ;
alter table assessment_ostomy change deleted_on  deleted_on datetime(6)  ;
alter table assessment_skin change deleted_on  deleted_on datetime(6)   ;
alter table braden change column  created_on created_on datetime(6) after current_flag;
alter table braden change deleted_on deleted_on datetime(6) ;
alter table braden change column  lastmodified_on lastmodified_on  datetime(6) ;
alter table diagnostic_investigation change column  created_on created_on  datetime(6) ;
alter table diagnostic_investigation change column  lastmodified_on lastmodified_on  datetime(6) ;
alter table diagnostic_investigation change deleted_on  deleted_on  datetime(6)   ;
alter table dressing_change_frequency change column  created_on created_on  datetime(6) ;
alter table dressing_change_frequency change deleted_on deleted_on  datetime(6)  ;
alter table foot_assessment change column  created_on created_on   datetime(6) ;
alter table foot_assessment change column lastmodified_on lastmodified_on  datetime(6) ;
alter table foot_assessment change deleted_on deleted_on  datetime(6)  ;
alter table limb_adv_assessment change column  created_on created_on  datetime(6) ;
alter table limb_adv_assessment change column  lastmodified_on lastmodified_on datetime(6) ;
alter table limb_adv_assessment change deleted_on deleted_on  datetime(6)  ;
alter table limb_basic_assessment change column  created_on created_on datetime(6) ;
alter table limb_basic_assessment change column  lastmodified_on lastmodified_on datetime(6) ;
alter table limb_basic_assessment change deleted_on deleted_on  datetime(6)  ;;
alter table limb_upper_assessment change column  created_on created_on datetime(6) ;
alter table limb_upper_assessment change column  lastmodified_on lastmodified_on datetime(6) ;
alter table limb_upper_assessment change deleted_on deleted_on  datetime(6) ;
alter table nursing_care_plan change column  created_on created_on datetime(6) ;
alter table nursing_care_plan change deleted_on deleted_on  datetime(6)   ;
alter table patient_profiles change column  created_on created_on datetime(6) ;
alter table patient_profiles change column  lastmodified_on lastmodified_on datetime(6) ;
alter table patient_profiles change deleted_on deleted_on  datetime(6)  ;
alter table wound_profiles change column  created_on created_on datetime(6) ;
alter table wound_profiles change column  lastmodified_on lastmodified_on datetime(6) ;
alter table wound_profiles change deleted_on deleted_on  datetime(6)  ;
alter table physical_exam change column  created_on created_on  datetime(6) ;
alter table physical_exam change deleted_on deleted_on  datetime(6)  ;
alter table referrals_tracking change column  created_on created_on datetime(6) ;
alter table treatment_comments change column  created_on created_on datetime(6) ;
alter table treatment_comments change deleted_on deleted_on  datetime(6)  ;
alter table wound_assessment change column  created_on created_on datetime(6) a;
alter table wound_assessment change column  lastmodified_on lastmodified_on datetime(6) ;
alter table wound_assessment_location change column  lastmodified_on lastmodified_on datetime(6) ;
alter table wound_profile_type change deleted_on deleted_on datetime(6) ;
alter table wound_assessment_location change deleted_on deleted_on datetime(6) ;
alter table wound_profile_type change column closed_date closed_date  datetime(6) ;
alter table wounds change deleted_on deleted_on datetime(6);
alter table patients change created_on created_on datetime(6);
alter table patient_accounts change created_on created_on datetime(6);


CREATE TABLE `schema_version` (
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  UNIQUE KEY `version` (`version`),
  KEY `schema_version_vr_idx` (`version_rank`),
  KEY `schema_version_ir_idx` (`installed_rank`),
  KEY `schema_version_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, success, installed_rank,version_rank)
VALUES
	('6.0.11','passwordresetconfig','SQL','V6_0_11__passwordresetconfig.sql',-1597298252,'root',now(),1519,1,1,1);