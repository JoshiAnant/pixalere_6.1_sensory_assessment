-- MySQL dump 10.13  Distrib 5.5.23, for Win64 (x86)
--
-- Host: localhost    Database: pixalere_live
-- ------------------------------------------------------
-- Server version	5.5.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_assignments`
--

DROP TABLE IF EXISTS `account_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `assign_to_id` int(11) DEFAULT NULL,
  `account_type` text,
  `time_stamp` int(11) DEFAULT NULL,
  `assigned_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `assign_to_id` (`assign_to_id`),
  CONSTRAINT `account_assignments_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `account_assignments_ibfk_2` FOREIGN KEY (`assign_to_id`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_assignments`
--

LOCK TABLES `account_assignments` WRITE;
/*!40000 ALTER TABLE `account_assignments` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_assignments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessment_antibiotics`
--

DROP TABLE IF EXISTS `assessment_antibiotics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_antibiotics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assessment_treatment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `antibiotic` varchar(255) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_treatment_id` (`assessment_treatment_id`),
  CONSTRAINT `assessment_antibiotics_ibfk_1` FOREIGN KEY (`assessment_treatment_id`) REFERENCES `assessment_treatment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_antibiotics`
--

LOCK TABLES `assessment_antibiotics` WRITE;
/*!40000 ALTER TABLE `assessment_antibiotics` DISABLE KEYS */;
/*!40000 ALTER TABLE `assessment_antibiotics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessment_burn`
--

DROP TABLE IF EXISTS `assessment_burn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_burn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `full_assessment` int(11) DEFAULT NULL,
  `pain` int(11) DEFAULT NULL,
  `pain_comments` text,
  `burn_wound` text,
  `exudate_colour` text,
  `exudate_amount` text,
  `grafts` text,
  `donors` text,
  `priority` int(11) DEFAULT NULL,
  `discharge_reason` varchar(50) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `discharge_other` varchar(255) DEFAULT NULL,
  `wound_date` varchar(255) DEFAULT NULL,
  `closed_date` varchar(255) DEFAULT NULL,
  `last_update` varchar(255) DEFAULT NULL,
  `cs_show` int(11) DEFAULT NULL,
  `prev_alpha_id` int(11) DEFAULT NULL,
  `moved_reason` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `assessment_burn_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_burn_ibfk_2` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_burn_ibfk_3` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_burn_ibfk_4` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_burn`
--

LOCK TABLES `assessment_burn` WRITE;
/*!40000 ALTER TABLE `assessment_burn` DISABLE KEYS */;
/*!40000 ALTER TABLE `assessment_burn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessment_comments`
--

DROP TABLE IF EXISTS `assessment_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `professional_id` int(11) DEFAULT NULL,
  `time_stamp` int(11) DEFAULT NULL,
  `body` text,
  `user_signature` text,
  `deleted` int(11) DEFAULT '0',
  `delete_reason` text,
  `delete_signature` varchar(255) DEFAULT NULL,
  `wcc_visit_required` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `comment_type` varchar(25) DEFAULT NULL,
  `ncp_changed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `professional_id` (`professional_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `assessment_comments_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `assessment_comments_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_comments_ibfk_3` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_comments_ibfk_4` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_comments`
--

--
-- Table structure for table `assessment_drain`
--

DROP TABLE IF EXISTS `assessment_drain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_drain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_assessment` int(11) NOT NULL DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `last_update` varchar(255) DEFAULT NULL,
  `type_of_drain` int(11) DEFAULT '0',
  `type_of_drain_other` text,
  `sutured` int(11) DEFAULT NULL,
  `tubes_changed_date` varchar(255) DEFAULT NULL,
  `peri_drain_area` text,
  `additional_days_date1` varchar(255) DEFAULT NULL,
  `drainage_amount_mls1` int(10) DEFAULT NULL,
  `drainage_amount_hrs1` int(2) DEFAULT NULL,
  `drainage_amount1_start` int(3) DEFAULT NULL,
  `drainage_amount1_end` int(3) DEFAULT NULL,
  `additional_days_date2` varchar(255) DEFAULT NULL,
  `drainage_amount_mls2` int(10) DEFAULT NULL,
  `drainage_amount_hrs2` int(2) DEFAULT NULL,
  `drainage_amount2_start` int(3) DEFAULT NULL,
  `drainage_amount2_end` int(3) DEFAULT NULL,
  `additional_days_date3` varchar(255) DEFAULT NULL,
  `drainage_amount_mls3` int(10) DEFAULT NULL,
  `drainage_amount_hrs3` int(2) DEFAULT NULL,
  `drainage_amount3_start` int(3) DEFAULT NULL,
  `drainage_amount3_end` int(3) DEFAULT NULL,
  `additional_days_date4` varchar(255) DEFAULT NULL,
  `drainage_amount_mls4` int(10) DEFAULT NULL,
  `drainage_amount_hrs4` int(2) DEFAULT NULL,
  `drainage_amount4_start` int(3) DEFAULT NULL,
  `drainage_amount4_end` int(3) DEFAULT NULL,
  `additional_days_date5` varchar(255) DEFAULT NULL,
  `drainage_amount_mls5` int(10) DEFAULT NULL,
  `drainage_amount_hrs5` int(2) DEFAULT NULL,
  `drainage_amount5_start` int(3) DEFAULT NULL,
  `drainage_amount5_end` int(3) DEFAULT NULL,
  `drainage_num` int(11) DEFAULT NULL,
  `drain_characteristics1` text,
  `drain_odour1` text,
  `drain_characteristics2` text,
  `drain_odour2` text,
  `drain_characteristics3` text,
  `drain_odour3` text,
  `drain_characteristics4` text,
  `drain_odour4` text,
  `drain_characteristics5` text,
  `drain_odour5` text,
  `drain_removed` int(11) DEFAULT NULL,
  `drain_removed_date` varchar(255) DEFAULT NULL,
  `drain_removed_intact` int(11) DEFAULT NULL,
  `discharge_reason` int(11) DEFAULT NULL,
  `drain_site` text,
  `adjunctive` text,
  `adjunctive_other_text` text,
  `cs_show` int(11) DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `pain` int(11) DEFAULT NULL,
  `pain_comments` text,
  `prev_alpha_id` int(11) DEFAULT NULL,
  `moved_reason` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `assessment_drain_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_drain_ibfk_3` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_drain_ibfk_7` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_drain_ibfk_8` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_drain_ibfk_9` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `assessment_eachwound`
--

DROP TABLE IF EXISTS `assessment_eachwound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_eachwound` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `full_assessment` int(11) DEFAULT '-1',
  `pain` int(11) DEFAULT NULL,
  `pain_comments` text,
  `length_cm` int(11) DEFAULT NULL,
  `length_mm` int(11) DEFAULT NULL,
  `width_cm` int(11) DEFAULT NULL,
  `width_mm` int(11) DEFAULT NULL,
  `depth_cm` int(11) DEFAULT NULL,
  `depth_mm` int(11) DEFAULT NULL,
  `wound_edge` text,
  `periwound_skin` text,
  `exudate_amount` int(11) DEFAULT NULL,
  `wound_depth` text,
  `exudate_colour` text,
  `exudate_odour` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `exudate_type` text,
  `discharge_reason` varchar(50) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `discharge_other` text,
  `recurrent` int(11) DEFAULT '0',
  `wound_date` text,
  `adjunctive` text,
  `adjunctive_other_text` text,
  `closed_date` text,
  `last_update` text,
  `num_fistulas` int(11) DEFAULT '0',
  `fistulas` text,
  `cs_show` int(11) DEFAULT '0',
  `prev_alpha_id` int(11) DEFAULT NULL,
  `moved_reason` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `assessment_eachwound_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_eachwound_ibfk_4` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_eachwound_ibfk_5` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_eachwound_ibfk_6` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_eachwound_ibfk_7` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `assessment_images`
--

DROP TABLE IF EXISTS `assessment_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT '0',
  `images` text,
  `which_image` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  CONSTRAINT `assessment_images_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_images_ibfk_2` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_images_ibfk_3` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_images`
--
-- Table structure for table `assessment_incision`
--

DROP TABLE IF EXISTS `assessment_incision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_incision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `full_assessment` int(11) NOT NULL DEFAULT '0',
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `date_surgery` varchar(25) DEFAULT NULL,
  `clinical_path` int(11) DEFAULT NULL,
  `clinical_path_date` varchar(255) DEFAULT '',
  `discharge_reason` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `discharge_other` varchar(255) DEFAULT NULL,
  `last_update` varchar(255) DEFAULT NULL,
  `closed_date` varchar(255) DEFAULT NULL,
  `pain` int(11) DEFAULT NULL,
  `pain_comments` text,
  `incision_status` int(11) DEFAULT NULL,
  `incision_exudate` text,
  `peri_incisional` text,
  `surgery_date` varchar(255) DEFAULT NULL,
  `postop_day` int(11) DEFAULT NULL,
  `incision_closure_methods` text,
  `exudate_amount` int(11) DEFAULT NULL,
  `adjunctive` text,
  `adjunctive_other_text` text,
  `cs_show` int(11) DEFAULT '0',
  `postop_management` text,
  `postop_days` int(5) DEFAULT '0',
  `prev_alpha_id` int(11) DEFAULT NULL,
  `moved_reason` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `assessment_incision_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_incision_ibfk_3` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_incision_ibfk_7` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_incision_ibfk_8` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_incision_ibfk_9` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `assessment_mucocutaneous`
--

DROP TABLE IF EXISTS `assessment_mucocutaneous`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_mucocutaneous` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alpha_id` int(11) DEFAULT NULL,
  `assessment_ostomy_id` int(11) DEFAULT NULL,
  `depth_cm` int(11) DEFAULT NULL,
  `depth_mm` int(11) DEFAULT NULL,
  `location_start` int(11) DEFAULT NULL,
  `location_end` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_ostomy_id` (`assessment_ostomy_id`),
  CONSTRAINT `assessment_mucocutaneous_ibfk_1` FOREIGN KEY (`assessment_ostomy_id`) REFERENCES `assessment_ostomy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_mucocutaneous`
--

LOCK TABLES `assessment_mucocutaneous` WRITE;
/*!40000 ALTER TABLE `assessment_mucocutaneous` DISABLE KEYS */;
/*!40000 ALTER TABLE `assessment_mucocutaneous` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessment_ostomy`
--

DROP TABLE IF EXISTS `assessment_ostomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_ostomy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) DEFAULT '0',
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `full_assessment` int(11) DEFAULT '-1',
  `date_surgery` varchar(25) DEFAULT NULL,
  `clinical_path` int(11) DEFAULT NULL,
  `clinical_path_date` varchar(255) DEFAULT '',
  `flange_pouch` int(11) DEFAULT '0',
  `flange_pouch_comments` text,
  `shape` int(11) DEFAULT NULL,
  `pouching` text,
  `appearance` text,
  `contour` text,
  `urine_type` text,
  `urine_colour` text,
  `urine_quantity` int(11) DEFAULT NULL,
  `stool_colour` text,
  `stool_consistency` text,
  `stool_quantity` int(11) DEFAULT NULL,
  `nutritional_status_other` text,
  `self_care_progress` text,
  `self_care_progress_comments` text,
  `profile` text,
  `num_mucocutaneous_margin` int(11) DEFAULT NULL,
  `mucocutaneous_margin` text,
  `mucocutaneous_margin_comments` text,
  `perifistula_skin` text,
  `mucous_fistula_present` int(11) DEFAULT '0',
  `devices` int(11) DEFAULT NULL,
  `periostomy_skin` text,
  `priority` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `discharge_reason` int(11) DEFAULT '0',
  `discharge_other` text,
  `last_update` varchar(255) DEFAULT NULL,
  `closed_date` text,
  `adjunctive` text,
  `adjunctive_other_text` text,
  `cs_show` int(11) DEFAULT '0',
  `construction` int(11) DEFAULT '0',
  `drainage` text,
  `drainage_comments` text,
  `nutritional_status` int(11) DEFAULT '0',
  `circumference_mm` int(11) DEFAULT '0',
  `length_mm` int(11) DEFAULT '0',
  `width_mm` int(11) DEFAULT '0',
  `postop_days` int(5) DEFAULT '0',
  `prev_alpha_id` int(11) DEFAULT NULL,
  `moved_reason` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `assessment_ostomy_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_ostomy_ibfk_3` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`),
  CONSTRAINT `assessment_ostomy_ibfk_7` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`),
  CONSTRAINT `assessment_ostomy_ibfk_8` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`),
  CONSTRAINT `assessment_ostomy_ibfk_9` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_ostomy`
--

LOCK TABLES `assessment_ostomy` WRITE;
/*!40000 ALTER TABLE `assessment_ostomy` DISABLE KEYS */;
/*!40000 ALTER TABLE `assessment_ostomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessment_product`
--

DROP TABLE IF EXISTS `assessment_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `wound_assessment_id` int(11) DEFAULT NULL,
  `ostomy_assessment_id` int(11) DEFAULT NULL,
  `incision_assessment_id` int(11) DEFAULT NULL,
  `drain_assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `burn_assessment_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `wound_assessment_id` (`wound_assessment_id`),
  KEY `ostomy_assessment_id` (`ostomy_assessment_id`),
  KEY `incision_assessment_id` (`incision_assessment_id`),
  KEY `drain_assessment_id` (`drain_assessment_id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `alpha_id` (`alpha_id`),
  CONSTRAINT `assessment_product_ibfk_4` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_product_ibfk_5` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`),
  CONSTRAINT `assessment_product_ibfk_6` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_product_ibfk_7` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_product`
--
--
-- Table structure for table `assessment_sinustracts`
--

DROP TABLE IF EXISTS `assessment_sinustracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_sinustracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alpha_id` int(11) DEFAULT NULL,
  `assessment_eachwound_id` int(11) DEFAULT NULL,
  `depth_cm` int(11) DEFAULT NULL,
  `depth_mm` int(11) DEFAULT NULL,
  `location_start` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_eachwound_id` (`assessment_eachwound_id`),
  CONSTRAINT `assessment_sinustracts_ibfk_1` FOREIGN KEY (`assessment_eachwound_id`) REFERENCES `assessment_eachwound` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_sinustracts`
--

--
-- Table structure for table `assessment_treatment`
--

DROP TABLE IF EXISTS `assessment_treatment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_treatment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wound_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `review_done` int(11) DEFAULT NULL,
  `vac_show` int(11) DEFAULT NULL,
  `vac_start_date` datetime(6) DEFAULT NULL,
  `vac_end_date` datetime(6) DEFAULT NULL,
  `pressure_reading` int(11) DEFAULT NULL,
  `therapy_setting` int(11) DEFAULT NULL,
  `adjunctive_other` varchar(255) DEFAULT NULL,
  `treatmentmodalities_show` int(11) DEFAULT NULL,
  `cs_date_show` int(11) DEFAULT NULL,
  `cs_date` datetime(6) DEFAULT NULL,
  `cs_show` int(11) DEFAULT NULL,
  `bandages_in` varchar(25) DEFAULT NULL,
  `bandages_out` varchar(25) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_id` (`assessment_id`),
  CONSTRAINT `assessment_treatment_ibfk_1` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_treatment`
--
--
-- Table structure for table `assessment_treatment_arrays`
--

DROP TABLE IF EXISTS `assessment_treatment_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_treatment_arrays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assessment_treatment_id` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `lookup_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_treatment_id` (`assessment_treatment_id`),
  CONSTRAINT `assessment_treatment_arrays_ibfk_1` FOREIGN KEY (`assessment_treatment_id`) REFERENCES `assessment_treatment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_treatment_arrays`
--

LOCK TABLES `assessment_treatment_arrays` WRITE;
/*!40000 ALTER TABLE `assessment_treatment_arrays` DISABLE KEYS */;
/*!40000 ALTER TABLE `assessment_treatment_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessment_undermining`
--

DROP TABLE IF EXISTS `assessment_undermining`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_undermining` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alpha_id` int(11) DEFAULT NULL,
  `assessment_eachwound_id` int(11) DEFAULT NULL,
  `depth_cm` int(11) DEFAULT NULL,
  `depth_mm` int(11) DEFAULT NULL,
  `location_start` int(11) DEFAULT NULL,
  `location_end` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_eachwound_id` (`assessment_eachwound_id`),
  CONSTRAINT `assessment_undermining_ibfk_1` FOREIGN KEY (`assessment_eachwound_id`) REFERENCES `assessment_eachwound` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_undermining`
--
--
-- Table structure for table `assessment_woundbed`
--

DROP TABLE IF EXISTS `assessment_woundbed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment_woundbed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alpha_id` int(11) DEFAULT NULL,
  `assessment_eachwound_id` int(11) DEFAULT NULL,
  `wound_bed` varchar(255) DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_eachwound_id` (`assessment_eachwound_id`),
  CONSTRAINT `assessment_woundbed_ibfk_1` FOREIGN KEY (`assessment_eachwound_id`) REFERENCES `assessment_eachwound` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessment_woundbed`
--
--
-- Table structure for table `braden`
--

DROP TABLE IF EXISTS `braden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `braden` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT '0',
  `active` int(11) DEFAULT NULL,
  `current_flag` int(11) DEFAULT NULL,
  `braden_sensory` int(1) DEFAULT '0',
  `braden_moisture` int(1) DEFAULT '0',
  `braden_activity` int(1) DEFAULT '0',
  `braden_mobility` int(1) DEFAULT '0',
  `braden_nutrition` int(1) DEFAULT '0',
  `braden_friction` int(1) DEFAULT '0',
  `professional_id` int(11) DEFAULT NULL,
  `user_signature` varchar(255) DEFAULT '',
  `last_update` varchar(100) DEFAULT '',
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `data_entry_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `professional_id` (`professional_id`),
  CONSTRAINT `braden_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `components`
--

DROP TABLE IF EXISTS `components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components` (
  `id` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `field_name` varchar(50) DEFAULT NULL,
  `field_type` varchar(11) DEFAULT NULL,
  `component_type` varchar(8) DEFAULT '',
  `flowchart` int(11) DEFAULT NULL,
  `label_key` varchar(255) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL,
  `column_layout` int(11) DEFAULT NULL,
  `orderby` int(11) DEFAULT NULL,
  `label_style` varchar(255) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `enabled_close` int(11) DEFAULT NULL,
  `details_popup` int(11) DEFAULT NULL,
  `info_popup_id` int(11) DEFAULT NULL,
  `allow_edit` int(11) DEFAULT NULL,
  `validation` text,
  `ignore_info_popup` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components`
--

LOCK TABLES `components` WRITE;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
INSERT INTO `components` VALUES (1,0,'assessment_type','INTEGER','AT',3,'pixalere.woundassessment.form.assessment_type',NULL,1,1,'normal',1,0,0,0,0,NULL,1),(2,31,'status','INTEGER','DRP',3,'pixalere.woundassessment.form.wound_status',NULL,1,2,'normal',1,0,0,35,1,NULL,0),(3,32,'discharge_reason','INTEGER','DRP',3,'pixalere.woundassessment.form.discharge_reason',NULL,5,3,'normal',1,1,0,0,1,NULL,0),(4,0,'closed_date','STRING','DATE',3,'pixalere.woundassessment.form.closure_date',NULL,1,4,'normal',1,1,0,0,1,NULL,0),(5,0,'wound_date','STRING','DATE',3,'pixalere.woundassessment.form.date_of_onset',NULL,1,3,'normal',1,0,0,34,1,NULL,0),(6,0,'recurrent','INTEGER','YN',3,'pixalere.woundassessment.form.recurrent',NULL,1,6,'normal',1,0,0,36,1,NULL,0),(7,97,'pain','INTEGER','DRP',3,'pixalere.woundassessment.form.pain',NULL,1,7,'normal',1,0,0,27,1,NULL,0),(380,0,'pain_comments','STRING','TXTA',3,'pixalere.pain_comments',NULL,1,7,'normal',1,0,0,37,1,NULL,0),(8,0,'length','INTEGER','MEAS',3,'pixalere.woundassessment.form.length',NULL,1,8,'normal',1,0,0,28,1,NULL,0),(9,0,'width','INTEGER','MEAS',3,'pixalere.woundassessment.form.width',NULL,1,9,'normal',1,0,0,29,1,NULL,0),(10,0,'depth','INTEGER','MEAS',3,'pixalere.woundassessment.form.depth',NULL,1,10,'normal',1,0,0,30,1,NULL,0),(11,0,'undermining_location','STRING','MMAS',3,'pixalere.woundassessment.form.undermining',20,1,11,'normal',1,0,0,31,1,NULL,0),(12,0,'undermining_depth','STRING','MMAS',3,'pixalere.woundassessment.form.undermining',20,1,12,'normal',1,0,0,90,0,NULL,1),(13,0,'sinus_depth','STRING','MMAS',3,'pixalere.woundassessment.form.sinus_tract',19,1,14,'normal',1,0,0,32,0,NULL,1),(381,0,'fistulas','STRING','MTXT',3,'pixalere.woundassessment.form.fistulas',NULL,1,14,'normal',1,0,0,33,1,NULL,0),(14,35,'wound_base','STRING','MPER',3,'pixalere.woundassessment.form.wound_base',21,1,14,'normal',1,0,0,24,1,'var c =0;var perc=0;for(i=0;i<document.form.woundbase.length;i++){if(document.form.woundbase[i].checked==true){if(document.getElementById(\"woundbase_percentage_\"+(i+1)).selectedIndex == -1 || document.getElementById(\"woundbase_percentage_\"+(i+1)).selectedIndex == 0){error=1;}var percentage = document.getElementById(\"woundbase_percentage_\"+(i+1)).options[document.getElementById(\"woundbase_percentage_\"+(i+1)).selectedIndex].value; if(c == 0){perc=\"\"+percentage;}else{perc=perc+\",\"+percentage; }c++;}}document.form.woundbase_percentage.value=perc;if(calcTotalWoundBed() != 100 && calcTotalWoundBed() != 0){themessage=themessage + \"pixalere.woundassessment.error.percentage,\";error=1;}',0),(15,24,'exudate_type','STRING','CHKB',3,'pixalere.woundassessment.form.exudate_type',NULL,1,15,'normal',1,0,0,38,1,NULL,0),(16,25,'exudate_amount','INTEGER','RADO',3,'pixalere.woundassessment.form.exudate_amount',NULL,1,16,'normal',1,0,0,39,1,NULL,0),(17,26,'exudate_odour','INTEGER','YN',3,'pixalere.woundassessment.form.odour',NULL,1,17,'normal',1,0,0,40,1,NULL,0),(18,34,'wound_edge','STRING','CHKB',3,'pixalere.woundassessment.form.wound_edge',NULL,1,18,'normal',1,0,0,41,1,NULL,0),(19,37,'periwound_skin','STRING','CHKB',3,'pixalere.woundassessment.form.periwound_skin',NULL,1,19,'normal',1,0,0,42,1,NULL,0),(23,0,'products','STRING','PROD',3,'pixalere.woundassessment.form.products',NULL,1,23,'normal',1,0,0,0,1,NULL,0),(24,0,'body','STRING','TXTA',3,'pixalere.woundassessment.form.treatment_comments',16,1,24,'normal',1,0,0,0,1,NULL,0),(27,0,'vac_start_date','DATE','DATE',3,'pixalere.woundassessment.form.vacstart',36,1,27,'normal',1,0,0,0,1,NULL,0),(28,0,'vac_end_date','DATE','DATE',3,'pixalere.woundassessment.form.vacend',36,1,28,'normal',1,0,0,0,1,NULL,0),(29,40,'pressure_reading','INTEGER','DRP',3,'pixalere.woundassessment.form.pressure_reading',36,1,29,'normal',1,0,0,0,1,NULL,0),(30,17,'adjunctive','STRING','CHKB',3,'pixalere.woundassessment.form.adjunctive',27,1,37,'normal',1,0,0,0,1,NULL,0),(423,38,'csresult','STRING','CHKB',3,'pixalere.woundassessment.form.c_s_result',27,1,34,'normal',1,0,0,0,1,NULL,0),(473,0,'antibiotics','STRING','MDDA',3,'pixalere.treatment.form.antibiotics',38,1,35,'normal',1,0,0,0,1,NULL,0),(31,0,'images','STRING','IMG',3,'pixalere.woundassessment.form.images',NULL,1,38,'normal',1,0,0,0,0,NULL,0),(450,0,'assessment_type','INTEGER','AT',4,'pixalere.woundassessment.form.assessment_type',NULL,1,1,'normal',1,0,0,0,0,NULL,1),(32,155,'status','INTEGER','DRP',4,'pixalere.ostomyassessment.form.wound_status',NULL,1,1,'normal',1,0,0,51,1,NULL,0),(33,149,'discharge_reason','INTEGER','DRP',4,'pixalere.ostomyassessment.form.discharge_reason',NULL,1,2,'normal',1,1,0,52,1,NULL,0),(34,0,'closed_date','STRING','DATE',4,'pixalere.ostomyassessment.form.closure_date',NULL,1,3,'normal',1,1,0,0,1,NULL,0),(35,0,'postop_days','INTEGER','NUM',4,'pixalere.ostomyassessment.form.postop_day',NULL,1,4,'normal',1,0,0,0,0,NULL,0),(386,0,'clinical_path','INTEGER','OON',4,'pixalere.ostomyassessment.form.clinical_path',NULL,1,5,'normal',0,0,0,92,1,NULL,0),(387,0,'clinical_path_date','STRING','DATE',4,'pixalere.ostomyassessment.form.clinical_path_date',NULL,1,6,'normal',0,0,0,0,1,NULL,0),(410,135,'construction','INTEGER','RADO',4,'pixalere.ostomyassessment.form.construction',NULL,1,7,'normal',1,0,0,53,1,NULL,0),(448,0,'circumference_mm','INTEGER','OMEA',4,'pixalere.ostomyassessment.form.circumference_mm',NULL,1,9,'normal',1,0,0,54,1,NULL,0),(454,0,'length_mm','INTEGER','OMEA',4,'pixalere.ostomyassessment.form.length',NULL,1,9,'normal',1,0,0,55,1,NULL,0),(455,0,'width_mm','INTEGER','OMEA',4,'pixalere.ostomyassessment.form.width',NULL,1,9,'normal',1,0,0,56,1,NULL,0),(56,78,'shape','INTEGER','RADO',4,'pixalere.ostomyassessment.form.stomashape',NULL,1,8,'normal',1,0,0,57,1,NULL,0),(57,62,'profile','STRING','CHKB',4,'pixalere.ostomyassessment.form.profile',NULL,1,10,'normal',1,0,0,58,1,NULL,0),(59,74,'appearance','STRING','CHKB',4,'pixalere.ostomyassessment.form.appearance',NULL,1,11,'normal',1,0,0,59,1,NULL,0),(61,89,'devices','INTEGER','RADO',4,'pixalere.ostomyassessment.form.devices',NULL,1,12,'normal',1,0,0,0,1,NULL,0),(60,75,'contour','STRING','CHKB',4,'pixalere.ostomyassessment.form.skincontour',NULL,1,13,'normal',1,0,0,61,1,NULL,0),(58,67,'pouching','STRING','CHKB',4,'pixalere.ostomyassessment.form.pouching',NULL,1,14,'normal',1,0,0,0,1,NULL,0),(414,138,'mucocutaneous_margin','STRING','CHKB',4,'pixalere.ostomyassessment.form.mucocutaneousmargin',NULL,1,15,'normal',1,0,0,62,1,NULL,0),(456,0,'mucocutaneous_margin_location','STRING','MMAS',4,'pixalere.ostomyassessment.form.mucocutaneous_margin_area',NULL,1,16,'normal',1,0,0,50,1,NULL,0),(457,0,'mucocutaneous_margin_depth','STRING','MMAS',4,'pixalere.ostomyassessment.form.mucocutaneous_margin_area',NULL,1,16,'normal',1,0,0,60,1,NULL,0),(415,0,'mucocutaneous_margin_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.mucocutaneousmargin_comments',NULL,1,17,'normal',1,0,0,0,1,NULL,0),(46,69,'periostomy_skin','STRING','CHKB',4,'pixalere.ostomyassessment.form.periostomyskin',NULL,1,18,'normal',1,0,0,63,1,NULL,0),(44,0,'mucous_fistula_present','INTEGER','YN',4,'pixalere.ostomyassessment.form.mucous_fistula_present',NULL,1,19,'normal',1,0,0,64,1,NULL,0),(411,137,'drainage','STRING','CHKB',4,'pixalere.ostomyassessment.form.drainage',NULL,1,20,'normal',1,0,0,65,1,NULL,0),(416,139,'perifistula_skin','STRING','CHKB',4,'pixalere.ostomyassessment.form.perifistulaskin',NULL,1,21,'normal',1,0,0,66,1,NULL,0),(453,0,'drainage_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.drainage_comments',NULL,1,21,'normal',1,0,0,0,1,NULL,0),(47,66,'urine_colour','STRING','CHKB',4,'pixalere.ostomyassessment.form.urinecolour',NULL,1,22,'normal',1,0,0,67,1,NULL,0),(48,70,'urine_type','STRING','CHKB',4,'pixalere.ostomyassessment.form.urinetype',NULL,1,23,'normal',1,0,0,68,1,NULL,0),(49,71,'urine_quantity','INTEGER','RADO',4,'pixalere.ostomyassessment.form.urinequantity',NULL,1,24,'normal',1,0,0,69,1,NULL,0),(460,61,'stool_colour','STRING','CHKB',4,'pixalere.ostomyassessment.form.stoolcolour',NULL,1,26,'normal',1,0,0,0,1,NULL,0),(461,72,'stool_consistency','STRING','CHKB',4,'pixalere.ostomyassessment.form.stoolconsistency',NULL,1,26,'normal',1,0,0,0,1,NULL,0),(51,73,'stool_quantity','INTEGER','RADO',4,'pixalere.ostomyassessment.form.stoolquantity',NULL,1,25,'normal',1,0,0,70,1,NULL,0),(52,0,'nutritional_status_other','STRING','TXTA',4,'pixalere.ostomyassessment.form.nutritionalstatusother',NULL,1,33,'normal',1,0,0,0,0,NULL,1),(45,65,'flange_pouch','INTEGER','RADO',4,'pixalere.ostomyassessment.form.flangepouch',NULL,1,34,'normal',1,0,0,0,1,NULL,0),(412,0,'flange_pouch_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.flangepouch_comments',NULL,1,35,'normal',1,0,0,0,1,NULL,0),(390,68,'self_care_progress','STRING','CHKB',4,'pixalere.ostomyassessment.form.selfcareprogress',NULL,1,37,'normal',1,0,0,0,1,NULL,0),(413,0,'self_care_progress_comments','STRING','TXTA',4,'pixalere.ostomyassessment.form.selfcareprogress_comments',NULL,1,37,'normal',1,0,0,0,1,NULL,0),(446,147,'nutritional_status','INTEGER','RADO',4,'pixalere.ostomyassessment.form.nutritional_status',NULL,1,32,'normal',1,0,0,0,1,NULL,0),(417,38,'csresult','STRING','CHKB',4,'pixalere.woundassessment.form.c_s_result',27,1,46,'normal',1,0,0,0,1,NULL,0),(68,0,'images','STRING','IMG',4,'pixalere.woundassessment.form.images',NULL,1,48,'normal',1,0,0,0,0,NULL,0),(146,0,'products','STRING','PROD',4,'pixalere.woundassessment.form.products',NULL,1,46,'normal',1,0,0,0,1,NULL,0),(147,0,'body','STRING','TXTA',4,'pixalere.woundassessment.form.treatment_comments',16,1,46,'normal',1,0,0,0,1,NULL,0),(156,0,'wound_location_detailed','STRING','TXT',8,'pixalere.woundprofile.form.wound_location',7,1,1,'normal',1,0,0,0,0,NULL,1),(176,0,'wound_location_detailed','STRING','TXT',9,'pixalere.woundprofile.form.wound_location',7,1,1,'normal',1,0,0,0,0,NULL,1),(226,0,'fix','STRING','FIX',2,'pixalere.viewer.fixes',NULL,1,0,'normal',0,0,0,0,0,NULL,1),(451,0,'assessment_type','INTEGER','AT',5,'pixalere.woundassessment.form.assessment_type',NULL,1,1,'normal',1,0,0,0,0,NULL,1),(69,154,'status','INTEGER','DRP',5,'pixalere.incisionassessment.form.wound_status',NULL,1,1,'normal',1,0,0,72,1,NULL,0),(425,0,'postop_days','INTEGER','NUM',5,'pixalere.incisionassessment.form.postop_day',NULL,1,4,'normal',1,0,0,0,0,NULL,0),(426,0,'clinical_path','INTEGER','OON',5,'pixalere.ostomyassessment.form.clinical_path',NULL,1,5,'normal',0,0,0,93,1,NULL,0),(427,0,'clinical_path_date','STRING','DATE',5,'pixalere.ostomyassessment.form.clinical_path_date',NULL,1,6,'normal',0,0,0,0,1,NULL,0),(70,150,'discharge_reason','INTEGER','DRP',5,'pixalere.incisionassessment.form.discharge_reason',NULL,1,2,'normal',1,1,0,0,1,NULL,0),(71,0,'closed_date','STRING','DATE',5,'pixalere.incisionassessment.form.closure_date',NULL,1,3,'normal',1,1,0,0,1,NULL,0),(459,152,'postop_management','STRING','CHKB',5,'pixalere.incisionassessment.form.postop_management',NULL,1,6,'normal',1,0,0,83,1,NULL,0),(72,97,'pain','INTEGER','DRP',5,'pixalere.incisionassessment.form.pain',NULL,1,6,'normal',1,0,0,84,1,NULL,0),(378,0,'pain_comments','STRING','TXTA',5,'pixalere.pain_comments',NULL,1,6,'normal',1,0,0,85,1,NULL,0),(75,92,'incision_status','INTEGER','RADO',5,'pixalere.incisionassessment.form.incisionstatus',NULL,1,7,'normal',1,0,0,87,1,NULL,0),(76,90,'incision_exudate','STRING','CHKB',5,'pixalere.incisionassessment.form.incisionexudate',NULL,1,8,'normal',1,0,0,88,1,NULL,0),(77,91,'peri_incisional','STRING','CHKB',5,'pixalere.incisionassessment.form.ssinfection',NULL,1,10,'normal',1,0,0,89,1,NULL,0),(78,0,'products','STRING','PROD',5,'pixalere.incisionassessment.form.products',NULL,1,30,'normal',1,0,0,0,1,NULL,0),(81,0,'images','STRING','IMG',5,'pixalere.woundassessment.form.images',NULL,1,36,'normal',1,0,0,0,0,NULL,0),(377,0,'body','STRING','TXTA',5,'pixalere.woundassessment.form.treatment_comments',16,1,32,'normal',1,0,0,0,1,NULL,0),(209,99,'incision_closure_methods','STRING','CHKB',5,'pixalere.incisionassessment.form.incision_closure_methods',NULL,1,7,'normal',1,0,0,0,1,NULL,0),(211,101,'exudate_amount','INTEGER','DRP',5,'pixalere.incisionassessment.form.incision_exudate_amount',NULL,1,9,'normal',1,0,0,0,1,NULL,0),(421,38,'csresult','STRING','CHKB',5,'pixalere.woundassessment.form.c_s_result',27,1,33,'normal',1,0,0,0,1,NULL,0),(475,0,'antibiotics','STRING','MDDA',5,'pixalere.treatment.form.antibiotics',38,1,35,'normal',1,0,0,0,1,NULL,0),(452,0,'assessment_type','INTEGER','AT',6,'pixalere.woundassessment.form.assessment_type',NULL,1,1,'normal',1,0,0,0,0,NULL,1),(428,153,'status','INTEGER','DRP',6,'pixalere.woundassessment.form.status',NULL,1,2,'normal',1,0,0,77,1,NULL,0),(196,93,'type_of_drain','INTEGER','DRP',6,'pixalere.drainassessment.form.type_of_drain',NULL,1,5,'normal',1,0,0,0,1,NULL,0),(200,0,'type_of_drain_other','STRING','TXT',6,'pixalere.drainassessment.form.type_of_drain_other',NULL,1,6,'normal',1,0,0,0,1,NULL,1),(429,0,'drain_removed_intact','INTEGER','YN',6,'pixalere.drainassessment.form.drain_removed_intact',NULL,1,4,'normal',1,1,0,0,1,NULL,0),(430,0,'drain_removed_date','STRING','DATE',6,'pixalere.drainassessment.form.drain_removed_date',NULL,1,2,'normal',1,1,0,0,1,NULL,0),(431,144,'discharge_reason','INTEGER','DRP',6,'pixalere.drainassessment.form.drain_removed_reason',NULL,1,3,'normal',1,1,0,0,1,NULL,0),(197,0,'sutured','INTEGER','YNN',6,'pixalere.drainassessment.form.sutured',NULL,1,8,'normal',1,0,0,0,1,NULL,0),(215,102,'drain_site','STRING','CHKB',6,'pixalere.drainassessment.form.drain_site',NULL,1,9,'normal',1,0,0,78,1,NULL,0),(433,0,'additional_days_date','STRING','DATE',6,'pixalere.drainassessment.form.additional_days_date',15,1,11,'normal',1,0,0,79,0,NULL,0),(434,0,'drainage_amount_mls','INTEGER','TXT',6,'pixalere.drainassessment.form.drainage_amount_mls',15,1,12,'normal',1,0,0,0,0,NULL,0),(435,0,'drainage_amount_hrs','INTEGER','TXT',6,'pixalere.drainassessment.form.drainage_amount_hrs',15,1,13,'normal',1,0,0,0,0,NULL,0),(436,0,'drainage_amount','INTEGER','CLK',6,'pixalere.drainassessment.form.drainage_amount_start',15,1,14,'normal',1,0,0,0,0,NULL,0),(199,95,'drain_characteristics','STRING','CHKB',6,'pixalere.drainassessment.form.characteristics',15,1,15,'normal',1,0,0,80,0,NULL,0),(447,0,'tubes_changed_date','STRING','DATE',6,'pixalere.drainassessment.form.tubes_changed_date',NULL,1,16,'normal',1,0,0,0,1,NULL,0),(198,94,'peri_drain_area','STRING','CHKB',6,'pixalere.drainassessment.form.peri_drain_area',NULL,1,17,'normal',1,0,0,81,1,NULL,0),(419,38,'csresult','STRING','CHKB',6,'pixalere.woundassessment.form.c_s_result',27,1,38,'normal',1,0,0,0,1,NULL,0),(478,0,'antibiotics','STRING','MDDA',6,'pixalere.treatment.form.antibiotics',38,1,40,'normal',1,0,0,0,1,NULL,0),(202,0,'products','STRING','PROD',6,'pixalere.woundassessment.form.products',NULL,1,35,'normal',1,0,0,0,1,NULL,0),(208,0,'body','STRING','TXTA',6,'pixalere.woundassessment.form.treatment_comments',16,1,36,'normal',1,0,0,0,1,NULL,0),(204,0,'images','STRING','IMG',6,'pixalere.woundassessment.form.images',NULL,1,41,'normal',1,0,0,0,0,NULL,0),(83,98,'therapy_setting','INTEGER','RADO',3,'pixalere.woundassessment.form.reading_group',36,1,28,'normal',1,0,0,0,1,NULL,0),(87,1,'funding_source_id','INTEGER','DRP',1,'pixalere.patientprofile.form.funding_source',NULL,1,9,'normal',1,0,0,0,1,NULL,0),(88,0,'treatment_location_id','INTEGER','DRP',1,'pixalere.patientprofile.form.treatment_location',NULL,1,8,'normal',1,0,0,0,0,NULL,0),(89,7,'gender','INTEGER','DRP',1,'pixalere.patientprofile.form.gender',NULL,1,3,'normal',1,0,0,0,1,NULL,0),(90,0,'dob','STRING','DOB',1,'pixalere.patientprofile.form.date_of_birth',NULL,1,4,'normal',1,0,0,0,1,'dob_year : {canadianDate: {data: [\'#dob_day\',\'#dob_month\',\'#dob_year\']}}},messages: {dob_year: \'Valid Date only\'',0),(91,0,'age','INTEGER','AGE',1,'pixalere.patientprofile.form.age',NULL,1,5,'normal',1,0,0,0,0,NULL,1),(92,0,'allergies','STRING','TXTA',1,'pixalere.patientprofile.form.allergies',NULL,1,6,'normal',1,0,0,99,1,NULL,0),(93,0,'professionals','STRING','TXTA',1,'pixalere.patientprofile.form.health_care_professional_list',NULL,1,10,'normal',1,0,0,0,1,NULL,0),(94,6,'interfering_meds','STRING','CHKB',1,'pixalere.patientprofile.form.factors_that_interfere',24,1,11,'normal',1,0,0,0,1,NULL,0),(95,0,'interfering_meds_other','STRING','TXT',1,'pixalere.patientprofile.form.factors_that_interfere_other',NULL,1,12,'normal',1,0,0,0,0,NULL,1),(96,165,'medications_comments','STRING','CHKB',1,'pixalere.patientprofile.form.current_medications',24,1,13,'normal',1,0,0,0,1,NULL,0),(97,0,'co_morbidities','STRING','MLST',1,'pixalere.patientprofile.form.co_morbidities',24,1,10,'normal',1,0,0,0,1,NULL,0),(217,0,'surgical_history','STRING','TXTA',1,'pixalere.patientprofile.form.surgical_history',NULL,1,10,'normal',1,0,0,0,1,NULL,0),(218,104,'braden_sensory','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_sensory',13,1,14,'normal',1,0,0,0,1,NULL,0),(219,105,'braden_moisture','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_moisture',13,1,14,'normal',1,0,0,0,1,NULL,0),(220,106,'braden_activity','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_activity',13,1,14,'normal',1,0,0,0,1,NULL,0),(221,107,'braden_mobility','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_mobility',13,1,14,'normal',1,0,0,0,1,NULL,0),(222,108,'braden_nutrition','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_nutrition',13,1,14,'normal',1,0,0,0,1,NULL,0),(223,109,'braden_friction','INTEGER','BRS',13,'pixalere.patientprofile.form.braden_friction',13,1,14,'normal',1,0,0,0,1,NULL,0),(224,110,'braden_score','INTEGER','BRT',13,'pixalere.patientprofile.form.braden_score',NULL,1,14,'normal',1,0,0,48,0,NULL,0),(342,16,'investigation','STRING','DDA',1,'pixalere.patientprofile.form.other_investigations',35,1,15,'normal',1,0,0,0,1,NULL,0),(343,13,'albumin','INTEGER','TXT',1,'pixalere.patientprofile.form.albumin',NULL,1,20,'normal',1,0,0,0,1,'albumin: {number: true,maxlength: 5}},messages: {albumin: \'Numbers and Decimal only please\'',0),(344,14,'pre_albumin','INTEGER','TXT',1,'pixalere.patientprofile.form.pre_albumin',NULL,1,22,'normal',1,0,0,0,1,'pre_albumin: {number: true,maxlength: 5}},messages: {pre_albumin: \'Numbers and Decimal only please\'',0),(345,0,'blood_sugar','DOUBLE','TXT',40,'pixalere.patientprofile.form.blood_sugar',NULL,1,18,'normal',1,0,0,0,1,'blood_sugar: {number: true}},messages: {blood_sugar: \'Numbers and Decimal only please\'',0),(346,0,'blood_sugar_date','DATE','DATE',40,'pixalere.patientprofile.form.date_taken',NULL,1,19,'normal',1,0,0,0,1,'blood_sugar_date_year : {canadianDate: {data: [\'#blood_sugar_date_day\',\'#blood_sugar_date_month\',\'#blood_sugar_date_year\']}}},messages: {blood_sugar_date_year: \'Valid Date only\'',0),(347,54,'blood_sugar_meal','INTEGER','TXT',40,'pixalere.patientprofile.form.blood_sugar_meal',NULL,1,20,'normal',1,0,0,0,1,'blood_sugar_meal: {digits: true}},messages: {blood_sugar_meal: \'Numbers only please\'',0),(348,0,'albumin_date','STRING','DATE',1,'pixalere.patientprofile.form.albumin_date',NULL,1,20,'normal',1,0,0,0,1,'albumin_date_year : {canadianDate: {data: [\'#albumin_date_day\',\'#albumin_date_month\',\'#albumin_date_year\']}}},messages: {albumin_date_year: \'Valid Date only\'',0),(349,0,'prealbumin_date','STRING','DATE',1,'pixalere.patientprofile.form.prealbumin_date',NULL,1,22,'normal',1,0,0,0,1,'prealbumin_date_year : {canadianDate: {data: [\'#prealbumin_date_day\',\'#prealbumin_date_month\',\'#prealbumin_date_year\']}}},messages: {prealbumin_date_year: \'Valid Date only\'',0),(98,0,'wound_location_detailed','STRING','TXT',2,'pixalere.woundprofile.form.wound_location',7,1,1,'normal',1,0,0,95,0,NULL,1),(100,8,'etiology','STRING','SALM',2,'pixalere.woundprofile.form.etiology',33,1,3,'normal',1,0,0,0,1,NULL,0),(102,0,'reason_for_care','STRING','TXTA',2,'pixalere.woundprofile.form.reason_for_care',NULL,1,2,'normal',1,0,0,0,1,NULL,0),(103,86,'goals','STRING','SALP',2,'pixalere.woundprofile.form.goals',34,1,6,'normal',1,0,0,0,1,NULL,0),(440,0,'operative_procedure_comments','STRING','TXTA',2,'pixalere.woundprofile.form.operative_procedure_comments',NULL,1,7,'normal',1,0,0,0,1,NULL,0),(441,0,'date_surgery','STRING','DATE',2,'pixalere.woundprofile.form.date_surgery',NULL,1,8,'normal',1,0,0,0,1,NULL,0),(442,0,'surgeon','STRING','TXT',2,'pixalere.woundprofile.form.surgeon',NULL,1,9,'normal',1,0,0,0,1,NULL,0),(443,0,'marking_prior_surgery','INTEGER','YN',2,'pixalere.woundprofile.form.marking_prior_surgery',NULL,1,10,'normal',1,0,0,0,1,NULL,0),(462,148,'type_of_ostomy','STRING','CHKB',2,'pixalere.woundprofile.form.type_of_ostomy',NULL,1,9,'normal',1,0,0,94,1,NULL,0),(444,89,'patient_limitations','STRING','CHKB',2,'pixalere.woundprofile.form.patient_limitations',NULL,1,11,'normal',1,0,0,0,1,NULL,0),(445,146,'teaching_goals','INTEGER','DRP',2,'pixalere.woundprofile.form.teaching_goals',NULL,1,12,'normal',1,0,0,0,1,NULL,0),(141,0,'sinus_location','STRING','MMAS',3,'pixalere.woundassessment.form.sinus_tract',19,1,13,'normal',1,0,0,91,1,NULL,0),(227,110,'left_missing_limbs','STRING','CHKB',11,'pixalere.patientprofile.form.left_missing_limb',NULL,1,20,'normal',0,0,0,45,1,NULL,0),(228,110,'right_missing_limbs','STRING','CHKB',11,'pixalere.patientprofile.form.right_missing_limb',NULL,1,21,'normal',0,0,0,0,1,NULL,1),(229,111,'left_pain_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.left_pain_assessment',NULL,1,22,'normal',0,0,0,46,1,NULL,0),(230,111,'right_pain_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.right_pain_assessment',NULL,1,23,'normal',0,0,0,0,1,NULL,1),(231,0,'pain_comments','STRING','TXTA',11,'pixalere.patientprofile.form.pain_comments',NULL,1,24,'normal',0,0,0,0,1,NULL,1),(233,112,'left_skin_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.left_skin_assessment',NULL,1,26,'normal',0,0,0,47,1,NULL,0),(234,112,'right_skin_assessment','STRING','CHKB',11,'pixalere.patientprofile.form.right_skin_assessment',NULL,1,27,'normal',0,0,0,0,1,NULL,1),(235,19,'left_temperature_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.left_temperature_leg',NULL,1,28,'normal',0,0,0,0,1,NULL,0),(236,19,'right_temperature_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.right_temperature_leg',NULL,1,29,'normal',0,0,0,0,1,NULL,1),(237,19,'left_temperature_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.left_temperature_foot',NULL,1,30,'normal',0,0,0,0,1,NULL,0),(238,19,'right_temperature_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.right_temperature_foot',NULL,1,31,'normal',0,0,0,0,1,NULL,1),(239,19,'left_temperature_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.left_temperature_toes',NULL,1,32,'normal',0,0,0,0,1,NULL,0),(240,19,'right_temperature_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.right_temperature_toes',NULL,1,33,'normal',0,0,0,0,1,NULL,1),(241,18,'left_skin_colour_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.left_skin_colour_leg',NULL,1,34,'normal',0,0,0,0,1,NULL,0),(242,18,'right_skin_colour_leg','INTEGER','DRP',11,'pixalere.patientprofile.form.right_skin_colour_leg',NULL,1,35,'normal',0,0,0,0,1,NULL,1),(243,18,'left_skin_colour_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.left_skin_colour_foot',NULL,1,36,'normal',0,0,0,0,1,NULL,0),(244,18,'right_skin_colour_foot','INTEGER','DRP',11,'pixalere.patientprofile.form.right_skin_colour_foot',NULL,1,37,'normal',0,0,0,0,1,NULL,1),(245,18,'left_skin_colour_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.left_skin_colour_toes',NULL,1,38,'normal',0,0,0,0,1,NULL,0),(246,18,'right_skin_colour_toes','INTEGER','DRP',11,'pixalere.patientprofile.form.right_skin_colour_toes',NULL,1,39,'normal',0,0,0,0,1,NULL,1),(247,23,'left_dorsalis_pedis_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.left_dorsalis_pedis_palpation',NULL,1,40,'normal',0,0,0,0,1,NULL,0),(248,23,'right_dorsalis_pedis_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.right_dorsalis_pedis_palpation',NULL,1,41,'normal',0,0,0,0,1,NULL,1),(249,23,'left_posterior_tibial_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.left_posterior_tibial_palpation',NULL,1,42,'normal',0,0,0,0,1,NULL,0),(250,23,'right_posterior_tibial_palpation','INTEGER','DRP',11,'pixalere.patientprofile.form.right_posterior_tibial_palpation',NULL,1,43,'normal',0,0,0,0,1,NULL,1),(251,127,'doppler_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.doppler',NULL,1,92,'normal',0,0,0,0,1,NULL,0),(253,23,'left_dorsalis_pedis_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.left_dorsalis_pedis_doppler',NULL,1,93,'normal',0,0,0,0,1,NULL,0),(254,23,'right_dorsalis_pedis_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.right_dorsalis_pedis_doppler',NULL,1,94,'normal',0,0,0,0,1,NULL,1),(407,23,'left_posterior_tibial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.left_posterior_tibial_doppler',NULL,1,95,'normal',0,0,0,0,1,NULL,0),(408,23,'right_posterior_tibial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.right_posterior_tibial_doppler',NULL,1,96,'normal',0,0,0,0,1,NULL,1),(255,23,'left_interdigitial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.left_interdigitial_doppler',NULL,1,97,'normal',0,0,0,0,1,NULL,0),(256,23,'right_interdigitial_doppler','STRING','CHKB',11,'pixalere.patientprofile.form.right_interdigitial_doppler',NULL,1,98,'normal',0,0,0,0,1,NULL,1),(257,127,'ankle_brachial_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.ankle_brachial_by',NULL,1,99,'normal',0,0,0,0,1,NULL,0),(259,0,'ankle_brachial_date','STRING','DATE',11,'pixalere.patientprofile.form.ankle_brachial_date',NULL,1,100,'normal',0,0,0,0,1,NULL,0),(261,0,'left_tibial_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.left_tibial_pedis',NULL,1,101,'normal',0,0,0,0,1,'left_tibial_pedis_ankle_brachial: {digits: true}},messages: {left_tibial_pedis_ankle_brachial: \'Numbers only please\'',0),(262,0,'right_tibial_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.right_tibial_pedis',NULL,1,102,'normal',0,0,0,0,1,'right_tibial_pedis_ankle_brachial: {digits: true}},messages: {right_tibial_pedis_ankle_brachial: \'Numbers only please\'',1),(263,0,'left_dorsalis_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.left_dorsalis_pedis_ankle_brachial',NULL,1,103,'normal',0,0,0,0,1,'left_dorsalis_pedis_ankle_brachial: {digits: true}},messages: {left_dorsalis_pedis_ankle_brachial: \'Numbers only please\'',0),(264,0,'right_dorsalis_pedis_ankle_brachial','INTEGER','TXT',11,'pixalere.patientprofile.form.right_dorsalis_pedis_ankle_brachial',NULL,1,104,'normal',0,0,0,0,1,'right_dorsalis_pedis_ankle_brachial: {digits: true}},messages: {right_dorsalis_pedis_ankle_brachial: \'Numbers only please\'',1),(265,0,'left_ankle_brachial','STRING','TXT',11,'pixalere.patientprofile.form.left_ankle_brachial',NULL,1,105,'normal',0,0,0,0,1,'left_ankle_brachial: {digits: true}},messages: {left_ankle_brachial: \'Numbers only please\'',0),(266,0,'right_ankle_brachial','STRING','TXT',11,'pixalere.patientprofile.form.right_ankle_brachial',NULL,1,106,'normal',0,0,0,0,1,'right_ankle_brachial: {digits: true}},messages: {right_ankle_brachial: \'Numbers only please\'',1),(267,0,'left_abi_score_ankle_brachial','STRING','BLSC',11,'pixalere.patientprofile.form.left_abi_score_ankle_brachial',NULL,1,107,'normal',0,0,0,0,0,NULL,0),(268,0,'right_abi_score_ankle_brachial','STRING','BRSC',11,'pixalere.patientprofile.form.right_abi_score_ankle_brachial',NULL,1,108,'normal',0,0,0,0,0,NULL,1),(269,127,'toe_brachial_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.toe_brachial_by',NULL,1,109,'normal',0,0,0,0,1,NULL,0),(271,0,'toe_brachial_date','STRING','DATE',11,'pixalere.patientprofile.form.toe_brachial_date',NULL,1,110,'normal',0,0,0,0,1,NULL,0),(273,0,'left_toe_pressure','STRING','TXT',11,'pixalere.patientprofile.form.left_toe_pressure',NULL,1,111,'normal',0,0,0,0,1,'left_toe_pressure: {digits: true}},messages: {left_toe_pressure: \'Numbers only please\'',0),(274,0,'right_toe_pressure','STRING','TXT',11,'pixalere.patientprofile.form.right_toe_pressure',NULL,1,112,'normal',0,0,0,0,1,'right_toe_pressure: {digits: true}},messages: {right_toe_pressure: \'Numbers only please\'',1),(275,0,'left_brachial_pressure','STRING','TXT',11,'pixalere.patientprofile.form.left_brachial_pressure',NULL,1,113,'normal',0,0,0,0,1,'left_brachial_pressure: {digits: true}},messages: {left_brachial_pressure: \'Numbers only please\'',0),(276,0,'right_brachial_pressure','STRING','TXT',11,'pixalere.patientprofile.form.right_brachial_pressure',NULL,1,114,'normal',0,0,0,0,1,'right_brachial_pressure: {digits: true}},messages: {right_brachial_pressure: \'Numbers only please\'',1),(277,0,'left_tbi_score','STRING','TLSC',11,'pixalere.patientprofile.form.left_tbi_score',NULL,1,115,'normal',0,0,0,0,0,NULL,0),(278,0,'right_tbi_score','STRING','TRSC',11,'pixalere.patientprofile.form.right_tbi_score',NULL,1,116,'normal',0,0,0,0,0,NULL,1),(352,0,'transcutaneous_date','STRING','DATE',11,'pixalere.patientprofile.form.transcutaneous_date',NULL,1,118,'normal',0,0,0,0,1,NULL,0),(354,127,'transcutaneous_lab','INTEGER','RADO',11,'pixalere.patientprofile.form.transcutaneous_by',NULL,1,117,'normal',0,0,0,0,1,NULL,0),(279,128,'left_transcutaneous_pressure','INTEGER','DRP',11,'pixalere.patientprofile.form.left_transcutaneous_pressures',NULL,1,119,'normal',0,0,0,0,1,NULL,0),(280,128,'right_transcutaneous_pressure','INTEGER','DRP',11,'pixalere.patientprofile.form.right_transcutaneous_pressures',NULL,1,120,'normal',0,0,0,0,1,NULL,1),(281,21,'left_edema_severity','INTEGER','DRP',11,'pixalere.patientprofile.form.left_edema_severity',NULL,1,74,'normal',0,0,0,74,1,NULL,0),(282,21,'right_edema_severity','INTEGER','DRP',11,'pixalere.patientprofile.form.right_edema_severity',NULL,1,75,'normal',0,0,0,0,1,NULL,1),(283,20,'left_edema_location','INTEGER','DRP',11,'pixalere.patientprofile.form.left_edema_location',NULL,1,76,'normal',0,0,0,0,1,NULL,0),(284,20,'right_edema_location','INTEGER','DRP',11,'pixalere.patientprofile.form.right_edema_location',NULL,1,77,'normal',0,0,0,0,1,NULL,1),(285,0,'sleep_positions','STRING','TXTA',11,'pixalere.patientprofile.form.sleep_positions',NULL,1,78,'normal',0,0,0,0,1,NULL,0),(287,0,'left_ankle','INTEGER','MEAS',11,'pixalere.patientprofile.form.left_ankle',NULL,1,80,'normal',0,0,0,0,1,NULL,0),(288,0,'right_ankle','INTEGER','MEAS',11,'pixalere.patientprofile.form.right_ankle',NULL,1,81,'normal',0,0,0,0,1,NULL,1),(291,0,'left_midcalf','INTEGER','MEAS',11,'pixalere.patientprofile.form.left_midcalf',NULL,1,82,'normal',0,0,0,0,1,NULL,0),(292,0,'right_midcalf','INTEGER','MEAS',11,'pixalere.patientprofile.form.right_midcalf',NULL,1,83,'normal',0,0,0,0,1,NULL,1),(295,126,'left_sensation','STRING','CHKB',11,'pixalere.patientprofile.form.left_sensation',NULL,1,84,'normal',0,0,0,75,1,NULL,0),(296,126,'right_sensation','STRING','CHKB',11,'pixalere.patientprofile.form.right_sensation',NULL,1,85,'normal',0,0,0,0,1,NULL,1),(297,0,'left_score_sensation','STRING','TXT',11,'pixalere.patientprofile.form.left_score_sensation',NULL,1,86,'normal',0,0,0,0,0,NULL,0),(298,0,'right_score_sensation','STRING','TXT',11,'pixalere.patientprofile.form.right_score_sensation',NULL,1,87,'normal',0,0,0,0,0,NULL,1),(299,113,'left_sensory','STRING','CHKB',11,'pixalere.patientprofile.form.left_sensory',NULL,1,88,'normal',0,0,0,0,1,NULL,0),(300,113,'right_sensory','STRING','CHKB',11,'pixalere.patientprofile.form.right_sensory',NULL,1,89,'normal',0,0,0,0,1,NULL,1),(301,114,'left_proprioception','INTEGER','DRP',12,'pixalere.patientprofile.form.left_proprioception',NULL,1,90,'normal',0,0,0,0,1,NULL,0),(302,114,'right_proprioception','INTEGER','DRP',12,'pixalere.patientprofile.form.right_proprioception',NULL,1,91,'normal',0,0,0,0,1,NULL,1),(328,115,'left_foot_deformities','STRING','CHKB',12,'pixalere.patientprofile.form.left_deformities',NULL,1,2,'normal',0,0,0,76,1,NULL,0),(331,115,'right_foot_deformities','STRING','CHKB',12,'pixalere.patientprofile.form.right_deformities',NULL,1,3,'normal',0,0,0,0,1,NULL,1),(329,116,'left_foot_skin','STRING','CHKB',12,'pixalere.patientprofile.form.left_skin',NULL,1,4,'normal',0,0,0,0,1,NULL,0),(330,117,'left_foot_toes','STRING','CHKB',12,'pixalere.patientprofile.form.left_toes',NULL,1,6,'normal',0,0,0,0,1,NULL,0),(332,116,'right_foot_skin','STRING','CHKB',12,'pixalere.patientprofile.form.right_skin',NULL,1,5,'normal',0,0,0,0,1,NULL,1),(333,117,'right_foot_toes','STRING','CHKB',12,'pixalere.patientprofile.form.right_toes',NULL,1,7,'normal',0,0,0,0,1,NULL,1),(403,118,'weight_bearing','INTEGER','DRP',12,'pixalere.patientprofile.form.weight_bearing_status',NULL,1,9,'normal',0,0,0,0,1,NULL,0),(304,119,'balance','INTEGER','DRP',12,'pixalere.patientprofile.form.balance',NULL,1,10,'normal',0,0,0,0,1,NULL,0),(305,120,'calf_muscle_pump','INTEGER','DRP',12,'pixalere.patientprofile.form.calf_muscle_pump',NULL,1,11,'normal',0,0,0,0,1,NULL,0),(306,121,'mobility_aids','INTEGER','DRP',12,'pixalere.patientprofile.form.mobility_aids',NULL,1,13,'normal',0,0,0,0,1,NULL,0),(307,0,'calf_muscle_comments','STRING','TXTA',12,'pixalere.patientprofile.form.calf_muscle_comments',NULL,1,12,'normal',0,0,0,0,1,NULL,1),(308,0,'mobility_aids_comments','STRING','TXTA',12,'pixalere.patientprofile.form.mobility_aids_comments',NULL,1,14,'normal',0,0,0,0,1,NULL,1),(309,0,'gait_pattern_comments','STRING','TXTA',12,'pixalere.patientprofile.form.gait_pattern_comments',NULL,1,15,'normal',0,0,0,0,1,NULL,1),(310,0,'walking_distance_comments','STRING','TXTA',12,'pixalere.patientprofile.form.walking_distance_comments',NULL,1,16,'normal',0,0,0,0,1,NULL,1),(311,0,'walking_endurance_comments','STRING','TXTA',12,'pixalere.patientprofile.form.walking_endurance_comments',NULL,1,17,'normal',0,0,0,0,1,NULL,1),(312,0,'indoor_footwear','INTEGER','YN',12,'pixalere.patientprofile.form.indoor_footwear',NULL,1,19,'normal',0,0,0,0,1,NULL,0),(313,0,'outdoor_footwear','INTEGER','YN',12,'pixalere.patientprofile.form.outdoor_footwear',NULL,1,21,'normal',0,0,0,0,1,NULL,0),(314,0,'orthotics','INTEGER','YN',12,'pixalere.patientprofile.form.orthotics',NULL,1,23,'normal',0,0,0,0,1,NULL,0),(315,0,'indoor_footwear_comments','STRING','TXTA',12,'pixalere.patientprofile.form.indoor_footwear_comments',NULL,1,20,'normal',0,0,0,0,1,NULL,1),(316,0,'outdoor_footwear_comments','STRING','TXTA',12,'pixalere.patientprofile.form.outdoor_footwear_comments',NULL,1,22,'normal',0,0,0,0,1,NULL,1),(317,0,'orthotics_comments','STRING','TXTA',12,'pixalere.patientprofile.form.orthotics_comments',NULL,1,24,'normal',0,0,0,0,1,NULL,1),(318,122,'muscle_tone_functional','INTEGER','DRP',12,'pixalere.patientprofile.form.muscle_tone',NULL,1,26,'normal',0,0,0,0,1,NULL,0),(319,123,'arches_foot_structure','INTEGER','DRP',12,'pixalere.patientprofile.form.arches_foot_structure',NULL,1,27,'normal',0,0,0,0,1,NULL,0),(320,0,'supination_foot_structure','INTEGER','YN',12,'pixalere.patientprofile.form.supination',NULL,1,28,'normal',0,0,0,0,1,NULL,0),(321,0,'pronation_foot_structure','INTEGER','YN',12,'pixalere.patientprofile.form.pronation',NULL,1,29,'normal',0,0,0,0,1,NULL,0),(322,124,'dorsiflexion_active','STRING','CHKB',12,'pixalere.patientprofile.form.dorsiflexion_active',NULL,1,30,'normal',0,0,0,0,1,NULL,0),(323,125,'dorsiflexion_passive','STRING','CHKB',12,'pixalere.patientprofile.form.dorsiflexion_passive',NULL,1,31,'normal',0,0,0,0,1,NULL,1),(324,124,'plantarflexion_active','STRING','CHKB',12,'pixalere.patientprofile.form.plantarflexion_active',NULL,1,32,'normal',0,0,0,0,1,NULL,0),(325,125,'plantarflexion_passive','STRING','CHKB',12,'pixalere.patientprofile.form.plantarflexion_passive',NULL,1,33,'normal',0,0,0,0,1,NULL,1),(326,124,'greattoe_active','STRING','CHKB',12,'pixalere.patientprofile.form.greattoes_active',NULL,1,34,'normal',0,0,0,0,1,NULL,0),(327,125,'greattoe_passive','STRING','CHKB',12,'pixalere.patientprofile.form.greattoes_passive',NULL,1,35,'normal',0,0,0,0,1,NULL,1),(382,0,'comments','STRING','COMM',3,'pixalere.woundassessment.form.contains_comments',NULL,1,999,'normal',0,0,0,0,0,NULL,0),(383,0,'comments','STRING','COMM',4,'pixalere.woundassessment.form.contains_comments',NULL,1,999,'normal',0,0,0,0,0,NULL,0),(384,0,'comments','STRING','COMM',5,'pixalere.woundassessment.form.contains_comments',NULL,1,999,'normal',0,0,0,0,0,NULL,0),(385,0,'comments','STRING','COMM',6,'pixalere.woundassessment.form.contains_comments',NULL,1,999,'normal',0,0,0,0,0,NULL,0),(467,0,'left_less_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.left_capillary_less',NULL,1,39,'normal',0,0,0,73,1,NULL,0),(468,0,'right_less_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.right_capillary_less',NULL,1,39,'normal',0,0,0,0,1,NULL,1),(469,0,'left_more_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.left_capillary_more',NULL,1,39,'normal',0,0,0,0,1,NULL,0),(470,0,'right_more_capillary','INTEGER','YN',11,'pixalere.patientprofile.form.right_capillary_more',NULL,1,39,'normal',0,0,0,0,1,NULL,1),(483,0,'referral','STRING','REF',3,'pixalere.referral.form.title_fc',NULL,1,998,'normal',0,0,0,0,0,NULL,1),(484,0,'referral','STRING','REF',4,'pixalere.referral.form.title_fc',NULL,1,998,'normal',0,0,0,0,0,NULL,1),(485,0,'referral','STRING','REF',5,'pixalere.referral.form.title_fc',NULL,1,998,'normal',0,0,0,0,0,NULL,1),(486,0,'referral','STRING','REF',6,'pixalere.referral.form.title_fc',NULL,1,998,'normal',0,0,0,0,0,NULL,1),(487,0,'foot_comments','STRING','TXTA',12,'pixalere.patientprofile.form.foot_comments',NULL,1,998,'normal',0,0,0,0,1,NULL,0),(507,0,'bandages_in','STRING','TXT',3,'pixalere.treatment.form.bandages_in',36,1,25,'normal',0,0,0,0,1,'bandages_out: {digits: true}},messages: {bandages_out: \'Numbers only please\'',0),(505,97,'pain','INTEGER','DRP',6,'pixalere.incisionassessment.form.pain',NULL,1,9,'normal',1,0,0,84,1,NULL,0),(506,0,'pain_comments','STRING','TXTA',6,'pixalere.pain_comments',NULL,1,10,'normal',1,0,0,85,1,NULL,0),(508,38,'cs_show','INTEGER','YN',4,'pixalere.treatment.form.cs_done',36,1,45,'normal',1,0,0,0,1,NULL,0),(509,38,'cs_show','INTEGER','YN',6,'pixalere.treatment.form.cs_done',36,1,37,'normal',1,0,0,0,1,NULL,0),(510,38,'cs_show','INTEGER','YN',5,'pixalere.treatment.form.cs_done',36,1,32,'normal',1,0,0,0,1,NULL,0),(511,38,'cs_show','INTEGER','YN',3,'pixalere.treatment.form.cs_done',36,1,32,'normal',1,0,0,0,1,NULL,0),(519,0,'maintenance','STRING','ADM',3,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(520,0,'maintenance','STRING','ADM',4,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(521,0,'maintenance','STRING','ADM',5,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(522,0,'maintenance','STRING','ADM',6,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(523,0,'maintenance','STRING','ADM',17,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(524,0,'maintenance','STRING','ADM',1,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(525,0,'maintenance','STRING','ADM',2,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(526,0,'maintenance','STRING','ADM',7,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(527,0,'maintenance','STRING','ADM',8,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(528,0,'maintenance','STRING','ADM',9,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(529,0,'maintenance','STRING','ADM',10,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(530,0,'maintenance','STRING','ADM',11,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(531,0,'maintenance','STRING','ADM',12,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(532,0,'maintenance','STRING','ADM',13,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(533,0,'height','DOUBLE','TXT',40,'pixalere.patientprofile.form.height',NULL,1,1,'normal',0,0,0,0,1,'height: {number: true}},messages: {height: \'Numbers and Decimal only please\'',0),(534,0,'weight','DOUBLE','TXT',40,'pixalere.patientprofile.form.weight',NULL,1,7,'normal',0,0,0,0,1,'weight: {number: true}},messages: {weight: \'Numbers and Decimal only please\'',0),(535,0,'temperature','DOUBLE','TXT',40,'pixalere.patientprofile.form.temperature',NULL,1,10,'normal',0,0,0,0,1,'temperature: {number: true}},messages: {temperature: \'Numbers and Decimal only please\'',0),(536,0,'pulse','INTEGER','TXT',40,'pixalere.patientprofile.form.pulse',NULL,1,15,'normal',0,0,0,0,1,'pulse: {digits: true}},messages: {pulse: \'Numbers only please\'',0),(537,0,'resp_rate','INTEGER','TXT',40,'pixalere.patientprofile.form.resp_rate',NULL,1,20,'normal',0,0,0,0,1,'resp_rate: {digits: true}},messages: {resp_rate: \'Numbers only please\'',0),(545,0,'blood_pressure','STRING','TXT',40,'pixalere.patientprofile.form.blood_pressure',NULL,1,60,'normal',0,0,0,0,1,'blood_pressure: {numberslash: true}},messages: {blood_pressure: \'Numbers and slashes only please\'',0),(544,0,'maintenance','STRING','ADM',40,'pixalere.viewer.assessment_updates',NULL,1,1000,'normal',0,0,0,0,0,NULL,0),(546,0,'pdf','STRING','PDF',3,'pixalere.viewer.pdf',NULL,1,1010,'normal',0,0,0,0,0,NULL,0),(547,0,'pdf','STRING','PDF',4,'pixalere.viewer.pdf',NULL,1,1010,'normal',0,0,0,0,0,NULL,0),(548,0,'pdf','STRING','PDF',5,'pixalere.viewer.pdf',NULL,1,1010,'normal',0,0,0,0,0,NULL,0),(549,0,'pdf','STRING','PDF',6,'pixalere.viewer.pdf',NULL,1,1010,'normal',0,0,0,0,0,NULL,0),(551,0,'bandages_out','STRING','TXT',3,'pixalere.treatment.form.bandages_out',36,1,26,'normal',0,0,0,0,1,'bandages_in: {digits: true}},messages: {bandages_in: \'Numbers only please\'',0),(552,0,'phn','STRING','TXT',1,'pixalere.patientaccount.form.phn',NULL,1,9,'normal',0,0,0,0,0,'',0),(553,0,'review_done','INTEGER','YN',3,'pixalere.treatment.review_done',36,1,960,'normal',0,0,0,0,0,NULL,0),(554,0,'review_done','INTEGER','YN',4,'pixalere.treatment.review_done',36,1,960,'normal',0,0,0,0,0,NULL,0),(555,0,'review_done','INTEGER','YN',5,'pixalere.treatment.review_done',36,1,960,'normal',0,0,0,97,0,NULL,0),(556,0,'review_done','INTEGER','YN',6,'pixalere.treatment.review_done',36,1,960,'normal',0,0,0,0,0,NULL,0);
/*!40000 ALTER TABLE `components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(3) DEFAULT NULL,
  `orderby` int(3) DEFAULT NULL,
  `selfadmin` int(1) DEFAULT NULL,
  `config_setting` varchar(255) DEFAULT NULL,
  `enable_component_rule` varchar(255) DEFAULT NULL,
  `enable_options_rule` varchar(255) DEFAULT NULL,
  `component_type` varchar(255) DEFAULT NULL,
  `component_options` varchar(255) DEFAULT NULL,
  `component_values` varchar(255) DEFAULT NULL,
  `current_value` text,
  `readonly` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES (4,0,3,1,'useDateCalendar','','','CHKB','','','1',0),(6,1,1,0,'allowWoundCare','','','CHKB','','','1',0),(7,1,2,0,'allowStoma','','','CHKB','','','1',0),(8,1,3,0,'allowDrain','','','CHKB','','','1',0),(9,1,4,0,'allowIncision','','','CHKB','','','1',0),(12,0,1,0,'useUserNames','','','CHKB','','','1',0),(13,0,2,1,'minPasswordLength','','','DRP','4,5,6,7,8,9,10','','8',0),(14,0,3,1,'requireNumbersAndCharsInPassword','','','DRP','alpha-numeric,alpha,alpha-numeric-uppercase,alpha-numeric-uppercase-specialcharacter','','alpha-numeric',0),(21,0,2,1,'showSpellCheckButton','','','CHKB','','','0',0),(23,0,1,1,'useDobCalendar','useDateCalendar==1','','CHKB','','','0',0),(25,0,3,1,'showBradenScale','','','CHKB','','','1',0),(26,0,4,1,'requiredBradenScale','showBradenScale==1','','CHKB','','','1',0),(30,6,3,1,'showCurvedIncisions','','','CHKB','','','0',0),(31,6,4,1,'allowSaveWPwithoutAlphas','','','CHKB','','','1',0),(38,6,11,1,'allowMultipleEtiologies','allowWoundCare==1','','CHKB','','','0',0),(39,6,12,1,'allowMultipleWoundProfiles','','','CHKB','','','1',0),(42,7,1,1,'showAssessDoneIndicator','','','CHKB','','','1',0),(43,7,2,1,'setupAssessmentScreen','','','CHKB','','','0',0),(45,7,4,1,'defaultWoundBed100Percent','','','CHKB','','','0',0),(46,7,5,1,'disableMeasurentsForPartial','','','CHKB','','','1',0),(47,7,6,1,'enforceLengthLargerThanWidth','','','CHKB','','','1',0),(48,7,7,1,'woundDepthRequiredForFullAssess','','','CHKB','','','1',0),(49,7,8,1,'woundLength','','','INPT','10,20,30,40,50,60,70,80,90,100','','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65',0),(50,7,9,1,'woundWidth','','','INPT','10,20,30,40,50,60,70,80,90,100','','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65',0),(51,7,10,1,'woundDepth','','','INPT','10,20,30,40,50,60,70,80,90,100','','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20',0),(52,7,11,1,'sinusTracts','allowWoundCare==1','','INPT','1,2,3,4,5,6,7,8,9,10','','1,2,3,4,5,6,7,8,9,10',0),(53,7,12,1,'undermining','allowWoundCare==1','','INPT','1,2,3,4,5,6,7,8,9,10','','1,2,3,4,5,6,7,8,9,10',0),(54,7,13,1,'exudateAutoNils','','','CHKB','','','1',0),(55,7,14,1,'disableImagesForPartial','','','CHKB','','','1',0),(57,8,1,1,'hideClosedAlphasOnTreatment','','','CHKB','','','0',0),(58,8,2,1,'showProductsFilter','','','CHKB','','','1',0),(59,8,3,1,'showProductsSearch','','','CHKB','','','0',0),(60,8,4,1,'showNursingVisitFrequency','','','CHKB','','','1',0),(62,0,1,1,'showCorrectionsWithStrikeThrough','','','CHKB','','','1',0),(64,10,1,1,'autoReferralsPrevRefDays','','','DRP','7,14,21,28','','21',0),(65,10,2,1,'makeAutoReferralsHealrate','','','CHKB','','','1',0),(66,10,3,1,'autoReferralsHealDays','makeAutoReferralsHealrate==1','','DRP','7,14,21,28','','7',0),(67,10,4,1,'autoReferralsHealrate','makeAutoReferralsHealrate==1','','DRP','10,20,30,40,50','10,20,30,40,50','30',0),(68,10,5,1,'makeAutoReferralsDressing','','','CHKB','','','1',0),(69,10,6,1,'autoReferralsDressingsDays','makeAutoReferralsDressing==1','','DRP','7,14,21,28','','21',0),(70,10,7,1,'autoReferralsDressingsPerWeek','makeAutoReferralsDressing==1','','DRP','1,2,3,4,5,6,7,8,9,10,11,12,13,14','','3',0),(71,10,8,1,'repopulateTreatmentComments','','','CHKB','','','0',0),(73,0,1,1,'checkPHN','','','DRP','pixalere.none,BC',',BC','BC',0),(75,0,1,0,'woundEtiologyPressure','allowWoundCare==1','','INPN','','','142',1),(76,7,2,0,'incisionClosureStatusRemoved','allowIncision==1','','INPN','','','1216',1),(77,7,3,0,'stoolQuantityNil','allowStoma==1','','INPN','','','1352',1),(78,7,4,0,'urineQuantityNil','allowStoma==1','','INPN','','','1346',1),(79,7,5,0,'mucocutaneous_margin_seperated','allowStoma==1','','INPN','','','1395',1),(80,8,6,0,'basicProductCategory','','','INPN','','','82',1),(81,7,11,0,'woundActive','','','INPN','','','256',1),(82,7,11,0,'woundClosed','','','INPN','','','257',1),(83,7,13,0,'incisionActive','','','INPN','','','1706',1),(84,7,14,0,'incisionClosed','','','INPN','','','1707',1),(85,7,15,0,'drainActive','','','INPN','','','1708',1),(86,7,16,0,'drainClosed','','','INPN','','','1709',1),(87,7,17,0,'ostomyActive','','','INPN','','','1704',1),(88,7,18,0,'ostomyClosed','','','INPN','','','1705',1),(89,7,19,0,'woundExudateTypeNil','','','INPN','','','222',1),(90,7,20,0,'woundExudateAmountNil','','','INPN','','','225',1),(91,7,21,0,'incisionExudateTypeNil','','','INPN','','','1085',1),(92,7,22,0,'incisionExudateAmountNil','','','INPN','','','1128',1),(93,7,23,0,'drainExudateTypeNil','','','INPN','','','1559',1),(94,7,24,0,'drainExudateAmountNil','','','INPN','','','1563',1),(95,0,25,0,'woundGoalHealable','','','INPN','','','1081',1),(97,0,26,0,'allowRecommendationAcknowledgement','','','CHKB','','','1',0),(0,0,27,0,'showLimbComments','','','CHKB','','','0',0),(101,0,28,1,'purgeTMPRecordsAfterXDays','','','INPN','','','7',0),(103,20,1,0,'reportWoundCount','','','CHKB','','','1',0),(104,20,1,0,'reportOverviewActivePatients','','','CHKB','','','1',0),(105,20,1,0,'reportReferralsAndRecommendations','','','CHKB','','','1',0),(107,20,1,0,'reportHealtimeAndCostPerEtiology','','','CHKB','','','1',0),(109,20,1,0,'reportLastAssessments','','','CHKB','','','1',0),(111,20,1,0,'reportPatientFile','','','CHKB','','','1',0),(115,0,1,0,'appAbb',' ','','INPN','','','Pix',0),(116,0,2,0,'appShort','','','INPN','','','Pixalere',0),(117,0,3,0,'appFull','',' ','INPN','','','Pixalere Wound Care Solution',0),(118,7,18,0,'ostomyHold','','','INPN','','','1710',1),(121,20,2,1,'includeOstomyInOnLastAssessmentReport','reportLastAssessments==1','','CHKB','','','0',0),(123,6,9,1,'requireIncisionTag','allowIncision==1','','CHKB','','','1',0),(124,6,9,1,'allowIncisionBeforeTag','allowIncision==1','','CHKB','','','0',0),(125,6,9,1,'removeIncisionWhenTagRemoved','allowIncision==1','','CHKB','','','1',0),(130,0,30,0,'male','','','INPN','','','130',1),(131,0,30,0,'female','','','INPN','','','131',0),(132,0,30,0,'acute','','','INPN','','','355',1),(133,0,30,0,'community','','','INPN','','','356',1),(134,0,30,0,'residential','','','INPN','','','-1',0),(135,0,30,1,'wrongpassword_info','','','INPT','','','411-411-4141',0),(136,0,30,1,'homeInfo_left','','','TEXT','','','',0),(137,0,30,1,'homeInfo_right','','','TEXT','','','<p style=\"font-style: normal; \"><label class=\"title\" style=\"font-size: 10pt; font-weight: normal; \">Trouble with:</label><br><label style=\"font-size: 10pt; font-weight: normal; \">Pixalere Website?</label><br><label style=\"font-size: 10pt; font-weight: normal; \">Forget your Password?</label><br><label style=\"font-size: 10pt; font-weight: normal; \">Camera Questions?</label><br><label style=\"font-size: 10pt; font-weight: normal; \">Step by Step Navigation?</label><br><label class=\"mainpageheader\" style=\"font-size: 10pt; font-weight: normal; \"><font size=\"2\">Help Desk:&nbsp;</font></label><span style=\"background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); font-family: Trebuchet, verdana, sans-serif; text-align: -webkit-left; \"><b><font size=\"4\">(705) 368-2182 ext. 210</font></b></span></p><p style=\"font-size: 10pt; font-weight: normal; \"><b>If you have <i>ideas</i> or <i>issues</i> with Pixalere\'s software please use the Report Issue link on the bottom of every page.</b></p>',0),(138,0,30,1,'portal_link','','','INPT','','','',0),(139,0,30,1,'portal_redirect','','','CHKB','','','0',0),(140,0,30,1,'server_ip','','','INPT','','','https://pix.mnaamodzawin.com:8443',0),(141,0,30,1,'allowFullAssessmentDue','','','CHKB','','','1',0),(143,0,29,1,'showReviewDone','','','CHKB','','','0',0),(144,8,29,1,'showTreatmentPlan','','','CHKB','','','0',0),(145,7,29,1,'recurrentRequired','','','CHKB','','','1',0),(146,10,9,1,'hideAllButAcknowledgeRecommendation','','','CHKB','','','1',0),(148,3,36,1,'deletePatientsOnSync','','','CHKB','','','1',0),(151,10,8,1,'makeAutoReferralAntimicrobial','','','CHKB','','','1',0),(153,10,9,1,'referralsAntimicrobialProducts','','','INPT','','','941,942,1230,1231,1173,1174,1000,1001,1170,1171,1172',0),(154,10,6,1,'autoReferralsAntimicrobialDays','makeAutoReferralAntimicrobial==1','','DRP','7,14,21,28','','21',0),(155,3,33,1,'offlineRedirect','','','CHKB','','','0',0),(157,10,7,0,'autoReferralPriority','','','INPN','','','1770',1),(158,10,7,0,'offlineAutoReferralHack','','','CHKB','','','1',0),(159,0,7,1,'showLast4MonthsOfComments','','','CHKB','','','0',0),(160,10,7,0,'alternativeLimbComment','','','CHKB','','','0',0),(171,2,7,1,'hidePatientAccountModification','','','CHKB','','','0',0),(172,2,8,1,'enableDataExtraction','','','CHKB','','','0',0),(173,2,9,1,'dataPath','','','INPT','','','c:/share/data',0),(174,2,10,1,'UNCPath','','','INPT','','','\\dc1serv534PixalereFileDrop',0),(175,2,11,1,'dataHowMuch','','','DRP','1,3,5','','3',0),(176,0,11,0,'noTreatmentListID','','','INPT','','','3200',1),(177,3,30,0,'unknownGender','','','INPN','','','2132',1),(178,3,30,1,'redirectOnPatientSearch','','','CHKB','','','1',0),(179,3,30,1,'hideCreateTabIfNotAdmin','redirectOnPatientSearch==1','','CHKB','','','1',0),(180,2,30,1,'integration_validation','','','DRP','phn,secondary_phn,secondary_phn or phn,phn or secondary_phn','','secondary_phn or phn',0),(181,7,30,1,'deleteAssessmentTimeframe','','','DRP','3,7,14,28,-1','','14',0),(183,20,1,0,'reportWoundManagementPlan','',' ','CHKB',' ',' ','0',0),(185,20,1,0,'showTransferOnSearch','',' ','CHKB',' ',' ','1',0),(186,0,1,0,'changePasswordFlag','',' ','CHKB',' ',' ','1',0),(187,0,1,0,'passwordExpiring','',' ','CHKB',' ',' ','0',0),(188,0,1,0,'passwordExpiringDays','',' ','DRP','90,120,180,360,720',' ','180',0),(189,0,7,0,'hidePatientTimezone','','','CHKB','','','0',0),(190,0,0,1,'hidephn2','','','CHKB','','','0',0),(191,0,0,1,'hidephn3','','','CHKB','','','0',0),(192,0,0,1,'phn1_name','','','INPT','','','PHN #',0),(193,0,0,1,'phn2_name','hidephn2==0','','INPT','','','MRN #',0),(194,0,0,1,'phn3_name','hidephn3==0','','INPT','','','Chart #',0),(195,0,0,1,'timezone','','','INPT','','','America/Vancouver',0),(196,0,0,1,'phn2_validate','','','DRP','intOnly(event),intAlphaOnly(event)','','intOnly(event)',0),(197,0,0,1,'phn3_validate','','','DRP','intOnly(event),intAlphaOnly(event)','','intOnly(event)',0),(198,0,0,1,'alphaDraggable','','','CHKB','','','1',0),(199,0,0,1,'phnFormat','','','INPT','','','4-3-3',0),(200,0,1,1,'treatmentCatName','',' ','INPT','',' ','Treatment Area',0),(201,0,1,1,'treatmentLocationName','',' ','INPT','',' ','Treatment Location',0),(202,0,1,1,'session_timeout','',' ','INPN','',' ','30',0),(203,0,1,1,'showDeleted','',' ','CHKB','',' ','1',0),(204,0,1,1,'passwordlockout','',' ','DRP','0,2,3,5,6,8,10',' ','0',0),(205,0,7,1,'image_width','',' ','DRP','640,1280,1600,2000',' ','640',0),(206,0,0,1,'customer_name','',' ','INPT','',' ','Mnaamodzawin Health Services Inc.',0),(207,0,20,1,'prevalence_population','',' ','INPT','',' ','300000',0),(208,0,20,1,'dashboard_email','',' ','INPT','',' ','travis@pixalere.com',0),(209,0,0,1,'email_host','',' ','INPT','',' ','smtp.gmail.com',0),(210,0,0,1,'email_user','',' ','INPT','',' ','sendmail@pixalere.com',0),(211,0,0,1,'email_password','',' ','INPT','',' ','SendM@il7',0),(212,0,0,0,'user_stats','',' ','INPT','',' ','travis@pixalere.com,ken@pixalere.com',1),(213,0,0,1,'support_email','',' ','INPT','',' ','travis@pixalere.com',0),(214,0,0,1,'photos_path','',' ','INPT','',' ','/opt/photos',0),(215,0,0,0,'customer_id','',' ','INPT','',' ','PIX1234',1),(216,0,0,1,'imperial','',' ','DRP','imperial,metric',' ','metric',1),(217,0,1,0,'termsandconditions','',' ','TEXT','',' ','This Agreement dated __DATE__ between Pixalere Healthcare Inc. (PHI) located at\nSuite 201,33711 Laurel St, Abbotsford, British Columbia and __________ (__________),\nlocated at ______________________.\n\nBY INSTALLING OR USING THE PIXALERE SOFTWARE KNOWN AS THE PIXALERE\nDIGITAL WOUND ASSESSMENT PACKAGE (THE \"PRODUCT\"), __________ IS\nCONSENTING TO BE BOUND BY, AND IS BECOMING, A PARTY TO THIS AGREEMENT\nAND ANY ADDENDUM HERETO. IF __________ DOES NOT AGREE TO ALL OF THE TERMS\nOF THIS AGREEMENT, THE __________ MUST NOT INSTALL OR USE THE PRODUCT. IF\nTHESE TERMS ARE CONSIDERED AN OFFER, ACCEPTANCE IS EXPRESSLY LIMITED TO\nTHESE TERMS.\n\n1. LICENSE AGREEMENT. The use of any third party software product or interface included in\nthe Product shall be governed by the third party\'s license agreement and not by this Agreement,\nwhether that license agreement is presented for acceptance the first time that the third party\nsoftware is invoked, is included in a file in electronic form, or is included in the package in printed\nform.\n\n2. LICENSE GRANT. Pixalere Healthcare Inc grants ____________ a limited, royalty-free, non-\nexclusive, non-sub-licensable, and non-transferable license to: (a) use the demonstration version\nof the Product; (b) use the fully-enabled version of the Product; and (c) when using the fully-\nenabled version of the Product, to receive from Pixalere Healthcare hard-copy documentation,\ntechnical support, telephone assistance, or enhancements, new releases or updates to the\nProduct (collectively the \"Support Programs. If __________ downloads or otherwise obtains in\nany manner any Support Programs they shall become part of the Product and the terms of this\nAgreement shall apply notwithstanding the version of the Product in use by the __________.\nPixalere Healthcare Inc may elect, at its sole discretion, to provide said products and/or services\nwithout any obligation to __________. Except to the extent specified to the contrary in this\nAgreement or in an addendum hereto, Pixalere Healthcare shall not be obligated to support\nthe Product or the Support Programs, whether by providing advice, training, error-correction,\nmodifications, updates, new releases or enhancement or otherwise.\n\n3. RESTRICTIONS. Except as otherwise expressly permitted in this Agreement, __________\nmay not: (i) customize, modify or create any derivative works of the Product, Support\nPrograms or documentation, including translation or localization; (ii) decompile, disassemble,\nreverse engineer, or otherwise attempt to derive the source code for the Product and/or any\nenhancements, new releases or updates to the Product (except to the extent applicable laws\nspecifically prohibit such restriction); (iii) encumber, sell, rent, lease, sublicense, or otherwise\ntransfer rights to the Product and/or the Support Programs; (iv) remove or alter any trademark,\nlogo, copyright or other proprietary notices, legends, symbols or labels in the Product and/or\nthe Support Programs; or (v) publish any results of benchmark tests run on the Product and/\nor any enhancements, new releases or updates to the Product to a third party without Pixalere\nHealthcare prior written consent. __________ may not copy, alter, modify or resell the Product\nand/or the Support Programs in whole or in part. The __________ is permitted to use the Product\nand the Support Programs under the license granted herein in the course of providing wound\ncare services by its staff to contracted customers and their clients.\n\n4. FEES. There is no license fee for the demonstration version of the Product; there is a license\nfee for the fully enabled version of the Product. If __________ wishes to receive the Product\non media, there may be a charge for the media and for shipping and handling. __________ is\nresponsible for any and all applicable taxes.\n\n21\n\nPIXALERE SOFTWARE AND SERVICES AGREEMENT\n\n5. PROPRIETARY RIGHTS. Title, ownership rights, and any and all intellectual property rights\nin the Product and any and all Support Programs shall remain in Pixalere Healthcare and/or its\nsuppliers. __________ acknowledges such ownership and intellectual property rights and will\nnot take any action to jeopardize, limit or interfere in any manner with Pixalere Healthcare or\nits suppliers\' ownership of or rights with respect to the Product or the Support Programs. The\nProduct and the Support Programs are protected by copyright and other intellectual property laws\nand by international treaties. Title and related rights in the content accessed through the Product\nand/or Support Programs are the property of the applicable content owner and are protected by\napplicable law. The license granted under this Agreement gives __________ no rights to such\ncontent. __________ shall ensure that the Product and the Support Programs are protected at\nall times from misuse, damage, destruction or any form of unauthorized use. Pixalere Healthcare\nmay use data collected via Pixalere for non-commercial, educational, or research purposes, and\nwill inform the __________ in writing prior to such usage.\n\n6. DISCLAIMERS. PIXALERE HEALTHCARE DOES NOT VERIFY THE CONTENT OF THE\nMATERIAL PROVIDED BY __________ OR ANY THIRD PARTY THAT IS ACCESSED BY YOU\nTHROUGH THE USE OF THE PRODUCT AND/OR ANY MATERIAL LINKED THROUGH SUCH\nCONTENT (COLLECTIVELY, THE ?CONTENT?).\n\nTHE PRODUCT AND CONTENT ARE PROVIDED ON AN \"AS IS\" BASIS, WITHOUT\nWARRANTY OF ANY KIND, INCLUDING WITHOUT LIMITATION ANY EXPRESS OR IMPLIED\nWARRANTIES OF ANY KIND INCLUDING BUT NOT LIMITED TO WARRANTIES THAT THE\nPRODUCT AND/OR CONTENT ARE FREE OF DEFECTS, MERCHANTABLE, FIT FOR A\nPARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY\nAND PERFORMANCE OF THE PRODUCT AND/OR CONTENT IS BORNE BY __________.\nTHIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS\nAGREEMENT. NO USE OF THE PRODUCT IS AUTHORIZED HEREUNDER EXCEPT UNDER\nTHIS DISCLAIMER.\n\nALTHOUGH ALL CONTENT IS BELIEVED TO BE RELIABLE, PIXALERE HEALTHCARE\nDOES NOT GUARANTEE THE RELIABILITY, ACCURACY, COMPLETENESS, SAFETY,\nTIMELINESS, LEGALITY, USEFULNESS, ADEQUACY, OR SUITABILITY OF ANY CONTENT\nNOR IS IT RESPONSIBLE FOR ANY LIABILITY ARISING OUT OF THE CONTENT. THE\nCONTENT MAY BE HARMFUL, UNTIMELY, INCOMPLETE, OR INACCURATE AND,\nACCORDINGLY, YOU AGREE TO EXERCISE CAUTION, DISCRETION AND COMMON\nSENSE WHEN USING THE CONTENT.\n\nTHE CONTENT IS NOT INTENDED TO SUBSTITUTE FOR INFORMED PROFESSIONAL\nADVICE. THE __________ SHOULD NOT USE THE PRODUCT AND/OR CONTENT FOR\nEMERGENCY PURPOSES OR TO SEEK OR PROVIDE DIAGNOSIS OR TREATMENT OF\nMEDICAL, PSYCHIATRIC OR PSYCHOLOGICAL PROBLEMS, OR AS A SUBSTITUTE\nFOR FACE-TO-FACE PROFESSIONAL MEDICAL, PSYCHIATRIC, PSYCHOLOGICAL, OR\nOTHER PROFESSIONAL CONSULTATION. IF YOU HAVE A MEDICAL OR PSYCHIATRIC\nEMERGENCY, CALL YOUR PHYSICIAN OR 911 IMMEDIATELY. YOU MUST USE YOUR\nJUDGMENT TO DETERMINE WHEN IT IS NECESSARY TO CONSULT WITH A PROVIDER\nWHO IS LOCAL, LICENSED IN YOUR PROVINCE, STATE OR COUNTRY, AVAILABLE IN\nPERSON, OR OTHERWISE POSSESSES THE QUALITIES REQUIRED TO PROPERLY\nDIAGNOSE OR ADVISE YOU, ESPECIALLY IN AREAS OF EXPERTISE, SUCH AS MEDICINE,\nREQUIRING LICENSING OR GOVERNMENT CERTIFICATION, OR FOR HIGH RISK\nACTIVITIES.\n\nPIXALERE HEALTHCARE DOES NOT DIRECTLY OR INDIRECTLY PRACTICE MEDICINE OR\nDISPENSE MEDICAL SERVICES.\n\n7. LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE\n22\n\nPIXALERE SOFTWARE AND SERVICES AGREEMENT\n\nLAW, IN NO EVENT WILL PIXALERE HEALTHCARE, ITS PHIS, SUPPLIERS OR RESELLERS\nBE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR PUNITIVE\nDAMAGES ARISING OUT OF THE USE OF OR INABILITY TO USE THE PRODUCT OR\nCONTENT, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL,\nWORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, ECONOMIC LOSS, LOSS\nOF PROFITS, THIRD PARTY CLAIMS, OR ANY AND ALL OTHER DAMAGES OR LOSSES,\nEVEN IF ADVISED OF THE POSSIBILITY THEREOF, AND REGARDLESS OF THE LEGAL\nOR EQUITABLE THEORY (CONTRACT, TORT OR OTHERWISE) UPON WHICH THE CLAIM\nIS BASED. IN ANY CASE, PIXALERE HEALTHCARE?S ENTIRE LIABILITY UNDER ANY\nPROVISION OF THIS AGREEMENT SHALL NOT EXCEED $100, WITH THE EXCEPTION\nOF DEATH OR PERSONAL INJURY CAUSED BY THE NEGLIGENCE OF PIXALERE\nHEALTHCARE TO THE EXTENT APPLICABLE LAW PROHIBITS THE LIMITATION OF\nDAMAGES IN SUCH CASES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR\nLIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THIS EXCLUSION AND\nLIMITATION MAY NOT BE APPLICABLE.\n\n__________ SHALL BE SOLELY RESPONSIBLE, AT ITS OWN EXPENSE, FOR ACQUIRING,\nINSTALLING, MAINTAINING AND UPDATING ALL CONNECTIVITY EQUIPMENT,\nHARDWARE, SOFTWARE AND OTHER EQUIPMENT AS MAY BE NECESSARY FOR IT TO\nUSE THE PRODUCT UNLESS PROVISIONS HAVE BEEN MADE IN AN ADDENDUM HERETO\nOR IN A SEPARATE AGREEMENT.\n\n8. RELEASE AND WAIVER. __________ USES PIXALERE HEALTHCARE?S PRODUCTS AND/\nOR SERVICES AT HIS OR HER OWN RISK. To the maximum extent permitted by applicable\nlaw, You hereby release, and waive all claims against, Pixalere Healthcare and its employees\nand agents from any and all liability for claims, damages (actual and consequential), costs and\nexpenses (including litigation costs and attorneys\' fees) of every nature and kind, arising out of or\nin any way connected with use of the Product and/or Support Programs. If You are a California\ncorporation or resident, You waive Your rights under California Civil Code Section 1542, which\nstates, \"A general release does not extend to claims which the creditor does not know or suspect\nto exist in his favor at the time of executing the release, which if known by him must have\nmaterially affected his settlement with the debtor.\" Residents of other states and nations similarly\nwaive their rights under applicable and/or analogous laws, statues, or regulations.\n\n9. MISCELLANEOUS. (a) This Agreement and any addendum or amendments hereto constitutes\nthe entire agreement between the parties concerning the subject matter hereof. (b) This\nAgreement and any addendum or amendments hereto may be amended only by a writing signed\nby both parties. (c) Except to the extent applicable law, if any, provides otherwise, this Agreement\nand any addendum or amendments hereto shall be governed by the laws of the Province of\nBritish Columbia and the federal laws of Canada applicable therein, excluding conflict of law\nprovisions. (d.) This Agreement and any addendum or amendments hereto shall not be governed\nby the United Nations Convention on Contracts for the International Sale of Goods. (e) If any\nprovision in this Agreement or any addendum or amendments hereto should be held illegal or\nunenforceable by a court having jurisdiction, such provision shall be modified to the extent\nnecessary to render it enforceable without losing its intent, or severed from this Agreement if no\nsuch modification is possible, and other provisions of this Agreement and any addendum or\namendments hereto shall remain in full force and effect. (f) The controlling language of this\nAgreement and any addendum or amendments hereto is English. If __________ has received a\ntranslation into another language, it has been provided for __________\'s convenience only. (g) A\nwaiver by either party of any term or condition of this Agreement and any addendum or\namendments hereto or any breach thereof, in any one instance, shall not waive such term or\ncondition or any subsequent breach thereof. (h) The provisions of this Agreement and any\naddendum or amendments hereto which require or contemplate performance after the expiration\nor termination of this Agreement shall be enforceable notwithstanding said expiration or\ntermination. (i) __________ may not assign or otherwise transfer by operation of law or otherwise\n23\n\nPIXALERE SOFTWARE AND SERVICES AGREEMENT\n\nthis Agreement or any addendum or amendments hereto or any rights or obligations herein\nexcept in the case of a merger or the sale of all or substantially all of __________\'s assets to\nanother entity. PHI may assign this Agreement and any addendum or amendments hereto to a\nthird party at any time. (j) This Agreement shall be binding upon and shall inure to the benefit of\nthe parties, their successors and permitted assigns. (k) Neither party shall be in default or be\nliable for any delay, failure in performance (excepting the obligation to pay) or interruption of\nservice resulting directly or indirectly from any cause beyond its reasonable control. (l) The\nrelationship between Pixalere Healthcare and __________ is that of independent contractors and\nneither __________ nor its agents shall have any authority to bind Pixalere Healthcare in any\nway. (m) If any dispute arises under this Agreement, the prevailing party shall be reimbursed by\nthe other party for any and all legal fees and costs associated therewith. (n) If any Pixalere\nHealthcare professional services are being provided, then such professional services are\nprovided pursuant to the terms of a separate Professional Services Agreement between Pixalere\nHealthcare and __________. The parties acknowledge that such services are acquired\nindependently of the Product licensed hereunder, and that provision of such services is not\nessential to the functionality of the Product or Support Programs. (o) The headings to the\nsections of this Agreement and any addendum or amendments hereto are used for convenience\nonly and shall have no substantive meaning. (p) Where any conflict occurs between the\nprovisions contained in two or more of the documents forming this Agreement, the documents\ncomprising this Agreement shall be read in the following order: (i) the provisions of this document;\nand (ii) the provisions of any addendum or amendment.\n\n10. __________ OUTSIDE OF CANADA AND THE U.S. If __________ is located outside of\nCanada or the U.S., then __________ is responsible for complying with any local laws in its\njurisdiction which might impact its right to import, export or use the Product, and __________\nrepresents that it has complied with any regulations or registration procedures required by\napplicable law to make this license enforceable.\n\n11. Notwithstanding provision 13 in the Service Agreement, the parties expressly agree that PHI\nreserves the right to collect, report, share and publish anonymized data contained in the Pixalere\nSoftware and any related database.',0),(218,0,1,0,'allowBurn','',' ','CHKB','',' ','0',0),(219,0,1,0,'img_server_url','',' ','INPT','',' ','https://pix.mnaamodzawin.com:8443/IpadImageUpload.do',0),(220,0,1,0,'img_server_url_redirect','',' ','INPT','',' ','https://pix.mnaamodzawin.com:8443/vm/viewer/ipad_success.vm',0),(221,0,0,0,'img_server_license_key','',' ','INPT','',' ','79FF1-0005B-55A90-00008-968FB-AB3E6D',1);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `dressing_change_frequency`
--

DROP TABLE IF EXISTS `dressing_change_frequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dressing_change_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `professional_id` int(11) DEFAULT NULL,
  `user_signature` varchar(255) DEFAULT NULL,
  `dcf` int(11) DEFAULT NULL,
  `time_stamp` varchar(255) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `deleted_reason` varchar(255) DEFAULT NULL,
  `delete_signature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dcf` (`dcf`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `assessment_id` (`assessment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messageId` varchar(255) DEFAULT '',
  `sendingFacility` varchar(255) DEFAULT '',
  `messageDateTime` varchar(255) DEFAULT '',
  `merge` int(11) DEFAULT '0',
  `patient_from` int(11) DEFAULT '0',
  `patient_to` int(11) DEFAULT '0',
  `table_name` varchar(255) DEFAULT '',
  `row_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foot_assessment`
--

DROP TABLE IF EXISTS `foot_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foot_assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) DEFAULT '0',
  `patient_id` int(11) DEFAULT '0',
  `foot_show` int(1) DEFAULT '0',
  `left_foot_deformities` text,
  `left_foot_skin` text,
  `left_foot_toes` text,
  `right_foot_deformities` text,
  `right_foot_skin` text,
  `right_foot_toes` text,
  `weight_bearing` int(11) DEFAULT '0',
  `balance` int(11) DEFAULT '0',
  `calf_muscle_pump` int(11) DEFAULT '0',
  `mobility_aids` int(11) DEFAULT '0',
  `calf_muscle_comments` text,
  `mobility_aids_comments` text,
  `gait_pattern_comments` text,
  `walking_distance_comments` text,
  `walking_endurance_comments` text,
  `indoor_footwear` int(11) DEFAULT '0',
  `outdoor_footwear` int(11) DEFAULT '0',
  `indoor_footwear_comments` text,
  `outdoor_footwear_comments` text,
  `orthotics` int(11) DEFAULT '0',
  `orthotics_comments` text,
  `muscle_tone_functional` int(11) DEFAULT '0',
  `arches_foot_structure` int(11) DEFAULT '0',
  `supination_foot_structure` int(11) DEFAULT '0',
  `pronation_foot_structure` int(11) DEFAULT '0',
  `dorsiflexion_active` text,
  `dorsiflexion_passive` text,
  `plantarflexion_active` text,
  `plantarflexion_passive` text,
  `greattoe_active` text,
  `greattoe_passive` text,
  `left_proprioception` int(11) DEFAULT '0',
  `right_proprioception` int(11) DEFAULT '0',
  `professional_id` int(11) DEFAULT '0',
  `last_update` varchar(255) DEFAULT '',
  `user_signature` text,
  `foot_comments` text,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `data_entry_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `professional_id` (`professional_id`),
  CONSTRAINT `footassess_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `footassess_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `foot_assessment_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `foot_assessment_ibfk_2` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `information_popup`
--

DROP TABLE IF EXISTS `information_popup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_popup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `description` text,
  `component_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_popup`
--

LOCK TABLES `information_popup` WRITE;
/*!40000 ALTER TABLE `information_popup` DISABLE KEYS */;
INSERT INTO `information_popup` VALUES (1,'','<b>Wound Edge</b>: wound margin immediately adjacent to the wound opening <br><br><b>Callused</b>: hyperkeratosis, thickened layer of epidermis <br><b>Diffuse</b>: not well defined, indistinct, difficult to clearly define wound outline <br><b>Demarcated</b>: well defined, distinct, easy to clearly define wound outline <br><b>Epithelialization</b>: new, pink to purple, shiny tissue <br><b>Rolled</b>: rolling under of epithelial wound edge in a cavity wound <br><b>Scarred</b>: fibrotic regenerated tissue following wound healing',18),(5,'','<b>Periwound Skin</b>: surrounding area immediately adjacent to the wound margin <br><br><b>Blister</b>: an elevation or separation of the epidermis containing fluid <br><b>Boggy</b>: soft, spongy <br><b>Bruised</b>: dark red purplish blue tissue that fades to yellow green grey depending on the skin colour <br><b>Denuded</b>: superficial skin loss <br><b>Dry</b>: flaky skin <br><b>Edema</b>: interstitial collect of fluid <br><b>Erythema</b>: redness of the skin-may be intense bright red to dark red or purple <br><b>Excoriated</b>: superficial loss of tissue <br><b>Indurated</b>: abnormal firmness of the tissues with palpable margins <br><b>Intact</b>: unbroken skin <br><b>Macerated</b>: wet, white <br><b>Rash</b>: temporary eruption on the skin-often raised, red, sometimes itchy <br><b>Tape tear</b>: tiny superficial skin loss due to tape <br><b>Weepy</b>: moist, draining areas <br><b>Warmth</b>: increased warmth in tissue than skin in adjacent area',19),(6,'','<b>Odour</b>: Unpleasant smell noted after cleansing',17),(7,'','<b>Exudate</b>: Wound drainage  <br><br><b>Amount</b>: consider amount in relationship to the wound size\r\n',16),(18,'','Intermittent Claudication: Characterized by pain, cramping, burning and aching, caused by insufficient blood flow to the limbs with ambulation.',229),(24,'','<b>Wound Bed</b>: the base of the wound, comprised of any combination of the following options<br><br><b>Adipose</b>: layer of yellow globular tissue where fat is stored<br><b>Blister</b>: an elevation or separation of the epidermis tissue containing fluid<br><b>Bone</b>: Hard, rigid white connective tissue<br><b>Eschar</b>: dry, black/brown, dead tissue<br><b>Fascia</b>: tough silvery white tissue found covering muscle or within a muscle group<br><b>Friable</b>: fragile tissue that may bleed easily<br><b>Fully Epithelialized</b>: covered completed with new epithelial tissue<br><b>Foreign body</b>: objects such as mesh, hardware, suture<br><b>Granulation tissue</b>: firm, red, moist, pebbled healthy tissue<br><b>Hematoma</b>: localized collection of blood<br><b>Hypergranulation</b>: (proud flesh) red, moist tissue raised above the level of the skin<br><b>Malignant/Fungating</b>: cancerous tissue<br><b>Muscle</b>: red, firm, striated tissue<br><b>New Tissue Damage</b>: new damage due to pressure or trauma on an open wound bed; presents as dark purple, deep red or grey coloured tissue<br><b>Non-granulation tissue</b>: moist, red (pale to bright) non-pebbled tissue<br><b>No open wound:</b> tissue damage noted but the skin is still intact<br><b>Not visible:</b> a portion or all of the open wound bed that cannot be visualized<br><b>Scab</b>: superficial, dry crust<br><b>Slough</b>: dry or wet, loose or firmly attached, yellow to brown dead tissue<br><b>Superficial, red</b>: clean, open area with non-measurable depth<br><b>Tendon</b>: shiny white cord of fibrous connective tissue that connects muscle to bone<br><b>Underlying Tissue Structures:</b> joints, organs, etc.<br><b>Weepy skin</b>: drainage but no obvious open areas noted',14),(27,'','<b>Pain</b>: Quantified on the Visual Analogue Scale where 0= no pain to 10= excruciating pain as described by the patient/client/resident',7),(28,'','<b>Size</b>: measurements of wound<br><b>Length</b>: edge to edge, the longest measurement of the wound\r\n',8),(29,'','<b>Size</b>: measurements of wound<br><b> Width</b>: from edge to edge, the widest measurement of the wound at right angles to the length',9),(30,'','<b>Depth</b>: the deepest vertical measurement from the base of the wound to the level of the skin.',10),(31,'','<b>Undermining</b>: a separation of tissue that occurs underneath the intact skin of the wound perimeter ',11),(32,'','<b>Sinus or Tunnel</b>: a channel that extends from any part of the wound and tracks into deeper tissue.',13),(33,'','<b>Fistula</b>: an abnormal track connecting an organ to the skin surface, wound bed or to another organ.',381),(34,'','<b>Date of Onset:</b> Best known date of when this wound started.',5),(35,'','<b>Active:</b> Wound is open<br><b>Closed</b> Wound is closed',2),(36,'','<b>Recurrent:</b>wound has occured before in this location',6),(37,'','<b>Pain Comments</b>: Type in any additional pain comments, if needed',380),(38,'','<b>Type</b>: composition of exudate<br><b>Nil</b>: No drainage<br><b>Serous</b>: thin, clear<br><b>Sanguineous</b>: bloody<br><b> Green</b>: drainge with a green colour<br><b>Purulent</b>: thick, cloudy<br><b>Other</b>\r\n',15),(39,'','<b>Amount</b>: consider amount in relationship to the wound size<br><b>Nil</b><br><b>Small/scant</b><br><b>Moderate</b><br><b>Large/copious</b>\r\n',16),(40,'','<b>Odour</b>: Unpleasant smell noted after cleansing',17),(41,'','<b>Edge</b>: wound margin immediately adjacent to the wound opening<br><b>Callused</b>: hyperkeratosis, thickened layer of epidermis<br><b>Demarcated</b>: well defined, distinct, easy to clearly define wound outline<br><b>Diffuse</b>: not well defined, indistinct, difficult to clearly define wound outline<br><b>Epithelialized</b>: new, pink to purple, shiny tissue<br><b>Hypergranulation</b> :(proud flesh) red, moist tissue raised above the level of the skin<br><b>Rolled</b>: epithelial wound edge of a cavity wound which rolls under<br><b>Scarred</b>: fibrotic regenerated tissue following wound healing\r\n',18),(42,'','<b>Periwound skin</b>: surrounding area immediately adjacent to the wound margin<br><br><b>Blister</b>: an elevation or separation of the epidermis containing fluid<br><b>Boggy</b>: soft, spongy<br><b>Bruised</b>: dark red purplish blue tissue that fades to yellow green grey depending on the skin colour<br><b>Callused</b>: hyperkeratosis, thickened layer of epidermis<br><b>Dry</b>: flaky skin<br><b>Edema</b>: interstitial collect of fluid<br><b>Elevated Warmth</b>: increased warmth when compaired to skin in adjacent area<br><b>Erythema</b>: redness of the skin-may be intense bright red to dark red or purple<br><b>Excoriated/denuded</b>: superficial loss of tissue<br><b>Fragile</b>: skin that is at risk for breakdown<br><b>Indurated</b>: abnormal firmness of the tissues with palpable margins<br><b>Intact</b>: unbroken skin<br><b>Macerated</b>: wet, white<br><b>Rash</b>: temporary eruption on the skin-often raised, red, sometimes itchy<br><b>Tape tear</b>: tiny superficial skin loss due to tape<br><b>Weepy</b>: moist, draining areas\r\n',19),(43,'','<b>Dressing Change Frequency</b>: How often the dressing is changed',20),(44,'','<b>Nursing Visit Frequency</b>: How often the nurse visits to change the dressing',21),(45,'','<b>Missing Limb or Toes</b>: Check all that apply',227),(46,'','<b>Pain at Rest</b>: Pain experienced by patient while legs are at rest<br><b>Pain at Night</b>:Pain experienced in legs by patient at night<br><b>Decrease with Elevation</b>: Pain that decreases when legs are elevated<br><b>Increase with Elevation</b>: Pain that increases when legs are elevated<br><b>Ache</b>: Patient decrides pain in legs as an \"ache\"<br><b>Knife-Like</b>: Patient describes pain in legs as \"knife-like\"<br><b>Continuous</b>: Patient descrides pain in legs as continuous<br><b>Intermittent</b>: Patient describes pain in legs as intermittent<br><b>Intermittent Claudication</b>: Characterized by pain, cramping, burning and aching, caused by insufficient blood flow to the limbs with ambulation<br><b>With Deep Palpation</b>:Patient describes pain felt with deep palpation to legs<br><b>Non-verbal Response</b>:Patient unable to give verbal response to evaluator, please follow with a pain comment below<br><b>Pain Comment</b>: Free text box to document further pain comments',229),(47,'','<b>Normal/healthy</b>:Skin to legs has a normal and healthy appearance<br><b>Dry/Flakey</b>: Skin\'s appearance is dry and flakey to touch on the patients legs<br><b>Varicosities</b>: Dilated and distended veins that become progressively larger and painful<br><b>Hemosiderin Staining</b>: Venous hypertension of the lower leg causing leaking of red blood cells in surrounding tissue which over time presents as grayish, brown hyperpigmentation of the skin<br><b>Woody/Fibrous (Lipodermatosclerosis)</b>: Woody, fibrous hardening of the soft tissue that will often present as a \"champagne\" shaped lower leg<br><b>Stasis Dematitis</b>: Increased permeability of dermal capillaries from venous in\r\nsufficiency that cause an inflammatory reaction (eczema and edema) of the lower legs<br><b>Atrophie Blanche</b>: White, atrophic lesions/plaques associated with venous disease<br><b>Cellulitis</b>: Infection of the skin often characterized with local erythema, pain, swelling and elevated warmth<br><b>Hairless/Fragile/Shiny</b>: Leg has a fragile and shiny appearance with the abscence of hair<br><b>Dependent Rubor</b>: The lower limb turns red/blue as blood rushes into ischemic tissue. This occurs because the peripheral vessels are so severely damaged they are no longer able to constrict, but remain dilated<br><b>Blanching with Elevation</b>: Skin\'s pallor becomes pale or lighter in pigmentation when leg is elevated<br><b>Mottled/Irregular/Discolouration</b>:Skin on patients legs have an irregular surface (mottled) with areas of discolouration<br><b>Moist/Waxy</b>: Skin to legs are moist to the touch and have a waxy texture   \r\n',233),(48,'Braden Scale Risk Score Interventions','<br><b>Little to no risk</b>: 18 or greater<br><b>Low Risk:</b>15-18<br><b>Moderate Risk:</b>13-14\r\n<br><b>High Risk:</b>10-12<br><b>Very High Risk:</b>9 or less<br><br><b>MILD RISK (15-18) *</b><br>Frequent turning (e.g. q 2 hours)<br>Maximal remobilization<br>Pressure-reduction support surface if bed or chair-bound<br>Protect Heels A? Offload with Pillows<br>Manage Moisture<br>Manage Nutrition<br> Manage Friction and Shear<br>\r\n* If other major risk factors are present (advanced age, poor dietary intake of protein, diastolic pressure below 60, hemodynamic instability) advance to next level of risk<br><br><b>MODERATE RISK (13-14) *</b><br>Frequent turning with a planned schedule<br>Use foam wedges for 30 degree lateral positioning<br>Pressure-reduction support surface<br>Maximal remobilization<br>Protect Heels A? Offload with Pillows<br>Manage Moisture<br>Manage Nutrition<br> Manage Friction and Shear<br>\r\n* If other major risk factors are present, advanced to next level of risk<br><br><b>HIGH RISK (10-12)</b><br>Frequent turning with a planned schedule<br>Supplement with small shifts in position<br>Pressure-reduction support surface<br>Use foam wedges for 30 degree lateral positioning<br>Maximal remobilization<br>Protect Heels A? Offload with Pillows<br>Manage Moisture<br>Manage Nutrition<br> Manage Friction and Shear<br><br><b>VERY HIGH RISK (9 or below)</b><br>All of the above +<br>Use pressure-relieving surface if patient has intractable pain, or severe pain exacerbated by turning, or<br>Additional risk factors<br><b>*NO PRESSURE REDUCTION/RELIEF SURFACE IS A SUBSTITUTE FOR TURNING SCHEDULE</b><br>TURNING SCHEDULE REQUIRED REGARDLESS OF PRESSURE RELIEF SURFACE<br><br><b>MANAGE MOISTURE</b><br>Use commercial moisture barrier (Secura/Proshile)<br>Use absorbent pads or diapers that wick and hold moisture<br>Address cause if possible<br><br><b>MANAGE NUTRITION</b><br>Increase protein intake<br>Increase calorie intake to spare proteins<br>Supplement with Multi-vitamin<br>Offer liquid diet supplement<br>Offer glass of H20 with turning schedule<br>Consult Dietitian<br><br><b>MANAGE FRICTION AND SHEAR</b><br>Elevate head of bed no more than 30 degrees<br> Use Trapeze when indicated<br>Use lift sheet to move patient<br>Protect elbows and heels if being exposed to friction<br><br><b>OTHER GENERAL CARE ISSUES</b><br>No massage of reddened bony prominences<br> Maintain good hydration<br>Avoid drying the skin<br>Frequent turning and a planned schedule<br><br><b>CORRECT OR MANAGE ALL CO-MORBIDITIES SIMULTANEOUSLY WITH IMPLEMENTING INTERVENTIONS</b>',224),(49,'','<b>Lower Limb Assessment- basic (pain; skin condition; temperature; colour; edema location and severity; pulses by palpation)</b><br> To be done within two weeks of a wound being noted on a lower limb and every six months or with any significant change in the limb<br><b>Lower Limb Assessments- Ankle Brachial Pressure Index (ABPI\'s)</b><br> When compression is being considered, if pulses by palpations are diminished or absent, and every six months if using compression therapy',392),(50,'','<b>Approximated</b>: margin where the skin and stoma meet is well connected<br><b>Dissolvable Sutures</b>: a stitch that is made of a material that will dissolve with the body?s fluids and disappear<br><b>Fistula</b>: an abnormal track connecting an organ to the skin surface, wound bed, ostomy, or to another organ<br><b>Removable Sutures</b>: a stitch that is made of a material that will need to be removed at some point in time<br><b>Separated</b>: area of detachment(s) from the stoma to the skin<br><b>Suture Granuloma</b>: red, friable tissue and skin in the stoma margin where there are areas of retained or reactive suture material<br><b>Tenuous</b>: Thin or fragile connection between the skin and stoma<br><b>Undermining</b>: a separation of tissue that occurs underneath the intact skin of the stoma perimeter\r\n',456),(51,'','<b>Active:</b> Ostomy is active and functioning<br><b>Closed:</b> The ONLY time you close an ostomy profile is if the ostomy was surgically reversed (e.g. re-anastomosis). Do NOT close ostomy profiles as the patient will continue to be followed by the ostomy nurse, even if they are ?self care? \r\n',32),(52,'','<b>Anastomosis:</b> joining of two tubular organs, such as, ureter to ileal conduit<br><b>Resiting:</b> changing location of the stoma<br><b>Revision</b>: altering stoma within same location\r\n',33),(53,'','<b>(Double) Barrel</b>: an opening of the bowel resulting from a resection. The proximal and distal end are brought through the abdominal wall, everted (opened and turned inside out) and attached, creating two end stomas. The proximal will be functioning and the distal is the non-functioning stoma- also known as a mucous fistula.<br><b>End:</b> an opening of the bowel, in which the proximal end is brought through the abdominal wall, is everted and attached. The distal portion of bowel is either removed or is closed and remains inside the abdominal cavity.<br><b>Loop:</b> a loop of the bowel is brought through the abdominal wall, often supported with a rod or bridge on the skin. The anterior wall is opened; the proximal end will act as a fecal diversion, while the distal end will secrete mucous. They will remain connected via the posterior wall of the bowel.<br><b>Unmatured:</b> loop of bowel is brought through the abdominal wall, but is not surgically sutured to myocutaneous skin and relies on serousal inflammation to evert stoma\r\n',410),(54,'','<b>Diameter:</b> distance across a round stoma in millimeters',448),(55,'','<b>Length:</b> the longest measurement of the stoma',454),(56,'','<b>Width:</b> the longest measurement of the stoma at a right angle to the length',455),(57,'','<b>Irregular:</b> border of the stoma has irregular shaped edges<br><b>Oblong:</b> width is different from one end of the stoma to the other<br><b>Oval:</b> length is longer than width<br><b>Round:</b> length and width are the same\r\n',56),(58,'','<b>Flat:</b> stoma sits at the same level of the skin<br><b>Os flush:</b> opening of the bowel at skin level<br><b> Os off-center</b>: opening is not located in the center of the protruding bowel<br><b>Os tilted:</b> opening of the bowel is tilted<br><b>Prolapsed:</b> the telescoping of the bowel through the stoma<br><b>Raised:</b> stoma is sitting above level of the skin<br><b>Retracted:</b>disappearance of the normal stoma opening below skin level<br><b>Stenosis:</b> a narrowing of the stoma at level of the skin or fascia that impairs drainage of effluent<br> \r\n',57),(59,'','<b>Black (Necrosis):</b> ischemia of the stoma from inadequate blood supply<br><b> Dark Red</b>: Stoma has a deep/dark red hue<br><b>Dusky:</b> purple to a deep wine coloured hue from altered blood supply<br><b>Edematous:</b> interstitial collection of fluid<br><b>Friable:</b> fragile tissue that bleeds easily<br><b>Moist:</b> mucosal tissue is damp<br><b>Pale</b><br><b>Pink</b><br><b>Red</b><br><b>Slough</b>: dry or wet, loose or firmly attached, yellow to brown dead tissue<br><b>Turgor:</b> ability to change shape and return to normal appearance after (elasticity)<br><b>Trauma:</b> mucosal injury\r\n',59),(60,'','<b>Approximated:</b> margin where the skin and stoma meet is well connected<br><b>Dissolvable Sutures:</b> a stitch that is made of a material that will dissolve with the body?s fluids and disappear.<br><b>Fistula:</b> an abnormal track connecting an organ to the skin surface, wound bed, ostomy, or to another organ<br><b>Removable Sutures:</b> a stitch that is made of a material that will need to be removed at some point in time<br><b>Separated:</b> area of detachment(s) from the stoma to the skin<br><b>Suture Granuloma:</b> red, friable tissue and skin in the stoma margin where there are areas of retained or reactive suture material<br><b>Tenuous:</b> Thin or fragile connection between the skin and stoma<br><b>Undermining:</b> a separation of tissue that occurs underneath the intact skin of the stoma perimeter\r\n',457),(61,'','<b>Distended:</b> abnormal to patient, protruding of abdomen<br><b>Flat:</b> abdominal plane is flat<br><b>Hernia:</b> deficit in the fascia that allows loops of the intestine to protrude in areas of weakness. Can present as abnormal bumps on the abdomen<br><b>Loose/Wrinkly:</b> abdomen has folds of loose skin<br><b>Pendulous</b>: abdominal tissue that hangs loose<br><b>Rounded:</b> normal to patient, abdominal plane is rounded<br><b>Ribs</b><br><b>Soft:</b> abdomen is soft with palpation<br><b>Hard:</b> abdomen is firm or hard with palpation\r\n',60),(62,'','<b>Mucocutaneous Margin</b>: point where the epidermis and mucosa merge<br><br><b>Approximated:</b> margin where the skin and stoma meet is well connected<br><b>Dissolvable Sutures:</b> a stitch that is made of a material that will dissolve with the body\'s fluids and disappear.<br><b>Fistula:</b> an abnormal track connecting an organ to the skin surface, wound bed, ostomy, or to another organ<br><b>Fully Epithelialized:</b> covered completely with new epithelial tissue<br><b>Removable Sutures:</b> a stitch that is made of a material that will need to be removed at some point in time<br><b>Separated:</b> area of detachment(s) from the stoma to the skin<br><b>Suture Granuloma:</b> red, friable tissue and skin in the stoma margin where there are areas of retained or reactive suture material<br><b>Tenuous:</b> Thin or fragile connection between the skin and stoma',414),(63,'','<b>Allergic Reaction</b>: hypersensitivity to area where product was applied resulting in an inflammatory reaction<br><b>Boggy:</b> soft, spongy<br><b>Bruised:</b> dark red, purplish, blue tissue that fades to yellow, green, grey depending on the skin colour<br><b>Caput Medusae: (Peristomal Varices):</b> a purple hue caused by dilation of blood vessels noted around the stoma. Intermittent, spontaneous, or profuse bleeding may be noted by the patient and is often caused by portal hypertension.<br><b>Contact Dermatitis:</b> Hypersensitivity to area where product, stool or urine contacted skin resulting in an inflammatory reaction<br><b>Intact:</b> Skin around stoma is intact<br><b>Denuded:</b> superficial smooth loss of epithelium<br><b>Erythema:</b> redness of the skin-may be intense, bright red to dark red, or purple<br><b>Excoriated:</b> superficial loss of tissue that presents irregular with areas of erythema, rash, and denudation<br><b>Eczema:</b> superficial inflammation of the skin often causing red papules that itch and weep that can leave crusting and scaling<br><b>Folliculitis:</b> seen as red pustules and papules that are from bacterial inflammation of hair follicles<br><b>Fungal Rash:</b> overgrowth of fungal organisms that present as pustules on the skin. Satellite lesions (small red pustules) are often seen advancing from the edge of affected area.<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br><b>Intact:</b> unbroken skin<br><b>Macerated:</b> wet, white<br><b>Mucosal Transplant:</b> seeding of viable intestinal mucosa along suture line and onto peristomal skin.<br><b>Pseudo-verrucous lesions:</b> wart-like lesions from chronic moisture irritation around stoma<br><b>Psoriasis:</b> chronic disease characterized by proliferation of epidermis that often appears as a erythematic, thick, silvery-white, scaling plaque.<br><b>Pyoderma Gangrenosum:</b> ulcerative inflammatory skin condition of unknown etiology that starts as pustules and break open to form full thickness ulcers often with undermining, raged edges, and overhanging margins<br><b>Trauma:</b> loss of epidermis around stoma<br><b>Weepy:</b> moist, draining areas\r\n',46),(64,'','<b>Mucous Fistula:</b> distal end on the intestine that is brought through the intestinal wall, everted, and attached. Is considered ?non-functioning? but may still produce mucous',44),(65,'','<b>Nil</b><br><b>Small:</b> <20mls in 24 hours<br><b>Moderate:</b> 25-100mls in 24 hours<br><b>Large:</b> > 100mls in 24 hours<br><b>Clear</b><br><b>Mucous</b><br><b>Odorous</b><br><b>Fecal\r\n',411),(66,'','<b>Boggy:</b> soft, spongy<br><b>Erythema:</b> redness of the skin-may be intense bright red to dark red or purple<br><b>Excoriated:</b> superficial loss of tissue that presents irregular with areas of erythema, rash, and denudation<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br><b>Intact:</b> unbroken skin<br><b>Macerated:</b> wet, white<br><b>Rash:</b> temporary eruption on the skin-often raised, red, sometimes itchy<br><b>Weepy:</b> moist, draining areas',416),(67,'','<b>Nil</b>: no drainge<br><b>Red</b>: urine is red in colour<br><b>Amber</b>: urine is amber in colour<br><b>Yellow</b>: urine is yellow in colour<br><b>Pale Yellow</b>: urine is pale yellow',47),(68,'','<b>Bloody</b><br><b>Clear</b>: Normal to patient<br><b>Cloudy</b><br><b>Concentrated</b><br><b>Mucous</b><br><b>Odorous\r\n',48),(69,'','<b>Nil</b><br><b>Small</b><br><b>Moderate</b><br><b>Large\r\n',49),(70,'','<b>Nil</b><br><b>Small:</b> <200mls in 24 hours<br><b>Moderate:</b> < 500mls in 24 hours<br><b>Large:</b> <1200mls in 24 hours<br><b>High Output:</b> >1200mls in 24 hours\r\n',51),(71,'','<b>Bloody</b><br><b>Green</b> (dark, tenacious)<br><b>Brown<br><b>Yellow\r\n',53),(72,'','<b>Active:</b> Incision is active<br><b>Closed:</b> Incision has closed by primary intention or for other reasons of closure (see below)',69),(73,'','<b>Capillary Refill:</b> Length of time taken for normal skin colour to resume after pressure is applied. Normal time for a limb is less than 4 seconds',467),(74,'','<b>Edema</b>: Insterstital Collection of fliud<br><br><b>0 or None</b><br><b>+1 or Trace</b><br><b>+2 or Moderate</b><br><b>+3 or Deep</b><br><b>+4 or Very Deep</b><br><b>Homan\'s Sign (+ive):</b> Calf pain on dorsiflexion of the foot. This may be a sign of deep vein thrombosis, but it\'s absence does not rule it out<br><b>Stemmer\'s Sign (+ive):</b> Inability to tent (when pinched) edematous skin on the dorsal surface of a toe. This suggests lymphedema, but its absence does not rule it out',281),(75,'','<b>Sensation</b>: The use of a 5.07 monofilament to assess the foot for loss of protective sensation<br><br><b>Proprioception:</b> perception from nerve endings that provides the body with information concerning movement and position',295),(76,'','<b>Foot Deformities</b><br><br><b>Bunion:</b> Misaligned great toe joint that becomes tender and swollen with the tip angling towards the other toes.<br><b>Callus:</b> Usually seen on the plantar aspect of the foot, it is the thickening of epidermis at a site of external pressure and friction<br><b>Corn:</b> Usually seen on the toes, it is the thickening of epidermis at a site of external pressure and friction. May be painful.<br><b>Planter Wart:</b> Small circular lesion caused by a virus that presents as a small black dot surrounded by a callus, located on the planter surface of the foot<br><b>Dropped Metatarsal Head:</b> Planter foot deformity often seen with peripheral neuropathy and associated with atrophied fat pads, calluses, and high-risk areas for ulcer and infection.<br><b>Hammer (claw) toe:</b> Contraction of a toe joint from tightened ligaments and tendons that causes the joint and toe to curl downward.<br><b>Acute Charcot:</b> Progressive, degenerative disease of the foot joints, characterized by edema, pain, hemorrhage, heat, bone deformity, bone fragmentation, and joint instability. Treatment is critical in this phase.<br><b>Chronic Charcot:</b> Reconstruction and healing of foot joints and bones after acute charcot treated. Remodeling and fusion of damaged structures lead to decreased joint mobility and rounding of large bone fragments.<br><b>Crossed Toe:</b> Toe or Toes cross each other',328),(77,'','<b>Active</b>: Drain is active, still in use<br><b>Closed</b>: Drain is no longer in use, it is discontinued/removed',428),(78,'','<b>Intact</b>: Skin at drain site is intact<br><b>Eroded</b>: Skin at drain site is eroded, tissue loss<br><b>Hypergranulated</b>: (proud flesh) skin at drain site is red, moist tissue raised above the level of the skin',215),(79,'','<b>Additional Days</b>: If you are wishing to document previous drainage dates with drainage amounts and characteristics, choose the number of days and press \"OK\". Then fill in as needed ',433),(80,'','<b>Clear</b><br><b>Cloudy</b><br><b>Mucousy</b><br><b>Bile</b>: dark, tenacious<br><b>Odourous</b><br><b>Serous</b>: thin, clear<br><b>Sanguineous</b>: bloody<br><b>Yellow</b><br><b>Green</b><br><b>Purulent</b>: thick, cloudy',199),(81,'','<b>Elevated Warmth</b>: increased warmth when compared to skin in adjacent area<br><b>Erythema:</b> redness of the skin-may be intense bright red to dark red or purple<br><b>Excoriated</b>: superficial loss of tissue<br> <b>Fragile</b>: skin that is at risk for breakdown<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br>\r\n<b>Intact:</b> unbroken skin<br><b>Macerated</b>: wet, white<br><b>Rash:</b> temporary eruption on the skin-often raised, red, sometimes itchy',198),(82,'','<b>Nil</b><br><b>Serous:</b> thin, clear<br><b>Sanguineous:</b> bloody<br><b>Green</b><br><b>Brown</b><br><b>Purulent:</b> thick, cloudy<br><b>Other</b>\r\n',438),(83,'','<b>Post-op Management:</b> Check all that you assessed and are within normal parameter. A parameter checkbox left blank or if you check \"Not Assessed\" it means not assessed today. If you check \"Concerns Noted- See Progress Note\", it indicates that there is a concern with one or more of the parameters and a Progress Note will be done. If the parameter that is requiring a Progress Note is wound related, chart this in Pixalere (The PN is located on the Summary page).',459),(84,'','<b>Pain:</b> quantified on the Visual Analogue Scale where 0 = no pain and 10 = excruciating as described by the patient/client/resident',72),(85,'','<b>Pain Comments</b>: Type in any additional comments for pain assessment, if needed',378),(86,'','<b>In situ</b>: Remains unchanged<br><b>Removed</b>: Closure device is removed',210),(87,'','<b>Fully Epithelialized</b>: covered completely with new epithelial tissue<br><b>Approximated</b>: Edges of cut tissue are in contact to each other and healing by primary intention.<br><b>Tenuous:</b> Thin or fragile connection between the edges of cut tissue<br><b>Gaping</b>: Edges of cut tissue are not in contact with each other. If this occurs, reassess wound charting for wound assessment: healing by secondary intention.  \r\n',75),(88,'','<b>Type</b>: composition of exudate<br><br><b>Nil</b>: No drainage<br><b>Serous</b>: thin, clear<br><b>Sanguineous</b>: bloody<br><b> Green</b>: drainge with a green colour<br><b>Purulent</b>: thick, cloudy',76),(89,'','<b>Intact:</b> unbroken skin<br><b>Indurated:</b> abnormal firmness of the tissues with palpable margins<br><b>Erythema:</b> redness of the skin-may be intense bright red to dark red or purple<br><b>Edema:</b> interstitial collect of fluid<br><b>Blister:</b> an elevation or separation of the epidermis containing fluid<br><b>Rash:</b> temporary eruption on the skin-often raised, red, sometimes itchy',77),(90,'','<b>Undermining</b>: a separation of tissue that occurs underneath the intact skin of the wound perimeter ',12),(91,'','<b>Sinus or Tunnel</b>: a channel that extends from any part of the wound and tracks into deeper tissue.',141),(92,'','<b>Clinical Path</b>: Some sites have guidelines for post-operative outcomes and \"usual care\" for patients having certain procedures. If your site utilizes these guidelines, choose either \"on\" or \"off\" (client was on clinical path but has deviated from the listed outcomes or care). Choose \"N/A\" if you site does not utilize these guidelines.',386),(93,'','<b>Clinical Path</b>: Some sites have guidelines for post-operative outcomes and \"usual care\" for patients having certain procedures. If your site utilizes these guidelines, choose either \"on\" or \"off\" (client was on clinical path but has deviated from the listed outcomes or care). Choose \"N/A\" if you site does not utilize these guidelines.',426),(94,'','<b>Urostomy</b>: general term for urinary ostomy<br><b>Ileal Conduit</b>: portion of the ileum is segmented and brought forward through the abdomen creating a stoma. The ureters are then anastomosted to the ileal segment, which acts as a conduit to allow the passage of urine into a pouch or appliance the patient wears.<br><b>Continent Pouch- urinary</b>: general term for neobladder or continent cutaneous diversion<br><b>Neobladder (eg: Studer Pouch):</b> internal urinary reservoir or pouch is constructed from a portion of ileum or colon in which the ureters are anastomosted. The reservoir is then anastomosted to the urethra. After several weeks of healing, continent voiding will occur through the urethra either by intermittent catheterization and/or retained urethral sphincter control.<br><b>Continent Cutaneous Diversion (eg: Indiana Pouch):</b> internal urinary reservoir created from portion of the ileum and colon in which the ureters are anastomosted. A portion of the ileum is then brought forward through the abdominal, creating a stoma. After several weeks of healing, continent voiding will occur through the stoma when client performs catheterization at regular intervals.<br><b>Jejunostomy</b>: rarely performed, a portion of patientA-s jejunum is brought through the abdomen and everted to create a stoma. Can be permanent or temporary and allows the passage of stool into a pouch or appliance the patient wears.<br><b>Ileostomy</b>: portion of patientA-s ileum is brought through the abdomen and everted to create a stoma. Usually located on the abdominal right lower quadrant. Can be permanent or temporary and allows the passage of stool into a pouch or appliance the patient wears.<br><b>Cecostomy</b>: rarely performed, the patientA-s cecum is brought through the abdomen. Often a temporary measure to allow decompression of the colon.<br><b>Colostomy:</b>  portion of patientA-s colon is brought through the abdomen and everted to create a stoma. Can be permanent or temporary and allows the passage of stool into a pouch or appliance the patient wears.<br>-Ascending: Usually located on the abdominal right lower quadrant<br>-Transverse: May be located on either the abdominal right, mid, or left lower quadrant<br>-Descending: Usually located on the abdominal left lower quadrant<br>-Sigmoid: Usually located on the abdominal left lower quadrant\r\n',462),(95,'','<b>Alphas, Numbers, Tubes/Drains and\r\nOstomies</b>: These will generate an Assessment page unique to its designation<br><br><b> \"A, B, C, etc\"</b> : Used for charting Wounds<br><b>\"1, 2, 3, etc\"</b> : Used for charting Incisions<br><b> Number with Grey Line (tube)</b> : Used for charting Tubes/Drains<br><b>Yellow Circle</b> : Used for charting a Urostomy<br><b>Brown Circle</b> : Used for charting a Fecal Ostomy<br><br>Click on the A?woundAr type you wish to mark on the blue person. Click on the blue person to mark where that A?woundAr type is. If you want multiples of that A?woundAr type, continue clicking on the blue person to generate further icons. If you made a mistake, click on the icon (in reverse order) again to erase\r\nFor example: This shows one wound, one tube/drain, one incision, and one fecal ostomy<br><br><b>Four Pointed Arrow (move icon)</b>: Used to move an active (yellow) Alpha, Number, Tube/Drain or Ostomy. Click on the A?Move\" icon then click on the alpha, number, tube/ drain, ostomy icon that is yellow in colour- you will see it becomes bolded. Click (not drag) your mouse to the location you wish to move the icon to then you will see it move. Continue charting as usual. You change will be documented in the Summary automatically for you.<br><br>\r\nThe second line that contains short incision images are extra features you can use to enhance Blue Person. Used to draw Incisions for reference only (non-functional).<b>Must be associated with an already existing incison number (\"1\", \"2\", etc), otherwise, no assessment documentation page will be generated.</b>',98),(96,'Tester','',88),(97,'Review','Review',555),(98,'Review','Review',555),(99,'Test Allergies','tadsfasdfadsf',92),(100,'Test Allergies','tadsfasdfadsf',92);
/*!40000 ALTER TABLE `information_popup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library`
--

DROP TABLE IF EXISTS `library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `orderby` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library`
--

LOCK TABLES `library` WRITE;
/*!40000 ALTER TABLE `library` DISABLE KEYS */;
INSERT INTO `library` VALUES (1,'Pixalere User Reference Guide','1336546904.pdf',1,4),(2,'Pixalere Administration Guide','1336547020.pdf',1,4);
/*!40000 ALTER TABLE `library` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license`
--

DROP TABLE IF EXISTS `license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `license` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license`
--

LOCK TABLES `license` WRITE;
/*!40000 ALTER TABLE `license` DISABLE KEYS */;
INSERT INTO `license` VALUES (1,'-----BEGIN PGP MESSAGE-----\nVersion: BCPG v1.43\n\nowJ4nJvAy8zAxHhA5Eru160fbzOeXp0knpOZnJpXnOqWmZPql5ibquuZnpdflJoS\nuMKjW1lXVwEqrZAGlOdSdivKVPBNrFQwNlQwMLIyNbQyNVFwdQlRMDIwNOZKKUrM\nzLM15MorzY1PS0zOzMksyUwttjU1AAKu/OKS/NxKoGxZYk5mSnxpXklmji1Qm4W+\ngSEQgTWVFqcWQZVn5iVnFmfmg4wrzy/NSwHSnUxeLAyMTAwmrEwgx+nKBGRWJOak\nFqUqaPhAHemdWqmpYFNSlFiWWexQAJXWS87PtWPg4hSAebtopwDDpukqv3coT201\n557O8MNGWyDvQ5m8idp5pjke7zh4f2+Vs+bN3be/accVpXe9zMExf8MKJjPvE3c7\n/3SbplNT88rZDhJ2Szf3STOqxXrOagiTtLmaGcaS9PfxFSUJboscy+j5izq7Jt22\nyq9RCDF6r8wnGJHR+ebtJI8tye76FeveLlQy6uHfdlZ6b17Uu+B41Tm1h+6Vvz1p\nVTmlemv0kXRP/rcf1JqMElfsip68vynihMbZu4eO3Lq5qKRz6+6F1ZKqsxUtP7nY\n1x3cyTHN8tan5BZBJxvPD+ePXePdzPtji2Ztxbr5y2bUlK+ffVRcQGjJM+ljWXat\nnDd4J3VcTjx379yHFTZu2Rb3y7uUZOzfSk73dbBg3nUyS2Tr9rMNm6TOaP0o1d5Q\nXusyRalsx6Pff+ze+5uVfxQXKUrh/M1ndn1F7YfVv7jCZguV19aePRvyXdzkwwX+\na68CWsVsPLUdg570p9WtqbGRdZrXWjPxspLymv0GO58VnJO+oJl82DOfZaNycU5u\n1+ljgauq/8VfPdpvEf3m1hfBLYu6N36x256388Csv14fNd7onPm0L3em1Zxd0o8a\nD6klXn29OVtn+rf4Reu6WT5c8j+47H/cp2e1M4yXrNZ7tN7vI+umR+nbNdwCbA9Y\nXigJfGI1teTTNb5Pz+yinkcLefku7dlxlkv03Mr0ZcxPJl9f6ROcm31mQtc1gSNe\nX8x3AgBLu1gF\n=N0Tf\n-----END PGP MESSAGE-----');
/*!40000 ALTER TABLE `license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `limb_assessment`
--

DROP TABLE IF EXISTS `limb_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `limb_assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) DEFAULT '0',
  `patient_id` int(11) DEFAULT '0',
  `limb_show` int(1) DEFAULT '0',
  `left_missing_limbs` text,
  `right_missing_limbs` text,
  `left_pain_assessment` text,
  `right_pain_assessment` text,
  `pain_comments` text,
  `left_skin_assessment` text,
  `right_skin_assessment` text,
  `left_temperature_leg` int(11) DEFAULT '0',
  `right_temperature_leg` int(11) DEFAULT '0',
  `left_temperature_foot` int(11) DEFAULT '0',
  `right_temperature_foot` int(11) DEFAULT '0',
  `left_temperature_toes` int(11) DEFAULT '0',
  `right_temperature_toes` int(11) DEFAULT '0',
  `left_skin_colour_leg` int(11) DEFAULT '0',
  `right_skin_colour_leg` int(11) DEFAULT '0',
  `left_skin_colour_foot` int(11) DEFAULT '0',
  `right_skin_colour_foot` int(11) DEFAULT '0',
  `left_skin_colour_toes` int(11) DEFAULT '0',
  `right_skin_colour_toes` int(11) DEFAULT '0',
  `left_less_capillary` int(11) DEFAULT '0',
  `right_less_capillary` int(11) DEFAULT '0',
  `left_more_capillary` int(11) DEFAULT '0',
  `right_more_capillary` int(11) DEFAULT '0',
  `left_dorsalis_pedis_palpation` int(11) DEFAULT '0',
  `right_dorsalis_pedis_palpation` int(11) DEFAULT '0',
  `left_posterior_tibial_palpation` int(11) DEFAULT '0',
  `right_posterior_tibial_palpation` int(11) DEFAULT '0',
  `doppler_lab` int(11) DEFAULT '0',
  `left_dorsalis_pedis_doppler` text,
  `right_dorsalis_pedis_doppler` text,
  `left_posterior_tibial_doppler` text,
  `right_posterior_tibial_doppler` text,
  `left_interdigitial_doppler` text,
  `right_interdigitial_doppler` text,
  `ankle_brachial_lab` int(11) DEFAULT '0',
  `ankle_brachial_date` text,
  `left_tibial_pedis_ankle_brachial` int(11) DEFAULT NULL,
  `right_tibial_pedis_ankle_brachial` int(11) DEFAULT NULL,
  `left_dorsalis_pedis_ankle_brachial` int(11) DEFAULT NULL,
  `right_dorsalis_pedis_ankle_brachial` int(11) DEFAULT NULL,
  `left_ankle_brachial` int(11) DEFAULT NULL,
  `right_ankle_brachial` int(11) DEFAULT NULL,
  `toe_brachial_lab` int(11) DEFAULT '0',
  `toe_brachial_date` text,
  `left_toe_pressure` text,
  `right_toe_pressure` text,
  `left_brachial_pressure` text,
  `right_brachial_pressure` text,
  `transcutaneous_lab` int(11) DEFAULT '0',
  `transcutaneous_date` text,
  `left_transcutaneous_pressure` int(11) DEFAULT '0',
  `right_transcutaneous_pressure` int(11) DEFAULT '0',
  `left_edema_severity` int(11) DEFAULT '0',
  `right_edema_severity` int(11) DEFAULT '0',
  `left_edema_location` int(11) DEFAULT '0',
  `right_edema_location` int(11) DEFAULT '0',
  `sleep_positions` text,
  `left_ankle_cm` int(11) DEFAULT NULL,
  `left_ankle_mm` int(11) DEFAULT NULL,
  `right_ankle_cm` int(11) DEFAULT NULL,
  `right_ankle_mm` int(11) DEFAULT NULL,
  `left_midcalf_cm` int(11) DEFAULT NULL,
  `left_midcalf_mm` int(11) DEFAULT NULL,
  `right_midcalf_cm` int(11) DEFAULT NULL,
  `right_midcalf_mm` int(11) DEFAULT NULL,
  `left_sensation` text,
  `right_sensation` text,
  `left_score_sensation` text,
  `right_score_sensation` text,
  `left_sensory` text,
  `right_sensory` text,
  `user_signature` text,
  `last_update` varchar(255) DEFAULT '',
  `professional_id` int(11) DEFAULT '0',
  `limb_comments` text,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `data_entry_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `professional_id` (`professional_id`),
  CONSTRAINT `limbassess_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `limbassess_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `limb_assessment_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `limb_assessment_ibfk_2` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `location_images`
--

DROP TABLE IF EXISTS `location_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderby` int(4) DEFAULT NULL,
  `location` text,
  `wound_name` text,
  `wound_view` text,
  `gender` text,
  `limb` int(1) DEFAULT NULL,
  `lower_limb` int(1) DEFAULT NULL,
  `foot` int(1) DEFAULT NULL,
  `filename_full` text,
  `filename_left` text,
  `filename_midline` text,
  `filename_right` text,
  `blocked_boxes` text,
  `woundcare` int(1) DEFAULT NULL,
  `stoma` int(1) DEFAULT NULL,
  `incision` int(1) DEFAULT NULL,
  `drain` int(1) DEFAULT NULL,
  `slices` text,
  `burn` int(11) DEFAULT NULL,
  `rows` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_images`
--

LOCK TABLES `location_images` WRITE;
/*!40000 ALTER TABLE `location_images` DISABLE KEYS */;
INSERT INTO `location_images` VALUES (1,1,'Head and neck','Head/neck (Anterior)','A','F',0,0,0,'f_ant_headneck.jpg','','','','F:C1,C2,C3,C4,C5,C13,C14,C15,C16,6,11,12,28,124,134,140,150,156,166,182,183,172,188',1,0,1,1,NULL,0,12),(2,1,'Head and neck','Head/neck (Posterior)','P','F',0,0,0,'f_pos_headneck.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,11,12,21,28,37,44,117,124,133,140,149,156,165,181,182,172,187,188',1,0,1,1,NULL,0,12),(3,1,'Head and neck','Head/neck (Anterior)','A','M',0,0,0,'m_ant_headneck.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,12,21,28,117,124,133,140,149,156,165,181,182,172,188',1,0,1,1,NULL,0,12),(4,1,'Head and neck','Head/neck (Posterior)','P','M',0,0,0,'m_pos_headneck.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,12,21,28,37,44,117,124,133,140,149,156,165,181,172,188',1,0,1,1,NULL,0,12),(5,2,'Full front','Front','A','F',0,0,0,'f_ant_full_front.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,12,101,117,133,149,165,181',0,0,0,0,NULL,0,12),(6,2,'Full front','Front','A','M',0,0,0,'m_ant_full_front.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,11,12,101,108,117,124,133,140,149,156,165,172,181,188',0,0,0,0,NULL,0,12),(7,3,'Shoulder','Shoulder (Anterior)','A','F',1,0,0,'','f_ant_left_shoulder.jpg','','f_ant_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R2,R9,R10,R11,R12,38,41,42,118;R:C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,R1,R2,R9,R10,R11,R12,39,40,55,123',1,0,1,1,NULL,0,12),(8,3,'Shoulder','Shoulder (Posterior)','P','F',1,0,0,'','f_pos_left_shoulder.jpg','','f_pos_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C6,C7,C12,C13,C14,C15,C16,R1,R2,R8,R9,R10,R11,R12,40,107;R:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R2,R9,R10,R11,R12,42,102,118,119',1,0,1,1,NULL,0,12),(9,3,'Shoulder','Shoulder (Anterior)','A','M',1,0,0,'','m_ant_left_shoulder.jpg','','m_ant_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C6,C7,C13,C14,C15,C16,R1,R9,R10,R11,R12,27,28,44,104,120,121;R:C1,C2,C3,C4,C10,C11,C12,C13,C14,C15,C16,R1,R9,R10,R11,R12,21,22,23,37,105,120,121',1,0,1,1,NULL,0,12),(10,3,'Shoulder','Shoulder (Posterior)','P','M',1,0,0,'','m_pos_left_shoulder.jpg','','m_pos_right_shoulder.jpg','L:C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,R1,R8,R9,R10,R11,R12,23,107;R:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R9,R10,R11,R12,26,102,118,119',1,0,1,1,NULL,0,12),(11,4,'Arm','Arm (Anterior)','A','F',1,0,0,'','f_ant_left_arm.jpg','','f_ant_right_arm.jpg','L:R1,C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,26,27,42,43,58,59,75,91,103,107,119,123,135,151,152,167,168,183,184,185;R:R1,C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,22,23,27,38,39,54,55,70,71,75,86,91,102,106,107,122,123,138,139,153,154,155,168,169,170,171,183,184,185,186,187',1,0,1,1,NULL,0,12),(12,4,'Arm','Arm (Posterior)','P','F',1,0,0,'','f_pos_left_arm.jpg','','f_pos_right_arm.jpg','L:C1,C2,C3,C4,C10,C11,C12,C13,C14,C15,C16,R1,R2,37,38,39,53,54,69,70,85,101,105,117,121,137,152,153,167,168,169,183,184,185;R:C1,C2,C3,C4,C5,C6,C7,C8,C14,C15,C16,R1,R2,43,44,45,59,60,61,76,77,92,93,109,121,125,137,141,153,154,169,170,185,186,187',1,0,1,1,NULL,0,12),(13,4,'Arm','Arm (Anterior)','A','M',1,0,0,'','m_ant_left_arm.jpg','','m_ant_right_arm.jpg','L:R1,C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,25,26,27,42,43,58,59,75,89,91,103,107,119,123,135,139,151,152,167,168,183,184,185;R:R1,C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,22,23,27,38,39,43,54,55,59,70,71,75,86,91,102,106,107,118,122,123,134,138,139,153,154,155,168,169,170,171,183,184,185,186,187',1,0,1,1,NULL,0,12),(14,4,'Arm','Arm (Posterior)','P','M',1,0,0,'','m_pos_left_arm.jpg','','m_pos_right_arm.jpg','L:C1,C2,C3,C10,C11,C12,C13,C14,C15,C16,R1,R2,36,37,38,39,40,52,53,54,55,68,69,70,84,85,100,101,116,121,132,137,152,153,167,168,169,183,184,185;R:C1,C2,C3,C4,C5,C6,C7,C14,C15,C16,R1,R2,41,42,43,44,45,58,59,60,61,75,76,77,92,93,104,108,109,120,125,136,137,152,153,168,169,170,184,185,186,187',1,0,1,1,NULL,0,12),(15,5,'Hand','Hand','O','F',1,0,0,'','f_left_hand.jpg','','f_right_hand.jpg','L:1,2,3,4,5,6,7,12,13,14,15,16,17,18,19,20,21,22,23,29,30,31,32,33,34,37,38,39,46,47,48,113,122,123,129,130,139,145,146,155,156,157,158,159,160,161,162,163,171,172,173,174,175,176,177,178,179,180,181,185,186,187,188,189,190,191,192;R:1,2,3,4,5,6,10,11,12,13,14,15,16,17,18,19,20,27,28,29,30,31,32,33,34,35,43,44,119,128,144,145,146,147,148,149,150,151,159,160,161,162,163,164,165,166,167,174,175,176,177,178,179,180,181,182,183,189,190,191,192',1,0,1,1,NULL,0,12),(16,5,'Hand','Hand','O','M',1,0,0,'','m_left_hand.jpg','','m_right_hand.jpg','L:C1,2,3,4,5,6,7,8,12,13,14,15,16,18,19,20,21,22,23,29,30,31,32,34,35,36,37,38,39,47,48,50,51,52,53,54,55,66,67,113,130,142,146,147,156,157,158,159,160,162,163,171,172,173,174,175,176,178,179,180,187,188,189,190,191,192;R:1,2,3,4,5,12,13,14,15,16,17,18,19,20,27,28,29,30,31,32,33,34,42,43,44,45,46,47,48,59,60,61,62,63,144,145,146,147,148,149,159,160,161,162,163,164,165,166,174,175,176,177,178,179,180,181,182,189,190,191,192',1,0,1,1,NULL,0,12),(17,6,'Chest','Chest','A','F',0,0,0,'f_ant_chest.jpg','','','','F:C1,C16,2,3,4,14,15,18,19,31,34,162,178',1,0,1,1,NULL,0,12),(18,6,'Chest','Chest','A','M',0,0,0,'m_ant_chest.jpg','','','','F:1,2,16,17,33,97,113,129,145,160,161,176,177,192',1,0,1,1,NULL,0,12),(19,7,'Abdomen','Abdomen','A','F',0,0,0,'f_ant_abdomen.jpg','','','','F:C1,C16,2,15,18,31,34,47,50,63,66,79,82,95,98,111,114,127,143',1,1,1,1,NULL,0,12),(20,7,'Abdomen','Abdomen','A','M',0,0,0,'m_ant_abdomen.jpg','','','','F:C1,C16,18,34,47,50,63,66,79,82,95,98,111,114,127,130,143,146,159,162,178',1,1,1,1,NULL,0,12),(21,8,'Flank','Flank','L','F',0,0,0,'','f_lat_left_flank.jpg','','f_lat_right_flank.jpg','L:C1,C2,C3,C4,C5,C6,C7,C13,C14,C15,C16,R1,R11,R12,24,25,26,40,41,56,60,76,92,107,108,123,124,136,140,152,153,156;R:C1,C2,C3,C4,C10,C11,C12,C13,C14,C15,C16,R1,R11,R12,24,25,41,53,69,85,101,117,118,133,134,149',1,0,1,1,NULL,0,12),(22,8,'Flank','Flank','L','M',0,0,0,'','m_lat_left_flank.jpg','','m_lat_right_flank.jpg','L:C1,C2,C3,C4,C5,C6,C7,C12,C13,C14,C15,C16,R1,R2,R11,R12,40,107,123,136,139,152,153,155;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R1,R2,R10,R11,R12,41,102,118,134,137',1,0,1,1,NULL,0,12),(23,9,'Full back','Back','P','F',0,0,0,'f_pos_full_back.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,6,11,12,85,101,117,124,133,140,149,165,181',1,1,1,1,NULL,0,12),(24,9,'Full back','Back','P','M',0,0,0,'m_pos_full_back.jpg','','','','F:C1,C2,C3,C4,C13,C14,C15,C16,5,12,37,101,108,117,124,133,149',1,1,1,1,NULL,0,12),(25,10,'Upper back','Upper back','P','F',0,0,0,'f_pos_upper_back.jpg','','','','F:1,2,3,4,17,18,19,33,34,49,65,13,14,15,16,30,31,32,47,48,64,80,113,128,129,144,145,160,161,175,176,177,178,191,192',0,0,0,0,NULL,0,12),(26,10,'Upper back','Upper back','P','M',0,0,0,'m_pos_upper_back.jpg','','','','F:C1,C16,2,15,162,175,178,191',0,0,0,0,NULL,0,12),(27,11,'Lower back','Lower back','P','F',0,0,0,'f_pos_lower_back.jpg','','','','F:C1,C16,34,47,50,63,66,79,82,95,98,111,114,127,130,143',0,0,0,0,NULL,0,12),(28,11,'Lower back','Lower back','P','M',0,0,0,'m_pos_lower_back.jpg','','','','F:C1,C16,50,63,66,79,82,95,98,111,114,127,130',0,0,0,0,NULL,0,12),(29,12,'Buttock','Buttock','P','F',0,0,0,'f_pos_buttock.jpg','','','','F:R1,R12,C1,C16,18,19,30,31,34,47,50,55,56,57,58,63,71,72,73,74,87,88,89,104,105,120',1,0,1,1,NULL,0,12),(30,12,'Buttock','Buttock','P','M',0,0,0,'m_pos_buttock.jpg','','','','F:R1,R12,C1,C16,18,31,55,56,57,71,72,73,87,88,89,104,105,120,162,175',1,0,1,1,NULL,0,12),(31,13,'Sacralcoccyx','Sacralcoccyx','P','F',0,0,0,'f_pos_sacralcoccyx.jpg','','','','F:C1,C2,C3,C4,C5,C6,C11,C12,C13,C14,C15,C16,R1,R2,R3,R10,R11,R12,119,135,106,122,138',1,0,1,1,NULL,0,12),(32,13,'Sacralcoccyx','Sacralcoccyx','P','M',0,0,0,'m_pos_sacralcoccyx.jpg','','','','F:C1,C2,C3,C4,C5,C11,C12,C13,C14,C15,C16,R1,R2,R3,R10,R11,R12,102,118,134,106,122,138',1,0,1,1,NULL,0,12),(33,23,'Full buttock (sx only)','Full buttock','P','F',0,0,0,'f_pos_full_buttock.jpg','','','','F:R1,R12,C1,C16,18,19,31,34,47,50,63,162,163,174,175',1,0,1,1,NULL,0,12),(34,23,'Full buttock (sx only)','Full buttock','P','M',0,0,0,'m_pos_full_buttock.jpg','','','','F:R1,R12,C1,C16,18,19,31,34,47,162,163,174,175',1,0,1,1,NULL,0,12),(35,15,'Hip','Hip','L','F',0,0,0,'','f_lat_left_hip.jpg','','f_lat_right_hip.jpg','L:C1,C2,C3,C4,C14,C15,C16,5,6,7,12,13,21,29,45,157,173,189;R:C1,C2,C3,C14,C15,C16,4,5,11,12,13,20,28,29,36,45,148,164,180',1,0,1,1,NULL,0,12),(36,15,'Hip','Hip','L','M',0,0,0,'','m_lat_left_hip.jpg','','m_lat_right_hip.jpg','L:C1,C2,C3,C4,C14,C15,C16,R1,21,22,23,28,29,37,38,45,53,157,172,173,188,189;R:C1,C2,C3,C14,C15,C16,R1,20,21,26,27,28,29,36,43,44,45,52,60,61,77,93,109,132,148,164,165,180,181',1,0,1,1,NULL,0,12),(37,16,'Pelvis','Pelvis (Anterior)','A','F',0,0,0,'f_ant_pelvis.jpg','','','','F:R8,R9,R10,R11,R12,1,16,17,80,81,82,94,95,96,97,98,99,109,110,111,112',1,0,1,1,NULL,0,12),(38,16,'Pelvis','Pelvis (Anterior)','A','M',0,0,0,'m_ant_pelvis.jpg','','','','F:R10,R11,R12,1,16,80,81,97,98,99,113,114,115,116,129,130,131,132,133,134,135,95,96,110,111,112,124,125,126,127,128,139,140,141,142,143,144,145,146',1,0,1,1,NULL,0,12),(39,17,'Perineum','Perineum','O','F',0,0,0,'f_perineum.jpg','','','','F:C1,C2,C3,C4,C5,C6,C11,C12,C13,C14,C15,C16,R1,R2,R3,R11,R12',1,0,1,1,NULL,0,12),(40,17,'Perineum','Perineum','O','M',0,0,0,'m_perineum.jpg','','','','F:C1,C2,C3,C4,C5,C6,C11,C12,C13,C14,C15,C16,R1,R2,R10,R11,R12,119,122,135,138',1,0,1,1,NULL,0,12),(41,24,'Full Leg Sx Only','Leg (Anterior)','A','F',1,1,0,'f_ant_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,8,9,10,25,41,54,57,70,86,73,89,102,105,121,137,153,166,169,182,185',1,0,1,1,NULL,0,12),(42,24,'Full Leg Sx Only','Leg (Posterior)','P','F',1,1,0,'f_pos_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C6,C12,C13,C14,C15,C16,8,9,10,25,41,57,73,89,105,121,137,153,169,185',1,0,1,1,NULL,0,12),(43,24,'Full Leg Sx Only','Leg (Anterior)','A','M',1,1,0,'m_ant_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,70,86,150,166,171,182,187',1,0,1,1,NULL,0,12),(44,24,'Full Leg Sx Only','Leg (Posterior)','P','M',1,1,0,'m_pos_full_leg.jpg','','','','F:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16,86,150,166,171,182,187',1,0,1,1,NULL,0,12),(45,19,'Upper leg','Upper leg (Anterior)','A','F',1,1,0,'','f_ant_left_upper_leg.jpg','','f_ant_right_upper_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,94,110,126,142,153,158,169,174,185,190;R:C1,C2,C9,C10,C11,C12,C13,C14,C15,C16,67,83,99,115,131,147,163,179',1,0,1,1,NULL,0,12),(46,19,'Upper leg','Upper leg (Posterior)','P','F',1,1,0,'','f_pos_left_upper_leg.jpg','','f_pos_right_upper_leg.jpg','L:C1,C2,C9,C10,C11,C12,C13,C14,C15,C16,51,67,83,99,115,131,147,163,179;R:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,94,110,126,142,158,174,190',1,0,1,1,NULL,0,12),(47,19,'Upper leg','Upper leg (Anterior)','A','M',1,1,0,'','m_ant_left_upper_leg.jpg','','m_ant_right_upper_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,105,121,137,142,153,158,169,174,185,190;R:C1,C2,C9,C10,C11,C12,C13,C14,C15,C16,99,115,131,147,163,179',1,0,1,1,NULL,0,12),(48,19,'Upper leg','Upper leg (Posterior)','P','M',1,1,0,'','m_pos_left_upper_leg.jpg','','m_pos_right_upper_leg.jpg','L:C1,C2,C3,C9,C10,C11,C12,C13,C14,C15,C16,116,132,148,164,180;R:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,62,78,94,110,126,142,158,174,190',1,0,1,1,NULL,0,12),(49,20,'Leg Above Knee Amp','Leg Above Knee Amp (Anterior)','A','F',1,1,0,'','f_ant_left_leg_amp_ak.jpg','','f_ant_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,73,89',1,0,1,1,NULL,0,12),(50,20,'Leg Above Knee Amp','Leg Above Knee Amp (Posterior)','P','F',1,1,0,'','f_pos_left_leg_amp_ak.jpg','','f_pos_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,70,86,;R:C1,C2,C3,C4,C5,C6,C7,C8,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,60,76,92',1,0,1,1,NULL,0,12),(51,20,'Leg Above Knee Amp','Leg Above Knee Amp (Anterior)','A','M',1,1,0,'','m_ant_left_leg_amp_ak.jpg','','m_ant_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,91;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,70,73,86,89',1,0,1,1,NULL,0,12),(52,20,'Leg Above Knee Amp','Leg Above Knee Amp (Posterior)','P','M',1,1,0,'','m_pos_left_leg_amp_ak.jpg','','m_pos_right_leg_amp_ak.jpg','L:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,70,73,86,89;R:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R7,R8,R9,R10,R11,R12,91',1,0,1,1,NULL,0,12),(53,21,'Leg Below Knee Amp','Leg Below Knee Amp (Anterior)','A','F',1,1,0,'','f_ant_left_leg_amp_bk.jpg','','f_ant_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R9,R10,R11,R12;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R9,R10,R11,R12,73,89,105,118,121',1,0,1,1,NULL,0,12),(54,21,'Leg Below Knee Amp','Leg Below Knee Amp (Posterior)','P','F',1,1,0,'','f_pos_left_leg_amp_bk.jpg','','f_pos_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C9,C10,C11,C12,C13,C14,C15,C16,R9,R10,R11,R12,70,86,102,118,121;R:C1,C2,C3,C4,C5,C6,C7,C8,C13,C14,C15,C16,R9,R10,R11,R12,76,92,108,121,124',1,0,1,1,NULL,0,12),(55,21,'Leg Below Knee Amp','Leg Below Knee Amp (Anterior)','A','M',1,1,0,'','m_ant_left_leg_amp_bk.jpg','','m_ant_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R8,R9,R10,R11,R12;R:C1,C2,C3,C4,C5,C10,C11,C12,C13,C14,C15,C16,R8,R9,R10,R11,R12,73,86,89,102,105,',1,0,1,1,NULL,0,12),(56,21,'Leg Below Knee Amp','Leg Below Knee Amp (Posterior)','P','M',1,1,0,'','m_pos_left_leg_amp_bk.jpg','','m_pos_right_leg_amp_bk.jpg','L:C1,C2,C3,C4,C5,C9,C10,C11,C12,C13,C14,C15,C16,R9,R10,R11,R12,70,86,102,118;R:C1,C2,C3,C4,C5,C6,C7,C8,C12,C13,C14,C15,C16,R9,R10,R11,R12',1,0,1,1,NULL,0,12),(57,22,'Lower leg','Lower leg (Anterior)','A','F',1,1,0,'','f_ant_left_lower_leg.jpg','','f_ant_right_lower_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C9,C15,C16,90,106,122,138,154,170,186;R:C1,C2,C8,C9,C10,C11,C12,C13,C14,C15,C16,103,119,135,151,167,183',0,0,0,0,NULL,0,12),(58,22,'Lower leg','Lower leg (Posterior)','P','F',1,1,0,'','f_pos_left_lower_leg.jpg','','f_pos_right_lower_leg.jpg','L:C1,C2,C8,C9,C10,C11,C12,C13,C14,C15,C16,3,131,147,151,163,167,179,183;R:C1,C2,C3,C4,C5,C6,C7,C8,C9,C15,C16,106,122,138,154,170,186',0,0,0,0,NULL,0,12),(59,22,'Lower leg','Lower leg (Anterior)','A','M',1,1,0,'','m_ant_left_lower_leg.jpg','','m_ant_right_lower_leg.jpg','L:C1,C2,C3,C4,C5,C6,C7,C8,C14,C15,C16,105,121,125,141,157,173,189;R:C1,C2,C3,C9,C10,C11,C12,C13,C14,C15,C16,104,120,132,136,148,164,180',0,0,0,0,NULL,0,12),(60,22,'Lower leg','Lower leg (Posterior)','P','M',1,1,0,'','m_pos_left_lower_leg.jpg','','m_pos_right_lower_leg.jpg','L:C1,C2,C3,C9,C10,C11,C12,C13,C14,C15,C16,148,164,180;R:C1,C2,C3,C4,C5,C6,C7,C8,C15,C16,94,105,110,121,126,137,141,142,153,157,158,169,173,174,185,189,190',0,0,0,0,NULL,0,12),(61,18,'Lower Legs','Lower Legs','O','F',1,1,0,'f_bil_lower_leg.jpg','','','','F:101,116,117,125,132,133,141,148,149,156,157,164,165,172,173,180,181,188,189',1,0,1,1,NULL,0,12),(62,18,'Lower Legs','Lower Legs','O','M',1,1,0,'m_bil_lower_leg.jpg','','','','F:129,136,137,144,145,152,153,160,161,168,169,176,177,184,185,192',1,0,1,1,NULL,0,12),(65,24,'Ankle and foot','Ankle/Foot','O','F',1,1,1,'','f_left_anklefoot.jpg','','f_right_anklefoot.jpg','L:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,37,38,39,40,41,42,43,44,48,53,54,55,56,57,58,59,60,71,72,73,74,75,76,88,104,113,114,115,116,117,118,119,129,130,131,132,145,146,147,148,161,162,163,164,165,166,177,178,179,180,181,182,183,184,185,186,187,188;R:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,38,39,40,41,42,43,44,45,54,55,56,57,58,59,60,70,71,72,73,74,75,89,90,105,106,123,124,125,126,127,128,141,142,143,144,156,157,158,159,160,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),(66,24,'Ankle and foot','Ankle/Foot','O','M',1,1,1,'','m_left_anklefoot.jpg','','m_right_anklefoot.jpg','L:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,37,38,39,40,41,42,43,44,48,53,54,55,56,57,58,59,60,71,72,73,74,75,76,88,104,113,114,115,116,117,118,119,129,130,131,132,145,146,147,148,161,162,163,164,165,166,177,178,179,180,181,182,183,184,185,186,187,188;R:R1,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,38,39,40,41,42,43,44,45,54,55,56,57,58,59,60,70,71,72,73,74,75,89,90,105,106,123,124,125,126,127,128,141,142,143,144,156,157,158,159,160,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),(67,25,'Foot with Toes Amp','Foot with Toes Amp','O','F',1,1,1,'','f_left_anklefoot_amp.jpg','','f_right_anklefoot_amp.jpg','L:R1,R2,37,38,39,40,41,42,43,44,45,46,47,48,53,54,55,56,57,58,59,60,61,62,63,64,71,72,73,74,75,76,87,88,113,114,115,116,117,118,129,130,131,132,133,145,146,147,148,149,161,162,163,164,165,166,177,178,179,180,181,182,183,184,185,186,187,188,189;R:R1,R2,33,34,35,36,37,38,39,40,41,42,43,44,53,54,55,56,57,58,59,70,71,72,73,74,75,90,91,106,107,124,125,126,127,128,141,142,143,144,156,157,158,159,160,171,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),(68,25,'Foot with Toes Amp.','Foot with Toes Amp','O','M',1,1,1,'','m_left_anklefoot_amp.jpg','','m_right_anklefoot_amp.jpg','L:R1,R2,37,38,39,40,41,42,43,44,45,46,47,48,53,54,55,56,57,58,59,60,61,62,63,64,71,72,73,74,75,76,87,88,113,114,115,116,117,118,129,130,131,132,133,145,146,147,148,149,161,162,163,164,165,166,167,177,178,179,180,181,182,183,184,185,186,187,188,189;R:R1,R2,33,34,35,36,37,38,39,40,41,42,43,44,49,53,54,55,56,57,58,59,70,71,72,73,74,75,90,91,106,107,123,124,125,126,127,128,140,141,142,143,144,156,157,158,159,160,171,172,173,174,175,176,181,182,183,184,185,186,187,188,189,190,191,192',1,0,1,1,NULL,0,12),(69,26,'Toes','Toes','O','F',1,1,1,'','f_left_toes.jpg','','f_right_toes.jpg','L:R1,R10,R11,R12,17,18,19,20,21,22,23,24,26,27,28,29,30,39,40,56,96,110,111,112,115,116,117,118,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144;R:R1,R10,R11,R12,19,20,21,22,23,26,27,28,29,30,31,32,42,43,57,58,65,81,82,97,98,99,113,114,115,116,124,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144',1,0,1,1,NULL,0,12),(70,26,'Toes','Toes','O','M',1,1,1,'','m_left_toes.jpg','','m_right_toes.jpg','L:R1,R10,R11,R12,17,18,19,20,21,22,23,24,26,27,28,29,30,39,40,56,96,110,111,112,115,116,117,118,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144;R:R1,R10,R11,R12,18,19,20,21,22,23,26,27,28,29,30,31,32,42,43,57,58,65,81,82,97,98,99,113,114,115,116,124,125,126,127,128,129,130,131,132,133,134,135,138,139,140,141,142,143,144',1,0,1,1,NULL,0,12),(71,21,'Knee','Knee (Anterior)','A','M',1,1,0,'','m_ant_left_knee.jpg','','m_ant_right_knee.jpg','L:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16;R:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16',1,0,1,1,NULL,0,12),(72,21,'Knee','Knee (Anterior)','A','F',1,1,0,'','f_ant_left_knee.jpg','','f_ant_right_knee.jpg','L:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16;R:C1,C2,C3,C4,C5,C12,C13,C14,C15,C16',1,0,1,1,NULL,0,12),(73,27,'Full body','Full body (Ant. & Post.)','O','M',0,0,0,'m_fullbody.jpg','','','','F:1,2,3,6,7,8,9,10,11,14,15,16,17,18,23,24,25,26,31,32,33,34,35,38,39,40,41,42,43,46,47,48,49,56,57,64,65,72,73,80,81,88,89,96,97,104,105,112,113,120,121,128,129,136,137,144,145,152,153,160,225,226,231,232,233,234,239,240,241,242,247,248,249,250,255,256,257,258,263,264,265,266,271,272,273,274,279,280,281,282,287,288,289,290,295,296,297,298,303,304,305,306,311,312,313,314,319,320,321,322,327,328,329,330,335,336,337,338,343,344,345,346,351,352,353,354,359,360,361,362,367,368,369,370,375,376,377,378,383,384',1,1,1,1,'fc(hd):4[],5[],20[],21[],36[],37[];el(hd):22[25],27[25];er(hd):19[25],30[25];sc(hd):12[],13[],28[],29[];nk(nk):44[],45[],52[],53[];ch(at):51[50],54[50],67[],68[],69[],70[],83[],84[],85[],86[],99[],100[],101[],102[];ab(at):115[],116[],117[],118[],131[],132[],133[],134[],147[],148[],149[],150[];bk(pt):59[],60[],61[],62[],75[],76[],77[],78[],91[],92[],93[],94[],107[],108[],109[],110[],123[],124[],125[],126[],139[],140[],141[],142[];bu(bu):157[],158[],173[],174[],155[],156[],171[],172[];pe(gn):164[],165[];ru(ru):50[],66[],82[],98[],114[],63[],79[],95[],111[],127[];lu(lu):55[],71[],87[],103[],119[],58[],74[],90[],106[],122[];ro(ro):130[],146[],162[],143[],159[],175[];lo(lo):135[],151[],167[],138[],154[],170[];rh(rh):161[00],177[],178[],193[],194[],209[00],210[00],176[],191[],192[],207[],208[],223[00],224[00];lh(lh):168[00],183[],184[],199[],200[],215[00],216[00],217[00],218[00],169[],185[],186[],201[],202[];rt(rt):163[],179[],180[],195[],196[],211[],212[],227[],228[],243[],244[],189[],190[],205[],206[],221[],222[],237[],238[],253[],254[];lt(lt):166[],181[],182[],197[],198[],213[],214[],229[],230[],245[],246[],187[],188[],203[],204[],219[],220[],235[],236[],251[],252[];rl(rl):259[],260[],275[],276[],291[],292[],307[],308[],323[],324[],269[],270[],285[],286[],301[],302[],317[],318[],333[],334[];ll(ll):261[],262[],277[],278[],293[],294[],309[],310[],325[],326[],267[],268[],283[],284[],299[],300[],315[],316[],331[],332[];rf(rf):339[],340[],355[],356[],371[],372[],349[],350[],365[],366[],381[],382[];lf(lf):341[],342[],357[],358[],373[],374[],347[],348[],363[],364[],379[],380[];',1,24),(74,27,'Full body','Full body (Ant. & Post.)','O','F',0,0,0,'f_fullbody.jpg','','','','F:1,2,3,6,7,8,9,10,11,14,15,16,17,18,23,24,25,26,31,32,33,34,39,40,41,42,43,47,48,49,56,57,64,65,72,73,80,81,88,89,96,97,104,105,112,113,120,121,128,129,136,137,144,153,160,225,226,231,232,233,234,239,240,241,242,247,248,249,250,255,256,257,258,263,264,265,266,271,272,273,274,279,280,281,282,287,288,289,290,295,296,297,298,303,304,305,306,311,312,313,314,319,320,321,322,327,328,329,330,335,336,337,338,343,344,345,346,351,352,353,354,359,360,361,362,367,368,369,370,375,376,377,378,383,384',1,1,1,1,'fc(hd):4[],5[],20[],21[],36[],37[];er(hd):19[25],30[25],35[00],46[00];el(hd):22[25],27[25],38[00];sc(hd):12[],13[],28[],29[];nk(nk):44[],45[],52[],53[];ch(at):51[50],54[50],67[],68[],69[],70[],83[],84[],85[],86[],99[],100[],101[],102[],115[],116[],117[],118[];ab(at):131[],132[],133[],134[],147[],148[],149[],150[];bk(pt):59[],60[],61[],62[],75[],76[],77[],78[],91[],92[],93[],94[],107[],108[],109[],110[],123[],124[],125[],126[],139[],140[],141[],142[];bu(bu):157[],158[],173[],174[],155[],156[],171[],172[];pe(gn):164[],165[];ru(ru):50[],66[],82[],98[],63[],79[],95[],111[];lu(lu):55[],71[],87[],103[],58[],74[],90[],106[];ro(ro):114[],130[],146[],127[],143[],159[];lo(lo):119[],135[],151[],152[00],122[],138[],154[];rh(rh):161[],162[],177[],178[],193[],194[],209[00],210[00],223[00],224[00],175[],176[],191[],192[],207[],208[];lh(lh):167[],168[],183[],184[],199[],200[],215[00],216[00],169[],170[],185[],186[],201[],202[],217[00],218[00];rt(rt):163[],179[],180[],195[],196[],211[],212[],227[],228[],243[],244[],189[],190[],205[],206[],221[],222[],237[],238[],253[],254[];lt(lt):166[],181[],182[],197[],198[],213[],214[],229[],230[],245[],246[],187[],188[],203[],204[],219[],220[],235[],236[],251[],252[];rl(rl):259[],260[],275[],276[],291[],292[],307[],308[],324[],269[],270[],285[],286[],301[],302[],317[],318[],333[],334[];ll(ll):261[],262[],277[],278[],293[],294[],309[],310[],325[],326[],267[],268[],283[],284[],299[],300[],315[],316[],331[],332[];rf(rf):340[],356[],371[00],372[],349[],365[],366[],381[],382[];lf(lf):341[],342[],357[],358[],373[],374[],347[],348[],363[],364[],379[],380[];',1,24);
/*!40000 ALTER TABLE `location_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs_access`
--

DROP TABLE IF EXISTS `logs_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs_access` (
  `time_stamp` int(11) DEFAULT NULL,
  `professional_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `professional_id` (`professional_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3149 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `lookup`
--

DROP TABLE IF EXISTS `lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `name` text,
  `other` int(11) DEFAULT NULL,
  `orderby` int(11) DEFAULT '1',
  `title` int(1) DEFAULT '0',
  `solobox` int(1) DEFAULT '0',
  `update_access` varchar(255) DEFAULT NULL,
  `report_value` text,
  `category_id` int(11) DEFAULT '0',
  `icd9` varchar(10) DEFAULT NULL,
  `icd10` varchar(10) DEFAULT NULL,
  `cpt` varchar(10) DEFAULT NULL,
  `locked` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3243 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lookup`
--

LOCK TABLES `lookup` WRITE;
/*!40000 ALTER TABLE `lookup` DISABLE KEYS */;
INSERT INTO `lookup` VALUES (1,1,1,'Work Save BC (WCB)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(2,1,1,'ICBC',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3,1,1,'Veterans Affairs Canada (DVA)',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(4,1,1,'Native Affairs',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(5,1,1,'Ministry of Human Resources (SS)',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(6,1,1,'Patient/Client/Resident',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(7,1,1,'Hospital/CHA/Facility',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(9,0,2,'Woundcare Clinician',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(10,1,2,'Physician Specialist',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(11,1,2,'GP',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(12,1,2,'Home Care Nurse',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(13,1,2,'Acute Hospital',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(14,1,2,'Rehab Hospital',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(15,1,2,'LTC Facility',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(16,1,2,'Home Support Agency',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(17,0,2,'Other',0,18,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(18,1,3,'Three Links',0,14,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(19,1,3,'GF Strong 2',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(20,1,3,'GF Strong 3',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(21,1,3,'GF Strong 4',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(22,1,3,'CHA1 3B',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(23,1,3,'CHA2 N',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(24,1,3,'CHA4 PS',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(25,1,3,'CHA5 RS',0,10,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(26,1,3,'CHA2 P',0,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(27,0,3,'CHA 3',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(28,1,3,'Vancouver Acute VGH',0,23,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(29,1,3,'Villa Carital',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(30,1,3,'Royal Ascot',0,16,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(31,1,3,'Royal Arch',0,17,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(32,1,3,'Yaletown',0,18,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(33,1,3,'SCI Wound Program',0,21,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(34,1,3,'Haro Park Center',0,19,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(35,1,3,'Louis Brier',0,20,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(36,1,4,'Interior Health',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(37,1,4,'Vancouver Coastal Health',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(38,1,4,'Vancouver Island Health',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(39,1,4,'Fraser Health',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(40,1,4,'Out of Province',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(41,1,4,'Northern Health',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(42,1,5,'Endocrine/Metabolic/Nutritional',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(43,0,41,'Diabetes mellitus',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(44,1,41,'Hyperthyroidism',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(45,1,41,'Hypothyroidism',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(46,1,5,'Heart/Circulation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(47,0,42,'Arteriosclerotic heart disease (ASHD)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(48,1,42,'Cardiac Dysrhythmia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(49,0,42,'Congestive heart failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(50,0,42,'Deep vein thrombosis',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(51,1,42,'Hypertension',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(52,0,42,'Peripheral vascular disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(53,0,42,'Other cardiovascular disease',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(54,1,5,'Muscoloskeletal',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(55,1,43,'Arthritis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(56,0,43,'Hip fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(57,0,43,'Missing limb (e.g. amputation)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(58,1,43,'Osteoporosis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(59,0,43,'Pathological bone fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(60,1,5,'Nuerological',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(61,0,44,'Amyotrophic lateral sclerosis (ALS)',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(62,0,44,'Alzheimers disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(63,1,44,'Aphasia',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(64,0,44,'Cerebral palsy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(65,0,44,'Cerebrovascular accident (stroke)',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(66,0,44,'Dementia other then Alzheimers disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(67,0,44,'Hemiplegia/hemiparesis',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(68,0,44,'Huntingtons chorea',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(69,0,44,'Multiple sclerosis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(70,1,44,'Paraplegia',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(71,0,44,'Parkinsons disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(72,1,44,'Quadriplegia',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(73,0,44,'Seizure disorder',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(74,0,44,'Transient ischemic attack (TIA)',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(75,0,44,'Traumatic brain injury',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(76,1,5,'Psychiatric/Mood',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(77,0,45,'Anxiety disorder',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(78,1,45,'Depression',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(79,0,45,'Manic depressive (bipolar disease)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(80,1,45,'Schizophrenia',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(81,1,5,'Pulmonary',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(82,1,46,'Asthma',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(83,1,46,'Emphysema/COPD',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(84,1,5,'Sensory',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(85,1,47,'Cataracts',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(86,0,47,'Diabetic retinopathy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(87,1,47,'Glaucoma',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(88,0,47,'Macular degeneration',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(89,1,5,'Infections',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(90,0,48,'MRSA',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(91,0,48,'VRE',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(92,1,5,'Other',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(93,1,48,'Osteomyelitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(94,1,48,'HIV',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(95,1,48,'Pneumonia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(96,1,48,'SARS',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(97,1,48,'Septicemia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(98,0,48,'STD',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(99,1,48,'Active TB',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(100,0,48,'UTI (last 30 days)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(101,1,48,'Viral Hepatitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(102,0,48,'Other',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(103,0,49,'Allergies',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(104,1,49,'Anemia',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(105,1,49,'Cancer',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(106,0,49,'Gastrointestinal disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(107,0,49,'Liver disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(108,0,49,'Renal failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(109,0,49,'Immunodeficiency',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(110,0,49,'Edema/lymphedema',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(111,0,49,'Immobility',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(112,0,49,'Loss of sensation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(113,0,49,'None of Above',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(114,0,6,'N/A',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(115,0,6,'Poor nutrition',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(116,0,6,'Anticoagulants',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(117,0,6,'Poor perfusion',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(118,0,6,'Chemotherapy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(119,0,6,'Wound infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(120,0,6,'Diabetes',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(121,0,6,'Smoking',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(122,0,6,'Gold/Methotrexate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(123,0,6,'Immobility',0,13,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(124,0,6,'NSAIDS',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(125,0,6,'Elevated B.P.',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(126,0,6,'Steroids',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(127,0,6,'Frail skin',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(128,0,6,'Uncontrolled Blood           Sugar',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(129,0,6,'Chemical Dependency',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(130,1,7,'Male',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(131,1,7,'Female',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,1),(132,1,7,'Other',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(133,0,8,'Neuropathic',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(134,1,8,'Arterial',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(135,1,8,'Malignant',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(136,0,8,'Surgical',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(137,0,8,'Venous',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(138,0,8,'Radiation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(139,1,8,'Trauma',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(140,0,8,'Inflammatory',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(141,0,8,'Burn',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(142,1,8,'Pressure Ulcer - Stage 1',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,1),(143,0,8,'Other',1,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(144,1,9,'2 weeks or less',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(145,0,9,'3-4 weeks',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(146,1,9,'4-6 weeks',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(147,1,9,'6-8 weeks',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(148,1,9,'3 months',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(149,1,9,'6 months',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(150,0,12,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(151,1,12,'2',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(152,1,12,'3',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(153,1,12,'4',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(154,1,12,'X',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(155,1,13,'Normal 34-50 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(156,1,13,'Mild 28- 33 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(157,1,13,'Moderate 21 - 27 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(158,1,13,'Severe < 21 g/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(159,1,14,'Normal 200 - 420 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(160,1,14,'Mild 160 - 199 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(161,1,14,'Moderate 105 - 159 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(162,1,14,'Severe < 105 mg/L',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(163,1,38,'MRSA',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(164,1,38,'Staph',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(165,1,38,'VRE',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(166,1,38,'E.coli',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(167,1,38,'Beta-hemolytic Strep',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(168,1,38,'Pseudomonas',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(169,1,38,'Other',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(170,0,39,'Oral',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(171,1,39,'Systemic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(172,1,39,'Topical',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(173,1,15,'Cellulitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(174,1,15,'Osteomyelitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(175,1,15,'Necrotizing fasciitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(176,1,15,'Deep wound infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(177,1,15,'Heavy colonization',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(178,1,16,'Biopsy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(179,1,16,'Bone Scan',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(180,1,16,'CT Scan',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(181,1,16,'X-Ray',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(182,1,16,'Sinogram',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(183,1,17,'Laser',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(184,1,17,'Massage',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(185,1,17,'Ultrasound',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(186,1,17,'Hyperbaric Oxygen',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(187,1,17,'Electro-stim',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(188,1,17,'Other',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(189,1,40,'25 mm/hg',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(190,1,40,'50 mm/hg',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(191,1,40,'75 mm/hg',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(192,1,40,'100 mm/hg',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(193,1,40,'125 mm/hg',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(194,1,40,'150 mm/hg',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(195,0,18,'P',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(196,0,18,'PK',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(197,0,18,'R',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(198,0,18,'H',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(199,0,18,'C',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(202,1,19,'Cold',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(203,1,19,'Cool',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(204,1,19,'Warm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(205,1,19,'Hot',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(206,0,20,'F',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(207,0,20,'A',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(208,0,20,'C',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(209,0,20,'K',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(210,0,20,'G',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(211,0,20,'NE',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(212,1,21,'+1 Trace',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(213,1,21,'+2 Moderate',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(214,1,21,'+3 Deep',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(215,1,21,'+4 Very Deep',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(216,0,22,'Y',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(217,0,22,'N',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(218,1,22,'Absent',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(219,0,23,'Y',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(220,0,23,'N',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(221,1,23,'Absent',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(222,1,24,'Nil',0,1,NULL,1,'0',NULL,0,NULL,NULL,NULL,1),(223,0,24,'Watery',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(224,0,24,'Thick',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(225,1,25,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,1),(226,0,25,'Wet/Moderate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(227,0,25,'Moist/Small',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(228,0,25,'Copious/Large',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(229,0,26,'Nil',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(230,0,26,'Foul',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(231,0,26,'Sweet',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(232,0,26,'Pungent',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(233,1,28,'TID',0,1,0,0,'0','21',0,NULL,NULL,NULL,0),(234,1,28,'BID',0,2,0,0,'0','14',0,NULL,NULL,NULL,0),(235,1,28,'QD/OD',0,3,0,0,'0','7',0,NULL,NULL,NULL,0),(236,1,28,'Q2 days',0,4,0,0,'0','4',0,NULL,NULL,NULL,0),(237,1,28,'Q3 days',0,5,0,0,'0','2',0,NULL,NULL,NULL,0),(238,1,28,'Q4 days',0,6,0,0,'0','2',0,NULL,NULL,NULL,0),(239,1,28,' 3x weekly',0,8,0,0,'0','3',0,NULL,NULL,NULL,0),(240,1,28,'2x weekly',0,9,0,0,'0','2',0,NULL,NULL,NULL,0),(241,1,28,'1x weekly',0,10,0,0,'0','1',0,NULL,NULL,NULL,0),(242,1,28,'other',0,14,0,0,'0','0',0,NULL,NULL,NULL,0),(243,1,29,'TID',0,1,NULL,0,'0','21',0,NULL,NULL,NULL,0),(244,1,29,'BID',0,1,NULL,0,'0','14',0,NULL,NULL,NULL,0),(245,1,29,'QD/OD',0,1,NULL,0,'0','7',0,NULL,NULL,NULL,0),(246,1,29,'Q2 days',0,1,NULL,0,'0','3.5',0,NULL,NULL,NULL,0),(247,1,29,'Q3 days',0,1,NULL,0,'0','2.33',0,NULL,NULL,NULL,0),(248,1,29,'Q4 days',0,1,NULL,0,'0','1.75',0,NULL,NULL,NULL,0),(249,1,29,'3x weekly',0,1,NULL,0,'0','3',0,NULL,NULL,NULL,0),(250,1,29,'2x weekly',0,1,NULL,0,'0','2',0,NULL,NULL,NULL,0),(251,1,29,'1x weekly',0,1,NULL,0,'0','1',0,NULL,NULL,NULL,0),(252,1,29,'other',0,1,NULL,0,'0','0',0,NULL,NULL,NULL,0),(253,0,30,'Paged Referral (within 2 hours)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(254,1,30,'Urgent (24 - 48hrs)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(255,1,30,'Clinical Review (1 - 7 days) ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(256,1,31,'Active',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(257,1,31,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(258,0,32,'Not Applicable',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(259,0,32,'Wound Healed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(260,0,32,'Patient Independant Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(261,0,32,'Patient Deceased',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(262,0,32,'Patient Moved',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(263,0,32,'Patient Transferred',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(265,0,34,'Attached',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(266,0,34,'Not Attached/Undermined',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(267,0,34,'Diffuse/Poorly Defined',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(268,0,34,'Demarcated/Punched Out',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(269,1,36,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(270,0,36,'Brown',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(271,0,36,'Red',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(272,0,36,'Yellow',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(273,1,36,'Green',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(274,0,37,'Healthy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(275,0,37,'Rash',0,9,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(276,0,37,'Erythema',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(277,0,37,'Painful',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(278,0,37,'Macerated',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(279,0,37,'Weepy',0,16,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(280,0,37,'Intact',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(281,0,37,'Itchy',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(282,0,37,'Indurated',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(283,1,10,'Patient inability to adhere to TX Plan',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(284,1,10,'Medication changes',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(285,1,10,'Unavailability of product(s)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(286,1,10,'Infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(287,1,10,'Limited social support',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(288,1,10,'Financial issues',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(289,1,10,'Delay in consultation',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(290,1,10,'Deterioration in physical condition',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(291,1,10,'Staffing limitations',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(292,1,10,'Physical and / or mental limitations',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(293,1,10,'Co-Morbidities',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(294,1,10,'Palliative',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(295,1,10,'Other',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(296,1,42,'Hypotension',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(297,1,48,'Clostridium Diff',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(298,0,1,'',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(299,0,1,'Palliative Care Benefits Program',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(300,1,3,'CHA2 P Clinic',0,13,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(301,0,25,'Wet/Moderate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(302,0,25,'Copious/Large',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(303,1,25,'Scant',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(304,0,25,'Moist/Small',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(305,1,25,'Moist/Small',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(306,1,25,'Wet/Moderate',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(307,1,25,'Copious/Large',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(308,1,34,'Rolled',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(309,0,34,'Calloused/Thickened',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(310,0,37,'Edematous',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(311,0,37,'Bruised',0,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(312,0,37,'Fibrotic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(313,0,37,'Blisters',0,13,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(314,0,37,'Calloused',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(315,0,37,'Fragile',0,14,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(316,0,37,'Psoriasis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(317,1,16,'WBC Scan',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(318,1,8,'Venous/Arterial',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(319,1,3,'CHA2 N Clinic',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(320,1,3,'CHA5 RS Clinic',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(321,1,3,'CHA1 3B Clinic',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(322,1,3,'CHA4 PS Clinic',0,9,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(323,0,3,'Parkgate',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(324,1,3,'NS HH Parkgate',0,23,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(325,1,3,'NS HH  West',0,24,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(326,1,3,'NS HH South',0,27,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(327,0,3,'Central Clinic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(328,1,3,'NS HH North',0,29,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(329,1,3,'NS HH Central Clinic',0,28,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(330,1,3,'NS HH West Clinic',0,25,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(331,1,3,'Lions Gate Hospital',0,30,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(332,1,3,'GPC Ward 2',0,30,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(333,1,3,'GPC Ward 3',0,31,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(334,1,3,'GPC Ward 4',0,32,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(335,1,3,'GPC Ward 5',0,33,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(336,1,3,'GPC Ward 6',0,34,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(337,1,3,'Minoru ',0,35,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(340,1,3,'Lions Manor',0,36,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(341,1,3,'Richmond Hospital',0,37,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(342,1,2,'Not Applicable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(343,1,3,'PR HCN Community',0,41,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(344,1,3,'Powell River Hospital',0,39,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(345,1,3,'PR Evergreen',0,43,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(346,1,3,'PR Olive Devaud',0,44,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(347,1,3,'PR HCN Clinic',0,42,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(348,1,3,'Powell River Hospital ACU',0,40,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(349,1,3,'Texada Island',0,45,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(350,0,1,'test_item',0,NULL,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(354,0,1,'test_item',0,NULL,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(355,0,11,'Acute',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(356,0,11,'Community',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(357,0,165,'Dougs Test Item',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(358,0,165,'Dougs Test Item2',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(359,0,165,'Dougs Test Item3',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(362,0,13,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(365,0,16,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(366,0,1,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(372,0,16,'q',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(377,0,39,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(378,0,10,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(379,1,12,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(380,0,8,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(381,0,7,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(382,0,6,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(383,0,49,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(384,0,48,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(385,0,41,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(386,0,4,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(390,0,43,'1`',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(392,0,55,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(393,1,55,'Walk-In',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(394,1,55,'On-Site',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(395,0,55,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(396,0,6,'Other',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(400,0,17,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(401,0,55,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(402,0,46,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(403,0,4,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(404,1,8,'Venous',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(405,1,18,'Pale',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(406,1,18,'Flesh tone',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(407,0,18,'Rubor',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(408,0,18,'Hemosiderin Staining',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(409,0,18,'Cyanotic',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(410,0,18,'Ischemia',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(411,0,18,'Woody/Fibrous',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(414,1,8,'Abscess',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(415,1,8,'Irradiation',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0),(416,0,8,'Inflammatory Disorder',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(417,1,8,'Skin Disease',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(418,1,9,'2-4 weeks',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(419,1,10,'Chronic Maintenance',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(420,0,11,'Acute',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(421,1,20,'Foot',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(422,1,20,'Up to Ankle',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(423,1,20,'Up to Midcalf',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(424,1,20,'Up to Knee',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(425,1,20,'Up to Groin',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(426,1,20,'No Edema',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(427,1,22,'Present',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(428,1,22,'Diminished',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(429,1,23,'Present',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(430,1,23,'Diminished',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(431,1,32,'Wound Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(432,0,32,'Patient Independent with Care',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(433,0,32,'Pt Selt Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(434,0,32,'Pt Self Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(435,1,32,'Patient Self Care',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(436,1,34,'Diffuse',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(437,1,34,'Demarcated',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(438,0,34,'Epithelialization',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(439,1,34,'Callused',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(440,1,34,'Scarred',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(441,1,34,'Epithelializing',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(442,0,36,'Grey',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(443,0,37,'Dry',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(444,0,37,'Indurated <2 cm',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(445,0,37,'Indurated >2 cm',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(446,0,37,'Warm',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(447,0,37,'Elevated Warmth',0,8,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(448,0,37,'Tape Tear',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(449,0,37,'Excoriated/Denuded',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(450,0,24,'Serous',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(451,0,24,'Sanguineous',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(452,0,24,'Purulent',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(453,0,24,'Other',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(463,0,36,'Serous',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(464,0,36,'Sanguineous',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(465,0,36,'Purulent',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(466,1,36,'Yellow',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(467,1,36,'Brown',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(468,0,24,'Thick',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(469,0,24,'Thin',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(470,0,24,'Watery/Thin',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(471,0,35,'test1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(472,1,35,'Bone',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0),(473,1,35,'Tendon',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0),(474,0,35,'Ligament',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0),(475,0,35,'Joint',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0),(476,1,35,'Underlying tissue structure(s)',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0),(477,1,35,'Muscle',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(478,1,35,'Fascia',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(479,1,35,'Adipose',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(480,1,35,'Eschar',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(481,1,35,'Slough',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(482,1,35,'Granulation tissue',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(483,1,35,'Non-granulation tissue',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(484,1,35,'Hypergranulation tissue',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(485,1,35,'Friable',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(486,1,35,'Blister',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(487,1,35,'Hematoma',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(488,0,35,'Non-blanchable(Stage 1)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(489,1,35,'Not visible',0,22,0,0,'0',NULL,0,NULL,NULL,NULL,0),(490,0,35,'Other',1,22,0,0,'0',NULL,0,NULL,NULL,NULL,0),(491,1,26,'No',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(492,1,26,'Yes',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(493,0,11,'residential care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(495,0,6,'Smoking',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(496,0,6,'Weight',0,3,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(497,0,6,'Immunocompromised',0,6,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(498,0,6,'Radiation Therapy',0,7,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(499,0,6,'Chemical Dependency',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(500,0,6,'Lifestyle Choices',0,9,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(501,0,6,'Altered Sensory Perception',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(502,0,6,'Moisture',0,11,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(503,0,6,'Inactivity',0,12,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(504,0,6,'Malnutrition',0,14,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(505,0,6,'Friction/Shear',0,15,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(506,0,6,'MRSA',0,16,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(507,0,6,'VRE',1,17,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(508,0,41,'Type I Diabetic',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(509,0,41,'Type II Diabetic',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(510,1,41,'Type I Diabetes Mellitus',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(511,1,41,'Type II Diabetes Mellitus',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(512,1,44,'Cerebral Palsy',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(513,1,41,'Other Endocrine/Metabolic/Nutritional Disorders',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(514,1,44,'Other Neurological Disorders',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(515,0,45,'Other Psychiatric Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(516,1,45,'Other Psychiatric/Mood Disorders',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(517,1,46,'Other Respitory Disorders',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(518,1,47,'Other Sensory Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(519,1,48,'Other Infectious Diseases',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(520,1,49,'Other Medical Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(521,0,43,'Other Muscoloskeletal Disorders',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(522,1,43,'Other Musculoskeletal Disorders',0,2,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(523,1,42,'Coronary Artery Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(524,0,16,'N/A',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(525,1,49,'Gastrointestinal Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(526,0,49,'Edema',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(527,1,49,'Lymphedema',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(528,1,49,'Renal Failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(529,1,49,'Liver Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(530,0,49,'Urinary Disease',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(531,1,48,'Necrotizing Fasciitis',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(532,1,48,'Gangrene',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(533,1,48,'Sexually Transmitted Infection',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(534,1,42,'Congestive Heart Failure',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(535,1,42,'Peripheral Vascular Disease',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(536,1,42,'Deep Vein Thrombosis',0,4,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(537,1,42,'Other Cardiovascular Disease',0,5,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(538,1,43,'Hip Fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(539,1,43,'Missing Limb (e.g. Amputation)',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(540,1,43,'Pathological Bone Fracture',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(541,1,44,'Alzheimers Disease',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(542,1,44,'Dementia other that Alzheimers Disease',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(543,1,44,'Multiple Sclerosis',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(544,1,44,'Parkinsons Disease',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(545,1,44,'Seizure Disorder',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(546,1,44,'Traumatic Brain Injury',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(547,1,44,'Cerebrovascular Accident (Stroke)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(548,1,44,'Transient Ischemic Attack (TIA)',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(549,1,44,'Hemiplegia/Hemiparesis',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(550,1,44,'Huntingtons Chorea',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(551,1,44,'Amyotrophic Lateral Sclerosis',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(552,1,45,'Anxiety Disorder',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(553,1,45,'Manic Depressive Disorder  (Bipolar)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(554,1,47,'Diabetic Retinopathy',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(555,1,47,'Macular Degeneration',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(556,1,48,'UTI (within the last 30 Days)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(557,1,2,'Wound Care Clinician',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(558,0,6,'VRE',0,18,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(559,0,1,'q',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(561,0,165,'Eduards Item',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(562,0,165,'Item 1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(563,0,165,'Item 2',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(564,0,165,'Item 3',1,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(565,0,6,'VRE',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(566,0,8,'Other',1,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(567,0,1,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(568,0,2,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(573,0,4,'Peace Arch unit',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(574,0,41,'1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(575,0,42,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(576,0,43,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(577,0,45,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(578,0,48,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(579,0,49,'111',1,19,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(580,0,6,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(581,0,165,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(582,0,7,'Unknown',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(583,0,8,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(584,0,9,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(585,0,10,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(586,0,11,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(587,0,12,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(588,0,13,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(589,0,14,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(590,0,38,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(591,0,39,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(592,0,16,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(593,0,17,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(594,0,18,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(597,1,54,'Title',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(598,0,37,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(599,0,34,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(600,0,32,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(601,0,30,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(602,0,29,'11',0,1,NULL,0,'0','0',0,NULL,NULL,NULL,0),(603,0,26,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(604,0,24,'11',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(612,1,51,'Training Location',0,5,0,0,'684','',990,NULL,NULL,NULL,0),(653,0,6,'Braden Risk Factors',0,2,1,0,'0',NULL,0,NULL,NULL,NULL,0),(654,0,6,'Chemical Dependency',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(655,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(656,0,6,'Smoking',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(657,0,6,'Weight',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(658,0,6,'Poor Perfusion',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(659,0,6,'Uncontrolled Blood Sugar',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(660,0,6,'Immumocompromised',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(661,0,6,'Radiation Therapy',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(662,0,6,'Chemical Dependecy',1,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(663,0,6,'Lifestyle Choices',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(664,0,6,'MRSA ',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(665,0,6,'VRE',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(666,0,6,'Braden Risk Factors',0,13,1,0,'0',NULL,0,NULL,NULL,NULL,0),(667,0,6,'Altered Sensory Perception',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(668,0,6,'Moisture',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(669,0,6,'Inactivity',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(670,0,6,'Immobility',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(671,0,6,'Malnutrition',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0),(672,0,6,'Friction/Shear',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0),(673,0,165,'Anticoagulants',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(674,0,165,'Vasoconstrictions',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(675,0,165,'Insulin',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(676,0,165,'Oral Hypoglycemics',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(677,0,165,'NSAIDS',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(678,0,165,'Steriods',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(679,0,165,'Gold',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(680,0,165,'Methotrexate',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(681,1,11,'Acute (e-Pro)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(682,0,11,'Community (PDC)',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(683,0,11,'Residential ',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(684,1,11,'Community',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(685,0,8,'Surgical (Secondary Intent',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(686,0,165,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(687,0,8,'Surgical(Secondary Intent)',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(688,1,24,'Serous',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(689,1,24,'Sanguineous',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(690,1,24,'Purulent',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(691,0,24,'Other ',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(692,1,36,'Red',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(693,1,36,'Pink',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(694,1,36,'Clear',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(695,1,36,'Grey',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(697,0,11,'111',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(698,0,49,'New',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(699,0,8,'Inflammatory Disorder',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(700,1,35,'Weepy skin',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(701,1,35,'Superficial, red',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(702,0,6,'Chemical Dependency',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(703,0,6,'Chemical Dependency',1,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(704,0,35,'Not Assessed',0,21,0,1,'0',NULL,0,NULL,NULL,NULL,0),(705,1,24,'Green',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(706,0,24,'Not Assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0),(707,0,6,'N/a',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(708,0,6,'Perception',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(709,0,6,'Altered Sensory Perception1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(710,0,14,'1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(714,0,6,'1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(715,0,6,'2',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(716,0,12,'XX',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(717,0,6,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(718,0,6,'test2',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(719,0,6,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(720,0,16,'2323',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(721,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(722,0,6,'Smoking',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(723,0,6,'Weigh',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(724,0,6,'Uncontrolled Blood Sugar',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(725,0,6,'Immunocompromised',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(726,0,6,'Radiation',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(727,0,6,'Chemical Dependency',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(728,0,6,'Lifestyle Choices',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(729,0,6,'Weight',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(730,0,6,'Chemical Depend',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(731,0,6,'Chemical Dependecy',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(732,0,6,'Braden Risk Factore',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(733,0,6,'Altered sensory perception',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(734,0,6,'Braden Risk Factors',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(735,1,6,'Radiation therapy',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(736,0,6,'Chemical Dependency',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(737,0,6,'Moisture',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(738,0,6,'Immobility',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(739,0,6,'Malnutrition',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0),(740,0,6,'Friction/Shear',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0),(741,0,165,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(742,0,34,'Not Assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0),(743,0,25,'Not Assessed',0,6,0,1,'0',NULL,0,NULL,NULL,NULL,0),(744,0,26,'Not Assessed',0,3,0,1,'0',NULL,0,NULL,NULL,NULL,0),(745,0,37,'Rash',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(746,1,37,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(747,1,37,'Dry',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(748,1,37,'Rash',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(749,1,37,'Macerated',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(750,0,37,'Erythema < 2cm ',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(751,0,37,'Indurated < 2cm',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(752,1,37,'Indurated 2cm and >',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(753,1,37,'Excoriated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(754,1,37,'Weepy',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(755,1,37,'Elevated Warmth',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(756,1,37,'Denuded',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(757,1,37,'Edema',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(758,1,37,'Boggy',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(759,1,37,'Blister',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(760,1,37,'Tape Tear',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(761,1,37,'Bruised',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(762,0,37,'Not Assessed',0,18,0,1,'0',NULL,0,NULL,NULL,NULL,0),(763,0,10,' 1',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(764,0,6,'New',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(765,0,6,'One more',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(767,0,11,'wqewq',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(768,0,7,'weq',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(769,0,42,'wqewqeqweqwe',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(770,0,165,'Vasoconstrictors',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(771,0,165,'Vasoconstrictors',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(772,1,165,'N/A',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0),(773,1,165,'Anticoagulants',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(774,1,165,'Insulin',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(775,1,165,'Oral Hypoglycemics',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(776,1,165,'Steroids',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(777,1,165,'NSAIDs',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(778,1,165,'Gold',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(779,1,165,'Immunosuppressants',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(780,1,165,'Vasoconstrictors',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(781,0,8,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(782,0,6,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(783,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(784,0,8,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(785,0,6,'Other',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(786,0,6,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(787,0,6,'Chemical Dependecny',1,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(788,1,32,'Wound Merged',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(789,0,37,'Erythema',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(790,1,6,'Chemical dependency',1,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(791,0,6,'Poor Perfusion',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(792,0,6,'MRSA',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(793,0,6,'VRE',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(794,0,6,'Inactivity',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(795,1,165,'Chemotherapy',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(796,0,1,'Ministry of Children & Family ',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(797,1,1,'First Nations and Inuit Health ',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(798,1,8,'Neuropathic/Diabetic',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(799,1,32,'Wound Split',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(800,1,2,'Self',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(801,1,8,'Burn',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(802,0,8,'Other',1,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(805,1,8,'Surgery (Secondary Intent)',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(810,0,6,'Non-adherence to treatment plan',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(811,1,18,'Red',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(812,0,8,'Abscess',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(813,1,6,'N/A',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0),(814,1,6,'Smoking',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(815,1,6,'Under weight',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(816,1,6,'Uncontrolled blood sugar',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(817,1,6,'Poor perfusion',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(818,1,6,'Immunocompromised',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(819,1,6,'Uncontrolled pain',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(820,1,6,'Non-adherence to treatment plan',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(821,1,6,'Life style choices',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(822,1,6,'MRSA positive',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(823,1,6,'VRE positive',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(824,0,6,'Braden Risk Factors',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(825,0,6,'Moisture',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(826,0,6,'Immobility',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(827,0,6,'Inactivity',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0),(828,0,6,'Friction/Shear',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0),(831,1,49,'Colostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(832,1,49,'Ileostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(833,1,49,'Urostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(852,1,8,'Inflammatory Disease',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(854,1,32,'Surgical Intervention',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(855,1,2,'Other HCPs',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(856,1,37,'Erythema < 2cm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(857,0,37,'Erythema 2cm or >',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(859,1,28,'M-W-F',0,11,0,0,'0','3',0,NULL,NULL,NULL,0),(860,1,29,'M-W-F',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(863,1,37,'Indurated < 2cm',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(864,1,35,'Fully epithelialized',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(874,1,35,'Malignant/fungating tissue',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(875,0,37,'Fragile',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(876,1,32,'Other',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(877,1,35,'Foreign body ',0,21,0,0,'0',NULL,0,NULL,NULL,NULL,0),(886,1,28,'Q5 days',0,7,0,0,'0','1',0,NULL,NULL,NULL,0),(887,1,29,'Q5 days',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(889,1,37,'Callused',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(890,1,35,'Scab',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(891,0,35,'Burn - Intact Skin ',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(898,1,41,'Morbidly Obese',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(899,0,34,'Hypergranulation',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(900,0,30,'FYI only',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(905,1,12,'SDTI',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(906,0,35,'Suspected Deep Tissue Injury (SDTI)',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(907,0,35,'Approximated (sx only)',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(908,0,8,'Surgery (Primary Intent)',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(200,1,50,'Training Area',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1000,1,60,'Incision',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1001,1,60,'Belt line',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1002,1,60,'Skin fold/crease',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1003,1,60,'Bony prominence',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1004,1,60,'Umbilicus',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1005,1,60,'Open Wound',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1006,1,166,'Rod',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1007,1,166,'Stent(s)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1008,1,166,'Catheter',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1009,1,166,'Bridge',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1010,1,148,'Urostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1011,1,148,'Ileostomy',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1012,1,148,'Descending Colostomy',0,14,0,0,'0','',0,NULL,NULL,NULL,0),(1013,1,148,'Sigmoid Colostomy',0,15,0,0,'0','',0,NULL,NULL,NULL,0),(1014,1,148,'Transverse Colostomy',0,13,0,0,'0','',0,NULL,NULL,NULL,0),(1015,1,63,'Nurse',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1016,1,63,'Family',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1017,1,68,'Viewed emptying pouch',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1018,1,77,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1019,1,77,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1020,1,76,'test',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1021,1,75,'Rounded',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1022,1,67,'Belt line',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1023,1,69,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1024,1,73,'Small',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1025,1,72,'Thick',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1026,1,66,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1027,1,70,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1028,1,71,'Small',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1029,1,61,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1030,1,80,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1031,1,80,'No',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1032,1,81,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1033,1,81,'No',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1034,1,82,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1035,1,82,'No',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1036,1,83,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1037,1,83,'No',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1038,1,84,'Loop',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1039,1,84,'End',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1040,1,84,'Barrel',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1041,1,65,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1042,1,65,'Changed - teaching',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1043,1,65,'Changed - routine',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1044,1,89,'Knowledge deficit',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1045,1,89,'Cognitive impairment',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1046,1,89,'Visual impairment',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1047,1,89,'Hearing impairment',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1048,1,89,'Poor dexterity',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1049,1,89,'Limited mobility',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1050,1,89,'Lack of reliable caregiver assistance',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1051,1,88,'Yes',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1052,1,88,'No',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1053,1,88,'Unknown',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1054,0,87,'Healing and stablization of ostomy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1055,0,87,'Independent management by client',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1056,0,87,'Family/support system management',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1057,0,87,'Maintenance of Ostomy by Nurse',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1058,1,85,'Congenital Disorder',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1059,1,85,'Diverticulitis',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1060,1,85,'Extrahepatic Biliary Atresia',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1061,1,85,'Familial Polyposis',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1062,1,85,'Infectious Enteritis',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1063,1,85,'Ischemic Disorders',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1064,1,85,'Crohns Disease',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1065,1,85,'Ulcerative Colitis',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1066,1,85,'Necrotizing Enterocolitis',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1067,1,85,'Obstructive Disorder',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1068,1,85,'Pseudomembraneous Colitis',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1069,1,85,'Cancer',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1070,1,85,'Radiation Enteritis',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1071,1,85,'Trauma',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1072,1,85,'Abscesses/fistulae',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1073,1,85,'Interstitial Cystitis',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1074,1,85,'Neurogenic Bladder',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1075,1,85,'Spinal Cord Injury',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1076,1,85,'Other',1,20,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1077,1,64,'No voiced concerns',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1078,1,64,'Able to discuss concerns',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1079,1,64,'Hesitant to discuss concerns',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1080,1,64,'Other',1,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1081,1,86,'Heal wound',0,1,0,0,'0','A',0,NULL,NULL,NULL,1),(1082,1,86,'Maintain wound',0,3,0,0,'0','A',0,NULL,NULL,NULL,0),(1083,1,2,'test_item',0,NULL,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(1085,1,90,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1),(1086,1,90,'Serous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1087,1,90,'Sanguinous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1088,1,90,'Green',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1089,1,90,'Purulent',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1090,1,91,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1091,1,91,'Indurated 2cm or >',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1092,1,91,'Erythema 2cm or >',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1093,1,91,'Edematous',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1094,1,91,'Blisters',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1095,1,92,'Approximated',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1096,1,62,'Flat',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1097,1,93,'Jackson Pratt drain',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1098,0,93,'Synder',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1099,1,94,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1100,1,94,'Indurated < 2cm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1101,1,94,'Indurated 2cm or >',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1102,1,94,'Erythema < 2cm',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1103,1,95,'Serous',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1104,1,95,'Sanguinous',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1105,1,95,'Green',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1106,1,95,'Purulent',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1107,1,95,'Bile',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1108,1,95,'Clear',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1109,1,95,'Cloudy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1110,1,95,'Mucousy',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1111,1,96,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1112,1,96,'Malodourous',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1113,1,97,'0',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1114,1,97,'1',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1115,1,97,'2',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1116,1,97,'3',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1117,1,97,'4',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1118,1,97,'5',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1119,1,97,'6',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1120,1,97,'7',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1121,1,97,'8',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1122,1,97,'9',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1123,1,97,'10',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1124,1,98,'intermittent',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1125,1,98,'Constant',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1126,1,99,'Sutures',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1127,1,100,'Insitu',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1128,1,101,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1),(1129,1,102,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1130,1,104,'<b>1. Completely Limited:</b><br>Unresponsive (does not moan, flinch or grasp) to painful stimuli, due to diminished level of consciousness or sedation, OR Limited ability to feel pain over most of body surface',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1131,1,104,'<b>2. Very Limited:</b><br>Responds only to painful stimuli.  Cannot communicate discomfort except by moaning or restlessness, OR Has a sensory impairment which limits the ability to feel pain or discomfort over 1/2 of body.',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1132,1,105,'<b>1. Constantly Moist:</b><br>Skin is kept moist almost constantly by perspiration, urine, etc.  Dampness is detected every time patient is moved or turned.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1133,1,106,'<b>1.Bedfast:</b><br>Confined to bed.  ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1134,1,107,'<b>1. Completely Immobile:</b><br>Does not make even slight changes in body or extremity position without assistance.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1135,1,108,'<b>1.Very Poor:</b><br>Never eats a complete meal.  Rarely eats more than 1/3 of any food offered.  Eats 2 servings or less of protein (meat or dairy products) per day.  Takes fluids poorly.  Does not take a liquid dietary supplement, OR Is NPO and/or maintained on clear liquids or IVs for more than 5 days.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1136,1,109,'<b>1.Problem:</b><br>Requires moderate to maximum assistance in moving.  Complete lifting without sliding against sheets is impossible.  Frequently slides down in bed or chair, requiring frequent repositioning with maximum assistance.  Spasticity contractures or agitation leads to almost constant friction.',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1157,1,84,'Muscous fistula',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1158,1,78,'Round',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1159,1,78,'Oval',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1160,1,78,'Oblong',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1161,1,78,'Irregular',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1162,1,110,'Leg a/k',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1163,1,110,'Leg b/k',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1164,1,110,'Foot partial',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1165,1,110,'Foot all',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1166,1,110,'Great toe',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1167,1,110,'Second toe',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1168,1,110,'Third toe',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1169,1,110,'Fourth toe',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1170,1,110,'Fifth toe',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1171,1,111,'Pain at rest',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1172,1,111,'Pain at night',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1173,1,111,'Decrease with elevation',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1174,1,111,'Increase with elevation',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1175,1,111,'Ache',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1176,1,111,'Knife-like',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1177,1,111,'Continuous',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1178,1,111,'Intermittent',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1179,1,111,'Intermittent claudication',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1180,1,111,'With deep palpation',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1181,1,111,'Non verbal response',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1182,1,112,'Dry/flakey',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1183,1,112,'Varicosities',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1184,1,112,'Hemosiderin staining',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1185,1,112,'Woody/fibrous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1186,1,112,'Stasis dermatitis',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1187,1,112,'Atrophie blanche',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1188,1,112,'Cellulitis',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1189,1,112,'Hairless/fragile/shiny',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1190,1,112,'Blanching on elevation',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1191,1,113,'Numbness',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1192,1,113,'Burning',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1193,1,113,'Tingling',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1194,1,113,'Crawling',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1195,1,113,'Continuous',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1196,1,113,'Intermittent',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1197,1,114,'Intact',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1198,1,114,'Impaired',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1199,1,115,'None noted',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1200,1,115,'Bunion(s)',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1201,1,115,'Callus(es)',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1202,1,115,'Corn(s)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1203,1,115,'Plantar wart(s)',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1204,1,115,'Hammertoe(s)',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1205,1,115,'Dropped MTH(s)',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1206,1,115,'Crossed toes',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1207,0,116,'None noted',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1208,0,116,'Incorrect nail length',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1209,0,116,'Ingrown nail(s)',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1210,0,116,'Involuted nail(s)',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1211,1,117,'Normal',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1212,1,117,'Nails too short',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1213,1,117,'Ingrown nail(s)',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1214,1,117,'Involuted nail(s)',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1215,1,117,'Thickened nail(s)',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1216,1,116,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,1),(1217,1,116,'Dry',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1218,1,116,'Cracks/fissures',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1219,1,116,'Rash',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1220,1,116,'Shiny',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1221,1,116,'Waxy',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1222,1,116,'Weepy',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1223,1,116,'Blisters',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1224,1,116,'Inflamed',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1225,1,118,'None',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1226,1,118,'Partial',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1227,1,119,'Steady',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1228,1,119,'Unsteady',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1229,1,120,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1230,1,120,'Impaired',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1231,1,121,'None',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1232,1,121,'Crutches',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1233,1,121,'Brace',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1234,1,121,'Cane',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1235,1,121,'Prosthesis',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1236,1,121,'Splint',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1237,1,121,'Walker - no wheels',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1238,1,121,'Walker - 2 wheels',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1239,1,121,'Walker - 4 wheels',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1240,1,122,'High',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1241,1,122,'Normal',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1242,1,122,'Low',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1243,1,122,'Flaccid',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1244,1,123,'High',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1245,1,123,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1246,1,123,'Flat',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1247,1,124,'Normal ROM',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1248,1,124,'Normal strength',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1249,1,124,'Decrd ROM',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1250,1,124,'Decrd strength',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1251,1,126,'1st digit',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1252,1,126,'3rd digit',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1253,1,126,'5th digit',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1254,1,126,'1st MTH',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1255,1,126,'3rd MTH',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1256,1,126,'5th MTH',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1257,1,126,'Medial',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1258,1,126,'Heel',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1259,1,126,'Dorsum',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1260,1,128,'>60mm/hg',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1261,1,128,'>40mm/hg',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1262,1,128,'>30mm/hg',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1263,1,128,'>20mm/hg',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1264,1,128,'>50mm/hg',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1265,1,6,'Advanced age 85 or >',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1266,1,6,'Over weight',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1267,1,6,'Muscle atrophy',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1268,1,6,'Bone deformities/contractures',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1269,1,6,'Posture/positioning preferences',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1270,1,6,'Poor transferring skills',0,19,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1271,1,148,'Ascending Colostomy',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1272,1,37,'Erythema 2cm or >',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1273,1,8,'Pressure Ulcer - Stage 2',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1274,1,8,'Pressure Ulcer - Stage 3',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1275,1,8,'Pressure Ulcer - Stage 4',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1276,1,8,'Pressure Ulcer - Stage X',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1277,1,104,'<b>3. Slightly Limited:</b><br>Responds to verbal commands, but cannot always communicate discomfort or need to be turned, OR Has some sensory impairment which limits ability to feel pain or discomfort in 1 or 2 extremities.',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1278,1,105,'<b>2. Moist:</b><br>Skin is often, but not always moist.  Linen must be changed at least once a shift.',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1279,1,104,'<b>4. No Impairment:</b><br>Responds to verbal commands.  Has no sensory deficit which would limit ability to feel or voice pain or discomfort',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1280,1,105,'<b>3. Occasionally Moist:</b><br>Skin is occasionally moist, requiring an extra linen change approximately once a day.',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1281,1,105,'<b>4. Rarely Moist:</b><br>Skin is usually dry, linen requires changing only at routine intervals.',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1282,1,106,'<b>2.Chairfast:</b><br>Ability to walk severely limited or non-existent.  Cannot bear own weight and/or must be assisted into chair or wheelchair.',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1283,1,106,'<b>3. Walks Occasionally:</b><br>Walks occasionally during day, but for very short distances, with or without assistance.  Spends majority of each shift in bed or chair.',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1284,1,106,'<b>4.Walks Frequently:</b><br>Walks outside the room at least twice a day and inside room at least once every 2 hours during waking hours.',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1285,1,107,'<b>2.Very Limited:</b><br>Makes occasional slight changes in body or extremity position but unable to make frequent or significant changes independently.',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1286,1,107,'<b>3.Slightly Limited:</b><br>Makes frequent though slight changes in body or extremity position independently.',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1287,1,107,'<b>4. No Limitations:</b><br>Makes major and frequent changes in position without assistance.',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1288,1,108,'<b>2. Probably Inadequate:</b><br>Rarely eats a complete meal and generally eats only about 1/2 of any food offered.  Protein intake includes only 3 servings of meat or dairy products per day.  Occasionally will take a dietary supplement. OR Receives less than optimum amount of liquid diet or tube feeding.',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1289,1,108,'<b>3.Adequate:</b><br>Eats over half of most meals.  Eats a total of four servings of protein (meat, dairy products) each day.  Occasionally will refuse a meal, but will usually take a supplement if offered.  OR is on tube feeding or TPN regimen, which probably meets most of nutritional needs.',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1290,1,108,'<b>4.Excellent:</b><br>Eats most of every meal.  Never refuses a meal.  Usually eats a total of four or more servings of meat and dairy products.  Occasionally eats between meals.  Does not require supplementation.',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1291,1,109,'<b>2.Potential Problem:</b><br>Moves feebly or requires minimum assistance.  During a move, skin probably slides to some extent against sheets, chair, restraints or other devices.  Maintains relatively good position in chair or bed most of the time, but occasionally slides down.',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1292,1,109,'<b>3.No Apparent Problem:</b><br>Moves in bed and in chair independently and has sufficient muscle strength to lift up completely during move.  Maintains good position in bed or chair at all times.',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1293,1,91,'Rash',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1294,1,112,'Mottled',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1295,1,100,'Removed',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1296,1,37,'Fragile',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1298,0,35,'Suspected Deep Tissue Injury SDTI',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1299,0,35,'Fully epithialized',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1300,0,35,'Intact burn area',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1302,1,8,'Suspected Deep Tissue Injury SDTI',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1303,1,34,'Hypergranulation',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1304,1,24,'Other',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1305,0,32,'Surgical Intervention',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1306,0,32,'Other',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1309,1,111,'No pain',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1310,1,112,'Normal/healthy',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1311,1,112,'Dependent rubor',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1312,1,112,'Moist/waxy',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1313,1,18,'Bluish-purple',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1314,1,18,'Black',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1315,1,22,'Triphasic',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1316,1,22,'Biphasic',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1317,1,22,'Monophasic',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1318,1,121,'Scooter',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1319,1,110,'No amputations',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1320,1,21,'0 None',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1321,1,21,'+ Homans Sign',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1322,1,21,'+ Stemmers Sign',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1323,1,92,'Tenuous',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1324,1,99,'Staples',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1325,1,99,'Retention Sutures',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1326,1,99,'Steri-strips',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1327,1,99,'Suriglue',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1328,1,101,'Scant ',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1329,1,101,'Small ',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1330,1,101,'Moderate',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1331,1,101,'Large',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1332,1,68,'Viewed pouch change',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1333,0,68,'Viewed appliance change',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1334,1,68,'Participated w emptying pouch',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1335,1,68,'Participated w pouch change',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1336,0,68,'Participated w appliance change',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1337,1,68,'Independent w emptying pouch',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1338,1,68,'Independent w pouch change',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1339,1,66,'Amber',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1340,1,66,'Yellow',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1341,1,66,'Pale yellow',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1342,1,70,'Cloudy',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1343,1,70,'Mucousy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1344,1,71,'Moderate',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1345,1,71,'Large',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1346,1,71,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1),(1347,1,61,'Bile',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1348,1,61,'Brown',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1349,1,61,'Yellow',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1350,1,72,'Mushy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1351,1,72,'Watery',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1352,1,73,'Nil',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,1),(1353,1,73,'Moderate',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1354,1,73,'Large',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1355,1,74,'Red',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1356,1,89,'Language barrier',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1357,1,93,'Hemovac drain',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1358,1,93,'Penrose drain',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1359,1,93,'Chest tube',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1360,1,93,'Nephrostomy tube',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1361,0,93,'PEG ',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1362,1,94,'Erythema 2 cm or >',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1363,0,94,'Edematous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1364,1,95,'Yellow',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1365,1,102,'Eroded',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1366,1,102,'Hypergranulated',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1367,1,134,'Genito-urinary',0,4,0,0,'681',NULL,0,NULL,NULL,NULL,0),(1368,1,135,'Post-op drainage',0,1,0,0,'681',NULL,0,NULL,NULL,NULL,0),(1369,1,136,'Loop',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1370,1,136,'End',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1371,1,136,'Barrel',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1372,1,137,'Clear',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1373,1,137,'Small',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1374,1,137,'Mucousy',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1375,1,138,'Dissolvable sutures',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1376,1,138,'Removable sutures',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1377,1,139,'Intact',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1378,1,139,'Rash',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1379,1,139,'Weepy',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1387,1,143,'Not applicable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1388,1,143,'Capped/Clamped',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1389,1,144,'Drain removed as ordered',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1390,1,146,'Patient independent with ostomy care',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1391,1,146,'Patient & caregiver independent with ostomy care',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1392,1,136,'Unmatured ',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1393,1,138,'Approximated',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1394,1,138,'Tenuous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1395,1,138,'Separated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,1),(1396,1,137,'Moderate',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1397,1,137,'Large',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1399,1,65,'Changed - leaked outside the flange',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1400,1,65,'Changed - accidentially removed',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1401,1,72,'Gas',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1402,1,75,'Flabby',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1403,1,75,'Distended',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1404,1,75,'Loose/wrinkly',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1405,1,75,'Soft',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1406,1,75,'Hard',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1407,1,75,'Hernia',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1408,0,75,'Flat',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1409,1,74,'Pink',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1410,1,74,'Dusky',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1411,1,74,'Pale',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1412,1,74,'Black',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1413,1,74,'Moist',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1414,1,74,'Edematous',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1415,1,74,'Friable',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1416,1,74,'Slough',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1417,1,74,'Trauma',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1418,1,138,'Suture granuloma ',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1419,1,70,'Odourous',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1420,1,89,'Limited visiability of stoma by patient',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1421,1,134,'Laceration closure',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1422,1,135,'Obstruction',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1423,1,146,'Caregiver independent with ostomy care',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1424,1,87,'Permanent ostomy ',0,7,0,0,'0','O',0,NULL,NULL,NULL,0),(1425,1,87,'Temporary ostomy ',0,6,0,0,'0','O',0,NULL,NULL,NULL,0),(1426,1,115,'Acute Charcot',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1427,1,115,'Chronic Charcot',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1428,1,117,'Yellow coloured',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1429,1,117,'Fungal infection',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1430,1,121,'Wheelchair',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1431,0,124,'ROM normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1432,1,125,'Decrd ROM ',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1433,1,126,'Lateral',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1434,1,128,'20mm/hg or<',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1435,0,23,'Triphasic',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1436,0,23,'Biphasic',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1437,0,23,'Monophasic',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1438,1,127,'Done by WCC/designate',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1439,1,127,'Done in Vascular Lab',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1464,1,138,'Fistula',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1465,1,139,'Boggy',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1466,1,139,'Erythemic',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1467,1,139,'Indurated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1468,1,139,'Excoriated',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1469,1,139,'Macerated',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1470,1,67,'Bony prominence',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1471,1,67,'Fold/crease',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1472,1,67,'Incision',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1473,1,67,'Open wound',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1474,1,67,'Scar',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1475,1,67,'Umbilicus',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1476,1,62,'Raised',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1477,1,62,'Prolapsed',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1478,1,62,'Os off-centre',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1479,1,62,'Retracted',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1480,1,62,'Stenosed',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1481,1,62,'Os titled',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1482,1,62,'Os flush',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1483,1,65,'Changed - test/procedure',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1484,1,72,'Dry/hard',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1485,1,72,'Soft/formed',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1486,1,147,'NPO',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1487,1,147,'TPN',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1488,1,147,'Clear fluids',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1489,1,147,'Full fluids',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1490,1,147,'Soft diet',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1491,1,147,'Low residue',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1492,1,147,'Regular diet',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1493,0,68,'Independent w appliance change',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1494,1,69,'Bruised',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1495,1,69,'Excoriated',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1496,1,69,'Boggy',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1497,1,69,'Trauma',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1498,1,69,'Erythemic',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1499,1,69,'Indurated',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1500,1,69,'Macerated',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1501,1,69,'Weepy',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1502,1,69,'Denuded',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1503,1,69,'Mucosal transplanting',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1504,1,69,'Pseudo-verrucous lesions',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1505,1,69,'Contact dermatits',0,13,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1506,1,69,'Folliculitis',0,15,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1507,1,69,'Fungal rash',0,16,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1508,1,69,'Psoriasis',0,17,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1509,1,69,'Allergic reaction',0,14,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1510,1,69,'Eczema',0,18,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1511,1,69,'Pyoderma gangrenosum',0,19,0,0,'0','',0,NULL,NULL,NULL,0),(1512,1,69,'Caput Medusae',0,20,0,0,'0','',0,NULL,NULL,NULL,0),(1513,1,89,'No limitations',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1514,1,90,'Not assessed',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1515,1,92,'Gaping',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1516,1,101,'Not assessed',0,6,0,1,'0','',0,NULL,NULL,NULL,0),(1517,1,92,'Not assessed',0,4,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1518,1,99,'Not assessed',0,6,0,1,'0','',0,NULL,NULL,NULL,0),(1519,1,100,'Not assessed',0,3,0,1,'0','',0,NULL,NULL,NULL,0),(1520,1,68,'Viewed flange change',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1521,1,68,'Participated w flange change',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1522,1,68,'Independent w flange change',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1523,0,68,'Teaching booklet given',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1524,1,68,'Viewed teaching video',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1525,0,68,'Skin problems literature provided',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1526,0,68,'General info literature provided',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1527,0,68,'Change instructions given',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1528,0,68,'ET literature provided',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1529,0,68,'List of ostomy suppliers provided',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1530,0,68,'UOA visitor requested ',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1531,1,68,'UOA visitor ',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1532,1,149,'Anastomosis',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1533,1,149,'Re-siting',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1534,1,149,'Revision',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1535,1,67,'Drain ',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1536,1,70,'Bloody',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1537,1,74,'Turgor good',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1538,1,74,'Turgor poor',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1539,1,148,'Jejunostomy',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1540,1,148,'Cecostomy',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1541,1,166,'Not applicable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1542,1,67,'N/A',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1543,1,61,'Not assessed',0,5,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1544,1,72,'Not assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1545,1,70,'Concentrated',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1546,1,150,'Closed (healed) ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1547,1,150,'Patient self care',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1548,1,150,'Revision',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1549,0,152,'Pain controlled',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1550,1,152,'Afebrile',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1551,1,152,'Lungs clear',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1552,1,152,'No calf pain',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1553,1,152,'Drinking well',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1554,1,152,'Eating well',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1555,1,152,'Bowels moving',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1556,1,152,'Voiding well',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1557,1,95,'Odourous',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1558,1,95,'Not applicable',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1561,1,95,'Not Assessed',0,12,0,1,'0','',0,NULL,NULL,NULL,0),(1565,1,144,'Drain accidentially pulled out',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1566,1,144,'Drain fell out',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1567,1,24,'Not assessed',0,7,0,1,'0','',0,NULL,NULL,NULL,0),(1568,1,25,'Not assessed',0,6,0,1,'0','',0,NULL,NULL,NULL,0),(1569,0,26,'Not assessed',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1570,1,34,'Not assessed',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1571,1,35,'Not assessed',0,23,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1572,1,37,'Not assessed',0,15,0,1,'0','',0,NULL,NULL,NULL,0),(1573,1,152,'Not assessed',0,10,0,1,'0','',0,NULL,NULL,NULL,0),(1574,1,91,'Indurated < 2cm',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1575,1,91,'Erythema < 2cm',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1576,1,137,'Not assessed',0,3,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1577,1,138,'Not assessed',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1578,1,139,'Not assessed',0,9,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1579,1,66,'Not assessed',0,5,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1580,1,70,'Not assessed',0,7,0,1,'0','',0,NULL,NULL,NULL,0),(1581,1,71,'Not assessed',0,5,0,1,'0','',0,NULL,NULL,NULL,0),(1582,1,73,'Not assessed',0,5,0,1,'0','',0,NULL,NULL,NULL,0),(1583,1,74,'Not assessed',0,13,0,0,'0','',0,NULL,NULL,NULL,0),(1584,1,75,'Not assessed',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1585,1,68,'Not assessed',0,17,0,0,'0','',0,NULL,NULL,NULL,0),(1586,1,69,'Not assessed',0,22,0,0,'0','',0,NULL,NULL,NULL,0),(1587,1,147,'Not assessed',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1588,1,85,'Ileus',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1591,0,150,'Fully epithialized',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1592,0,150,'Fully ',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1593,0,150,'To be closed with 2nd intention',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1594,0,150,'Closed with primary intention',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1595,1,66,'Red',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1596,1,70,'Clear',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1597,1,61,'Bloody',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1598,1,72,'Nil',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1599,0,150,'Self care',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1600,0,136,'Not Applicable',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1601,1,75,'Pendulous',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1602,1,168,'Temporary tube/drain',0,4,0,0,'681','D',0,NULL,NULL,NULL,0),(1603,1,169,'Permanent tube/drain',0,5,0,0,'681','D',0,NULL,NULL,NULL,0),(1604,1,89,'Lack of bathroom facilities',0,11,0,0,'681','',0,NULL,NULL,NULL,0),(1605,1,165,'Diuretics',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1606,1,165,'Multivitamins',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1607,1,165,'Herbal supplements',0,13,0,0,'0','',0,NULL,NULL,NULL,0),(1610,1,118,'Full',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1611,1,125,'Normal ROM',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1612,1,125,'Normal strength ',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1613,1,125,'Decrd strength ',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1614,1,85,'Perforation',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1615,1,134,'Orthopedic - Knee',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1616,1,134,'Orthopedic - Other',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1617,1,134,'Cardiovascular',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1618,1,134,'Gastrointestinal',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1619,1,93,'Urethral catheter',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1620,1,93,'Suprapubic catheter',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1621,0,93,'Percutaneous tube',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1622,1,93,'Peritoneal Dialysis(PD) catheter',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1623,1,93,'Gastrostomy tube (G-tube; PEG)',0,13,0,0,'0','',0,NULL,NULL,NULL,0),(1624,1,93,'Jejeunostomy tube (J-tube)',0,14,0,0,'0','',0,NULL,NULL,NULL,0),(1625,1,93,'Biliary tube',0,14,0,0,'0','',0,NULL,NULL,NULL,0),(1626,1,68,'Viewed stoma',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1627,1,8,'Skin Tear',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1628,1,134,'Oncology - Other',0,7,0,0,'681',NULL,0,NULL,NULL,NULL,0),(1629,1,134,'Neuro',0,11,0,0,'681',NULL,0,NULL,NULL,NULL,0),(1630,1,134,'Orthopedic - Hip',0,9,0,0,'681',NULL,0,NULL,NULL,NULL,0),(1631,1,134,'Plastics/Skin/Wound',0,12,0,0,'681','',0,NULL,NULL,NULL,0),(1632,1,134,'Oncology - Breast',0,6,0,0,'681','',0,NULL,NULL,NULL,0),(1633,1,134,'Oncology - Prostate',0,6,0,0,'681',NULL,0,NULL,NULL,NULL,0),(1634,1,134,'Trauma',0,13,0,0,'681',NULL,0,NULL,NULL,NULL,0),(1635,1,135,'Retention',0,3,0,0,'681','',0,NULL,NULL,NULL,0),(1636,1,148,'Ileal Conduit',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1637,1,148,'Neobladder',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1638,1,148,'Continent pouch - urinary',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1639,1,148,'Continent pouch - fecal',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1640,1,35,'No open wound',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1641,1,35,'New tissue damage ',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1642,1,152,'Not applicable',0,1,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1643,1,152,'Concerns noted - see Progress Notes',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1644,1,92,'Fully epithelialized',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1645,1,99,'Not applicable',0,1,0,1,'0','',0,NULL,NULL,NULL,0),(1646,1,94,'Rash',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1647,1,94,'Not assessed',0,7,0,1,'0',NULL,0,NULL,NULL,NULL,0),(1648,1,94,'Elevated warmth',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1649,1,136,'Unknown',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1650,1,138,'Fully epithelialized',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1651,1,28,'M-Th',0,12,0,0,'0','2',0,NULL,NULL,NULL,0),(1652,1,28,'Tu-F',0,13,0,0,'0','2',0,NULL,NULL,NULL,0),(1653,1,29,'M-Th',0,12,0,0,'0','2',0,NULL,NULL,NULL,0),(1654,1,29,'Tu-F',0,13,0,0,'0','2',0,NULL,NULL,NULL,0),(1655,1,67,'Difficult placement',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1656,1,135,'Inability to swallow',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1661,0,150,'test',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1662,1,34,'Not applicable',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1663,1,94,'Fragile',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1664,1,94,'Macerated',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1665,1,94,'Excoriated',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1668,1,113,'Normal',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1669,1,117,'Nails too long',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1670,1,74,'Dark red',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1671,1,1,'Pharmacare Coverage - 100% (ostomy)',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1672,1,89,'Limited financial means',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1673,1,8,'Fistula - stomatized',0,19,0,0,'0','',0,NULL,NULL,NULL,0),(1674,1,8,'Fistula - non stomatized',0,20,0,0,'0',NULL,0,NULL,NULL,NULL,0),(1678,1,8,'Infectious',0,14,0,0,'0','',0,NULL,NULL,NULL,0),(1679,1,34,'Eroding',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1681,0,65,'Changed - leaked within the flange',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1684,1,167,'Heal incision',0,2,0,0,'0','T',0,NULL,NULL,NULL,0),(1685,0,93,'Synder drain',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(1686,1,69,'Inflammatory process',0,21,0,0,'0','',0,NULL,NULL,NULL,0),(1687,1,144,'Patient/Client self care',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1688,1,93,'PleurX catheter',0,17,0,0,'0','',0,NULL,NULL,NULL,0),(1689,1,138,'Not visible/obscured',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1690,1,49,'Fecal Incontinence',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1691,1,49,'Urinary Incontinence',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1692,1,67,'Peristomal skin protruding',0,15,0,0,'0','',0,NULL,NULL,NULL,0),(1693,1,67,'Peristomal skin retracting',0,16,0,0,'0','',0,NULL,NULL,NULL,0),(1698,1,147,'NG feeding',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1699,1,147,'NJ feeding',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1700,1,147,'Gastrostomy feeding',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1701,1,91,'Not assessed',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1702,1,75,'Flat',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1703,1,93,'Seton drain',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1704,1,155,'Active/Nursing',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(1705,1,155,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(1706,1,154,'Active',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(1707,1,154,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(1708,1,153,'Active',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(1709,1,153,'Closed',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(1710,1,155,'Active/Client Self Care',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,1),(1711,1,134,'Gynecology',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1712,1,38,'No Growth',0,1,0,0,'0','',0,NULL,NULL,NULL,0),(1714,1,150,'Gaping(heal by 2nd intent)',0,3,0,0,'0','',0,NULL,NULL,NULL,0),(1718,1,93,'Trans Esophgeal Prosthesis',0,18,0,0,'0','',0,NULL,NULL,NULL,0),(1719,1,144,'Drain/tube removed by Dr.',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1720,1,93,'Percutaneous tube',0,16,0,0,'0','',0,NULL,NULL,NULL,0),(1721,1,149,'Ostomy not required',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1722,1,93,'Mace tube',0,19,0,0,'0','',0,NULL,NULL,NULL,0),(1723,1,85,'Incontinence',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1724,1,85,'Infection',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1725,1,87,'To be detemined - ostomy',0,8,0,0,'0','',0,NULL,NULL,NULL,0),(1726,1,147,'Ileostomy Diet',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1727,1,68,'Sexual concerns discussed',0,13,0,0,'0','',0,NULL,NULL,NULL,0),(1728,1,68,'Colostomy irrigations',0,14,0,0,'0','',0,NULL,NULL,NULL,0),(1729,1,62,'Os centered',0,7,0,0,'0','',0,NULL,NULL,NULL,0),(1730,1,67,'Stoma flush ',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1731,1,67,'Stoma retracted',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1732,1,67,'Os tilted',0,13,0,0,'0','',0,NULL,NULL,NULL,0),(1733,1,67,'Os flush',0,14,0,0,'0','',0,NULL,NULL,NULL,0),(1734,1,62,'Not assessed',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1735,1,67,'Not assessed',0,17,0,0,'0','',0,NULL,NULL,NULL,0),(1736,1,74,'Cancerous lesion(s)',0,14,0,0,'0','',0,NULL,NULL,NULL,0),(1737,1,69,'Cancerous lesion(s)',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1738,1,69,'Peristiomal hernia',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1747,1,93,'Nasogastric tube',0,12,0,0,'0','',0,NULL,NULL,NULL,0),(1748,1,93,'Gastrojejunal (GJ ) tube',0,13,0,0,'0','',0,NULL,NULL,NULL,0),(1749,1,135,'Nutrition suppport',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1750,1,148,'J-Pouch Stage 1',0,9,0,0,'0','',0,NULL,NULL,NULL,0),(1751,1,148,'J-Pouch Stage 2',0,10,0,0,'0','',0,NULL,NULL,NULL,0),(1752,1,148,'J-Pouch Stage 3',0,11,0,0,'0','',0,NULL,NULL,NULL,0),(1753,1,149,'J-Pouch takedown',0,4,0,0,'0','',0,NULL,NULL,NULL,0),(1754,1,68,'J-Pouch bowel training',0,15,0,0,'0','',0,NULL,NULL,NULL,0),(1755,1,68,'J-Pouch perianal skin maintenence',0,16,0,0,'0','',0,NULL,NULL,NULL,0),(1756,1,32,'Lost contact with patient',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1757,1,150,'Lost contact with patient',0,5,0,0,'0','',0,NULL,NULL,NULL,0),(1758,1,144,'Lost contact with patient',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1759,1,149,'Lost contact with patient',0,6,0,0,'0','',0,NULL,NULL,NULL,0),(1770,0,30,'PAN Referral 1-7 days',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,1),(1771,1,140,'Other',1,99,0,0,'0',NULL,0,NULL,NULL,NULL,0),(2907,169,30,'Deceased',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(2908,169,30,'Transferred - Non Pix Site',1,99,0,0,'0',NULL,0,NULL,NULL,NULL,0),(2909,169,30,'Move out of Region',1,99,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3200,1,0,'No Treatment Designation',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,1),(3201,1,169,'Deceased',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3202,1,169,'Moved out of Region',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3203,1,169,'Transferred - Non Pix Site',0,99,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3204,1,170,'America/Vancouver',0,3,0,0,'0','',0,'','','',0),(3205,1,170,'America/Dawson_Creek',0,2,0,0,'0','',0,NULL,NULL,NULL,0),(3206,1,172,'Burn: Etiology 1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(3207,1,173,'Burn: Goal 1',0,1,NULL,0,'0',NULL,0,NULL,NULL,NULL,0),(3208,1,157,'Pink - Light Red',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3209,1,157,'Dark Red',0,2,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3210,1,157,'White',0,3,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3211,1,157,'Yellow/Tan',0,4,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3212,1,157,'Brown/Black',0,5,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3213,1,157,'Blisters',0,6,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3214,1,157,'Eschar/Leathery',0,7,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3215,1,157,'Loose Tissue',0,8,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3216,1,157,'Granulation Buds',0,9,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3217,1,157,'Friable',0,10,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3218,1,157,'Healed',0,11,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3219,1,157,'Not Assessed',0,12,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3220,1,158,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3221,1,158,'Serous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3222,1,158,'Purulent',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3223,1,158,'Sanguineous',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3224,1,158,'Dried',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3225,1,158,'Not Assessed',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3226,1,159,'Nil',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3227,1,159,'Scant',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3228,1,159,'Moist/Small',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3229,1,159,'Wet/Moderate',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3230,1,159,'Copious/Large',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3231,1,159,'Not Assessed',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3232,1,160,'Tie-Over/Occlusive',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3233,1,160,'Open/Sheet/Mesh',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3234,1,160,'Fluid Pockets',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3235,1,160,'White/Pink',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3236,1,160,'Dusky/Purple',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3237,1,160,'Open Areas',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3238,1,160,'Crusted',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3239,1,160,'Friable',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3240,1,160,'Healed/Dry/Flaking',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3241,1,160,'Not Assessed',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0),(3242,1,161,'N/A',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `lookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nursing_care_plan`
--

DROP TABLE IF EXISTS `nursing_care_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing_care_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT '0',
  `wound_id` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `professional_id` int(11) DEFAULT '0',
  `user_signature` varchar(255) DEFAULT NULL,
  `body` text,
  `time_stamp` varchar(255) DEFAULT NULL,
  `assessment_id` int(11) NOT NULL DEFAULT '0',
  `wound_profile_type_id` int(11) NOT NULL DEFAULT '0',
  `visit_frequency` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `deleted_reason` varchar(255) DEFAULT '',
  `delete_signature` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `id` (`id`),
  KEY `wound_id` (`wound_id`),
  KEY `professional_id` (`professional_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `nursing_care_plan_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `nursing_care_plan_ibfk_2` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `nursing_care_plan_ibfk_3` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `nursing_fixes`
--

DROP TABLE IF EXISTS `nursing_fixes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing_fixes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `row_id` int(11) NOT NULL DEFAULT '0',
  `professional_id` int(11) NOT NULL DEFAULT '0',
  `time_stamp` text,
  `initials` text,
  `field` text,
  `error` text,
  `delete_reason` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `professional_id` (`professional_id`),
  KEY `patient_id` (`patient_id`),
  CONSTRAINT `nursing_fixes_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `nursing_fixes_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nursing_fixes`
--


DROP TABLE IF EXISTS `password_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `professional_id` int(11) DEFAULT NULL,
  `password` text,
  `time_stamp` text,
  PRIMARY KEY (`id`),
  KEY `professional_id` (`professional_id`),
  CONSTRAINT `password_history_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `patient_accounts`
--

DROP TABLE IF EXISTS `patient_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `name` text,
  `lastname` text,
  `lastname_search` text,
  `treatment_location_id` int(11) DEFAULT NULL,
  `phn` text,
  `outofprovince` int(11) NOT NULL DEFAULT '0',
  `current_flag` int(11) DEFAULT '0',
  `account_status` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_by` int(11) DEFAULT NULL,
  `user_signature` text,
  `gender` int(11) DEFAULT NULL,
  `phn2` varchar(255) DEFAULT NULL,
  `phn3` varchar(255) DEFAULT NULL,
  `patient_residence` int(11) DEFAULT NULL,
  `addressLine1` varchar(255) DEFAULT '',
  `addressLine2` varchar(255) DEFAULT '',
  `addressCity` varchar(255) DEFAULT '',
  `addressProvince` varchar(255) DEFAULT '',
  `addressPostalCode` varchar(255) DEFAULT '',
  `dob` varchar(255) DEFAULT '',
  `time_stamp` varchar(255) DEFAULT '',
  `allergies` text,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `patient_accounts_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `patient_investigations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_investigations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_profile_id` int(11) DEFAULT NULL,
  `investigation_date` datetime(6) DEFAULT NULL,
  `lookup_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_profile_id` (`patient_profile_id`),
  CONSTRAINT `patient_investigations_ibfk_1` FOREIGN KEY (`patient_profile_id`) REFERENCES `patient_profiles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `patient_investigations_ibfk_2` FOREIGN KEY (`patient_profile_id`) REFERENCES `patient_profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_investigations`
--

LOCK TABLES `patient_investigations` WRITE;
/*!40000 ALTER TABLE `patient_investigations` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_investigations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_profile_arrays`
--

DROP TABLE IF EXISTS `patient_profile_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_profile_arrays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_profile_id` int(11) DEFAULT '0',
  `lookup_id` int(11) DEFAULT '0',
  `other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_profile_id` (`patient_profile_id`),
  CONSTRAINT `patient_profile_arrays_ibfk_1` FOREIGN KEY (`patient_profile_id`) REFERENCES `patient_profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `patient_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `current_flag` tinyint(4) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `offline_flag` int(11) DEFAULT NULL,
  `treatment_location_id` int(11) DEFAULT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `funding_source_id` int(11) DEFAULT NULL,
  `professionals` text,
  `professional_id` int(11) DEFAULT '0',
  `surgical_history` text,
  `albumin` int(11) DEFAULT NULL,
  `pre_albumin` int(11) DEFAULT NULL,
  `albumin_date` text,
  `prealbumin_date` text,
  `start_date` text,
  `last_update` text,
  `user_signature` varchar(50) DEFAULT NULL,
  `review_done` int(11) DEFAULT '0',
  `deleted` int(11)  NOT NULL DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `data_entry_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `professional_id` (`professional_id`),
  CONSTRAINT `patient_profiles_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `patient_profiles_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `patient_profiles_ibfk_3` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `patient_report`
--

DROP TABLE IF EXISTS `patient_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT '0',
  `time_stamp` varchar(255) DEFAULT '',
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` text,
  `professional_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--



--
-- Table structure for table `physical_exam`
--

DROP TABLE IF EXISTS `physical_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physical_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `deleted` int(11)  NOT NULL DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `temperature` float DEFAULT NULL,
  `pulse` int(11) DEFAULT NULL,
  `resp_rate` int(11) DEFAULT NULL,
  `blood_sugar` float DEFAULT NULL,
  `blood_sugar_meal` int(11) DEFAULT NULL,
  `blood_sugar_date` date DEFAULT NULL,
  `blood_pressure` varchar(255) DEFAULT NULL,
  `height` float DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `user_signature` varchar(255) DEFAULT NULL,
  `last_update` datetime(6) DEFAULT NULL,
  `professional_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physical_exam`
--



--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `category` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `cost` text,
  `description` text,
  `company` text,
  `url` text,
  `image` text,
  `active` int(11) DEFAULT NULL,
  `type` text,
  `quantity` text,
  `base_id` int(11) DEFAULT NULL,
  `all_treatment_locations` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1125 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

--
-- Table structure for table `products_by_facility`
--

DROP TABLE IF EXISTS `products_by_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_by_facility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `treatment_location_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_by_facility`
--

LOCK TABLES `products_by_facility` WRITE;
/*!40000 ALTER TABLE `products_by_facility` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_by_facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_categories`
--

DROP TABLE IF EXISTS `products_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_categories`
--


--
-- Table structure for table `professional_accounts`
--

DROP TABLE IF EXISTS `professional_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional_accounts`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` text,
  `account_status` int(11) DEFAULT NULL,
  `firstname` text,
  `firstname_search` text,
  `lastname` text,
  `lastname_search` text,
  `pager` text,
  `email` text,
  `training_flag` int(1) DEFAULT '0',
  `access_uploader` int(1) DEFAULT NULL,
  `access_reporting` int(1) DEFAULT NULL,
  `access_viewer` int(1) DEFAULT NULL,
  `access_admin` int(1) DEFAULT NULL,
  `access_allpatients` int(1) DEFAULT NULL,
  `access_assigning` int(1) DEFAULT NULL,
  `access_professionals` int(11) DEFAULT NULL,
  `access_patients` int(11) DEFAULT NULL,
  `access_regions` int(11) DEFAULT NULL,
  `access_referrals` int(11) DEFAULT NULL,
  `access_listdata` int(11) DEFAULT NULL,
  `access_audit` int(11) DEFAULT NULL,
  `access_superadmin` int(11) DEFAULT NULL,
  `individuals` text,
  `user_name` text,
  `creation_date` text,
  `updated_date` text,
  `deletion_date` text,
  `new_user` int(11) DEFAULT NULL,
  `timezone` varchar(100) DEFAULT NULL,
  `access_create_patient` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `locked` int(11) DEFAULT NULL,
  `invalid_password_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_accounts`
--

LOCK TABLES `professional_accounts` WRITE;
/*!40000 ALTER TABLE `professional_accounts` DISABLE KEYS */;
INSERT INTO `professional_accounts` VALUES (7,'dcbe61b3a1554a0d10206ad36179b711',1,'Webmed','WEBMED','Test','TEST','','',0,1,1,1,1,1,1,1,1,1,0,0,1,1,'1',NULL,NULL,NULL,NULL,0,'America/Vancouver',0,5,0,0);
/*!40000 ALTER TABLE `professional_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referrals_tracking`
--

DROP TABLE IF EXISTS `referrals_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referrals_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_flag` int(11) DEFAULT NULL,
  `patient_account_id` int(11) NOT NULL DEFAULT '0',
  `wound_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `entry_type` int(11) DEFAULT NULL,
  `time_stamp` text,
  `top_priority` int(11) DEFAULT '0',
  `assessment_id` int(11) NOT NULL DEFAULT '0',
  `professional_id` int(11) DEFAULT NULL,
  `alpha_type` varchar(5) DEFAULT '',
  `auto_referral` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `professional_id` (`professional_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `patient_account_id` (`patient_account_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `referrals_tracking_ibfk_5` FOREIGN KEY (`patient_account_id`) REFERENCES `patient_accounts` (`id`),
  CONSTRAINT `referrals_tracking_ibfk_7` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `referrals_tracking_ibfk_8` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`),
  CONSTRAINT `referrals_tracking_ibfk_9` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referrals_tracking`
--


--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `report_value_label_key` text,
  `report_value_component_type` text,
  `report_value_field_type` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (1,'Funding Source',NULL,NULL,NULL),(2,'Referral Source',NULL,NULL,NULL),(204,'Treatment Location',NULL,NULL,NULL),(4,'Patient Residence',NULL,NULL,NULL),(5,'Co-morbidities',NULL,NULL,NULL),(6,'Interfering Factors',NULL,NULL,NULL),(7,'Gender',NULL,NULL,NULL),(8,'Etiology',NULL,NULL,NULL),(9,'Program Stay',NULL,NULL,NULL),(10,'Program Stay Variance',NULL,NULL,NULL),(11,'Patient Care',NULL,NULL,NULL),(12,'Pressure Ulcer Stage',NULL,NULL,NULL),(13,'Albumin',NULL,NULL,NULL),(14,'Pre Albumin',NULL,NULL,NULL),(15,'Infections',NULL,NULL,NULL),(16,'Investigations',NULL,NULL,NULL),(17,'VAC',NULL,NULL,NULL),(18,'Limb Color',NULL,NULL,NULL),(19,'Limb Temperature',NULL,NULL,NULL),(20,'Limb Edema',NULL,NULL,NULL),(21,'Limb Edema Severity',NULL,NULL,NULL),(22,'Doppler List',NULL,NULL,NULL),(23,'Palpation List',NULL,NULL,NULL),(24,'Exudate Type',NULL,NULL,NULL),(25,'Exudate Amount',NULL,NULL,NULL),(26,'Odour',NULL,NULL,NULL),(28,'Dressing Change Frequency','pixalere.admin.resources.form.weekly_dressing_changes','TXTN','INTEGER'),(29,'Nursing Visit Frequency','pixalere.admin.resources.form.weekly_nursing_visits','TXTN','INTEGER'),(30,'Priority','pixalere.admin.resources.form.response_time','TXTN','INTEGER'),(31,'Wound Status',NULL,NULL,NULL),(32,'Discharge Reason',NULL,NULL,NULL),(33,'Wound Bed',NULL,NULL,NULL),(34,'Wound Edge',NULL,NULL,NULL),(35,'Wound Bed Percent',NULL,NULL,NULL),(36,'Colour',NULL,NULL,NULL),(37,'Periwound Skin',NULL,NULL,NULL),(38,'C&S Result',NULL,NULL,NULL),(39,'C&S Route',NULL,NULL,NULL),(40,'Pressure Reading',NULL,NULL,NULL),(41,'Endocrine/Metabolic/Nutritional',NULL,NULL,NULL),(42,'Heart/Circulation',NULL,NULL,NULL),(43,'Musculoskeletal',NULL,NULL,NULL),(44,'Neurological',NULL,NULL,NULL),(45,'Psychiatric/Mood',NULL,NULL,NULL),(46,'Pulmonary',NULL,NULL,NULL),(47,'Sensory',NULL,NULL,NULL),(48,'Infections',NULL,NULL,NULL),(49,'Other',NULL,NULL,NULL),(50,'Richmond',NULL,NULL,NULL),(51,'Vancouver',NULL,NULL,NULL),(52,'Coast Garibaldi',NULL,NULL,NULL),(53,'North Shore',NULL,NULL,NULL),(54,'Blood Sugar',NULL,NULL,NULL),(55,'Visits',NULL,NULL,NULL),(56,'Professionals','pixalere.admin.resources.form.specialist_type','TXTN','INTEGER'),(57,'Positions',NULL,NULL,NULL),(58,'Interfering Medications',NULL,NULL,NULL),(59,'Ostomy: Pouch Selection',NULL,NULL,NULL),(60,'Ostomy: Proximity Of Ostomy',NULL,NULL,NULL),(61,'Ostomy: Stool Colour',NULL,NULL,NULL),(62,'Ostomy: Profile',NULL,NULL,NULL),(63,'Ostomy: Application Change',NULL,NULL,NULL),(64,'Ostomy: Psycho Social Concern',NULL,NULL,NULL),(65,'Ostomy: Nutritional Status',NULL,NULL,NULL),(66,'Ostomy: Urine Colour',NULL,NULL,NULL),(67,'Ostomy: Skin Texture',NULL,NULL,NULL),(68,'Ostomy: Stoma Colour',NULL,NULL,NULL),(69,'Ostomy: Stool Discharge',NULL,NULL,NULL),(70,'Ostomy: Urine Type',NULL,NULL,NULL),(71,'Ostomy: Urine Quantity',NULL,NULL,NULL),(72,'Ostomy: Stool Consistency',NULL,NULL,NULL),(73,'Ostomy: Stool Quantity',NULL,NULL,NULL),(74,'Ostomy: Skin Appearance',NULL,NULL,NULL),(75,'Ostomy: Skin Contour',NULL,NULL,NULL),(76,'Ostomy: Stoma Moisture',NULL,NULL,NULL),(77,'Ostomy: Stoma Height',NULL,NULL,NULL),(78,'Ostomy: Stoma Shape',NULL,NULL,NULL),(79,'Ostomy: Teaching Learning',NULL,NULL,NULL),(80,'Ostomy: Ostomy Permanent',NULL,NULL,NULL),(81,'Ostomy: Device Present',NULL,NULL,NULL),(82,'Ostomy: Teaching Status',NULL,NULL,NULL),(83,'Ostomy: Irrigation',NULL,NULL,NULL),(84,'Ostomy: Stoma Construction',NULL,NULL,NULL),(85,'Ostomy Etiology',NULL,NULL,NULL),(86,'Wound Goals',NULL,NULL,NULL),(87,'Ostomy: Goals',NULL,NULL,NULL),(88,'Ostomy: Measurement',NULL,NULL,NULL),(89,'Ostomy: Patient Limitations',NULL,NULL,NULL),(90,'Incision: Exudate',NULL,NULL,NULL),(91,'Incision: S & S Infection',NULL,NULL,NULL),(92,'Incision: Status',NULL,NULL,NULL),(93,'Drain: Type Of Drain',NULL,NULL,NULL),(94,'Drain: Peri Drain Skin',NULL,NULL,NULL),(95,'Drain: Drainage',NULL,NULL,NULL),(96,'Drain: Odour',NULL,NULL,NULL),(97,'Pain',NULL,NULL,NULL),(98,'Reading Variable',NULL,NULL,NULL),(99,'Incision Closure Type',NULL,NULL,NULL),(100,'Incision Closure Status',NULL,NULL,NULL),(101,'Incision Exudate Amount',NULL,NULL,NULL),(102,'Drain Site',NULL,NULL,NULL),(103,'Laser',NULL,NULL,NULL),(104,'Braden Sensory',NULL,NULL,NULL),(105,'Braden Moisture',NULL,NULL,NULL),(106,'Braden Activity',NULL,NULL,NULL),(107,'Braden Mobility',NULL,NULL,NULL),(108,'Braden Nutrition',NULL,NULL,NULL),(109,'Braden Friction',NULL,NULL,NULL),(110,'Missing Limbs',NULL,NULL,NULL),(111,'Pain Assessment',NULL,NULL,NULL),(112,'Skin Assessment',NULL,NULL,NULL),(113,'Sensory',NULL,NULL,NULL),(114,'Proprioception',NULL,NULL,NULL),(115,'Deformities',NULL,NULL,NULL),(116,'Skin',NULL,NULL,NULL),(117,'Toes',NULL,NULL,NULL),(118,'Weight Bearing Status',NULL,NULL,NULL),(119,'Balance',NULL,NULL,NULL),(120,'Calf Muscle Function',NULL,NULL,NULL),(121,'Mobility Aids',NULL,NULL,NULL),(122,'Muscle Tone',NULL,NULL,NULL),(123,'Arches',NULL,NULL,NULL),(124,'Foot Active',NULL,NULL,NULL),(125,'Foot Passive',NULL,NULL,NULL),(126,'Sensation',NULL,NULL,NULL),(127,'Lab Done',NULL,NULL,NULL),(128,'Trancutaneous Pressures',NULL,NULL,NULL),(129,'E-Stim',NULL,NULL,NULL),(130,'Massage',NULL,NULL,NULL),(131,'Ultrasound',NULL,NULL,NULL),(132,'Hbot',NULL,NULL,NULL),(134,'PostOp Etiology',NULL,NULL,NULL),(135,'Tubes&Drains Etiology',NULL,NULL,NULL),(136,'Ostomy: Construction',NULL,NULL,NULL),(137,'Ostomy: Drainage',NULL,NULL,NULL),(138,'Ostomy: Mucocutaneous Margin',NULL,NULL,NULL),(139,'Ostomy: Peri-fistula Skin',NULL,NULL,NULL),(143,'Tubes&Drains: Drainage Amount Desc',NULL,NULL,NULL),(144,'Tubes&Drains: Drain Removed Reason',NULL,NULL,NULL),(146,'Wound Profile: Teaching Goals',NULL,NULL,NULL),(147,'Ostomy: Nutritional Status',NULL,NULL,NULL),(148,'Ostomy: Type of Ostomy',NULL,NULL,NULL),(149,'Ostomy Assess: Discharge Reason',NULL,NULL,NULL),(150,'Incision Assess: Discharge Reason',NULL,NULL,NULL),(151,'Incision Assess: Discharge Reason',NULL,NULL,NULL),(152,'Incision Assess: Postop Management',NULL,NULL,NULL),(153,'Drain Status',NULL,NULL,NULL),(154,'Incision Status',NULL,NULL,NULL),(155,'Ostomy Status',NULL,NULL,NULL),(157,'Burn: Wound',NULL,NULL,NULL),(158,'Burn: Exudate',NULL,NULL,NULL),(159,'Burn: Exudate Amount',NULL,NULL,NULL),(160,'Burn: Grafts',NULL,NULL,NULL),(161,'Burn: Donors',NULL,NULL,NULL),(167,'Incision Goals',NULL,NULL,NULL),(168,'Tube/Drain Goals',NULL,NULL,NULL),(169,'Inactivate Reasons',NULL,NULL,NULL),(170,'Timezone','','',''),(172,'Burn Etiology',NULL,NULL,NULL),(173,'Burn Goals',NULL,NULL,NULL);
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table_updates`
--

DROP TABLE IF EXISTS `table_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `components` int(11) NOT NULL DEFAULT '0',
  `configuration` int(11) NOT NULL DEFAULT '0',
  `information_popup` int(11) NOT NULL DEFAULT '0',
  `listdata` int(11) NOT NULL DEFAULT '0',
  `location_images` int(11) NOT NULL DEFAULT '0',
  `products` int(11) NOT NULL DEFAULT '0',
  `products_categories` int(11) NOT NULL DEFAULT '0',
  `resources` int(11) NOT NULL DEFAULT '0',
  `blue_model_images` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_updates`
--

LOCK TABLES `table_updates` WRITE;
/*!40000 ALTER TABLE `table_updates` DISABLE KEYS */;
INSERT INTO `table_updates` VALUES (1,0,1336582232,1332453286,1336581304,0,1336584572,1328039549,1336581304,0);
/*!40000 ALTER TABLE `table_updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `treatment_comments`
--

DROP TABLE IF EXISTS `treatment_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `treatment_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT '0',
  `wound_id` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `professional_id` int(11) DEFAULT '0',
  `user_signature` varchar(255) DEFAULT NULL,
  `body` text,
  `time_stamp` varchar(255) DEFAULT NULL,
  `assessment_id` int(11) NOT NULL DEFAULT '0',
  `wound_profile_type_id` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11)  NOT NULL DEFAULT '0',
  `deleted_reason` varchar(255) DEFAULT '',
  `delete_signature` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `id` (`id`),
  KEY `wound_id` (`wound_id`),
  KEY `professional_id` (`professional_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  KEY `assessment_id` (`assessment_id`),
  CONSTRAINT `treatment_comments_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `treatment_comments_ibfk_2` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `treatment_comments_ibfk_3` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE,
  CONSTRAINT `treatment_comments_ibfk_4` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `treatment_comments_ibfk_5` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `update_log`
--

DROP TABLE IF EXISTS `update_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `update_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `professional` text,
  `which` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `update_log`
--

LOCK TABLES `update_log` WRITE;
/*!40000 ALTER TABLE `update_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `update_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_account_regions`
--

DROP TABLE IF EXISTS `user_account_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_account_regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `treatment_location_id` int(11) DEFAULT '0',
  `user_account_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `treatment_location_id` (`treatment_location_id`),
  KEY `user_account_id` (`user_account_id`),
  CONSTRAINT `user_account_regions_ibfk_1` FOREIGN KEY (`treatment_location_id`) REFERENCES `lookup` (`id`),
  CONSTRAINT `user_account_regions_ibfk_2` FOREIGN KEY (`user_account_id`) REFERENCES `professional_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_account_regions`
--

LOCK TABLES `user_account_regions` WRITE;
/*!40000 ALTER TABLE `user_account_regions` DISABLE KEYS */;
INSERT INTO `user_account_regions` VALUES (7,612,7),(10,3268,7),(11,3269,7),(12,3270,7),(13,3271,7),(14,3272,7);
/*!40000 ALTER TABLE `user_account_regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_position`
--

DROP TABLE IF EXISTS `user_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referral_popup` int(11) DEFAULT '0',
  `recommendation_popup` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_position`
--

LOCK TABLES `user_position` WRITE;
/*!40000 ALTER TABLE `user_position` DISABLE KEYS */;
INSERT INTO `user_position` VALUES (1,0,1,'RN',1),(2,0,1,'LPN',0),(3,0,1,'UGN',0),(4,1,0,'CWC',0),(5,1,0,'WOCN',1),(7,1,1,'PTA',0),(8,0,0,'UC',0),(9,0,0,'MOA',0),(10,0,0,'NP',0),(11,0,0,'RD',0),(12,0,0,'RN(ET)',0),(13,0,0,'Dr',0),(14,0,0,'dr',0),(15,0,0,'X',0),(16,0,0,'Abc',0),(17,0,0,'table',0),(18,0,0,'abc',0),(19,0,0,'abc ',0),(20,0,0,'test',0),(21,0,0,'testname',0),(22,0,0,'testname',0),(23,0,0,'test test',0),(24,0,0,'nikita',0),(25,0,0,'lol',0),(26,0,0,'lol',0),(27,0,0,'test',0),(28,0,0,'nick',0),(29,0,0,'nick',0),(30,0,0,'',0),(31,0,0,'123',0),(32,1,0,'doc',0),(33,0,0,'abc',0),(34,0,0,'abc',0),(35,0,0,'abc',0),(36,0,0,'abs',0),(37,0,0,'absx',0),(38,0,0,'xxx',0),(39,0,0,'xxx',0),(40,0,0,'xzxz',0),(41,0,0,'cwscwc',0),(42,0,0,'xzxzx',0),(43,0,0,'aaa',0),(44,0,0,'zxcv',0),(45,0,0,'xczxc',0),(46,0,0,'aaa',0),(47,0,0,'wew',0),(48,0,0,'xzx',0),(49,0,0,'awq',0),(50,0,0,'132',0),(51,0,0,'xxx',0),(52,0,0,'23',0),(53,1,1,'Test',1),(54,0,0,'',0),(55,0,0,'wew',0),(56,1,1,'Nick',0),(57,0,0,'test',0),(58,1,1,'Test',0),(59,0,0,'',0),(60,0,0,'',0),(61,0,0,'',0),(62,0,0,'',0),(63,0,0,'RN++',1),(64,0,0,'Program Coordinator',1),(65,0,1,'RPN',1);
/*!40000 ALTER TABLE `user_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wound_assessment`
--

DROP TABLE IF EXISTS `wound_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '0',
  `patient_account_id` int(11) DEFAULT '0',
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `wound_id` int(11) DEFAULT NULL,
  `treatment_location_id` int(11) DEFAULT NULL,
  `has_comments` int(11) DEFAULT '0',
  `offline_flag` int(11) DEFAULT NULL,
  `professional_id` int(11) NOT NULL DEFAULT '0',
  `time_stamp` text,
  `user_signature` text,
  `visit` int(11) DEFAULT '0',
  `reset_visit` int(11) DEFAULT '0',
  `reset_reason` varchar(255) DEFAULT NULL,
  `data_entry_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `professional_id` (`professional_id`),
  KEY `id` (`id`),
  CONSTRAINT `wound_assessment_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `wound_assessment_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `wound_assessment_ibfk_3` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `wound_assessment_location`
--

DROP TABLE IF EXISTS `wound_assessment_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_assessment_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) NOT NULL DEFAULT '0',
  `alpha` varchar(25) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `discharge` int(1) DEFAULT NULL,
  `reactivate` int(1) DEFAULT NULL,
  `open_time_stamp` text,
  `close_time_stamp` text,
  `professional_id` int(11) DEFAULT '0',
  `deleted` int(11)  NOT NULL DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `data_entry_timestamp` varchar(255) DEFAULT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `id` (`id`),
  KEY `alpha` (`alpha`),
  KEY `professional_id` (`professional_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `wound_assessment_location_ibfk_1` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `wound_assessment_location_ibfk_7` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wound_assessment_location_ibfk_8` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `wound_assessment_location_ibfk_9` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wound_assessment_location`
--


--
-- Table structure for table `wound_etiology`
--

DROP TABLE IF EXISTS `wound_etiology`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_etiology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wound_profile_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `lookup_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `lookup_id` (`lookup_id`),
  KEY `wound_profile_id` (`wound_profile_id`),
  CONSTRAINT `wound_etiology_ibfk_1` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wound_etiology_ibfk_2` FOREIGN KEY (`lookup_id`) REFERENCES `lookup` (`id`),
  CONSTRAINT `wound_etiology_ibfk_3` FOREIGN KEY (`wound_profile_id`) REFERENCES `wound_profiles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wound_etiology_ibfk_4` FOREIGN KEY (`wound_profile_id`) REFERENCES `wound_profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wound_etiology`
--

--
-- Table structure for table `wound_goals`
--

DROP TABLE IF EXISTS `wound_goals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wound_profile_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `lookup_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `lookup_id` (`lookup_id`),
  KEY `wound_profile_id` (`wound_profile_id`),
  CONSTRAINT `wound_goals_ibfk_1` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wound_goals_ibfk_2` FOREIGN KEY (`lookup_id`) REFERENCES `lookup` (`id`),
  CONSTRAINT `wound_goals_ibfk_3` FOREIGN KEY (`wound_profile_id`) REFERENCES `wound_profiles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wound_goals_ibfk_4` FOREIGN KEY (`wound_profile_id`) REFERENCES `wound_profiles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `wound_location_details`
--

DROP TABLE IF EXISTS `wound_location_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_location_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alpha_id` int(11) DEFAULT NULL,
  `image` char(25) DEFAULT NULL,
  `box` int(11) DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `alpha_id` (`alpha_id`),
  CONSTRAINT `wound_location_details_ibfk_8` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `wound_profile_type`
--

DROP TABLE IF EXISTS `wound_profile_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_profile_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `alpha_type` varchar(5) DEFAULT '',
  `closed` int(1) DEFAULT '0',
  `closed_date` text,
  `deleted` int(11)  NOT NULL DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `professional_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `id` (`id`),
  CONSTRAINT `wound_profile_type_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `wound_profile_type_ibfk_2` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `wound_profiles`
--

DROP TABLE IF EXISTS `wound_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_flag` int(11) DEFAULT NULL,
  `offline_flag` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) NOT NULL DEFAULT '0',
  `last_update` text,
  `pressure_ulcer` int(11) DEFAULT NULL,
  `prev_prods` text,
  `referral` int(11) DEFAULT NULL,
  `reason_for_care` text,
  `active` int(11) NOT NULL DEFAULT '0',
  `professional_id` int(11) DEFAULT '0',
  `user_signature` varchar(255) DEFAULT NULL,
  `surgeon` text,
  `date_surgery` text,
  `operative_procedure_comments` text,
  `teaching_goals` int(11) DEFAULT NULL,
  `patient_limitations` text,
  `marking_prior_surgery` int(11) DEFAULT NULL,
  `type_of_ostomy` text,
  `review_done` int(11) DEFAULT '0',
  `burn_areas` text,
  `preliminary_tbsa` text,
  `final_tbsa` text,
  `tbsa_percentage` text,
  `deleted` int(11)  NOT NULL DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  `data_entry_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `id` (`id`),
  KEY `wound_id` (`wound_id`),
  KEY `professional_id` (`professional_id`),
  CONSTRAINT `wound_profiles_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `wound_profiles_ibfk_2` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wound_profiles_ibfk_5` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `wound_profiles_ibfk_6` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `wound_visits`
--

DROP TABLE IF EXISTS `wound_visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wound_visits` (
  `total` int(11) DEFAULT '0',
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wound_visits`
--

LOCK TABLES `wound_visits` WRITE;
/*!40000 ALTER TABLE `wound_visits` DISABLE KEYS */;
/*!40000 ALTER TABLE `wound_visits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wounds`
--

DROP TABLE IF EXISTS `wounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wounds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '0',
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `professional_id` int(11) NOT NULL DEFAULT '0',
  `wound_location` varchar(255) NOT NULL DEFAULT '',
  `wound_location_detailed` varchar(255) NOT NULL DEFAULT '',
  `wound_location_position` char(1) NOT NULL DEFAULT '',
  `image_name` varchar(255) NOT NULL DEFAULT '',
  `location_image_id` int(11) NOT NULL DEFAULT '0',
  `start_date` varchar(55) NOT NULL DEFAULT '',
  `discharge` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `startdate_signature` varchar(255) NOT NULL DEFAULT '',
  `closed_date` varchar(255) DEFAULT '',
  `deleted` int(11)  NOT NULL DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `professional_id` (`professional_id`),
  KEY `location_image_id` (`location_image_id`),
  CONSTRAINT `wounds_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `wounds_ibfk_2` FOREIGN KEY (`professional_id`) REFERENCES `professional_accounts` (`id`),
  CONSTRAINT `wounds_ibfk_3` FOREIGN KEY (`location_image_id`) REFERENCES `location_images` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-11-23 19:32:25

delete from configuration where config_setting='hideCreateTab';
delete from configuration where config_setting='integration_validation_secondary';
delete from configuration where config_setting='redirectOnPatientSearch';