update lookup set name='Paris' where id=681;
delete from configuration   where config_setting='acute';
delete from configuration   where config_setting='community';
insert into lookup (id,active,resource_id,name,other,orderby,solobox,update_access,category_id,locked) values(2132,1,7,'Uncertain',0,3,0,0,0,0);
update lookup set name='No Treatment Location' where id=3200;
update components set orderby=11 where id=433;
CREATE TABLE  config_yearly_quarters (id  int(11) NOT NULL auto_increment,primary key(id), quarter int default 0,start_date date, end_date date) ENGINE=InnoDB;
CREATE TABLE  dashboard_reports (id  int(11) NOT NULL auto_increment,primary key(id), email varchar(255) default 0,wound_type varchar(25), etiology_all int) ENGINE=InnoDB;
CREATE TABLE  dashboard_reports_etiology (id  int(11) NOT NULL auto_increment,primary key(id), dashboard_report_id int default 0,etiology_id int not null ) ENGINE=InnoDB;
CREATE TABLE  dashboard_reports_location (id  int(11) NOT NULL auto_increment,primary key(id), dashboard_report_id int default 0,treatment_location_id int not null) ENGINE=InnoDB;
alter table dashboard_reports_etiology add  constraint report_etiology_fk_id Foreign key (dashboard_report_id) REFERENCES dashboard_reports(id) ON DELETE CASCADE;
alter table dashboard_reports_location add  constraint report_location_fk_id Foreign key (dashboard_report_id) REFERENCES dashboard_reports(id) ON DELETE CASCADE;
CREATE TABLE report_storage (id int(11) NOT NULL auto_increment,primary key(id),date datetime(6), description text not null,filename varchar(255),report_name varchar(255)) ENGINE=InnoDB;
update products set cost=NULL where cost='';
alter table products add uid varchar(255);
alter table components modify id int auto_increment, add primary key(id);
alter table components add hide int;
update products set cost=null;
alter table products modify column cost float;
alter table patient_accounts add sig_temp varchar(255);
update patient_accounts set sig_temp=user_signature;
update patient_accounts set sig_temp=substring(sig_temp,6,length(sig_temp));
update patient_accounts set sig_temp=substring(sig_temp,0,13);
update patient_accounts set sig_temp=replace(sig_temp,' ','');
update patient_accounts set sig_temp=replace(sig_temp,'Jan','01');
update patient_accounts set sig_temp=replace(sig_temp,'Feb','02');
update patient_accounts set sig_temp=replace(sig_temp,'Mar','03');
update patient_accounts set sig_temp=replace(sig_temp,'Apr','04');
update patient_accounts set sig_temp=replace(sig_temp,'May','05');
update patient_accounts set sig_temp=replace(sig_temp,'Jun','06');
update patient_accounts set sig_temp=replace(sig_temp,'Jul','07');
update patient_accounts set sig_temp=replace(sig_temp,'Aug','08');
update patient_accounts set sig_temp=replace(sig_temp,'Sep','09');
update patient_accounts set sig_temp=replace(sig_temp,'Oct','10');
update patient_accounts set sig_temp=replace(sig_temp,'Nov','11');
update patient_accounts set sig_temp=replace(sig_temp,'Dec','12');
--update patient_accounts set sig_temp=' '+sig_temp;
--update patient_accounts set sig_temp=replace(sig_temp,' 1/','01/');
--update patient_accounts set sig_temp=replace(sig_temp,' 2/','02/');
--update patient_accounts set sig_temp=replace(sig_temp,' 3/','03/');
--update patient_accounts set sig_temp=replace(sig_temp,' 4/','04/');
--update patient_accounts set sig_temp=replace(sig_temp,' 5/','05/');
--update patient_accounts set sig_temp=replace(sig_temp,' 6/','06/');
--update patient_accounts set sig_temp=replace(sig_temp,' 7/','07/');
--update patient_accounts set sig_temp=replace(sig_temp,' 8/','08/');
--update patient_accounts set sig_temp=replace(sig_temp,' 9/','09/');
--update patient_accounts set sig_temp=replace(sig_temp,' ','');
--update patient_accounts set time_stamp = unix_timestamp(sig_temp+ '00:00:00') where sig_temp IS NOT NULL;
--alter table patient_accounts drop column sig_temp;
insert into lookup (id,active,resource_id,name,other,orderby,solobox,update_access,category_id,locked) values(3195,1,169,'Discharged: Healed',0,1,0,0,0,0);
insert into lookup (id,active,resource_id,name,other,orderby,solobox,update_access,category_id,locked) values(3196,1,169,'Discharged: Self Care',0,1,0,0,0,0);
insert into lookup (id,active,resource_id,name,other,orderby,solobox,update_access,category_id,locked) values(3197,1,169,'Discharged: Other',0,1,0,0,0,0);
alter table patient_accounts drop delete_reason;
alter table patient_accounts add delete_reason int;
update lookup set solobox=1 where name='Not Assessed';
CREATE TABLE auto_referrals (id int(11) NOT NULL auto_increment,primary key(id),alpha_id int(11),auto_referral_type varchar(255), initial_within_3weeks int,referral_id int) ENGINE=InnoDB;
alter table auto_referrals add  constraint auto_referral_fk_id Foreign key (referral_id) REFERENCES referrals_tracking(id) ON DELETE CASCADE;
INSERT INTO auto_referrals (referral_id,auto_referral_type) SELECT id,'Dressing Change Frequency' from referrals_tracking where auto_referral like '%7%' OR auto_referral like '%8%' OR auto_referral like '%10%' OR auto_referral like '%12%';
INSERT INTO auto_referrals (referral_id,auto_referral_type) SELECT id,'Antimicrobial' from referrals_tracking where auto_referral like '%12%' OR auto_referral like '%9%' OR auto_referral like '%10%'  OR auto_referral like '%11%'  ;
INSERT INTO auto_referrals (referral_id,auto_referral_type) SELECT id,'Heal Rate' from referrals_tracking where auto_referral like '%6%' OR auto_referral like '%8%'  OR auto_referral like '%11%' OR auto_referral like '%12%'  ;
INSERT INTO `lookup` ( `active`, `resource_id`, `name`, `other`, `orderby`, `title`, `solobox`, `update_access`, `report_value`, `category_id`, `icd9`, `icd10`, `cpt`, `locked`) VALUES (1,156,'Closed (healed) ',0,1,0,0,'0',NULL,0,NULL,NULL,NULL,0);
INSERT INTO `lookup` ( `active`, `resource_id`, `name`, `other`, `orderby`, `title`, `solobox`, `update_access`, `report_value`, `category_id`, `icd9`, `icd10`, `cpt`, `locked`) VALUES (1,156,'Patient self care',0,2,0,0,'0','',0,NULL,NULL,NULL,0);
INSERT INTO `lookup` ( `active`, `resource_id`, `name`, `other`, `orderby`, `title`, `solobox`, `update_access`, `report_value`, `category_id`, `icd9`, `icd10`, `cpt`, `locked`) VALUES (1,156,'Revision',0,4,0,0,'0','',0,NULL,NULL,NULL,0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'purgeReferrals','','','CHKB','','','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'hideVersionCode','','','CHKB','','','0');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'fiscalQuarter','','','DRP','3 months,12 weeks','3months,12weeks','3months');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,1,'fiscalStart','','','DRP','Jan 1st,Apr 1st','jan1st,apr1st','jan1st');
INSERT INTO `components` (`id`, `resource_id`, `field_name`, `field_type`, `component_type`, `flowchart`, `label_key`, `table_id`, `orderby`, `required`, `enabled_close`, `info_popup_id`, `allow_edit`, `validation`, `hide`, `ignore_info_popup`) VALUES (581,175,'wound_acquired','STRING','SALP',2,'pixalere.woundprofile.form.obtained',43,2,1,0,0,0,NULL,0,NULL);
CREATE TABLE `assessment_skin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `patient_id` int(11) DEFAULT NULL,
  `wound_id` int(11) DEFAULT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `alpha_id` int(11) DEFAULT NULL,
  `wound_profile_type_id` int(11) DEFAULT NULL,
  `full_assessment` int(11) DEFAULT '-1',
  `pain` int(11) DEFAULT NULL,
  `pain_comments` text,
  `length_cm` int(11) DEFAULT NULL,
  `length_mm` int(11) DEFAULT NULL,
  `width_cm` int(11) DEFAULT NULL,
  `width_mm` int(11) DEFAULT NULL,
  `depth_cm` int(11) DEFAULT NULL,
  `depth_mm` int(11) DEFAULT NULL,
  `discharge_reason` varchar(50) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `discharge_other` text,
  `recurrent` int(11) DEFAULT '0',
  `wound_date` text,
  `adjunctive` text,
  `adjunctive_other_text` text,
  `closed_date` text,
  `last_update` text,
  `site` text,
  `prev_alpha_id` int(11) DEFAULT NULL,
  `moved_reason` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `delete_signature` varchar(255) DEFAULT NULL,
  `deleted_reason` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `wound_id` (`wound_id`),
  KEY `assessment_id` (`assessment_id`),
  KEY `alpha_id` (`alpha_id`),
  KEY `wound_profile_type_id` (`wound_profile_type_id`),
  CONSTRAINT `assessment_skin_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `assessment_skin_ibfk_4` FOREIGN KEY (`assessment_id`) REFERENCES `wound_assessment` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_skin_ibfk_5` FOREIGN KEY (`wound_id`) REFERENCES `wounds` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_skin_ibfk_6` FOREIGN KEY (`wound_profile_type_id`) REFERENCES `wound_profile_type` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assessment_skin_ibfk_7` FOREIGN KEY (`alpha_id`) REFERENCES `wound_assessment_location` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `components` ( `resource_id`, `field_name`, `field_type`, `component_type`, `flowchart`, `label_key`, `table_id`, `orderby`, `required`, `enabled_close`, `info_popup_id`, `allow_edit`,  `ignore_info_popup`)
VALUES
	( 0, 'assessment_type', 'INTEGER', 'AT',50, 'pixalere.woundassessment.form.assessment_type', NULL,  1, 1,  0, 0, 0,1),
	( 181, 'status', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.wound_status', NULL,  2, 1, 0,  35, 1,0),
	( 32, 'discharge_reason', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.discharge_reason', NULL, 3, 1, 1,  0, 1,0),
	( 0, 'wound_date', 'STRING', 'DATE',50, 'pixalere.woundassessment.form.date_of_onset', NULL, 3, 1, 0,  34, 1,0),
	( 0, 'closed_date', 'STRING', 'DATE',50, 'pixalere.woundassessment.form.closure_date', NULL,  4, 1, 1,  0, 1,0),
	( 0, 'recurrent', 'INTEGER', 'YN',50, 'pixalere.woundassessment.form.recurrent', NULL,  6, 1, 0,  36, 1,0),
	( 97, 'pain', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.pain', NULL,  7, 1, 0,  27, 1,0),
	( 0, 'pain_comments', 'STRING', 'TXTA',50, 'pixalere.pain_comments', NULL,  7, 1, 0,  37, 1,0),
	( 0, 'length', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.length', NULL,  8, 1,  0, 28, 1,0),
	( 0, 'width', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.width', NULL,  9, 1, 0, 29, 1,0),
	( 0, 'depth', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.depth', NULL,  10, 1, 0,  30, 1,0),
	( 0, 'site', 'STRING', 'TXTA',50, 'pixalere.skinassessment.form.site', NULL,  16, 1, 0, 0,  1,0),
	( 0, 'products', 'STRING', 'PROD',50, 'pixalere.woundassessment.form.products', NULL,  23, 1,  0, 0, 1,0),
	( 0, 'body', 'STRING', 'TXTA',50, 'pixalere.woundassessment.form.treatment_comments', 16,  24, 1,  0, 0, 1,0),
	( 0, 'vac_start_date', 'DATE', 'DATE',50, 'pixalere.woundassessment.form.vacstart', 36,  27, 1, 0,  0, 1,0),
	( 0, 'vac_end_date', 'DATE', 'DATE',50, 'pixalere.woundassessment.form.vacend', 36,  28, 1, 0, 0,  1,0),
	( 98, 'therapy_setting', 'INTEGER', 'RADO',50, 'pixalere.woundassessment.form.reading_group', 36,  28, 1,  0, 0, 1,0),
	( 40, 'pressure_reading', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.pressure_reading', 36,  29, 1,  0, 0, 1,0),
	( 38, 'cs_show', 'INTEGER', 'YN',50, 'pixalere.treatment.form.cs_done', NULL,  32, 1, 0, 0, 1,0),
	( 38, 'csresult', 'STRING', 'CHKB',50, 'pixalere.woundassessment.form.c_s_result', 27,  34, 1, 0, 0, 1,0),
	( 0, 'antibiotics', 'STRING', 'MDDA',50, 'pixalere.treatment.form.antibiotics', 38,  35, 1, 0, 0, 1,0),
	( 17, 'adjunctive', 'STRING', 'CHKB',50, 'pixalere.woundassessment.form.adjunctive', 27,  37, 1, 0,  0, 1,0),
	( 0, 'images', 'STRING', 'IMG',50, 'pixalere.woundassessment.form.images', NULL,  38, 1, 0,  0, 0,0),
	( 0, 'review_done', 'INTEGER', 'YN',50, 'pixalere.treatment.review_done', 36,  960, 0, 0, 0,  0,0),
	( 0, 'referral', 'STRING', 'REF',50, 'pixalere.referral.form.title_fc', NULL,  998, 0, 0, 0,  0,   1),
	( 0, 'comments', 'STRING', 'COMM',50, 'pixalere.woundassessment.form.contains_comments', NULL,  999, 0,  0, 0, 0,0),
	( 0, 'maintenance', 'STRING', 'ADM',50, 'pixalere.viewer.assessment_updates', NULL,  1000, 0, 0, 0,  0,0),
	( 0, 'pdf', 'STRING', 'PDF',50, 'pixalere.viewer.pdf', NULL,  1010,0,0, 0, 0,    0);
insert into resources (id,name) values (180,'Skin Etiology');
insert into resources (id,name) values (181,'Skin Status');
insert into resources (id,name) values (182,'Skin Discharge Reason');
insert into resources (id,name) values (183,'Skin Goals');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (1,1,0,'allowSkin','',' ','CHKB','',' ','1','0');
INSERT INTO `configuration` ( `category`, `orderby`, `selfadmin`, `config_setting`, `enable_component_rule`, `enable_options_rule`, `component_type`, `component_options`, `component_values`, `current_value`, `readonly`)
VALUES
	( 7, 15, 0, 'skinActive', '', '', 'INPN', '', '', '3283', 1),
	( 7, 16, 0, 'skinClosed', '', '', 'INPN', '', '', '3284', 1);
alter table assessment_product add skin_assessment_id int;
