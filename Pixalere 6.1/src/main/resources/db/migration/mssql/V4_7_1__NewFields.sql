CREATE TABLE components_tmp ( id int IDENTITY NOT NULL, resource_id int, field_name varchar(50), field_type varchar(11), component_type varchar(8) DEFAULT '', flowchart int, label_key varchar(255), table_id int, column_layout int, orderby int, label_style varchar(255), required int, enabled_close int, details_popup int,info_popup_id int,  allow_edit int DEFAULT NULL, validation nvarchar(max), ignore_info_popup int DEFAULT '0', primary key(id));
set identity_insert components_tmp ON
insert into components_tmp (id,resource_id,field_name,field_type,component_type,flowchart,label_key,table_id,column_layout,orderby,label_style,required,enabled_close,details_popup,info_popup_id,allow_edit,validation,ignore_info_popup) select id,resource_id,field_name,field_type,component_type,page,label_key,section,column_layout,orderby,label_style,required,enabled_close,details_popup,info_popup_id,allow_edit,validation,ignore_info_popup from components;
set identity_insert components_tmp Off
drop table components;
if not exists (select * from information_schema.columns where table_name = 'patient_accounts' and column_name = 'time_stamp') begin alter table patient_accounts add time_stamp datetime; end
alter table patient_accounts add funding_source int;
EXEC sp_rename 'professional_accounts.access_programs','access_listdata', 'COLUMN';
EXEC sp_rename 'professional_accounts.access_product_info','access_superadmin', 'COLUMN';
EXEC sp_rename 'patient_accounts.third_phn','phn3', 'COLUMN';
EXEC sp_rename 'patient_accounts.secondary_phn','phn2', 'COLUMN';
EXEC sp_rename 'components_tmp','components';
EXEC sp_rename 'listdata','lookup';
EXEC sp_rename 'lookup.item_en','name', 'COLUMN';
EXEC sp_rename 'lookup.status','active', 'COLUMN';
