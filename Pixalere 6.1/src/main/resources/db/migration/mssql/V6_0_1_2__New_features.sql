alter table assessment_wound drop column cs_show;
alter table assessment_drain drop column cs_show;
alter table assessment_incision drop column cs_show;
alter table assessment_ostomy drop column cs_show;
update components set field_name='cs_done' where field_name='cs_show';
alter table library add url varchar(255);
update library set media_type='pdf';
alter table physical_exam drop column blood_sugar;
alter table physical_exam drop column blood_sugar_meal;
alter table physical_exam drop column blood_sugar_date;
--clean up configuration
delete from configuration where config_setting='useDateCalendar' or config_setting='showAssessDoneIndicator' or config_setting='showHistoryOnAssessment';
delete from configuration where config_setting='showNursingVisitFrequency';
update referrals_tracking set patient_id=(select top 1 patient_id from patient_accounts where id=referrals_tracking.patient_account_id);
set identity_insert limb_adv_assessment On
insert into limb_adv_assessment (id,patient_id,last_update,active,left_dorsalis_pedis_doppler,left_pain_assessment,right_pain_assessment,right_dorsalis_pedis_doppler,left_posterior_tibial_doppler,right_posterior_tibial_doppler,left_interdigitial_doppler,right_interdigitial_doppler,ankle_brachial_lab,ankle_brachial_date,left_tibial_pedis_ankle_brachial,right_tibial_pedis_ankle_brachial,left_dorsalis_pedis_ankle_brachial,right_dorsalis_pedis_ankle_brachial,left_ankle_brachial,right_ankle_brachial,toe_brachial_lab,toe_brachial_date,left_toe_pressure,right_toe_pressure,left_brachial_pressure,right_brachial_pressure,transcutaneous_lab,transcutaneous_date,left_transcutaneous_pressure,right_transcutaneous_pressure,left_sensation,right_sensation,left_score_sensation,right_score_sensation,data_entry_timestamp,user_signature,professional_id) select id,patient_id,last_update,active,left_dorsalis_pedis_doppler,right_dorsalis_pedis_doppler,left_posterior_tibial_doppler,right_posterior_tibial_doppler,left_interdigitial_doppler,right_interdigitial_doppler,left_pain_assessment,right_pain_assessment,ankle_brachial_lab,ankle_brachial_date,left_tibial_pedis_ankle_brachial,right_tibial_pedis_ankle_brachial,left_dorsalis_pedis_ankle_brachial,right_dorsalis_pedis_ankle_brachial,left_ankle_brachial,right_ankle_brachial,toe_brachial_lab,toe_brachial_date,left_toe_pressure,right_toe_pressure,left_brachial_pressure,right_brachial_pressure,transcutaneous_lab,transcutaneous_date,left_transcutaneous_pressure,right_transcutaneous_pressure,left_sensation,right_sensation,left_score_sensation,right_score_sensation,data_entry_timestamp,user_signature,professional_id from limb_assessment where exists (select id from patients where id=limb_assessment.patient_id);
set identity_insert limb_adv_assessment Off
update assessment_comments set referral_id=null where exists(select id from referrals_tracking where referrals_tracking.id=assessment_comments.referral_id and assessment_comments.patient_id!=referrals_tracking.patient_id);
update limb_adv_assessment set left_foot_deformities=(select top 1 left_foot_deformities from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc );
update limb_adv_assessment set right_foot_deformities=(select top 1 right_foot_deformities from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc );
update limb_adv_assessment set left_foot_skin=(select top 1 left_foot_skin from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc );
update limb_adv_assessment set right_foot_skin=(select top 1 right_foot_skin from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc );
update limb_adv_assessment set left_foot_toes=(select top 1 left_foot_toes from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc );
update limb_adv_assessment set right_foot_toes=(select top 1 right_foot_toes from foot_assessment where patient_id=limb_adv_assessment.patient_id and active=1 order by id desc );
alter table limb_assessment drop column doppler_lab;
alter table limb_assessment drop column left_dorsalis_pedis_doppler;
alter table limb_assessment drop column right_dorsalis_pedis_doppler;
alter table limb_assessment drop column left_posterior_tibial_doppler;
alter table limb_assessment drop column right_posterior_tibial_doppler;
alter table limb_assessment drop column left_interdigitial_doppler;
alter table limb_assessment drop column right_interdigitial_doppler;
alter table limb_assessment drop column ankle_brachial_lab;
alter table limb_assessment drop column ankle_brachial_date;
alter table limb_assessment drop column left_tibial_pedis_ankle_brachial;
alter table limb_assessment drop column right_tibial_pedis_ankle_brachial;
alter table limb_assessment drop column left_dorsalis_pedis_ankle_brachial;
alter table limb_assessment drop column right_dorsalis_pedis_ankle_brachial;
alter table limb_assessment drop column left_ankle_brachial;
alter table limb_assessment drop column right_ankle_brachial;
alter table limb_assessment drop column toe_brachial_lab;
alter table limb_assessment drop column toe_brachial_date;
alter table limb_assessment drop column left_toe_pressure;
alter table limb_assessment drop column right_toe_pressure;
alter table limb_assessment drop column left_proprioception;
alter table limb_assessment drop column right_proprioception;
alter table limb_assessment drop column left_brachial_pressure;
alter table limb_assessment drop column right_brachial_pressure;
alter table foot_assessment drop column left_foot_deformities;
alter table foot_assessment drop column right_foot_deformities;
alter table foot_assessment drop column left_foot_skin;
alter table foot_assessment drop column right_foot_skin;
alter table foot_assessment drop column left_foot_toes;
alter table foot_assessment drop column right_foot_toes;
alter table limb_assessment drop column transcutaneous_lab;
alter table limb_assessment drop column transcutaneous_date;
alter table limb_assessment drop column left_transcutaneous_pressure;
alter table limb_assessment drop column right_transcutaneous_pressure;
alter table limb_assessment drop column left_sensation;
alter table limb_assessment drop column right_sensation;
alter table limb_assessment drop column left_score_sensation;
alter table limb_assessment drop column right_score_sensation;
exec sp_rename 'limb_assessment','limb_basic_assessment';
alter table referrals_tracking add assigned_to int default -1;
alter table professional_accounts add access_ccac_reporting int default 0;
CREATE TABLE diagnostic_investigation (
  id int NOT NULL identity,
  patient_id int NOT NULL,
    active int NOT NULL,
   blood_sugar float,
  blood_sugar_meal varchar(6),
  blood_sugar_date date,
albumin int ,
  pre_albumin int ,
  albumin_date varchar(max),
  prealbumin_date varchar(max),
    hga1c int,
  professional_id int,
  creation_date varchar(max),
  last_modified_date varchar(max),
  user_signature varchar(50),
  review_done int DEFAULT '0',
  deleted int  NOT NULL DEFAULT '0',
  delete_signature varchar(255) DEFAULT NULL,
  deleted_reason varchar(255) DEFAULT NULL,
  data_entry_timestamp varchar(255),
  PRIMARY KEY (id),
  CONSTRAINT di_ibfk_3 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id)
  ) ;
