update assessment_drain set drain_removed_date='' where convert(bigint,drain_removed_date)>1392353966 OR convert(bigint,drain_removed_date)<-3783006034 and status=(select current_value from configuration where config_setting='drainClosed');
update assessment_incision set closed_date='' where convert(bigint,closed_date)>1392353966 OR convert(bigint,closed_date)<-3783006034 and status=(select current_value from configuration where config_setting='incisionClosed');
update wounds set closed_date=(select top 1 close_time_stamp from wound_assessment_location where discharge=1 and wound_id=wounds.id order by close_time_stamp desc) where closed_date=null and status='Closed';
update configuration set current_value=0 where config_setting='enableDataExtraction';
with cte as (select row_number() over (partition by patient_id order by id) as patient , flag from patient_accounts) update cte set flag = case when patient = 1 then 1 else 0 end;
CREATE TABLE auto_referrals (id int NOT NULL identity,primary key(id),alpha_id int,auto_referral_type varchar(255), initial_within_3weeks int,referral_id int);
alter table auto_referrals add  constraint auto_referrals_fk_id Foreign key (referral_id) REFERENCES referrals_tracking(id) ON DELETE CASCADE;
alter table wound_profiles add root_cause_addressed_comments  varchar(max);