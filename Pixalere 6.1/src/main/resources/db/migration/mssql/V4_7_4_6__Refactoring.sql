update assessment_eachwound set closed_date='' where closed_date LIKE '2%' or closed_date LIKE '4%';
update assessment_eachwound set closed_date=(select close_time_stamp from wound_assessment_location where id=assessment_eachwound.alpha_id) where closed_date='' and status=257;
update wound_assessment_location  set close_time_stamp=(select top 1 closed_date from assessment_eachwound where alpha_id=wound_assessment_location.id and closed_date!='' and closed_date>0);
update wound_assessment_location  set close_time_stamp=(select top 1 closed_date from assessment_ostomy where alpha_id=wound_assessment_location.id and closed_date!='' and closed_date>0);
update wound_assessment_location  set close_time_stamp=(select top 1 closed_date from assessment_drain where alpha_id=wound_assessment_location.id and closed_date!='' and closed_date>0);
update wound_assessment_location  set close_time_stamp=(select top 1 closed_date from assessment_incision where alpha_id=wound_assessment_location.id and closed_date!='' and closed_date>0);
