update wal set close_time_stamp = (select top 1 last_update from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and wal.close_time_stamp='' and exists(select top 1 closed_date from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionClosed') order by last_update desc);
update wal set close_time_stamp = (select top 1 last_update from assessment_ostomy ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='ostomyClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and wal.close_time_stamp='' and exists(select top 1 closed_date from assessment_ostomy ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='ostomyClosed') order by last_update desc);
update wal set close_time_stamp = (select top 1 last_update from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and wal.close_time_stamp='' and exists(select top 1 closed_date from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainClosed') order by last_update desc);
update wal set close_time_stamp = (select top 1 last_update from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by last_update desc) from wound_assessment_location wal where close_time_stamp<open_time_stamp and wal.discharge=1 and wal.close_time_stamp!=''  and exists(select top 1 closed_date from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by last_update desc)
update wal set close_time_stamp = (select top 1 last_update from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainClosed') order by last_update desc) from wound_assessment_location wal where close_time_stamp<open_time_stamp and wal.discharge=1 and wal.close_time_stamp!=''  and exists(select top 1 closed_date from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainClosed') order by last_update desc)
update wal set close_time_stamp = (select top 1 last_update from assessment_ostomy ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='ostomyClosed') order by last_update desc) from wound_assessment_location wal where close_time_stamp<open_time_stamp and wal.discharge=1 and wal.close_time_stamp!=''  and exists(select top 1 closed_date from assessment_ostomy ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='ostomyClosed') order by last_update desc)
update wal set close_time_stamp = (select top 1 last_update from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionClosed') order by last_update desc) from wound_assessment_location wal where close_time_stamp<open_time_stamp and wal.discharge=1 and wal.close_time_stamp!=''  and exists(select top 1 closed_date from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionClosed') order by last_update desc)
alter table assessment_wound add target_discharge_date date;
alter table assessment_ostomy add target_discharge_date date;
alter table assessment_drain add target_discharge_date date;
alter table assessment_skin add target_discharge_date date;
alter table assessment_incision add target_discharge_date date;
alter table assessment_burn add target_discharge_date date;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'target_discharge_date', 'DATE', 'DATE',3, 'pixalere.woundassessment.form.target_discharge_date', NULL,  1,  1, 0,  0, 0, NULL, 0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'allowMultipleGoals','',' ','CHKB','',' ','0','0');
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart ) VALUES( 0, 'discharge_planning_initiated', 'INTEGER', 'YN',1, 'pixalere.nursingcareplan.form.discharge_planning_initiated', NULL,  64,  1, 0,  0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart ) VALUES( 0, 'chronic_disease_initiated', 'INTEGER', 'YN',1, 'pixalere.nursingcareplan.form.chronic_disease_initiated', NULL,  66,  1, 0,  0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart ) VALUES( 191, 'ccac_questions', 'STRING', 'CHKB',3, 'pixalere.nursingcareplan.form.ccac_questions', 27,  37,  1, 0,  0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart ) VALUES( 0, 'quality_of_life_addressed', 'INTEGER', 'YN',1, 'pixalere.nursingcareplan.form.quality_of_life_addressed', NULL,  68,  1, 0,  0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart ) VALUES( 0, 'barriers_addressed', 'INTEGER', 'YNWHY',1, 'pixalere.nursingcareplan.form.barriers_addressed', NULL,  70,  1, 0, 0,  0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart ) VALUES( 0, 'root_cause_addressed', 'INTEGER', 'YNWHY',2, 'pixalere.nursingcareplan.form.root_cause_addressed', NULL,  45,  1, 0,  0, 0, NULL, 1);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'system_barriers', 'INTEGER', 'YNWHY',1, 'pixalere.nursingcareplan.form.system_barriers', NULL,  72, 1, 0, 0,  0, NULL, 1);
set identity_insert resources On
insert into resources (id,name)  values(191,'CCAC Questions');
set identity_insert resources Off
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Wound Therapy Initiated: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Wound Therapy Reassessed: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Referral Initiated for Long-Term Pressure: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Client has obtained and is adhering to pressure redistribution system: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Pressure Redistribution Initiated: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Compression Therapy Initiated: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Referral Initiated for Long-Term Compression System: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Client is independent with Long-Term Compression System by 14 weeks: Yes',resource_id from lookup order by id desc;
set identity_insert resources On
insert into resources  (id,name) values(192,'Clinical Barriers');
set identity_insert resources Off
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Tissue Breakdown',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Allergy',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Other',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Amputation',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,192);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Infection',resource_id from lookup order by id desc;
--insert into lookup (active,resource_id) values(1,6);
--insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Lack of supply/equipment',resource_id from lookup order by id desc;
--insert into lookup (active,resource_id) values(1,6);
--insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Delay in available community support',resource_id from lookup order by id desc;
set identity_insert resources On
insert into resources (id,name) values(193,'IVTherapy Barriers');
set identity_insert resources Off
insert into lookup (active,resource_id) values(1,193);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Peripheral',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,193);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Central Venous',resource_id from lookup order by id desc;
alter table patient_profiles add discharge_planning_initiated int default 0;
alter table patient_profiles add chronic_disease_initiated int default 0;
alter table patient_profiles add barriers_addressed int default 0;
alter table patient_profiles add barriers_addressed_comments  varchar(max);
alter table wound_profiles add root_cause_addressed int default 0;
alter table patient_profiles add quality_of_life_addressed int default 0;
alter table limb_adv_assessment add referral_vascular_assessment int default 0;
alter table patient_profiles add quality_of_life_addressed_comments varchar(max);
alter table patient_profiles add system_barriers_comments varchar(max);
update referrals_tracking set assigned_to=-1;
delete from components where id=92;
alter table user_position add orderby int default 0;
insert into lookup (active,resource_id) values(1,6);
insert into lookup_l10n (language_id,lookup_id,name)  select top 1 1,id,'Not Adherent to Care Plan' from lookup order by id desc;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'referral_vascular_assessment', 'INTEGER', 'YN',11, 'pixalere.limb.form.referral_for_vascular_assessment', NULL,  70, 1, 0, 0, 0, NULL, 1);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'allowReferralByPositions','',' ','CHKB','',' ','1','0');
cREATE TABLE ccac_report (
  id int NOT NULL  identity,
  active int NOT NULL DEFAULT '0',
  patient_id int NOT NULL,
patient_account_id int NOT NULL,
  professional_id int NOT NULL,
  patient_profile_id int NOT NULL,
  wound_profile_id int NOT NULL,
  limb_assessment_id int ,
  alpha_id int NOT NULL,
  assessment_id int NOT NULL,
  wound_id int NOT NULL ,
  type_of_report varchar(25) default '',
  week int  default '0',
  wound_status varchar(25) default '',
  discharge varchar(25) default '',
    suitable_transfer varchar(25) default '',
change_in_wound varchar(25) default '',
wound_change_comments varchar(255) default '',
wound_progress varchar(25) default '',
  date_of_transfer date ,
    already_retrieved int default '0',
  correct_pathway_confirmed varchar(5) default '',
  comments varchar(max) ,
  time_stamp datetime,
  PRIMARY KEY (id),
  CONSTRAINT ccac_ibfk_1 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id),
  CONSTRAINT ccac_ibfk_4 FOREIGN KEY (patient_account_id) REFERENCES patient_accounts (id),
  CONSTRAINT ccac_ibfk_2 FOREIGN KEY (professional_id) REFERENCES professional_accounts (id),
  CONSTRAINT ccac_ibfk_3 FOREIGN KEY (wound_id) REFERENCES wounds (id)
);
update professional_accounts set access_ccac_reporting=1 where id=7;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'allowCCACReporting','',' ','CHKB','',' ','0','0');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacClientIndependent','allowCCACReporting==1',' ','INPT','',' ','3275','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacLongTermCompressionSystem','allowCCACReporting==1',' ','INPT','',' ','3274','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacCompressionTherapyInitiated','allowCCACReporting==1',' ','INPT','',' ','3273','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacPressureRedistributionInitiated','allowCCACReporting==1',' ','INPT','',' ','3272','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacAdheringPressureRedistribution','allowCCACReporting==1',' ','INPT','',' ','3271','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacReferralLongTermPressure','allowCCACReporting==1',' ','INPT','',' ','3270','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacWoundTherapyReassessed','allowCCACReporting==1',' ','INPT','',' ','3269','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacWoundTherapyInitiated','allowCCACReporting==1',' ','INPT','',' ','3268','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacArterialLegUlcer','allowCCACReporting==1',' ','INPT','',' ','134,138','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacDiabeticFootUlcer','allowCCACReporting==1',' ','INPT','',' ','3268','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacVenousLegUlcer','allowCCACReporting==1',' ','INPT','',' ','318,404','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacPressureUlcer','allowCCACReporting==1',' ','INPT','',' ','142,1273,1274,1275,1276','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacMalignant','allowCCACReporting==1',' ','INPT','',' ','135','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacTrauma','allowCCACReporting==1',' ','INPT','',' ','139','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacMaintenance','allowCCACReporting==1',' ','INPT','',' ','1082','1');
update lookup set locked=1 where id=135;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacRedistributionMeasures','allowCCACReporting==1',' ','INPT','',' ','3278','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacWoundRelatedSymptoms','allowCCACReporting==1',' ','INPT','',' ','3279','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacNotAdherentCarePlan','allowCCACReporting==1',' ','INPT','',' ','3280','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacClinical','allowCCACReporting==1',' ','INPT','',' ','3281,3282,3283,3284','1');
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (101,1,0,'ccacSystemBarriers','allowCCACReporting==1',' ','INPT','',' ','3285,3286','1');
update lookup set locked=1 where id=134;
update lookup set locked=1 where id=182;
update lookup set locked=1 where id=139;
update lookup set locked=1 where id=138;
update lookup set locked=1 where id=1273;
update lookup set locked=1 where id=1274;
update lookup set locked=1 where id=1275;
update lookup set locked=1 where id=1276;
update lookup set locked=1 where id=318;
update lookup set locked=1 where id=404;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Pressure Redistribution Measures Initiated: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Wound Related Symptoms Managed: Yes',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,191);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Tissue Breakdown',resource_id from lookup order by id desc;
--insert into lookup (active,resource_id) values(1,6);
--insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Allergy',resource_id from lookup order by id desc;
--insert into lookup (active,resource_id) values(1,6);
--insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Amputation',resource_id from lookup order by id desc;
--insert into lookup (active,resource_id) values(1,6);
--insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Infection',resource_id from lookup order by id desc;
--insert into lookup (active,resource_id) values(1,6);
--insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Lack of supply/equipment',resource_id from lookup order by id desc;
--insert into lookup (active,resource_id) values(1,6);
--insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Delay in available community support',resource_id from lookup order by id desc;
update resources set name='Patient Limitations' where id=89;
CREATE TABLE external_referrals (
  id int NOT NULL identity,
  patient_profile_id int NOT NULL,
    external_referral_id int NOT NULL,
    arranged_id int NOT NULL,

  PRIMARY KEY (id),
  CONSTRAINT extreferral_ibfk_4 FOREIGN KEY (patient_profile_id) REFERENCES patient_profiles (id) ON DELETE CASCADE
);
set identity_insert resources On
insert into resources (id,name) values (195,'External Referrals');
insert into resources (id,name) values (196,'Arranged');
set identity_insert resources Off
set identity_insert resources On
insert into resources (id,name) values (198,'Advanced Pain Assessment');
insert into resources (id,name) values (199,'Advanced Limb Temperature');
set identity_insert resources Off
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'external_referrals', 'STRING', 'MDRP',1, 'pixalere.patientprofile.form.external_referral', 61,  75,  1, 0, 0, 0, NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (198,'left_pain_assessment','STRING','CHKB',52,'pixalere.patientprofile.form.left_pain_assessment',NULL,22,0,0,46,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (198,'right_pain_assessment','STRING','CHKB',52,'pixalere.patientprofile.form.right_pain_assessment',NULL,23,0,0,0,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (199,'left_temperature_leg','INTEGER','DRP',52,'pixalere.patientprofile.form.left_temperature_leg',NULL,28,0,0,0,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (199,'right_temperature_leg','INTEGER','DRP',52,'pixalere.patientprofile.form.right_temperature_leg',NULL,29,0,0,0,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (199,'left_temperature_foot','INTEGER','DRP',52,'pixalere.patientprofile.form.left_temperature_foot',NULL,30,0,0,0,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (199,'right_temperature_foot','INTEGER','DRP',52,'pixalere.patientprofile.form.right_temperature_foot',NULL,31,0,0,0,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (199,'left_temperature_toes','INTEGER','DRP',52,'pixalere.patientprofile.form.left_temperature_toes',NULL,32,0,0,0,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (199,'right_temperature_toes','INTEGER','DRP',52,'pixalere.patientprofile.form.right_temperature_toes',NULL,33,0,0,0,1,NULL, 0);
	INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation, hide_flowchart) VALUES (0,'limb_comments','STRING','TXT',52,'pixalere.patientprofile.form.limb_comments',NULL,35,0,0,0,1,NULL, 0);
update configuration set component_type='CHKB' where component_type='CHK';
alter table account_assignments alter column time_stamp varchar(255) ;
alter table assessment_comments alter column time_stamp varchar(255);
alter table logs_access alter column time_stamp  varchar(max);
alter table wound_assessment_location alter column open_time_stamp varchar(255);
alter table wound_assessment_location alter column close_time_stamp varchar(255);
update limb_adv_assessment set ankle_brachial_date='' where ankle_brachial_date LIKE '-%';
update limb_adv_assessment set ankle_brachial_date='' where ankle_brachial_date LIKE '7%';
