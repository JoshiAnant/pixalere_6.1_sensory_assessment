 alter table patient_profiles drop column investigation;
                alter table patient_profiles drop column investigations_show;
                alter table assessment_wound drop column wound_base;
                alter table assessment_wound drop column wound_base_percentage;
                alter table assessment_wound drop column sinus_tract_array;
                alter table assessment_wound drop column undermining_array;
                alter table assessment_wound drop column num_sinus_tracts;
                alter table assessment_wound drop column num_undermining;
                alter table assessment_ostomy drop column mucocutaneous_margin_num;
                alter table assessment_ostomy drop column mucocutaneous_margin_array;
create function isnumeric(val varchar(1024)) returns tinyint(1) deterministic return val regexp '^(-|\\+)?([0-9]+\\.[0-9]*|[0-9]*\\.[0-9]+|[0-9]+)$'; 
update lookup_l10n set name=REPLACE(name,'<br>:','<br/>');
update components set hide_flowchart=0;
update lookup set active=0 where exists (select id from lookup_l10n where lookup_id=lookup.id and name='');
update user_position set active=0 where title='';
update components set table_id=NULL where field_name='cs_show';
update assessment_product set quantity=0 where ISNUMERIC(quantity)=0;
update wound_assessment set visit=NULL where visit=0;
update wounds set status=NULL where status='' or status='Open';
alter table products add uid varchar(255);
update lookup_l10n set name='Meditech' where lookup_id=681;
delete from configuration   where config_setting='acute';
delete from configuration   where config_setting='cfommunity';
update lookup_l10n set name='No Treatment Location' where lookup_id=3200;
update components set orderby=11 where id=433;
CREATE TABLE  config_yearly_quarters (id  int NOT NULL identity,primary key(id), quarter int default 0,start_date date, end_date date);
CREATE TABLE  dashboard_reports (id  int NOT NULL identity,primary key(id), email varchar(255) default 0,wound_type varchar(25), etiology_all int);
CREATE TABLE  dashboard_reports_etiology (id  int NOT NULL identity,primary key(id), dashboard_report_id int default 0,etiology_id int not null );
CREATE TABLE  dashboard_reports_location (id  int NOT NULL identity,primary key(id), dashboard_report_id int default 0,treatment_location_id int not null);
alter table dashboard_reports_etiology add  constraint report_etiology_fk_id Foreign key (dashboard_report_id) REFERENCES dashboard_reports(id) ON DELETE CASCADE;
alter table dashboard_reports_location add  constraint report_location_fk_id Foreign key (dashboard_report_id) REFERENCES dashboard_reports(id) ON DELETE CASCADE;
CREATE TABLE report_storage (id int NOT NULL identity,primary key(id),date datetime, description varchar(255) not null,filename varchar(255),report_name varchar(255));
update products set cost=NULL where cost='';
update products set cost=null where ISNumeric(cost)=0;
alter table products alter column cost float;
update components set hide_flowchart=0 where hide_flowchart IS NULL;
insert into lookup (active,resource_id) values(1,169);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Discharged: Healed',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,169);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Discharged: Self Care',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,169);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Discharged: Other',resource_id from lookup order by id desc;
alter table patient_accounts drop column delete_reason;
alter table patient_accounts add delete_reason int;
update lookup set solobox=1 where exists (select id from lookup_l10n where lookup_id=lookup.id and name='Not Assessed');
alter table referrals_tracking drop column auto_referral;
alter table referrals_tracking drop column alpha_type;
update referrals_tracking set active=1;
update professional_accounts set position_id=(select top 1 id from user_position where professional_accounts.role=user_position.title order by id desc);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value)  select top 1 0,1,0,'patient_position_id','',' ','INPT','',' ',id from user_position where title='Patient';
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, column_layout, orderby, label_style, required, enabled_close, details_popup, info_popup_id, allow_edit, validation, hide_flowchart, ignore_info_popup)
VALUES
	( 0, 'assessment_type', 'INTEGER', 'AT',50, 'pixalere.woundassessment.form.assessment_type', NULL, 1, 1, 'normal', 1, 0, 0, 0, 0, NULL, 0, 1),
	( 181, 'status', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.wound_status', NULL, 1, 2, 'normal', 1, 0, 0, 35, 1, NULL, 0, 0),
	( 32, 'discharge_reason', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.discharge_reason', NULL, 5,50, 'normal', 1, 1, 0, 0, 1, NULL, 0, 0),
	( 0, 'wound_date', 'STRING', 'DATE',50, 'pixalere.woundassessment.form.date_of_onset', NULL, 1,50, 'normal', 1, 0, 0, 34, 1, NULL, 0, 0),
	( 0, 'closed_date', 'STRING', 'DATE',50, 'pixalere.woundassessment.form.closure_date', NULL, 1, 4, 'normal', 1, 1, 0, 0, 1, NULL, 0, 0),
	( 0, 'recurrent', 'INTEGER', 'YN',50, 'pixalere.woundassessment.form.recurrent', NULL, 1, 6, 'normal', 1, 0, 0, 36, 1, NULL, 0, 0),
	( 97, 'pain', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.pain', NULL, 1, 7, 'normal', 1, 0, 0, 27, 1, NULL, 0, 0),
	( 0, 'pain_comments', 'STRING', 'TXTA',50, 'pixalere.pain_comments', NULL, 1, 7, 'normal', 1, 0, 0, 37, 1, NULL, 0, 0),
	( 0, 'length', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.length', NULL, 1, 8, 'normal', 1, 0, 0, 28, 1, NULL, 0, 0),
	( 0, 'width', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.width', NULL, 1, 9, 'normal', 1, 0, 0, 29, 1, NULL, 0, 0),
	( 0, 'depth', 'INTEGER', 'MEAS',50, 'pixalere.woundassessment.form.depth', NULL, 1, 10, 'normal', 1, 0, 0, 30, 1, NULL, 0, 0),
	( 0, 'site', 'STRING', 'TXTA',50, 'pixalere.skinassessment.form.site', NULL, 1, 16, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 0, 'products', 'STRING', 'PROD',50, 'pixalere.woundassessment.form.products', NULL, 1, 23, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 0, 'body', 'STRING', 'TXTA',50, 'pixalere.woundassessment.form.treatment_comments', 16, 1, 24, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 0, 'vac_start_date', 'DATE', 'DATE',50, 'pixalere.woundassessment.form.vacstart', 36, 1, 27, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 0, 'vac_end_date', 'DATE', 'DATE',50, 'pixalere.woundassessment.form.vacend', 36, 1, 28, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 98, 'therapy_setting', 'INTEGER', 'RADO',50, 'pixalere.woundassessment.form.reading_group', 36, 1, 28, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 40, 'pressure_reading', 'INTEGER', 'DRP',50, 'pixalere.woundassessment.form.pressure_reading', 36, 1, 29, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 38, 'cs_show', 'INTEGER', 'YN',50, 'pixalere.treatment.form.cs_done', NULL, 1, 32, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 38, 'csresult', 'STRING', 'CHKB',50, 'pixalere.woundassessment.form.c_s_result', 27, 1, 34, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 0, 'antibiotics', 'STRING', 'MDDA',50, 'pixalere.treatment.form.antibiotics', 38, 1, 35, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 17, 'adjunctive', 'STRING', 'CHKB',50, 'pixalere.woundassessment.form.adjunctive', 27, 1, 37, 'normal', 1, 0, 0, 0, 1, NULL, 0, 0),
	( 0, 'images', 'STRING', 'IMG',50, 'pixalere.woundassessment.form.images', NULL, 1, 38, 'normal', 1, 0, 0, 0, 0, NULL, 0, 0),
	( 0, 'review_done', 'INTEGER', 'YN',50, 'pixalere.treatment.review_done', 36, 1, 24, 'normal', 0, 0, 0, 0, 0, NULL, 0, 0),
	( 0, 'referral', 'STRING', 'REF',50, 'pixalere.referral.form.title_fc', NULL, 1, 998, 'normal', 0, 0, 0, 0, 0, NULL, 0, 1),
	( 0, 'comments', 'STRING', 'COMM',50, 'pixalere.woundassessment.form.contains_comments', NULL, 1, 999, 'normal', 0, 0, 0, 0, 0, NULL, 0, 0),
	( 0, 'maintenance', 'STRING', 'ADM',50, 'pixalere.viewer.assessment_updates', NULL, 1, 1000, 'normal', 0, 0, 0, 0, 0, NULL, 1, 0),
	( 0, 'pdf', 'STRING', 'PDF',50, 'pixalere.viewer.pdf', NULL, 1, 1010, 'normal', 0, 0, 0, 0, 0, NULL, 1, 0);
--update patient_accounts set time_stamp = DATEDIFF(SECOND,'1970-01-01',sig_temp) where sig_temp IS NOT NULL;
--update patient_accounts set user_signature=sig_temp;
--alter table patient_accounts drop column sig_temp;