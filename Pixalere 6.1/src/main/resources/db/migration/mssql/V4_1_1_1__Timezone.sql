alter table professional_accounts add timezone varchar(100)
GO
alter table professional_accounts add timezone_flag int
GO
alter table patient_accounts add timezone varchar(100)
GO
alter table professional_accounts add usepatient_timezone int
GO
update professional_accounts set usepatient_timezone=0;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,7,0,'hidePatientTimezone','','','CHKB','','','1');
SET IDENTITY_INSERT resources ON;
insert into resources (id,name,report_value_label_key,report_value_component_type,report_value_field_type) values(170,'Timezone','','','');
SET IDENTITY_INSERT resources OFF;
insert into listdata (status,resource_id,item_en,item_fr,value,other,orderby,title,indent,checkbox_column,solobox,item_filter,report_value,category_id) values(1,170,'America/Vancouver',NULL,'',0,1,0,0,0,0,0,'',0);
insert into listdata (status,resource_id,item_en,item_fr,value,other,orderby,title,indent,checkbox_column,solobox,item_filter,report_value,category_id) values(1,170,'America/Dawson_Creek',NULL,'',0,2,0,0,0,0,0,'',0);
--update listdata set report_value='' where resource_id=51;
update professional_accounts set timezone='America/Vancouver';
alter table assessment_recommendation alter column user_signature varchar(255);
alter table limb_assessment alter column user_signature varchar(255);
update assessment_comments set user_signature=REPLACE(user_signature,':ID#:','#');
update assessment_comments set user_signature=REPLACE(user_signature,' - ','PST - ');
--update assessment_comments set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND time_stamp>1289161512;
update assessment_recommendation set user_signature=REPLACE(user_signature,':ID#:','#');
update assessment_recommendation set user_signature=REPLACE(user_signature,' - ','PST - ');
--update assessment_recommendation set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND time_stamp>1289161512;
update wound_assessment set user_signature=REPLACE(user_signature,':ID#:','#');
update wound_assessment set user_signature=REPLACE(user_signature,' - ','PST - ');
--update wound_assessment set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND time_stamp>1289161512;
update wounds set startdate_signature=REPLACE(startdate_signature,':ID#:','#');
update wounds set startdate_signature=REPLACE(startdate_signature,' - ','PST - ');
--update wounds set startdate_signature=REPLACE(startdate_signature,SUBSTRING(startdate_signature,1,2),cast(cast(substring(startdate_signature,1,2) as int)+1 as varchar))  where startdate_signature != 'NA' AND start_date>1289161512;
update wound_profiles set user_signature=REPLACE(user_signature,':ID#:','#');
update wound_profiles set user_signature=REPLACE(user_signature,' - ','PST - ');
--update wound_profiles set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND last_update>1289161512;
update treatment_comments set user_signature=REPLACE(user_signature,':ID#:','#');
update treatment_comments set user_signature=REPLACE(user_signature,' - ','PST - ');
--update treatment_comments set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND time_stamp>1289161512;
update patient_profiles set user_signature=REPLACE(user_signature,':ID#:','#');
update patient_profiles set user_signature=REPLACE(user_signature,' - ','PST - ');
--update patient_profiles set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND last_update>1289161512;
update patient_accounts set user_signature=REPLACE(user_signature,':ID#:','#');
update patient_accounts set user_signature=REPLACE(user_signature,' - ','PST - ');
--update patient_accounts set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND time_stamp>1289161512;
update nursing_care_plan set user_signature=REPLACE(user_signature,':ID#:','#');
update nursing_care_plan set user_signature=REPLACE(user_signature,' - ','PST - ');
--update nursing_care_plan set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND time_stamp>1289161512;
update limb_assessment set user_signature=REPLACE(user_signature,':ID#:','#');
update limb_assessment set user_signature=REPLACE(user_signature,' - ','PST - ');
--update limb_assessment set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND last_update>1289161512;
update foot_assessment set user_signature=REPLACE(user_signature,':ID#:','#');
update foot_assessment set user_signature=REPLACE(user_signature,' - ','PST - ');
--update foot_assessment set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND last_update>1289161512;
update dressing_change_frequency set user_signature=REPLACE(user_signature,':ID#:','#');
update dressing_change_frequency set user_signature=REPLACE(user_signature,' - ','PST - ');
--update dressing_change_frequency set user_signature=REPLACE(user_signature,SUBSTRING(user_signature,1,2),cast(cast(substring(user_signature,1,2) as int)+1 as varchar))  where user_signature != 'NA' AND time_stamp>1289161512;
