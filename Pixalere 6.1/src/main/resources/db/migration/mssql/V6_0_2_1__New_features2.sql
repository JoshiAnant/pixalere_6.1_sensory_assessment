delete from limb_upper_assessment;
EXEC sp_rename 'limb_upper_assessment.left_skin_colour_leg','left_skin_colour_arm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_skin_colour_leg','right_skin_colour_arm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_skin_colour_foot','left_skin_colour_hand','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_skin_colour_foot','right_skin_colour_hand','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_skin_colour_toes','left_skin_colour_fingers','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_skin_colour_toes','right_skin_colour_fingers','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_temperature_leg','left_temperature_arm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_temperature_leg','right_temperature_arm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_temperature_foot','left_temperature_hand','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_temperature_foot','right_temperature_hand','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_temperature_toes','left_temperature_fingers','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_temperature_toes','right_temperature_fingers','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_ankle_cm','left_wrist_cm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_ankle_mm','left_wrist_mm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_ankle_cm','right_wrist_cm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_ankle_mm','right_wrist_mm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_midcalf_cm','left_elbow_cm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.left_midcalf_mm','left_elbow_mm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_midcalf_cm','right_elbow_cm','COLUMN';
EXEC sp_rename 'limb_upper_assessment.right_midcalf_mm','right_elbow_mm','COLUMN';
set identity_insert resources On
insert into resources (id,name) values (188,'Missing Upper Limbs');
insert into resources (id,name) values (189,'Upper Limb Edema Location');
set identity_insert resources Off
update resources set name='Limb Edema Location' where id=20;
update components set label_key='pixalere.patientprofile.form.left_wrist',field_name='left_wrist' where field_name='left_ankle' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_wrist',field_name='right_wrist' where field_name='right_ankle' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.left_elbow',field_name='left_elbow' where field_name='left_midcalf' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.right_elbow',field_name='right_elbow' where field_name='right_midcalf' and flowchart=53;
update components set field_name='left_skin_colour_arm',label_key='pixalere.patientprofile.form.left_skin_colour_arm' where field_name='left_skin_colour_leg' and flowchart=53;
update components set field_name='right_skin_colour_arm',label_key='pixalere.patientprofile.form.right_skin_colour_arm' where field_name='right_skin_colour_leg' and flowchart=53;
update components set field_name='left_skin_colour_hand',label_key='pixalere.patientprofile.form.left_skin_colour_hand' where field_name='left_skin_colour_foot' and flowchart=53;
update components set field_name='right_skin_colour_hand',label_key='pixalere.patientprofile.form.right_skin_colour_hand' where field_name='right_skin_colour_foot' and flowchart=53;
update components set field_name='left_skin_colour_fingers',label_key='pixalere.patientprofile.form.left_skin_colour_fingers' where field_name='left_skin_colour_toes' and flowchart=53;
update components set field_name='right_skin_colour_fingers',label_key='pixalere.patientprofile.form.right_skin_colour_fingers' where field_name='right_skin_colour_toes' and flowchart=53;
update components set field_name='left_temperature_arm',label_key='pixalere.patientprofile.form.left_temperature_arm' where field_name='left_temperature_leg' and flowchart=53;
update components set field_name='right_temperature_arm',label_key='pixalere.patientprofile.form.right_temperature_arm' where field_name='right_temperature_leg' and flowchart=53;
update components set field_name='left_temperature_hand',label_key='pixalere.patientprofile.form.left_temperature_hand' where field_name='left_temperature_foot' and flowchart=53;
update components set field_name='right_temperature_hand',label_key='pixalere.patientprofile.form.right_temperature_hand' where field_name='right_temperature_foot' and flowchart=53;
update components set field_name='left_temperature_fingers',label_key='pixalere.patientprofile.form.left_temperature_fingers' where field_name='left_temperature_toes' and flowchart=53;
update components set field_name='right_temperature_fingers',label_key='pixalere.patientprofile.form.right_temperature_fingers' where field_name='right_temperature_toes' and flowchart=53;
update components set label_key='pixalere.patientprofile.form.left_missing_limb_upper',resource_id=188 where flowchart=53 and field_name='left_missing_limbs';
update components set label_key='pixalere.patientprofile.form.right_missing_limb_upper',resource_id=188 where flowchart=53 and field_name='right_missing_limbs' and flowchart=53;
delete from components where (field_name='pain_comments' or field_name='left_intedigitial_doppler' or  field_name='right_intedigitial_doppler' ) and ( flowchart=11 or flowchart=53 or flowchart=52);
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'No amputations',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Arm above elbow',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Arm below elbow',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Hand partial',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Hand all',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Thumb',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Second finger',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Third finger',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Fourth finger',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,188);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Fifth finger',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'No Edema',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Hand',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Up to Wrist',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Up to Elbow',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,189);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Up to Shoulder',resource_id from lookup order by id desc ;
alter table limb_basic_assessment add left_range_motion_knee int;
alter table limb_basic_assessment add left_range_motion_ankle int;
alter table limb_basic_assessment add left_range_motion_toe int;
alter table limb_basic_assessment add right_range_motion_knee int;
alter table limb_basic_assessment add right_range_motion_ankle int;
alter table limb_basic_assessment add right_range_motion_toe int;
set identity_insert resources On
insert into resources (id,name) values (190,'Range of Motion');
set identity_insert resources Off
insert into lookup (active,resource_id) values(1,190);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Normal',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,190);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Decreased',resource_id from lookup order by id desc ;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 190, 'left_range_motion_ankle', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.left_range_motion_ankle', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 190, 'left_range_motion_knee', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.left_range_motion_knee', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 190, 'left_range_motion_toe', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.left_range_motion_toe', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 190, 'right_range_motion_ankle', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.right_range_motion_ankle', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 190, 'right_range_motion_knee', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.right_range_motion_knee', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 190, 'right_range_motion_toe', 'INTEGER', 'DRP',11, 'pixalere.patientprofile.form.right_range_motion_toe', NULL,  67, 1,  0, 0, 1, NULL);
alter table limb_upper_assessment add left_range_motion_arm int;
alter table limb_upper_assessment add left_range_motion_hand int;
alter table limb_upper_assessment add left_range_motion_fingers int;
alter table limb_upper_assessment add right_range_motion_arm int;
alter table limb_upper_assessment add right_range_motion_hand int;
alter table limb_upper_assessment add right_range_motion_fingers int;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 190, 'left_range_motion_arm', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.left_range_motion_arm', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 190, 'left_range_motion_hand', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.left_range_motion_hand', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 190, 'left_range_motion_fingers', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.left_range_motion_fingers', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 190, 'right_range_motion_arm', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.right_range_motion_arm', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 190, 'right_range_motion_hand', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.right_range_motion_hand', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 190, 'right_range_motion_fingers', 'INTEGER', 'DRP',53, 'pixalere.patientprofile.form.right_range_motion_fingers', NULL,  67, 1,  0, 0, 1, NULL);
alter table limb_adv_assessment add left_stemmers int;
alter table limb_adv_assessment add right_stemmers int;
alter table limb_adv_assessment add left_homans int;
alter table limb_adv_assessment add right_homans int;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'left_stemmers', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.left_stemmers', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'right_stemmers', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.right_stemmers', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'left_homans', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.left_homans', NULL,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'right_homans', 'INTEGER', 'YN',52, 'pixalere.patientprofile.form.right_homans', NULL,  67, 1,  0, 0, 1, NULL);
update configuration set component_type='DRP',component_options='alpha-numeric,alpha,2alpha-2numeric-uppercase,alpha-numeric-uppercase,alpha-numeric-uppercase-specialcharacter',current_value='alpha-numeric' where config_setting='requireNumbersAndCharsInPassword';
update components set orderby=45 where field_name='hga1c';
update components set orderby=27 where field_name='purs_score';
update components set orderby=25 where field_name='investigation';
update components set orderby=23 where field_name='medications_comments';
update components set orderby=22 where field_name='interfering_meds_other';
update components set orderby=21 where field_name='interfering_meds';
update components set orderby=19 where field_name='surgical_history';
update components set orderby=17 where field_name='co_morbidities';
update components set orderby=14 where field_name='professionals';
update components set orderby=12 where field_name='funding_source';
update components set orderby=10 where field_name='type_of_ostomy';
update components set orderby=11 where field_name='marking_prior_surgery';
update components set orderby=12,flowchart=1,table_id=24 where field_name='patient_limitations';
update components set orderby=13 where field_name='teaching_goals';
update components set resource_id=136 where field_name='construction';
update components set resource_id=166 where field_name='devices';
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value) VALUES (0,0,0,'webservice_password','','','INPT','','','p1x.Srv!@');
update assessment_drain set closed_date=drain_removed_date;
alter table assessment_drain drop column drain_removed_date;
update components set field_name='closed_date' where field_name='drain_removed_date' and flowchart=6;
update professional_accounts set access_create_patient=1 where id=7;
INSERT INTO license (id, license) VALUES (1,'-----BEGIN PGP MESSAGE-----
Version: BCPG v1.43

owJ4nJvAy8zAxHhA5Eru160fbzOeXp0knpOZnJpXnOqWmZPql5ibquuZnpdflJoS
uMKjW1lXVwEqrZAGlOdSdivKVPBNrFQwNlQwMLIyNbQyNVFwdQlRMDIwNOZKKUrM
zLM15MorzY1PS0zOzMksyUwttjU1AAKu/OKS/NxKoGxZYk5mSnxpXklmji1Qm4W+
gSEQgTWVFqcWQZVn5iVnFmfmg4wrzy/NSwHSnUxeLAyMTAwmrEwgx+nKBGRWJOak
FqUqaPhAHemdWqmpYFNSlFiWWexQAJXWS87PtWPg4hSAebtopwDDpukqv3coT201
557O8MNGWyDvQ5m8idp5pjke7zh4f2+Vs+bN3be/accVpXe9zMExf8MKJjPvE3c7
/3SbplNT88rZDhJ2Szf3STOqxXrOagiTtLmaGcaS9PfxFSUJboscy+j5izq7Jt22
yq9RCDF6r8wnGJHR+ebtJI8tye76FeveLlQy6uHfdlZ6b17Uu+B41Tm1h+6Vvz1p
VTmlemv0kXRP/rcf1JqMElfsip68vynihMbZu4eO3Lq5qKRz6+6F1ZKqsxUtP7nY
1x3cyTHN8tan5BZBJxvPD+ePXePdzPtji2Ztxbr5y2bUlK+ffVRcQGjJM+ljWXat
nDd4J3VcTjx379yHFTZu2Rb3y7uUZOzfSk73dbBg3nUyS2Tr9rMNm6TOaP0o1d5Q
XusyRalsx6Pff+ze+5uVfxQXKUrh/M1ndn1F7YfVv7jCZguV19aePRvyXdzkwwX+
a68CWsVsPLUdg570p9WtqbGRdZrXWjPxspLymv0GO58VnJO+oJl82DOfZaNycU5u
1+ljgauq/8VfPdpvEf3m1hfBLYu6N36x256388Csv14fNd7onPm0L3em1Zxd0o8a
D6klXn29OVtn+rf4Reu6WT5c8j+47H/cp2e1M4yXrNZ7tN7vI+umR+nbNdwCbA9Y
XigJfGI1teTTNb5Pz+yinkcLefku7dlxlkv03Mr0ZcxPJl9f6ROcm31mQtc1gSNe
X8x3AgBLu1gF
=N0Tf
-----END PGP MESSAGE-----');
delete from configuration where config_setting='useDateCalendar' or config_setting='showAssessDoneIndicator' or config_setting='setupAssessmentScreen' or config_setting='defaultWoundBed100Percent' or config_setting='exudateAutoNils' or config_setting='showNursingVisitFrequency' or config_setting='showLimbComments' or config_setting='recurrentRequired' or config_setting='prevalence_population' or config_setting='img_server_license_key' or config_setting='img_server_url' or config_setting='img_server_url_redirect' or config_setting='dashboard_email' or config_setting='support_email' or config_setting='dataHowMuch' or config_setting='offlineAutoReferralHack' or config_setting='allowIncisionBeforeTag' or config_setting='showCorrectionsWithStrikeThrough';
update configuration set enable_component_rule='' where config_setting='showTransferOnSearch' or config_setting='useDobCalendar';
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,1,0,'XML2Webservice','',' ','CHKB','',' ','0','0');
update configuration set config_setting='integration',component_type='DRP',component_options='PCC Interface,HL7 Web Service,HL7 Flat File,XML Flate File (Paris),XML Web Service',component_values='pcc_webservice,hl7_webservice,hl7_flatfile,xml_flatfile,xml_webservice',current_value='' where config_setting='XML2Webservice';
update wal set close_time_stamp = (select top 1 closed_date from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and exists(select top 1 closed_date from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by last_update desc);
update wal set close_time_stamp = (select top 1 closed_date from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and exists(select top 1 closed_date from assessment_drain ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='drainClosed') order by last_update desc);
update wal set close_time_stamp = (select top 1 closed_date from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and exists(select top 1 closed_date from assessment_incision ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='incisionClosed') order by last_update desc);
update wal set close_time_stamp = (select top 1 closed_date from assessment_ostomy ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='ostomyClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and exists(select top 1 closed_date from assessment_ostomy ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='ostomyClosed') order by last_update desc);
update wal set close_time_stamp = (select top 1 last_update from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by last_update desc) from wound_assessment_location wal where wal.discharge=1 and wal.close_time_stamp='' and exists(select top 1 closed_date from assessment_wound ae where ae.alpha_id=wal.id and status=(select current_value from configuration where config_setting='woundClosed') order by last_update desc);
