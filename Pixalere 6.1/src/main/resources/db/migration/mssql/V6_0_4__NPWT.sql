update wound_profiles set created_on=dateadd(hh,-7,created_on);
update wound_assessment set created_on=dateadd(hh,-7,created_on);
update patient_profiles set created_on=dateadd(hh,-7,created_on);
update limb_adv_assessment set created_on=dateadd(hh,-7,created_on);
update limb_basic_assessment set created_on=dateadd(hh,-7,created_on);
update limb_upper_assessment set created_on=dateadd(hh,-7,created_on);
update wound_profiles set visited_on=created_on;
update wound_assessment set visited_on=created_on;
update assessment_wound set visited_on=created_on;
update assessment_drain set visited_on=created_on;
update assessment_incision set visited_on=created_on;
update assessment_ostomy set visited_on=created_on;
update patient_profiles set visited_on=created_on;
update limb_adv_assessment set visited_on=created_on;
update limb_basic_assessment set visited_on=created_on;
update limb_upper_assessment set visited_on=created_on;
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value) VALUES ( 50, 1, 1, 'useTriggers', '', '', 'CHKB', '', '', '0');

