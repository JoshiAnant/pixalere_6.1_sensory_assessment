alter table patient_profiles drop column interfering_meds;
alter table patient_profiles drop column interfering_meds_other;
alter table patient_profiles drop column co_morbidities;
alter table patient_profiles drop column medications_comments;
update patient_profiles set deleted=0 , deleted_reason='', delete_signature='';
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,1,'customer_name','',' ','INPT','',' ','Acme Health Authority',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,20,1,'prevalence_population','',' ','INPT','',' ','300000',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,20,1,'dashboard_email','',' ','INPT','',' ','travis@pixalere.com',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,1,'email_host','',' ','INPT','',' ','smtp.gmail.com',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,1,'email_user','',' ','INPT','',' ','sendmail@pixalere.com',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,1,'email_password','',' ','INPT','',' ','SendM@il7',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,0,'user_stats','',' ','INPT','',' ','travis@pixalere.com,ken@pixalere.com',1);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,1,'support_email','',' ','INPT','',' ','travis@pixalere.com',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,1,'photos_path','',' ','INPT','',' ','c:\share\photos',0);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,0,'customer_id','',' ','INPT','',' ','PIX1234',1);
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (0,0,1,'imperial','',' ','DRP','imperial,metric',' ','metric',1);
update assessment_comments set referral_id=6  where  body LIKE '%s healing rate is%';
update assessment_comments set referral_id=7   where  body LIKE '%dressing changes are 3 times a week or more frequent%';
