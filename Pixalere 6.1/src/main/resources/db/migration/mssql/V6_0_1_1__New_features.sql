update assessment_treatment set professional_id=(select top 1 professional_id from wound_assessment where wound_assessment.id=assessment_treatment.assessment_id);
CREATE TABLE assessment_npwt (
  id int NOT NULL identity,
    patient_id int DEFAULT NULL,
  wound_id int DEFAULT NULL,
secondary int default 0,
  assessment_id int DEFAULT NULL,
    professional_id int default null,
  vac_start_date date DEFAULT NULL,
  vac_end_date date DEFAULT NULL,
  np_connector int DEFAULT NULL,
  pressure_reading int DEFAULT NULL,
  therapy_setting int DEFAULT NULL,
    initiated_by int DEFAULT NULL,
    serial_num varchar(255)   default NULL,
    kci_num varchar(255)  default NULL,
    reason_for_ending int default null,
    goal_of_therapy int default null,
primary key(id),
    CONSTRAINT assessment_npwt_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id),
  CONSTRAINT assessment_npwt_ibfk_4 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE,
  CONSTRAINT assessment_npwt_ibfk_5 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE);
CREATE TABLE assessment_npwt_alpha (
  id int NOT NULL identity,
  assessment_npwt_id int DEFAULT NULL,
  alpha_id int default NULL,
primary key(id),
    CONSTRAINT assessment_npwt_alpha_ibfk_1 FOREIGN KEY (alpha_id) REFERENCES wound_assessment_location (id),
  CONSTRAINT assessment_npwt_alpha_ibfk_4 FOREIGN KEY (assessment_npwt_id) REFERENCES assessment_npwt (id) ON DELETE CASCADE) ;
set identity_insert assessment_npwt On
insert into assessment_npwt (id,professional_id,vac_start_date,vac_end_date,pressure_reading,therapy_setting,assessment_id,patient_id,wound_id) select id,professional_id,vac_start_date,vac_end_date,pressure_reading,therapy_setting,assessment_id,patient_id,wound_id from assessment_treatment where pressure_reading IS NOT NULL and therapy_setting IS NOT NULL and vac_start_date iS NOT NULL and exists (select id from patients where id=patient_id);
set identity_insert assessment_npwt Off
set identity_insert assessment_npwt_alpha On
insert into assessment_npwt_alpha (id,assessment_npwt_id,alpha_id) select id,id,alpha_id from assessment_treatment where pressure_reading IS NOT NULL and therapy_setting IS NOT NULL and vac_start_date iS NOT NULL and exists (select id from patients where id=patient_id) and exists (select id from wound_assessment_location where id=assessment_treatment.alpha_id);
set identity_insert assessment_npwt_alpha Off
alter table assessment_treatment drop column pressure_reading;
alter table assessment_treatment drop column therapy_setting;
alter table assessment_treatment drop column vac_start_date;
alter table assessment_treatment drop column vac_end_date;
alter table assessment_treatment drop column vac_show;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 185, 'initiated_by', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.initiated_by', 51,  36, 1,  0, 0, 1, NULL);
set identity_insert resources On
insert into resources (id,name) values (185,'Initiated By');
set identity_insert resources Off
insert into lookup (active,resource_id) values(1,185);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Acute',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,185);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Community',resource_id from lookup order by id desc;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 186, 'reason_for_ending', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.reason_for_ending', 51,  45, 1,  0, 0, 1, NULL);
set identity_insert resources On
insert into resources (id,name) values (186,'Reason for Ending');
set identity_insert resources Off
insert into lookup (active,resource_id) values(1,186);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Wound Closure',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,186);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Patient Died',resource_id from lookup order by id desc;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',3, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',4, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',5, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',6, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',17, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0,1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message) VALUES( 187, 'goal_of_therapy', 'INTEGER', 'DRP',50, 'pixalere.treatment.form.goal_of_therapy', 51,  44, 1,  0, 0, 1, NULL);
set identity_insert resources On
insert into resources (id,name) values (187,'Goal of Therapy');
set identity_insert resources Off
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Wound Closure',resource_id from lookup order by id desc;
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Prep for STSG',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Post STSG/Flap',resource_id from lookup order by id desc ;
insert into lookup (active,resource_id) values(1,187);
insert into lookup_l10n (language_id,lookup_id,name,resource_id)  select top 1 1,id,'Fistula Closure',resource_id from lookup order by id desc;
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'kci_num', 'STRING', 'TXT',3, 'pixalere.treatment.form.kci_num', 51,  66, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'kci_num', 'STRING', 'TXT',4, 'pixalere.treatment.form.kci_num', 51,  66, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'kci_num', 'STRING', 'TXT',5, 'pixalere.treatment.form.kci_num', 51,  66, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'kci_num', 'STRING', 'TXT',6, 'pixalere.treatment.form.kci_num', 51,  66, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'kci_num', 'STRING', 'TXT',17, 'pixalere.treatment.form.kci_num', 51,  66, 1,  0, 0,1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'kci_num', 'STRING', 'TXT',50, 'pixalere.treatment.form.kci_num', 51,  66, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'serial_num', 'STRING', 'TXT',3, 'pixalere.treatment.form.serial_num', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'serial_num', 'STRING', 'TXT',4, 'pixalere.treatment.form.serial_num', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES(0, 'serial_num', 'STRING', 'TXT',5, 'pixalere.treatment.form.serial_num', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'serial_num', 'STRING', 'TXT',6, 'pixalere.treatment.form.serial_num', 51,  67, 1,  0, 0, 1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message  ) VALUES( 0, 'serial_num', 'STRING', 'TXT',17, 'pixalere.treatment.form.serial_num', 51,  67, 1,  0, 0,1, NULL);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id, orderby, required, enabled_close, info_popup_id, allow_edit, validation_message ) VALUES( 0, 'serial_num', 'STRING', 'TXT',50, 'pixalere.treatment.form.serial_num', 51,  67, 1,  0, 0, 1, NULL);
update components set orderby=68,table_id=51 where field_name='vac_start_date';
update components set orderby=75,table_id=51 where field_name='vac_end_date';
update components set orderby=60 where field_name='antibiotics';
update components set orderby=57,table_id=36 where field_name='cs_show';
update components set orderby=58 where field_name='csresult';
update components set orderby=90 where field_name='images';
update components set orderby=71,table_id=51 where field_name='therapy_setting';
update components set orderby=70,table_id=51 where field_name='pressure_reading';
update components set orderby=61 where field_name='adjunctive';
update components set orderby=53 where field_name='body' and table_id=16;
update components set orderby=72 where field_name='np_connector';
update components set orderby=65 where field_name='initiated_by';
update components set orderby=73 where field_name='reason_for_ending';
update components set orderby=69 where field_name='goal_of_therapy';
alter table assessment_treatment drop column cs_show;
alter table assessment_treatment drop column cs_date_show;
alter table assessment_treatment drop column treatmentmodalities_show;
update assessment_treatment set cs_done=(select top 1 cs_show from assessment_wound where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
update assessment_treatment set cs_done=(select top 1 cs_show from assessment_drain where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
update assessment_treatment set cs_done=(select top 1 cs_show from assessment_incision where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
update assessment_treatment set cs_done=(select top 1 cs_show from assessment_ostomy where alpha_id=assessment_treatment.alpha_id and assessment_id=assessment_treatment.assessment_id and cs_show IS NOT NULL);
