alter table patient_profiles drop column allergies;
update configuration set current_value=0 where id=189;
update wound_location_details set y=0 where box>=1 and box<=16;
update wound_location_details set y=20 where box>=17 and box<=32;
update wound_location_details set y=40 where box>=33 and box<=48;
update wound_location_details set y=60 where box>=49 and box<=64;
update wound_location_details set y=80 where box>=65 and box<=80;
update wound_location_details set y=100 where box>=81 and box<=96;
update wound_location_details set y=120 where box>=97 and box<=112;
update wound_location_details set y=140 where box>=113 and box<=128;
update wound_location_details set y=160 where box>=129 and box<=144;
update wound_location_details set y=180 where box>=145 and box<=160;
update wound_location_details set y=200 where box>=161 and box<=176;
update wound_location_details set y=220 where box>=177 and box<=192;
update wound_location_details set y=240 where box>=193 and box<=208;
update wound_location_details set y=260 where box>=209 and box<=224;
update wound_location_details set y=280 where box>=225 and box<=240;
update wound_location_details set y=300 where box>=241 and box<=256;
update wound_location_details set y=320 where box>=257 and box<=272;
update wound_location_details set y=340 where box>=273 and box<=288;
update wound_location_details set y=360 where box>=289 and box<=304;
update wound_location_details set y=380 where box>=305 and box<=320;
update wound_location_details set y=400 where box>=321 and box<=336;
update wound_location_details set y=420 where box>=337 and box<=352;
update wound_location_details set y=440 where box>=353 and box<=368;
update wound_location_details set y=460 where box>=369 and box<=384;
update wound_location_details set y=480 where box>=385 and box<=400;
update wound_location_details set y=500 where box>=401 and box<=416;
update wound_location_details set x=0 where box=1 OR box=17 OR box=33 OR box=49 OR box=65 OR box=81 OR box=97 OR box=113 OR box=129 OR box=145 OR box=161 OR box=177 OR box=193 or box=209 or box=225 or box=241 or box=257 or box=273 or box=289 or box=305 or box=321 or box=337 or box=353 or box=369 or box=385;
update wound_location_details set x=20 where box=2 OR box=18 OR box=34 OR box=50 OR box=66 OR box=82 OR box=98 OR box=114 OR box=130 OR box=146 OR box=162 OR box=178 OR box=194 or box=210 or box=226 or box=242 or box=258 or box=274 or box=290 or box=306 or box=322 or box=338 or box=354 or box=370 or box=386;
update wound_location_details set x=40 where box=3 OR box=19 OR box=35 OR box=51 OR box=67 OR box=83 OR box=99 OR box=115 OR box=131 OR box=147 OR box=163 OR box=179 OR box=195 or box=211 or box=227 or box=243 or box=259 or box=275 or box=291 or box=307 or box=323 or box=339 or box=355 or box=371 or box=387;
update wound_location_details set x=60 where box=4 OR box=20 OR box=36 OR box=52 OR box=68 OR box=84 OR box=100 OR box=116 OR box=132 OR box=148 OR box=164 OR box=180 OR box=196 or box=212 or box=228 or box=244 or box=260 or box=276 or box=292 or box=308 or box=324 or box=340 or box=356 or box=372 or box=388;
update wound_location_details set x=80 where box=5 OR box=21 OR box=37 OR box=53 OR box=69 OR box=85 OR box=101 OR box=117 OR box=133 OR box=149 OR box=165 OR box=181 OR box=197 or box=213 or box=229 or box=245 or box=261 or box=277 or box=293 or box=309 or box=325 or box=341 or box=357 or box=373 or box=389;
update wound_location_details set x=100 where box=6 OR box=22 OR box=38 OR box=54 OR box=70 OR box=86 OR box=102 OR box=118 OR box=134 OR box=150 OR box=166 OR box=182 OR box=198 or box=214 or box=230 or box=246 or box=262 or box=278 or box=294 or box=310 or box=326 or box=342 or box=358 or box=374 or box=390;
update wound_location_details set x=120 where box=7 OR box=23 OR box=39 OR box=55 OR box=71 OR box=87 OR box=103 OR box=119 OR box=135 OR box=151 OR box=167 OR box=183 OR box=199 or box=215 or box=231 or box=247 or box=263 or box=279 or box=295 or box=311 or box=327 or box=343 or box=359 or box=375 or box=391;
update wound_location_details set x=140 where box=8 OR box=24 OR box=40 OR box=56 OR box=72 OR box=88 OR box=104 OR box=120 OR box=136 OR box=152 OR box=168 OR box=184 OR box=200 or box=216 or box=232 or box=248 or box=264 or box=280 or box=296 or box=312 or box=328 or box=344 or box=360 or box=376 or box=392;
update wound_location_details set x=160 where box=9 OR box=25 OR box=41 OR box=57 OR box=73 OR box=89 OR box=105 OR box=121 OR box=137 OR box=153 OR box=169 OR box=185 OR box=201 or box=217 or box=233 or box=249 or box=265 or box=281 or box=297 or box=313 or box=329 or box=345 or box=361 or box=377 or box=393;
update wound_location_details set x=180 where box=10 OR box=26 OR box=42 OR box=58 OR box=74 OR box=90 OR box=106 OR box=122 OR box=138 OR box=154 OR box=170 OR box=186 OR box=202 or box=218 or box=234 or box=250 or box=266 or box=282 or box=298 or box=314 or box=330 or box=346 or box=362 or box=378 or box=394;
update wound_location_details set x=200 where box=11 OR box=27 OR box=43 OR box=59 OR box=75 OR box=91 OR box=107 OR box=123 OR box=139 OR box=155 OR box=171 OR box=187 OR box=203 or box=219 or box=235 or box=251 or box=267 or box=283 or box=299 or box=315 or box=331 or box=347 or box=363 or box=379 or box=395;
update wound_location_details set x=220 where box=12 OR box=28 OR box=44 OR box=60 OR box=76 OR box=92 OR box=108 OR box=124 OR box=140 OR box=156 OR box=172 OR box=188 OR box=204 or box=220 or box=236 or box=252 or box=268 or box=284 or box=300 or box=316 or box=332 or box=348 or box=364 or box=380 or box=396;
update wound_location_details set x=240 where box=13 OR box=29 OR box=45 OR box=61 OR box=77 OR box=93 OR box=109 OR box=125 OR box=141 OR box=157 OR box=173 OR box=189 OR box=205 or box=221 or box=237 or box=253 or box=269 or box=285 or box=301 or box=317 or box=333 or box=349 or box=365 or box=381 or box=397;
update wound_location_details set x=260 where box=14 OR box=30 OR box=46 OR box=62 OR box=78 OR box=94 OR box=110 OR box=126 OR box=142 OR box=158 OR box=174 OR box=190 OR box=206 or box=222 or box=238 or box=254 or box=270 or box=286 or box=302 or box=318 or box=334 or box=350 or box=366 or box=382 or box=398;
update wound_location_details set x=280 where box=15 OR box=31 OR box=47 OR box=63 OR box=79 OR box=95 OR box=111 OR box=127 OR box=143 OR box=159 OR box=175 OR box=191 OR box=207 or box=223 or box=239 or box=255 or box=271 or box=287 or box=303 or box=319 or box=335 or box=351 or box=367 or box=383 or box=399;
update wound_location_details set x=300 where box=16 OR box=32 OR box=48 OR box=64 OR box=80 OR box=96 OR box=112 OR box=128 OR box=144 OR box=160 OR box=176 OR box=192 OR box=208 or box=224 or box=240 or box=256 or box=272 or box=288 or box=304 or box=320 or box=336 or box=352 or box=368 or box=384 or box=400;
