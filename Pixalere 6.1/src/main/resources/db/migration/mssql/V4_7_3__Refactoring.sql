update resources set name='Interfering Factors' where id=6
update resources set name='Interfering Medications' where id=165;
update components set table_id=33 where field_name='etiology';
update components set table_id=34 where field_name='goals';
update components set table_id=35 where field_name='investigation';
INSERT INTO patient_profile_arrays (patient_profile_id,lookup_id) SELECT patient_profiles.id,lookup.id from patient_profiles   LEFT JOIN lookup ON patient_profiles.interfering_meds LIKE  '%"'+CAST(lookup.id as VARCHAR)+'"%'  WHERE lookup.id IS NOT NULL;
update  patient_profile_arrays  set patient_profile_arrays.other= patient_profiles.interfering_meds_other from patient_profile_arrays,patient_profiles  where patient_profiles.id=patient_profile_arrays.patient_profile_id AND EXISTS (select id from lookup WHERE  patient_profile_arrays.lookup_id=lookup.id  and lookup.name='Chemical Dependency');
INSERT INTO patient_profile_arrays (patient_profile_id,lookup_id) SELECT patient_profiles.id,lookup.id from patient_profiles   LEFT JOIN lookup ON patient_profiles.co_morbidities LIKE  '%"'+CAST(lookup.id as VARCHAR)+'"%'  WHERE lookup.id IS NOT NULL;
INSERT INTO patient_profile_arrays (patient_profile_id,lookup_id) SELECT patient_profiles.id,lookup.id from patient_profiles   LEFT JOIN lookup ON patient_profiles.medications_comments LIKE  '%"'+CAST(lookup.id as VARCHAR)+'"%'  WHERE lookup.id IS NOT NULL;
