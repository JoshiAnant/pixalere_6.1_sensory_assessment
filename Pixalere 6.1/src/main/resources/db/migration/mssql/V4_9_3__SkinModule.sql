CREATE TABLE assessment_skin (id int NOT NULL identity,primary key(id),status int DEFAULT '0',patient_id int DEFAULT NULL,wound_id int DEFAULT NULL,assessment_id int DEFAULT NULL,alpha_id int DEFAULT NULL,wound_profile_type_id int DEFAULT NULL,full_assessment int DEFAULT '-1',pain int DEFAULT NULL,pain_comments varchar(255),length_cm int DEFAULT NULL,length_mm int DEFAULT NULL,width_cm int DEFAULT NULL,width_mm int DEFAULT NULL,depth_cm int DEFAULT NULL,depth_mm int DEFAULT NULL,discharge_reason varchar(50) DEFAULT NULL,active int DEFAULT '0',discharge_other varchar(255),recurrent int DEFAULT '0',wound_date varchar(255),adjunctive varchar(255),adjunctive_other_text varchar(255),closed_date varchar(255),last_update varchar(255),site varchar(255),prev_alpha_id int DEFAULT NULL,moved_reason varchar(255) DEFAULT NULL,deleted int DEFAULT '0',delete_signature varchar(255) DEFAULT NULL,deleted_reason varchar(255) DEFAULT NULL,CONSTRAINT assessment_skin_ibfk_1 FOREIGN KEY (patient_id) REFERENCES patients (id) on delete cascade,CONSTRAINT assessment_skin_ibfk_4 FOREIGN KEY (assessment_id) REFERENCES wound_assessment (id) ON DELETE CASCADE,CONSTRAINT assessment_skin_ibfk_5 FOREIGN KEY (wound_id) REFERENCES wounds (id) ON DELETE CASCADE,CONSTRAINT assessment_skin_ibfk_6 FOREIGN KEY (wound_profile_type_id) REFERENCES wound_profile_type (id) ON DELETE CASCADE) ;
INSERT INTO configuration (category,orderby,selfadmin,config_setting,enable_component_rule,enable_options_rule,component_type,component_options,component_values,current_value,readonly) VALUES (1,1,0,'allowSkin','',' ','CHKB','',' ','1','0');
set identity_insert resources On
insert into resources (id,name) values (180,'Skin Etiology');
insert into resources (id,name) values (183,'Skin Goals');
insert into resources (id,name) values (181,'Skin Status');
insert into resources (id,name) values (182,'Skin Discharge Reason');
set identity_insert resources Off
INSERT INTO configuration ( category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
	( 7, 15, 0, 'skinActive', '', '', 'INPN', '', '', '3283', 1),
	( 7, 16, 0, 'skinClosed', '', '', 'INPN', '', '', '3284', 1);
alter table assessment_product add skin_assessment_id int;
alter table library add category_id int;
alter table assessment_treatment add patient_id int;
alter table assessment_treatment add professional_id int;
alter table assessment_treatment add cs_done int;
alter table referrals_tracking add patient_id int;
alter table library add media_type varchar(3);
delete from configuration where config_setting='hideCreateTab';
delete from configuration where config_setting='integration_validation_secondary';
delete from configuration where config_setting='redirectOnPatientSearch';