alter table account_assignments alter column time_stamp datetime ;
EXEC sp_rename 'account_assignments.time_stamp','created_on', 'COLUMN';
alter table assessment_burn alter column  last_update  datetime;
EXEC sp_rename 'assessment_burn.last_update','created_on', 'COLUMN';
alter table assessment_wound alter column  last_update  datetime;
EXEC sp_rename 'assessment_wound.last_update','created_on', 'COLUMN';
alter table assessment_drain alter column  last_update  datetime;
EXEC sp_rename 'assessment_drain.last_update','created_on', 'COLUMN';
alter table assessment_skin alter column  last_update  datetime;
EXEC sp_rename 'assessment_skin.last_update','created_on', 'COLUMN';
alter table assessment_ostomy alter column  last_update  datetime;
EXEC sp_rename 'assessment_ostomy.last_update','created_on', 'COLUMN';
alter table assessment_incision alter column  last_update  datetime;
EXEC sp_rename 'assessment_incision.last_update','created_on', 'COLUMN';
alter table assessment_comments alter column  time_stamp  datetime;
EXEC sp_rename 'assessment_comments.time_stamp','created_on', 'COLUMN';
