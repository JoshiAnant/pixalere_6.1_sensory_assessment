-- Configuration Options
INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- Enable/Disable SSO - Checkbox - All Administrators
    (131, 0, 1, 'enableSSO', '', '','CHKB', '',        '', '0', 0);
INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- Enable/Disable fullSSO (All users have to login by SSO, except users with admin token for Pix management) - Checkbox - All Administrators
    (131, 0, 1, 'fullSSO', '', '','CHKB', '',        '', '0', 0);
INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- Token to bypass SSO for partial SSO, (when fullSSO == 0) String - All Administrators
    (131, 0, 1, 'bypassSSOtoken', '', '','INPT', '',        '', 'token=allowLogin', 0);
INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- Token to bypass SSO for Pix Admins/Users, String - Only Super Admin can manage
    (131, 0, 0, 'bypassSSOsuper', '', '','INPT', '',        '', 'bpSSO=!2xaP1X', 0);

INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- File types and URIs that bypass SSO - String - Only Super Admin
    (131, 0, 0, 'excludedTypesSSO', '', '','INPT', '',        '', '.css;.png;.jpg;.jpeg;.js;.gif;/login.do', 0);

INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- Can create Users with SSO (Users can be created even if they don't exist) - Checkbox - All Administrators
    (131, 0, 1, 'createUsersSSO', '', '','CHKB', '',        '', '1', 0);
INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- All the User settings can be set by SSO attributes (Manual post-creation adjustment is not necessary) - Checkbox - All Administrators
    (131, 0, 1, 'fullSSOUserCreation', '', '','CHKB', '',        '', '1', 0);
INSERT INTO configuration 
    (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)
VALUES
    -- Display SSO messages in console (Error debugging) - Checkbox - All Administrators
    (131, 0, 1, 'enableSSODebug', '', '','CHKB', '',        '', '0', 0);


-- Create SSO Config tables
CREATE TABLE sso_providers (
    id int NOT NULL identity,
    active int NOT NULL DEFAULT '0',
    sso_type varchar(25) default '',
    description varchar(512) default '',
    pix_identityid varchar(255) default '',
    idp_identityid varchar(255) default '',
    saml2_sso_url varchar(255) default '',
    saml2_artifact_url varchar(255) default '',
    authrequest_signed int NOT NULL DEFAULT '0',
    authrequest_encrypted int NOT NULL DEFAULT '0',
    authnresponse_signed int NOT NULL DEFAULT '0',
    authresponse_encrypted int NOT NULL DEFAULT '0',
    saml2_bindingtype varchar(25) default '',
    logout_enabled int NOT NULL DEFAULT '0',
    logout_request_signed int NOT NULL DEFAULT '0',
    logout_requesturl varchar(255) default '',
    cert_filepath varchar(255) default '',
    cert_algorithm varchar(25) default '',
    timezone varchar(100) default '',
    main_field varchar(80) default '',
    only_main_field int NOT NULL DEFAULT '0',
    allow_sso_update int NOT NULL DEFAULT '0',
    attribute_name_pixauth varchar(80) default '',
    attribute_value_pixauth varchar(255) default '',
    attribute_name_username varchar(80) default '',
    attribute_name_email varchar(80) default '',
    attribute_name_firstname varchar(80) default '',
    attribute_name_lastname varchar(80) default '',
    attribute_name_position varchar(80) default '',
    attribute_name_locations varchar(80) default '',
    attribute_name_roles varchar(80) default '',
    self_conf_roles int NOT NULL DEFAULT '0',
    self_conf_locations int NOT NULL DEFAULT '0',
    self_conf_positions int NOT NULL DEFAULT '0',
    use_attribute_training int NOT NULL DEFAULT '0',
    use_attribute_reporting int NOT NULL DEFAULT '0',
    attribute_name_training varchar(80) default '',
    attribute_name_reporting varchar(80) default '',
    attribute_value_training varchar(80) default '',
    attribute_value_reporting varchar(80) default '',
    PRIMARY KEY (id)
);
CREATE TABLE sso_locations (
    id int NOT NULL identity,
    sso_provider_id int NOT NULL,
    attribute_value varchar(80) default '',
    pix_locations_ids varchar(512) default '',
    PRIMARY KEY (id)
);
CREATE TABLE sso_roles (
    id int NOT NULL identity,
    sso_provider_id int NOT NULL,
    role_name varchar(80) default '',
    description varchar(512) default '',
    active int NOT NULL DEFAULT '0',
    attribute_value varchar(80) default '',
    access_allpatients int NOT NULL DEFAULT '0',
    access_patients int NOT NULL DEFAULT '0',
    access_uploader int NOT NULL DEFAULT '0',
    access_viewer int NOT NULL DEFAULT '0',
    access_admin int NOT NULL DEFAULT '0',
    access_create_patient int NOT NULL DEFAULT '0',
    access_assigning int NOT NULL DEFAULT '0',
    access_listdata int NOT NULL DEFAULT '0',
    access_professionals int NOT NULL DEFAULT '0',
    access_ccac_reporting int NOT NULL DEFAULT '0',
    access_reporting int NOT NULL DEFAULT '0',
    access_audit int NOT NULL DEFAULT '0',
    access_superadmin int NOT NULL DEFAULT '0',
    training_flag int NOT NULL DEFAULT '0',
    PRIMARY KEY (id)
);
CREATE TABLE sso_positions (
    id int NOT NULL identity,
    sso_provider_id int NOT NULL,
    position_id int NOT NULL,
    attribute_value varchar(80) default '',
    title varchar(255) default '',
    PRIMARY KEY (id)
);
-- Test entry, disabled
INSERT INTO sso_providers 
(
    active, 
    sso_type, 
    description, 
    pix_identityid, 
    idp_identityid, 
    saml2_sso_url, 
    saml2_artifact_url,
    authrequest_signed, authrequest_encrypted, authnresponse_signed, authresponse_encrypted, 
    saml2_bindingtype, 
    logout_enabled, logout_request_signed, logout_requesturl, 
    cert_filepath, cert_algorithm,
    timezone,
    main_field,
    only_main_field, allow_sso_update,
    attribute_name_pixauth, attribute_value_pixauth, 
    attribute_name_username, attribute_name_email, attribute_name_firstname, attribute_name_lastname, attribute_name_position,
    attribute_name_locations, attribute_name_roles,
    use_attribute_training, use_attribute_reporting,
    attribute_name_training, attribute_name_reporting,
    attribute_value_training, attribute_value_reporting,
    self_conf_roles, self_conf_locations, self_conf_positions
)
VALUES
(
    0,
    'SAML2',
    'Locally setup Identity Provider for development using simpleSAMLphp. Read the corresponding Pixalere documentation to install according to these parameters.',
    'Pixalere',
    'https://mysamlidp.com/idp/saml2/idp/metadata.php',
    'https://mysamlidp.com/idp/saml2/idp/SSOService.php',
    '',
    0, 0, 1, 0,
    'POST',
    0, 0, '',
    'C:\pixalere\ssocerts\mysamlidp.com.crt', 'RSA',
    'America/Vancouver',
    'username',
    0, 0,
    'securityGroup', 'pixalere',
    'uid', 'mail', 'first_name', 'last_name', 'position',
    'userGroup', 'role',
    0, 0,
    '', '',
    '', '',
    0, 0, 0
);
-- Test/sample roles
INSERT INTO sso_roles 
(
	sso_provider_id,
	role_name,
	description,
	active,
	attribute_value,
	access_allpatients, access_patients, access_uploader, access_viewer, access_ccac_reporting, access_admin, access_create_patient, access_assigning, access_listdata, access_professionals, access_audit, access_superadmin, 
	training_flag,
	access_reporting
)
VALUES
(
	1,
	'Training role',
	'Special role. The training flag will be enabled for any user with the role identified here. The role permissions are additive if the user has more than one role. An alternative is to use a dedicated attribute by enabling: Use attribute for training.',
	1,
	'training',
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	1,
	0
);
INSERT INTO sso_roles 
(
	sso_provider_id,
	role_name,
	description,
	active,
	attribute_value,
	access_allpatients, access_patients, access_uploader, access_viewer, access_ccac_reporting, access_admin, access_create_patient, access_assigning, access_listdata, access_professionals, access_audit, access_superadmin, 
	training_flag,
	access_reporting
)
VALUES
(
	1,
	'Reporting role',
	'Special role. The reporting flag will be enabled for any user with the role identified here. The role permissions are additive if the user has more than one role. An alternative is to use a dedicated attribute by enabling: Use attribute for reporting.',
	1,
	'reporting',
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	0,
	1
);
INSERT INTO sso_roles 
(
	sso_provider_id,
	role_name,
	description,
	active,
	attribute_value,
	access_allpatients, access_patients, access_uploader, access_viewer, access_ccac_reporting, access_admin, access_create_patient, access_assigning, access_listdata, access_professionals, access_audit, access_superadmin, 
	training_flag,
	access_reporting
)
VALUES
(
	1,
	'Administrator',
	'Test role setting. It shows a possible configuration for a clinic/client administrator. Training flag and Reporting can be set in separated roles and/or attributes. The role permissions are additive if the user has more than one role.',
	1,
	'administrator',
	1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 
	0,
	0
);
INSERT INTO sso_roles 
(
	sso_provider_id,
	role_name,
	description,
	active,
	attribute_value,
	access_allpatients, access_patients, access_uploader, access_viewer, access_ccac_reporting, access_admin, access_create_patient, access_assigning, access_listdata, access_professionals, access_audit, access_superadmin, 
	training_flag,
	access_reporting
)
VALUES
(
	1,
	'Clinician',
	'Test role setting. A possible configuration for a specialist/physician. Training flag and Reporting can be set in separated roles. The role permissions are additive if the user has more than one role.',
	1,
	'clinician',
	0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 
	0,
	0
);
INSERT INTO sso_roles 
(
	sso_provider_id,
	role_name,
	description,
	active,
	attribute_value,
	access_allpatients, access_patients, access_uploader, access_viewer, access_ccac_reporting, access_admin, access_create_patient, access_assigning, access_listdata, access_professionals, access_audit, access_superadmin, 
	training_flag,
	access_reporting
)
VALUES
(
	1,
	'Nurse',
	'Demo role setting. A possible configuration for a nurse. Training flag and Reporting can be set in separated roles. The role permissions are additive if the user has more than one role.',
	1,
	'nurse',
	0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 
	0,
	0
);
