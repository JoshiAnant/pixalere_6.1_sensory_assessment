update wound_profiles set goals=replace(goals,'To Maintain/Non-healable/Non-healable','To Maintain/Non-healable');
update wound_profiles set goals=replace(goals,'To Maintain/Non-healable/Palliative','To Maintain/Palliative');
update assessment_wound set closed_date='' where convert(bigint,closed_date)>1392353966 OR convert(bigint,closed_date)<-3783006034 and status=(select current_value from configuration where config_setting='woundClosed');
