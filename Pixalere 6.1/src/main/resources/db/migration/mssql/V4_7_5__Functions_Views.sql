--CREATE FUNCTION fnYesNo(@param int) RETURNS varchar(10) BEGIN DECLARE @value varchar(10); set @value = ''; IF @param = 2 BEGIN set @value = 'Yes'; END  ELSE BEGIN IF @param = 1 BEGIN  set @value = 'No'; END ELSE BEGIN IF (@param = -1)  set @value = 'N/A'; END; END; RETURN @value; END
--GO
--CREATE FUNCTION fnGetAlpha(@id int) RETURNS varchar(20) BEGIN DECLARE @alpha varchar(20); SELECT @alpha = alpha FROM wound_assessment_location WHERE (id=@id);IF @alpha = 'ostf' BEGIN  set @alpha = 'Fecal Ostomy'; END ELSE BEGIN IF @alpha = 'ostu'  BEGIN   set @alpha = 'Urostomy'; END ELSE BEGIN IF @alpha = 'td'  BEGIN   set @alpha = 'Tube/Drain '+substring(@alpha,1,3); END ELSE BEGIN IF @alpha = 'tag' BEGIN  set @alpha = 'Incision '+substring(@alpha,1,4); END ELSE BEGIN IF len(@alpha) = 1 BEGIN  set @alpha = 'Wound '+@alpha; END; END; END; END; END; RETURN @alpha; END
--GO
--CREATE FUNCTION fnPatientProfileOnSameLine(@id int,@resource_id int,@Delimiter varchar(10)) RETURNS varchar(max) BEGIN DECLARE @OnSameLine varchar(max); set @OnSameLine=''; SELECT @OnSameLine = @OnSameLine + coalesce (lookup.name,'')+@Delimiter FROM  lookup INNER JOIN patient_profile_arrays      ON lookup.id = patient_profile_arrays.lookup_id WHERE (lookup.resource_id = resource_id AND patient_profile_arrays.patient_profile_id=@id) IF len(@OnSameLine)>1 BEGIN set @OnSameLine = substring(@OnSameLine,1,len(@OnSameLine)-2) END;  RETURN @OnSameLine; END
--GO
--CREATE FUNCTION fnLookupValue(@id int) RETURNS varchar(255) BEGIN DECLARE @value varchar(255); set @value=''; select @value = name from lookup where id=@id RETURN @value; END
--GO
--CREATE function fnInvestigations(@id int) RETURNS varchar(max) BEGIN DECLARE @value varchar(255); set @value=''; select @value = @value +convert(varchar(25),investigation_date,101)+': ' +coalesce (lookup.name,'')+', ' FROM lookup INNER JOIN investigations     ON lookup.id = investigations.lookup_id WHERE (investigations.patient_profile_id=@id) IF len(@value)>1 BEGIN set @value = substring(@value,1,len(@value)-2); END; RETURN @value; END
--GO
--CREATE FUNCTION fnGetProduct(@id int) RETURNS VARCHAR(255) BEGIN DECLARE @value varchar(255); set @value=''; select @value = title from products where id=@id RETURN @value; END
--GO
--CREATE VIEW patient_profile_view AS select patient_id, dbo.fnLookupValue(funding_source_id) as funding_source, start_date, user_signature, professionals, dbo.fnLookupValue(treatment_location_id) as treatmentLocation, albumin,convert(varchar(25),albumin_date,101) as albumin_date, pre_albumin,convert(varchar(25),prealbumin_date,101) as prealbumin_date, blood_sugar, convert(varchar(25),blood_sugar_date,101) as blood_sugar_date,blood_sugar_meal, dbo.fnPatientProfileOnSameLine(id,6,', ') as interfering_factors, dbo.fnPatientProfileOnSameLine(id,165,', ') as interfering_medications, dbo.fnInvestigations(id) as investigations,dbo.fnPatientProfileOnSameLine(id,5,', ') as comorbidities from patient_profiles where  active=1
--GO
--CREATE VIEW wound_profile_view AS select wounds.patient_id, wound_id, prev_prods as product_history, convert(varchar(25),wounds.start_date,101) as start_date,user_signature, wound_location_detailed,'images/blueman/'+image_name as image_name,status,convert(varchar(25),closed_date,101) as closed_date,dbo.fnLookupValue(pressure_ulcer) as pressure_ulcer, reason_for_care, surgeon, operative_procedure_comments, marking_prior_surgery, convert(varchar(25), date_surgery, 101) as surgery_date, dbo.fnLookupValue(teaching_goals) as teaching_goals, convert(varchar(25), preliminary_tbsa, 101) as preliminary_tbsa, convert(varchar(25), final_tbsa, 101) as final_tbsa,tbsa_percentage from wound_profiles LEFT JOIN wounds ON wounds.id=wound_profiles.wound_id where wound_profiles.active=1
--GO
--CREATE VIEW goals_view AS select patient_id, wound_profile_id, alpha_id, dbo.fnGetAlpha(alpha_id) as alpha,dbo.fnLookupValue(lookup_id) as goal from wound_goals LEFT JOIN wound_profiles ON wound_profiles.id=wound_goals.wound_profile_id
--GO
--CREATE VIEW etiology_view AS select patient_id, wound_profile_id, alpha_id, dbo.fnGetAlpha(alpha_id) as alpha, dbo.fnLookupValue(lookup_id) as etiology from wound_etiology lEfT join wound_profiles ON wound_profiles.id=wound_etiology.wound_profile_id
--GO
--Need to do this sooner because 5.0.1 TuffConversion breaks...
CREATE TABLE languages (id int NOT NULL identity,primary key(id), name varchar(255)) ;
CREATE TABLE lookup_l10n (id int identity,primary key(id),language_id int,lookup_id int,name varchar(max),resource_id int);
ALTER TABLE lookup_l10n add Foreign key (lookup_id) REFERENCES lookup(id) ;
ALTER TABLE lookup_l10n add Foreign key (language_id) REFERENCES languages(id) ;
insert into languages (name) values ('en');
insert into languages (name) values ('fr'); 
insert into  lookup_l10n (language_id,lookup_id,name,resource_id) select 1,id,name,resource_id from lookup;
alter table lookup drop column name;
update resources set lock_add_n_del=0;
update resources set lock_add_n_del=1 where id=175;
EXEC sp_rename 'assessment_eachwound', 'assessment_wound';
--alter table assessment_sinustracts drop constraint assessment_sinustracts_ibfk_1;
EXEC sp_rename 'assessment_sinustracts.assessment_eachwound_id','assessment_wound_id', 'COLUMN';
