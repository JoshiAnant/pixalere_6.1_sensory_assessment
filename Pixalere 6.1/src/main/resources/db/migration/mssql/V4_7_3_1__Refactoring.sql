INSERT INTO user_account_regions (user_account_id,treatment_location_id) SELECT professional_accounts.id,lookup.id from professional_accounts   LEFT JOIN lookup ON professional_accounts.regions LIKE ('%"'+CONVERT(VARCHAR,lookup.id)+'"%') AND lookup.id IS NOT NULL;
--alter table lookup drop DF__listdata__indent__3B75D760;
alter table lookup drop column checkbox_column;
alter table lookup drop column indent;
alter table products drop column cost2;
alter table products drop column groupby;
