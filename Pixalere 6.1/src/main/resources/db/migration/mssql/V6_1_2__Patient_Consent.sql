INSERT INTO configuration 
(category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) 
VALUES (120, 1, 1, 'patientConsentEnabled', '', '', 'CHKB', '', '', '0', 0);

INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentIntro', '', '', 'TEXT', '', '', 'VHA Home HealthCare (VHA) is pleased to be providing service to you by request of your local Community Care Access Centre (CCAC).\nIn order to provide the very best service, VHA’s staff will be asking you for your consent:\nTo provide nursing services to you as described below and in the Care Plan\nTo ask about, record, and use your health information as necessary to provide service to you (see explanation below)\nTo use your telephone to call the VHA office (at no cost to you) to record the time of arrival and departure of our staff (to ensure that you are receiving the service).', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentCriteria1', '', '', 'INPT', '', '', 'General Services', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentCriteria2', '', '', 'INPT', '', '', 'Specific Procedures', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentSharingHeader', '', '', 'TEXT', '', '', 'In addition to the CCAC, my doctor and others providing care to me, VHA may share my personal health information with:', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentDetails', '', '', 'TEXT', '', '', 'Consents for Service\nI authorize VHA Home HealthCare to provide the health services described above and detailed in my Care Plan.  The nature, benefits, and risks of these services have been explained to me along with any alternatives to these services\nAny changes in my services will be discussed with me before they are made', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentServicesChk', '', '', 'INPT', '', '', 'I consent to receiving services as stated above.', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentInformationChk', '', '', 'INPT', '', '', 'I consent to VHA collecting and using my personal health information as stated above.', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentDisclaimers', '', '', 'TEXT', '', '', 'I understand that I may ask to see and/or get a copy of my health information I give to VHA\nI understand that I can add an addendum to my VHA record to correct information\nI understand that VHA may share my information with the persons or organizations named in this document\nI understand that I can complain to VHAs Privacy Officer about how my information is handled and that I can withdraw my consent at any time. VHAs Privacy Officer can be reached at 416-482-8782 or 1-888-314-6622 ext. 8782.', 0);
INSERT INTO configuration (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly) VALUES (120, 1, 1, 'consentAcceptance', '', '', 'TEXT', '', '', 'This summary has been reviewed with me.  I have had an opportunity to have my questions answered about my services, VHAs privacy policy and my rights.', 0);
set identity_insert resources On
INSERT INTO resources (id,name,report_value_label_key) VALUES (205,'Verbal Patient Consent','pixalere.admin.resources.form.patient_consent_verbal');
set identity_insert resources Off
INSERT INTO lookup (active,resource_id) VALUES (1,205);
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 1, id, 'Client',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 2, id, 'Cliente',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 3, id, 'Cliente',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup (active,resource_id) VALUES (1,205);
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 1, id, 'Parent/Guardian',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 2, id, 'Parent/Tuteur',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 3, id, 'Padre/Tutor',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup (active,resource_id) VALUES (1,205);
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 1, id, 'Substitute Decision Maker',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 2, id, 'Subrogé',resource_id FROM lookup ORDER BY id DESC;
INSERT INTO lookup_l10n (language_id,lookup_id,name,resource_id)  SELECT top 1 3, id, 'Responsable sustituto',resource_id FROM lookup ORDER BY id DESC;
ALTER TABLE patient_accounts ADD patient_consent_name VARCHAR(255);
ALTER TABLE patient_accounts ADD patient_consent_sig VARCHAR(255);
ALTER TABLE patient_accounts ADD patient_consent_blurb VARCHAR(MAX);
ALTER TABLE patient_accounts ADD consent_services INT NULL DEFAULT '0';
ALTER TABLE patient_accounts ADD consent_information INT NULL DEFAULT '0';
ALTER TABLE patient_accounts ADD consent_verbal INT;
ALTER TABLE patient_accounts ADD consent_date datetime;
INSERT INTO configuration  (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)VALUES (0, 1, 1, 'disableBackspace', '', '', 'CHKB', '', '', '0', 0);
INSERT INTO configuration  (category, orderby, selfadmin, config_setting, enable_component_rule, enable_options_rule, component_type, component_options, component_values, current_value, readonly)VALUES (0, 1, 1, 'disablePopProducts', '', '', 'CHKB', '', '', '0', 0);
INSERT INTO components 
(resource_id, field_name, field_type, component_type, flowchart, label_key, 
	table_id, orderby, required, enabled_close, info_popup_id, allow_edit, 
	validation, hide_flowchart,hide_gui) 
VALUES 
(0,'view_patient_consent','STRING','VPCO',1,'pixalere.patientprofile.form.view_patient_consent',
	NULL,47,0,0,0,0,
	NULL, 0,0);