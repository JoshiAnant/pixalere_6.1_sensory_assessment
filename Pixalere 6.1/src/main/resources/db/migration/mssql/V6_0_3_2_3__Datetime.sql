CREATE INDEX diagnostic_index ON diagnostic_investigation ([patient_id], [active],[created_on],[deleted]);
CREATE  NONCLUSTERED INDEX id ON wound_etiology (id);
CREATE  NONCLUSTERED INDEX id ON wound_goals (id);
CREATE  NONCLUSTERED INDEX id ON patient_investigations (id);
update components set orderby=15 where field_name='fistulas';
update components set orderby=16 where field_name='wound_base';
update components set orderby=17 where field_name='exudate_type';
update components set orderby=18 where field_name='exudate_amount';
update components set orderby=19 where field_name='exudate_odour';
update components set orderby=20 where field_name='wound_edge';
update components set orderby=21 where field_name='periwound_skin';
update components set orderby=22,hide_gui=1,hide_flowchart=1 where field_name='push_scale';
update components set resource_id=136 where field_name='construction';
update components set allow_edit=0,table_id=23 where field_name='mucocutaneous_margin_depth';
update components set table_id=23 where field_name='mucocutaneous_margin_location';
update configuration set current_value=0 where config_setting='enforceLengthLargerThanWidth';
update referrals_tracking set current_flag=0 where current_flag=1 and entry_type=1 and exists (select id from patient_accounts where id=referrals_tracking.patient_account_id and account_status=0);
update referrals_tracking set current_flag=0 where current_flag=1 and entry_type=1 and exists (select id from wounds where id=referrals_tracking.wound_id and status='Closed');
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',3, 'pixalere.common.visited_on', NULL,  5,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',4, 'pixalere.common.visited_on', NULL,  6,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',5, 'pixalere.common.visited_on', NULL,  3,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',6, 'pixalere.common.visited_on', NULL,  3,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',17, 'pixalere.common.visited_on', NULL,  5,  0, 0, 0, 0, NULL, 0);
INSERT INTO components ( resource_id, field_name, field_type, component_type, flowchart, label_key, table_id,  orderby,  required, enabled_close,  info_popup_id, allow_edit, validation, hide_flowchart) VALUES( 0, 'visited_on', 'DATE', 'DATE',50, 'pixalere.common.visited_on', NULL,  5,  0, 0, 0, 0, NULL, 0);
alter table assessment_antibiotics drop constraint fk_ad_alpha_id;
alter table assessment_antibiotics drop column alpha_id;
update components set allow_edit=1 where field_name='professionals';
update components set allow_edit=1 where field_name='surgical_history';
update components set allow_edit=1 where field_name='external_referrals';
update components set allow_edit=1 where field_name='co_morbidities';
delete from lookup_l10n where lookup_id=200 or lookup_id=201;
delete from lookup where id=200 or id=201;
set identity_insert lookup On;
insert into lookup (id,active,resource_id,locked) values(200,1,50,0);
set identity_insert lookup Off;
insert into lookup_l10n (language_id,lookup_id,name,resource_id) values (1,200,'Training Area',50);
set identity_insert lookup On;
insert into lookup (id,active,resource_id,category_id,locked) values(201,1,51,5,0);
set identity_insert lookup Off;
insert into lookup_l10n (language_id,lookup_id,name,resource_id) values (1,201,'Training Location',51);
update components set allow_required=0,required=0,allow_edit=0 where field_name='funding_source';
update components set allow_required=0 where field_name='pain_comments';
update lookup_l10n set name='+1 Trace' where name='+1 Trace 2mm pitting';
update lookup_l10n set name='+2 Trace' where name='+2 Trace 4mm pitting';
update lookup_l10n set name='+3 Trace' where name='+3 Trace 6mm pitting';
update lookup_l10n set name='+4 Trace' where name='+4 Trace 8mm pitting';
CREATE STATISTICS [assess_prod_stat1] ON [dbo].[assessment_product]([id], [wound_assessment_id])
CREATE STATISTICS [assess_prod_stat2] ON [dbo].[assessment_product]([assessment_id], [alpha_id], [id])
CREATE STATISTICS [assess_prod_stat3] ON [dbo].[assessment_product]([id], [alpha_id])
CREATE STATISTICS [assess_prod_stat4] ON [dbo].[assessment_product]([id], [assessment_id])
CREATE CLUSTERED INDEX [assess_sinus_index1] ON [dbo].[assessment_sinustracts] ([assessment_wound_id] ASC)
CREATE STATISTICS [assess_sinus_stat1] ON [dbo].[assessment_sinustracts]([id], [assessment_wound_id])
CREATE STATISTICS [assess_treat_stat1] ON [dbo].[assessment_treatment]([assessment_id], [alpha_id])
CREATE STATISTICS [assess_treat_stat2] ON [dbo].[assessment_treatment]([id], [assessment_id], [alpha_id])
CREATE NONCLUSTERED INDEX [assess_treat_clustindex] ON [dbo].[assessment_treatment] ([alpha_id] ASC,[assessment_id] ASC,[id] ASC)INCLUDE ( [wound_id],[review_done],[adjunctive_other],[cs_date],[bandages_in],[bandages_out],[wound_profile_type_id],[patient_id],[professional_id],[cs_done])
CREATE STATISTICS [assess_treatarray_stat1] ON [dbo].[assessment_treatment_arrays]([id], [assessment_treatment_id])
CREATE STATISTICS [assess_under_stat1] ON [dbo].[assessment_undermining]([id], [assessment_wound_id])
CREATE STATISTICS [assess_wound_stat1] ON [dbo].[assessment_wound]([id], [alpha_id], [active], [wound_profile_type_id], [assessment_id])
CREATE STATISTICS [assess_wound_stat2] ON [dbo].[assessment_wound]([id], [assessment_id], [alpha_id])
CREATE STATISTICS [assess_wound_stat3] ON [dbo].[assessment_wound]([id], [wound_profile_type_id], [alpha_id])
CREATE STATISTICS [assess_wound_stat4] ON [dbo].[assessment_wound]([alpha_id], [active], [id], [assessment_id])
CREATE STATISTICS [assess_wound_stat5] ON [dbo].[assessment_wound]([assessment_id], [wound_profile_type_id], [alpha_id])
CREATE STATISTICS [assess_wound_stat6] ON [dbo].[assessment_wound]([wound_profile_type_id], [alpha_id], [active], [assessment_id])
CREATE CLUSTERED INDEX [assess_woundbed_clustindex] ON [dbo].[assessment_woundbed] ([assessment_wound_id] ASC,[id] ASC)
CREATE STATISTICS [assess_woundbed_stat1] ON [dbo].[assessment_woundbed]([assessment_wound_id], [id])
CREATE STATISTICS [assess_woundbed_stat2] ON [dbo].[assessment_woundbed]([id])
CREATE CLUSTERED INDEX [limb_upper_clustindex] ON [dbo].[limb_upper_assessment]([patient_id] ASC,[active] ASC,[deleted] ASC)
CREATE STATISTICS [limb_upper_stat1] ON [dbo].[limb_upper_assessment]([created_on], [patient_id])
CREATE STATISTICS [limb_upper_stat2] ON [dbo].[limb_upper_assessment]([patient_id], [active], [deleted])
CREATE STATISTICS [limb_upper_stat3] ON [dbo].[limb_upper_assessment]([id], [patient_id], [active], [deleted])
CREATE STATISTICS [limb_upper_stat4] ON [dbo].[limb_upper_assessment]([active], [patient_id], [created_on])
CREATE STATISTICS [lookup_l10n_stat1] ON [dbo].[lookup_l10n]([lookup_id], [id])
alter table patient_accounts alter column name varchar(255);
alter table patient_accounts alter column lastname varchar(255);
alter table patient_accounts alter column lastname_search varchar(255);
alter table professional_accounts alter column user_name varchar(255);
CREATE NONCLUSTERED INDEX [patient_acc_index1] ON [dbo].[patient_accounts] ([account_status] ASC,[current_flag] ASC,[treatment_location_id] ASC,[name] ASC,[lastname] ASC)INCLUDE ( [id],[patient_id],[discharge_reason])
CREATE STATISTICS [patient_acc_stat1] ON [dbo].[patient_accounts]([account_status], [id], [treatment_location_id], [patient_id])
CREATE STATISTICS [patient_acc_stat2] ON [dbo].[patient_accounts]([name], [lastname], [treatment_location_id], [account_status])
CREATE STATISTICS [patient_acc_stat3] ON [dbo].[patient_accounts]([treatment_location_id], [account_status])
CREATE STATISTICS [patient_acc_stat4] ON [dbo].[patient_accounts]([discharge_date], [current_flag], [account_status], [treatment_location_id])
CREATE STATISTICS [patient_acc_stat5] ON [dbo].[patient_accounts]([id], [patient_id], [current_flag])
CREATE STATISTICS [patient_acc_stat6] ON [dbo].[patient_accounts]([patient_id], [treatment_location_id], [account_status])
CREATE STATISTICS [patient_acc_stat7] ON [dbo].[patient_accounts]([id], [treatment_location_id])
CREATE CLUSTERED INDEX [patient_prof_arrays_clustindex] ON [dbo].[patient_profile_arrays] ([patient_profile_id] ASC,[id] ASC)
CREATE STATISTICS [patient_prof_arrays_stat1] ON [dbo].[patient_profile_arrays]([id], [patient_profile_id])
CREATE STATISTICS [ref_track_stat1] ON [dbo].[referrals_tracking]([assessment_id], [wound_profile_type_id], [id])
CREATE STATISTICS [ref_track_stat2] ON [dbo].[referrals_tracking]([current_flag], [entry_type], [wound_profile_type_id], [id])
CREATE STATISTICS [ref_track_stat3] ON [dbo].[referrals_tracking]([patient_account_id], [current_flag])
CREATE STATISTICS [ref_track_stat4] ON [dbo].[referrals_tracking]([patient_account_id], [entry_type])
CREATE STATISTICS [ref_track_stat5] ON [dbo].[referrals_tracking]([entry_type], [assessment_id])
CREATE STATISTICS [ref_track_stat6] ON [dbo].[referrals_tracking]([id], [entry_type], [current_flag])
CREATE STATISTICS [ref_track_stat7] ON [dbo].[referrals_tracking]([id], [wound_profile_type_id], [entry_type])
CREATE STATISTICS [ref_track_stat8] ON [dbo].[referrals_tracking]([entry_type], [current_flag], [patient_account_id])
CREATE STATISTICS [ref_track_stat9] ON [dbo].[referrals_tracking]([id], [assessment_id], [entry_type])
CREATE STATISTICS [wound_assess_stat1] ON [dbo].[wound_assessment]([id], [created_on])
CREATE STATISTICS [wound_assess_stat2] ON [dbo].[wound_assessment]([created_on], [wound_id], [active])
CREATE NONCLUSTERED INDEX [wound_assess_index1] ON [dbo].[wound_assessment] ([wound_id] ASC,[active] ASC,[id] ASC,[created_on] ASC)INCLUDE ( [patient_id],[professional_id],[user_signature],[visit],[reset_visit],[reset_reason],[treatment_location_id],[has_comments],[offline_flag],[lastmodified_on],[visited_on])
CREATE STATISTICS [wound_assess_stat3] ON [dbo].[wound_assessment]([has_comments], [id])
CREATE STATISTICS [wal_stat1] ON [dbo].[wound_assessment_location]([id], [wound_profile_type_id], [patient_id])
CREATE STATISTICS [wal_stat2] ON [dbo].[wound_assessment_location]([id], [wound_profile_type_id], [wound_id], [active])
CREATE STATISTICS [wal_stat3] ON [dbo].[wound_assessment_location]([wound_id], [active], [alpha])
CREATE STATISTICS [wal_stat4] ON [dbo].[wound_assessment_location]([id], [patient_id], [wound_id], [alpha])
CREATE STATISTICS [wal_stat5] ON [dbo].[wound_assessment_location]([alpha], [wound_id], [patient_id])
CREATE STATISTICS [wal_stat6] ON [dbo].[wound_assessment_location]([active], [wound_profile_type_id], [id])
CREATE STATISTICS [wal_stat7] ON [dbo].[wound_assessment_location]([id], [wound_profile_type_id], [active], [discharge])
CREATE STATISTICS [wal_stat8] ON [dbo].[wound_assessment_location]([id], [wound_id], [active], [alpha], [wound_profile_type_id])
CREATE STATISTICS [wal_stat9] ON [dbo].[wound_assessment_location]([patient_id], [wound_profile_type_id])
CREATE STATISTICS [wal_stat10] ON [dbo].[wound_assessment_location]([patient_id], [id], [alpha])
CREATE NONCLUSTERED INDEX [wal_index1] ON [dbo].[wound_assessment_location] ([wound_profile_type_id] ASC,[id] ASC)INCLUDE ( [patient_id],[wound_id],[alpha],[active],[discharge],[professional_id],[reactivate],[open_time_stamp],[close_time_stamp],[lastmodified_on],[x],[y],[deleted],[delete_signature],[deleted_reason],[deleted_on])
CREATE NONCLUSTERED INDEX [_dta_index_wound_assessment_location_10_757577737__K11_K3_K4_1_2_8_9_10_12_13_14_15_16_17_18_19_20_21] ON [dbo].[wound_assessment_location] ([wound_profile_type_id] ASC,[id] ASC,[alpha] ASC)INCLUDE ( [patient_id],[wound_id],[active],[discharge],[professional_id],[reactivate],[open_time_stamp],[close_time_stamp],[lastmodified_on],[x],[y],[deleted],[delete_signature],[deleted_reason],[deleted_on])
CREATE STATISTICS [wal_stat11] ON [dbo].[wound_assessment_location]([wound_profile_type_id], [discharge], [id])
CREATE STATISTICS [wal_stat12] ON [dbo].[wound_assessment_location]([wound_id], [id], [alpha])
CREATE STATISTICS [wound_etiology_stat1] ON [dbo].[wound_etiology]([alpha_id], [wound_profile_id])
CREATE STATISTICS [wound_etiology_stat1] ON [dbo].[wound_goals]([alpha_id])
CREATE STATISTICS [wound_etiology_stat2] ON [dbo].[wound_goals]([wound_profile_id], [alpha_id])
CREATE CLUSTERED INDEX [user_acc_region_clustindex1] ON [dbo].[user_account_regions] ([user_account_id] ASC,[id] ASC)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
CREATE STATISTICS [user_acc_region_stat1] ON [dbo].[user_account_regions]([id], [user_account_id])
CREATE STATISTICS [wal_details_stat1] ON [dbo].[wound_location_details]([id], [alpha_id])
--CREATE VIEW [dbo].[referrals_tracking_view] WITH SCHEMABINDING AS SELECT  [dbo].[patient_accounts].[account_status] as _col_1,  [dbo].[patient_accounts].[treatment_location_id] as _col_2,  [dbo].[referrals_tracking].[current_flag] as _col_3,  [dbo].[referrals_tracking].[entry_type] as _col_4,  [dbo].[referrals_tracking].[id] as _col_5,  [dbo].[referrals_tracking].[professional_id] as _col_6,  [dbo].[referrals_tracking].[wound_id] as _col_7,  [dbo].[referrals_tracking].[created_on] as _col_8,  [dbo].[referrals_tracking].[top_priority] as _col_9,  [dbo].[referrals_tracking].[assessment_id] as _col_10,  [dbo].[referrals_tracking].[patient_account_id] as _col_11,  [dbo].[referrals_tracking].[wound_profile_type_id] as _col_12,  [dbo].[referrals_tracking].[active] as _col_13,  [dbo].[referrals_tracking].[patient_id] as _col_14,  [dbo].[referrals_tracking].[assigned_to] as _col_15,  [dbo].[patient_accounts].[patient_id] as _col_16,  count_big(*) as _col_17 FROM  [dbo].[referrals_tracking],  [dbo].[patient_accounts]   WHERE  [dbo].[referrals_tracking].[patient_account_id] = [dbo].[patient_accounts].[id]  GROUP BY  [dbo].[patient_accounts].[account_status],  [dbo].[patient_accounts].[treatment_location_id],  [dbo].[referrals_tracking].[current_flag],  [dbo].[referrals_tracking].[entry_type],  [dbo].[referrals_tracking].[id],  [dbo].[referrals_tracking].[professional_id],  [dbo].[referrals_tracking].[wound_id],  [dbo].[referrals_tracking].[created_on],  [dbo].[referrals_tracking].[top_priority],  [dbo].[referrals_tracking].[assessment_id],  [dbo].[referrals_tracking].[patient_account_id],  [dbo].[referrals_tracking].[wound_profile_type_id],  [dbo].[referrals_tracking].[active],  [dbo].[referrals_tracking].[patient_id],  [dbo].[referrals_tracking].[assigned_to],  [dbo].[patient_accounts].[patient_id]  
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'images_index_1442_1441' AND object_id = OBJECT_ID('assessment_images'))
    BEGIN
       CREATE INDEX images_index_1442_1441 ON [assessment_images] ([assessment_id], [wound_profile_type_id]);
    END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'comments_index_1389_1388' AND object_id = OBJECT_ID('assessment_comments'))
    BEGIN
       CREATE INDEX comments_index_1389_1388 ON [assessment_comments] ([assessment_id], [wound_profile_type_id]);
    END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'eachwound_index_1846_1845' AND object_id = OBJECT_ID('assessment_wound'))
    BEGIN
       CREATE INDEX eachwound_index_1846_1845 ON [assessment_wound] ([alpha_id], [active]);
    END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'nursing_fixes_index_1650_1649' AND object_id = OBJECT_ID('nursing_fixes'))
    BEGIN
       CREATE INDEX nursing_fixes_index_1650_1649 ON [nursing_fixes] ([row_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ncp_index_2448_2447' AND object_id = OBJECT_ID('nursing_care_plan'))
    BEGIN
       CREATE INDEX ncp_index_2448_2447 ON [nursing_care_plan] ([assessment_id], [wound_profile_type_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ncp_index_1451_1450' AND object_id = OBJECT_ID('nursing_care_plan'))
    BEGIN
       CREATE INDEX ncp_index_1451_1450 ON [nursing_care_plan] ([active], [wound_profile_type_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'comments_index_1658_1657' AND object_id = OBJECT_ID('assessment_comments'))
    BEGIN
       CREATE INDEX comments_index_1658_1657 ON [assessment_comments] ([assessment_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'alphas_index_1393_1392' AND object_id = OBJECT_ID('wound_assessment_location'))
    BEGIN
       CREATE INDEX alphas_index_1393_1392 ON [wound_assessment_location] ([wound_id], [active]) ;
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ncp_index_2489_2488' AND object_id = OBJECT_ID('nursing_care_plan'))
    BEGIN
       CREATE INDEX ncp_index_2489_2488 ON [nursing_care_plan] ([assessment_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'images_index_1854_1853' AND object_id = OBJECT_ID('assessment_images'))
    BEGIN
       CREATE INDEX images_index_1854_1853 ON [assessment_images] ([assessment_id], [alpha_id], [which_image]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'images_index_1656_1655' AND object_id = OBJECT_ID('assessment_images'))
    BEGIN
       CREATE INDEX images_index_1656_1655 ON [assessment_images] ([assessment_id], [alpha_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'treamtentcomments_index_1653_1652' AND object_id = OBJECT_ID('treatment_comments'))
    BEGIN
       CREATE INDEX treamtentcomments_index_1653_1652 ON [treatment_comments] ([assessment_id], [wound_profile_type_id]) ;
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'dcf_index_2446_2445' AND object_id = OBJECT_ID('dressing_change_frequency'))
    BEGIN
       CREATE INDEX dcf_index_2446_2445 ON [dressing_change_frequency] ([assessment_id], [wound_profile_type_id], [alpha_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'eachwound_index_1860_1859' AND object_id = OBJECT_ID('assessment_wound'))
    BEGIN
       CREATE INDEX eachwound_index_1860_1859 ON [assessment_wound] ([alpha_id], [active], [wound_profile_type_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'dcf_index_2444_2443' AND object_id = OBJECT_ID('dressing_change_frequency'))
    BEGIN
       CREATE INDEX dcf_index_2444_2443 ON [dressing_change_frequency] ([alpha_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'dcf_index_1453_1452' AND object_id = OBJECT_ID('dressing_change_frequency'))
    BEGIN
       CREATE INDEX dcf_index_1453_1452 ON [dressing_change_frequency] ([wound_profile_type_id], [alpha_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'dcf_index_2478_2477' AND object_id = OBJECT_ID('dressing_change_frequency'))
    BEGIN
       CREATE INDEX dcf_index_2478_2477 ON [dressing_change_frequency] ([assessment_id], [alpha_id]) ;
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'dcf_index_1449_1448' AND object_id = OBJECT_ID('dressing_change_frequency'))
    BEGIN
       CREATE INDEX dcf_index_1449_1448 ON [dressing_change_frequency] ([active], [wound_profile_type_id]) ;
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'patient_accounts_1385_1384' AND object_id = OBJECT_ID('patient_accounts'))
    BEGIN
       CREATE INDEX patient_accounts_1385_1384 ON [patient_accounts] ([treatment_location_id],[patient_id], [current_flag]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'assessment_images_2481_2480' AND object_id = OBJECT_ID('assessment_images'))
    BEGIN
       CREATE INDEX assessment_images_2481_2480 ON [assessment_images] ([assessment_id]);
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'patientprofile_index_1385_1384' AND object_id = OBJECT_ID('patient_profiles'))
    BEGIN
       CREATE INDEX patientprofile_index_1385_1384 ON patient_profiles ([patient_id], [current_flag],active)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'limb_index_1385_1384' AND object_id = OBJECT_ID('limb_basic_assessment'))
    BEGIN
       CREATE INDEX limb_index_1385_1384 ON limb_basic_assessment ([patient_id], active)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'foot_index_1385_1384' AND object_id = OBJECT_ID('foot_assessment'))
    BEGIN
       CREATE INDEX foot_index_1385_1384 ON foot_assessment ([patient_id],active)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'referrals_index' AND object_id = OBJECT_ID('referrals_tracking'))
    BEGIN
       CREATE INDEX referrals_index ON referrals_tracking (assessment_id,patient_account_id, current_flag, professional_id,entry_type,wound_id,wound_profile_type_id)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'professionals_index' AND object_id = OBJECT_ID('professional_accounts'))
    BEGIN
       CREATE INDEX professionals_index ON professional_accounts (id,user_name, account_status)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'woundassessment_index' AND object_id = OBJECT_ID('wound_assessment'))
    BEGIN
       CREATE INDEX woundassessment_index ON wound_assessment (patient_id,wound_id, active)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'wounds_index' AND object_id = OBJECT_ID('wounds'))
    BEGIN
       CREATE INDEX wounds_index ON wounds (patient_id, active)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'woundprofiles_index' AND object_id = OBJECT_ID('wound_profiles'))
    BEGIN
       CREATE INDEX woundprofiles_index ON wound_profiles (patient_id,wound_id,active)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'woundprofilestypes_index' AND object_id = OBJECT_ID('wound_profile_type'))
    BEGIN
       CREATE INDEX woundprofilestypes_index ON wound_profile_type (patient_id,wound_id,active)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'wal_index' AND object_id = OBJECT_ID('wound_assessment_location'))
    BEGIN
       CREATE INDEX wal_index ON wound_assessment_location (patient_id,wound_id,wound_profile_type_id,active)
END
CREATE INDEX productsfac_index1 ON products_by_facility (product_id, treatment_location_id);
CREATE INDEX lookupl10n_index1 ON lookup_l10n (lookup_id, language_id);
CREATE INDEX lookup_index1 ON lookup (resource_id);