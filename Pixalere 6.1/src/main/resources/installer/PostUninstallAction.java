import com.install4j.api.*;
import com.install4j.api.windows.WinEnvVars;



import java.io.*;
public class PostUninstallAction extends UninstallAction
{

    public String BASE_PATH = "c:\\Program Files\\Pixalere Offline\\";

	private boolean canceled;

    // Implementation of abstract method. This method is called when the action should be performed.
    // You can use the progress interface to tell the user how the installation is proceeding.
    // The InstallerWizardContext provides access to all important information and to installation-
    // related services.

    public boolean performAction(Context context, ProgressInterface progressInterface) {
            deleteDir(new File(BASE_PATH+"ApplicationServer"));
        deleteDir(new File(BASE_PATH+"photos"));
        return true;
        }

        // If you want the unattended installer to remove the file type
        // as well, you have to override the performUnattendedAction
        // method. The actual code has been factored out to the
        // separate unassociateFileType method.
        public boolean performUnattendedAction(Context context) {
            deleteDir(new File(BASE_PATH+"ApplicationServer"));
        deleteDir(new File(BASE_PATH+"photos"));
        return true;
        }

    

	public int getPercentOfTotalInstallation() {
        return 10;
    }
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
/*public PathInstallAction (){
CmdExec("C:\\Program Files\\Pixalere Offline\\ApplicationServer\\jakarta-tomcat-5.0.28\\bin\\installService.bat");

}
public static void main(String argv[]) {

}*/
}
