package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testSkinAssessment  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestSkinAssessment() throws Exception {
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Pelvis (Anterior)");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img.ui-datepicker-trigger");
		selenium.click("link=1");
		selenium.select("id=status", "label=Status1");
		selenium.click("id=recurrent1");
	
		selenium.select("id=pain", "label=1");
		selenium.type("id=pain_comments", "test");
		selenium.type("id=site", "site");
		selenium.type("id=length", "18");
		selenium.type("id=width", "4");
		selenium.type("id=depth", "4");
		selenium.type("id=length", "8");
		
		selenium.click("css=#navTreatment > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=products", "label=Acticoat 10cm x 10cm");
		selenium.select("wound_ass", "Skin 1");
		selenium.click("id=addAlpha");
		selenium.select("id=dressing_change_frequency_alphas", "label=Skin 1");

	
		selenium.select("id=dressing_change_frequency_list", "label=2x weekly");
	
		selenium.click("id=review_done");
		selenium.click("id=adddressing_change_frequency");
                
		selenium.select("id=visit_freq", "label=TID");
		selenium.click("css=option[value=\"243\"]");
		selenium.click("id=as_needed");
                selenium.click("id=cs_done1");
		selenium.type("id=skin_treatment_comments", "test1");
		selenium.select("id=csresult_alphas", "label=Skin 1");
	
		selenium.select("id=csresult_list", "label=Beta-hemolytic Strep");
	
		selenium.click("id=addcsresult");
                selenium.selectFrame("skin_nursing_care_plan_ifr");
                selenium.focus("id=tinymce");
		selenium.type("id=tinymce", "Full Assessment due1");
                selenium.selectWindow("null");
                driver.switchTo().defaultContent();
                //java.io.File scrFile3 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                //org.apache.commons.io.FileUtils.copyFile(scrFile3, new java.io.File("c:\\test22.jpg"));
		selenium.click("css=#navSummary > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
                selenium.click("css=#ui-accordion-accordion-header-1 > label.title");
		assertTrue(selenium.isTextPresent("Status: Status1"));
		assertTrue(selenium.isTextPresent("Pain Scale (0-10): 1"));
		assertTrue(selenium.isTextPresent("Pain Comments: test"));
		assertTrue(selenium.isTextPresent("Length (cm): 8.0"));
		assertTrue(selenium.isTextPresent("Width (cm): 4.0"));
		assertTrue(selenium.isTextPresent("Depth (cm): 4.0"));
		assertTrue(selenium.isTextPresent("Products: 1 Acticoat 10cm x 10cm"));
		assertTrue(selenium.isTextPresent("Treatment Comments: test1"));
		assertTrue(selenium.isTextPresent("C&S Result: Beta-hemolytic Strep"));
		selenium.click("xpath=(//input[@name='referral_skin'])[2]");
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
