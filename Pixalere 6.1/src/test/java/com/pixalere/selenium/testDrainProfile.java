/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class testDrainProfile  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                // selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		selenium.start();
	}
	@Test
	public void testTestWoundProfile() throws Exception {
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
    	
		selenium.select("id=wound_id", "label=New Wound Profile");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=anterior", "label=Chest");
		selenium.click("link=Tubes and Drains");
		selenium.dragAndDropToObject("id=dragTD", "id=blueman_grid");
              // WebElement element = driver.findElement(By.id("dragTD"));
              //  WebElement target = driver.findElement(By.id("blueman_grid"));
               // (new Actions(driver)).dragAndDrop(element, target).perform();
		//selenium.dragAndDropToObject("id=dragTD", "id=blueman_grid");
                /*selenium.mouseMove("id=dragTD");
                selenium.mouseDown("id=dragTD");
                selenium.mouseMove("id=blueman_grid");
                selenium.mouseUp("id=blueman_grid");*/
		selenium.click("link=Incision");
                //element = driver.findElement(By.id("dragTAG"));
                // target = driver.findElement(By.id("blueman_grid"));
                //(new Actions(driver)).dragAndDrop(element, target).perform();
		selenium.dragAndDropToObject("id=dragTAG", "id=blueman_grid");
		/*selenium.mouseMove("id=dragTAG");
                selenium.mouseDown("id=dragTAG");
                selenium.mouseMove("id=blueman_grid");
                selenium.mouseUp("id=blueman_grid");*/
                selenium.windowMaximize();
               
                       // java.io.File scrFile = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                //org.apache.commons.io.FileUtils.copyFile(scrFile, new java.io.File("c:\\test2.jpg"));
                
		selenium.addSelection("id=etiology_alphas", "label=Incision 1");
                selenium.click("css=#etiology_alphas > option[value=\"Incision 1\"]");
                selenium.addSelection("id=etiology_alphas", "label=Tube/Drain 1");
                selenium.click("css=#etiology_alphas > option[value=\"Tube/Drain 1\"]");
                
		selenium.select("id=etiology_list", "label=Obstruction");
		selenium.click("id=addetiology");
		selenium.addSelection("id=goals_alphas", "label=Incision 1");
                selenium.click("css=#goals_alphas > option[value=\"Incision 1\"]");
                selenium.addSelection("id=goals_alphas", "label=Tube/Drain 1");
                //selenium.click("css=#goals_alphas > option[value=\"Tube/Drain 1\"]");
                //selenium.captureScreenshot("c:\\\\test23.jpg");
		selenium.select("id=goals_list", "label=Temporary tube/drain");
	
		selenium.click("id=addgoals");
		selenium.addSelection("id=wound_acquired_alphas", "label=Incision 1");
                selenium.click("css=#wound_acquired_alphas > option[value=\"Incision 1\"]");
                selenium.addSelection("id=wound_acquired_alphas", "label=Tube/Drain 1");
                selenium.click("css=#wound_acquired_alphas > option[value=\"Tube/Drain 1\"]");
	
		selenium.select("id=wound_acquired_list", "label=Internal");
		selenium.click("id=addwound_acquired");
		selenium.click("id=navSummary");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Wound Location: Chest"));
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
