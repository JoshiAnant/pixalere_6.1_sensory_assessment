package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testDrainAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestDrainAssessment() throws Exception {
		selenium.setTimeout("300000");
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Chest");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=cancel1");
		selenium.click("id=bgbtntd1");
		selenium.waitForPageToLoad("30000");

		selenium.select("id=pain", "label=3");
		selenium.type("id=pain_comments", "comments");

		selenium.select("id=type_of_drain", "label=Jackson Pratt drain");

		selenium.type("id=type_of_drain_other", "test");
		selenium.select("id=tubes_changed_date_day", "label=1");
		selenium.select("id=tubes_changed_date_month", "label=Jan");
		selenium.type("id=tubes_changed_date_year", "1999");
		selenium.click("id=sutured3");
                selenium.click("id=drain_characteristics_panel_uiicon");
		selenium.type("id=drainage_amount_mls1", "1");
		selenium.type("name=drainage_amount_hrs1", "2");

		selenium.select("drainage_amount_start1", "1:00");

		selenium.select("drainage_amount_end1", "1:00");
		selenium.click("id=drain_characteristics11");
		selenium.click("id=drain_site2");
		selenium.click("id=peri_drain_area1");
		selenium.select("id=drainage_num", "label=1");
		selenium.click("id=oku");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=additional_days_date2_day", "label=1");
		selenium.select("id=additional_days_date2_month", "label=Jan");
		selenium.type("id=additional_days_date2_year", "2000");
		selenium.type("id=drainage_amount_mls2", "2");
		selenium.type("name=drainage_amount_hrs2", "1");

		selenium.select("id=drainage_amount_start2", "label=1:00");
		selenium.click("css=#drainage_amount_start2 > option[value=\"1\"]");

		selenium.select("id=drainage_amount_end2", "label=1:00");
		selenium.click("css=#drainage_amount_end2 > option[value=\"1\"]");
		selenium.click("id=drain_characteristics21");
		selenium.type("id=pain_comments", "comments");
		selenium.click("id=navTreatment");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("id=go_confirm")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("id=go_confirm");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=products", "label=Acticoat 10cm x 10cm");
		selenium.select("id=products", "label=Acticoat 10cm x 20cm");
		selenium.select("id=products", "label=Actisorb Silver 200 10.5cm x 10.5cm");
		selenium.click("id=addAlpha");
		selenium.select("id=dressing_change_frequency_alphas", "label=Tube/Drain 1");
                
		selenium.select("id=dressing_change_frequency_list", "label=TID");

		selenium.click("id=adddressing_change_frequency");
                 //java.io.File scrFile5 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                //org.apache.commons.io.FileUtils.copyFile(scrFile5, new java.io.File("c:\\test3.jpg"));
		selenium.click("id=cs_done1");
                selenium.select("id=csresult_alphas", "label=Tube/Drain 1");
		selenium.click("id=csresult_list");
                
		selenium.click("id=review_done");
		selenium.select("id=csresult_list", "label=MRSA");
		selenium.click("css=option[value=\"163\"]");
		selenium.click("id=addcsresult");
                selenium.click("link=Negative Pressure Wound Therapy (NPWT)");
		selenium.select("id=vacstart1_day", "label=1");
		selenium.select("id=vacstart1_month", "label=Jan");
		selenium.type("id=vacstart1_year", "1999");
		selenium.type("id=vacend1_year", "2000");
		selenium.select("id=vacend1_month", "label=Jan");
		selenium.select("id=vacend1_day", "label=1");
		selenium.select("id=pressure_reading1", "label=50 mm/hg");
		selenium.click("id=therapy_setting1_1");
		
		selenium.click("name=npwt_alphas1");
		selenium.select("name=initiated_by1", "label=Acute");
		selenium.select("name=goal_of_therapy1", "label=Wound Closure");
		selenium.select("name=goal_of_therapy1", "label=Prep for STSG");
		selenium.select("name=reason_for_ending1", "label=Wound Closure");
		selenium.type("name=serial_num1", "11111");
		selenium.type("name=kci_num1", "22222");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("name=referral_drain");
		selenium.type("name=body_referral_drain", "comments");
		selenium.select("id=priority_drain", "label=Urgent (24 - 48hrs)");
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
		selenium.click("css=#logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
