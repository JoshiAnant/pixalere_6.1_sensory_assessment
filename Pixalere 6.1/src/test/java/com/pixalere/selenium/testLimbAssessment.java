package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testLimbAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestLimbAssessment() throws Exception {
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("60000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Limb Assessment");
                
		selenium.waitForPageToLoad("30000");
		selenium.click("id=left_range_motion_ankle_1");
		selenium.click("id=right_range_motion_ankle_1");
		selenium.click("id=left_range_motion_knee_1");
		selenium.click("id=right_range_motion_knee_1");
		selenium.click("id=left_range_motion_toes_1");
		selenium.click("id=right_range_motion_toes_1");
		selenium.click("id=left_missing_limbs1");
		selenium.click("id=right_missing_limbs1");
		selenium.click("id=left_skin_assessment1");
		selenium.click("id=right_skin_assessment1");
		selenium.click("id=left_skin_colour_leg_1");
		selenium.click("id=right_skin_colour_leg_1");
		selenium.click("id=right_skin_colour_foot_1");
		selenium.click("id=left_skin_colour_foot_1");
		selenium.click("id=left_edema_severity_1");
		selenium.click("id=right_edema_severity_1");
		selenium.click("id=left_edema_location_1");
		selenium.click("id=right_edema_location_1");
		selenium.type("id=left_ankle_cm", "1");
		selenium.type("id=left_ankle_mm", "2");
		selenium.type("id=right_ankle_cm", "1");
		selenium.type("id=right_ankle_mm", "2");
		selenium.type("id=right_midcalf_mm", "2");
		selenium.type("id=right_midcalf_cm", "1");
		selenium.type("id=left_midcalf_mm", "2");
		selenium.type("id=left_midcalf_cm", "1");
		selenium.click("id=left_pain_assessment1");
		selenium.click("id=right_pain_assessment1");
		selenium.click("id=left_skin_colour_toes_1");
		selenium.click("id=right_skin_colour_toes_1");
		selenium.click("id=left_temperature_leg_1");
		selenium.click("id=right_temperature_leg_1");
		selenium.click("id=right_temperature_foot_1");
		selenium.click("id=left_temperature_foot_1");
		selenium.type("id=sleep_positions", "side");
		selenium.click("id=left_temperature_toes_1");
		selenium.click("id=right_temperature_toes_1");
		selenium.click("id=right_sensory1");
		selenium.click("id=left_sensory1");
		selenium.select("id=left_dorsalis_pedis_palpation", "label=Present");
		selenium.select("id=right_dorsalis_pedis_palpation", "label=Present");
		selenium.select("id=left_posterior_tibial_palpation", "label=Present");
		selenium.select("id=right_posterior_tibial_palpation", "label=Present");
		selenium.click("name=left_capillary_less");
		selenium.click("name=right_capillary_less");
		selenium.click("link=Advanced Lower Limb Assessment");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=left_dorsalis_pedis_doppler1");
		selenium.click("id=right_dorsalis_pedis_doppler1");
		selenium.click("id=left_posterior_tibial_doppler1");
		selenium.click("id=right_posterior_tibial_doppler1");
		selenium.click("id=left_limb_shape1");
		selenium.click("id=right_limb_shape1");
		selenium.click("id=left_foot_toes1");
		selenium.click("id=right_foot_toes1");
		selenium.click("css=#navSummary > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Left Missing Limb or Toes: No amputations"));
		assertTrue(selenium.isTextPresent("Right Missing Limb or Toes: No amputations"));
		assertTrue(selenium.isTextPresent("Left Limb Pain: Ache"));
		assertTrue(selenium.isTextPresent("Right Limb Pain: Ache"));
		assertTrue(selenium.isTextPresent("Left Skin Appearance: Dry/flaky"));
		assertTrue(selenium.isTextPresent("Right Skin Appearance: Dry/flaky"));
		assertTrue(selenium.isTextPresent("Right Cap Refill less than/equal to 3 secs: Yes"));
		assertTrue(selenium.isTextPresent("Left Cap Refill less than/equal to 3 secs: Yes"));
		assertTrue(selenium.isTextPresent("Right Skin Colour Toes: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Left Skin Colour Toes: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Right Skin Colour Foot: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Left Skin Colour Foot: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Right Skin Colour Lower Leg: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Left Skin Colour Lower Leg: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Right Calf Measurement (cm): 1.2"));
		assertTrue(selenium.isTextPresent("Left Calf Measurement (cm): 1.2"));
		assertTrue(selenium.isTextPresent("Right Ankle Measurement (cm): 1.2"));
		assertTrue(selenium.isTextPresent("Left Ankle Measurement (cm): 1.2"));
		assertTrue(selenium.isTextPresent("Sleep Position: side"));
		assertTrue(selenium.isTextPresent("Right Edema Location: Foot"));
		assertTrue(selenium.isTextPresent("Left Edema Location: Foot"));
		assertTrue(selenium.isTextPresent("Right Edema Severity: Non-pitting"));
		assertTrue(selenium.isTextPresent("Left Edema Severity: Non-pitting"));
		assertTrue(selenium.isTextPresent("Right Posterior Tibial Palpation: Present"));
		assertTrue(selenium.isTextPresent("Left Dorsalis Pedis Palpation: Present"));
		assertTrue(selenium.isTextPresent("Right Dorsalis Pedis Palpation: Present"));
		assertTrue(selenium.isTextPresent("Left Posterior Tibial Palpation: Present"));
 
		assertTrue(selenium.isTextPresent("Left Range of Motion: Knee: Normal"));
		assertTrue(selenium.isTextPresent("Right Range of Motion: Knee: Normal"));
		assertTrue(selenium.isTextPresent("Left Range of Motion: Great Toe: Normal"));
		assertTrue(selenium.isTextPresent("Right Range of Motion: Great Toe: Normal"));
		assertTrue(selenium.isTextPresent("Left Range of Motion: Ankle: Normal"));
		assertTrue(selenium.isTextPresent("Right Range of Motion: Ankle: Normal"));
                
		//com.pixalere.ConstantsTest.click("saveEntries",driver);
                selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
        }

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
