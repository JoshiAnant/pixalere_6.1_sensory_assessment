package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testEditWoundProfile   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}
	@Test
	public void testTestEditWoundProfile() throws Exception {
		selenium.setSpeed("750");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.select("css=select[name=\"wound_profile_type_id\"]", "label=Chest - Incision");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Wound Flowchart");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#editProfile1-102 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=reason_for_care", "history");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-100 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=etiology_alphas", "label=Incision 1");

		selenium.select("id=etiology_list", "label=Gastrointestinal");

		selenium.click("id=addetiology");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-103 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=goals_alphas", "label=Incision 1");

		selenium.select("id=goals_list", "label=To heal (Primary Intention)");
	
		selenium.click("id=addgoals");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-440 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-441 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=date_surgery_day", "label=1");
		selenium.select("id=date_surgery_month", "label=Jan");
		selenium.type("id=date_surgery_year", "1999");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-442 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=surgeon", "dr burns");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-462 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=type_of_ostomy1");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-443 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("name=marking_prior_surgery");
		selenium.click("id=submitbutton");
	
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-445 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=teaching_goals", "label=Patient independent with ostomy care");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		assertTrue(selenium.isTextPresent("history ..."));
		assertTrue(selenium.isTextPresent("Incision 1: Gastrointestinal,Tube/Drain 1: Obstruction Incision 1: Obstruction, Tube/Drain 1: Obstruction"));
		assertTrue(selenium.isTextPresent("Incision 1: To heal (Primary Intention),Tube/Drain 1: Temporary tube/drain Incision 1: Temporary tube/drain, Tube/Drain 1: Temporary tube/drain"));
		assertTrue(selenium.isTextPresent("01/Jan/1999 ..."));
		assertTrue(selenium.isTextPresent("dr burns ..."));
		assertTrue(selenium.isTextPresent("Jejunostomy ..."));
		assertTrue(selenium.isTextPresent("Yes ..."));
		assertTrue(selenium.isTextPresent("Patient independent with ostomy care ..."));
		selenium.click("id=logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
