package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testPasswordChange   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestPasswordChange() throws Exception {
		selenium.open("/pixalere/LoginSetup.do");
		selenium.type("id=userId", "9");
		selenium.type("id=password", "skywalker77");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=password", "skywalker77");
		selenium.type("id=newpassword", "fat.burning2@");
		selenium.type("id=confirmpassword", "fat.burning2@");
                selenium.type("id=phone", "6047980440");
		selenium.click("name=terms");
		selenium.click("name=submit2");
		//selenium.type("id=password", "fat.burning2@");
		//selenium.click("name=submit2");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your password has been changed. Please login using your new password."));
		
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
