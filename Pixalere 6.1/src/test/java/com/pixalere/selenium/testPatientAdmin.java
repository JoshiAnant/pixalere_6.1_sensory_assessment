package com.pixalere.selenium;

import java.sql.*;
import com.pixalere.common.ConnectionLocator;
import com.pixalere.common.ConnectionLocatorException;
import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testPatientAdmin extends SeleneseTestBase {

    private DefaultSelenium selenium;
    private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

    @Test
    public void testTestPatientAdmin() throws Exception {
        selenium.open("/pixalere/LoginSetup.do");
                driver.manage().window().maximize();
        selenium.type("id=userId", "7");
        selenium.type("id=password", "fat.burning2@");
        selenium.click("name=submit");
        selenium.waitForPageToLoad("30000");
   
        selenium.click("id=btnclose");
        selenium.click("css=#navAdmin > img.toolbarSpacer");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "adsfsdf");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastName", "Morris");
        selenium.type("id=firstName", "Travis");
        selenium.select("id=gender", "label=Male");
        selenium.select("id=dob_day", "label=9");
        selenium.select("id=dob_month", "label=Mar");
        selenium.type("id=dob_year", "2012");
        selenium.type("id=allergies", "bees");
        selenium.click("id=oop2");
        selenium.type("id=phn11","1111");
        selenium.type("id=version_code", "AB");
        selenium.select("id=treatmentLocation", "label=Sample Location");
        selenium.click("id=addSubmit");
        assertTrue(selenium.isTextPresent("Funding Source is required"));
        selenium.select("id=fundingSource", "label=Hospital/CHA/Facility");
        
        // com.pixalere.ConstantsTest.click("addSubmit",driver);
        selenium.click("id=addSubmit");
        selenium.waitForPageToLoad("30000");
        
        assertTrue(selenium.isTextPresent("Travis Morris"));
        assertTrue(selenium.isTextPresent("Sample Location"));
        assertTrue(selenium.isTextPresent("Admitted"));
        assertTrue(selenium.isTextPresent("09/Mar/2012"));
        selenium.click("link=Edit");
        selenium.select("id=editdob_day", "label=1");
        selenium.click("id=edit_submit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("01/Mar/2012"));
        selenium.click("link=Discharge");
        selenium.select("name=inactivate_reason", "label=Permanent tube/drain");
        selenium.click("css=option[value=\"1603\"]");
        selenium.click("id=inactivate_submit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Discharged"));
        selenium.click("link=Admit");
        selenium.select("name=treatmentRegion", "label=Sample Area");
        selenium.select("document.forms['activate_form'].elements['treatmentLocation']", "label=Sample Location");
        
        selenium.click("id=activate_submit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Admitted"));
        selenium.click("css=#logout");
        selenium.waitForPageToLoad("30000");
        
        //empty dummy interface patient (no patient_id)
        /*try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");

        } catch (java.lang.ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        //UPDATE patient_accounts set secondary_phn =  REPLACE(secondary_phn,'AR','MO');

        Connection con;

        con = DriverManager.getConnection("jdbc:mysql://localhost/test_live", "root", "774977");

        Statement stmt = con.createStatement();
        stmt.executeUpdate("insert into patient_accounts (name,lastname,middle_name,phn2, current_flag,account_status) values ('paris','parislast','p','phn1',1,1)");
        stmt.executeUpdate("insert into patient_accounts (name,lastname,middle_name,phn2,current_flag,account_status) values ('paris2','parislast2','p2','phn2',1,1)");
        stmt.executeUpdate("insert into patient_accounts (name,lastname,middle_name,phn2,current_flag,account_status) values ('paris3','parislast3','p3','phn3',1,1)");
        */
        selenium.open("/pixalere/LoginSetup.do");
        selenium.type("id=userId", "7");
        selenium.type("id=password", "fat.burning2@");
        selenium.click("name=submit");
        
        selenium.waitForPageToLoad("30000");
        
        selenium.click("id=btnclose");
        selenium.click("css=#navAdmin2 > img.toolbarSpacer");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "parislast1");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
    }

   
    @Test
    public void testHidePatientInSearch() throws Exception{
        //test hide patient search
        
    }
    @After
    public void tearDown() throws Exception {
        selenium.stop();
    }
}
