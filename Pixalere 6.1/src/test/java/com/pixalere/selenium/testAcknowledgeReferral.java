package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testAcknowledgeReferral   extends SeleneseTestBase{
        private DefaultSelenium selenium;
        private String browser = System.getProperty("selenium.browser");
	private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestAcknowledgeReferral() throws Exception {
		selenium.open("/pixalere/LoginSetup.do");
		selenium.type("id=userId", "9");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		
		assertEquals("Sample Location", selenium.getTable("id=table_overview.1.0"));
		assertEquals("#1/Morris, Travis", selenium.getTable("id=table_overview.1.1"));
		selenium.click("id=referral1");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=ack6");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=ref_popup");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=referral2");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=ack8");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
