package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;

public class testReporting  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    @Before
    public void setUp() throws Exception {
        selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
        selenium.start();
    }

	@Test
	public void testTestReporting() throws Exception {
		selenium.open("/pixalere/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
                
		selenium.click("id=btnclose");
		selenium.click("css=#navAdmin2 > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAdmin2 > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=overviewTab");
		selenium.select("name=region", "label=Sample Location");
		selenium.click("id=addTreatoverview_form");
		selenium.click("xpath=(//input[@name='patient_status'])[3]");
		selenium.click("xpath=(//input[@name='caretype_status'])[3]");
		selenium.click("id=caretypes_all");
		selenium.click("id=overview_submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=patient_1");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Home");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAdmin2 > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=lastNameSearch", "Potter");
		selenium.click("id=search_admin");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=searchTab");
		selenium.type("id=lastNameSearch", "Morris");
		selenium.click("id=search_admin");
		selenium.waitForPageToLoad("30000");
		selenium.select("name=pixActionMenu", "label=Professional Accounts");
		selenium.click("id=adminMenu");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Edit Professional Account");
		selenium.type("name=id", "7");
		selenium.click("name=searchAdmin");
		selenium.waitForPageToLoad("30000");
		selenium.select("name=pixActionMenu", "label=Patient Accounts");
		selenium.click("css=option[value=\"patient_accounts\"]");
		selenium.click("id=adminMenu");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=lastNameSearch", "Test");
		selenium.click("id=search_admin");
		selenium.waitForPageToLoad("30000");
		selenium.select("name=pixActionMenu", "label=Professional Accounts");
		selenium.click("id=adminMenu");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Edit Professional Account");
		selenium.type("name=id", "7");
		selenium.click("name=searchAdmin");
		selenium.waitForPageToLoad("30000");
		selenium.click("xpath=(//input[@id='profLog7'])[2]");
		selenium.waitForPageToLoad("30000");
		selenium.select("name=pixActionMenu", "label=Patient Accounts");
		selenium.click("id=adminMenu");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=lastNameSearch", "Test");
		selenium.click("id=search_admin");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=firstName", "Test");
		selenium.type("id=middle_name", "Test");
		selenium.type("id=lastName", "Test");
		selenium.select("id=gender", "label=Male");
		selenium.select("id=fundingSource", "label=Hospital/CHA/Facility");
		selenium.click("css=img.ui-datepicker-trigger");
		selenium.click("link=1");
		
                
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
