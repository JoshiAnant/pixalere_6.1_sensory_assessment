package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testEditExam  extends SeleneseTestBase {
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
   private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestEditExam() throws Exception {
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Exam Flowchart");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=editProfile1-534");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
                selenium.type("name=weight", "3.0");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
                /*selenium.click("id=editProfile1-533");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=height", "170.4");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		
                
		selenium.click("id=editProfile1-535");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=temperature", "103.0");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-536");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=pulse", "57");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-537");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=resp_rate", "655");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-545");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=blood_pressure", "66/65");
		selenium.click("id=submitbutton");
		selenium.selectWindow("null");
		assertEquals("170.4 170.0", selenium.getTable("css=td.column-layer_lightb > table.0.1"));
		assertEquals("3.0 2.0", selenium.getTable("css=td.column-layer_darkb > table.0.1"));
		assertEquals("103.0 100.0", selenium.getTable("//table[@id='examflowchart']/tbody/tr[4]/td[2]/table.0.1"));
		assertEquals("57 54", selenium.getTable("//table[@id='examflowchart']/tbody/tr[5]/td[2]/table.0.1"));
		assertEquals("655 65", selenium.getTable("//table[@id='examflowchart']/tbody/tr[6]/td[2]/table.0.1"));
		assertEquals("66/65 65", selenium.getTable("//table[@id='examflowchart']/tbody/tr[7]/td[2]/table.0.1"));
		selenium.click("id=logout");
		selenium.waitForPageToLoad("30000");*/
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
