package com.pixalere.selenium;
import org.openqa.selenium.WebElement;
import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;

public class testSkinProfile  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestSkinProfile() throws Exception {
		selenium.open("/pixalere/logout.do");
                driver.manage().window().maximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=New Wound Profile");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=anterior", "label=Pelvis");
		selenium.click("css=option[value=\"5\"]");
                selenium.click("link=Skin");
                
       
		selenium.dragAndDropToObject("id=dragSKIN", "id=blueman_grid");
		//selenium.dragAndDropToObject("id=dragSKIN", "id=blueman_grid");
             
		selenium.select("id=etiology_alphas", "label=Skin 1");
	
		selenium.select("id=etiology_list", "label=Blisters");
	
		selenium.click("id=addetiology");
		selenium.select("id=goals_alphas", "label=Skin 1");

	
		selenium.select("id=goals_list", "label=Heal Skin");

		selenium.click("id=addgoals");
		selenium.type("name=reason_for_care", "Herpes");
		selenium.click("css=#navAssessment > img.toolbarSpacer");

		selenium.click("css=label.error");
		assertTrue(selenium.isTextPresent("Acquired is required"));
		selenium.select("id=wound_acquired_alphas", "label=Skin 1");

		selenium.select("id=wound_acquired_list", "label=Internal");
	
		selenium.click("id=addwound_acquired");
		selenium.click("link=Skin");
		selenium.click("css=#navSummary > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
                //java.io.File scrFile3 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                //org.apache.commons.io.FileUtils.copyFile(scrFile3, new java.io.File("c:\\test3333.jpg"));
		assertTrue(selenium.isTextPresent("Wound Location: Pelvis (Anterior)"));
		assertTrue(selenium.isTextPresent("Acquired: Skin 1: Internal"));
		assertTrue(selenium.isTextPresent("Etiology: Skin 1: Blisters"));
		assertTrue(selenium.isTextPresent("Goal of Care: Skin 1: Heal Skin"));
		assertTrue(selenium.isTextPresent("Cause/History: Herpes"));
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
