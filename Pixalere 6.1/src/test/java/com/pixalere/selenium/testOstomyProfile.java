/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class testOstomyProfile  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 //selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		selenium.start();
	}
	@Test
	public void testTestWoundProfile() throws Exception {
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
                selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=New Wound Profile");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=anterior", "label=Abdomen");
		selenium.click("id=ui-id-7");
                //WebElement element2 = driver.findElement(By.id("dragU"));
                //WebElement target2 = driver.findElement(By.id("blueman_grid"));
                //(new Actions(driver)).dragAndDrop(element2, target2).perform();
		selenium.dragAndDropToObject("id=dragU", "id=blueman_grid");
                
		selenium.select("id=etiology_alphas", "label=Urostomy");
		selenium.select("id=etiology_list", "label=Ulcerative Colitis");
		selenium.click("id=addetiology");
		selenium.select("id=goals_alphas", "label=Urostomy");
		selenium.select("id=goals_list", "label=Temporary ostomy");
		selenium.click("id=addgoals");
		selenium.select("id=wound_acquired_alphas", "label=Urostomy");
		selenium.select("id=wound_acquired_list", "label=Internal");
		selenium.click("id=addwound_acquired");
		selenium.click("id=navSummary");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Wound Location: Abdomen"));
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		selenium.click("xpath=(//button[@type='button'])[3]");
        	
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
