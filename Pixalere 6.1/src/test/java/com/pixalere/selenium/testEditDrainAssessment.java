package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testEditDrainAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestEditDrainAssessment() throws Exception {
		selenium.setSpeed("550");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.select("css=select[name=\"wound_profile_type_id\"]", "label=Chest - Tube/Drain");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Assessment Flowchart");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#editProfile1-196 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=type_of_drain", "label=Hemovac drain");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-200 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=type_of_drain_other", "test2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-197 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("name=sutured");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-215 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=drain_site1");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-505 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=pain", "label=2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-506 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=pain_comments", "comments2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-447 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=tubes_changed_date_month", "label=Mar");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-198 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=peri_drain_area1");
		selenium.click("id=peri_drain_area2");
		selenium.click("id=peri_drain_area1");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-202 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=current_products", "label=1:Tube/Drain 1:Actisorb Silver 200 10.5cm x 10.5cm");
		selenium.click("id=removeAlpha");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-208 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-208 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-509 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("xpath=(//input[@name='cs_done'])[2]");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-419 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=csresult1");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-478 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("id=cns_name", "test");
		selenium.select("id=cns_start_day", "label=3");
		selenium.select("id=cns_start_month", "label=Jan");
		selenium.type("id=cns_start_year", "1999");
		selenium.type("id=cns_end_year", "2000");
		selenium.select("id=cns_end_day", "label=4");
		selenium.select("id=cns_end_month", "label=Feb");
		selenium.click("id=addAnti");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		assertEquals("test : 03/Jan/1999 to 04/Feb/2000 ...", selenium.getTable("//table[@id='aflowchart']/tbody/tr[27]/td[2]/table.0.1"));
		assertEquals("", selenium.getTable("//table[@id='aflowchart']/tbody/tr[26]/td[2]/table.0.1"));
		assertEquals("No Yes", selenium.getTable("//table[@id='aflowchart']/tbody/tr[25]/td[2]/table.0.1"));
		assertEquals("1 Actisorb Silver 200 10.5cm x 10.5cm", selenium.getTable("//table[@id='aflowchart']/tbody/tr[23]/td[2]/table.0.1"));
		assertEquals("Intact, Erythema < 2cm Intact", selenium.getTable("//table[@id='aflowchart']/tbody/tr[21]/td[2]/table.0.1"));
		assertEquals("01/Mar/1999 01/Jan/1999", selenium.getTable("//table[@id='aflowchart']/tbody/tr[14]/td[2]/table.0.1"));
		assertEquals("2 3", selenium.getTable("//table[@id='aflowchart']/tbody/tr[10]/td[2]/table.0.1"));
		assertEquals("Intact, Eroded Eroded", selenium.getTable("//table[@id='aflowchart']/tbody/tr[22]/td[2]/table.0.1"));
		assertEquals("Yes N/A", selenium.getTable("//table[@id='aflowchart']/tbody/tr[15]/td[2]/table.0.1"));
		assertEquals("test2 test", selenium.getTable("//table[@id='aflowchart']/tbody/tr[13]/td[2]/table.0.1"));
		assertEquals("Hemovac drain Jackson Pratt drain", selenium.getTable("//table[@id='aflowchart']/tbody/tr[12]/td[2]/table.0.1"));
		selenium.click("css=#logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
