package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testOstomyAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestOstomyAssessment() throws Exception {
		selenium.setTimeout("300000");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
                //ensure the last assessment report isn't loading training patients.
                assertFalse(selenium.isTextPresent("Warning there are patients with overdue assessments, please review this list and clean-up or schedule accordingly."));
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Abdomen");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=construction_1");
		selenium.click("id=shape_1");
		selenium.click("id=drainage1");
		selenium.click("id=perifistula_skin1");
		selenium.click("id=profile1");
		selenium.select("id=circumference_mm", "label=1");
		selenium.click("css=#circumference_mm > option[value=\"1\"]");
		selenium.click("id=length_mm");
		selenium.click("id=appearance1");
		selenium.click("id=urine_colour1");
		selenium.click("id=urine_quantity_1");
		selenium.click("id=urine_type2");
		selenium.type("id=drainage_comments", "test");
		selenium.click("id=nutritional_status_1");
		selenium.click("id=contour2");
		selenium.click("id=devices_2");
         
                selenium.click("id=pouching_panel_uiicon");
		selenium.click("id=pouching1");
		selenium.type("id=nutritional_status_other", "stat");
		selenium.click("id=flange_pouch_4");
		selenium.type("id=flange_pouch_comments", "puch");
		selenium.click("id=mucocutaneous_margin1");
		selenium.click("id=self_care_progress2");
		selenium.click("id=periostomy_skin2");
		selenium.type("id=self_care_progress_comments", "test");
		selenium.click("id=navTreatment");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=products", "label=Acticoat 10cm x 10cm");
		selenium.select("id=products", "label=Acticoat 10cm x 20cm");
		selenium.select("id=products", "label=Actisorb Silver 200 10.5cm x 10.5cm");
		selenium.click("id=addAlpha");
		selenium.select("dressing_change_frequency_alphas", "Urostomy");
		selenium.select("dressing_change_frequency_list", "TID");
		selenium.click("id=adddressing_change_frequency");
		selenium.click("id=cs_done1");
		selenium.click("id=review_done");
		selenium.select("csresult_alphas", "Urostomy");

		selenium.select("csresult_list", "MRSA");
		selenium.click("id=addcsresult");
                selenium.click("link=Negative Pressure Wound Therapy (NPWT)");
		selenium.select("id=vacstart1_day", "label=1");
		selenium.select("id=vacstart1_month", "label=Jan");
		selenium.type("id=vacstart1_year", "1999");
		selenium.type("id=vacend1_year", "2000");
		selenium.select("id=vacend1_day", "label=1");
		selenium.select("id=pressure_reading1", "label=50 mm/hg");
		selenium.click("id=therapy_setting1_1");
		selenium.click("name=npwt_alphas1");
		selenium.select("name=initiated_by1", "label=Acute");
		selenium.select("name=goal_of_therapy1", "label=Wound Closure");
		selenium.select("name=goal_of_therapy1", "label=Prep for STSG");
		selenium.select("name=reason_for_ending1", "label=Wound Closure");
		selenium.type("name=serial_num1", "11111");
		selenium.type("name=kci_num1", "22222");
		selenium.select("id=vacend1_month", "label=Jan");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("name=referral_ostomy");
		selenium.type("name=body_referral_ostomy", "comments");
		selenium.select("id=priority_ostomy", "label=Urgent (24 - 48hrs)");
                
                selenium.click("css=#ui-accordion-accordion-header-1 > label.title");
		assertTrue(selenium.isTextPresent("Wound Status: Open"));
		assertTrue(selenium.isTextPresent("Post-Op Day:"));
		assertTrue(selenium.isTextPresent("Clinical Pathway: N/A"));
		assertTrue(selenium.isTextPresent("Construction: Loop"));
		assertTrue(selenium.isTextPresent("Stoma Shape: Round"));
		assertTrue(selenium.isTextPresent("Circumference (mm): 1"));
		assertTrue(selenium.isTextPresent("Length: 0"));
		assertTrue(selenium.isTextPresent("Width: 0"));
		assertTrue(selenium.isTextPresent("Profile: Os centered"));
		assertTrue(selenium.isTextPresent("Appearance: Red"));
		assertTrue(selenium.isTextPresent("Devices: Rod"));
		assertTrue(selenium.isTextPresent("Abdominal Contour: Pendulous"));
		assertTrue(selenium.isTextPresent("Concerns for Pouching: N/A"));
		assertTrue(selenium.isTextPresent("Mucocutaneous Margin: Dissolvable sutures"));
		assertTrue(selenium.isTextPresent("Peri-Ostomy Skin: Weepy"));
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
		selenium.click("css=#logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
