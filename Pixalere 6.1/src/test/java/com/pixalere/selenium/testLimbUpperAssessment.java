package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testLimbUpperAssessment  extends SeleneseTestBase {
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestLimbUpperAssessment() throws Exception {
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Limb Assessment");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Upper Limb Assessment");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=left_missing_limbs_upper1");
		selenium.click("id=right_missing_limbs_upper1");
		selenium.click("id=left_skin_assessment_upper1");
		selenium.click("id=right_skin_assessment_upper1");
                
		selenium.click("id=left_range_motion_arm_1");
		selenium.click("id=right_range_motion_arm_1");
		selenium.click("id=left_range_motion_fingers_1");
		selenium.click("id=right_range_motion_fingers_1");
		selenium.click("id=left_range_motion_hand_1");
		selenium.click("id=right_range_motion_hand_1");
		selenium.click("id=left_edema_severity_upper_2");
		selenium.click("id=right_edema_severity_upper_2");
		selenium.click("id=left_edema_location_upper_2");
		selenium.click("id=right_edema_location_upper_2");
		selenium.type("id=left_wrist_cm", "1");
		selenium.type("left_wrist_mm", "2");
		selenium.type("right_wrist_cm", "1");
		selenium.type("right_wrist_mm", "2");
		selenium.type("id=left_elbow_cm", "1");
		selenium.type("left_elbow_mm", "2");
		selenium.type("right_elbow_cm", "1");
		selenium.type("right_elbow_mm", "2");
		selenium.click("id=left_skin_colour_arm_1");
		selenium.click("id=right_skin_colour_arm_1");
		selenium.click("id=right_skin_colour_hand_1");
		selenium.click("id=left_skin_colour_hand_1");
		selenium.click("id=left_skin_colour_fingers_1");
		selenium.click("id=right_skin_colour_fingers_1");
		selenium.click("id=left_pain_assessment_upper1");
		selenium.click("id=right_pain_assessment_upper1");
		selenium.click("id=left_temperature_arm_1");
		selenium.click("id=left_temperature_hand_1");
		selenium.click("id=right_temperature_hand_1");
		selenium.click("id=right_temperature_arm_1");
		selenium.click("id=left_temperature_fingers_1");
		selenium.click("id=right_temperature_fingers_1");
		selenium.click("id=right_sensory_upper1");
		selenium.click("id=left_sensory_upper1");		
                selenium.click("name=right_capillary_less_upper");
		selenium.click("name=left_capillary_less_upper");
		selenium.select("id=left_dorsalis_pedis_palpation_upper", "label=Present");
		selenium.select("name=right_dorsalis_pedis_palpation_upper", "label=Present");
		selenium.select("id=left_posterior_tibial_palpation_upper", "label=Present");
		selenium.select("name=right_posterior_tibial_palpation_upper", "label=Present");
		selenium.click("css=#navSummary > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Upper Limb Assessment"));
		assertTrue(selenium.isTextPresent("Left Missing Limb or Fingers: No amputations"));
		assertTrue(selenium.isTextPresent("Right Missing Limb or Fingers: No amputations"));
		assertTrue(selenium.isTextPresent("Left Limb Pain: Ache"));
		assertTrue(selenium.isTextPresent("Right Limb Pain: Ache"));
		assertTrue(selenium.isTextPresent("Left Skin Appearance: Dry/flaky"));
		assertTrue(selenium.isTextPresent("Right Skin Appearance: Dry/flaky"));
		assertTrue(selenium.isTextPresent("Left Skin Colour Arm: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Right Skin Colour Arm: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Left Skin Colour Fingers: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Left Skin Colour Hand: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Right Skin Colour Hand: Bluish-purple"));
		assertTrue(selenium.isTextPresent("Right Upper Limb Sensation: None of the above"));
		assertTrue(selenium.isTextPresent("Left Upper Limb Sensation: None of the above"));
		assertTrue(selenium.isTextPresent("Left Forearm Measurement (cm): 1.2"));
		assertTrue(selenium.isTextPresent("Left Wrist Measurement (cm): 1.2"));
		assertTrue(selenium.isTextPresent("Right Edema Location: No Edema"));
		assertTrue(selenium.isTextPresent("Right Edema Severity: Non-pitting"));
		assertTrue(selenium.isTextPresent("Right Radial Palpation: Present"));
		assertTrue(selenium.isTextPresent("Right Brachial Palpation: Present"));
		assertTrue(selenium.isTextPresent("Left Range of Motion: Elbow: Normal"));
		assertTrue(selenium.isTextPresent("Right Range of Motion: Elbow: Normal"));
		assertTrue(selenium.isTextPresent("Left Range of Motion: Thumb: Normal"));
		assertTrue(selenium.isTextPresent("Right Range of Motion: Thumb: Normal"));
		assertTrue(selenium.isTextPresent("Left Range of Motion: Wrist: Normal"));
		assertTrue(selenium.isTextPresent("Right Range of Motion: Wrist: Normal"));
                assertTrue(selenium.isTextPresent("Right Cap Refill less than/equal to 3 secs: Yes"));
                assertTrue(selenium.isTextPresent("Left Cap Refill less than/equal to 3 secs: Yes"));
                assertTrue(selenium.isTextPresent(""));
                
		// com.pixalere.ConstantsTest.click("saveEntries",driver);
                selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
	}
        @Test
        public void testLimbAdvAssessment() throws Exception {
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		//selenium.click("link=Limb Assessment");
		//selenium.waitForPageToLoad("30000");
		//selenium.click("link=Adv. Limb Assessment");
		//selenium.waitForPageToLoad("30000");
                
        }
	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
