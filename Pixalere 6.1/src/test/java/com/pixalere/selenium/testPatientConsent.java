package com.pixalere.selenium;

import com.thoughtworks.selenium.SeleneseTestBase;
import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * 
 */
public class testPatientConsent extends SeleneseTestBase {
    private Selenium selenium;
    private final String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
    WebDriverWait wait;
    
    @Before
    public void setUp() throws Exception {
        if(browser.equals("*chrome")){driver=new FirefoxDriver();}
        else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
        else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
        
        selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
        //selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
        //selenium.start();
        
        wait = new WebDriverWait(driver, 10, 500);
    }
        
        @Test
	public void testEnablePatienConsent() throws Exception {
            selenium.open("/pixalere/LoginSetup.do");
            selenium.type("id=userId", "7");
            selenium.type("id=password", "fat.burning2@");
            selenium.click("name=submit");
            selenium.waitForPageToLoad("30000");
            selenium.click("id=btnclose");
            
            selenium.click("css=#navAdmin > img.toolbarSpacer");
            selenium.waitForPageToLoad("30000");
            selenium.select("id=pixActionMenu", "label=Configuration");
            selenium.click("id=adminMenu");
            selenium.waitForPageToLoad("30000");
            selenium.click("link=Activate/Deactivate Patient Consent");
            selenium.waitForPageToLoad("30000");
            selenium.click("name=value");
            selenium.click("name=button");
            selenium.waitForPageToLoad("30000");
            
            selenium.type("id=search", "1");
            selenium.click("name=OK");
            selenium.waitForPageToLoad("30000");
            assertTrue(selenium.isTextPresent("Warning: This patient has not signed/accepted a Patient Consent"));
            
            selenium.click("css=#navViewer > img.toolbarSpacer");
            selenium.waitForPopUp("large_fix_win", "30000");
            selenium.selectWindow("name=large_fix_win");
            assertTrue(selenium.isTextPresent("Client signature"));
            
            selenium.chooseOkOnNextConfirmation();
            selenium.click("css=button.button.cancel");
            
            //selenium.getConfirmation();
            //assertEquals("Please confirm cancelation of Patient Consent.\n\nClick OK to close this page and return to the Homepage.\nClick Cancel to continue in this page.", selenium.getConfirmation());

            selenium.selectWindow(null);
            selenium.waitForPageToLoad("30000");
            assertTrue(selenium.isTextPresent("Home"));
        }
        
        @Test
	public void testFillPatienConsent() throws Exception {
//            ExpectedCondition<Boolean> reload = new ExpectedCondition<Boolean>() {
//                public Boolean apply(WebDriver driver) {
//                    return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
//                }
//            };
            
            selenium.open("/pixalere/LoginSetup.do");
            selenium.type("id=userId", "7");
            selenium.type("id=password", "fat.burning2@");
            selenium.click("name=submit");
            selenium.waitForPageToLoad("30000");
            selenium.click("id=btnclose");
            
            selenium.type("id=search", "1");
            selenium.click("name=OK");
            selenium.waitForPageToLoad("30000");
            assertTrue(selenium.isTextPresent("Warning: This patient has not signed/accepted a Patient Consent"));
            
            selenium.click("link=click here");
            selenium.waitForPopUp("large_fix_win", "30000");
            selenium.selectWindow("name=large_fix_win");
            selenium.type("id=criteria1", "Services 1\nServices 2\nServices 3");
            selenium.type("id=criteria2", "Procedures 1\nProcedures 2\nProcedures 3");
            selenium.type("name=sharing", "Sharing 1, sharing 2, sharing 3, etc.");
            selenium.click("id=consent_services_chk");
            selenium.click("id=consent_information_chk");
            selenium.click("css=button.button.save");
            
            //wait.until(ExpectedConditions.alertIsPresent());
            //assertEquals("A signature or a type of verbal consent is required.", selenium.getAlert());
            selenium.click("id=consent_verbal_3");
            selenium.type("id=patient_consent_name", "");
            selenium.click("css=button.button.save");
            
            selenium.type("id=patient_consent_name", "Travis Morris");
            selenium.click("css=button.button.save");
            
            selenium.waitForPageToLoad("30000");
            assertTrue(selenium.isTextPresent("Patient Consent saved"));
            
            selenium.selectWindow("null");
            //wait.until(reload);
            //selenium.waitForPageToLoad("30000");
            //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            //wait.until(ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(By.id("consent_warning"))));
            selenium.open("/pixalere/AdminSetup.do");
            assertFalse(selenium.isTextPresent("Warning: This patient has not signed/accepted a Patient Consent"));
        }
        
        @Test
	public void testRevokePatienConsent() throws Exception {          
            selenium.open("/pixalere/LoginSetup.do");
            selenium.type("id=userId", "7");
            selenium.type("id=password", "fat.burning2@");
            selenium.click("name=submit");
            selenium.waitForPageToLoad("30000");
            selenium.click("id=btnclose");
            
            selenium.type("id=search", "1");
            selenium.click("name=OK");
            selenium.waitForPageToLoad("30000");
            
            selenium.click("xpath=(//button[@type='button'])[3]");
            selenium.waitForPageToLoad("30000");
            selenium.click("css=#navViewer > img.toolbarSpacer");
            selenium.waitForPageToLoad("30000");
            
            selenium.click("id=view_consent");
            selenium.waitForPopUp("large_fix_win", "30000");
            selenium.selectWindow("name=large_fix_win");
            selenium.click("css=button.button.ok");
            selenium.waitForPageToLoad("30000");
            selenium.selectWindow("null");
            
            selenium.click("id=view_consent");
            selenium.waitForPopUp("large_fix_win", "30000");
            selenium.selectWindow("name=large_fix_win");
            selenium.click("css=button.button.revoke");
            
            selenium.waitForPageToLoad("30000");
            assertTrue(selenium.isTextPresent("Patient Consent revoked"));
            
            selenium.selectWindow("null");
            selenium.open("/pixalere/AdminSetup.do");
            assertTrue(selenium.isTextPresent("Warning: This patient has not signed/accepted a Patient Consent"));
        }
        
        @Test
	public void testDisablePatienConsent() throws Exception {
            selenium.open("/pixalere/LoginSetup.do");
            selenium.type("id=userId", "7");
            selenium.type("id=password", "fat.burning2@");
            selenium.click("name=submit");
            selenium.waitForPageToLoad("30000");
            selenium.click("id=btnclose");
            
            selenium.click("css=#navAdmin > img.toolbarSpacer");
            selenium.waitForPageToLoad("30000");
            
            selenium.type("id=search", "1");
            selenium.click("name=OK");
            selenium.waitForPageToLoad("30000");
            assertTrue(selenium.isTextPresent("Warning: This patient has not signed/accepted a Patient Consent"));
            
            selenium.select("id=pixActionMenu", "label=Configuration");
            selenium.click("id=adminMenu");
            selenium.waitForPageToLoad("30000");
            selenium.click("link=Activate/Deactivate Patient Consent");
            selenium.waitForPageToLoad("30000");
            selenium.click("name=value");
            selenium.click("name=button");
            selenium.waitForPageToLoad("30000");
            
            selenium.open("/pixalere/AdminSetup.do");
            assertFalse(selenium.isTextPresent("Warning: This patient has not signed/accepted a Patient Consent"));
        }
        
        @After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
