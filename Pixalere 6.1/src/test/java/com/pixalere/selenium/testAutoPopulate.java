
package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testAutoPopulate   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestAutoPopulate() throws Exception {
		selenium.setTimeout("300000");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Head/neck (Anterior)");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=assessment_type1");
		selenium.click("id=populate1");
		selenium.waitForPageToLoad("30000");
                assertEquals("2005", selenium.getValue("id=wound_date_year"));
                assertEquals(selenium.getSelectedLabel("status"),"Open");
                assertTrue(selenium.isChecked("recurrent2"));
                //assertEquals(selenium.getSelectedLabel("pain"),"1");
                assertEquals("3.0", selenium.getValue("id=length"));
               assertEquals("3.0", selenium.getValue("id=width"));
               assertEquals("2.0", selenium.getValue("id=depth"));
               assertEquals(selenium.getSelectedLabel("undermining_start_1"),"3:00");
               assertEquals(selenium.getSelectedLabel("undermining_end_1"),"3:00");
               assertEquals(selenium.getSelectedLabel("underminingdepth_1"),"1");
               assertEquals(selenium.getSelectedLabel("underminingdepth_mini_1"),"2");
               assertEquals(selenium.getSelectedLabel("num_sinus_tracts"),"1");
               assertEquals(selenium.getSelectedLabel("sinustract_start_1"),"2:00");
               assertEquals(selenium.getSelectedLabel("sinusdepth_1"),"6");
               assertEquals(selenium.getSelectedLabel("sinusdepth_mini_1"),"1");
               assertEquals(selenium.getSelectedLabel("num_fistulas"),"1");
                assertEquals(" fistula comment 223 ", selenium.getValue("id=fistulas_1"));
                assertTrue(selenium.isChecked("woundbase2"));
               assertEquals(selenium.getSelectedLabel("woundbase_percentage_2"),"50%");
                assertTrue(selenium.isChecked("exudate_type2"));
                assertTrue(selenium.isChecked("exudate_amount_2"));
                assertTrue(selenium.isChecked("exudate_odour1"));
                assertTrue(selenium.isChecked("wound_edge2"));
                assertTrue(selenium.isChecked("periwound1"));
                assertTrue(selenium.isChecked("periwound2"));
                
                
                
		// tr>
		// 	<td>verifyValue</td>
		// 	<td>id=recurrent2</td>
		// 	<td>on</td>
		// </tr>
		// <tr>
		// 	<td>click</td>
		// 	<td>id=pain</td>
		// 	<td></td>
		// </tr>
		// <tr>
		// 	<td>verifyValue</td>
		// 	<td>name=pain_comments</td>
		// 	<td>pain comments2</td>
		// </tr>
		// <tr>
		// 	<td>verifyText</td>
		// 	<td>//form[@id='wound_assessment_form']/div[3]/p[3]/label[3]</td>
		// 	<td>Serous</td>
		// </tr>
		// <tr>
		// 	<td>verifyText</td>
		// 	<td>//form[@id='wound_assessment_form']/div[3]/p[4]/label[3]</td>
		// 	<td>Scant</td>
		// </tr>
		// <tr>
		// 	<td>verifyText</td>
		// 	<td>//form[@id='wound_assessment_form']/div[3]/p[6]/label[3]</td>
		// 	<td>Demarcated</td>
		// </tr>
		// <tr>
		// 	<td>verifyText</td>
		// 	<td>//form[@id='wound_assessment_form']/div[3]/p[7]/label[2]</td>
		// 	<td>Indurated &lt; 2cm</td>
		// </tr>
		// <tr>
		// 	<td>verifyText</td>
		// 	<td>//form[@id='wound_assessment_form']/div[3]/p[7]/label[3]</td>
		// 	<td>Intact</td>
		// </tr>
		// <tr>
		// 	<td>verifyText</td>
		// 	<td>//form[@id='wound_assessment_form']/div[2]/p[18]/label</td>
		// 	<td>No open wound</td>
		// </tr>
		// <tr>
		// 	<td>verifyText</td>
		// 	<td>//form[@id='wound_assessment_form']/div[2]/p[19]/label</td>
		// 	<td>Fully epithelialized</td>
		// </tr>
		// <tr>
		// 	<td>verifyValue</td>
		// 	<td>name=fistulas_1</td>
		// 	<td>fistula comment 223</td>
		// </tr
	}
        @Test
        public void testLimbAssessment() throws Exception{
                selenium.open("/pixalere/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		//selenium.click("//div[7]/div/button");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		//selenium.click("xpath=(//button[@type='button'])[4]");
		selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=limbLink");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=populate1");
		selenium.waitForPageToLoad("30000");
                assertTrue(selenium.isChecked("right_missing_limbs2"));
                assertTrue(selenium.isChecked("left_missing_limbs2"));
                assertTrue(selenium.isChecked("left_skin_colour_leg_3"));
                assertTrue(selenium.isChecked("right_skin_colour_leg_5"));
                assertTrue(selenium.isChecked("left_skin_colour_foot_5"));
                assertTrue(selenium.isChecked("right_skin_colour_foot_1"));
                assertTrue(selenium.isChecked("left_skin_colour_toes_2"));
                assertTrue(selenium.isChecked("right_skin_colour_toes_5"));
                assertTrue(selenium.isChecked("right_temperature_leg_3"));
                assertTrue(selenium.isChecked("left_temperature_leg_3"));
                assertTrue(selenium.isChecked("right_temperature_foot_2"));
                assertTrue(selenium.isChecked("left_temperature_foot_3"));
                assertTrue(selenium.isChecked("right_temperature_toes_2"));
                assertTrue(selenium.isChecked("left_temperature_toes_3"));
               assertEquals(selenium.getSelectedLabel("right_dorsalis_pedis_palpation"),"Diminished");
               assertEquals(selenium.getSelectedLabel("left_dorsalis_pedis_palpation"),"Diminished");
               assertEquals(selenium.getSelectedLabel("right_dorsalis_pedis_palpation"),"Diminished");
               assertEquals(selenium.getSelectedLabel("left_dorsalis_pedis_palpation"),"Diminished");
                assertTrue(selenium.isChecked("right_capillary_less"));
                assertTrue(selenium.isChecked("right_range_motion_knee_1"));
                assertTrue(selenium.isChecked("left_range_motion_knee_1"));
                assertTrue(selenium.isChecked("right_range_motion_ankle_1"));
                assertTrue(selenium.isChecked("left_range_motion_ankle_1"));
                assertTrue(selenium.isChecked("right_range_motion_toes_1"));
                assertTrue(selenium.isChecked("left_range_motion_toes_1"));
                assertTrue(selenium.isChecked("right_edema_location_3"));
                assertTrue(selenium.isChecked("left_edema_location_3"));
                assertTrue(selenium.isChecked("right_edema_severity_4"));
                assertTrue(selenium.isChecked("left_edema_severity_4"));
                assertEquals(selenium.getValue("id=sleep_positions"),"comments");
                assertEquals("3", selenium.getValue("id=right_ankle_cm"));
                assertEquals("4", selenium.getValue("id=right_ankle_mm"));
                assertEquals("3", selenium.getValue("id=left_ankle_cm"));
                assertEquals("3", selenium.getValue("id=left_ankle_mm"));
                assertEquals("1", selenium.getValue("id=right_midcalf_cm"));
                assertEquals("5", selenium.getValue("id=right_midcalf_mm"));
                assertEquals("4", selenium.getValue("id=left_midcalf_cm"));
                assertEquals("2", selenium.getValue("id=left_midcalf_mm"));
                assertTrue(selenium.isChecked("right_skin_assessment4"));
                assertTrue(selenium.isChecked("left_skin_assessment2"));
                assertTrue(selenium.isChecked("right_sensory2"));
                assertTrue(selenium.isChecked("left_sensory1"));
                assertTrue(selenium.isChecked("left_sensory2"));
                assertTrue(selenium.isChecked("right_pain_assessment4"));
                assertTrue(selenium.isChecked("left_pain_assessment2"));
                
        }
        @Test
       public void testPopulateLimbAdv() throws Exception {
                selenium.open("/pixalere/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		//selenium.click("//div[7]/div/button");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=limbLink");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=advLink");
		selenium.waitForPageToLoad("30000");
                selenium.click("id=populate1");
                selenium.waitForPageToLoad("30000");
                assertTrue(selenium.isChecked("right_dorsalis_pedis_doppler1"));
                assertTrue(selenium.isChecked("left_dorsalis_pedis_doppler1"));
                assertTrue(selenium.isChecked("right_posterior_tibial_doppler1"));
                assertTrue(selenium.isChecked("left_posterior_tibial_doppler1"));
                
        }
        @Test
       public void testPopulateLimbUpper() throws Exception {
                selenium.open("/pixalere/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		//selenium.click("//div[7]/div/button");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=limbLink");
		selenium.waitForPageToLoad("30000");
                selenium.click("id=upperLink");
                selenium.waitForPageToLoad("30000");
                selenium.click("id=populate1");
                selenium.waitForPageToLoad("30000");
                assertTrue(selenium.isChecked("right_missing_limbs_upper1"));
                assertTrue(selenium.isChecked("left_missing_limbs_upper1"));
                assertTrue(selenium.isChecked("right_skin_colour_arm_1"));
                assertTrue(selenium.isChecked("left_skin_colour_arm_1"));
                assertTrue(selenium.isChecked("right_skin_colour_hand_1"));
                assertTrue(selenium.isChecked("left_skin_colour_hand_1"));
                assertTrue(selenium.isChecked("right_skin_colour_fingers_1"));
                assertTrue(selenium.isChecked("left_skin_colour_fingers_1"));
                assertTrue(selenium.isChecked("right_temperature_arm_1"));
                assertTrue(selenium.isChecked("left_temperature_arm_1"));
                assertTrue(selenium.isChecked("right_temperature_hand_1"));
                assertTrue(selenium.isChecked("left_temperature_hand_1"));
                assertTrue(selenium.isChecked("right_temperature_fingers_1"));
                assertTrue(selenium.isChecked("left_temperature_fingers_1"));
               assertEquals(selenium.getSelectedLabel("right_dorsalis_pedis_palpation_upper"),"Present");
               assertEquals(selenium.getSelectedLabel("left_dorsalis_pedis_palpation_upper"),"Present");
               assertEquals(selenium.getSelectedLabel("right_posterior_tibial_palpation_upper"),"Present");
               assertEquals(selenium.getSelectedLabel("left_posterior_tibial_palpation_upper"),"Present");
                assertTrue(selenium.isChecked("right_range_motion_arm_1"));
                assertTrue(selenium.isChecked("left_range_motion_arm_1"));
                assertTrue(selenium.isChecked("right_range_motion_hand_1"));
                assertTrue(selenium.isChecked("left_range_motion_hand_1"));
                assertTrue(selenium.isChecked("right_range_motion_fingers_1"));
                assertTrue(selenium.isChecked("left_range_motion_fingers_1"));
                assertTrue(selenium.isChecked("right_edema_location_upper_1"));
                assertTrue(selenium.isChecked("left_edema_location_upper_1"));
                assertTrue(selenium.isChecked("right_edema_severity_upper_1"));
                assertTrue(selenium.isChecked("left_edema_severity_upper_1"));
                assertEquals("1", selenium.getValue("id=right_wrist_cm"));
                assertEquals("2", selenium.getValue("id=right_wrist_mm"));
                assertEquals("1", selenium.getValue("id=left_wrist_cm"));
                assertEquals("2", selenium.getValue("id=left_wrist_mm"));
                assertEquals("1", selenium.getValue("id=right_elbow_cm"));
                assertEquals("2", selenium.getValue("id=right_elbow_mm"));
                assertEquals("1", selenium.getValue("id=left_elbow_cm"));
                assertEquals("2", selenium.getValue("id=left_elbow_mm"));
                assertTrue(selenium.isChecked("right_skin_assessment_upper1"));
                assertTrue(selenium.isChecked("left_skin_assessment_upper1"));
                assertTrue(selenium.isChecked("right_pain_assessment_upper1"));
                assertTrue(selenium.isChecked("left_pain_assessment_upper1"));
        }
        @Test
        public void testMobilityAssessment() throws Exception {
            selenium.open("/pixalere/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		//selenium.click("//div[7]/div/button");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=footLink");
		selenium.waitForPageToLoad("30000");
                selenium.click("id=populate1");
                selenium.waitForPageToLoad("30000");
               assertEquals(selenium.getSelectedLabel("weight_bearing"),"Partial");
               assertEquals(selenium.getSelectedLabel("balance"),"Unsteady");
               assertEquals(selenium.getSelectedLabel("calf_muscle_pump"),"Impaired");
                assertEquals("comments", selenium.getValue("id=calf_muscle_comments"));
               assertEquals(selenium.getSelectedLabel("mobility_aids"),"Crutches");
                assertEquals("comments", selenium.getValue("id=gait_pattern_comments"));
                assertEquals("comments", selenium.getValue("id=walking_distance_comments"));
                assertEquals("comments", selenium.getValue("id=walking_endurance_comments"));
                assertTrue(selenium.isChecked("indoor_footwear1"));
                assertEquals("comments", selenium.getValue("id=indoor_footwear_comments"));
                assertTrue(selenium.isChecked("outdoor_footwear1"));
                assertEquals("comments", selenium.getValue("id=outdoor_footwear_comments"));
                assertTrue(selenium.isChecked("orthotics1"));
                assertEquals("comments", selenium.getValue("id=orthotics_comments"));
               assertEquals(selenium.getSelectedLabel("muscle_tone_functional"),"Normal");
               assertEquals(selenium.getSelectedLabel("arches_foot_structure"),"Normal");
                assertTrue(selenium.isChecked("pronation_foot_structure2"));
                assertTrue(selenium.isChecked("supination_foot_structure2"));
                assertTrue(selenium.isChecked("dorsiflexion_passive1"));
                assertTrue(selenium.isChecked("dorsiflexion_active1"));
                assertTrue(selenium.isChecked("plantarflexion_passive1"));
                assertTrue(selenium.isChecked("plantarflexion_active1"));
                assertTrue(selenium.isChecked("plantarflexion_passive2"));
                assertTrue(selenium.isChecked("plantarflexion_active2"));
                assertTrue(selenium.isChecked("greattoe_passive2"));
                assertTrue(selenium.isChecked("greattoe_active2"));
                
                
                
        }
        
	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
