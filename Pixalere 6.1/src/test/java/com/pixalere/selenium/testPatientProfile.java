package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testPatientProfile  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestPatientProfile() throws Exception {
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Travis Morris"));
		assertTrue(selenium.isTextPresent("01/Mar/2012"));
		assertTrue(selenium.isTextPresent("Sample Location"));
		assertTrue(selenium.isTextPresent("Admitted"));
		assertTrue(selenium.isTextPresent("Male"));
		assertTrue(selenium.isTextPresent("bees"));
		selenium.click("id=im1");
		selenium.click("id=medication_comments1");
                selenium.select("id=comorbidities", "label=Type I Diabetes Mellitus");
		//selenium.click("css=option[value=\"510\"]");
		
                //com.pixalere.ConstantsTest.click("addComo",driver);
		selenium.click("id=addComo");
                
		selenium.type("name=professionals", "professionals");
		selenium.type("name=surgical_history", "history");
                selenium.click("link=Physical Exam");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=height", "170");
		selenium.type("name=weight", "2");
		selenium.type("name=temperature", "100");
		selenium.type("name=pulse", "54");
		selenium.type("name=resp_rate", "65");
		selenium.type("name=blood_pressure", "65/65");
		selenium.click("link=Braden");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=braden_sensory_1");
		selenium.click("id=braden_moisture_1");
		selenium.click("id=braden_activity_1");
		//selenium.click("//div[@id='bradenDiv']/table/tbody/tr[3]/td[2]/label");
		selenium.click("id=braden_mobility_1");
		selenium.click("id=braden_friction_1");
		selenium.click("id=braden_nutrition_1");
		selenium.click("link=Investigations");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=investigation_list", "label=Biopsy");
		selenium.select("id=investigation_day", "label=2");
		selenium.select("id=investigation_month", "label=May");
		selenium.type("id=investigation_year", "2000");
                selenium.type("id=blood_sugar", "12");
		selenium.select("id=bloodsugar_day", "label=1");
		selenium.select("id=bloodsugar_month", "label=Jan");
		selenium.type("id=bloodsugar_year", "1998");
		selenium.type("id=blood_sugar_meal", "1200");
		//selenium.type("id=purs_score", "12");
		selenium.click("id=AddAnti");
		selenium.type("id=albumin", "55");
		selenium.type("id=pre_albumin", "66");
		selenium.select("id=albumin_day", "label=1");
		selenium.select("id=prealbumin_day", "label=2");
		selenium.select("id=albumin_month", "label=Jan");
		selenium.select("id=prealbumin_month", "label=Feb");
		selenium.type("id=albumin_year", "1998");
		selenium.type("id=prealbumin_year", "1999");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("60000");
	
		assertTrue(selenium.isTextPresent("HCP Information: professionals"));
		assertTrue(selenium.isTextPresent("Co-Morbidities: Type I Diabetes Mellitus"));
		assertTrue(selenium.isTextPresent("History of Past Surgeries: history"));
		assertTrue(selenium.isTextPresent("Factors that Affect Wound Healing: N/A"));
		assertTrue(selenium.isTextPresent("Medications that Affect Wound Healing: N/A"));
                selenium.click("css=#ui-accordion-accordion-header-3 > label.title");
		assertTrue(selenium.isTextPresent("Diagnostic Tests: Biopsy : 02/May/2000"));
		assertTrue(selenium.isTextPresent("Albumin g/L: 55"));
		assertTrue(selenium.isTextPresent("ate: 01/Jan/1998"));
		assertTrue(selenium.isTextPresent("Prealbumin mg/L: 6"));
		assertTrue(selenium.isTextPresent("Date: 02/Feb/1999"));
		
		assertTrue(selenium.isTextPresent("Blood Glucose mmol/L: 12.0"));
		assertTrue(selenium.isTextPresent("Date: 01/Jan/1998"));
		assertTrue(selenium.isTextPresent("Time (Example: 1600): 1200"));
                selenium.click("css=#ui-accordion-accordion-header-1 > label.title");
                //java.io.File scrFile = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
		        //org.apache.commons.io.FileUtils.copyFile(scrFile, new java.io.File("c:\\test.jpg"));
                assertTrue(selenium.isTextPresent("Height: 170.0"));
		assertTrue(selenium.isTextPresent("Weight: 2.0"));
		assertTrue(selenium.isTextPresent("Pulse: 54"));
		//assertTrue(selenium.isTextPresent("PURS Score: 12"));
		assertTrue(selenium.isTextPresent("emperature: 100.0"));
		assertTrue(selenium.isTextPresent("espiratory Rate: 65"));
		assertTrue(selenium.isTextPresent("Blood Pressure: 65/65"));
                selenium.click("css=#ui-accordion-accordion-header-2 > label.title");
		assertTrue(selenium.isTextPresent("Sensory/Perception: 1"));
		assertTrue(selenium.isTextPresent("Moisture: 1"));
		assertTrue(selenium.isTextPresent("Braden Risk Score: 6"));
		
		//com.pixalere.ConstantsTest.click("saveEntries",driver);
                selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		//selenium.click("xpath=(//button[@type='button'])[5]");
		//selenium.click("css=#logout");
		//selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
