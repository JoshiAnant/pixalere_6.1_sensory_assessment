package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testFootAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestFootAssessment() throws Exception {
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.setSpeed("250");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
                selenium.click("css=img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Mobility Assessment");
		
		selenium.waitForPageToLoad("30000");
		selenium.click("id=indoor_footwear2");
		selenium.click("id=outdoor_footwear2");
		selenium.click("id=orthotics2");
		selenium.select("id=weight_bearing", "label=None");
		selenium.select("id=balance", "label=Steady");
		selenium.select("id=calf_muscle_pump", "label=Normal");
		selenium.click("css=option[value=\"1229\"]");
		selenium.select("id=muscle_tone_functional", "label=High");
		selenium.select("id=arches_foot_structure", "label=High");
		selenium.click("id=supination_foot_structure1");
		selenium.click("id=pronation_foot_structure1");
		selenium.click("id=dorsiflexion_passive1");
		selenium.click("id=dorsiflexion_passive3");
		selenium.click("id=dorsiflexion_active3");
		selenium.click("id=dorsiflexion_active1");
		selenium.click("id=plantarflexion_passive1");
		selenium.click("id=plantarflexion_active1");
		selenium.click("id=greattoe_active1");
		selenium.click("id=greattoe_passive1");
		selenium.select("id=mobility_aids", "label=Wheelchair");
		selenium.type("id=foot_comments", "ftt");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Mobility Assessment"));
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
