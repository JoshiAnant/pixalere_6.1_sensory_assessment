package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testEditLimbAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestEditLimbAssessment() throws Exception {
		selenium.setSpeed("550");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Limb Flowchart");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=editProfile1-227");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=left_missing_limbs2");
		selenium.click("id=left_missing_limbs1");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-228");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=right_missing_limbs1");
		selenium.click("id=right_missing_limbs2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-229");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=left_pain_assessment1");
		selenium.click("id=left_pain_assessment2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-230");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=right_pain_assessment1");
		selenium.click("id=right_pain_assessment4");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-233");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=left_skin_assessment1");
		selenium.click("id=left_skin_assessment2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-234");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=right_skin_assessment1");
		selenium.click("id=right_skin_assessment4");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-235");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_temperature_leg", "label=Warm");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-236");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_temperature_leg", "label=Warm");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-237");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_temperature_foot", "label=Warm");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-238");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_temperature_foot", "label=Cool");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-239");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_temperature_toes", "label=Warm");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-240");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_temperature_toes", "label=Cool");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-241");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_skin_colour_leg", "label=Red");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-242");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_skin_colour_leg", "label=Flesh tone");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-243");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_skin_colour_foot", "label=Flesh tone");
	
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-244");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_skin_colour_foot", "label=Bluish-purple");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-245");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_skin_colour_toes", "label=Black");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-246");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_skin_colour_toes", "label=Flesh tone");

		selenium.click("id=right_skin_colour_toes");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-467");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("xpath=(//input[@name='left_less_capillary'])[2]");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-468");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("name=right_less_capillary");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-281");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_edema_severity", "label=+2 Moderate 4mm");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-282");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_edema_severity", "label=+2 Moderate 4mm");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-283");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_edema_location", "label=Up to midcalf");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-284");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_edema_location", "label=Up to midcalf");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
                //
                selenium.click("id=editProfile1-247");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_dorsalis_pedis_palpation", "label=Diminished");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
                selenium.click("id=editProfile1-248");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_dorsalis_pedis_palpation", "label=Diminished");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
                selenium.click("id=editProfile1-249");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=left_posterior_tibial_palpation", "label=Diminished");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
                
                selenium.click("id=editProfile1-250");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.select("id=right_posterior_tibial_palpation", "label=Diminished");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
                //
		selenium.click("id=editProfile1-285");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=sleep_positions", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-287");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
	
		selenium.select("id=left_ankle_cm", "label=3");

		selenium.select("id=left_ankle_mm", "label=3");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-288");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=right_ankle_cm", "label=3");

		selenium.select("id=right_ankle_mm", "label=4");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-291");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=left_midcalf_cm", "label=4");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-292");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=right_midcalf_mm", "label=5");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-299");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=left_sensory2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-300");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=right_sensory1");
		selenium.click("id=right_sensory2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		
		assertEquals("Flesh tone Bluish-purple", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[4]/td[2]/table.0.1"));
		//assertEquals("Decrease with elevation No pain", selenium.getTable("id=limbflowchart.4.1"));
		//assertEquals("comments ...", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[6]/td[2]/table.0.1"));
		assertEquals("Bluish-purple Bluish-purple", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[6]/td[2]/table.0.1"));
		assertEquals("Flesh tone Bluish-purple", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[7]/td[2]/table.0.1"));
		assertEquals("Flesh tone Bluish-purple", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[8]/td[2]/table.0.1"));
		assertEquals("Black Bluish-purple", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[9]/td[2]/table.0.1"));
		assertEquals("Warm Cold", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[10]/td[2]/table.0.1"));
		assertEquals("Warm Cold", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[11]/td[2]/table.0.1"));
		assertEquals("Cool Cold", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[12]/td[2]/table.0.1"));
		assertEquals("Warm Cold", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[13]/td[2]/table.0.1"));
		assertEquals("Warm Cold", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[15]/td[2]/table.0.1"));
		assertEquals("Cool Cold", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[14]/td[2]/table.0.1"));
		assertEquals("Diminished Present", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[16]/td[2]/table.0.1"));
		assertEquals("Diminished Present", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[17]/td[2]/table.0.1"));
		assertEquals("Diminished Present", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[18]/td[2]/table.0.1"));
		assertEquals("Diminished Present", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[19]/td[2]/table.0.1"));
		assertEquals("Yes Yes", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[20]/td[2]/table.0.1"));
		assertEquals("No Yes", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[21]/td[2]/table.0.1"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[24]/td[2]/table.0.1"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[25]/td[2]/table.0.1"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[26]/td[2]/table.0.1"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[27]/td[2]/table.0.1"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[28]/td[2]/table.0.1"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[29]/td[2]/table.0.1"));
		assertEquals("Up to midcalf Foot", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[30]/td[2]/table.0.1"));
		assertEquals("Up to midcalf Foot", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[31]/td[2]/table.0.1"));
		assertEquals("+2 Moderate 4mm Non-pitting", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[32]/td[2]/table.0.1"));
		assertEquals("+2 Moderate 4mm Non-pitting", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[33]/td[2]/table.0.1"));
		assertEquals("3.4 1.2", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[35]/td[2]/table.0.1"));
		assertEquals("3.3 1.2", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[36]/td[2]/table.0.1"));
		assertEquals("1.5 1.2", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[37]/td[2]/table.0.1"));
		assertEquals("4.2 1.2", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[38]/td[2]/table.0.1"));
		assertEquals("Fragile Dry/flaky", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[39]/td[2]/table.0.1"));
		assertEquals("Itchy Dry/flaky", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[40]/td[2]/table.0.1"));
		assertEquals("Numbness None of the above", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[41]/td[2]/table.0.1"));
		assertEquals("None of the above, Numbness None of the above", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[42]/td[2]/table.0.1"));
		assertEquals("Intermittent Ache", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[43]/td[2]/table.0.1"));
		assertEquals("Knife-like Ache", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[44]/td[2]/table.0.1"));
		selenium.click("id=logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
