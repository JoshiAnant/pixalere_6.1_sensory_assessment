package com.pixalere.selenium;

import org.openqa.selenium.WebDriver;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.thoughtworks.selenium.*;
import java.sql.Connection;
import java.sql.Statement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.By;
import com.pixalere.common.service.ConfigurationServiceImpl;
import com.pixalere.common.bean.ConfigurationVO;
/*
 *firefox
 *mock
 *firefoxproxy
 *pifirefox
 *chrome
 *iexploreproxy
 *iexplore
 *firefox3
 *safariproxy
 *googlechrome
 *konqueror
 *firefox2
 *safari
 *piiexplore
 *firefoxchrome
 *opera
 *iehta
 *custom
 */

public class testManageResources extends SeleneseTestBase {

    private Selenium selenium;
    private final String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;

    @Before
    public void setUp() throws Exception {
        if (browser.equals("*chrome")) {
            driver = new FirefoxDriver();
        } else if (browser.equals("*iexplore")) {
            System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");
            driver = new InternetExplorerDriver();
        } else if (browser.equals("*googlechrome")) {
            System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
            driver = new ChromeDriver();
        }
        selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
        //selenium.start();
        ConfigurationServiceImpl cservice = new ConfigurationServiceImpl();
        ConfigurationVO conf = new ConfigurationVO();
        conf.setConfig_setting("showFeedback");
        ConfigurationVO newbie = cservice.getConfiguration(conf);
        newbie.setCurrent_value("0");
        cservice.saveConfiguration(newbie);
    }

    @Test
    public void testTestManageResources() throws Exception {
        selenium.open("/pixalere/LoginSetup.do");
        driver.manage().window().maximize();
        selenium.type("id=userId", "7");
        selenium.type("id=password", "fat.burning2@");
        selenium.click("name=submit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Question 1"));
        assertTrue(selenium.isTextPresent("Question 2"));
        selenium.type("answer_one", "Simcoe");
        selenium.type("answer_two", "Simcoe");
        selenium.type("phone", "16047980440");
        selenium.click("id=submit2");
        selenium.waitForPageToLoad("30000");
        selenium.click("id=btnclose");
                //com.pixalere.ConstantsTest.click("logout",driver);

        java.io.File scrFile = ((org.openqa.selenium.TakesScreenshot) driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
        org.apache.commons.io.FileUtils.copyFile(scrFile, new java.io.File("c:\\test123.jpg"));
        selenium.windowMaximize();
        selenium.click("css=#navAdmin > img.toolbarSpacer");
        selenium.waitForPageToLoad("30000");
        selenium.select("pixActionMenu", "Manage Resources");

        selenium.click("id=adminMenu");
        selenium.waitForPageToLoad("30000");

        selenium.select("name=resourceMenu", "label=Treatment Area");
        selenium.click("//input[@value=' Continue ']");
        selenium.waitForPageToLoad("30000");
        selenium.type("name=en", "Sample Area");
        selenium.type("name=fr", "Sample Area Qui");
        selenium.click("id=editsubmit");
        selenium.waitForPageToLoad("30000");
        selenium.select("name=resourceMenu", "label=Treatment Location");
        selenium.click("//input[@value=' Continue ']");
        selenium.waitForPageToLoad("30000");
        selenium.select("name=categoryId", "Sample Area");
        selenium.type("name=en", "Sample Location");
        selenium.type("name=fr", "Sample Location Qui qui");
        selenium.click("name=editsubmit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Sample Location"));
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

}
