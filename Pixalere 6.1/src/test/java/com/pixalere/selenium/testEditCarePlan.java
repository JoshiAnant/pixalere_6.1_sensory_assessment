package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testEditCarePlan   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}
	@Test
	public void testTestEditCarePlan() throws Exception {
		selenium.setSpeed("450");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "burn.the.fat11");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		//selenium.click("xpath=(//a[contains(@href, '#')])[3]");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.select("css=select[name=\"wound_profile_type_id\"]", "label=Head/neck (Anterior) - Wound");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Care Plan");
		selenium.waitForPageToLoad("30000");
                selenium.selectFrame("body_ifr");
                selenium.focus("id=tinymce");
                selenium.typeKeys("id=tinymce", "nursing care plan2");
		selenium.type("id=tinymce", "nursing care plan2");
                selenium.selectWindow("null");
                driver.switchTo().defaultContent();
		selenium.click("id=commit2");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("nursing care plan2"));
		selenium.type("name=delete_reason", "delete me");
		selenium.click("id=submit6");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Reason for Deletion: delete me"));
		assertTrue(selenium.isTextPresent("nursing care plan2"));
		selenium.click("css=#logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
