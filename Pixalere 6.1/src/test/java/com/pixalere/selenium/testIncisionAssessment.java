package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testIncisionAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestIncisionAssessment() throws Exception {
		selenium.setTimeout("300000");
		selenium.setSpeed("450");
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Chest");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=postop_management1");
		selenium.click("id=incision_exudate1");
	
		selenium.select("id=pain", "label=1");
		selenium.click("id=incision_status_1");
		selenium.click("id=incision_closure_methods1");
		selenium.click("id=peri_incisional1");
		selenium.type("id=pain_comments", "comments");
		selenium.click("id=navTreatment");
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if (selenium.isElementPresent("id=go_confirm")) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		selenium.click("id=go_confirm");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=products", "label=Acticoat 10cm x 20cm");
                selenium.select("wound_ass", "Incision 1");
		selenium.click("id=review_done");
		selenium.click("id=addAlpha");
		selenium.select("id=dressing_change_frequency_alphas", "label=Incision 1");
		selenium.click("id=cs_done1");
		selenium.select("id=dressing_change_frequency_list", "label=BID");
		selenium.click("id=adddressing_change_frequency");
                
		selenium.type("id=incision_treatment_comments", "tssdf");
                selenium.click("link=Negative Pressure Wound Therapy (NPWT)");
		selenium.select("id=vacend1_day", "label=1");
		selenium.select("id=vacend1_month", "label=Jan");
		selenium.type("id=vacend1_year", "2000");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
                
		selenium.click("name=referral_incision");
		selenium.select("id=priority_incision", "label=Clinical Review (1 - 7 days)");
		selenium.type("id=body_referral_incision", "comments");
                
                selenium.click("css=#ui-accordion-accordion-header-1 > label.title");
		assertTrue(selenium.isTextPresent("Wound Status: Open"));
		assertTrue(selenium.isTextPresent("Post-Op Day:"));
		assertTrue(selenium.isTextPresent("Clinical Pathway: N/A"));
		assertTrue(selenium.isTextPresent("Post-Op Management: Not applicable"));
		assertTrue(selenium.isTextPresent("Pain at Site (scale 0-10): 1"));
		assertTrue(selenium.isTextPresent("Pain Comments: comments"));
		assertTrue(selenium.isTextPresent("Incision Status: Approximated"));
		assertTrue(selenium.isTextPresent("Closure Method(s) Insitu: Not applicable"));
		assertTrue(selenium.isTextPresent("Exudate: Nil"));
		assertTrue(selenium.isTextPresent("Exudate Amount: Nil"));
		assertTrue(selenium.isTextPresent("eri-Incisional Skin: Indurated < 2cm"));
		assertTrue(selenium.isTextPresent("Products: 1 Acticoat 10cm x 20cm"));
		assertTrue(selenium.isTextPresent("Treatment Comments: tssdf"));
		assertTrue(selenium.isTextPresent("C&S Done: Yes"));
		assertTrue(selenium.isTextPresent("Dressing Change Frequency: BID"));
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
		selenium.click("css=#logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
