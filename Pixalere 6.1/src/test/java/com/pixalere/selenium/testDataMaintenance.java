package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testDataMaintenance   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestDataMaintenance() throws Exception {
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "9");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Chest");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		//  Should get warning 
                
                assertTrue(selenium.isTextPresent("There is an unacknowledged Wound Clinician PN/Recommendation for this alpha that needs to be acknowledged before you can continue with your charting."));
		assertTrue(selenium.isTextPresent("Recommendation: test1"));
		assertTrue(selenium.isTextPresent("Progress Notes: test2"));
		selenium.click("id=ack");selenium.waitForPageToLoad("30000");
		selenium.click("id=assessment_type1");
		selenium.click("id=populate1");
		selenium.waitForPageToLoad("30000");
                
		selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
		selenium.click("id=go_confirm");
		selenium.waitForPageToLoad("30000");
                
		selenium.click("id=review_done");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
                java.io.File scrFile5 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                org.apache.commons.io.FileUtils.copyFile(scrFile5, new java.io.File("c:\\summ.jpg"));
                assertTrue(selenium.isTextPresent("No product or product quantities were changed"));
                selenium.click("id=go_confirm");
                selenium.waitForPageToLoad("30000");
                
		selenium.select("id=backdated_day", "label=1");
		selenium.select("id=backdated_month", "label=Jan");
		selenium.type("id=backdated_year", "2011");
		selenium.click("id=saveEntries");
		assertTrue(selenium.isTextPresent("Referral is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)"));
		selenium.click("xpath=(//input[@name='referral_incision'])[2]");
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.select("css=select[name=\"wound_profile_type_id\"]", "label=Chest - Incision");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Assessment Flowchart");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("01/Jan/2011"));
		selenium.click("link=Wound Flowchart");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Progress Notes");
		selenium.waitForPageToLoad("30000");
		
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
