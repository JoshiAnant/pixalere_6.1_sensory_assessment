package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testEditBraden   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
  private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestEditBraden() throws Exception {
		selenium.setSpeed("1050");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Braden Flowchart");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#editProfile1-218 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=braden_sensory_2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-219 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=braden_moisture_2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-220 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=braden_activity_2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-221 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=braden_mobility_2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-222 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=braden_nutrition_2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("css=#editProfile1-223 > img");
		selenium.waitForPopUp("large_fix_win", "30000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=braden_friction_3");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		assertEquals("13", selenium.getTable("//table[@id='bradenflowchart']/tbody/tr[8]/td[2]/table.0.0"));
		assertEquals("3 1", selenium.getTable("//table[@id='bradenflowchart']/tbody/tr[7]/td[2]/table.0.1"));
		assertEquals("2 1", selenium.getTable("//table[@id='bradenflowchart']/tbody/tr[6]/td[2]/table.0.1"));
		assertEquals("2 1", selenium.getTable("//table[@id='bradenflowchart']/tbody/tr[5]/td[2]/table.0.1"));
		assertEquals("2 1", selenium.getTable("//table[@id='bradenflowchart']/tbody/tr[4]/td[2]/table.0.1"));
		assertEquals("2 1", selenium.getTable("css=td.column-layer_darkb > table.0.1"));
		assertEquals("2 1", selenium.getTable("css=td.column-layer_lightb > table.0.1"));
		assertEquals("13", selenium.getTable("//table[@id='bradenflowchart']/tbody/tr[8]/td[2]/table.0.0"));
		selenium.click("id=logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
