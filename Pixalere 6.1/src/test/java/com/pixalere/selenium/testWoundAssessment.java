package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
public class testWoundAssessment  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
     private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestWoundAssessment() throws Exception {
		selenium.setTimeout("300000");
		selenium.open("/pixalere/logout.do");
                selenium.windowMaximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
                selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Head/neck (Anterior)");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=etiology", "label=Wound A: Neuropathic/Diabetic");
		selenium.click("css=option[value=\"Wound A: 798\"]");
		assertTrue(selenium.isTextPresent("Wound A: Neuropathic/Diabetic"));
		selenium.select("id=goals", "label=Wound A: Heal wound");
		selenium.click("css=option[value=\"Wound A: 1081\"]");
		assertTrue(selenium.isTextPresent("Wound A: Heal wound"));
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=assessment_type1");
		assertEquals("on", selenium.getValue("id=assessment_type1"));
		selenium.click("css=#navAssessment > img.toolbarSpacer");
		selenium.click("id=assessment_type1");
		selenium.click("css=#navAssessment > img.toolbarSpacer");
		selenium.click("id=pain");
		
		assertTrue(selenium.isTextPresent("Pain is required."));
		assertTrue(selenium.isTextPresent("Wound Bed is required."));
		assertTrue(selenium.isTextPresent("Exudate Characteristics is required."));
		assertTrue(selenium.isTextPresent("Exudate Amount is required."));
		assertTrue(selenium.isTextPresent("Wound Edge is required."));
		assertTrue(selenium.isTextPresent("Peri-Wound Skin is required."));
		//assertTrue(selenium.isTextPresent("Length is required."));
		//assertTrue(selenium.isTextPresent("Width is required."));
		//assertTrue(selenium.isTextPresent("Depth is required."));
		selenium.select("id=wound_date_day", "label=1");
		selenium.select("id=wound_date_month", "label=Jun");
		selenium.type("id=wound_date_year", "2005");
		selenium.click("id=recurrent1");
                
		selenium.select("pain", "1");
                
		selenium.type("id=pain_comments", "pain comments");

		selenium.type("id=length", "8.0");
		selenium.type("id=width", "6.0");
		selenium.type("id=depth", "1.0");
		selenium.click("id=exudate_type1");selenium.click("id=exudate_odour1");

		selenium.click("id=exudate_amount_1");
		selenium.click("id=woundbase1");
		selenium.click("id=woundbase1");
		selenium.click("id=woundbase1");

		selenium.select("woundbase_percentage_1", "100%");
	
                //
		selenium.click("id=wound_edge1");
                selenium.click("id=undermining_location_panel_uiicon");
		selenium.select("id=num_under", "label=1");
		selenium.click("id=oku");
		selenium.waitForPageToLoad("30000");

		selenium.select("undermining_start_1", "1:00");
                selenium.click("css=#undermining_start_1 > option[value=\"1\"]");
      
		selenium.select("undermining_end_1", "1:00");
                selenium.click("css=#undermining_end_1 > option[value=\"1\"]");
    
		selenium.select("underminingdepth_1", "2");
                selenium.click("css=#underminingdepth_1 > option[value=\"2\"]");

		selenium.select("underminingdepth_mini_1", "1");
                selenium.click("css=#underminingdepth_mini_1 > option[value=\"1\"]");
                
                //selenium.click("id=sinus_location_panel_uiicon");
               
		selenium.select("id=num_sinus_tracts", "label=1");
		selenium.click("id=oks");
		selenium.waitForPageToLoad("30000");
		selenium.select("sinustract_start_1", "3:00");
		selenium.select("sinusdepth_1", "2");
		selenium.select("sinusdepth_mini_1", "1");
                //selenium.click("id=fistulas_panel_uiicon");
		selenium.select("id=num_fistulas", "label=1");
		selenium.click("id=okf");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=fistulas_1", "fistula comment");
		selenium.click("id=periwound1");
		selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
		
		//assertTrue(selenium.isTextPresent("PUSH Scale : 10"));
		selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
                
		selenium.waitForPageToLoad("30000");
                
                selenium.select("wound_ass", "Wound A");
		selenium.select("products", "10cc syringe");
		selenium.click("id=addAlpha");
		selenium.select("dressing_change_frequency_alphas", "Wound A");
		selenium.select("dressing_change_frequency_list", "TID");
		selenium.click("id=adddressing_change_frequency");
		selenium.click("id=visit_freq");
		selenium.select("id=visit_freq", "label=TID");
		selenium.click("id=cs_done1");
		selenium.click("id=as_needed");
                
		selenium.click("id=review_done");
		selenium.click("css=option[value=\"243\"]");
                selenium.click("link=Negative Pressure Wound Therapy (NPWT)");
               
		selenium.select("vacstart1_day", "2");
		selenium.select("vacstart1_month", "Jan");
		selenium.type("vacstart1_year", "1999");
		selenium.select("vacend1_day", "3");
		selenium.select("vacend1_month", "Jan");
		selenium.type("vacend1_year", "2000");
		selenium.select("pressure_reading1", "25 mm/hg");
                
		selenium.select("id=vendor_name1", "label=KCI");
                selenium.click("id=machine_acquirement1_2");
		selenium.click("id=therapy_setting1_1");
		selenium.click("name=npwt_alphas1");
		selenium.select("name=initiated_by1", "label=Acute");
		selenium.select("name=goal_of_therapy1", "label=Wound Closure");
		selenium.select("name=goal_of_therapy1", "label=Prep for STSG");
		selenium.select("name=reason_for_ending1", "label=Wound Closure");
		selenium.type("name=serial_num1", "11111");
		selenium.type("name=kci_num1", "22222");
                
                selenium.click("link=Treatment");
		selenium.select("csresult_alphas", "Wound A");
		selenium.select("csresult_list", "MRSA");
		selenium.click("id=addcsresult");
                
		selenium.type("id=wound_treatment_comments", "tsc");
	
		selenium.type("id=cns_name", "test");
		selenium.select("cns_start_day", "1");
		selenium.select("id=cns_start_month", "label=Jan");
		selenium.type("id=cns_start_year", "2000");
		selenium.select("cns_end_day", "4");
		selenium.select("id=cns_end_month", "label=Jan");
		selenium.type("id=cns_end_year", "2000");
		
		selenium.type("id=bandages_out1", "1");
		selenium.type("id=bandages_in1", "2");
		
		selenium.click("id=addAnti");
		selenium.click("id=adjunctive1");
                selenium.selectFrame("wound_nursing_care_plan_ifr");
                selenium.focus("id=tinymce");
                //selenium.getEval("this.browserbot.findElement('id=tinymce').innerHTML='care plan1'");
		selenium.type("id=tinymce", "care plan1");
                selenium.selectWindow("null");
                driver.switchTo().defaultContent();
                
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
                selenium.click("css=#ui-accordion-accordion-header-1 > label.title");
		assertTrue(selenium.isTextPresent("Status: Open"));
		assertTrue(selenium.isTextPresent("Date of Onset: 01/Jun/2005"));
		assertTrue(selenium.isTextPresent("Recurrent: Yes"));
		assertTrue(selenium.isTextPresent("Pain Scale (0-10): 1"));
		assertTrue(selenium.isTextPresent("Pain Comments: pain comments"));
		assertTrue(selenium.isTextPresent("Length (cm): 8.0"));
		assertTrue(selenium.isTextPresent("Width (cm): 6.0"));
		assertTrue(selenium.isTextPresent("Depth (cm): 1.0"));
		assertTrue(selenium.isTextPresent("Undermining Location: #1 1:00 to 1:00"));
		assertTrue(selenium.isTextPresent("Undermining Depth (cm): #1 2."));
		assertTrue(selenium.isTextPresent("Sinus Tract Location: #1 3:00"));
		assertTrue(selenium.isTextPresent("Sinus Tract Depth (cm): #1 2.1"));
		assertTrue(selenium.isTextPresent("Fistula: Fistula #1: fistula comment"));
		assertTrue(selenium.isTextPresent("Wound Bed: Not assessed: 100%"));
		assertTrue(selenium.isTextPresent("Exudate Characteristics: Other"));
		assertTrue(selenium.isTextPresent("Exudate Amount: Not assessed"));
		assertTrue(selenium.isTextPresent("Odour (after cleansing): Yes"));
		assertTrue(selenium.isTextPresent("Wound Edge: Eroding"));
		assertTrue(selenium.isTextPresent("Peri-Wound Skin: Indurated < 2cm"));
		assertTrue(selenium.isTextPresent("Products: 1 10cc syringe"));
		assertTrue(selenium.isTextPresent("Treatment Comments: tsc"));
		assertTrue(selenium.isTextPresent("Packing Count # pieces inserted: 2"));
		assertTrue(selenium.isTextPresent("Packing Count # pieces removed: 1"));
                //java.io.File scrFile34 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                //org.apache.commons.io.FileUtils.copyFile(scrFile34, new java.io.File("c:\\test11.jpg"));
		assertTrue(selenium.isTextPresent("NPWT Start Date: 02/Jan/1999"));
		assertTrue(selenium.isTextPresent("NPWT End Date: 03/Jan/2000"));
		assertTrue(selenium.isTextPresent("NPWT Therapy Setting: intermittent"));
		assertTrue(selenium.isTextPresent("NPWT Pressure Reading: 25 mm/hg"));
                assertTrue(selenium.isTextPresent("Machine Acquirement: Rented"));
                assertTrue(selenium.isTextPresent("Vendor: KCI"));
		assertTrue(selenium.isTextPresent("C&S Done: Yes"));
		assertTrue(selenium.isTextPresent("C&S Result: MRSA"));
                //selenium.captureScreenshot("/Users/travis/editlimb.png");
		assertTrue(selenium.isTextPresent("Antibiotic Therapy: test : 01/Jan/2000 to 04/Jan/2000"));
		assertTrue(selenium.isTextPresent("Adjunctive Therapies: Laser"));
		assertTrue(selenium.isTextPresent("Dressing Change Frequency: TID"));
		//assertTrue(selenium.isTextPresent("Care Plan: care plan1"));
		assertTrue(selenium.isTextPresent("Visit Frequency: TID"));
                selenium.click("id=saveEntries");
		assertTrue(selenium.isTextPresent("Visit Frequency: TID"));
                
		selenium.click("name=referral_wound");
		selenium.type("name=body_referral_wound", "comments");
		selenium.select("id=priority_wound", "label=Urgent (24 - 48hrs)");
                selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
                
		selenium.waitForPageToLoad("30000");
                
                
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
                selenium.click("css=#ui-accordion-accordion-header-1 > label.title");
		assertTrue(selenium.isTextPresent("Visit Frequency: TID"));
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
		selenium.click("css=#logout");
		selenium.waitForPageToLoad("30000");
	}
        
	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
