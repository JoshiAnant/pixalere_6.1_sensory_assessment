package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;

public class testOfflineViewer  extends SeleneseTestBase{
    private DefaultSelenium selenium;
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*chrome", "http://localhost:8080/");
		selenium.start();
	}

	@Test
	public void testTestOfflineViewer() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
