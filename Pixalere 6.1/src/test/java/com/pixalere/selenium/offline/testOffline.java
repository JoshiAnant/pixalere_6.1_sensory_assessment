package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testOffline  extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}
        @Test
        public void testCreateMorePatients() throws Exception {
            //4
            selenium.open("/pixalere/LoginSetup.do");
        selenium.type("id=userId", "7");
        selenium.type("id=password", "fat.burning2@");
        selenium.click("name=submit");
        selenium.waitForPageToLoad("30000");
        selenium.click("id=btnclose");
        selenium.click("css=#navAdmin2 > img.toolbarSpacer");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "Morris");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "adsfsdf");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastName", "Morris4");
        selenium.type("id=firstName", "Travis4");
        selenium.select("id=gender", "label=Male");
        selenium.select("id=dob_day", "label=9");
        selenium.select("id=dob_month", "label=Mar");
        selenium.type("id=dob_year", "2012");
        selenium.type("id=allergies", "bees");

        //selenium.click("name=randomPHN");
        selenium.click("id=oop2");

        selenium.type("id=phn11","1111");
        selenium.type("id=version_code", "AB");
        selenium.select("id=treatmentLocation", "label=Sample Location");
        selenium.click("id=addSubmit");
        assertTrue(selenium.isTextPresent("Funding Source is required"));
        selenium.select("id=fundingSource", "label=Hospital/CHA/Facility");
        selenium.click("id=addSubmit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Travis4 Morris4"));
        assertTrue(selenium.isTextPresent("Sample Location"));
        assertTrue(selenium.isTextPresent("Admitted"));
        assertTrue(selenium.isTextPresent("09/Mar/2012"));
        //5
        selenium.open("/pixalere/LoginSetup.do");
        selenium.type("id=userId", "7");
        selenium.type("id=password", "fat.burning2@");
        selenium.click("name=submit");
        selenium.waitForPageToLoad("30000");
        java.io.File scrFile5 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                org.apache.commons.io.FileUtils.copyFile(scrFile5, new java.io.File("c:\\summ333.jpg"));
        //selenium.click("id=btnclose");
        selenium.click("css=#navAdmin2 > img.toolbarSpacer");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "Morris");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "adsfsdf");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastName", "Morris5");
        selenium.type("id=firstName", "Travis5");
        selenium.select("id=gender", "label=Male");
        selenium.select("id=dob_day", "label=9");
        selenium.select("id=dob_month", "label=Mar");
        selenium.type("id=dob_year", "2012");
        selenium.type("id=allergies", "bees");

        selenium.click("id=oop2");

        selenium.type("id=phn11","1111");
        selenium.type("id=version_code", "AB");
        selenium.select("id=treatmentLocation", "label=Sample Location");
        selenium.click("id=addSubmit");
        assertTrue(selenium.isTextPresent("Funding Source is required"));
        selenium.select("id=fundingSource", "label=Hospital/CHA/Facility");
        selenium.click("id=addSubmit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Travis5 Morris5"));
        assertTrue(selenium.isTextPresent("Sample Location"));
        assertTrue(selenium.isTextPresent("Admitted"));
        assertTrue(selenium.isTextPresent("09/Mar/2012"));
        //6
        selenium.open("/pixalere/LoginSetup.do");
        selenium.type("id=userId", "7");
        selenium.type("id=password", "fat.burning2@");
        selenium.click("name=submit");
        selenium.waitForPageToLoad("30000");
        //selenium.click("id=btnclose");
        selenium.click("css=#navAdmin2 > img.toolbarSpacer");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "Morris");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "adsfsdf");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastName", "Morris6");
        selenium.type("id=firstName", "Travis6");
        selenium.select("id=gender", "label=Male");
        selenium.select("id=dob_day", "label=9");
        selenium.select("id=dob_month", "label=Mar");
        selenium.type("id=dob_year", "2012");
        selenium.type("id=allergies", "bees");

        selenium.click("id=oop2");

        selenium.type("id=phn11","1111");
        selenium.type("id=version_code", "AB");
        selenium.select("id=treatmentLocation", "label=Sample Location");
        selenium.click("id=addSubmit");
        assertTrue(selenium.isTextPresent("Funding Source is required"));
        selenium.select("id=fundingSource", "label=Hospital/CHA/Facility");
        selenium.click("id=addSubmit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Travis6 Morris6"));
        assertTrue(selenium.isTextPresent("Sample Location"));
        assertTrue(selenium.isTextPresent("Admitted"));
        assertTrue(selenium.isTextPresent("09/Mar/2012"));
        //7
        selenium.open("/pixalere/LoginSetup.do");
        selenium.type("id=userId", "7");
        selenium.type("id=password", "fat.burning2@");
        selenium.click("name=submit");
        selenium.waitForPageToLoad("30000");
       // selenium.click("id=btnclose");
        selenium.click("css=#navAdmin2 > img.toolbarSpacer");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "Morris");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastNameSearch", "adsfsdf");
        selenium.click("id=search_admin");
        selenium.waitForPageToLoad("30000");
        selenium.type("id=lastName", "Morris7");
        selenium.type("id=firstName", "Travis7");
        selenium.select("id=gender", "label=Male");
        selenium.select("id=dob_day", "label=9");
        selenium.select("id=dob_month", "label=Mar");
        selenium.type("id=dob_year", "2012");
        selenium.type("id=allergies", "bees");
        selenium.click("id=oop2");
        selenium.type("id=phn11","1111");
        selenium.type("id=version_code", "AB");
        selenium.select("id=treatmentLocation", "label=Sample Location");
        selenium.click("id=addSubmit");
        assertTrue(selenium.isTextPresent("Funding Source is required"));
        selenium.select("id=fundingSource", "label=Hospital/CHA/Facility");
        selenium.click("id=addSubmit");
        selenium.waitForPageToLoad("30000");
        assertTrue(selenium.isTextPresent("Travis7 Morris7"));
        assertTrue(selenium.isTextPresent("Sample Location"));
        assertTrue(selenium.isTextPresent("Admitted"));
        assertTrue(selenium.isTextPresent("09/Mar/2012"));
        
		selenium.open("/offline/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("id=offlineoption");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=patient1", "1");
		selenium.type("id=patient2", "2");
		selenium.type("id=patient3", "3");
		selenium.type("id=patient4", "4");
		selenium.type("id=patient5", "5");
		selenium.type("id=patient6", "6");
		selenium.type("id=patient7", "7");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
                
		assertTrue(selenium.isTextPresent("Patient 2 was downloaded."));
                assertTrue(selenium.isTextPresent("Patient 4 was downloaded."));
		selenium.click("//button[@type='button']");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
