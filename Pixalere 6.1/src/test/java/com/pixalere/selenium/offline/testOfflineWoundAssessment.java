package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
public class testOfflineWoundAssessment  extends SeleneseTestBase{
    private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
	}

	@Test
	public void testTestOfflineWoundAssessment() throws Exception {
		selenium.open("/offline/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Head/neck (Anterior)");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=assessment_type1");
	
		selenium.select("pain", "1");
		selenium.type("id=pain_comments", "pain comments");
		selenium.click("id=length");
                selenium.click("id=recurrent1");selenium.click("id=exudate_odour1");
		selenium.type("id=length", "8.0");

		selenium.type("id=width", "6.0");
		selenium.type("id=depth", "1.0");

		selenium.click("id=exudate_type1");
		selenium.click("id=woundbase1");

		selenium.select("woundbase_percentage_1", "100%");
		selenium.click("id=wound_edge1");
                selenium.click("id=undermining_location_panel_uiicon");
		selenium.select("id=num_under", "label=1");
		selenium.click("id=oku");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=undermining_start_1");
		selenium.select("id=undermining_start_1", "label=1:00");
		selenium.click("css=#undermining_start_1 > option[value=\"1\"]");
		selenium.click("id=undermining_end_1");
		selenium.select("id=undermining_end_1", "label=1:00");
		selenium.click("css=#undermining_end_1 > option[value=\"1\"]");
		selenium.click("id=underminingdepth_1");
		selenium.select("id=underminingdepth_1", "label=2");
		selenium.click("css=#underminingdepth_1 > option[value=\"2\"]");
		selenium.click("id=underminingdepth_mini_1");
		selenium.select("id=underminingdepth_mini_1", "label=1");
		selenium.click("css=#underminingdepth_mini_1 > option[value=\"1\"]");
               // selenium.click("id=sinus_location_panel_uiicon");
		selenium.select("id=num_sinus_tracts", "label=1");
		selenium.click("id=oks");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=sinustract_start_1");
		selenium.select("id=sinustract_start_1", "label=3:00");
		selenium.click("css=#sinustract_start_1 > option[value=\"3\"]");
		selenium.click("id=sinusdepth_1");
		selenium.select("id=sinusdepth_1", "label=2");
		selenium.click("css=#sinusdepth_1 > option[value=\"2\"]");
		selenium.select("id=sinusdepth_mini_1", "label=1");
		selenium.click("css=#sinusdepth_mini_1 > option[value=\"1\"]");
		selenium.select("id=num_fistulas", "label=1");
		selenium.click("id=okf");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=fistulas_1", "fistula comment");
		selenium.click("id=periwound1");

		
		selenium.click("id=periwound4");
		selenium.type("name=fistulas_1", "fistula comment 223 565 offf");
                selenium.click("css=#navTreatment > img[alt=\"spacer\"]");

                
           
		
                
                selenium.click("id=review_done");
                selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
                verifyTrue(selenium.isTextPresent("No product or product quantities were changed, if this is okay click"));
                
                selenium.click("id=go_confirm");
                selenium.waitForPageToLoad("30000");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("60000");
                
		selenium.click("name=referral_wound");
		selenium.select("id=priority_wound", "label=Urgent (24 - 48hrs)");
                selenium.type("name=body_referral_wound", "commentsoff");
		
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
