package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testOfflineOstomyAssessment extends SeleneseTestBase {
private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
	}

	@Test
	public void testTestOfflineOstomyAssessment() throws Exception {
		selenium.open("/offline/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Abdomen");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=assessment_type1");
                selenium.click("id=construction_1");
		selenium.click("id=shape_1");
		//selenium.click("id=mucous_fistula_present_show1");
		selenium.click("id=drainage1");
		selenium.click("id=perifistula_skin1");
		selenium.click("id=profile1");
		
		selenium.select("id=circumference_mm", "1");
		selenium.click("id=length_mm");
		selenium.click("id=appearance1");
		selenium.click("id=urine_colour1");
		selenium.click("id=urine_quantity_1");
		selenium.click("id=urine_type2");
		selenium.type("id=drainage_comments", "test");
		selenium.click("id=nutritional_status_1");
		selenium.click("id=contour2");
		selenium.click("id=devices_2");
                selenium.click("id=pouching_panel_uiicon");
		selenium.click("id=pouching1");
		selenium.type("id=nutritional_status_other", "stat");
		selenium.click("id=flange_pouch_4");
		selenium.type("id=flange_pouch_comments", "puch");
		selenium.click("id=mucocutaneous_margin1");
		selenium.click("id=self_care_progress2");
		selenium.click("id=periostomy_skin2");
		selenium.type("id=self_care_progress_comments", "test");
		selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
                java.io.File scrFile566 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                org.apache.commons.io.FileUtils.copyFile(scrFile566, new java.io.File("c:\\summ333off22.jpg"));
		selenium.select("id=products", "label=10cc syringe");
		selenium.click("css=option[value=\"1110\"]");
		selenium.click("id=addAlpha");
		selenium.click("id=visit_freq");
		selenium.select("id=visit_freq", "label=other");
		selenium.click("css=option[value=\"252\"]");
		selenium.click("id=cs_done1");
		selenium.select("id=csresult_alphas", "label=Urostomy");
		selenium.click("css=#csresult_alphas > option[value=\"Urostomy\"]");
		selenium.click("id=csresult_list");
		selenium.select("id=csresult_list", "label=E.coli");
		selenium.click("css=option[value=\"166\"]");
		selenium.click("id=addcsresult");
	
		selenium.type("id=cns_name", "off");
		selenium.select("id=cns_start_day", "label=1");
		selenium.select("id=cns_end_day", "label=9");
		selenium.select("id=cns_start_month", "label=Jun");
		selenium.select("id=cns_end_month", "label=Aug");
		selenium.type("id=cns_start_year", "1999");
		selenium.type("id=cns_end_year", "1999");
                selenium.click("id=review_done");
		selenium.click("id=addAnti");
		selenium.click("id=adjunctive3");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
                
                /*verifyTrue(selenium.isTextPresent("No product or product quantities were changed, if this is okay click"));
                java.io.File scrFile34 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                org.apache.commons.io.FileUtils.copyFile(scrFile34, new java.io.File("c:\\test11.jpg"));
                selenium.click("id=go_confirm");
		selenium.waitForPageToLoad("30000");*/
		selenium.click("name=referral_ostomy");
		selenium.select("id=priority_ostomy", "label=Urgent (24 - 48hrs)");
                selenium.type("name=body_referral_ostomy", "commentsoff");
		
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
