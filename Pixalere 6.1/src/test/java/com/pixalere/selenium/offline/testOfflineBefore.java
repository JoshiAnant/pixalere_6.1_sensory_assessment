package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testOfflineBefore extends SeleneseTestBase {
 private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
	}

	@Test
	public void testTestOfflineBefore() throws Exception {
		selenium.open("/offline/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		//assertEquals("Male", selenium.getTable("css=td.column-layer_lightb > table.0.0"));
		//assertEquals("01/Mar/2012", selenium.getTable("css=td.column-layer_darkb > table.0.0"));
		//assertEquals("bees", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[5]/td[2]/table.0.0"));
		//assertEquals("Training Location", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[5]/td[2]/table.0.0"));

		//assertEquals("Hospital/CHA/Facility", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[3]/td[2]/table.0.0"));
		assertEquals("professionals", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[5]/td[2]/table.0.0"));
		assertEquals("Type I Diabetes Mellitus", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[6]/td[2]/table.0.0"));
		assertEquals("history", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[7]/td[2]/table.0.0"));
		//assertEquals("N/A", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[14]/td[2]/table.0.0"));
		selenium.click("link=Investigations");
		selenium.waitForPageToLoad("30000");
                assertEquals("Biopsy : 02/May/2000", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[6]/td[2]/table.0.0"));
        assertEquals("12.0", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[7]/td[2]/table.0.0"));
        assertEquals("01/Jan/1998", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[8]/td[2]/table.0.0"));
        assertEquals("1200", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[9]/td[2]/table.0.0"));
        assertEquals("55.0", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[3]/td[2]/table.0.0"));
        assertEquals("01/Jan/1998", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[4]/td[2]/table.0.0"));
        assertEquals("66.0", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[10]/td[2]/table.0.0"));
        assertEquals("02/Feb/1999", selenium.getTable("//table[@id='ppflowchart']/tbody/tr[5]/td[2]/table.0.0"));
		/*selenium.click("id=examLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("170.0", selenium.getTable("css=td.column-layer_lightb > table.0.0"));
		assertEquals("3.0", selenium.getTable("css=td.column-layer_darkb > table.0.0"));
		assertEquals("100.0", selenium.getTable("//table[@id='examflowchart']/tbody/tr[4]/td[2]/table.0.0"));
		assertEquals("54", selenium.getTable("//table[@id='examflowchart']/tbody/tr[5]/td[2]/table.0.0"));
		assertEquals("65", selenium.getTable("//table[@id='examflowchart']/tbody/tr[6]/td[2]/table.0.0"));
		assertEquals("65", selenium.getTable("//table[@id='examflowchart']/tbody/tr[7]/td[2]/table.0.0"));
		*/selenium.click("id=bradenLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("13", selenium.getTable("//table[@id='bradenflowchart']/tbody/tr[8]/td[2]/table.0.0"));
		selenium.click("id=limbLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("Leg a/k", selenium.getTable("css=td.column-layer_lightb > table.0.0"));
		assertEquals("Leg a/k", selenium.getTable("css=td.column-layer_darkb > table.0.0"));
		assertEquals("Flesh tone", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[4]/td[2]/table.0.0"));
		assertEquals("Red", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[5]/td[2]/table.0.0"));
		assertEquals("Bluish-purple", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[6]/td[2]/table.0.0"));
		assertEquals("Flesh tone", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[7]/td[2]/table.0.0"));
		assertEquals("Flesh tone", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[8]/td[2]/table.0.0"));
		assertEquals("Black", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[9]/td[2]/table.0.0"));
		assertEquals("Warm", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[10]/td[2]/table.0.0"));
		assertEquals("Warm", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[11]/td[2]/table.0.0"));
		assertEquals("Cool", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[12]/td[2]/table.0.0"));
		assertEquals("Warm", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[13]/td[2]/table.0.0"));
		assertEquals("Cool", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[14]/td[2]/table.0.0"));
		assertEquals("Warm", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[15]/td[2]/table.0.0"));
		assertEquals("Diminished", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[16]/td[2]/table.0.0"));
		assertEquals("Diminished", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[17]/td[2]/table.0.0"));
		assertEquals("Diminished", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[18]/td[2]/table.0.0"));
		assertEquals("Diminished", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[19]/td[2]/table.0.0"));
		assertEquals("Yes", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[20]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[21]/td[2]/table.0.0"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[24]/td[2]/table.0.0"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[25]/td[2]/table.0.0"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[26]/td[2]/table.0.0"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[27]/td[2]/table.0.0"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[28]/td[2]/table.0.0"));
		assertEquals("Normal", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[29]/td[2]/table.0.0"));
		assertEquals("Up to midcalf", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[30]/td[2]/table.0.0"));
		assertEquals("Up to midcalf", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[31]/td[2]/table.0.0"));
		assertEquals("+2 Moderate 4mm", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[32]/td[2]/table.0.0"));
		assertEquals("+2 Moderate 4mm", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[33]/td[2]/table.0.0"));
		assertEquals("3.4", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[35]/td[2]/table.0.0"));
		assertEquals("3.3", selenium.getTable("//table[@id='limbflowchart']/tbody/tr[36]/td[2]/table.0.0"));
		selenium.click("id=footLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("Partial", selenium.getTable("//table[@id='footflowchart']/tbody/tr[2]/td[2]/table.0.0"));
		assertEquals("Unsteady", selenium.getTable("//table[@id='footflowchart']/tbody/tr[3]/td[2]/table.0.0"));
		assertEquals("Crutches", selenium.getTable("//table[@id='footflowchart']/tbody/tr[6]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='footflowchart']/tbody/tr[11]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='footflowchart']/tbody/tr[13]/td[2]/table.0.0"));
	
		assertEquals("Normal", selenium.getTable("//table[@id='footflowchart']/tbody/tr[17]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='footflowchart']/tbody/tr[19]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='footflowchart']/tbody/tr[20]/td[2]/table.0.0"));
		assertEquals("Normal ROM", selenium.getTable("//table[@id='footflowchart']/tbody/tr[21]/td[2]/table.0.0"));
		assertEquals("Normal ROM", selenium.getTable("//table[@id='footflowchart']/tbody/tr[22]/td[2]/table.0.0"));
		assertEquals("Normal ROM, Normal strength", selenium.getTable("//table[@id='footflowchart']/tbody/tr[23]/td[2]/table.0.0"));
		assertEquals("Normal ROM, Normal strength", selenium.getTable("//table[@id='footflowchart']/tbody/tr[24]/td[2]/table.0.0"));
		assertEquals("Normal strength", selenium.getTable("//table[@id='footflowchart']/tbody/tr[25]/td[2]/table.0.0"));
		assertEquals("Normal strength", selenium.getTable("//table[@id='footflowchart']/tbody/tr[26]/td[2]/table.0.0"));
		assertEquals("ftt22", selenium.getTable("//table[@id='footflowchart']/tbody/tr[29]/td[2]/table.0.0"));
		selenium.select("css=select[name=\"wound_profile_type_id\"]", "label=Head/neck (Anterior) - Wound");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=woundLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("Head/neck (Anterior)", selenium.getTable("css=td.column-layer_darkb > table.0.0"));
		assertEquals("Wound A: Internal", selenium.getTable("//table[@id='wpflowchart']/tbody/tr[6]/td[2]/table.0.0"));
		assertEquals("Wound A: Neuropathic/Diabetic", selenium.getTable("//table[@id='wpflowchart']/tbody/tr[7]/td[2]/table.0.0"));
		assertEquals("Wound A: Heal wound", selenium.getTable("//table[@id='wpflowchart']/tbody/tr[8]/td[2]/table.0.0"));
		selenium.click("id=assessmentLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("01/Apr/2005", selenium.getTable("//table[@id='aflowchart']/tbody/tr[7]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='aflowchart']/tbody/tr[11]/td[2]/table.0.0"));
		assertEquals("3", selenium.getTable("//table[@id='aflowchart']/tbody/tr[13]/td[2]/table.0.0"));
		assertEquals("pain comments2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[12]/td[2]/table.0.0"));
		assertEquals("3.0", selenium.getTable("//table[@id='aflowchart']/tbody/tr[14]/td[2]/table.0.0"));
		assertEquals("3.0", selenium.getTable("//table[@id='aflowchart']/tbody/tr[15]/td[2]/table.0.0"));
		assertEquals("2.0", selenium.getTable("//table[@id='aflowchart']/tbody/tr[16]/td[2]/table.0.0"));
		assertEquals("#1 3:00 to 3:00", selenium.getTable("//table[@id='aflowchart']/tbody/tr[17]/td[2]/table.0.0"));
		assertEquals("#1 1.2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[18]/td[2]/table.0.0"));
		assertEquals("#1 2:00", selenium.getTable("//table[@id='aflowchart']/tbody/tr[19]/td[2]/table.0.0"));
		assertEquals("#1 6.1", selenium.getTable("//table[@id='aflowchart']/tbody/tr[20]/td[2]/table.0.0"));
		assertEquals("Fistula #1: fistula comment 223", selenium.getTable("//table[@id='aflowchart']/tbody/tr[21]/td[2]/table.0.0"));
		assertEquals("Fully epithelialized: 50%, No open wound: 50%", selenium.getTable("//table[@id='aflowchart']/tbody/tr[22]/td[2]/table.0.0"));
		assertEquals("Not assessed", selenium.getTable("//table[@id='aflowchart']/tbody/tr[23]/td[2]/table.0.0"));
		assertEquals("Not assessed", selenium.getTable("//table[@id='aflowchart']/tbody/tr[23]/td[2]/table.0.0"));
		assertEquals("Scant", selenium.getTable("//table[@id='aflowchart']/tbody/tr[24]/td[2]/table.0.0"));
		assertEquals("Yes", selenium.getTable("//table[@id='aflowchart']/tbody/tr[25]/td[2]/table.0.0"));
		assertEquals("Diffuse", selenium.getTable("//table[@id='aflowchart']/tbody/tr[26]/td[2]/table.0.0"));
		assertEquals("Diffuse", selenium.getTable("//table[@id='aflowchart']/tbody/tr[26]/td[2]/table.0.0"));
		assertEquals("Indurated < 2cm, Intact", selenium.getTable("//table[@id='aflowchart']/tbody/tr[27]/td[2]/table.0.0"));
		// tr>
		// 	<td>verifyTable</td>
		// 	<td>//table[@id='aflowchart']/tbody/tr[26]/td[2]/table.0.0</td>
		// 	<td>10</td>
		// </tr
		//assertEquals("1 10cc syringe 1 Actisorb Silver 220 6.5cm x 9.5cm", selenium.getTable("//table[@id='aflowchart']/tbody/tr[27]/td[2]/table.0.0"));
		assertEquals("tsc2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[32]/td[2]/table.0.0"));
		assertEquals("21", selenium.getTable("//table[@id='aflowchart']/tbody/tr[31]/td[2]/table.0.0"));
		assertEquals("02/Feb/1999", selenium.getTable("//table[@id='aflowchart']/tbody/tr[42]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='aflowchart']/tbody/tr[33]/td[2]/table.0.0"));
		assertEquals("07/Jan/2000", selenium.getTable("//table[@id='aflowchart']/tbody/tr[48]/td[2]/table.0.0"));
		assertEquals("Constant", selenium.getTable("//table[@id='aflowchart']/tbody/tr[45]/td[2]/table.0.0"));
		assertEquals("75 mm/hg", selenium.getTable("//table[@id='aflowchart']/tbody/tr[44]/td[2]/table.0.0"));

		assertEquals("Acute", selenium.getTable("//table[@id='aflowchart']/tbody/tr[37]/td[2]/table.0.0"));
		assertEquals("Prep for STSG", selenium.getTable("//table[@id='aflowchart']/tbody/tr[43]/td[2]/table.0.0"));
		assertEquals("Wound Closure", selenium.getTable("//table[@id='aflowchart']/tbody/tr[47]/td[2]/table.0.0"));
		assertEquals("MRSA, VRE", selenium.getTable("//table[@id='aflowchart']/tbody/tr[34]/td[2]/table.0.0"));
		//assertEquals("Wound A: test : 1/Jan/2000 to 4/Jan/2000", selenium.getTable("//table[@id='aflowchart']/tbody/tr[33]/td[2]/table.0.0"));
		assertEquals("Laser, Massage", selenium.getTable("//table[@id='aflowchart']/tbody/tr[36]/td[2]/table.0.0"));
		assertEquals("0 Attached Images", selenium.getTable("//table[@id='aflowchart']/tbody/tr[49]/td[2]/table.0.0"));
	
		selenium.click("id=measurementLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("8.3", selenium.getTable("//div[@id='Dates_table']/table/tbody/tr/td[3]/table.1.0"));
		assertEquals("6.3", selenium.getTable("//div[@id='Dates_table']/table/tbody/tr/td[4]/table.1.0"));
		assertEquals("1.2", selenium.getTable("//div[@id='Dates_table']/table/tbody/tr/td[5]/table.1.0"));
		selenium.click("id=historyLink");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=ncpLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("TID", selenium.getText("css=label.BODY"));
		assertEquals("Visit Frequency", selenium.getText("css=label.leftLabel.BOLD"));
		selenium.select("css=select[name=\"wound_profile_type_id\"]", "label=Abdomen - Ostomy");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=woundLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("Urostomy: Ulcerative Colitis", selenium.getTable("//table[@id='wpflowchart']/tbody/tr[7]/td[2]/table.0.0"));
		assertEquals("Urostomy: Temporary ostomy", selenium.getTable("//table[@id='wpflowchart']/tbody/tr[8]/td[2]/table.0.0"));
		assertEquals("Abdomen", selenium.getTable("css=td.column-layer_darkb > table.0.0"));
		selenium.click("id=assessmentLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("On", selenium.getTable("//table[@id='aflowchart']/tbody/tr[9]/td[2]/table.0.0"));
		assertEquals("Obstruction", selenium.getTable("//table[@id='aflowchart']/tbody/tr[12]/td[2]/table.0.0"));
		assertEquals("Oblong", selenium.getTable("//table[@id='aflowchart']/tbody/tr[13]/td[2]/table.0.0"));
		assertEquals("0", selenium.getTable("//table[@id='aflowchart']/tbody/tr[14]/td[2]/table.0.0"));
		assertEquals("0", selenium.getTable("//table[@id='aflowchart']/tbody/tr[15]/td[2]/table.0.0"));
		assertEquals("3", selenium.getTable("id=aflowchart.15.1"));
		assertEquals("Retracted", selenium.getTable("//table[@id='aflowchart']/tbody/tr[17]/td[2]/table.0.0"));
		assertEquals("Pink, Red", selenium.getTable("//table[@id='aflowchart']/tbody/tr[18]/td[2]/table.0.0"));
		assertEquals("Cognitive impairment", selenium.getTable("//table[@id='aflowchart']/tbody/tr[19]/td[2]/table.0.0"));
		assertEquals("Distended, Rounded", selenium.getTable("//table[@id='aflowchart']/tbody/tr[20]/td[2]/table.0.0"));
		assertEquals("Belt line", selenium.getTable("//table[@id='aflowchart']/tbody/tr[21]/td[2]/table.0.0"));
		assertEquals("Approximated", selenium.getTable("//table[@id='aflowchart']/tbody/tr[22]/td[2]/table.0.0"));
		assertEquals("Excoriated, Bruised", selenium.getTable("//table[@id='aflowchart']/tbody/tr[26]/td[2]/table.0.0"));
		assertEquals("testest", selenium.getTable("//table[@id='aflowchart']/tbody/tr[25]/td[2]/table.0.0"));
		assertEquals("Not assessed", selenium.getTable("//table[@id='aflowchart']/tbody/tr[28]/td[2]/table.0.0"));
		assertEquals("Rash, Intact", selenium.getTable("//table[@id='aflowchart']/tbody/tr[29]/td[2]/table.0.0"));
		assertEquals("test2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[30]/td[2]/table.0.0"));
		assertEquals("Red", selenium.getTable("//table[@id='aflowchart']/tbody/tr[31]/td[2]/table.0.0"));
		assertEquals("Moderate", selenium.getTable("//table[@id='aflowchart']/tbody/tr[33]/td[2]/table.0.0"));
		assertEquals("TPN", selenium.getTable("//table[@id='aflowchart']/tbody/tr[37]/td[2]/table.0.0"));
		assertEquals("stat", selenium.getTable("//table[@id='aflowchart']/tbody/tr[38]/td[2]/table.0.0"));
		assertEquals("Changed - routine", selenium.getTable("//table[@id='aflowchart']/tbody/tr[39]/td[2]/table.0.0"));
		assertEquals("puch4", selenium.getTable("//table[@id='aflowchart']/tbody/tr[40]/td[2]/table.0.0"));
		assertEquals("Viewed stoma, UOA visitor", selenium.getTable("//table[@id='aflowchart']/tbody/tr[42]/td[2]/table.0.0"));
		assertEquals("test2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[41]/td[2]/table.0.0"));
		assertEquals("MRSA, No Growth", selenium.getTable("//table[@id='aflowchart']/tbody/tr[46]/td[2]/table.0.0"));
		//assertEquals("No", selenium.getTable("//table[@id='aflowchart']/tbody/tr[55]/td[2]/table.0.0"));
		assertEquals("1 Acticoat 10cm x 10cm\n 1 Acticoat 10cm x 20cm\n 1 Actisorb Silver 200 10.5cm x 10.5cm", selenium.getTable("//table[@id='aflowchart']/tbody/tr[43]/td[2]/table.0.0"));
		selenium.select("css=select[name=\"wound_profile_type_id\"]", "label=Chest - Tube/Drain");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=woundLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("Chest", selenium.getTable("css=td.column-layer_darkb > table.0.0"));
		assertEquals("Incision 1: Gastrointestinal,Tube/Drain 1: Obstruction", selenium.getTable("//table[@id='wpflowchart']/tbody/tr[7]/td[2]/table.0.0"));
		assertEquals("Incision 1: Heal incision,Tube/Drain 1: Temporary tube/drain", selenium.getTable("//table[@id='wpflowchart']/tbody/tr[8]/td[2]/table.0.0"));
		selenium.click("id=assessmentLink");
		selenium.waitForPageToLoad("30000");
		assertEquals("Full Assessment", selenium.getTable("css=td.column-layer_lightb > table.0.0"));
		assertEquals("Hemovac drain", selenium.getTable("//table[@id='aflowchart']/tbody/tr[10]/td[2]/table.0.0"));
		assertEquals("test2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[11]/td[2]/table.0.0"));
		assertEquals("Yes", selenium.getTable("//table[@id='aflowchart']/tbody/tr[12]/td[2]/table.0.0"));
		assertEquals("Yes", selenium.getTable("//table[@id='aflowchart']/tbody/tr[12]/td[2]/table.0.0"));
		assertEquals("Intact, Eroded", selenium.getTable("//table[@id='aflowchart']/tbody/tr[14]/td[2]/table.0.0"));
		assertEquals("2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[13]/td[2]/table.0.0"));
		assertEquals("comments2", selenium.getTable("//table[@id='aflowchart']/tbody/tr[15]/td[2]/table.0.0"));
		assertTrue(selenium.isTextPresent("1, 2"));
		assertTrue(selenium.isTextPresent("2, 1"));
		assertEquals("1:00 to 1:00, 1:00 to 1:00", selenium.getTable("//table[@id='aflowchart']/tbody/tr[19]/td[2]/table.0.0"));
		assertEquals("Not applicable, Not applicable", selenium.getTable("//table[@id='aflowchart']/tbody/tr[20]/td[2]/table.0.0"));
		assertEquals("01/Mar/1999", selenium.getTable("//table[@id='aflowchart']/tbody/tr[21]/td[2]/table.0.0"));
		assertEquals("Intact, Indurated < 2cm", selenium.getTable("//table[@id='aflowchart']/tbody/tr[22]/td[2]/table.0.0"));
		assertEquals("1 Acticoat 10cm x 10cm", selenium.getTable("//table[@id='aflowchart']/tbody/tr[23]/td[2]/table.0.0"));
		assertEquals("No", selenium.getTable("//table[@id='aflowchart']/tbody/tr[25]/td[2]/table.0.0"));
		assertEquals("No Growth, MRSA", selenium.getTable("//table[@id='aflowchart']/tbody/tr[26]/td[2]/table.0.0"));
		assertEquals("0 Attached Images", selenium.getTable("//table[@id='aflowchart']/tbody/tr[34]/td[2]/table.0.0"));
		assertEquals("Yes", selenium.getTable("//table[@id='aflowchart']/tbody/tr[35]/td[2]/table.0.0"));
		
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
