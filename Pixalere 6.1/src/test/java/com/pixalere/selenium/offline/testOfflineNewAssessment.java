package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testOfflineNewAssessment extends SeleneseTestBase {
private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
	}

	@Test
	public void testTestOfflineNewAssessment() throws Exception {
		selenium.open("/offline/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=New Wound Profile");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=anterior", "label=Shoulder");
		selenium.dragAndDropToObject("id=dragA", "id=blueman_grid");
		selenium.select("id=etiology_alphas", "label=Wound A");
		selenium.select("id=etiology_list", "label=Neuropathic/Diabetic");
		selenium.click("id=addetiology");
		selenium.select("id=goals_alphas", "label=Wound A");
		selenium.select("id=goals_list", "label=Heal wound");
		selenium.click("id=addgoals");
                selenium.select("id=wound_acquired_alphas", "label=Wound A");
		selenium.select("id=wound_acquired_list", "label=Internal");
		selenium.click("id=addwound_acquired");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_day", "label=1");
		selenium.select("id=wound_month", "label=Jan");
		selenium.type("id=wound_year", "1999");
		selenium.click("id=recurrent2");
		selenium.select("id=pain", "label=1");
		selenium.type("id=pain_comments", "pain");
		selenium.select("id=length", "label=14");
		selenium.select("id=width", "label=11");
		selenium.select("id=depth", "label=4");
		selenium.select("id=num_under", "label=1");
		selenium.click("id=oku");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=undermining_start_1", "label=8:00");
		selenium.select("id=undermining_end_1", "label=5:00");
		selenium.select("id=underminingdepth_1", "label=11");
		selenium.select("id=num_sinus_tracts", "label=1");
		selenium.click("id=oks");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=sinustract_start_1", "label=10:00");
		selenium.select("id=sinusdepth_1", "label=12");
		selenium.select("id=num_fistulas", "label=1");
		selenium.click("id=okf");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=fistulas_1", "tester");
		selenium.click("id=exudate_type4");
		selenium.click("id=exudate_amount_4");
		selenium.click("id=wound_edge7");
		selenium.click("id=periwound3");
		selenium.click("id=woundbase2");
		selenium.select("id=woundbase_percentage_2", "label=100%");
		selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=products", "label=Acticoat 10cm x 10cm");
		selenium.click("id=addAlpha");
		selenium.select("id=dressing_change_frequency_alphas", "label=Wound A");
		selenium.select("id=dressing_change_frequency_list", "label=BID");
		selenium.click("id=adddressing_change_frequency");
		selenium.select("id=visit_freq", "label=1x weekly");
		selenium.click("css=option[value=\"Wound A\"]");
		selenium.type("id=bandages_out18", "1");
		selenium.type("id=bandages_in18", "1");
		selenium.select("id=vacstart1_day", "label=18");
		selenium.select("id=vacend1_day", "label=14");
		selenium.select("id=vacend1_month", "label=Nov");
		selenium.select("id=vacstart1_month", "label=Mar");
		selenium.type("id=vacstart1_year", "1999");
		selenium.type("id=vacend1_year", "1999");
		selenium.select("id=pressure_reading1", "label=50 mm/hg");
		selenium.click("id=therapy_setting1_1");
		selenium.click("id=cs_done1");
		selenium.select("id=csresult_alphas", "label=Wound A");
		selenium.select("id=csresult_list", "label=MRSA");
		selenium.click("id=addcsresult");

                selenium.click("id=review_done");
		selenium.click("id=adjunctive4");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("name=referral_wound");
		selenium.select("id=priority_wound", "label=Clinical Review (1 - 7 days)");
                selenium.type("name=body_referral_wound", "commentsoff2");
		
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
