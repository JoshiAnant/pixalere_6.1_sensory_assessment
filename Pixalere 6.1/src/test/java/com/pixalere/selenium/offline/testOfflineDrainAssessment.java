package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testOfflineDrainAssessment extends SeleneseTestBase {
private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
    private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
	}

	@Test
	public void testTestOfflineDrainAssessment() throws Exception {
		selenium.open("/offline/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navWound > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=wound_id", "label=Chest");
		selenium.waitForPageToLoad("30000");
		selenium.click("css=#navAssessment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=assessment_type1");
		selenium.click("id=populate1");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=pain");
		selenium.select("id=pain", "label=4");
		selenium.click("css=option[value=\"1117\"]");
		selenium.click("id=exudate_amount_4");
		selenium.click("id=bgbtntd1");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=assessment_type1");
		selenium.select("id=pain", "label=3");
		selenium.type("id=pain_comments", "comments");
		selenium.select("id=type_of_drain", "label=Jackson Pratt drain");
		selenium.type("id=type_of_drain_other", "test");
		selenium.select("id=tubes_changed_date_day", "label=1");
		selenium.select("id=tubes_changed_date_month", "label=Jan");
		selenium.type("id=tubes_changed_date_year", "1999");
		selenium.click("id=sutured3");
                
                selenium.click("id=drain_characteristics_panel_uiicon");
		selenium.type("id=drainage_amount_mls1", "1");
		selenium.type("name=drainage_amount_hrs1", "2");
                
		selenium.select("id=drainage_amount_start1", "label=1:00");
		selenium.select("id=drainage_amount_end1", "label=1:00");
		selenium.click("id=drain_characteristics11");
		selenium.click("id=drain_site2");
		selenium.click("id=peri_drain_area1");
		selenium.select("id=drainage_num", "label=1");
		selenium.click("id=oku");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=additional_days_date2_day", "label=1");
		selenium.select("id=additional_days_date2_month", "label=Jan");
		selenium.type("id=additional_days_date2_year", "2000");
		selenium.type("id=drainage_amount_mls2", "2");
		selenium.type("name=drainage_amount_hrs2", "1");
		selenium.select("id=drainage_amount_start2", "label=1:00");
		selenium.select("id=drainage_amount_end2", "label=1:00");
		selenium.click("id=drain_characteristics21");
		selenium.type("id=pain_comments", "pain comments");
		selenium.click("css=#navTreatment > img[alt=\"spacer\"]");
		selenium.waitForPageToLoad("30000");
                java.io.File scrFile34 = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
                org.apache.commons.io.FileUtils.copyFile(scrFile34, new java.io.File("c:\\test13.jpg"));
                selenium.click("id=review_done");
		selenium.select("id=products", "label=30cc syringe");
		selenium.click("css=option[value=\"940\"]");
		selenium.select("name=wound_ass", "label=Each");
		selenium.click("id=addAlpha");
		selenium.click("id=adjunctive4");
		selenium.click("css=#navSummary > img[alt=\"spacer\"]");
                
                /*verifyTrue(selenium.isTextPresent("No product or product quantities were changed, if this is okay click"));
                selenium.click("id=go_confirm");*/
		selenium.waitForPageToLoad("30000");
		selenium.click("name=referral_incision");
		selenium.select("id=priority_incision", "label=Urgent (24 - 48hrs)");
		selenium.click("name=referral_drain");
		selenium.select("id=priority_drain", "label=Urgent (24 - 48hrs)");
                selenium.type("name=body_referral_drain", "commentsoff");
		selenium.type("name=body_referral_incision", "commentsoff");
		
		selenium.click("id=saveEntries");
		selenium.waitForPageToLoad("30000");
		verifyTrue(selenium.isTextPresent("Your work has been successfully saved."));
		selenium.click("xpath=(//button[@type='button'])[3]");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
