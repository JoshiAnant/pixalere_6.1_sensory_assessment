package com.pixalere.selenium.offline;

import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;

public class testOfflineUpload  extends SeleneseTestBase{
    private DefaultSelenium selenium;
	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("localhost", 4444, "*chrome", "http://localhost:8080/");
		selenium.start();
	}

	@Test
	public void testTestOfflineUpload() throws Exception {
		selenium.open("/offline/LoginSetup.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("xpath=(//input[@id='offlineoption'])[2]");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=uploadlist", "label=ID: 2");
		selenium.click("id=button1");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
