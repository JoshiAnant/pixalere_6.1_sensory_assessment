package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testProfessionalAdmin  extends SeleneseTestBase{
        private DefaultSelenium selenium;
        private String browser = System.getProperty("selenium.browser");
	 private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestProfessionalAdmin() throws Exception {
		selenium.open("/pixalere/logout.do");
                
                driver.manage().window().maximize();
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.click("css=#navAdmin > img.toolbarSpacer");
		selenium.waitForPageToLoad("30000");
		selenium.select("name=pixActionMenu", "label=Professional Accounts");
		selenium.click("id=adminMenu");
		selenium.waitForPageToLoad("30000");
		selenium.select("id=position", "label=RN");
		selenium.type("name=first_name", "Jonathan");
                selenium.type("name=pager","11111111");
		selenium.type("name=employeeid", "666");
		selenium.type("name=last_name", "Burns");
		selenium.type("name=user_name", "jburns");
		selenium.type("id=password", "skywalker77");
		selenium.type("id=password2", "skywalker77");
		selenium.select("name=region", "label=Sample Location");
		selenium.click("id=addTreatprofessional_form");
		selenium.click("name=access_allpatients");
		selenium.click("name=access_admin_patients");
		selenium.click("name=access_uploader");
		selenium.click("name=access_viewer");
		selenium.click("name=access_admin");
		selenium.click("id=addsubmit");
		selenium.waitForPageToLoad("300000");
		assertTrue(selenium.isTextPresent("Jonathan Burns"));
                
                selenium.click("id=ui-id-3");
		selenium.type("name=id", "9");
		selenium.click("name=searchAdmin");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=profEdit9");
		selenium.waitForPageToLoad("30000");

                selenium.windowMaximize();
                selenium.type("id=editpager","6047110440");
                selenium.select("document.forms['professional_edit_form'].elements['region']", "label=Sample Location");
		selenium.click("id=addTreatprofessional_edit_form");
                selenium.getEval("window.scrollTo(0,700)");

		selenium.click("id=editsubmit");
          
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
