package com.pixalere.selenium;

import com.thoughtworks.selenium.*;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class testEditFootAssessment   extends SeleneseTestBase{
        private DefaultSelenium selenium;
	private String browser = System.getProperty("selenium.browser");
   private WebDriver driver = null;
	@Before
	public void setUp() throws Exception {
                if(browser.equals("*chrome")){driver=new FirefoxDriver();}
                else if(browser.equals("*iexplore")){System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");driver=new InternetExplorerDriver();}
                else if(browser.equals("*googlechrome")){System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");driver=new ChromeDriver();}
                 selenium = new WebDriverBackedSelenium(driver, "http://localhost:8080/");
		//selenium = new DefaultSelenium("localhost", 4444, browser, "http://localhost:8080/");
		//selenium.start();
	}

	@Test
	public void testTestEditFootAssessment() throws Exception {
		selenium.setSpeed("550");
		selenium.open("/pixalere/logout.do");
		selenium.type("id=userId", "7");
		selenium.type("id=password", "fat.burning2@");
		selenium.click("name=submit");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=btnclose");
		selenium.type("id=search", "1");
		selenium.click("name=OK");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Mobility Flowchart");
		selenium.waitForPageToLoad("30000");
		selenium.click("id=editProfile1-403");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=weight_bearing", "label=Partial");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-304");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=balance", "label=Unsteady");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-305");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=calf_muscle_pump", "label=Impaired");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-307");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=calf_muscle_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-306");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=mobility_aids", "label=Crutches");
	
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-308");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=mobility_aids_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-309");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=gait_pattern_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-310");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=walking_distance_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-311");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=walking_endurance_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-312");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("xpath=(//input[@name='indoor_footwear'])[2]");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-315");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=indoor_footwear_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-313");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("xpath=(//input[@name='outdoor_footwear'])[2]");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-316");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=outdoor_footwear_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-314");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("xpath=(//input[@name='orthotics'])[2]");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-317");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=orthotics_comments", "comments");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-318");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=muscle_tone_functional", "label=Normal");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-319");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");

		selenium.select("id=arches_foot_structure", "label=Normal");

		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-320");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("xpath=(//input[@name='supination_foot_structure'])[2]");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-321");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("xpath=(//input[@name='pronation_foot_structure'])[2]");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-322");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=dorsiflexion_active3");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-323");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=dorsiflexion_passive3");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-324");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=plantarflexion_active2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-325");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=plantarflexion_passive2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-326");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=greattoe_active1");
		selenium.click("id=greattoe_active2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-327");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.click("id=greattoe_passive1");
		selenium.click("id=greattoe_passive2");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		selenium.click("id=editProfile1-487");
		selenium.waitForPopUp("large_fix_win", "60000");
		selenium.selectWindow("name=large_fix_win");
		selenium.type("name=foot_comments", "ftt22");
		selenium.click("id=submitbutton");
		selenium.waitForPageToLoad("30000");
		selenium.selectWindow("null");
		assertEquals("Impaired Normal", selenium.getTable("//table[@id='footflowchart']/tbody/tr[4]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[5]/td[2]/table.0.1"));
		assertEquals("Crutches Wheelchair", selenium.getTable("//table[@id='footflowchart']/tbody/tr[6]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[8]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[10]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[9]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[10]/td[2]/table.0.1"));
		assertEquals("No Yes", selenium.getTable("//table[@id='footflowchart']/tbody/tr[11]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[12]/td[2]/table.0.1"));
		assertEquals("No Yes", selenium.getTable("//table[@id='footflowchart']/tbody/tr[13]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[14]/td[2]/table.0.1"));
		assertEquals("No Yes", selenium.getTable("//table[@id='footflowchart']/tbody/tr[15]/td[2]/table.0.1"));
		assertEquals("comments ...", selenium.getTable("//table[@id='footflowchart']/tbody/tr[16]/td[2]/table.0.1"));
		assertEquals("Normal High", selenium.getTable("//table[@id='footflowchart']/tbody/tr[17]/td[2]/table.0.1"));
		assertEquals("Normal High", selenium.getTable("//table[@id='footflowchart']/tbody/tr[18]/td[2]/table.0.1"));
		assertEquals("No Yes", selenium.getTable("//table[@id='footflowchart']/tbody/tr[19]/td[2]/table.0.1"));
		assertEquals("No Yes", selenium.getTable("//table[@id='footflowchart']/tbody/tr[20]/td[2]/table.0.1"));
		assertEquals("Normal ROM Normal ROM, Decrd ROM", selenium.getTable("//table[@id='footflowchart']/tbody/tr[21]/td[2]/table.0.1"));
		assertEquals("Normal ROM Normal ROM, Decrd strength", selenium.getTable("//table[@id='footflowchart']/tbody/tr[22]/td[2]/table.0.1"));
		assertEquals("Normal ROM, Normal strength Normal ROM", selenium.getTable("//table[@id='footflowchart']/tbody/tr[23]/td[2]/table.0.1"));
		assertEquals("Normal ROM, Normal strength Normal ROM", selenium.getTable("//table[@id='footflowchart']/tbody/tr[24]/td[2]/table.0.1"));
		assertEquals("Normal strength Normal ROM", selenium.getTable("//table[@id='footflowchart']/tbody/tr[25]/td[2]/table.0.1"));
		assertEquals("Normal strength Normal ROM", selenium.getTable("//table[@id='footflowchart']/tbody/tr[26]/td[2]/table.0.1"));
		assertEquals("ftt22 ftt", selenium.getTable("//table[@id='footflowchart']/tbody/tr[29]/td[2]/table.0.1"));
		selenium.click("id=logout");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
