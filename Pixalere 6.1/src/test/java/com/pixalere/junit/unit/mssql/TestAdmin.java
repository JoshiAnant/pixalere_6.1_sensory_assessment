
/*
 * Copyright (c) 2005 WebMed Technology Inc. All Rights Reserved.
 */
package com.pixalere.junit.unit.mssql;

import com.pixalere.common.bean.ProductsVO;
import com.pixalere.common.bean.ProductCategoryVO;
import java.util.Vector;
import java.util.Collection;
import junit.framework.TestCase;
import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.admin.service.ResourcesServiceImpl;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.common.bean.LookupLocalizationVO;
import com.pixalere.admin.bean.LogAuditVO;
import com.pixalere.admin.service.LogAuditServiceImpl;
import com.pixalere.ConstantsTest;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.admin.dao.LogAuditDAO;
import com.pixalere.common.*;
import com.pixalere.common.dao.ProductCategoryDAO;
import com.pixalere.common.dao.ProductsDAO;

/**
 * Created by IntelliJ IDEA. User: travismorris Date: Jul 4, 2005 Time: 11:17:15
 * PM To change this template use File | Settings | File Templates.
 */
public class TestAdmin extends TestCase {

    private static int list_id = 0;
    private static LookupVO list1;
    private static int category_id = 0;
    private static LogAuditVO audit1;
    private static int log_id = 0;

    public void testAddList() {
        ListServiceImpl bd = new ListServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        ListsDAO dao = new ListsDAO();
        list1 = new LookupVO();
        LookupLocalizationVO l1 = new LookupLocalizationVO();
        l1.setName("test_item");l1.setLanguage_id(1);
        Collection<LookupLocalizationVO> c1 = new java.util.ArrayList();
        c1.add(l1);
        list1.setNames(c1);
        list1.setActive(new Integer(1));
        list1.setResourceId(new Integer(1));
        list1.setOther(new Integer(0));
        try {
            bd.saveListItem(list1);
            list1.setId(null);
            list1.setResourceId(new Integer(LookupVO.REFERRAL_SOURCE));
            bd.saveListItem(list1);
            list1.setId(null);
            list1.setCategoryId(991);
            list1.setResourceId(new Integer(LookupVO.TREATMENT_LOCATION[0]));
            bd.saveListItem(list1);
            list1.setCategoryId(0);
            list1.setId(null);
            list1.setResourceId(new Integer(LookupVO.FUNDING_SOURCE));
            bd.saveListItem(list1);
            list1.setId(null);
            list1.setResourceId(new Integer(LookupVO.PATIENT_RESIDENCE));
            bd.saveListItem(list1);
            list1.setId(null);
            list1.setResourceId(new Integer(LookupVO.POSITION));
            LookupLocalizationVO l2 = new LookupLocalizationVO();
        l2.setName("LRN");l2.setLanguage_id(1);
        Collection<LookupLocalizationVO> c2 = new java.util.ArrayList();
        c2.add(l2);
        list1.setNames(c2);
            list1.setActive(new Integer(1));
            list1.setOther(new Integer(0));
            bd.saveListItem(list1);
            
            list1.setId(null);
            LookupLocalizationVO l3 = new LookupLocalizationVO();
        l3.setName("test_item");l3.setLanguage_id(1);
        Collection<LookupLocalizationVO> c3 = new java.util.ArrayList();
        c3.add(l3);
        list1.setNames(c3);
            list1.setActive(new Integer(1));
            list1.setResourceId(new Integer(LookupVO.WOUND_BED_PERCENT));
            bd.saveListItem(list1);
            //1660
            LookupVO i = bd.getListItem(11);//1736);

            String item = i.getName(1);
            assertEquals("Item should equal GP", item, "GP");

        } catch (ApplicationException ex) {
            this.fail("Error while retrieving inserted record from database." + ex.getMessage());
        }
        bd = new ListServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        
        
        
        try {
            list1.setId(null);
            LookupVO list = (LookupVO) dao.findByCriteria(list1);
            System.out.println(list1.getId()+" "+list1.getName(1)+" LookupVO : "+list);
            list_id = list.getId().intValue();

            boolean bool = false;
            if (list != null) {
                bool = true;
            }
            assertEquals("test_item must equal", list.getName(1), "test_item");
            assertTrue("List should exist", bool);
            bd = new ListServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            bd.saveListItem(list);
        } catch (DataAccessException ex) {
            this.fail("Error while retrieving inserted record from database." + ex.getMessage());
        } catch (ApplicationException ex) {
            this.fail("Error while retrieving inserted record from database." + ex.getMessage());
        }
    }

    /*
     * public void testUpdateList() throws ApplicationException{ ListBD bd=new
     * ListBD("test"); list1.setId(new Integer(list_id));
     * bd.updateItem(list1); LookupVO list=bd.retrieveItem(list_id);
     *
     * boolean bool=false; if(list!=null)bool=true;
     *
     * assertTrue("List should exist",bool);
     *  }
     */
    public void testRemoveList() {
        ListServiceImpl bd = new ListServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        LookupVO vo = new LookupVO();
        vo.setId(new Integer(1681));
        try {
            bd.removeListItem(vo);
            LookupVO list = bd.getListItem(1681);

            boolean bool = false;
            if (list.getActive().equals(new Integer(0))) {
                bool = true;
            }
            assertTrue("List shouldnt exist", bool);
        } catch (ApplicationException ex) {
            this.fail("Error while retrieving inserted record from database." + ex.getMessage());
        }
        

    }

    public void testFindAllListItems() {
        ListServiceImpl bd = new ListServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        int[] res = {LookupVO.WOUND_STATUS};
        try {
            Vector items = bd.getLists(res, null, true);

            assertNotNull("List items should exist", items);
            assertEquals("Should be X items in the list", items.size(), 3);
            LookupVO resources = ((LookupVO) items.get(0));

            //assertEquals("Item should include Category","Funding Source",resources.getValue());

            Vector items2 = bd.getLists(res, null, false);

            assertNotNull("List items should exist", items2);
            assertEquals("Should be X items in the list", items2.size(), 2);
            resources = ((LookupVO) items2.get(0));
            assertEquals("Item should include Category", "Open", resources.getName(1));
        
            Collection items3 =bd.getLists(LookupVO.FUNDING_SOURCE);
            assertNotNull("Collection should not be null",items3);
            
        } catch (ApplicationException ex) {
            this.fail("Error while retrieving inserted record from database." + ex.getMessage());
        }
        
    }

    public void testAddLogAudit() throws DataAccessException {
        LogAuditServiceImpl bd = new LogAuditServiceImpl();
        LogAuditDAO dao = new LogAuditDAO();
        audit1 = new LogAuditVO();
        audit1.setPatientId(new Integer(1));
        audit1.setProfessionalId(new Integer(1));
        audit1.setCreated_on(new java.util.Date());
        try {
            bd.saveLogAudit(audit1);
        } catch (ApplicationException ex) {
        }
        audit1.setId(null);
        LogAuditVO list = (LogAuditVO) dao.findByCriteria(audit1);

        boolean bool = false;
        if (list != null) {
            bool = true;
        }
        assertTrue("LogAudit should exist", bool);
        
    }

    public void testAddProductCategory() throws DataAccessException {
        ProductCategoryDAO dao = new ProductCategoryDAO();
        ProductCategoryVO vo = new ProductCategoryVO();
        vo.setActive(new Integer(1));
        vo.setTitle("testCategory");
        dao.insert(vo);
        vo.setId(null);
        ProductCategoryVO tmp = (ProductCategoryVO) dao.findByCriteria(vo);
        category_id = tmp.getId().intValue();
        boolean bool = false;
        if (tmp != null) {
            bool = true;
        }
        assertTrue("Product Category should exist", bool);
        
    }

    public void testAddProduct() throws DataAccessException {
        ProductsDAO dao = new ProductsDAO();

        ProductsVO vo = new ProductsVO();
        vo.setCategory_id(new Integer(category_id));
        vo.setTitle("testCategory");
        vo.setDescription("test");
        vo.setQuantity("1");
        vo.setType("1");
        vo.setCost(1.0);
        dao.insert(vo);
        vo.setId(null);
        ProductsVO tmp = (ProductsVO) dao.findByCriteria(vo);
        boolean bool = false;
        if (tmp != null) {
            bool = true;
        }
        assertTrue("Product Category should exist", bool);
    //assertEquals("Products Title should equal.", "Blue Pads", com.pixalere.utils.Common.getProductTitleByID(632, "test"));
    }
    public void testGetResource() throws DataAccessException {
        try{
        ResourcesServiceImpl bd = new ResourcesServiceImpl();
        ResourcesVO r = bd.getResource(1);
        assertEquals("Should equals Funding Source",r.getName(),"Funding Source");
        } catch (ApplicationException ex) {
            this.fail("Error while retrieving inserted record from database." + ex.getMessage());
        }
       
        
        try{
        ResourcesServiceImpl bd = new ResourcesServiceImpl();
        Vector v = bd.getAllResources(LookupVO.TREATMENT_LOCATION);
        
        assertNotNull("Vector shouldn't be null",v);
        } catch (ApplicationException ex) {
            this.fail("Error while retrieving inserted record from database." + ex.getMessage());
        }
        
    }
    public void tearDown() {
        list1 = null;
        audit1 = null;
    }
}
