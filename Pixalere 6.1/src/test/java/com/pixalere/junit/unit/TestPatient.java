package com.pixalere.junit.unit;
import java.util.Date;
import junit.framework.TestCase;
import com.pixalere.patient.dao.PatientProfileDAO;
import java.util.List;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Hashtable;
import com.pixalere.admin.dao.AssignPatientsDAO;
import com.pixalere.admin.bean.AssignPatientsVO;
import com.pixalere.admin.service.AssignPatientsServiceImpl;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.guibeans.RowData;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.guibeans.PatientSearchVO;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.utils.*;
import com.pixalere.common.dao.ListsDAO;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.common.bean.LookupVO;

public class TestPatient extends TestCase {

    private PatientAccountVO account1;
    private static PatientProfileVO profile1;
    private PatientProfileVO profile3;
    private static int patient = 0;
    private PatientProfileServiceImpl bd = null;
    private PatientServiceImpl pbd = null;
    private PatientProfileDAO dao = null;
    int referral = 0;
    int funding = 0;
    int patientres = 0;
    int gender = 0;
    int patient_care = 0;

    protected void setUp() {

        bd = new PatientProfileServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        pbd = new PatientServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        dao = new PatientProfileDAO();


        ListsDAO listBD = new ListsDAO();
        LookupVO tmpList = null;
        try {
            LookupVO list = new LookupVO();

            list.setResourceId(new Integer(LookupVO.REFERRAL_SOURCE));
            tmpList = (LookupVO) listBD.findByCriteria(list);
            referral = tmpList.getId().intValue();
            list.setResourceId(new Integer(LookupVO.FUNDING_SOURCE));
            tmpList = (LookupVO) listBD.findByCriteria(list);
            funding = tmpList.getId().intValue();
            list.setResourceId(new Integer(LookupVO.PATIENT_RESIDENCE));
            tmpList = (LookupVO) listBD.findByCriteria(list);
            patientres = tmpList.getId().intValue();
            list.setResourceId(new Integer(LookupVO.GENDER));
            tmpList = (LookupVO) listBD.findByCriteria(list);

            //gender = tmpList.getId().intValue();
            list.setResourceId(new Integer(LookupVO.PATIENT_CARE));
            tmpList = (LookupVO) listBD.findByCriteria(list);
            patient_care = tmpList.getId().intValue();


            account1 = new PatientAccountVO();

            account1.setFirstName("Travis");
            account1.setLastName("Morris");
            account1.setLastName_search("morris");
            account1.setTreatment_location_id(3268);
            account1.setAccount_status(new Integer(1));
            account1.setPhn("3333333333");

            account1.setOutofprovince(new Integer(1));
            com.pixalere.auth.bean.ProfessionalVO userVO = new com.pixalere.auth.bean.ProfessionalVO();
            userVO.setId(new Integer(1));
            userVO.setFirstname("test");
            userVO.setLastname("p;ass");

            userVO.setPosition_id(1);
            account1.setUser_signature(new PDate().getProfessionalSignature(new Date(), userVO,"en"));


            //added for search by professional
            account1.setCreated_by(new Integer(7));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void testAddPatient() throws com.pixalere.common.ApplicationException {

        patient = pbd.addPatient(account1);
        boolean bool = false;
        if (patient != 0) {
            bool = true;
        }
        assertTrue("Add Patient should have returned true, but returned" + patient, bool);

        //Setup pp
        String[] im = {"other"};

        profile1 = new PatientProfileVO();
        profile1.setPatient_id(new Integer(patient));
        profile1.setUser_signature("signature");
        profile1.setReferral_id(new Integer(referral));
        
        //profile1.setInterfering_meds(Serialize.serialize(im));
        //profile1.setCo_morbidities(Serialize.serialize(im));
        /*BradenVO profile12 =new BradenVO();
        profile12.setBraden_activity(1);
        profile12.setBraden_friction(1);
        profile12.setBraden_nutrition(1);
        profile12.setBraden_mobility(1);
        profile12.setBraden_moisture(1);
        profile12.setBraden_sensory(1);*/
        profile1.setStart_date(new java.util.Date());
        //profile1.setMedications_comments("");
        //profile1.setInterfering_meds_other("");
        profile1.setProfessional_id(new Integer(7));
        profile1.setCurrent_flag(new Integer(1));
        profile1.setActive(new Integer(1));
        bd.savePatientProfile(profile1);

        PatientServiceImpl bd = new PatientServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        com.pixalere.auth.bean.ProfessionalVO userVO = new com.pixalere.auth.bean.ProfessionalVO();
        userVO.setId(new Integer(7));
        userVO.setAccount_status(new Integer(1));

        account1.setTreatment_location_id(3268);
        UserAccountRegionsVO r = new UserAccountRegionsVO();
        r.setTreatment_location_id(3268);
        userVO.setTraining_flag(0);
        r.setUser_account_id(7);
        List<UserAccountRegionsVO> list = new ArrayList();
        list.add(r);
        userVO.setRegions(list);
        Vector result = bd.findAllBySearch(1, "", userVO, "", 3268,"","","", true, com.pixalere.utils.Constants.ENGLISH);

        assertEquals("Result should have included 1 row.", 1, result.size());

        result =
                bd.findAllBySearch(1, account1.getEncrypted("Morris", "pass4lastname"), userVO, "", 3268,"","","", true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Result should only include 1 row.", new Integer(1), new Integer(result.size()));

        result =
                bd.findAllBySearch(0, "DorkME", userVO, "", 0, "","","",true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Did not expect to find Patient.", new Integer(0), new Integer(result.size()));

        result =
                bd.findAllBySearch(0, account1.getEncrypted("Morris", "pass4lastname"), userVO, "", 0, "","","",true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Expected to find 1 patient.", 1, result.size());

        result =
                bd.findAllBySearch(0, "", userVO, account1.getEncrypted("3333333333", "pass4phn"), 0,"","","",true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Expected to find 1 patient..", new Integer(1), new Integer(result.size()));
        
        PatientAccountVO patient1 = bd.getPatient(patient);
        PatientAccountVO patient2 = bd.getPatient(patient);
        assertEquals("Retrieve patient should return same object.", patient1.getPatient_id(), patient2.getPatient_id());
        assertEquals("Treatment location from same objects should match.", (patient1.getTreatmentLocation()).getId(), (patient2.getTreatmentLocation()).getId());
        
        userVO = new com.pixalere.auth.bean.ProfessionalVO();
        userVO.setId(new Integer(7));
        userVO.setAccount_status(new Integer(1));
        account1.setTreatment_location_id(3268);
        r = new UserAccountRegionsVO();
        r.setTreatment_location_id(3268);
        userVO.setTraining_flag(0);
        r.setUser_account_id(7);
        List<UserAccountRegionsVO> list2 = new ArrayList();
        list2.add(r);
        userVO.setRegions(list2);
        Vector result2 = bd.findAllBySearch(1, "", userVO, "", 3268,"","","", true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Result should have included 1 row.", 1, result2.size());

        result =
                bd.findAllBySearch(1, account1.getEncrypted("Morris", "pass4lastname"), userVO, "", 3268,"","","", true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Result should only include 1 row.", new Integer(1), new Integer(result.size()));

        result =
                bd.findAllBySearch(0, "DorkME", userVO, "", 0, "","","",true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Did not expect to find Patient.", new Integer(0), new Integer(result.size()));

        result =
                bd.findAllBySearch(0, account1.getEncrypted("Morris", "pass4lastname"), userVO, "", 0, "","","",true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Expected to find 1 patient.", 1, result.size());

        result =
                bd.findAllBySearch(0, "", userVO, account1.getEncrypted("3333333333", "pass4phn"), 0,"","","",true, com.pixalere.utils.Constants.ENGLISH);
        assertEquals("Expected to find 1 patient..", new Integer(1), new Integer(result.size()));

        account1.setPatient_id(new Integer(patient));
        account1.setFirstName("Mallory");
        account1.setCurrent_flag(new Integer(1));
        account1.setCreated_by(7);
        try {
            pbd.savePatient(account1);

            PatientAccountVO account2 = pbd.getPatient(patient);
            assertEquals("Patient should have the firstname 'Mallory'", account1.getDecrypted(account2.getFirstName(), "pass4firstname"), "Mallory");
        } catch (ApplicationException ex) {
            ex.printStackTrace();
        }
    }





    /*
    public void testCompareProfiles() {
    Hashtable hash = null;
    try {
    hash = bd.comparePatientProfiles(profile1,profile3);
    } catch (ApplicationException ex) {
    ex.printStackTrace();
    }
    profile3=profile1;
    //@todo look into this one..
    assertEquals("Profiles should equal", hash.get("patient_changed"), "false");
    
    profile3.setCurrent_flag(new Integer(0));
    profile3.setActive(new Integer(0));
    bd.updatePatientProfile(profile3);
    try {
    hash = bd.comparePatientProfiles(profile1,profile3);
    } catch (ApplicationException ex) {
    ex.printStackTrace();
    }
    //@todo what the hell?
    //assertEquals("Profiles shouldnt equal", hash.get("patient_changed"), "true");
    
    }*/

    

    /*public void testAssignedPatients()  throws ApplicationException {
    PatientManagerBD bd=new PatientManagerBD("test");
    int result = bd.validatePatientId(0,0);
    assertEquals("Object should be null ( int = 1)",new Integer(1),new Integer(result));
    
    result = bd.validatePatientId(0161,1);
    assertEquals("Isnt suppose to  belong to treatment location.",new Integer(2),new Integer(result));
    
    result = bd.validatePatientId(1,1);
    assertEquals("Suppose to be assigned to patient.",new Integer(0),new Integer(result));
    }*/




    /*
     */
}