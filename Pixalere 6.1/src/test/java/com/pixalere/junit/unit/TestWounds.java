package com.pixalere.junit.unit;
import java.util.Collection;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import java.util.Vector;
import com.pixalere.guibeans.PatientSearchVO;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.assessment.service.*;
import com.pixalere.assessment.bean.*;
import com.pixalere.wound.bean.*;
import com.pixalere.wound.service.*;
import com.pixalere.auth.bean.ProfessionalVO;

import com.pixalere.guibeans.PatientSearchVO;
import com.pixalere.guibeans.RowData;

import com.pixalere.utils.*;
import junit.framework.TestCase;
import java.util.Collection;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Date;

public class TestWounds extends TestCase {
    private PatientServiceImpl bd;
    private NursingCarePlanServiceImpl nbd;
    
    private WoundProfileVO wmp;
    private WoundProfileVO wmpTMP;
    private static int patient=1;
    private static int wound_id=0;
    protected void tearDown(){
        bd=null;
        wmpTMP=null;
        wmp=null;
        
    }
    protected void setUp() throws ApplicationException{
        try{
            com.pixalere.patient.dao.PatientProfileDAO dao = new com.pixalere.patient.dao.PatientProfileDAO();
            ProfessionalVO userVO = new ProfessionalVO();
            nbd=new NursingCarePlanServiceImpl();
            userVO.setId(new Integer(7));
            Vector<PatientSearchVO> col = dao.searchPatients(userVO,"", 1, "", "0","", "", 0, 0,1);
            
            for(PatientSearchVO acc : col){
                patient=acc.getPatient_id();
            }
            //assertEquals("Patient "+col.size()+"should exist"+patient,new Integer(col.size()),new Integer(2));
            initializeWoundTMP();
        }catch(Exception e){e.printStackTrace();}
        
    }
    /*public void testNewWound() throws ApplicationException{
        WoundServiceImpl bd=new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        ProfessionalVO userVO = new ProfessionalVO();
        userVO.setId(new Integer(7));
        WoundVO woundVO = new WoundVO();
        woundVO.setWound_location("myWound");
        woundVO.setImage_name("image.jpg");
        woundVO.setWound_location_detailed("");
        woundVO.setWound_location_position("");
        woundVO.setLocation_image_id(new Integer(1));
        woundVO.setPatient_id(patient);
        Date start_date = new Date();
        String startdate_signature = new PDate().getProfessionalSignature(new Date(), userVO,"en");
                woundVO.setStart_date(start_date);
                woundVO.setStartdate_signature(startdate_signature);
                woundVO.setProfessional_id(userVO.getId());
                woundVO.setActive(new Integer(1));
                int  wound_id = bd.getWoundId(woundVO);
                
        com.pixalere.patient.dao.PatientProfileDAO dao = new com.pixalere.patient.dao.PatientProfileDAO();
        System.out.println("Save WP: "+PDate.getEpochTime());
        boolean bool=false;
        if(wound_id!=0 || wound_id !=-1)bool=true;
        assertTrue("Wound id should exist."+wound_id,bool);
        wmpTMP.setDeleted(0);
        bd.saveWoundProfile(wmpTMP);
        
    }*/
   
    
    public void testFindWoundProfiles() throws ApplicationException{
        WoundServiceImpl bd=new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        ProfessionalVO userVO = new ProfessionalVO();
        userVO.setId(new Integer(7));
        WoundVO woundVO = new WoundVO();
        woundVO.setWound_location("myWound");
        woundVO.setImage_name("image.jpg");
        woundVO.setWound_location_detailed("");
        woundVO.setWound_location_position("");
        woundVO.setLocation_image_id(new Integer(1));
        woundVO.setPatient_id(patient);
        Date start_date = new Date();
        String startdate_signature = new PDate().getProfessionalSignature(new Date(), userVO,"en");
                woundVO.setStart_date(start_date);
                woundVO.setStartdate_signature(startdate_signature);
                woundVO.setProfessional_id(userVO.getId());
                woundVO.setActive(new Integer(1));
                int  wound_id = bd.getWoundId(woundVO);
                
        com.pixalere.patient.dao.PatientProfileDAO dao = new com.pixalere.patient.dao.PatientProfileDAO();
        System.out.println("Save WP: "+PDate.getEpochTime());
        boolean bool=false;
        if(wound_id!=0 || wound_id !=-1)bool=true;
        
        assertTrue("Wound id should exist."+wound_id,bool);
        wmpTMP.setDeleted(0);
        bd.saveWoundProfile(wmpTMP);
        
        userVO.setId(7);
        WoundProfileVO crit=new WoundProfileVO();
        crit.setWound_id(1);crit.setActive(1);
            
        Collection<WoundProfileVO> items= bd.getWoundProfiles(crit,0);
           System.out.println(PDate.getEpochTime()+" Flowchart: "+items.size());
        RowData[] result = bd.getAllWoundProfilesForFlowchart(items,userVO,false,false);
        boolean tmp=false;
        if(result.length>0){tmp=true;}
        assertTrue("Wound Profiles must exist.",tmp);
    }
    
 
    
    /*public void testDate(){
        WoundProfileVO profile=new WoundProfileVO();
        profile.setCns_date("1058338800");//7/16/2003
        Vector result = profile.getCNSDate();
        assertEquals("Month should be 7.","7",(String)result.get(0));
        assertEquals("Day should be 16.","16",(String)result.get(1));
        assertEquals("Year should be 2003.","2003",(String)result.get(2));
    }*/
    public void initializeWoundTMP()  throws com.pixalere.common.ApplicationException{
        String[] arr={"other"};
        wmpTMP=new WoundProfileVO();
        WoundVO woundVO=new WoundVO();
        wmpTMP.setWound_id(new Integer(1));
        wmpTMP.setPatient_id(new Integer(patient));
        woundVO.setId(new Integer(wound_id));
        woundVO.setWound_location("Foot");
        woundVO.setProfessional_id(new Integer(7));
        wmpTMP.setActive(1);
        //wmpTMP.setEtiology(Serialize.serialize(arr));
        //wmpTMP.setWound_etiology_other("other");
        wmpTMP.setPressure_ulcer(new Integer(1));
        //wmpTMP.setGoals("");
        wmpTMP.setProfessional_id(new Integer(7));
        
        /*wmpTMP.setCns_show(new Integer(0));
        
        wmpTMP.setCns_name("name");
        wmpTMP.setCns_duration_start(new PDate().getDate(1, 1, 1999));
        wmpTMP.setCns_duration_end(new PDate().getDate(1, 1, 1999));*/
         woundVO.setImage_name("foot.jpg");
        wmpTMP.setPrev_prods("test");
        woundVO.setStart_date(new Date());
        
        //wmpTMP.setAntibiotics("");
        //wmpTMP.setCns_date(new PDate().getDate(1, 1, 1));
        //wmpTMP.setCns_route(new Integer(1));
        wmpTMP.setUser_signature("user");
        wmpTMP.setCurrent_flag(new Integer(1));
        wmpTMP.setActive(new Integer(1));
        woundVO.setStartdate_signature("user");
        woundVO.setActive(new Integer(1));
        wmpTMP.setWound_id(new Integer(1));
        woundVO.setWound_location_detailed("Left Foot");
        wmpTMP.setWound(woundVO);
        System.out.println("initialize WP");
    }
}