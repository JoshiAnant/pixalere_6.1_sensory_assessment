package com.pixalere.junit.unit;

import junit.framework.TestCase;
import com.pixalere.auth.bean.UserAccountRegionsVO;
import com.pixalere.auth.bean.PasswordHistoryVO;
import com.pixalere.auth.service.ProfessionalServiceImpl;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.auth.service.ProfessionalServiceImpl.PasswordStatus;
import com.pixalere.auth.dao.UserDAO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.utils.MD5;
import com.pixalere.utils.PDate;
import com.pixalere.utils.Constants;

public class TestProfessional extends TestCase {

    private static int user_id = 10;

    public void testCreateUser() throws ApplicationException {
        ProfessionalServiceImpl bd = new ProfessionalServiceImpl();
        ProfessionalVO userVO = new ProfessionalVO();
        userVO.setPosition_id(1);
        userVO.setFirstname("Test");
        userVO.setLastname("User");
        userVO.setUser_name("webmed");
        userVO.setPager("travdes@wnt.ca");
        userVO.setEmail("travdes@wmt.ca");
        userVO.setIndividuals("");
        userVO.setAccess_viewer(new Integer(1));
        userVO.setAccess_admin(new Integer(1));
        userVO.setAccess_allpatients(new Integer(1));
        userVO.setAccess_assigning(new Integer(1));
        userVO.setAccess_professionals(new Integer(1));
        userVO.setAccess_patients(new Integer(1));
        userVO.setAccess_regions(new Integer(1));
        userVO.setAccess_referrals(new Integer(1));
        userVO.setAccess_listdata(new Integer(1));
        userVO.setAccess_audit(new Integer(1));
        userVO.setAccess_superadmin(new Integer(0));
        userVO.setAccess_uploader(new Integer(1));

        userVO.setTraining_flag(new Integer(0));
        userVO.setAccess_reporting(new Integer(1));
        userVO.setNew_user(new Integer(1));
        userVO.setAccount_status(new Integer(1));
        userVO.setPassword(MD5.hash("marshall01"));
        bd.saveProfessional(userVO, -1);
        
        UserAccountRegionsVO u = new UserAccountRegionsVO();
        u.setUser_account_id(9);
        u.setTreatment_location_id(3268);
        bd.saveTreatmentLocation(u);
        
        try {

            UserDAO dao = new UserDAO();
            userVO = new ProfessionalVO();
            userVO.setUser_name("webmed");
            userVO.setAccount_status(1);
            ProfessionalVO vo = (ProfessionalVO) dao.findByCriteria(userVO);
            assertNotNull("Patient should exist.", vo);
            user_id = vo.getId().intValue();
            System.out.println("UserID " + user_id);

        } catch (DataAccessException e) {
            e.printStackTrace();
        }


        PasswordHistoryVO hist = new PasswordHistoryVO();
        hist.setProfessional_id(new Integer(user_id));

        //5 days left. Note that this must come after the test for a "not expired" password (see above).
        hist.setCreated_on(PDate.subtractDays(new java.util.Date(), 175));
		
        hist.setPassword("");
        bd.insertPasswordForHistory(hist);

        /*PasswordStatus result = bd.passwordExpired(user_id + "");
        assertEquals("Password should have 5 days till expiry","5", result.toString());

        //0 not expired. Note that this must not come before the test for a "five days left" password (see below).
        java.util.Date timestamp = new java.util.Date();
        hist.setCreated_on( timestamp);
        hist.setPassword("");
        bd.insertPasswordForHistory(hist);
        result = bd.passwordExpired(user_id + "");
        assertEquals("Password shouldn't have expired", PasswordStatus.NOT_EXPIRED, result);


 
        
        
        System.out.println("Validate: " + PDate.getEpochTime());

        assertNotNull("Professional object should exist.", bd.validateUserId("webmed", "marshall01"));

  
    
        ProfessionalVO vo = bd.getProfessional(user_id);
        vo.setFirstname("Tester2");
        bd.saveProfessional(vo, -1);// updating

        ProfessionalVO vo2 = bd.getProfessional(user_id);
        assertEquals("Professional should be 'Tester2'.", "Tester2", vo2.getFirstname());*/

    }

    

    
}