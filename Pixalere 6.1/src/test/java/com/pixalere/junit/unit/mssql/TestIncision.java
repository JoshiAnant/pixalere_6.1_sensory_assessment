package com.pixalere.junit.unit.mssql;

import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.assessment.service.*;
import junit.framework.TestCase;
import com.pixalere.ConstantsTest;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.bean.*;
import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.utils.*;
import java.util.Collection;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Vector;
import com.pixalere.guibeans.RowData;
import java.util.Date;
public class TestIncision extends TestCase {

    private AssessmentImagesServiceImpl ibd = null;
    private AssessmentCommentsServiceImpl cbd = null;
    private WoundAssessmentServiceImpl wbd = null;
    private WoundAssessmentLocationServiceImpl wlbd = null;
    private AssessmentServiceImpl bd = null;
    private AssessmentIncisionVO assessmentIncisionVO = null;
    private AssessmentCommentsVO comm = null;
    private WoundAssessmentVO wa = null;
    private static int patient = 1;
    private static int wound_id = 0;

    protected void tearDown() {
        bd = null;

    }

    private WoundAssessmentLocationVO walTMP=null;
    private WoundAssessmentVO waTMP=null;
    private AssessmentIncisionVO aTMP=null;
    protected void setUp() throws ApplicationException {
        try {
            walTMP=new WoundAssessmentLocationVO();
            walTMP.setId(15968);
            waTMP=new WoundAssessmentVO();
            waTMP.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
            aTMP=new AssessmentIncisionVO();
            aTMP.setId(1);
            aTMP.setWound_id(2612);
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            ibd = new AssessmentImagesServiceImpl();
            cbd = new AssessmentCommentsServiceImpl();
            wbd = new WoundAssessmentServiceImpl();
            wlbd = new WoundAssessmentLocationServiceImpl();
            assessmentIncisionVO = new AssessmentIncisionVO();
            comm = new AssessmentCommentsVO();
            wa = new WoundAssessmentVO();
        } catch (Exception e) {
        }

    }
public void testSaveAlpha() throws ApplicationException {
        //save apha
        //save alphadetails
        //save wound_profile_type
        WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
        wal.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        wal.setProfessional_id(7);
        wal.setActive(1);
        wal.setAlpha("tag1");
        wal.setDischarge(0);
        wal.setOpen_timestamp(new Date());
        wal.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        wal.setWound_profile_type_id(ConstantsTest.MSSQL_WOUND_TYPE);
        wlbd.saveAlpha(wal);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        wal.setActive(0);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        wal.setId(null);
        WoundAssessmentLocationVO w1 = wlbd.getAlpha(wal, false, false, null);
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_INCISION);
        WoundAssessmentLocationVO w2 = wlbd.getAlpha(walTMP);
        wal.setActive(1);
        Collection<WoundAssessmentLocationVO> w5 = wlbd.getAllAlphas(wal);
        assertNotNull("Could not retrieve saved alpha", w5);
        assertEquals("Should be two alphas", new Integer(2), new Integer(w5.size()));
        assertNotNull("Could not retrieve saved alpha", w1);
        assertNotNull("Could not retrieve saved alpha", w2);

        WoundLocationDetailsVO wld = new WoundLocationDetailsVO();
        wld.setAlpha_id(ConstantsTest.MSSQL_ALPHA_ID_INCISION);
        //wld.setBox(1);
        wld.setImage("tag1");
        wlbd.saveAlphaDetail(wld);
        wld.setId(null);
        WoundLocationDetailsVO w3 = wlbd.getAlphaDetail(wld);
        assertNotNull("Could not retreive alpha details", w3);

        WoundProfileTypeVO wpt = new WoundProfileTypeVO();
        wpt.setAlpha_type("T");
        wpt.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        wpt.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        WoundServiceImpl wm = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        wm.saveWoundProfileType(wpt);
        wpt.setId(null);
        Collection<WoundProfileTypeVO> w4 = wm.getWoundProfileTypes(wpt, 0);
        assertNotNull("Could not retrieve saved wound profile type", w4);

        wlbd.removeAlpha(ConstantsTest.MSSQL_ALPHA_ID_INCISION+1, 7);
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_INCISION+1);
        WoundAssessmentLocationVO ww = wlbd.getAlpha(walTMP);
        assertNull("Alpha should be null (removed)", ww);
    }

    
    public void testNewAssessment() throws ApplicationException {
        try {
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            assessmentIncisionVO.setFull_assessment(1);
    	assessmentIncisionVO.setClosed_date(new PDate().getDate(1,1,1977));
        assessmentIncisionVO.setWound_profile_type_id(2612);
        assessmentIncisionVO.setAssessment_id(261261);
    	assessmentIncisionVO.setPatient_id(2078);
    	assessmentIncisionVO.setWound_id(2612);
    	assessmentIncisionVO.setAlpha_id(15968);
        assessmentIncisionVO.setPostop_management(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentIncisionVO.setIncision_exudate(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentIncisionVO.setIncision_status(ConstantsTest.MOCK_ID);
        assessmentIncisionVO.setPeri_incisional(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentIncisionVO.setIncision_closure_methods(ConstantsTest.SERIALIZEDARRAY_ID);
        //assessmentIncisionVO.setIncision_closure_status(new Integer(getIncision_closure_status()));
        assessmentIncisionVO.setExudate_amount(ConstantsTest.MOCK_ID);
        assessmentIncisionVO.setPain_comments(Common.convertCarriageReturns("teste",false));
        assessmentIncisionVO.setClinical_path(ConstantsTest.MOCK_ID);
        assessmentIncisionVO.setClinical_path_date(new PDate().getDate(1,1,1999));
        assessmentIncisionVO.setPostop_days(1);
        assessmentIncisionVO.setActive(1);
        //dropdown
        assessmentIncisionVO.setCreated_on(new Date());
        assessmentIncisionVO.setPain(ConstantsTest.MOCK_ID);
        assessmentIncisionVO.setStatus(256);
       
        //set for all alphas incase some do not goto treatment.
        
            bd.saveAssessment(assessmentIncisionVO);
        } catch (ApplicationException ex) {
            //ex.printStackTrace();
        }
        /*try {
        bd = new AssessmentManagerBD("junit2");
        bd.saveAssessment(assessmentEachwoundVO);
        this.fail("This should have failed");
        } catch (ApplicationException ex) {
        }*/
        //try{
        assessmentIncisionVO.setId(null);
        AbstractAssessmentVO a = bd.getAssessment(assessmentIncisionVO);
        assertNotNull("Should not be null", a);

        AbstractAssessmentVO ab = bd.retrieveLastActiveAssessment(assessmentIncisionVO, "T", 1);
        assertNotNull("Should not be null", ab);

        AbstractAssessmentVO ab2 = bd.retrieveLastActiveAssessment(assessmentIncisionVO, "T", 0);
        assertNotNull("Should not be null", ab2);

        assessmentIncisionVO.setFull_assessment(null);


        Collection c2 = bd.getAllAssessments(assessmentIncisionVO, 0, false);
        assertEquals("Should find 1 assessment", 1, c2.size());



        ProfessionalVO u = new ProfessionalVO();
        u.setId(7);u.setAccess_superadmin(1);
        RowData[] rr = bd.getAllIncisionAssessmentsForFlowchart(c2, u, false);
        assertEquals("Should contain X rows", 1, rr.length);

        assertEquals("Should contain 26 columns", 26, rr[0].getFields().size());
        waTMP.setProfessional_id(7);
        waTMP.setActive(0);
        
        WoundAssessmentVO w1 = wbd.getAssessment(waTMP);//, wound_id)

        assertNull("Shouldn't find any inactive wound assessments", w1);
        waTMP.setProfessional_id(null);
        waTMP.setActive(1);
        waTMP = new WoundAssessmentVO();
        waTMP.setId(11589);
        WoundAssessmentVO w2 = wbd.getAssessment(waTMP);//, wound_id)

        //assertNotNull("Should find 1 wound assessments", w2);

       


        //} catch (ApplicationException ex) {
        //  ex.printStackTrace();
        //}
        

    }

    public void testWoundHealed() throws ApplicationException {
        try {
            boolean h = wlbd.isWoundHealed(ConstantsTest.MSSQL_ALPHA_ID_INCISION, 7);
            assertFalse("Should be healed", h);
        } catch (ApplicationException ex) {
        }
        
    }

    public void testAssessmentExists() throws ApplicationException {
        //checkAssessmentExistForProfId
    }

    
    

    public void testDeleteTMP() throws ApplicationException {
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_INCISION+2);
        WoundAssessmentLocationVO ww = wlbd.getAlpha(walTMP);
        assertNotNull("Alpha should be not null", ww);

        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_INCISION+2);
        wlbd.removeAlpha(walTMP,null);//, wound_id, wound_id);


        WoundAssessmentLocationVO ww2 = wlbd.getAlpha(walTMP);
        assertNull("Alpha should be null (removed)", ww2);
    }

    

        
}