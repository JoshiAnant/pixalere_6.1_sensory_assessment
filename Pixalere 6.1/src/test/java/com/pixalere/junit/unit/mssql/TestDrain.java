package com.pixalere.junit.unit.mssql;
import java.util.Date;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.assessment.service.*;
import junit.framework.TestCase;
import com.pixalere.ConstantsTest;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.bean.*;import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.utils.*;
import java.util.Collection;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Vector;
import com.pixalere.ConstantsTest;
import com.pixalere.guibeans.RowData;
public class TestDrain extends TestCase {

    private AssessmentImagesServiceImpl ibd = null;
    private AssessmentCommentsServiceImpl cbd = null;
    
    private WoundAssessmentServiceImpl wbd = null;
    private WoundAssessmentLocationServiceImpl wlbd = null;
    private AssessmentServiceImpl bd = null;
    private AssessmentDrainVO assessmentDrainVO = null;
    private AssessmentCommentsVO comm = null;
    private WoundAssessmentVO wa = null;

    protected void tearDown() {
        bd = null;

    }
private WoundAssessmentLocationVO walTMP=null;
    private WoundAssessmentVO waTMP=null;
    private AssessmentDrainVO aTMP=null;
    protected void setUp() throws ApplicationException {
        try {
            walTMP=new WoundAssessmentLocationVO();
            walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_DRAIN);
            waTMP=new WoundAssessmentVO();
            waTMP.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
            aTMP=new AssessmentDrainVO();
            aTMP.setId(1);
            aTMP.setWound_id(2612);
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            ibd = new AssessmentImagesServiceImpl();
            cbd = new AssessmentCommentsServiceImpl();
            wbd = new WoundAssessmentServiceImpl();
            wlbd = new WoundAssessmentLocationServiceImpl();
            assessmentDrainVO = new AssessmentDrainVO();
            comm = new AssessmentCommentsVO();
            wa = new WoundAssessmentVO();
        } catch (Exception e) {
        }

    }
    public void testSaveAlpha() throws ApplicationException{
        //save apha
        //save alphadetails
        //save wound_profile_type
        WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
        wal.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        wal.setProfessional_id(7);
        wal.setActive(1);
        wal.setAlpha("td1");
        wal.setDischarge(0);
        wal.setOpen_timestamp(new Date());
        wal.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        wal.setWound_profile_type_id(ConstantsTest.MSSQL_DRAIN_TYPE);
        wlbd.saveAlpha(wal);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        wal.setActive(0);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        wal.setId(null);

        WoundAssessmentLocationVO w1 = wlbd.getAlpha(wal, false, false, null);
        WoundAssessmentLocationVO w2 = wlbd.getAlpha(walTMP);
        wal.setActive(1);
        Collection<WoundAssessmentLocationVO> w5 = wlbd.getAllAlphas(wal);
        assertNotNull("Could not retrieve saved alpha",w5);
        assertEquals("Should be two alphas", new Integer(2),new Integer(w5.size()));
        assertNotNull("Could not retrieve saved alpha", w1 );
        assertNotNull("Could not retrieve saved alpha", w2 );
        
        WoundLocationDetailsVO wld = new WoundLocationDetailsVO();
        wld.setAlpha_id(ConstantsTest.MSSQL_ALPHA_ID_DRAIN);
        //wld.setBox(1);
        wld.setImage("td1");
        wlbd.saveAlphaDetail(wld);
        wld.setId(null);
        WoundLocationDetailsVO w3 = wlbd.getAlphaDetail(wld);
        assertNotNull("Could not retreive alpha details",w3);
        
        WoundProfileTypeVO wpt = new WoundProfileTypeVO();
        wpt.setAlpha_type("td1");wpt.setPatient_id(1);wpt.setWound_id(1);
        WoundServiceImpl wm = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        wm.saveWoundProfileType(wpt);
        wpt.setId(null);
        Collection<WoundProfileTypeVO> w4 = wm.getWoundProfileTypes(wpt, 0);
        assertNotNull("Could not retrieve saved wound profile type",w4);
        
        wlbd.removeAlpha(ConstantsTest.MSSQL_ALPHA_ID_DRAIN+1, 7);
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_DRAIN+1);
        WoundAssessmentLocationVO ww = wlbd.getAlpha(walTMP);
        assertNull("Alpha should be null (removed)",ww);
    }
    public void testNewAssessment() throws ApplicationException {
        try {
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            assessmentDrainVO.setFull_assessment(1);
    	assessmentDrainVO.setPatient_id(2078);
    	assessmentDrainVO.setWound_id(2612);
        assessmentDrainVO.setAlpha_id(15965);
        assessmentDrainVO.setAssessment_id(261261);
        assessmentDrainVO.setWound_profile_type_id(2613);
        assessmentDrainVO.setType_of_drain(ConstantsTest.MOCK_ID);
        assessmentDrainVO.setType_of_drain_other("");
        assessmentDrainVO.setSutured(ConstantsTest.MOCK_ID);
        assessmentDrainVO.setCreated_on(new Date());
        assessmentDrainVO.setDrain_characteristics1(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentDrainVO.setDrain_odour2("");
        assessmentDrainVO.setDrain_characteristics2("");
        assessmentDrainVO.setDrain_odour3("");
        assessmentDrainVO.setDrain_characteristics3("");
        assessmentDrainVO.setDrain_odour4("");
        assessmentDrainVO.setDrain_characteristics4("");
        assessmentDrainVO.setDrain_odour5("");
        assessmentDrainVO.setDrain_characteristics5("");
        assessmentDrainVO.setDrain_odour1(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentDrainVO.setActive(1);
        assessmentDrainVO.setDrainage_amount_mls1(1);
        assessmentDrainVO.setDrainage_amount_hrs1(1);
        assessmentDrainVO.setDrainage_amount1_end(1);
        assessmentDrainVO.setDrainage_amount1_start(1);
        assessmentDrainVO.setAdditional_days_date1(new Date());//new PDate().getDate(getAdditional_days_date1_month(),getAdditional_days_date1_day(),getAdditional_days_date1_year()));
        
        assessmentDrainVO.setDrainage_amount_mls2(0);
        assessmentDrainVO.setDrainage_amount_hrs2(0);
        assessmentDrainVO.setDrainage_amount2_end(0);
        assessmentDrainVO.setDrainage_amount2_start(0);
        assessmentDrainVO.setAdditional_days_date2(null);
        
        assessmentDrainVO.setDrainage_amount_mls3(0);
        assessmentDrainVO.setDrainage_amount_hrs3(0);
        assessmentDrainVO.setDrainage_amount3_end(0);
        assessmentDrainVO.setDrainage_amount3_start(0);
        assessmentDrainVO.setAdditional_days_date3(null);
        
        
        assessmentDrainVO.setDrainage_amount_mls4(0);
        assessmentDrainVO.setDrainage_amount_hrs4(0);
        assessmentDrainVO.setDrainage_amount4_end(0);
        assessmentDrainVO.setDrainage_amount4_start(0);
        assessmentDrainVO.setAdditional_days_date4(null);
        
        
        assessmentDrainVO.setDrainage_amount_mls5(0);
        assessmentDrainVO.setDrainage_amount_hrs5(0);
        assessmentDrainVO.setDrainage_amount5_end(0);
        assessmentDrainVO.setDrainage_amount5_start(0);
        assessmentDrainVO.setAdditional_days_date5(null);
        
        
        assessmentDrainVO.setTubes_changed_date(null);
        
        assessmentDrainVO.setDrainage_num(1);
        assessmentDrainVO.setDrain_site(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentDrainVO.setDrain_removed_intact(1);
        assessmentDrainVO.setClosed_date(null);

        
        //dropdown
        assessmentDrainVO.setStatus(256);
        bd.saveAssessment(assessmentDrainVO);
        } catch (ApplicationException ex) {
            //ex.printStackTrace();
        }
        /*try {
            bd = new AssessmentManagerBD("junit2");
            bd.saveAssessment(assessmentEachwoundVO);
            this.fail("This should have failed");
        } catch (ApplicationException ex) {
        }*/
        //try{
            assessmentDrainVO.setId(null);
            AbstractAssessmentVO a = bd.getAssessment(assessmentDrainVO);
            assertNotNull("Should not be null",a);
            
            AbstractAssessmentVO ab = bd.retrieveLastActiveAssessment(assessmentDrainVO, "D", 1);
            assertNotNull("Should not be null",ab);
            
            AbstractAssessmentVO ab2 = bd.retrieveLastActiveAssessment(assessmentDrainVO, "D", 0);
            assertNotNull("Should not be null",ab2);
            
            assessmentDrainVO.setFull_assessment(null);
            
            
            Collection c2 = bd.getAllAssessments(assessmentDrainVO,0,false);
            assertEquals("Should find 1 assessment", 1,c2.size());
            
            
            
            ProfessionalVO u = new ProfessionalVO();
            u.setId(7);u.setAccess_superadmin(1);
            RowData[] rr= bd.getAllDrainAssessmentsForFlowchart(c2,u,false);
            assertEquals("Should contain X rows",1,rr.length);
            
            assertEquals("Should contain 30 columns",30,rr[0].getFields().size());
            waTMP.setProfessional_id(7);
            waTMP.setActive(0);
            
            WoundAssessmentVO w1 = wbd.getAssessment(waTMP);//, wound_id)
            assertNull("Shouldn't find any inactive wound assessments",w1);
            waTMP.setProfessional_id(null);
            waTMP.setActive(1);
            waTMP = new WoundAssessmentVO();
            waTMP.setId(11589);
            //WoundAssessmentVO w2 = wbd.getAssessment(waTMP);//, wound_id)
            //assertNotNull("Should find 1 wound assessments",w2);
            
          
            
        //} catch (ApplicationException ex) {
          //  ex.printStackTrace();
        //}
        
        
    }
  
    public void testWoundHealed() throws ApplicationException{
        try{
            boolean h = wlbd.isWoundHealed(ConstantsTest.MSSQL_WOUND_ID,7);
            assertFalse("Should be healed",h);
        } catch (ApplicationException ex) {
        }
       
    }
    public void testAssessmentExists() throws ApplicationException{
        //checkAssessmentExistForProfId
    }
    
    
    public void testDeleteTMP() throws ApplicationException {
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_DRAIN+2);
        WoundAssessmentLocationVO ww = wlbd.getAlpha(walTMP);
        assertNotNull("Alpha should be not null",ww);

        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_DRAIN+2);
        wlbd.removeAlpha(walTMP,null);//, wound_id, wound_id);
        WoundAssessmentLocationVO ww2 = wlbd.getAlpha(walTMP);

        assertNull("Alpha should be null (removed)",ww2);
    }
    

        
}