package com.pixalere.junit.unit.mssql;
import com.pixalere.ConstantsTest;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.assessment.service.*;
import junit.framework.TestCase;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.bean.*;import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.utils.*;
import java.util.Collection;
import com.pixalere.ConstantsTest;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Vector;
import com.pixalere.guibeans.RowData;
public class TestOstomy extends TestCase {

    private AssessmentImagesServiceImpl ibd = null;
    private AssessmentCommentsServiceImpl cbd = null;
    private WoundAssessmentServiceImpl wbd = null;
    private WoundAssessmentLocationServiceImpl wlbd = null;
    private AssessmentServiceImpl bd = null;
    private AssessmentOstomyVO assessmentOstomyVO = null;
    private AssessmentCommentsVO comm = null;
    private WoundAssessmentVO wa = null;
    private static int patient = 1;
    private static int wound_id = 0;

    protected void tearDown() {
        bd = null;

    }
private WoundAssessmentLocationVO walTMP=null;
    private WoundAssessmentVO waTMP=null;
    private AssessmentOstomyVO aTMP=null;
    protected void setUp() throws ApplicationException {
        try {
            walTMP=new WoundAssessmentLocationVO();
            walTMP.setId(1);
            waTMP=new WoundAssessmentVO();
            waTMP.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
            aTMP=new AssessmentOstomyVO();
            aTMP.setId(1);
            aTMP.setWound_id(2612);
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            ibd = new AssessmentImagesServiceImpl();
            cbd = new AssessmentCommentsServiceImpl();
            wbd = new WoundAssessmentServiceImpl();
            wlbd = new WoundAssessmentLocationServiceImpl();
            assessmentOstomyVO = new AssessmentOstomyVO();
            comm = new AssessmentCommentsVO();
            wa = new WoundAssessmentVO();
        } catch (Exception e) {
        }

    }
public void testSaveAlpha() throws ApplicationException{
        //save apha
        //save alphadetails
        //save wound_profile_type
        WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
        wal.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        wal.setProfessional_id(ConstantsTest.PROFESSIONAL_ID);
        wal.setActive(1);
        wal.setAlpha("O");
        wal.setDischarge(0);
        wal.setOpen_timestamp(new java.util.Date());
        wal.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        wal.setWound_profile_type_id(ConstantsTest.MSSQL_OSTOMY_TYPE);
        wlbd.saveAlpha(wal);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        wal.setActive(0);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_OSTOMY);
        wal.setId(null);
        walTMP.setId(null);
        WoundAssessmentLocationVO w1 = wlbd.getAlpha(wal, false, false, null);
        WoundAssessmentLocationVO w2 = wlbd.getAlpha(walTMP);
        wal.setActive(1);
        Collection<WoundAssessmentLocationVO> w5 = wlbd.getAllAlphas(wal);
        assertNotNull("Could not retrieve saved alpha",w5);
        assertEquals("Should be two alphas", new Integer(2),new Integer(w5.size()));
        assertNotNull("Could not retrieve saved alpha", w1 );
        assertNotNull("Could not retrieve saved alpha", w2 );
        
        WoundLocationDetailsVO wld = new WoundLocationDetailsVO();
        wld.setAlpha_id(ConstantsTest.MSSQL_ALPHA_ID_OSTOMY);
        //wld.setBox(1);
        wld.setImage("ostu");
        wlbd.saveAlphaDetail(wld);
        wld.setId(null);
        WoundLocationDetailsVO w3 = wlbd.getAlphaDetail(wld);
        assertNotNull("Could not retreive alpha details",w3);
        
        WoundProfileTypeVO wpt = new WoundProfileTypeVO();
        wpt.setAlpha_type("O");wpt.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);wpt.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        WoundServiceImpl wm = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        wm.saveWoundProfileType(wpt);
        wpt.setId(null);
        Collection<WoundProfileTypeVO> w4 = wm.getWoundProfileTypes(wpt, 0);
        assertNotNull("Could not retrieve saved wound profile type",w4);
        
        wlbd.removeAlpha(ConstantsTest.MSSQL_ALPHA_ID_OSTOMY+1, 7);
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_OSTOMY+1);
        WoundAssessmentLocationVO ww = wlbd.getAlpha(walTMP);
        assertNull("Alpha should be null (removed)",ww);
    }
    public void testNewAssessment() throws ApplicationException {
        try {
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            assessmentOstomyVO.setPatient_id(2078);
    	assessmentOstomyVO.setWound_id(2612);
    	assessmentOstomyVO.setCircumference_mm(1 );
        assessmentOstomyVO.setLength_mm(1);
        assessmentOstomyVO.setWidth_mm(1);
        assessmentOstomyVO.setAlpha_id(15971);
        assessmentOstomyVO.setWound_profile_type_id(2612);
        assessmentOstomyVO.setAssessment_id(261261);
        assessmentOstomyVO.setActive(1);
    	//stoma
        
        assessmentOstomyVO.setSelf_care_progress(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setSelf_care_progress_comments(Common.convertCarriageReturns("testsf",false));
        assessmentOstomyVO.setDrainage(ConstantsTest.SERIALIZEDARRAY_VALUE);
        assessmentOstomyVO.setShape(ConstantsTest.MOCK_ID);      
        assessmentOstomyVO.setDrainage_comments(Common.convertCarriageReturns("testers",false));
        //flange
       
        assessmentOstomyVO.setFull_assessment(1);
        //multi-selection
        assessmentOstomyVO.setFlange_pouch(ConstantsTest.MOCK_ID);
        assessmentOstomyVO.setFlange_pouch_comments(Common.convertCarriageReturns("tesrses",false));
        assessmentOstomyVO.setDevices(ConstantsTest.MOCK_ID);
        assessmentOstomyVO.setStool_colour(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setStool_consistency(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setProfile(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setPouching(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setAppearance(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setContour(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setPeriostomy_skin(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setMucocutaneous_margin("");
        assessmentOstomyVO.setMucocutaneous_margin_comments(Common.convertCarriageReturns("",false));
        assessmentOstomyVO.setPerifistula_skin(ConstantsTest.SERIALIZEDARRAY_ID);
        
        //date
        assessmentOstomyVO.setClosed_date(new PDate().getDate(1,1,1977));
        assessmentOstomyVO.setClinical_path(ConstantsTest.MOCK_ID);
        assessmentOstomyVO.setClinical_path_date(new PDate().getDate(1,1,1977));
                //assessmentOstomyVO.setWound_date(new PDate().getDate(getWound_month(), getWound_day(), getWound_year()));
        assessmentOstomyVO.setCreated_on(new java.util.Date());
        //dropdown
        assessmentOstomyVO.setStool_quantity(ConstantsTest.MOCK_ID);
        assessmentOstomyVO.setStatus(256);
        assessmentOstomyVO.setDischarge_reason(ConstantsTest.MOCK_ID);
        assessmentOstomyVO.setUrine_type(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setUrine_colour(ConstantsTest.SERIALIZEDARRAY_ID);
        assessmentOstomyVO.setConstruction(ConstantsTest.MOCK_ID);
        assessmentOstomyVO.setUrine_quantity(ConstantsTest.MOCK_ID);
       assessmentOstomyVO.setPostop_days(1);
        //other 
        assessmentOstomyVO.setStool_quantity_other("");
        assessmentOstomyVO.setNutritional_status(ConstantsTest.MOCK_ID);
        assessmentOstomyVO.setNutritional_status_other("");
        
        bd.saveAssessment(assessmentOstomyVO);
        } catch (ApplicationException ex) {
            //ex.printStackTrace();
        }
        /*try {
            bd = new AssessmentManagerBD("junit2");
            bd.saveAssessment(assessmentEachwoundVO);
            this.fail("This should have failed");
        } catch (ApplicationException ex) {
        }*/
        //try{
            assessmentOstomyVO.setId(null);
            AbstractAssessmentVO a = bd.getAssessment(assessmentOstomyVO);
            assertNotNull("Should not be null",a);
            
            AbstractAssessmentVO ab = bd.retrieveLastActiveAssessment(assessmentOstomyVO, "O", 1);
            assertNotNull("Should not be null",ab);
            
            assessmentOstomyVO.setFull_assessment(0);
            
            
            assessmentOstomyVO.setFull_assessment(null);
            
            
            Collection c2 = bd.getAllAssessments(assessmentOstomyVO,0,false);
            assertEquals("Should find 1 assessment", 1,c2.size());
            
            
            
            ProfessionalVO u = new ProfessionalVO();
            u.setId(7);u.setAccess_superadmin(1);
            RowData[] rr= bd.getAllOstomyAssessmentsForFlowchart(c2,u,false);
            assertEquals("Should contain X rows",1,rr.length);
            
            assertEquals("Should contain 48 columns",48,rr[0].getFields().size());
            waTMP.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
            waTMP.setId(null);
            waTMP.setProfessional_id(7);
            waTMP.setActive(0);
            WoundAssessmentVO w1 = wbd.getAssessment(waTMP);//, wound_id)
            assertNull("Shouldn't find any inactive wound assessments",w1);
            waTMP.setActive(1);
            waTMP.setProfessional_id(null);
            waTMP = new WoundAssessmentVO();
        waTMP.setId(11589);
            WoundAssessmentVO w2 = wbd.getAssessment(waTMP);//, wound_id)
            //assertNotNull("Should find 1 wound assessments",w2);
            
          
            
            
        //} catch (ApplicationException ex) {
          //  ex.printStackTrace();
        //}
        try {
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            bd.getAssessment(assessmentOstomyVO);
            this.fail("This should have faild");
        } catch (ApplicationException ex) {
        }
        
    }
  
    public void testWoundHealed() throws ApplicationException{
        try{
            boolean h = wlbd.isWoundHealed(ConstantsTest.MSSQL_WOUND_ID,7);
            assertFalse("Should be healed",h);
        } catch (ApplicationException ex) {
        }
          
    }
    public void testAssessmentExists() throws ApplicationException{
        //checkAssessmentExistForProfId
    }
   
    public void testDeleteTMP() throws ApplicationException {
        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_OSTOMY+2);
        WoundAssessmentLocationVO ww = wlbd.getAlpha(walTMP);
        assertNotNull("Alpha should be not null",ww);

        walTMP.setId(ConstantsTest.MSSQL_ALPHA_ID_OSTOMY+2);
        wlbd.removeAlpha(walTMP,null);//, wound_id, wound_id);
        WoundAssessmentLocationVO ww2 = wlbd.getAlpha(walTMP);

        assertNull("Alpha should be null (removed)",ww2);
    }
 
        
}