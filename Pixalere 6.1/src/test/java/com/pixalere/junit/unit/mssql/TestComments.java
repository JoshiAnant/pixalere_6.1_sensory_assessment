package com.pixalere.junit.unit.mssql;
import com.pixalere.guibeans.Comments;
import com.pixalere.guibeans.AssessmentListVO;
import java.util.Date;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.assessment.service.*;
import junit.framework.TestCase;
import com.pixalere.ConstantsTest;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.bean.*;import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.utils.*;
import java.util.Collection;
import java.util.Vector;
public class TestComments extends TestCase {

    private AssessmentImagesServiceImpl ibd = null;
    private AssessmentCommentsServiceImpl cbd = null;
    private WoundAssessmentServiceImpl wbd = null;
    private WoundAssessmentLocationServiceImpl wlbd = null;
    private AssessmentServiceImpl bd = null;private AssessmentEachwoundVO assessmentEachwoundVO = null;
    private AssessmentCommentsVO comm = null;
    private WoundAssessmentVO wa = null;


    protected void tearDown() {
        bd = null;

    }

    protected void setUp() throws ApplicationException {
        try {
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            ibd = new AssessmentImagesServiceImpl();
            cbd = new AssessmentCommentsServiceImpl();
            wbd = new WoundAssessmentServiceImpl();
            wlbd = new WoundAssessmentLocationServiceImpl();
            assessmentEachwoundVO = new AssessmentEachwoundVO();
            comm = new AssessmentCommentsVO();
            wa = new WoundAssessmentVO();
        } catch (Exception e) {
        }

    }
    public void testAddComments() throws ApplicationException {
        
        comm.setAssessment_id(38);
        AssessmentCommentsVO c = cbd.getComment(comm);
        assertNotNull("Comment should be null",c);
        
        comm.setBody("body");
        comm.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);comm.setProfessional_id(7);
        comm.setCreated_on(new Date());
        comm.setUser_signature("user_sig");
        comm.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        comm.setWound_profile_type_id(ConstantsTest.MSSQL_WOUND_TYPE);
        cbd.saveComment(comm);
        comm.setAssessment_id(-1);
        comm.setId(null);
        cbd.saveComment(comm);
        comm.setId(null);
        AssessmentCommentsVO c2 = cbd.getComment(comm);
        assertNotNull("Comment should be not null",c2);
        
    }
    
    public void removeComments() throws ApplicationException {
       
    }

}