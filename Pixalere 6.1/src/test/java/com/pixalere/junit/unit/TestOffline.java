package com.pixalere.junit.unit;

import com.pixalere.common.service.ListServiceImpl;
import com.pixalere.common.service.ListService;
import com.pixalere.common.bean.LookupVO;
import com.pixalere.admin.service.*;
import com.pixalere.admin.client.*;
import com.pixalere.wound.service.*;
import com.pixalere.wound.bean.*;
import com.pixalere.wound.client.*;
import com.pixalere.patient.service.*;
import com.pixalere.patient.bean.*;
import com.pixalere.patient.client.*;
import com.pixalere.common.service.*;
import com.pixalere.common.client.*;
import com.pixalere.common.bean.*;
import com.pixalere.assessment.service.*;
import com.pixalere.assessment.bean.*;
import com.pixalere.assessment.client.*;
import com.pixalere.admin.bean.*;
import com.pixalere.auth.client.*;
import com.pixalere.auth.service.*;
import com.pixalere.auth.bean.*;
import java.util.Collection;
//Test Offline application
import junit.framework.TestCase;
import com.pixalere.admin.service.ResourcesService;
import com.pixalere.admin.bean.ResourcesVO;
import com.pixalere.admin.client.ResourcesClient;

public class TestOffline extends TestCase {

    ProfessionalVO user = null;

    protected void tearDown() {
    }

    protected void setUp() throws Exception {
        //l = new LoginService();
        //u = new UploadService();
        //d = new DownloadService();
        //user = l.validateLogin("7", "wmcastle07", "junit");
    }

    public void testGetResources() throws Exception {
        ResourcesClient rc = new ResourcesClient();
        ResourcesService rServiceImpl = rc.createResourceClient();
        
        ResourcesVO resource = rServiceImpl.getResource(51);
        ResourcesServiceImpl res = new ResourcesServiceImpl();
        
        res.saveResource(resource);
        ResourcesVO offObj = res.getResource(51);
        assertEquals("Should say Vancouver", "Vancouver", offObj.getName());
        assertEquals("Should say Vancouver", "Vancouver", resource.getName());
    }

    public void testLoginOffline() throws Exception {
        ProfessionalClient pc = new ProfessionalClient();
        ProfessionalService pServiceImpl = pc.createProfessionalClient();
        
        ProfessionalVO user1 = pServiceImpl.validateUserId("7", "fat.burning2@");
        assertNotNull("Profesional record should not be null", user1);

        ProfessionalVO u2 = pServiceImpl.validateUserId("7", "wmcastle08");
        assertNull("Profesional record should be null", u2);
    }

    public void testDownloadPatientOffline() throws Exception {
        String database = "live_hsqldb";
        
        ResourcesClient rc = new ResourcesClient();
        ResourcesService removeRService = rc.createResourceClient();
        ResourcesServiceImpl localRService = new ResourcesServiceImpl();
        
        Collection<ResourcesVO> rr = removeRService.getAllResources();
        if (rr != null) {
            for (ResourcesVO listVO : rr) {
                localRService.saveResource(listVO);
            }
        }
        Collection<ResourcesVO> lr = localRService.getAllResources();
        assertEquals("Resources count should equal",rr.size(),lr.size());
        
        ListClient lc = new ListClient();
        ListService remoteLService = lc.createListClient();
        
        ListServiceImpl localLService = new ListServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        Collection<LookupVO> lv = remoteLService.getAllListsForOffline();
        if (lv != null) {
            for (LookupVO listVO : lv) {
                localLService.saveListItemForOffline(listVO);
            }
        }
        Collection<LookupVO> lv2 = localLService.getAllLists();
        //assertEquals("Resources count should equal",lv.size(),lv2.size());
        
        GUIClient cc = new GUIClient();
        GUIService gs = cc.createGUIClient();
        
        GUIServiceImpl gsi = new GUIServiceImpl();
        ComponentsVO crit = new ComponentsVO();
        Collection<ComponentsVO> components = gs.getAllComponents(crit);
        if (components != null) {
            for (ComponentsVO listVO : components) {
                gsi.saveComponent(listVO);
            }
        }
        Collection<ComponentsVO> components2 = gsi.getAllComponents(crit);
        assertEquals("Components count should equal",components.size(),components2.size());
        
        ProfessionalClient pc = new ProfessionalClient();
        ProfessionalService pServiceImpl = pc.createProfessionalClient();
        
        ProfessionalVO us = pServiceImpl.getProfessional(7);

        ProfessionalServiceImpl userBD = new ProfessionalServiceImpl();
        userBD.saveProfessional(us,-1);
        
        ProfessionalVO checkProf = userBD.getProfessional(7);
        assertNotNull("Professional shoudl exist",checkProf);
        
        PatientClient patc = new PatientClient();
        PatientService pats = patc.createPatientClient();
      
        PatientAccountVO account = pats.getPatient(499);
        PatientServiceImpl patsi = new PatientServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        //patsi.addPatient(account);
        PatientAccountVO chkPatient = patsi.getPatient(account);
        assertNotNull("Patient Account should exist",chkPatient);
        
        PatientProfileClient ppc = new PatientProfileClient();
        PatientProfileService pps = ppc.createPatientProfileClient();
        
        PatientProfileVO crit2 = new PatientProfileVO();
        crit2.setPatient_id(499);
        PatientProfileVO vo = pps.getPatientProfile(crit2);
        PatientProfileServiceImpl pmanager = new PatientProfileServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        if (vo != null) {
            pmanager.savePatientProfile(vo);
            PatientProfileVO chkPatient2 = pmanager.getPatientProfile(vo);
            assertNotNull("Patient Profile should exist",chkPatient2);
        }
        
        
        FootAssessmentVO critf = new FootAssessmentVO();
        critf.setActive(1);
        critf.setPatient_id(499);
        FootAssessmentVO foot = pps.getFootAssessment(critf);
        if (foot != null) {
            pmanager.saveFoot(foot);
            FootAssessmentVO ff = pmanager.getFootAssessment(foot);
            assertNotNull("Foot Assessment should exist",ff);
        }
        
        LimbBasicAssessmentVO critl = new LimbBasicAssessmentVO();
        critl.setActive(1);
        critl.setPatient_id(499);
        LimbBasicAssessmentVO limb = pps.getLimbBasicAssessment(critl);
        if (limb != null) {
            pmanager.saveBasicLimb(limb);
            
        }
        WoundClient wc = new WoundClient();
        WoundService ws = wc.createWoundClient();
       
        WoundServiceImpl wsi = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        WoundProfileVO critw = new WoundProfileVO();
        critw.setActive(1);
        critw.setCurrent_flag(1);
        critw.setPatient_id(499);
        Collection<WoundProfileVO> wve = ws.getWoundProfiles(critw, -1);
        for (WoundProfileVO woundVO : wve) {
            if (woundVO != null) {
                WoundVO wVO = woundVO.getWound();
                wsi.saveWound(wVO);
                wsi.saveWoundProfile(woundVO);
                
                WoundVO wVOCHK = wsi.getWound(wVO);
                assertNotNull("Wound is not null",wVOCHK);
                
                WoundProfileVO woundCHK = wsi.getWoundProfile(woundVO);
                assertNotNull("WoundProfile is not null",woundVO);
                
                AssessmentClient ac = new AssessmentClient();
                AssessmentService as = ac.createAssessmentClient();
                
                AssessmentServiceImpl assessBD = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
                WoundAssessmentServiceImpl wass = new WoundAssessmentServiceImpl();
                int last_assessment_id = 0;
                System.out.println("saving WP");
                WoundAssessmentLocationClient walc = new WoundAssessmentLocationClient();
                WoundAssessmentLocationService wals = walc.createWoundAssessmentLocationClient();
                
                WoundAssessmentLocationServiceImpl walsi = new WoundAssessmentLocationServiceImpl();
                WoundAssessmentLocationVO critwal = new WoundAssessmentLocationVO();
                critwal.setWound_id(wVO.getId());
                critwal.setActive(1);
                Collection<WoundAssessmentLocationVO> alphas = wals.getAllAlphas(critwal);
                if (alphas != null) {
                    for (WoundAssessmentLocationVO eachVO : alphas) {

                        WoundLocationDetailsVO[] details = eachVO.getAlpha_details();

                        walsi.saveAlpha(eachVO);
                        WoundAssessmentLocationVO alphaCHK = walsi.getAlpha(eachVO);
                        assertNotNull("Alpha not equal to NULL",alphaCHK);
                        if(details!=null){
                            for (WoundLocationDetailsVO detail : details) {
                                walsi.saveAlphaDetail(detail);
                                WoundLocationDetailsVO detailCHK = walsi.getAlphaDetail(detail);
                                assertNotNull("Alpha detail does not equal to NULL",detailCHK);
                            }
                        }
                        AbstractAssessmentVO crita = new AbstractAssessmentVO();
                        crita.setPatient_id(499);
                        crita.setWound_id(wVO.getId());
                        crita.setAlpha_id(eachVO.getId());
                        Collection<AssessmentEachwoundVO> ave = as.getWoundAssessmentsForOffline(crita);//, last_assessment_id, desc)d.downloadAssessmentEachwoundVector(woundVO.getWound_id(), rm_database);
                        if (ave != null) {
                            for (AssessmentEachwoundVO abstractAssessment : ave) {
                                if (abstractAssessment != null) {
                                    System.out.println("delete3" + abstractAssessment.getId());

                                    abstractAssessment.setId(null);
                                    assessBD.saveAssessment(abstractAssessment);
                                    abstractAssessment.setId(null);
                                    AbstractAssessmentVO assessCHK = assessBD.getAssessment(abstractAssessment);
                                    assertNotNull("Assessment is not equal to NULL",assessCHK);
                                    
                                    last_assessment_id = abstractAssessment.getAssessment_id().intValue();
                                    WoundAssessmentVO assessVO = abstractAssessment.getWoundAssessment();
                                    //AssessmentProductVO[] products = abstractAssessment.getProducts();

                                    if (assessVO != null) {
                                        wass.saveAssessment(assessVO);
                                        WoundAssessmentVO wassessCHK = wass.getAssessment(assessVO);
                                        assertNotNull("Wound Assessment is not NULL",wassessCHK);
                                    }


                                }

                                AssessmentImagesClient aic = new AssessmentImagesClient();
                                AssessmentImagesService ais = aic.createAssessmentImagesClient();
                               
                                AssessmentImagesServiceImpl aisi = new AssessmentImagesServiceImpl();
                                AssessmentImagesVO criti = new AssessmentImagesVO();
                                criti.setWound_id(wVO.getId());
                                criti.setAssessment_id(abstractAssessment.getAssessment_id());
                                Collection<AssessmentImagesVO> images = aisi.getAllImages(criti);
                                if(images!=null){
                                    for (AssessmentImagesVO imgVO : images) {
                                        //d.downloadFile("1", imgVO, database);
                                        //@todo download image file!
                                        aisi.saveImage(imgVO);
                                        AssessmentImagesVO imagesCHK = aisi.getImage(imgVO);
                                        assertNotNull("Images are not null",imgVO);

                                    }
                                }
                                AssessmentCommentsClient acc = new AssessmentCommentsClient();
                                AssessmentCommentsService acs = acc.createAssessmentCommentsClient();
                               
                                AssessmentCommentsServiceImpl acsi = new AssessmentCommentsServiceImpl();
                                AssessmentCommentsVO critc = new AssessmentCommentsVO();
                                critc.setWound_id(wVO.getId());
                                critc.setAssessment_id(abstractAssessment.getAssessment_id());
                                Collection<AssessmentCommentsVO> comments = acs.getAllComments(critc);
                                if(comments!=null){
                                    for (AssessmentCommentsVO c : comments) {
                                        acsi.saveComment(c);
                                        AssessmentCommentsVO commentsCHK = acsi.getComment(c);
                                        assertNotNull("Images are not null",commentsCHK);
                                    }
                                }

                               
                            }
                        }


                    }
                }


            }
        }

    }

    /*public void testDownloadEmptyPatientOffline() {
    }
    
    public void testDoOfflineUploader() {
    
    }
    
    public void testUploadPatientOffline() {
    }
    
    public void testUploadEmptyPatientOffline() {
    }*/
}