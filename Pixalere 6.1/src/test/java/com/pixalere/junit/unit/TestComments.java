package com.pixalere.junit.unit;
import com.pixalere.guibeans.Comments;
import com.pixalere.guibeans.AssessmentListVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.assessment.service.*;
import junit.framework.TestCase;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.bean.*;import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.utils.*;
import java.util.Collection;
import java.util.Vector;
public class TestComments extends TestCase {

    private AssessmentImagesServiceImpl ibd = null;
    private AssessmentCommentsServiceImpl cbd = null;
    private WoundAssessmentServiceImpl wbd = null;
    private WoundAssessmentLocationServiceImpl wlbd = null;
    private AssessmentServiceImpl bd = null;private AssessmentEachwoundVO assessmentEachwoundVO = null;
    private AssessmentCommentsVO comm = null;
    private WoundAssessmentVO wa = null;
    private static int patient = 1;
    private static int wound_id = 0;

    protected void tearDown() {
        bd = null;

    }

    protected void setUp() throws ApplicationException {
        try {
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            ibd = new AssessmentImagesServiceImpl();
            cbd = new AssessmentCommentsServiceImpl();
            wbd = new WoundAssessmentServiceImpl();
            wlbd = new WoundAssessmentLocationServiceImpl();
            assessmentEachwoundVO = new AssessmentEachwoundVO();
            comm = new AssessmentCommentsVO();
            wa = new WoundAssessmentVO();
        } catch (Exception e) {
        }

    }
    
    public void testComments() throws ApplicationException {
         comm.setAssessment_id(1);
        AssessmentCommentsVO c = cbd.getComment(comm);
        assertNull("Comment should be null",c);
        
        comm.setBody("body");
        comm.setPatient_id(1);comm.setProfessional_id(7);
        comm.setCreated_on(new java.util.Date());
        comm.setUser_signature("user_sig");
        comm.setWound_id(1);
        comm.setWound_profile_type_id(1);
        cbd.saveComment(comm);
        comm.setAssessment_id(-1);
        comm.setId(null);
        cbd.saveComment(comm);
        comm.setId(null);
        AssessmentCommentsVO c2 = cbd.getComment(comm);
        assertNotNull("Comment should be not null",c2);
        AssessmentCommentsVO rt = new AssessmentCommentsVO();
        rt.setAssessment_id(new Integer(1));
        c = cbd.getComment(rt);
        assertNotNull("Comment should be not null",c);
        WoundAssessmentVO wa=new WoundAssessmentVO();
        wa.setWound_id(1);
        Collection<WoundAssessmentVO> wounds = wbd.getAllAssessments(wa);
        Vector wnds = new Vector();
        for (WoundAssessmentVO w : wounds){
            wnds.add(w);
        }
        AbstractAssessmentVO a = new AbstractAssessmentVO();
        a.setPatient_id(1);
        a.setWound_id(1);
        a.setWound_profile_type_id(1);
        Vector<AssessmentListVO> comments = cbd.getAllComments(wnds, a, "7", "1", "1","en");
        
        //Vector v = bd.filterWoundAssessmentsByDateRange(wounds,1,1);
        assertEquals("Should be 3 Assessments (2 unassigned)",2,comments.size());
        //4 total comments
        int cnt=0;
        for(AssessmentListVO l : comments){
            Vector<Comments> vc = l.getComments();
            for(Comments cc : vc){
                cnt++;
            }
        }
        assertEquals("Should bd 2 comments",2,cnt);
    }
    public void removeComments() throws ApplicationException {
       
    }

}