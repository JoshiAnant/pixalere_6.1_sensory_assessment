package com.pixalere.junit.unit.mssql;
import com.pixalere.ConstantsTest;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.assessment.service.*;
import junit.framework.TestCase;
import com.pixalere.ConstantsTest;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.assessment.bean.*;import com.pixalere.wound.bean.WoundProfileTypeVO;
import com.pixalere.utils.*;
import java.util.Date;
import java.util.Collection;
import com.pixalere.auth.bean.ProfessionalVO;
import java.util.Vector;
import java.util.Date;
import com.pixalere.guibeans.RowData;
public class TestAssessment extends TestCase {
    private AssessmentImagesServiceImpl ibd = null;
    private AssessmentCommentsServiceImpl cbd = null;
    private WoundAssessmentServiceImpl wbd = null;
    private WoundAssessmentLocationServiceImpl wlbd = null;
    private AssessmentServiceImpl bd = null;
    private AssessmentEachwoundVO assessmentEachwoundVO = null;
    private AssessmentCommentsVO comm = null;
    private WoundAssessmentVO wa = null;

    protected void tearDown() {
        bd = null;

    }
    private WoundAssessmentLocationVO walTMP=null;
    private WoundAssessmentVO waTMP=null;
    private AssessmentEachwoundVO aTMP=null;
    protected void setUp() throws ApplicationException {
        try {
            walTMP=new WoundAssessmentLocationVO();
            walTMP.setId(24);
            waTMP=new WoundAssessmentVO();
            waTMP.setId(7);
            aTMP=new AssessmentEachwoundVO();
            aTMP.setWound_id(1);
            
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            ibd = new AssessmentImagesServiceImpl();
            cbd = new AssessmentCommentsServiceImpl();
            wbd = new WoundAssessmentServiceImpl();
            wlbd = new WoundAssessmentLocationServiceImpl();
            assessmentEachwoundVO = new AssessmentEachwoundVO();
            comm = new AssessmentCommentsVO();
            wa = new WoundAssessmentVO();
        } catch (Exception e) {
        }

    }

    public void testNewWoundAssessment() throws ApplicationException {
        //try {
            wa.setPatient_id(2);
            wa.setOffline_flag(0);
            wa.setActive(1);
            wa.setCreated_on(new Date());
            wa.setProfessional_id(7);
            wa.setTreatment_location_id(-1);
            wa.setUser_signature("NA");
            wa.setWound_id(1);
            wbd.saveAssessment(wa);
           
            WoundAssessmentVO w = wbd.getAssessment(waTMP);
            assertNotNull("WoundAssessment should not be null",w);
            
            
            Collection wounds = wbd.getAllAssessments(waTMP);
            assertEquals("Should be 1 Wound Assessment",1,wounds.size());
            
        //} catch (ApplicationException ex) {
        //    ex.printStackTrace();
        //}
        
        try{
            wa.setId(null);
            WoundAssessmentVO w2 =    wbd.getAssessment(wa);
            assertNotNull("Should not be null",w2);
        } catch (ApplicationException ex) {
        }
        
    }
public void testSaveAlpha() throws ApplicationException{
        //save apha
        //save alphadetails
        //save wound_profile_type
        WoundAssessmentLocationVO wal = new WoundAssessmentLocationVO();
        wal.setPatient_id(2);
        //wal.setProfessional_id(176);
        wal.setActive(1);
        wal.setAlpha("A");
        wal.setDischarge(0);
        //wal.setOpen_timestamp("1127411403");
        wal.setWound_id(1);
        wal.setWound_profile_type_id(1);
        wlbd.saveAlpha(wal);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        wal.setActive(0);
        wal.setId(null);
        wlbd.saveAlpha(wal);
        wal.setId(null);
        WoundAssessmentLocationVO w1 = wlbd.getAlpha(wal, false, false, null);
        
        WoundAssessmentLocationVO w2 = wlbd.getAlpha(walTMP);
        wal.setActive(1);
        wal.setId(null);
        Collection<WoundAssessmentLocationVO> w5 = wlbd.getAllAlphas(wal);
        assertNotNull("Could not retrieve saved alpha",w5);
        assertEquals("Should be two alphas", new Integer(2),new Integer(w5.size()));
        assertNotNull("Could not retrieve saved alpha", w1 );
        assertNotNull("Could not retrieve saved alpha", w2 );
        
        WoundLocationDetailsVO wld = new WoundLocationDetailsVO();
        wld.setAlpha_id(1);
       // wld.setBox(46);
        wld.setImage("A");
        wlbd.saveAlphaDetail(wld);
        wld.setId(null);
        WoundLocationDetailsVO w3 = wlbd.getAlphaDetail(wld);
        assertNotNull("Could not retreive alpha details",w3);
        
        WoundProfileTypeVO wpt = new WoundProfileTypeVO();
        wpt.setAlpha_type("A");wpt.setPatient_id(2);wpt.setWound_id(1);
        WoundServiceImpl wm = new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        wm.saveWoundProfileType(wpt);
        wpt.setId(null);
        Collection<WoundProfileTypeVO> w4 = wm.getWoundProfileTypes(wpt, 0);
        assertNotNull("Could not retrieve saved wound profile type",w4);
        
        wlbd.removeAlpha(3493+1, 480);
        walTMP.setId(3493+1);
        WoundAssessmentLocationVO ww = wlbd.getAlpha(walTMP);
        assertNull("Alpha should be null (removed)",ww);
        
        /*Vector aa = wlbd.retrieveLocationsForWound(266,true,false);
        AbstractAssessmentVO abb = new AbstractAssessmentVO();
        abb.setPatient_id(2);
        abb.setWound_id(1);
        abb.setAssessment_id(7);
        java.util.List bb = wlbd.computeUncompletedAlphas(abb);
        assertEquals("Should be none uncompleted...", 2,bb.size());*/
    }
    public void testNewAssessment() throws ApplicationException {
        try {
            bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            assessmentEachwoundVO.setActive(1);
            assessmentEachwoundVO.setAssessment_id(261261);
            assessmentEachwoundVO.setPatient_id(1708);
        assessmentEachwoundVO.setWound_id(2610);
        assessmentEachwoundVO.setWound_profile_type_id(2610);
        assessmentEachwoundVO.setPain(1);
        assessmentEachwoundVO.setLength(1.0);
        assessmentEachwoundVO.setWidth(2.0);
        assessmentEachwoundVO.setDepth(3.0);
        
        assessmentEachwoundVO.setRecurrent(1);
        assessmentEachwoundVO.setWound_edge("a:1:{}");
        assessmentEachwoundVO.setPeriwound_skin("a:1:{}");
        assessmentEachwoundVO.setClosed_date(new PDate().getDate(1,1,1988));
        assessmentEachwoundVO.setWound_date(new PDate().getDate(1, 1, 1999));
        assessmentEachwoundVO.setFull_assessment(1);
        assessmentEachwoundVO.setExudate_amount(1);
        assessmentEachwoundVO.setWound_depth("1");
        assessmentEachwoundVO.setExudate_colour("a:1:{}");
        assessmentEachwoundVO.setExudate_odour(1);
        assessmentEachwoundVO.setExudate_type("a:1:{}");
        assessmentEachwoundVO.setAlpha_id(ConstantsTest.MSSQL_ALPHA_ID_WOUND);
        //set for all alphas incase some do not goto treatment.
        assessmentEachwoundVO.setPain_comments(Common.convertCarriageReturns("testser",false));
        assessmentEachwoundVO.setStatus(256);
        
        
        
        // ---------------
        assessmentEachwoundVO.setCreated_on(new Date());
        assessmentEachwoundVO.setDischarge_reason(1);
        assessmentEachwoundVO.setDischarge_other("");
        assessmentEachwoundVO.setFistulas(Common.convertCarriageReturns("a:1:{\"testsr\"}",false));
        assessmentEachwoundVO.setNum_fistulas(1);
        bd.saveAssessment(assessmentEachwoundVO);
        } catch (ApplicationException ex) {
            //ex.printStackTrace();
        }
        /*try {
            bd = new AssessmentManagerBD("junit2");
            bd.saveAssessment(assessmentEachwoundVO);
            this.fail("This should have failed");
        } catch (ApplicationException ex) {
        }*/
        //try{
            assessmentEachwoundVO.setId(null);
            AbstractAssessmentVO a = bd.getAssessment(assessmentEachwoundVO);
            assertNotNull("Should not be null",a);
            
            AbstractAssessmentVO ab = bd.retrieveLastActiveAssessment(assessmentEachwoundVO, "A", 1);
            assertNotNull("Should not be null",ab);
            
            //AbstractAssessmentVO ab2 = bd.retrieveLastActiveAssessment(assessmentEachwoundVO, "A", 0);
            //assertNotNull("Should not be null",ab2);
            
            assessmentEachwoundVO.setFull_assessment(null);
            
            
            Collection c2 = bd.getAllAssessments(assessmentEachwoundVO,0,false);
            assertEquals("Should find 1 assessment", 1,c2.size());
            
            
            
            ProfessionalVO u = new ProfessionalVO();
            u.setId(7);
            u.setAccess_superadmin(1);
            RowData[] rr= bd.getAllAssessmentsForFlowchart(c2,u,false);
            assertEquals("Should contain 1 column",1,rr.length);
            
            assertEquals("Should contain 39 rows",39,rr[0].getFields().size());
            waTMP.setId(null);
            waTMP.setPatient_id(2);
            waTMP.setProfessional_id(7);
            waTMP.setActive(0);
            WoundAssessmentVO w1 = wbd.getAssessment(waTMP);//, wound_id)
            //assertNull("Shouldn't find any inactive wound assessments",w1);
            waTMP.setActive(1);
            waTMP.setProfessional_id(null);
            WoundAssessmentVO w2 = wbd.getAssessment(waTMP);//, wound_id)
            assertNotNull("Should find 1 wound assessments",w2);
            
            
            AbstractAssessmentVO abb = bd.getAssessment(aTMP);//, wound_id, alpha_type)
            assertNotNull("Should find 1 active Assessment",abb);
            
            AbstractAssessmentVO abbb = new AbstractAssessmentVO();
            abbb.setAssessment_id(3);
            Collection ass = bd.findAllPastAssessmentsByCriteria(abbb);
            //assertEquals("Found 3 assessments",1,ass.size());
            
        //} catch (ApplicationException ex) {
          //  ex.printStackTrace();
        //}
       
        
    }
  
    public void testWoundHealed() throws ApplicationException{
        try{
            boolean h = wlbd.isWoundHealed(ConstantsTest.MSSQL_WOUND_ID,7);
            assertFalse("Should be healed",h);
        } catch (ApplicationException ex) {
        }
           
    }
    public void testAssessmentExists() throws ApplicationException{
        //checkAssessmentExistForProfId
    }
    
    
    
    public void testSaveImage() throws ApplicationException{
        AssessmentImagesVO i = new AssessmentImagesVO();
        i.setActive(1);
        i.setAlpha_id(ConstantsTest.MSSQL_ALPHA_ID_WOUND);
        i.setAssessment_id(1);i.setImages("A:1{\"image.jpg\"}");
        i.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        i.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        i.setWound_profile_type_id(ConstantsTest.MSSQL_WOUND_TYPE);
        i.setWhichImage(1);
        ibd.saveImage(i);
        AssessmentImagesVO im = new AssessmentImagesVO();
        im.setAssessment_id(1);
        im.setAlpha_id(ConstantsTest.MSSQL_ALPHA_ID_WOUND);
        im.setWhichImage(1);
        AssessmentImagesVO i1 = ibd.getImage(im);
        assertNotNull("AssessmentImages should not be null",i1);
        
        //AssessmentImagesVO i2 = ibd.getImage("1");
        //assertNotNull("AssessmentImages should not be null",i2);
        
        AssessmentImagesVO im3 = new AssessmentImagesVO();
        im3.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        Collection i3 = ibd.getAllImages(im3);
        assertNotNull("AssessmentImages should not be null",i3);
        assertEquals("Shold be 1 image",1,i3.size());
        
        
        AssessmentImagesVO i2 = new AssessmentImagesVO();
        i2.setAssessment_id(1);
        i2.setAlpha_id(ConstantsTest.MSSQL_ALPHA_ID_WOUND);
        Collection i4 = ibd.getAllImages(i2);//, wound_id)
        assertNotNull("AssessmentImages should not be null",i4);
        assertEquals("Shold be 1 image",1,i4.size());
        
        /*ibd.removeImages("10590","4123");//, alpha_id);
        Collection i5 = ibd.getAllImages(i2);//, wound_id)
        assertEquals("Shold be 0 image",0,i5.size());
        i.setId(null);
        ibd.saveImage(i);
        */
        //Retrieve all assessments iwth atelast 1 image
            //Vector imass = ibd.retrieveAssessmentDates("2287","2287");
            //assertEquals("Should find 1 assessment",1,imass.size());
            
            //Vector imass2 = ibd.getImageList("1", 1);
            //assertEquals("Should find 1 assessment",1,imass2.size());
            
            //Vector assessmentSigs = bd.getAllAssessmentSignaturesByAlpha(assessmentEachwoundVO, 0);
            //assertEquals("should find 1 assessment",1, assessmentSigs.size());
    }

        
}