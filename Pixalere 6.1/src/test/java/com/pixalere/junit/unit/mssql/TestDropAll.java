/*
 * Copyright (c) 2005 WebMed Technology Inc. All Rights Reserved.
 */
package com.pixalere.junit.unit.mssql;

import junit.framework.TestCase;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.dao.PatientProfileDAO;
import com.pixalere.patient.dao.PatientDAO;
import com.pixalere.common.ApplicationException;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.wound.dao.WoundProfileDAO;

import java.util.Vector;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: travismorris
 * Date: Jul 4, 2005
 * Time: 9:46:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestDropAll extends TestCase {
    private PatientServiceImpl bd =new PatientServiceImpl(com.pixalere.utils.Constants.ENGLISH);
    private WoundProfileDAO daow = new WoundProfileDAO();
    private PatientProfileDAO dao = new PatientProfileDAO();
    private static Vector patientv = new Vector();
    public void tearDown(){
        bd=null;
        daow=null;
        dao=null;
        patientv=null;
    }
    protected void setUp() throws ApplicationException {
        try{
        PatientDAO dao = new PatientDAO();
        PatientAccountVO vo=new PatientAccountVO();
        Collection col = dao.findAllByCriteria(vo,false);
        Iterator iter = col.iterator();
        while (iter.hasNext()) {
            PatientAccountVO acc = (PatientAccountVO) iter.next();
            patientv.add(acc.getPatient_id());
        }
        }catch(Exception e) {

        }
    }

    public void testRemovePatient() throws ApplicationException {
        for (int x = 0; x < patientv.size(); x++) {
            int patient = ((Integer)patientv.get(x)).intValue();
            bd.removePatient(patient+"",0);
            try {
                PatientAccountVO account2 = bd.getPatient(patient);
                assertEquals("Patient should be inactive", new Integer(0),account2.getAccount_status());
            } catch (ApplicationException ex) {
                ex.printStackTrace();
            }
        }
    }

    /*public void testDropTMPProfile() {
        for (int x = 0; x < patientv.size(); x++) {
            int patient = ((Integer)patientv.get(x)).intValue();
            bd.dropTMP(patient + "");
        }
    }

    public void testDeleteProfile() {
        for (int x = 0; x < patientv.size(); x++) {
            int patient = ((Integer)patientv.get(x)).intValue();
            PatientProfileVO profile3 = new PatientProfileVO();
            profile3.setPatient_id(new Integer(patient));
            dao.delete(profile3);
        }
    }

    public void testDropTMPWound() {
        for (int x = 0; x < patientv.size(); x++) {
            int patient = ((Integer)patientv.get(x)).intValue();
            daow.deleteTMP(patient + "");
        }
    }

    public void testDropWound() {
        for (int x = 0; x < patientv.size(); x++) {
            int patient = ((Integer)patientv.get(x)).intValue();
            WoundProfileVO vo = new WoundProfileVO();
            vo.setPatient_id(new Integer(patient));
            daow.delete(vo);
        }
    }  */
}
