package com.pixalere.junit.unit.mssql;
import java.util.Collection;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.bean.WoundVO;
import java.util.Vector;
import com.pixalere.ConstantsTest;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.common.ApplicationException;
import com.pixalere.common.DataAccessException;
import com.pixalere.patient.bean.PatientAccountVO;
import com.pixalere.patient.service.PatientServiceImpl;
import com.pixalere.assessment.service.*;
import com.pixalere.assessment.bean.NursingCarePlanVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.guibeans.RowData;
import com.pixalere.utils.*;
import junit.framework.TestCase;
import java.util.Collection;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Date;

public class TestWounds extends TestCase {
    private PatientServiceImpl bd;
    private NursingCarePlanServiceImpl nbd;
    
    private WoundProfileVO wmp;
    private WoundProfileVO wmpTMP;

    protected void tearDown(){
        bd=null;
        wmpTMP=null;
        wmp=null;
        
    }
    protected void setUp() throws ApplicationException{
        try{
            com.pixalere.patient.dao.PatientProfileDAO dao = new com.pixalere.patient.dao.PatientProfileDAO();
            ProfessionalVO userVO = new ProfessionalVO();
            nbd=new NursingCarePlanServiceImpl();
            userVO.setId(new Integer(7));
            
            //assertEquals("Patient "+col.size()+"should exist"+patient,new Integer(col.size()),new Integer(2));
            initializeWoundTMP();
        }catch(Exception e){}
        
    }
    public void testNewWound() throws ApplicationException{
        WoundServiceImpl bd=new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        ProfessionalVO userVO = new ProfessionalVO();
        userVO.setId(new Integer(7));
        WoundVO woundVO = new WoundVO();
        woundVO.setWound_location("myWound");
        woundVO.setImage_name("image.jpg");
        woundVO.setWound_location_detailed("");
        woundVO.setWound_location_position("");
        woundVO.setLocation_image_id(new Integer(0));
        woundVO.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        Date start_date = new Date();
        String startdate_signature = new PDate().getProfessionalSignature(new Date(), userVO,"en");
                woundVO.setStart_date(start_date);
                woundVO.setStartdate_signature(startdate_signature);
                woundVO.setProfessional_id(userVO.getId());
                woundVO.setActive(new Integer(1));
              int  wound_id = bd.getWoundId(woundVO);
                
        com.pixalere.patient.dao.PatientProfileDAO dao = new com.pixalere.patient.dao.PatientProfileDAO();
        
        boolean bool=false;
        if(wound_id!=0 || wound_id !=-1)bool=true;
        assertTrue("Wound id should exist."+wound_id,bool);
        bd.saveWoundProfile(wmpTMP);
        
    }
    
    public void testSaveNCP() throws ApplicationException{
        AssessmentServiceImpl bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        NursingCarePlanVO newncp=new NursingCarePlanVO();
                newncp.setActive(1);
                newncp.setAssessment_id(-1);
                newncp.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
                newncp.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
                newncp.setProfessional_id(7);
                newncp.setCreated_on(new Date());
                newncp.setUser_signature(new PDate().getProfessionalSignature(new Date(),new ProfessionalVO(7),"en"));
                newncp.setBody("ncp");
                newncp.setWound_profile_type_id(1);
                
                nbd.saveNursingCarePlan(newncp);
                newncp.setId(null);
                NursingCarePlanVO ncp = nbd.getNursingCarePlan(newncp);
                assertNotNull("NursingCarePlan was added",ncp);
                assertEquals("Nursing Care Plan should be 'ncp'","ncp",ncp.getBody());
    }
    public void testGetAllNCP() throws ApplicationException{
        AssessmentServiceImpl bd = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        NursingCarePlanVO newncp=new NursingCarePlanVO();
        newncp.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        Collection<NursingCarePlanVO> results = nbd.getAllNursingCarePlans(newncp,0);
        assertEquals("Should be 16 NCP","1",results.size()+"");
    }
    public void testFindWoundProfiles() throws ApplicationException{
        WoundServiceImpl bd=new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        ProfessionalVO userVO = new ProfessionalVO();
        userVO.setId(7);
        WoundProfileVO crit=new WoundProfileVO();
        crit.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
            
        Collection<WoundProfileVO> items= bd.getWoundProfiles(crit,0);
           
        RowData[] result = bd.getAllWoundProfilesForFlowchart(items,userVO,false,false);
        boolean tmp=false;
        if(result.length>0){tmp=true;}
        assertTrue("Wound Profiles must exist.",tmp);
    }
    
    public void testWoundProfilesDrop() throws ApplicationException{
        WoundServiceImpl bd=new WoundServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        Vector result = bd.getWoundProfilesDropdown(ConstantsTest.MSSQL_PATIENT_ID,true,7);
        boolean tmp=false;
        if(result.size()>0){tmp=true;}
        assertTrue("Wound Profiles must exist.",tmp);
    }
    
    /*public void testDate(){
        WoundProfileVO profile=new WoundProfileVO();
        profile.setCns_date("1058338800");//7/16/2003
        Vector result = profile.getCNSDate();
        assertEquals("Month should be 7.","7",(String)result.get(0));
        assertEquals("Day should be 16.","16",(String)result.get(1));
        assertEquals("Year should be 2003.","2003",(String)result.get(2));
    }*/
    public void initializeWoundTMP()  throws com.pixalere.common.ApplicationException{
        String[] arr={"other"};
        wmpTMP=new WoundProfileVO();
        WoundVO woundVO=new WoundVO();
        wmpTMP.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        wmpTMP.setPatient_id(ConstantsTest.MSSQL_PATIENT_ID);
        woundVO.setId(ConstantsTest.MSSQL_WOUND_ID);
        woundVO.setWound_location("Foot");
        woundVO.setProfessional_id(new Integer(7));
        wmpTMP.setActive(1);
        //wmpTMP.setEtiology(Serialize.serialize(arr));
        //wmpTMP.setWound_etiology_other("other");
        wmpTMP.setPressure_ulcer(new Integer(1));
        //wmpTMP.setGoals("");
        wmpTMP.setProfessional_id(new Integer(7));
        
        /*wmpTMP.setCns_show(new Integer(0));
        
        wmpTMP.setCns_name("name");
        wmpTMP.setCns_duration_start(new PDate().getDate(1, 1, 1999));
        wmpTMP.setCns_duration_end(new PDate().getDate(1, 1, 1999));*/
         woundVO.setImage_name("foot.jpg");
        wmpTMP.setPrev_prods("test");
        woundVO.setStart_date(new Date());
        
        //wmpTMP.setAntibiotics("");
        //wmpTMP.setCns_date(new PDate().getDate(1, 1, 1));
        //wmpTMP.setCns_route(new Integer(1));
        wmpTMP.setUser_signature("user");
        wmpTMP.setCurrent_flag(new Integer(1));
        wmpTMP.setActive(new Integer(1));
        woundVO.setStartdate_signature("user");
        woundVO.setActive(new Integer(1));
        wmpTMP.setWound_id(ConstantsTest.MSSQL_WOUND_ID);
        woundVO.setWound_location_detailed("Left Foot");
        wmpTMP.setWound(woundVO);
    }
}