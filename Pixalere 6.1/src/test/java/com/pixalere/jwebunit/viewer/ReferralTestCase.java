package com.pixalere.jwebunit.viewer;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import com.pixalere.common.service.ReferralsTrackingServiceImpl;
import com.pixalere.guibeans.ReferralsList;
import net.sourceforge.jwebunit.html.Cell;
//import net.sourceforge.jwebunit.html.ExpectedTable;
import net.sourceforge.jwebunit.html.Table;
import com.pixalere.utils.Constants;
import java.util.Vector;
import com.pixalere.auth.bean.ProfessionalVO;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class ReferralTestCase  {
    //private final String CONTEXT="/LoginS";
    ReferralsTrackingServiceImpl dao = new ReferralsTrackingServiceImpl(com.pixalere.utils.Constants.ENGLISH);
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        
    }
    
    
    @Test
    public void testCase10dot11_ReferralExists(){
        
        assertTextPresent("Nurses' Request for Referral");
        String date="";
        String date2="";
        String date3="";
        String date4="";String date5="";
       
    
          
        //assertTableEquals("referralTable",table);
        clickButton("referral1");
        //verify abdomen is selected
        //System.out.println(getPageSource());
        setWorkingForm("woundform");
        assertSelectedOptionEquals("wound_profile_type_id","Abdomen - Ostomy");
        //clickLink("commentsLink");
        setWorkingForm("commentsform1");
        //assertSelectedOptionEquals("assessment_id",date);
        //verify correct comment
        setTextField("comments_recommendations","Ostomy Comment");
        submit();
        assertTextPresent("Ostomy Comment");
        
        clickLink("ref_popup");
        
        clickButton("referral4");
        setWorkingForm("woundform");
        assertSelectedOptionEquals("wound_profile_type_id","Chest - Wound");
        //System.out.println(getPageSource());
        //clickLink("commentsLink");
        //verify correct comment
        setWorkingForm("commentsform1");
        
        setTextField("comments_recommendations","Wound Comment");
        submit();
        assertTextPresent("Wound Comment");
        clickLink("ref_popup");
        
        clickButton("referral1");
        setWorkingForm("woundform");
        assertSelectedOptionEquals("wound_profile_type_id","Head/neck (Anterior) - Tube/Drain");
        //clickLink("commentsLink");
        setWorkingForm("commentsform1");
        //assertSelectedOptionEquals("assessment_id",date);
        //verify correct comment
        setTextField("comments_recommendations","Drain Comment");
        submit();
        //verify correct comment
        assertTextPresent("Drain Comment");
        clickLink("ref_popup");
        
        clickButton("referral1");
        setWorkingForm("woundform");
        assertSelectedOptionEquals("wound_profile_type_id","Head/neck (Anterior) - Incision");
        //clickLink("commentsLink");
        setWorkingForm("commentsform1");
        //assertSelectedOptionEquals("assessment_id",date);
        //verify correct comment
        setTextField("comments_recommendations","Incision Comment");
        submit();
        //verify correct comment
        assertTextPresent("Incision Comment");
        clickLink("ref_popup");

        clickButton("referral1");
    }
    @Test
    public void testCase10dot14_Ack(){
        clickLink("navLogout");
        setTextField("userId","1818");
        setTextField("password","donthugth@3");
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();

        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        clickLink("commentsLink");
        clickButton("ack1442");
        clickButton("ack1439");
        assertTextNotPresent("PN/Recommendation Acknowledged");
        //gotoWindow("window_name");
        //change prof
        //check recommendation popup
        //check comments
        
        //enter recommendation
        //logout repeat with 1 more
        //change position type to nurse
    }
     @After
    public void close() {
        closeBrowser();
    }
    
}
