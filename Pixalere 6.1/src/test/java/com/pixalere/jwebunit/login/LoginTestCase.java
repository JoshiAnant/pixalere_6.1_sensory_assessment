/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.login;

import com.pixalere.ConstantsTest;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

public class LoginTestCase  {
    //private final String CONTEXT="/LoginS";
    
   /* public java.util.Properties loadProperties(){
        java.util.Properties properties = new java.util.Properties();
        boolean found = true;
        try {
            properties.load(new java.io.FileInputStream("config/local.nolongerneeded"));
        } catch (java.io.FileNotFoundException ex){
           // ex.printStackTrace();
            found=false;
        } catch (java.io.IOException e){
            found=false;
            e.printStackTrace();
        } catch(Exception e){
            found=false;
            e.printStackTrace();
        }
        if(found == false){
            try {
                properties.load(new java.io.FileInputStream("config/build.properties"));
            } catch (java.io.FileNotFoundException ex){
                
            } catch (java.io.IOException e){
                found=false;
            }
        }
        return properties;
    }*/
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        
        //getTestContext().setBaseUrl(app);
        //getTestContext().setResourceBundleName("ApplicationResources");
        
        //setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        // or setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);
    }
    @Test
    public void testSuccessfulLogin() {
        System.out.println("1");
        beginAt("/LoginSetup.do");
        System.out.println("12");
        assertTitleEquals("Pixalere Login");
        System.out.println("13");
        assertElementPresent("userId");
        System.out.println("14");
        assertElementPresent("password");
        System.out.println("15");
        //assertElementPresent("database");
        setTextField("userId","7");
        System.out.println("16");
        setTextField("password",ConstantsTest.PASSWORD);
        System.out.println("17");
        submit();
        System.out.println("18");
        assertTitleEquals("Pixalere Wound Care Solution");
        System.out.println("19");
        //System.out.println(getPageSource());
        clickLink("navLogout");
    }
    @Test
    public void testFailedLogins() {
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","skywalker89");
        submit();
        
        assertTitleEquals("Pixalere Login");
        //assertTitleEqualsKey("pixalere.main.form.title");
        //clickLink("navLogout");
    }
    @Test
    public void testFailedEmptyLogins() {
        beginAt("/LoginSetup.do");
        setTextField("userId","");
        setTextField("password","");
        submit();
        
        assertTextPresent(ConstantsTest.USERNAME+" and/or Password may be empty");
        
        //assertTitleEqualsKey("pixalere.main.form.title");
        //clickLink("navLogout");
    }
    @After
    public void close() {
        closeBrowser();
    }
}
