/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.login;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

public class UpgradeTestCase {
  
   @Before
    public void prepare(){
        //String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        setBaseUrl("http://localhost:8080/pixalere");
        
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        // or setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);
    }
   @Test
    public void testUpgrade() {
        beginAt("/LoginSetup.do");
        //System.out.println(getPageSource());
        assertTextPresent("version mismatch");
        //setWorkingForm("start");
        setTextField("password","cyclo.carbon1!");
        submit();
        assertTextPresent("User Number");
    }
    @After
    public void close() {
        closeBrowser();
    }
}
