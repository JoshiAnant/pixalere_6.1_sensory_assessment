/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class OstomyProfileTestCase  {
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navPatient");
        clickLink("navWound");
    }
    
    
    @Test
    public void testCase3dotx_createOstomyProfile(){
        
        setWorkingForm("woundform");
        selectOption("wound_id","New Wound Profile");
        //submit();
        assertElementPresent("anterior");
        assertElementPresent("posterior");
        
        selectOption("anterior","Abdomen");
        clickRadioOption("rad_anterior","R");
        //assertElementPresent("etiology1");
        
        
        //checkCheckbox("ostomy_etiology","923");
       setTextField("reason_for_care","Reason");
        
        
        clickLink("selectOstf");
        clickLink("alpha106");
        clickLink("selectOstu");
        clickLink("alpha167");
        clickLink("navSummary");
        assertTextPresent("Etiology is required");//test case 3.5
        assertTextPresent("Goals is required");//test case 3.5
        String[] a ={"Urostomy","Fecal Stoma"};
        selectOptions("goals_alphas",a);
        selectOption("goals_list","To be detemined - ostomy");
        clickButton("addgoals");
        /*selectOption("goals_alphas","Fecal Stoma");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");*/
        
        selectOptions("etiology_alphas",a);
        selectOption("etiology_list","Trauma");
        clickButton("addetiology");
        
        String[] g2 = {"Fecal Stoma","Urostomy"};
        assertSelectOptionsEqual("goals_alphas",g2);
        
        String[] g3 = {"Fecal Stoma","Urostomy"};
        assertSelectOptionsEqual("etiology_alphas",g3);
        
        /*
        selectOption("etiology_alphas","Urostomy");
        selectOption("etiology_list","Trauma");
        
        clickButton("addetiology");*/
        
        String[] et2 = {"Fecal Stoma: Trauma","Urostomy: Trauma"};
        assertSelectOptionsEqual("etiology",et2);
        checkCheckbox("type_of_ostomy","1010");
        setTextField("operative_procedure_comments","Operative Procedure Comments");
        selectOption("date_surgery_day","1");
        selectOption("date_surgery_month","Jul");
        setTextField("date_surgery_year","1977");
        setTextField("surgeon","Dr. Smithers");
        clickRadioOption("marking_prior_surgery","1");
        checkCheckbox("patient_limitations","1513");
        selectOption("teaching_goals","Patient independent with ostomy care");
//clickLink("img167");
        clickLink("navSummary");
        //check list to verify items
        clickButton("saveEntries");
        gotoRootWindow();
    }
    @Test
    public void testCase3dot23_verifyOstomyProfile(){
        setWorkingForm("woundform");
        //System.out.println(getPageSource());
        selectOption("wound_id","Abdomen");
        //submit();
        assertSelectedOptionEquals("anterior","Abdomen");
        //assertRadioOptionSelected("cns_done","1");
        //assertCheckboxSelected("ostomy_etiology","923");
        assertTextFieldEquals("reason_for_care","Reason");
        assertTextFieldEquals("operative_procedure_comments","Operative Procedure Comments");
        assertSelectedOptionEquals("date_surgery_day","1");
        assertSelectedOptionEquals("date_surgery_month","Jul");
        assertTextFieldEquals("date_surgery_year","1977");
        assertTextFieldEquals("surgeon","Dr. Smithers");
        assertRadioOptionSelected("marking_prior_surgery","1");
        assertCheckboxSelected("patient_limitations","1513");
        assertSelectedOptionEquals("teaching_goals","Patient independent with ostomy care");
        //check antiboitics
        
    }
    @After
    public void close() {
        closeBrowser();
    }
}
