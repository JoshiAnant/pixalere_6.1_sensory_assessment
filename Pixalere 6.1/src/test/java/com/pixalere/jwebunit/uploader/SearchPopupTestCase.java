package com.pixalere.jwebunit.uploader;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class SearchPopupTestCase  {
    //private final String CONTEXT="/LoginS";
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","donthugth@3");
        submit();
        
        
    }
    
    //PIX-906
    @Test
    public void testSearchPatient(){
        setWorkingForm("searchN");
        clickButton("searchPatient");
        gotoWindow("searchPopup");
        setWorkingForm("form");
        setTextField("patient_id","3");
        clickButton("searchButton");
        assertTextPresent("Pixalere ID# 3");
        assertTextPresent("Sample Office");
        
    }
    @After
    public void close() {
        closeBrowser();
    }
}