/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;

import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class PostOpProfileTestCase {
    //private final String CONTEXT="/LoginS";
   
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        //clickLink("navPatient");
        clickLink("navWound");
    }
    
    
    @Test
    public void testCase3dotx_createWoundProfile(){
        setWorkingForm("woundform");
        selectOption("wound_id","New Wound Profile");
        //submit();
        assertElementPresent("anterior");
        assertElementPresent("posterior");
        
        selectOption("anterior","Head and neck");
        clickRadioOption("rad_anterior","R");
        

        clickLink("selectDrain");
        clickLink("alpha167");
        clickLink("alpha40");
        clickLink("selectintag");
        clickLink("alpha106");
        clickLink("navSummary");
        assertTextPresent("Etiology is required");//test case 3.5
        assertTextPresent("Goals is required");//test case 3.5
        //clickRadioOption("cns_done","1");
       
        
        setTextField("reason_for_care","Reason");
        String[] a={"Tube/Drain 1","Tube/Drain 2","Incision 1"};
        selectOptions("goals_alphas",a);
        selectOption("goals_list","Temporary tube/drain");
        clickButton("addgoals");
        /*selectOption("goals_alphas","Tube/Drain 2");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");
        selectOption("goals_alphas","Incision 1");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");*/
        //String[] gt = {"Wound Drain or Tube 1: To maintain wound","Wound Drain or Tube 2: To maintain wound","Wound Incision Tag 1: To maintain wound"};
        //assertSelectOptionsEqual("goals",gt);
        
        selectOptions("etiology_alphas",a);
        selectOption("etiology_list","Obstruction");
        clickButton("addetiology");
        
       /*
        selectOption("etiology_alphas","Tube/Drain 1");
        selectOption("etiology_list","Obstruction");
        clickButton("addetiology");
        
        selectOption("etiology_alphas","Tube/Drain 2");
        selectOption("etiology_list","Obstruction");
        clickButton("addetiology");*/
        
        setTextField("operative_procedure_comments","Operative Procedure Comments");
        selectOption("date_surgery_day","1");
        selectOption("date_surgery_month","Jul");
        setTextField("date_surgery_year","1977");
        setTextField("surgeon","Dr. Smithers");
        //String[] et = {"Wound Incision Tag 1: Malignant","Wound Drain or Tube 1: Obstruction","Wound Drain or Tube 2: Obstruction"};
        //assertSelectOptionsEqual("etiology",et);
        
 
       
        clickLink("navSummary");
        //check list to verify items
        clickButton("saveEntries");
        gotoRootWindow();
    }
    
  @After
    public void close() {
        closeBrowser();
    }
}
