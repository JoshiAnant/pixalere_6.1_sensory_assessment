/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import com.pixalere.utils.PDate;
public class OstomyAssessmentTestCase  {
    //private final String CONTEXT="/LoginS";
   
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; FDM; .NET CLR 2.0.50727; .NET CLR 1.1.4322)");
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Abdomen");

        //PIX-1021 - Add alpha so its inactive on point of assessment completion to test wound healed check on summary
        
        
    }
    @Test
    public void testCase5dotx_assessments(){
        
        clickLink("navAssessment");
        int count=0;
        //for(String alpha : WOUND_ALPHAS){
        count++;
       
        setWorkingForm("void");
        assertElementPresent("cancel1");
        checkCheckbox("cancel");
        System.out.println("2");
        //clickLink("alphaOstu");
       
        clickButton("bgbtnostu1");
setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        setWorkingForm("form");
        setWorkingForm("form");
        
        assertElementPresent("clinical_path_date_day");
        assertElementPresent("clinical_path_date_month");
        assertElementPresent("clinical_path_date_year");
        
        assertElementPresent("urine_colour1");
        assertElementPresent("urine_type1");
        assertElementPresent("urine_quantity_1");
        //assertElementPresent("stool_colour");
        //assertElementPresent("stool_consistency");
        //assertElementPresent("stool_quantity");
        //assertElementPresent("stool_quantity_other");
        
        assertElementPresent("shape_1");
        
        assertElementPresent("pouching1");
        assertElementPresent("appearance1");
        assertElementPresent("contour1");
        
        assertElementPresent("devices_1");
        
        
        System.out.println("1");
        //clinical path
        clickRadioOption("clinical_path","1");

        System.out.println("got this far...");
        selectOption("clinical_path_date_day","1");
        selectOption("clinical_path_date_month","Jul");
        setTextField("clinical_path_date_year","1977");
        //selectOption("circumference_mm","2");
        selectOption("length_mm","3");
        selectOption("width_mm","4");
        clickRadioOption("construction","1369");
   
        clickRadioOption("shape" ,"1158");
        checkCheckbox("profile","1476");
        checkCheckbox("pouching","1542");
        checkCheckbox("appearance","1355");
        checkCheckbox("contour","1021");
        clickRadioOption("devices","1541");
        checkCheckbox("mucocutaneous_margin","1395");
        selectOption("mucocutaneous_margin_num","1");
        //System.out.println(getPageSource());
        System.out.println("-----------------------------------------");
        clickButton("okm");
        System.out.println("got this far...");
        //System.out.println(getPageSource());
        selectOption("mucocutaneous_margin_start_1","3:00");selectOption("mucocutaneous_margin_end_1","3:00");
        selectOption("mucocutaneous_margin_cm_1","3");selectOption("mucocutaneous_margin_mm_1","3");
        setTextField("mucocutaneous_margin_comments","mucocutaneous margin comments");
        checkCheckbox("periostomy_skin","1023");
        //selectOption("status","Active");
        assertSelectedOptionEquals("length_mm","3");
        assertElementPresent("mucous_fistula_present_show1");
        setWorkingForm("form");
        clickRadioOption("mucous_fistula_present_show","1");//mucous_fistula_present_show
       checkCheckbox("drainage","1374");
        setTextField("drainage_comments","testing 1 2 3");
        checkCheckbox("perifistula_skin","1379");
        
        System.out.println("1");
        checkCheckbox("urine_colour","1026");
        checkCheckbox("urine_type","1596");
        clickRadioOption("urine_quantity","1028");
        //checkCheckbox("stool_colour","892");
        //checkCheckbox("stool_consistency","888");
        //selectOptionByValue("stool_quantity","887");
        //setTextField("stool_quantity_other","test");
        //selectOptionByValue("stool_discharge","886");
        checkCheckbox("self_care_progress","1017");
        clickRadioOption("flange_pouch","1043");
        setTextField("self_care_progress_comments","scpc");
        setTextField("flange_pouch_comments","fpc");
        clickRadioOption("nutritional_status","1486");
        setTextField("nutritional_status_other","Nutritional Status Other");
        
        //setTextField("pouch_selection_other","test");
        System.out.println("1metoo");
        //}
         clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (Fecal Stoma 1). Click Stay to remain on the assessment screen, or Go to proceed");
        
        clickButton("go_confirm");
        
        //PIX-925
        setWorkingForm("form");
        selectOption("dressing_change_frequency_alphas","Urostomy");
        selectOption("dressing_change_frequency_list","BID");
        clickButton("adddressing_change_frequency");
        clickLink("navAssessment");

        
        setWorkingForm("form");
        assertRadioOptionSelected("devices","1541");
        //PIX-921
        
        clickLink("navTreatment");
        assertTextPresent("There are unfinished assessments (Fecal Stoma 1). Click Stay to remain on the assessment screen, or Go to proceed");
        clickButton("go_confirm");
        String[] assess = {"Urostomy"};
        assertSelectOptionsEqual("wound_ass",assess);
        //End of PIX-925
        System.out.println("1");
        selectOption("products","Apron");
        
        setTextField("ostomy_treatment_comments","comments\ncomments");
        setTextField("ostomy_nursing_care_plan","nursingcareplan");
        selectOption("wound_ass","Urostomy");
        clickButton("addAlpha");
        //clickRadioOption("vac_show","1");
        System.out.println("1");
        
        //vac date
        /*setTextField("vacstart_year","1977");
        selectOption("vacstart_month","Jul");
        selectOption("vacstart_day","26");
        setTextField("vacend_year","1977");
        selectOption("vacend_month","Jul");
        selectOption("vacend_day","26");
        selectOption("pressure_reading","25");
        clickRadioOption("reading_group","1013");*/
        clickRadioOption("cs_show","2");
        clickRadioOption("treatmentmodalities_show","2");
        selectOption("csresult_list","MRSA");
        //System.out.println(getPageSource());
        
        selectOption("csresult_alphas","Urostomy");
        clickButton("addcsresult");
        
        setTextField("cns_name","drug A");
        selectOption("cns_start_day","26");
        setTextField("cns_start_year","1977");
        selectOption("cns_start_month","Jul");
        selectOption("cns_end_day","26");
        setTextField("cns_end_year","1977");
        selectOption("cns_end_month","Jul");
        selectOption("antibiotic_alphas","Urostomy");
        clickButton("addAnti");
        checkCheckbox("adjunctive","183");
        //PIX-1145
        //clickButton("save");
        setWorkingForm("form");
        assertTextFieldEquals("ostomy_nursing_care_plan","nursingcareplan");
        assertTextFieldEquals("ostomy_treatment_comments","comments\ncomments");
        System.out.println("1");
        /*assertTextFieldEquals("vacstart_year","1977");//need to hide vac for non wounds.
        assertSelectedOptionEquals("vacstart_day","26");
        assertSelectedOptionEquals("vacstart_month","Jul");
        assertTextFieldEquals("vacend_year","1977");
        assertSelectedOptionEquals("vacend_day","26");
        assertSelectedOptionEquals("vacend_month","Jul");
        assertSelectedOptionEquals("pressure_reading","25");
        assertRadioOptionSelected("reading_group","1013");*/
        String[] et = {"Urostomy: MRSA"};
        assertSelectOptionsEqual("csresult",et);
        String[] anti = {"Urostomy: drug A : 26/7/1977 to 26/7/1977"};
        assertSelectOptionsEqual("antibiotics",anti);
        
        clickLink("navSummary");
        //gotoWindow("infowindow");
        //System.out.println(getPageSource());
        setTextField("body_ostomy","Ostomy Comment");
        clickButton("saveEntries");
        //System.out.println("Going to Summary: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        System.out.println("1");
        //
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_ostomy","y");
        selectOption("priority_ostomy","Urgent (24 - 48hrs)");
        clickButton("saveEntries");
        gotoRootWindow();
        //System.out.println("Assessment Ssaved: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        System.out.println("1");
    }/*
    public void testVerifyAssessment_A(){
        clickLink("navAssessment");
        setWorkingForm("void");
        checkCheckbox("cancel");
        clickLink("alphaOstu");
        //clickLink("alphaOstu");
        setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        setWorkingForm("form");
       
        
        assertRadioOptionSelected("clinical_path","0");
        assertSelectedOptionEquals("clinical_path_date_day","1");
        assertSelectedOptionEquals("clinical_path_date_month","Jul");
        assertTextFieldEquals("clinical_path_date_year","1977");
        assertSelectedOptionEquals("circumference_mm","2");
        assertSelectedOptionEquals("length_mm","3");
        assertSelectedOptionEquals("width_mm","4");
        assertRadioOptionSelected("construction","1369");
   
        assertRadioOptionSelected("shape" ,"1158");
        assertCheckboxSelected("profile","1476");
        assertCheckboxSelected("pouching","1542");
        assertCheckboxSelected("appearance","1355");
        assertCheckboxSelected("contour","1021");
        assertRadioOptionSelected("devices","1541");
        assertCheckboxSelected("mucocutaneous_margin","1395");
        assertSelectedOptionEquals("mucocutaneous_margin_num","1");
       
        assertSelectedOptionEquals("mucocutaneous_margin_start_1","3:00");assertSelectedOptionEquals("mucocutaneous_margin_end_1","3:00");
        assertSelectedOptionEquals("mucocutaneous_margin_cm_1","3");assertSelectedOptionEquals("mucocutaneous_margin_mm_1","3");
        assertTextFieldEquals("mucocutaneous_margin_comments","mucocutaneous margin comments");
        assertCheckboxSelected("periostomy_skin","1023");
        assertSelectedOptionEquals("status","Active");
        assertRadioOptionSelected("mucous_fistula_present_show","1");
       assertCheckboxSelected("drainage","1374");
        assertTextFieldEquals("drainage_comments","testing 1 2 3");
        assertCheckboxSelected("perifistula_skin","1379");
        
        System.out.println("1");
        assertCheckboxSelected("urine_colour","1026");
        assertCheckboxSelected("urine_type","1596");
        assertRadioOptionSelected("urine_quantity","1028");
        assertCheckboxSelected("self_care_progress","1017");
        assertRadioOptionSelected("flange_pouch","1043");
        assertTextFieldEquals("self_care_progress_comments","scpc");
        assertTextFieldEquals("flange_pouch_comments","fpc");
        assertRadioOptionSelected("nutritional_status","1486");
        assertTextFieldEquals("nutritional_status_other","Nutritional Status Other");
        //TODOassertTextFieldEquals("pouch_selection_other","test");
        
        
        clickLink("navTreatment");
        gotoWindow("infowindow");
        
        assertTextPresent("There are unfinished assessments (Fecal Stoma 1). Click Stay to remain on the assessment screen, or Go to proceed");
        clickButton("cancel");
        gotoRootWindow();
       // System.out.println("Treatment doing: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        //System.out.println(getPageSource());
        selectOption("products","30cc syringe");
        //PIX-1507
        assertTextFieldEquals("ostomy_treatment_comments","");
        assertTextFieldEquals("ostomy_nursing_care_plan","nursingcareplan");
        setTextField("ostomy_treatment_comments","comments");
        selectOption("wound_ass","Urostomy 1");
        clickButton("addAlpha");
        clickLink("navSummary");
        clickButton("saveEntries");
        //System.out.println("Going to Summary: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        //
        gotoWindow("infowindow");
        assertTextPresent("Information Popup ** Attention ** Patient requires referral to Wound Care Clinician is a required field. You must select Yes or No.");
        clickLink("closeWindow");
        gotoRootWindow();
        clickRadioOption("referral_ostomy","n");
        checkCheckbox("send_message_ostomy");
        assertTextPresent("Send Message to WCC");
        //selectOption("priority","");
        clickButton("saveEntries");
        gotoRootWindow();
        gotoWindow("infowindow");
        //System.out.println("Assessment Ssaved: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        
    }*/
    @After
    public void close() {
        closeBrowser();
    }
}
