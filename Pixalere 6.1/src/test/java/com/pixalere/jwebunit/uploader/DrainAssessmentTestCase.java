/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;
import net.sourceforge.jwebunit.api.IElement;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.common.ApplicationException;
import com.pixalere.utils.Constants;
import com.pixalere.utils.PDate;
import java.util.Date;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class DrainAssessmentTestCase {
    //private final String CONTEXT="/LoginS";
    private String[] WOUND_ALPHAS={"A","B","C","D"};
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; FDM; .NET CLR 2.0.50727; .NET CLR 1.1.4322)");
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navWound");
        //System.out.println(getPageSource());
        setWorkingForm("woundform");
        selectOption("wound_id","Head/neck (Anterior)");

        clickLink("navAssessment");
    }
    @Test
    public void testCase7dotx_assessments() throws ApplicationException{
        int count=0;
        setWorkingForm("void");
        checkCheckbox("cancel");
        clickButton("bgbtntd1");
        
        assertElementPresent("type_of_drain");
        assertElementPresent("type_of_drain_other");
        assertElementPresent("drain_removed_date_month");
        assertElementPresent("drain_removed_date_day");
        assertElementPresent("drain_removed_date_year");
       // assertElementPresent("discharge_reason");
        assertElementPresent("peri_drain_area1");
        assertElementPresent("drain_removed_intact1");
        assertElementPresent("sutured1");
        assertElementPresent("drain_site1");
        assertElementPresent("drainage_num");
        assertElementPresent("tubes_changed_date_month");
        assertElementPresent("tubes_changed_date_day");
        assertElementPresent("tubes_changed_date_year");
        
        selectOption("type_of_drain","Jackson Pratt drain");
        setTextField("type_of_drain_other","tod other");
        clickRadioOption("drain_removed_intact","2");
        
        //assertRadioOptionSelected("drain_removed_intact","1");
        selectOption("status","Active");
        //selectOption("drain_removed_date_month","Jul");
        //setTextField("drain_removed_date_year","1977");
        //selectOption("drain_removed_date_day","1");
        //selectOption("discharge_reason","Drain fell out");
        clickRadioOption("sutured","2");
        checkCheckbox("drain_site","1129");
        selectOption("tubes_changed_date_month","Jul");
        setTextField("tubes_changed_date_year","1977");
        selectOption("tubes_changed_date_day","1");
        clickRadioOption("cs_date_show","1");
        checkCheckbox("peri_drain_area","1099");
        
        
        //PIX-1455
        selectOption("drainage_num","3");
        clickButton("oku");
        //selectOption("additional_days_date1_month","Jul");
        //setTextField("additional_days_date1_year","1977");
        //selectOption("additional_days_date1_day","1");
        //clickRadioOption("tube_capped1","1");
        setTextField("drainage_amount_mls1","1");
        setTextField("drainage_amount_hrs1","1");
        selectOption("drainage_amount_start1","3:00");
        selectOption("drainage_amount_end1","3:00");
        checkCheckbox("drain_characteristics1","1558");
        selectOption("additional_days_date2_month","Jul");
        setTextField("additional_days_date2_year","1977");
        selectOption("additional_days_date2_day","1");
        //clickRadioOption("tube_capped2","1");
        setTextField("drainage_amount_mls2","1");
        setTextField("drainage_amount_hrs2","1");
        selectOption("drainage_amount_start2","3:00");
        selectOption("drainage_amount_end2","3:00");
        checkCheckbox("drain_characteristics2","1558");
        selectOption("additional_days_date3_month","Jul");
        setTextField("additional_days_date3_year","1977");
        selectOption("additional_days_date3_day","1");
        //clickRadioOption("tube_capped3","1");
        setTextField("drainage_amount_mls3","1");
        setTextField("drainage_amount_hrs3","1");
        selectOption("drainage_amount_start3","3:00");
        selectOption("drainage_amount_end3","3:00");
        checkCheckbox("drain_characteristics3","1558");
        //PIX-1148 - check that unfinished shows for extra drains
        clickLink("navTreatment");
        assertTextPresent("Pain is required");
        selectOption("pain","2");
         clickLink("navTreatment");
        assertTextPresent("There are unfinished assessments (Incision 1,Tube/Drain 2). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("stay_confirm");
        clickButton("bgbtntd2");
        
        clickRadioOption("drain_removed_intact","1");
        clickRadioOption("sutured","1");
        checkCheckbox("drain_site","1129");
        selectOption("tubes_changed_date_month","Jul");
        setTextField("tubes_changed_date_year","1977");
        selectOption("tubes_changed_date_day","1");
        clickRadioOption("cs_date_show","1");
        selectOption("pain","2");
        
        clickLink("navTreatment");
        assertTextPresent("Type of Drain is required.");
        
        
        selectOption("type_of_drain","Jackson Pratt drain");
        setTextField("type_of_drain_other","tod other");
        
        clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (Incision 1). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("go_confirm");
        selectOption("products","Apron");
        setTextField("drain_nursing_care_plan","drain ncp");
        setTextField("drain_treatment_comments","comments\ncomments");
        //pix-1138 - ensure all alphas show up.
        String[] alpha_check={"Each","Tube/Drain 1","Tube/Drain 2"};
        assertSelectOptionsEqual("wound_ass",alpha_check);
        
        selectOption("wound_ass","Tube/Drain 1");
        clickButton("addAlpha");
        clickRadioOption("cs_show","2");
        clickRadioOption("treatmentmodalities_show","2");
        selectOption("csresult_list","MRSA");
        //System.out.println(getPageSource());
        String[] dcfalphas={"Tube/Drain 1","Tube/Drain 2"};
        selectOptions("dressing_change_frequency_alphas",dcfalphas);
        selectOption("dressing_change_frequency_list","TID");
        clickButton("adddressing_change_frequency");
        selectOption("csresult_alphas","Tube/Drain 1");
        clickButton("addcsresult");
        String[] csresults={"Tube/Drain 1: MRSA"};
        assertSelectOptionsEqual("csresult",csresults);
        //clickButton("save");
        //assertSelectOptionsEqual("csresult",csresults);//PIX-1251
        
        setTextField("cns_name","drug A");
        selectOption("cns_start_day","26");
        setTextField("cns_start_year","1977");
        selectOption("cns_start_month","Jul");
        selectOption("cns_end_day","26");
        setTextField("cns_end_year","1977");
        selectOption("cns_end_month","Jul");
        selectOption("antibiotic_alphas","Tube/Drain 1");
        clickButton("addAnti");
        checkCheckbox("adjunctive","183");
        
        clickLink("navSummary");

        
        
        AssessmentServiceImpl b = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        AssessmentDrainVO drt1  = new AssessmentDrainVO();
        drt1.setAlpha_id(1639);
        AbstractAssessmentVO dr1 = b.getAssessment(drt1);
        String[] d1 = {"Status: Active","Pain at Site (scale 0-10): 2","Type of Tube/Drain: Jackson Pratt drain","Other: tod other","Tube/Drain Sutured: Yes","Drain Site: Intact","Additional Days Date: , 1/Jul/1977, 1/Jul/1977,",
        "Tube Capped/Clamped: Yes, Yes, Yes","Drainage Amount ml: 1, 1, 1","Drainage Amount hrs: 1, 1, 1","Drainage Amount Start: 3:00 to 3:00, 3:00 to 3:00, 3:00 to 3:00",
        "Tube/Drain Changed: 1/Jul/1977","Peri - Tube/Drain Skin: Intact","C&S Result :",
        "Products: 1 Apron","Treatment Comments: comments comments"};
        //assertSelectOptionsEqual("Wound Assessment",d1);
        for (String str : d1) {
            //assertTextPresent(str);
        }
        AssessmentDrainVO drt2  = new AssessmentDrainVO();
        drt2.setAlpha_id(1640);
        
        //pix-1112 - can't see 2nd draina ssess on summary'
        AbstractAssessmentVO dr2 = b.getAssessment(drt2);
        String[] d2 = {"Status: Active","Pain at Site (scale 0-10): 2","Type of Tube/Drain: Jackson Pratt drain","Type of Drain Other: tod other","Tube/Drain Sutured: Yes","Drain Site: Intact","Additional Days Date: ,",
        "Tube Capped/Clamped: No, No","Drainage Amount ml: 0, 0","Drainage Amount hrs: 0, 0","Drainage Amount Start: -1:00 to -1:00, 0:00 to 0:00",
        "Tube/Drain Changed: 1/Jul/1977","Peri - Tube/Drain Skin: Intact","C&S Result: MRSA",
        "Products :","Treatment Comments: comments"};
        //clickButton("bgbtntd2");
        //assertSelectOptionsEqual("Wound Assessment",d2);
        for (String str : d2) {
            //assertTextPresent(str);
        }
        clickRadioOption("referral_drain","y");
        IElement priority = getElementById("priority_drain");
        System.out.println(priority.getName()+" =-=- "+priority.getTextContent()+" ");

        for(IElement option : priority.getChildren()){
            System.out.println("Option: "+option.getAttribute("value"));
        }
        //System.out.println(getPageSource());
        selectOption("priority_drain","Urgent (24 - 48hrs)");
        setTextField("body_drain","Drain Comment");
Date now = new Date();
PDate pdate = new PDate(Constants.TIMEZONE);
        clickLink("navTreatment");
        clickLink("navSummary");
        assertTextFieldEquals("body_drain","Drain Comment");
        assertSelectedOptionEquals("priority_drain","Urgent (24 - 48hrs)");
        System.out.println(getPageSource());
        setWorkingForm("form");
        //setTextField("backdated_time",pdate.getHour((now.getTime()/1000)+"")+":"+pdate.getMin((now.getTime()/1000)+""));
        //selectOption("backdated_month","September");
        clickButton("saveEntries");
        assertTextPresent("Your work has been successfully saved");
       // gotoWindow("infowindow");
        
        
    }
     @After
    public void close() {
        closeBrowser();
    }
}
