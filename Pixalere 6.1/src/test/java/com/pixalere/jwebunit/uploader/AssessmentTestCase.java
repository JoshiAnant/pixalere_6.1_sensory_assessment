/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;
import java.util.*;
import com.pixalere.utils.PDate;
import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AbstractAssessmentVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class AssessmentTestCase  {
    //private final String CONTEXT="/LoginS";
    private String[] WOUND_ALPHAS={"A","B","C","D"};
    
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; FDM; .NET CLR 2.0.50727; .NET CLR 1.1.4322)");
        
        beginAt("/LoginSetup.do");
                
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        //System.out.println("Going to Wound: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Chest");
        //submit();
        clickLink("navAssessment");
        
        
        //System.out.println("Going to Assessment: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
    }
    @Test
    public void testCase4dotx_assessments() throws com.pixalere.common.ApplicationException{
        int count=0;
        //for(String alpha : WOUND_ALPHAS){
        count++;
        
        setWorkingForm("void");
        assertElementPresent("cancel1");
        checkCheckbox("cancel");
        clickButton("bgbtnA");
        //PIX-203
        /*setWorkingForm("void");
        checkCheckbox("cancel");
        clickLink("navTreatment");
        gotoWindow("infowindow");
        
        assertTextPresent("There are unfinished assessments (A,B,C,D). Click Stay to remain on the assessment screen, or Go to proceed");
        clickButton("ok");
        gotoRootWindow();
        setWorkingForm("void");
        uncheckCheckbox("cancel");//uncheck it*/
        //System.out.println(getPageSource());
        
        setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        setWorkingForm("form");
        assertElementPresent("wound_day");
        assertElementPresent("wound_month");
        assertElementPresent("wound_year");
        assertElementPresent("recurrent1");
        assertElementPresent("pain");
        assertElementPresent("length");
        assertElementPresent("width");
        assertElementPresent("depth");
        assertElementPresent("length_milli");
        assertElementPresent("width_milli");
        assertElementPresent("depth_milli");
        //assertElementPresent("woundbase1");
        assertElementPresent("exudate_type1");
        //assertElementPresent("exudate_amount1");
        assertElementPresent("exudate_odour1");
        assertElementPresent("wound_edge1");
        assertElementPresent("periwound1");
        assertElementPresent("status");
        assertElementPresent("discharge_reason");
        assertElementPresent("closed_day");
        assertElementPresent("closed_month");
        assertElementPresent("closed_year");
        
        
        
        selectOption("status","Active");
        
        selectOption("wound_day","1");selectOption("wound_month","Jul");setTextField("wound_year","1977");
        clickRadioOption("recurrent","2");
        assertSelectedOptionEquals("wound_day","1");
        assertSelectedOptionEquals("wound_month","Jul");
        assertTextFieldEquals("wound_year","1977");
        selectOption("pain","1");
        selectOption("length","1");
        selectOption("length_milli","2");
        selectOption("width","1");selectOption("width_milli","2");
        selectOption("depth","1");selectOption("depth_milli","2");
        assertTextPresent("Wound Area (LxW): 1.44cm");
        //PIX-
        selectOption("num_under","1");
        clickButton("oku");
        setWorkingForm("form");
        selectOption("underminingdepth_1","1");selectOption("underminingdepth_mini_1","2");
        selectOption("undermining_start_1","1:00");selectOption("undermining_end_1","1:00");
        
        selectOption("num_sinus_tracts","1");
        clickButton("oks");
       // System.out.println(getPageSource());
        setWorkingForm("form");
        
        //assertRadioOptionNotSelected("exudate_odour","0");
        selectOption("sinusdepth_1","1");selectOption("sinusdepth_mini_1","1");
        selectOption("sinustract_start_1","1:00");
        selectOption("num_fistulas","1");
        clickButton("okf");
        setTextField("fistulas_1","fistulas");
        checkCheckbox("woundbase","Hematoma");
        selectOption("woundbase_percentage_4","100%");
        checkCheckbox("exudate_type","222");
        clickRadioOption("exudate_amount","225");clickRadioOption("exudate_odour","1");
        checkCheckbox("wound_edge","436");checkCheckbox("periwound","746");
        if(count <= 2){
            selectOption("status","Active");
        }else{
            selectOption("status","Closed");
            selectOption("discharge_reason","Wound Closed");
            selectOption("closed_day","1");
            selectOption("closed_month","Jul");
            setTextField("closed_year","1977");
        }
        //}
        assertCheckboxSelected("woundbase","Hematoma");
        assertSelectedOptionEquals("woundbase_percentage_4","100%");
        
        clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (B,C,D). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("go_confirm");

        setWorkingForm("form");
        assertTextNotPresent("Populate Previous Products");

        setTextField("filtertext","30cc syringe");
        assertSelectOptionPresent("products","30cc syringe");

        //assertSelectOptionNotPresent("products","10cc syringe");
        selectOption("products","Apron");
        selectOption("products","Blue Pads");
        setTextField("wound_treatment_comments","comments\ncomments");
        selectOption("wound_ass","Wound A");
        clickButton("addAlpha");
        selectOption("current_products","1:Wound A:Blue Pads");
        clickButton("removeAlpha");
        selectOption("current_products","1:Wound A:Apron");
        
        setTextField("quantity","3");
        clickButton("btn_update");
        String[] dcfalphas={"Wound A"};
        selectOptions("dressing_change_frequency_alphas",dcfalphas);
        selectOption("dressing_change_frequency_list","TID");
        clickButton("adddressing_change_frequency");
        selectOption("visit_freq","TID");
        clickRadioOption("vac_show","2");
        selectOption("vacstart_day","26");
        selectOption("vacstart_month","Jul");
        setTextField("vacstart_year","1977");
        selectOption("vacend_day","26");
        selectOption("vacend_month","Jul");
        setTextField("vacend_year","1977");
        selectOption("pressure_reading","25 mm/hg");
        clickRadioOption("reading_group","1125");
        clickRadioOption("cs_show","2");
        selectOption("csresult_alphas","Wound A");
        selectOption("csresult_list","No Growth");
        clickButton("addcsresult");
        assertSelectedOptionEquals("csresult","Wound A: No Growth");
        selectOption("antibiotic_alphas","Wound A");
        setTextField("cns_name","Stuff");
        selectOption("cns_start_day","26");
        selectOption("cns_start_month","Jul");
        setTextField("cns_start_year","1977");
        selectOption("cns_end_day","26");
        selectOption("cns_end_month","Jul");
        setTextField("cns_end_year","1977");
        clickButton("addAnti");
        clickRadioOption("treatmentmodalities_show","2");
        checkCheckbox("adjunctive","183");
        selectOption("bandages_alphas","Wound A");
        setTextField("bandages_in","1");
        setTextField("bandages_out","2");
        clickButton("addbandages");
        clickLink("navSummary");
        clickButton("saveEntries");
     
        //
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_wound","n");
        //selectOption("priority_wound","Urgent (24 - 48hrs)");
        checkCheckbox("send_message_wound","1");
        setTextField("body_wound","Wound Care Comment");
        //test popup PIX-1087
        //selectOption("patientprofile","Gender: Male");
        //gotoWindow("hoverwindow");
        //assertTextPresent("Gender: Male");
        //clickLink("closeWindow");
        //gotoRootWindow();
        clickButton("saveEntries");
        
        
        
    }
    
    @Test
    public void testVerifyAssessment_A() throws com.pixalere.common.ApplicationException{
        clickButton("bgbtnA");
        
        setWorkingForm("form");
        //Test Auto pop date of onset
        assertSelectedOptionEquals("wound_day","1");
        assertSelectedOptionEquals("wound_month","Jul");
        assertTextFieldEquals("wound_year","1977");
        setWorkingForm("populate");
        //test assessment_type grey procedrues
        assertElementPresent("assessment_type1");
        assertElementPresent("assessment_type2");
        assertElementPresent("assessment_type3");
        //clickRadioOption("assessment_type","1");
        //assertElementNotPresent("assessment_type1");
        //assertElementNotPresent("assessment_type2");
        //assertElementNotPresent("assessment_type3");
        //checkCheckbox("phone_visit","1");
        //assertElementNotPresent("assessment_type1");
        //assertElementPresent("assessment_type2");
        //assertElementNotPresent("assessment_type3");
        //assertElementPresent("phone_visit");
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        //System.out.println(getPageSource());
        setWorkingForm("form");
        assertSelectedOptionEquals("wound_day","1");
        assertSelectedOptionEquals("wound_month","Jul");
        assertTextFieldEquals("wound_year","1977");
        assertRadioOptionSelected("recurrent","2");
        assertSelectedOptionEquals("pain","1");
        assertSelectedOptionEquals("length","1");
        assertSelectedOptionEquals("length_milli","2");
        assertSelectedOptionEquals("width","1");
        assertSelectedOptionEquals("width_milli","2");
        assertSelectedOptionEquals("depth","1");
        assertSelectedOptionEquals("depth_milli","2");
        assertCheckboxSelected("woundbase","Hematoma");
        assertSelectedOptionEquals("woundbase_percentage_4","100%");
        assertCheckboxSelected("exudate_type","222");
        assertRadioOptionSelected("exudate_amount","225");
        assertRadioOptionSelected("exudate_odour","1");
        assertCheckboxSelected("wound_edge","436");
        assertCheckboxSelected("periwound","746");
        selectOption("pain","2");
        
        assertSelectedOptionEquals("num_under","1");
        assertSelectedOptionEquals("underminingdepth_1","1");assertSelectedOptionEquals("underminingdepth_mini_1","2");
        assertSelectedOptionEquals("undermining_start_1","1:00");assertSelectedOptionEquals("undermining_end_1","1:00");
        
        assertSelectedOptionEquals("num_sinus_tracts","1");
        assertSelectedOptionEquals("sinusdepth_1","1");assertSelectedOptionEquals("sinusdepth_mini_1","1");
        assertSelectedOptionEquals("sinustract_start_1","1:00");
        
        clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (B,C,D). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("go_confirm");

        selectOption("products","Apron");
        setTextField("wound_treatment_comments","comments");
        selectOption("wound_ass","Wound A");
        clickButton("addAlpha");
        clickLink("navSummary");

        AssessmentServiceImpl b = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        AssessmentEachwoundVO drt1  = new AssessmentEachwoundVO();
        drt1.setAlpha_id(1632);
        AbstractAssessmentVO dr1 = b.getAssessment(drt1);
        //System.out.println(getPageSource());
        String[] d1 = {"Status: Active",
        "Date of Onset: 1/Jul/1977","Recurrent: Yes","Pain (scale 0-10): 2","Length (cm.mm): 1.2","Width (cm.mm): 1.2",
        "Depth (cm.mm): 1.2","Undermined Location: #1 1:00 to 1:00","Undermined Depth (cm.mm): #1 1.2","Sinus Tract Location: #1 1:00","Sinus Tract Depth (cm.mm): #1 1.1","Fistula #1: fistulas",
        "Wound Bed: Hematoma: 100%","Exudate: Nil","Exudate Amount: Nil","Odour (after cleansing): No",
        "Wound Edge: Diffuse","Peri-Wound Skin: Intact",
        "Products: 1 Apron, 3 Apron","Treatment Comments: comments","Packing pieces In: 1","Packing Pieces Out: 2",
        "NPWT (VAC) Start Date: 26/Jul/1977","NPWT (VAC) End Date: 26/Jul/1977","NPWT (VAC) Therapy Setting: Constant","NPWT (VAC): Pressure Reading: 25 mm/hg","C&S Done: Yes","C&S Result: No Growth","Antibiotic Therapy: Wound A: Stuff : 26/Jul/1977 to 26/Jul/1977","Treatment Modalities: Laser"};
        //assertSelectOptionsEqual("Wound Assessment",d1);
        for (String str : d1) {
            assertTextPresent(str);
        }
        clickButton("saveEntries");
 
        //
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_wound","n");
        //selectOption("priority","");
        clickButton("saveEntries");
        
    }
    @Test
    public void testpopulateBAssessment()throws com.pixalere.common.ApplicationException{
        setWorkingForm("void");
        assertElementPresent("cancel1");
        checkCheckbox("cancel");
        clickButton("bgbtnB");
        //PIX-203
        /*setWorkingForm("void");
        checkCheckbox("cancel");
        clickLink("navTreatment");
        gotoWindow("infowindow");
        
        assertTextPresent("There are unfinished assessments (A,B,C,D). Click Stay to remain on the assessment screen, or Go to proceed");
        clickButton("ok");
        gotoRootWindow();
        setWorkingForm("void");
        uncheckCheckbox("cancel");//uncheck it*/
        //System.out.println(getPageSource());
        setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        setWorkingForm("form");
        assertElementPresent("wound_day");
        assertElementPresent("wound_month");
        assertElementPresent("wound_year");
        assertElementPresent("recurrent1");
        assertElementPresent("pain");
        assertElementPresent("length");
        assertElementPresent("width");
        assertElementPresent("depth");
        assertElementPresent("length_milli");
        assertElementPresent("width_milli");
        assertElementPresent("depth_milli");
        //assertElementPresent("woundbase1");
        assertElementPresent("exudate_type1");
        //assertElementPresent("exudate_amount1");
        assertElementPresent("exudate_odour1");
        assertElementPresent("wound_edge1");
        assertElementPresent("periwound1");
        assertElementPresent("status");
        assertElementPresent("discharge_reason");
        assertElementPresent("closed_day");
        assertElementPresent("closed_month");
        assertElementPresent("closed_year");
        
        
        
        selectOption("status","Active");
        selectOption("wound_day","2");selectOption("wound_month","Jul");setTextField("wound_year","1977");
        clickRadioOption("recurrent","2");
        
        selectOption("pain","2");selectOption("length","2");selectOption("length_milli","2");
        selectOption("width","2");selectOption("width_milli","2");
        selectOption("depth","2");selectOption("depth_milli","2");
        //PIX-
        //selectOption("num_under","1");
        /*clickButton("oku");
        setWorkingForm("form");
        selectOption("underminingdepth_1","2");selectOption("underminingdepth_mini_1","2");
        selectOption("undermining_start_1","2:00");selectOption("undermining_end_1","2:00");
        */
        //selectOption("num_sinus_tracts","1");
        //clickButton("oks");

        setWorkingForm("form");
        
        //assertRadioOptionNotSelected("exudate_odour","0");
        //selectOption("sinusdepth_1","1");selectOption("sinusdepth_mini_1","1");
        //selectOption("sinustract_start_1","1:00");
        /*selectOption("num_fistulas","1");
        clickButton("okf");
        setTextField("fistulas_1","fistulas");*/
        checkCheckbox("woundbase","Hematoma");
        selectOption("woundbase_percentage_4","100%");
        checkCheckbox("exudate_type","222");
        clickRadioOption("exudate_amount","225");clickRadioOption("exudate_odour","1");
        checkCheckbox("wound_edge","436");checkCheckbox("periwound","746");
        
            selectOption("status","Active");
        
        //}
        assertCheckboxSelected("woundbase","Hematoma");
        assertSelectedOptionEquals("woundbase_percentage_4","100%");
        
        clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (A,C,D). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("go_confirm");

        assertTextNotPresent("Populate Previous Products");
        selectOption("products","Apron");
        selectOption("products","Blue Pads");
        setTextField("wound_treatment_comments","comments\ncomments");
        selectOption("wound_ass","Wound B");
        clickButton("addAlpha");
        selectOption("current_products","1:Wound B:Blue Pads");
        clickButton("removeAlpha");
        selectOption("current_products","1:Wound B:Apron");
        
        setTextField("quantity","3");
        clickButton("btn_update");
        String[] dcfalphas={"Wound B"};
        selectOptions("dressing_change_frequency_alphas",dcfalphas);
        selectOption("dressing_change_frequency_list","TID");
        clickButton("adddressing_change_frequency");
        clickLink("navSummary");
        clickButton("saveEntries");
  
        
        //
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_wound","y");
        selectOption("priority_wound","Urgent (24 - 48hrs)");
        setTextField("body_wound","Wound Care Comment");
        //test popup PIX-1087
        //selectOption("patientprofile","Gender: Male");
        //gotoWindow("hoverwindow");
        //assertTextPresent("Gender: Male");
        //clickLink("closeWindow");
        //gotoRootWindow();
        clickButton("saveEntries");
    }
    @Test
    public void testverifyAssessmentB()throws com.pixalere.common.ApplicationException{
        setWorkingForm("void");
        assertElementPresent("cancel1");
        checkCheckbox("cancel");
        clickButton("bgbtnB");
        setWorkingForm("populate");
        //test assessment_type grey procedrues
        assertElementPresent("assessment_type1");
        assertElementPresent("assessment_type2");
        assertElementPresent("assessment_type3");
        //clickRadioOption("assessment_type","1");
        //assertElementNotPresent("assessment_type1");
        //assertElementNotPresent("assessment_type2");
        //assertElementNotPresent("assessment_type3");
        //checkCheckbox("phone_visit","1");
        //assertElementNotPresent("assessment_type1");
        //assertElementPresent("assessment_type2");
        //assertElementNotPresent("assessment_type3");
        //assertElementPresent("phone_visit");
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        //System.out.println(getPageSource());
        setWorkingForm("form");
        assertSelectedOptionEquals("wound_day","2");
        assertSelectedOptionEquals("wound_month","Jul");
        assertTextFieldEquals("wound_year","1977");
        assertRadioOptionSelected("recurrent","2");
        assertSelectedOptionEquals("pain","2");
        assertSelectedOptionEquals("length","2");
        assertSelectedOptionEquals("length_milli","2");
        assertSelectedOptionEquals("width","2");
        assertSelectedOptionEquals("width_milli","2");
        assertSelectedOptionEquals("depth","2");
        assertSelectedOptionEquals("depth_milli","2");
        assertCheckboxSelected("woundbase","Hematoma");
        
        assertCheckboxSelected("exudate_type","222");
        assertRadioOptionSelected("exudate_amount","225");
        assertRadioOptionSelected("exudate_odour","1");
        assertCheckboxSelected("wound_edge","436");
        assertCheckboxSelected("periwound","746");
        selectOption("pain","4");
        
        //assertSelectedOptionEquals("num_under","1");
        //assertSelectedOptionEquals("underminingdepth_1","2");assertSelectedOptionEquals("underminingdepth_mini_1","2");
        //assertSelectedOptionEquals("undermining_start_1","2:00");assertSelectedOptionEquals("undermining_end_1","2:00");
        
        //assertSelectedOptionEquals("num_sinus_tracts","1");
        //assertSelectedOptionEquals("sinusdepth_1","1");assertSelectedOptionEquals("sinusdepth_mini_1","1");
        //assertSelectedOptionEquals("sinustract_start_1","1:00");
        assertSelectedOptionEquals("woundbase_percentage_4","100%");
        
        //selectOption("pain","");
        
        clickButton("bgbtnA");
        
        setWorkingForm("populate");
        //test assessment_type grey procedrues
        assertElementPresent("assessment_type1");
        assertElementPresent("assessment_type2");
        assertElementPresent("assessment_type3");
        //clickRadioOption("assessment_type","1");
        //assertElementNotPresent("assessment_type1");
        //assertElementNotPresent("assessment_type2");
        //assertElementNotPresent("assessment_type3");
        //checkCheckbox("phone_visit","1");
        //assertElementNotPresent("assessment_type1");
        //assertElementPresent("assessment_type2");
        //assertElementNotPresent("assessment_type3");
        //assertElementPresent("phone_visit");
        //
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        //System.out.println(getPageSource());
        setWorkingForm("form");
        
        assertSelectedOptionEquals("wound_day","1");
        assertSelectedOptionEquals("wound_month","Jul");
        assertTextFieldEquals("wound_year","1977");
        assertRadioOptionSelected("recurrent","2");
        assertSelectedOptionEquals("pain","2");
        assertSelectedOptionEquals("length","1");
        assertSelectedOptionEquals("length_milli","2");
        assertSelectedOptionEquals("width","1");
        assertSelectedOptionEquals("width_milli","2");
        assertSelectedOptionEquals("depth","1");
        assertSelectedOptionEquals("depth_milli","2");
        assertCheckboxSelected("woundbase","Hematoma");
        assertSelectedOptionEquals("woundbase_percentage_4","100%");
        assertCheckboxSelected("exudate_type","222");
        assertRadioOptionSelected("exudate_amount","225");
        assertRadioOptionSelected("exudate_odour","1");
        assertCheckboxSelected("wound_edge","436");
        assertCheckboxSelected("periwound","746");
        selectOption("pain","2");
     
        
        clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (C,D). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("go_confirm");

        selectOption("products","Apron");
        setTextField("wound_treatment_comments","comments");
        selectOption("wound_ass","Wound A");
        selectOption("products","Apron");
        selectOption("wound_ass","Wound B");
        clickButton("addAlpha");
        clickLink("navSummary");
        
        
        AssessmentServiceImpl b = new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
        AssessmentEachwoundVO drt1  = new AssessmentEachwoundVO();
        drt1.setAlpha_id(1632);
        AbstractAssessmentVO dr1 = b.getAssessment(drt1);
        String[] d1 = {"Status: Active",
        "Date of Onset: 1/Jul/1977","Recurrent: Yes","Pain (scale 0-10): 2","Length (cm.mm): 1.2","Width (cm.mm): 1.2",
        "Depth (cm.mm): 1.2",
        "Wound Bed: Hematoma: 100%","Exudate: Nil","Exudate Amount: Nil","Odour (after cleansing): No",
        "Wound Edge: Diffuse","Peri-Wound Skin: Intact",
        "Products: 0.5 Apron","Treatment Comments: comments","Packing pieces In: 1","Packing Pieces Out: 2",
        "NPWT (VAC) Start Date: 26/Jul/1977","NPWT (VAC) End Date: 26/Jul/1977","NPWT (VAC) Therapy Setting: Constant","NPWT (VAC): Pressure Reading: 25 mm/hg","C&S Done: Yes","C&S Result: No Growth","Antibiotic Therapy: Wound A: Stuff : 26/Jul/1977 to 26/Jul/1977"};
        //assertSelectOptionsEqual("Wound Assessment",d1);
      for (String str : d1) {
            assertTextPresent(str);
        }
        
        
        AssessmentEachwoundVO drt2  = new AssessmentEachwoundVO();
        drt1.setAlpha_id(15975);
        AbstractAssessmentVO dr2 = b.getAssessment(drt2);
        String[] d2 = {"Status: Active",
        "Date of Onset: 2/Jul/1977","Recurrent: Yes","Pain (scale 0-10): 4","Length (cm.mm): 2.2","Width (cm.mm): 2.2",
        "Depth (cm.mm): 2.2",
        "Wound Bed: Hematoma: 100%","Exudate: Nil","Exudate Amount: Nil","Odour (after cleansing): No",
        "Wound Edge: Diffuse","Peri-Wound Skin: Intact",
        "Products: 0.5 Apron","Treatment Comments: comments",
        "C&S Done: Yes"};
        //assertSelectOptionsEqual("Wound Assessment",d2);
        for (String str : d2) {
            assertTextPresent(str);
        }
        clickRadioOption("referral_wound","y");
        selectOption("priority_wound","Urgent (24 - 48hrs)");
        setTextField("body_wound","Wound Care Comment");clickButton("saveEntries");

        /*
        //
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_wound","n");
        System.out.println(getPageSource());
        //selectOption("priority","");
        clickButton("saveEntries");*/
    }
    @After
    public void close() {
        closeBrowser();
    }
}
