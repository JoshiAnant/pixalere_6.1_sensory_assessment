/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;

import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class WoundProfileTestCase  {
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navPatient");
        clickLink("navWound");

    }
   /* public void testDragAndDrop() {
        final String url = "http://localhost:8080/pixalere/WoundProfileSetup.do";
        final WebClient web = new WebClient();
        HtmlPage page = (HtmlPage)web.getPage(url);
        HtmlElement a = page.getHtmlElementById("")
    }*/
    
    @Test
    public void testCase3dotx_createWoundProfile(){
        setWorkingForm("woundform");
        selectOption("wound_id","New Wound Profile");
        //submit();
        assertElementPresent("anterior");
        assertElementPresent("posterior");
        
        selectOption("anterior","Chest");
        clickRadioOption("rad_anterior","R");
        //assertElementPresent("wound_etiology1");
        //assertElementPresent("goals_1");
        
        
        //selectOption()
        //checkCheckbox("wound_etiology","798");
        setTextField("reason_for_care","Reason");
        
        clickLink("selectWound");

        clickLink("alpha167");
        clickLink("alpha40");
        clickLink("alpha101");
        clickLink("alpha106");
        clickLink("navSummary");
        assertTextPresent("Etiology is required");//test case 3.5
        assertTextPresent("Goals is required");//test case 3.5
        String[] a ={"Wound A","Wound B","Wound C","Wound D"};
        selectOptions("goals_alphas",a);
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");
        /*selectOption("goals_alphas","Wound B");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");
        selectOption("goals_alphas","Wound C");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");
        selectOption("goals_alphas","Wound D");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");*/
        String[] gt = {"Wound A: Maintain wound","Wound B: Maintain wound","Wound C: Maintain wound","Wound D: Maintain wound"};
        assertSelectOptionsEqual("goals",gt);
        
        selectOptions("etiology_alphas",a);
        selectOption("etiology_list","Venous");
        clickButton("addetiology");
        /*selectOption("etiology_alphas","Wound B");
        selectOption("etiology_list","Venous");
        clickButton("addetiology");
        selectOption("etiology_alphas","Wound C");
        selectOption("etiology_list","Venous");
        clickButton("addetiology");
        selectOption("etiology_alphas","Wound D");
        selectOption("etiology_list","Venous");
        clickButton("addetiology");*/
        String[] et = {"Wound A: Venous","Wound B: Venous","Wound C: Venous","Wound D: Venous"};
        assertSelectOptionsEqual("etiology",et);
        setTextField("operative_procedure_comments","comment");
        selectOption("date_surgery_day","26");
        selectOption("date_surgery_month","Jul");
        setTextField("date_surgery_year","1977");
        setTextField("surgeon","dr. burns");
        
        //clickLink("img167");
        clickLink("navSummary");
        //check list to verify items
        String[] pp ={
            "Wound Location: Chest",
            "Cause/History: Reason",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
            "Etiology: Wound A: Venous, Wound B: Venous, Wound C: Venous, Wound D: Venous",
            "Goal of Care: Wound A: Maintain wound, Wound B: Maintain wound, Wound C: Maintain wound, Wound D: Maintain wound"
            };
        for (String str : pp) {
            assertTextPresent(str);
        }
        //assertSelectOptionsEqual("woundprofile",pp);
        
        clickButton("saveEntries");
        gotoRootWindow();
    }
    @Test
    public void testCase3dot23_verifyWoundProfile(){
        setWorkingForm("woundform");
        selectOption("wound_id","Chest");
        //submit();
        assertSelectedOptionEquals("anterior","Chest");
        assertTextFieldEquals("reason_for_care","Reason");
        
        //check antiboitics
        
        
    }
    /*public testCheckAlphaOrdering(){
        setWorkingForm("woundform");
        
    }*/
    @After
    public void close() {
        closeBrowser();
    }
}
