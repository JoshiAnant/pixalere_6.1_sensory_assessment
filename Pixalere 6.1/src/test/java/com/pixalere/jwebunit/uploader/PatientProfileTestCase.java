/* 8058420
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;
import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class PatientProfileTestCase  {
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        //System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        //setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        //setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        //clickLink("navAdmin");
        setWorkingForm("searchN");
        setTextField("search",ConstantsTest.PATIENT_ID_WEB+"");
        submit();
    }
    @Test
    public void tc2dot1_checkPatientProfile(){
        assertTextPresent("Physical Exam");
    }
    
    @Test
    public void tc2dotx_patientProfile(){
        setWorkingForm("form");
        assertTextPresent("N/A");//test case 2.2
        assertTextPresent("3		Professor Snap'e		26/Jul/1977		Richmond Lions Manor		 Admitted");
        //clickLink("navSummary");
        //assertTextPresent("Interfering Medications is required");//test case 2.4

        checkCheckbox("interfering_factors","817");
        //selectOption("funding_source","ICBC");

        setTextField("professionals","Dr Burns");
        //setTextField("allergies","Bees");
        checkCheckbox("medications_comments","776");
        selectOptionByValue("co_morbidities","296");
        
        
        /*clickLink("navSummary");
        gotoWindow("infowindow");
        
        assertTextPresent("Information Popup WARNING: Invalid date of birth");
        clickLink("closeWindow");
        gotoRootWindow();*/
       
       /*clickLink("loadBraden");
       gotoWindow("braden_scale");
       
       clickRadioOption("braden_moisture","1034");
       clickRadioOption("braden_sensory","1032");
       clickRadioOption("braden_activity","1035");
       clickRadioOption("braden_mobility","1038");
       clickRadioOption("braden_friction","1037");
       clickRadioOption("braden_nutrition","1036");
       closeWindow();
       gotoRootWindow();*/
        assertTextFieldEquals("braden_score","");
        //selectOption("investigation_list","317");
        
        
        selectOptionByValue("investigation_list","178");
        selectOptionByValue("investigation_day","26");
        selectOptionByValue("investigation_month","7");
        setTextField("investigation_year","1977");
        
        clickButton("AddAnti");
        setTextField("blood_pressure","100");
        setTextField("temperature","1");
        setTextField("resp_rate","2");
        setTextField("pulse","3");
        setTextField("height","160");
        setTextField("weight","240");
        setTextField("blood_sugar","1");
        selectOptionByValue("bloodsugar_day","26");
        selectOptionByValue("bloodsugar_month","7");
        setTextField("bloodsugar_year","1977");

        setTextField("blood_sugar_meal","1");
        selectOptionByValue("albumin_day","26");
        selectOptionByValue("albumin_month","7");
        setTextField("albumin_year","1977");
        selectOptionByValue("prealbumin_day","26");
        selectOptionByValue("prealbumin_month","7");
        setTextField("prealbumin_year","1977");
        
        setTextField("albumin","1.1");
        setTextField("pre_albumin","1.2");
        
        clickRadioOption("braden_sensory_popup","1");
        clickRadioOption("braden_moisture_popup","4");
        clickRadioOption("braden_activity_popup","3");
        clickRadioOption("braden_nutrition_popup","1");
        clickRadioOption("braden_mobility_popup","1");
        clickRadioOption("braden_friction_popup","1");
        assertRadioOptionSelected("braden_activity_popup","3");
        
        //hack for testing purposes only
        //`1                                                setTextField("braden_score","10");
        //assertTextFieldEquals("braden_score","11");
        setTextField("surgical_history","test surgical");
        //clickLink("navSummary");
       /// submit();
        System.out.println(getPageSource());
        clickLink("navSummary");
        
        
        String[] pp = {
        
        "Funding Source: ICBC",
        "Gender: Male",
        "Date of Birth: 26/Jul/1977",
        "Age: 34",
        "Allergies: Bees",
        "Other Health Care Professionals Involved: Dr Burns",
        "Co-Morbidities: Hypotension",
        "Surgical History: test surgical",
        "Factors that affect wound healing: Poor perfusion",
        "Factors that affect wound healing; Other:",
        "Medications that affect wound healing: Steroids",
        
        "Diagnostic Tests: Biopsy : 1/Jul/1977",
        
        "Blood Glucose mmol.L: 1",
        "Date: 1/Jul/1977",
        "Time: 1",
        "Albumin g/L: 1.1",
        "Date: 1/Jul/1977",
        "Prealbumin mg/L: 1.2",
        "Date: 1/Jul/1977",
        "Sensory/Perception: 1",
        "Moisture: 4",
        "Activity: 3",
        "Mobility: 1",
        "Nutrition: 1",
        "Friction: 1",
	"Braden Risk Score: 11"

        };
        for(String p : pp){
            assertTextPresent(p);
        }
        clickButton("saveEntries");
        gotoRootWindow();
        
    }
    
    @Test
    public void tc2dot13_verifyPatientProfile(){
        assertSelectedOptionEquals("patient_res","Fraser Health");
        assertCheckboxSelected("im","817");
        assertSelectedOptionEquals("funding_source","ICBC");
        assertSelectedOptionEquals("gender","Male");
        assertSelectedOptionEquals("dob_day","26");
        assertSelectedOptionEquals("dob_month","Jul");
        assertTextFieldEquals("dob_year","1977");
        assertTextFieldEquals("professionals","Dr Burns");
        assertTextFieldEquals("allergies","Bees");
        assertCheckboxSelected("medication_comments","776");
        assertSelectedOptionEquals("bloodsugar_day","1");
        assertSelectedOptionEquals("bloodsugar_month","Jul");
        assertTextFieldEquals("bloodsugar_year","1977");
        assertTextFieldEquals("blood_sugar_meal","1");
        assertSelectedOptionEquals("albumin_day","1");
        assertTextFieldEquals("albumin_year","1977");
        assertSelectedOptionEquals("albumin_month","Jul");
        assertSelectedOptionEquals("prealbumin_month","Jul");
        assertTextFieldEquals("prealbumin_year","1977");
        assertSelectedOptionEquals("prealbumin_day","31");
        assertTextFieldEquals("pre_albumin","1.2");
        assertTextFieldEquals("albumin","1.1");
        //PIX-1046
        selectOption("funding_source","Work Save BC (WCB)");
        clickLink("navSummary");
        
        clickButton("saveEntries");
        gotoRootWindow();
    }
    
    @After
    public void close() {
        closeBrowser();
    }
}
