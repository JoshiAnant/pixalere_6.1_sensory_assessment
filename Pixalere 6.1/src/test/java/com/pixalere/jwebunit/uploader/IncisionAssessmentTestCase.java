/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.uploader;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import com.pixalere.ConstantsTest;

public class IncisionAssessmentTestCase {
    //private final String CONTEXT="/LoginS";
    private String[] WOUND_ALPHAS={"A","B","C","D"};
    
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; FDM; .NET CLR 2.0.50727; .NET CLR 1.1.4322)");
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navWound");
        setWorkingForm("woundform");
       // System.out.println(getPageSource()+"\n-=-=-=-=-=-=-=-=-=-=-Mallory=-=-=-=-=-=-=-=-=-=-=-=-");
        selectOption("wound_id","Head/neck (Anterior)");
        //submit();
        assertTextFieldEquals("operative_procedure_comments","Operative Procedure Comments");
        assertSelectedOptionEquals("date_surgery_day","1");
        assertSelectedOptionEquals("date_surgery_month","Jul");
        assertTextFieldEquals("date_surgery_year","1977");
        assertTextFieldEquals("surgeon","Dr. Smithers");
        clickLink("navAssessment");
    }
    @Test
    public void testAssessments(){
        int count=0;
        //for(String alpha : WOUND_ALPHAS){
        count++;
       setWorkingForm("void");
        checkCheckbox("cancel");
       clickButton("bgbtntag1");
       
        //gotoWindow("infowindow");
         assertElementPresent("pain");
        assertElementPresent("status");
        assertElementPresent("discharge_reason");
        assertElementPresent("closed_day");
        assertElementPresent("closed_month");
        assertElementPresent("closed_year");
        //assertElementPresent("incision_status");
        //assertElementPresent("incision_closure_status");
        //assertElementPresent("incision_closure_methods1");
        //assertElementPresent("incision_exudate1");
        //assertElementPresent("exudate_amount1");
        //assertElementPresent("peri_incisional");
       
        //clinical path
        clickRadioOption("clinical_path","2");
        selectOption("clinical_path_date_day","1");
        selectOption("clinical_path_date_month","Jul");
        setTextField("clinical_path_date_year","1977");
        
        clickRadioOption("incision_status","1095");
        selectOption("pain","1");
        checkCheckbox("incision_closure_methods","1126");
        checkCheckbox("incision_exudate","1085");

        checkCheckbox("peri_incisional","1090");
        clickRadioOption("exudate_amount","1128");
        
       
        clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (Tube/Drain 1,Tube/Drain 2). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("go_confirm");
        /// System.out.println("Treatment doing: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        selectOption("products","Apron");
        setTextField("incision_treatment_comments","comments\ncomments");
        setTextField("incision_nursing_care_plan","incision ncp");
        //pix-1138 - ensure all alphas show up.
        String[] alpha_check={"Incision 1"};
        assertSelectedOptionsEqual("wound_ass",alpha_check);
        
        selectOption("wound_ass","Incision 1");
        clickButton("addAlpha");
        clickRadioOption("cs_show","2");
        clickRadioOption("treatmentmodalities_show","2");
        selectOption("csresult_list","MRSA");
        //System.out.println(getPageSource());
        
        selectOption("csresult_alphas","Incision 1");
        clickButton("addcsresult");
        
        setTextField("cns_name","drug A");
        selectOption("cns_start_day","26");
        setTextField("cns_start_year","1977");
        selectOption("cns_start_month","Jul");
        selectOption("cns_end_day","26");
        setTextField("cns_end_year","1977");
        selectOption("cns_end_month","Jul");
        selectOption("antibiotic_alphas","Incision 1");
        clickButton("addAnti");
        checkCheckbox("adjunctive","183");
        selectOption("dressing_change_frequency_alphas","Incision 1");
        selectOption("dressing_change_frequency_list","TID");
        clickButton("adddressing_change_frequency");
        clickLink("navSummary");
        clickButton("saveEntries");
        //System.out.println("Going to Summary: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        //
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_incision","y");
        selectOption("priority_incision","Urgent (24 - 48hrs)");
        setTextField("body_incision","Incision Comment");
        clickButton("saveEntries");
        
    }
             
    @Test
    public void verifyIncisionAssessment(){
        clickButton("bgbtntag1");
        setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        //System.out.println(getPageSource());
        setWorkingForm("form");
        
        assertRadioOptionSelected("clinical_path","2");
        assertSelectedOptionEquals("clinical_path_date_day","1");
        assertSelectedOptionEquals("clinical_path_date_month","Jul");
        assertTextFieldEquals("clinical_path_date_year","1977");
        
        assertRadioOptionSelected("incision_status","1095");
        assertSelectedOptionEquals("pain","1");
        assertCheckboxSelected("incision_closure_methods","1126");
        assertCheckboxSelected("incision_exudate","1085");

        assertCheckboxSelected("peri_incision","1090");
        assertRadioOptionSelected("exudate_amount","1128");
        
        assertSelectedOptionEquals("status","Closed");
        assertSelectedOptionEquals("discharge_reason","Self care");
        assertSelectedOptionEquals("closed_day","1");
        assertSelectedOptionEquals("closed_month","Jul");
        assertTextFieldEquals("closed_year","1977");
        
        clickButton("bgbtntd1");
        setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        
        
        clickLink("navTreatment");
        assertTextPresent("There are unfinished assessments (Tube/Drain 1,Tube/Drain 2). Click Stay to remain on the assessment screen, or Go to proceed.");
        
        /// System.out.println("Treatment doing: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        setWorkingForm("form");
        assertTextFieldEquals("drain_treatment_comments","");
        assertTextFieldEquals("drain_nursing_care_plan","drain ncp");
        
        assertTextFieldEquals("incision_treatment_comments","");
        assertTextFieldEquals("incision_nursing_care_plan","incision ncp");
        //pix-1138 - ensure all alphas show up.
        assertRadioOptionSelected("cs_show","2");
        assertRadioOptionSelected("treatmentmodalities_show","2");
        String[] csresult={"Tube/Drain 1: MRSA","Incision 1: MRSA"};
        assertSelectedOptionsEqual("csresult",csresult);
        String[] cns={"Tube/Drain 1: drug A 26/Jul/1977 to 26/Jul/1977","Incision 1: drug A 26/Jul/1977 to 26/Jul/1977"};
        assertSelectedOptionsEqual("antibiotics",cns);
        
        assertCheckboxSelected("adjunctive","2183");
        
        
        clickButton("populate");
        String[] alpha_check={"Tube/Drain 1: Apron","Incision 1: Apron"};
        assertSelectedOptionsEqual("current_products",alpha_check);
        
        clickLink("navSummary");
        
        //
        clickRadioOption("referral_incision","n");
        clickButton("saveEntries");
        
        
    }
     @After
    public void close() {
        closeBrowser();
    }
}
