package com.pixalere.jwebunit.uploader;
import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import net.sourceforge.jwebunit.api.IElement;
import java.util.List;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class FootLimbAssessmentTestCase  {
    //private final String CONTEXT="/LoginS";
   
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search",ConstantsTest.PATIENT_ID_WEB+"");
        submit();
        clickLink("navPatient");
    }
    
    
    @Test
    public void testPopulateLimbFootProfile(){
       // System.out.println(getPageSource());
        setWorkingForm("form");

        setWorkingForm("form");
        checkCheckbox("left_missing_limbs","1319");
        checkCheckbox("right_missing_limbs","1319");
        checkCheckbox("left_pain_assessment","1309");
        checkCheckbox("right_pain_assessment","1309");
        setTextField("pain_comments","Comment");
        checkCheckbox("left_skin_assessment","1310");
        checkCheckbox("right_skin_assessment","1310");
        selectOption("left_temperature_leg","Cold");
        selectOption("right_temperature_leg","Cold");
        selectOption("left_temperature_foot","Cold");
        selectOption("right_temperature_foot","Cold");
        selectOption("left_temperature_toes","Cold");
        selectOption("right_temperature_toes","Cold");
        selectOption("left_skin_colour_leg","Pale");
        selectOption("right_skin_colour_leg","Pale");
        selectOption("left_skin_colour_foot","Pale");
        selectOption("right_skin_colour_foot","Pale");
        selectOption("left_skin_colour_toes","Pale");
        selectOption("right_skin_colour_toes","Pale");
        selectOption("left_dorsalis_pedis_palpation","Present");
        selectOption("right_dorsalis_pedis_palpation","Present");
        selectOption("left_posterior_tibial_palpation","Present");
        selectOption("right_posterior_tibial_palpation","Present");
        clickRadioOption("doppler_lab","1438");
        checkCheckbox("left_dorsalis_pedis_doppler","427");
        checkCheckbox("right_dorsalis_pedis_doppler","427");
        checkCheckbox("left_posterior_tibial_doppler","427");
        checkCheckbox("right_posterior_tibial_doppler","427");
        checkCheckbox("left_interdigitial_doppler","427");
        checkCheckbox("right_interdigitial_doppler","427");
        
        clickRadioOption("ankle_brachial_lab","1438");
        
        setTextField("ankle_brachial_year","1977");
        IElement i = getElementById("ankle_brachial_day");
        List<IElement> children = i.getChildren();
        for(IElement e : children){
            System.out.println(e.getName()+" "+e.getTextContent());
        }
        selectOptionByValue("ankle_brachial_day","31");
        selectOptionByValue("ankle_brachial_month","7");
        //clickRadioOption("left_capillary_less","0");
         //clickRadioOption("left_capillary_more","0");
          clickRadioOption("right_capillary_less","2");
           clickRadioOption("right_capillary_more","2");

        clickRadioOption("toe_brachial_lab","1438");
        selectOptionByValue("toe_brachial_month","7");
        setTextField("toe_brachial_year","1977");
        selectOptionByValue("toe_brachial_day","31");
        
        clickRadioOption("transcutaneous_lab","1438");
        selectOptionByValue("left_transcutaneous_pressure","1434");
        selectOptionByValue("right_transcutaneous_pressure","1434");
        selectOptionByValue("transcutaneous_month","7");
        setTextField("transcutaneous_year","1977");
        selectOptionByValue("transcutaneous_day","31");

        setTextField("left_dorsalis_pedis_ankle_brachial","1");
        setTextField("right_dorsalis_pedis_ankle_brachial","1");
        
        setTextField("left_tibial_pedis_ankle_brachial","1");
        setTextField("right_tibial_pedis_ankle_brachial","1");
        
        setTextField("left_ankle_brachial","1");
        setTextField("right_ankle_brachial","1");
        
        //setTextField("left_abi_score_ankle_brachial","1");
        //setTextField("right_abi_score_ankle_brachial","1");
        
        setTextField("left_toe_pressure","1");
        setTextField("right_toe_pressure","1");
        
        setTextField("left_brachial_pressure","1");
        setTextField("right_brachial_pressure","1");
        
        checkCheckbox("left_sensation","1251");
        checkCheckbox("right_sensation","1251");
        //setTextField("left_tbi_score","1");
        //setTextField("right_tbi_score","1");
        selectOption("left_edema_severity","+1 Trace");
        selectOption("right_edema_severity","+1 Trace");
        
        selectOption("left_edema_location","Foot");
        selectOption("right_edema_location","Foot");
        setTextField("sleep_positions","up");
        setTextField("left_ankle_cm","1");
        setTextField("right_ankle_cm","1");
        setTextField("left_ankle_mm","1");
        setTextField("right_ankle_mm","1");
        
        setTextField("left_midcalf_cm","2");
        setTextField("right_midcalf_cm","2");
        setTextField("left_midcalf_mm","2");
        setTextField("right_midcalf_mm","2");
        setTextField("foot_comments","foot comments");
        //setTextField("limb_comments","limb comments");

        //checkCheckbox("left_capillary_less","1");
        clickRadioOption("left_capillary_more","2");
        //checkCheckbox("right_capillary_less","1");
        clickRadioOption("right_capillary_more","2");

        checkCheckbox("left_sensory","1191");
        checkCheckbox("right_sensory","1191");
        selectOption("left_proprioception","Intact");
        selectOption("right_proprioception","Intact");
        checkCheckbox("left_foot_deformities","1199");
        checkCheckbox("right_foot_deformities","1199");
        
        checkCheckbox("left_foot_skin","1216");
        checkCheckbox("right_foot_skin","1216");
        
        checkCheckbox("left_foot_toes","1211");
        checkCheckbox("right_foot_toes","1211");
        selectOption("weight_bearing","None");
        selectOption("balance","Steady");
        selectOption("calf_muscle_pump","Normal");
        selectOption("mobility_aids","Brace");
        setTextField("mobility_aids_comments","test");
        setTextField("calf_muscle_comments","test");
        setTextField("gait_pattern_comments","test");
        
        setTextField("walking_distance_comments","test");
    
        setTextField("walking_endurance_comments","test");
        clickRadioOption("indoor_footwear","2");
        setTextField("indoor_footwear_comments","testers");
        
        clickRadioOption("outdoor_footwear","2");
        setTextField("outdoor_footwear_comments","testers");
        
        clickRadioOption("orthotics","2");
        setTextField("orthotics_comments","testers");
        selectOption("muscle_tone_functional","High");
        selectOption("arches_foot_structure","High");
        
        clickRadioOption("supination_foot_structure","2");
        clickRadioOption("pronation_foot_structure","2");
        checkCheckbox("dorsiflexion_active","1247");
        
        checkCheckbox("plantarflexion_active","1247");
        checkCheckbox("greattoe_active","1247");
        checkCheckbox("dorsiflexion_passive","1432");
        
        checkCheckbox("plantarflexion_passive","1432");
        checkCheckbox("greattoe_passive","1432");
        clickLink("navSummary");
        String[] pp = {
        "Patient Residence: Fraser Health",
        "Treatment Location: Overlander",
        "Funding Source: ICBC",
        "Gender: Male",
        "Date of Birth: 26/Jul/1977",
        "Age: 32",
        "Allergies: Bees",
        "Other Health Care Professionals Involved: Dr Burns",
        "Co-Morbidities: Hypotension",
        "Surgical History: test surgical",
        "Factors that affect wound healing: Poor perfusion",
        "Factors that affect wound healing; Other:",
        "Medications that affect wound healing: Steroids",
        //"Braden Risk Score: 11 (4/1/1/1/3/1)",
        "Diagnostic Tests: Biopsy : 1/Jul/1977",
        "Blood Glucose mmol.L: 1",
        "Date: 1/Jul/1977",
        "Time: 1",
        "Albumin g/L: 1.1",
        "Date: 1/Jul/1977",
        "Prealbumin mg/L: 1.2",
        "Date: 31/Jul/1977",
        //"Braden Score: 0",
        /*"Sensory/Perception: 1",
        "Moisture:",
        "Activity:",
        "Mobility:",
        "Nutrition:",
        "Friction:",
		"Braden Risk Score: 0",*/
        "Left Missing Limb or Toes: No amputations",
        "Right Missing Limb or Toes: No amputations",
    
        "Left Pain: No pain",
        "Right Pain: No pain",
        "Pain Comments: Comment",
        "Left Skin: Normal/healthy",
        "Right Skin: Normal/healthy",
        "Left Temperature Leg: Cold",
        "Right Temperature Leg: Cold",
        "Left Temperature Foot: Cold",
        "Right Temperature Foot: Cold",
        "Left Temperature Toes: Cold",
        "Right Temperature Toes: Cold",
        "Left Skin Colour Leg: Pale",
        "Right Skin Colour Leg: Pale",
        "Left Skin Colour Foot: Pale",
        "Right Skin Colour Foot: Pale",
        "Left Skin Colour Toes: Pale",
        "Right Skin Colour Toes: Pale",
        "Left Cap Refill less than 4 secs:",
        "Right Cap Refill less than 4 secs:",
        
        "Left Cap Refill 4 secs or greater: Yes",
        "Right Cap Refill 4 secs or greater: Yes",
        "Left Dorsalis Pedis Palpation: Present",
        "Right Dorsalis Pedis Palpation: Present",
        "Left Posterior Tibial Palpation: Present",
        "Right Posterior Tibial Palpation: Present",
        
        "Left Edema Severity: +1 Trace",
        "Right Edema Severity: +1 Trace",
        "Left Edema Location: Foot",
        "Right Edema Location: Foot",
        "Sleep Position: up",
        "Left Ankle (cm.mm): 1.1",
        "Right Ankle (cm.mm): 1.1",
        "Left Calf (cm.mm): 2.2",
        "Right Calf (cm.mm): 2.2",
        "Left Sensation: 1st digit",
        "Right Sensation: 1st digit",
        "Left Sensation Score/10: 1",
        "Right Sensation Score/10: 1",
        "Left Sensory: Numbness",
        "Right Sensory: Numbness",
        "Left Proprioception: Intact",
        "Right Proprioception: Intact",
        
        "Pulses by Doppler: Done by WCC/designate",
        "Left Dorsalis Pedis Doppler: Present",
        "Right Dorsalis Pedis Doppler: Present",
        "Left Posterior Tibial Doppler: Present",
        "Right Posterior Tibial Doppler: Present",
        "Left Interdigitial Doppler: Present",
        "Right Interdigitial Doppler: Present",
        "Ankle Brachial Pressure Index: Done by WCC/designate",
        "ABPI Date: 31/Jul/1977",
        "Left Posterior Tibialis: 1",
        "Right Posterior Tibialis: 1",
        "Left Dorsalis Pedis: 1",
        "Right Dorsalis Pedis: 1",
        "Left Brachial Systolic: 1",
        "Right Brachial Systolic: 1",
        "Left ABPI Score: 1.0",
        "Right ABPI Score: 1.0",
        "Toe Brachial Pressure Index: Done by WCC/designate",
        "TBPI Date: 31/Jul/1977",
        "Left Toe: 1",
        "Right Toe: 1",
        "Left Brachial Systolic: 1",
        "Right Brachial Systolic: 1",
        "Left TBPI Score: 1.0",
        "Right TBPI Score: 1.0",
        "Transcutaneous O2 Pressures: Done by WCC/designate",
        "TO2P Date: 31/Jul/1977",
        "Left Transcutaneous O2 Pressure: >20mm/hg",
        "Right Transcutaneous O2 Pressure: >20mm/hg",
       // "Limb Progress Notes: limb comments",
        "Left Deformities: None noted",
        "Right Deformities: None noted",
        "Left Skin: Normal/healthy",
        "Right Skin: Normal/healthy",
        "Left Toes: Normal",
        "Right Toes: Normal",
        "Weight Bearing Status: None",
        "Balance: Steady",
        "Calf Muscle Pump: Normal",
        "Calf Muscle Pump Comments: test",
        "Mobility Aids: Brace",
        "Mobility Aids Comments: test",
        "Gait Pattern Comments: test",
        "Walking Distance Comments: test",
        "Walking Endurance Comments: test",
        "Indoor Footwear: Yes",
        "Indoor Footwear Comments: testers",
        "Outdoor Footwear: Yes",
        "Outdoor Footwear Comments: testers",
        "Orthotics: Yes",
        "Orthotics Comments: testers",
        "Muscle Tone: High",
        "Arches Foot Structure: High",
        "Supination: Yes",
        "Pronation: Yes",
        "Dorsiflexion Active: Normal ROM",
        "Dorsiflexion Passive: Decrd ROM",
        "Plantarflexion Active: Normal ROM",
        "Plantarflexion Passive: Decrd ROM",
        "Great Toes Active: Normal ROM",
        "Great Toes Passive: Decrd ROM",
        "Foot Progress Notes: foot comments"};
        assertSelectOptionsEqual("patientprofile",pp);
        clickButton("saveEntries");
        gotoRootWindow();
        
        setWorkingForm("searchN");
        setTextField("search",ConstantsTest.PASSWORD+"");
        submit();
        clickLink("navPatient");
        setWorkingForm("form");
        
        assertCheckboxSelected("left_missing_limbs","1319");
        assertCheckboxSelected("right_missing_limbs","1319");
        assertCheckboxSelected("left_pain_assessment","1309");
        assertCheckboxSelected("right_pain_assessment","1309");
        assertCheckboxSelected("left_skin_assessment","1310");
        assertCheckboxSelected("right_skin_assessment","1310");
        assertSelectedOptionEquals("left_temperature_leg","Cold");
        assertSelectedOptionEquals("right_temperature_leg","Cold");
        assertSelectedOptionEquals("left_temperature_foot","Cold");
        assertSelectedOptionEquals("right_temperature_foot","Cold");
        assertSelectedOptionEquals("left_temperature_toes","Cold");
        assertSelectedOptionEquals("right_temperature_toes","Cold");
        assertSelectedOptionEquals("left_skin_colour_leg","Pale");
        assertSelectedOptionEquals("right_skin_colour_leg","Pale");
        assertSelectedOptionEquals("left_skin_colour_foot","Pale");
        assertSelectedOptionEquals("right_skin_colour_foot","Pale");
        assertSelectedOptionEquals("left_skin_colour_toes","Pale");
        assertSelectedOptionEquals("right_skin_colour_toes","Pale");
        assertSelectedOptionEquals("left_dorsalis_pedis_palpation","Present");
        assertSelectedOptionEquals("right_dorsalis_pedis_palpation","Present");
        assertSelectedOptionEquals("left_posterior_tibial_palpation","Present");
        assertSelectedOptionEquals("right_posterior_tibial_palpation","Present");
        assertCheckboxSelected("left_dorsalis_pedis_doppler","2427");
        assertCheckboxSelected("right_dorsalis_pedis_doppler","2427");
        assertCheckboxSelected("left_posterior_tibial_doppler","2427");
        assertCheckboxSelected("right_posterior_tibial_doppler","2427");
        assertCheckboxSelected("left_interdigitial_doppler","2427");
        assertCheckboxSelected("right_interdigitial_doppler","2427");
        assertTextFieldEquals("pain_comments","Comment");
        assertSelectedOptionEquals("ankle_brachial_month","Jul");
        assertTextFieldEquals("ankle_brachial_year","1977");
        assertSelectedOptionEquals("ankle_brachial_day","31");

        
        assertSelectedOptionEquals("toe_brachial_month","Jul");
        assertTextFieldEquals("toe_brachial_year","1977");
        assertSelectedOptionEquals("toe_brachial_day","31");
        
        assertTextFieldEquals("left_dorsalis_pedis_ankle_brachial","1");
        assertTextFieldEquals("right_dorsalis_pedis_ankle_brachial","1");
        
        assertTextFieldEquals("left_tibial_pedis_ankle_brachial","1");
        assertTextFieldEquals("right_tibial_pedis_ankle_brachial","1");
        
        assertTextFieldEquals("left_ankle_brachial","1");
        assertTextFieldEquals("right_ankle_brachial","1");
        
        //assertTextFieldEquals("left_abi_score_ankle_brachial","1");
        //assertTextFieldEquals("right_abi_score_ankle_brachial","1");
        
        assertTextFieldEquals("left_toe_pressure","1");
        assertTextFieldEquals("right_toe_pressure","1");
        
        assertTextFieldEquals("left_brachial_pressure","1");
        assertTextFieldEquals("right_brachial_pressure","1");
        
        //assertTextFieldEquals("left_tbi_score","1");
        //assertTextFieldEquals("right_tbi_score","1");
        
        assertSelectedOptionEquals("left_edema_severity","+1 Trace");
        assertSelectedOptionEquals("right_edema_severity","+1 Trace");
        
        assertSelectedOptionEquals("left_edema_location","Foot");
        assertSelectedOptionEquals("right_edema_location","Foot");
        assertTextFieldEquals("sleep_positions","up");
        assertTextFieldEquals("left_ankle_cm","1");
        assertTextFieldEquals("right_ankle_cm","1");
        assertTextFieldEquals("left_ankle_mm","1");
        assertTextFieldEquals("right_ankle_mm","1");
        
        assertTextFieldEquals("left_midcalf_cm","2");
        assertTextFieldEquals("right_midcalf_cm","2");
        assertTextFieldEquals("left_midcalf_mm","2");
        assertTextFieldEquals("right_midcalf_mm","2");
        
        assertCheckboxSelected("left_sensory","1191");
        assertCheckboxSelected("right_sensory","1191");
        assertSelectedOptionEquals("left_proprioception","Intact");
        assertSelectedOptionEquals("right_proprioception","Intact");
        
        assertCheckboxSelected("left_foot_deformities","1199");
        assertCheckboxSelected("right_foot_deformities","1199");
        
        assertCheckboxSelected("left_foot_skin","1216");
        assertCheckboxSelected("right_foot_skin","1216");
        
        assertCheckboxSelected("left_foot_toes","1211");
        assertCheckboxSelected("right_foot_toes","1211");
        
        assertSelectedOptionEquals("weight_bearing","None");
        assertSelectedOptionEquals("balance","Steady");
        assertSelectedOptionEquals("calf_muscle_pump","Normal");
        assertSelectedOptionEquals("mobility_aids","Brace");
        assertTextFieldEquals("mobility_aids_comments","test");
        assertTextFieldEquals("calf_muscle_comments","test");
        assertTextFieldEquals("gait_pattern_comments","test");

        //assertCheckboxSelected("left_capillary_less","1");
         assertRadioOptionSelected("left_capillary_more","2");
          //assertCheckboxSelected("right_capillary_less","1");
           assertRadioOptionSelected("right_capillary_more","2");

        assertTextFieldEquals("walking_distance_comments","test");
    
        assertTextFieldEquals("walking_endurance_comments","test");
        
        assertRadioOptionSelected("indoor_footwear","2");
        assertTextFieldEquals("indoor_footwear_comments","testers");
        
        assertRadioOptionSelected("outdoor_footwear","2");
        assertTextFieldEquals("outdoor_footwear_comments","testers");
        
        assertRadioOptionSelected("orthotics","2");
        assertTextFieldEquals("orthotics_comments","testers");
        
        assertSelectedOptionEquals("muscle_tone_functional","High");
        assertSelectedOptionEquals("arches_foot_structure","High");
        assertRadioOptionSelected("supination_foot_structure","2");
        assertRadioOptionSelected("pronation_foot_structure","2");
        assertCheckboxSelected("dorsiflexion_active","1247");
        
        assertCheckboxSelected("plantarflexion_active","1247");
        assertCheckboxSelected("greattoe_active","1247");
        assertCheckboxSelected("dorsiflexion_passive","1432");
        
        assertCheckboxSelected("plantarflexion_passive","1432");
        assertCheckboxSelected("greattoe_passive","1432");
        
        
    }
    @After
    public void close() {
        closeBrowser();
    }
}
