/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.offline;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class OfflineDownloadTestCase {
    //private final String CONTEXT="/LoginS";
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        // or setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);
        
        
        
    }
    
    @Test
 public void testVerifyConnection(){
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        assertTextPresent("Download Patients");
        assertTextPresent("Upload Patients");
    }

    @Test
 public void testFailedLogins() {
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","skywalker89");
        submit();
        assertTextPresent("Your user number and/or password has been entered incorrectly");
        
        //assertTitleEqualsKey("pixalere.main.form.title");
        //clickLink("navLogout");
    }
    @Test
 public void testFailedEmptyLogins() {
        beginAt("/LoginSetup.do");
        setTextField("userId","");
        setTextField("password","");
        submit();
        
        assertTextPresent("User Number and/or Password may be empty");
        
        //assertTitleEqualsKey("pixalere.main.form.title");
        //clickLink("navLogout");
    }
    @Test
 public void testFailedDownloadPatients(){
        //test downloading patients in TIP (should failed)
        //test downloadingpatients in correct locaton (should fail).. may need to use non 7 account as they have access to all
        
    }
    @Test
 public void testDownloadPatients(){
        beginAt("/LoginSetup.do");
        //assertTitleEqualsKey("pixalere.login.form.title");
        assertElementPresent("userId");
        assertElementPresent("password");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        clickRadioOption("offlineoption","sync");
        submit();
        assertTextPresent("Download Patients");
        assertTitleEqualsKey("pixalere.sync.form.title");
        setWorkingForm("form1");
        assertElementPresent("patient1");
        assertElementPresent("patient10");
        setTextField("patient1","3");
        setTextField("patient2","1711");
        setTextField("patient3","1712");
        setTextField("patient4","7167");
        submit();
        
        assertTextPresent("Patient 3 was downloaded");
    }
    @Test
 public void testSuccessfulLogin() {
        beginAt("/LoginSetup.do");
        //assertTitleEqualsKey("pixalere.login.form.title");
        assertElementPresent("userId");
        assertElementPresent("password");
        //assertElementPresent("database");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        //System.out.println(getPageSource());
        //assertTitleEqualsKey("pixalere.main.form.title");
        //clickLink("navLogout");
    }
    @After
    public void close() {
        closeBrowser();
    }
}
