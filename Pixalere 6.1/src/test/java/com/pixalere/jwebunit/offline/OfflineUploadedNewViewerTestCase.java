package com.pixalere.jwebunit.offline;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import net.sourceforge.jwebunit.html.Table;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.dao.PatientProfileDAO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.guibeans.RowData;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import java.util.Collection;

public class OfflineUploadedNewViewerTestCase  {
    //private final String CONTEXT="/LoginS";
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        // or setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        setWorkingForm("searchN");
        setTextField("search","500");
        submit();
        clickLink("navViewer");


    }


    @Test
 public void testNewPatientProfileFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");

        //submit();
        clickLink("patientFlowLink");
        String[] signatures = new String[3];
        try{
            PatientProfileVO vo = new PatientProfileVO();
            String patient_id="500";
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(1));
            PatientProfileDAO dao = new PatientProfileDAO();
            Collection<PatientProfileVO> results = dao.findAllByCriteria(vo,3);

            int count=0;
            for(PatientProfileVO wp : results){
                signatures[count]=wp.getUser_signature();
                count++;
            }
        }catch(Exception ex){}

        Table table = new Table(new Object[][] {

            {"Patient Profile",signatures[0]},
            {"Patient Residence","Fraser Health"},
            {"Treatment Location","Overlander"},

            {"Funding Source","ICBC"},
            {"Gender","Male"},
            {"Date of Birth","26/Jul/1977"},
            {"Age","31"},
            {"Allergies","Bees"},
            {"Other Health Care Professionals Involved","Dr Burns"},
            {"Co-Morbidities","Hypotension"},
            {"Surgical History","test surgical"},

            {"Factors that affect wound healing","Poor perfusion"},
            {"Factors that affect wound healing; Other",""},
            {"Medications that affect wound healing","Steroids"},

            //{"Braden Risk Score","11 (4/1/1/1/3/1)","11 (4/1/1/1/3/1)"},
            {"Diagnostic Tests","Biopsy : 1/July/1977"},
            {"Blood Glucose mmol.L","1"},
            {"Date","1/Jul/1977"},
            {"Time","1"},

            {"Albumin g/L","1.1"},
            {"Date","1/Jul/1977"},

            {"Prealbumin mg/L","1.2"},
            {"Date","31/Jul/1977"},

            {"","Print Column"}
        });
        assertTablePresent("ppflowchart");
        assertTableEquals("ppflowchart",table);


    }

    @Test
 public void testNewWoundProfileFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");

        //submit();
        clickLink("woundFlowLink");
        //get signatures
        String[] signatures = new String[3];
        try{
            WoundProfileVO vo = new WoundProfileVO();
            String wound_id="12073";
            vo.setWound_id(new Integer(wound_id));
            vo.setActive(new Integer(1));
            WoundProfileDAO dao = new WoundProfileDAO();
            Collection<WoundProfileVO> results = dao.findAllByCriteria(vo,3);

            int count=0;
            for(WoundProfileVO wp : results){
                signatures[count]=wp.getUser_signature();
                count++;
            }
        }catch(Exception ex){}

        Table table = new Table(new Object[][] {
            {""},
            {"Profile", signatures[0]},

            {"Amendments to Blue Person Graphics",""},
            {"Wound Location","Chest"},
            {"Cause/History", "Reason"},
            {"Etiology","Wound A: Venous Wound B: Venous Wound C: Venous Wound D: Venous"},
            {"Goal of Care", "Wound A: Maintain wound Wound B: Maintain wound Wound C: Maintain wound Wound D: Maintain wound"},

            {"","Print Column"},
            {""}
        });
        assertTablePresent("wpflowchart");
        assertTableEquals("wpflowchart",table);
    }
    @Test
 public void testNewWoundAssessmentFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        //submit();
        clickLink("assessFlowLink");
        clickLink("alphaa");
        //get signatures
        String[] signatures = new String[4];
        try{
            AssessmentServiceImpl manager=new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            AssessmentEachwoundVO crit= new AssessmentEachwoundVO();
            crit.setAlpha_id(new Integer("15983"));
            crit.setActive(new Integer(1));
            Collection<AssessmentEachwoundVO> items =  manager.getAllAssessments(crit,0,true);
            int count=0;
            for(AssessmentEachwoundVO wp : items){
                signatures[count]=wp.getWoundAssessment().getUser_signature();
                count++;
            }


        }catch(Exception ex){ex.printStackTrace();}
        System.out.println("Signatures on : "+ signatures.length);
        Table table = new Table(new Object[][] {
            {"Wound A"},
            {"Full Assessments Only: unchecked"},
            {"Wound Assessment", signatures[1],signatures[0]},
            {"Assessment Type","Full Assessment","Partial Assessment"},
            {"Status","Active","Active"},
            {"Reason for Closure","",""},
            {"Closure Date","",""},
            {"Date of Onset","1/Jul/1977","1/Jul/1977"},
            {"Recurrent","Yes","Yes"},
            {"Pain (scale 0-10)","1","1"},
            {"Pain Comments","",""},
            {"Length (cm.mm)","1.2",""},
            {"Width (cm.mm)","1.2",""},
            {"Depth (cm.mm)","1.2",""},
            {"Undermined Location","",""},
            {"Undermined Depth (cm.mm)","",""},
            {"Sinus Tract Location","",""},
            {"Sinus Tract Depth (cm.mm)","",""},
            {"Fistula","",""},
            {"Wound Bed","Hematoma: 100%","Hematoma: 100%"},
            {"Exudate","Nil","Nil"},
            {"Exudate Amount","Nil","Nil"},
            {"Odour (after cleansing)","No","No"},
            {"Wound Edge","Diffuse","Diffuse"},
            {"Peri-Wound Skin","Intact","Intact"},


            {"Products","3 Apron","3 Apron"},
            {"Packing Pieces Out/In","",""},
            {"Treatment Comments","comments comments","comments comments"},

            {"NPWT (VAC) Start Date","",""},
            {"NPWT (VAC) End Date","","",""},
            {"NPWT (VAC): Pressure Reading","",""},
            {"NPWT (VAC) Therapy Setting","",""},
            {"C&S Done","No","No"},
            {"C&S Result","",""},
            {"Antibiotic Therapy","",""},
            {"Treatment Modalities","",""},
            {"Assessment Images","0 Attached Images","0 Attached Images"},
            {"Request for Referral","No","No"},
            {"See Progress Notes","Yes","Yes"},
            {"","Print Column","Print Column"}


        });

        assertTablePresent("aflowchart");
        assertTableEquals("aflowchart",table);

        clickLink("alphab");
        //get signatures
        String[] signatures3 = new String[4];
        try{
            AssessmentServiceImpl manager=new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            AssessmentEachwoundVO crit= new AssessmentEachwoundVO();
            crit.setAlpha_id(new Integer("15984"));
            crit.setActive(new Integer(1));
            Collection<AssessmentEachwoundVO> items =  manager.getAllAssessments(crit,0,true);
            int count=0;
            for(AssessmentEachwoundVO wp : items){
                signatures3[count]=wp.getWoundAssessment().getUser_signature();
                count++;
            }


        }catch(Exception ex){ex.printStackTrace();}
        System.out.println("Signatures on : "+ signatures.length);
        Table table3 = new Table(new Object[][] {
            {"Wound B"},
            {"Full Assessments Only: unchecked"},
            {"Wound Assessment", signatures3[0]},
            {"Assessment Type","Full Assessment"},
            {"Status","Active"},
            {"Reason for Closure",""},
            {"Closure Date",""},
            {"Date of Onset","1/Jul/1977"},
            {"Recurrent","Yes"},
            {"Pain (scale 0-10)","1"},
            {"Pain Comments",""},
            {"Length","1.2"},
            {"Width","1.2"},
            {"Depth","1.2"},
            {"Undermined Location",""},
            {"Undermined Depth",""},
            {"Sinus Tract Location",""},
            {"Sinus Tract Depth",""},
            {"Fistula",""},
            {"Wound Bed","Hematoma: 100%"},
            {"Exudate","Nil"},
            {"Exudate Amount","Nil"},
            {"Odour (after cleansing)","No"},
            {"Wound Edge","Diffuse"},
            {"Peri-Wound Skin","Intact"},


            {"Products","3 Apron"},
            {"Packing Pieces Out/In",""},
            {"Treatment Comments","comments comments"},

            {"NPWT (VAC) Start Date",""},
            {"NPWT (VAC) End Date",""},
            {"NPWT (VAC): Pressure Reading",""},
            {"NPWT (VAC) Therapy Setting",""},
            {"C&S Done","No"},
            {"C&S Result",""},
            {"Antibiotic Therapy",""},
            {"Treatment Modalities",""},
            {"Assessment Images","0 Attached Images"},
            {"Request for Referral","No"},
            {"See Progress Notes","Yes"},
            {"","Print Column"}


        });

        assertTablePresent("aflowchart");
        assertTableEquals("aflowchart",table3);
    }
    @Test
 public void testNewCommentsTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        //submit();
        clickLink("commentsLink");
        assertElementPresent("comments_body");
        assertElementPresent("comments_recommendations");
        //PIX-973

        assertTextPresent("Wound Care Comment");


    }
   @After
    public void close() {
        closeBrowser();
    }

}