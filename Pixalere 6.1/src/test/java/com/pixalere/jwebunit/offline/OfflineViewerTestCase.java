/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.offline;
import com.pixalere.guibeans.RowData;
import com.pixalere.guibeans.FieldValues;
import java.util.Date;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.*;
import com.pixalere.utils.PDate;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

import java.util.List;
import net.sourceforge.jwebunit.html.Cell;
//import net.sourceforge.jwebunit.html.ExpectedTable;
import net.sourceforge.jwebunit.html.Table;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.patient.dao.PatientProfileDAO;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.wound.service.WoundServiceImpl;
import com.pixalere.wound.*;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import java.util.Collection;
public class OfflineViewerTestCase {
    //private final String CONTEXT="/LoginS";
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        
        //setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        // or setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        //clickLink("navAdmin");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navViewer");
        
    }
    @Test
 public void testViewerHomeTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        //submit();
        
        assertTextPresent("Progress");
        assertTextPresent("Patient");
        assertTextPresent("Wound");
        assertTextPresent("Assessment");
        assertTextPresent("Care");
        assertTextPresent("Product");
        assertTextPresent("Measurement");
        assertTextPresent("Viewer");
    }
    
    
    
    @Test
 public void testPatientProfileFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        
        //submit();
        clickLink("patientLink");
        String[] signatures = new String[3];
        try{
            PatientProfileVO vo = new PatientProfileVO();
            String patient_id="3";
            vo.setPatient_id(new Integer(patient_id));
            vo.setActive(new Integer(1));
            PatientProfileDAO dao = new PatientProfileDAO();
            Collection<PatientProfileVO> results = dao.findAllByCriteria(vo,3);
            
            int count=0;
            for(PatientProfileVO wp : results){
                signatures[count]=wp.getUser_signature();
                count++;
            }
        }catch(Exception ex){}
        
        String[][] table = new String[][] {
            
            {"Patient Profile",signatures[0]},
            
            {"Gender","Male"},
            {"Date of Birth","26/Jul/1977"},
            {"Age","33"},
            {"Allergies","Bees"},
            {"PHN","93765054"},{"Treatment Location","Richmond Lions Manor"},

            {"Funding Source","ICBC"},
             
            {"Co-Morbidities","Hypotension"},{"History or Present Illness","test surgical"},
           {"Other Health Care Professionals Involved","Dr Burns"},
            
            {"Factors that affect wound healing","Poor perfusion"},
            {"Factors that affect wound healing; Other",""},
            {"Medications that affect wound healing","Insulin"},
            
            {"Diagnostic Tests","Biopsy : 1/July/1977"},
           
            
            {"Date","1/Jul/1977"},
            {"Albumin g/L","1.1"},
            
            {"Date","31/Jul/1977"},
            {"Prealbumin mg/L","1.2"},
           

             {"Assessment Updates",""}
        };
        assertTablePresent("ppflowchart");

        for(int x = 0;x<table.length;x++){
            assertMatchInTable("ppflowchart",table[x]);
        }
        
       
        
        //check flowchart
    }
    @Test
 public void testWoundProfileFlowchartTab(){
        
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        
        //submit();
        clickLink("woundLink");
        //get signatures
        String[] signatures = new String[3];
        try{
            WoundProfileVO vo = new WoundProfileVO();
            String wound_id="810";
            vo.setWound_id(new Integer(wound_id));
            vo.setActive(new Integer(1));
            WoundProfileDAO dao = new WoundProfileDAO();
            Collection<WoundProfileVO> results = dao.findAllByCriteria(vo,3);
            
            int count=0;
            for(WoundProfileVO wp : results){
                signatures[count]=wp.getUser_signature();
                count++;
            }
        }catch(Exception ex){}
        
        String[][] table = new String[][] {
            {""},
            {"Profile", signatures[0]},
            
            {"Amendments to Blue Person Graphics",""},
            {"Wound Location","Chest"},
            {"Cause/History", "Reason"},
            {"Etiology","Wound A: Venous,Wound B: Venous,Wound C: Venous,Wound D: Venous"},
            {"Goal of Care", "Wound A: Maintain wound,Wound B: Maintain wound,Wound C: Maintain wound,Wound D: Maintain wound"},
            {"Surgical Procedure",""},{"Date of Surgery",""},
            {"Type of Ostomy",""},{"Surgeon",""},
            
            {"Ostomy Marking Prior to Surgery",""},
            {"Ostomy Patient Limitations",""},
            {"Ostomy Teaching Goals",""},
             {"Assessment Updates",""},
            {""}
        };
        assertTablePresent("wpflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("wpflowchart",table[x]);
        }
    }
    @Test
 public void testOstomyProfileFlowchartTab(){
        setWorkingForm("woundform");
        //get signatures
        WoundProfileVO vo = new WoundProfileVO();
        String wound_id="811";
        vo.setWound_id(new Integer(wound_id));
        vo.setActive(new Integer(1));
        String[] signature = new String[3];
        try{
            WoundProfileDAO dao = new WoundProfileDAO();
            Collection<WoundProfileVO> results = dao.findAllByCriteria(vo,3);
            
            int count=0;System.out.println("OfflineOstomy"+results.size());
            for(WoundProfileVO wp : results){
                signature[count]=wp.getUser_signature();
                count++;
            }
        } catch(Exception ex){}
        selectOption("wound_profile_type_id","Abdomen - Ostomy");
        //submit();
        clickLink("woundLink");
        String[][] table = new String[][] {
            {""},
            
            {"Profile", signature[0]},
            {"Amendments to Blue Person Graphics",""},
            {"Wound Location","Abdomen"},
            {"Cause/History", "Reason"},
            {"Etiology", "Fecal Stoma: Trauma,Urostomy: Trauma"},
            {"Goal of Care", "Fecal Stoma: To be detemined - ostomy,Urostomy: To be detemined - ostomy"},
            {"Surgical Procedure",""},{"Date of Surgery",""},
            {"Type of Ostomy",""},{"Surgeon",""},
            
            {"Ostomy Marking Prior to Surgery",""},
            {"Ostomy Patient Limitations",""},
            {"Ostomy Teaching Goals",""},
             {"Assessment Updates",""},
            
            {""}
        };
        assertTablePresent("wpflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("wpflowchart",table[x]);
        }
    }
    @Test
 public void testPostOpProfileFlowchartTab(){
        setWorkingForm("woundform");
        //get signatures
        WoundProfileVO vo = new WoundProfileVO();
        String wound_id="812";
        vo.setWound_id(new Integer(wound_id));
        vo.setActive(new Integer(1));
        String[] signatures = new String[3];
        try{
            WoundProfileDAO dao = new WoundProfileDAO();
            Collection<WoundProfileVO> results = dao.findAllByCriteria(vo,0);
            
            int count=0;
            for(WoundProfileVO wp : results){
                signatures[count]=wp.getUser_signature();
                count++;
            }
        }catch(Exception ex){}
        selectOption("wound_profile_type_id","Head/neck (Anterior) - Incision");
        //submit();
        clickLink("woundLink");
        String[][] table = new String[][] {
            {""},
            {"Profile", signatures[0]},
            {"Amendments to Blue Person Graphics",""},
            {"Wound Location","Head/neck*"},
            {"Cause/History", "Reason"},

            {"Etiology", "Incision 1: Obstruction,Tube/Drain 1: Obstruction,Tube/Drain 2: Obstruction"},
            {"Goal of Care", "Incision 1: Temporary tube/drain,Tube/Drain 1: Temporary tube/drain,Tube/Drain 2: Temporary tube/drain"},
            {"Surgical Procedure","Operative Procedure Comments"},{"Date of Surgery","1/Jul/1977"},
            {"Type of Ostomy",""},{"Surgeon","Dr. Smithers"},
            
            {"Ostomy Marking Prior to Surgery",""},
            {"Ostomy Patient Limitations",""},
            {"Ostomy Teaching Goals",""},
             {"Assessment Updates",""},
            {""}
        };
        assertTablePresent("wpflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("wpflowchart",table[x]);
        }
    }
   @Test
 public void testFootFlowchart(){

        clickLink("footLink");
        //get signatures
        String[] signatures = new String[3];
        try{
            FootAssessmentVO vo = new FootAssessmentVO();

            vo.setPatient_id(new Integer(3));
            vo.setActive(new Integer(1));
            PatientProfileServiceImpl b = new PatientProfileServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            Collection<FootAssessmentVO> results = b.getAllFootAssessments(vo,3);

            int count=0;
            for(FootAssessmentVO wp : results){
                signatures[count]=wp.getUser_signature();
                count++;
            }
        }catch(Exception ex){}

        String[][] table = new String[][] {
            {"Foot Assessment", signatures[0]},
           {"Left Deformities","None noted"},
           {"Right Deformities","None noted"},
           {"Left Skin","Normal"},
           {"Right Skin","Normal"},
           {"Left Toes","Normal"},
           {"Right Toes","Normal"},
           {"Weight Bearing Status","None"},
           {"Balance","Steady"},
           {"Calf Muscle Pump","Normal"},
           {"Calf Muscle Pump Comments","test"},
           {"Mobility Aids","Brace"},
           {"Mobility Aids Comments","test"},
           {"Gait Pattern Comments","test"},
           {"Walking Distance Comments","test"},
           {"Walking Endurance Comments","test"},
           {"Indoor Footwear","Yes"},
           {"Indoor Footwear Comments","testers"},
           {"Outdoor Footwear","Yes"},
           {"Outdoor Footwear Comments","testers"},
           {"Orthotics","Yes"},
           {"Orthotics Comments","testers"},
           {"Muscle Tone","High"},
           {"Arches Foot Structure","High"},
           {"Supination","Yes"},
           {"Pronation","Yes"},
           {"Dorsiflexion Active","Normal ROM"},
           {"Dorsiflexion Passive","Decrd ROM"},
           {"Plantarflexion Active","Normal ROM"},
           {"Plantarflexion Passive","Decrd ROM"},
           {"Great Toes Active","Normal ROM"},
           {"Great Toes Passive","Decrd ROM"},
           {"Foot Progress Notes","foot comments"},
             {"Assessment Updates",""}
        };
        assertTablePresent("footflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("footflowchart",table[x]);
        }
    }
    @Test
 public void testLimbFlowchart(){

        clickLink("limbLink");
        //get signatures
        String[] signatures = new String[3];
        try{
            LimbBasicAssessmentVO vo = new LimbBasicAssessmentVO();

            vo.setPatient_id(new Integer(3));
            vo.setActive(new Integer(1));
            PatientProfileServiceImpl b = new PatientProfileServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            Collection<LimbBasicAssessmentVO> results = b.getAllBasicLimbAssessments(vo,3);

            int count=0;
            for(LimbBasicAssessmentVO wp : results){
                signatures[count]=wp.getUser_signature();
                count++;
            }
        }catch(Exception ex){}

       String[][] table = new String[][] {

            {"Limb Assessment", signatures[0]},
       {"Left Missing Limb or Toes","No amputations"},
       {"Right Missing Limb or Toes","No amputations"},
       {"Left Pain","No pain"},
       {"Right Pain","No pain"},
       {"Pain Comments","Comment"},
       {"Left Skin","Normal*"},
       {"Right Skin","Normal*"},
       {"Left Temperature Leg","Cold"},
       {"Right Temperature Leg","Cold"},
       {"Left Temperature Foot","Cold"},
       {"Right Temperature Foot","Cold"},
       {"Left Temperature Toes","Cold"},
       {"Right Temperature Toes","Cold"},
       {"Left Skin Colour Leg","Pale"},
       {"Right Skin Colour Leg","Pale"},
       {"Left Skin Colour Foot","Pale"},
       {"Right Skin Colour Foot","Pale"},
       {"Left Skin Colour Toes","Pale"},
       {"Right Cap Refill 4 secs or greater","Yes"},
       {"Right Cap Refill less than 4 secs",""},
       {"Left Cap Refill 4 secs or greater","Yes"},{"Left Cap Refill less than 4 secs" ,""},
       

       {"Right Skin Colour Toes","Pale"},
       {"Left Dorsalis Pedis Palpation","Present"},
       {"Right Dorsalis Pedis Palpation","Present"},
       {"Left Posterior Tibial Palpation","Present"},
       {"Right Posterior Tibial Palpation","Present"},


       //{"Left Edema Severity","*Trace"},
       //{"Right Edema Severity","*Trace"},
       {"Left Edema Location","Foot"},
       {"Right Edema Location","Foot"},
       {"Sleep Position","up"},
       {"Left Ankle*","1.1"},
       {"Right Ankle*","1.1"},
       {"Left Calf*","2.2"},
       {"Right Calf*","2.2"},
       {"Left Sensation","1st digit"},
       {"Right Sensation","1st digit"},
       {"Left Sensation Score*","1"},
       {"Right Sensation Score*","1"},
       {"Left Sensory","Numbness"},
       {"Right Sensory","Numbness"},
       {"Left Proprioception","Intact"},
       {"Right Proprioception","Intact"},
       {"Pulses by Doppler","Done by WCC*"},
       {"Left Dorsalis Pedis Doppler","Present"},
       {"Right Dorsalis Pedis Doppler","Present"},
       {"Left Posterior Tibial Doppler","Present"},
       {"Right Posterior Tibial Doppler","Present"},
       {"Left Interdigitial Doppler","Present"},
       {"Right Interdigitial Doppler","Present"},
       {"Ankle Brachial Pressure Index","Done by WCC*"},
       {"ABPI Date","31/Jul/1977"},
       {"Left Posterior Tibialis","1"},
       {"Right Posterior Tibialis","1"},
       {"Left Dorsalis Pedis","1"},
       {"Right Dorsalis Pedis","1"},
       {"Left Brachial Systolic","1"},
       {"Right Brachial Systolic","1"},
       {"Left ABPI Score","1.0"},
       {"Right ABPI Score","1.0"},
       {"Toe Brachial Pressure Index","Done by WCC*"},
       {"TBPI Date","31/Jul/1977"},
       {"Left Toe","1"},
       {"Right Toe","1"},
       {"Left Brachial Systolic","1"},
       {"Right Brachial Systolic","1"},
       {"Left TBPI Score","1.0"},
       {"Right TBPI Score","1.0"},
       {"Transcutaneous*","Done by WCC*"},
       {"TO*","31/Jul/1977"},
       //Left Transcutaneous*","*20mm/hg"},
       //{"Right Transcutaneous*","*20mm/hg"},
             {"Assessment Updates",""}
        
        };
        assertTablePresent("limbflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("limbflowchart",table[x]);
        }
    }
@Test
 public void testWoundAssessmentFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        //submit();
        clickLink("assessmentLink");
        clickButton("alphaa");
        //get signatures
        String[] signatures = new String[4];
        try{
            AssessmentServiceImpl manager=new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            AssessmentEachwoundVO crit= new AssessmentEachwoundVO();
            crit.setAlpha_id(new Integer("1632"));
            crit.setActive(new Integer(1));
            Collection<AssessmentEachwoundVO> items =  manager.getAllAssessments(crit,0,true);
            int count=0;
            for(AssessmentEachwoundVO wp : items){
                signatures[count]=wp.getWoundAssessment().getUser_signature();
                count++;
            }


        }catch(Exception ex){ex.printStackTrace();}
        System.out.println("Signatures on : "+ signatures.length);
        String[][] table = new String[][] {
            {"Wound A"},
            {"Full Assessments Only: unchecked"},
            {"Wound Assessment", signatures[0]},
            {"Assessment Type","Full Assessment"},
            {"Status","Active"},
            {"Date of Onset","1/Jul/1977"},
            {"Reason for Closure",""},
            {"Closure Date",""},
            {"Recurrent","Yes"},
            {"Pain Comments",""},{"Pain*","2"},
            
            {"Length*","1.2"},
            {"Width*","1.2"},
            {"Depth*","1.2"},
            {"Undermined Location","#1 1:00 to 1:00"},
            {"Undermined Depth*","#1 1.2"},
            {"Sinus Tract Location","#1 1:00"},
          
            {"Fistula","Fistula #1: fistulas"},
            {"Sinus Tract Depth*","#1 1.1"},  {"Wound Bed","Hematoma: 100%"},
            {"Exudate","Nil"},
            {"Exudate Amount","Nil"},
            {"Odour*","No"},
            {"Wound Edge","Diffuse"},
            {"Peri-Wound Skin","Intact"},


            
            {"Products","0.5 Apron"},
            {"Treatment Comments","comments"},
            
            {"Packing pieces In","2"},{"Packing Pieces Out","1"},
            {"NPWT*","26/Jul/1977"},
                {"NPWT*","26/Jul/1977"},
                {"NPWT*","Constant"},

            {"NPWT*","25 mm/hg"},
            {"C&S Done","Yes"},
            {"C&S Result","No Growth"},
            {"Antibiotic Therapy","Wound A: Stuff : 26/Jul/1977 to 26/Jul/1977"},
            {"Treatment Modalities",""},
            {"Assessment Images","0 Attached Images"},
            {"Request for Referral","No"},
            {"See Progress Notes","No"},
             {"Assessment Updates",""},{"Printable Summary Report",""}


        };
        assertTablePresent("aflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("aflowchart",table[x]);
        }



    }
    @Test
 public void testUrostomyAssessmentFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Abdomen - Ostomy");
        //submit();
        clickLink("assessmentLink");
        clickElementByXPath("//button[@id='alphaostu1']");
        //System.out.println(getPageSource());
        clickButton("alphaostu1");//System.out.println(getPageSource());
        //get signatures
        String[] signatures = new String[3];
        String[] postopdays= new String[2];
        try{
            AssessmentServiceImpl manager=new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            AssessmentOstomyVO crit= new AssessmentOstomyVO();
            crit.setAlpha_id(new Integer("1637"));
            crit.setActive(new Integer(1));
            Collection<AssessmentOstomyVO> items =  manager.getAllAssessments(crit,0,true);
            
            RowData[] results = manager.getAllOstomyAssessmentsForFlowchart(items, new ProfessionalVO(7), true);
            System.out.println("ostomy rows"+results.length);
            RowData row1 = (RowData)results[0];
            //RowData row2 = (RowData)results[1];
            
            signatures[0]=((List<FieldValues>)row1.getFields()).get(1).getValue();
            postopdays[0]=((List<FieldValues>)row1.getFields()).get(6).getValue();
            //postopdays[1]=((List<FieldValues>)row2.getFields())[6].getValue();
            signatures[0]=signatures[0].substring(87,signatures[0].length()-8);
            //signatures[1]=((List<FieldValues>)row2.getFields())[1].getValue();
            //signatures[1]=signatures[1].substring(87,signatures[1].length()-8);
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        String[][] table = new String[][] {
            {"Urostomy 1"},
            {"Full Assessments Only: unchecked"},
            {"Ostomy Assessment", signatures[0]},
            {"Assessment Type","Full Assessment"},
            {"Wound Status","Active/Nursing"},
            {"Reason for Closure",""},
            {"Closure Date",""},
            {"Post-Op Day",postopdays[0]},
            {"Clinical Pathway","Off"},
            {"Off Clinical Path Date",""},
            {"Construction","Loop"},
            {"Stoma Shape","Round"},
            {"Width","4"},
            {"Length","3"},
            {"Circumference*","0"},
            {"Profile","Raised"},
            {"Appearance","Red"},
            {"Devices","Not applicable"},
            {"Abdominal Contour","Rounded"},
            {"Concerns for Pouching","N/A"},
            {"Mucocutaneous Margin","Separated"},
            {"Mucocutaneous Margin Separation Depth","#1 3.3"},
            {"Mucocutaneous Margin Separation Location","#1 3:00 to 3:00"},
            {"Separation Comments",""},
            {"Peri-Ostomy Skin","Intact"},
            {"Mucous Fistula","No"},
            {"Fistula Drainage","Mucousy"},
            {"Mucous Fistula Comments","testing 1 2 3"},
            {"Peri-Fistula Skin","Weepy"},
            {"Urine Colour","Nil"},
            {"Urine Type","Clear"},
            {"Urine Quantity","Small"},
            {"Stool Quantity",""},
            {"Stool Consistancy",""},
            {"Stool Colour",""},
            {"Nutritional Status","NPO"},
            {"Nutritional Status Other","Nutritional Status Other"},
            {"Flange Pouch","Changed - routine"},
            {"Flange Pouch Comments","fpc"},
            {"Self Care Progress Comments","scpc"},
            {"Self Care Progress","Viewed emptying pouch"},
            {"C&S Done","Yes"},
                {"C&S Result",""},
{"Treatment Comments","comments\ncomments"},
{"Products","1 Apron"},
            {"Assessment Images","0 Attached Images"},
            {"Request for Referral","No"},
            {"See Progress Notes","Yes"},
             {"Assessment Updates",""},{"Printable Summary Report",""}
        };
        assertTablePresent("aflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("aflowchart",table[x]);
        }
    }
    @Test
 public void testIncisionAssessmentFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Head/neck (Anterior) - Incision");
        //submit();
        clickLink("assessmentLink");
        clickButton("alphatag1");
        //get signatures
        String[] signatures = new String[2];
        String[] postopdays = new String[2];
        try{
            AssessmentServiceImpl manager=new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            AssessmentIncisionVO crit= new AssessmentIncisionVO();
            crit.setAlpha_id(new Integer("1638"));
            crit.setActive(new Integer(1));
            Collection<AssessmentIncisionVO> items =  manager.getAllAssessments(crit,0,true);
            RowData[] results = manager.getAllIncisionAssessmentsForFlowchart(items, new ProfessionalVO(7), true);
            RowData row1 = (RowData)results[0];
            signatures[0]=((List<FieldValues>)row1.getFields()).get(1).getValue();
            signatures[0]=signatures[0].substring(87,signatures[0].length()-8);
            postopdays[0]=((List<FieldValues>)row1.getFields()).get(6).getValue();
        }catch(Exception ex){}
        
String[][] table = new String[][] {
            {"Incision 1"},
            {"Full Assessments Only: unchecked"},
            {"Incision Assessment", signatures[0]},
            {"Assessment Type","Full Assessment"},
            {"Wound Status","Active"},
            {"Reason for Closure",""},
            {"Closure Date",""},
            {"Post-Op Day",postopdays[0]},
            {"Clinical Pathway","On"},
            
            {"Post-Op Management",""},
{"Off Clinical Path Date",""},
            {"Pain Comments",""},{"Pain at Site*","1"},
            {"Closure Method*","Sutures"},
            {"Incision Status","Approximated"},
            
            {"Exudate","Nil"},
            {"Exudate Amount","Nil"},
            {"Peri-Incisional Skin","Intact"},


            {"Products","1 Apron"},
 {"C&S Done","Yes"},           {"Treatment Comments","comments\ncomments"},
 
                    {"C&S Result","MRSA"},
            {"Antibiotic Therapy","Incision 1: drug A : 26/7/1977 to 26/7/1977"},
            {"Assessment Images","0 Attached Images"},

            {"Request for Referral","No"},
            {"See Progress Notes","Yes"},
            {"Assessment Updates",""},{"Printable Summary Report",""}

        };
        assertTablePresent("aflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("aflowchart",table[x]);
        }
    }
    @Test
 public void testDrainAssessmentFlowchartTab(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Head/neck (Anterior) - Tube/Drain");
        //submit();
        //get signatures
        String[] signatures = new String[3];
        String dte="";
        try{
            AssessmentServiceImpl manager=new AssessmentServiceImpl(com.pixalere.utils.Constants.ENGLISH);
            AssessmentDrainVO crit= new AssessmentDrainVO();
            crit.setAlpha_id(new Integer("1639"));
            crit.setActive(new Integer(1));
            Collection<AssessmentDrainVO> items =  manager.getAllAssessments(crit,0,true);
            RowData[] results = manager.getAllDrainAssessmentsForFlowchart(items, new ProfessionalVO(7),  true);
            RowData row1 = (RowData)results[0];
            signatures[0]=((List<FieldValues>)row1.getFields()).get(1).getValue();
            
            signatures[0]=signatures[0].substring(87,signatures[0].length()-8);
             Date d = new Date();
            String y = new PDate().getYear(d);
            String m = new PDate().getMonth(d,"en");
            String dd = new PDate().getDay(d);
            dte = dd+"/"+m+"/"+y;
        }catch(Exception ex){}
        clickLink("assessmentLink");
       
       String[][] table = new String[][] {
            {"Tube/Drain 1"},{"Full Assessments Only: unchecked"},
            {"Tube/Drain Assessment", signatures[0]},
              {"Assessment Type","Full Assessment"},
            {"Remove/Close Date",""}, {"Status","Active"},
           
            {"Reason for Removal",""},

            {"Removed Intact",""},
            {"Type of Tube/Drain","Jackson Pratt drain"},
            {"Other","tod other"},
            {"Tube/Drain Sutured","Yes"},
            
            {"Pain at Site*",""},{"Tube/Drain Site","Intact"},
           {"Pain Comments",""}, {"Additional Days Date",", 1/Jul/1977, 1/Jul/1977,"},
            
            {"Drainage Amount ml","1, 1, 1, 0"},
            {"Drainage Amount hrs","1, 1, 1, 0"},
            {"Drainage Amount Start","3:00 to 3:00, 3:00 to 3:00, 3:00 to 3:00,"},

                {"Characteristics",""},

            {"Tube/Drain Changed","1/Jul/1977"},
             {"Peri - Tube/Drain Skin",""},
            {"Products","1 Apron"},
              {"Treatment Comments","comments\ncomments"},
             {"C&S Done","Yes"},
                {"C&S Result","MRSA"},

            {"Antibiotic Therapy","drug A : 26/7/1977 to 26/7/1977"},

            {"Assessment Images","0 Attached Images"},
             {"Request for Referral","No"},
            {"See Progress Notes","Yes"},
            {"Assessment Updates",""},{"Printable Summary Report",""}
        };
        assertTablePresent("aflowchart");
        for(int x = 0;x<table.length;x++){
            assertMatchInTable("aflowchart",table[x]);
        }
    }
    @Test
 public void testProductHistoryTab(){
        
    }
    /*Need to test each field type:
     * input
     * select
     * date component
     * products
     * comments
     * wound base etc
     *public static final String MULTI_DROPDOWN_DATE_COMPONENT_TYPE="MDDA";//antibiotics
    public static final String DROPDOWN_DATE_COMPONENT_TYPE="DDA";//investgations
    public static final String TEXTFIELD_COMPONENT_TYPE="TXT";
    public static final String MULTI_DROPDOWN_COMPONENT_TYPE="MDRP";
    public static final String RADIO_COMPONENT_TYPE="RADO";
    public static final String MEASUREMENT_COMPONENT_TYPE="MEAS";
    public static final String MULTI_MEASUREMENT_COMPONENT_TYPE="MMAS";
    public static final String YN_COMPONENT_TYPE="YN";
    public static final String MULTILIST_COMPONENT_TYPE="MLST";
    public static final String ASSIGN_AND_SELECT_COMPONENT_TYPE="ASSS";
    public static final String SELECT_BY_ALPHA_COMPONENT_TYPE="SALP";
     */
    
    @After
    public void close() {
        closeBrowser();
    }
}
