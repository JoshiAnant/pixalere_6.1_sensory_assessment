
package com.pixalere.jwebunit.offline;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import net.sourceforge.jwebunit.html.Table;
import com.pixalere.patient.bean.PatientProfileVO;
import com.pixalere.patient.dao.PatientProfileDAO;
import com.pixalere.patient.service.PatientProfileServiceImpl;
import com.pixalere.wound.bean.WoundProfileVO;
import com.pixalere.patient.bean.FootAssessmentVO;
import com.pixalere.patient.bean.LimbBasicAssessmentVO;
import com.pixalere.wound.dao.WoundProfileDAO;
import com.pixalere.assessment.service.AssessmentServiceImpl;
import com.pixalere.assessment.bean.AssessmentEachwoundVO;
import com.pixalere.assessment.bean.AssessmentOstomyVO;
import com.pixalere.assessment.bean.AssessmentIncisionVO;
import com.pixalere.assessment.bean.AssessmentDrainVO;
import com.pixalere.guibeans.RowData;
import com.pixalere.guibeans.FieldValues;
import com.pixalere.auth.bean.ProfessionalVO;
import com.pixalere.utils.PDate;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import java.util.Collection;

public class OfflineUploadTestCase  {
    //private final String CONTEXT="/LoginS";
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        // or setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);
        
        
        
    }
    
    
    @Test
 public void testUploadPatients(){

        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        clickRadioOption("offlineoption","upload");
        submit();
        setWorkingForm("form");
        String[] values = {"3"};
        selectOptionsByValues("uploadlist",values);
        clickButton("button1");
        assertTextPresent("Successfully Upload Files");
        
    }

    @After
    public void close() {
        closeBrowser();
    }
   
}
