package com.pixalere.jwebunit.offline;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class OfflinePostOpAssessmentTestCase  {
    //private final String CONTEXT="/LoginS";
    private String[] WOUND_ALPHAS={"A","B","C","D"};
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","shadow7749");
        submit();
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Facial - Post-Op");
        //submit();
        clickLink("navAssessment");
    }
     @After
    public void close() {
        closeBrowser();
    }
    
}