package com.pixalere.jwebunit.offline;

import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class OfflineOstomyAssessmentTestCase  {
    //private final String CONTEXT="/LoginS";
    //private String[] WOUND_ALPHAS={"A","B","C","D"};
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Abdomen");
        //submit();
        clickLink("navAssessment");
    }
    @Test
 public void testVerifyAssessment_Urostomy(){
        setWorkingForm("void");
        assertElementPresent("cancel1");
        checkCheckbox("cancel");
        //System.out.println(getPageSource());
        clickButton("bgbtnostu1");
        setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        //System.out.println(getPageSource());
        
        //assertSelectedOptionValueEquals("clinical_path_date_day","1");
        //assertSelectedOptionEquals("clinical_path_date_month","July");
        //assertTextFieldEquals("clinical_path_date_year","1977");
        setWorkingForm("populate");
        clickRadioOption("assessment_type","1");
        setWorkingForm("form");
        assertSelectedOptionEquals("circumference_mm","N/A");
        assertSelectedOptionEquals("length_mm","3");
        assertSelectedOptionEquals("width_mm","4");
        assertRadioOptionSelected("construction","1369");
        
        assertCheckboxSelected("urine_colour","1026");
        assertCheckboxSelected("urine_type","1596");
        assertRadioOptionSelected("urine_quantity","1028");
        //TODOassertTextFieldEquals("urine_quantity_other","");
        //TODOassertTextFieldEquals("stool_quantity_other","");
        
        assertCheckboxSelected("self_care_progress","1017");
        assertRadioOptionSelected("shape" ,"1158");
        assertCheckboxSelected("profile","1476");
        assertCheckboxSelected("pouching","1542");
        assertCheckboxSelected("appearance","1355");
        assertCheckboxSelected("contour","1021");
        
        assertRadioOptionSelected("devices","1541");
        //TODOassertTextFieldEquals("pouch_selection_other","test");
        
        clickLink("navTreatment");
        
        assertTextPresent("There are unfinished assessments (Fecal Stoma 1). Click Stay to remain on the assessment screen, or Go to proceed.");
        clickButton("go_confirm");
       // System.out.println("Treatment doing: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        //System.out.println(getPageSource());
        selectOption("products","Apron");
        setTextField("ostomy_treatment_comments","comments offline2");
        selectOption("wound_ass","Urostomy");
        clickButton("addAlpha");
        clickLink("navSummary");
        clickButton("saveEntries");
        //System.out.println("Going to Summary: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        //
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_ostomy","n");
        //selectOption("priority","");
        clickButton("saveEntries");
      
        //System.out.println("Assessment Ssaved: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        
    }
    @After
    public void close() {
        closeBrowser();
    }
}