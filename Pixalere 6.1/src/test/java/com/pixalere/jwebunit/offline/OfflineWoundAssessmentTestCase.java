package com.pixalere.jwebunit.offline;
import java.util.*;

import com.pixalere.utils.PDate;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class OfflineWoundAssessmentTestCase  {
    //private final String CONTEXT="/LoginS";
    private String[] WOUND_ALPHAS={"A","B","C","D"};
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        beginAt("/LoginSetup.do");
        //System.out.println("Assessment SetUp: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
                
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        //System.out.println("Going to Wound: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Chest");
        //submit();
        clickLink("navAssessment");
        //System.out.println("Going to Assessment: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
    }
    @Test
 public void testVerifyAssessment_A() throws com.pixalere.common.ApplicationException{
        setWorkingForm("void");
        assertElementPresent("cancel1");
        checkCheckbox("cancel");
        clickLink("alphaA");
        setWorkingForm("populate");
        
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        //System.out.println(getPageSource());

        setWorkingForm("form");
        assertSelectedOptionEquals("wound_day","1");
        assertSelectedOptionEquals("wound_month","Jul");
        assertTextFieldEquals("wound_year","1977");
        assertRadioOptionSelected("recurrent","2");
        assertSelectedOptionEquals("pain","2");
        assertSelectedOptionEquals("length","1");
        assertSelectedOptionEquals("length_milli","2");
        assertSelectedOptionEquals("width","1");
        assertSelectedOptionEquals("width_milli","2");
        assertSelectedOptionEquals("depth","1");
        assertSelectedOptionEquals("depth_milli","2");

        assertCheckboxSelected("woundbase","Hematoma");
        assertSelectedOptionEquals("woundbase_percentage_4","100%");
        
        assertCheckboxSelected("exudate_type","222");
        assertRadioOptionSelected("exudate_amount","225");
        assertRadioOptionSelected("exudate_odour","1");
        assertCheckboxSelected("wound_edge","436");
        assertCheckboxSelected("periwound","746");
        selectOption("pain","3");
        //System.out.println(getPageSource());

        clickLink("navTreatment");
        setWorkingForm("form");
        //submit();
      
        
        assertTextPresent("There are unfinished assessments (B,C,D). Click Stay to remain on the assessment screen, or Go to proceed");

        clickButton("go_confirm");
        

        //System.out.println(getPageSource());
        selectOption("products","Apron");
        setTextField("wound_treatment_comments","comments offline");
        setTextField("wound_nursing_care_plan","ncp offline");
        selectOption("wound_ass","Wound A");
        clickButton("addAlpha");
        clickLink("navSummary");
        clickButton("saveEntries");
     
        
        //
        
        assertTextPresent("Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
        
        clickRadioOption("referral_wound","n");
        //selectOption("priority","");
       
        clickButton("saveEntries");
        
        //gotoWindow("infowindow");
    
        
    }
 @After
    public void close() {
        closeBrowser();
    }
}