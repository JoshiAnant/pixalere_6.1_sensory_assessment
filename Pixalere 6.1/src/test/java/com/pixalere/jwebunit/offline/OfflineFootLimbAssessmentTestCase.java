package com.pixalere.jwebunit.offline;
import java.util.*;
import com.pixalere.utils.PDate;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class OfflineFootLimbAssessmentTestCase  {
    //private final String CONTEXT="/LoginS";
    private String[] WOUND_ALPHAS={"A","B","C","D"};
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        beginAt("/LoginSetup.do");
        //System.out.println("Assessment SetUp: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
                
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        //System.out.println("Going to Wound: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
        
        clickLink("navPatient");
        
        }
    @Test
 public void testLimbProfile(){
        //System.out.println(getPageSource());
        //assertRadioOptionSelected("goals","946");
        assertCheckboxSelected("investigation","317");
        
        assertSelectedOptionEquals("bloodsugar_day","1");
        assertSelectedOptionEquals("bloodsugar_month","Jul");
        assertTextFieldEquals("bloodsugar_year","1977");
        assertTextFieldEquals("blood_sugar_meal"," 1 ");
        assertSelectedOptionEquals("albumin_day","1");
        assertTextFieldEquals("albumin_year","1977");
        assertSelectedOptionEquals("albumin_month","Jul");
        assertSelectedOptionEquals("prealbumin_month","Jul");
        assertTextFieldEquals("prealbumin_year","1977");
        assertSelectedOptionEquals("prealbumin_day","31");
 
        assertCheckboxSelected("left_missing_limbs","1039");
        assertCheckboxSelected("right_missing_limbs","1039");
        assertCheckboxSelected("left_pain_assessment","1040");
        assertCheckboxSelected("right_pain_assessment","1040");
        assertCheckboxSelected("left_skin_assessment","1041");
        assertCheckboxSelected("right_skin_assessment","1041");
        assertSelectedOptionEquals("left_temperature_leg","Cold");
        assertSelectedOptionEquals("right_temperature_leg","Cold");
        assertSelectedOptionEquals("left_temperature_foot","Cold");
        assertSelectedOptionEquals("right_temperature_foot","Cold");
        assertSelectedOptionEquals("left_temperature_toes","Cold");
        assertSelectedOptionEquals("right_temperature_toes","Cold");
        assertSelectedOptionEquals("left_skin_colour_leg","Pale");
        assertSelectedOptionEquals("right_skin_colour_leg","Pale");
        assertSelectedOptionEquals("left_skin_colour_foot","Pale");
        assertSelectedOptionEquals("right_skin_colour_foot","Pale");
        assertSelectedOptionEquals("left_skin_colour_toes","Pale");
        assertSelectedOptionEquals("right_skin_colour_toes","Pale");
        assertSelectedOptionEquals("left_dorsalis_pedis_palpation","Yes");
        assertSelectedOptionEquals("right_dorsalis_pedis_palpation","Yes");
        assertSelectedOptionEquals("left_posterior_tibial_palpation","Yes");
        assertSelectedOptionEquals("right_posterior_tibial_palpation","Yes");
        assertSelectedOptionEquals("left_dorsalis_pedis_doppler","Yes");
        assertSelectedOptionEquals("right_dorsalis_pedis_doppler","Yes");
        assertSelectedOptionEquals("left_posterior_tibial_doppler","Yes");
        assertSelectedOptionEquals("right_posterior_tibial_doppler","Yes");
        assertSelectedOptionEquals("left_interdigitial_doppler","Yes");
        assertSelectedOptionEquals("right_interdigitial_doppler","Yes");
        assertTextFieldEquals("left_pain_comments","Comment");
        assertTextFieldEquals("right_pain_comments","Comment");
        assertSelectedOptionEquals("left_ankle_brachial_month","Jul");
        assertTextFieldEquals("left_ankle_brachial_year","1977");
        assertSelectedOptionEquals("left_ankle_brachial_day","31");
        
        assertSelectedOptionEquals("right_ankle_brachial_month","Jul");
        assertTextFieldEquals("right_ankle_brachial_year","1977");
        assertSelectedOptionEquals("right_ankle_brachial_day","31");
        
        assertSelectedOptionEquals("left_toe_brachial_month","Jul");
        assertTextFieldEquals("left_toe_brachial_year","1977");
        assertSelectedOptionEquals("left_toe_brachial_day","31");
        
        assertTextFieldEquals("left_dorsalis_pedis_ankle_brachial","1");
        assertTextFieldEquals("right_dorsalis_pedis_ankle_brachial","1");
        
        assertTextFieldEquals("left_tibial_pedis_ankle_brachial","1");
        assertTextFieldEquals("right_tibial_pedis_ankle_brachial","1");
        
        assertTextFieldEquals("left_ankle_brachial","1");
        assertTextFieldEquals("right_ankle_brachial","1");
        
        assertTextFieldEquals("left_abi_score_ankle_brachial","1");
        assertTextFieldEquals("right_abi_score_ankle_brachial","1");
        
        assertTextFieldEquals("left_toe_pressure","1");
        assertTextFieldEquals("right_toe_pressure","1");
        
        assertTextFieldEquals("left_brachial_pressure","1");
        assertTextFieldEquals("right_brachial_pressure","1");
        
        assertTextFieldEquals("left_tbi_score","1");
        assertTextFieldEquals("right_tbi_score","1");
        
        assertSelectedOptionEquals("left_edema_severity","+1");
        assertSelectedOptionEquals("right_edema_severity","+1");
        
        assertSelectedOptionEquals("left_edema_location","Foot");
        assertSelectedOptionEquals("right_edema_location","Foot");
        assertTextFieldEquals("left_sleep_positions","up");
        assertTextFieldEquals("right_sleep_positions","up");
        assertTextFieldEquals("left_ankle_cm","1");
        assertTextFieldEquals("right_ankle_cm","1");
        assertTextFieldEquals("left_ankle_mm","1");
        assertTextFieldEquals("right_ankle_mm","1");
        
        assertTextFieldEquals("left_midcalf_cm","2");
        assertTextFieldEquals("right_midcalf_cm","2");
        assertTextFieldEquals("left_midcalf_mm","2");
        assertTextFieldEquals("right_midcalf_mm","2");
        
        assertSelectedOptionEquals("left_sensory","Item 4");
        assertSelectedOptionEquals("right_sensory","Item 4");
        assertSelectedOptionEquals("left_proprioception","Item 4");
        assertSelectedOptionEquals("right_proprioception","Item 4");
        
        assertCheckboxSelected("left_foot_deformities","1044");
        assertCheckboxSelected("right_foot_deformities","1044");
        
        assertCheckboxSelected("left_foot_skin","1045");
        assertCheckboxSelected("right_foot_skin","1045");
        
        assertCheckboxSelected("left_foot_toes","1046");
        assertCheckboxSelected("right_foot_toes","1046");
        
        assertSelectedOptionEquals("weight_bearing","Item 4");
        assertSelectedOptionEquals("balance","Item 4");
        assertSelectedOptionEquals("calf_muscle_pump","Item 4");
        assertSelectedOptionEquals("mobility_aids","Item 4");
        assertTextFieldEquals("mobility_aids_comments","test");
        assertTextFieldEquals("calf_muscle_comments","test");
        assertTextFieldEquals("gait_pattern_comments","test");
        
        assertTextFieldEquals("walking_distance_comments","test");
    
        assertTextFieldEquals("walking_endurance_comments","test");
        
        assertRadioOptionSelected("indoor_footwear","1");
        assertTextFieldEquals("indoor_footwear_comments","testers");
        
        assertRadioOptionSelected("outdoor_footwear","1");
        assertTextFieldEquals("outdoor_footwear_comments","testers");
        
        assertRadioOptionSelected("orthotics","1");
        assertTextFieldEquals("orthotics_comments","testers");
        
        assertSelectedOptionEquals("muscle_tone_functional","Item 4");
        assertSelectedOptionEquals("arches_foot_structure","Item 4");
        assertRadioOptionSelected("supination_foot_structure","1");
        assertRadioOptionSelected("pronation_foot_structure","1");
        assertSelectedOptionEquals("dorsiflexion_active","Item 4");
        
        assertSelectedOptionEquals("plantarflexion_active","Item 4");
        assertSelectedOptionEquals("greattoe_active","Item 4");
        assertSelectedOptionEquals("dorsiflexion_passive","Item 4");
        
        assertSelectedOptionEquals("plantarflexion_passive","Item 4");
        assertSelectedOptionEquals("greattoe_passive","Item 4");
        setTextField("orthotics_comments","orthotics commenting");
        clickLink("navSummary");
        clickButton("saveEntries");
        gotoRootWindow();
    }
     @After
    public void close() {
        closeBrowser();
    }
}