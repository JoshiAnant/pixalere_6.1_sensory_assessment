package com.pixalere.jwebunit.offline;
import java.util.*;
import com.pixalere.utils.PDate;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
public class OfflineNewPatientTestCase  {
    //private final String CONTEXT="/LoginS";
    private String[] WOUND_ALPHAS={"A","B","C","D"};
    @Before
    public void prepare(){
       // String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        System.out.println("Testing");
        setBaseUrl("http://localhost:8080/pixalere");
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        beginAt("/LoginSetup.do");
        //System.out.println("Assessment SetUp: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));

        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        setWorkingForm("searchN");
        setTextField("search","501");
        submit();
        //System.out.println("Going to Wound: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
//

        //System.out.println("Going to Assessment: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));
    }
    @Test
 public void testPatientProfile() throws com.pixalere.common.ApplicationException{
        //System.out.println(getPageSource());
        selectOption("patient_res","Fraser Health");
        checkCheckbox("im","2817");
        selectOption("funding_source","ICBC");
        selectOption("gender","Male");

        setTextField("professionals","Dr Burns");
        setTextField("allergies","Bees");
        checkCheckbox("medication_comments","2776");
        selectOption("comorbidities","Hypotension");
        clickButton("addComo");

        /*clickLink("navSummary");
        gotoWindow("infowindow");

        assertTextPresent("Information Popup WARNING: Invalid date of birth");
        clickLink("closeWindow");
        gotoRootWindow();*/
        selectOption("dob_day","26");
        selectOption("dob_month","Jul");
        setTextField("dob_year","1977");
       /*clickLink("loadBraden");
       gotoWindow("braden_scale");

       clickRadioOption("braden_moisture","1034");
       clickRadioOption("braden_sensory","1032");
       clickRadioOption("braden_activity","1035");
       clickRadioOption("braden_mobility","1038");
       clickRadioOption("braden_friction","1037");
       clickRadioOption("braden_nutrition","1036");
       closeWindow();
       gotoRootWindow();*/
        assertTextFieldEquals("braden_score","");
        //selectOption("investigation_list","317");

        clickRadioOption("investigations_show","2");
        selectOption("investigation_list","Biopsy");
        selectOption("investigation_day","1");
        selectOption("investigation_month","Jul");
        setTextField("investigation_year","1977");
        clickButton("addAnti");

        setTextField("blood_sugar","1");
        selectOption("bloodsugar_day","1");
        selectOption("bloodsugar_month","Jul");
        setTextField("bloodsugar_year","1977");
        setTextField("blood_sugar_meal","1");
        selectOption("albumin_day","1");
        setTextField("albumin_year","1977");
        selectOption("albumin_month","Jul");
        selectOption("prealbumin_month","Jul");
        setTextField("prealbumin_year","1977");
        selectOption("prealbumin_day","31");
        setTextField("albumin","1.1");
        setTextField("pre_albumin","1.2");
        clickLink("loadBraden");
        gotoWindow("braden_scale");
        clickRadioOption("braden_sensory","1");
        clickRadioOption("braden_moisture","4");
        clickRadioOption("braden_activity","3");
        clickRadioOption("braden_nutrition","1");
        clickRadioOption("braden_mobility","1");
        clickRadioOption("braden_friction","1");
        assertRadioOptionSelected("braden_activity","3");
        clickButton("done");
        gotoRootWindow();
        //hack for testing purposes only
        //`1                                                setTextField("braden_score","10");
        assertTextFieldEquals("braden_score","11");
        setTextField("surgical_history","test surgical");
        clickLink("navSummary");
        clickButton("saveEntries");
        gotoRootWindow();
    }
    @Test
 public void testCreateWoundProfile(){
        clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","New Wound Profile");
        //submit();
        assertElementPresent("anterior");
        assertElementPresent("posterior");

        selectOption("anterior","Chest");
        clickRadioOption("rad_anterior","R");
        //assertElementPresent("wound_etiology1");
        //assertElementPresent("goals_1");


        //selectOption()
        //checkCheckbox("wound_etiology","798");
        setTextField("reason_for_care","Reason");

        clickLink("selectWound");
        clickLink("alpha167");
        clickLink("alpha40");
        clickLink("alpha101");
        clickLink("alpha106");
        String[] a ={"Wound A","Wound B","Wound C","Wound D"};
        selectOptions("goals_alphas",a);
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");
        /*selectOption("goals_alphas","Wound B");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");
        selectOption("goals_alphas","Wound C");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");
        selectOption("goals_alphas","Wound D");
        selectOption("goals_list","Maintain wound");
        clickButton("addgoals");*/
        String[] gt = {"Wound A: Maintain wound","Wound B: Maintain wound","Wound C: Maintain wound","Wound D: Maintain wound"};
        assertSelectOptionsEqual("goals",gt);

        selectOptions("etiology_alphas",a);
        selectOption("etiology_list","Venous");
        clickButton("addetiology");
        /*selectOption("etiology_alphas","Wound B");
        selectOption("etiology_list","Venous");
        clickButton("addetiology");
        selectOption("etiology_alphas","Wound C");
        selectOption("etiology_list","Venous");
        clickButton("addetiology");
        selectOption("etiology_alphas","Wound D");
        selectOption("etiology_list","Venous");
        clickButton("addetiology");*/
        String[] et = {"Wound A: Venous","Wound B: Venous","Wound C: Venous","Wound D: Venous"};
        assertSelectOptionsEqual("etiology",et);
        setTextField("operative_procedure_comments","comment");
        selectOption("date_surgery_day","26");
        selectOption("date_surgery_month","Jul");
        setTextField("date_surgery_year","1977");
        setTextField("surgeon","dr. burns");

        //clickLink("img167");
        clickLink("navSummary");
        //check list to verify items
        String[] pp ={
            "Wound Location: Chest",
            "Cause/History: Reason",
            "Etiology: Wound A: Venous Wound B: Venous Wound C: Venous Wound D: Venous",
            "Goal of Care: Wound A: Maintain wound Wound B: Maintain wound Wound C: Maintain wound Wound D: Maintain wound"
            };
        assertSelectOptionsEqual("woundprofile",pp);

        clickButton("saveEntries");
        gotoRootWindow();
    }
 @Test
 public void testAssessment_A() throws com.pixalere.common.ApplicationException{
             int count=0;
             //for(String alpha : WOUND_ALPHAS){
             count++;
             clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Chest");
        //submit();
        clickLink("navAssessment");
             setWorkingForm("void");
             assertElementPresent("cancel1");
             checkCheckbox("cancel");
             clickLink("alphaAOffline");
             //PIX-203

             setWorkingForm("populate");
             clickRadioOption("assessment_type","1");
             setWorkingForm("form");




             selectOption("status","Active");
             selectOption("wound_day","1");selectOption("wound_month","Jul");setTextField("wound_year","1977");
             clickRadioOption("recurrent","2");
             assertSelectedOptionEquals("wound_day","1");
             assertSelectedOptionEquals("wound_month","Jul");
             assertTextFieldEquals("wound_year","1977");
             selectOption("pain","1");
             selectOption("length","1");
             selectOption("length_milli","2");
             selectOption("width","1");selectOption("width_milli","2");
             selectOption("depth","1");selectOption("depth_milli","2");
             //PIX-
             //selectOption("num_under","1");
             /*clickButton("oku");
             setWorkingForm("form");
             selectOption("underminingdepth_1","1");selectOption("underminingdepth_mini_1","2");
             selectOption("undermining_start_1","1:00");selectOption("undermining_end_1","1:00");
             */
             //selectOption("num_sinus_tracts","1");
             //clickButton("oks");
            // System.out.println(getPageSource());
             setWorkingForm("form");

             //assertRadioOptionNotSelected("exudate_odour","0");
             //selectOption("sinusdepth_1","1");selectOption("sinusdepth_mini_1","1");
             //selectOption("sinustract_start_1","1:00");
             /*selectOption("num_fistulas","1");
             clickButton("okf");
             setTextField("fistulas_1","fistulas");*/
             checkCheckbox("woundbase","Hematoma");
             selectOption("woundbase_percentage_4","100%");
             checkCheckbox("exudate_type","2222");
             clickRadioOption("exudate_amount","2225");clickRadioOption("exudate_odour","1");
             checkCheckbox("wound_edge","2436");checkCheckbox("periwound","2746");
             if(count <= 2){
                 selectOption("status","Active");
             }else{
                 selectOption("status","Closed");
                 selectOption("discharge_reason","Wound Closed");
                 selectOption("closed_day","1");
                 selectOption("closed_month","Jul");
                 setTextField("closed_year","1977");
             }
             //}
             assertCheckboxSelected("woundbase","Hematoma");
             assertSelectedOptionEquals("woundbase_percentage_4","100%");

             clickLink("navTreatment");
             gotoWindow("infowindow");

             assertTextPresent("Information Popup ** Confirm ** There are unfinished assessments (B,C,D). Click Stay to remain on the assessment screen, or Go to proceed.");
             clickButton("cancel");
             gotoRootWindow();
             
             setWorkingForm("form");
             assertTextNotPresent("Populate Previous Products");
             selectOption("products","Apron");
             selectOption("products","Blue Pad");
             setTextField("wound_treatment_comments","comments\ncomments");
             selectOption("wound_ass","Wound A");
             clickButton("addAlpha");
             selectOption("current_products","1:Wound A:Blue Pad");
             clickButton("removeAlpha");
             selectOption("current_products","1:Wound A:Apron");

             setTextField("quantity","3");
             clickButton("btn_update");
             String[] dcfalphas={"Wound A"};
             selectOptions("dressing_change_frequency_alphas",dcfalphas);
             selectOption("dressing_change_frequency_list","TID");
             clickButton("adddressing_change_frequency");
             clickLink("navSummary");
             clickButton("saveEntries");
             
             //
             gotoWindow("infowindow");
             assertTextPresent("Information Popup ** Attention ** Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
             clickLink("closeWindow");
             gotoRootWindow();
             clickRadioOption("referral_wound","n");
             //selectOption("priority_wound","Urgent (24 - 48hrs)");
             checkCheckbox("send_message_wound","1");
             setTextField("body_wound","Wound Care Comment");
             //test popup PIX-1087
             //selectOption("patientprofile","Gender: Male");
             //gotoWindow("hoverwindow");
             //assertTextPresent("Gender: Male");
             //clickLink("closeWindow");
             //gotoRootWindow();
             clickButton("saveEntries");


    }
    @Test
 public void testAssessment_B() throws com.pixalere.common.ApplicationException{
             int count=0;
             //for(String alpha : WOUND_ALPHAS){
             count++;
             clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Chest");
        //submit();
        clickLink("navAssessment");
             setWorkingForm("void");
             assertElementPresent("cancel1");
             checkCheckbox("cancel");
             clickLink("alphaBOffline");
             //PIX-203
             /*setWorkingForm("void");
             checkCheckbox("cancel");
             clickLink("navTreatment");
             gotoWindow("infowindow");

             assertTextPresent("There are unfinished assessments (A,B,C,D). Click Stay to remain on the assessment screen, or Go to proceed");
             clickButton("ok");
             gotoRootWindow();
             setWorkingForm("void");
             uncheckCheckbox("cancel");//uncheck it*/
             //System.out.println(getPageSource());

             setWorkingForm("populate");
             clickRadioOption("assessment_type","1");
             setWorkingForm("form");
             assertElementPresent("wound_day");
             assertElementPresent("wound_month");
             assertElementPresent("wound_year");
             assertElementPresent("recurrent1");
             assertElementPresent("pain");
             assertElementPresent("length");
             assertElementPresent("width");
             assertElementPresent("depth");
             assertElementPresent("length_milli");
             assertElementPresent("width_milli");
             assertElementPresent("depth_milli");
             //assertElementPresent("woundbase1");
             assertElementPresent("exudate_type1");
             //assertElementPresent("exudate_amount1");
             assertElementPresent("exudate_odour1");
             assertElementPresent("wound_edge1");
             assertElementPresent("periwound1");
             assertElementPresent("status");
             assertElementPresent("discharge_reason");
             assertElementPresent("closed_day");
             assertElementPresent("closed_month");
             assertElementPresent("closed_year");



             selectOption("status","Active");
             selectOption("wound_day","1");selectOption("wound_month","Jul");setTextField("wound_year","1977");
             clickRadioOption("recurrent","2");
             assertSelectedOptionEquals("wound_day","1");
             assertSelectedOptionEquals("wound_month","Jul");
             assertTextFieldEquals("wound_year","1977");
             selectOption("pain","1");
             selectOption("length","1");
             selectOption("length_milli","2");
             selectOption("width","1");selectOption("width_milli","2");
             selectOption("depth","1");selectOption("depth_milli","2");
             //PIX-
             //selectOption("num_under","1");
             /*clickButton("oku");
             setWorkingForm("form");
             selectOption("underminingdepth_1","1");selectOption("underminingdepth_mini_1","2");
             selectOption("undermining_start_1","1:00");selectOption("undermining_end_1","1:00");
             */
             //selectOption("num_sinus_tracts","1");
             //clickButton("oks");
            // System.out.println(getPageSource());
             setWorkingForm("form");

             //assertRadioOptionNotSelected("exudate_odour","0");
             //selectOption("sinusdepth_1","1");selectOption("sinusdepth_mini_1","1");
             //selectOption("sinustract_start_1","1:00");
             /*selectOption("num_fistulas","1");
             clickButton("okf");
             setTextField("fistulas_1","fistulas");*/
             checkCheckbox("woundbase","Hematoma");
             selectOption("woundbase_percentage_4","100%");
             checkCheckbox("exudate_type","2222");
             clickRadioOption("exudate_amount","2225");clickRadioOption("exudate_odour","1");
             checkCheckbox("wound_edge","2436");checkCheckbox("periwound","2746");
             if(count <= 2){
                 selectOption("status","Active");
             }else{
                 selectOption("status","Closed");
                 selectOption("discharge_reason","Wound Closed");
                 selectOption("closed_day","1");
                 selectOption("closed_month","Jul");
                 setTextField("closed_year","1977");
             }
             //}
             assertCheckboxSelected("woundbase","Hematoma");
             assertSelectedOptionEquals("woundbase_percentage_4","100%");

             clickLink("navTreatment");
             gotoWindow("infowindow");

             assertTextPresent("Information Popup ** Confirm ** There are unfinished assessments (A,C,D). Click Stay to remain on the assessment screen, or Go to proceed.");
             clickButton("cancel");
             gotoRootWindow();
  
             setWorkingForm("form");
             assertTextNotPresent("Populate Previous Products");
             selectOption("products","Apron");
             selectOption("products","Blue Pad");
             setTextField("wound_treatment_comments","comments\ncomments");
             selectOption("wound_ass","Wound B");
             clickButton("addAlpha");
             selectOption("current_products","1:Wound B:Blue Pad");
             clickButton("removeAlpha");
             selectOption("current_products","1:Wound B:Apron");

             setTextField("quantity","3");
             clickButton("btn_update");
             String[] dcfalphas={"Wound B"};
             selectOptions("dressing_change_frequency_alphas",dcfalphas);
             selectOption("dressing_change_frequency_list","TID");
             clickButton("adddressing_change_frequency");
             clickLink("navSummary");
             clickButton("saveEntries");
 

             //
             gotoWindow("infowindow");
             assertTextPresent("Information Popup ** Attention ** Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
             clickLink("closeWindow");
             gotoRootWindow();
             clickRadioOption("referral_wound","n");
             //selectOption("priority_wound","Urgent (24 - 48hrs)");
             checkCheckbox("send_message_wound","1");
             setTextField("body_wound","Wound Care Comment");
             //test popup PIX-1087
             //selectOption("patientprofile","Gender: Male");
             //gotoWindow("hoverwindow");
             //assertTextPresent("Gender: Male");
             //clickLink("closeWindow");
             //gotoRootWindow();
             clickButton("saveEntries");


    }
    @Test
 public void testAssessment_A2() throws com.pixalere.common.ApplicationException{
             int count=0;
             //for(String alpha : WOUND_ALPHAS){
             count++;
             clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Chest");
        //submit();
        clickLink("navAssessment");
             setWorkingForm("void");
             assertElementPresent("cancel1");
             checkCheckbox("cancel");
             clickLink("alphaAOffline");
             //PIX-203
             /*setWorkingForm("void");
             checkCheckbox("cancel");
             clickLink("navTreatment");
             gotoWindow("infowindow");

             assertTextPresent("There are unfinished assessments (A,B,C,D). Click Stay to remain on the assessment screen, or Go to proceed");
             clickButton("ok");
             gotoRootWindow();
             setWorkingForm("void");
             uncheckCheckbox("cancel");//uncheck it*/
             //System.out.println(getPageSource());

             setWorkingForm("populate");
             clickRadioOption("assessment_type","0");
             setWorkingForm("form");
             assertElementPresent("wound_day");
             assertElementPresent("wound_month");
             assertElementPresent("wound_year");
             assertElementPresent("recurrent1");
             assertElementPresent("pain");
             assertElementPresent("length");
             assertElementPresent("width");
             assertElementPresent("depth");
             assertElementPresent("length_milli");
             assertElementPresent("width_milli");
             assertElementPresent("depth_milli");
             //assertElementPresent("woundbase1");
             assertElementPresent("exudate_type1");
             //assertElementPresent("exudate_amount1");
             assertElementPresent("exudate_odour1");
             assertElementPresent("wound_edge1");
             assertElementPresent("periwound1");
             assertElementPresent("status");
             assertElementPresent("discharge_reason");
             assertElementPresent("closed_day");
             assertElementPresent("closed_month");
             assertElementPresent("closed_year");



             selectOption("status","Active");
             selectOption("wound_day","1");selectOption("wound_month","Jul");setTextField("wound_year","1977");
             clickRadioOption("recurrent","2");
             assertSelectedOptionEquals("wound_day","1");
             assertSelectedOptionEquals("wound_month","Jul");
             assertTextFieldEquals("wound_year","1977");
             selectOption("pain","1");

             //PIX-
             //selectOption("num_under","1");
             /*clickButton("oku");
             setWorkingForm("form");
             selectOption("underminingdepth_1","1");selectOption("underminingdepth_mini_1","2");
             selectOption("undermining_start_1","1:00");selectOption("undermining_end_1","1:00");
             */
             //selectOption("num_sinus_tracts","1");
             //clickButton("oks");
            // System.out.println(getPageSource());
             setWorkingForm("form");

             //assertRadioOptionNotSelected("exudate_odour","0");
             //selectOption("sinusdepth_1","1");selectOption("sinusdepth_mini_1","1");
             //selectOption("sinustract_start_1","1:00");
             /*selectOption("num_fistulas","1");
             clickButton("okf");
             setTextField("fistulas_1","fistulas");*/
             checkCheckbox("woundbase","Hematoma");
             selectOption("woundbase_percentage_4","100%");
             checkCheckbox("exudate_type","2222");
             clickRadioOption("exudate_amount","2225");clickRadioOption("exudate_odour","1");
             checkCheckbox("wound_edge","2436");checkCheckbox("periwound","2746");
             if(count <= 2){
                 selectOption("status","Active");
             }else{
                 selectOption("status","Closed");
                 selectOption("discharge_reason","Wound Closed");
                 selectOption("closed_day","1");
                 selectOption("closed_month","Jul");
                 setTextField("closed_year","1977");
             }
             //}
             assertCheckboxSelected("woundbase","Hematoma");
             assertSelectedOptionEquals("woundbase_percentage_4","100%");

             clickLink("navTreatment");
             gotoWindow("infowindow");

             assertTextPresent("Information Popup ** Confirm ** There are unfinished assessments (B,C,D). Click Stay to remain on the assessment screen, or Go to proceed.");
             clickButton("cancel");
             gotoRootWindow();
  
             setWorkingForm("form");
             assertTextNotPresent("Populate Previous Products");
             selectOption("products","Apron");
             selectOption("products","Blue Pad");
             setTextField("wound_treatment_comments","comments\ncomments");
             selectOption("wound_ass","Wound A");
             clickButton("addAlpha");
             selectOption("current_products","1:Wound A:Blue Pad");
             clickButton("removeAlpha");
             selectOption("current_products","1:Wound A:Apron");

             setTextField("quantity","3");
             clickButton("btn_update");
             String[] dcfalphas={"Wound A"};
             selectOptions("dressing_change_frequency_alphas",dcfalphas);
             selectOption("dressing_change_frequency_list","TID");
             clickButton("adddressing_change_frequency");
             clickLink("navSummary");
             clickButton("saveEntries");


             //
             gotoWindow("infowindow");
             assertTextPresent("Information Popup ** Attention ** Wound Care Clinician referral required is a mandatory field. You must select Yes or No for each care type (Wound, Incision, Tube/Drain, Ostomy)");
             clickLink("closeWindow");
             gotoRootWindow();
             clickRadioOption("referral_wound","n");
             //selectOption("priority_wound","Urgent (24 - 48hrs)");
             checkCheckbox("send_message_wound","1");
             setTextField("body_wound","Wound Care Comment");
             //test popup PIX-1087
             //selectOption("patientprofile","Gender: Male");
             //gotoWindow("hoverwindow");
             //assertTextPresent("Gender: Male");
             //clickLink("closeWindow");
             //gotoRootWindow();
             clickButton("saveEntries");


    }
     @After
    public void close() {
        closeBrowser();
    }
}