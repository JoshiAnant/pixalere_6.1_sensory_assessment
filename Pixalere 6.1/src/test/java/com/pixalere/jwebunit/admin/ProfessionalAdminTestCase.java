/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.admin;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

public class ProfessionalAdminTestCase {
    
    
    @Before
    public void prepare(){
        //String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        setBaseUrl("http://localhost:8080/pixalere");
        
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        clickLink("navAdmin");
        
        setWorkingForm("adminMenuFrm");
        selectOption("pixActionMenu","Professional Accounts");
        submit();
    }
    
    
    @Test
    public void testcreateProfessional(){
        setWorkingForm("professional_form");
        selectOption("position","RN");
        setTextField("first_name","Dr.");
        setTextField("last_name","Burns");
        setTextField("password","skywalker77");
        setTextField("password2","skywalker77");
        String[] treats = {"Training Location"};
        selectOptions("region",treats);
        clickButton("addTreatprofessional_form");
        checkCheckbox("access_viewer","1");
        checkCheckbox("access_allpatients","1");
        clickButton("addsubmit");
        assertTextPresent("Dr. Burns");
        assertTextPresent("9");
        clickLink("navLogout");
        setTextField("userId","9");
        setTextField("password","skywalker77");
        submit();
        
        assertTextPresent("New Password");
        setTextField("newpassword","donthugth@3");
        setTextField("confirmpassword","skywalker99");
        submit();
        //confirm error
        assertTextPresent("");
        setTextField("confirmpassword","donthugth@3");
        System.out.println(getPageSource());
        submit();
        
    }
    @After
    public void close() {
        closeBrowser();
    }
}
