/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.admin;

import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;

public class ResourcesAdminTestCase {
    //private final String CONTEXT="/LoginS";
  
    @Before
    public void prepare(){
        //String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        setBaseUrl("http://localhost:8080/pixalere");
        
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","fat.burning2@");
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        
        clickLink("navAdmin");
        setWorkingForm("adminMenuFrm");
        selectOption("pixActionMenu","Manage Resources");
        submit();
    }
    @Test
    public void testAddResource(){
        setWorkingForm("resources");
        selectOption("resourceMenu","Treatment Location");
        submit();
        
        setWorkingForm("content_form");
        setTextField("name","Training Location2");
        clickRadioOption("item_filter","0");
        clickButton("editsubmit");
        assertTextPresent("Training Location2");
    }
    @Test
    public void testAddInformationPopup(){
        setWorkingForm("adminMenuFrm");
        selectOption("pixActionMenu","Information Popups");
        submit();

        setWorkingForm("resources");
        selectOption("component_id","Patient Profile: Allergies");
        submit();

        setWorkingForm("content_form");
        setTextField("title","Test Allergies");
        setTextField("description","tadsfasdfadsf");
        clickButton("save");
//
      //  assertTextPresent("Test Allergies");
//
        

    }
    @Test
    public void testDeleteInformationPopup(){
        
    }
    /*public void testSuccessfulLogin() {
        beginAt("/LoginSetup.do");
        assertTitleEquals("Pixalere Login");
        assertElementPresent("userId");
        assertElementPresent("password");
        assertElementPresent("database");
        setTextField("userId","7");
        setTextField("password","donthugth@3");
        submit();
        assertTitleEqualsKey("pixalere.main.form.title");
        clickLink("navLogout");
    }
    public void testFailedLogins() {
        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password","skywalker89");
        submit();
        gotoWindow("infowindow");
        assertTextPresent("Username and/or Password is incorrect");
        assertTextPresent("WARNING:Validation Failed");
        clickLinkWithImage("closeWindow");
        gotoRootWindow();
        //assertTitleEqualsKey("pixalere.main.form.title");
        //clickLink("navLogout");
    }
    public void testFailedEmptyLogins() {
        beginAt("/LoginSetup.do");
        setTextField("userId","");
        setTextField("password","");
        submit();
        gotoWindow("infowindow");
        assertTextPresent("User Number or Password may be empty");
        assertTextPresent("WARNING:Validation Failed");
        clickLinkWithImage("closeWindow");
        gotoRootWindow();
        //assertTitleEqualsKey("pixalere.main.form.title");
        //clickLink("navLogout");
    }*/
     @After
    public void close() {
        closeBrowser();
    }
}
