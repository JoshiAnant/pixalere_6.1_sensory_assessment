/*
 * LoginTestCase.java
 *
 * Created on August 3, 2007, 8:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.admin;
import com.pixalere.ConstantsTest;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import net.sourceforge.jwebunit.html.Cell;
//import net.sourceforge.jwebunit.html.ExpectedTable;
import net.sourceforge.jwebunit.html.Table;
public class PatientAdminTestCase  {
    
    
    @Before
    public void prepare(){
        //String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        setBaseUrl("http://localhost:8080/pixalere");
        setTimeout(15000);
        
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        //getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
        //setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_SELENIUM);

        beginAt("/LoginSetup.do");
        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();

clickElementByXPath("//div[6]/div[1]/a/span");
        clickLink("navAdmin");
        
    }
    @Test
    public void testCase1dot1_1dot3_search_patient() {
        //System.out.println(getPageSource());
        //search for invalid patient to trigger create page
        setWorkingForm("search_form");
        setTextField("lastNameSearch","testeroni");
        submit();
        clickLink("createTab");
        assertFormPresent("create_form");
        setWorkingForm("create_form");
        
        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("id","345345");
        submit();
        clickLink("createTab");
        assertFormPresent("create_form");
        setWorkingForm("create_form");

        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("phn1_1","3433");setTextField("phn1_2","343");setTextField("phn1_3","343");
        submit();
        clickLink("createTab");
        assertFormPresent("create_form");
        setWorkingForm("create_form");

        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("otherSearch","3433");
        submit();
        clickLink("createTab");
        assertFormPresent("create_form");
        setWorkingForm("create_form");

        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("parisSearch","3433");
        submit();
        clickLink("createTab");
        assertFormPresent("create_form");
        setWorkingForm("create_form");

        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("id","1");
        submit();
        assertTextPresent("Mallory Morris");

        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("lastNameSearch","Morris");
        submit();
        assertTextPresent("Mallory Morris");

        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("phn1_1","3333");setTextField("phn1_2","333");setTextField("phn1_3","333");
        submit();
        assertTextPresent("Mallory Morris");


        


    }
    @Test
    public void testCase1dot2_create_new_patient(){
        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("otherSearch","3433");
        submit();
        clickLink("createTab");
        assertFormPresent("create_form");
        setWorkingForm("create_form");

        setTextField("phn1_1","343");
        setTextField("phn1_2","765");
        setTextField("phn1_3","");
        
        selectOption("treatmentRegion",ConstantsTest.TREATMENT_CATEGORY);
        clickButton("addSubmit");
        setTextField("allergies","Bees");
        //setTextField("phn3","055");
        clickButton("addSubmit");
        assertTextPresent("PHN is invalid");
        
        assertFormPresent("create_form");
        setWorkingForm("create_form");
        selectOption("gender","Male");
        selectOption("dob_day","26");
        selectOption("dob_month","Jul");
        setTextField("dob_year","1977");
        setTextField("phn1_3","054");
        setTextField("phn1_2","765");
        setTextField("phn1_1","9499");
        setTextField("firstName","Professor");
        setTextField("lastName","Snape");
        setTextField("allergies","Bees");
        selectOption("treatmentLocation",ConstantsTest.TREATMENT_LOCATION);
        
        clickButton("addSubmit");
        //System.out.println(getPageSource());
        assertTextPresent("Professor Snape");
        
        //PIX-566 = Duplicate PHN test
        setWorkingForm("search_form");
        setTextField("lastNameSearch","testeroni");
        clickButton("search_admin");
        setWorkingForm("create_form");
        selectOption("gender","Male");
        selectOption("dob_day","26");
        selectOption("dob_month","Jul");
        setTextField("dob_year","1977");
        setTextField("phn1_1","343");
        setTextField("phn1_2","765");
        setTextField("phn1_3","054");
        setTextField("allergies","Bees");
        selectOption("treatmentRegion",ConstantsTest.TREATMENT_CATEGORY);
        setTextField("phn1_1","9499");
        setTextField("firstName","Professor");
        setTextField("lastName","Snape");
        selectOption("treatmentLocation",ConstantsTest.TREATMENT_LOCATION);
        clickButton("addSubmit");
        
        assertTextPresent("A Patient Account with this PHN already exists as Pix #ID "+ConstantsTest.PATIENT_ID_WEB+" at the "+ConstantsTest.TREATMENT_LOCATION+" treatment location");
        
        
    }
    @Test
    public void testCase1dot2_FindPatient(){
        setWorkingForm("searchN");
        setTextField("search",ConstantsTest.PATIENT_ID_WEB+"");
        submit();

        assertTextPresent("Physical Exam");//on pp
    }
    @Test
    public void testCase1dot4_viewPatient(){
        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("id","2");
        submit();
        assertTextPresent("Professor Snape");
        clickButton("patient_view_2");
        assertTextPresent("Patient Flowchart");
        

    }
    @Test
    public void testCase1dot5_AdminPatient(){
        System.out.println("Cast1dot5");
        setWorkingForm("adminMenuFrm");
        selectOptionByValue("pixActionMenu","patient_accounts");
        submit();
        setWorkingForm("search_form");
        setTextField("id","2");
        submit();
        assertTextPresent("Professor Snape");
        clickButton("patient_admin_2");
        assertTextPresent("Professor Snape");
        assertTextNotPresent("Admin Fn");
    }
    @Test
    public void testEditPatient() {
                System.out.println("edit");
        setWorkingForm("search_form");
        setTextField("lastNameSearch","Snape");
        submit();
        assertTablePresent("tablePatientList");
        patientTable = new Table(new Object[][] {
            {"","","Pix ID#", "","Patient Name","","Date of Birth","","Treatment Location","","Status"},
            {"Log Audit View Admin Fn","",ConstantsTest.PATIENT_ID_WEB, "","Professor Snape","","26/Jul/1977","",ConstantsTest.TREATMENT_LOCATION,"","Admitted"},
            {new Cell("", 5, 1)}
        });
        assertTableEquals("tablePatientList",patientTable);
        clickButton("patient_admin_"+ConstantsTest.PATIENT_ID_WEB);
        clickLink("editTab");
        setWorkingForm("edit_form");
        
        assertTextFieldEquals("editphn1_2","765");
        assertTextFieldEquals("editphn1_3","054");
        assertTextFieldEquals("editphn1_1","9499");
        assertTextFieldEquals("firstName","Professor");
        assertTextFieldEquals("lastName","Snape");
        setTextField("lastName","Snap'e");
       // assertSelectedOptionEquals("treatmentLocation","Richmond Hospital");
        //setTextField("phn1","5555");
        clickButton("edit_submit");
        patientTable = new Table(new Object[][] {
            {"","","Pix ID#", "","Patient Name","","Date of Birth","","Treatment Location","","Status"},
            {"","",ConstantsTest.PATIENT_ID_WEB, "","Professor Snap'e","","26/Jul/1977","",ConstantsTest.TREATMENT_LOCATION,"","Admitted"},
            {new Cell("", 5, 1)}
        });
        assertTextPresent("Professor Snap'e");
        //System.out.println(getPageSource());
        //assertTableEquals("tablePatientList",patientTable);
    }
    @Test
    public void testDeletePatient() {
               System.out.println("delete");
        setWorkingForm("search_form");
        setTextField("lastNameSearch","testeroni");
        submit();
        assertFormPresent("create_form");
        setWorkingForm("create_form");
        setTextField("phn1_1","343");
        setTextField("phn1_2","765");
        setTextField("phn1_3","");
System.out.println("1111111");
        selectOption("treatmentRegion",ConstantsTest.TREATMENT_CATEGORY);
        clickButton("addSubmit");
        setTextField("allergies","Bees");
        setTextField("phn3","055");
        clickButton("addSubmit");
        //assertTextPresent("PHN is invalid");
System.out.println("111111122222222");
        assertFormPresent("create_form");
        setWorkingForm("create_form");
        selectOption("gender","Male");
        selectOption("dob_day","26");
        selectOption("dob_month","Jul");
        setTextField("dob_year","1977");
        setTextField("phn1_3","062");
        setTextField("phn1_2","924");
        setTextField("phn1_1","9754");
        setTextField("firstName","Harry");
        setTextField("lastName","Potters");
        setTextField("allergies","Bees");
        selectOption("treatmentLocation",ConstantsTest.TREATMENT_LOCATION);

        System.out.println("1111111333333");
        clickButton("addSubmit");
        //System.out.println(getPageSource());
        assertTextPresent("Harry Potters");

        setWorkingForm("search_form");
        setTextField("lastNameSearch","Potters");
        submit();
        assertTablePresent("tablePatientList");
        patientTable = new Table(new Object[][] {
            {"","","Pix ID#", "","Patient Name","","Date of Birth","","Treatment Location","","Status"},
            {"Log Audit View Admin Fn","",ConstantsTest.PATIENT_ID_DELETE_WEB, "","Harry Potters","","26/Jul/1977","",ConstantsTest.TREATMENT_LOCATION,"","Admitted"},
            {new Cell("", 5, 1)}
        });
        assertTableEquals("tablePatientList",patientTable);
        System.out.println("111111144444444");
        clickButton("patient_admin_"+ConstantsTest.PATIENT_ID_DELETE_WEB);
        clickLink("inactivateTab");
        assertTextPresent("You have selected to discharge this patient");
        System.out.println("1111111555555");
    }
    @Test
    public void testTransferPatient(){
                System.out.println("trasn");
        setWorkingForm("search_form");
        setTextField("lastNameSearch","Snap'e");
        submit();
        assertTablePresent("tablePatientList");
        patientTable = new Table(new Object[][] {
            {"","","Pix ID#", "","Patient Name","","Date of Birth","","Treatment Location","","Status"},
            {"Log Audit View Admin Fn","",ConstantsTest.PATIENT_ID_WEB, "","Professor Snap'e","","26/Jul/1977","",ConstantsTest.TREATMENT_LOCATION,"","Admitted"},
            {new Cell("", 5, 1)}
        });
        assertTableEquals("tablePatientList",patientTable);
        clickButton("patient_admin_"+ConstantsTest.PATIENT_ID_WEB);
        clickLink("transferTab");
        setWorkingForm("transfer_form");
        //assertSelectedOptionEquals("treatmentLocation",ConstantsTest.TREATMENT_LOCATION);
        //assertSelectedOptionEquals("treatmentRegion",ConstantsTest.TREATMENT_CATEGORY);
        selectOption("treatmentLocation",ConstantsTest.TREATMENT_LOCATION_TO);
        clickButton("transfer_submit");
        assertTextPresent("Training Location");
        
        //clickButton("editSubmit");
        //assertTextPresent(ConstantsTest.PATIENT_ID_WEB+" Professor Snap'e "+ConstantsTest.TREATMENT_LOCATION);
        
    }
    @Test
    public void testCreateOfflinePatient(){
                System.out.println("reateoff");
        setWorkingForm("search_form");
        setTextField("lastNameSearch","kjhijh");
        submit();

        setWorkingForm("create_form");
        setTextField("phn1_2","852");
        selectOption("treatmentRegion",ConstantsTest.TREATMENT_CATEGORY);
        setTextField("phn1_3","852");
        setTextField("phn1_1","9499");
        setTextField("firstName","Professor");
        setTextField("lastName","Offline");
        selectOption("treatmentLocation",ConstantsTest.TREATMENT_LOCATION);
        selectOption("gender","Male");
        selectOption("dob_day","26");
        setTextField("allergies","bees");
        selectOption("dob_month","Jul");
        setTextField("dob_year","1977");
        clickButton("addSubmit");
        patientTable = new Table(new Object[][] {
            {"","","Pix ID#", "","Patient Name","","Date of Birth","","Treatment Location","","Status"},
            {"","","501", "","Professor Offline","","26/Jul/1977","",ConstantsTest.TREATMENT_LOCATION,"","Admitted"},
            {new Cell("", 5, 1)}
        });
        //System.out.println(getPageSource());
        assertTextPresent("Professor Offline");
        //assertTableEquals("tablePatientList",patientTable);
    }
    /*public void testConfiguration(){
        setWorkingForm("adminMenuFrm");
        selectOption("pixActionMenu","Manage Configuration");
        submit();
        setWorkingForm("content_form");
        setTextField("config219","/usr/share/photos");
        submit();
    }*/
    @After
    public void close() {
        closeBrowser();
    }
    Table patientTable = new Table(new Object[][] {
            {"","","Pix ID#", "","Patient Name","","Date of Birth","","Treatment Location","","Status"},
            {"","",ConstantsTest.PATIENT_ID_WEB, "","Professor Snap'e","","26/Jul/1977","",ConstantsTest.TREATMENT_LOCATION,"","Admitted"},
            {new Cell("", 5, 1)}
        });
}
