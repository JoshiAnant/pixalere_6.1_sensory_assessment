/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pixalere.jwebunit.amendment;

import java.util.*;
import com.pixalere.utils.PDate;
import com.pixalere.ConstantsTest;
import net.sourceforge.jwebunit.util.TestContext;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.junit.*;
import static net.sourceforge.jwebunit.junit.JWebUnit.*;

public class DataAmendmentTestCase {

    @Before
    public void prepare(){
        //String app = loadProperties().getProperty("jwebunitURL");
       //System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "trace");
        setBaseUrl("http://localhost:8080/pixalere");
        
        setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
        getTestContext().setResourceBundleName("ApplicationResources");
        getTestContext().setUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; FDM; .NET CLR 2.0.50727; .NET CLR 1.1.4322)");
        beginAt("/LoginSetup.do");

        setTextField("userId","7");
        setTextField("password",ConstantsTest.PASSWORD);
        submit();
        clickElementByXPath("//div[6]/div[1]/a/span");
        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        //System.out.println("Going to Wound: "+new PDate().getDateStringWithHour(new PDate().getEpochTime()+""));


        
        //submit();
        
    }
    @Test
    public void testDeleteWoundAssessment(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Chest - Wound");
        clickLink("assessmentLink");

        clickButton("delbtn_1963");
        setWorkingForm("delete_form");
        setTextField("moved_reason","Wrong Patient");
        clickElementByXPath("//div[6]/div[3]/div/button");

    }
    @Test
    public void testDeleteOstomyAssessment(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Abdomen - Ostomy");
        clickLink("assessmentLink");
        clickElementByXPath("//button[@id='alphaostu1']");
        //System.out.println(getPageSource());
        clickButton("alphaostu1");//System.out.println(getPageSource());
        System.out.println(getPageSource());
        clickButton("delbtn_253");
        setWorkingForm("delete_form");
        setTextField("moved_reason","Wrong Patient");
        clickElementByXPath("//div[6]/div[3]/div/button");
    }
    @Test
    public void testDeleteDrainAssessment(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Head/neck (Anterior) - Tube/Drain");
        clickLink("assessmentLink");
        clickButton("delbtn_365");
        setWorkingForm("delete_form");
        setTextField("moved_reason","Wrong Patient");
        clickElementByXPath("//div[6]/div[3]/div/button");//div[6]/div[3]/div/button
    }
    public void testDeleteIncisionAssessment(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Head/neck (Anterior) - Incision");
        clickLink("assessmentLink");
        clickButton("delbtn_558");
        setWorkingForm("delete_form");
        setTextField("moved_reason","Wrong Patient");
        clickElementByXPath("//div[6]/div[3]/div/button");
    }
    @Test
    public void testPopulatePrevious(){
        clickLink("navWound");
        setWorkingForm("woundform");
        selectOption("wound_id","Chest");
        //submit();
        clickLink("navAssessment");
        setWorkingForm("void");
        assertElementPresent("cancel1");
        checkCheckbox("cancel");
        clickButton("bgbtnA");
         setWorkingForm("populate");
        
        clickRadioOption("assessment_type","1");
        clickButton("populate1");
        assertSelectedOptionEquals("pain","2");//if 3 shows up, its populating the deleted assessment.

        setWorkingForm("searchN");
        setTextField("search","3");
        submit();
        //dlete unsaved
        clickElementByXPath("//div[6]/div[3]/div/button[2]");
    }
    @Test
    public void testDeleteWoundProfile(){
        setWorkingForm("woundform");
        selectOption("wound_profile_type_id","Abdomen - Ostomy");
        clickLink("woundLink");
        clickButton("delbtn_1609");
        setWorkingForm("delete_form");
        setTextField("deleted_reason","Wrong Patient");
        clickElementByXPath("//div[4]/div[3]/div/button[1]");
        clickLink("navWound");
        assertSelectOptionNotPresent("wound_id","Abdomen");
    }
    @Test
    public void testWoundProfileRename(){
        clickLink("navWound");
        selectOption("wound_id","Chest");
        clickLink("unlock");
        selectOption("anterior","Pelvis");
        clickLink("navWound");
        assertSelectOptionNotPresent("wound_id","Chest");
    }
     @After
    public void close() {
        closeBrowser();
    }

}
