package com.pixalere;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * @author travismorris
 */
public class ConstantsTest {

    public ConstantsTest() {
    }
    public static final String PASSWORD = "fat.burning2@";//"fat.burning2@";//"donthugth@3";
    public static final String TREATMENT_CATEGORY = "Sample Area";//"Thompson Cariboo Shuswap";
    public static final String TREATMENT_LOCATION = "Training Location";//"Overlander";
    public static final String TREATMENT_LOCATION_TO = "Training Location2";//"Overlander";
    public static final String USERNAME = "User Number";//"User Name";
    public static final int PATIENT_ID_WEB = 2;//7165;
    public static final int PATIENT_ID_DELETE_WEB = 3;//7166;
    public static final int PATIENT_ID = 1;
    public static final int WOUND_ID = 1;
    public static final int WOUND_PROFILE_TYPE_ID = 1;
    public static final int PROFESSIONAL_ID = 7;
    public static final int ALPHA_ID_WOUND = 3;
    public static final int ALPHA_ID_INCISION = 7;
    public static final int ALPHA_ID_DRAIN = 4;
    public static final int ALPHA_ID_OSTOMY = 10;

    public static final int WOUND_TYPE_WOUND = 5;
    public static final int WOUND_TYPE_DRAIN = 1;
    public static final int WOUND_TYPE_INCISION = 6;
    public static final int WOUND_TYPE_OSTOMY = 13;
    public static final int ASSESSMENT_ID = 1;

    public static final int MOCK_ID = 1;
    public static final String SERIALIZEDARRAY_ID = "a:1:{\"1\"}";
    public static final String SERIALIZEDARRAY_VALUE = "a:1:{\"test\"}";

    public static final int MSSQL_PATIENT_ID = 2078;
    public static final int MSSQL_WOUND_ID = 2612;
    public static final int MSSQL_PROF_ID = 576;
    public static final int MSSQL_WOUND_TYPE = 2612;
    public static final int MSSQL_OSTOMY_TYPE = 2614;
    public static final int MSSQL_TAG_TYPE = 2613;
    public static final int MSSQL_DRAIN_TYPE = 2613;
    public static final int MSSQL_ALPHA_ID_INCISION = 15968;
    public static final int MSSQL_ALPHA_ID_WOUND = 15962;
    public static final int MSSQL_ALPHA_ID_OSTOMY = 15971;
    public static final int MSSQL_ALPHA_ID_DRAIN = 15965;
    public static final int MSSQL_ASSESSMENT_ID = 261261;
    /** used for Google Chrome when click events are out of focus 
     @todo create a abstract class for all webdriver methods */
    public static void click(String id, WebDriver driver) {
        WebElement element = driver.findElement(By.id(id));
        Capabilities cp = ((RemoteWebDriver) driver).getCapabilities();
        
        if (cp.getBrowserName().equals("chrome")) {
            try {
                //((JavascriptExecutor) driver).executeScript(
                //        "arguments[0].scrollIntoView(true);", element);
                int elementPosition = element.getLocation().getY();
                String js = String.format("window.scroll(0, %s)", elementPosition);
                ((JavascriptExecutor)driver).executeScript(js);
                java.io.File scrFile = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
        org.apache.commons.io.FileUtils.copyFile(scrFile, new java.io.File("c:\\test"+id+".jpg"));
            } catch (Exception e) {

            }
        }
        element.click();
    }
}
